//// En son calistirilan script tepeye eklenecek
/*
///////////////////////////////////////////////////////////////////////////////////////////alper 11.06.2013 14:08

ALTER TABLE isolation_test_definition ADD COLUMN "global_id" INTEGER;

///////////////////////////////////////////////////////////////////////////////////////////alper 10.06.2013 11:26

ALTER TABLE isolation_test_definition ADD COLUMN "hassonuc" BOOLEAN DEFAULT FALSE;

///////////////////////////////////////////////////////////////////////////////////////////alper 06.06.2013 15:58

ALTER TABLE isolation_test_definition ADD COLUMN "hasDetay" BOOLEAN DEFAULT FALSE;

////////////////////////////////////////////////////////////////////////////////////////// alper 05.06.2013 20:51
ALTER TABLE planned_isolation ALTER COLUMN standard set data type varchar(255);
ALTER TABLE planned_isolation ALTER COLUMN note set data type varchar(255);
ALTER TABLE planned_isolation ALTER COLUMN izolasyon_sekli set data type varchar(255);
ALTER TABLE planned_isolation ALTER COLUMN kalite set data type varchar(255);
ALTER TABLE planned_isolation ALTER COLUMN renk set data type varchar(255);
ALTER TABLE planned_isolation ALTER COLUMN kumlama_sartlari set data type varchar(255);
ALTER TABLE planned_isolation ALTER COLUMN yuzey_profili set data type varchar(255);
ALTER TABLE planned_isolation ALTER COLUMN yuzey_temizligi set data type varchar(255);
ALTER TABLE planned_isolation ALTER COLUMN ortam_sartlari set data type varchar(255);
*/