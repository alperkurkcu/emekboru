package com.emekboru.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.YetkiMenu;
import com.emekboru.jpa.YetkiMenuKullaniciErisim;
import com.emekboru.jpaman.YetkiMenuKullaniciErisimManager;
import com.emekboru.jpaman.YetkiMenuManager;

public class YetkiUtilViewControl {
	@SuppressWarnings("rawtypes")
	public static boolean sayfaYetkisiVarMi(String sayfaStr,
			HttpServletRequest req) {
		HashMap hmSayfa = getKullaniciYetkiSayfaList(req);
		boolean yetkisiVar = false;

		if (hmSayfa != null)
			yetkisiVar = hmSayfa.containsKey(sayfaStr.replaceFirst("/", ""));

		if (!yetkisiVar) {
			HashMap hmFilter = getYetkiFilterPageList(req);
			if (hmFilter != null) {
				if (hmFilter.containsKey(sayfaStr.replaceFirst("/", ""))) {
					Object deger = hmFilter.get(sayfaStr.replaceFirst("/", ""));
					if (deger == null)
						yetkisiVar = true;
					else
						yetkisiVar = hmSayfa.containsKey(deger.toString());

				} else
					yetkisiVar = false;
			}

		}

		return yetkisiVar;
	}

	public static boolean sayfaYetkisiVarMi2(String sayfaStr,
			SystemUser systemUser) {
		boolean yetkisiVar = true;

		YetkiMenu menu = YetkiMenuManager.getYetkiMenuByAbbr(sayfaStr);
		List<YetkiMenuKullaniciErisim> erisimList = YetkiMenuKullaniciErisimManager
				.getDefinedUserMenuRolesByUserIdAndMenuId(systemUser.getId(),
						menu.getId());
		if (erisimList != null)
			return yetkisiVar;
		return !yetkisiVar;
	}

	@SuppressWarnings("rawtypes")
	public static HashMap getYetkiFilterPageList(HttpServletRequest req) {
		return (HashMap) req.getSession().getAttribute(
				ConstsCore.KULLANICI_YETKI_FILTER_PAGE);
	}

	@SuppressWarnings("rawtypes")
	public static void setYetkiFilterPageList(HashMap sayfa) {
		UtilViewControl.setSessionAttr(ConstsCore.KULLANICI_YETKI_FILTER_PAGE,
				sayfa);
	}

	@SuppressWarnings("rawtypes")
	public static HashMap getKullaniciYetkiSayfaList(HttpServletRequest req) {
		return (HashMap) req.getSession().getAttribute(
				ConstsCore.KULLANICI_YETKI_SAYFA);
	}

	@SuppressWarnings("rawtypes")
	public static void setKullaniciYetkiSayfaList(HashMap sayfa) {
		UtilViewControl.setSessionAttr(ConstsCore.KULLANICI_YETKI_SAYFA, sayfa);
	}

	@SuppressWarnings("rawtypes")
	public static HashMap getKullaniciYetkiActionList() {
		return (HashMap) UtilViewControl
				.getSessionAttr(ConstsCore.KULLANICI_YETKI_ACTION);
	}

	@SuppressWarnings("rawtypes")
	public static void setKullaniciYetkiActionList(HashMap action) {
		UtilViewControl.setSessionAttr(ConstsCore.KULLANICI_YETKI_ACTION,
				action);
	}

	public static String getExceptionTraceString(Throwable e,
			boolean forWebDisplay) {
		StringWriter writer = new StringWriter();
		e.printStackTrace(new PrintWriter(writer));

		if (!forWebDisplay)
			return writer.toString();
		String str = writer.toString();
		StringBuffer webStr = new StringBuffer();
		StringTokenizer st = new StringTokenizer(str, "\n");
		while (st.hasMoreTokens()) {
			webStr.append(st.nextToken());
			webStr.append("<br>");
		}
		return webStr.toString();
	}

	public static UIComponent getUIComponentById(String componentId) {
		FacesContext context = FacesContext.getCurrentInstance();
		UIViewRoot root = context.getViewRoot();
		UIComponent c = findComponent(root, componentId);
		return c;
	}

	/**
	 * Finds component with the given id
	 */
	public static UIComponent findComponent(UIComponent c, String id) {
		if (id.equals(c.getId())) {
			return c;
		}
		Iterator<UIComponent> kids = c.getFacetsAndChildren();
		while (kids.hasNext()) {
			UIComponent found = findComponent(kids.next(), id);
			if (found != null) {
				return found;
			}
		}
		return null;
	}

}
