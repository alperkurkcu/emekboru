package com.emekboru.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;


public class ExcelCellStyles {
	
	private static final String STYLE_FILE_PATH = "C:\\Users\\eldi\\Desktop\\style.xls";
	private HSSFCell leftTopCell;
	private HSSFCell centerTopCell;
	private HSSFCell rightTopCell;
	private HSSFCell leftBottomCell;
	private HSSFCell centerBottomCell;
	private HSSFCell rightBottomCell;
	private HSSFCell resultCell;
	
	public ExcelCellStyles()
	{
		POIFSFileSystem fs;
		try {
			fs = new POIFSFileSystem(new FileInputStream(new  File(STYLE_FILE_PATH)));
			HSSFWorkbook workbook = new HSSFWorkbook(fs);
			HSSFSheet sheet = workbook.getSheetAt(0);
			HSSFRow row = sheet.getRow(0);
			leftTopCell = row.getCell(0);
			centerTopCell = row.getCell(2);
			rightTopCell = row.getCell(4);
			row = sheet.getRow(2);
			leftBottomCell = row.getCell(0);
			centerBottomCell = row.getCell(2);
			rightBottomCell = row.getCell(4);
			row = sheet.getRow(4);
			resultCell = row.getCell(0);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
		}
	}

	/*
	 * 	GETTERS AND SETTERS
	 */
	
	public HSSFCell getLeftTopCell() {
		return leftTopCell;
	}

	public void setLeftTopCell(HSSFCell leftTopCell) {
		this.leftTopCell = leftTopCell;
	}

	public HSSFCell getCenterTopCell() {
		return centerTopCell;
	}

	public void setCenterTopCell(HSSFCell centerTopCell) {
		this.centerTopCell = centerTopCell;
	}

	public HSSFCell getRightTopCell() {
		return rightTopCell;
	}

	public void setRightTopCell(HSSFCell rightTopCell) {
		this.rightTopCell = rightTopCell;
	}

	public HSSFCell getLeftBottomCell() {
		return leftBottomCell;
	}

	public void setLeftBottomCell(HSSFCell leftBottomCell) {
		this.leftBottomCell = leftBottomCell;
	}

	public HSSFCell getCenterBottomCell() {
		return centerBottomCell;
	}

	public void setCenterBottomCell(HSSFCell centerBottomCell) {
		this.centerBottomCell = centerBottomCell;
	}

	public HSSFCell getRightBottomCell() {
		return rightBottomCell;
	}

	public void setRightBottomCell(HSSFCell rightBottomCell) {
		this.rightBottomCell = rightBottomCell;
	}

	public HSSFCell getResultCell() {
		return resultCell;
	}

	public void setResultCell(HSSFCell resultCell) {
		this.resultCell = resultCell;
	}

	public static String getStyleFilePath() {
		return STYLE_FILE_PATH;
	}
}
