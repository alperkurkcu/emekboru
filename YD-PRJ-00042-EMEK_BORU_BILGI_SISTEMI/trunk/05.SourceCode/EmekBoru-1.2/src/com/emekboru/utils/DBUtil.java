package com.emekboru.utils;

import java.sql.Timestamp;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.emekboru.jpa.YetkiLoginLog;

public class DBUtil {

	public static String insertLoginLog(EntityManager em, String userName,
			String sessionId, Timestamp loginTime) {

		EntityTransaction tx = em.getTransaction();

		if (tx.isActive()) {
			em.close();
		}

		try {

			YetkiLoginLog loginLog = new YetkiLoginLog();
			loginLog.setUserName(userName);
			loginLog.setSessionId(sessionId);
			System.out.println(" xxxxxxxxxxxxxxx user name= " + userName
					+ "*** sessionId=" + sessionId + "*****login time="
					+ loginTime);

			loginLog.setGirisZamani(loginTime);
			loginLog.setCikisZamani(null);
			tx.begin();
			em.persist(loginLog);

			tx.commit();
			em.close();
			return null;
		} catch (Exception e) {

			System.err.println("DBUtil.insertLoginLog-HATA");
			e.printStackTrace();
			em.flush();
			tx.rollback();
			return null;
		}
	}

}