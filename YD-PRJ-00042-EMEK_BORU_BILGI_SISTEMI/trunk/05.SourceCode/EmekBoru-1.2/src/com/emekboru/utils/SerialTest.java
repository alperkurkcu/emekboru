/**
 * 
 */
package com.emekboru.utils;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.util.Random;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "serialTest")
@ViewScoped
public class SerialTest {

	public void deneme() {
		main("A");

	}

	public static void main(String AsD) {

		String hexBaslama = "13";
		String hexBitis = "0D";
		StringBuilder outputBaslama = new StringBuilder();
		StringBuilder outputBitis = new StringBuilder();
		for (int i = 0; i < hexBaslama.length(); i += 2) {
			String str = hexBaslama.substring(i, i + 2);
			outputBaslama.append((char) Integer.parseInt(str, 16));
		}
		for (int i = 0; i < hexBitis.length(); i += 2) {
			String str = hexBitis.substring(i, i + 2);
			outputBaslama.append((char) Integer.parseInt(str, 16));
		}

		Random rastgele = new Random();

		String abc = outputBaslama + "UEC 700001	" + rastgele.nextDouble()
				+ "	10675" + outputBitis;
		System.out.println(abc);

		Runtime rt = Runtime.getRuntime();
		Process p = null;
		String portname = "com3:";
		// for Win95 : c:\\windows\\command.com
		// c:\\windows\\command\\mode.com
		String cmd[] = { "c:\\windows\\system32\\cmd.exe", "/c", "start",
				"/min", "c:\\windows\\system32\\mode.com", portname,
				"baud=9600", "parity=n", "data=8", "stop=1", };
		try {
			p = rt.exec(cmd);
			if (p.waitFor() != 0) {
				System.out.println("Error executing command: " + cmd);
				System.exit(-1);
			}
			byte data[] = abc.getBytes();
			FileOutputStream fos = new FileOutputStream(portname);
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			fos.write(data, 0, data.length);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
