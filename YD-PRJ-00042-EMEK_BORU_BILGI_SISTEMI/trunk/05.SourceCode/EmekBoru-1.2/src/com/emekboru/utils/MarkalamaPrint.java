/**
 * 
 */
package com.emekboru.utils;

import java.awt.Color;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.coatingmaterials.KaplamaMalzemeKullanmaPe;
import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiPolietilen;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizGozOlcuSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.coatingmaterials.KaplamaMalzemeKullanmaPeManager;
import com.emekboru.jpaman.kaplamamakinesi.KaplamaMakinesiPolietilenManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizGozOlcuSonucManager;
import com.itextpdf.text.pdf.Barcode128;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "markalamaPrint")
@ViewScoped
public class MarkalamaPrint {

	private Pipe selectedPipe = new Pipe();
	private TestTahribatsizGozOlcuSonuc selectedGozOlcu = new TestTahribatsizGozOlcuSonuc();
	private KaplamaMakinesiPolietilen selectedPe = new KaplamaMakinesiPolietilen();

	private String barkodNo = null;

	private boolean kontrol = false;

	final static String hexBaslama = "13";
	final static String hexBitis = "0D";
	final static String hexAra = "09";

	StringBuilder outputBaslama = new StringBuilder();
	StringBuilder outputBitis = new StringBuilder();
	StringBuilder outputAra = new StringBuilder();

	String markalamayaGidecek = null;

	BigDecimal binSayisi = new BigDecimal("1000");

	private Integer prmVardiya = null;

	public MarkalamaPrint() {
	}

	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		TestTahribatsizGozOlcuSonucManager gozMan = new TestTahribatsizGozOlcuSonucManager();
		FacesContext context = FacesContext.getCurrentInstance();

		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_FATAL,
							prmBarkod
									+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR, MARKALAMA YAPMAYINIZ!",
							null));
			kontrol = false;
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			try {
				selectedGozOlcu = gozMan.getAllTestTahribatsizGozOlcuSonuc(
						selectedPipe.getPipeId()).get(0);
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								prmBarkod
										+ " BARKOD NUMARALI BORU İÇİN GÖZ ÖLÇÜ GİRİLMEMİŞTİR, MARKALAMA YAPMAYINIZ!",
								null));
				kontrol = false;
				return;
			}

			if (selectedGozOlcu.getBoruAgirlik() == null
					|| selectedGozOlcu.getBoruUzunluk() == null) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								prmBarkod
										+ " İÇİN GÖZ OLÇU BİLGİLERİNDE HATA / EKSİK VADIR, MARKALAMA YAPMAYINIZ!",
								null));
				kontrol = false;
				return;
			}
			kontrol = true;
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, prmBarkod
							+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!", null));
		}
	}

	@SuppressWarnings("unused")
	public void degiskenGonder() {

		FacesContext context = FacesContext.getCurrentInstance();

		if (!kontrol) {
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_FATAL,
							" GÖNDERİLEN VERİLER EKSİKTİR, KONTROL EDİNİZ, MARKALAMA YAPMAYINIZ!",
							null));
			return;
		}

		for (int i = 0; i < hexBaslama.length(); i += 2) {
			String str = hexBaslama.substring(i, i + 2);
			outputBaslama.append((char) Integer.parseInt(str, 16));
		}
		for (int i = 0; i < hexBitis.length(); i += 2) {
			String str = hexBitis.substring(i, i + 2);
			outputBitis.append((char) Integer.parseInt(str, 16));
		}

		for (int i = 0; i < hexAra.length(); i += 2) {
			String str = hexAra.substring(i, i + 2);
			outputAra.append((char) Integer.parseInt(str, 16));
		}

		markalamayaGidecek = outputBaslama
				+ selectedGozOlcu.getBoruUzunluk()
						.divide(binSayisi, 2, RoundingMode.HALF_UP).toString()
				+ outputAra
				+ selectedGozOlcu.getBoruAgirlik().toString()
				+ outputAra
				+ Integer.toString(selectedPipe.getPipeIndex())
				+ outputAra
				+ selectedPipe.getRuloPipeLink().getRulo().getRuloDokumNo()
						.toString() + outputBitis;
		System.out.println(markalamayaGidecek);

		Runtime rt = Runtime.getRuntime();
		Process p = null;
		String portname = "com4:";
		String cmd[] = { "c:\\windows\\system32\\cmd.exe", "/c", "start",
				"/min", "c:\\windows\\system32\\mode.com", portname,
				"baud=9600", "parity=n", "data=8", "stop=1", };
		try {
			p = rt.exec(cmd);
			if (p.waitFor() != 0) {
				System.out.println("Error executing command: " + cmd);
				System.exit(-1);
			}
			byte data[] = markalamayaGidecek.getBytes();
			FileOutputStream fos = new FileOutputStream(portname);
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			fos.write(data, 0, data.length);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL,
					" COM PORT BAĞLANTI HATASI, MARKALAMA YAPMAYINIZ!", null));
			return;
		}
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				"MARKALA İÇİN HAZIR, MARKALAMA YAPABİLİRSİNİZ!", null));

		markalamayaGidecek = null;
		outputBaslama = new StringBuilder();
		outputBitis = new StringBuilder();
		outputAra = new StringBuilder();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void tanapBarkoduBas() {

		if (selectedPipe == null || selectedPipe.getPipeId() == null
				|| selectedPipe.getPipeId() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		ReportUtil reportUtil = new ReportUtil();

		Barcode128 code128 = new Barcode128();
		code128.setCode(selectedPipe.getPipeBarkodNo());
		code128.setChecksumText(true);
		code128.setX(0.5f);

		java.awt.Image image = code128.createAwtImage(Color.BLACK, Color.WHITE);

		try {
			HSSFWorkbook hssfWorkbook = reportUtil
					.getWorkbookFromFilePath("/reportformats/barcode/tanapBarcode.xls");

			reportUtil.insertImageToWorkbook(hssfWorkbook, image, "testImage",
					image.getWidth(null) * 2, image.getHeight(null) * 5);

			reportUtil.writeWorkbookToFile(
					hssfWorkbook,
					"/reportformats/barcode/tanapBarcode-"
							+ selectedPipe.getPipeIndex() + ".xls");
		} catch (IOException e1) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"BARKOD İMAJI OLUŞTURULAMADI, HATA!", null));
			e1.printStackTrace();
		}

		try {

			KaplamaMalzemeKullanmaPeManager peManager = new KaplamaMalzemeKullanmaPeManager();
			List<KaplamaMalzemeKullanmaPe> peList = new ArrayList<KaplamaMalzemeKullanmaPe>();

			if (peKontrol()) {
				prmVardiya = UtilInsCore.findShift(selectedPe
						.getKaplamaBaslamaZamani().getHours());
				String prmTarih1 = new SimpleDateFormat("yyyy-MM-dd")
						.format(selectedPe.getKaplamaBaslamaZamani());
				peList = peManager.findByDateVardiya(prmTarih1, prmVardiya);
			} else {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"PE KAPLAMASI BULUNAMADI, BARKODU KONTROL EDİNİZ!!",
						null));
			}
			Map dataMap = new HashMap();
			dataMap.put("barcodeInfo", selectedPipe);
			dataMap.put(
					"boruUzunluk",
					selectedPipe.getTestTahribatsizGozOlcuSonuc()
							.getBoruUzunluk()
							.divide(binSayisi, 2, RoundingMode.HALF_UP));
			if (peList == null || peList.size() == 0) {
				dataMap.put("kaplama", "Bare");
			} else if (peList.size() > 0) {
				dataMap.put("kaplama", peList.get(0).getCoatRawMaterial()
						.getPartiNo());
			}
			dataMap.put("nominalEtKalinligi", selectedPipe.getSalesItem()
					.getThickness());

			reportUtil.jxlsExportAsXls(
					dataMap,
					"/reportformats/barcode/tanapBarcode-"
							+ selectedPipe.getPipeIndex() + ".xls", "");

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"BARKOD BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"BARKOD EXCELİ OLUŞTURULAMADI, HATA!", null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings("static-access")
	public boolean peKontrol() {

		KaplamaMakinesiPolietilenManager manager = new KaplamaMakinesiPolietilenManager();

		if (manager.getAllKaplamaMakinesiPolietilen(selectedPipe.getPipeId())
				.size() > 0) {
			selectedPe = manager.getAllKaplamaMakinesiPolietilen(
					selectedPipe.getPipeId()).get(0);
			return true;
		} else {
			selectedPe = new KaplamaMakinesiPolietilen();
			return false;
		}
	}

	public void barcodeYazdir() {

		try {
			FileOperations.getInstance().streamFile(
					FacesContext
							.getCurrentInstance()
							.getExternalContext()
							.getRealPath(
									"/reportformats/barcode/tanapBarcode-"
											+ selectedPipe.getPipeIndex()
											+ ".xls"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// setters getters

	/**
	 * @return the selectedPipe
	 */
	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	/**
	 * @param selectedPipe
	 *            the selectedPipe to set
	 */
	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	/**
	 * @return the barkodNo
	 */
	public String getBarkodNo() {
		return barkodNo;
	}

	/**
	 * @param barkodNo
	 *            the barkodNo to set
	 */
	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	/**
	 * @return the selectedGozOlcu
	 */
	public TestTahribatsizGozOlcuSonuc getSelectedGozOlcu() {
		return selectedGozOlcu;
	}

	/**
	 * @param selectedGozOlcu
	 *            the selectedGozOlcu to set
	 */
	public void setSelectedGozOlcu(TestTahribatsizGozOlcuSonuc selectedGozOlcu) {
		this.selectedGozOlcu = selectedGozOlcu;
	}
}
