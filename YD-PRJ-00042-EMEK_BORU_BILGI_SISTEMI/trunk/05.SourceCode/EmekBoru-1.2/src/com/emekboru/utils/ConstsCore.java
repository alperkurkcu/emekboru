package com.emekboru.utils;



public class ConstsCore {
    public static final String TIMEZONE = "Europe/Istanbul";
    public static final String DATE_FORMAT = "dd/MM/yyyy";
    public static final String DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss";
    public static final String DATE_TIME_FORMAT2 = "yyyy-MM-dd HH:mm:ss";
    public static final String TIME_FORMAT = "HH:mm:ss";
    public static final String TIME_FORMAT2 = "HH:mm";
    public static final String TIME_FORMAT3 = "HHmm";
    public static final String DATE_TIME_FORMAT_NOKTA = "dd.MM.yyyy HH:mm";
    public static final String PROPERTIES_PARAMETER_STR = "$$";
    public static final String PROPERTIES_PARAMETER_STR_URL = "%24%24";
    public static final String SEPERATE_UNDER_SCORE = "_";
    public static final String SEPERATE_EQUAL  = "=";
    public static final String SEPERATE_COMMA = ",";
    public static final String SEPERATE_AMPERSAND = "&";
    public static final String SEPERATE_AT = "@";
    public static final String SEPERATE_DOT = ".";
    public static final String SEPERATE_TWO_UP_DOT = ":";
    public static final String SEPERATE_QUESTION = "?";
    public static final String SEPERATE_COMMA_DOT_URL = "%3B";
    public static final String SEPERATE_COMMA_DOT = ";";
    public static final String KULLANICI_YETKI_SAYFA = "Yetki.KullaniciYetkiSayfa";
    public static final String KULLANICI_YETKI_FILTER_PAGE = "Yetki.KullaniciYetkiFilterPage";
    public static final String KULLANICI_YETKI_ACTION = "Yetki.KullaniciYetkiAction";
    public static final String MESSAGE_AKTIF="A";
    public static final String MESSAGE_PASIF="P";
    public static final String IC_BETON="internal1";
    public static final String IC_BOYA="internal2";
    public static final String DIS_POLIETILEN="external1";
    public static final String DIS_POLIPROPILEN="external2";
    public static final String DIS_BOYA="external3";
	public static final String DIS_KAPLAMA = "diskaplama";
	public static final String IC_KAPLAMA = "ickaplama";
    
}
