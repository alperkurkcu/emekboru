/**
 * 
 */
package com.emekboru.utils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * @author kursat
 * 
 */
public class ExchangeRateUtil {

	// private final String webServiceUrl = "http://xn--dviz-5qa.com/tcmbxml/";
	private final String webServiceUrl = "http://www.tcmb.gov.tr/kurlar/today.xml";
	private Document doc;

	public ExchangeRateUtil() {
		super();
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			doc = db.parse(new URL(webServiceUrl).openStream());

		} catch (SAXException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private NodeList evaluateXpathExpression(String expression)
			throws XPathExpressionException {

		NodeList nodeList = null;

		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		XPathExpression expr = xpath.compile(expression);

		nodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);

		return nodeList;
	}

	/**
	 * 
	 * @param curreny
	 * @options USD, CAD, DKK, SEK, CHF, NOK, JPY, SAR, KWD, AUD, EUR, GBP, RUB,
	 *          RON, PKR, IRR, CNY, BGN
	 * @param exchangeType
	 * @options ForexBuying, ForexSelling, BanknoteBuying, BanknoteSelling
	 * @return
	 */
	public String getExchangeRate(String curreny, String exchangeType)
			throws XPathExpressionException {

		NodeList nodeList = evaluateXpathExpression("//Currency[@Kod = '"
				+ curreny + "']/" + exchangeType);
		Node node = nodeList.item(0);

		return node.getTextContent().toString().trim();
	}

}
