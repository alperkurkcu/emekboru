/**
 * 
 */
package com.emekboru.utils.lazymodels;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.emekboru.jpa.satinalma.SatinalmaAmbarMalzemeTalepForm;
import com.emekboru.jpaman.satinalma.SatinalmaAmbarMalzemeTalepFormManager;

/**
 * @author KRKC
 * 
 */
public class LazyAmbarTalepDataModel extends
		LazyDataModel<SatinalmaAmbarMalzemeTalepForm> {

	private static final long serialVersionUID = 1L;

	private List<SatinalmaAmbarMalzemeTalepForm> datasource;
	private int pageSize;
	private int rowIndex;
	private int rowCount;
	private SatinalmaAmbarMalzemeTalepFormManager manager = new SatinalmaAmbarMalzemeTalepFormManager();
	private int prmEmployeeId = 0;
	private boolean prmOlder = false;

	public LazyAmbarTalepDataModel(int prmUserId, boolean prmOlder) {
		this.prmEmployeeId = prmUserId;
		this.prmOlder = prmOlder;
	}

	public List<SatinalmaAmbarMalzemeTalepForm> load(int first, int pageSize,
			String sortField, SortOrder sortOrder, Map<String, String> filters) {
		ArrayList<String> alanlar = new ArrayList<String>();
		ArrayList<String> veriler = new ArrayList<String>();

		// filter
		for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
			try {
				String filterProperty = it.next();
				String filterValue = filters.get(filterProperty);
				if (filterValue != null) {
					alanlar.add(filterProperty);
					veriler.add(filterValue);
				}
			} catch (Exception e) {
			}
		}
		List<SatinalmaAmbarMalzemeTalepForm> list = null;
		if (sortField != null) {
			list = manager.findRangeFilter(first, pageSize, alanlar, veriler,
					sortField, sortOrder, prmEmployeeId, prmOlder);
		} else {
			list = manager.findRangeFilter(first, pageSize, alanlar, veriler,
					null, null, prmEmployeeId, prmOlder);
		}
		setRowCount(manager.countTotalRecord(prmEmployeeId, prmOlder));
		return list;
	}

	@Override
	public boolean isRowAvailable() {
		if (datasource == null)
			return false;
		int index = rowIndex % pageSize;
		return index >= 0 && index < datasource.size();
	}

	@Override
	public Object getRowKey(SatinalmaAmbarMalzemeTalepForm talep) {
		return talep.getId() + "";
	}

	@Override
	public SatinalmaAmbarMalzemeTalepForm getRowData() {
		if (datasource == null)
			return null;
		int index = rowIndex % pageSize;
		if (index > datasource.size()) {
			return null;
		}
		return datasource.get(index);
	}

	@Override
	public SatinalmaAmbarMalzemeTalepForm getRowData(String rowKey) {
		if (datasource == null)
			return null;
		for (SatinalmaAmbarMalzemeTalepForm talep : datasource) {
			if ((talep.getId() + "").equals(rowKey))
				return talep;
		}
		return null;
	}

	public List<SatinalmaAmbarMalzemeTalepForm> getDatasource() {
		return datasource;
	}

	public void setDatasource(List<SatinalmaAmbarMalzemeTalepForm> datasource) {
		this.datasource = datasource;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getRowIndex() {
		return rowIndex;
	}

	public void setRowIndex(int rowIndex) {
		this.rowIndex = rowIndex;
	}

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setWrappedData(Object list) {
		this.datasource = (List<SatinalmaAmbarMalzemeTalepForm>) list;
	}

	/**
	 * Returns wrapped data
	 * 
	 * @return
	 */
	@Override
	public Object getWrappedData() {
		return datasource;
	}

	/**
	 * @return the manager
	 */
	public SatinalmaAmbarMalzemeTalepFormManager getManager() {
		return manager;
	}

	/**
	 * @param manager
	 *            the manager to set
	 */
	public void setManager(SatinalmaAmbarMalzemeTalepFormManager manager) {
		this.manager = manager;
	}

}