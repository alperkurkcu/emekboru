/**
 * 
 */
package com.emekboru.utils.lazymodels;

import java.lang.reflect.Field;
import java.util.Comparator;

import org.primefaces.model.SortOrder;

import com.emekboru.jpa.sales.order.SalesItem;

/**
 * @author KRKC
 * 
 */
public class LazySalesItemSorter implements Comparator<SalesItem> {

	private String sortField;

	private SortOrder sortOrder;

	public LazySalesItemSorter(String sortField, SortOrder sortOrder) {
		this.sortField = sortField;
		this.sortOrder = sortOrder;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public int compare(SalesItem data1, SalesItem data2) {
		try {

			Field field = data1.getClass().getDeclaredField(this.sortField);
			field.setAccessible(true);
			Object value1 = field.get(data1);

			Field field2 = data2.getClass().getDeclaredField(this.sortField);
			field2.setAccessible(true);
			Object value2 = field2.get(data2);

			int value = ((Comparable) value1).compareTo(value2);

			return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}

}
