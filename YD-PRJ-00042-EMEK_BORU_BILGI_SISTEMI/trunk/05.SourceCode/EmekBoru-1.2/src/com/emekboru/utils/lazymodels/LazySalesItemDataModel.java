/**
 * 
 */
package com.emekboru.utils.lazymodels;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.sales.order.SalesItemManager;

/**
 * @author KRKC
 * 
 */
public class LazySalesItemDataModel extends LazyDataModel<SalesItem> {

	private static final long serialVersionUID = 1L;

	private List<SalesItem> datasource;
	private int pageSize;
	private int rowIndex;
	private int rowCount;
	private SalesItemManager manager = new SalesItemManager();

	public LazySalesItemDataModel() {
	}

	public List<SalesItem> load(int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, String> filters) {
		ArrayList<String> alanlar = new ArrayList<String>();
		ArrayList<String> veriler = new ArrayList<String>();

		// filter
		for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
			try {
				String filterProperty = it.next();
				String filterValue = filters.get(filterProperty);
				if (filterValue != null) {
					alanlar.add(filterProperty);
					veriler.add(filterValue);
				}
			} catch (Exception e) {
			}
		}
		List<SalesItem> list = null;
		if (sortField != null) {
			list = manager.findRangeFilter(first, pageSize, alanlar, veriler,
					sortField, sortOrder);
		} else {
			list = manager.findRangeFilter(first, pageSize, alanlar, veriler,
					null, null);
		}
		setRowCount(manager.countTotalRecord());
		return list;
	}

	@Override
	public boolean isRowAvailable() {
		if (datasource == null)
			return false;
		int index = rowIndex % pageSize;
		return index >= 0 && index < datasource.size();
	}

	@Override
	public Object getRowKey(SalesItem salesItem) {
		return salesItem.getItemId() + "";
	}

	@Override
	public SalesItem getRowData() {
		if (datasource == null)
			return null;
		int index = rowIndex % pageSize;
		if (index > datasource.size()) {
			return null;
		}
		return datasource.get(index);
	}

	@Override
	public SalesItem getRowData(String rowKey) {
		if (datasource == null)
			return null;
		for (SalesItem salesItem : datasource) {
			if ((salesItem.getItemId() + "").equals(rowKey))
				return salesItem;
		}
		return null;
	}

	public List<SalesItem> getDatasource() {
		return datasource;
	}

	public void setDatasource(List<SalesItem> datasource) {
		this.datasource = datasource;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getRowIndex() {
		return rowIndex;
	}

	public void setRowIndex(int rowIndex) {
		this.rowIndex = rowIndex;
	}

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	/**
	 * @return the manager
	 */
	public SalesItemManager getManager() {
		return manager;
	}

	/**
	 * @param manager
	 *            the manager to set
	 */
	public void setManager(SalesItemManager manager) {
		this.manager = manager;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setWrappedData(Object list) {
		this.datasource = (List<SalesItem>) list;
	}

	/**
	 * Returns wrapped data
	 * 
	 * @return
	 */
	@Override
	public Object getWrappedData() {
		return datasource;
	}

}