/**
 * 
 */
package com.emekboru.utils.lazymodels;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.emekboru.jpa.stok.StokGirisCikis;
import com.emekboru.jpaman.stok.StokGirisCikisManager;

/**
 * @author KRKC
 * 
 */
public class LazyStokDataModel extends LazyDataModel<StokGirisCikis> {

	private static final long serialVersionUID = 1L;

	private List<StokGirisCikis> datasource;
	private int pageSize;
	private int rowIndex;
	private int rowCount;
	private StokGirisCikisManager manager = new StokGirisCikisManager();

	public LazyStokDataModel() {
	}

	public List<StokGirisCikis> load(int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, String> filters) {
		ArrayList<String> alanlar = new ArrayList<String>();
		ArrayList<String> veriler = new ArrayList<String>();

		// filter
		for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
			try {
				String filterProperty = it.next();
				String filterValue = filters.get(filterProperty);
				if (filterValue != null) {
					alanlar.add(filterProperty);
					veriler.add(filterValue);
				}
			} catch (Exception e) {
			}
		}
		List<StokGirisCikis> list = null;
		if (sortField != null) {
			list = manager.findRangeFilter(first, pageSize, alanlar, veriler,
					sortField, sortOrder);
		} else {
			list = manager.findRangeFilter(first, pageSize, alanlar, veriler,
					null, null);
		}
		setRowCount(manager.countTotalRecord());
		return list;
	}

	@Override
	public boolean isRowAvailable() {
		if (datasource == null)
			return false;
		int index = rowIndex % pageSize;
		return index >= 0 && index < datasource.size();
	}

	@Override
	public Object getRowKey(StokGirisCikis stokGirisCikis) {
		return stokGirisCikis.getId() + "";
	}

	@Override
	public StokGirisCikis getRowData() {
		if (datasource == null)
			return null;
		int index = rowIndex % pageSize;
		if (index > datasource.size()) {
			return null;
		}
		return datasource.get(index);
	}

	@Override
	public StokGirisCikis getRowData(String rowKey) {
		if (datasource == null)
			return null;
		for (StokGirisCikis stokGirisCikis : datasource) {
			if ((stokGirisCikis.getId() + "").equals(rowKey))
				return stokGirisCikis;
		}
		return null;
	}

	public List<StokGirisCikis> getDatasource() {
		return datasource;
	}

	public void setDatasource(List<StokGirisCikis> datasource) {
		this.datasource = datasource;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getRowIndex() {
		return rowIndex;
	}

	public void setRowIndex(int rowIndex) {
		this.rowIndex = rowIndex;
	}

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	/**
	 * @return the manager
	 */
	public StokGirisCikisManager getManager() {
		return manager;
	}

	/**
	 * @param manager
	 *            the manager to set
	 */
	public void setManager(StokGirisCikisManager manager) {
		this.manager = manager;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setWrappedData(Object list) {
		this.datasource = (List<StokGirisCikis>) list;
	}

	/**
	 * Returns wrapped data
	 * 
	 * @return
	 */
	@Override
	public Object getWrappedData() {
		return datasource;
	}

}
