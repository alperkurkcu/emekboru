/**
 * 
 */
package com.emekboru.utils.lazymodels;

import java.util.Comparator;

import org.primefaces.model.SortOrder;

import com.emekboru.jpa.satinalma.SatinalmaAmbarMalzemeTalepForm;

/**
 * @author KRKC
 * 
 */
public class LazyAmbarTalepSorter implements
		Comparator<SatinalmaAmbarMalzemeTalepForm> {

	private String sortField;

	private SortOrder sortOrder;

	public LazyAmbarTalepSorter(String sortField, SortOrder sortOrder) {
		this.sortField = sortField;
		this.sortOrder = sortOrder;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public int compare(SatinalmaAmbarMalzemeTalepForm data1,
			SatinalmaAmbarMalzemeTalepForm data2) {
		try {
			Object value1 = SatinalmaAmbarMalzemeTalepForm.class.getField(
					this.sortField).get(data1);
			Object value2 = SatinalmaAmbarMalzemeTalepForm.class.getField(
					this.sortField).get(data2);

			int value = ((Comparable) value1).compareTo(value2);

			return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}
}
