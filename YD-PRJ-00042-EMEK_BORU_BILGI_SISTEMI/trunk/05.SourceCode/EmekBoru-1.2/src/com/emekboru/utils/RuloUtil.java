package com.emekboru.utils;

import java.util.Calendar;
import java.util.List;

import com.emekboru.jpa.rulo.Rulo;
import com.emekboru.jpaman.rulo.RuloManager;

public class RuloUtil {
	public static Rulo getRuloKimlikId(RuloManager ruloManager, Rulo rulo) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(rulo.getRuloTarih());
		String ruloYear = Integer.toString(cal.get(Calendar.YEAR));
		rulo.setRuloYil(ruloYear);
		List<Rulo> rulos = ruloManager.getYear(ruloYear);
		if (rulos.size() == 0) {
			rulo.setRuloKimlikId("0001");
		} else {
			// -1 -2 -3 gibi dilimlenmiş rulolarda cıkan sıkıntı giderildi.
			Integer kimlikId;
			int spacePos = rulos.get(0).getRuloKimlikId().indexOf("-");
			if (spacePos > 1) {
				kimlikId = Integer.parseInt(rulos.get(0).getRuloKimlikId()
						.substring(0, spacePos));
			} else {
				kimlikId = Integer.parseInt(rulos.get(0).getRuloKimlikId());
			}
			kimlikId++;
			if (kimlikId < 10)
				rulo.setRuloKimlikId("000" + kimlikId);
			else if (kimlikId > 9 && kimlikId < 100) {
				rulo.setRuloKimlikId("00" + kimlikId);
			} else if (kimlikId > 99 && kimlikId < 1000) {
				rulo.setRuloKimlikId("0" + kimlikId);
			} else if (kimlikId > 999 && kimlikId < 10000) {
				rulo.setRuloKimlikId(kimlikId.toString());
			}
		}
		return rulo;
	}

}
