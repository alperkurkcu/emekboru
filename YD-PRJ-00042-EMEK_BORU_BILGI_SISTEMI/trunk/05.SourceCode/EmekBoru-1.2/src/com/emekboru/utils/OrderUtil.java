package com.emekboru.utils;

import java.util.Calendar;
import java.util.List;

import com.emekboru.jpa.Order;
import com.emekboru.jpaman.OrderManager;

public class OrderUtil {
	public static Order getOrderKimlikId(OrderManager orderManager, Order order) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(order.getStartDate());
		String orderYear = Integer.toString(cal.get(Calendar.YEAR));
		order.setOrderYear(Integer.parseInt(orderYear));
		List<Order> orders = orderManager.getOrderByOrderYear(orderYear);
		if (orders != null && orders.size() > 0) {
			order.setOrderNumber(orders.get(0).getOrderNumber() + 1);
		} else {
			order.setOrderNumber(1);
		}
		return order;
	}
}
