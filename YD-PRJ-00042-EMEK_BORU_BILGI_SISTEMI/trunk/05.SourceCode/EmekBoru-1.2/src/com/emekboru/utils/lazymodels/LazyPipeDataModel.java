/**
 * 
 */
package com.emekboru.utils.lazymodels;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpaman.PipeManager;

/**
 * @author KRKC
 * 
 */
public class LazyPipeDataModel extends LazyDataModel<Pipe> {

	private static final long serialVersionUID = 1L;

	private List<Pipe> datasource;
	private int pageSize;
	private int rowIndex;
	private int rowCount;
	private PipeManager manager = new PipeManager();
	private int prmItemId = 0;

	public LazyPipeDataModel(int prmItemId) {
		this.prmItemId = prmItemId;
	}

	public List<Pipe> load(int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, String> filters) {
		ArrayList<String> alanlar = new ArrayList<String>();
		ArrayList<String> veriler = new ArrayList<String>();

		// filter
		for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
			try {
				String filterProperty = it.next();
				String filterValue = filters.get(filterProperty);
				if (filterValue != null) {
					alanlar.add(filterProperty);
					veriler.add(filterValue);
				}
			} catch (Exception e) {
			}
		}
		List<Pipe> list = null;
		if (sortField != null) {
			list = manager.findRangeFilter(first, pageSize, alanlar, veriler,
					sortField, sortOrder, prmItemId);
		} else {
			list = manager.findRangeFilter(first, pageSize, alanlar, veriler,
					null, null, prmItemId);
		}
		setRowCount(manager.countTotalRecord(prmItemId));
		return list;
	}

	@Override
	public boolean isRowAvailable() {
		if (datasource == null)
			return false;
		int index = rowIndex % pageSize;
		return index >= 0 && index < datasource.size();
	}

	@Override
	public Object getRowKey(Pipe pipe) {
		return pipe.getPipeId() + "";
	}

	@Override
	public Pipe getRowData() {
		if (datasource == null)
			return null;
		int index = rowIndex % pageSize;
		if (index > datasource.size()) {
			return null;
		}
		return datasource.get(index);
	}

	@Override
	public Pipe getRowData(String rowKey) {
		if (datasource == null)
			return null;
		for (Pipe pipe : datasource) {
			if ((pipe.getPipeId() + "").equals(rowKey))
				return pipe;
		}
		return null;
	}

	public List<Pipe> getDatasource() {
		return datasource;
	}

	public void setDatasource(List<Pipe> datasource) {
		this.datasource = datasource;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getRowIndex() {
		return rowIndex;
	}

	public void setRowIndex(int rowIndex) {
		this.rowIndex = rowIndex;
	}

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setWrappedData(Object list) {
		this.datasource = (List<Pipe>) list;
	}

	/**
	 * Returns wrapped data
	 * 
	 * @return
	 */
	@Override
	public Object getWrappedData() {
		return datasource;
	}

	/**
	 * @return the manager
	 */
	public PipeManager getManager() {
		return manager;
	}

	/**
	 * @param manager
	 *            the manager to set
	 */
	public void setManager(PipeManager manager) {
		this.manager = manager;
	}

}
