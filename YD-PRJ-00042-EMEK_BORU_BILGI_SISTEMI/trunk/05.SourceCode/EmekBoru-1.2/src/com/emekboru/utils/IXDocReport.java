/**
 * 
 */
package com.emekboru.utils;

/**
 * @author Alper K
 *
 */

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Map;

import fr.opensagres.xdocreport.converter.IConverter;
import fr.opensagres.xdocreport.converter.MimeMapping;
import fr.opensagres.xdocreport.converter.Options;
import fr.opensagres.xdocreport.converter.XDocConverterException;
import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.core.io.XDocArchive;
import fr.opensagres.xdocreport.document.ProcessState;
import fr.opensagres.xdocreport.document.dump.DumperOptions;
import fr.opensagres.xdocreport.document.dump.IDumper;
import fr.opensagres.xdocreport.document.preprocessor.IXDocPreprocessor;
import fr.opensagres.xdocreport.template.FieldsExtractor;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.ITemplateEngine;
import fr.opensagres.xdocreport.template.formatter.FieldsMetadata;

public abstract interface IXDocReport extends Serializable {
	public abstract String getKind();

	public abstract String getId();

	public abstract void setId(String paramString);

	public abstract void load(InputStream paramInputStream) throws IOException,
			XDocReportException;

	public abstract void save(ProcessState paramProcessState,
			OutputStream paramOutputStream) throws IOException,
			XDocReportException;

	public abstract void saveEntry(String paramString,
			ProcessState paramProcessState, OutputStream paramOutputStream)
			throws IOException, XDocReportException;

	public abstract void setDocumentArchive(XDocArchive paramXDocArchive)
			throws IOException, XDocReportException;

	public abstract XDocArchive getOriginalDocumentArchive();

	public abstract XDocArchive getPreprocessedDocumentArchive();

	public abstract void setTemplateEngine(ITemplateEngine paramITemplateEngine);

	public abstract ITemplateEngine getTemplateEngine();

	public abstract void setFieldsMetadata(FieldsMetadata paramFieldsMetadata);

	public abstract IContext createContext() throws XDocReportException;

	public abstract IContext createContext(Map<String, Object> paramMap)
			throws XDocReportException;

	public abstract void process(IContext paramIContext,
			OutputStream paramOutputStream) throws XDocReportException,
			IOException;

	public abstract void process(Map<String, Object> paramMap,
			OutputStream paramOutputStream) throws XDocReportException,
			IOException;

	public abstract void process(IContext paramIContext, String paramString,
			OutputStream paramOutputStream) throws XDocReportException,
			IOException;

	public abstract void process(Map<String, Object> paramMap,
			String paramString, OutputStream paramOutputStream)
			throws XDocReportException, IOException;

	public abstract IConverter getConverter(Options paramOptions)
			throws XDocConverterException;

	public abstract void convert(IContext paramIContext, Options paramOptions,
			OutputStream paramOutputStream) throws XDocReportException,
			XDocConverterException, IOException;

	public abstract void convert(Map<String, Object> paramMap,
			Options paramOptions, OutputStream paramOutputStream)
			throws XDocReportException, XDocConverterException, IOException;

	public abstract MimeMapping getMimeMapping();

	public abstract void setData(String paramString, Object paramObject);

	public abstract void clearData(String paramString);

	public abstract <T> T getData(String paramString);

	public abstract void extractFields(FieldsExtractor paramFieldsExtractor,
			ITemplateEngine paramITemplateEngine) throws XDocReportException,
			IOException;

	public abstract void extractFields(FieldsExtractor paramFieldsExtractor)
			throws XDocReportException, IOException;

	public abstract void setCacheOriginalDocument(boolean paramBoolean);

	public abstract boolean isPreprocessed();

	public abstract long getLastModified();

	public abstract FieldsMetadata getFieldsMetadata();

	public abstract FieldsMetadata createFieldsMetadata();

	public abstract void addPreprocessor(String paramString,
			IXDocPreprocessor paramIXDocPreprocessor);

	public abstract void removePreprocessor(String paramString);

	public abstract void removeAllPreprocessors();

	public abstract void preprocess() throws XDocReportException, IOException;

	public abstract IDumper getDumper(DumperOptions paramDumperOptions)
			throws XDocReportException;

	public abstract void dump(IContext paramIContext,
			InputStream paramInputStream, DumperOptions paramDumperOptions,
			OutputStream paramOutputStream) throws IOException,
			XDocReportException;

	public abstract void dump(IContext paramIContext,
			DumperOptions paramDumperOptions, OutputStream paramOutputStream)
			throws IOException, XDocReportException;
}
