/**
 * 
 */
package com.emekboru.utils;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 * @author kursat
 * @todo Set up properties.
 * 
 */
public class FileOperations {

	private static FileOperations instance = null;
	private Logger logger = Logger.getAnonymousLogger();

	private static final int BUFSIZE = 4096;

	public static synchronized FileOperations getInstance() {
		if (instance == null) {
			instance = new FileOperations();
		}
		return instance;
	}

	public FileOperations() {

	}

	public String uploadFile(String path, String fileName,
			FileUploadEvent fileUploadEvent) {

		logger.warning(getCallerClassName() + ":" + getCallerMethodName()
				+ " uploading a file...");

		UploadedFile uploadedFile = fileUploadEvent.getFile();
		String uploadedFileName = uploadedFile.getFileName();
		String uploadedFileCheckedName = this.checkFileName(uploadedFileName);
		String uploadedFileExtention = FilenameUtils
				.getExtension(uploadedFileCheckedName);
		File theFile = null;

		if (fileName == null)
			fileName = createUniqueFileName();

		fileName = fileName + "." + uploadedFileExtention;

		OutputStream outputStream = null;
		// InputStream inputStream = null;

		try {
			theFile = new File(path + fileName);

			if (theFile.createNewFile()) {

				outputStream = new FileOutputStream(theFile);
				IOUtils.copy(uploadedFile.getInputstream(), outputStream);
				outputStream.close();

				logger.warning("Done!");
			} else {
				// TODO growl
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}

		return theFile.getName();
	}

	public StreamedContent downloadFile(String path, String fileName) {

		logger.warning(getCallerClassName() + ":" + getCallerMethodName()
				+ " downloading a file...");

		StreamedContent downloadedFile = null;

		try {
			if (fileName != null) {
				RandomAccessFile randomAccessFile = new RandomAccessFile(path
						+ fileName, "r");
				byte[] downloadByte = new byte[(int) randomAccessFile.length()];
				randomAccessFile.read(downloadByte);
				InputStream inputStream = new ByteArrayInputStream(downloadByte);

				String extention = FilenameUtils.getExtension(fileName);
				String contentType = "text/plain";
				if (extention.contentEquals("jpg")
						|| extention.contentEquals("png")
						|| extention.contentEquals("gif")) {
					contentType = "image/" + contentType;
				} else if (extention.contentEquals("doc")
						|| extention.contentEquals("docx")) {
					contentType = "application/msword";
				} else if (extention.contentEquals("xls")
						|| extention.contentEquals("xlsx")) {
					contentType = "application/vnd.ms-excel";
				} else if (extention.contentEquals("pdf")) {
					contentType = "application/pdf";
				}
				downloadedFile = new DefaultStreamedContent(inputStream,
						"contentType", fileName);
				randomAccessFile.close();
				inputStream.close();

				logger.warning("Done!");
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}
		return downloadedFile;
	}

	private String createUniqueFileName() {

		String DATE_FORMAT = "yyyyMMdd_HHmmss-SSS";
		java.text.SimpleDateFormat simpleDateFormat = new java.text.SimpleDateFormat(
				DATE_FORMAT);

		String date = simpleDateFormat.format(new java.util.Date());

		java.util.Random random = new java.util.Random();
		String randomInt = "" + random.nextInt(9999);

		return "file_" + date + "_" + randomInt;
	}

	private String checkFileName(String fileName) {

		fileName = fileName.replace(" ", "_");
		fileName = fileName.replace("ç", "c");
		fileName = fileName.replace("Ç", "C");
		fileName = fileName.replace("ð", "g");
		fileName = fileName.replace("Ð", "G");
		fileName = fileName.replace("Ý", "i");
		fileName = fileName.replace("ý", "i");
		fileName = fileName.replace("ö", "o");
		fileName = fileName.replace("Ö", "O");
		fileName = fileName.replace("þ", "s");
		fileName = fileName.replace("Þ", "S");
		fileName = fileName.replace("ü", "u");
		fileName = fileName.replace("Ü", "U");

		return fileName;
	}

	private String getCallerMethodName() {
		StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
		StackTraceElement e = stacktrace[3];
		String callerMethodName = e.getMethodName();

		return callerMethodName;
	}

	private String getCallerClassName() {

		StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
		StackTraceElement e = stacktrace[3];
		String callerClassName = e.getClassName();

		return callerClassName;
	}

	public void deleteFile(String path, String fileName) {

		File file = new File(path + fileName);
		if (file.exists()) {
			file.delete();
		}
	}

	public Boolean exists(String path, String fileName) {

		File file = new File(path + fileName);
		if (file.exists())
			return true;
		else
			return false;
	}

	public void streamFile(String filePath) throws IOException {

		HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext
				.getCurrentInstance().getExternalContext().getResponse();
		File file = new File(filePath);
		int length = 0;
		ServletOutputStream servletOutputStream = httpServletResponse
				.getOutputStream();
		ExternalContext context = FacesContext.getCurrentInstance()
				.getExternalContext();
		String mimetype = context.getMimeType(filePath);

		// sets response content type
		if (mimetype == null) {
			mimetype = "application/octet-stream";
		}
		httpServletResponse.setContentType(mimetype);
		httpServletResponse.setContentLength((int) file.length());
		String fileName = (new File(filePath)).getName();

		// sets HTTP header
		httpServletResponse.setHeader("Content-Disposition",
				"attachment; filename=\"" + fileName + "\"");

		byte[] byteBuffer = new byte[BUFSIZE];
		DataInputStream dataInputStream = new DataInputStream(
				new FileInputStream(file));

		// reads the file's bytes and writes them to the response stream
		while ((dataInputStream != null)
				&& ((length = dataInputStream.read(byteBuffer)) != -1)) {
			servletOutputStream.write(byteBuffer, 0, length);
		}

		dataInputStream.close();
		servletOutputStream.close();

		FacesContext.getCurrentInstance().responseComplete();
	}

}
