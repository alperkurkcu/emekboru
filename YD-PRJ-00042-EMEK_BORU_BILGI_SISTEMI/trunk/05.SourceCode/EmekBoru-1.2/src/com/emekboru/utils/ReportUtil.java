/**
 * 
 */
package com.emekboru.utils;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRPptxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jxls.exception.ParsePropertyException;
import net.sf.jxls.transformer.XLSTransformer;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.util.IOUtils;

/**
 * @author kursat
 * 
 */
@SuppressWarnings("deprecation")
public class ReportUtil {

	/**
	 * 
	 */
	public ReportUtil() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void jrExportAsXlsx(Collection<?> dataList, String reportPath)
			throws JRException, IOException {

		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
				dataList);

		String realReportPath = getRealFilePath(reportPath);

		JasperPrint jasperPrint = JasperFillManager.fillReport(realReportPath,
				hashMap, beanCollectionDataSource);

		HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext
				.getCurrentInstance().getExternalContext().getResponse();
		httpServletResponse.addHeader("Content-disposition",
				"attachment; filename=report.xlsx");
		ServletOutputStream servletOutputStream = httpServletResponse
				.getOutputStream();

		JRXlsxExporter exporter = new JRXlsxExporter();
		exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM,
				servletOutputStream);

		exporter.exportReport();

		FacesContext.getCurrentInstance().responseComplete();
	}

	public void jrExportAsDocx(Collection<?> dataList, String reportPath)
			throws JRException, IOException {

		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
				dataList);

		String realReportPath = getRealFilePath(reportPath);

		JasperPrint jasperPrint = JasperFillManager.fillReport(realReportPath,
				hashMap, beanCollectionDataSource);

		HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext
				.getCurrentInstance().getExternalContext().getResponse();
		httpServletResponse.addHeader("Content-disposition",
				"attachment; filename=report.docx");
		ServletOutputStream servletOutputStream = httpServletResponse
				.getOutputStream();

		JRDocxExporter exporter = new JRDocxExporter();
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
				servletOutputStream);
		exporter.exportReport();

		FacesContext.getCurrentInstance().responseComplete();
	}

	public void jrExportAsOdt(Collection<?> dataList, String reportPath)
			throws JRException, IOException {

		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
				dataList);

		String realReportPath = getRealFilePath(reportPath);

		JasperPrint jasperPrint = JasperFillManager.fillReport(realReportPath,
				hashMap, beanCollectionDataSource);

		HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext
				.getCurrentInstance().getExternalContext().getResponse();
		httpServletResponse.addHeader("Content-disposition",
				"attachment; filename=report.odt");
		ServletOutputStream servletOutputStream = httpServletResponse
				.getOutputStream();

		JROdtExporter exporter = new JROdtExporter();
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
				servletOutputStream);
		exporter.exportReport();

		FacesContext.getCurrentInstance().responseComplete();
	}

	public void jrExportAsPptx(Collection<?> dataList, String reportPath)
			throws JRException, IOException {

		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
				dataList);

		String realReportPath = getRealFilePath(reportPath);

		JasperPrint jasperPrint = JasperFillManager.fillReport(realReportPath,
				hashMap, beanCollectionDataSource);

		HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext
				.getCurrentInstance().getExternalContext().getResponse();
		httpServletResponse.addHeader("Content-disposition",
				"attachment; filename=report.pptx");
		ServletOutputStream servletOutputStream = httpServletResponse
				.getOutputStream();

		JRPptxExporter exporter = new JRPptxExporter();
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
				servletOutputStream);
		exporter.exportReport();

		FacesContext.getCurrentInstance().responseComplete();
	}

	public void jrExportAsPdf(Collection<?> dataList, String reportPath)
			throws JRException, IOException {

		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(
				dataList);

		String realReportPath = getRealFilePath(reportPath);

		JasperPrint jasperPrint = JasperFillManager.fillReport(realReportPath,
				hashMap, beanCollectionDataSource);

		HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext
				.getCurrentInstance().getExternalContext().getResponse();
		httpServletResponse.addHeader("Content-disposition",
				"attachment; filename=report.pdf");
		ServletOutputStream servletOutputStream = httpServletResponse
				.getOutputStream();

		JRPdfExporter exporter = new JRPdfExporter();
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,
				servletOutputStream);
		exporter.exportReport();

		FacesContext.getCurrentInstance().responseComplete();
	}

	private String getRealFilePath(String filePath) {

		String realFilePath = FacesContext.getCurrentInstance()
				.getExternalContext().getRealPath(filePath);

		return realFilePath;
	}

	@SuppressWarnings("rawtypes")
	public void jxlsExportAsXls(Map dataMap, String templatePath,
			String appendFilename) {

		String realFilePath = getRealFilePath(templatePath);
		String realOutFilePath = getRealFilePath(templatePath.replace(".",
				appendFilename + "."));

		XLSTransformer transformer = new XLSTransformer();

		try {
			transformer.transformXLS(realFilePath, dataMap, realOutFilePath);
		} catch (ParsePropertyException e) {
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			FileOperations.getInstance().streamFile(realOutFilePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author kursat
	 * @desc Örnektir. Gerekirse kullanılabilir.
	 * @throws IOException
	 */
	public void poiTraverse() throws IOException {

		FileInputStream fileInputStream = new FileInputStream(new File(
				getRealFilePath("/reportformats/test/test.xls")));

		HSSFWorkbook hssfWorkbook = new HSSFWorkbook(fileInputStream);

		for (int i = 0; i < hssfWorkbook.getNumberOfSheets(); i++) {
			HSSFSheet hssfSheet = hssfWorkbook.getSheetAt(i);

			for (Row row : hssfSheet) {
				for (Cell cell : row) {
					final String IMG_PREFIX = "#IMG#";
					if (cell.toString().startsWith(IMG_PREFIX)) {
						cell.setCellValue("");
						String imagePath = getRealFilePath("/reportformats/test/test.png");
						File imageFile = new File(imagePath);
						if (imageFile.exists()) {
							FileInputStream isi = new FileInputStream(imageFile);
							byte[] imgBytes = IOUtils.toByteArray(isi);
							int pictureIdx = hssfWorkbook.addPicture(imgBytes,
									HSSFWorkbook.PICTURE_TYPE_PNG);
							CreationHelper helper = hssfWorkbook
									.getCreationHelper();

							Drawing drawing = hssfWorkbook.getSheetAt(0)
									.createDrawingPatriarch();
							ClientAnchor anchor = helper.createClientAnchor();
							anchor.setCol1(cell.getColumnIndex());
							anchor.setRow1(cell.getRowIndex());
							Picture pict = drawing.createPicture(anchor,
									pictureIdx);
							pict.resize();
							isi.close();
						}
					}
				}
			}

		}
	} // Method ending

	public HSSFWorkbook getWorkbookFromFilePath(String filePath)
			throws IOException {

		FileInputStream fileInputStream = new FileInputStream(new File(
				getRealFilePath(filePath)));

		return new HSSFWorkbook(fileInputStream);
	}

	public HSSFWorkbook insertImageToWorkbook(HSSFWorkbook hssfWorkbook,
			java.awt.Image image, String markOnFile, int width, int height)
			throws IOException {

		for (int i = 0; i < hssfWorkbook.getNumberOfSheets(); i++) {
			HSSFSheet sheet = hssfWorkbook.getSheetAt(i);

			for (Row row : sheet) {
				for (Cell cell : row) {
					final String IMG_PREFIX = "${" + markOnFile + "}";
					if (cell.toString().startsWith(IMG_PREFIX)) {

						int colIndex = cell.getColumnIndex();
						int rowIndex = cell.getRowIndex();
						cell.setCellValue("");

						int pictureIdx = hssfWorkbook.addPicture(this
								.getImageBytes(image.getScaledInstance(width,
										height, Image.SCALE_SMOOTH)),
								HSSFWorkbook.PICTURE_TYPE_PNG);

						CreationHelper helper = hssfWorkbook
								.getCreationHelper();

						Drawing drawing = sheet.createDrawingPatriarch();

						ClientAnchor anchor = helper.createClientAnchor();

						// set top-left corner for the image
						anchor.setCol1(colIndex);
						anchor.setRow1(rowIndex);

						Picture pict = drawing
								.createPicture(anchor, pictureIdx);
						pict.resize();
					}

				}
			}
		}

		return hssfWorkbook;
	}

	public Boolean writeWorkbookToFile(HSSFWorkbook hssfWorkbook,
			String filePath) throws IOException {

		FileOutputStream fileOut = new FileOutputStream(
				getRealFilePath(filePath));
		hssfWorkbook.write(fileOut);
		fileOut.close();

		return Boolean.TRUE;
	}

	private byte[] getImageBytes(Image image) throws IOException {

		// Convert BufferedImage
		int width = image.getWidth(null);
		int height = image.getHeight(null);
		BufferedImage bufferedImage = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics2d = bufferedImage.createGraphics();
		graphics2d.drawImage(image, 0, 0, null);

		// Convert byte[]
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(bufferedImage, "png", baos);
		baos.flush();
		byte[] imageBytes = baos.toByteArray();
		baos.close();

		return imageBytes;
	}
}
