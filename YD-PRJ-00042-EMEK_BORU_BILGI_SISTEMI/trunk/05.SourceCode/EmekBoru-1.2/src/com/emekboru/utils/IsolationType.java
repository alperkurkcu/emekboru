package com.emekboru.utils;

public enum IsolationType {

	IC_KAPLAMA(1), DIS_KAPLAMA(2);

	public void setType(int type) {
		this.type = type;
	}

	private int type;

	private IsolationType(int type) {

		this.type = type;
	}

	public int getType() {
		return type;
	}
}
