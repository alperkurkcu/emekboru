package com.emekboru.utils.lazymodels;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.emekboru.jpa.rulo.Rulo;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.rulo.RuloManager;

public class LazyRuloDataModel extends LazyDataModel<Rulo> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<Rulo> datasource;
	private int pageSize;
	private int rowIndex;
	private int rowCount;
	private RuloManager ruloManager = new RuloManager();
	private SalesItem prmSalesItem = new SalesItem();

	public LazyRuloDataModel() {
		this.prmSalesItem = new SalesItem();
	}

	public LazyRuloDataModel(SalesItem salesItem) {
		this.prmSalesItem = salesItem;
	}

	public List<Rulo> load(int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, String> filters) {
		ArrayList<String> alanlar = new ArrayList<String>();
		ArrayList<String> veriler = new ArrayList<String>();

		// filter
		for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
			try {
				String filterProperty = it.next();
				String filterValue = filters.get(filterProperty);
				if (filterValue != null) {
					alanlar.add(filterProperty);
					veriler.add(filterValue);
				}
			} catch (Exception e) {
			}
		}
		List<Rulo> rulos = null;
		if (sortField != null) {
			rulos = ruloManager.findRangeFilter(first, pageSize, alanlar,
					veriler, sortField, sortOrder, prmSalesItem);
		} else {
			rulos = ruloManager.findRangeFilter(first, pageSize, alanlar,
					veriler, null, null, prmSalesItem);
		}

		if (prmSalesItem.getItemId() > 0) {
			setRowCount(ruloManager.countTotalRecord(prmSalesItem));
		} else {
			setRowCount(ruloManager.countTotalRecord());
		}
		return rulos;
	}

	@Override
	public boolean isRowAvailable() {
		if (datasource == null)
			return false;
		int index = rowIndex % pageSize;
		return index >= 0 && index < datasource.size();
	}

	@Override
	public Object getRowKey(Rulo rulo) {
		return rulo.getRuloId().toString();
	}

	@Override
	public Rulo getRowData() {
		if (datasource == null)
			return null;
		int index = rowIndex % pageSize;
		if (index > datasource.size()) {
			return null;
		}
		return datasource.get(index);
	}

	@Override
	public Rulo getRowData(String rowKey) {
		if (datasource == null)
			return null;
		for (Rulo rulo : datasource) {
			if (rulo.getRuloId().toString().equals(rowKey))
				return rulo;
		}
		return null;
	}

	public List<Rulo> getDatasource() {
		return datasource;
	}

	public void setDatasource(List<Rulo> datasource) {
		this.datasource = datasource;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getRowIndex() {
		return rowIndex;
	}

	public void setRowIndex(int rowIndex) {
		this.rowIndex = rowIndex;
	}

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	public RuloManager getRuloManager() {
		return ruloManager;
	}

	public void setRuloManager(RuloManager ruloManager) {
		this.ruloManager = ruloManager;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setWrappedData(Object list) {
		this.datasource = (List<Rulo>) list;
	}

	/**
	 * Returns wrapped data
	 * 
	 * @return
	 */
	@Override
	public Object getWrappedData() {
		return datasource;
	}

}
