package com.emekboru.utils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.beanutils.PropertyUtils;

public class UtilInsCore {

	public static Timestamp getTarihZaman() {
		TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-4"));
		return new Timestamp(System.currentTimeMillis());
	}

	public static Timestamp getYesterday() {
		TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-4"));
		return new Timestamp(System.currentTimeMillis() - 86400000);
	}

	public static void setProperties(Object fromObj, boolean b) {
		PropertyDescriptor pFrom[] = PropertyUtils
				.getPropertyDescriptors(fromObj.getClass());
		for (int i = 0; i < pFrom.length; i++) {
			if (!pFrom[i].getName().equals("class")) {
				// Object fromObjField =
				// PropertyUtils.getProperty(fromObj,pFrom[i].getName());
				try {
					PropertyUtils.setProperty(fromObj, pFrom[i].getName(), b);
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public static void writeProperties(Object fromObj) {
		PropertyDescriptor pFrom[] = PropertyUtils
				.getPropertyDescriptors(fromObj.getClass());

		for (int i = 0; i < pFrom.length; i++) {
			if (!pFrom[i].getName().equals("class")) {
				System.out.println(pFrom[i].getName() + " : "
						+ pFrom[i].getValue(pFrom[i].getName()));
			}
		}
	}

	@SuppressWarnings("deprecation")
	public static Timestamp yesterdayFirst() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

		// Create a calendar object with today date. Calendar is in java.util
		// pakage.
		Calendar calendar = Calendar.getInstance();

		// Move calendar to yesterday
		calendar.add(Calendar.DATE, -1);
		calendar.set(calendar.getTime().getYear() + 1900, calendar.getTime()
				.getMonth(), calendar.getTime().getDate(), 00, 00, 00);

		// Get current date of calendar which point to the yesterday now
		Date yesterday = calendar.getTime();

		Timestamp ts = Timestamp.valueOf(dateFormat.format(yesterday));

		return ts;
	}

	@SuppressWarnings("deprecation")
	public static Timestamp yesterdayLast() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

		// Create a calendar object with today date. Calendar is in java.util
		// pakage.
		Calendar calendar = Calendar.getInstance();

		// Move calendar to yesterday
		calendar.add(Calendar.DATE, -1);
		calendar.set(calendar.getTime().getYear() + 1900, calendar.getTime()
				.getMonth(), calendar.getTime().getDate(), 23, 59, 59);

		// Get current date of calendar which point to the yesterday now
		Date yesterday = calendar.getTime();

		Timestamp ts = Timestamp.valueOf(dateFormat.format(yesterday));

		return ts;
	}

	public static Integer findShift(Integer prmHour) {

		if (8 <= prmHour && prmHour < 20) {
			return 1;
		} else {
			return 2;
		}
	}
}
