package com.emekboru.utils;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class UtilViewControl {

	public static Object getObjectFromRequestParam(String name) {
		Object obj = null;
		FacesContext context = FacesContext.getCurrentInstance();
		obj = ((HttpServletRequest) context.getExternalContext().getRequest())
				.getParameter(name);
		return obj;
	}

	public static Object getRequestAttr(String name) {
		Object obj = null;
		FacesContext context = FacesContext.getCurrentInstance();
		obj = ((HttpServletRequest) context.getExternalContext().getRequest())
				.getAttribute(name);
		return obj;
	}

	public static Object getRequestParam(String name) {
		Object obj = null;
		FacesContext context = FacesContext.getCurrentInstance();
		obj = ((HttpServletRequest) context.getExternalContext().getRequest())
				.getParameter(name);
		return obj;
	}

	public static final String sessionCount = "06";

	public static Object getSessionAttr(String name) {
		Object obj = null;
		FacesContext context = FacesContext.getCurrentInstance();
		if (context != null)
			obj = ((HttpSession) context.getExternalContext().getSession(false))
					.getAttribute(name);
		return obj;
	}

	public static void setRequestAttr(String name, Object obj) {
		FacesContext context = FacesContext.getCurrentInstance();
		((HttpServletRequest) context.getExternalContext().getRequest())
				.setAttribute(name, obj);
	}

	public static void removeRequestAttr(String name) {
		FacesContext context = FacesContext.getCurrentInstance();
		((HttpServletRequest) context.getExternalContext().getRequest())
				.removeAttribute(name);
	}

	public static void setSessionAttr(String name, Object obj) {
		FacesContext context = FacesContext.getCurrentInstance();
		((HttpSession) context.getExternalContext().getSession(false))
				.setAttribute(name, obj);
	}

	public static String getSessionId() {
		FacesContext context = FacesContext.getCurrentInstance();
		Object obj = context.getExternalContext().getSession(true);
		javax.servlet.http.HttpSession h = (javax.servlet.http.HttpSession) obj;
		String sessionID = h.getId();

		/*************************/
		SessionTimeoutNotifier sto = new SessionTimeoutNotifier();
		h.setAttribute("SessionObjName", sto);
		// h.setMaxInactiveInterval(100);
		/*************************/
		return sessionID;
	}

	public static HttpSession getSession() {
		FacesContext context = FacesContext.getCurrentInstance();
		javax.servlet.http.HttpSession h = null;
		if (context != null) {
			Object obj = context.getExternalContext().getSession(true);
			h = (javax.servlet.http.HttpSession) obj;
		}
		return h;
	}

	public static void getNewSession() {
		FacesContext context = FacesContext.getCurrentInstance();
		Object obj = context.getExternalContext().getSession(true);
		javax.servlet.http.HttpSession h = (javax.servlet.http.HttpSession) obj;
		if (!h.isNew()) {
			h.invalidate();
			obj = context.getExternalContext().getSession(true);
			h = (javax.servlet.http.HttpSession) obj;
		}
	}

	public static void closeSession() {
		FacesContext context = FacesContext.getCurrentInstance();
		Object obj = context.getExternalContext().getSession(true);
		javax.servlet.http.HttpSession h = (javax.servlet.http.HttpSession) obj;
		try {
			h.invalidate();

		} catch (Throwable t) {
			System.out.println("hata var");
		}
	}

	public static String getIP() {
		FacesContext context = FacesContext.getCurrentInstance();
		Object obj = context.getExternalContext().getRequest();
		javax.servlet.http.HttpServletRequest h = (javax.servlet.http.HttpServletRequest) obj;
		String clientIp = h.getHeader("Client-IP");

		String remoteHost = UtilViewControl.getRequestParam("RemoteHost") != null ? UtilViewControl
				.getRequestParam("RemoteHost").toString() : "";
		String remoteAddress = h.getRemoteAddr();

		System.out.println("----> clientIp:" + clientIp + "  remoteHost:"
				+ remoteHost + " remoteAdress:" + remoteAddress);

		if (clientIp == null || clientIp.trim().equals(""))
			clientIp = UtilViewControl.getRequestParam("RemoteHost") == null ? h
					.getRemoteAddr() : UtilViewControl.getRequestParam(
					"RemoteHost").toString();
		clientIp = clientIp.replace(" ", "");
		return clientIp;
	}

	public static Object getFormWithNoAction(String name) {
		return getObjectFromValue(name);
	}

	@SuppressWarnings("deprecation")
	public static Object getObjectFromValue(String name) {
		Object obj = null;
		FacesContext context = FacesContext.getCurrentInstance();
		if (context == null)
			return null;
		obj = context.getApplication().createValueBinding("#{" + name + "}")
				.getValue(context);
		return obj;
	}

}
