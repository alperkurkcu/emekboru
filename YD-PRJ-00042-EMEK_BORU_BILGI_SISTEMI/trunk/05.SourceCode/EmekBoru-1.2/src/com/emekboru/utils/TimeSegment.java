package com.emekboru.utils;

import java.util.Calendar;
import java.util.Date;

public class TimeSegment {
	
	private Date startDate;
	private Date endDate;
	
	public TimeSegment(Date date)
	{
		startDate = getFirstDayOfYear(date);
		endDate = getLastDayOfYear(date);
	}
	
	public TimeSegment(Integer year)
	{
		startDate = getFirstDayOfYear(year);
		endDate = getLastDayOfYear(year);
	}
	
	public Date getFirstDayOfYear(Date date)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(calendar.get(Calendar.YEAR), Calendar.JANUARY, 1,0,0);
		date = calendar.getTime();
		System.out.println(date);
		return date;
	}

	public Date getLastDayOfYear(Date date)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(calendar.get(Calendar.YEAR), Calendar.DECEMBER, 31,23,59);
		date = calendar.getTime();
		System.out.println(date);
		return date;
	}

	public Date getFirstDayOfYear(Integer year)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(year, Calendar.JANUARY, 1,0,0);
		Date date = calendar.getTime();
		return date;
	}

	public Date getLastDayOfYear(Integer year)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(year, Calendar.DECEMBER, 31,23,59);
		Date date = calendar.getTime();
		return date;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
