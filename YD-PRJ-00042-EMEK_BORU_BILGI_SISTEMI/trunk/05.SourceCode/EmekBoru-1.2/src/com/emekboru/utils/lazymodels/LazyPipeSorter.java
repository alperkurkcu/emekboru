/**
 * 
 */
package com.emekboru.utils.lazymodels;

import java.util.Comparator;

import org.primefaces.model.SortOrder;

import com.emekboru.jpa.Pipe;

/**
 * @author KRKC
 * 
 */
public class LazyPipeSorter implements Comparator<Pipe> {

	private String sortField;

	private SortOrder sortOrder;

	public LazyPipeSorter(String sortField, SortOrder sortOrder) {
		this.sortField = sortField;
		this.sortOrder = sortOrder;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public int compare(Pipe data1, Pipe data2) {
		try {
			Object value1 = Pipe.class.getField(this.sortField).get(data1);
			Object value2 = Pipe.class.getField(this.sortField).get(data2);

			int value = ((Comparable) value1).compareTo(value2);

			return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}
}
