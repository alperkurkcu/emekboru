package com.emekboru.utils;

import java.util.Calendar;
import java.util.Date;

public class DateTrans {
	
	public static Calendar calendar = Calendar.getInstance();
	
	
	public static Integer getYearOfDate(Date date){
		
		calendar.setTime(date);
		Integer year = calendar.get(Calendar.YEAR);
		return year;
	}
	
	public static Integer getMonthOfDate(Date date){
		
		calendar.setTime(date);
		Integer month = calendar.get(Calendar.MONTH);
		return month;
	}
	
	public static Integer getDayOfDate(Date date){
		
		calendar.setTime(date);
		Integer day = calendar.get(Calendar.DAY_OF_MONTH);
		return day;
	}
	
	public static Integer getHourOfDate(Date date){
		
		calendar.setTime(date);
		Integer day = calendar.get(Calendar.HOUR_OF_DAY);
		return day;
	}
	
	public static Integer getMinOfDate(Date date){
		
		calendar.setTime(date);
		Integer day = calendar.get(Calendar.MINUTE);
		return day;
	}
	
	public static String getSimpleFormatDate(Date date){
		
		String year = getYearOfDate(date).toString();
		Integer monthInteger = getMonthOfDate(date) + 1;
		String month = monthInteger.toString();
		String day= getDayOfDate(date).toString();
		String hour = getHourOfDate(date).toString();
		String min = getMinOfDate(date).toString();
		String simpleDate = year + " / " + month + " / " +day + "   " +hour + " : " +min;
		return simpleDate;
	}
}
