package com.emekboru.utils;

import java.io.IOException;

import javax.faces.bean.ManagedProperty;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.emekboru.beans.login.UserCredentialsBean;


public class LoginFilter implements Filter {
	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpSession session = req.getSession();
        System.out.println("URL: "+req.getRequestURL());
       System.out.println("Full Name: "+userBean.getUserFullName());
        if (session.getAttribute("user") != null || req.getRequestURI().endsWith("login.xhtml")) {
            chain.doFilter(request, response);
        } else {
            HttpServletResponse res = (HttpServletResponse) response;
            res.sendRedirect("/EmekBoru-1.2/login.xhtml");
           
            return;
        }

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    //	filterConfig.getServletContext().setAttribute("user",userBean.getUser().getEmployee().getFirstname()+" "+userBean.getUser().getEmployee().getLastname() );
    }

    @Override
    public void destroy() {
    }
}