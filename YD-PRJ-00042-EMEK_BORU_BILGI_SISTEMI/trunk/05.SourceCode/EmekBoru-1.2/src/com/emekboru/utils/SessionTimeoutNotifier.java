package com.emekboru.utils;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.apache.log4j.Logger;

import com.emekboru.config.ConfigBean;
import com.emekboru.jpaman.YetkiLoginLogManager;

@SuppressWarnings("serial")
public class SessionTimeoutNotifier implements HttpSessionBindingListener,
		Serializable {
	@EJB
	private ConfigBean config;

	private static final Logger logger = Logger
			.getLogger(SessionTimeoutNotifier.class.getName());

	public SessionTimeoutNotifier() {
		System.out
				.println("SessionTimeoutNotifier.SessionTimeoutNotifier CREATE EDILDI");
		logger.info("SessionTimeoutNotifier.SessionTimeoutNotifier CREATE EDILDI");
	}

	public void valueBound(HttpSessionBindingEvent event) {
		System.out.println("SESSION STARTED:" + event.getSession().getId()
				+ "Time:" + System.currentTimeMillis());
		logger.info("SESSION STARTED:" + event.getSession().getId() + "Time:"
				+ System.currentTimeMillis());
		event.getSession().setMaxInactiveInterval(
				YetkiConstInsCore.SESSION_TIME);// 15 dakika

	}

	public void valueUnbound(HttpSessionBindingEvent event) {
		System.out.println("SESSION TIME OUT OLDU:"
				+ event.getSession().getId() + " Time:"
				+ System.currentTimeMillis());

		try {
			YetkiLoginLogManager yetkiLoginLogManager = new YetkiLoginLogManager();
			yetkiLoginLogManager.updateTimeOutOlanlarLogOut(event.getSession()
					.getId());
		} catch (Throwable t) {
			logger.info("SESSION TIME OUT OLDU: Ancak bu timeout alirken hata "
					+ event.getSession().getId() + "Time:"
					+ System.currentTimeMillis() + "err:" + t.getMessage());
		}
		logger.info("SESSION TIME OUT OLDU:" + event.getSession().getId()
				+ "Time:" + System.currentTimeMillis());

	}
}
