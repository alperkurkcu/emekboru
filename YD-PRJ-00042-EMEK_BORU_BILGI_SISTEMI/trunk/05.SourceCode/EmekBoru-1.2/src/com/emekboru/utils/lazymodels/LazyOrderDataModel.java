package com.emekboru.utils.lazymodels;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.emekboru.jpa.Order;
import com.emekboru.jpaman.OrderManager;

public class LazyOrderDataModel extends LazyDataModel<Order> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<Order> datasource;
	private int pageSize;
	private int rowIndex;
	private int rowCount;
	private OrderManager orderManager = new OrderManager();

	public List<Order> load(int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, String> filters) {
		ArrayList<String> alanlar = new ArrayList<String>();
		ArrayList<String> veriler = new ArrayList<String>();

		// filter
		for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
			try {
				String filterProperty = it.next();
				String filterValue = filters.get(filterProperty);
				if (filterValue != null) {
					alanlar.add(filterProperty);
					veriler.add(filterValue);
				}
			} catch (Exception e) {
			}
		}
		List<Order> orders = null;
		if (sortField != null) {
			orders = orderManager.findRangeFilter(first, pageSize, alanlar,
					veriler, sortField, sortOrder);
		} else {
			orders = orderManager.findRangeFilter(first, pageSize, alanlar,
					veriler, null, null);
		}
		setRowCount(orderManager.countTotalRecord());
		return orders;
	}

	@Override
	public boolean isRowAvailable() {
		if (datasource == null)
			return false;
		int index = rowIndex % pageSize;
		return index >= 0 && index < datasource.size();
	}

	@Override
	public Object getRowKey(Order order) {
		return order.getOrderId() + "";
	}

	@Override
	public Order getRowData() {
		if (datasource == null)
			return null;
		int index = rowIndex % pageSize;
		if (index > datasource.size()) {
			return null;
		}
		return datasource.get(index);
	}

	@Override
	public Order getRowData(String rowKey) {
		if (datasource == null)
			return null;
		for (Order order : datasource) {
			if ((order.getOrderId() + "").equals(rowKey))
				return order;
		}
		return null;
	}

	public List<Order> getDatasource() {
		return datasource;
	}

	public void setDatasource(List<Order> datasource) {
		this.datasource = datasource;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getRowIndex() {
		return rowIndex;
	}

	public void setRowIndex(int rowIndex) {
		this.rowIndex = rowIndex;
	}

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	public OrderManager getOrderManager() {
		return orderManager;
	}

	public void setOrderManager(OrderManager orderManager) {
		this.orderManager = orderManager;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setWrappedData(Object list) {
		this.datasource = (List<Order>) list;
	}

	/**
	 * Returns wrapped data
	 * 
	 * @return
	 */
	@Override
	public Object getWrappedData() {
		return datasource;
	}
}
