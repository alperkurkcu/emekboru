package com.emekboru.jpa.satinalma;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.emekboru.jpa.Employee;
import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the satinalma_malzeme_hizmet_talep_formu database
 * table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "SatinalmaMalzemeHizmetTalepFormu.findAll", query = "SELECT r FROM SatinalmaMalzemeHizmetTalepFormu r order by r.eklemeZamani DESC"),
		@NamedQuery(name = "SatinalmaMalzemeHizmetTalepFormu.findAllByFormId", query = "SELECT r FROM SatinalmaMalzemeHizmetTalepFormu r where r.satinalmaAmbarMalzemeTalepForm.id=:prmFormId order by r.eklemeZamani desc"),
		@NamedQuery(name = "SatinalmaMalzemeHizmetTalepFormu.findByUserId", query = "SELECT r FROM SatinalmaMalzemeHizmetTalepFormu r where r.ekleyenKullanici=:prmEmployeeId order by r.eklemeZamani desc"),
		@NamedQuery(name = "SatinalmaMalzemeHizmetTalepFormu.findOnayBekleyenler", query = "SELECT r FROM SatinalmaMalzemeHizmetTalepFormu r where r.supervisor.employeeId=:prmEmployeeId and r.supervisorApproval=0 and r.tamamlanma=true order by r.eklemeZamani desc"),
		@NamedQuery(name = "SatinalmaMalzemeHizmetTalepFormu.findGMOnayBekleyenler", query = "SELECT r FROM SatinalmaMalzemeHizmetTalepFormu r where r.generalManager.employeeId=:prmEmployeeId and r.gmApproval=0 and r.tamamlanma=true order by r.eklemeZamani desc"),
		@NamedQuery(name = "SatinalmaMalzemeHizmetTalepFormu.findHistory", query = "SELECT r FROM SatinalmaMalzemeHizmetTalepFormu r where r.supervisor.employeeId=:prmEmployeeId and r.tamamlanma=true and r.supervisorApproval!=0 order by r.eklemeZamani desc"),
		@NamedQuery(name = "SatinalmaMalzemeHizmetTalepFormu.findSatinalmayaDusenler", query = "SELECT r FROM SatinalmaMalzemeHizmetTalepFormu r where r.satinalmaDurum.id=30 order by r.eklemeZamani desc"),
		@NamedQuery(name = "SatinalmaMalzemeHizmetTalepFormu.findYonlendirilenler", query = "SELECT r FROM SatinalmaMalzemeHizmetTalepFormu r, SatinalmaTakipDosyalarIcerik s WHERE r.id=s.satinalmaMalzemeHizmetTalepFormu.id AND s.gorevliPersonel.employeeId=:prmUserId GROUP BY r.id ORDER BY s.id") })
@Table(name = "satinalma_malzeme_hizmet_talep_formu")
public class SatinalmaMalzemeHizmetTalepFormu implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SATINALMA_MALZEME_HIZMET_TALEP_FORMU_ID_GENERATOR", sequenceName = "SATINALMA_MALZEME_HIZMET_TALEP_FORMU_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SATINALMA_MALZEME_HIZMET_TALEP_FORMU_ID_GENERATOR")
	@Column(name = "ID")
	private Integer id;

	@Column(name = "butce_kodu")
	private String butceKodu;

	@Column(name = "ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser user;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "proje_adi")
	private String projeAdi;

	@Column(name = "sayi")
	private Integer sayi;

	// @Column(name = "durum")
	// private Integer durum;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "durum", referencedColumnName = "ID", insertable = false)
	private SatinalmaDurum satinalmaDurum;

	// @Column(name = "ambar_id")
	// private Integer ambarId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ambar_id", referencedColumnName = "ID", insertable = false)
	private SatinalmaAmbarMalzemeTalepForm satinalmaAmbarMalzemeTalepForm;

	@Column(name = "supervisor_approval")
	private Integer supervisorApproval;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "supervisor_id", referencedColumnName = "employee_id", insertable = false)
	private Employee supervisor;

	@Column(name = "gm_approval")
	private Integer gmApproval;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "gm_id", referencedColumnName = "employee_id", insertable = false)
	private Employee generalManager;

	@Column(name = "satinalma_approval")
	private Integer satinalmaApproval;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "satinalma_id", referencedColumnName = "employee_id", insertable = false)
	private Employee satinalma;

	@Column(name = "finans_approval")
	private Integer finansApproval;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "finans_id", referencedColumnName = "employee_id", insertable = false)
	private Employee finansManager;

	@Transient
	public static final int PENDING = 0;
	@Transient
	public static final int APPROVED = 1;
	@Transient
	public static final int DECLINED = 2;

	@Column(name = "tamamlanma")
	private boolean tamamlanma = false;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "onay_tarihi", nullable = false)
	private Date onayTarihi;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "talep_tarihi", nullable = false)
	private Date talepTarihi;

	@Column(name = "form_sayisi")
	private String formSayisi;

	@Column(name = "aciklama")
	private String aciklama;

	@Column(name = "rev_aciklama")
	private String revAciklama;

	public SatinalmaMalzemeHizmetTalepFormu() {

	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getButceKodu() {
		return this.butceKodu;
	}

	public void setButceKodu(String butceKodu) {
		this.butceKodu = butceKodu;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getProjeAdi() {
		return this.projeAdi;
	}

	public void setProjeAdi(String projeAdi) {
		this.projeAdi = projeAdi;
	}

	public Integer getSayi() {
		return this.sayi;
	}

	public void setSayi(Integer sayi) {
		this.sayi = sayi;
	}

	public SystemUser getUser() {
		return user;
	}

	public void setUser(SystemUser user) {
		this.user = user;
	}

	public SatinalmaDurum getSatinalmaDurum() {
		return satinalmaDurum;
	}

	public void setSatinalmaDurum(SatinalmaDurum satinalmaDurum) {
		this.satinalmaDurum = satinalmaDurum;
	}

	public SatinalmaAmbarMalzemeTalepForm getSatinalmaAmbarMalzemeTalepForm() {
		return satinalmaAmbarMalzemeTalepForm;
	}

	public void setSatinalmaAmbarMalzemeTalepForm(
			SatinalmaAmbarMalzemeTalepForm satinalmaAmbarMalzemeTalepForm) {
		this.satinalmaAmbarMalzemeTalepForm = satinalmaAmbarMalzemeTalepForm;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getSupervisorApproval() {
		return supervisorApproval;
	}

	public void setSupervisorApproval(Integer supervisorApproval) {
		this.supervisorApproval = supervisorApproval;
	}

	public Employee getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(Employee supervisor) {
		this.supervisor = supervisor;
	}

	public Integer getGmApproval() {
		return gmApproval;
	}

	public void setGmApproval(Integer gmApproval) {
		this.gmApproval = gmApproval;
	}

	public Employee getGeneralManager() {
		return generalManager;
	}

	public void setGeneralManager(Employee generalManager) {
		this.generalManager = generalManager;
	}

	public Integer getSatinalmaApproval() {
		return satinalmaApproval;
	}

	public void setSatinalmaApproval(Integer satinalmaApproval) {
		this.satinalmaApproval = satinalmaApproval;
	}

	public Employee getSatinalma() {
		return satinalma;
	}

	public void setSatinalma(Employee satinalma) {
		this.satinalma = satinalma;
	}

	public Integer getFinansApproval() {
		return finansApproval;
	}

	public void setFinansApproval(Integer finansApproval) {
		this.finansApproval = finansApproval;
	}

	public Employee getFinansManager() {
		return finansManager;
	}

	public void setFinansManager(Employee finansManager) {
		this.finansManager = finansManager;
	}

	public boolean isTamamlanma() {
		return tamamlanma;
	}

	public void setTamamlanma(boolean tamamlanma) {
		this.tamamlanma = tamamlanma;
	}

	public Date getOnayTarihi() {
		return onayTarihi;
	}

	public void setOnayTarihi(Date onayTarihi) {
		this.onayTarihi = onayTarihi;
	}

	public Date getTalepTarihi() {
		return talepTarihi;
	}

	public void setTalepTarihi(Date talepTarihi) {
		this.talepTarihi = talepTarihi;
	}

	public String getFormSayisi() {
		return formSayisi;
	}

	public void setFormSayisi(String formSayisi) {
		this.formSayisi = formSayisi;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	/**
	 * @return the revAciklama
	 */
	public String getRevAciklama() {
		return revAciklama;
	}

	/**
	 * @param revAciklama
	 *            the revAciklama to set
	 */
	public void setRevAciklama(String revAciklama) {
		this.revAciklama = revAciklama;
	}

}