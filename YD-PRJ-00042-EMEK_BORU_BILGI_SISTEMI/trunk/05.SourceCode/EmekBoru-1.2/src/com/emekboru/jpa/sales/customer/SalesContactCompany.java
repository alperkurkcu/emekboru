package com.emekboru.jpa.sales.customer;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.emekboru.jpa.Ulkeler;

@Entity
@Table(name = "sales_contact_company")
public class SalesContactCompany implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8124427128820206192L;

	@Id
	@Column(name = "sales_contact_company_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_CONTACT_COMPANY_GENERATOR")
	@SequenceGenerator(name = "SALES_CONTACT_COMPANY_GENERATOR", sequenceName = "SALES_CONTACT_COMPANY_SEQUENCE", allocationSize = 1)
	private int salesContactCompanyId;

	@Column(name = "short_name")
	private String shortName;

	@Column(name = "commercial_name")
	private String commercialName;

	@Column(name = "code")
	private String code;

	/* JOIN FUNCTIONS */
	@Transient
	private List<SalesCustomerFunctionType> functionTypeList;
	/* end of JOIN FUNCTIONS */

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "classification", referencedColumnName = "class_id")
	private SalesCustomerClass classification;

	@Column(name = "address")
	private String address;

	@Column(name = "city")
	private String city;

	// @ManyToOne(fetch = FetchType.EAGER)
	// @JoinColumn(name = "country", referencedColumnName = "country_code")
	// private Country country;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ulke_id", referencedColumnName = "ID")
	private Ulkeler ulkeler;

	@Column(name = "tax_office")
	private String taxOffice;

	@Column(name = "tax_number")
	private String taxNumber;

	@Column(name = "record_date")
	private String recordDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_customer_id", referencedColumnName = "sales_customer_id")
	private SalesCustomer salesCustomer;

	public SalesContactCompany() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(("dd/MM/yyyy"));
		recordDate = sdf.format(cal.getTime());
		ulkeler = new Ulkeler();
	}

	@Override
	public boolean equals(Object o) {
		return this.getSalesContactCompanyId() == ((SalesContactCompany) o)
				.getSalesContactCompanyId();
	}

	/**
	 * GETTERS
	 */
	public int getSalesContactCompanyId() {
		return salesContactCompanyId;
	}

	public String getShortName() {
		return shortName;
	}

	public String getCommercialName() {
		return commercialName;
	}

	public String getCode() {
		return code;
	}

	public List<SalesCustomerFunctionType> getFunctionTypeList() {
		return functionTypeList;
	}

	public SalesCustomerClass getClassification() {
		return classification;
	}

	public String getAddress() {
		return address;
	}

	public String getCity() {
		return city;
	}

	public String getTaxOffice() {
		return taxOffice;
	}

	public String getTaxNumber() {
		return taxNumber;
	}

	public String getRecordDate() {
		return recordDate;
	}

	public SalesCustomer getSalesCustomer() {
		return salesCustomer;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * SETTERS
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public void setCommercialName(String commercialName) {
		this.commercialName = commercialName;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setFunctionTypeList(
			List<SalesCustomerFunctionType> functionTypeList) {
		this.functionTypeList = functionTypeList;
	}

	public void setClassification(SalesCustomerClass classification) {
		this.classification = classification;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setTaxOffice(String taxOffice) {
		this.taxOffice = taxOffice;
	}

	public void setTaxNumber(String taxNumber) {
		this.taxNumber = taxNumber;
	}

	public void setRecordDate(String recordDate) {
		this.recordDate = recordDate;
	}

	public void setSalesCustomer(SalesCustomer salesCustomer) {
		this.salesCustomer = salesCustomer;
	}

	public Ulkeler getUlkeler() {
		return ulkeler;
	}

	public void setUlkeler(Ulkeler ulkeler) {
		this.ulkeler = ulkeler;
	}

}