package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "layer")
public class Layer implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "layer_id")
	@SequenceGenerator(name = "LAYER_GENERATOR", sequenceName = "LAYER_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LAYER_GENERATOR")
	private Integer layerId;

	@Column(name = "type")
	private String type;

	@Column(name = "number")
	private String number;

	@Column(name = "min_kalinlik")
	private double minKalinlik;

	@Column(name = "nom_kalinlik")
	private double nomKalinlik;

	@Column(name = "max_kalinlik")
	private double maxKalinlik;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinColumn(name = "isolation_id", referencedColumnName = "isolation_id")
	private PlannedIsolation isolation;

	public Layer() {

	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;

		if (!(obj instanceof Layer))
			return false;

		if (((Layer) obj).getLayerId().equals(layerId))
			return true;

		return false;
	}

	public Integer getLayerId() {
		return layerId;
	}

	public void setLayerId(Integer layerId) {
		this.layerId = layerId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getMinKalinlik() {
		return minKalinlik;
	}

	public void setMinKalinlik(double minKalinlik) {
		this.minKalinlik = minKalinlik;
	}

	public double getNomKalinlik() {
		return nomKalinlik;
	}

	public void setNomKalinlik(double nomKalinlik) {
		this.nomKalinlik = nomKalinlik;
	}

	public double getMaxKalinlik() {
		return maxKalinlik;
	}

	public void setMaxKalinlik(double maxKalinlik) {
		this.maxKalinlik = maxKalinlik;
	}

	public PlannedIsolation getIsolation() {
		return isolation;
	}

	public void setIsolation(PlannedIsolation isolation) {
		this.isolation = isolation;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
}
