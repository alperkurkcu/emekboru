package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.isolation.IsolationTestDefinition;

/**
 * The persistent class for the test_kaplama_beton_yuzey_hazirligi_sonuc
 * database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaBetonYuzeyHazirligiSonuc.findAll", query = "SELECT r FROM TestKaplamaBetonYuzeyHazirligiSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_kaplama_beton_yuzey_hazirligi_sonuc")
public class TestKaplamaBetonYuzeyHazirligiSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KAPLAMA_BETON_YUZEY_HAZIRLIGI_SONUC_ID_GENERATOR", sequenceName = "TEST_KAPLAMA_BETON_YUZEY_HAZIRLIGI_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KAPLAMA_BETON_YUZEY_HAZIRLIGI_SONUC_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ekleme_zamani")
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	// @Column(name="global_id")
	// private Integer globalId;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name="pipe_id")
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;

	@Column(name = "red_akintili")
	private boolean redAkintili;

	@Column(name = "red_eksik_bolge")
	private boolean redEksikBolge;

	@Column(name = "red_puruzlu_degil")
	private boolean redPuruzluDegil;

	@Column(name = "red_yas")
	private boolean redYas;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi")
	private Date testTarihi;

	@Column(name = "vardiya")
	private Integer vardiya;

	@Column(name = "sonuc")
	private boolean sonuc;

	public TestKaplamaBetonYuzeyHazirligiSonuc() {
	}

	/**
	 * @return the bagliGlobalId
	 */
	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}

	/**
	 * @param bagliGlobalId
	 *            the bagliGlobalId to set
	 */
	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public boolean getRedAkintili() {
		return this.redAkintili;
	}

	public void setRedAkintili(Boolean redAkintili) {
		this.redAkintili = redAkintili;
	}

	public boolean getRedEksikBolge() {
		return this.redEksikBolge;
	}

	public void setRedEksikBolge(Boolean redEksikBolge) {
		this.redEksikBolge = redEksikBolge;
	}

	public boolean getRedPuruzluDegil() {
		return this.redPuruzluDegil;
	}

	public void setRedPuruzluDegil(Boolean redPuruzluDegil) {
		this.redPuruzluDegil = redPuruzluDegil;
	}

	public boolean getRedYas() {
		return this.redYas;
	}

	public void setRedYas(Boolean redYas) {
		this.redYas = redYas;
	}

	public Date getTestTarihi() {
		return this.testTarihi;
	}

	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	public Integer getVardiya() {
		return this.vardiya;
	}

	public void setVardiya(Integer vardiya) {
		this.vardiya = vardiya;
	}

	/**
	 * @return the sonuc
	 */
	public boolean isSonuc() {
		return sonuc;
	}

	/**
	 * @param sonuc
	 *            the sonuc to set
	 */
	public void setSonuc(boolean sonuc) {
		this.sonuc = sonuc;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @param redAkintili
	 *            the redAkintili to set
	 */
	public void setRedAkintili(boolean redAkintili) {
		this.redAkintili = redAkintili;
	}

	/**
	 * @param redEksikBolge
	 *            the redEksikBolge to set
	 */
	public void setRedEksikBolge(boolean redEksikBolge) {
		this.redEksikBolge = redEksikBolge;
	}

	/**
	 * @param redPuruzluDegil
	 *            the redPuruzluDegil to set
	 */
	public void setRedPuruzluDegil(boolean redPuruzluDegil) {
		this.redPuruzluDegil = redPuruzluDegil;
	}

	/**
	 * @param redYas
	 *            the redYas to set
	 */
	public void setRedYas(boolean redYas) {
		this.redYas = redYas;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the bagliTestId
	 */
	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

}