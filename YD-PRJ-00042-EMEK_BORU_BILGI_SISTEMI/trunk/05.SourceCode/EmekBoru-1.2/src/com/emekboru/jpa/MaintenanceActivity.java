package com.emekboru.jpa;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="maintenance_activity")
public class MaintenanceActivity implements Serializable{

	private static final long serialVersionUID = -1223661951636577280L;

	@Id
	@Column(name="maintenance_activity_id")
	@SequenceGenerator(name="MAINTENANCE_ACTIVITY_GENERATOR", sequenceName="MAINTENANCE_ACTIVITY_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MAINTENANCE_ACTIVITY_GENERATOR")
	private Integer maintenanceActivityId;

	@Column(name="name")
	private String name;
	
	@Column(name="short_desc")
	private String shortDesc;
	
	@OneToMany(mappedBy = "maintenanceActivity", fetch=FetchType.LAZY)
	private List<MaintenanceExecutedActivities> maintenanceExecutedActivities;
	
	public MaintenanceActivity(){
		
	}

	/**
	 * 	GETTERS AND SETTERS
	 */
	public Integer getMaintenanceActivityId() {
		return maintenanceActivityId;
	}

	public void setMaintenanceActivityId(Integer maintenanceActivityId) {
		this.maintenanceActivityId = maintenanceActivityId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<MaintenanceExecutedActivities> getMaintenanceExecutedActivities() {
		return maintenanceExecutedActivities;
	}

	public void setMaintenanceExecutedActivities(
			List<MaintenanceExecutedActivities> maintenanceExecutedActivities) {
		this.maintenanceExecutedActivities = maintenanceExecutedActivities;
	}
	
}
