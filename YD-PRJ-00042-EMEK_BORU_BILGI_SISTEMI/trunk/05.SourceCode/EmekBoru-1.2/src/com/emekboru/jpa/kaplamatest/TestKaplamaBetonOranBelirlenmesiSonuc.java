package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
 

/**
 * The persistent class for the test_kaplama_beton_oran_belirlenmesi_sonuc database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaBetonOranBelirlenmesiSonuc.findAll", query = "SELECT r FROM TestKaplamaBetonOranBelirlenmesiSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name="test_kaplama_beton_oran_belirlenmesi_sonuc")
public class TestKaplamaBetonOranBelirlenmesiSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TEST_KAPLAMA_BETON_ORAN_BELIRLENMESI_SONUC_ID_GENERATOR", sequenceName="TEST_KAPLAMA_BETON_ORAN_BELIRLENMESI_SONUC_SEQ",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TEST_KAPLAMA_BETON_ORAN_BELIRLENMESI_SONUC_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="ekleme_zamani")
	private Date eklemeZamani;

	@Column(name="ekleyen_kullanici")
	private Integer ekleyenKullanici;

	//@Column(name="global_id")
	//private Integer globalId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name="guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name="kum_cimento_orani")
	private float kumCimentoOrani;

	@Column(name="kum_cimento_sonuc")
	private Boolean kumCimentoSonuc;

	@Column(name="kum_kutlesi")
	private float kumKutlesi;

	//@Column(name="pipe_id")
	//private Integer pipeId;

	@Column(name="su_cimento_orani")
	private float suCimentoOrani;

	@Column(name="su_cimento_sonuc")
	private Boolean suCimentoSonuc;

	@Column(name="su_kutlesi")
	private float suKutlesi;

	@Column(name="taze_harc_kutlesi_m1")
	private float tazeHarcKutlesiM1;

	@Column(name="taze_harc_kutlesi_m2")
	private float tazeHarcKutlesiM2;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="test_tarihi")
	private Date testTarihi;

	@Column(name="vardiya")
	private Integer vardiya;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;

	public TestKaplamaBetonOranBelirlenmesiSonuc() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public float getKumCimentoOrani() {
		return this.kumCimentoOrani;
	}

	public void setKumCimentoOrani(float kumCimentoOrani) {
		this.kumCimentoOrani = kumCimentoOrani;
	}

	public Boolean getKumCimentoSonuc() {
		return this.kumCimentoSonuc;
	}

	public void setKumCimentoSonuc(Boolean kumCimentoSonuc) {
		this.kumCimentoSonuc = kumCimentoSonuc;
	}

	public float getKumKutlesi() {
		return this.kumKutlesi;
	}

	public void setKumKutlesi(float kumKutlesi) {
		this.kumKutlesi = kumKutlesi;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the bagliGlobalId
	 */
	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}

	/**
	 * @param bagliGlobalId the bagliGlobalId to set
	 */
	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public float getSuCimentoOrani() {
		return this.suCimentoOrani;
	}

	public void setSuCimentoOrani(float suCimentoOrani) {
		this.suCimentoOrani = suCimentoOrani;
	}

	public Boolean getSuCimentoSonuc() {
		return this.suCimentoSonuc;
	}

	public void setSuCimentoSonuc(Boolean suCimentoSonuc) {
		this.suCimentoSonuc = suCimentoSonuc;
	}

	public float getSuKutlesi() {
		return this.suKutlesi;
	}

	public void setSuKutlesi(float suKutlesi) {
		this.suKutlesi = suKutlesi;
	}

	public float getTazeHarcKutlesiM1() {
		return this.tazeHarcKutlesiM1;
	}

	public void setTazeHarcKutlesiM1(float tazeHarcKutlesiM1) {
		this.tazeHarcKutlesiM1 = tazeHarcKutlesiM1;
	}

	public float getTazeHarcKutlesiM2() {
		return this.tazeHarcKutlesiM2;
	}

	public void setTazeHarcKutlesiM2(float tazeHarcKutlesiM2) {
		this.tazeHarcKutlesiM2 = tazeHarcKutlesiM2;
	}

	public Date getTestTarihi() {
		return this.testTarihi;
	}

	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	public Integer getVardiya() {
		return this.vardiya;
	}

	public void setVardiya(Integer vardiya) {
		this.vardiya = vardiya;
	}

	/**
	 * @return the bagliTestId
	 */
	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId the bagliTestId to set
	 */
	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

}