package com.emekboru.jpa.sales.customer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.emekboru.jpa.Employee;
import com.emekboru.jpa.sales.SalesStatus;

@Entity
@Table(name = "sales_crm_visit")
public class SalesCRMVisit implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1199700003809440564L;

	@Id
	@Column(name = "sales_crm_visit_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_CRM_VISIT_GENERATOR")
	@SequenceGenerator(name = "SALES_CRM_VISIT_GENERATOR", sequenceName = "SALES_CRM_VISIT_SEQUENCE", allocationSize = 1)
	private int salesCRMVisitId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "visiting_frequency", referencedColumnName = "frequency_id")
	private SalesCustomerVisitingFrequency visitingFrequency;

	@Column(name = "meeting_agenda")
	private String meetingAgenda;

	@Temporal(TemporalType.TIME)
	@Column(name = "visit_time")
	private Date visitTime;

	@Temporal(TemporalType.DATE)
	@Column(name = "visit_date")
	private Date visitDate;

	@Column(name = "place")
	private String place;

	@Column(name = "notes")
	private String notes;

	@Column(name = "documents")
	private String documents;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "status", referencedColumnName = "status_id")
	private SalesStatus status;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_customer_id", referencedColumnName = "sales_customer_id")
	private SalesCustomer salesCustomer;

	/* JOIN FUNCTIONS */
	@Transient
	private List<Employee> participantList;

	@Transient
	private List<SalesContactPeople> contactPeopleList;

	/* end of JOIN FUNCTIONS */

	/* FILE UPLOAD-DOWNLOAD FUNCTIONS */
	@Transient
	private List<String> fileNames;

	@Transient
	private int fileNumber;

	/* end of FILE UPLOAD-DOWNLOAD FUNCTIONS */

	public SalesCRMVisit() {
		fileNumber = 0;
		participantList = new ArrayList<Employee>();
	}

	@Override
	public boolean equals(Object o) {
		return this.getSalesCRMVisitId() == ((SalesCRMVisit) o)
				.getSalesCRMVisitId();
	}

	/**
	 * GETTERS
	 */
	public int getSalesCRMVisitId() {
		return salesCRMVisitId;
	}

	public SalesCustomerVisitingFrequency getVisitingFrequency() {
		return visitingFrequency;
	}

	public String getMeetingAgenda() {
		return meetingAgenda;
	}

	public Date getVisitDate() {
		return visitDate;
	}

	public Date getVisitTime() {
		return visitTime;
	}

	public String getPlace() {
		return place;
	}

	public String getNotes() {
		return notes;
	}

	public String getDocuments() {
		return documents;
	}

	public SalesCustomer getSalesCustomer() {
		return salesCustomer;
	}

	public SalesStatus getStatus() {
		return status;
	}

	public List<Employee> getParticipantList() {
		return participantList;
	}

	public List<SalesContactPeople> getContactPeopleList() {
		return contactPeopleList;
	}

	public List<String> getFileNames() {
		try {
			String[] names = documents.split("//");
			fileNames = new ArrayList<String>();

			for (int i = 1; i < names.length; i++) {
				fileNames.add(names[i]);
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return fileNames;
	}

	public int getFileNumber() {
		try {
			String[] names = documents.split("//");

			fileNumber = names.length - 1;
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return fileNumber;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * SETTERS
	 */
	public void setVisitingFrequency(
			SalesCustomerVisitingFrequency visitingFrequency) {
		this.visitingFrequency = visitingFrequency;
	}

	public void setMeetingAgenda(String meetingAgenda) {
		this.meetingAgenda = meetingAgenda;
	}

	public void setVisitTime(Date visitTime) {
		this.visitTime = visitTime;
	}

	public void setVisitDate(Date visitDate) {
		this.visitDate = visitDate;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public void setDocuments(String documents) {
		this.documents = documents;
	}

	public void setStatus(SalesStatus status) {
		this.status = status;
	}

	public void setSalesCustomer(SalesCustomer salesCustomer) {
		this.salesCustomer = salesCustomer;
	}

	public void setParticipantList(List<Employee> participantList) {
		this.participantList = participantList;
	}

	public void setContactPeopleList(List<SalesContactPeople> contactPeopleList) {
		this.contactPeopleList = contactPeopleList;
	}

	public void setFileNames(List<String> fileNames) {
		this.fileNames = fileNames;
	}

}