package com.emekboru.jpa.kalibrasyon;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the kalibrasyon_ultrasonik_laminasyon database
 * table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "KalibrasyonUltrasonikLaminasyon.findAll", query = "SELECT r FROM KalibrasyonUltrasonikLaminasyon r where r.onlineOffline=:prmOnlineOffline order by r.kalibrasyonZamani DESC") })
@Table(name = "kalibrasyon_ultrasonik_laminasyon")
public class KalibrasyonUltrasonikLaminasyon implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "KALIBRASYON_ULTRASONIK_LAMINASYON_ID_GENERATOR", sequenceName = "KALIBRASYON_ULTRASONIK_LAMINASYON_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "KALIBRASYON_ULTRASONIK_LAMINASYON_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "aciklama")
	private String aciklama;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "kalibrasyon_zamani")
	private Date kalibrasyonZamani;

	@Column(name = "kalibrasyonu_yapan")
	private String kalibrasyonuYapan;

	@Column(name = "kayit_kazanc_degeri_alti")
	private String kayitKazancDegeriAlti;

	@Column(name = "kayit_kazanc_degeri_bes")
	private String kayitKazancDegeriBes;

	@Column(name = "kayit_kazanc_degeri_bir")
	private String kayitKazancDegeriBir;

	@Column(name = "kayit_kazanc_degeri_dokuz")
	private String kayitKazancDegeriDokuz;

	@Column(name = "kayit_kazanc_degeri_dort")
	private String kayitKazancDegeriDort;

	@Column(name = "kayit_kazanc_degeri_elli")
	private String kayitKazancDegeriElli;

	@Column(name = "kayit_kazanc_degeri_ellibir")
	private String kayitKazancDegeriEllibir;

	@Column(name = "kayit_kazanc_degeri_elliiki")
	private String kayitKazancDegeriElliiki;

	@Column(name = "kayit_kazanc_degeri_iki")
	private String kayitKazancDegeriIki;

	@Column(name = "kayit_kazanc_degeri_kirk")
	private String kayitKazancDegeriKirk;

	@Column(name = "kayit_kazanc_degeri_kirkalti")
	private String kayitKazancDegeriKirkalti;

	@Column(name = "kayit_kazanc_degeri_kirkbes")
	private String kayitKazancDegeriKirkbes;

	@Column(name = "kayit_kazanc_degeri_kirkbir")
	private String kayitKazancDegeriKirkbir;

	@Column(name = "kayit_kazanc_degeri_kirkdokuz")
	private String kayitKazancDegeriKirkdokuz;

	@Column(name = "kayit_kazanc_degeri_kirkdort")
	private String kayitKazancDegeriKirkdort;

	@Column(name = "kayit_kazanc_degeri_kirkiki")
	private String kayitKazancDegeriKirkiki;

	@Column(name = "kayit_kazanc_degeri_kirksekiz")
	private String kayitKazancDegeriKirksekiz;

	@Column(name = "kayit_kazanc_degeri_kirkuc")
	private String kayitKazancDegeriKirkuc;

	@Column(name = "kayit_kazanc_degeri_kirkyedi")
	private String kayitKazancDegeriKirkyedi;

	@Column(name = "kayit_kazanc_degeri_on")
	private String kayitKazancDegeriOn;

	@Column(name = "kayit_kazanc_degeri_onalti")
	private String kayitKazancDegeriOnalti;

	@Column(name = "kayit_kazanc_degeri_onbes")
	private String kayitKazancDegeriOnbes;

	@Column(name = "kayit_kazanc_degeri_onbir")
	private String kayitKazancDegeriOnbir;

	@Column(name = "kayit_kazanc_degeri_ondokuz")
	private String kayitKazancDegeriOndokuz;

	@Column(name = "kayit_kazanc_degeri_ondort")
	private String kayitKazancDegeriOndort;

	@Column(name = "kayit_kazanc_degeri_oniki")
	private String kayitKazancDegeriOniki;

	@Column(name = "kayit_kazanc_degeri_onsekiz")
	private String kayitKazancDegeriOnsekiz;

	@Column(name = "kayit_kazanc_degeri_onuc")
	private String kayitKazancDegeriOnuc;

	@Column(name = "kayit_kazanc_degeri_onyedi")
	private String kayitKazancDegeriOnyedi;

	@Column(name = "kayit_kazanc_degeri_otuz")
	private String kayitKazancDegeriOtuz;

	@Column(name = "kayit_kazanc_degeri_otuzalti")
	private String kayitKazancDegeriOtuzalti;

	@Column(name = "kayit_kazanc_degeri_otuzbes")
	private String kayitKazancDegeriOtuzbes;

	@Column(name = "kayit_kazanc_degeri_otuzbir")
	private String kayitKazancDegeriOtuzbir;

	@Column(name = "kayit_kazanc_degeri_otuzdokuz")
	private String kayitKazancDegeriOtuzdokuz;

	@Column(name = "kayit_kazanc_degeri_otuzdort")
	private String kayitKazancDegeriOtuzdort;

	@Column(name = "kayit_kazanc_degeri_otuziki")
	private String kayitKazancDegeriOtuziki;

	@Column(name = "kayit_kazanc_degeri_otuzsekiz")
	private String kayitKazancDegeriOtuzsekiz;

	@Column(name = "kayit_kazanc_degeri_otuzuc")
	private String kayitKazancDegeriOtuzuc;

	@Column(name = "kayit_kazanc_degeri_otuzyedi")
	private String kayitKazancDegeriOtuzyedi;

	@Column(name = "kayit_kazanc_degeri_sekiz")
	private String kayitKazancDegeriSekiz;

	@Column(name = "kayit_kazanc_degeri_uc")
	private String kayitKazancDegeriUc;

	@Column(name = "kayit_kazanc_degeri_yedi")
	private String kayitKazancDegeriYedi;

	@Column(name = "kayit_kazanc_degeri_yirmi")
	private String kayitKazancDegeriYirmi;

	@Column(name = "kayit_kazanc_degeri_yirmialti")
	private String kayitKazancDegeriYirmialti;

	@Column(name = "kayit_kazanc_degeri_yirmibes")
	private String kayitKazancDegeriYirmibes;

	@Column(name = "kayit_kazanc_degeri_yirmibir")
	private String kayitKazancDegeriYirmibir;

	@Column(name = "kayit_kazanc_degeri_yirmidokuz")
	private String kayitKazancDegeriYirmidokuz;

	@Column(name = "kayit_kazanc_degeri_yirmidort")
	private String kayitKazancDegeriYirmidort;

	@Column(name = "kayit_kazanc_degeri_yirmiiki")
	private String kayitKazancDegeriYirmiiki;

	@Column(name = "kayit_kazanc_degeri_yirmisekiz")
	private String kayitKazancDegeriYirmisekiz;

	@Column(name = "kayit_kazanc_degeri_yirmiuc")
	private String kayitKazancDegeriYirmiuc;

	@Column(name = "kayit_kazanc_degeri_yirmiyedi")
	private String kayitKazancDegeriYirmiyedi;

	@Column(name = "prob_capi_frekansi_bir")
	private String probCapiFrekansiBir;

	@Column(name = "prob_capi_frekansi_iki")
	private String probCapiFrekansiIki;

	@Column(name = "referans_seviye_bir")
	private String referansSeviyeBir;

	@Column(name = "referans_seviye_iki")
	private String referansSeviyeIki;

	@Column(name = "referans_yansitici_bir")
	private String referansYansiticiBir;

	@Column(name = "referans_yansitici_iki")
	private String referansYansiticiIki;

	@Column(name = "ilave_kazanc_degeri_bir")
	private String ilaveKazancDegeriBir;

	@Column(name = "ilave_kazanc_degeri_iki")
	private String ilaveKazancDegeriIki;

	@Column(name = "kayit_seviyesi_bir")
	private String kayitSeviyesiBir;

	@Column(name = "kayit_seviyesi_iki")
	private String kayitSeviyesiIki;

	@Column(name = "online_offline", nullable = false)
	private Integer onlineOffline;

	@Column(name = "tarama_alani_bir")
	private String taramaAlaniBir;

	@Column(name = "tarama_alani_iki")
	private String taramaAlaniIki;

	@Column(name = "tarama_hizi_bir")
	private String taramaHiziBir;

	@Column(name = "tarama_hizi_iki")
	private String taramaHiziIki;

	@Column(name = "kalibrasyon_mesafesi_bir")
	private String kalibrasyonMesafesiBir;

	@Column(name = "kalibrasyon_mesafesi_iki")
	private String kalibrasyonMesafesiIki;

	@Column(name = "kalibrasyon_blogu_bir")
	private String kalibrasyonBloguBir;

	@Column(name = "kalibrasyon_blogu_iki")
	private String kalibrasyonBloguIki;

	@Column(name = "cihaz")
	private String cihaz;

	public KalibrasyonUltrasonikLaminasyon() {
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getKalibrasyonuYapan() {
		return this.kalibrasyonuYapan;
	}

	public void setKalibrasyonuYapan(String kalibrasyonuYapan) {
		this.kalibrasyonuYapan = kalibrasyonuYapan;
	}

	public String getKayitKazancDegeriAlti() {
		return this.kayitKazancDegeriAlti;
	}

	public void setKayitKazancDegeriAlti(String kayitKazancDegeriAlti) {
		this.kayitKazancDegeriAlti = kayitKazancDegeriAlti;
	}

	public String getKayitKazancDegeriBes() {
		return this.kayitKazancDegeriBes;
	}

	public void setKayitKazancDegeriBes(String kayitKazancDegeriBes) {
		this.kayitKazancDegeriBes = kayitKazancDegeriBes;
	}

	public String getKayitKazancDegeriBir() {
		return this.kayitKazancDegeriBir;
	}

	public void setKayitKazancDegeriBir(String kayitKazancDegeriBir) {
		this.kayitKazancDegeriBir = kayitKazancDegeriBir;
	}

	public String getKayitKazancDegeriDokuz() {
		return this.kayitKazancDegeriDokuz;
	}

	public void setKayitKazancDegeriDokuz(String kayitKazancDegeriDokuz) {
		this.kayitKazancDegeriDokuz = kayitKazancDegeriDokuz;
	}

	public String getKayitKazancDegeriDort() {
		return this.kayitKazancDegeriDort;
	}

	public void setKayitKazancDegeriDort(String kayitKazancDegeriDort) {
		this.kayitKazancDegeriDort = kayitKazancDegeriDort;
	}

	public String getKayitKazancDegeriElli() {
		return this.kayitKazancDegeriElli;
	}

	public void setKayitKazancDegeriElli(String kayitKazancDegeriElli) {
		this.kayitKazancDegeriElli = kayitKazancDegeriElli;
	}

	public String getKayitKazancDegeriEllibir() {
		return this.kayitKazancDegeriEllibir;
	}

	public void setKayitKazancDegeriEllibir(String kayitKazancDegeriEllibir) {
		this.kayitKazancDegeriEllibir = kayitKazancDegeriEllibir;
	}

	public String getKayitKazancDegeriElliiki() {
		return this.kayitKazancDegeriElliiki;
	}

	public void setKayitKazancDegeriElliiki(String kayitKazancDegeriElliiki) {
		this.kayitKazancDegeriElliiki = kayitKazancDegeriElliiki;
	}

	public String getKayitKazancDegeriIki() {
		return this.kayitKazancDegeriIki;
	}

	public void setKayitKazancDegeriIki(String kayitKazancDegeriIki) {
		this.kayitKazancDegeriIki = kayitKazancDegeriIki;
	}

	public String getKayitKazancDegeriKirk() {
		return this.kayitKazancDegeriKirk;
	}

	public void setKayitKazancDegeriKirk(String kayitKazancDegeriKirk) {
		this.kayitKazancDegeriKirk = kayitKazancDegeriKirk;
	}

	public String getKayitKazancDegeriKirkalti() {
		return this.kayitKazancDegeriKirkalti;
	}

	public void setKayitKazancDegeriKirkalti(String kayitKazancDegeriKirkalti) {
		this.kayitKazancDegeriKirkalti = kayitKazancDegeriKirkalti;
	}

	public String getKayitKazancDegeriKirkbes() {
		return this.kayitKazancDegeriKirkbes;
	}

	public void setKayitKazancDegeriKirkbes(String kayitKazancDegeriKirkbes) {
		this.kayitKazancDegeriKirkbes = kayitKazancDegeriKirkbes;
	}

	public String getKayitKazancDegeriKirkbir() {
		return this.kayitKazancDegeriKirkbir;
	}

	public void setKayitKazancDegeriKirkbir(String kayitKazancDegeriKirkbir) {
		this.kayitKazancDegeriKirkbir = kayitKazancDegeriKirkbir;
	}

	public String getKayitKazancDegeriKirkdokuz() {
		return this.kayitKazancDegeriKirkdokuz;
	}

	public void setKayitKazancDegeriKirkdokuz(String kayitKazancDegeriKirkdokuz) {
		this.kayitKazancDegeriKirkdokuz = kayitKazancDegeriKirkdokuz;
	}

	public String getKayitKazancDegeriKirkdort() {
		return this.kayitKazancDegeriKirkdort;
	}

	public void setKayitKazancDegeriKirkdort(String kayitKazancDegeriKirkdort) {
		this.kayitKazancDegeriKirkdort = kayitKazancDegeriKirkdort;
	}

	public String getKayitKazancDegeriKirkiki() {
		return this.kayitKazancDegeriKirkiki;
	}

	public void setKayitKazancDegeriKirkiki(String kayitKazancDegeriKirkiki) {
		this.kayitKazancDegeriKirkiki = kayitKazancDegeriKirkiki;
	}

	public String getKayitKazancDegeriKirksekiz() {
		return this.kayitKazancDegeriKirksekiz;
	}

	public void setKayitKazancDegeriKirksekiz(String kayitKazancDegeriKirksekiz) {
		this.kayitKazancDegeriKirksekiz = kayitKazancDegeriKirksekiz;
	}

	public String getKayitKazancDegeriKirkuc() {
		return this.kayitKazancDegeriKirkuc;
	}

	public void setKayitKazancDegeriKirkuc(String kayitKazancDegeriKirkuc) {
		this.kayitKazancDegeriKirkuc = kayitKazancDegeriKirkuc;
	}

	public String getKayitKazancDegeriKirkyedi() {
		return this.kayitKazancDegeriKirkyedi;
	}

	public void setKayitKazancDegeriKirkyedi(String kayitKazancDegeriKirkyedi) {
		this.kayitKazancDegeriKirkyedi = kayitKazancDegeriKirkyedi;
	}

	public String getKayitKazancDegeriOn() {
		return this.kayitKazancDegeriOn;
	}

	public void setKayitKazancDegeriOn(String kayitKazancDegeriOn) {
		this.kayitKazancDegeriOn = kayitKazancDegeriOn;
	}

	public String getKayitKazancDegeriOnalti() {
		return this.kayitKazancDegeriOnalti;
	}

	public void setKayitKazancDegeriOnalti(String kayitKazancDegeriOnalti) {
		this.kayitKazancDegeriOnalti = kayitKazancDegeriOnalti;
	}

	public String getKayitKazancDegeriOnbes() {
		return this.kayitKazancDegeriOnbes;
	}

	public void setKayitKazancDegeriOnbes(String kayitKazancDegeriOnbes) {
		this.kayitKazancDegeriOnbes = kayitKazancDegeriOnbes;
	}

	public String getKayitKazancDegeriOnbir() {
		return this.kayitKazancDegeriOnbir;
	}

	public void setKayitKazancDegeriOnbir(String kayitKazancDegeriOnbir) {
		this.kayitKazancDegeriOnbir = kayitKazancDegeriOnbir;
	}

	public String getKayitKazancDegeriOndokuz() {
		return this.kayitKazancDegeriOndokuz;
	}

	public void setKayitKazancDegeriOndokuz(String kayitKazancDegeriOndokuz) {
		this.kayitKazancDegeriOndokuz = kayitKazancDegeriOndokuz;
	}

	public String getKayitKazancDegeriOndort() {
		return this.kayitKazancDegeriOndort;
	}

	public void setKayitKazancDegeriOndort(String kayitKazancDegeriOndort) {
		this.kayitKazancDegeriOndort = kayitKazancDegeriOndort;
	}

	public String getKayitKazancDegeriOniki() {
		return this.kayitKazancDegeriOniki;
	}

	public void setKayitKazancDegeriOniki(String kayitKazancDegeriOniki) {
		this.kayitKazancDegeriOniki = kayitKazancDegeriOniki;
	}

	public String getKayitKazancDegeriOnsekiz() {
		return this.kayitKazancDegeriOnsekiz;
	}

	public void setKayitKazancDegeriOnsekiz(String kayitKazancDegeriOnsekiz) {
		this.kayitKazancDegeriOnsekiz = kayitKazancDegeriOnsekiz;
	}

	public String getKayitKazancDegeriOnuc() {
		return this.kayitKazancDegeriOnuc;
	}

	public void setKayitKazancDegeriOnuc(String kayitKazancDegeriOnuc) {
		this.kayitKazancDegeriOnuc = kayitKazancDegeriOnuc;
	}

	public String getKayitKazancDegeriOnyedi() {
		return this.kayitKazancDegeriOnyedi;
	}

	public void setKayitKazancDegeriOnyedi(String kayitKazancDegeriOnyedi) {
		this.kayitKazancDegeriOnyedi = kayitKazancDegeriOnyedi;
	}

	public String getKayitKazancDegeriOtuz() {
		return this.kayitKazancDegeriOtuz;
	}

	public void setKayitKazancDegeriOtuz(String kayitKazancDegeriOtuz) {
		this.kayitKazancDegeriOtuz = kayitKazancDegeriOtuz;
	}

	public String getKayitKazancDegeriOtuzalti() {
		return this.kayitKazancDegeriOtuzalti;
	}

	public void setKayitKazancDegeriOtuzalti(String kayitKazancDegeriOtuzalti) {
		this.kayitKazancDegeriOtuzalti = kayitKazancDegeriOtuzalti;
	}

	public String getKayitKazancDegeriOtuzbes() {
		return this.kayitKazancDegeriOtuzbes;
	}

	public void setKayitKazancDegeriOtuzbes(String kayitKazancDegeriOtuzbes) {
		this.kayitKazancDegeriOtuzbes = kayitKazancDegeriOtuzbes;
	}

	public String getKayitKazancDegeriOtuzbir() {
		return this.kayitKazancDegeriOtuzbir;
	}

	public void setKayitKazancDegeriOtuzbir(String kayitKazancDegeriOtuzbir) {
		this.kayitKazancDegeriOtuzbir = kayitKazancDegeriOtuzbir;
	}

	public String getKayitKazancDegeriOtuzdokuz() {
		return this.kayitKazancDegeriOtuzdokuz;
	}

	public void setKayitKazancDegeriOtuzdokuz(String kayitKazancDegeriOtuzdokuz) {
		this.kayitKazancDegeriOtuzdokuz = kayitKazancDegeriOtuzdokuz;
	}

	public String getKayitKazancDegeriOtuzdort() {
		return this.kayitKazancDegeriOtuzdort;
	}

	public void setKayitKazancDegeriOtuzdort(String kayitKazancDegeriOtuzdort) {
		this.kayitKazancDegeriOtuzdort = kayitKazancDegeriOtuzdort;
	}

	public String getKayitKazancDegeriOtuziki() {
		return this.kayitKazancDegeriOtuziki;
	}

	public void setKayitKazancDegeriOtuziki(String kayitKazancDegeriOtuziki) {
		this.kayitKazancDegeriOtuziki = kayitKazancDegeriOtuziki;
	}

	public String getKayitKazancDegeriOtuzsekiz() {
		return this.kayitKazancDegeriOtuzsekiz;
	}

	public void setKayitKazancDegeriOtuzsekiz(String kayitKazancDegeriOtuzsekiz) {
		this.kayitKazancDegeriOtuzsekiz = kayitKazancDegeriOtuzsekiz;
	}

	public String getKayitKazancDegeriOtuzuc() {
		return this.kayitKazancDegeriOtuzuc;
	}

	public void setKayitKazancDegeriOtuzuc(String kayitKazancDegeriOtuzuc) {
		this.kayitKazancDegeriOtuzuc = kayitKazancDegeriOtuzuc;
	}

	public String getKayitKazancDegeriOtuzyedi() {
		return this.kayitKazancDegeriOtuzyedi;
	}

	public void setKayitKazancDegeriOtuzyedi(String kayitKazancDegeriOtuzyedi) {
		this.kayitKazancDegeriOtuzyedi = kayitKazancDegeriOtuzyedi;
	}

	public String getKayitKazancDegeriSekiz() {
		return this.kayitKazancDegeriSekiz;
	}

	public void setKayitKazancDegeriSekiz(String kayitKazancDegeriSekiz) {
		this.kayitKazancDegeriSekiz = kayitKazancDegeriSekiz;
	}

	public String getKayitKazancDegeriUc() {
		return this.kayitKazancDegeriUc;
	}

	public void setKayitKazancDegeriUc(String kayitKazancDegeriUc) {
		this.kayitKazancDegeriUc = kayitKazancDegeriUc;
	}

	public String getKayitKazancDegeriYedi() {
		return this.kayitKazancDegeriYedi;
	}

	public void setKayitKazancDegeriYedi(String kayitKazancDegeriYedi) {
		this.kayitKazancDegeriYedi = kayitKazancDegeriYedi;
	}

	public String getKayitKazancDegeriYirmi() {
		return this.kayitKazancDegeriYirmi;
	}

	public void setKayitKazancDegeriYirmi(String kayitKazancDegeriYirmi) {
		this.kayitKazancDegeriYirmi = kayitKazancDegeriYirmi;
	}

	public String getKayitKazancDegeriYirmialti() {
		return this.kayitKazancDegeriYirmialti;
	}

	public void setKayitKazancDegeriYirmialti(String kayitKazancDegeriYirmialti) {
		this.kayitKazancDegeriYirmialti = kayitKazancDegeriYirmialti;
	}

	public String getKayitKazancDegeriYirmibes() {
		return this.kayitKazancDegeriYirmibes;
	}

	public void setKayitKazancDegeriYirmibes(String kayitKazancDegeriYirmibes) {
		this.kayitKazancDegeriYirmibes = kayitKazancDegeriYirmibes;
	}

	public String getKayitKazancDegeriYirmibir() {
		return this.kayitKazancDegeriYirmibir;
	}

	public void setKayitKazancDegeriYirmibir(String kayitKazancDegeriYirmibir) {
		this.kayitKazancDegeriYirmibir = kayitKazancDegeriYirmibir;
	}

	public String getKayitKazancDegeriYirmidokuz() {
		return this.kayitKazancDegeriYirmidokuz;
	}

	public void setKayitKazancDegeriYirmidokuz(
			String kayitKazancDegeriYirmidokuz) {
		this.kayitKazancDegeriYirmidokuz = kayitKazancDegeriYirmidokuz;
	}

	public String getKayitKazancDegeriYirmidort() {
		return this.kayitKazancDegeriYirmidort;
	}

	public void setKayitKazancDegeriYirmidort(String kayitKazancDegeriYirmidort) {
		this.kayitKazancDegeriYirmidort = kayitKazancDegeriYirmidort;
	}

	public String getKayitKazancDegeriYirmiiki() {
		return this.kayitKazancDegeriYirmiiki;
	}

	public void setKayitKazancDegeriYirmiiki(String kayitKazancDegeriYirmiiki) {
		this.kayitKazancDegeriYirmiiki = kayitKazancDegeriYirmiiki;
	}

	public String getKayitKazancDegeriYirmisekiz() {
		return this.kayitKazancDegeriYirmisekiz;
	}

	public void setKayitKazancDegeriYirmisekiz(
			String kayitKazancDegeriYirmisekiz) {
		this.kayitKazancDegeriYirmisekiz = kayitKazancDegeriYirmisekiz;
	}

	public String getKayitKazancDegeriYirmiuc() {
		return this.kayitKazancDegeriYirmiuc;
	}

	public void setKayitKazancDegeriYirmiuc(String kayitKazancDegeriYirmiuc) {
		this.kayitKazancDegeriYirmiuc = kayitKazancDegeriYirmiuc;
	}

	public String getKayitKazancDegeriYirmiyedi() {
		return this.kayitKazancDegeriYirmiyedi;
	}

	public void setKayitKazancDegeriYirmiyedi(String kayitKazancDegeriYirmiyedi) {
		this.kayitKazancDegeriYirmiyedi = kayitKazancDegeriYirmiyedi;
	}

	public String getProbCapiFrekansiBir() {
		return this.probCapiFrekansiBir;
	}

	public void setProbCapiFrekansiBir(String probCapiFrekansiBir) {
		this.probCapiFrekansiBir = probCapiFrekansiBir;
	}

	public String getProbCapiFrekansiIki() {
		return this.probCapiFrekansiIki;
	}

	public void setProbCapiFrekansiIki(String probCapiFrekansiIki) {
		this.probCapiFrekansiIki = probCapiFrekansiIki;
	}

	public String getReferansSeviyeBir() {
		return this.referansSeviyeBir;
	}

	public void setReferansSeviyeBir(String referansSeviyeBir) {
		this.referansSeviyeBir = referansSeviyeBir;
	}

	public String getReferansSeviyeIki() {
		return this.referansSeviyeIki;
	}

	public void setReferansSeviyeIki(String referansSeviyeIki) {
		this.referansSeviyeIki = referansSeviyeIki;
	}

	public String getReferansYansiticiBir() {
		return this.referansYansiticiBir;
	}

	public void setReferansYansiticiBir(String referansYansiticiBir) {
		this.referansYansiticiBir = referansYansiticiBir;
	}

	public String getReferansYansiticiIki() {
		return this.referansYansiticiIki;
	}

	public void setReferansYansiticiIki(String referansYansiticiIki) {
		this.referansYansiticiIki = referansYansiticiIki;
	}

	/**
	 * @return the eklemeZamani
	 */
	public Date getEklemeZamani() {
		return eklemeZamani;
	}

	/**
	 * @param eklemeZamani
	 *            the eklemeZamani to set
	 */
	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncellemeZamani
	 */
	public Date getGuncellemeZamani() {
		return guncellemeZamani;
	}

	/**
	 * @param guncellemeZamani
	 *            the guncellemeZamani to set
	 */
	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the kalibrasyonZamani
	 */
	public Date getKalibrasyonZamani() {
		return kalibrasyonZamani;
	}

	/**
	 * @param kalibrasyonZamani
	 *            the kalibrasyonZamani to set
	 */
	public void setKalibrasyonZamani(Date kalibrasyonZamani) {
		this.kalibrasyonZamani = kalibrasyonZamani;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the ilaveKazancDegeriBir
	 */
	public String getIlaveKazancDegeriBir() {
		return ilaveKazancDegeriBir;
	}

	/**
	 * @param ilaveKazancDegeriBir
	 *            the ilaveKazancDegeriBir to set
	 */
	public void setIlaveKazancDegeriBir(String ilaveKazancDegeriBir) {
		this.ilaveKazancDegeriBir = ilaveKazancDegeriBir;
	}

	/**
	 * @return the ilaveKazancDegeriIki
	 */
	public String getIlaveKazancDegeriIki() {
		return ilaveKazancDegeriIki;
	}

	/**
	 * @param ilaveKazancDegeriIki
	 *            the ilaveKazancDegeriIki to set
	 */
	public void setIlaveKazancDegeriIki(String ilaveKazancDegeriIki) {
		this.ilaveKazancDegeriIki = ilaveKazancDegeriIki;
	}

	/**
	 * @return the kayitSeviyesiBir
	 */
	public String getKayitSeviyesiBir() {
		return kayitSeviyesiBir;
	}

	/**
	 * @param kayitSeviyesiBir
	 *            the kayitSeviyesiBir to set
	 */
	public void setKayitSeviyesiBir(String kayitSeviyesiBir) {
		this.kayitSeviyesiBir = kayitSeviyesiBir;
	}

	/**
	 * @return the kayitSeviyesiIki
	 */
	public String getKayitSeviyesiIki() {
		return kayitSeviyesiIki;
	}

	/**
	 * @param kayitSeviyesiIki
	 *            the kayitSeviyesiIki to set
	 */
	public void setKayitSeviyesiIki(String kayitSeviyesiIki) {
		this.kayitSeviyesiIki = kayitSeviyesiIki;
	}

	/**
	 * @return the onlineOffline
	 */
	public Integer getOnlineOffline() {
		return onlineOffline;
	}

	/**
	 * @param onlineOffline
	 *            the onlineOffline to set
	 */
	public void setOnlineOffline(Integer onlineOffline) {
		this.onlineOffline = onlineOffline;
	}

	/**
	 * @return the taramaAlaniBir
	 */
	public String getTaramaAlaniBir() {
		return taramaAlaniBir;
	}

	/**
	 * @param taramaAlaniBir
	 *            the taramaAlaniBir to set
	 */
	public void setTaramaAlaniBir(String taramaAlaniBir) {
		this.taramaAlaniBir = taramaAlaniBir;
	}

	/**
	 * @return the taramaAlaniIki
	 */
	public String getTaramaAlaniIki() {
		return taramaAlaniIki;
	}

	/**
	 * @param taramaAlaniIki
	 *            the taramaAlaniIki to set
	 */
	public void setTaramaAlaniIki(String taramaAlaniIki) {
		this.taramaAlaniIki = taramaAlaniIki;
	}

	/**
	 * @return the taramaHiziBir
	 */
	public String getTaramaHiziBir() {
		return taramaHiziBir;
	}

	/**
	 * @param taramaHiziBir
	 *            the taramaHiziBir to set
	 */
	public void setTaramaHiziBir(String taramaHiziBir) {
		this.taramaHiziBir = taramaHiziBir;
	}

	/**
	 * @return the taramaHiziIki
	 */
	public String getTaramaHiziIki() {
		return taramaHiziIki;
	}

	/**
	 * @param taramaHiziIki
	 *            the taramaHiziIki to set
	 */
	public void setTaramaHiziIki(String taramaHiziIki) {
		this.taramaHiziIki = taramaHiziIki;
	}

	/**
	 * @return the kalibrasyonMesafesiBir
	 */
	public String getKalibrasyonMesafesiBir() {
		return kalibrasyonMesafesiBir;
	}

	/**
	 * @param kalibrasyonMesafesiBir
	 *            the kalibrasyonMesafesiBir to set
	 */
	public void setKalibrasyonMesafesiBir(String kalibrasyonMesafesiBir) {
		this.kalibrasyonMesafesiBir = kalibrasyonMesafesiBir;
	}

	/**
	 * @return the kalibrasyonMesafesiIki
	 */
	public String getKalibrasyonMesafesiIki() {
		return kalibrasyonMesafesiIki;
	}

	/**
	 * @param kalibrasyonMesafesiIki
	 *            the kalibrasyonMesafesiIki to set
	 */
	public void setKalibrasyonMesafesiIki(String kalibrasyonMesafesiIki) {
		this.kalibrasyonMesafesiIki = kalibrasyonMesafesiIki;
	}

	/**
	 * @return the kalibrasyonBloguBir
	 */
	public String getKalibrasyonBloguBir() {
		return kalibrasyonBloguBir;
	}

	/**
	 * @param kalibrasyonBloguBir
	 *            the kalibrasyonBloguBir to set
	 */
	public void setKalibrasyonBloguBir(String kalibrasyonBloguBir) {
		this.kalibrasyonBloguBir = kalibrasyonBloguBir;
	}

	/**
	 * @return the kalibrasyonBloguIki
	 */
	public String getKalibrasyonBloguIki() {
		return kalibrasyonBloguIki;
	}

	/**
	 * @param kalibrasyonBloguIki
	 *            the kalibrasyonBloguIki to set
	 */
	public void setKalibrasyonBloguIki(String kalibrasyonBloguIki) {
		this.kalibrasyonBloguIki = kalibrasyonBloguIki;
	}

	/**
	 * @return the cihaz
	 */
	public String getCihaz() {
		return cihaz;
	}

	/**
	 * @param cihaz
	 *            the cihaz to set
	 */
	public void setCihaz(String cihaz) {
		this.cihaz = cihaz;
	}

}