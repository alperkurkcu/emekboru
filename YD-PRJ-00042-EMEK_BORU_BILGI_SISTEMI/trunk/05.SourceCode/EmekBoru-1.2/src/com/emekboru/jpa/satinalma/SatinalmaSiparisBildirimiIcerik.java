package com.emekboru.jpa.satinalma;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the satinalma_siparis_bildirimi_icerik database
 * table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "SatinalmaSiparisBildirimiIcerik.findByBildirimId", query = "SELECT r FROM SatinalmaSiparisBildirimiIcerik r where r.satinalmaSiparisBildirimi.id=:prmBildirimId order by r.eklemeZamani ASC") })
@Table(name = "satinalma_siparis_bildirimi_icerik")
public class SatinalmaSiparisBildirimiIcerik implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SATINALMA_SIPARIS_BILDIRIMI_ICERIK_ID_GENERATOR", sequenceName = "SATINALMA_SIPARIS_BILDIRIMI_ICERIK_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SATINALMA_SIPARIS_BILDIRIMI_ICERIK_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(length = 255, name = "aciklama")
	private String aciklama;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	// @Column(name = "siparis_bildirimi_id", nullable = false)
	// private Integer siparisBildirimiId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "siparis_bildirimi_id", referencedColumnName = "ID", insertable = false)
	private SatinalmaSiparisBildirimi satinalmaSiparisBildirimi;

	@Column(name = "siparis_tutari")
	private Integer siparisTutari;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "tahmini_odeme_tarihi")
	private Date tahminiOdemeTarihi;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "tahmini_sevk_tarihi")
	private Date tahminiSevkTarihi;

	@Column(name = "siparis_no")
	private Integer siparisNo;

	public SatinalmaSiparisBildirimiIcerik() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Integer getSiparisTutari() {
		return this.siparisTutari;
	}

	public void setSiparisTutari(Integer siparisTutari) {
		this.siparisTutari = siparisTutari;
	}

	/**
	 * @return the satinalmaSiparisBildirimi
	 */
	public SatinalmaSiparisBildirimi getSatinalmaSiparisBildirimi() {
		return satinalmaSiparisBildirimi;
	}

	/**
	 * @param satinalmaSiparisBildirimi
	 *            the satinalmaSiparisBildirimi to set
	 */
	public void setSatinalmaSiparisBildirimi(
			SatinalmaSiparisBildirimi satinalmaSiparisBildirimi) {
		this.satinalmaSiparisBildirimi = satinalmaSiparisBildirimi;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the siparisNo
	 */
	public Integer getSiparisNo() {
		return siparisNo;
	}

	/**
	 * @param siparisNo
	 *            the siparisNo to set
	 */
	public void setSiparisNo(Integer siparisNo) {
		this.siparisNo = siparisNo;
	}

	/**
	 * @return the tahminiOdemeTarihi
	 */
	public Date getTahminiOdemeTarihi() {
		return tahminiOdemeTarihi;
	}

	/**
	 * @param tahminiOdemeTarihi
	 *            the tahminiOdemeTarihi to set
	 */
	public void setTahminiOdemeTarihi(Date tahminiOdemeTarihi) {
		this.tahminiOdemeTarihi = tahminiOdemeTarihi;
	}

	/**
	 * @return the tahminiSevkTarihi
	 */
	public Date getTahminiSevkTarihi() {
		return tahminiSevkTarihi;
	}

	/**
	 * @param tahminiSevkTarihi
	 *            the tahminiSevkTarihi to set
	 */
	public void setTahminiSevkTarihi(Date tahminiSevkTarihi) {
		this.tahminiSevkTarihi = tahminiSevkTarihi;
	}

}