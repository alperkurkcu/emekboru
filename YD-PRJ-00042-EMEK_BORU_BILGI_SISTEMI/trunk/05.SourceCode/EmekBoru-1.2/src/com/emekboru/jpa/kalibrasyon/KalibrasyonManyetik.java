package com.emekboru.jpa.kalibrasyon;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the kalibrasyon_manyetik database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "KalibrasyonManyetik.findAll", query = "SELECT r FROM KalibrasyonManyetik r order by r.kalibrasyonZamani DESC"),
		@NamedQuery(name = "KalibrasyonManyetik.findAllByType", query = "SELECT r FROM KalibrasyonManyetik r where r.kalibrasyonIstasyonu=:prmType order by r.kalibrasyonZamani DESC") })
@Table(name = "kalibrasyon_manyetik")
public class KalibrasyonManyetik implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "KALIBRASYON_MANYETIK_ID_GENERATOR", sequenceName = "KALIBRASYON_MANYETIK_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "KALIBRASYON_MANYETIK_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "aciklama")
	private String aciklama;

	@Column(name = "akim_tipi")
	private Integer akimTipi;

	@Column(name = "ayaklar_arasi_mesafe")
	private String ayaklarArasiMesafe;

	@Column(name = "cevre_aydinligi")
	private String cevreAydinligi;

	@Column(name = "cihaz")
	private String cihaz;

	@Column(name = "demagnetizasyon")
	private String demagnetizasyon;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "isik_siddeti")
	private String isikSiddeti;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "kalibrasyon_zamani")
	private Date kalibrasyonZamani;

	@Column(name = "kalibrasyonu_yapan")
	private String kalibrasyonuYapan;

	@Column(name = "kalici_manyetiklik")
	private String kaliciManyetiklik;

	@Column(name = "kaplama_kalinligi")
	private String kaplamaKalinligi;

	@Column(name = "manyetik_alan_siddeti")
	private String manyetikAlanSiddeti;

	@Column(name = "miknatislama_teknigi")
	private String miknatislamaTeknigi;

	@Column(name = "miknatislanma_yonu")
	private Integer miknatislanmaYonu;

	@Column(name = "ortamin_performans_kontrolu")
	private String ortaminPerformansKontrolu;

	@Column(name = "son_temizlik")
	private String sonTemizlik;

	@Column(name = "flourcescent")
	private boolean flourcescent;

	@Column(name = "wet")
	private boolean wet;

	@Column(name = "dry")
	private boolean dry;

	@Column(name = "kalibrasyon_istasyonu")
	private Integer kalibrasyonIstasyonu;

	@Column(name = "kapsam")
	private String kapsam;

	@Column(name = "astm")
	private String astm;

	@Column(name = "manyetik_cihaz_seri_no")
	private String manyetikCihazSeriNo;

	@Column(name = "manyetik_cihaz_tarihi")
	private String manyetikCihazTarihi;

	@Column(name = "lux_metre_cihaz_seri_no")
	private String luxMetreCihazSeriNo;

	@Column(name = "lux_metre_cihaz_tarihi")
	private String luxMetreCihazTarihi;

	@Column(name = "sarf_madde_karisim_orani")
	private String sarfMaddeKarisimOrani;

	@Column(name = "bobin_bilgileri")
	private String bobinBilgileri;

	public KalibrasyonManyetik() {
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Integer getAkimTipi() {
		return this.akimTipi;
	}

	public void setAkimTipi(Integer akimTipi) {
		this.akimTipi = akimTipi;
	}

	public String getAyaklarArasiMesafe() {
		return this.ayaklarArasiMesafe;
	}

	public void setAyaklarArasiMesafe(String ayaklarArasiMesafe) {
		this.ayaklarArasiMesafe = ayaklarArasiMesafe;
	}

	public String getCevreAydinligi() {
		return this.cevreAydinligi;
	}

	public void setCevreAydinligi(String cevreAydinligi) {
		this.cevreAydinligi = cevreAydinligi;
	}

	public String getCihaz() {
		return this.cihaz;
	}

	public void setCihaz(String cihaz) {
		this.cihaz = cihaz;
	}

	public String getDemagnetizasyon() {
		return this.demagnetizasyon;
	}

	public void setDemagnetizasyon(String demagnetizasyon) {
		this.demagnetizasyon = demagnetizasyon;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getIsikSiddeti() {
		return this.isikSiddeti;
	}

	public void setIsikSiddeti(String isikSiddeti) {
		this.isikSiddeti = isikSiddeti;
	}

	public String getKalibrasyonuYapan() {
		return this.kalibrasyonuYapan;
	}

	public void setKalibrasyonuYapan(String kalibrasyonuYapan) {
		this.kalibrasyonuYapan = kalibrasyonuYapan;
	}

	public String getKaliciManyetiklik() {
		return this.kaliciManyetiklik;
	}

	public void setKaliciManyetiklik(String kaliciManyetiklik) {
		this.kaliciManyetiklik = kaliciManyetiklik;
	}

	public String getKaplamaKalinligi() {
		return this.kaplamaKalinligi;
	}

	public void setKaplamaKalinligi(String kaplamaKalinligi) {
		this.kaplamaKalinligi = kaplamaKalinligi;
	}

	public String getManyetikAlanSiddeti() {
		return this.manyetikAlanSiddeti;
	}

	public void setManyetikAlanSiddeti(String manyetikAlanSiddeti) {
		this.manyetikAlanSiddeti = manyetikAlanSiddeti;
	}

	public String getMiknatislamaTeknigi() {
		return this.miknatislamaTeknigi;
	}

	public void setMiknatislamaTeknigi(String miknatislamaTeknigi) {
		this.miknatislamaTeknigi = miknatislamaTeknigi;
	}

	public Integer getMiknatislanmaYonu() {
		return this.miknatislanmaYonu;
	}

	public void setMiknatislanmaYonu(Integer miknatislanmaYonu) {
		this.miknatislanmaYonu = miknatislanmaYonu;
	}

	public String getOrtaminPerformansKontrolu() {
		return this.ortaminPerformansKontrolu;
	}

	public void setOrtaminPerformansKontrolu(String ortaminPerformansKontrolu) {
		this.ortaminPerformansKontrolu = ortaminPerformansKontrolu;
	}

	public String getSonTemizlik() {
		return this.sonTemizlik;
	}

	public void setSonTemizlik(String sonTemizlik) {
		this.sonTemizlik = sonTemizlik;
	}

	/**
	 * @return the eklemeZamani
	 */
	public Date getEklemeZamani() {
		return eklemeZamani;
	}

	/**
	 * @param eklemeZamani
	 *            the eklemeZamani to set
	 */
	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncellemeZamani
	 */
	public Date getGuncellemeZamani() {
		return guncellemeZamani;
	}

	/**
	 * @param guncellemeZamani
	 *            the guncellemeZamani to set
	 */
	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the kalibrasyonZamani
	 */
	public Date getKalibrasyonZamani() {
		return kalibrasyonZamani;
	}

	/**
	 * @param kalibrasyonZamani
	 *            the kalibrasyonZamani to set
	 */
	public void setKalibrasyonZamani(Date kalibrasyonZamani) {
		this.kalibrasyonZamani = kalibrasyonZamani;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the flourcescent
	 */
	public boolean isFlourcescent() {
		return flourcescent;
	}

	/**
	 * @param flourcescent
	 *            the flourcescent to set
	 */
	public void setFlourcescent(boolean flourcescent) {
		this.flourcescent = flourcescent;
	}

	/**
	 * @return the wet
	 */
	public boolean isWet() {
		return wet;
	}

	/**
	 * @param wet
	 *            the wet to set
	 */
	public void setWet(boolean wet) {
		this.wet = wet;
	}

	/**
	 * @return the dry
	 */
	public boolean isDry() {
		return dry;
	}

	/**
	 * @param dry
	 *            the dry to set
	 */
	public void setDry(boolean dry) {
		this.dry = dry;
	}

	/**
	 * @return the kalibrasyonIstasyonu
	 */
	public Integer getKalibrasyonIstasyonu() {
		return kalibrasyonIstasyonu;
	}

	/**
	 * @param kalibrasyonIstasyonu
	 *            the kalibrasyonIstasyonu to set
	 */
	public void setKalibrasyonIstasyonu(Integer kalibrasyonIstasyonu) {
		this.kalibrasyonIstasyonu = kalibrasyonIstasyonu;
	}

	/**
	 * @return the kapsam
	 */
	public String getKapsam() {
		return kapsam;
	}

	/**
	 * @param kapsam
	 *            the kapsam to set
	 */
	public void setKapsam(String kapsam) {
		this.kapsam = kapsam;
	}

	/**
	 * @return the astm
	 */
	public String getAstm() {
		return astm;
	}

	/**
	 * @param astm
	 *            the astm to set
	 */
	public void setAstm(String astm) {
		this.astm = astm;
	}

	/**
	 * @return the manyetikCihazSeriNo
	 */
	public String getManyetikCihazSeriNo() {
		return manyetikCihazSeriNo;
	}

	/**
	 * @param manyetikCihazSeriNo
	 *            the manyetikCihazSeriNo to set
	 */
	public void setManyetikCihazSeriNo(String manyetikCihazSeriNo) {
		this.manyetikCihazSeriNo = manyetikCihazSeriNo;
	}

	/**
	 * @return the manyetikCihazTarihi
	 */
	public String getManyetikCihazTarihi() {
		return manyetikCihazTarihi;
	}

	/**
	 * @param manyetikCihazTarihi
	 *            the manyetikCihazTarihi to set
	 */
	public void setManyetikCihazTarihi(String manyetikCihazTarihi) {
		this.manyetikCihazTarihi = manyetikCihazTarihi;
	}

	/**
	 * @return the luxMetreCihazSeriNo
	 */
	public String getLuxMetreCihazSeriNo() {
		return luxMetreCihazSeriNo;
	}

	/**
	 * @param luxMetreCihazSeriNo
	 *            the luxMetreCihazSeriNo to set
	 */
	public void setLuxMetreCihazSeriNo(String luxMetreCihazSeriNo) {
		this.luxMetreCihazSeriNo = luxMetreCihazSeriNo;
	}

	/**
	 * @return the luxMetreCihazTarihi
	 */
	public String getLuxMetreCihazTarihi() {
		return luxMetreCihazTarihi;
	}

	/**
	 * @param luxMetreCihazTarihi
	 *            the luxMetreCihazTarihi to set
	 */
	public void setLuxMetreCihazTarihi(String luxMetreCihazTarihi) {
		this.luxMetreCihazTarihi = luxMetreCihazTarihi;
	}

	/**
	 * @return the sarfMaddeKarisimOrani
	 */
	public String getSarfMaddeKarisimOrani() {
		return sarfMaddeKarisimOrani;
	}

	/**
	 * @param sarfMaddeKarisimOrani
	 *            the sarfMaddeKarisimOrani to set
	 */
	public void setSarfMaddeKarisimOrani(String sarfMaddeKarisimOrani) {
		this.sarfMaddeKarisimOrani = sarfMaddeKarisimOrani;
	}

	/**
	 * @return the bobinBilgileri
	 */
	public String getBobinBilgileri() {
		return bobinBilgileri;
	}

	/**
	 * @param bobinBilgileri
	 *            the bobinBilgileri to set
	 */
	public void setBobinBilgileri(String bobinBilgileri) {
		this.bobinBilgileri = bobinBilgileri;
	}

}