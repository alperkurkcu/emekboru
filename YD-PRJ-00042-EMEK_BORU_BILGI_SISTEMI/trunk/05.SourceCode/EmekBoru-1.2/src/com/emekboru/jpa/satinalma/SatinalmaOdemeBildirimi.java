package com.emekboru.jpa.satinalma;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * The persistent class for the satinalma_odeme_bildirimi database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "SatinalmaOdemeBildirimi.findAll", query = "SELECT r FROM SatinalmaOdemeBildirimi r order by r.eklemeZamani DESC") })
@Table(name = "satinalma_odeme_bildirimi")
public class SatinalmaOdemeBildirimi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SATINALMA_ODEME_BILDIRIMI_ID_GENERATOR", sequenceName = "SATINALMA_ODEME_BILDIRIMI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SATINALMA_ODEME_BILDIRIMI_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(length = 255, name = "aciklama")
	private String aciklama;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Column(name = "fatura_no", length = 255)
	private String faturaNo;

	@Column(name = "fatura_tarihi")
	private Timestamp faturaTarihi;

	@Column(name = "fatura_tutari")
	private Integer faturaTutari;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(length = 255, name = "iban")
	private String iban;

	@Column(name = "kesinti")
	private Integer kesinti;

	@Column(name = "kur_bilgileri", length = 255)
	private String kurBilgileri;

	@Column(name = "odeme_vadesi", length = 255)
	private String odemeVadesi;

	@Column(name = "odenecek_tutar")
	private Integer odenecekTutar;

	// @Column(name = "siparis_bildirimi_id", nullable = false)
	// private Integer siparisBildirimiId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "siparis_bildirimi_id", referencedColumnName = "ID", insertable = false)
	private SatinalmaSiparisBildirimi satinalmaSiparisBildirimi;

	@Column(name = "siparis_tutari")
	private Integer siparisTutari;

	@Column(name = "documents")
	private String documents;

	@Transient
	private List<String> fileNames;

	@Transient
	private int fileNumber;

	public SatinalmaOdemeBildirimi() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public String getFaturaNo() {
		return this.faturaNo;
	}

	public void setFaturaNo(String faturaNo) {
		this.faturaNo = faturaNo;
	}

	public Timestamp getFaturaTarihi() {
		return this.faturaTarihi;
	}

	public void setFaturaTarihi(Timestamp faturaTarihi) {
		this.faturaTarihi = faturaTarihi;
	}

	public Integer getFaturaTutari() {
		return this.faturaTutari;
	}

	public void setFaturaTutari(Integer faturaTutari) {
		this.faturaTutari = faturaTutari;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getIban() {
		return this.iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public Integer getKesinti() {
		return this.kesinti;
	}

	public void setKesinti(Integer kesinti) {
		this.kesinti = kesinti;
	}

	public String getKurBilgileri() {
		return this.kurBilgileri;
	}

	public void setKurBilgileri(String kurBilgileri) {
		this.kurBilgileri = kurBilgileri;
	}

	public String getOdemeVadesi() {
		return this.odemeVadesi;
	}

	public void setOdemeVadesi(String odemeVadesi) {
		this.odemeVadesi = odemeVadesi;
	}

	public Integer getOdenecekTutar() {
		return this.odenecekTutar;
	}

	public void setOdenecekTutar(Integer odenecekTutar) {
		this.odenecekTutar = odenecekTutar;
	}

	public Integer getSiparisTutari() {
		return this.siparisTutari;
	}

	public void setSiparisTutari(Integer siparisTutari) {
		this.siparisTutari = siparisTutari;
	}

	/**
	 * @return the satinalmaSiparisBildirimi
	 */
	public SatinalmaSiparisBildirimi getSatinalmaSiparisBildirimi() {
		return satinalmaSiparisBildirimi;
	}

	/**
	 * @param satinalmaSiparisBildirimi
	 *            the satinalmaSiparisBildirimi to set
	 */
	public void setSatinalmaSiparisBildirimi(
			SatinalmaSiparisBildirimi satinalmaSiparisBildirimi) {
		this.satinalmaSiparisBildirimi = satinalmaSiparisBildirimi;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the documents
	 */
	public String getDocuments() {
		return documents;
	}

	/**
	 * @param documents
	 *            the documents to set
	 */
	public void setDocuments(String documents) {
		this.documents = documents;
	}

	/**
	 * @return the fileNames
	 */
	public List<String> getFileNames() {
		try {
			String[] names = documents.split("//");
			fileNames = new ArrayList<String>();

			for (int i = 1; i < names.length; i++) {
				fileNames.add(names[i]);
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return fileNames;
	}

	/**
	 * @param fileNames
	 *            the fileNames to set
	 */
	public void setFileNames(List<String> fileNames) {
		this.fileNames = fileNames;
	}

	/**
	 * @return the fileNumber
	 */
	public int getFileNumber() {
		try {
			String[] names = documents.split("//");

			fileNumber = names.length - 1;
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return fileNumber;
	}

	/**
	 * @param fileNumber
	 *            the fileNumber to set
	 */
	public void setFileNumber(int fileNumber) {
		this.fileNumber = fileNumber;
	}

}