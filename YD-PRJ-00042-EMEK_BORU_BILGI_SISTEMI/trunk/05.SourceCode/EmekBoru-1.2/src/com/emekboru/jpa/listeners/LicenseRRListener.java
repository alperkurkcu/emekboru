package com.emekboru.jpa.listeners;

import javax.persistence.PostLoad;

import com.emekboru.jpa.LicenseRenewResponsible;

public class LicenseRRListener {

	@PostLoad
	public void onLoad(LicenseRenewResponsible lrr){
		
		if(lrr.getRenewDate()!=null)
			lrr.setRenewed(false);
		else lrr.setRenewed(true);
	}
}
