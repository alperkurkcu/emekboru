package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;

/**
 * The persistent class for the test_kaplama_beton_egilme_basinc_sonuc database
 * table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaBetonEgilmeBasincSonuc.findAll", query = "SELECT r FROM TestKaplamaBetonEgilmeBasincSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_kaplama_beton_egilme_basinc_sonuc")
public class TestKaplamaBetonEgilmeBasincSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KAPLAMA_BETON_EGILME_BASINC_SONUC_ID_GENERATOR", sequenceName = "TEST_KAPLAMA_BETON_EGILME_BASINC_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KAPLAMA_BETON_EGILME_BASINC_SONUC_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Column(name = "basinc_2_gunluk_1")
	private BigDecimal basinc2Gunluk1;

	@Column(name = "basinc_2_gunluk_2")
	private BigDecimal basinc2Gunluk2;

	@Column(name = "basinc_2_gunluk_3")
	private BigDecimal basinc2Gunluk3;

	@Column(name = "basinc_2_gunluk_4")
	private BigDecimal basinc2Gunluk4;

	@Column(name = "basinc_2_gunluk_5")
	private BigDecimal basinc2Gunluk5;

	@Column(name = "basinc_2_gunluk_6")
	private BigDecimal basinc2Gunluk6;

	@Column(name = "basinc_2_gunluk_ortalama")
	private BigDecimal basinc2GunlukOrtalama;

	@Column(name = "basinc_2_gunluk_sonuc")
	private Integer basinc2GunlukSonuc;

	@Column(name = "basinc_28_gunluk_1")
	private BigDecimal basinc28Gunluk1;

	@Column(name = "basinc_28_gunluk_2")
	private BigDecimal basinc28Gunluk2;

	@Column(name = "basinc_28_gunluk_3")
	private BigDecimal basinc28Gunluk3;

	@Column(name = "basinc_28_gunluk_4")
	private BigDecimal basinc28Gunluk4;

	@Column(name = "basinc_28_gunluk_5")
	private BigDecimal basinc28Gunluk5;

	@Column(name = "basinc_28_gunluk_6")
	private BigDecimal basinc28Gunluk6;

	@Column(name = "basinc_28_gunluk_ortalama")
	private BigDecimal basinc28GunlukOrtalama;

	@Column(name = "basinc_28_gunluk_sonuc")
	private Integer basinc28GunlukSonuc;

	@Column(name = "basinc_7_gunluk_1")
	private BigDecimal basinc7Gunluk1;

	@Column(name = "basinc_7_gunluk_2")
	private BigDecimal basinc7Gunluk2;

	@Column(name = "basinc_7_gunluk_3")
	private BigDecimal basinc7Gunluk3;

	@Column(name = "basinc_7_gunluk_4")
	private BigDecimal basinc7Gunluk4;

	@Column(name = "basinc_7_gunluk_5")
	private BigDecimal basinc7Gunluk5;

	@Column(name = "basinc_7_gunluk_6")
	private BigDecimal basinc7Gunluk6;

	@Column(name = "basinc_7_gunluk_ortalama")
	private BigDecimal basinc7GunlukOrtalama;

	@Column(name = "basinc_7_gunluk_sonuc")
	private Integer basinc7GunlukSonuc;

	@Column(name = "egilme_2_gunluk_1")
	private BigDecimal egilme2Gunluk1;

	@Column(name = "egilme_2_gunluk_2")
	private BigDecimal egilme2Gunluk2;

	@Column(name = "egilme_2_gunluk_3")
	private BigDecimal egilme2Gunluk3;

	@Column(name = "egilme_2_gunluk_ortalama")
	private BigDecimal egilme2GunlukOrtalama;

	@Column(name = "egilme_2_gunluk_sonuc")
	private Integer egilme2GunlukSonuc;

	@Column(name = "egilme_28_gunluk_1")
	private BigDecimal egilme28Gunluk1;

	@Column(name = "egilme_28_gunluk_2")
	private BigDecimal egilme28Gunluk2;

	@Column(name = "egilme_28_gunluk_3")
	private BigDecimal egilme28Gunluk3;

	@Column(name = "egilme_28_gunluk_ortalama")
	private BigDecimal egilme28GunlukOrtalama;

	@Column(name = "egilme_28_gunluk_sonuc")
	private Integer egilme28GunlukSonuc;

	@Column(name = "egilme_7_gunluk_1")
	private BigDecimal egilme7Gunluk1;

	@Column(name = "egilme_7_gunluk_2")
	private BigDecimal egilme7Gunluk2;

	@Column(name = "egilme_7_gunluk_3")
	private BigDecimal egilme7Gunluk3;

	@Column(name = "egilme_7_gunluk_ortalama")
	private BigDecimal egilme7GunlukOrtalama;

	@Column(name = "egilme_7_gunluk_sonuc")
	private Integer egilme7GunlukSonuc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ekleme_zamani")
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	// @Column(name="global_id")
	// private Integer globalId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	// @Column(name="pipe_id")
	// private Integer pipeId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi")
	private Date testTarihi;

	@Column(name = "vardiya")
	private Integer vardiya;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;

	public TestKaplamaBetonEgilmeBasincSonuc() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the basinc2Gunluk1
	 */
	public BigDecimal getBasinc2Gunluk1() {
		return basinc2Gunluk1;
	}

	/**
	 * @param basinc2Gunluk1
	 *            the basinc2Gunluk1 to set
	 */
	public void setBasinc2Gunluk1(BigDecimal basinc2Gunluk1) {
		this.basinc2Gunluk1 = basinc2Gunluk1;
	}

	/**
	 * @return the basinc2Gunluk2
	 */
	public BigDecimal getBasinc2Gunluk2() {
		return basinc2Gunluk2;
	}

	/**
	 * @param basinc2Gunluk2
	 *            the basinc2Gunluk2 to set
	 */
	public void setBasinc2Gunluk2(BigDecimal basinc2Gunluk2) {
		this.basinc2Gunluk2 = basinc2Gunluk2;
	}

	/**
	 * @return the basinc2Gunluk3
	 */
	public BigDecimal getBasinc2Gunluk3() {
		return basinc2Gunluk3;
	}

	/**
	 * @param basinc2Gunluk3
	 *            the basinc2Gunluk3 to set
	 */
	public void setBasinc2Gunluk3(BigDecimal basinc2Gunluk3) {
		this.basinc2Gunluk3 = basinc2Gunluk3;
	}

	/**
	 * @return the basinc2Gunluk4
	 */
	public BigDecimal getBasinc2Gunluk4() {
		return basinc2Gunluk4;
	}

	/**
	 * @param basinc2Gunluk4
	 *            the basinc2Gunluk4 to set
	 */
	public void setBasinc2Gunluk4(BigDecimal basinc2Gunluk4) {
		this.basinc2Gunluk4 = basinc2Gunluk4;
	}

	/**
	 * @return the basinc2Gunluk5
	 */
	public BigDecimal getBasinc2Gunluk5() {
		return basinc2Gunluk5;
	}

	/**
	 * @param basinc2Gunluk5
	 *            the basinc2Gunluk5 to set
	 */
	public void setBasinc2Gunluk5(BigDecimal basinc2Gunluk5) {
		this.basinc2Gunluk5 = basinc2Gunluk5;
	}

	/**
	 * @return the basinc2Gunluk6
	 */
	public BigDecimal getBasinc2Gunluk6() {
		return basinc2Gunluk6;
	}

	/**
	 * @param basinc2Gunluk6
	 *            the basinc2Gunluk6 to set
	 */
	public void setBasinc2Gunluk6(BigDecimal basinc2Gunluk6) {
		this.basinc2Gunluk6 = basinc2Gunluk6;
	}

	/**
	 * @return the basinc2GunlukOrtalama
	 */
	public BigDecimal getBasinc2GunlukOrtalama() {
		return basinc2GunlukOrtalama;
	}

	/**
	 * @param basinc2GunlukOrtalama
	 *            the basinc2GunlukOrtalama to set
	 */
	public void setBasinc2GunlukOrtalama(BigDecimal basinc2GunlukOrtalama) {
		this.basinc2GunlukOrtalama = basinc2GunlukOrtalama;
	}

	/**
	 * @return the basinc28Gunluk1
	 */
	public BigDecimal getBasinc28Gunluk1() {
		return basinc28Gunluk1;
	}

	/**
	 * @param basinc28Gunluk1
	 *            the basinc28Gunluk1 to set
	 */
	public void setBasinc28Gunluk1(BigDecimal basinc28Gunluk1) {
		this.basinc28Gunluk1 = basinc28Gunluk1;
	}

	/**
	 * @return the basinc28Gunluk2
	 */
	public BigDecimal getBasinc28Gunluk2() {
		return basinc28Gunluk2;
	}

	/**
	 * @param basinc28Gunluk2
	 *            the basinc28Gunluk2 to set
	 */
	public void setBasinc28Gunluk2(BigDecimal basinc28Gunluk2) {
		this.basinc28Gunluk2 = basinc28Gunluk2;
	}

	/**
	 * @return the basinc28Gunluk3
	 */
	public BigDecimal getBasinc28Gunluk3() {
		return basinc28Gunluk3;
	}

	/**
	 * @param basinc28Gunluk3
	 *            the basinc28Gunluk3 to set
	 */
	public void setBasinc28Gunluk3(BigDecimal basinc28Gunluk3) {
		this.basinc28Gunluk3 = basinc28Gunluk3;
	}

	/**
	 * @return the basinc28Gunluk4
	 */
	public BigDecimal getBasinc28Gunluk4() {
		return basinc28Gunluk4;
	}

	/**
	 * @param basinc28Gunluk4
	 *            the basinc28Gunluk4 to set
	 */
	public void setBasinc28Gunluk4(BigDecimal basinc28Gunluk4) {
		this.basinc28Gunluk4 = basinc28Gunluk4;
	}

	/**
	 * @return the basinc28Gunluk5
	 */
	public BigDecimal getBasinc28Gunluk5() {
		return basinc28Gunluk5;
	}

	/**
	 * @param basinc28Gunluk5
	 *            the basinc28Gunluk5 to set
	 */
	public void setBasinc28Gunluk5(BigDecimal basinc28Gunluk5) {
		this.basinc28Gunluk5 = basinc28Gunluk5;
	}

	/**
	 * @return the basinc28Gunluk6
	 */
	public BigDecimal getBasinc28Gunluk6() {
		return basinc28Gunluk6;
	}

	/**
	 * @param basinc28Gunluk6
	 *            the basinc28Gunluk6 to set
	 */
	public void setBasinc28Gunluk6(BigDecimal basinc28Gunluk6) {
		this.basinc28Gunluk6 = basinc28Gunluk6;
	}

	/**
	 * @return the basinc28GunlukOrtalama
	 */
	public BigDecimal getBasinc28GunlukOrtalama() {
		return basinc28GunlukOrtalama;
	}

	/**
	 * @param basinc28GunlukOrtalama
	 *            the basinc28GunlukOrtalama to set
	 */
	public void setBasinc28GunlukOrtalama(BigDecimal basinc28GunlukOrtalama) {
		this.basinc28GunlukOrtalama = basinc28GunlukOrtalama;
	}

	/**
	 * @return the basinc7Gunluk1
	 */
	public BigDecimal getBasinc7Gunluk1() {
		return basinc7Gunluk1;
	}

	/**
	 * @param basinc7Gunluk1
	 *            the basinc7Gunluk1 to set
	 */
	public void setBasinc7Gunluk1(BigDecimal basinc7Gunluk1) {
		this.basinc7Gunluk1 = basinc7Gunluk1;
	}

	/**
	 * @return the basinc7Gunluk2
	 */
	public BigDecimal getBasinc7Gunluk2() {
		return basinc7Gunluk2;
	}

	/**
	 * @param basinc7Gunluk2
	 *            the basinc7Gunluk2 to set
	 */
	public void setBasinc7Gunluk2(BigDecimal basinc7Gunluk2) {
		this.basinc7Gunluk2 = basinc7Gunluk2;
	}

	/**
	 * @return the basinc7Gunluk3
	 */
	public BigDecimal getBasinc7Gunluk3() {
		return basinc7Gunluk3;
	}

	/**
	 * @param basinc7Gunluk3
	 *            the basinc7Gunluk3 to set
	 */
	public void setBasinc7Gunluk3(BigDecimal basinc7Gunluk3) {
		this.basinc7Gunluk3 = basinc7Gunluk3;
	}

	/**
	 * @return the basinc7Gunluk4
	 */
	public BigDecimal getBasinc7Gunluk4() {
		return basinc7Gunluk4;
	}

	/**
	 * @param basinc7Gunluk4
	 *            the basinc7Gunluk4 to set
	 */
	public void setBasinc7Gunluk4(BigDecimal basinc7Gunluk4) {
		this.basinc7Gunluk4 = basinc7Gunluk4;
	}

	/**
	 * @return the basinc7Gunluk5
	 */
	public BigDecimal getBasinc7Gunluk5() {
		return basinc7Gunluk5;
	}

	/**
	 * @param basinc7Gunluk5
	 *            the basinc7Gunluk5 to set
	 */
	public void setBasinc7Gunluk5(BigDecimal basinc7Gunluk5) {
		this.basinc7Gunluk5 = basinc7Gunluk5;
	}

	/**
	 * @return the basinc7Gunluk6
	 */
	public BigDecimal getBasinc7Gunluk6() {
		return basinc7Gunluk6;
	}

	/**
	 * @param basinc7Gunluk6
	 *            the basinc7Gunluk6 to set
	 */
	public void setBasinc7Gunluk6(BigDecimal basinc7Gunluk6) {
		this.basinc7Gunluk6 = basinc7Gunluk6;
	}

	/**
	 * @return the basinc7GunlukOrtalama
	 */
	public BigDecimal getBasinc7GunlukOrtalama() {
		return basinc7GunlukOrtalama;
	}

	/**
	 * @param basinc7GunlukOrtalama
	 *            the basinc7GunlukOrtalama to set
	 */
	public void setBasinc7GunlukOrtalama(BigDecimal basinc7GunlukOrtalama) {
		this.basinc7GunlukOrtalama = basinc7GunlukOrtalama;
	}

	/**
	 * @return the egilme2Gunluk1
	 */
	public BigDecimal getEgilme2Gunluk1() {
		return egilme2Gunluk1;
	}

	/**
	 * @param egilme2Gunluk1
	 *            the egilme2Gunluk1 to set
	 */
	public void setEgilme2Gunluk1(BigDecimal egilme2Gunluk1) {
		this.egilme2Gunluk1 = egilme2Gunluk1;
	}

	/**
	 * @return the egilme2Gunluk2
	 */
	public BigDecimal getEgilme2Gunluk2() {
		return egilme2Gunluk2;
	}

	/**
	 * @param egilme2Gunluk2
	 *            the egilme2Gunluk2 to set
	 */
	public void setEgilme2Gunluk2(BigDecimal egilme2Gunluk2) {
		this.egilme2Gunluk2 = egilme2Gunluk2;
	}

	/**
	 * @return the egilme2Gunluk3
	 */
	public BigDecimal getEgilme2Gunluk3() {
		return egilme2Gunluk3;
	}

	/**
	 * @param egilme2Gunluk3
	 *            the egilme2Gunluk3 to set
	 */
	public void setEgilme2Gunluk3(BigDecimal egilme2Gunluk3) {
		this.egilme2Gunluk3 = egilme2Gunluk3;
	}

	/**
	 * @return the egilme2GunlukOrtalama
	 */
	public BigDecimal getEgilme2GunlukOrtalama() {
		return egilme2GunlukOrtalama;
	}

	/**
	 * @param egilme2GunlukOrtalama
	 *            the egilme2GunlukOrtalama to set
	 */
	public void setEgilme2GunlukOrtalama(BigDecimal egilme2GunlukOrtalama) {
		this.egilme2GunlukOrtalama = egilme2GunlukOrtalama;
	}

	/**
	 * @return the egilme28Gunluk1
	 */
	public BigDecimal getEgilme28Gunluk1() {
		return egilme28Gunluk1;
	}

	/**
	 * @param egilme28Gunluk1
	 *            the egilme28Gunluk1 to set
	 */
	public void setEgilme28Gunluk1(BigDecimal egilme28Gunluk1) {
		this.egilme28Gunluk1 = egilme28Gunluk1;
	}

	/**
	 * @return the egilme28Gunluk2
	 */
	public BigDecimal getEgilme28Gunluk2() {
		return egilme28Gunluk2;
	}

	/**
	 * @param egilme28Gunluk2
	 *            the egilme28Gunluk2 to set
	 */
	public void setEgilme28Gunluk2(BigDecimal egilme28Gunluk2) {
		this.egilme28Gunluk2 = egilme28Gunluk2;
	}

	/**
	 * @return the egilme28Gunluk3
	 */
	public BigDecimal getEgilme28Gunluk3() {
		return egilme28Gunluk3;
	}

	/**
	 * @param egilme28Gunluk3
	 *            the egilme28Gunluk3 to set
	 */
	public void setEgilme28Gunluk3(BigDecimal egilme28Gunluk3) {
		this.egilme28Gunluk3 = egilme28Gunluk3;
	}

	/**
	 * @return the egilme28GunlukOrtalama
	 */
	public BigDecimal getEgilme28GunlukOrtalama() {
		return egilme28GunlukOrtalama;
	}

	/**
	 * @param egilme28GunlukOrtalama
	 *            the egilme28GunlukOrtalama to set
	 */
	public void setEgilme28GunlukOrtalama(BigDecimal egilme28GunlukOrtalama) {
		this.egilme28GunlukOrtalama = egilme28GunlukOrtalama;
	}

	/**
	 * @return the egilme7Gunluk1
	 */
	public BigDecimal getEgilme7Gunluk1() {
		return egilme7Gunluk1;
	}

	/**
	 * @param egilme7Gunluk1
	 *            the egilme7Gunluk1 to set
	 */
	public void setEgilme7Gunluk1(BigDecimal egilme7Gunluk1) {
		this.egilme7Gunluk1 = egilme7Gunluk1;
	}

	/**
	 * @return the egilme7Gunluk2
	 */
	public BigDecimal getEgilme7Gunluk2() {
		return egilme7Gunluk2;
	}

	/**
	 * @param egilme7Gunluk2
	 *            the egilme7Gunluk2 to set
	 */
	public void setEgilme7Gunluk2(BigDecimal egilme7Gunluk2) {
		this.egilme7Gunluk2 = egilme7Gunluk2;
	}

	/**
	 * @return the egilme7Gunluk3
	 */
	public BigDecimal getEgilme7Gunluk3() {
		return egilme7Gunluk3;
	}

	/**
	 * @param egilme7Gunluk3
	 *            the egilme7Gunluk3 to set
	 */
	public void setEgilme7Gunluk3(BigDecimal egilme7Gunluk3) {
		this.egilme7Gunluk3 = egilme7Gunluk3;
	}

	/**
	 * @return the egilme7GunlukOrtalama
	 */
	public BigDecimal getEgilme7GunlukOrtalama() {
		return egilme7GunlukOrtalama;
	}

	/**
	 * @param egilme7GunlukOrtalama
	 *            the egilme7GunlukOrtalama to set
	 */
	public void setEgilme7GunlukOrtalama(BigDecimal egilme7GunlukOrtalama) {
		this.egilme7GunlukOrtalama = egilme7GunlukOrtalama;
	}

	/**
	 * @return the eklemeZamani
	 */
	public Date getEklemeZamani() {
		return eklemeZamani;
	}

	/**
	 * @param eklemeZamani
	 *            the eklemeZamani to set
	 */
	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	/**
	 * @return the ekleyenKullanici
	 */
	public Integer getEkleyenKullanici() {
		return ekleyenKullanici;
	}

	/**
	 * @param ekleyenKullanici
	 *            the ekleyenKullanici to set
	 */
	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	/**
	 * @return the guncellemeZamani
	 */
	public Date getGuncellemeZamani() {
		return guncellemeZamani;
	}

	/**
	 * @param guncellemeZamani
	 *            the guncellemeZamani to set
	 */
	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	/**
	 * @return the guncelleyenKullanici
	 */
	public Integer getGuncelleyenKullanici() {
		return guncelleyenKullanici;
	}

	/**
	 * @param guncelleyenKullanici
	 *            the guncelleyenKullanici to set
	 */
	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	/**
	 * @return the testTarihi
	 */
	public Date getTestTarihi() {
		return testTarihi;
	}

	/**
	 * @param testTarihi
	 *            the testTarihi to set
	 */
	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	/**
	 * @return the vardiya
	 */
	public Integer getVardiya() {
		return vardiya;
	}

	/**
	 * @param vardiya
	 *            the vardiya to set
	 */
	public void setVardiya(Integer vardiya) {
		this.vardiya = vardiya;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the bagliTestId
	 */
	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the bagliGlobalId
	 */
	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}

	/**
	 * @param bagliGlobalId
	 *            the bagliGlobalId to set
	 */
	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the basinc2GunlukSonuc
	 */
	public Integer getBasinc2GunlukSonuc() {
		return basinc2GunlukSonuc;
	}

	/**
	 * @param basinc2GunlukSonuc
	 *            the basinc2GunlukSonuc to set
	 */
	public void setBasinc2GunlukSonuc(Integer basinc2GunlukSonuc) {
		this.basinc2GunlukSonuc = basinc2GunlukSonuc;
	}

	/**
	 * @return the basinc28GunlukSonuc
	 */
	public Integer getBasinc28GunlukSonuc() {
		return basinc28GunlukSonuc;
	}

	/**
	 * @param basinc28GunlukSonuc
	 *            the basinc28GunlukSonuc to set
	 */
	public void setBasinc28GunlukSonuc(Integer basinc28GunlukSonuc) {
		this.basinc28GunlukSonuc = basinc28GunlukSonuc;
	}

	/**
	 * @return the basinc7GunlukSonuc
	 */
	public Integer getBasinc7GunlukSonuc() {
		return basinc7GunlukSonuc;
	}

	/**
	 * @param basinc7GunlukSonuc
	 *            the basinc7GunlukSonuc to set
	 */
	public void setBasinc7GunlukSonuc(Integer basinc7GunlukSonuc) {
		this.basinc7GunlukSonuc = basinc7GunlukSonuc;
	}

	/**
	 * @return the egilme2GunlukSonuc
	 */
	public Integer getEgilme2GunlukSonuc() {
		return egilme2GunlukSonuc;
	}

	/**
	 * @param egilme2GunlukSonuc
	 *            the egilme2GunlukSonuc to set
	 */
	public void setEgilme2GunlukSonuc(Integer egilme2GunlukSonuc) {
		this.egilme2GunlukSonuc = egilme2GunlukSonuc;
	}

	/**
	 * @return the egilme28GunlukSonuc
	 */
	public Integer getEgilme28GunlukSonuc() {
		return egilme28GunlukSonuc;
	}

	/**
	 * @param egilme28GunlukSonuc
	 *            the egilme28GunlukSonuc to set
	 */
	public void setEgilme28GunlukSonuc(Integer egilme28GunlukSonuc) {
		this.egilme28GunlukSonuc = egilme28GunlukSonuc;
	}

	/**
	 * @return the egilme7GunlukSonuc
	 */
	public Integer getEgilme7GunlukSonuc() {
		return egilme7GunlukSonuc;
	}

	/**
	 * @param egilme7GunlukSonuc
	 *            the egilme7GunlukSonuc to set
	 */
	public void setEgilme7GunlukSonuc(Integer egilme7GunlukSonuc) {
		this.egilme7GunlukSonuc = egilme7GunlukSonuc;
	}
}