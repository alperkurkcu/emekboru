package com.emekboru.jpa.personel;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the personel_aktivite_bilgileri database table.
 * 
 */
@Entity
@Table(name="personel_aktivite_bilgileri")
public class PersonelAktiviteBilgileri implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PERSONEL_AKTIVITE_BILGILERI_ID_GENERATOR", sequenceName="PERSONEL_AKTIVITE_BILGILERI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PERSONEL_AKTIVITE_BILGILERI_ID_GENERATOR")
	@Column(name="ID")
	private Integer id;
	
	@Column(name="aciklama")
	private String aciklama;

	@Column(name="basari_durumu_id")
	private Integer basariDurumuId;

	@Column(name="egitim_adi")
	private String egitimAdi;

	@Column(name="eklenme_tarihi")
	private Timestamp eklenmeTarihi;

	@Column(name="kimlik_id")
	private Integer kimlikId;

	@Column(name="kullanici_id_ekleyen")
	private Integer kullaniciIdEkleyen;

	@Column(name="kullanici_id_guncelleyen")
	private Integer kullaniciIdGuncelleyen;

	@Column(name="sertifika_belgesi_path")
	private String sertifikaBelgesiPath;

	@Column(name="sertifika_bilgisi")
	private String sertifikaBilgisi;

	@Column(name="son_guncellenme_tarihi")
	private Timestamp sonGuncellenmeTarihi;

	private Integer sure;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="tarih")
	private Date tarih;

	@Column(name="veren_kurulus")
	private String verenKurulus;

	public PersonelAktiviteBilgileri() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Integer getBasariDurumuId() {
		return this.basariDurumuId;
	}

	public void setBasariDurumuId(Integer basariDurumuId) {
		this.basariDurumuId = basariDurumuId;
	}

	public String getEgitimAdi() {
		return this.egitimAdi;
	}

	public void setEgitimAdi(String egitimAdi) {
		this.egitimAdi = egitimAdi;
	}

	public Timestamp getEklenmeTarihi() {
		return this.eklenmeTarihi;
	}

	public void setEklenmeTarihi(Timestamp eklenmeTarihi) {
		this.eklenmeTarihi = eklenmeTarihi;
	}

	public Integer getKimlikId() {
		return this.kimlikId;
	}

	public void setKimlikId(Integer kimlikId) {
		this.kimlikId = kimlikId;
	}

	public Integer getKullaniciIdEkleyen() {
		return this.kullaniciIdEkleyen;
	}

	public void setKullaniciIdEkleyen(Integer kullaniciIdEkleyen) {
		this.kullaniciIdEkleyen = kullaniciIdEkleyen;
	}

	public Integer getKullaniciIdGuncelleyen() {
		return this.kullaniciIdGuncelleyen;
	}

	public void setKullaniciIdGuncelleyen(Integer kullaniciIdGuncelleyen) {
		this.kullaniciIdGuncelleyen = kullaniciIdGuncelleyen;
	}

	public String getSertifikaBelgesiPath() {
		return this.sertifikaBelgesiPath;
	}

	public void setSertifikaBelgesiPath(String sertifikaBelgesiPath) {
		this.sertifikaBelgesiPath = sertifikaBelgesiPath;
	}

	public String getSertifikaBilgisi() {
		return this.sertifikaBilgisi;
	}

	public void setSertifikaBilgisi(String sertifikaBilgisi) {
		this.sertifikaBilgisi = sertifikaBilgisi;
	}

	public Timestamp getSonGuncellenmeTarihi() {
		return this.sonGuncellenmeTarihi;
	}

	public void setSonGuncellenmeTarihi(Timestamp sonGuncellenmeTarihi) {
		this.sonGuncellenmeTarihi = sonGuncellenmeTarihi;
	}

	public Integer getSure() {
		return this.sure;
	}

	public void setSure(Integer sure) {
		this.sure = sure;
	}

	public Date getTarih() {
		return this.tarih;
	}

	public void setTarih(Date tarih) {
		this.tarih = tarih;
	}

	public String getVerenKurulus() {
		return this.verenKurulus;
	}

	public void setVerenKurulus(String verenKurulus) {
		this.verenKurulus = verenKurulus;
	}

}