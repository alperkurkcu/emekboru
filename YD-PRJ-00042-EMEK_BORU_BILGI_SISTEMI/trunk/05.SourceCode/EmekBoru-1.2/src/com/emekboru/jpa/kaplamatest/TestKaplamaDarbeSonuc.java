package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;

/**
 * The persistent class for the test_kaplama_darbe_sonuc database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaDarbeSonuc.findAll", query = "SELECT r FROM TestKaplamaDarbeSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_kaplama_darbe_sonuc")
public class TestKaplamaDarbeSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KAPLAMA_DARBE_SONUC_ID_GENERATOR", sequenceName = "TEST_KAPLAMA_DARBE_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KAPLAMA_DARBE_SONUC_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Column(name = "aciklama", length = 255)
	private String aciklama;

	@Column(name = "darbe_adedi", nullable = false)
	private Integer darbeAdedi;

	@Column(name = "darbe_enerjisi", nullable = false)
	private Integer darbeEnerjisi;

	// @Column(name="destructive_test_id", nullable=false)
	// private Integer destructiveTestId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	// @Column(name="global_id", nullable=false)
	// private Integer globalId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	// @Column(name="pipe_id", nullable=false)
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@Column(nullable = false)
	private Integer sonuc;

	@Column(name = "test_sicakligi", nullable = false)
	private Integer testSicakligi;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi", nullable = false)
	private Date testTarihi;

	@Column(name = "test_voltaji", nullable = false)
	private Integer testVoltaji;

	@Column(name = "malzemesi", nullable = false)
	private String malzemesi;

	@Column(name = "malzeme_lotu", nullable = false)
	private Integer malzemeLotu;

	// @Column(name = "holiday_pinhole", nullable = false)
	// private Integer holidayPinhole;

	public TestKaplamaDarbeSonuc() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Integer getDarbeAdedi() {
		return this.darbeAdedi;
	}

	public void setDarbeAdedi(Integer darbeAdedi) {
		this.darbeAdedi = darbeAdedi;
	}

	public Integer getDarbeEnerjisi() {
		return this.darbeEnerjisi;
	}

	public void setDarbeEnerjisi(Integer darbeEnerjisi) {
		this.darbeEnerjisi = darbeEnerjisi;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Integer getTestSicakligi() {
		return this.testSicakligi;
	}

	public void setTestSicakligi(Integer testSicakligi) {
		this.testSicakligi = testSicakligi;
	}

	public Date getTestTarihi() {
		return this.testTarihi;
	}

	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}

	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}

	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the sonuc
	 */
	public Integer getSonuc() {
		return sonuc;
	}

	/**
	 * @param sonuc
	 *            the sonuc to set
	 */
	public void setSonuc(Integer sonuc) {
		this.sonuc = sonuc;
	}

	/**
	 * @return the testVoltaji
	 */
	public Integer getTestVoltaji() {
		return testVoltaji;
	}

	/**
	 * @param testVoltaji
	 *            the testVoltaji to set
	 */
	public void setTestVoltaji(Integer testVoltaji) {
		this.testVoltaji = testVoltaji;
	}

	/**
	 * @return the malzemesi
	 */
	public String getMalzemesi() {
		return malzemesi;
	}

	/**
	 * @param malzemesi
	 *            the malzemesi to set
	 */
	public void setMalzemesi(String malzemesi) {
		this.malzemesi = malzemesi;
	}

	/**
	 * @return the malzemeLotu
	 */
	public Integer getMalzemeLotu() {
		return malzemeLotu;
	}

	/**
	 * @param malzemeLotu
	 *            the malzemeLotu to set
	 */
	public void setMalzemeLotu(Integer malzemeLotu) {
		this.malzemeLotu = malzemeLotu;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}