package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.config.Test;
import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the agirlik_tests_spec database table.
 * 
 */
@NamedQueries({
		@NamedQuery(name = "ATS.silmeKontrolu", query = "SELECT r.testId FROM AgirlikTestsSpec r WHERE r.globalId = :prmGlobalId and r.salesItem.itemId=:prmItemId"),
		@NamedQuery(name = "ATS.seciliAgirlikTestTanim", query = "SELECT r FROM AgirlikTestsSpec r WHERE r.globalId = :prmGlobalId and r.salesItem.itemId=:prmItemId"),
		@NamedQuery(name = "ATS.findByItemId", query = "SELECT r FROM AgirlikTestsSpec r WHERE r.salesItem.itemId=:prmItemId") })
@Entity
@Table(name = "agirlik_tests_spec")
public class AgirlikTestsSpec implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final int MALZEME_AGIRLIK = 1029;
	public static final int AGIRLIK_DUSURME = 1030;

	@Id
	@SequenceGenerator(name = "AGIRLIK_TESTS_SPEC_GENERATOR", sequenceName = "AGIRLIK_TESTS_SPEC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AGIRLIK_TESTS_SPEC_GENERATOR")
	@Column(name = "test_id")
	private Integer testId;

	@Column(name = "completed")
	private Boolean completed;

	@Column(name = "explanation")
	private String explanation;

	@Column(name = "global_id")
	private Integer globalId;

	@Column(name = "name")
	private String name;

	@Column(name = "numune_boyutu")
	private String numuneBoyutu;

	@Column(name = "numune_kesme_max")
	private Float numuneKesmeMax;

	@Column(name = "numune_kesme_min")
	private Float numuneKesmeMin;

	// @JoinColumn(name = "order_id", referencedColumnName = "order_id",
	// insertable = false)
	// @ManyToOne(fetch = FetchType.EAGER)
	// private Order order;

	@Column(name = "sicaklik")
	private String sicaklik;

	@Column(name = "type")
	private String type;

	// @ManyToOne
	// @JoinColumns({
	// @JoinColumn(name = "order_id", referencedColumnName = "order_id",
	// insertable = false, updatable = false),
	// @JoinColumn(name = "global_id", referencedColumnName = "global_id",
	// insertable = false, updatable = false) })
	// private DestructiveTestsSpec mainDestructiveTestSpecs;

	// entegrasyon
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	private SalesItem salesItem;

	@ManyToOne
	@JoinColumns({
			@JoinColumn(name = "sales_item_id", referencedColumnName = "sales_item_id", insertable = false, updatable = false),
			@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false, updatable = false) })
	private DestructiveTestsSpec salesDestructiveTestSpecs;

	// entegrasyon
	public AgirlikTestsSpec() {
	}

	public AgirlikTestsSpec(Test t) {

		globalId = t.getGlobalId();
		name = t.getDisplayName();
	}

	public void reset() {

		testId = 0;
		explanation = "";
		numuneBoyutu = "";
		numuneKesmeMax = (float) 0.0;
		numuneKesmeMin = (float) 0.0;
		sicaklik = "";
		type = "";
	}

	// GETTERS AND SETTERS
	public Integer getTestId() {
		return this.testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	public Boolean getCompleted() {
		return this.completed;
	}

	public void setCompleted(Boolean completed) {
		this.completed = completed;
	}

	public String getExplanation() {
		return this.explanation;
	}

	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}

	public Integer getGlobalId() {
		return this.globalId;
	}

	public void setGlobalId(Integer globalId) {
		this.globalId = globalId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public DestructiveTestsSpec getSalesDestructiveTestSpecs() {
		return salesDestructiveTestSpecs;
	}

	public void setSalesDestructiveTestSpecs(
			DestructiveTestsSpec salesDestructiveTestSpecs) {
		this.salesDestructiveTestSpecs = salesDestructiveTestSpecs;
	}

	public Float getNumuneKesmeMax() {
		return numuneKesmeMax;
	}

	public void setNumuneKesmeMax(Float numuneKesmeMax) {
		this.numuneKesmeMax = numuneKesmeMax;
	}

	public Float getNumuneKesmeMin() {
		return numuneKesmeMin;
	}

	public void setNumuneKesmeMin(Float numuneKesmeMin) {
		this.numuneKesmeMin = numuneKesmeMin;
	}

	/**
	 * @return the numuneBoyutu
	 */
	public String getNumuneBoyutu() {
		return numuneBoyutu;
	}

	/**
	 * @param numuneBoyutu
	 *            the numuneBoyutu to set
	 */
	public void setNumuneBoyutu(String numuneBoyutu) {
		this.numuneBoyutu = numuneBoyutu;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the sicaklik
	 */
	public String getSicaklik() {
		return sicaklik;
	}

	/**
	 * @param sicaklik
	 *            the sicaklik to set
	 */
	public void setSicaklik(String sicaklik) {
		this.sicaklik = sicaklik;
	}

}