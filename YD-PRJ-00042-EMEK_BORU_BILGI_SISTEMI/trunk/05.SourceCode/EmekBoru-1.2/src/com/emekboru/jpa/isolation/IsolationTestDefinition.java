package com.emekboru.jpa.isolation;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.emekboru.jpa.IncelemeGrubu;
import com.emekboru.jpa.Order;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.utils.IsolationType;

@Entity
@Table(name = "isolation_test_definition")
@NamedQueries({
		@NamedQuery(name = "IsolationTestDefinition.findIsolationByItemIdAndIsolationType", query = "SELECT e FROM IsolationTestDefinition e where e.isolationType =?1 and e.salesItem=?2 order by e.id"),
		@NamedQuery(name = "IsolationTestDefinition.findById", query = "SELECT e FROM IsolationTestDefinition e where e.id =?1") })
public class IsolationTestDefinition {

	@Id
	@Column(name = "ID")
	@SequenceGenerator(name = "AUTOMATIC_ISOLATION_TEST_GENERATOR", sequenceName = "AUTOMATIC_ISOLATION_TEST_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AUTOMATIC_ISOLATION_TEST_GENERATOR")
	private Integer id;

	@Column(name = "test_adi")
	private String testAdi;

	@Column(name = "testkodu")
	private IsolationTestType testKodu;

	@Column(name = "uygulamastandardi")
	private String uygulamaStandardi;

	@Column(name = "degerlendirmestandardi")
	private String degerlendirmeStandardi;

	@Column(name = "siklik")
	private String siklik;

	@Column(name = "isolationtype")
	private IsolationType isolationType;

	// @ManyToOne(fetch = FetchType.EAGER)
	// @JoinColumn
	// private Order order;

	@Basic
	@Column(name = "order_order_id", insertable = false, updatable = false)
	private Integer orderOrderId;

	@JoinColumn(name = "order_order_id", referencedColumnName = "order_id", insertable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private Order order;

	// entegrasyon
	@Basic
	@Column(name = "sales_item_id", insertable = false, updatable = false)
	private Integer salesItemId;

	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private SalesItem salesItem;
	// entegrasyon

	// @Basic
	// @Column(name = "incelemegrubu")
	// private Integer incelemeGrubu;

	@Column(name = "aciklama")
	private String aciklama;

	@Column(name = "hasDetay")
	private Boolean hasDetay;

	@Column(name = "hasSonuc")
	private Boolean hasSonuc;

	@Transient
	private String typePanelId;

	@Column(name = "global_id")
	private Integer globalId;

	// coating test specs
	/*
	 * @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	 * "mainIsolationDefinition") private BetonBasmaTestsSpec
	 * betonBasmaTestsSpecs;
	 * 
	 * @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	 * "mainIsolationDefinition") private BetonEgmeTestsSpec
	 * betonEgmeTestsSpecs;
	 * 
	 * @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	 * "mainIsolationDefinition") private BukmeIsolationTestsSpec
	 * bukmeIsolationTestsSpecs;
	 * 
	 * @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	 * "mainIsolationDefinition") private DarbeTestsSpec darbeTestsSpecs;
	 * 
	 * @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	 * "mainIsolationDefinition") private DeliciUcaKarsiDirencTestsSpec
	 * deliciUcaKarsiDirencTestsSpecs;
	 * 
	 * @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	 * "mainIsolationDefinition") private HolidayTestsSpec holidayTestsSpecs;
	 * 
	 * @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	 * "mainIsolationDefinition") private KaplamaKalinlikTestsSpec
	 * kaplamaKalinlikTestsSpecs;
	 * 
	 * @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	 * "mainIsolationDefinition") private KatodikSoyulmaTestsSpec
	 * katodikSoyulmaTestsSpecs;
	 * 
	 * @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	 * "mainIsolationDefinition") private PolyolefinUzamaTestsSpec
	 * polyolefinUzamaTestsSpecs;
	 * 
	 * @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	 * "mainIsolationDefinition") private SertlikTestsSpec sertlikTestsSpecs;
	 * 
	 * @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	 * "mainIsolationDefinition") private SicakSuyaKarsiDirencTestsSpec
	 * sicakSuyaKarsiDirencTestsSpecs;
	 * 
	 * @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	 * "mainIsolationDefinition") private TozBoyaDSCTestsSpec
	 * tozBoyaDSCTestsSpecs;
	 * 
	 * @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	 * "mainIsolationDefinition") private YapismaTestsSpec yapismaTestsSpecs;
	 * 
	 * @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	 * "mainIsolationDefinition") private YuzeyKontrolTestsSpec
	 * yuzeyKontrolTestsSpecs;
	 * 
	 * @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	 * "mainIsolationDefinition") private IncelemeGrubu incelemeGruplari;
	 */

	// entegrasyon
	// coating test specs
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesIsolationDefinition")
	private BetonBasmaTestsSpec betonBasmaTestsSpecs;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesIsolationDefinition")
	private BetonEgmeTestsSpec betonEgmeTestsSpecs;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesIsolationDefinition")
	private BukmeIsolationTestsSpec bukmeIsolationTestsSpecs;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesIsolationDefinition")
	private DarbeTestsSpec darbeTestsSpecs;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesIsolationDefinition")
	private DeliciUcaKarsiDirencTestsSpec deliciUcaKarsiDirencTestsSpecs;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesIsolationDefinition")
	private HolidayTestsSpec holidayTestsSpecs;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesIsolationDefinition")
	private KaplamaKalinlikTestsSpec kaplamaKalinlikTestsSpecs;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesIsolationDefinition")
	private KatodikSoyulmaTestsSpec katodikSoyulmaTestsSpecs;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesIsolationDefinition")
	private PolyolefinUzamaTestsSpec polyolefinUzamaTestsSpecs;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesIsolationDefinition")
	private SertlikTestsSpec sertlikTestsSpecs;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesIsolationDefinition")
	private TozBoyaDSCTestsSpec tozBoyaDSCTestsSpecs;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesIsolationDefinition")
	private YapismaTestsSpec yapismaTestsSpecs;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesIsolationDefinition")
	private YuzeyKontrolTestsSpec yuzeyKontrolTestsSpecs;

	@JoinColumn(name = "incelemegrubu", referencedColumnName = "ID", insertable = false)
	@ManyToOne(fetch = FetchType.EAGER)
	private IncelemeGrubu incelemeGrubu;

	public IsolationTestDefinition() {

		incelemeGrubu = new IncelemeGrubu();
	}

	// setters getters

	public PolyolefinUzamaTestsSpec getPolyolefinUzamaTestsSpecs() {
		return polyolefinUzamaTestsSpecs;
	}

	public void setPolyolefinUzamaTestsSpecs(
			PolyolefinUzamaTestsSpec polyolefinUzamaTestsSpecs) {
		this.polyolefinUzamaTestsSpecs = polyolefinUzamaTestsSpecs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTestAdi() {
		return testAdi;
	}

	public void setTestAdi(String testAdi) {
		this.testAdi = testAdi;
	}

	public IsolationTestType getTestKodu() {
		return testKodu;
	}

	public void setTestKodu(IsolationTestType testKodu) {
		this.testKodu = testKodu;
	}

	public String getUygulamaStandardi() {
		return uygulamaStandardi;
	}

	public void setUygulamaStandardi(String uygulamaStandardi) {
		this.uygulamaStandardi = uygulamaStandardi;
	}

	public String getDegerlendirmeStandardi() {
		return degerlendirmeStandardi;
	}

	public void setDegerlendirmeStandardi(String degerlendirmeStandardi) {
		this.degerlendirmeStandardi = degerlendirmeStandardi;
	}

	public String getSiklik() {
		return siklik;
	}

	public void setSiklik(String siklik) {
		this.siklik = siklik;
	}

	public IsolationType getIsolationType() {
		return isolationType;
	}

	public void setIsolationType(IsolationType isolationType) {
		this.isolationType = isolationType;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Boolean getHasDetay() {
		return hasDetay;
	}

	public void setHasDetay(Boolean hasDetay) {
		this.hasDetay = hasDetay;
	}

	public Boolean getHasSonuc() {
		return hasSonuc;
	}

	public void setHasSonuc(Boolean hasSonuc) {
		this.hasSonuc = hasSonuc;
	}

	public Integer getGlobalId() {
		return globalId;
	}

	public void setGlobalId(Integer globalId) {
		this.globalId = globalId;
	}

	public BetonBasmaTestsSpec getBetonBasmaTestsSpecs() {
		return betonBasmaTestsSpecs;
	}

	public void setBetonBasmaTestsSpecs(BetonBasmaTestsSpec betonBasmaTestsSpecs) {
		this.betonBasmaTestsSpecs = betonBasmaTestsSpecs;
	}

	public BetonEgmeTestsSpec getBetonEgmeTestsSpecs() {
		return betonEgmeTestsSpecs;
	}

	public void setBetonEgmeTestsSpecs(BetonEgmeTestsSpec betonEgmeTestsSpecs) {
		this.betonEgmeTestsSpecs = betonEgmeTestsSpecs;
	}

	public BukmeIsolationTestsSpec getBukmeIsolationTestsSpecs() {
		return bukmeIsolationTestsSpecs;
	}

	public void setBukmeIsolationTestsSpecs(
			BukmeIsolationTestsSpec bukmeIsolationTestsSpecs) {
		this.bukmeIsolationTestsSpecs = bukmeIsolationTestsSpecs;
	}

	public DarbeTestsSpec getDarbeTestsSpecs() {
		return darbeTestsSpecs;
	}

	public void setDarbeTestsSpecs(DarbeTestsSpec darbeTestsSpecs) {
		this.darbeTestsSpecs = darbeTestsSpecs;
	}

	public DeliciUcaKarsiDirencTestsSpec getDeliciUcaKarsiDirencTestsSpecs() {
		return deliciUcaKarsiDirencTestsSpecs;
	}

	public void setDeliciUcaKarsiDirencTestsSpecs(
			DeliciUcaKarsiDirencTestsSpec deliciUcaKarsiDirencTestsSpecs) {
		this.deliciUcaKarsiDirencTestsSpecs = deliciUcaKarsiDirencTestsSpecs;
	}

	public HolidayTestsSpec getHolidayTestsSpecs() {
		return holidayTestsSpecs;
	}

	public void setHolidayTestsSpecs(HolidayTestsSpec holidayTestsSpecs) {
		this.holidayTestsSpecs = holidayTestsSpecs;
	}

	public KaplamaKalinlikTestsSpec getKaplamaKalinlikTestsSpecs() {
		return kaplamaKalinlikTestsSpecs;
	}

	public void setKaplamaKalinlikTestsSpecs(
			KaplamaKalinlikTestsSpec kaplamaKalinlikTestsSpecs) {
		this.kaplamaKalinlikTestsSpecs = kaplamaKalinlikTestsSpecs;
	}

	public KatodikSoyulmaTestsSpec getKatodikSoyulmaTestsSpecs() {
		return katodikSoyulmaTestsSpecs;
	}

	public void setKatodikSoyulmaTestsSpecs(
			KatodikSoyulmaTestsSpec katodikSoyulmaTestsSpecs) {
		this.katodikSoyulmaTestsSpecs = katodikSoyulmaTestsSpecs;
	}

	public SertlikTestsSpec getSertlikTestsSpecs() {
		return sertlikTestsSpecs;
	}

	public void setSertlikTestsSpecs(SertlikTestsSpec sertlikTestsSpecs) {
		this.sertlikTestsSpecs = sertlikTestsSpecs;
	}

	public TozBoyaDSCTestsSpec getTozBoyaDSCTestsSpecs() {
		return tozBoyaDSCTestsSpecs;
	}

	public void setTozBoyaDSCTestsSpecs(TozBoyaDSCTestsSpec tozBoyaDSCTestsSpecs) {
		this.tozBoyaDSCTestsSpecs = tozBoyaDSCTestsSpecs;
	}

	public YapismaTestsSpec getYapismaTestsSpecs() {
		return yapismaTestsSpecs;
	}

	public void setYapismaTestsSpecs(YapismaTestsSpec yapismaTestsSpecs) {
		this.yapismaTestsSpecs = yapismaTestsSpecs;
	}

	public YuzeyKontrolTestsSpec getYuzeyKontrolTestsSpecs() {
		return yuzeyKontrolTestsSpecs;
	}

	public void setYuzeyKontrolTestsSpecs(
			YuzeyKontrolTestsSpec yuzeyKontrolTestsSpecs) {
		this.yuzeyKontrolTestsSpecs = yuzeyKontrolTestsSpecs;
	}

	public IncelemeGrubu getIncelemeGrubu() {
		return incelemeGrubu;
	}

	public void setIncelemeGrubu(IncelemeGrubu incelemeGrubu) {
		this.incelemeGrubu = incelemeGrubu;
	}

	public String getTypePanelId() {
		return IsolationTestsUtil.getPanelIdByTestCode(testKodu);
	}

}
