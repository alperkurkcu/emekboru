package com.emekboru.jpa.satinalma;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the satinalma_durum database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "SatinalmaDurum.findByFormId", query = "SELECT r FROM SatinalmaDurum r where r.formId=:prmFormId order by r.id") })
@Table(name = "satinalma_durum")
public class SatinalmaDurum implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	// @SequenceGenerator(name = "SATINALMA_DURUM_ID_GENERATOR", sequenceName =
	// "SATINALMA_DURUM_SEQ", allocationSize = 1)
	// @GeneratedValue(strategy = GenerationType.SEQUENCE, generator =
	// "SATINALMA_DURUM_ID_GENERATOR")
	@Column(name = "ID")
	private Integer id;

	@Column(name = "title")
	private String title;

	@OneToMany
	private List<SatinalmaTeklifIsteme> teklifIstemeList;

	@Column(name = "form_id")
	private Integer formId;

	public SatinalmaDurum() {
	}

	@Override
	public boolean equals(Object obj) {

		if (!(obj instanceof SatinalmaDurum))
			return false;

		if (this.id == ((SatinalmaDurum) obj).id)
			return true;

		return false;
	}

	// setters getters
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<SatinalmaTeklifIsteme> getTeklifIstemeList() {
		return teklifIstemeList;
	}

	public void setTeklifIstemeList(List<SatinalmaTeklifIsteme> teklifIstemeList) {
		this.teklifIstemeList = teklifIstemeList;
	}

	public Integer getFormId() {
		return formId;
	}

	public void setFormId(Integer formId) {
		this.formId = formId;
	}

}