package com.emekboru.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the boru_kalite_kaplama_turleri database table.
 * 
 */

@NamedQueries({ @NamedQuery(name = "BoruKaliteKaplamaTurleri.findAll", query = "SELECT r FROM BoruKaliteKaplamaTurleri r WHERE r.salesItem.itemId=:prmItemId order by r.eklemeZamani") })
@Entity
@Table(name = "boru_kalite_kaplama_turleri")
public class BoruKaliteKaplamaTurleri implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "BORU_KALITE_KAPLAMA_TURLERI_ID_GENERATOR", sequenceName = "BORU_KALITE_KAPLAMA_TURLERI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BORU_KALITE_KAPLAMA_TURLERI_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "dis_astar")
	private Boolean disAstar;

	@Column(name = "dis_boya")
	private Boolean disBoya;

	@Column(name = "dis_ciplak")
	private Boolean disCiplak;

	@Column(name = "dis_polietilen")
	private Boolean disPolietilen;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "ic_astar")
	private Boolean icAstar;

	@Column(name = "ic_beton")
	private Boolean icBeton;

	@Column(name = "ic_boya")
	private Boolean icBoya;

	@Column(name = "ic_ciplak")
	private Boolean icCiplak;

	@Basic
	@Column(name = "sales_item_id", nullable = false, updatable = false, insertable = false)
	private Integer salesItemId;

	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private SalesItem salesItem;

	@Column(name = "tamir_sayisi")
	private Integer tamirSayisi;

	@Column(name = "ic_boya_ilk_kat", nullable = false)
	private Integer icBoyaIlkKat;

	@Column(name = "ic_boya_ikinci_kat", nullable = false)
	private Integer icBoyaIkinciKat;

	@Column(name = "ic_boya_ucuncu_kat", nullable = false)
	private Integer icBoyaUcuncuKat;

	@Column(name = "dis_boya_ilk_kat", nullable = false)
	private Integer disBoyaIlkKat;

	@Column(name = "dis_boya_ikinci_kat", nullable = false)
	private Integer disBoyaIkinciKat;

	@Column(name = "dis_boya_ucuncu_kat", nullable = false)
	private Integer disBoyaUcuncuKat;

	public BoruKaliteKaplamaTurleri() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getDisAstar() {
		return this.disAstar;
	}

	public void setDisAstar(Boolean disAstar) {
		this.disAstar = disAstar;
	}

	public Boolean getDisBoya() {
		return this.disBoya;
	}

	public void setDisBoya(Boolean disBoya) {
		this.disBoya = disBoya;
	}

	public Boolean getDisCiplak() {
		return this.disCiplak;
	}

	public void setDisCiplak(Boolean disCiplak) {
		this.disCiplak = disCiplak;
	}

	public Boolean getDisPolietilen() {
		return this.disPolietilen;
	}

	public void setDisPolietilen(Boolean disPolietilen) {
		this.disPolietilen = disPolietilen;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Boolean getIcAstar() {
		return this.icAstar;
	}

	public void setIcAstar(Boolean icAstar) {
		this.icAstar = icAstar;
	}

	public Boolean getIcBeton() {
		return this.icBeton;
	}

	public void setIcBeton(Boolean icBeton) {
		this.icBeton = icBeton;
	}

	public Boolean getIcBoya() {
		return this.icBoya;
	}

	public void setIcBoya(Boolean icBoya) {
		this.icBoya = icBoya;
	}

	public Boolean getIcCiplak() {
		return this.icCiplak;
	}

	public void setIcCiplak(Boolean icCiplak) {
		this.icCiplak = icCiplak;
	}

	public Integer getTamirSayisi() {
		return this.tamirSayisi;
	}

	public void setTamirSayisi(Integer tamirSayisi) {
		this.tamirSayisi = tamirSayisi;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getIcBoyaIlkKat() {
		return icBoyaIlkKat;
	}

	public void setIcBoyaIlkKat(Integer icBoyaIlkKat) {
		this.icBoyaIlkKat = icBoyaIlkKat;
	}

	public Integer getIcBoyaIkinciKat() {
		return icBoyaIkinciKat;
	}

	public void setIcBoyaIkinciKat(Integer icBoyaIkinciKat) {
		this.icBoyaIkinciKat = icBoyaIkinciKat;
	}

	public Integer getIcBoyaUcuncuKat() {
		return icBoyaUcuncuKat;
	}

	public void setIcBoyaUcuncuKat(Integer icBoyaUcuncuKat) {
		this.icBoyaUcuncuKat = icBoyaUcuncuKat;
	}

	public Integer getDisBoyaIlkKat() {
		return disBoyaIlkKat;
	}

	public void setDisBoyaIlkKat(Integer disBoyaIlkKat) {
		this.disBoyaIlkKat = disBoyaIlkKat;
	}

	public Integer getDisBoyaIkinciKat() {
		return disBoyaIkinciKat;
	}

	public void setDisBoyaIkinciKat(Integer disBoyaIkinciKat) {
		this.disBoyaIkinciKat = disBoyaIkinciKat;
	}

	public Integer getDisBoyaUcuncuKat() {
		return disBoyaUcuncuKat;
	}

	public void setDisBoyaUcuncuKat(Integer disBoyaUcuncuKat) {
		this.disBoyaUcuncuKat = disBoyaUcuncuKat;
	}

}