package com.emekboru.jpa.sales.customer;

import java.io.Serializable;

import javax.persistence.*;

import com.emekboru.jpa.Employee;

@Entity
@Table(name = "sales_join_crm_visit_employee")
public class SalesJoinCRMVisitEmployee implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5088580685948674671L;

	@Id
	@Column(name = "join_id")
	@SequenceGenerator(name = "SALES_JOIN_CRM_VISIT_EMPLOYEE_GENERATOR", sequenceName = "SALES_JOIN_CRM_VISIT_EMPLOYEE_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_JOIN_CRM_VISIT_EMPLOYEE_GENERATOR")
	private int joinId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "employee_id", referencedColumnName = "employee_id", updatable = false)
	private Employee employee;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "visit_id", referencedColumnName = "sales_crm_visit_id", updatable = false)
	private SalesCRMVisit visit;

	public SalesJoinCRMVisitEmployee() {
		employee = new Employee();
		visit = new SalesCRMVisit();
	}

	/**
	 * GETTERS AND SETTERS
	 */

	public int getJoinId() {
		return joinId;
	}

	public SalesCRMVisit getVisit() {
		return visit;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setJoinId(Integer joinId) {
		this.joinId = joinId;
	}

	public void setVisit(SalesCRMVisit visit) {
		this.visit = visit;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
