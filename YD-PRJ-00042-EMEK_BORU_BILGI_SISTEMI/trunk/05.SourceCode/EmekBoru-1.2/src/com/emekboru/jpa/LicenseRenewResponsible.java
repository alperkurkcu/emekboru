package com.emekboru.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.emekboru.jpa.listeners.LicenseRRListener;

@Entity
@Table(name = "license_renew_responsible")
@EntityListeners(LicenseRRListener.class)
public class LicenseRenewResponsible implements Serializable{

	private static final long serialVersionUID = 370392673054538824L;

	@Id
	@Column(name = "license_renew_responsible_id")
	@SequenceGenerator(name="LICENSE_RENEW_RESPONSIBLE_GENERATOR", sequenceName="LICENSE_RENEW_RESPONSIBLE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="LICENSE_RENEW_RESPONSIBLE_GENERATOR")	
	private Integer licenseRenewResponsibleId;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "previous_expiration_date")
	private Date previousExpirationDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "renew_date")
	private Date renewDate;
	
	@Column(name = "note")
	private String note;
	
	@Column(name = "renew_authority")
	private String renewAuthority;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "employee_id", referencedColumnName = "employee_id", updatable = false)
	private Employee employee;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "license_id", referencedColumnName = "license_id", updatable = false)
	private License license;
	
	
	@Transient
	private boolean renewed;
	
	public LicenseRenewResponsible(){
		
		employee = new Employee();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof LicenseRenewResponsible){
			if(((LicenseRenewResponsible) obj).getLicenseRenewResponsibleId()
					.equals(this.getLicenseRenewResponsibleId()))
				return true;
		}
		return false;
	}

	/**
	 * 			GETTERS AND SETTERS
	 */
	public Integer getLicenseRenewResponsibleId() {
		return licenseRenewResponsibleId;
	}

	public void setLicenseRenewResponsibleId(Integer licenseRenewResponsibleId) {
		this.licenseRenewResponsibleId = licenseRenewResponsibleId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public License getLicense() {
		return license;
	}

	public void setLicense(License license) {
		this.license = license;
	}

	public Date getRenewDate() {
		return renewDate;
	}

	public void setRenewDate(Date renewDate) {
		this.renewDate = renewDate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getRenewAuthority() {
		return renewAuthority;
	}

	public void setRenewAuthority(String renewAuthority) {
		this.renewAuthority = renewAuthority;
	}

	public Date getPreviousExpirationDate() {
		return previousExpirationDate;
	}

	public void setPreviousExpirationDate(Date previousExpirationDate) {
		this.previousExpirationDate = previousExpirationDate;
	}

	public boolean getRenewed() {
		return renewed;
	}

	public void setRenewed(boolean renewed) {
		this.renewed = renewed;
	}
}
