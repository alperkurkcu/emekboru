package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;

/**
 * The persistent class for the test_kaplama_bukme_sonuc database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaBukmeSonuc.findAll", query = "SELECT r FROM TestKaplamaBukmeSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_kaplama_bukme_sonuc")
public class TestKaplamaBukmeSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KAPLAMA_BUKME_SONUC_ID_GENERATOR", sequenceName = "TEST_KAPLAMA_BUKME_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KAPLAMA_BUKME_SONUC_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Column(name = "aciklama", length = 255)
	private String aciklama;

	// @Column(name="destructive_test_id", nullable=false)
	// private Integer destructiveTestId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ekleme_zamani")
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	// @Column(name="global_id", nullable=false)
	// private Integer globalId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "guncelleme_zamani", nullable = false)
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici", nullable = false)
	private Integer guncelleyenKullanici;

	@Column(name = "mandrel_capi", nullable = false)
	private Integer mandrelCapi;

	@Column(name = "numune_bekletme_sicakligi", nullable = false)
	private Integer numuneBekletmeSicakligi;

	@Column(name = "numune_bekletme_suresi", nullable = false)
	private Integer numuneBekletmeSuresi;

	// @Column(name="pipe_id", nullable=false)
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@Column(name = "sogutma_sicakligi", nullable = false)
	private Integer sogutmaSicakligi;

	@Column(name = "sogutma_suresi", nullable = false)
	private Integer sogutmaSuresi;

	@Column(nullable = false)
	private Boolean sonuc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi", nullable = false)
	private Date testTarihi;

	@Column(name = "sapma_acisi")
	private Integer sapmaAcisi;

	@Column(name = "numune_boyutu")
	private Integer numuneBoyutu;

	@Column(name = "toplam_kaplama_kalinligi")
	private Integer toplamKaplamaKalinligi;

	@Column(name = "lot_no")
	private Integer lotNo;

	public TestKaplamaBukmeSonuc() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Date getEklemeZamani() {
		return eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getGuncellemeZamani() {
		return guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Integer getNumuneBekletmeSicakligi() {
		return numuneBekletmeSicakligi;
	}

	public void setNumuneBekletmeSicakligi(Integer numuneBekletmeSicakligi) {
		this.numuneBekletmeSicakligi = numuneBekletmeSicakligi;
	}

	public Integer getNumuneBekletmeSuresi() {
		return numuneBekletmeSuresi;
	}

	public void setNumuneBekletmeSuresi(Integer numuneBekletmeSuresi) {
		this.numuneBekletmeSuresi = numuneBekletmeSuresi;
	}

	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}

	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}

	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public Integer getSogutmaSicakligi() {
		return sogutmaSicakligi;
	}

	public void setSogutmaSicakligi(Integer sogutmaSicakligi) {
		this.sogutmaSicakligi = sogutmaSicakligi;
	}

	public Integer getSogutmaSuresi() {
		return sogutmaSuresi;
	}

	public void setSogutmaSuresi(Integer sogutmaSuresi) {
		this.sogutmaSuresi = sogutmaSuresi;
	}

	public Boolean getSonuc() {
		return sonuc;
	}

	public void setSonuc(Boolean sonuc) {
		this.sonuc = sonuc;
	}

	public Date getTestTarihi() {
		return testTarihi;
	}

	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	public Integer getMandrelCapi() {
		return mandrelCapi;
	}

	public void setMandrelCapi(Integer mandrelCapi) {
		this.mandrelCapi = mandrelCapi;
	}

	/**
	 * @return the sapmaAcisi
	 */
	public Integer getSapmaAcisi() {
		return sapmaAcisi;
	}

	/**
	 * @param sapmaAcisi
	 *            the sapmaAcisi to set
	 */
	public void setSapmaAcisi(Integer sapmaAcisi) {
		this.sapmaAcisi = sapmaAcisi;
	}

	/**
	 * @return the numuneBoyutu
	 */
	public Integer getNumuneBoyutu() {
		return numuneBoyutu;
	}

	/**
	 * @param numuneBoyutu
	 *            the numuneBoyutu to set
	 */
	public void setNumuneBoyutu(Integer numuneBoyutu) {
		this.numuneBoyutu = numuneBoyutu;
	}

	/**
	 * @return the toplamKaplamaKalinligi
	 */
	public Integer getToplamKaplamaKalinligi() {
		return toplamKaplamaKalinligi;
	}

	/**
	 * @param toplamKaplamaKalinligi
	 *            the toplamKaplamaKalinligi to set
	 */
	public void setToplamKaplamaKalinligi(Integer toplamKaplamaKalinligi) {
		this.toplamKaplamaKalinligi = toplamKaplamaKalinligi;
	}

	/**
	 * @return the lotNo
	 */
	public Integer getLotNo() {
		return lotNo;
	}

	/**
	 * @param lotNo
	 *            the lotNo to set
	 */
	public void setLotNo(Integer lotNo) {
		this.lotNo = lotNo;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}