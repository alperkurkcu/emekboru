package com.emekboru.jpa.employee;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.Employee;
import com.emekboru.jpa.common.CommonEducationalStatus;

/**
 * The persistent class for the employee_education database table.
 * 
 */
@Entity
@Table(name = "employee_education")
@NamedQuery(name = "EmployeeEducation.findAll", query = "SELECT e FROM EmployeeEducation e")
public class EmployeeEducation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "EMPLOYEE_EDUCATION_ID_GENERATOR", sequenceName = "EMPLOYEE_EDUCATION_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMPLOYEE_EDUCATION_ID_GENERATOR")
	@Column(name = "id")
	private Integer id;

	@Column(name = "department")
	private String department;

	@Column(name = "description")
	private String description;

	@JoinColumn(name = "common_educational_status_id", referencedColumnName = "id", insertable = false)
	private CommonEducationalStatus commonEducationalStatus;

	@Column(name = "ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@ManyToOne
	@JoinColumn(name = "employee_id", insertable = false)
	private Employee employee;

	@Column(name = "graduate_year")
	private Integer graduateYear;

	@Column(name = "graduated_instutition")
	private String graduatedInstutition;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	public EmployeeEducation() {
		this.commonEducationalStatus = new CommonEducationalStatus();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDepartment() {
		return this.department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public CommonEducationalStatus getCommonEducationalStatus() {
		return commonEducationalStatus;
	}

	public void setCommonEducationalStatus(
			CommonEducationalStatus commonEducationalStatus) {
		this.commonEducationalStatus = commonEducationalStatus;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Integer getGraduateYear() {
		return this.graduateYear;
	}

	public void setGraduateYear(Integer graduateYear) {
		this.graduateYear = graduateYear;
	}

	public String getGraduatedInstutition() {
		return this.graduatedInstutition;
	}

	public void setGraduatedInstutition(String graduatedInstutition) {
		this.graduatedInstutition = graduatedInstutition;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

}