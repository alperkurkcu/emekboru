package com.emekboru.jpa.personel;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the personel_performans_bilgileri database table.
 * 
 */
@Entity
@Table(name="personel_performans_bilgileri")
public class PersonelPerformansBilgileri implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PERSONEL_PERFORMANS_BILGILERI_ID_GENERATOR", sequenceName="PERSONEL_PERFORMANS_BILGILERI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PERSONEL_PERFORMANS_BILGILERI_ID_GENERATOR")
	@Column(name="ID")
	private Integer id;
	@Column(name="aciklama")
	private String aciklama;

	@Column(name="eklenme_tarihi")
	private Timestamp eklenmeTarihi;

	@Column(name="kimlik_id")
	private Integer kimlikId;

	@Column(name="kullanici_id_ekleyen")
	private Integer kullaniciIdEkleyen;

	@Column(name="kullanici_id_guncelleyen")
	private Integer kullaniciIdGuncelleyen;

	@Column(name="performans_degerlendirmesi")
	private String performansDegerlendirmesi;

	@Column(name="performans_donemi_id")
	private Integer performansDonemiId;

	@Column(name="performans_puani")
	private Integer performansPuani;

	@Column(name="son_guncellenme_tarihi")
	private Timestamp sonGuncellenmeTarihi;

	public PersonelPerformansBilgileri() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Timestamp getEklenmeTarihi() {
		return this.eklenmeTarihi;
	}

	public void setEklenmeTarihi(Timestamp eklenmeTarihi) {
		this.eklenmeTarihi = eklenmeTarihi;
	}

	public Integer getKimlikId() {
		return this.kimlikId;
	}

	public void setKimlikId(Integer kimlikId) {
		this.kimlikId = kimlikId;
	}

	public Integer getKullaniciIdEkleyen() {
		return this.kullaniciIdEkleyen;
	}

	public void setKullaniciIdEkleyen(Integer kullaniciIdEkleyen) {
		this.kullaniciIdEkleyen = kullaniciIdEkleyen;
	}

	public Integer getKullaniciIdGuncelleyen() {
		return this.kullaniciIdGuncelleyen;
	}

	public void setKullaniciIdGuncelleyen(Integer kullaniciIdGuncelleyen) {
		this.kullaniciIdGuncelleyen = kullaniciIdGuncelleyen;
	}

	public String getPerformansDegerlendirmesi() {
		return this.performansDegerlendirmesi;
	}

	public void setPerformansDegerlendirmesi(String performansDegerlendirmesi) {
		this.performansDegerlendirmesi = performansDegerlendirmesi;
	}

	public Integer getPerformansDonemiId() {
		return this.performansDonemiId;
	}

	public void setPerformansDonemiId(Integer performansDonemiId) {
		this.performansDonemiId = performansDonemiId;
	}

	public Integer getPerformansPuani() {
		return this.performansPuani;
	}

	public void setPerformansPuani(Integer performansPuani) {
		this.performansPuani = performansPuani;
	}

	public Timestamp getSonGuncellenmeTarihi() {
		return this.sonGuncellenmeTarihi;
	}

	public void setSonGuncellenmeTarihi(Timestamp sonGuncellenmeTarihi) {
		this.sonGuncellenmeTarihi = sonGuncellenmeTarihi;
	}

}