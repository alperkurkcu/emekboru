package com.emekboru.jpa.sales.customer;

import java.io.Serializable;

import javax.persistence.*;

import com.emekboru.jpa.sales.customer.SalesContactPeople;

@Entity
@Table(name = "sales_join_crm_visit_contact_people")
public class SalesJoinCRMVisitContactPeople implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6337238723468501926L;

	@Id
	@Column(name = "join_id")
	@SequenceGenerator(name = "SALES_JOIN_CRM_VISIT_CONTACT_PEOPLE_GENERATOR", sequenceName = "SALES_JOIN_CRM_VISIT_CONTACT_PEOPLE_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_JOIN_CRM_VISIT_CONTACT_PEOPLE_GENERATOR")
	private int joinId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "contact_people_id", referencedColumnName = "sales_contact_person_id", updatable = false)
	private SalesContactPeople contactPeople;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "visit_id", referencedColumnName = "sales_crm_visit_id", updatable = false)
	private SalesCRMVisit visit;

	public SalesJoinCRMVisitContactPeople() {
		contactPeople = new SalesContactPeople();
		visit = new SalesCRMVisit();
	}

	/**
	 * GETTERS AND SETTERS
	 */

	public int getJoinId() {
		return joinId;
	}

	public SalesCRMVisit getVisit() {
		return visit;
	}

	public SalesContactPeople getContactPeople() {
		return contactPeople;
	}

	public void setJoinId(Integer joinId) {
		this.joinId = joinId;
	}

	public void setVisit(SalesCRMVisit visit) {
		this.visit = visit;
	}

	public void setContactPeople(SalesContactPeople contactPeople) {
		this.contactPeople = contactPeople;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
