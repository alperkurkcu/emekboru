package com.emekboru.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.sales.customer.SalesCustomer;

@Entity
@Table(name = "sold_edw")
public class SoldEdw implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "sold_edw_id")
	@SequenceGenerator(name = "SOLD_EDW_GENERATOR", sequenceName = "SOLD_EDW_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SOLD_EDW_GENERATOR")
	private Integer soldEdwId;

	@Column(name = "price")
	private double price;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date")
	private Date date;

	@Column(name = "explanation")
	private String explanation;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "currency_code", referencedColumnName = "currency_code")
	private Currency currency;

	// @ManyToOne(fetch = FetchType.EAGER)
	// @JoinColumn(name = "customer_id", referencedColumnName = "customer_id")
	// private Customer customer;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "electrode_dust_wire_id", referencedColumnName = "electrode_dust_wire_id")
	private ElectrodeDustWire edw;

	// entegre
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "customer_id", referencedColumnName = "sales_customer_id")
	private SalesCustomer salesCustomer;

	// entegre

	public SoldEdw() {

		edw = new ElectrodeDustWire();
		currency = new Currency();
		// customer = new Customer();
		salesCustomer = new SalesCustomer();
	}

	// **********************************************************************//
	// GETTERS AND SETTERS //
	// **********************************************************************//
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getExplanation() {
		return explanation;
	}

	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getSoldEdwId() {
		return soldEdwId;
	}

	public void setSoldEdwId(Integer soldEdwId) {
		this.soldEdwId = soldEdwId;
	}

	public ElectrodeDustWire getEdw() {
		return edw;
	}

	public void setEdw(ElectrodeDustWire edw) {
		this.edw = edw;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public SalesCustomer getSalesCustomer() {
		return salesCustomer;
	}

	public void setSalesCustomer(SalesCustomer salesCustomer) {
		this.salesCustomer = salesCustomer;
	}

}
