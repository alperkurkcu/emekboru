package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the coat_raw_material database table.
 * 
 */
@Entity
@Table(name = "coat_material_type")
public class CoatMaterialType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "coat_material_type_id")
	@SequenceGenerator(name = "COAT_MATERIAL_TYPE_GENERATOR", sequenceName = "COAT_MATERIAL_TYPE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COAT_MATERIAL_TYPE_GENERATOR")
	private Integer coatMaterialTypeId;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	public CoatMaterialType() {
	}

	public Integer getCoatMaterialTypeId() {
		return coatMaterialTypeId;
	}

	public void setCoatMaterialTypeId(Integer coatMaterialId) {
		this.coatMaterialTypeId = coatMaterialId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}