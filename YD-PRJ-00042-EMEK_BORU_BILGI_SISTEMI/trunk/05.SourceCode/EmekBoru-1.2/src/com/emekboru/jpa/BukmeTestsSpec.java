package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.config.Test;
import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the bukme_tests_spec database table.
 * 
 */
@NamedQueries({
		@NamedQuery(name = "BTS.kontrol", query = "SELECT r.testId FROM BukmeTestsSpec r WHERE r.globalId = :prmGlobalId and r.salesItem.itemId=:prmItemId"),
		@NamedQuery(name = "BTS.seciliBukmeTestTanim", query = "SELECT r FROM BukmeTestsSpec r WHERE r.globalId = :prmGlobalId and r.salesItem.itemId=:prmItemId"),
		@NamedQuery(name = "BTS.findByItemId", query = "SELECT r FROM BukmeTestsSpec r WHERE r.salesItem.itemId=:prmItemId") })
@Entity
@Table(name = "bukme_tests_spec")
public class BukmeTestsSpec implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "BUKME_TESTS_SPEC_GENERATOR", sequenceName = "BUKME_TESTS_SPEC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BUKME_TESTS_SPEC_GENERATOR")
	@Column(name = "test_id")
	private Integer testId;

	@Column(name = "bukme_acisi")
	private Float bukmeAcisi;

	@Column(name = "completed")
	private Boolean completed;

	@Column(name = "explanation")
	private String explanation;

	@Column(name = "global_id")
	private Integer globalId;

	@Column(name = "mandrel_capi")
	private Float mandrelCapi;

	@Column(name = "name")
	private String name;

	@Column(name = "code")
	private String code;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	private SalesItem salesItem;

	@ManyToOne
	@JoinColumns({
			@JoinColumn(name = "sales_item_id", referencedColumnName = "sales_item_id", insertable = false, updatable = false),
			@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false, updatable = false) })
	private DestructiveTestsSpec salesDestructiveTestSpecs;

	// entegrasyon

	public BukmeTestsSpec() {
	}

	public BukmeTestsSpec(Test t) {

		globalId = t.getGlobalId();
		name = t.getDisplayName();
		code = t.getCode();
	}

	public void reset() {

		testId = 0;
		bukmeAcisi = (float) 0.0;
		completed = true;
		explanation = "";
		mandrelCapi = (float) 0.0;
	}

	// GETTERS AND SETTERS
	public Integer getTestId() {
		return this.testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	public Boolean getCompleted() {
		return this.completed;
	}

	public void setCompleted(Boolean completed) {
		this.completed = completed;
	}

	public String getExplanation() {
		return this.explanation;
	}

	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}

	public Integer getGlobalId() {
		return this.globalId;
	}

	public void setGlobalId(Integer globalId) {
		this.globalId = globalId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public DestructiveTestsSpec getSalesDestructiveTestSpecs() {
		return salesDestructiveTestSpecs;
	}

	public void setSalesDestructiveTestSpecs(
			DestructiveTestsSpec salesDestructiveTestSpecs) {
		this.salesDestructiveTestSpecs = salesDestructiveTestSpecs;
	}

	public Float getBukmeAcisi() {
		return bukmeAcisi;
	}

	public void setBukmeAcisi(Float bukmeAcisi) {
		this.bukmeAcisi = bukmeAcisi;
	}

	public Float getMandrelCapi() {
		return mandrelCapi;
	}

	public void setMandrelCapi(Float mandrelCapi) {
		this.mandrelCapi = mandrelCapi;
	}

}