package com.emekboru.jpa.rulo;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.Pipe;

/**
 * The persistent class for the rulo_pipe_link database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "RuloPipeLink.findByPipeId", query = "select o from RuloPipeLink o where o.pipe.pipeId=:prmPipeId") })
@Table(name = "rulo_pipe_link")
public class RuloPipeLink implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "RULO_PIPE_LINK_LINKID_GENERATOR", sequenceName = "RULO_PIPE_LINK_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RULO_PIPE_LINK_LINKID_GENERATOR")
	@Column(name = "link_id", unique = true, nullable = false)
	private Integer ID;

	@Basic
	@Column(name = "pipe_id", nullable = false, insertable = false, updatable = false)
	private Integer pipeId;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	// @Column(name = "rulo_id", nullable = false)
	// private Integer ruloId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "rulo_id", referencedColumnName = "rulo_id", insertable = false)
	private Rulo rulo;

	@Column(name = "rulo_yil", nullable = false, length = 2147483647)
	private String ruloYil;

	public RuloPipeLink() {
	}

	public String getRuloYil() {
		return this.ruloYil;
	}

	public void setRuloYil(String ruloYil) {
		this.ruloYil = ruloYil;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public Rulo getRulo() {
		return rulo;
	}

	public void setRulo(Rulo rulo) {
		this.rulo = rulo;
	}

}