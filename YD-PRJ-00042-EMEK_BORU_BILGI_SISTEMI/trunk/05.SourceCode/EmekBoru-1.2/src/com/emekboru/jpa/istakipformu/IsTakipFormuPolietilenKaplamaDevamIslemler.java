package com.emekboru.jpa.istakipformu;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the is_takip_formu_polietilen_kaplama_devam_islemler
 * database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "IsTakipFormuPolietilenKaplamaDevamIslemler.findAll", query = "SELECT r FROM IsTakipFormuPolietilenKaplamaDevamIslemler r WHERE r.salesItem.itemId=:prmItemId") })
@Table(name = "is_takip_formu_polietilen_kaplama_devam_islemler")
public class IsTakipFormuPolietilenKaplamaDevamIslemler implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IS_TAKIP_FORMU_POLIETILEN_KAPLAMA_DEVAM_ISLEMLER_ID_GENERATOR", sequenceName = "IS_TAKIP_FORMU_POLIETILEN_KAPLAMA_DEVAM_ISLEMLER_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IS_TAKIP_FORMU_POLIETILEN_KAPLAMA_DEVAM_ISLEMLER_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "extruder_a")
	private Integer extruderA;

	@Column(name = "extruder_b")
	private Integer extruderB;

	@Column(name = "bobin_voltaj")
	private Integer bobinVoltaj;

	@Column(name = "cekme_testi_min")
	private Integer cekmeTestiMin;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "intercoat_suresi")
	private float intercoatSuresi;

	@Column(name = "isitma_oncesi")
	private float isitmaOncesi;

	@Column(name = "isitma_sonrasi", length = 2147483647)
	private String isitmaSonrasi;

	@Column(name = "kaplama_hizi")
	private Integer kaplamaHizi;

	// @Column(name = "pipe_id", nullable = false)
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@Column(name = "polietilen_film_sicakligi")
	private Integer polietilenFilmSicakligi;

	// @Column(name = "sales_item_id", nullable = false)
	// private Integer salesItemId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	private SalesItem salesItem;

	@Column(name = "tabanca_mesafesi")
	private Integer tabancaMesafesi;

	@Temporal(TemporalType.DATE)
	@Column(name = "tarih")
	private Date tarih;

	@Column(nullable = false, name = "vardiya")
	private Integer vardiya;

	@Column(name = "extruder_yap")
	private Integer extruderYap;

	@Column(name = "yapistirici_film_sicakligi")
	private Integer yapistiriciFilmSicakligi;

	public IsTakipFormuPolietilenKaplamaDevamIslemler() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBobinVoltaj() {
		return this.bobinVoltaj;
	}

	public void setBobinVoltaj(Integer bobinVoltaj) {
		this.bobinVoltaj = bobinVoltaj;
	}

	public Integer getCekmeTestiMin() {
		return this.cekmeTestiMin;
	}

	public void setCekmeTestiMin(Integer cekmeTestiMin) {
		this.cekmeTestiMin = cekmeTestiMin;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public float getIntercoatSuresi() {
		return this.intercoatSuresi;
	}

	public void setIntercoatSuresi(float intercoatSuresi) {
		this.intercoatSuresi = intercoatSuresi;
	}

	public float getIsitmaOncesi() {
		return this.isitmaOncesi;
	}

	public void setIsitmaOncesi(float isitmaOncesi) {
		this.isitmaOncesi = isitmaOncesi;
	}

	public String getIsitmaSonrasi() {
		return this.isitmaSonrasi;
	}

	public void setIsitmaSonrasi(String isitmaSonrasi) {
		this.isitmaSonrasi = isitmaSonrasi;
	}

	public Integer getKaplamaHizi() {
		return this.kaplamaHizi;
	}

	public void setKaplamaHizi(Integer kaplamaHizi) {
		this.kaplamaHizi = kaplamaHizi;
	}

	public Integer getPolietilenFilmSicakligi() {
		return this.polietilenFilmSicakligi;
	}

	public void setPolietilenFilmSicakligi(Integer polietilenFilmSicakligi) {
		this.polietilenFilmSicakligi = polietilenFilmSicakligi;
	}

	public Integer getTabancaMesafesi() {
		return this.tabancaMesafesi;
	}

	public void setTabancaMesafesi(Integer tabancaMesafesi) {
		this.tabancaMesafesi = tabancaMesafesi;
	}

	public Date getTarih() {
		return this.tarih;
	}

	public void setTarih(Date tarih) {
		this.tarih = tarih;
	}

	public Integer getVardiya() {
		return this.vardiya;
	}

	public void setVardiya(Integer vardiya) {
		this.vardiya = vardiya;
	}

	public Integer getYapistiriciFilmSicakligi() {
		return this.yapistiriciFilmSicakligi;
	}

	public void setYapistiriciFilmSicakligi(Integer yapistiriciFilmSicakligi) {
		this.yapistiriciFilmSicakligi = yapistiriciFilmSicakligi;
	}

	public Integer getExtruderA() {
		return extruderA;
	}

	public void setExtruderA(Integer extruderA) {
		this.extruderA = extruderA;
	}

	public Integer getExtruderB() {
		return extruderB;
	}

	public void setExtruderB(Integer extruderB) {
		this.extruderB = extruderB;
	}

	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public Integer getExtruderYap() {
		return extruderYap;
	}

	public void setExtruderYap(Integer extruderYap) {
		this.extruderYap = extruderYap;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}