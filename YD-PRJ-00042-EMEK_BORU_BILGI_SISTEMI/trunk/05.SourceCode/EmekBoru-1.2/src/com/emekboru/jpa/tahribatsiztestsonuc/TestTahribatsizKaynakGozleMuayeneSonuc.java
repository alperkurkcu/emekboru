package com.emekboru.jpa.tahribatsiztestsonuc;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.rulo.RuloPipeLink;

/**
 * The persistent class for the test_tahribatsiz_kaynak_gozle_muayene_sonuc
 * database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestTahribatsizKaynakGozleMuayeneSonuc.findAll", query = "SELECT r FROM TestTahribatsizKaynakGozleMuayeneSonuc r WHERE r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_tahribatsiz_kaynak_gozle_muayene_sonuc")
public class TestTahribatsizKaynakGozleMuayeneSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_TAHRIBATSIZ_KAYNAK_GOZLE_MUAYENE_SONUC_ID_GENERATOR", sequenceName = "TEST_TAHRIBATSIZ_KAYNAK_GOZLE_MUAYENE_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_TAHRIBATSIZ_KAYNAK_GOZLE_MUAYENE_SONUC_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(length = 255, name = "aciklama")
	private String aciklama;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "koordinat_l", nullable = false)
	private Integer koordinatL;

	@Column(name = "koordinat_q", nullable = false)
	private Integer koordinatQ;

	// @Column(name = "pipe_id", nullable = false)
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	private Integer sonuc;

	@Column(name = "tamir_sonrasi_durum")
	private Integer tamirSonrasiDurum;

	@Column(name = "test_id")
	private Integer testId;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	private RuloPipeLink ruloPipeLink;

	private boolean c;

	private boolean kc;

	private boolean ye;

	private boolean tkn;

	private boolean syo;

	private boolean kyo;

	private boolean co;

	private boolean akm;

	private boolean an;

	private boolean b;

	private boolean ydka;

	@Column(name = "kib")
	private boolean kib;

	public TestTahribatsizKaynakGozleMuayeneSonuc() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getKoordinatL() {
		return this.koordinatL;
	}

	public void setKoordinatL(Integer koordinatL) {
		this.koordinatL = koordinatL;
	}

	public Integer getKoordinatQ() {
		return this.koordinatQ;
	}

	public void setKoordinatQ(Integer koordinatQ) {
		this.koordinatQ = koordinatQ;
	}

	public Integer getSonuc() {
		return this.sonuc;
	}

	public void setSonuc(Integer sonuc) {
		this.sonuc = sonuc;
	}

	public Integer getTamirSonrasiDurum() {
		return this.tamirSonrasiDurum;
	}

	public void setTamirSonrasiDurum(Integer tamirSonrasiDurum) {
		this.tamirSonrasiDurum = tamirSonrasiDurum;
	}

	public Integer getTestId() {
		return this.testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the ruloPipeLink
	 */
	public RuloPipeLink getRuloPipeLink() {
		return ruloPipeLink;
	}

	/**
	 * @param ruloPipeLink
	 *            the ruloPipeLink to set
	 */
	public void setRuloPipeLink(RuloPipeLink ruloPipeLink) {
		this.ruloPipeLink = ruloPipeLink;
	}

	/**
	 * @return the c
	 */
	public boolean isC() {
		return c;
	}

	/**
	 * @param c
	 *            the c to set
	 */
	public void setC(boolean c) {
		this.c = c;
	}

	/**
	 * @return the kc
	 */
	public boolean isKc() {
		return kc;
	}

	/**
	 * @param kc
	 *            the kc to set
	 */
	public void setKc(boolean kc) {
		this.kc = kc;
	}

	/**
	 * @return the ye
	 */
	public boolean isYe() {
		return ye;
	}

	/**
	 * @param ye
	 *            the ye to set
	 */
	public void setYe(boolean ye) {
		this.ye = ye;
	}

	/**
	 * @return the tkn
	 */
	public boolean isTkn() {
		return tkn;
	}

	/**
	 * @param tkn
	 *            the tkn to set
	 */
	public void setTkn(boolean tkn) {
		this.tkn = tkn;
	}

	/**
	 * @return the syo
	 */
	public boolean isSyo() {
		return syo;
	}

	/**
	 * @param syo
	 *            the syo to set
	 */
	public void setSyo(boolean syo) {
		this.syo = syo;
	}

	/**
	 * @return the kyo
	 */
	public boolean isKyo() {
		return kyo;
	}

	/**
	 * @param kyo
	 *            the kyo to set
	 */
	public void setKyo(boolean kyo) {
		this.kyo = kyo;
	}

	/**
	 * @return the co
	 */
	public boolean isCo() {
		return co;
	}

	/**
	 * @param co
	 *            the co to set
	 */
	public void setCo(boolean co) {
		this.co = co;
	}

	/**
	 * @return the akm
	 */
	public boolean isAkm() {
		return akm;
	}

	/**
	 * @param akm
	 *            the akm to set
	 */
	public void setAkm(boolean akm) {
		this.akm = akm;
	}

	/**
	 * @return the an
	 */
	public boolean isAn() {
		return an;
	}

	/**
	 * @param an
	 *            the an to set
	 */
	public void setAn(boolean an) {
		this.an = an;
	}

	/**
	 * @return the b
	 */
	public boolean isB() {
		return b;
	}

	/**
	 * @param b
	 *            the b to set
	 */
	public void setB(boolean b) {
		this.b = b;
	}

	/**
	 * @return the ydka
	 */
	public boolean isYdka() {
		return ydka;
	}

	/**
	 * @param ydka
	 *            the ydka to set
	 */
	public void setYdka(boolean ydka) {
		this.ydka = ydka;
	}

	/**
	 * @return the kib
	 */
	public boolean isKib() {
		return kib;
	}

	/**
	 * @param kib
	 *            the kib to set
	 */
	public void setKib(boolean kib) {
		this.kib = kib;
	}

}