package com.emekboru.jpa.sales.customer;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sales_customer_function_type")
public class SalesCustomerFunctionType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3794909456593006805L;

	@Id
	@Column(name = "function_id")
	private int functionId;

	@Column(name = "subject")
	private String subject;

	// @ManyToOne(fetch = FetchType.LAZY)
	// @JoinColumn(name = "function_id", referencedColumnName =
	// "type_of_function", insertable = false, updatable = false)
	// private SalesCustomer customer;

	public SalesCustomerFunctionType() {

	}

	@Override
	public boolean equals(Object o) {
		return this.getFunctionId() == ((SalesCustomerFunctionType) o)
				.getFunctionId();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public int getFunctionId() {
		return functionId;
	}

	public void setFunctionId(int functionId) {
		this.functionId = functionId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
