package com.emekboru.jpa.rulo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "rulo_test_cekme_boyuna")
public class RuloTestCekmeBoyuna {
	@Id
	@SequenceGenerator(name = "rulo_test_cekme_boyuna_generator", sequenceName = "rulo_test_cekme_boyuna_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rulo_test_cekme_boyuna_generator")
	@Column(name = "rulo_cekme_testi_boyuna_id")
	private Integer ruloTestCekmeBoyunaId;

	@Column(name = "rulo_cekme_testi_boyuna_genislik")
	private Float ruloTestCekmeBoyunaGenislik;

	@Column(name = "rulo_cekme_testi_boyuna_et_kalinligi")
	private Float ruloTestCekmeBoyunaEtKalinligi;

	@Column(name = "rulo_cekme_testi_boyuna_alan")
	private Float ruloTestCekmeBoyunaAlan;

	@Column(name = "rulo_cekme_testi_boyuna_akma_yuku")
	private Float ruloTestCekmeBoyunaAkmaYuku;

	@Column(name = "rulo_cekme_testi_boyuna_cekme_yuku")
	private Float ruloTestCekmeBoyunaCekmeYuku;

	@Column(name = "rulo_cekme_testi_boyuna_akma_dayanci")
	private Float ruloTestCekmeBoyunaAkmaDayanci;

	@Column(name = "rulo_cekme_testi_boyuna_cekme_dayanci")
	private Float ruloTestCekmeBoyunaCekmeDayanci;

	@Column(name = "rulo_cekme_testi_boyuna_akma_cekme")
	private Float ruloTestCekmeBoyunaAkmaCekme;

	@Column(name = "rulo_cekme_testi_boyuna_uzama")
	private Float ruloTestCekmeBoyunaUzama;

	@Temporal(TemporalType.DATE)
	@Column(name = "rulo_cekme_testi_boyuna_tarih")
	private Date ruloTestCekmeBoyunaTarih;

	@Column(name = "rulo_cekme_testi_boyuna_aciklama")
	private String ruloTestCekmeBoyunaAciklama;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "rulo_id", nullable = false)
	private Rulo rulo;

	@Column(name = "durum")
	private Integer durum = 0;

	@Column(name = "islem_yapan_id")
	private int islemYapanId;

	@Column(name = "islem_yapan_isim")
	private String islemYapanIsim;

	// setters getters
	public int getIslemYapanId() {
		return islemYapanId;
	}

	public void setIslemYapanId(int islemYapanId) {
		this.islemYapanId = islemYapanId;
	}

	public String getIslemYapanIsim() {
		return islemYapanIsim;
	}

	public void setIslemYapanIsim(String islemYapanIsim) {
		this.islemYapanIsim = islemYapanIsim;
	}

	public Integer getDurum() {
		return durum;
	}

	public void setDurum(Integer durum) {
		this.durum = durum;
	}

	public Rulo getRulo() {
		return rulo;
	}

	public void setRulo(Rulo rulo) {
		this.rulo = rulo;
	}

	public Integer getRuloTestCekmeBoyunaId() {
		return ruloTestCekmeBoyunaId;
	}

	public void setRuloTestCekmeBoyunaId(Integer ruloTestCekmeBoyunaId) {
		this.ruloTestCekmeBoyunaId = ruloTestCekmeBoyunaId;
	}

	public Date getRuloTestCekmeBoyunaTarih() {
		return ruloTestCekmeBoyunaTarih;
	}

	public void setRuloTestCekmeBoyunaTarih(Date ruloTestCekmeBoyunaTarih) {
		this.ruloTestCekmeBoyunaTarih = ruloTestCekmeBoyunaTarih;
	}

	public String getRuloTestCekmeBoyunaAciklama() {
		return ruloTestCekmeBoyunaAciklama;
	}

	public void setRuloTestCekmeBoyunaAciklama(
			String ruloTestCekmeBoyunaAciklama) {
		this.ruloTestCekmeBoyunaAciklama = ruloTestCekmeBoyunaAciklama;
	}

	public Float getRuloTestCekmeBoyunaGenislik() {
		return ruloTestCekmeBoyunaGenislik;
	}

	public void setRuloTestCekmeBoyunaGenislik(Float ruloTestCekmeBoyunaGenislik) {
		this.ruloTestCekmeBoyunaGenislik = ruloTestCekmeBoyunaGenislik;
	}

	public Float getRuloTestCekmeBoyunaEtKalinligi() {
		return ruloTestCekmeBoyunaEtKalinligi;
	}

	public void setRuloTestCekmeBoyunaEtKalinligi(
			Float ruloTestCekmeBoyunaEtKalinligi) {
		this.ruloTestCekmeBoyunaEtKalinligi = ruloTestCekmeBoyunaEtKalinligi;
	}

	public Float getRuloTestCekmeBoyunaAlan() {
		return ruloTestCekmeBoyunaAlan;
	}

	public void setRuloTestCekmeBoyunaAlan(Float ruloTestCekmeBoyunaAlan) {
		this.ruloTestCekmeBoyunaAlan = ruloTestCekmeBoyunaAlan;
	}

	public Float getRuloTestCekmeBoyunaAkmaYuku() {
		return ruloTestCekmeBoyunaAkmaYuku;
	}

	public void setRuloTestCekmeBoyunaAkmaYuku(Float ruloTestCekmeBoyunaAkmaYuku) {
		this.ruloTestCekmeBoyunaAkmaYuku = ruloTestCekmeBoyunaAkmaYuku;
	}

	public Float getRuloTestCekmeBoyunaCekmeYuku() {
		return ruloTestCekmeBoyunaCekmeYuku;
	}

	public void setRuloTestCekmeBoyunaCekmeYuku(Float ruloTestCekmeBoyunaCekmeYuku) {
		this.ruloTestCekmeBoyunaCekmeYuku = ruloTestCekmeBoyunaCekmeYuku;
	}

	public Float getRuloTestCekmeBoyunaAkmaDayanci() {
		return ruloTestCekmeBoyunaAkmaDayanci;
	}

	public void setRuloTestCekmeBoyunaAkmaDayanci(
			Float ruloTestCekmeBoyunaAkmaDayanci) {
		this.ruloTestCekmeBoyunaAkmaDayanci = ruloTestCekmeBoyunaAkmaDayanci;
	}

	public Float getRuloTestCekmeBoyunaCekmeDayanci() {
		return ruloTestCekmeBoyunaCekmeDayanci;
	}

	public void setRuloTestCekmeBoyunaCekmeDayanci(
			Float ruloTestCekmeBoyunaCekmeDayanci) {
		this.ruloTestCekmeBoyunaCekmeDayanci = ruloTestCekmeBoyunaCekmeDayanci;
	}

	public Float getRuloTestCekmeBoyunaAkmaCekme() {
		return ruloTestCekmeBoyunaAkmaCekme;
	}

	public void setRuloTestCekmeBoyunaAkmaCekme(Float ruloTestCekmeBoyunaAkmaCekme) {
		this.ruloTestCekmeBoyunaAkmaCekme = ruloTestCekmeBoyunaAkmaCekme;
	}

	public Float getRuloTestCekmeBoyunaUzama() {
		return ruloTestCekmeBoyunaUzama;
	}

	public void setRuloTestCekmeBoyunaUzama(Float ruloTestCekmeBoyunaUzama) {
		this.ruloTestCekmeBoyunaUzama = ruloTestCekmeBoyunaUzama;
	}

}
