package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MaintenancePlannedActivitiesPK implements Serializable{

	
	private static final long serialVersionUID = -208307748625889531L;

	@Column(name = "maintenance_plan_id")
	private Integer maintenancePlanId;
	
	@Column(name = "maintenance_activity_id")
	private Integer maintenanceActivityId;
	
	public MaintenancePlannedActivitiesPK(){
		
	}

	/**
	 * 		GETTERS AND SETTERS
	 */
	public Integer getMaintenancePlanId() {
		return maintenancePlanId;
	}

	public void setMaintenancePlanId(Integer maintenancePlanId) {
		this.maintenancePlanId = maintenancePlanId;
	}

	public Integer getMaintenanceActivityId() {
		return maintenanceActivityId;
	}

	public void setMaintenanceActivityId(Integer maintenanceActivityId) {
		this.maintenanceActivityId = maintenanceActivityId;
	}
	
}
