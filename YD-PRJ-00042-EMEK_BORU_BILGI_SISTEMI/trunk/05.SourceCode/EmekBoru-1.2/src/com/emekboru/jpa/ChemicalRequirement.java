package com.emekboru.jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.emekboru.jpa.sales.order.SalesItem;

@NamedQueries({
		@NamedQuery(name = "CR.kontrol", query = "SELECT r.chemicalReqId FROM ChemicalRequirement r WHERE r.salesItem.itemId=:prmItemId"),
		@NamedQuery(name = "CR.seciliChemicalRequirementTanim", query = "SELECT r FROM ChemicalRequirement r WHERE r.salesItem.itemId=:prmItemId") })
@Entity
@Table(name = "chemical_requirements")
public class ChemicalRequirement implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "chemical_req_id")
	@SequenceGenerator(name = "CHEMICAL_REQ_GENERATOR", sequenceName = "CHEMICAL_REQ_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CHEMICAL_REQ_GENERATOR")
	private Integer chemicalReqId;

	@Column(name = "min_al")
	private Double minAl;

	@Column(name = "min_b")
	private Double minB;

	@Column(name = "min_c")
	private Double minC;

	@Column(name = "min_ce1")
	private Double minCe1;

	@Column(name = "min_ce2")
	private Double minCe2;

	@Column(name = "min_ae")
	private Double minAe;

	@Column(name = "min_co")
	private Double minCo;

	@Column(name = "min_cr")
	private Double minCr;

	@Column(name = "min_cu")
	private Double minCu;

	@Column(name = "min_mn")
	private Double minMn;

	@Column(name = "min_mo")
	private Double minMo;

	@Column(name = "min_nb")
	private Double minNb;

	@Column(name = "min_ni")
	private Double minNi;

	@Column(name = "min_p")
	private Double minP;

	@Column(name = "min_s")
	private Double minS;

	@Column(name = "min_si")
	private Double minSi;

	@Column(name = "min_ti")
	private Double minTi;

	@Column(name = "min_v")
	private Double minV;

	@Column(name = "min_n")
	private Double minN;

	@Column(name = "max_al")
	private Double maxAl;

	@Column(name = "max_b")
	private Double maxB;

	@Column(name = "max_c")
	private Double maxC;

	@Column(name = "max_ce1")
	private Double maxCe1;

	@Column(name = "max_ce2")
	private Double maxCe2;

	@Column(name = "max_Ae")
	private Double maxAe;

	@Column(name = "max_co")
	private Double maxCo;

	@Column(name = "max_cr")
	private Double maxCr;

	@Column(name = "max_cu")
	private Double maxCu;

	@Column(name = "max_mn")
	private Double maxMn;

	@Column(name = "max_mo")
	private Double maxMo;

	@Column(name = "max_nb")
	private Double maxNb;

	@Column(name = "max_ni")
	private Double maxNi;

	@Column(name = "max_p")
	private Double maxP;

	@Column(name = "max_s")
	private Double maxS;

	@Column(name = "max_si")
	private Double maxSi;

	@Column(name = "max_ti")
	private Double maxTi;

	@Column(name = "max_v")
	private Double maxV;

	@Column(name = "max_n")
	private Double maxN;

	@Column(name = "min_w")
	private Double minW;

	@Column(name = "max_w")
	private Double maxW;

	@Column(name = "min_as")
	private Double minAs;

	@Column(name = "max_as")
	private Double maxAs;

	@Column(name = "min_Sb")
	private Double minSb;

	@Column(name = "max_Sb")
	private Double maxSb;

	@Column(name = "min_Sn")
	private Double minSn;

	@Column(name = "max_Sn")
	private Double maxSn;

	@Column(name = "min_pb")
	private Double minPb;

	@Column(name = "max_pb")
	private Double maxPb;

	@Column(name = "min_ca")
	private Double minCa;

	@Column(name = "max_ca")
	private Double maxCa;

	@Column(name = "aciklama")
	private String aciklama;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	private SalesItem salesItem;

	@Column(name = "documents")
	private String documents;

	@Transient
	private List<String> fileNames;

	@Transient
	private int fileNumber;

	@ManyToOne
	@JoinColumns({ @JoinColumn(name = "sales_item_id", referencedColumnName = "sales_item_id", insertable = false, updatable = false) })
	private DestructiveTestsSpec salesDestructiveTestSpecs;

	public ChemicalRequirement() {

	}

	// ***********************************************************************//
	// *********************** GETTERS AND SETTERS *************************//
	// ***********************************************************************//

	public Integer getChemicalReqId() {
		return chemicalReqId;
	}

	public void setChemicalReqId(Integer chemicalReqId) {
		this.chemicalReqId = chemicalReqId;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public String getDocuments() {
		return documents;
	}

	public void setDocuments(String documents) {
		this.documents = documents;
	}

	public List<String> getFileNames() {
		try {
			String[] names = documents.split("//");
			fileNames = new ArrayList<String>();

			for (int i = 1; i < names.length; i++) {
				fileNames.add(names[i]);
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return fileNames;
	}

	public void setFileNames(List<String> fileNames) {
		this.fileNames = fileNames;
	}

	public int getFileNumber() {
		try {
			String[] names = documents.split("//");

			fileNumber = names.length - 1;
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return fileNumber;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	/**
	 * @return the minAl
	 */
	public Double getMinAl() {
		return minAl;
	}

	/**
	 * @param minAl
	 *            the minAl to set
	 */
	public void setMinAl(Double minAl) {
		this.minAl = minAl;
	}

	/**
	 * @return the minB
	 */
	public Double getMinB() {
		return minB;
	}

	/**
	 * @param minB
	 *            the minB to set
	 */
	public void setMinB(Double minB) {
		this.minB = minB;
	}

	/**
	 * @return the minC
	 */
	public Double getMinC() {
		return minC;
	}

	/**
	 * @param minC
	 *            the minC to set
	 */
	public void setMinC(Double minC) {
		this.minC = minC;
	}

	/**
	 * @return the minCe1
	 */
	public Double getMinCe1() {
		return minCe1;
	}

	/**
	 * @param minCe1
	 *            the minCe1 to set
	 */
	public void setMinCe1(Double minCe1) {
		this.minCe1 = minCe1;
	}

	/**
	 * @return the minCe2
	 */
	public Double getMinCe2() {
		return minCe2;
	}

	/**
	 * @param minCe2
	 *            the minCe2 to set
	 */
	public void setMinCe2(Double minCe2) {
		this.minCe2 = minCe2;
	}

	/**
	 * @return the minAe
	 */
	public Double getMinAe() {
		return minAe;
	}

	/**
	 * @param minAe
	 *            the minAe to set
	 */
	public void setMinAe(Double minAe) {
		this.minAe = minAe;
	}

	/**
	 * @return the minCo
	 */
	public Double getMinCo() {
		return minCo;
	}

	/**
	 * @param minCo
	 *            the minCo to set
	 */
	public void setMinCo(Double minCo) {
		this.minCo = minCo;
	}

	/**
	 * @return the minCr
	 */
	public Double getMinCr() {
		return minCr;
	}

	/**
	 * @param minCr
	 *            the minCr to set
	 */
	public void setMinCr(Double minCr) {
		this.minCr = minCr;
	}

	/**
	 * @return the minCu
	 */
	public Double getMinCu() {
		return minCu;
	}

	/**
	 * @param minCu
	 *            the minCu to set
	 */
	public void setMinCu(Double minCu) {
		this.minCu = minCu;
	}

	/**
	 * @return the minMn
	 */
	public Double getMinMn() {
		return minMn;
	}

	/**
	 * @param minMn
	 *            the minMn to set
	 */
	public void setMinMn(Double minMn) {
		this.minMn = minMn;
	}

	/**
	 * @return the minMo
	 */
	public Double getMinMo() {
		return minMo;
	}

	/**
	 * @param minMo
	 *            the minMo to set
	 */
	public void setMinMo(Double minMo) {
		this.minMo = minMo;
	}

	/**
	 * @return the minNb
	 */
	public Double getMinNb() {
		return minNb;
	}

	/**
	 * @param minNb
	 *            the minNb to set
	 */
	public void setMinNb(Double minNb) {
		this.minNb = minNb;
	}

	/**
	 * @return the minNi
	 */
	public Double getMinNi() {
		return minNi;
	}

	/**
	 * @param minNi
	 *            the minNi to set
	 */
	public void setMinNi(Double minNi) {
		this.minNi = minNi;
	}

	/**
	 * @return the minP
	 */
	public Double getMinP() {
		return minP;
	}

	/**
	 * @param minP
	 *            the minP to set
	 */
	public void setMinP(Double minP) {
		this.minP = minP;
	}

	/**
	 * @return the minS
	 */
	public Double getMinS() {
		return minS;
	}

	/**
	 * @param minS
	 *            the minS to set
	 */
	public void setMinS(Double minS) {
		this.minS = minS;
	}

	/**
	 * @return the minSi
	 */
	public Double getMinSi() {
		return minSi;
	}

	/**
	 * @param minSi
	 *            the minSi to set
	 */
	public void setMinSi(Double minSi) {
		this.minSi = minSi;
	}

	/**
	 * @return the minTi
	 */
	public Double getMinTi() {
		return minTi;
	}

	/**
	 * @param minTi
	 *            the minTi to set
	 */
	public void setMinTi(Double minTi) {
		this.minTi = minTi;
	}

	/**
	 * @return the minV
	 */
	public Double getMinV() {
		return minV;
	}

	/**
	 * @param minV
	 *            the minV to set
	 */
	public void setMinV(Double minV) {
		this.minV = minV;
	}

	/**
	 * @return the minN
	 */
	public Double getMinN() {
		return minN;
	}

	/**
	 * @param minN
	 *            the minN to set
	 */
	public void setMinN(Double minN) {
		this.minN = minN;
	}

	/**
	 * @return the maxAl
	 */
	public Double getMaxAl() {
		return maxAl;
	}

	/**
	 * @param maxAl
	 *            the maxAl to set
	 */
	public void setMaxAl(Double maxAl) {
		this.maxAl = maxAl;
	}

	/**
	 * @return the maxB
	 */
	public Double getMaxB() {
		return maxB;
	}

	/**
	 * @param maxB
	 *            the maxB to set
	 */
	public void setMaxB(Double maxB) {
		this.maxB = maxB;
	}

	/**
	 * @return the maxC
	 */
	public Double getMaxC() {
		return maxC;
	}

	/**
	 * @param maxC
	 *            the maxC to set
	 */
	public void setMaxC(Double maxC) {
		this.maxC = maxC;
	}

	/**
	 * @return the maxCe1
	 */
	public Double getMaxCe1() {
		return maxCe1;
	}

	/**
	 * @param maxCe1
	 *            the maxCe1 to set
	 */
	public void setMaxCe1(Double maxCe1) {
		this.maxCe1 = maxCe1;
	}

	/**
	 * @return the maxCe2
	 */
	public Double getMaxCe2() {
		return maxCe2;
	}

	/**
	 * @param maxCe2
	 *            the maxCe2 to set
	 */
	public void setMaxCe2(Double maxCe2) {
		this.maxCe2 = maxCe2;
	}

	/**
	 * @return the maxAe
	 */
	public Double getMaxAe() {
		return maxAe;
	}

	/**
	 * @param maxAe
	 *            the maxAe to set
	 */
	public void setMaxAe(Double maxAe) {
		this.maxAe = maxAe;
	}

	/**
	 * @return the maxCo
	 */
	public Double getMaxCo() {
		return maxCo;
	}

	/**
	 * @param maxCo
	 *            the maxCo to set
	 */
	public void setMaxCo(Double maxCo) {
		this.maxCo = maxCo;
	}

	/**
	 * @return the maxCr
	 */
	public Double getMaxCr() {
		return maxCr;
	}

	/**
	 * @param maxCr
	 *            the maxCr to set
	 */
	public void setMaxCr(Double maxCr) {
		this.maxCr = maxCr;
	}

	/**
	 * @return the maxCu
	 */
	public Double getMaxCu() {
		return maxCu;
	}

	/**
	 * @param maxCu
	 *            the maxCu to set
	 */
	public void setMaxCu(Double maxCu) {
		this.maxCu = maxCu;
	}

	/**
	 * @return the maxMn
	 */
	public Double getMaxMn() {
		return maxMn;
	}

	/**
	 * @param maxMn
	 *            the maxMn to set
	 */
	public void setMaxMn(Double maxMn) {
		this.maxMn = maxMn;
	}

	/**
	 * @return the maxMo
	 */
	public Double getMaxMo() {
		return maxMo;
	}

	/**
	 * @param maxMo
	 *            the maxMo to set
	 */
	public void setMaxMo(Double maxMo) {
		this.maxMo = maxMo;
	}

	/**
	 * @return the maxNb
	 */
	public Double getMaxNb() {
		return maxNb;
	}

	/**
	 * @param maxNb
	 *            the maxNb to set
	 */
	public void setMaxNb(Double maxNb) {
		this.maxNb = maxNb;
	}

	/**
	 * @return the maxNi
	 */
	public Double getMaxNi() {
		return maxNi;
	}

	/**
	 * @param maxNi
	 *            the maxNi to set
	 */
	public void setMaxNi(Double maxNi) {
		this.maxNi = maxNi;
	}

	/**
	 * @return the maxP
	 */
	public Double getMaxP() {
		return maxP;
	}

	/**
	 * @param maxP
	 *            the maxP to set
	 */
	public void setMaxP(Double maxP) {
		this.maxP = maxP;
	}

	/**
	 * @return the maxS
	 */
	public Double getMaxS() {
		return maxS;
	}

	/**
	 * @param maxS
	 *            the maxS to set
	 */
	public void setMaxS(Double maxS) {
		this.maxS = maxS;
	}

	/**
	 * @return the maxSi
	 */
	public Double getMaxSi() {
		return maxSi;
	}

	/**
	 * @param maxSi
	 *            the maxSi to set
	 */
	public void setMaxSi(Double maxSi) {
		this.maxSi = maxSi;
	}

	/**
	 * @return the maxTi
	 */
	public Double getMaxTi() {
		return maxTi;
	}

	/**
	 * @param maxTi
	 *            the maxTi to set
	 */
	public void setMaxTi(Double maxTi) {
		this.maxTi = maxTi;
	}

	/**
	 * @return the maxV
	 */
	public Double getMaxV() {
		return maxV;
	}

	/**
	 * @param maxV
	 *            the maxV to set
	 */
	public void setMaxV(Double maxV) {
		this.maxV = maxV;
	}

	/**
	 * @return the maxN
	 */
	public Double getMaxN() {
		return maxN;
	}

	/**
	 * @param maxN
	 *            the maxN to set
	 */
	public void setMaxN(Double maxN) {
		this.maxN = maxN;
	}

	/**
	 * @return the minW
	 */
	public Double getMinW() {
		return minW;
	}

	/**
	 * @param minW
	 *            the minW to set
	 */
	public void setMinW(Double minW) {
		this.minW = minW;
	}

	/**
	 * @return the maxW
	 */
	public Double getMaxW() {
		return maxW;
	}

	/**
	 * @param maxW
	 *            the maxW to set
	 */
	public void setMaxW(Double maxW) {
		this.maxW = maxW;
	}

	/**
	 * @return the minAs
	 */
	public Double getMinAs() {
		return minAs;
	}

	/**
	 * @param minAs
	 *            the minAs to set
	 */
	public void setMinAs(Double minAs) {
		this.minAs = minAs;
	}

	/**
	 * @return the maxAs
	 */
	public Double getMaxAs() {
		return maxAs;
	}

	/**
	 * @param maxAs
	 *            the maxAs to set
	 */
	public void setMaxAs(Double maxAs) {
		this.maxAs = maxAs;
	}

	/**
	 * @return the minSb
	 */
	public Double getMinSb() {
		return minSb;
	}

	/**
	 * @param minSb
	 *            the minSb to set
	 */
	public void setMinSb(Double minSb) {
		this.minSb = minSb;
	}

	/**
	 * @return the maxSb
	 */
	public Double getMaxSb() {
		return maxSb;
	}

	/**
	 * @param maxSb
	 *            the maxSb to set
	 */
	public void setMaxSb(Double maxSb) {
		this.maxSb = maxSb;
	}

	/**
	 * @return the minSn
	 */
	public Double getMinSn() {
		return minSn;
	}

	/**
	 * @param minSn
	 *            the minSn to set
	 */
	public void setMinSn(Double minSn) {
		this.minSn = minSn;
	}

	/**
	 * @return the maxSn
	 */
	public Double getMaxSn() {
		return maxSn;
	}

	/**
	 * @param maxSn
	 *            the maxSn to set
	 */
	public void setMaxSn(Double maxSn) {
		this.maxSn = maxSn;
	}

	/**
	 * @return the minPb
	 */
	public Double getMinPb() {
		return minPb;
	}

	/**
	 * @param minPb
	 *            the minPb to set
	 */
	public void setMinPb(Double minPb) {
		this.minPb = minPb;
	}

	/**
	 * @return the maxPb
	 */
	public Double getMaxPb() {
		return maxPb;
	}

	/**
	 * @param maxPb
	 *            the maxPb to set
	 */
	public void setMaxPb(Double maxPb) {
		this.maxPb = maxPb;
	}

	/**
	 * @return the minCa
	 */
	public Double getMinCa() {
		return minCa;
	}

	/**
	 * @param minCa
	 *            the minCa to set
	 */
	public void setMinCa(Double minCa) {
		this.minCa = minCa;
	}

	/**
	 * @return the maxCa
	 */
	public Double getMaxCa() {
		return maxCa;
	}

	/**
	 * @param maxCa
	 *            the maxCa to set
	 */
	public void setMaxCa(Double maxCa) {
		this.maxCa = maxCa;
	}

	/**
	 * @param fileNumber
	 *            the fileNumber to set
	 */
	public void setFileNumber(int fileNumber) {
		this.fileNumber = fileNumber;
	}

	/**
	 * @return the salesDestructiveTestSpecs
	 */
	public DestructiveTestsSpec getSalesDestructiveTestSpecs() {
		return salesDestructiveTestSpecs;
	}

	/**
	 * @param salesDestructiveTestSpecs
	 *            the salesDestructiveTestSpecs to set
	 */
	public void setSalesDestructiveTestSpecs(
			DestructiveTestsSpec salesDestructiveTestSpecs) {
		this.salesDestructiveTestSpecs = salesDestructiveTestSpecs;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
