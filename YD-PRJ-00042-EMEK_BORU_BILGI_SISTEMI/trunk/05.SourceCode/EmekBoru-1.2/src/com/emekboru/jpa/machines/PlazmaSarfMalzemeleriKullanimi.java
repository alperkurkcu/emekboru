package com.emekboru.jpa.machines;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Machine;
import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the plazma_sarf_malzemeleri_kullanimi database
 * table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "PlazmaSarfMalzemeleriKullanimi.findAll", query = "SELECT r FROM PlazmaSarfMalzemeleriKullanimi r order by r.id DESC") })
@Table(name = "plazma_sarf_malzemeleri_kullanimi")
public class PlazmaSarfMalzemeleriKullanimi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "PLAZMA_SARF_MALZEMELERI_KULLANIMI_ID_GENERATOR", sequenceName = "PLAZMA_SARF_MALZEMELERI_KULLANIMI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PLAZMA_SARF_MALZEMELERI_KULLANIMI_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "bant_kenar_frezeleme_altlik")
	private Integer bantKenarFrezelemeAltlik;

	@Column(name = "bant_kenar_frezeleme_elmas")
	private Integer bantKenarFrezelemeElmas;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name = "machine_id", nullable = false)
	// private Integer machineId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "machine_id", referencedColumnName = "machine_id", insertable = false)
	private Machine machine;

	@Column(name = "electrode")
	private Integer electrode;

	@Column(name = "kaynak_yuksekligi_frezeleme_altlik")
	private Integer kaynakYuksekligiFrezelemeAltlik;

	@Column(name = "kaynak_yuksekligi_frezeleme_elmas")
	private Integer kaynakYuksekligiFrezelemeElmas;

	@Column(name = "main_body")
	private Integer mainBody;

	@Column(name = "nozzle")
	private Integer nozzle;

	@Column(name = "plazma_suyu")
	private Integer plazmaSuyu;

	@Column(name = "plazma_memesi")
	private Integer plazmaMemesi;

	@Column(name = "shield")
	private Integer shield;

	@Column(name = "shield_cap")
	private Integer shieldCap;

	@Column(name = "swirling_ring")
	private Integer swirlingRing;

	@Column(name = "vardiya")
	private Integer vardiya;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "vardiya_tarihi")
	private Date vardiyaTarihi;

	public PlazmaSarfMalzemeleriKullanimi() {
		machine = new Machine();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBantKenarFrezelemeAltlik() {
		return this.bantKenarFrezelemeAltlik;
	}

	public void setBantKenarFrezelemeAltlik(Integer bantKenarFrezelemeAltlik) {
		this.bantKenarFrezelemeAltlik = bantKenarFrezelemeAltlik;
	}

	public Integer getBantKenarFrezelemeElmas() {
		return this.bantKenarFrezelemeElmas;
	}

	public void setBantKenarFrezelemeElmas(Integer bantKenarFrezelemeElmas) {
		this.bantKenarFrezelemeElmas = bantKenarFrezelemeElmas;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getElectrode() {
		return this.electrode;
	}

	public void setElectrode(Integer electrode) {
		this.electrode = electrode;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getKaynakYuksekligiFrezelemeAltlik() {
		return this.kaynakYuksekligiFrezelemeAltlik;
	}

	public void setKaynakYuksekligiFrezelemeAltlik(
			Integer kaynakYuksekligiFrezelemeAltlik) {
		this.kaynakYuksekligiFrezelemeAltlik = kaynakYuksekligiFrezelemeAltlik;
	}

	public Integer getKaynakYuksekligiFrezelemeElmas() {
		return this.kaynakYuksekligiFrezelemeElmas;
	}

	public void setKaynakYuksekligiFrezelemeElmas(
			Integer kaynakYuksekligiFrezelemeElmas) {
		this.kaynakYuksekligiFrezelemeElmas = kaynakYuksekligiFrezelemeElmas;
	}

	public Integer getMainBody() {
		return this.mainBody;
	}

	public void setMainBody(Integer mainBody) {
		this.mainBody = mainBody;
	}

	public Integer getNozzle() {
		return this.nozzle;
	}

	public void setNozzle(Integer nozzle) {
		this.nozzle = nozzle;
	}

	public Integer getPlazmaSuyu() {
		return this.plazmaSuyu;
	}

	public void setPlazmaSuyu(Integer plazmaSuyu) {
		this.plazmaSuyu = plazmaSuyu;
	}

	public Integer getShield() {
		return this.shield;
	}

	public void setShield(Integer shield) {
		this.shield = shield;
	}

	public Integer getShieldCap() {
		return this.shieldCap;
	}

	public void setShieldCap(Integer shieldCap) {
		this.shieldCap = shieldCap;
	}

	public Integer getSwirlingRing() {
		return this.swirlingRing;
	}

	public void setSwirlingRing(Integer swirlingRing) {
		this.swirlingRing = swirlingRing;
	}

	public Integer getVardiya() {
		return this.vardiya;
	}

	public void setVardiya(Integer vardiya) {
		this.vardiya = vardiya;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the machine
	 */
	public Machine getMachine() {
		return machine;
	}

	/**
	 * @param machine
	 *            the machine to set
	 */
	public void setMachine(Machine machine) {
		this.machine = machine;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the plazmaMemesi
	 */
	public Integer getPlazmaMemesi() {
		return plazmaMemesi;
	}

	/**
	 * @param plazmaMemesi
	 *            the plazmaMemesi to set
	 */
	public void setPlazmaMemesi(Integer plazmaMemesi) {
		this.plazmaMemesi = plazmaMemesi;
	}

	/**
	 * @return the vardiyaTarihi
	 */
	public Date getVardiyaTarihi() {
		return vardiyaTarihi;
	}

	/**
	 * @param vardiyaTarihi
	 *            the vardiyaTarihi to set
	 */
	public void setVardiyaTarihi(Date vardiyaTarihi) {
		this.vardiyaTarihi = vardiyaTarihi;
	}

}