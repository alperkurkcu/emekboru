package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kalibrasyon.KalibrasyonFloroskopik;

/**
 * The persistent class for the test_kaplama_online_holiday_sonuc database
 * table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaOnlineHolidaySonuc.findAll", query = "SELECT r FROM TestKaplamaOnlineHolidaySonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_kaplama_online_holiday_sonuc")
public class TestKaplamaOnlineHolidaySonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KAPLAMA_ONLINE_HOLIDAY_SONUC_ID_GENERATOR", sequenceName = "TEST_KAPLAMA_ONLINE_HOLIDAY_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KAPLAMA_ONLINE_HOLIDAY_SONUC_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(length = 255, name = "aciklama")
	private String aciklama;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name = "global_id")
	// private Integer globalId;

	// @Column(name = "isolation_test_id")
	// private Integer isolationTestId;

	@Column(name = "kaplama_kalinligi")
	private String kaplamaKalinligi;

	@Column(name = "kaplama_voltaji")
	private String kaplamaVoltaji;

	// @Column(name="pipe_id", nullable=false)
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	private Integer sonuc;

	@Column(name = "tamir_adedi")
	private String tamirAdedi;

	@Column(name = "tamir_adedi_alani")
	private String tamirAdediAlani;

	@Column(name = "tamir_sonrasi_holiday")
	private Integer tamirSonrasiHoliday;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi", nullable = false)
	private Date testTarihi;

	@Column(name = "vardiya")
	private Integer vardiya;

	// @Column(name = "kalibrasyon_id")
	// private Integer kalibrasyonId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "kalibrasyon_id", referencedColumnName = "ID", insertable = false)
	private KalibrasyonFloroskopik kalibrasyon;

	public TestKaplamaOnlineHolidaySonuc() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Integer getSonuc() {
		return this.sonuc;
	}

	public void setSonuc(Integer sonuc) {
		this.sonuc = sonuc;
	}

	public Integer getTamirSonrasiHoliday() {
		return this.tamirSonrasiHoliday;
	}

	public void setTamirSonrasiHoliday(Integer tamirSonrasiHoliday) {
		this.tamirSonrasiHoliday = tamirSonrasiHoliday;
	}

	public Integer getVardiya() {
		return this.vardiya;
	}

	public void setVardiya(Integer vardiya) {
		this.vardiya = vardiya;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the bagliGlobalId
	 */
	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}

	/**
	 * @param bagliGlobalId
	 *            the bagliGlobalId to set
	 */
	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	/**
	 * @return the bagliTestId
	 */
	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the testTarihi
	 */
	public Date getTestTarihi() {
		return testTarihi;
	}

	/**
	 * @param testTarihi
	 *            the testTarihi to set
	 */
	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the kalibrasyon
	 */
	public KalibrasyonFloroskopik getKalibrasyon() {
		return kalibrasyon;
	}

	/**
	 * @param kalibrasyon
	 *            the kalibrasyon to set
	 */
	public void setKalibrasyon(KalibrasyonFloroskopik kalibrasyon) {
		this.kalibrasyon = kalibrasyon;
	}

	/**
	 * @return the kaplamaKalinligi
	 */
	public String getKaplamaKalinligi() {
		return kaplamaKalinligi;
	}

	/**
	 * @param kaplamaKalinligi
	 *            the kaplamaKalinligi to set
	 */
	public void setKaplamaKalinligi(String kaplamaKalinligi) {
		this.kaplamaKalinligi = kaplamaKalinligi;
	}

	/**
	 * @return the kaplamaVoltaji
	 */
	public String getKaplamaVoltaji() {
		return kaplamaVoltaji;
	}

	/**
	 * @param kaplamaVoltaji
	 *            the kaplamaVoltaji to set
	 */
	public void setKaplamaVoltaji(String kaplamaVoltaji) {
		this.kaplamaVoltaji = kaplamaVoltaji;
	}

	/**
	 * @return the tamirAdedi
	 */
	public String getTamirAdedi() {
		return tamirAdedi;
	}

	/**
	 * @param tamirAdedi
	 *            the tamirAdedi to set
	 */
	public void setTamirAdedi(String tamirAdedi) {
		this.tamirAdedi = tamirAdedi;
	}

	/**
	 * @return the tamirAdediAlani
	 */
	public String getTamirAdediAlani() {
		return tamirAdediAlani;
	}

	/**
	 * @param tamirAdediAlani
	 *            the tamirAdediAlani to set
	 */
	public void setTamirAdediAlani(String tamirAdediAlani) {
		this.tamirAdediAlani = tamirAdediAlani;
	}

}