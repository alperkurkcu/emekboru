package com.emekboru.jpa.sales.customer;

import java.io.Serializable;

import javax.persistence.*;

import com.emekboru.jpa.sales.customer.SalesContactPeople;

@Entity
@Table(name = "sales_join_crm_meeting_contact_people")
public class SalesJoinCRMMeetingContactPeople implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2417072982121072299L;

	@Id
	@Column(name = "join_id")
	@SequenceGenerator(name = "SALES_JOIN_CRM_MEETING_CONTACT_PEOPLE_GENERATOR", sequenceName = "SALES_JOIN_CRM_MEETING_CONTACT_PEOPLE_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_JOIN_CRM_MEETING_CONTACT_PEOPLE_GENERATOR")
	private int joinId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "contact_people_id", referencedColumnName = "sales_contact_person_id", updatable = false)
	private SalesContactPeople contactPeople;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "meeting_id", referencedColumnName = "sales_crm_meeting_id", updatable = false)
	private SalesCRMMeeting meeting;

	public SalesJoinCRMMeetingContactPeople() {
		contactPeople = new SalesContactPeople();
		meeting = new SalesCRMMeeting();
	}

	/**
	 * GETTERS AND SETTERS
	 */

	public int getJoinId() {
		return joinId;
	}

	public SalesCRMMeeting getMeeting() {
		return meeting;
	}

	public SalesContactPeople getContactPeople() {
		return contactPeople;
	}

	public void setJoinId(Integer joinId) {
		this.joinId = joinId;
	}

	public void setMeeting(SalesCRMMeeting meeting) {
		this.meeting = meeting;
	}

	public void setContactPeople(SalesContactPeople contactPeople) {
		this.contactPeople = contactPeople;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
