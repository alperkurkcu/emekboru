package com.emekboru.jpa.satinalma;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the satinalma_siparis_mektubu_icerik database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "SatinalmaSiparisMektubuIcerik.findAll", query = "SELECT r FROM SatinalmaSiparisMektubuIcerik r order by r.id"),
		@NamedQuery(name = "SatinalmaSiparisMektubuIcerik.findByKararId", query = "SELECT r FROM SatinalmaSiparisMektubuIcerik r where r.satinalmaSatinalmaKarari.id=:prmDosyaId order by r.id"),
		@NamedQuery(name = "SatinalmaSiparisMektubuIcerik.findByKararIdMusteriId", query = "SELECT r FROM SatinalmaSiparisMektubuIcerik r where r.satinalmaSatinalmaKarari.id=:prmKararId and r.satinalmaSatinalmaKarariIcerik.satinalmaTeklifIsteme.satinalmaMusteri.id=:prmMusteriId order by r.id") })
@Table(name = "satinalma_siparis_mektubu_icerik")
public class SatinalmaSiparisMektubuIcerik implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SATINALMA_SIPARIS_MEKTUBU_ICERIK_ID_GENERATOR", sequenceName = "SATINALMA_SIPARIS_MEKTUBU_ICERIK_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SATINALMA_SIPARIS_MEKTUBU_ICERIK_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	// @Column(name = "satinalma_karari_icerik_id", nullable = false)
	// private Integer satinalmaKarariIcerikId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "satinalma_karari_icerik_id", referencedColumnName = "ID", insertable = false)
	private SatinalmaSatinalmaKarariIcerik satinalmaSatinalmaKarariIcerik;

	// @Column(name = "satinalma_karari_id", nullable = false)
	// private Integer satinalmaKarariId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "satinalma_karari_id", referencedColumnName = "ID", insertable = false)
	private SatinalmaSatinalmaKarari satinalmaSatinalmaKarari;

	// @Column(name = "siparis_mektubu_id", nullable = false)
	// private Integer siparisMektubuId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "siparis_mektubu_id", referencedColumnName = "ID", insertable = false)
	private SatinalmaSiparisMektubu satinalmaSiparisMektubu;

	@Column(name = "siparis_tutari")
	private double siparisTutari;

	@Temporal(TemporalType.DATE)
	@Column(name = "tahmini_sevk_tarihi", nullable = false)
	private Date tahminiSevkTarihi;

	@Temporal(TemporalType.DATE)
	@Column(name = "tahmini_odeme_tarihi", nullable = false)
	private Date tahminiOdemeTarihi;

	public SatinalmaSiparisMektubuIcerik() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	/**
	 * @return the satinalmaSatinalmaKarariIcerik
	 */
	public SatinalmaSatinalmaKarariIcerik getSatinalmaSatinalmaKarariIcerik() {
		return satinalmaSatinalmaKarariIcerik;
	}

	/**
	 * @param satinalmaSatinalmaKarariIcerik
	 *            the satinalmaSatinalmaKarariIcerik to set
	 */
	public void setSatinalmaSatinalmaKarariIcerik(
			SatinalmaSatinalmaKarariIcerik satinalmaSatinalmaKarariIcerik) {
		this.satinalmaSatinalmaKarariIcerik = satinalmaSatinalmaKarariIcerik;
	}

	/**
	 * @return the satinalmaSatinalmaKarari
	 */
	public SatinalmaSatinalmaKarari getSatinalmaSatinalmaKarari() {
		return satinalmaSatinalmaKarari;
	}

	/**
	 * @param satinalmaSatinalmaKarari
	 *            the satinalmaSatinalmaKarari to set
	 */
	public void setSatinalmaSatinalmaKarari(
			SatinalmaSatinalmaKarari satinalmaSatinalmaKarari) {
		this.satinalmaSatinalmaKarari = satinalmaSatinalmaKarari;
	}

	/**
	 * @return the satinalmaSiparisMektubu
	 */
	public SatinalmaSiparisMektubu getSatinalmaSiparisMektubu() {
		return satinalmaSiparisMektubu;
	}

	/**
	 * @param satinalmaSiparisMektubu
	 *            the satinalmaSiparisMektubu to set
	 */
	public void setSatinalmaSiparisMektubu(
			SatinalmaSiparisMektubu satinalmaSiparisMektubu) {
		this.satinalmaSiparisMektubu = satinalmaSiparisMektubu;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the tahminiSevkTarihi
	 */
	public Date getTahminiSevkTarihi() {
		return tahminiSevkTarihi;
	}

	/**
	 * @param tahminiSevkTarihi
	 *            the tahminiSevkTarihi to set
	 */
	public void setTahminiSevkTarihi(Date tahminiSevkTarihi) {
		this.tahminiSevkTarihi = tahminiSevkTarihi;
	}

	/**
	 * @return the tahminiOdemeTarihi
	 */
	public Date getTahminiOdemeTarihi() {
		return tahminiOdemeTarihi;
	}

	/**
	 * @param tahminiOdemeTarihi
	 *            the tahminiOdemeTarihi to set
	 */
	public void setTahminiOdemeTarihi(Date tahminiOdemeTarihi) {
		this.tahminiOdemeTarihi = tahminiOdemeTarihi;
	}

	/**
	 * @return the siparisTutari
	 */
	public double getSiparisTutari() {
		return siparisTutari;
	}

	/**
	 * @param siparisTutari the siparisTutari to set
	 */
	public void setSiparisTutari(double siparisTutari) {
		this.siparisTutari = siparisTutari;
	}

}