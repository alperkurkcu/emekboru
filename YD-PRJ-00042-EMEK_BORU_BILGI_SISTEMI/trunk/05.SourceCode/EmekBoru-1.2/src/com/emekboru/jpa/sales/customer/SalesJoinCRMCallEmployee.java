package com.emekboru.jpa.sales.customer;

import java.io.Serializable;

import javax.persistence.*;

import com.emekboru.jpa.Employee;

@Entity
@Table(name = "sales_join_crm_call_employee")
public class SalesJoinCRMCallEmployee implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7730751844706192881L;

	@Id
	@Column(name = "join_id")
	@SequenceGenerator(name = "SALES_JOIN_CRM_CALL_EMPLOYEE_GENERATOR", sequenceName = "SALES_JOIN_CRM_CALL_EMPLOYEE_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_JOIN_CRM_CALL_EMPLOYEE_GENERATOR")
	private int joinId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "employee_id", referencedColumnName = "employee_id", updatable = false)
	private Employee employee;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "call_id", referencedColumnName = "sales_crm_call_id", updatable = false)
	private SalesCRMCall call;

	public SalesJoinCRMCallEmployee() {
		employee = new Employee();
		call = new SalesCRMCall();
	}

	/**
	 * GETTERS AND SETTERS
	 */

	public int getJoinId() {
		return joinId;
	}

	public SalesCRMCall getCall() {
		return call;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setJoinId(Integer joinId) {
		this.joinId = joinId;
	}

	public void setCall(SalesCRMCall call) {
		this.call = call;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
