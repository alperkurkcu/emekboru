package com.emekboru.jpa.kaplamagirditest;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the test_hammadde_girdi_pdd1 database table.
 * 
 */
@Entity
@Table(name = "test_hammadde_girdi_pdd1")
public class TestHammaddeGirdiPdd1 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_HAMMADDE_GIRDI_PDD1_ID_GENERATOR", sequenceName = "TEST_HAMMADDE_GIRDI_PDD1_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_HAMMADDE_GIRDI_PDD1_ID_GENERATOR")
	@Column(name = "ID")
	private Integer id;

	// @Column(name = "coat_material_id")
	// private Integer coatMaterialId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coat_material_id", referencedColumnName = "coat_material_id", insertable = false)
	private CoatRawMaterial coatRawMaterial;

	@Column(name = "daldirma_sivisi")
	private String daldirmaSivisi;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "kullanilan_method")
	private String kullanilanMethod;

	@Column(name = "ortalama_yogunluk")
	private String ortalamaYogunluk;

	@Column(name = "parti_no")
	private String partiNo;

	private Boolean sonuc;

	private String standard;

	@Column(name = "test_sicakligi")
	private String testSicakligi;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi", nullable = false)
	private Date testTarihi;

	@Column(name = "uretici_spec")
	private String ureticiSpec;

	@Column(name = "yogunluk_test_degeri_1")
	private String yogunlukTestDegeri1;

	@Column(name = "yogunluk_test_degeri_2")
	private String yogunlukTestDegeri2;

	@Column(name = "yogunluk_test_degeri_3")
	private String yogunlukTestDegeri3;

	@Column(name = "degerlendirme_standard")
	private String degerlendirmeStandard;
	
	@Column(name = "rapor_no")
	private String raporNo;

	public TestHammaddeGirdiPdd1() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDaldirmaSivisi() {
		return this.daldirmaSivisi;
	}

	public void setDaldirmaSivisi(String daldirmaSivisi) {
		this.daldirmaSivisi = daldirmaSivisi;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getKullanilanMethod() {
		return this.kullanilanMethod;
	}

	public void setKullanilanMethod(String kullanilanMethod) {
		this.kullanilanMethod = kullanilanMethod;
	}

	public Boolean getSonuc() {
		return this.sonuc;
	}

	public void setSonuc(Boolean sonuc) {
		this.sonuc = sonuc;
	}

	public String getStandard() {
		return this.standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public void setTestTarihi(Timestamp testTarihi) {
		this.testTarihi = testTarihi;
	}

	public String getUreticiSpec() {
		return this.ureticiSpec;
	}

	public void setUreticiSpec(String ureticiSpec) {
		this.ureticiSpec = ureticiSpec;
	}

	/**
	 * @return the coatRawMaterial
	 */
	public CoatRawMaterial getCoatRawMaterial() {
		return coatRawMaterial;
	}

	/**
	 * @param coatRawMaterial
	 *            the coatRawMaterial to set
	 */
	public void setCoatRawMaterial(CoatRawMaterial coatRawMaterial) {
		this.coatRawMaterial = coatRawMaterial;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the ortalamaYogunluk
	 */
	public String getOrtalamaYogunluk() {
		return ortalamaYogunluk;
	}

	/**
	 * @param ortalamaYogunluk
	 *            the ortalamaYogunluk to set
	 */
	public void setOrtalamaYogunluk(String ortalamaYogunluk) {
		this.ortalamaYogunluk = ortalamaYogunluk;
	}

	/**
	 * @return the testSicakligi
	 */
	public String getTestSicakligi() {
		return testSicakligi;
	}

	/**
	 * @param testSicakligi
	 *            the testSicakligi to set
	 */
	public void setTestSicakligi(String testSicakligi) {
		this.testSicakligi = testSicakligi;
	}

	/**
	 * @return the testTarihi
	 */
	public Date getTestTarihi() {
		return testTarihi;
	}

	/**
	 * @param testTarihi
	 *            the testTarihi to set
	 */
	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	/**
	 * @return the yogunlukTestDegeri1
	 */
	public String getYogunlukTestDegeri1() {
		return yogunlukTestDegeri1;
	}

	/**
	 * @param yogunlukTestDegeri1
	 *            the yogunlukTestDegeri1 to set
	 */
	public void setYogunlukTestDegeri1(String yogunlukTestDegeri1) {
		this.yogunlukTestDegeri1 = yogunlukTestDegeri1;
	}

	/**
	 * @return the yogunlukTestDegeri2
	 */
	public String getYogunlukTestDegeri2() {
		return yogunlukTestDegeri2;
	}

	/**
	 * @param yogunlukTestDegeri2
	 *            the yogunlukTestDegeri2 to set
	 */
	public void setYogunlukTestDegeri2(String yogunlukTestDegeri2) {
		this.yogunlukTestDegeri2 = yogunlukTestDegeri2;
	}

	/**
	 * @return the yogunlukTestDegeri3
	 */
	public String getYogunlukTestDegeri3() {
		return yogunlukTestDegeri3;
	}

	/**
	 * @param yogunlukTestDegeri3
	 *            the yogunlukTestDegeri3 to set
	 */
	public void setYogunlukTestDegeri3(String yogunlukTestDegeri3) {
		this.yogunlukTestDegeri3 = yogunlukTestDegeri3;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the partiNo
	 */
	public String getPartiNo() {
		return partiNo;
	}

	/**
	 * @param partiNo
	 *            the partiNo to set
	 */
	public void setPartiNo(String partiNo) {
		this.partiNo = partiNo;
	}

	/**
	 * @return the degerlendirmeStandard
	 */
	public String getDegerlendirmeStandard() {
		return degerlendirmeStandard;
	}

	/**
	 * @param degerlendirmeStandard
	 *            the degerlendirmeStandard to set
	 */
	public void setDegerlendirmeStandard(String degerlendirmeStandard) {
		this.degerlendirmeStandard = degerlendirmeStandard;
	}

	/**
	 * @return the raporNo
	 */
	public String getRaporNo() {
		return raporNo;
	}

	/**
	 * @param raporNo the raporNo to set
	 */
	public void setRaporNo(String raporNo) {
		this.raporNo = raporNo;
	}
}