package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.config.Test;
import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the centik_darbe_tests_spec database table.
 * 
 */
@NamedQueries({
		@NamedQuery(name = "CDTS.kontrol", query = "SELECT r.testId FROM CentikDarbeTestsSpec r WHERE r.globalId = :prmGlobalId and r.salesItem.itemId=:prmItemId"),
		@NamedQuery(name = "CDTS.seciliCentikTestTanim", query = "SELECT r FROM CentikDarbeTestsSpec r WHERE r.globalId = :prmGlobalId and r.salesItem.itemId=:prmItemId"),
		@NamedQuery(name = "CDTS.findByItemId", query = "SELECT r FROM CentikDarbeTestsSpec r WHERE r.salesItem.itemId=:prmItemId") })
@Entity
@Table(name = "centik_darbe_tests_spec")
public class CentikDarbeTestsSpec implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "CENTIK_DARBE_TESTS_SPEC_GENERATOR", sequenceName = "CENTIK_DARBE_TESTS_SPEC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CENTIK_DARBE_TESTS_SPEC_GENERATOR")
	@Column(name = "test_id")
	private Integer testId;

	@Column(name = "completed")
	private Boolean completed;

	@Column(name = "explanation")
	private String explanation;

	@Column(name = "global_id")
	private Integer globalId;

	@Column(name = "name")
	private String name;

	@Column(name = "numune_boyutu")
	private String numuneBoyutu;

	@Column(name = "numune_enerji_min")
	private Float numuneEnerjiMin;

	@Column(name = "numune_kesme_min")
	private Float numuneKesmeMin;

	@Column(name = "ortalama_enerji_min")
	private Float ortalamaEnerjiMin;

	@Column(name = "ortalama_kesme_min")
	private Float ortalamaKesmeMin;

	@Column(name = "sicaklik")
	private String sicaklik;

	@Column(name = "code")
	private String code;

	@Column(name = "type")
	private String type;

	// @JoinColumn(name = "order_id", referencedColumnName = "order_id",
	// insertable = false)
	// @ManyToOne(fetch = FetchType.LAZY)
	// private Order order;
	//
	// @ManyToOne
	// @JoinColumns({
	// @JoinColumn(name = "order_id", referencedColumnName = "order_id",
	// insertable = false, updatable = false),
	// @JoinColumn(name = "global_id", referencedColumnName = "global_id",
	// insertable = false, updatable = false) })
	// private DestructiveTestsSpec mainDestructiveTestSpecs;

	// entegrasyon
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	private SalesItem salesItem;

	@ManyToOne
	@JoinColumns({
			@JoinColumn(name = "sales_item_id", referencedColumnName = "sales_item_id", insertable = false, updatable = false),
			@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false, updatable = false) })
	private DestructiveTestsSpec salesDestructiveTestSpecs;

	// entegrasyon

	public CentikDarbeTestsSpec() {
	}

	public CentikDarbeTestsSpec(Test t) {

		globalId = t.getGlobalId();
		name = t.getDisplayName();
		code = t.getCode();
		testId = t.getTestId();
	}

	// public void reset() {
	//
	// testId = 0;
	// numuneBoyutu = (float) 0.0;
	// numuneEnerjiMin = (float) 0.0;
	// numuneKesmeMin = (float) 0.0;
	// ortalamaEnerjiMin = (float) 0.0;
	// ortalamaKesmeMin = (float) 0.0;
	// sicaklik = (float) 0.0;
	// type = "";
	// explanation = "";
	// }

	// GETTERS AND SETTERS
	public Integer getTestId() {
		return this.testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	public Boolean getCompleted() {
		return this.completed;
	}

	public void setCompleted(Boolean completed) {
		this.completed = completed;
	}

	public String getExplanation() {
		return this.explanation;
	}

	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}

	public Integer getGlobalId() {
		return this.globalId;
	}

	public void setGlobalId(Integer globalId) {
		this.globalId = globalId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public DestructiveTestsSpec getSalesDestructiveTestSpecs() {
		return salesDestructiveTestSpecs;
	}

	public void setSalesDestructiveTestSpecs(
			DestructiveTestsSpec salesDestructiveTestSpecs) {
		this.salesDestructiveTestSpecs = salesDestructiveTestSpecs;
	}

	public Float getNumuneEnerjiMin() {
		return numuneEnerjiMin;
	}

	public void setNumuneEnerjiMin(Float numuneEnerjiMin) {
		this.numuneEnerjiMin = numuneEnerjiMin;
	}

	public Float getNumuneKesmeMin() {
		return numuneKesmeMin;
	}

	public void setNumuneKesmeMin(Float numuneKesmeMin) {
		this.numuneKesmeMin = numuneKesmeMin;
	}

	public Float getOrtalamaEnerjiMin() {
		return ortalamaEnerjiMin;
	}

	public void setOrtalamaEnerjiMin(Float ortalamaEnerjiMin) {
		this.ortalamaEnerjiMin = ortalamaEnerjiMin;
	}

	public Float getOrtalamaKesmeMin() {
		return ortalamaKesmeMin;
	}

	public void setOrtalamaKesmeMin(Float ortalamaKesmeMin) {
		this.ortalamaKesmeMin = ortalamaKesmeMin;
	}

	/**
	 * @return the numuneBoyutu
	 */
	public String getNumuneBoyutu() {
		return numuneBoyutu;
	}

	/**
	 * @param numuneBoyutu
	 *            the numuneBoyutu to set
	 */
	public void setNumuneBoyutu(String numuneBoyutu) {
		this.numuneBoyutu = numuneBoyutu;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the sicaklik
	 */
	public String getSicaklik() {
		return sicaklik;
	}

	/**
	 * @param sicaklik
	 *            the sicaklik to set
	 */
	public void setSicaklik(String sicaklik) {
		this.sicaklik = sicaklik;
	}

}