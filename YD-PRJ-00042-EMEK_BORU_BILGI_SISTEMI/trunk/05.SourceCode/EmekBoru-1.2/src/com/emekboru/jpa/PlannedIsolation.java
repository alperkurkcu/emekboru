package com.emekboru.jpa;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the planned_isolation database table.
 * 
 */
@Entity
@Table(name = "planned_isolation")
public class PlannedIsolation implements Serializable {
	public static final int INTERNAL = 0;
	public static final int EXTERNAL = 1;
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "isolation_id")
	@SequenceGenerator(name = "ISOLATION_GENERATOR", sequenceName = "PLANNED_ISOLATION_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ISOLATION_GENERATOR")
	private int isolationId;

	@Column(name = "standard")
	private String standard;

	@Column(name = "note")
	private String note;

	@Column(name = "internal_external")
	private Integer internalExternal;

	@Column(name = "izolasyon_sekli")
	private String izolasyonSekli;

	@Column(name = "kalite")
	private String kalite;

	@Column(name = "renk")
	private String renk;

	@Column(name = "kumlama_sartlari")
	private String kumlamaSartlari;

	@Column(name = "yuzey_profili")
	private String yuzeyProfili;

	@Column(name = "yuzey_temizligi")
	private String yuzeyTemizligi;

	@Column(name = "ortam_sartlari")
	private String ortamSartlari;

	@Column(name = "a_uncoated_area")
	private double aUncoatedArea;

	@Column(name = "b_uncoated_area")
	private double bUncoatedArea;

	@Column(name = "durum")
	private Integer durum;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "order_id", referencedColumnName = "order_id", insertable = false)
	private Order order;

	// entegrasyon
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	private SalesItem salesItem;
	// entegrasyon

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "isolation", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Layer> layers;

	public PlannedIsolation() {

	}

	public Integer getDurum() {
		return durum;
	}

	public void setDurum(Integer durum) {
		this.durum = durum;
	}

	public int getIsolationId() {
		return isolationId;
	}

	public void setIsolationId(int isolationId) {
		this.isolationId = isolationId;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Integer getInternalExternal() {
		return internalExternal;
	}

	public void setInternalExternal(Integer internalExternal) {
		this.internalExternal = internalExternal;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public List<Layer> getLayers() {
		return layers;
	}

	public void setLayers(List<Layer> layers) {
		this.layers = layers;
	}

	public String getIzolasyonSekli() {
		return izolasyonSekli;
	}

	public void setIzolasyonSekli(String izolasyonSekli) {
		this.izolasyonSekli = izolasyonSekli;
	}

	public String getKalite() {
		return kalite;
	}

	public void setKalite(String kalite) {
		this.kalite = kalite;
	}

	public String getRenk() {
		return renk;
	}

	public void setRenk(String renk) {
		this.renk = renk;
	}

	public String getKumlamaSartlari() {
		return kumlamaSartlari;
	}

	public void setKumlamaSartlari(String kumlamaSartlari) {
		this.kumlamaSartlari = kumlamaSartlari;
	}

	public String getYuzeyProfili() {
		return yuzeyProfili;
	}

	public void setYuzeyProfili(String yuzeyProfili) {
		this.yuzeyProfili = yuzeyProfili;
	}

	public String getYuzeyTemizligi() {
		return yuzeyTemizligi;
	}

	public void setYuzeyTemizligi(String yuzeyTemizligi) {
		this.yuzeyTemizligi = yuzeyTemizligi;
	}

	public String getOrtamSartlari() {
		return ortamSartlari;
	}

	public void setOrtamSartlari(String ortamSartlari) {
		this.ortamSartlari = ortamSartlari;
	}

	public double getaUncoatedArea() {
		return aUncoatedArea;
	}

	public void setaUncoatedArea(double aUncoatedArea) {
		this.aUncoatedArea = aUncoatedArea;
	}

	public double getbUncoatedArea() {
		return bUncoatedArea;
	}

	public void setbUncoatedArea(double bUncoatedArea) {
		this.bUncoatedArea = bUncoatedArea;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

}