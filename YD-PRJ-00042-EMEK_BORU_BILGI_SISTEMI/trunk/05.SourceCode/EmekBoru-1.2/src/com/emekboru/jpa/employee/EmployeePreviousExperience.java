package com.emekboru.jpa.employee;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Employee;

/**
 * The persistent class for the employee_previous_experience database table.
 * 
 */
@Entity
@Table(name = "employee_previous_experience")
@NamedQuery(name = "EmployeePreviousExperience.findAll", query = "SELECT e FROM EmployeePreviousExperience e")
public class EmployeePreviousExperience implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "EMPLOYEE_PREVIOUS_EXPERIENCE_ID_GENERATOR", sequenceName = "EMPLOYEE_PREVIOUS_EXPERIENCE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMPLOYEE_PREVIOUS_EXPERIENCE_ID_GENERATOR")
	@Column(name = "id")
	private Integer id;

	@Column(name = "company_name")
	private String companyName;

	@Column(name = "ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@ManyToOne
	@JoinColumn(name = "employee_id", insertable = false)
	private Employee employee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "position")
	private String position;

	@Column(name = "reason_of_leaving")
	private String reasonOfLeaving;

	@Temporal(TemporalType.DATE)
	@Column(name = "start_date")
	private Date startDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "end_date")
	private Date endDate;

	public EmployeePreviousExperience() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getPosition() {
		return this.position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getReasonOfLeaving() {
		return this.reasonOfLeaving;
	}

	public void setReasonOfLeaving(String reasonOfLeaving) {
		this.reasonOfLeaving = reasonOfLeaving;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

}