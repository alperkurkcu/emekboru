package com.emekboru.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the pipe_coat_layer database table.
 * 
 */
@Entity
@Table(name="pipe_coat_layer")
public class PipeCoatLayer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="layer_id")
	@SequenceGenerator(name="PIPE_COAT_LAYER_GENERATOR", sequenceName="PIPE_COAT_LAYER_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PIPE_COAT_LAYER_GENERATOR")	
	private Integer layerId;

	@Column(name="amount_consumed")
	private double amountConsumed;

	@Column(name="coat_material_id")
	private Integer coatMaterialId;

    @Temporal( TemporalType.TIMESTAMP)
	private Date date;

	@Column(name="pipe_coat_id")
	private Integer pipeCoatId;

	@Column(name="layer_number")
	private Integer layerNumber;

    public PipeCoatLayer() {
    }

	public Integer getLayerId() {
		return this.layerId;
	}

	public void setLayerId(Integer layerId) {
		this.layerId = layerId;
	}

	public double getAmountConsumed() {
		return this.amountConsumed;
	}

	public void setAmountConsumed(double amountConsumed) {
		this.amountConsumed = amountConsumed;
	}

	public Integer getCoatMaterialId() {
		return this.coatMaterialId;
	}

	public void setCoatMaterialId(Integer coatMaterialId) {
		this.coatMaterialId = coatMaterialId;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getPipeCoatId() {
		return this.pipeCoatId;
	}

	public void setPipeCoatId(Integer pipeCoatId) {
		this.pipeCoatId = pipeCoatId;
	}

	public Integer getLayerNumber() {
		return layerNumber;
	}

	public void setLayerNumber(Integer layerNumber) {
		this.layerNumber = layerNumber;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}