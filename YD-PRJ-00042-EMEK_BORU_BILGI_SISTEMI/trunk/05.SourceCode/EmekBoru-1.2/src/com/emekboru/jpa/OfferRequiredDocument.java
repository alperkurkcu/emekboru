package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "offer_reduqired_documents")
public class OfferRequiredDocument implements Serializable {

	private static final long serialVersionUID = 6889483695022423107L;

	@Id
	@Column(name = "offer_required_document_id")
	private Integer offerRequiredDocumentId;

	@Column(name = "included")
	private Boolean included;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "document_type_id", referencedColumnName = "document_type_id")
	private DocumentType documentType;

	public OfferRequiredDocument() {

	}

	/**
	 * GETTERS AND SETTERS
	 */
	public Integer getOfferRequiredDocumentId() {
		return offerRequiredDocumentId;
	}

	public void setOfferRequiredDocumentId(Integer offerRequiredDocumentId) {
		this.offerRequiredDocumentId = offerRequiredDocumentId;
	}

	public Boolean getIncluded() {
		return included;
	}

	public void setIncluded(Boolean included) {
		this.included = included;
	}

	public DocumentType getDocumentType() {
		return documentType;
	}

	public void setDocumentType(DocumentType documentType) {
		this.documentType = documentType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
