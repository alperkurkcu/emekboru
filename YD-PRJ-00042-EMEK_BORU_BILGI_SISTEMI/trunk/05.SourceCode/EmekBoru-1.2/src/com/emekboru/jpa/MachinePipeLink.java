package com.emekboru.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQueries({
		@NamedQuery(name = "MachinePipeLink.findYesterdaysProduction", query = "select o from MachinePipeLink o where o.pipe.eklemeZamani BETWEEN :prmDate1 AND :prmDate2 and o.machine.machineId=:prmMachineId order by o.pipe.pipeId DESC"),
		@NamedQuery(name = "MachinePipeLink.findByItemId", query = "select o from MachinePipeLink o where o.pipe.salesItem.itemId=:prmItemId and o.machine.machineId=:prmMachineId order by o.pipe.pipeId DESC"),
		@NamedQuery(name = "MachinePipeLink.findByPipeId", query = "select o from MachinePipeLink o where o.pipe.pipeId=:prmPipeId order by o.pipe.pipeId DESC") })
@Table(name = "machine_pipe_link")
public class MachinePipeLink implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@SequenceGenerator(name = "MACHINE_PIPE_LINK_GENERATOR", sequenceName = "MACHINE_PIPE_LINK_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MACHINE_PIPE_LINK_GENERATOR")
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date")
	private Date date;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_date")
	private Date endDate;

	@Column(name = "amount_waste")
	private Double amountWaste;

	@Column(name = "purpose")
	private String purpose;

	@Column(name = "ended")
	private boolean ended;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "machine_id", referencedColumnName = "machine_id", insertable = false)
	private Machine machine;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	public MachinePipeLink() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Double getAmountWaste() {
		return amountWaste;
	}

	public void setAmountWaste(Double amountWaste) {
		this.amountWaste = amountWaste;
	}

	public Machine getMachine() {
		return machine;
	}

	public void setMachine(Machine machine) {
		this.machine = machine;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public boolean getEnded() {
		return ended;
	}

	public void setEnded(boolean ended) {
		this.ended = ended;
	}
}