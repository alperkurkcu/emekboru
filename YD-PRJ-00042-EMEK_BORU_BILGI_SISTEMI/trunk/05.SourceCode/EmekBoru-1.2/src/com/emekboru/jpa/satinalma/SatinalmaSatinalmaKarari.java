package com.emekboru.jpa.satinalma;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the satinalma_satinalma_karari database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "SatinalmaSatinalmaKarari.findAll", query = "SELECT r FROM SatinalmaSatinalmaKarari r order by r.id"),
		@NamedQuery(name = "SatinalmaSatinalmaKarari.findAllByFormId", query = "SELECT r FROM SatinalmaSatinalmaKarari r where r.satinalmaMalzemeHizmetTalepFormu.id=:prmFormId order by r.id"),
		@NamedQuery(name = "SatinalmaSatinalmaKarari.findAllByUserId", query = "SELECT r FROM SatinalmaSatinalmaKarari r where r.ekleyenKullanici=:prmUserId order by r.eklemeZamani DESC") })
@Table(name = "satinalma_satinalma_karari")
public class SatinalmaSatinalmaKarari implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SATINALMA_SATINALMA_KARARI_ID_GENERATOR", sequenceName = "SATINALMA_SATINALMA_KARARI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SATINALMA_SATINALMA_KARARI_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(length = 255, name = "aciklama")
	private String aciklama;

	@Column(name = "dosya_no", insertable = false, updatable = false)
	private Integer dosyaNo;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "dosya_no", referencedColumnName = "ID", insertable = false)
	private SatinalmaTakipDosyalar satinalmaTakipDosyalar;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser user;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "sa_mudur_onay")
	private Boolean saMudurOnay;

	@Column(name = "sa_uzman_onay")
	private Boolean saUzmanOnay;

	// @Column(name = "sonuc_firma", updatable = false, insertable = false)
	// private Integer sonucFirma;
	//
	// @ManyToOne(fetch = FetchType.LAZY)
	// @JoinColumn(name = "sonuc_firma", referencedColumnName = "ID", insertable
	// = false)
	// private SatinalmaMusteri satinalmaMusteriSonuc;

	@Column(name = "sonuc_firma")
	private String sonucFirma;

	@Column(name = "talep_mudur_onay")
	private Boolean talepMudurOnay;

	@Column(name = "form_id", insertable = false, updatable = false)
	private Integer formId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "form_id", referencedColumnName = "ID", insertable = false)
	private SatinalmaMalzemeHizmetTalepFormu satinalmaMalzemeHizmetTalepFormu;

	@Column(name = "documents")
	private String documents;

	@Transient
	private List<String> fileNames;

	@Transient
	private int fileNumber;

	public SatinalmaSatinalmaKarari() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Boolean getSaMudurOnay() {
		return this.saMudurOnay;
	}

	public void setSaMudurOnay(Boolean saMudurOnay) {
		this.saMudurOnay = saMudurOnay;
	}

	public Boolean getSaUzmanOnay() {
		return this.saUzmanOnay;
	}

	public void setSaUzmanOnay(Boolean saUzmanOnay) {
		this.saUzmanOnay = saUzmanOnay;
	}

	public Boolean getTalepMudurOnay() {
		return this.talepMudurOnay;
	}

	public void setTalepMudurOnay(Boolean talepMudurOnay) {
		this.talepMudurOnay = talepMudurOnay;
	}

	public SystemUser getUser() {
		return user;
	}

	public void setUser(SystemUser user) {
		this.user = user;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public SatinalmaMalzemeHizmetTalepFormu getSatinalmaMalzemeHizmetTalepFormu() {
		return satinalmaMalzemeHizmetTalepFormu;
	}

	public void setSatinalmaMalzemeHizmetTalepFormu(
			SatinalmaMalzemeHizmetTalepFormu satinalmaMalzemeHizmetTalepFormu) {
		this.satinalmaMalzemeHizmetTalepFormu = satinalmaMalzemeHizmetTalepFormu;
	}

	/**
	 * @return the satinalmaTakipDosyalar
	 */
	public SatinalmaTakipDosyalar getSatinalmaTakipDosyalar() {
		return satinalmaTakipDosyalar;
	}

	/**
	 * @param satinalmaTakipDosyalar
	 *            the satinalmaTakipDosyalar to set
	 */
	public void setSatinalmaTakipDosyalar(
			SatinalmaTakipDosyalar satinalmaTakipDosyalar) {
		this.satinalmaTakipDosyalar = satinalmaTakipDosyalar;
	}

	/**
	 * @return the sonucFirma
	 */
	public String getSonucFirma() {
		return sonucFirma;
	}

	/**
	 * @param sonucFirma
	 *            the sonucFirma to set
	 */
	public void setSonucFirma(String sonucFirma) {
		this.sonucFirma = sonucFirma;
	}

	public List<String> getFileNames() {
		try {
			String[] names = documents.split("//");
			fileNames = new ArrayList<String>();

			for (int i = 1; i < names.length; i++) {
				fileNames.add(names[i]);
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return fileNames;
	}

	public void setFileNames(List<String> fileNames) {
		this.fileNames = fileNames;
	}

	public int getFileNumber() {
		try {
			String[] names = documents.split("//");

			fileNumber = names.length - 1;
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return fileNumber;
	}

	/**
	 * @return the dosyaNo
	 */
	public Integer getDosyaNo() {
		return dosyaNo;
	}

	/**
	 * @param dosyaNo
	 *            the dosyaNo to set
	 */
	public void setDosyaNo(Integer dosyaNo) {
		this.dosyaNo = dosyaNo;
	}

	/**
	 * @return the documents
	 */
	public String getDocuments() {
		return documents;
	}

	/**
	 * @param documents
	 *            the documents to set
	 */
	public void setDocuments(String documents) {
		this.documents = documents;
	}

}