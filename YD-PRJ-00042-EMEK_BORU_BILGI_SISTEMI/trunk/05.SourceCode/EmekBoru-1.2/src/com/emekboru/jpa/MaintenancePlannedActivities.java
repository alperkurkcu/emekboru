package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "maintenance_planned_activities")
public class MaintenancePlannedActivities implements Serializable {

	private static final long serialVersionUID = -3162355481731171253L;

	@EmbeddedId
	private MaintenancePlannedActivitiesPK id;

	@Column(name = "optional")
	private Boolean optional;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "maintenance_plan_id", referencedColumnName = "maintenance_plan_id", insertable = false, updatable = false)
	private MaintenancePlan maintenancePlan;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "maintenance_activity_id", referencedColumnName = "maintenance_activity_id", insertable = false, updatable = false)
	private MaintenanceActivity maintenanceActivity;

	public MaintenancePlannedActivities() {

	}

	/**
	 * GETTERS AND SETTERS
	 */
	public Boolean getOptional() {
		return optional;
	}

	public void setOptional(Boolean optional) {
		this.optional = optional;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public MaintenancePlannedActivitiesPK getId() {
		return id;
	}

	public void setId(MaintenancePlannedActivitiesPK id) {
		this.id = id;
	}

	public MaintenancePlan getMaintenancePlan() {
		return maintenancePlan;
	}

	public void setMaintenancePlan(MaintenancePlan maintenancePlan) {
		this.maintenancePlan = maintenancePlan;
	}

	public MaintenanceActivity getMaintenanceActivity() {
		return maintenanceActivity;
	}

	public void setMaintenanceActivity(MaintenanceActivity maintenanceActivity) {
		this.maintenanceActivity = maintenanceActivity;
	}
}
