package com.emekboru.jpa;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.rulo.Rulo;

@Entity
@NamedQueries({
		@NamedQuery(name = "MachineRuloLink.findAll", query = "select o from MachineRuloLink o"),
		@NamedQuery(name = "MachineRuloLink.findByMachineIdAndEnded", query = "select o from MachineRuloLink o where o.machine.machineId=:prmMachineId and o.ended=:prmEnded"),
		@NamedQuery(name = "MachineRuloLink.findByMachineIdEndedRuloId", query = "select o from MachineRuloLink o where o.machine.machineId=:prmMachineId and o.ended=:prmEnded and o.rulo.ruloId=:prmRuloId") })
@Table(name = "machine_rulo_link")
public class MachineRuloLink implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@SequenceGenerator(name = "MACHINE_RULO_LINK_GENERATOR", sequenceName = "MACHINE_RULO_LINK_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MACHINE_RULO_LINK_GENERATOR")
	private int id;

	@Column(name = "amount_consumed")
	private Float amountConsumed;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_date")
	private Date endDate;

	private boolean ended;

	private Integer purpose;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_date")
	private Date startDate;

	@Column(name = "amount_scrap")
	private Float amountScrap;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "machine_id", referencedColumnName = "machine_id", insertable = false)
	private Machine machine;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(name = "rulo_id", referencedColumnName = "rulo_id", insertable = false),
			@JoinColumn(name = "rulo_yil", referencedColumnName = "rulo_yil", insertable = false) })
	private Rulo rulo;

	@OneToMany(mappedBy = "machineRuloLink", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<PipeMachineRuloLink> pmrLinks;

	@Override
	public boolean equals(Object obj) {

		if (!(obj instanceof MachineRuloLink))
			return false;
		if (this.id == ((MachineRuloLink) obj).id)
			return true;
		return false;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getPurpose() {
		return this.purpose;
	}

	public void setPurpose(Integer purpose) {
		this.purpose = purpose;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Machine getMachine() {
		return machine;
	}

	public void setMachine(Machine machine) {
		this.machine = machine;
	}

	public List<PipeMachineRuloLink> getPmrLinks() {
		return pmrLinks;
	}

	public void setPmcLinks(List<PipeMachineRuloLink> pmrLinks) {
		this.pmrLinks = pmrLinks;
	}

	public Rulo getRulo() {
		return rulo;
	}

	public void setRulo(Rulo rulo) {
		this.rulo = rulo;
	}

	public boolean isEnded() {
		return ended;
	}

	public void setEnded(boolean ended) {
		this.ended = ended;
	}

	public Float getAmountConsumed() {
		return amountConsumed;
	}

	public void setAmountConsumed(Float amountConsumed) {
		this.amountConsumed = amountConsumed;
	}

	public Float getAmountScrap() {
		return amountScrap;
	}

	public void setAmountScrap(Float amountScrap) {
		this.amountScrap = amountScrap;
	}

}
