package com.emekboru.jpa.employee;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.emekboru.jpa.Employee;

/**
 * The persistent class for the employee_advance_request database table.
 * 
 */
@Entity
@Table(name = "employee_advance_request")
@NamedQuery(name = "EmployeeAdvanceRequest.findAll", query = "SELECT e FROM EmployeeAdvanceRequest e")
public class EmployeeAdvanceRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "EMPLOYEE_ADVANCE_REQUEST_GENERATOR", sequenceName = "EMPLOYEE_ADVANCE_REQUEST_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMPLOYEE_ADVANCE_REQUEST_GENERATOR")
	@Column(name = "id")
	private Integer id;

	@Column(name = "advance_amount")
	private Integer advanceAmount;

	@Column(name = "ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "employee_id", insertable = false, updatable = true)
	private Employee employee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "installment_count")
	private Integer installmentCount;

	@Column(name = "reason")
	private String reason;

	@Temporal(TemporalType.DATE)
	@Column(name = "request_date")
	private Date requestDate;

	@Column(name = "hr_approval")
	private Integer hrApproval;

	@Column(name = "supervisor_approval")
	private Integer supervisorApproval;

//	@ManyToOne(fetch = FetchType.EAGER)
//	@JoinColumn(name = "supervisor_id", insertable = false, updatable = true)
//	private Employee supervisor;

	@Transient
	public static final int PENDING = 0;
	@Transient
	public static final int APPROVED = 1;
	@Transient
	public static final int DECLINED = 2;

	public EmployeeAdvanceRequest() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAdvanceAmount() {
		return advanceAmount;
	}

	public void setAdvanceAmount(Integer advanceAmount) {
		this.advanceAmount = advanceAmount;
	}

	public Timestamp getEklemeZamani() {
		return eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Timestamp getGuncellemeZamani() {
		return guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Integer getInstallmentCount() {
		return installmentCount;
	}

	public void setInstallmentCount(Integer installmentCount) {
		this.installmentCount = installmentCount;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Integer getHrApproval() {
		return hrApproval;
	}

	public void setHrApproval(Integer hrApproval) {
		this.hrApproval = hrApproval;
	}

	public Integer getSupervisorApproval() {
		return supervisorApproval;
	}

	public void setSupervisorApproval(Integer supervisorApproval) {
		this.supervisorApproval = supervisorApproval;
	}

//	public Employee getSupervisor() {
//		return supervisor;
//	}
//
//	public void setSupervisor(Employee supervisor) {
//		this.supervisor = supervisor;
//	}

}