package com.emekboru.jpa.satinalma;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.emekboru.jpa.Employee;
import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the satinalma_takip_dosyalar_icerik database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "SatinalmaTakipDosyalarIcerik.findAll", query = "SELECT r FROM SatinalmaTakipDosyalarIcerik r order by r.eklemeZamani DESC"),
		@NamedQuery(name = "SatinalmaTakipDosyalarIcerik.findAllByFormId", query = "SELECT r FROM SatinalmaTakipDosyalarIcerik r where r.satinalmaMalzemeHizmetTalepFormu.id=:prmFormId order by r.eklemeZamani ASC"),
		@NamedQuery(name = "SatinalmaTakipDosyalarIcerik.findAllByUserId", query = "SELECT r FROM SatinalmaTakipDosyalarIcerik r where r.gorevliPersonel.employeeId=:prmUserId order by r.eklemeZamani DESC"),
		@NamedQuery(name = "SatinalmaTakipDosyalarIcerik.findByFormIdUserId", query = "SELECT r FROM SatinalmaTakipDosyalarIcerik r where r.gorevliPersonel.employeeId=:prmUserId and r.satinalmaMalzemeHizmetTalepFormu.id=:prmFormId order by r.eklemeZamani DESC"),
		@NamedQuery(name = "SatinalmaTakipDosyalarIcerik.findAllByDosyaIdFormId", query = "SELECT r FROM SatinalmaTakipDosyalarIcerik r where r.satinalmaTakipDosyalar.id=:prmDosyaId and r.satinalmaMalzemeHizmetTalepFormu.id=:prmFormId order by r.eklemeZamani ASC"),
		@NamedQuery(name = "SatinalmaTakipDosyalarIcerik.findAllByDosyaId", query = "SELECT r FROM SatinalmaTakipDosyalarIcerik r where r.satinalmaTakipDosyalar.id=:prmDosyaId order by r.eklemeZamani ASC") })
@Table(name = "satinalma_takip_dosyalar_icerik")
public class SatinalmaTakipDosyalarIcerik implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SATINALMA_TAKIP_DOSYALAR_ICERIK_ID_GENERATOR", sequenceName = "SATINALMA_TAKIP_DOSYALAR_ICERIK_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SATINALMA_TAKIP_DOSYALAR_ICERIK_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Long id;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser user;

	@Column(name = "gorevli_id", insertable = false, updatable = false)
	private Integer gorevliId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "gorevli_id", referencedColumnName = "employee_id", insertable = false)
	private Employee gorevliPersonel;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "takip_dosya_id", insertable = false, updatable = false)
	private Integer takipDosyaId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "takip_dosya_id", referencedColumnName = "ID", insertable = false)
	private SatinalmaTakipDosyalar satinalmaTakipDosyalar;

	@Column(name = "talep_icerik_id", insertable = false, updatable = false)
	private Integer talepIcerikId;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "talep_icerik_id", referencedColumnName = "ID", insertable = false)
	private SatinalmaMalzemeHizmetTalepFormuIcerik satinalmaMalzemeHizmetTalepFormuIcerik;

	@Column(name = "malzeme_talep_id", insertable = false, updatable = false)
	private Integer malzemeTalepId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "malzeme_talep_id", referencedColumnName = "ID", insertable = false)
	private SatinalmaMalzemeHizmetTalepFormu satinalmaMalzemeHizmetTalepFormu;

	@Transient
	public static final int PENDING = 0;
	@Transient
	public static final int APPROVED = 1;
	@Transient
	public static final int DECLINED = 2;

	public SatinalmaTakipDosyalarIcerik() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	/**
	 * @return the user
	 */
	public SystemUser getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(SystemUser user) {
		this.user = user;
	}

	/**
	 * @return the gorevliPersonel
	 */
	public Employee getGorevliPersonel() {
		return gorevliPersonel;
	}

	/**
	 * @param gorevliPersonel
	 *            the gorevliPersonel to set
	 */
	public void setGorevliPersonel(Employee gorevliPersonel) {
		this.gorevliPersonel = gorevliPersonel;
	}

	/**
	 * @return the satinalmaTakipDosyalar
	 */
	public SatinalmaTakipDosyalar getSatinalmaTakipDosyalar() {
		return satinalmaTakipDosyalar;
	}

	/**
	 * @param satinalmaTakipDosyalar
	 *            the satinalmaTakipDosyalar to set
	 */
	public void setSatinalmaTakipDosyalar(
			SatinalmaTakipDosyalar satinalmaTakipDosyalar) {
		this.satinalmaTakipDosyalar = satinalmaTakipDosyalar;
	}

	/**
	 * @return the satinalmaMalzemeHizmetTalepFormuIcerik
	 */
	public SatinalmaMalzemeHizmetTalepFormuIcerik getSatinalmaMalzemeHizmetTalepFormuIcerik() {
		return satinalmaMalzemeHizmetTalepFormuIcerik;
	}

	/**
	 * @param satinalmaMalzemeHizmetTalepFormuIcerik
	 *            the satinalmaMalzemeHizmetTalepFormuIcerik to set
	 */
	public void setSatinalmaMalzemeHizmetTalepFormuIcerik(
			SatinalmaMalzemeHizmetTalepFormuIcerik satinalmaMalzemeHizmetTalepFormuIcerik) {
		this.satinalmaMalzemeHizmetTalepFormuIcerik = satinalmaMalzemeHizmetTalepFormuIcerik;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the satinalmaMalzemeHizmetTalepFormu
	 */
	public SatinalmaMalzemeHizmetTalepFormu getSatinalmaMalzemeHizmetTalepFormu() {
		return satinalmaMalzemeHizmetTalepFormu;
	}

	/**
	 * @param satinalmaMalzemeHizmetTalepFormu
	 *            the satinalmaMalzemeHizmetTalepFormu to set
	 */
	public void setSatinalmaMalzemeHizmetTalepFormu(
			SatinalmaMalzemeHizmetTalepFormu satinalmaMalzemeHizmetTalepFormu) {
		this.satinalmaMalzemeHizmetTalepFormu = satinalmaMalzemeHizmetTalepFormu;
	}

	/**
	 * @return the takipDosyaId
	 */
	public Integer getTakipDosyaId() {
		return takipDosyaId;
	}

	/**
	 * @return the talepIcerikId
	 */
	public Integer getTalepIcerikId() {
		return talepIcerikId;
	}

	/**
	 * @return the malzemeTalepId
	 */
	public Integer getMalzemeTalepId() {
		return malzemeTalepId;
	}

}