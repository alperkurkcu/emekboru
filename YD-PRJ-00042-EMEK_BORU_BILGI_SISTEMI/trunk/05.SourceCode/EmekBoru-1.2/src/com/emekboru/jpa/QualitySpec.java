package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the quality_spec database table.
 * 
 */
@NamedQueries({ @NamedQuery(name = "QS.seciliQualitySpec", query = "SELECT r FROM QualitySpec r WHERE r.salesItem.itemId=:prmItemId") })
@Entity
@Table(name = "quality_spec")
public class QualitySpec implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "QUALITY_SPEC_GENERATOR", sequenceName = "QUALITY_SPEC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "QUALITY_SPEC_GENERATOR")
	@Column(name = "quality_spec_id")
	private Integer qualitySpecId;

	@Column(name = "a_end_agirlik_max")
	private float aEndAgirlikMax;

	@Column(name = "a_end_agirlik_min")
	private float aEndAgirlikMin;

	@Column(name = "a_end_boruucudis_max")
	private float aEndBoruucudisMax;

	@Column(name = "a_end_boruucudis_min")
	private float aEndBoruucudisMin;

	@Column(name = "a_end_boruucudis_nom")
	private float aEndBoruucudisNom;

	@Column(name = "a_end_boruucuic_max")
	private float aEndBoruucuicMax;

	@Column(name = "a_end_boruucuic_min")
	private float aEndBoruucuicMin;

	@Column(name = "a_end_boruucuic_nom")
	private float aEndBoruucuicNom;

	@Column(name = "a_end_boy_max")
	private float aEndBoyMax;

	@Column(name = "a_end_boy_min")
	private float aEndBoyMin;

	@Column(name = "a_end_boy_nom")
	private float aEndBoyNom;

	@Column(name = "a_end_cap_max")
	private float aEndCapMax;

	@Column(name = "a_end_cap_min")
	private float aEndCapMin;

	@Column(name = "a_end_cap_nom")
	private float aEndCapNom;

	@Column(name = "a_end_dent_max")
	private float aEndDentMax;

	@Column(name = "a_end_dikligi_max")
	private float aEndDikligiMax;

	@Column(name = "a_end_dikligi_min")
	private float aEndDikligiMin;

	@Column(name = "a_end_dikligi_nom")
	private float aEndDikligiNom;

	@Column(name = "a_end_diskaynak_max")
	private float aEndDiskaynakMax;

	@Column(name = "a_end_diskaynak_min")
	private float aEndDiskaynakMin;

	@Column(name = "a_end_dogrusallik_max")
	private String aEndDogrusallikMax;

	@Column(name = "a_end_ickaynak_max")
	private float aEndIckaynakMax;

	@Column(name = "a_end_ickaynak_min")
	private float aEndIckaynakMin;

	@Column(name = "a_end_kaynak_max")
	private float aEndKaynakMax;

	@Column(name = "a_end_kaynak_min")
	private float aEndKaynakMin;

	@Column(name = "a_end_kaynak_nom")
	private float aEndKaynakNom;

	@Column(name = "a_end_kokyuzey_max")
	private float aEndKokyuzeyMax;

	@Column(name = "a_end_kokyuzey_min")
	private float aEndKokyuzeyMin;

	@Column(name = "a_end_kokyuzey_nom")
	private float aEndKokyuzeyNom;

	@Column(name = "a_end_manyetiklik_max")
	private float aEndManyetiklikMax;

	@Column(name = "a_end_manyetiklik_nom")
	private float aEndManyetiklikNom;

	@Column(name = "a_end_munferit_tamir_max")
	private float aEndMunferitTamirMax;

	@Column(name = "a_end_munferit_tamir_min")
	private float aEndMunferitTamirMin;

	@Column(name = "a_end_ovallik_max")
	private float aEndOvallikMax;

	@Column(name = "a_end_ovallik_min")
	private float aEndOvallikMin;

	@Column(name = "a_end_ovallik_nom")
	private float aEndOvallikNom;

	@Column(name = "a_end_radial_max")
	private float aEndRadialMax;

	@Column(name = "a_end_toplam_tamir_max")
	private float aEndToplamTamirMax;

	@Column(name = "agirlik_siklik")
	private float agirlikSiklik;

	@Column(name = "b_end_boruucudis_max")
	private float bEndBoruucudisMax;

	@Column(name = "b_end_boruucudis_min")
	private float bEndBoruucudisMin;

	@Column(name = "b_end_boruucudis_nom")
	private float bEndBoruucudisNom;

	@Column(name = "b_end_boruucuic_max")
	private float bEndBoruucuicMax;

	@Column(name = "b_end_boruucuic_min")
	private float bEndBoruucuicMin;

	@Column(name = "b_end_boruucuic_nom")
	private float bEndBoruucuicNom;

	@Column(name = "b_end_cap_max")
	private float bEndCapMax;

	@Column(name = "b_end_cap_min")
	private float bEndCapMin;

	@Column(name = "b_end_cap_nom")
	private float bEndCapNom;

	@Column(name = "b_end_dikligi_max")
	private float bEndDikligiMax;

	@Column(name = "b_end_dikligi_min")
	private float bEndDikligiMin;

	@Column(name = "b_end_dikligi_nom")
	private float bEndDikligiNom;

	@Column(name = "b_end_kaynak_max")
	private float bEndKaynakMax;

	@Column(name = "b_end_kaynak_min")
	private float bEndKaynakMin;

	@Column(name = "b_end_kaynak_nom")
	private float bEndKaynakNom;

	@Column(name = "b_end_kokyuzey_max")
	private float bEndKokyuzeyMax;

	@Column(name = "b_end_kokyuzey_min")
	private float bEndKokyuzeyMin;

	@Column(name = "b_end_kokyuzey_nom")
	private float bEndKokyuzeyNom;

	@Column(name = "b_end_ovallik_max")
	private float bEndOvallikMax;

	@Column(name = "b_end_ovallik_min")
	private float bEndOvallikMin;

	@Column(name = "b_end_ovallik_nom")
	private float bEndOvallikNom;

	@Column(name = "baglanti_sekli")
	private String baglantiSekli;

	@Column(name = "boru_boy_siklik")
	private float boruBoySiklik;

	@Column(name = "boru_ucu_dis_siklik")
	private float boruUcuDisSiklik;

	@Column(name = "boru_ucu_ic_siklik")
	private float boruUcuIcSiklik;

	@Column(name = "boru_ucu_siklik")
	private float boruUcuSiklik;

	@Column(name = "dent_siklik")
	private float dentSiklik;

	@Column(name = "dis_cap_siklik")
	private float disCapSiklik;

	@Column(name = "dis_kaynak_siklik")
	private float disKaynakSiklik;

	@Column(name = "dogrusalliktan_siklik")
	private float dogrusalliktanSiklik;

	@Column(name = "dokuman_kayit_adi")
	private String dokumanKayitAdi;

	@Column(name = "dokuman_orijinal_adi")
	private String dokumanOrijinalAdi;

	@Column(name = "et_kalinligi_siklik")
	private float etKalinligiSiklik;

	@Column(name = "govde_cap_max")
	private float govdeCapMax;

	@Column(name = "govde_cap_min")
	private float govdeCapMin;

	@Column(name = "govde_cap_nom")
	private float govdeCapNom;

	@Column(name = "govde_ovallik_max")
	private float govdeOvallikMax;

	@Column(name = "govde_ovallik_min")
	private float govdeOvallikMin;

	@Column(name = "govde_ovallik_nom")
	private float govdeOvallikNom;

	@Column(name = "ic_kaynak_siklik")
	private float icKaynakSiklik;

	@Column(name = "kalici_siklik")
	private float kaliciSiklik;

	@Column(name = "kalite_plani")
	private String kalitePlani;

	@Column(name = "kaynak_azgi_koruma")
	private String kaynakAzgiKoruma;

	@Column(name = "kaynak_azgi_siklik")
	private float kaynakAzgiSiklik;

	@Column(name = "kok_yuzey_siklik")
	private float kokYuzeySiklik;

	private String markalama;

	@Column(name = "munferit_tamir_siklik")
	private float munferitTamirSiklik;

	@Column(name = "ovallik_siklik")
	private float ovallikSiklik;

	@Column(name = "radial_set_siklik")
	private float radialSetSiklik;

	@Column(name = "toplam_tamir_siklik")
	private float toplamTamirSiklik;

	// @ManyToOne(fetch = FetchType.LAZY)
	// @JoinColumn(name = "order_id", referencedColumnName = "order_id",
	// insertable = false)
	// private Order order;

	@Column(name = "sales_item_id", insertable = false, updatable = false)
	private Integer salesItemId;

	// entegrasyon
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	private SalesItem salesItem;

	// entegrasyon

	public QualitySpec() {
		qualitySpecId = 0;
	}

	public Integer getQualitySpecId() {
		return qualitySpecId;
	}

	public void setQualitySpecId(Integer qualitySpecId) {
		this.qualitySpecId = qualitySpecId;
	}

	public float getaEndAgirlikMax() {
		return aEndAgirlikMax;
	}

	public void setaEndAgirlikMax(float aEndAgirlikMax) {
		this.aEndAgirlikMax = aEndAgirlikMax;
	}

	public float getaEndAgirlikMin() {
		return aEndAgirlikMin;
	}

	public void setaEndAgirlikMin(float aEndAgirlikMin) {
		this.aEndAgirlikMin = aEndAgirlikMin;
	}

	public float getaEndBoruucudisMax() {
		return aEndBoruucudisMax;
	}

	public void setaEndBoruucudisMax(float aEndBoruucudisMax) {
		this.aEndBoruucudisMax = aEndBoruucudisMax;
	}

	public float getaEndBoruucudisMin() {
		return aEndBoruucudisMin;
	}

	public void setaEndBoruucudisMin(float aEndBoruucudisMin) {
		this.aEndBoruucudisMin = aEndBoruucudisMin;
	}

	public float getaEndBoruucudisNom() {
		return aEndBoruucudisNom;
	}

	public void setaEndBoruucudisNom(float aEndBoruucudisNom) {
		this.aEndBoruucudisNom = aEndBoruucudisNom;
	}

	public float getaEndBoruucuicMax() {
		return aEndBoruucuicMax;
	}

	public void setaEndBoruucuicMax(float aEndBoruucuicMax) {
		this.aEndBoruucuicMax = aEndBoruucuicMax;
	}

	public float getaEndBoruucuicMin() {
		return aEndBoruucuicMin;
	}

	public void setaEndBoruucuicMin(float aEndBoruucuicMin) {
		this.aEndBoruucuicMin = aEndBoruucuicMin;
	}

	public float getaEndBoruucuicNom() {
		return aEndBoruucuicNom;
	}

	public void setaEndBoruucuicNom(float aEndBoruucuicNom) {
		this.aEndBoruucuicNom = aEndBoruucuicNom;
	}

	public float getaEndBoyMax() {
		return aEndBoyMax;
	}

	public void setaEndBoyMax(float aEndBoyMax) {
		this.aEndBoyMax = aEndBoyMax;
	}

	public float getaEndBoyMin() {
		return aEndBoyMin;
	}

	public void setaEndBoyMin(float aEndBoyMin) {
		this.aEndBoyMin = aEndBoyMin;
	}

	public float getaEndBoyNom() {
		return aEndBoyNom;
	}

	public void setaEndBoyNom(float aEndBoyNom) {
		this.aEndBoyNom = aEndBoyNom;
	}

	public float getaEndCapMax() {
		return aEndCapMax;
	}

	public void setaEndCapMax(float aEndCapMax) {
		this.aEndCapMax = aEndCapMax;
	}

	public float getaEndCapMin() {
		return aEndCapMin;
	}

	public void setaEndCapMin(float aEndCapMin) {
		this.aEndCapMin = aEndCapMin;
	}

	public float getaEndCapNom() {
		return aEndCapNom;
	}

	public void setaEndCapNom(float aEndCapNom) {
		this.aEndCapNom = aEndCapNom;
	}

	public float getaEndDentMax() {
		return aEndDentMax;
	}

	public void setaEndDentMax(float aEndDentMax) {
		this.aEndDentMax = aEndDentMax;
	}

	public float getaEndDikligiMax() {
		return aEndDikligiMax;
	}

	public void setaEndDikligiMax(float aEndDikligiMax) {
		this.aEndDikligiMax = aEndDikligiMax;
	}

	public float getaEndDikligiMin() {
		return aEndDikligiMin;
	}

	public void setaEndDikligiMin(float aEndDikligiMin) {
		this.aEndDikligiMin = aEndDikligiMin;
	}

	public float getaEndDikligiNom() {
		return aEndDikligiNom;
	}

	public void setaEndDikligiNom(float aEndDikligiNom) {
		this.aEndDikligiNom = aEndDikligiNom;
	}

	public float getaEndDiskaynakMax() {
		return aEndDiskaynakMax;
	}

	public void setaEndDiskaynakMax(float aEndDiskaynakMax) {
		this.aEndDiskaynakMax = aEndDiskaynakMax;
	}

	public float getaEndDiskaynakMin() {
		return aEndDiskaynakMin;
	}

	public void setaEndDiskaynakMin(float aEndDiskaynakMin) {
		this.aEndDiskaynakMin = aEndDiskaynakMin;
	}

	public float getaEndIckaynakMax() {
		return aEndIckaynakMax;
	}

	public void setaEndIckaynakMax(float aEndIckaynakMax) {
		this.aEndIckaynakMax = aEndIckaynakMax;
	}

	public float getaEndIckaynakMin() {
		return aEndIckaynakMin;
	}

	public void setaEndIckaynakMin(float aEndIckaynakMin) {
		this.aEndIckaynakMin = aEndIckaynakMin;
	}

	public float getaEndKaynakMax() {
		return aEndKaynakMax;
	}

	public void setaEndKaynakMax(float aEndKaynakMax) {
		this.aEndKaynakMax = aEndKaynakMax;
	}

	public float getaEndKaynakMin() {
		return aEndKaynakMin;
	}

	public void setaEndKaynakMin(float aEndKaynakMin) {
		this.aEndKaynakMin = aEndKaynakMin;
	}

	public float getaEndKaynakNom() {
		return aEndKaynakNom;
	}

	public void setaEndKaynakNom(float aEndKaynakNom) {
		this.aEndKaynakNom = aEndKaynakNom;
	}

	public float getaEndKokyuzeyMax() {
		return aEndKokyuzeyMax;
	}

	public void setaEndKokyuzeyMax(float aEndKokyuzeyMax) {
		this.aEndKokyuzeyMax = aEndKokyuzeyMax;
	}

	public float getaEndKokyuzeyMin() {
		return aEndKokyuzeyMin;
	}

	public void setaEndKokyuzeyMin(float aEndKokyuzeyMin) {
		this.aEndKokyuzeyMin = aEndKokyuzeyMin;
	}

	public float getaEndKokyuzeyNom() {
		return aEndKokyuzeyNom;
	}

	public void setaEndKokyuzeyNom(float aEndKokyuzeyNom) {
		this.aEndKokyuzeyNom = aEndKokyuzeyNom;
	}

	public float getaEndManyetiklikMax() {
		return aEndManyetiklikMax;
	}

	public void setaEndManyetiklikMax(float aEndManyetiklikMax) {
		this.aEndManyetiklikMax = aEndManyetiklikMax;
	}

	public float getaEndManyetiklikNom() {
		return aEndManyetiklikNom;
	}

	public void setaEndManyetiklikNom(float aEndManyetiklikNom) {
		this.aEndManyetiklikNom = aEndManyetiklikNom;
	}

	public float getaEndMunferitTamirMax() {
		return aEndMunferitTamirMax;
	}

	public void setaEndMunferitTamirMax(float aEndMunferitTamirMax) {
		this.aEndMunferitTamirMax = aEndMunferitTamirMax;
	}

	public float getaEndMunferitTamirMin() {
		return aEndMunferitTamirMin;
	}

	public void setaEndMunferitTamirMin(float aEndMunferitTamirMin) {
		this.aEndMunferitTamirMin = aEndMunferitTamirMin;
	}

	public float getaEndOvallikMax() {
		return aEndOvallikMax;
	}

	public void setaEndOvallikMax(float aEndOvallikMax) {
		this.aEndOvallikMax = aEndOvallikMax;
	}

	public float getaEndOvallikMin() {
		return aEndOvallikMin;
	}

	public void setaEndOvallikMin(float aEndOvallikMin) {
		this.aEndOvallikMin = aEndOvallikMin;
	}

	public float getaEndOvallikNom() {
		return aEndOvallikNom;
	}

	public void setaEndOvallikNom(float aEndOvallikNom) {
		this.aEndOvallikNom = aEndOvallikNom;
	}

	public float getaEndRadialMax() {
		return aEndRadialMax;
	}

	public void setaEndRadialMax(float aEndRadialMax) {
		this.aEndRadialMax = aEndRadialMax;
	}

	public float getaEndToplamTamirMax() {
		return aEndToplamTamirMax;
	}

	public void setaEndToplamTamirMax(float aEndToplamTamirMax) {
		this.aEndToplamTamirMax = aEndToplamTamirMax;
	}

	public float getAgirlikSiklik() {
		return agirlikSiklik;
	}

	public void setAgirlikSiklik(float agirlikSiklik) {
		this.agirlikSiklik = agirlikSiklik;
	}

	public float getbEndBoruucudisMax() {
		return bEndBoruucudisMax;
	}

	public void setbEndBoruucudisMax(float bEndBoruucudisMax) {
		this.bEndBoruucudisMax = bEndBoruucudisMax;
	}

	public float getbEndBoruucudisMin() {
		return bEndBoruucudisMin;
	}

	public void setbEndBoruucudisMin(float bEndBoruucudisMin) {
		this.bEndBoruucudisMin = bEndBoruucudisMin;
	}

	public float getbEndBoruucudisNom() {
		return bEndBoruucudisNom;
	}

	public void setbEndBoruucudisNom(float bEndBoruucudisNom) {
		this.bEndBoruucudisNom = bEndBoruucudisNom;
	}

	public float getbEndBoruucuicMax() {
		return bEndBoruucuicMax;
	}

	public void setbEndBoruucuicMax(float bEndBoruucuicMax) {
		this.bEndBoruucuicMax = bEndBoruucuicMax;
	}

	public float getbEndBoruucuicMin() {
		return bEndBoruucuicMin;
	}

	public void setbEndBoruucuicMin(float bEndBoruucuicMin) {
		this.bEndBoruucuicMin = bEndBoruucuicMin;
	}

	public float getbEndBoruucuicNom() {
		return bEndBoruucuicNom;
	}

	public void setbEndBoruucuicNom(float bEndBoruucuicNom) {
		this.bEndBoruucuicNom = bEndBoruucuicNom;
	}

	public float getbEndCapMax() {
		return bEndCapMax;
	}

	public void setbEndCapMax(float bEndCapMax) {
		this.bEndCapMax = bEndCapMax;
	}

	public float getbEndCapMin() {
		return bEndCapMin;
	}

	public void setbEndCapMin(float bEndCapMin) {
		this.bEndCapMin = bEndCapMin;
	}

	public float getbEndCapNom() {
		return bEndCapNom;
	}

	public void setbEndCapNom(float bEndCapNom) {
		this.bEndCapNom = bEndCapNom;
	}

	public float getbEndDikligiMax() {
		return bEndDikligiMax;
	}

	public void setbEndDikligiMax(float bEndDikligiMax) {
		this.bEndDikligiMax = bEndDikligiMax;
	}

	public float getbEndDikligiMin() {
		return bEndDikligiMin;
	}

	public void setbEndDikligiMin(float bEndDikligiMin) {
		this.bEndDikligiMin = bEndDikligiMin;
	}

	public float getbEndDikligiNom() {
		return bEndDikligiNom;
	}

	public void setbEndDikligiNom(float bEndDikligiNom) {
		this.bEndDikligiNom = bEndDikligiNom;
	}

	public float getbEndKaynakMax() {
		return bEndKaynakMax;
	}

	public void setbEndKaynakMax(float bEndKaynakMax) {
		this.bEndKaynakMax = bEndKaynakMax;
	}

	public float getbEndKaynakMin() {
		return bEndKaynakMin;
	}

	public void setbEndKaynakMin(float bEndKaynakMin) {
		this.bEndKaynakMin = bEndKaynakMin;
	}

	public float getbEndKaynakNom() {
		return bEndKaynakNom;
	}

	public void setbEndKaynakNom(float bEndKaynakNom) {
		this.bEndKaynakNom = bEndKaynakNom;
	}

	public float getbEndKokyuzeyMax() {
		return bEndKokyuzeyMax;
	}

	public void setbEndKokyuzeyMax(float bEndKokyuzeyMax) {
		this.bEndKokyuzeyMax = bEndKokyuzeyMax;
	}

	public float getbEndKokyuzeyMin() {
		return bEndKokyuzeyMin;
	}

	public void setbEndKokyuzeyMin(float bEndKokyuzeyMin) {
		this.bEndKokyuzeyMin = bEndKokyuzeyMin;
	}

	public float getbEndKokyuzeyNom() {
		return bEndKokyuzeyNom;
	}

	public void setbEndKokyuzeyNom(float bEndKokyuzeyNom) {
		this.bEndKokyuzeyNom = bEndKokyuzeyNom;
	}

	public float getbEndOvallikMax() {
		return bEndOvallikMax;
	}

	public void setbEndOvallikMax(float bEndOvallikMax) {
		this.bEndOvallikMax = bEndOvallikMax;
	}

	public float getbEndOvallikMin() {
		return bEndOvallikMin;
	}

	public void setbEndOvallikMin(float bEndOvallikMin) {
		this.bEndOvallikMin = bEndOvallikMin;
	}

	public float getbEndOvallikNom() {
		return bEndOvallikNom;
	}

	public void setbEndOvallikNom(float bEndOvallikNom) {
		this.bEndOvallikNom = bEndOvallikNom;
	}

	public String getBaglantiSekli() {
		return baglantiSekli;
	}

	public void setBaglantiSekli(String baglantiSekli) {
		this.baglantiSekli = baglantiSekli;
	}

	public float getBoruBoySiklik() {
		return boruBoySiklik;
	}

	public void setBoruBoySiklik(float boruBoySiklik) {
		this.boruBoySiklik = boruBoySiklik;
	}

	public float getBoruUcuDisSiklik() {
		return boruUcuDisSiklik;
	}

	public void setBoruUcuDisSiklik(float boruUcuDisSiklik) {
		this.boruUcuDisSiklik = boruUcuDisSiklik;
	}

	public float getBoruUcuIcSiklik() {
		return boruUcuIcSiklik;
	}

	public void setBoruUcuIcSiklik(float boruUcuIcSiklik) {
		this.boruUcuIcSiklik = boruUcuIcSiklik;
	}

	public float getBoruUcuSiklik() {
		return boruUcuSiklik;
	}

	public void setBoruUcuSiklik(float boruUcuSiklik) {
		this.boruUcuSiklik = boruUcuSiklik;
	}

	public float getDentSiklik() {
		return dentSiklik;
	}

	public void setDentSiklik(float dentSiklik) {
		this.dentSiklik = dentSiklik;
	}

	public float getDisCapSiklik() {
		return disCapSiklik;
	}

	public void setDisCapSiklik(float disCapSiklik) {
		this.disCapSiklik = disCapSiklik;
	}

	public float getDisKaynakSiklik() {
		return disKaynakSiklik;
	}

	public void setDisKaynakSiklik(float disKaynakSiklik) {
		this.disKaynakSiklik = disKaynakSiklik;
	}

	public float getDogrusalliktanSiklik() {
		return dogrusalliktanSiklik;
	}

	public void setDogrusalliktanSiklik(float dogrusalliktanSiklik) {
		this.dogrusalliktanSiklik = dogrusalliktanSiklik;
	}

	public String getDokumanKayitAdi() {
		return dokumanKayitAdi;
	}

	public void setDokumanKayitAdi(String dokumanKayitAdi) {
		this.dokumanKayitAdi = dokumanKayitAdi;
	}

	public String getDokumanOrijinalAdi() {
		return dokumanOrijinalAdi;
	}

	public void setDokumanOrijinalAdi(String dokumanOrijinalAdi) {
		this.dokumanOrijinalAdi = dokumanOrijinalAdi;
	}

	public float getEtKalinligiSiklik() {
		return etKalinligiSiklik;
	}

	public void setEtKalinligiSiklik(float etKalinligiSiklik) {
		this.etKalinligiSiklik = etKalinligiSiklik;
	}

	public float getGovdeCapMax() {
		return govdeCapMax;
	}

	public void setGovdeCapMax(float govdeCapMax) {
		this.govdeCapMax = govdeCapMax;
	}

	public float getGovdeCapMin() {
		return govdeCapMin;
	}

	public void setGovdeCapMin(float govdeCapMin) {
		this.govdeCapMin = govdeCapMin;
	}

	public float getGovdeCapNom() {
		return govdeCapNom;
	}

	public void setGovdeCapNom(float govdeCapNom) {
		this.govdeCapNom = govdeCapNom;
	}

	public float getGovdeOvallikMax() {
		return govdeOvallikMax;
	}

	public void setGovdeOvallikMax(float govdeOvallikMax) {
		this.govdeOvallikMax = govdeOvallikMax;
	}

	public float getGovdeOvallikMin() {
		return govdeOvallikMin;
	}

	public void setGovdeOvallikMin(float govdeOvallikMin) {
		this.govdeOvallikMin = govdeOvallikMin;
	}

	public float getGovdeOvallikNom() {
		return govdeOvallikNom;
	}

	public void setGovdeOvallikNom(float govdeOvallikNom) {
		this.govdeOvallikNom = govdeOvallikNom;
	}

	public float getIcKaynakSiklik() {
		return icKaynakSiklik;
	}

	public void setIcKaynakSiklik(float icKaynakSiklik) {
		this.icKaynakSiklik = icKaynakSiklik;
	}

	public float getKaliciSiklik() {
		return kaliciSiklik;
	}

	public void setKaliciSiklik(float kaliciSiklik) {
		this.kaliciSiklik = kaliciSiklik;
	}

	public String getKalitePlani() {
		return kalitePlani;
	}

	public void setKalitePlani(String kalitePlani) {
		this.kalitePlani = kalitePlani;
	}

	public String getKaynakAzgiKoruma() {
		return kaynakAzgiKoruma;
	}

	public void setKaynakAzgiKoruma(String kaynakAzgiKoruma) {
		this.kaynakAzgiKoruma = kaynakAzgiKoruma;
	}

	public float getKaynakAzgiSiklik() {
		return kaynakAzgiSiklik;
	}

	public void setKaynakAzgiSiklik(float kaynakAzgiSiklik) {
		this.kaynakAzgiSiklik = kaynakAzgiSiklik;
	}

	public float getKokYuzeySiklik() {
		return kokYuzeySiklik;
	}

	public void setKokYuzeySiklik(float kokYuzeySiklik) {
		this.kokYuzeySiklik = kokYuzeySiklik;
	}

	public String getMarkalama() {
		return markalama;
	}

	public void setMarkalama(String markalama) {
		this.markalama = markalama;
	}

	public float getMunferitTamirSiklik() {
		return munferitTamirSiklik;
	}

	public void setMunferitTamirSiklik(float munferitTamirSiklik) {
		this.munferitTamirSiklik = munferitTamirSiklik;
	}

	public float getOvallikSiklik() {
		return ovallikSiklik;
	}

	public void setOvallikSiklik(float ovallikSiklik) {
		this.ovallikSiklik = ovallikSiklik;
	}

	public float getRadialSetSiklik() {
		return radialSetSiklik;
	}

	public void setRadialSetSiklik(float radialSetSiklik) {
		this.radialSetSiklik = radialSetSiklik;
	}

	public float getToplamTamirSiklik() {
		return toplamTamirSiklik;
	}

	public void setToplamTamirSiklik(float toplamTamirSiklik) {
		this.toplamTamirSiklik = toplamTamirSiklik;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	/**
	 * @return the aEndDogrusallikMax
	 */
	public String getaEndDogrusallikMax() {
		return aEndDogrusallikMax;
	}

	/**
	 * @param aEndDogrusallikMax
	 *            the aEndDogrusallikMax to set
	 */
	public void setaEndDogrusallikMax(String aEndDogrusallikMax) {
		this.aEndDogrusallikMax = aEndDogrusallikMax;
	}

}