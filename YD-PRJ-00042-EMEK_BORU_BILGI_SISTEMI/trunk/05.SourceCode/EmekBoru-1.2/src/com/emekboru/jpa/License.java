package com.emekboru.jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "license")
public class License implements Serializable{

	private static final long serialVersionUID = 8546946612699820687L;

	
	@Id
	@Column(name = "license_id")
	@SequenceGenerator(name="LICENSE_GENERATOR", sequenceName="LICENSE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="LICENSE_GENERATOR")	
	private Integer licenseId;
	
	@Column(name = "name")
	private String name;

	@Column(name = "type")
	private String type;
	
	@Column(name = "short_desc")
	private String shortDesc;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "issue_date")
	private Date issueDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "expiration_date")
	private Date expirationDate;
	
	@Column(name = "issue_institution")
	private String issueInstitution;
	
	@Column(name = "status")
	private boolean status;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "license", cascade = CascadeType.ALL)
	private List<LicenseRenewResponsible> licenseRenewResponsibles;
	
	public License(){
		licenseRenewResponsibles = new ArrayList<LicenseRenewResponsible>();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof License){
			if(((License) obj).getLicenseId().equals(this.getLicenseId()))
				return true;
		}
		return false;
	}


	/**
	 * 		GETTERS AND SETTERS
	 */
	public Integer getLicenseId() {
		return licenseId;
	}

	public void setLicenseId(Integer licenseId) {
		this.licenseId = licenseId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public String getIssueInstitution() {
		return issueInstitution;
	}

	public void setIssueInstitution(String issueInstitution) {
		this.issueInstitution = issueInstitution;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<LicenseRenewResponsible> getLicenseRenewResponsibles() {
		return licenseRenewResponsibles;
	}

	public void setLicenseRenewResponsibles(
			List<LicenseRenewResponsible> licenseRenewResponsibles) {
		this.licenseRenewResponsibles = licenseRenewResponsibles;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
}
