package com.emekboru.jpa.sales.customer;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.Employee;

@Entity
@Table(name = "sales_join_crm_meeting_employee")
public class SalesJoinCRMMeetingEmployee implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1805662881730923990L;

	@Id
	@Column(name = "join_id")
	@SequenceGenerator(name = "SALES_JOIN_CRM_MEETING_EMPLOYEE_GENERATOR", sequenceName = "SALES_JOIN_CRM_MEETING_EMPLOYEE_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_JOIN_CRM_MEETING_EMPLOYEE_GENERATOR")
	private int joinId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "employee_id", referencedColumnName = "employee_id", updatable = false)
	private Employee employee;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "meeting_id", referencedColumnName = "sales_crm_meeting_id", updatable = false)
	private SalesCRMMeeting meeting;

	public SalesJoinCRMMeetingEmployee() {
		employee = new Employee();
		meeting = new SalesCRMMeeting();
	}

	/**
	 * GETTERS AND SETTERS
	 */

	public int getJoinId() {
		return joinId;
	}

	public SalesCRMMeeting getMeeting() {
		return meeting;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setJoinId(Integer joinId) {
		this.joinId = joinId;
	}

	public void setMeeting(SalesCRMMeeting meeting) {
		this.meeting = meeting;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
