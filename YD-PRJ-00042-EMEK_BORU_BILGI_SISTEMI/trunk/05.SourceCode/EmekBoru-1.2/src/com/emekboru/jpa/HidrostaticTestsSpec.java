package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.config.Test;
import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the hidrostatic_tests_spec database table.
 * 
 */
@NamedQueries({
		@NamedQuery(name = "HidrostaticTestsSpec.kontrol", query = "SELECT r.testId FROM HidrostaticTestsSpec r WHERE r.globalId = :prmGlobalId and r.salesItem.itemId=:prmItemId"),
		@NamedQuery(name = "HidrostaticTestsSpec.findByItemId", query = "SELECT r FROM HidrostaticTestsSpec r WHERE r.salesItem.itemId=:prmItemId") })
@Entity
@Table(name = "hidrostatic_tests_spec")
public class HidrostaticTestsSpec implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final int GLOBAL_ID_1 = 1011;

	@Id
	@SequenceGenerator(name = "HIDROSTATIC_TESTS_SPEC_GENERATOR", sequenceName = "HIDROSTATIC_TESTS_SPEC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HIDROSTATIC_TESTS_SPEC_GENERATOR")
	@Column(name = "test_id")
	private Integer testId;

	@Column(name = "code")
	private String code;

	@Column(name = "completed")
	private Boolean completed;

	@Column(name = "duration")
	private Float duration;

	@Column(name = "frequency")
	private Float frequency;

	@Column(name = "global_id")
	private Integer globalId;

	@Column(name = "name")
	private String name;

	// @Column(name = "order_id")
	// private Integer orderId;

	@Column(name = "pressure")
	private Float pressure;

	@Column(name = "standard")
	private String standard;

	// @JoinColumn(name = "order_id", referencedColumnName = "order_id",
	// insertable = false, updatable = false)
	// @ManyToOne(fetch = FetchType.LAZY)
	// private Order order;

	// entegrasyon
	@Column(name = "sales_item_id")
	private Integer salesItemId;

	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private SalesItem salesItem;

	// entegrasyon

	public HidrostaticTestsSpec() {
	}

	public HidrostaticTestsSpec(Test t) {

		globalId = t.getGlobalId();
		name = t.getDisplayName();
		code = t.getCode();
	}

	public Integer getTestId() {
		return this.testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getCompleted() {
		return this.completed;
	}

	public void setCompleted(Boolean completed) {
		this.completed = completed;
	}

	public Integer getGlobalId() {
		return this.globalId;
	}

	public void setGlobalId(Integer globalId) {
		this.globalId = globalId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStandard() {
		return this.standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public Integer getSalesItemId() {
		return salesItemId;
	}

	public void setSalesItemId(Integer salesItemId) {
		this.salesItemId = salesItemId;
	}

	public Float getDuration() {
		return duration;
	}

	public void setDuration(Float duration) {
		this.duration = duration;
	}

	public Float getFrequency() {
		return frequency;
	}

	public void setFrequency(Float frequency) {
		this.frequency = frequency;
	}

	public Float getPressure() {
		return pressure;
	}

	public void setPressure(Float pressure) {
		this.pressure = pressure;
	}

}