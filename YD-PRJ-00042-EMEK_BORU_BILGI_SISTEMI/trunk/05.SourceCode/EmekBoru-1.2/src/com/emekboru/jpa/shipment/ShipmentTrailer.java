package com.emekboru.jpa.shipment;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the shipment_trailer database table.
 * 
 */
@Entity
@Table(name = "shipment_trailer")
@NamedQuery(name = "ShipmentTrailer.findAll", query = "SELECT s FROM ShipmentTrailer s")
public class ShipmentTrailer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SHIPMENT_TRAILER_ID_GENERATOR", sequenceName = "SHIPMENT_TRAILER_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SHIPMENT_TRAILER_ID_GENERATOR")
	@Column(name = "id")
	private Integer id;

	@Column(name = "ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "plate")
	private String plate;

	// bi-directional many-to-one association to Shipment
	@OneToMany(mappedBy = "shipmentTrailer", orphanRemoval = true, cascade = CascadeType.ALL)
	private List<Shipment> shipments;

	public ShipmentTrailer() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getPlate() {
		return this.plate;
	}

	public void setPlate(String plate) {
		this.plate = plate;
	}

	public List<Shipment> getShipments() {
		return this.shipments;
	}

	public void setShipments(List<Shipment> shipments) {
		this.shipments = shipments;
	}

	public Shipment addShipment(Shipment shipment) {
		getShipments().add(shipment);
		shipment.setShipmentTrailer(this);

		return shipment;
	}

	public Shipment removeShipment(Shipment shipment) {
		getShipments().remove(shipment);
		shipment.setShipmentTrailer(null);

		return shipment;
	}

}