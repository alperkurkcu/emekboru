package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the cement_machine_parameters database table.
 * 
 */
@Entity
@Table(name="cement_machine_parameters")
public class CementMachineParameter implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="cement_parameters_id")
	@SequenceGenerator(name="CEMENT_MACHINE_PARAMETERS_GENERATOR", sequenceName="CEMENT_MACHINE_PARAMETERS_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CEMENT_MACHINE_PARAMETERS_GENERATOR")	
	private Integer cementParametersId;

	@Column(name="araba_hizi")
	private Integer arabaHizi;

	@Column(name="boru_devri")
	private Integer boruDevri;

	@Column(name="cimento")
	private Integer cimento;

	@Column(name="karasim_no")
	private Integer karasimNo;

	private Integer kum;

	@Column(name="machine_id")
	private Integer machineId;

	private Integer mad;

	@Column(name="pompa_konumu")
	private Integer pompaKonumu;

	private Integer su;

    public CementMachineParameter() {
    }

	public Integer getCementParametersId() {
		return this.cementParametersId;
	}

	public void setCementParametersId(Integer cementParametersId) {
		this.cementParametersId = cementParametersId;
	}

	public Integer getArabaHizi() {
		return this.arabaHizi;
	}

	public void setArabaHizi(Integer arabaHizi) {
		this.arabaHizi = arabaHizi;
	}

	public Integer getBoruDevri() {
		return this.boruDevri;
	}

	public void setBoruDevri(Integer boruDevri) {
		this.boruDevri = boruDevri;
	}

	public Integer getCimento() {
		return this.cimento;
	}

	public void setCimento(Integer cimento) {
		this.cimento = cimento;
	}

	public Integer getKarasimNo() {
		return this.karasimNo;
	}

	public void setKarasimNo(Integer karasimNo) {
		this.karasimNo = karasimNo;
	}

	public Integer getKum() {
		return this.kum;
	}

	public void setKum(Integer kum) {
		this.kum = kum;
	}

	public Integer getMachineId() {
		return this.machineId;
	}

	public void setMachineId(Integer machineId) {
		this.machineId = machineId;
	}

	public Integer getMad() {
		return this.mad;
	}

	public void setMad(Integer mad) {
		this.mad = mad;
	}

	public Integer getPompaKonumu() {
		return this.pompaKonumu;
	}

	public void setPompaKonumu(Integer pompaKonumu) {
		this.pompaKonumu = pompaKonumu;
	}

	public Integer getSu() {
		return this.su;
	}

	public void setSu(Integer su) {
		this.su = su;
	}

}