package com.emekboru.jpa.kalibrasyon;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the kalibrasyon_penetrant database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "KalibrasyonPenetrant.findAll", query = "SELECT r FROM KalibrasyonPenetrant r order by r.kalibrasyonZamani DESC") })
@Table(name = "kalibrasyon_penetrant")
public class KalibrasyonPenetrant implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "KALIBRASYON_PENETRANT_ID_GENERATOR", sequenceName = "KALIBRASYON_PENETRANT_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "KALIBRASYON_PENETRANT_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "aciklama")
	private String aciklama;

	@Column(name = "ara_temizleyici")
	private String araTemizleyici;

	@Column(name = "cevre_aydinligi")
	private String cevreAydinligi;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "gelistirici")
	private String gelistirici;

	@Column(name = "gelistirici_suresi")
	private String gelistiriciSuresi;

	@Column(name = "isik_siddeti")
	private String isikSiddeti;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "kalibrasyon_zamani")
	private Date kalibrasyonZamani;

	@Column(name = "kalibrasyonu_yapan")
	private String kalibrasyonuYapan;

	@Column(name = "muayene_sistemi_kontrolu")
	private String muayeneSistemiKontrolu;

	@Column(name = "nufuz_edici")
	private String nufuzEdici;

	@Column(name = "nufuziyet_suresi")
	private String nufuziyetSuresi;

	@Column(name = "on_temizlik")
	private String onTemizlik;

	@Column(name = "son_temizlik")
	private String sonTemizlik;

	@Column(name = "kapsam")
	private String kapsam;

	public KalibrasyonPenetrant() {
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public String getAraTemizleyici() {
		return this.araTemizleyici;
	}

	public void setAraTemizleyici(String araTemizleyici) {
		this.araTemizleyici = araTemizleyici;
	}

	public String getCevreAydinligi() {
		return this.cevreAydinligi;
	}

	public void setCevreAydinligi(String cevreAydinligi) {
		this.cevreAydinligi = cevreAydinligi;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public String getGelistirici() {
		return this.gelistirici;
	}

	public void setGelistirici(String gelistirici) {
		this.gelistirici = gelistirici;
	}

	public String getGelistiriciSuresi() {
		return this.gelistiriciSuresi;
	}

	public void setGelistiriciSuresi(String gelistiriciSuresi) {
		this.gelistiriciSuresi = gelistiriciSuresi;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getIsikSiddeti() {
		return this.isikSiddeti;
	}

	public void setIsikSiddeti(String isikSiddeti) {
		this.isikSiddeti = isikSiddeti;
	}

	public String getKalibrasyonuYapan() {
		return this.kalibrasyonuYapan;
	}

	public void setKalibrasyonuYapan(String kalibrasyonuYapan) {
		this.kalibrasyonuYapan = kalibrasyonuYapan;
	}

	public String getMuayeneSistemiKontrolu() {
		return this.muayeneSistemiKontrolu;
	}

	public void setMuayeneSistemiKontrolu(String muayeneSistemiKontrolu) {
		this.muayeneSistemiKontrolu = muayeneSistemiKontrolu;
	}

	public String getNufuzEdici() {
		return this.nufuzEdici;
	}

	public void setNufuzEdici(String nufuzEdici) {
		this.nufuzEdici = nufuzEdici;
	}

	public String getNufuziyetSuresi() {
		return this.nufuziyetSuresi;
	}

	public void setNufuziyetSuresi(String nufuziyetSuresi) {
		this.nufuziyetSuresi = nufuziyetSuresi;
	}

	public String getOnTemizlik() {
		return this.onTemizlik;
	}

	public void setOnTemizlik(String onTemizlik) {
		this.onTemizlik = onTemizlik;
	}

	public String getSonTemizlik() {
		return this.sonTemizlik;
	}

	public void setSonTemizlik(String sonTemizlik) {
		this.sonTemizlik = sonTemizlik;
	}

	/**
	 * @return the eklemeZamani
	 */
	public Date getEklemeZamani() {
		return eklemeZamani;
	}

	/**
	 * @param eklemeZamani
	 *            the eklemeZamani to set
	 */
	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncellemeZamani
	 */
	public Date getGuncellemeZamani() {
		return guncellemeZamani;
	}

	/**
	 * @param guncellemeZamani
	 *            the guncellemeZamani to set
	 */
	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the kalibrasyonZamani
	 */
	public Date getKalibrasyonZamani() {
		return kalibrasyonZamani;
	}

	/**
	 * @param kalibrasyonZamani
	 *            the kalibrasyonZamani to set
	 */
	public void setKalibrasyonZamani(Date kalibrasyonZamani) {
		this.kalibrasyonZamani = kalibrasyonZamani;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the kapsam
	 */
	public String getKapsam() {
		return kapsam;
	}

	/**
	 * @param kapsam
	 *            the kapsam to set
	 */
	public void setKapsam(String kapsam) {
		this.kapsam = kapsam;
	}

}