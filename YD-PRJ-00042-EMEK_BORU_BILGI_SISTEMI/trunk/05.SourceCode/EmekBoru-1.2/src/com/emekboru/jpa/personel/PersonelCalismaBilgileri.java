package com.emekboru.jpa.personel;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;


/**
 * The persistent class for the personel_calisma_bilgileri database table.
 * 
 */
@Entity
@Table(name="personel_calisma_bilgileri")
public class PersonelCalismaBilgileri implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PERSONEL_CALISMA_BILGILERI_ID_GENERATOR", sequenceName="PERSONEL_CALISMA_BILGILERI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PERSONEL_CALISMA_BILGILERI_ID_GENERATOR")
	@Column(name="ID")
	private Integer id;

	@Column(name="departman_id_gorevi")
	private Integer departmanIdGorevi;

	@Column(name="eklenme_tarihi")
	private Timestamp eklenmeTarihi;

	@Temporal(TemporalType.DATE)
	@Column(name="gorev_bitis_tarihi")
	private Date gorevBitisTarihi;

	@Column(name="gorev_id_amir")
	private Integer gorevIdAmir;

	@Column(name="gorev_id_ust_amir")
	private Integer gorevIdUstAmir;

	@Temporal(TemporalType.DATE)
	@Column(name="goreve_baslama_tarihi")
	private Date goreveBaslamaTarihi;

	@Column(name="kimlik_id")
	private Integer kimlikId;

	@Column(name="kullanici_id_ekleyen")
	private Integer kullaniciIdEkleyen;

	@Column(name="kullanici_id_guncelleyen")
	private Integer kullaniciIdGuncelleyen;

	@Column(name="son_guncellenme_tarihi")
	private Timestamp sonGuncellenmeTarihi;

	public PersonelCalismaBilgileri() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDepartmanIdGorevi() {
		return this.departmanIdGorevi;
	}

	public void setDepartmanIdGorevi(Integer departmanIdGorevi) {
		this.departmanIdGorevi = departmanIdGorevi;
	}

	public Timestamp getEklenmeTarihi() {
		return this.eklenmeTarihi;
	}

	public void setEklenmeTarihi(Timestamp eklenmeTarihi) {
		this.eklenmeTarihi = eklenmeTarihi;
	}

	public Date getGorevBitisTarihi() {
		return this.gorevBitisTarihi;
	}

	public void setGorevBitisTarihi(Date gorevBitisTarihi) {
		this.gorevBitisTarihi = gorevBitisTarihi;
	}

	public Integer getGorevIdAmir() {
		return this.gorevIdAmir;
	}

	public void setGorevIdAmir(Integer gorevIdAmir) {
		this.gorevIdAmir = gorevIdAmir;
	}

	public Integer getGorevIdUstAmir() {
		return this.gorevIdUstAmir;
	}

	public void setGorevIdUstAmir(Integer gorevIdUstAmir) {
		this.gorevIdUstAmir = gorevIdUstAmir;
	}

	public Date getGoreveBaslamaTarihi() {
		return this.goreveBaslamaTarihi;
	}

	public void setGoreveBaslamaTarihi(Date goreveBaslamaTarihi) {
		this.goreveBaslamaTarihi = goreveBaslamaTarihi;
	}

	public Integer getKimlikId() {
		return this.kimlikId;
	}

	public void setKimlikId(Integer kimlikId) {
		this.kimlikId = kimlikId;
	}

	public Integer getKullaniciIdEkleyen() {
		return this.kullaniciIdEkleyen;
	}

	public void setKullaniciIdEkleyen(Integer kullaniciIdEkleyen) {
		this.kullaniciIdEkleyen = kullaniciIdEkleyen;
	}

	public Integer getKullaniciIdGuncelleyen() {
		return this.kullaniciIdGuncelleyen;
	}

	public void setKullaniciIdGuncelleyen(Integer kullaniciIdGuncelleyen) {
		this.kullaniciIdGuncelleyen = kullaniciIdGuncelleyen;
	}

	public Timestamp getSonGuncellenmeTarihi() {
		return this.sonGuncellenmeTarihi;
	}

	public void setSonGuncellenmeTarihi(Timestamp sonGuncellenmeTarihi) {
		this.sonGuncellenmeTarihi = sonGuncellenmeTarihi;
	}

}