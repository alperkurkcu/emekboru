package com.emekboru.jpa.employee;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.Employee;

/**
 * The persistent class for the employee_activitity_participation database
 * table.
 * 
 */
@Entity
@Table(name = "employee_activity_participation")
@NamedQuery(name = "EmployeeActivityParticipation.findAll", query = "SELECT e FROM EmployeeActivityParticipation e")
public class EmployeeActivityParticipation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "EMPLOYEE_ACTIVITY_PARTICIPATION_ID_GENERATOR", sequenceName = "EMPLOYEE_ACTIVITY_PARTICIPATION_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMPLOYEE_ACTIVITY_PARTICIPATION_ID_GENERATOR")
	@Column(name = "id")
	private Integer id;

	@Column(name = "certificate_file_location")
	private String certificateFileLocation;

	@Column(name = "certificatetion_information")
	private String certificatetionInformation;

	@Column(name = "description")
	private String description;

	@Column(name = "ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@ManyToOne
	@JoinColumn(name = "employee_id", insertable = false)
	private Employee employee;

	@ManyToOne
	@JoinColumn(name = "activity_id", insertable = false)
	private EmployeeActivity employeeActivity;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	public EmployeeActivityParticipation() {
		this.employeeActivity = new EmployeeActivity();
		this.employee = new Employee();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCertificateFileLocation() {
		return this.certificateFileLocation;
	}

	public void setCertificateFileLocation(String certificateFileLocation) {
		this.certificateFileLocation = certificateFileLocation;
	}

	public String getCertificatetionInformation() {
		return this.certificatetionInformation;
	}

	public void setCertificatetionInformation(String certificatetionInformation) {
		this.certificatetionInformation = certificatetionInformation;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public EmployeeActivity getEmployeeActivity() {
		return employeeActivity;
	}

	public void setEmployeeActivity(EmployeeActivity employeeActivity) {
		this.employeeActivity = employeeActivity;
	}

}