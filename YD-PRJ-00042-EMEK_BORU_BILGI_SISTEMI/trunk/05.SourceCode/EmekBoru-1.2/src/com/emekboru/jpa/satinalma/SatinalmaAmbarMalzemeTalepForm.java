package com.emekboru.jpa.satinalma;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.emekboru.jpa.Employee;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the satinalma_ambar_malzeme_talep_form database
 * table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "SatinalmaAmbarMalzemeTalepForm.findAll", query = "SELECT r FROM SatinalmaAmbarMalzemeTalepForm r order by r.id DESC"),
		@NamedQuery(name = "SatinalmaAmbarMalzemeTalepForm.findByFormNo", query = "SELECT r FROM SatinalmaAmbarMalzemeTalepForm r where r.formSayisi=:prmFormNo order by r.id DESC"),
		@NamedQuery(name = "SatinalmaAmbarMalzemeTalepForm.findByUserId", query = "SELECT r FROM SatinalmaAmbarMalzemeTalepForm r where r.ekleyenKullanici=:prmEmployeeId order by r.id DESC"),
		@NamedQuery(name = "SatinalmaAmbarMalzemeTalepForm.findOnayBekleyenler", query = "SELECT r FROM SatinalmaAmbarMalzemeTalepForm r where r.supervisor.employeeId=:prmEmployeeId and r.tamamlanma=true and r.supervisorApproval=0 order by r.id DESC"),
		@NamedQuery(name = "SatinalmaAmbarMalzemeTalepForm.findHistory", query = "SELECT r FROM SatinalmaAmbarMalzemeTalepForm r where r.supervisor.employeeId=:prmEmployeeId and r.tamamlanma=true and r.supervisorApproval!=0 order by r.id DESC"),
		@NamedQuery(name = "SatinalmaAmbarMalzemeTalepForm.findOpenedFinished", query = "SELECT r FROM SatinalmaAmbarMalzemeTalepForm r where r.satinalmaDurum.id=1 and r.tamamlanma=true"),
		@NamedQuery(name = "SatinalmaAmbarMalzemeTalepForm.totalAll", query = "SELECT COUNT(r) FROM SatinalmaAmbarMalzemeTalepForm r"),
		@NamedQuery(name = "SatinalmaAmbarMalzemeTalepForm.totalAllByEmployeeId", query = "SELECT COUNT(r) FROM SatinalmaAmbarMalzemeTalepForm r where r.ekleyenKullanici=:prmEmployeeId"),
		@NamedQuery(name = "SatinalmaAmbarMalzemeTalepForm.totalAllHistory", query = "SELECT COUNT(r) FROM SatinalmaAmbarMalzemeTalepForm r where r.supervisor.employeeId=:prmEmployeeId and r.tamamlanma=true and r.supervisorApproval!=0") })
@Table(name = "satinalma_ambar_malzeme_talep_form")
public class SatinalmaAmbarMalzemeTalepForm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SATINALMA_AMBAR_MALZEME_TALEP_FORM_ID_GENERATOR", sequenceName = "SATINALMA_AMBAR_MALZEME_TALEP_FORM_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SATINALMA_AMBAR_MALZEME_TALEP_FORM_ID_GENERATOR")
	@Column(name = "ID")
	private Integer id;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser user;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "butce_kodu")
	private String butceKodu;

	@Column(name = "proje_adi")
	private String projeAdi;

	@Column(name = "sayi")
	private Integer sayi;

	// @Column(name = "proje_id")
	// private Integer projeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "proje_id", referencedColumnName = "item_id", insertable = false)
	private SalesItem salesItem;

	// @Column(name = "durum")
	// private Integer durum;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "durum", referencedColumnName = "ID", insertable = false)
	private SatinalmaDurum satinalmaDurum;

	@Column(name = "aciklama")
	private String aciklama;

	@Column(name = "form_sayisi")
	private String formSayisi;

	@Column(name = "tamamlanma")
	private boolean tamamlanma = false;

	@Column(name = "supervisor_approval")
	private Integer supervisorApproval;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "supervisor_id", referencedColumnName = "employee_id", insertable = false)
	private Employee supervisor;

	@Transient
	public static final int PENDING = 0;
	@Transient
	public static final int APPROVED = 1;
	@Transient
	public static final int DECLINED = 2;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "onay_tarihi", nullable = false)
	private Date onayTarihi;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "talep_tarihi", nullable = false)
	private Date talepTarihi;

	// @Column(name = "talep_sahibi_id")
	// private Integer talepSahibiId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "talep_sahibi_id", referencedColumnName = "employee_id", insertable = false)
	private Employee talepSahibi;

	@Column(name = "rev_aciklama")
	private String revAciklama;

	public SatinalmaAmbarMalzemeTalepForm() {

	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getEklemeZamani() {
		return eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getButceKodu() {
		return butceKodu;
	}

	public void setButceKodu(String butceKodu) {
		this.butceKodu = butceKodu;
	}

	public String getProjeAdi() {
		return projeAdi;
	}

	public void setProjeAdi(String projeAdi) {
		this.projeAdi = projeAdi;
	}

	public Integer getSayi() {
		return sayi;
	}

	public void setSayi(Integer sayi) {
		this.sayi = sayi;
	}

	public SystemUser getUser() {
		return user;
	}

	public void setUser(SystemUser user) {
		this.user = user;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public SatinalmaDurum getSatinalmaDurum() {
		return satinalmaDurum;
	}

	public void setSatinalmaDurum(SatinalmaDurum satinalmaDurum) {
		this.satinalmaDurum = satinalmaDurum;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getFormSayisi() {
		return formSayisi;
	}

	public void setFormSayisi(String formSayisi) {
		this.formSayisi = formSayisi;
	}

	public boolean isTamamlanma() {
		return tamamlanma;
	}

	public void setTamamlanma(boolean tamamlanma) {
		this.tamamlanma = tamamlanma;
	}

	public Integer getSupervisorApproval() {
		return supervisorApproval;
	}

	public void setSupervisorApproval(Integer supervisorApproval) {
		this.supervisorApproval = supervisorApproval;
	}

	public Employee getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(Employee supervisor) {
		this.supervisor = supervisor;
	}

	public Date getOnayTarihi() {
		return onayTarihi;
	}

	public void setOnayTarihi(Date onayTarihi) {
		this.onayTarihi = onayTarihi;
	}

	public Date getTalepTarihi() {
		return talepTarihi;
	}

	public void setTalepTarihi(Date talepTarihi) {
		this.talepTarihi = talepTarihi;
	}

	/**
	 * @return the talepSahibi
	 */
	public Employee getTalepSahibi() {
		return talepSahibi;
	}

	/**
	 * @param talepSahibi
	 *            the talepSahibi to set
	 */
	public void setTalepSahibi(Employee talepSahibi) {
		this.talepSahibi = talepSahibi;
	}

	/**
	 * @return the revAciklama
	 */
	public String getRevAciklama() {
		return revAciklama;
	}

	/**
	 * @param revAciklama
	 *            the revAciklama to set
	 */
	public void setRevAciklama(String revAciklama) {
		this.revAciklama = revAciklama;
	}

}