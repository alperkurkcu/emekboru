package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "coating_cost")
public class CoatingCost implements Serializable{

	private static final long serialVersionUID = -1888299458154701045L;

	
	@Id
	@Column(name = "coating_cost_id")
	private Integer coatingCostId;
	
	@Column(name = "sq_meter_cost")
	private Double sqMeterCost;
	
	@Column(name = "total_cost")
	private Double totalCost;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "currency_code", referencedColumnName = "currency_code", insertable = false, updatable = false)
	private Currency currency;
	
	public CoatingCost(){
		
	}

	/**
	 * 		GETTERS AND SETTERS
	 */
	public Integer getCoatingCostId() {
		return coatingCostId;
	}

	public void setCoatingCostId(Integer coatingCostId) {
		this.coatingCostId = coatingCostId;
	}

	public Double getSqMeterCost() {
		return sqMeterCost;
	}

	public void setSqMeterCost(Double sqMeterCost) {
		this.sqMeterCost = sqMeterCost;
	}

	public Double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
