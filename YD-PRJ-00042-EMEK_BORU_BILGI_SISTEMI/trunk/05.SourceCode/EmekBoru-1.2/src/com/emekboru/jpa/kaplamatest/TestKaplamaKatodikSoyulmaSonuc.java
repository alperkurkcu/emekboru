package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.isolation.IsolationTestDefinition;

/**
 * The persistent class for the test_kaplama_katodik_soyulma_sonuc database
 * table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaKatodikSoyulmaSonuc.findAll", query = "SELECT r FROM TestKaplamaKatodikSoyulmaSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_kaplama_katodik_soyulma_sonuc")
public class TestKaplamaKatodikSoyulmaSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KAPLAMA_KATODIK_SOYULMA_SONUC_ID_GENERATOR", sequenceName = "TEST_KAPLAMA_KATODIK_SOYULMA_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KAPLAMA_KATODIK_SOYULMA_SONUC_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Column(name = "aciklama", length = 255)
	private String aciklama;

	@Column(name = "art_ort")
	private BigDecimal artOrt;

	@Column(name = "deligin_capi")
	private BigDecimal deliginCapi;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name = "global_id")
	// private Integer globalId;

	@Column(name = "holiday")
	private Boolean holiday;

	@Column(name = "holiday_sonuc")
	private Boolean holidaySonuc;

	@Column(name = "holiday_test_voltaji")
	private String holidayTestVoltaji;

	// @Column(name = "isolation_test_id")
	// private Integer isolationTestId;

	@Column(name = "kaplama_kalinligi")
	private BigDecimal kaplamaKalinligi;

	@Column(name = "pinhole")
	private Boolean pinhole;

	@Column(name = "pinhole_sonuc")
	private Boolean pinholeSonuc;

	@Column(name = "pinhole_test_voltaji")
	private String pinholeTestVoltaji;

	// @Column(name = "pipe_id")
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	private String sonuc;

	@Column(name = "spec_degeri")
	private BigDecimal specDegeri;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_bas_tarihi")
	private Date testBasTarihi;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_bit_tarihi")
	private Date testBitTarihi;

	@Column(name = "test_num_boyutlari")
	private String testNumBoyutlari;

	@Column(name = "test_sicakligi")
	private String testSicakligi;

	@Column(name = "test_suresi")
	private String testSuresi;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi")
	private Date testTarihi;

	@Column(name = "test_voltaji")
	private String testVoltaji;

	@Column(name = "yaricap_a")
	private BigDecimal yaricapA;

	@Column(name = "yaricap_b")
	private BigDecimal yaricapB;

	@Column(name = "yaricap_c")
	private BigDecimal yaricapC;

	@Column(name = "yaricap_d")
	private BigDecimal yaricapD;

	@Column(name = "yaricap_e")
	private BigDecimal yaricapE;

	@Column(name = "yaricap_f")
	private BigDecimal yaricapF;

	@Column(name = "yaricap_g")
	private BigDecimal yaricapG;

	@Column(name = "yaricap_h")
	private BigDecimal yaricapH;

	@Column(name = "yaricap_i")
	private BigDecimal yaricapI;

	@Column(name = "yaricap_j")
	private BigDecimal yaricapJ;

	@Column(name = "yaricap_k")
	private BigDecimal yaricapK;

	@Column(name = "yaricap_l")
	private BigDecimal yaricapL;

	public TestKaplamaKatodikSoyulmaSonuc() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Boolean getHoliday() {
		return this.holiday;
	}

	public void setHoliday(Boolean holiday) {
		this.holiday = holiday;
	}

	public Boolean getHolidaySonuc() {
		return this.holidaySonuc;
	}

	public void setHolidaySonuc(Boolean holidaySonuc) {
		this.holidaySonuc = holidaySonuc;
	}

	public String getHolidayTestVoltaji() {
		return this.holidayTestVoltaji;
	}

	public void setHolidayTestVoltaji(String holidayTestVoltaji) {
		this.holidayTestVoltaji = holidayTestVoltaji;
	}

	public Boolean getPinhole() {
		return this.pinhole;
	}

	public void setPinhole(Boolean pinhole) {
		this.pinhole = pinhole;
	}

	public Boolean getPinholeSonuc() {
		return this.pinholeSonuc;
	}

	public void setPinholeSonuc(Boolean pinholeSonuc) {
		this.pinholeSonuc = pinholeSonuc;
	}

	public String getPinholeTestVoltaji() {
		return this.pinholeTestVoltaji;
	}

	public void setPinholeTestVoltaji(String pinholeTestVoltaji) {
		this.pinholeTestVoltaji = pinholeTestVoltaji;
	}

	public String getTestNumBoyutlari() {
		return this.testNumBoyutlari;
	}

	public void setTestNumBoyutlari(String testNumBoyutlari) {
		this.testNumBoyutlari = testNumBoyutlari;
	}

	public String getTestSicakligi() {
		return this.testSicakligi;
	}

	public void setTestSicakligi(String testSicakligi) {
		this.testSicakligi = testSicakligi;
	}

	public String getTestSuresi() {
		return this.testSuresi;
	}

	public void setTestSuresi(String testSuresi) {
		this.testSuresi = testSuresi;
	}

	public String getTestVoltaji() {
		return this.testVoltaji;
	}

	public void setTestVoltaji(String testVoltaji) {
		this.testVoltaji = testVoltaji;
	}

	/**
	 * @return the artOrt
	 */
	public BigDecimal getArtOrt() {
		return artOrt;
	}

	/**
	 * @param artOrt
	 *            the artOrt to set
	 */
	public void setArtOrt(BigDecimal artOrt) {
		this.artOrt = artOrt;
	}

	/**
	 * @return the deliginCapi
	 */
	public BigDecimal getDeliginCapi() {
		return deliginCapi;
	}

	/**
	 * @param deliginCapi
	 *            the deliginCapi to set
	 */
	public void setDeliginCapi(BigDecimal deliginCapi) {
		this.deliginCapi = deliginCapi;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the kaplamaKalinligi
	 */
	public BigDecimal getKaplamaKalinligi() {
		return kaplamaKalinligi;
	}

	/**
	 * @param kaplamaKalinligi
	 *            the kaplamaKalinligi to set
	 */
	public void setKaplamaKalinligi(BigDecimal kaplamaKalinligi) {
		this.kaplamaKalinligi = kaplamaKalinligi;
	}

	/**
	 * @return the bagliGlobalId
	 */
	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}

	/**
	 * @param bagliGlobalId
	 *            the bagliGlobalId to set
	 */
	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	/**
	 * @return the bagliTestId
	 */
	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the specDegeri
	 */
	public BigDecimal getSpecDegeri() {
		return specDegeri;
	}

	/**
	 * @param specDegeri
	 *            the specDegeri to set
	 */
	public void setSpecDegeri(BigDecimal specDegeri) {
		this.specDegeri = specDegeri;
	}

	/**
	 * @return the yaricapA
	 */
	public BigDecimal getYaricapA() {
		return yaricapA;
	}

	/**
	 * @param yaricapA
	 *            the yaricapA to set
	 */
	public void setYaricapA(BigDecimal yaricapA) {
		this.yaricapA = yaricapA;
	}

	/**
	 * @return the yaricapB
	 */
	public BigDecimal getYaricapB() {
		return yaricapB;
	}

	/**
	 * @param yaricapB
	 *            the yaricapB to set
	 */
	public void setYaricapB(BigDecimal yaricapB) {
		this.yaricapB = yaricapB;
	}

	/**
	 * @return the yaricapC
	 */
	public BigDecimal getYaricapC() {
		return yaricapC;
	}

	/**
	 * @param yaricapC
	 *            the yaricapC to set
	 */
	public void setYaricapC(BigDecimal yaricapC) {
		this.yaricapC = yaricapC;
	}

	/**
	 * @return the yaricapD
	 */
	public BigDecimal getYaricapD() {
		return yaricapD;
	}

	/**
	 * @param yaricapD
	 *            the yaricapD to set
	 */
	public void setYaricapD(BigDecimal yaricapD) {
		this.yaricapD = yaricapD;
	}

	/**
	 * @return the yaricapE
	 */
	public BigDecimal getYaricapE() {
		return yaricapE;
	}

	/**
	 * @param yaricapE
	 *            the yaricapE to set
	 */
	public void setYaricapE(BigDecimal yaricapE) {
		this.yaricapE = yaricapE;
	}

	/**
	 * @return the yaricapF
	 */
	public BigDecimal getYaricapF() {
		return yaricapF;
	}

	/**
	 * @param yaricapF
	 *            the yaricapF to set
	 */
	public void setYaricapF(BigDecimal yaricapF) {
		this.yaricapF = yaricapF;
	}

	/**
	 * @return the yaricapG
	 */
	public BigDecimal getYaricapG() {
		return yaricapG;
	}

	/**
	 * @param yaricapG
	 *            the yaricapG to set
	 */
	public void setYaricapG(BigDecimal yaricapG) {
		this.yaricapG = yaricapG;
	}

	/**
	 * @return the yaricapH
	 */
	public BigDecimal getYaricapH() {
		return yaricapH;
	}

	/**
	 * @param yaricapH
	 *            the yaricapH to set
	 */
	public void setYaricapH(BigDecimal yaricapH) {
		this.yaricapH = yaricapH;
	}

	/**
	 * @return the yaricapI
	 */
	public BigDecimal getYaricapI() {
		return yaricapI;
	}

	/**
	 * @param yaricapI
	 *            the yaricapI to set
	 */
	public void setYaricapI(BigDecimal yaricapI) {
		this.yaricapI = yaricapI;
	}

	/**
	 * @return the yaricapJ
	 */
	public BigDecimal getYaricapJ() {
		return yaricapJ;
	}

	/**
	 * @param yaricapJ
	 *            the yaricapJ to set
	 */
	public void setYaricapJ(BigDecimal yaricapJ) {
		this.yaricapJ = yaricapJ;
	}

	/**
	 * @return the yaricapK
	 */
	public BigDecimal getYaricapK() {
		return yaricapK;
	}

	/**
	 * @param yaricapK
	 *            the yaricapK to set
	 */
	public void setYaricapK(BigDecimal yaricapK) {
		this.yaricapK = yaricapK;
	}

	/**
	 * @return the yaricapL
	 */
	public BigDecimal getYaricapL() {
		return yaricapL;
	}

	/**
	 * @param yaricapL
	 *            the yaricapL to set
	 */
	public void setYaricapL(BigDecimal yaricapL) {
		this.yaricapL = yaricapL;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the testBasTarihi
	 */
	public Date getTestBasTarihi() {
		return testBasTarihi;
	}

	/**
	 * @param testBasTarihi
	 *            the testBasTarihi to set
	 */
	public void setTestBasTarihi(Date testBasTarihi) {
		this.testBasTarihi = testBasTarihi;
	}

	/**
	 * @return the testBitTarihi
	 */
	public Date getTestBitTarihi() {
		return testBitTarihi;
	}

	/**
	 * @param testBitTarihi
	 *            the testBitTarihi to set
	 */
	public void setTestBitTarihi(Date testBitTarihi) {
		this.testBitTarihi = testBitTarihi;
	}

	/**
	 * @return the testTarihi
	 */
	public Date getTestTarihi() {
		return testTarihi;
	}

	/**
	 * @param testTarihi
	 *            the testTarihi to set
	 */
	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	/**
	 * @return the sonuc
	 */
	public String getSonuc() {
		return sonuc;
	}

	/**
	 * @param sonuc the sonuc to set
	 */
	public void setSonuc(String sonuc) {
		this.sonuc = sonuc;
	}

}