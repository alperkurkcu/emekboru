package com.emekboru.jpa.isolation;

import java.util.HashMap;
import java.util.Map;

public abstract class IsolationTestsUtil {

	private static Map<IsolationTestType, String> typeCodeToTypePanel = new HashMap<IsolationTestType, String>();

	static {
		// external
		typeCodeToTypePanel.put(IsolationTestType.CSC1, "SC1Panel");
		typeCodeToTypePanel.put(IsolationTestType.CSP1, "SP1Panel");
		typeCodeToTypePanel.put(IsolationTestType.CSD1, "SD1Panel");
		typeCodeToTypePanel.put(IsolationTestType.CSS1, "SS1Panel");
		typeCodeToTypePanel.put(IsolationTestType.CAS1, "AS1Panel");
		typeCodeToTypePanel.put(IsolationTestType.CAC1, "AC1Panel");
		typeCodeToTypePanel.put(IsolationTestType.CBT1, "BT1Panel");
		typeCodeToTypePanel.put(IsolationTestType.CDS1, "DS1Panel");
		typeCodeToTypePanel.put(IsolationTestType.CCT1, "CT1Panel");
		typeCodeToTypePanel.put(IsolationTestType.OHT1, "HT1Panel");
		typeCodeToTypePanel.put(IsolationTestType.CIT1, "IT1Panel");
		typeCodeToTypePanel.put(IsolationTestType.CAT1, "AT1Panel");
		typeCodeToTypePanel.put(IsolationTestType.CIT2, "IT2Panel");
		typeCodeToTypePanel.put(IsolationTestType.CET1, "ET1Panel");
		typeCodeToTypePanel.put(IsolationTestType.CCD1, "CD1Panel");
		typeCodeToTypePanel.put(IsolationTestType.CHT2, "HT2Panel");
		typeCodeToTypePanel.put(IsolationTestType.CFI1, "FI1Panel");
		typeCodeToTypePanel.put(IsolationTestType.CKK1, "KK1Panel");
//		typeCodeToTypePanel.put(IsolationTestType.CSK1, "SK1Panel");
		typeCodeToTypePanel.put(IsolationTestType.KSK1, "KSK1Panel");
		typeCodeToTypePanel.put(IsolationTestType.CPP1, "PP1Panel");
		typeCodeToTypePanel.put(IsolationTestType.CPP2, "PP2Panel");
		typeCodeToTypePanel.put(IsolationTestType.CKB1, "KB1Panel");
		typeCodeToTypePanel.put(IsolationTestType.CKT2, "KT2Panel");
		typeCodeToTypePanel.put(IsolationTestType.CGK1, "GK1Panel");

		// internal
		typeCodeToTypePanel.put(IsolationTestType.LCT1, "CT1Panel");
		typeCodeToTypePanel.put(IsolationTestType.LSC1, "SC1Panel");
		typeCodeToTypePanel.put(IsolationTestType.LSP1, "SP1Panel");
		typeCodeToTypePanel.put(IsolationTestType.LSD1, "SD1Panel");
		typeCodeToTypePanel.put(IsolationTestType.LSS1, "SS1Panel");
		typeCodeToTypePanel.put(IsolationTestType.LAS1, "AS1Panel");
		typeCodeToTypePanel.put(IsolationTestType.LAC1, "AC1Panel");
		typeCodeToTypePanel.put(IsolationTestType.LBT1, "BT1Panel");
		typeCodeToTypePanel.put(IsolationTestType.LHT1, "HT1Panel");
		typeCodeToTypePanel.put(IsolationTestType.LIT1, "IT1Panel");
		typeCodeToTypePanel.put(IsolationTestType.LAT1, "AT1Panel");
		typeCodeToTypePanel.put(IsolationTestType.LIT2, "IT2Panel");
		typeCodeToTypePanel.put(IsolationTestType.LCD1, "CD1Panel");
		typeCodeToTypePanel.put(IsolationTestType.LHT2, "HT2Panel");
		typeCodeToTypePanel.put(IsolationTestType.LFI1, "FI1Panel");
		typeCodeToTypePanel.put(IsolationTestType.BCB1, "CB1Panel");
		typeCodeToTypePanel.put(IsolationTestType.BSC1, "BSC1Panel");
		typeCodeToTypePanel.put(IsolationTestType.BYH1, "YH1Panel");
		typeCodeToTypePanel.put(IsolationTestType.BGK1, "GK1Panel");
		typeCodeToTypePanel.put(IsolationTestType.LKB1, "LK1Panel");
//		typeCodeToTypePanel.put(IsolationTestType.BUS1, "US1Panel");
		typeCodeToTypePanel.put(IsolationTestType.LKK1, "KK1Panel");
		typeCodeToTypePanel.put(IsolationTestType.LSK1, "SK1Panel");
		typeCodeToTypePanel.put(IsolationTestType.LDT1, "DT1Panel");

	}

	public static String getPanelIdByTestCode(
			IsolationTestType isolationTestType) {
		return typeCodeToTypePanel.get(isolationTestType);
	}

}
