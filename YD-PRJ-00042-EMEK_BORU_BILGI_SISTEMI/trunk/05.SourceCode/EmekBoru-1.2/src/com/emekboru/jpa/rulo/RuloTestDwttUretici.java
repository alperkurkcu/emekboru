package com.emekboru.jpa.rulo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the rulo_test_dwtt_uretici database table.
 * 
 */
@Entity
@Table(name = "rulo_test_dwtt_uretici")
public class RuloTestDwttUretici implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "RULO_TEST_DWTT_URETICI_RULODWTTURETICIID_GENERATOR", sequenceName = "RULO_TEST_DWTT_URETICI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RULO_TEST_DWTT_URETICI_RULODWTTURETICIID_GENERATOR")
	@Column(name = "rulo_dwtt_uretici_id", unique = true, nullable = false)
	private Integer ruloDwttUreticiId;

	@Column(name = "durum")
	private Integer durum = 0;

	@Column(name = "islem_yapan_id")
	private Integer islemYapanId;

	@Column(name = "islem_yapan_isim", length = 2147483647)
	private String islemYapanIsim;

	@Column(name = "rulo_dwtt_uretici_aciklama", length = 2147483647)
	private String ruloDwttUreticiAciklama;

	@Column(name = "rulo_dwtt_uretici_kirilma_alani_bir")
	private float ruloDwttUreticiKirilmaAlaniBir;

	@Column(name = "rulo_dwtt_uretici_kirilma_alani_iki")
	private float ruloDwttUreticiKirilmaAlaniIki;

	@Column(name = "rulo_dwtt_uretici_ortalama_kirilma_alani")
	private float ruloDwttUreticiOrtalamaKirilmaAlani;

	@Column(name = "rulo_dwtt_uretici_sicaklik")
	private float ruloDwttUreticiSicaklik;

	@Temporal(TemporalType.DATE)
	@Column(name = "rulo_dwtt_uretici_tarih")
	private Date ruloDwttUreticiTarih;

	// @Column(name = "rulo_id")
	// private Integer ruloId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "rulo_id", nullable = false)
	private Rulo rulo;

	public RuloTestDwttUretici() {
	}

	public Integer getRuloDwttUreticiId() {
		return this.ruloDwttUreticiId;
	}

	public void setRuloDwttUreticiId(Integer ruloDwttUreticiId) {
		this.ruloDwttUreticiId = ruloDwttUreticiId;
	}

	public Integer getDurum() {
		return this.durum;
	}

	public void setDurum(Integer durum) {
		this.durum = durum;
	}

	public Integer getIslemYapanId() {
		return this.islemYapanId;
	}

	public void setIslemYapanId(Integer islemYapanId) {
		this.islemYapanId = islemYapanId;
	}

	public String getIslemYapanIsim() {
		return this.islemYapanIsim;
	}

	public void setIslemYapanIsim(String islemYapanIsim) {
		this.islemYapanIsim = islemYapanIsim;
	}

	public String getRuloDwttUreticiAciklama() {
		return this.ruloDwttUreticiAciklama;
	}

	public void setRuloDwttUreticiAciklama(String ruloDwttUreticiAciklama) {
		this.ruloDwttUreticiAciklama = ruloDwttUreticiAciklama;
	}

	public float getRuloDwttUreticiKirilmaAlaniBir() {
		return this.ruloDwttUreticiKirilmaAlaniBir;
	}

	public void setRuloDwttUreticiKirilmaAlaniBir(
			float ruloDwttUreticiKirilmaAlaniBir) {
		this.ruloDwttUreticiKirilmaAlaniBir = ruloDwttUreticiKirilmaAlaniBir;
	}

	public float getRuloDwttUreticiKirilmaAlaniIki() {
		return this.ruloDwttUreticiKirilmaAlaniIki;
	}

	public void setRuloDwttUreticiKirilmaAlaniIki(
			float ruloDwttUreticiKirilmaAlaniIki) {
		this.ruloDwttUreticiKirilmaAlaniIki = ruloDwttUreticiKirilmaAlaniIki;
	}

	public float getRuloDwttUreticiOrtalamaKirilmaAlani() {
		return this.ruloDwttUreticiOrtalamaKirilmaAlani;
	}

	public void setRuloDwttUreticiOrtalamaKirilmaAlani(
			float ruloDwttUreticiOrtalamaKirilmaAlani) {
		this.ruloDwttUreticiOrtalamaKirilmaAlani = ruloDwttUreticiOrtalamaKirilmaAlani;
	}

	/**
	 * @return the rulo
	 */
	public Rulo getRulo() {
		return rulo;
	}

	/**
	 * @param rulo
	 *            the rulo to set
	 */
	public void setRulo(Rulo rulo) {
		this.rulo = rulo;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the ruloDwttUreticiTarih
	 */
	public Date getRuloDwttUreticiTarih() {
		return ruloDwttUreticiTarih;
	}

	/**
	 * @param ruloDwttUreticiTarih
	 *            the ruloDwttUreticiTarih to set
	 */
	public void setRuloDwttUreticiTarih(Date ruloDwttUreticiTarih) {
		this.ruloDwttUreticiTarih = ruloDwttUreticiTarih;
	}

	/**
	 * @return the ruloDwttUreticiSicaklik
	 */
	public float getRuloDwttUreticiSicaklik() {
		return ruloDwttUreticiSicaklik;
	}

	/**
	 * @param ruloDwttUreticiSicaklik
	 *            the ruloDwttUreticiSicaklik to set
	 */
	public void setRuloDwttUreticiSicaklik(float ruloDwttUreticiSicaklik) {
		this.ruloDwttUreticiSicaklik = ruloDwttUreticiSicaklik;
	}

}