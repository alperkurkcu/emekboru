package com.emekboru.jpa.isolation;

import com.emekboru.jpa.sales.order.SalesItem;

public abstract class AbstractTestsSpec {

	// public abstract void setOrder(Order order);

	// entegrasyon
	public abstract void setSalesItem(SalesItem salesItem);

	// entegrasyon

	public abstract void setIsolationTestDefinition(
			IsolationTestDefinition isolationTestDefinition);
}
