package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.config.Test;
import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the kaynak_sertlik_tests_spec database table.
 * 
 */
@NamedQueries({
		@NamedQuery(name = "KSTS.kontrol", query = "SELECT r.testId FROM KaynakSertlikTestsSpec r WHERE r.globalId = :prmGlobalId and r.salesItem.itemId=:prmItemId"),
		@NamedQuery(name = "KSTS.seciliKaynakSertlikTestTanim", query = "SELECT r FROM KaynakSertlikTestsSpec r WHERE r.globalId = :prmGlobalId and r.salesItem.itemId=:prmItemId"),
		@NamedQuery(name = "KSTS.findByItemId", query = "SELECT r FROM KaynakSertlikTestsSpec r WHERE r.salesItem.itemId=:prmItemId") })
@Entity
@Table(name = "kaynak_sertlik_tests_spec")
public class KaynakSertlikTestsSpec implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final int KAYNAK_SERTLIK_TEST = 1032;

	@Id
	@SequenceGenerator(name = "KAYNAK_SERTLIK_TESTS_SPEC_GENERATOR", sequenceName = "KAYNAK_SERTLIK_TESTS_SPEC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "KAYNAK_SERTLIK_TESTS_SPEC_GENERATOR")
	@Column(name = "test_id")
	private Integer testId;

	@Column(name = "completed")
	private Boolean completed;

	@Column(name = "explanation")
	private String explanation;

	@Column(name = "global_id")
	private Integer globalId;

	@Column(name = "max_sertlik")
	private Float maxSertlik;

	@Column(name = "name")
	private String name;

	@Column(name = "code")
	private String code;

	// @JoinColumn(name = "order_id", referencedColumnName = "order_id",
	// insertable = false)
	// @ManyToOne(fetch = FetchType.LAZY)
	// private Order order;
	//
	// @ManyToOne
	// @JoinColumns({
	// @JoinColumn(name = "order_id", referencedColumnName = "order_id",
	// insertable = false, updatable = false),
	// @JoinColumn(name = "global_id", referencedColumnName = "global_id",
	// insertable = false, updatable = false) })
	// private DestructiveTestsSpec mainDestructiveTestSpecs;

	// entegrasyon
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	private SalesItem salesItem;

	@ManyToOne
	@JoinColumns({
			@JoinColumn(name = "sales_item_id", referencedColumnName = "sales_item_id", insertable = false, updatable = false),
			@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false, updatable = false) })
	private DestructiveTestsSpec salesDestructiveTestSpecs;

	// entegrasyon

	public KaynakSertlikTestsSpec() {
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public DestructiveTestsSpec getSalesDestructiveTestSpecs() {
		return salesDestructiveTestSpecs;
	}

	public void setSalesDestructiveTestSpecs(
			DestructiveTestsSpec salesDestructiveTestSpecs) {
		this.salesDestructiveTestSpecs = salesDestructiveTestSpecs;
	}

	public KaynakSertlikTestsSpec(Test t) {

		globalId = t.getGlobalId();
		name = t.getDisplayName();
		code = t.getCode();
		completed = false;
	}

	public void reset() {

		testId = 0;
		explanation = "";
		maxSertlik = (float) 0.0;
	}

	// GETTERS AND SETTERS
	public Integer getTestId() {
		return this.testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	public Boolean getCompleted() {
		return this.completed;
	}

	public void setCompleted(Boolean completed) {
		this.completed = completed;
	}

	public String getExplanation() {
		return this.explanation;
	}

	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}

	public Integer getGlobalId() {
		return this.globalId;
	}

	public void setGlobalId(Integer globalId) {
		this.globalId = globalId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Float getMaxSertlik() {
		return maxSertlik;
	}

	public void setMaxSertlik(Float maxSertlik) {
		this.maxSertlik = maxSertlik;
	}

}