package com.emekboru.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the weld_parameters database table.
 * 
 */
@Entity
@Table(name="weld_parameters")
public class WeldParameter implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="weld_parameter_id")
	@SequenceGenerator(name="WELD_PARAMETER_GENERATOR", sequenceName="WELD_PARAMETERS_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="WELD_PARAMETER_GENERATOR")	
	private Integer weldParameterId;

    @Temporal( TemporalType.TIMESTAMP)
	private Date date;

	@Column(name="dis_ac_tel_boyu")
	private double disAcTelBoyu;

	@Column(name="dis_akim_ac")
	private double disAkimAc;

	@Column(name="dis_akim_dc")
	private double disAkimDc;

	@Column(name="dis_dc_tel_boyu")
	private double disDcTelBoyu;

	@Column(name="dis_tel_ac")
	private double disTelAc;

	@Column(name="dis_tel_dc")
	private double disTelDc;

	@Column(name="dis_volt_ac")
	private double disVoltAc;

	@Column(name="dis_volt_dc")
	private double disVoltDc;

	@Column(name="ic_ac_tel_boyu")
	private double icAcTelBoyu;

	@Column(name="ic_akim_ac")
	private double icAkimAc;

	@Column(name="ic_akim_dc")
	private double icAkimDc;

	@Column(name="ic_dc_tel_boyu")
	private double icDcTelBoyu;

	@Column(name="ic_tel_ac")
	private double icTelAc;

	@Column(name="ic_tel_dc")
	private double icTelDc;

	@Column(name="ic_volt_ac")
	private double icVoltAc;

	@Column(name="ic_volt_dc")
	private double icVoltDc;

	@Column(name="kaynak_acisi")
	private double kaynakAcisi;

	@Column(name="kaynak_hizi")
	private double kaynakHizi;

	@Column(name="kaynak_yontemi")
	private Boolean kaynakYontemi;

	@Column(name="order_item_id")
	private Integer orderItemId;

    public WeldParameter() {
    }

	public Integer getWeldParameterId() {
		return this.weldParameterId;
	}

	public void setWeldParameterId(Integer weldParameterId) {
		this.weldParameterId = weldParameterId;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getDisAcTelBoyu() {
		return this.disAcTelBoyu;
	}

	public void setDisAcTelBoyu(double disAcTelBoyu) {
		this.disAcTelBoyu = disAcTelBoyu;
	}

	public double getDisAkimAc() {
		return this.disAkimAc;
	}

	public void setDisAkimAc(double disAkimAc) {
		this.disAkimAc = disAkimAc;
	}

	public double getDisAkimDc() {
		return this.disAkimDc;
	}

	public void setDisAkimDc(double disAkimDc) {
		this.disAkimDc = disAkimDc;
	}

	public double getDisDcTelBoyu() {
		return this.disDcTelBoyu;
	}

	public void setDisDcTelBoyu(double disDcTelBoyu) {
		this.disDcTelBoyu = disDcTelBoyu;
	}

	public double getDisTelAc() {
		return this.disTelAc;
	}

	public void setDisTelAc(double disTelAc) {
		this.disTelAc = disTelAc;
	}

	public double getDisTelDc() {
		return this.disTelDc;
	}

	public void setDisTelDc(double disTelDc) {
		this.disTelDc = disTelDc;
	}

	public double getDisVoltAc() {
		return this.disVoltAc;
	}

	public void setDisVoltAc(double disVoltAc) {
		this.disVoltAc = disVoltAc;
	}

	public double getDisVoltDc() {
		return this.disVoltDc;
	}

	public void setDisVoltDc(double disVoltDc) {
		this.disVoltDc = disVoltDc;
	}

	public double getIcAcTelBoyu() {
		return this.icAcTelBoyu;
	}

	public void setIcAcTelBoyu(double icAcTelBoyu) {
		this.icAcTelBoyu = icAcTelBoyu;
	}

	public double getIcAkimAc() {
		return this.icAkimAc;
	}

	public void setIcAkimAc(double icAkimAc) {
		this.icAkimAc = icAkimAc;
	}

	public double getIcAkimDc() {
		return this.icAkimDc;
	}

	public void setIcAkimDc(double icAkimDc) {
		this.icAkimDc = icAkimDc;
	}

	public double getIcDcTelBoyu() {
		return this.icDcTelBoyu;
	}

	public void setIcDcTelBoyu(double icDcTelBoyu) {
		this.icDcTelBoyu = icDcTelBoyu;
	}

	public double getIcTelAc() {
		return this.icTelAc;
	}

	public void setIcTelAc(double icTelAc) {
		this.icTelAc = icTelAc;
	}

	public double getIcTelDc() {
		return this.icTelDc;
	}

	public void setIcTelDc(double icTelDc) {
		this.icTelDc = icTelDc;
	}

	public double getIcVoltAc() {
		return this.icVoltAc;
	}

	public void setIcVoltAc(double icVoltAc) {
		this.icVoltAc = icVoltAc;
	}

	public double getIcVoltDc() {
		return this.icVoltDc;
	}

	public void setIcVoltDc(double icVoltDc) {
		this.icVoltDc = icVoltDc;
	}

	public double getKaynakAcisi() {
		return this.kaynakAcisi;
	}

	public void setKaynakAcisi(double kaynakAcisi) {
		this.kaynakAcisi = kaynakAcisi;
	}

	public double getKaynakHizi() {
		return this.kaynakHizi;
	}

	public void setKaynakHizi(double kaynakHizi) {
		this.kaynakHizi = kaynakHizi;
	}

	public Boolean getKaynakYontemi() {
		return this.kaynakYontemi;
	}

	public void setKaynakYontemi(Boolean kaynakYontemi) {
		this.kaynakYontemi = kaynakYontemi;
	}

	public Integer getOrderItemId() {
		return this.orderItemId;
	}

	public void setOrderItemId(Integer orderItemId) {
		this.orderItemId = orderItemId;
	}

}