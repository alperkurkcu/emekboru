package com.emekboru.jpa.rulo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQueries({ @NamedQuery(name = "RuloTestKimyasalGirdi.getByDokumNo", query = "SELECT r FROM RuloTestKimyasalGirdi r WHERE r.rulo.ruloDokumNo=:prmDokumNo") })
@Table(name = "rulo_test_kimyasal_girdi")
public class RuloTestKimyasalGirdi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "rulo_test_kimyasal_girdi_generator", sequenceName = "rulo_test_kimyasal_girdi_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rulo_test_kimyasal_girdi_generator")
	@Column(name = "rulo_test_kimyasal_girdi_id")
	private Integer ruloTestKimyasalGirdiId;

	@Column(name = "rulo_test_kimyasal_girdi_ti")
	private Float ruloTestKimyasalGirdiTi;

	@Column(name = "rulo_test_kimyasal_girdi_v")
	private Float ruloTestKimyasalGirdiV;

	@Column(name = "rulo_test_kimyasal_girdi_nb")
	private Float ruloTestKimyasalGirdiNb;

	@Column(name = "rulo_test_kimyasal_girdi_b")
	private Float ruloTestKimyasalGirdiB;

	@Column(name = "rulo_test_kimyasal_girdi_co")
	private Float ruloTestKimyasalGirdiCo;

	@Column(name = "rulo_test_kimyasal_girdi_n")
	private Float ruloTestKimyasalGirdiN;

	@Column(name = "rulo_test_kimyasal_girdi_ce1")
	private Float ruloTestKimyasalGirdiCe1;

	@Column(name = "rulo_test_kimyasal_girdi_ce2")
	private Float ruloTestKimyasalGirdiCe2;

	@Column(name = "rulo_test_kimyasal_girdi_ae")
	private Float ruloTestKimyasalGirdiAe;

	@Column(name = "rulo_test_kimyasal_girdi_c")
	private Float ruloTestKimyasalGirdiC;

	@Column(name = "rulo_test_kimyasal_girdi_si")
	private Float ruloTestKimyasalGirdiSi;

	@Column(name = "rulo_test_kimyasal_girdi_as")
	private Float ruloTestKimyasalGirdiAs;

	@Column(name = "rulo_test_kimyasal_girdi_sn")
	private Float ruloTestKimyasalGirdiSn;

	@Column(name = "rulo_test_kimyasal_girdi_sb")
	private Float ruloTestKimyasalGirdiSb;

	@Column(name = "rulo_test_kimyasal_girdi_w")
	private Float ruloTestKimyasalGirdiW;

	@Column(name = "rulo_test_kimyasal_girdi_pb")
	private Float ruloTestKimyasalGirdiPb;

	@Column(name = "rulo_test_kimyasal_girdi_ca")
	private Float ruloTestKimyasalGirdiCa;

	@Column(name = "rulo_test_kimyasal_uretici_mn")
	private Float ruloTestKimyasalGirdiMn;

	@Column(name = "rulo_test_kimyasal_uretici_p")
	private Float ruloTestKimyasalGirdiP;

	@Column(name = "rulo_test_kimyasal_uretici_s")
	private Float ruloTestKimyasalGirdiS;

	@Column(name = "rulo_test_kimyasal_uretici_cr")
	private Float ruloTestKimyasalGirdiCr;

	@Column(name = "rulo_test_kimyasal_uretici_ni")
	private Float ruloTestKimyasalGirdiNi;

	@Column(name = "rulo_test_kimyasal_uretici_mo")
	private Float ruloTestKimyasalGirdiMo;

	@Column(name = "rulo_test_kimyasal_uretici_cu")
	private Float ruloTestKimyasalGirdiCu;

	@Column(name = "rulo_test_kimyasal_uretici_al")
	private Float ruloTestKimyasalGirdiAl;

	@Column(name = "rulo_test_kimyasal_girdi_aciklama")
	private String ruloTestKimyasalGirdiAciklama;

	@Temporal(TemporalType.DATE)
	@Column(name = "rulo_test_kimyasal_girdi_tarih")
	private Date ruloTestKimyasalGirdiTarih;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "rulo_id", nullable = false)
	private Rulo rulo;

	@Column(name = "durum")
	private Integer durum;

	@Column(name = "islem_yapan_id")
	private int islemYapanId;

	@Column(name = "islem_yapan_isim")
	private String islemYapanIsim;

	@Column(name = "ti_")
	private String tiOnEk;
	@Column(name = "nb_")
	private String nbOnEk;
	@Column(name = "mo_")
	private String moOnEk;
	@Column(name = "ni_")
	private String niOnEk;
	@Column(name = "s_")
	private String sOnEk;
	@Column(name = "cu_")
	private String cuOnEk;
	@Column(name = "al_")
	private String alOnEk;
	@Column(name = "c_")
	private String cOnEk;
	@Column(name = "cr_")
	private String crOnEk;
	@Column(name = "b_")
	private String bOnEk;
	@Column(name = "v_")
	private String vOnEk;
	@Column(name = "co_")
	private String coOnEk;

	@Column(name = "si_")
	private String siOnEk;
	@Column(name = "w_")
	private String wOnEk;
	@Column(name = "as_")
	private String asOnEk;
	@Column(name = "sb_")
	private String sbOnEk;
	@Column(name = "sn_")
	private String snOnEk;
	@Column(name = "pb_")
	private String pbOnEk;
	@Column(name = "ca_")
	private String caOnEk;
	@Column(name = "n_")
	private String nOnEk;

	public String getTiOnEk() {
		return tiOnEk;
	}

	public void setTiOnEk(String tiOnEk) {
		this.tiOnEk = tiOnEk;
	}

	public String getNbOnEk() {
		return nbOnEk;
	}

	public void setNbOnEk(String nbOnEk) {
		this.nbOnEk = nbOnEk;
	}

	public String getMoOnEk() {
		return moOnEk;
	}

	public void setMoOnEk(String moOnEk) {
		this.moOnEk = moOnEk;
	}

	public String getNiOnEk() {
		return niOnEk;
	}

	public void setNiOnEk(String niOnEk) {
		this.niOnEk = niOnEk;
	}

	public String getsOnEk() {
		return sOnEk;
	}

	public void setsOnEk(String sOnEk) {
		this.sOnEk = sOnEk;
	}

	public String getCuOnEk() {
		return cuOnEk;
	}

	public void setCuOnEk(String cuOnEk) {
		this.cuOnEk = cuOnEk;
	}

	public String getAlOnEk() {
		return alOnEk;
	}

	public void setAlOnEk(String alOnEk) {
		this.alOnEk = alOnEk;
	}

	public String getcOnEk() {
		return cOnEk;
	}

	public void setcOnEk(String cOnEk) {
		this.cOnEk = cOnEk;
	}

	public String getCrOnEk() {
		return crOnEk;
	}

	public void setCrOnEk(String crOnEk) {
		this.crOnEk = crOnEk;
	}

	public String getbOnEk() {
		return bOnEk;
	}

	public void setbOnEk(String bOnEk) {
		this.bOnEk = bOnEk;
	}

	public String getvOnEk() {
		return vOnEk;
	}

	public void setvOnEk(String vOnEk) {
		this.vOnEk = vOnEk;
	}

	public String getCoOnEk() {
		return coOnEk;
	}

	public void setCoOnEk(String coOnEk) {
		this.coOnEk = coOnEk;
	}

	public int getIslemYapanId() {
		return islemYapanId;
	}

	public void setIslemYapanId(int islemYapanId) {
		this.islemYapanId = islemYapanId;
	}

	public String getIslemYapanIsim() {
		return islemYapanIsim;
	}

	public void setIslemYapanIsim(String islemYapanIsim) {
		this.islemYapanIsim = islemYapanIsim;
	}

	public RuloTestKimyasalGirdi() {
		durum = new Integer("0");
	}

	public Integer getDurum() {
		if (durum == null)
			return 0;
		return durum;
	}

	public void setDurum(Integer durum) {
		this.durum = durum;
	}

	public Rulo getRulo() {
		return rulo;
	}

	public void setRulo(Rulo rulo) {
		this.rulo = rulo;
	}

	public Integer getRuloTestKimyasalGirdiId() {
		return ruloTestKimyasalGirdiId;
	}

	public void setRuloTestKimyasalGirdiId(Integer ruloTestKimyasalGirdiId) {
		this.ruloTestKimyasalGirdiId = ruloTestKimyasalGirdiId;
	}

	public String getRuloTestKimyasalGirdiAciklama() {
		return ruloTestKimyasalGirdiAciklama;
	}

	public void setRuloTestKimyasalGirdiAciklama(
			String ruloTestKimyasalGirdiAciklama) {
		this.ruloTestKimyasalGirdiAciklama = ruloTestKimyasalGirdiAciklama;
	}

	public Date getRuloTestKimyasalGirdiTarih() {
		return ruloTestKimyasalGirdiTarih;
	}

	public void setRuloTestKimyasalGirdiTarih(Date ruloTestKimyasalGirdiTarih) {
		this.ruloTestKimyasalGirdiTarih = ruloTestKimyasalGirdiTarih;
	}

	public Float getRuloTestKimyasalGirdiTi() {
		return ruloTestKimyasalGirdiTi;
	}

	public void setRuloTestKimyasalGirdiTi(Float ruloTestKimyasalGirdiTi) {
		this.ruloTestKimyasalGirdiTi = ruloTestKimyasalGirdiTi;
	}

	public Float getRuloTestKimyasalGirdiV() {
		return ruloTestKimyasalGirdiV;
	}

	public void setRuloTestKimyasalGirdiV(Float ruloTestKimyasalGirdiV) {
		this.ruloTestKimyasalGirdiV = ruloTestKimyasalGirdiV;
	}

	public Float getRuloTestKimyasalGirdiNb() {
		return ruloTestKimyasalGirdiNb;
	}

	public void setRuloTestKimyasalGirdiNb(Float ruloTestKimyasalGirdiNb) {
		this.ruloTestKimyasalGirdiNb = ruloTestKimyasalGirdiNb;
	}

	public Float getRuloTestKimyasalGirdiB() {
		return ruloTestKimyasalGirdiB;
	}

	public void setRuloTestKimyasalGirdiB(Float ruloTestKimyasalGirdiB) {
		this.ruloTestKimyasalGirdiB = ruloTestKimyasalGirdiB;
	}

	public Float getRuloTestKimyasalGirdiCo() {
		return ruloTestKimyasalGirdiCo;
	}

	public void setRuloTestKimyasalGirdiCo(Float ruloTestKimyasalGirdiCo) {
		this.ruloTestKimyasalGirdiCo = ruloTestKimyasalGirdiCo;
	}

	public Float getRuloTestKimyasalGirdiN() {
		return ruloTestKimyasalGirdiN;
	}

	public void setRuloTestKimyasalGirdiN(Float ruloTestKimyasalGirdiN) {
		this.ruloTestKimyasalGirdiN = ruloTestKimyasalGirdiN;
	}

	public Float getRuloTestKimyasalGirdiCe1() {
		return ruloTestKimyasalGirdiCe1;
	}

	public void setRuloTestKimyasalGirdiCe1(Float ruloTestKimyasalGirdiCe1) {
		this.ruloTestKimyasalGirdiCe1 = ruloTestKimyasalGirdiCe1;
	}

	public Float getRuloTestKimyasalGirdiCe2() {
		return ruloTestKimyasalGirdiCe2;
	}

	public void setRuloTestKimyasalGirdiCe2(Float ruloTestKimyasalGirdiCe2) {
		this.ruloTestKimyasalGirdiCe2 = ruloTestKimyasalGirdiCe2;
	}

	public Float getRuloTestKimyasalGirdiAe() {
		return ruloTestKimyasalGirdiAe;
	}

	public void setRuloTestKimyasalGirdiAe(Float ruloTestKimyasalGirdiAe) {
		this.ruloTestKimyasalGirdiAe = ruloTestKimyasalGirdiAe;
	}

	public Float getRuloTestKimyasalGirdiC() {
		return ruloTestKimyasalGirdiC;
	}

	public void setRuloTestKimyasalGirdiC(Float ruloTestKimyasalGirdiC) {
		this.ruloTestKimyasalGirdiC = ruloTestKimyasalGirdiC;
	}

	public Float getRuloTestKimyasalGirdiSi() {
		return ruloTestKimyasalGirdiSi;
	}

	public void setRuloTestKimyasalGirdiSi(Float ruloTestKimyasalGirdiSi) {
		this.ruloTestKimyasalGirdiSi = ruloTestKimyasalGirdiSi;
	}

	public Float getRuloTestKimyasalGirdiMn() {
		return ruloTestKimyasalGirdiMn;
	}

	public void setRuloTestKimyasalGirdiMn(Float ruloTestKimyasalGirdiMn) {
		this.ruloTestKimyasalGirdiMn = ruloTestKimyasalGirdiMn;
	}

	public Float getRuloTestKimyasalGirdiP() {
		return ruloTestKimyasalGirdiP;
	}

	public void setRuloTestKimyasalGirdiP(Float ruloTestKimyasalGirdiP) {
		this.ruloTestKimyasalGirdiP = ruloTestKimyasalGirdiP;
	}

	public Float getRuloTestKimyasalGirdiS() {
		return ruloTestKimyasalGirdiS;
	}

	public void setRuloTestKimyasalGirdiS(Float ruloTestKimyasalGirdiS) {
		this.ruloTestKimyasalGirdiS = ruloTestKimyasalGirdiS;
	}

	public Float getRuloTestKimyasalGirdiCr() {
		return ruloTestKimyasalGirdiCr;
	}

	public void setRuloTestKimyasalGirdiCr(Float ruloTestKimyasalGirdiCr) {
		this.ruloTestKimyasalGirdiCr = ruloTestKimyasalGirdiCr;
	}

	public Float getRuloTestKimyasalGirdiNi() {
		return ruloTestKimyasalGirdiNi;
	}

	public void setRuloTestKimyasalGirdiNi(Float ruloTestKimyasalGirdiNi) {
		this.ruloTestKimyasalGirdiNi = ruloTestKimyasalGirdiNi;
	}

	public Float getRuloTestKimyasalGirdiMo() {
		return ruloTestKimyasalGirdiMo;
	}

	public void setRuloTestKimyasalGirdiMo(Float ruloTestKimyasalGirdiMo) {
		this.ruloTestKimyasalGirdiMo = ruloTestKimyasalGirdiMo;
	}

	public Float getRuloTestKimyasalGirdiCu() {
		return ruloTestKimyasalGirdiCu;
	}

	public void setRuloTestKimyasalGirdiCu(Float ruloTestKimyasalGirdiCu) {
		this.ruloTestKimyasalGirdiCu = ruloTestKimyasalGirdiCu;
	}

	public Float getRuloTestKimyasalGirdiAl() {
		return ruloTestKimyasalGirdiAl;
	}

	public void setRuloTestKimyasalGirdiAl(Float ruloTestKimyasalGirdiAl) {
		this.ruloTestKimyasalGirdiAl = ruloTestKimyasalGirdiAl;
	}

	/**
	 * @return the ruloTestKimyasalGirdiAs
	 */
	public Float getRuloTestKimyasalGirdiAs() {
		return ruloTestKimyasalGirdiAs;
	}

	/**
	 * @param ruloTestKimyasalGirdiAs
	 *            the ruloTestKimyasalGirdiAs to set
	 */
	public void setRuloTestKimyasalGirdiAs(Float ruloTestKimyasalGirdiAs) {
		this.ruloTestKimyasalGirdiAs = ruloTestKimyasalGirdiAs;
	}

	/**
	 * @return the ruloTestKimyasalGirdiSn
	 */
	public Float getRuloTestKimyasalGirdiSn() {
		return ruloTestKimyasalGirdiSn;
	}

	/**
	 * @param ruloTestKimyasalGirdiSn
	 *            the ruloTestKimyasalGirdiSn to set
	 */
	public void setRuloTestKimyasalGirdiSn(Float ruloTestKimyasalGirdiSn) {
		this.ruloTestKimyasalGirdiSn = ruloTestKimyasalGirdiSn;
	}

	/**
	 * @return the ruloTestKimyasalGirdiSb
	 */
	public Float getRuloTestKimyasalGirdiSb() {
		return ruloTestKimyasalGirdiSb;
	}

	/**
	 * @param ruloTestKimyasalGirdiSb
	 *            the ruloTestKimyasalGirdiSb to set
	 */
	public void setRuloTestKimyasalGirdiSb(Float ruloTestKimyasalGirdiSb) {
		this.ruloTestKimyasalGirdiSb = ruloTestKimyasalGirdiSb;
	}

	/**
	 * @return the ruloTestKimyasalGirdiW
	 */
	public Float getRuloTestKimyasalGirdiW() {
		return ruloTestKimyasalGirdiW;
	}

	/**
	 * @param ruloTestKimyasalGirdiW
	 *            the ruloTestKimyasalGirdiW to set
	 */
	public void setRuloTestKimyasalGirdiW(Float ruloTestKimyasalGirdiW) {
		this.ruloTestKimyasalGirdiW = ruloTestKimyasalGirdiW;
	}

	/**
	 * @return the ruloTestKimyasalGirdiPb
	 */
	public Float getRuloTestKimyasalGirdiPb() {
		return ruloTestKimyasalGirdiPb;
	}

	/**
	 * @param ruloTestKimyasalGirdiPb
	 *            the ruloTestKimyasalGirdiPb to set
	 */
	public void setRuloTestKimyasalGirdiPb(Float ruloTestKimyasalGirdiPb) {
		this.ruloTestKimyasalGirdiPb = ruloTestKimyasalGirdiPb;
	}

	/**
	 * @return the ruloTestKimyasalGirdiCa
	 */
	public Float getRuloTestKimyasalGirdiCa() {
		return ruloTestKimyasalGirdiCa;
	}

	/**
	 * @param ruloTestKimyasalGirdiCa
	 *            the ruloTestKimyasalGirdiCa to set
	 */
	public void setRuloTestKimyasalGirdiCa(Float ruloTestKimyasalGirdiCa) {
		this.ruloTestKimyasalGirdiCa = ruloTestKimyasalGirdiCa;
	}

	/**
	 * @return the siOnEk
	 */
	public String getSiOnEk() {
		return siOnEk;
	}

	/**
	 * @param siOnEk
	 *            the siOnEk to set
	 */
	public void setSiOnEk(String siOnEk) {
		this.siOnEk = siOnEk;
	}

	/**
	 * @return the wOnEk
	 */
	public String getwOnEk() {
		return wOnEk;
	}

	/**
	 * @param wOnEk
	 *            the wOnEk to set
	 */
	public void setwOnEk(String wOnEk) {
		this.wOnEk = wOnEk;
	}

	/**
	 * @return the asOnEk
	 */
	public String getAsOnEk() {
		return asOnEk;
	}

	/**
	 * @param asOnEk
	 *            the asOnEk to set
	 */
	public void setAsOnEk(String asOnEk) {
		this.asOnEk = asOnEk;
	}

	/**
	 * @return the sbOnEk
	 */
	public String getSbOnEk() {
		return sbOnEk;
	}

	/**
	 * @param sbOnEk
	 *            the sbOnEk to set
	 */
	public void setSbOnEk(String sbOnEk) {
		this.sbOnEk = sbOnEk;
	}

	/**
	 * @return the snOnEk
	 */
	public String getSnOnEk() {
		return snOnEk;
	}

	/**
	 * @param snOnEk
	 *            the snOnEk to set
	 */
	public void setSnOnEk(String snOnEk) {
		this.snOnEk = snOnEk;
	}

	/**
	 * @return the pbOnEk
	 */
	public String getPbOnEk() {
		return pbOnEk;
	}

	/**
	 * @param pbOnEk
	 *            the pbOnEk to set
	 */
	public void setPbOnEk(String pbOnEk) {
		this.pbOnEk = pbOnEk;
	}

	/**
	 * @return the caOnEk
	 */
	public String getCaOnEk() {
		return caOnEk;
	}

	/**
	 * @param caOnEk
	 *            the caOnEk to set
	 */
	public void setCaOnEk(String caOnEk) {
		this.caOnEk = caOnEk;
	}

	/**
	 * @return the nOnEk
	 */
	public String getnOnEk() {
		return nOnEk;
	}

	/**
	 * @param nOnEk
	 *            the nOnEk to set
	 */
	public void setnOnEk(String nOnEk) {
		this.nOnEk = nOnEk;
	}

}
