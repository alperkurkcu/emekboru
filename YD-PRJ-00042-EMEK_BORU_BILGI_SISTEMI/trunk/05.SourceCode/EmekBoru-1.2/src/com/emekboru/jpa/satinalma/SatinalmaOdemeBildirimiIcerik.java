package com.emekboru.jpa.satinalma;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the satinalma_odeme_bildirimi_icerik database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "SatinalmaOdemeBildirimiIcerik.findByBildirimId", query = "SELECT r FROM SatinalmaOdemeBildirimiIcerik r where r.satinalmaOdemeBildirimi.id=:prmBildirimId order by r.eklemeZamani DESC") })
@Table(name = "satinalma_odeme_bildirimi_icerik")
public class SatinalmaOdemeBildirimiIcerik implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SATINALMA_ODEME_BILDIRIMI_ICERIK_ID_GENERATOR", sequenceName = "SATINALMA_ODEME_BILDIRIMI_ICERIK_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SATINALMA_ODEME_BILDIRIMI_ICERIK_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(length = 250, name = "aciklama")
	private String aciklama;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Column(name = "fatura_no", length = 32)
	private String faturaNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fatura_tarihi")
	private Date faturaTarihi;

	@Column(name = "fatura_tutari")
	private Integer faturaTutari;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "kesinti_tutari")
	private Integer kesintiTutari;

	@Column(name = "odenecek_tutar")
	private Integer odenecekTutar;

	// @Column(name = "siparis_odeme_bildirimi_id", nullable = false)
	// private Integer siparisOdemeBildirimiId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "siparis_odeme_bildirimi_id", referencedColumnName = "ID", insertable = false)
	private SatinalmaOdemeBildirimi satinalmaOdemeBildirimi;

	@Column(name = "siparis_tutari")
	private Integer siparisTutari;

	public SatinalmaOdemeBildirimiIcerik() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public String getFaturaNo() {
		return this.faturaNo;
	}

	public void setFaturaNo(String faturaNo) {
		this.faturaNo = faturaNo;
	}

	public Integer getFaturaTutari() {
		return this.faturaTutari;
	}

	public void setFaturaTutari(Integer faturaTutari) {
		this.faturaTutari = faturaTutari;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Integer getKesintiTutari() {
		return this.kesintiTutari;
	}

	public void setKesintiTutari(Integer kesintiTutari) {
		this.kesintiTutari = kesintiTutari;
	}

	public Integer getOdenecekTutar() {
		return this.odenecekTutar;
	}

	public void setOdenecekTutar(Integer odenecekTutar) {
		this.odenecekTutar = odenecekTutar;
	}

	public Integer getSiparisTutari() {
		return this.siparisTutari;
	}

	public void setSiparisTutari(Integer siparisTutari) {
		this.siparisTutari = siparisTutari;
	}

	/**
	 * @return the satinalmaOdemeBildirimi
	 */
	public SatinalmaOdemeBildirimi getSatinalmaOdemeBildirimi() {
		return satinalmaOdemeBildirimi;
	}

	/**
	 * @param satinalmaOdemeBildirimi
	 *            the satinalmaOdemeBildirimi to set
	 */
	public void setSatinalmaOdemeBildirimi(
			SatinalmaOdemeBildirimi satinalmaOdemeBildirimi) {
		this.satinalmaOdemeBildirimi = satinalmaOdemeBildirimi;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the faturaTarihi
	 */
	public Date getFaturaTarihi() {
		return faturaTarihi;
	}

	/**
	 * @param faturaTarihi
	 *            the faturaTarihi to set
	 */
	public void setFaturaTarihi(Date faturaTarihi) {
		this.faturaTarihi = faturaTarihi;
	}

}