package com.emekboru.jpa;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="maintenance_plan_executed")
public class MaintenancePlanExecuted implements Serializable{

	private static final long serialVersionUID = 7876805779563756050L;

	@Id
	@Column(name="maintenance_plan_executed_id")
	private Integer maintenancePlanExecutedId;

	@Temporal(TemporalType.DATE)
	@Column(name="execution_date")
	private Date 	executionDay;
	
	@Column(name="title")
	private String 	title;
	
	@Column(name="short_desc")
	private String 	shortDesc;
	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "maintenance_plan_id", referencedColumnName = "maintenance_plan_id")
	private MaintenancePlan maintenancePlan;
	
	@OneToMany
	private List<MaintenanceExecutedActivities> maintenanceExecutedActivities;
	
	public MaintenancePlanExecuted(){
		
	}

	/**
	 * 	GETTERS AND SETTERS
	 */
	public Integer getMaintenancePlanExecutedId() {
		return maintenancePlanExecutedId;
	}

	public void setMaintenancePlanExecutedId(Integer maintenancePlanExecutedId) {
		this.maintenancePlanExecutedId = maintenancePlanExecutedId;
	}

	public Date getExecutionDay() {
		return executionDay;
	}

	public void setExecutionDay(Date executionDay) {
		this.executionDay = executionDay;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public MaintenancePlan getMaintenancePlan() {
		return maintenancePlan;
	}

	public void setMaintenancePlan(MaintenancePlan maintenancePlan) {
		this.maintenancePlan = maintenancePlan;
	}

	public List<MaintenanceExecutedActivities> getMaintenanceExecutedActivities() {
		return maintenanceExecutedActivities;
	}

	public void setMaintenanceExecutedActivities(
			List<MaintenanceExecutedActivities> maintenanceExecutedActivities) {
		this.maintenanceExecutedActivities = maintenanceExecutedActivities;
	}
	
}
