package com.emekboru.jpa.tahribatsiztestsonuc;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.kalibrasyon.KalibrasyonManyetik;
import com.emekboru.jpa.rulo.RuloPipeLink;

/**
 * The persistent class for the test_tahribatsiz_manyetik_parcacik_sonuc
 * database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestTahribatsizManyetikParcacikSonuc.findAll", query = "SELECT r FROM TestTahribatsizManyetikParcacikSonuc r WHERE r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_tahribatsiz_manyetik_parcacik_sonuc")
public class TestTahribatsizManyetikParcacikSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_TAHRIBATSIZ_MANYETIK_PARCACIK_SONUC_ID_GENERATOR", sequenceName = "TEST_TAHRIBATSIZ_MANYETIK_PARCACIK_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_TAHRIBATSIZ_MANYETIK_PARCACIK_SONUC_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "koordinat_l", nullable = false)
	private Integer koordinatL;

	@Column(name = "koordinat_q", nullable = false)
	private Integer koordinatQ;

	@Column(length = 50)
	private String note;

	// @Column(name = "pipe_id", nullable = false)
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	private Integer sonuc;

	@Column(name = "sureksizlik_tanimi")
	private Integer sureksizlikTanimi;

	@Column(name = "tamir_sonrasi_durum")
	private Integer tamirSonrasiDurum;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	private RuloPipeLink ruloPipeLink;

	// @Column(name = "kalibrasyon_id")
	// private Integer kalibrasyonId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "kalibrasyon_id", referencedColumnName = "ID", insertable = false)
	private KalibrasyonManyetik kalibrasyon;

	@Column(name = "hata_tipi")
	private Integer hataTipi;

	public TestTahribatsizManyetikParcacikSonuc() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getKoordinatL() {
		return this.koordinatL;
	}

	public void setKoordinatL(Integer koordinatL) {
		this.koordinatL = koordinatL;
	}

	public Integer getKoordinatQ() {
		return this.koordinatQ;
	}

	public void setKoordinatQ(Integer koordinatQ) {
		this.koordinatQ = koordinatQ;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Integer getSonuc() {
		return this.sonuc;
	}

	public void setSonuc(Integer sonuc) {
		this.sonuc = sonuc;
	}

	public Integer getSureksizlikTanimi() {
		return this.sureksizlikTanimi;
	}

	public void setSureksizlikTanimi(Integer sureksizlikTanimi) {
		this.sureksizlikTanimi = sureksizlikTanimi;
	}

	public Integer getTamirSonrasiDurum() {
		return this.tamirSonrasiDurum;
	}

	public void setTamirSonrasiDurum(Integer tamirSonrasiDurum) {
		this.tamirSonrasiDurum = tamirSonrasiDurum;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the ruloPipeLink
	 */
	public RuloPipeLink getRuloPipeLink() {
		return ruloPipeLink;
	}

	/**
	 * @param ruloPipeLink
	 *            the ruloPipeLink to set
	 */
	public void setRuloPipeLink(RuloPipeLink ruloPipeLink) {
		this.ruloPipeLink = ruloPipeLink;
	}

	/**
	 * @return the kalibrasyon
	 */
	public KalibrasyonManyetik getKalibrasyon() {
		return kalibrasyon;
	}

	/**
	 * @param kalibrasyon
	 *            the kalibrasyon to set
	 */
	public void setKalibrasyon(KalibrasyonManyetik kalibrasyon) {
		this.kalibrasyon = kalibrasyon;
	}

	/**
	 * @return the hataTipi
	 */
	public Integer getHataTipi() {
		return hataTipi;
	}

	/**
	 * @param hataTipi
	 *            the hataTipi to set
	 */
	public void setHataTipi(Integer hataTipi) {
		this.hataTipi = hataTipi;
	}

}