package com.emekboru.jpa;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the currency database table.
 * 
 */
@Entity
@Table(name = "currency")
public class Currency implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "currency_code")
	private String currencyCode;

	@Column(name = "name")
	private String name;

	@Column(name = "is_default")
	private boolean isDefault;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "currency")
	private List<EventCostBreakdown> eventCostBreakdown;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "currency")
	private List<CoatingCost> coatingCosts;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "currency")
	private List<TransportationCost> transportationCosts;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "currency")
	private List<OtherCostItem> otherCostItems;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "currency")
	private List<SoldEdw> soldEdws;

	public Currency() {
	}

	/**
	 * GETTERS AND SETTERS
	 */
	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public List<EventCostBreakdown> getEventCostBreakdown() {
		return eventCostBreakdown;
	}

	public void setEventCostBreakdown(
			List<EventCostBreakdown> eventCostBreakdown) {
		this.eventCostBreakdown = eventCostBreakdown;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<CoatingCost> getCoatingCosts() {
		return coatingCosts;
	}

	public void setCoatingCosts(List<CoatingCost> coatingCosts) {
		this.coatingCosts = coatingCosts;
	}

	public List<TransportationCost> getTransportationCosts() {
		return transportationCosts;
	}

	public void setTransportationCosts(
			List<TransportationCost> transportationCosts) {
		this.transportationCosts = transportationCosts;
	}

	public List<OtherCostItem> getOtherCostItems() {
		return otherCostItems;
	}

	public void setOtherCostItems(List<OtherCostItem> otherCostItems) {
		this.otherCostItems = otherCostItems;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}

	public List<SoldEdw> getSoldEdws() {
		return soldEdws;
	}

	public void setSoldEdws(List<SoldEdw> soldEdws) {
		this.soldEdws = soldEdws;
	}

	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}
}