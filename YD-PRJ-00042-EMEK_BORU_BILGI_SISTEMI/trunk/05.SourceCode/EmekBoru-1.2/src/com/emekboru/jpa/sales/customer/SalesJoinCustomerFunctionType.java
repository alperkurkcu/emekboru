package com.emekboru.jpa.sales.customer;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "sales_join_customer_function_type")
public class SalesJoinCustomerFunctionType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6281036240459861414L;

	@Id
	@Column(name = "join_id")
	@SequenceGenerator(name = "SALES_JOIN_CUSTOMER_FUNCTION_TYPE_GENERATOR", sequenceName = "SALES_JOIN_CUSTOMER_FUNCTION_TYPE_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_JOIN_CUSTOMER_FUNCTION_TYPE_GENERATOR")
	private int joinId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "function_id", referencedColumnName = "function_id", updatable = false)
	private SalesCustomerFunctionType functionType;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "customer_id", referencedColumnName = "sales_customer_id", updatable = false)
	private SalesCustomer customer;

	public SalesJoinCustomerFunctionType() {
		functionType = new SalesCustomerFunctionType();
		customer = new SalesCustomer();
	}

	/**
	 * GETTERS AND SETTERS
	 */

	public int getJoinId() {
		return joinId;
	}

	public SalesCustomerFunctionType getFunctionType() {
		return functionType;
	}

	public SalesCustomer getCustomer() {
		return customer;
	}

	public void setJoinId(Integer joinId) {
		this.joinId = joinId;
	}

	public void setFunctionType(SalesCustomerFunctionType functionType) {
		this.functionType = functionType;
	}

	public void setCustomer(SalesCustomer customer) {
		this.customer = customer;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
