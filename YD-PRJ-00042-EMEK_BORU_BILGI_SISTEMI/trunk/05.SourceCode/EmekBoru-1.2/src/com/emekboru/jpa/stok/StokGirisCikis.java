package com.emekboru.jpa.stok;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.emekboru.jpa.Employee;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpa.satinalma.SatinalmaAmbarMalzemeTalepForm;
import com.emekboru.jpa.satinalma.SatinalmaMalzemeHizmetTalepFormu;
import com.emekboru.jpa.satinalma.SatinalmaSarfMerkezi;

/**
 * The persistent class for the stok_giris_cikis database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "StokGirisCikis.findAll", query = "SELECT r FROM StokGirisCikis r ORDER BY r.id DESC"),
		@NamedQuery(name = "StokGirisCikis.findByUrunId", query = "SELECT s FROM StokGirisCikis s WHERE s.stokUrunKartlari.id=:prmUrunId"),
		// @NamedQuery(name = "StokGirisCikis.findCikis", query =
		// " SELECT SUM(s.miktar) FROM StokGirisCikis s WHERE s.stokUrunKartlari.id=:prmUrunId AND s.islemDurumu=0 GROUP BY s.islemDurumu"),
		@NamedQuery(name = "StokGirisCikis.findGirisJoinId", query = " SELECT SUM(s.miktar) FROM StokGirisCikis s WHERE s.stokJoinUreticiKoduBarkod.joinId=:prmJoinId AND s.islemDurumu=1 GROUP BY s.islemDurumu"),
		@NamedQuery(name = "StokGirisCikis.findCikisJoinId", query = " SELECT SUM(s.miktar) FROM StokGirisCikis s WHERE s.stokJoinUreticiKoduBarkod.joinId=:prmJoinId AND s.islemDurumu=0 GROUP BY s.islemDurumu"),
		@NamedQuery(name = "StokGirisCikis.kritikStok", query = "select s, SUM(s.miktar) FROM StokGirisCikis s, StokUrunKartlari k WHERE s.stokUrunKartlari.id = k.id AND s.islemDurumu = TRUE GROUP BY s, k.id, k.minStok, k.maxStok ORDER BY k.id"),
		@NamedQuery(name = "StokGirisCikis.urunGirisCikisMktari", query = "SELECT SUM(s.miktar) FROM StokGirisCikis s WHERE s.stokUrunKartlari.id=:urunKartiId AND s.islemDurumu =:islemDurumu"),
		@NamedQuery(name = "StokGirisCikis.totalAll", query = "SELECT COUNT(o) FROM StokGirisCikis o") })
@Table(name = "stok_giris_cikis")
public class StokGirisCikis implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "STOK_GIRIS_CIKIS_ID_GENERATOR", sequenceName = "STOK_GIRIS_CIKIS_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STOK_GIRIS_CIKIS_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	// @Column(nullable = false, name = "birim")
	// private Integer birim;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "birim", referencedColumnName = "ID", insertable = false)
	private StokTanimlarBirim stokTanimlarBirim;

	// @Column(nullable = false)
	// private Integer depo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "depo", referencedColumnName = "ID", insertable = false)
	private StokTanimlarDepoTanimlari stokTanimlarDepoTanimlari;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "islem_durumu")
	private String islemDurumu;

	@Column(nullable = false, name = "miktar")
	private Integer miktar;

	// @Column(name = "sarf_yeri", nullable = false)
	// private Integer sarfYeri;

	// @ManyToOne(fetch = FetchType.EAGER)
	// @JoinColumn(name = "sarf_yeri", referencedColumnName = "ID", insertable =
	// false)
	// private StokTanimlarSarfYeri stokTanimlarSarfYeri;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sarf_yeri", referencedColumnName = "id", insertable = false)
	private SatinalmaSarfMerkezi satinalmaSarfMerkezi;

	// @Column(name = "teslim_alan_kullanici", nullable = false)
	// private Integer teslimAlanKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "teslim_alan_kullanici", referencedColumnName = "employee_id", insertable = false)
	private Employee teslimAlanKullanici;

	// @Column(name = "teslim_eden_kullanici", nullable = false)
	// private Integer teslimEdenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "teslim_eden_kullanici", referencedColumnName = "employee_id", insertable = false)
	private Employee teslimEdenKullanici;

	// @Column(name = "urun_id", nullable = false)
	// private Integer urunId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "urun_id", referencedColumnName = "ID", insertable = false)
	private StokUrunKartlari stokUrunKartlari;

	@Transient
	private Integer depoDurumu = 0;

	// @Column(name = "proje_kodu")
	// private Integer projeKodu;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "proje_kodu", referencedColumnName = "item_id", insertable = false)
	private SalesItem salesItem;

	// @Column(name = "evrak_no")
	// private Integer evrakNo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "evrak_no", referencedColumnName = "ID", insertable = false)
	private SatinalmaAmbarMalzemeTalepForm satinalmaAmbarMalzemeTalepForm;

	@Transient
	private Date sorguIlkTarih;

	@Transient
	private Date sorguSonTarih;

	// @Column(name = "urun_join_id", nullable = false)
	// private Integer urunJoinId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "urun_join_id", referencedColumnName = "join_id", insertable = false)
	private StokJoinUreticiKoduBarkod stokJoinUreticiKoduBarkod;

	// @Column(name = "malzeme_talep_id")
	// private Integer malzemeTalepId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "malzeme_talep_id", referencedColumnName = "ID", insertable = false)
	private SatinalmaMalzemeHizmetTalepFormu satinalmaMalzemeHizmetTalepFormu;

	@Column(name = "irsaliye_no")
	private String irsaliyeNo;

	@Temporal(TemporalType.DATE)
	@Column(name = "irsaliye_tarihi")
	private Date irsaliyeTarihi;

	@Column(name = "aciklama")
	private String aciklama;

	@Transient
	public static final int GIRIS_ISLEMI = 1;
	@Transient
	public static final int CIKIS_ISLEMI = 0;
	@Transient
	public static final int IADE_ISLEMI = 2;
	@Transient
	public static final int SATIS_ISLEMI = 3;
	@Transient
	public static final int ODUNC_ISLEMI = 4;

	public StokGirisCikis() {

		stokTanimlarBirim = new StokTanimlarBirim();
		stokTanimlarDepoTanimlari = new StokTanimlarDepoTanimlari();
		// stokTanimlarSarfYeri = new StokTanimlarSarfYeri();
		satinalmaSarfMerkezi = new SatinalmaSarfMerkezi();
		stokUrunKartlari = new StokUrunKartlari();
		stokJoinUreticiKoduBarkod = new StokJoinUreticiKoduBarkod();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	/**
	 * @return the islemDurumu
	 */
	public String getIslemDurumu() {
		return islemDurumu;
	}

	/**
	 * @param islemDurumu
	 *            the islemDurumu to set
	 */
	public void setIslemDurumu(String islemDurumu) {
		this.islemDurumu = islemDurumu;
	}

	public Integer getMiktar() {
		return this.miktar;
	}

	public void setMiktar(Integer miktar) {
		this.miktar = miktar;
	}

	public StokTanimlarBirim getStokTanimlarBirim() {
		return stokTanimlarBirim;
	}

	public void setStokTanimlarBirim(StokTanimlarBirim stokTanimlarBirim) {
		this.stokTanimlarBirim = stokTanimlarBirim;
	}

	public StokTanimlarDepoTanimlari getStokTanimlarDepoTanimlari() {
		return stokTanimlarDepoTanimlari;
	}

	public void setStokTanimlarDepoTanimlari(
			StokTanimlarDepoTanimlari stokTanimlarDepoTanimlari) {
		this.stokTanimlarDepoTanimlari = stokTanimlarDepoTanimlari;
	}

	public Employee getTeslimAlanKullanici() {
		return teslimAlanKullanici;
	}

	public void setTeslimAlanKullanici(Employee teslimAlanKullanici) {
		this.teslimAlanKullanici = teslimAlanKullanici;
	}

	public Employee getTeslimEdenKullanici() {
		return teslimEdenKullanici;
	}

	public void setTeslimEdenKullanici(Employee teslimEdenKullanici) {
		this.teslimEdenKullanici = teslimEdenKullanici;
	}

	public Integer getDepoDurumu() {
		return depoDurumu;
	}

	public void setDepoDurumu(Integer depoDurumu) {
		this.depoDurumu = depoDurumu;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public SatinalmaAmbarMalzemeTalepForm getSatinalmaAmbarMalzemeTalepForm() {
		return satinalmaAmbarMalzemeTalepForm;
	}

	public void setSatinalmaAmbarMalzemeTalepForm(
			SatinalmaAmbarMalzemeTalepForm satinalmaAmbarMalzemeTalepForm) {
		this.satinalmaAmbarMalzemeTalepForm = satinalmaAmbarMalzemeTalepForm;
	}

	public StokJoinUreticiKoduBarkod getStokJoinUreticiKoduBarkod() {
		return stokJoinUreticiKoduBarkod;
	}

	public void setStokJoinUreticiKoduBarkod(
			StokJoinUreticiKoduBarkod stokJoinUreticiKoduBarkod) {
		this.stokJoinUreticiKoduBarkod = stokJoinUreticiKoduBarkod;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public StokUrunKartlari getStokUrunKartlari() {
		return stokUrunKartlari;
	}

	public void setStokUrunKartlari(StokUrunKartlari stokUrunKartlari) {
		this.stokUrunKartlari = stokUrunKartlari;
	}

	public SatinalmaMalzemeHizmetTalepFormu getSatinalmaMalzemeHizmetTalepFormu() {
		return satinalmaMalzemeHizmetTalepFormu;
	}

	public void setSatinalmaMalzemeHizmetTalepFormu(
			SatinalmaMalzemeHizmetTalepFormu satinalmaMalzemeHizmetTalepFormu) {
		this.satinalmaMalzemeHizmetTalepFormu = satinalmaMalzemeHizmetTalepFormu;
	}

	/**
	 * @return the satinalmaSarfMerkezi
	 */
	public SatinalmaSarfMerkezi getSatinalmaSarfMerkezi() {
		return satinalmaSarfMerkezi;
	}

	/**
	 * @param satinalmaSarfMerkezi
	 *            the satinalmaSarfMerkezi to set
	 */
	public void setSatinalmaSarfMerkezi(
			SatinalmaSarfMerkezi satinalmaSarfMerkezi) {
		this.satinalmaSarfMerkezi = satinalmaSarfMerkezi;
	}

	/**
	 * @return the sorguIlkTarih
	 */
	public Date getSorguIlkTarih() {
		return sorguIlkTarih;
	}

	/**
	 * @param sorguIlkTarih
	 *            the sorguIlkTarih to set
	 */
	public void setSorguIlkTarih(Date sorguIlkTarih) {
		this.sorguIlkTarih = sorguIlkTarih;
	}

	/**
	 * @return the sorguSonTarih
	 */
	public Date getSorguSonTarih() {
		return sorguSonTarih;
	}

	/**
	 * @param sorguSonTarih
	 *            the sorguSonTarih to set
	 */
	public void setSorguSonTarih(Date sorguSonTarih) {
		this.sorguSonTarih = sorguSonTarih;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public String getIrsaliyeNo() {
		return irsaliyeNo;
	}

	public void setIrsaliyeNo(String irsaliyeNo) {
		this.irsaliyeNo = irsaliyeNo;
	}

	public Date getIrsaliyeTarihi() {
		return irsaliyeTarihi;
	}

	public void setIrsaliyeTarihi(Date irsaliyeTarihi) {
		this.irsaliyeTarihi = irsaliyeTarihi;
	}

}