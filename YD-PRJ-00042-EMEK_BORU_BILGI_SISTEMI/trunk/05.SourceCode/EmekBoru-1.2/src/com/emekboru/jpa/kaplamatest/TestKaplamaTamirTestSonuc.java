package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.isolation.IsolationTestDefinition;

/**
 * The persistent class for the test_kaplama_tamir_test_sonuc database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaTamirTestSonuc.findAll", query = "SELECT r FROM TestKaplamaTamirTestSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_kaplama_tamir_test_sonuc")
public class TestKaplamaTamirTestSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KAPLAMA_TAMIR_TEST_SONUC_ID_GENERATOR", sequenceName = "TEST_KAPLAMA_TAMIR_TEST_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KAPLAMA_TAMIR_TEST_SONUC_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ekleme_zamani")
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	// @Column(name="global_id")
	// private Integer globalId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "olmasi_gereken_tamirat_sayisi")
	private String olmasiGerekenTamiratSayisi;

	// @Column(name="pipe_id")
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;

	private Boolean sonuc;

	@Column(name = "tamirat_alani")
	private String tamiratAlani;

	@Column(name = "tamirat_yontemi")
	private Boolean tamiratYontemi;

	@Column(name = "tamirati_yapan")
	private Integer tamiratiYapan;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi")
	private Date testTarihi;

	@Column(name = "var_olan_tamirat_sayisi")
	private String varOlanTamiratSayisi;

	@Column(name = "vardiya")
	private Integer vardiya;

	@Column(name = "holiday_kontrol")
	private String holidayKontrol;

	@Column(name = "holiday_kv")
	private String holidayKv;

	@Column(name = "wet_sponge_kontrol")
	private String wetSpongeKontrol;

	@Column(name = "wet_sponge_kv")
	private String wetSpongeKv;

	@Column(name = "kalinlik_kontrol")
	private Integer kalinlikKontrol;

	@Column(name = "itp_istenilen_alan_adet")
	private String itpIstenilenAlanAdet;

	@Column(name = "remarks")
	private String remarks;

	@Column(name = "stripping_temp")
	private String strippingTemp;

	public TestKaplamaTamirTestSonuc() {
	}

	/**
	 * @return the bagliGlobalId
	 */
	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}

	/**
	 * @param bagliGlobalId
	 *            the bagliGlobalId to set
	 */
	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Boolean getSonuc() {
		return this.sonuc;
	}

	public void setSonuc(Boolean sonuc) {
		this.sonuc = sonuc;
	}

	public Boolean getTamiratYontemi() {
		return this.tamiratYontemi;
	}

	public void setTamiratYontemi(Boolean tamiratYontemi) {
		this.tamiratYontemi = tamiratYontemi;
	}

	public Integer getTamiratiYapan() {
		return this.tamiratiYapan;
	}

	public void setTamiratiYapan(Integer tamiratiYapan) {
		this.tamiratiYapan = tamiratiYapan;
	}

	public Date getTestTarihi() {
		return this.testTarihi;
	}

	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	public Integer getVardiya() {
		return this.vardiya;
	}

	public void setVardiya(Integer vardiya) {
		this.vardiya = vardiya;
	}

	/**
	 * @return the bagliTestId
	 */
	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the holidayKontrol
	 */
	public String getHolidayKontrol() {
		return holidayKontrol;
	}

	/**
	 * @param holidayKontrol
	 *            the holidayKontrol to set
	 */
	public void setHolidayKontrol(String holidayKontrol) {
		this.holidayKontrol = holidayKontrol;
	}

	/**
	 * @return the holidayKv
	 */
	public String getHolidayKv() {
		return holidayKv;
	}

	/**
	 * @param holidayKv
	 *            the holidayKv to set
	 */
	public void setHolidayKv(String holidayKv) {
		this.holidayKv = holidayKv;
	}

	/**
	 * @return the wetSpongeKontrol
	 */
	public String getWetSpongeKontrol() {
		return wetSpongeKontrol;
	}

	/**
	 * @param wetSpongeKontrol
	 *            the wetSpongeKontrol to set
	 */
	public void setWetSpongeKontrol(String wetSpongeKontrol) {
		this.wetSpongeKontrol = wetSpongeKontrol;
	}

	/**
	 * @return the wetSpongeKv
	 */
	public String getWetSpongeKv() {
		return wetSpongeKv;
	}

	/**
	 * @param wetSpongeKv
	 *            the wetSpongeKv to set
	 */
	public void setWetSpongeKv(String wetSpongeKv) {
		this.wetSpongeKv = wetSpongeKv;
	}

	/**
	 * @return the kalinlikKontrol
	 */
	public Integer getKalinlikKontrol() {
		return kalinlikKontrol;
	}

	/**
	 * @param kalinlikKontrol
	 *            the kalinlikKontrol to set
	 */
	public void setKalinlikKontrol(Integer kalinlikKontrol) {
		this.kalinlikKontrol = kalinlikKontrol;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the olmasiGerekenTamiratSayisi
	 */
	public String getOlmasiGerekenTamiratSayisi() {
		return olmasiGerekenTamiratSayisi;
	}

	/**
	 * @param olmasiGerekenTamiratSayisi
	 *            the olmasiGerekenTamiratSayisi to set
	 */
	public void setOlmasiGerekenTamiratSayisi(String olmasiGerekenTamiratSayisi) {
		this.olmasiGerekenTamiratSayisi = olmasiGerekenTamiratSayisi;
	}

	/**
	 * @return the tamiratAlani
	 */
	public String getTamiratAlani() {
		return tamiratAlani;
	}

	/**
	 * @param tamiratAlani
	 *            the tamiratAlani to set
	 */
	public void setTamiratAlani(String tamiratAlani) {
		this.tamiratAlani = tamiratAlani;
	}

	/**
	 * @return the varOlanTamiratSayisi
	 */
	public String getVarOlanTamiratSayisi() {
		return varOlanTamiratSayisi;
	}

	/**
	 * @param varOlanTamiratSayisi
	 *            the varOlanTamiratSayisi to set
	 */
	public void setVarOlanTamiratSayisi(String varOlanTamiratSayisi) {
		this.varOlanTamiratSayisi = varOlanTamiratSayisi;
	}

	/**
	 * @return the itpIstenilenAlanAdet
	 */
	public String getItpIstenilenAlanAdet() {
		return itpIstenilenAlanAdet;
	}

	/**
	 * @param itpIstenilenAlanAdet
	 *            the itpIstenilenAlanAdet to set
	 */
	public void setItpIstenilenAlanAdet(String itpIstenilenAlanAdet) {
		this.itpIstenilenAlanAdet = itpIstenilenAlanAdet;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks
	 *            the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the strippingTemp
	 */
	public String getStrippingTemp() {
		return strippingTemp;
	}

	/**
	 * @param strippingTemp
	 *            the strippingTemp to set
	 */
	public void setStrippingTemp(String strippingTemp) {
		this.strippingTemp = strippingTemp;
	}

}