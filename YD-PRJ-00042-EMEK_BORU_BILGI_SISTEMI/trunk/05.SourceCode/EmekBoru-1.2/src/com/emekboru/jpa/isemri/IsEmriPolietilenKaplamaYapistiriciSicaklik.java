package com.emekboru.jpa.isemri;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the is_emri_polietilen_kaplama_yapistirici_sicaklik
 * database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "IsEmriPolietilenKaplamaYapistiriciSicaklik.findAllByIsEmriId", query = "SELECT r FROM IsEmriPolietilenKaplamaYapistiriciSicaklik r WHERE r.isEmriPolietilenKaplama.id=:prmIsEmriId order by r.eklemeZamani") })
@Table(name = "is_emri_polietilen_kaplama_yapistirici_sicaklik")
public class IsEmriPolietilenKaplamaYapistiriciSicaklik implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IS_EMRI_POLIETILEN_KAPLAMA_YAPISTIRICI_SICAKLIK_ID_GENERATOR", sequenceName = "IS_EMRI_POLIETILEN_KAPLAMA_YAPISTIRICI_SICAKLIK_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IS_EMRI_POLIETILEN_KAPLAMA_YAPISTIRICI_SICAKLIK_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", insertable = false, updatable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "govde_alti")
	private Integer govdeAlti;

	@Column(name = "govde_bes")
	private Integer govdeBes;

	@Column(name = "govde_bir")
	private Integer govdeBir;

	@Column(name = "govde_dort")
	private Integer govdeDort;

	@Column(name = "govde_iki")
	private Integer govdeIki;

	@Column(name = "govde_uc")
	private Integer govdeUc;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Basic
	@Column(name = "is_emri_id", nullable = false, insertable = false, updatable = false)
	private Integer isEmriId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "is_emri_id", referencedColumnName = "ID", insertable = false)
	private IsEmriPolietilenKaplama isEmriPolietilenKaplama;

	@Column(name = "kafa_bir")
	private Integer kafaBir;

	public IsEmriPolietilenKaplamaYapistiriciSicaklik() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getGovdeAlti() {
		return this.govdeAlti;
	}

	public void setGovdeAlti(Integer govdeAlti) {
		this.govdeAlti = govdeAlti;
	}

	public Integer getGovdeBes() {
		return this.govdeBes;
	}

	public void setGovdeBes(Integer govdeBes) {
		this.govdeBes = govdeBes;
	}

	public Integer getGovdeBir() {
		return this.govdeBir;
	}

	public void setGovdeBir(Integer govdeBir) {
		this.govdeBir = govdeBir;
	}

	public Integer getGovdeDort() {
		return this.govdeDort;
	}

	public void setGovdeDort(Integer govdeDort) {
		this.govdeDort = govdeDort;
	}

	public Integer getGovdeIki() {
		return this.govdeIki;
	}

	public void setGovdeIki(Integer govdeIki) {
		this.govdeIki = govdeIki;
	}

	public Integer getGovdeUc() {
		return this.govdeUc;
	}

	public void setGovdeUc(Integer govdeUc) {
		this.govdeUc = govdeUc;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getKafaBir() {
		return this.kafaBir;
	}

	public void setKafaBir(Integer kafaBir) {
		this.kafaBir = kafaBir;
	}

	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	public IsEmriPolietilenKaplama getIsEmriPolietilenKaplama() {
		return isEmriPolietilenKaplama;
	}

	public void setIsEmriPolietilenKaplama(
			IsEmriPolietilenKaplama isEmriPolietilenKaplama) {
		this.isEmriPolietilenKaplama = isEmriPolietilenKaplama;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}