package com.emekboru.jpa.tahribatsiztestsonuc;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.kalibrasyon.KalibrasyonRadyografik;
import com.emekboru.jpa.rulo.RuloPipeLink;

/**
 * The persistent class for the test_tahribatsiz_radyografik_sonuc database
 * table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestTahribatsizRadyografikSonuc.findAll", query = "SELECT r FROM TestTahribatsizRadyografikSonuc r WHERE r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_tahribatsiz_radyografik_sonuc")
public class TestTahribatsizRadyografikSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_TAHRIBATSIZ_RADYOGRAFIK_SONUC_ID_GENERATOR", sequenceName = "TEST_TAHRIBATSIZ_RADYOGRAFIK_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_TAHRIBATSIZ_RADYOGRAFIK_SONUC_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	private Boolean aa;

	private Boolean ab;

	private Boolean ad;

	private Boolean ba;

	private Boolean bb;

	private Boolean c;

	private Boolean d;

	private Boolean e;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	private Boolean f;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	private Boolean h;

	@Column(name = "kaynakci_no")
	private Integer kaynakciNo;

	@Column(length = 50)
	private String note;

	// @Column(name = "pipe_id", nullable = false)
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	private Integer sonuc;

	@Column(name = "tamir_sonrasi_durum", nullable = false)
	private Integer tamirSonrasiDurum;

	@Column(name = "koordinat_q")
	private Integer koordinatQ;

	@Column(name = "koordinat_l")
	private Integer koordinatL;

	@Column(name = "test_id")
	private Integer testId;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	private RuloPipeLink ruloPipeLink;

	// @Column(name = "kalibrasyon_id")
	// private Integer kalibrasyonId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "kalibrasyon_id", referencedColumnName = "ID", insertable = false)
	private KalibrasyonRadyografik kalibrasyon;

	public TestTahribatsizRadyografikSonuc() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getAa() {
		return this.aa;
	}

	public void setAa(Boolean aa) {
		this.aa = aa;
	}

	public Boolean getAb() {
		return this.ab;
	}

	public void setAb(Boolean ab) {
		this.ab = ab;
	}

	public Boolean getAd() {
		return this.ad;
	}

	public void setAd(Boolean ad) {
		this.ad = ad;
	}

	public Boolean getBa() {
		return this.ba;
	}

	public void setBa(Boolean ba) {
		this.ba = ba;
	}

	public Boolean getBb() {
		return this.bb;
	}

	public void setBb(Boolean bb) {
		this.bb = bb;
	}

	public Boolean getC() {
		return this.c;
	}

	public void setC(Boolean c) {
		this.c = c;
	}

	public Boolean getD() {
		return this.d;
	}

	public void setD(Boolean d) {
		this.d = d;
	}

	public Boolean getE() {
		return this.e;
	}

	public void setE(Boolean e) {
		this.e = e;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Boolean getF() {
		return this.f;
	}

	public void setF(Boolean f) {
		this.f = f;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Boolean getH() {
		return this.h;
	}

	public void setH(Boolean h) {
		this.h = h;
	}

	public Integer getKaynakciNo() {
		return this.kaynakciNo;
	}

	public void setKaynakciNo(Integer kaynakciNo) {
		this.kaynakciNo = kaynakciNo;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the sonuc
	 */
	public Integer getSonuc() {
		return sonuc;
	}

	/**
	 * @param sonuc
	 *            the sonuc to set
	 */
	public void setSonuc(Integer sonuc) {
		this.sonuc = sonuc;
	}

	/**
	 * @return the tamirSonrasiDurum
	 */
	public Integer getTamirSonrasiDurum() {
		return tamirSonrasiDurum;
	}

	/**
	 * @param tamirSonrasiDurum
	 *            the tamirSonrasiDurum to set
	 */
	public void setTamirSonrasiDurum(Integer tamirSonrasiDurum) {
		this.tamirSonrasiDurum = tamirSonrasiDurum;
	}

	/**
	 * @return the koordinatQ
	 */
	public Integer getKoordinatQ() {
		return koordinatQ;
	}

	/**
	 * @param koordinatQ
	 *            the koordinatQ to set
	 */
	public void setKoordinatQ(Integer koordinatQ) {
		this.koordinatQ = koordinatQ;
	}

	/**
	 * @return the koordinatL
	 */
	public Integer getKoordinatL() {
		return koordinatL;
	}

	/**
	 * @param koordinatL
	 *            the koordinatL to set
	 */
	public void setKoordinatL(Integer koordinatL) {
		this.koordinatL = koordinatL;
	}

	/**
	 * @return the testId
	 */
	public Integer getTestId() {
		return testId;
	}

	/**
	 * @param testId
	 *            the testId to set
	 */
	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	/**
	 * @return the ruloPipeLink
	 */
	public RuloPipeLink getRuloPipeLink() {
		return ruloPipeLink;
	}

	/**
	 * @param ruloPipeLink
	 *            the ruloPipeLink to set
	 */
	public void setRuloPipeLink(RuloPipeLink ruloPipeLink) {
		this.ruloPipeLink = ruloPipeLink;
	}

	/**
	 * @return the kalibrasyon
	 */
	public KalibrasyonRadyografik getKalibrasyon() {
		return kalibrasyon;
	}

	/**
	 * @param kalibrasyon
	 *            the kalibrasyon to set
	 */
	public void setKalibrasyon(KalibrasyonRadyografik kalibrasyon) {
		this.kalibrasyon = kalibrasyon;
	}

}