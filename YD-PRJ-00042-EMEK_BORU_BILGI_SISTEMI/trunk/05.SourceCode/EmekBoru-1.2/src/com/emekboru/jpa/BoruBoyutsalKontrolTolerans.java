package com.emekboru.jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the boru_boyutsal_kontrol_tolerans database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "BoruBoyutsalKontrolTolerans.seciliBoyutsalKontrol", query = "SELECT r FROM BoruBoyutsalKontrolTolerans r WHERE r.salesItem.itemId=:prmItemId") })
@Table(name = "boru_boyutsal_kontrol_tolerans")
public class BoruBoyutsalKontrolTolerans implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "BORU_BOYUTSAL_KONTROL_TOLERANS_ID_GENERATOR", sequenceName = "BORU_BOYUTSAL_KONTROL_TOLERANS_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BORU_BOYUTSAL_KONTROL_TOLERANS_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Column(name = "a_ucu_kaynak_agizi_acisi_toleransi_max")
	private Float aUcuKaynakAgiziAcisiToleransiMax;

	@Column(name = "a_ucu_kaynak_agizi_acisi_toleransi_min")
	private Float aUcuKaynakAgiziAcisiToleransiMin;

	@Column(name = "a_ucu_kaynak_agizi_acisi_toleransi_nominal")
	private Float aUcuKaynakAgiziAcisiToleransiNominal;

	@Column(name = "a_ucu_kok_yuzey_olcusu_max")
	private Float aUcuKokYuzeyOlcusuMax;

	@Column(name = "a_ucu_kok_yuzey_olcusu_min")
	private Float aUcuKokYuzeyOlcusuMin;

	@Column(name = "a_ucu_kok_yuzey_olcusu_nominal")
	private Float aUcuKokYuzeyOlcusuNominal;

	@Column(name = "b_ucu_kaynak_agizi_acisi_toleransi_max")
	private Float bUcuKaynakAgiziAcisiToleransiMax;

	@Column(name = "b_ucu_kaynak_agizi_acisi_toleransi_min")
	private Float bUcuKaynakAgiziAcisiToleransiMin;

	@Column(name = "b_ucu_kaynak_agizi_acisi_toleransi_nominal")
	private Float bUcuKaynakAgiziAcisiToleransiNominal;

	@Column(name = "b_ucu_kok_yuzey_olcusu_max")
	private Float bUcuKokYuzeyOlcusuMax;

	@Column(name = "b_ucu_kok_yuzey_olcusu_min")
	private Float bUcuKokYuzeyOlcusuMin;

	@Column(name = "b_ucu_kok_yuzey_olcusu_nominal")
	private Float bUcuKokYuzeyOlcusuNominal;

	@Column(name = "baglanti_sekli", length = 255)
	private String baglantiSekli;

	@Column(name = "boru_agirligi_toleransi_max")
	private Float boruAgirligiToleransiMax;

	@Column(name = "boru_agirligi_toleransi_min")
	private Float boruAgirligiToleransiMin;

	@Column(name = "boru_govdesi_dis_cap_toleransi_max")
	private Float boruGovdesiDisCapToleransiMax;

	@Column(name = "boru_govdesi_dis_cap_toleransi_min")
	private Float boruGovdesiDisCapToleransiMin;

	@Column(name = "boru_govdesi_dis_cap_toleransi_nominal")
	private Float boruGovdesiDisCapToleransiNominal;

	@Column(name = "boru_govdesi_ovallik_toleransi_max")
	private Float boruGovdesiOvallikToleransiMax;

	@Column(name = "boru_ucu_cap_toleransi_max")
	private Float boruUcuCapToleransiMax;

	@Column(name = "boru_ucu_cap_toleransi_min")
	private Float boruUcuCapToleransiMin;

	@Column(name = "boru_ucu_cap_toleransi_nominal")
	private Float boruUcuCapToleransiNominal;

	@Column(name = "boru_ucu_dikligi_toleransi_max")
	private Float boruUcuDikligiToleransiMax;

	@Column(name = "boru_ucu_ovallik_toleransi_max")
	private Float boruUcuOvallikToleransiMax;

	@Column(name = "boy_tolerans_max")
	private Float boyToleransMax;

	@Column(name = "boy_tolerans_min")
	private Float boyToleransMin;

	@Column(name = "boy_tolerans_nominal")
	private Float boyToleransNominal;

	@Column(name = "dis_kaynak_dikisi_yukseklik_toleransi_max")
	private Float disKaynakDikisiYukseklikToleransiMax;

	@Column(name = "dis_kaynak_dikisi_yukseklik_toleransi_min")
	private Float disKaynakDikisiYukseklikToleransiMin;

	@Column(name = "dogrusallik_toleransi_max")
	private String dogrusallikToleransiMax;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Column(name = "et_kalinligi_toleransi_max")
	private Float etKalinligiToleransiMax;

	@Column(name = "et_kalinligi_toleransi_min")
	private Float etKalinligiToleransiMin;

	@Column(name = "et_kalinligi_toleransi_nominal")
	private Float etKalinligiToleransiNominal;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "ic_kaynak_dikisi_yukseklik_toleransi_max")
	private Float icKaynakDikisiYukseklikToleransiMax;

	@Column(name = "ic_kaynak_dikisi_yukseklik_toleransi_min")
	private Float icKaynakDikisiYukseklikToleransiMin;

	@Column(name = "kalici_manyetiklik_toleransi_max")
	private Float kaliciManyetiklikToleransiMax;

	@Column(name = "kalici_manyetiklik_toleransi_ortalama")
	private Float kaliciManyetiklikToleransiOrtalama;

	@Column(name = "kalite_plani", length = 255)
	private String kalitePlani;

	@Column(name = "kaynak_agizi_koruma", length = 255)
	private String kaynakAgiziKoruma;

	@Column(length = 255)
	private String markalama;

	@Column(name = "max_metre_ikinci")
	private Float maxMetreIkinci;

	@Column(name = "max_metre_ilk")
	private Float maxMetreIlk;

	@Column(name = "max_metre_ucuncu")
	private Float maxMetreUcuncu;

	@Column(name = "min_metre_ikinci")
	private Float minMetreIkinci;

	@Column(name = "min_metre_ilk")
	private Float minMetreIlk;

	@Column(name = "min_metre_ucuncu")
	private Float minMetreUcuncu;

	@Column(name = "min_ortalama_boru_boyu", length = 255)
	private Float minOrtalamaBoruBoyu;

	@Column(name = "muf_boyu_toleransi_max")
	private Float mufBoyuToleransiMax;

	@Column(name = "muf_boyu_toleransi_min")
	private Float mufBoyuToleransiMin;

	@Column(name = "muf_boyu_toleransi_nominal")
	private Float mufBoyuToleransiNominal;

	@Column(name = "muf_ic_capi_toleransi_max")
	private Float mufIcCapiToleransiMax;

	@Column(name = "muf_ic_capi_toleransi_min")
	private Float mufIcCapiToleransiMin;

	@Column(name = "muf_ic_capi_toleransi_nominal")
	private Float mufIcCapiToleransiNominal;

	// @Column(name = "order_id", nullable = false)
	// private Integer orderId;

	@Column(name = "radyal_set_toleransi_max")
	private Float radyalSetToleransiMax;

	@Column(name = "yuzde_metre_ikinci")
	private Float yuzdeMetreIkinci;

	@Column(name = "yuzde_metre_ilk")
	private Float yuzdeMetreIlk;

	@Column(name = "yuzde_metre_ucuncu")
	private Float yuzdeMetreUcuncu;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	private SalesItem salesItem;

	@Column(name = "kazik_boy_tolerans", length = 255)
	private String kazikBoyTolerans;

	@Column(name = "a_ucu_ic_kaynak_taslama_mesafesi", length = 255)
	private String aUcuIcKaynakTaslamaMesafesi;

	@Column(name = "a_ucu_dis_kaynak_taslama_mesafesi", length = 255)
	private String aUcuDisKaynakTaslamaMesafesi;

	@Column(name = "b_ucu_ic_kaynak_taslama_mesafesi", length = 255)
	private String bUcuIcKaynakTaslamaMesafesi;

	@Column(name = "b_ucu_dis_kaynak_taslama_mesafesi", length = 255)
	private String bUcuDisKaynakTaslamaMesafesi;

	@Column(name = "dent_max")
	private Float dentMax;

	@Column(name = "dent_min")
	private Float dentMin;

	public BoruBoyutsalKontrolTolerans() {
		id = 0;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

	@Column(name = "documents")
	private String documents;

	@Transient
	private List<String> fileNames;

	@Transient
	private int fileNumber;

	// setters getters

	public Integer getId() {
		return this.id;
	}

	public String getBaglantiSekli() {
		return baglantiSekli;
	}

	public void setBaglantiSekli(String baglantiSekli) {
		this.baglantiSekli = baglantiSekli;
	}

	public Date getEklemeZamani() {
		return eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getGuncellemeZamani() {
		return guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getKalitePlani() {
		return kalitePlani;
	}

	public void setKalitePlani(String kalitePlani) {
		this.kalitePlani = kalitePlani;
	}

	public String getKaynakAgiziKoruma() {
		return kaynakAgiziKoruma;
	}

	public void setKaynakAgiziKoruma(String kaynakAgiziKoruma) {
		this.kaynakAgiziKoruma = kaynakAgiziKoruma;
	}

	public String getMarkalama() {
		return markalama;
	}

	public void setMarkalama(String markalama) {
		this.markalama = markalama;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public String getDocuments() {
		return documents;
	}

	public void setDocuments(String documents) {
		this.documents = documents;
	}

	public List<String> getFileNames() {
		try {
			String[] names = documents.split("//");
			fileNames = new ArrayList<String>();

			for (int i = 1; i < names.length; i++) {
				fileNames.add(names[i]);
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return fileNames;
	}

	public void setFileNames(List<String> fileNames) {
		this.fileNames = fileNames;
	}

	public int getFileNumber() {
		try {
			String[] names = documents.split("//");

			fileNumber = names.length - 1;
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return fileNumber;
	}

	public Float getaUcuKaynakAgiziAcisiToleransiMax() {
		return aUcuKaynakAgiziAcisiToleransiMax;
	}

	public void setaUcuKaynakAgiziAcisiToleransiMax(
			Float aUcuKaynakAgiziAcisiToleransiMax) {
		this.aUcuKaynakAgiziAcisiToleransiMax = aUcuKaynakAgiziAcisiToleransiMax;
	}

	public Float getaUcuKaynakAgiziAcisiToleransiMin() {
		return aUcuKaynakAgiziAcisiToleransiMin;
	}

	public void setaUcuKaynakAgiziAcisiToleransiMin(
			Float aUcuKaynakAgiziAcisiToleransiMin) {
		this.aUcuKaynakAgiziAcisiToleransiMin = aUcuKaynakAgiziAcisiToleransiMin;
	}

	public Float getaUcuKaynakAgiziAcisiToleransiNominal() {
		return aUcuKaynakAgiziAcisiToleransiNominal;
	}

	public void setaUcuKaynakAgiziAcisiToleransiNominal(
			Float aUcuKaynakAgiziAcisiToleransiNominal) {
		this.aUcuKaynakAgiziAcisiToleransiNominal = aUcuKaynakAgiziAcisiToleransiNominal;
	}

	public Float getaUcuKokYuzeyOlcusuMax() {
		return aUcuKokYuzeyOlcusuMax;
	}

	public void setaUcuKokYuzeyOlcusuMax(Float aUcuKokYuzeyOlcusuMax) {
		this.aUcuKokYuzeyOlcusuMax = aUcuKokYuzeyOlcusuMax;
	}

	public Float getaUcuKokYuzeyOlcusuMin() {
		return aUcuKokYuzeyOlcusuMin;
	}

	public void setaUcuKokYuzeyOlcusuMin(Float aUcuKokYuzeyOlcusuMin) {
		this.aUcuKokYuzeyOlcusuMin = aUcuKokYuzeyOlcusuMin;
	}

	public Float getaUcuKokYuzeyOlcusuNominal() {
		return aUcuKokYuzeyOlcusuNominal;
	}

	public void setaUcuKokYuzeyOlcusuNominal(Float aUcuKokYuzeyOlcusuNominal) {
		this.aUcuKokYuzeyOlcusuNominal = aUcuKokYuzeyOlcusuNominal;
	}

	public Float getbUcuKaynakAgiziAcisiToleransiMax() {
		return bUcuKaynakAgiziAcisiToleransiMax;
	}

	public void setbUcuKaynakAgiziAcisiToleransiMax(
			Float bUcuKaynakAgiziAcisiToleransiMax) {
		this.bUcuKaynakAgiziAcisiToleransiMax = bUcuKaynakAgiziAcisiToleransiMax;
	}

	public Float getbUcuKaynakAgiziAcisiToleransiMin() {
		return bUcuKaynakAgiziAcisiToleransiMin;
	}

	public void setbUcuKaynakAgiziAcisiToleransiMin(
			Float bUcuKaynakAgiziAcisiToleransiMin) {
		this.bUcuKaynakAgiziAcisiToleransiMin = bUcuKaynakAgiziAcisiToleransiMin;
	}

	public Float getbUcuKaynakAgiziAcisiToleransiNominal() {
		return bUcuKaynakAgiziAcisiToleransiNominal;
	}

	public void setbUcuKaynakAgiziAcisiToleransiNominal(
			Float bUcuKaynakAgiziAcisiToleransiNominal) {
		this.bUcuKaynakAgiziAcisiToleransiNominal = bUcuKaynakAgiziAcisiToleransiNominal;
	}

	public Float getbUcuKokYuzeyOlcusuMax() {
		return bUcuKokYuzeyOlcusuMax;
	}

	public void setbUcuKokYuzeyOlcusuMax(Float bUcuKokYuzeyOlcusuMax) {
		this.bUcuKokYuzeyOlcusuMax = bUcuKokYuzeyOlcusuMax;
	}

	public Float getbUcuKokYuzeyOlcusuMin() {
		return bUcuKokYuzeyOlcusuMin;
	}

	public void setbUcuKokYuzeyOlcusuMin(Float bUcuKokYuzeyOlcusuMin) {
		this.bUcuKokYuzeyOlcusuMin = bUcuKokYuzeyOlcusuMin;
	}

	public Float getbUcuKokYuzeyOlcusuNominal() {
		return bUcuKokYuzeyOlcusuNominal;
	}

	public void setbUcuKokYuzeyOlcusuNominal(Float bUcuKokYuzeyOlcusuNominal) {
		this.bUcuKokYuzeyOlcusuNominal = bUcuKokYuzeyOlcusuNominal;
	}

	public Float getBoruAgirligiToleransiMax() {
		return boruAgirligiToleransiMax;
	}

	public void setBoruAgirligiToleransiMax(Float boruAgirligiToleransiMax) {
		this.boruAgirligiToleransiMax = boruAgirligiToleransiMax;
	}

	public Float getBoruAgirligiToleransiMin() {
		return boruAgirligiToleransiMin;
	}

	public void setBoruAgirligiToleransiMin(Float boruAgirligiToleransiMin) {
		this.boruAgirligiToleransiMin = boruAgirligiToleransiMin;
	}

	public Float getBoruGovdesiDisCapToleransiMax() {
		return boruGovdesiDisCapToleransiMax;
	}

	public void setBoruGovdesiDisCapToleransiMax(
			Float boruGovdesiDisCapToleransiMax) {
		this.boruGovdesiDisCapToleransiMax = boruGovdesiDisCapToleransiMax;
	}

	public Float getBoruGovdesiDisCapToleransiMin() {
		return boruGovdesiDisCapToleransiMin;
	}

	public void setBoruGovdesiDisCapToleransiMin(
			Float boruGovdesiDisCapToleransiMin) {
		this.boruGovdesiDisCapToleransiMin = boruGovdesiDisCapToleransiMin;
	}

	public Float getBoruGovdesiDisCapToleransiNominal() {
		return boruGovdesiDisCapToleransiNominal;
	}

	public void setBoruGovdesiDisCapToleransiNominal(
			Float boruGovdesiDisCapToleransiNominal) {
		this.boruGovdesiDisCapToleransiNominal = boruGovdesiDisCapToleransiNominal;
	}

	public Float getBoruGovdesiOvallikToleransiMax() {
		return boruGovdesiOvallikToleransiMax;
	}

	public void setBoruGovdesiOvallikToleransiMax(
			Float boruGovdesiOvallikToleransiMax) {
		this.boruGovdesiOvallikToleransiMax = boruGovdesiOvallikToleransiMax;
	}

	public Float getBoruUcuCapToleransiMax() {
		return boruUcuCapToleransiMax;
	}

	public void setBoruUcuCapToleransiMax(Float boruUcuCapToleransiMax) {
		this.boruUcuCapToleransiMax = boruUcuCapToleransiMax;
	}

	public Float getBoruUcuCapToleransiMin() {
		return boruUcuCapToleransiMin;
	}

	public void setBoruUcuCapToleransiMin(Float boruUcuCapToleransiMin) {
		this.boruUcuCapToleransiMin = boruUcuCapToleransiMin;
	}

	public Float getBoruUcuCapToleransiNominal() {
		return boruUcuCapToleransiNominal;
	}

	public void setBoruUcuCapToleransiNominal(Float boruUcuCapToleransiNominal) {
		this.boruUcuCapToleransiNominal = boruUcuCapToleransiNominal;
	}

	public Float getBoruUcuDikligiToleransiMax() {
		return boruUcuDikligiToleransiMax;
	}

	public void setBoruUcuDikligiToleransiMax(Float boruUcuDikligiToleransiMax) {
		this.boruUcuDikligiToleransiMax = boruUcuDikligiToleransiMax;
	}

	public Float getBoruUcuOvallikToleransiMax() {
		return boruUcuOvallikToleransiMax;
	}

	public void setBoruUcuOvallikToleransiMax(Float boruUcuOvallikToleransiMax) {
		this.boruUcuOvallikToleransiMax = boruUcuOvallikToleransiMax;
	}

	public Float getBoyToleransMax() {
		return boyToleransMax;
	}

	public void setBoyToleransMax(Float boyToleransMax) {
		this.boyToleransMax = boyToleransMax;
	}

	public Float getBoyToleransMin() {
		return boyToleransMin;
	}

	public void setBoyToleransMin(Float boyToleransMin) {
		this.boyToleransMin = boyToleransMin;
	}

	public Float getBoyToleransNominal() {
		return boyToleransNominal;
	}

	public void setBoyToleransNominal(Float boyToleransNominal) {
		this.boyToleransNominal = boyToleransNominal;
	}

	public Float getDisKaynakDikisiYukseklikToleransiMax() {
		return disKaynakDikisiYukseklikToleransiMax;
	}

	public void setDisKaynakDikisiYukseklikToleransiMax(
			Float disKaynakDikisiYukseklikToleransiMax) {
		this.disKaynakDikisiYukseklikToleransiMax = disKaynakDikisiYukseklikToleransiMax;
	}

	public Float getDisKaynakDikisiYukseklikToleransiMin() {
		return disKaynakDikisiYukseklikToleransiMin;
	}

	public void setDisKaynakDikisiYukseklikToleransiMin(
			Float disKaynakDikisiYukseklikToleransiMin) {
		this.disKaynakDikisiYukseklikToleransiMin = disKaynakDikisiYukseklikToleransiMin;
	}

	public Float getEtKalinligiToleransiMax() {
		return etKalinligiToleransiMax;
	}

	public void setEtKalinligiToleransiMax(Float etKalinligiToleransiMax) {
		this.etKalinligiToleransiMax = etKalinligiToleransiMax;
	}

	public Float getEtKalinligiToleransiMin() {
		return etKalinligiToleransiMin;
	}

	public void setEtKalinligiToleransiMin(Float etKalinligiToleransiMin) {
		this.etKalinligiToleransiMin = etKalinligiToleransiMin;
	}

	public Float getEtKalinligiToleransiNominal() {
		return etKalinligiToleransiNominal;
	}

	public void setEtKalinligiToleransiNominal(Float etKalinligiToleransiNominal) {
		this.etKalinligiToleransiNominal = etKalinligiToleransiNominal;
	}

	public Float getIcKaynakDikisiYukseklikToleransiMax() {
		return icKaynakDikisiYukseklikToleransiMax;
	}

	public void setIcKaynakDikisiYukseklikToleransiMax(
			Float icKaynakDikisiYukseklikToleransiMax) {
		this.icKaynakDikisiYukseklikToleransiMax = icKaynakDikisiYukseklikToleransiMax;
	}

	public Float getIcKaynakDikisiYukseklikToleransiMin() {
		return icKaynakDikisiYukseklikToleransiMin;
	}

	public void setIcKaynakDikisiYukseklikToleransiMin(
			Float icKaynakDikisiYukseklikToleransiMin) {
		this.icKaynakDikisiYukseklikToleransiMin = icKaynakDikisiYukseklikToleransiMin;
	}

	public Float getKaliciManyetiklikToleransiMax() {
		return kaliciManyetiklikToleransiMax;
	}

	public void setKaliciManyetiklikToleransiMax(
			Float kaliciManyetiklikToleransiMax) {
		this.kaliciManyetiklikToleransiMax = kaliciManyetiklikToleransiMax;
	}

	public Float getKaliciManyetiklikToleransiOrtalama() {
		return kaliciManyetiklikToleransiOrtalama;
	}

	public void setKaliciManyetiklikToleransiOrtalama(
			Float kaliciManyetiklikToleransiOrtalama) {
		this.kaliciManyetiklikToleransiOrtalama = kaliciManyetiklikToleransiOrtalama;
	}

	public Float getMaxMetreIkinci() {
		return maxMetreIkinci;
	}

	public void setMaxMetreIkinci(Float maxMetreIkinci) {
		this.maxMetreIkinci = maxMetreIkinci;
	}

	public Float getMaxMetreIlk() {
		return maxMetreIlk;
	}

	public void setMaxMetreIlk(Float maxMetreIlk) {
		this.maxMetreIlk = maxMetreIlk;
	}

	public Float getMaxMetreUcuncu() {
		return maxMetreUcuncu;
	}

	public void setMaxMetreUcuncu(Float maxMetreUcuncu) {
		this.maxMetreUcuncu = maxMetreUcuncu;
	}

	public Float getMinMetreIkinci() {
		return minMetreIkinci;
	}

	public void setMinMetreIkinci(Float minMetreIkinci) {
		this.minMetreIkinci = minMetreIkinci;
	}

	public Float getMinMetreIlk() {
		return minMetreIlk;
	}

	public void setMinMetreIlk(Float minMetreIlk) {
		this.minMetreIlk = minMetreIlk;
	}

	public Float getMinMetreUcuncu() {
		return minMetreUcuncu;
	}

	public void setMinMetreUcuncu(Float minMetreUcuncu) {
		this.minMetreUcuncu = minMetreUcuncu;
	}

	public Float getMinOrtalamaBoruBoyu() {
		return minOrtalamaBoruBoyu;
	}

	public void setMinOrtalamaBoruBoyu(Float minOrtalamaBoruBoyu) {
		this.minOrtalamaBoruBoyu = minOrtalamaBoruBoyu;
	}

	public Float getMufBoyuToleransiMax() {
		return mufBoyuToleransiMax;
	}

	public void setMufBoyuToleransiMax(Float mufBoyuToleransiMax) {
		this.mufBoyuToleransiMax = mufBoyuToleransiMax;
	}

	public Float getMufBoyuToleransiMin() {
		return mufBoyuToleransiMin;
	}

	public void setMufBoyuToleransiMin(Float mufBoyuToleransiMin) {
		this.mufBoyuToleransiMin = mufBoyuToleransiMin;
	}

	public Float getMufBoyuToleransiNominal() {
		return mufBoyuToleransiNominal;
	}

	public void setMufBoyuToleransiNominal(Float mufBoyuToleransiNominal) {
		this.mufBoyuToleransiNominal = mufBoyuToleransiNominal;
	}

	public Float getMufIcCapiToleransiMax() {
		return mufIcCapiToleransiMax;
	}

	public void setMufIcCapiToleransiMax(Float mufIcCapiToleransiMax) {
		this.mufIcCapiToleransiMax = mufIcCapiToleransiMax;
	}

	public Float getMufIcCapiToleransiMin() {
		return mufIcCapiToleransiMin;
	}

	public void setMufIcCapiToleransiMin(Float mufIcCapiToleransiMin) {
		this.mufIcCapiToleransiMin = mufIcCapiToleransiMin;
	}

	public Float getMufIcCapiToleransiNominal() {
		return mufIcCapiToleransiNominal;
	}

	public void setMufIcCapiToleransiNominal(Float mufIcCapiToleransiNominal) {
		this.mufIcCapiToleransiNominal = mufIcCapiToleransiNominal;
	}

	public Float getRadyalSetToleransiMax() {
		return radyalSetToleransiMax;
	}

	public void setRadyalSetToleransiMax(Float radyalSetToleransiMax) {
		this.radyalSetToleransiMax = radyalSetToleransiMax;
	}

	public Float getYuzdeMetreIkinci() {
		return yuzdeMetreIkinci;
	}

	public void setYuzdeMetreIkinci(Float yuzdeMetreIkinci) {
		this.yuzdeMetreIkinci = yuzdeMetreIkinci;
	}

	public Float getYuzdeMetreIlk() {
		return yuzdeMetreIlk;
	}

	public void setYuzdeMetreIlk(Float yuzdeMetreIlk) {
		this.yuzdeMetreIlk = yuzdeMetreIlk;
	}

	public Float getYuzdeMetreUcuncu() {
		return yuzdeMetreUcuncu;
	}

	public void setYuzdeMetreUcuncu(Float yuzdeMetreUcuncu) {
		this.yuzdeMetreUcuncu = yuzdeMetreUcuncu;
	}

	public void setFileNumber(int fileNumber) {
		this.fileNumber = fileNumber;
	}

	public String getKazikBoyTolerans() {
		return kazikBoyTolerans;
	}

	public void setKazikBoyTolerans(String kazikBoyTolerans) {
		this.kazikBoyTolerans = kazikBoyTolerans;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the aUcuIcKaynakTaslamaMesafesi
	 */
	public String getaUcuIcKaynakTaslamaMesafesi() {
		return aUcuIcKaynakTaslamaMesafesi;
	}

	/**
	 * @param aUcuIcKaynakTaslamaMesafesi
	 *            the aUcuIcKaynakTaslamaMesafesi to set
	 */
	public void setaUcuIcKaynakTaslamaMesafesi(
			String aUcuIcKaynakTaslamaMesafesi) {
		this.aUcuIcKaynakTaslamaMesafesi = aUcuIcKaynakTaslamaMesafesi;
	}

	/**
	 * @return the aUcuDisKaynakTaslamaMesafesi
	 */
	public String getaUcuDisKaynakTaslamaMesafesi() {
		return aUcuDisKaynakTaslamaMesafesi;
	}

	/**
	 * @param aUcuDisKaynakTaslamaMesafesi
	 *            the aUcuDisKaynakTaslamaMesafesi to set
	 */
	public void setaUcuDisKaynakTaslamaMesafesi(
			String aUcuDisKaynakTaslamaMesafesi) {
		this.aUcuDisKaynakTaslamaMesafesi = aUcuDisKaynakTaslamaMesafesi;
	}

	/**
	 * @return the bUcuIcKaynakTaslamaMesafesi
	 */
	public String getbUcuIcKaynakTaslamaMesafesi() {
		return bUcuIcKaynakTaslamaMesafesi;
	}

	/**
	 * @param bUcuIcKaynakTaslamaMesafesi
	 *            the bUcuIcKaynakTaslamaMesafesi to set
	 */
	public void setbUcuIcKaynakTaslamaMesafesi(
			String bUcuIcKaynakTaslamaMesafesi) {
		this.bUcuIcKaynakTaslamaMesafesi = bUcuIcKaynakTaslamaMesafesi;
	}

	/**
	 * @return the bUcuDisKaynakTaslamaMesafesi
	 */
	public String getbUcuDisKaynakTaslamaMesafesi() {
		return bUcuDisKaynakTaslamaMesafesi;
	}

	/**
	 * @param bUcuDisKaynakTaslamaMesafesi
	 *            the bUcuDisKaynakTaslamaMesafesi to set
	 */
	public void setbUcuDisKaynakTaslamaMesafesi(
			String bUcuDisKaynakTaslamaMesafesi) {
		this.bUcuDisKaynakTaslamaMesafesi = bUcuDisKaynakTaslamaMesafesi;
	}

	/**
	 * @return the dogrusallikToleransiMax
	 */
	public String getDogrusallikToleransiMax() {
		return dogrusallikToleransiMax;
	}

	/**
	 * @param dogrusallikToleransiMax
	 *            the dogrusallikToleransiMax to set
	 */
	public void setDogrusallikToleransiMax(String dogrusallikToleransiMax) {
		this.dogrusallikToleransiMax = dogrusallikToleransiMax;
	}

	/**
	 * @return the dentMax
	 */
	public Float getDentMax() {
		return dentMax;
	}

	/**
	 * @param dentMax
	 *            the dentMax to set
	 */
	public void setDentMax(Float dentMax) {
		this.dentMax = dentMax;
	}

	/**
	 * @return the dentMin
	 */
	public Float getDentMin() {
		return dentMin;
	}

	/**
	 * @param dentMin
	 *            the dentMin to set
	 */
	public void setDentMin(Float dentMin) {
		this.dentMin = dentMin;
	}

}