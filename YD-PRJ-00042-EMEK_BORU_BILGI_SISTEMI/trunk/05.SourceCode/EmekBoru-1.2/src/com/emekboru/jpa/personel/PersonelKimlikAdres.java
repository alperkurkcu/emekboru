package com.emekboru.jpa.personel;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.Employee;


/**
 * The persistent class for the personel_kimlik_adres database table.
 * 
 */
@Entity
@Table(name="personel_kimlik_adres")
public class PersonelKimlikAdres implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PERSONEL_KIMLIK_ADRES_ID_GENERATOR", sequenceName="PERSONEL_KIMLIK_ADRES_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PERSONEL_KIMLIK_ADRES_ID_GENERATOR")
	@Column(name="ID")
	private Integer id;

	private String adres;

	@Column(name="eklenme_tarihi")
	private Timestamp eklenmeTarihi;

//	@Column(name="kimlik_id")
//	private Integer kimlikId;

	@Column(name="kullanici_id_ekleyen")
	private Integer kullaniciIdEkleyen;

	@Column(name="kullanici_id_guncelleyen")
	private Integer kullaniciIdGuncelleyen;

	@Column(name="son_guncellenme_tarihi")
	private Timestamp sonGuncellenmeTarihi;

	@Column(name="varsayilan_mi")
	private Boolean varsayilanMi;

	// bi-directional many-to-one association to Employee
	@ManyToOne
	@JoinColumn(name = "kimlik_id", insertable = false)
	private Employee employee;

	public PersonelKimlikAdres() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAdres() {
		return this.adres;
	}

	public void setAdres(String adres) {
		this.adres = adres;
	}

	public Timestamp getEklenmeTarihi() {
		return this.eklenmeTarihi;
	}

	public void setEklenmeTarihi(Timestamp eklenmeTarihi) {
		this.eklenmeTarihi = eklenmeTarihi;
	}

//	public Integer getKimlikId() {
//		return this.kimlikId;
//	}
//
//	public void setKimlikId(Integer kimlikId) {
//		this.kimlikId = kimlikId;
//	}

	public Integer getKullaniciIdEkleyen() {
		return this.kullaniciIdEkleyen;
	}

	public void setKullaniciIdEkleyen(Integer kullaniciIdEkleyen) {
		this.kullaniciIdEkleyen = kullaniciIdEkleyen;
	}

	public Integer getKullaniciIdGuncelleyen() {
		return this.kullaniciIdGuncelleyen;
	}

	public void setKullaniciIdGuncelleyen(Integer kullaniciIdGuncelleyen) {
		this.kullaniciIdGuncelleyen = kullaniciIdGuncelleyen;
	}

	public Timestamp getSonGuncellenmeTarihi() {
		return this.sonGuncellenmeTarihi;
	}

	public void setSonGuncellenmeTarihi(Timestamp sonGuncellenmeTarihi) {
		this.sonGuncellenmeTarihi = sonGuncellenmeTarihi;
	}

	public Boolean getVarsayilanMi() {
		if (this.varsayilanMi == null) return false;
		return this.varsayilanMi;
	}

	public void setVarsayilanMi(Boolean varsayilanMi) {
		this.varsayilanMi = varsayilanMi;
	}

	/**
	 * @return the employee
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 * @param employee the employee to set
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

}