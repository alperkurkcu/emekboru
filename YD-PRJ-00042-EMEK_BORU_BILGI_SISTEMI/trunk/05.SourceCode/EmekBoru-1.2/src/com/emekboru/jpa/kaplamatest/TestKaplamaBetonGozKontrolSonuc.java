package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.isolation.IsolationTestDefinition;

/**
 * The persistent class for the test_kaplama_beton_goz_kontrol_sonuc database
 * table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaBetonGozKontrolSonuc.findAll", query = "SELECT r FROM TestKaplamaBetonGozKontrolSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_kaplama_beton_goz_kontrol_sonuc")
public class TestKaplamaBetonGozKontrolSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KAPLAMA_BETON_GOZ_KONTROL_SONUC_ID_GENERATOR", sequenceName = "TEST_KAPLAMA_BETON_GOZ_KONTROL_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KAPLAMA_BETON_GOZ_KONTROL_SONUC_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Column(name = "cut_back")
	private Boolean cutBack;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ekleme_zamani")
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	// @Column(name="global_id")
	// private Integer globalId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "kaynak_agzi")
	private Boolean kaynakAgzi;

	@Column(name = "kaynak_agzi_koruma")
	private Boolean kaynakAgziKoruma;

	private Boolean markalama;

	// @Column(name="pipe_id")
	// private Integer pipeId;

	@Column(name = "red_komple_altibos")
	private Boolean redKompleAltibos;

	@Column(name = "red_komple_catlak")
	private Boolean redKompleCatlak;

	@Column(name = "red_komple_ince")
	private Boolean redKompleInce;

	private Integer sonuc;

	@Column(name = "tamir_akma")
	private Boolean tamirAkma;

	@Column(name = "tamir_catlak_altibos")
	private Boolean tamirCatlakAltibos;

	@Column(name = "tamir_ince")
	private Boolean tamirInce;

	@Column(name = "tamir_kilcal_catlak")
	private Boolean tamirKilcalCatlak;

	@Column(name = "tamir_ruzgar_catlagi")
	private Boolean tamirRuzgarCatlagi;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi")
	private Date testTarihi;

	@Column(name = "vardiya")
	private Integer vardiya;

	@Column(name = "vernik")
	private Boolean vernik;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	public TestKaplamaBetonGozKontrolSonuc() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getCutBack() {
		return this.cutBack;
	}

	public void setCutBack(Boolean cutBack) {
		this.cutBack = cutBack;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Boolean getKaynakAgzi() {
		return this.kaynakAgzi;
	}

	public void setKaynakAgzi(Boolean kaynakAgzi) {
		this.kaynakAgzi = kaynakAgzi;
	}

	public Boolean getKaynakAgziKoruma() {
		return this.kaynakAgziKoruma;
	}

	public void setKaynakAgziKoruma(Boolean kaynakAgziKoruma) {
		this.kaynakAgziKoruma = kaynakAgziKoruma;
	}

	public Boolean getMarkalama() {
		return this.markalama;
	}

	public void setMarkalama(Boolean markalama) {
		this.markalama = markalama;
	}

	/**
	 * @return the bagliGlobalId
	 */
	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}

	/**
	 * @param bagliGlobalId
	 *            the bagliGlobalId to set
	 */
	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Boolean getRedKompleAltibos() {
		return this.redKompleAltibos;
	}

	public void setRedKompleAltibos(Boolean redKompleAltibos) {
		this.redKompleAltibos = redKompleAltibos;
	}

	public Boolean getRedKompleCatlak() {
		return this.redKompleCatlak;
	}

	public void setRedKompleCatlak(Boolean redKompleCatlak) {
		this.redKompleCatlak = redKompleCatlak;
	}

	public Boolean getRedKompleInce() {
		return this.redKompleInce;
	}

	public void setRedKompleInce(Boolean redKompleInce) {
		this.redKompleInce = redKompleInce;
	}

	public Integer getSonuc() {
		return this.sonuc;
	}

	public void setSonuc(Integer sonuc) {
		this.sonuc = sonuc;
	}

	public Boolean getTamirAkma() {
		return this.tamirAkma;
	}

	public void setTamirAkma(Boolean tamirAkma) {
		this.tamirAkma = tamirAkma;
	}

	public Boolean getTamirCatlakAltibos() {
		return this.tamirCatlakAltibos;
	}

	public void setTamirCatlakAltibos(Boolean tamirCatlakAltibos) {
		this.tamirCatlakAltibos = tamirCatlakAltibos;
	}

	public Boolean getTamirInce() {
		return this.tamirInce;
	}

	public void setTamirInce(Boolean tamirInce) {
		this.tamirInce = tamirInce;
	}

	public Boolean getTamirKilcalCatlak() {
		return this.tamirKilcalCatlak;
	}

	public void setTamirKilcalCatlak(Boolean tamirKilcalCatlak) {
		this.tamirKilcalCatlak = tamirKilcalCatlak;
	}

	public Boolean getTamirRuzgarCatlagi() {
		return this.tamirRuzgarCatlagi;
	}

	public void setTamirRuzgarCatlagi(Boolean tamirRuzgarCatlagi) {
		this.tamirRuzgarCatlagi = tamirRuzgarCatlagi;
	}

	public Date getTestTarihi() {
		return this.testTarihi;
	}

	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	public Integer getVardiya() {
		return this.vardiya;
	}

	public void setVardiya(Integer vardiya) {
		this.vardiya = vardiya;
	}

	public Boolean getVernik() {
		return this.vernik;
	}

	public void setVernik(Boolean vernik) {
		this.vernik = vernik;
	}

	/**
	 * @return the bagliTestId
	 */
	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

}