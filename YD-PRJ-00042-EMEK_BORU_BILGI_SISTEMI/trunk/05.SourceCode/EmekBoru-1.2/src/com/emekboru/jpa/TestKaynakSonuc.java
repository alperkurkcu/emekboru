package com.emekboru.jpa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.emekboru.jpa.rulo.RuloPipeLink;

/**
 * The persistent class for the test_kaynak_sonuc database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "TestKaynakSonuc.findAll", query = "SELECT r FROM TestKaynakSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId"),
		@NamedQuery(name = "TestKaynakSonuc.getBySalesItem", query = "SELECT r FROM TestKaynakSonuc r WHERE r.pipe.salesItem.itemId=:prmSalesItem and r.bagliGlobalId.globalId=:prmGlobalId order by r.partiNo"),
		@NamedQuery(name = "TestKaynakSonuc.getByDokumNoGlobalId", query = "SELECT r FROM TestKaynakSonuc r WHERE r.pipe.ruloPipeLink.rulo.ruloDokumNo=:prmDokumNo and r.bagliGlobalId.globalId=:prmGlobalId order by r.pipe.pipeBarkodNo, r.bagliTestId.code") })
@Table(name = "test_kaynak_sonuc")
public class TestKaynakSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KAYNAK_SONUC_ID_GENERATOR", sequenceName = "TEST_KAYNAK_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KAYNAK_SONUC_ID_GENERATOR")
	@Column(name = "ID")
	private Integer id;

	@Column(name = "aciklama")
	private String aciklama;

	// @Column(name = "destructive_test_id")
	// private Integer destructiveTestId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	// @Column(name = "global_id")
	// private Integer globalId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "genislik_dis")
	private BigDecimal genislikDis;

	@Column(name = "genislik_ic")
	private BigDecimal genislikIc;

	@Column(name = "yukseklik_dis")
	private BigDecimal yukseklikDis;

	@Column(name = "yukseklik_ic")
	private BigDecimal yukseklikIc;

	@Column(name = "parti_no")
	private String partiNo;

	@Column(name = "penetrasyon")
	private BigDecimal penetrasyon;

	@Column(name = "penetrasyon_ic")
	private BigDecimal penetrasyonIc;

	@Column(name = "penetrasyon_dis")
	private BigDecimal penetrasyonDis;

	// @Column(name = "pipe_id")
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private DestructiveTestsSpec bagliGlobalId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "destructive_test_id", referencedColumnName = "test_id", insertable = false)
	private DestructiveTestsSpec bagliTestId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	private Boolean sonuc;

	@Column(name = "kaynak_kacikligi")
	private BigDecimal kaynakKacikligi;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi", nullable = false)
	private Date testTarihi;

	@Column(name = "documents")
	private String documents;

	@Transient
	private List<String> fileNames;

	@Transient
	private int fileNumber;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", nullable = false, referencedColumnName = "pipe_id", insertable = false, updatable = false)
	private RuloPipeLink ruloPipeLink;

	@Column(name = "test_durum")
	private Boolean testDurum;

	@Column(name = "sample_no")
	private String sampleNo;

	@Column(name = "bir_bes")
	private String birBes;

	@Column(name = "alti_bes")
	private String altiBes;

	@Column(name = "iki_bes")
	private String ikiBes;

	@Column(name = "bes_bes")
	private String besBes;

	@Column(name = "uc_bes")
	private String ucBes;

	@Column(name = "dort_bes")
	private String dortBes;

	@Column(name = "bir_bir")
	private String birBir;

	@Column(name = "bir_uc")
	private String birUc;

	@Column(name = "bir_dort")
	private String birDort;

	@Column(name = "alti_bir")
	private String altiBir;

	@Column(name = "alti_uc")
	private String altiUc;

	@Column(name = "alti_dort")
	private String altiDort;

	@Column(name = "iki_bir")
	private String ikiBir;

	@Column(name = "iki_uc")
	private String ikiUc;

	@Column(name = "iki_dort")
	private String ikiDort;

	@Column(name = "bes_bir")
	private String besBir;

	@Column(name = "bes_uc")
	private String besUc;

	@Column(name = "bes_dort")
	private String besDort;

	@Column(name = "uc_bir")
	private String ucBir;

	@Column(name = "uc_uc")
	private String ucUc;

	@Column(name = "uc_dort")
	private String ucDort;

	@Column(name = "dort_bir")
	private String dortBir;

	@Column(name = "dort_uc")
	private String dortUc;

	@Column(name = "dort_dort")
	private String dortDort;

	@Column(name = "alti_iki")
	private String altiIki;

	@Column(name = "yedi_bir")
	private String yediBir;

	@Column(name = "bir_iki")
	private String birIki;

	@Column(name = "bes_iki")
	private String besIki;

	@Column(name = "yedi_iki")
	private String yediIki;

	@Column(name = "iki_iki")
	private String ikiIki;

	@Column(name = "dort_iki")
	private String dortIki;

	@Column(name = "yedi_uc")
	private String yediUc;

	@Column(name = "uc_iki")
	private String ucIki;

	public TestKaynakSonuc() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Date getEklemeZamani() {
		return eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Date getGuncellemeZamani() {
		return guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public DestructiveTestsSpec getBagliGlobalId() {
		return bagliGlobalId;
	}

	public void setBagliGlobalId(DestructiveTestsSpec bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	public DestructiveTestsSpec getBagliTestId() {
		return bagliTestId;
	}

	public void setBagliTestId(DestructiveTestsSpec bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public Boolean getSonuc() {
		return sonuc;
	}

	public void setSonuc(Boolean sonuc) {
		this.sonuc = sonuc;
	}

	public Date getTestTarihi() {
		return testTarihi;
	}

	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	public String getDocuments() {
		return documents;
	}

	public void setDocuments(String documents) {
		this.documents = documents;
	}

	public List<String> getFileNames() {
		try {
			String[] names = documents.split("//");
			fileNames = new ArrayList<String>();

			for (int i = 1; i < names.length; i++) {
				fileNames.add(names[i]);
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return fileNames;
	}

	public void setFileNames(List<String> fileNames) {
		this.fileNames = fileNames;
	}

	public int getFileNumber() {
		try {
			String[] names = documents.split("//");

			fileNumber = names.length - 1;
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return fileNumber;
	}

	/**
	 * @return the ruloPipeLink
	 */
	public RuloPipeLink getRuloPipeLink() {
		return ruloPipeLink;
	}

	/**
	 * @param ruloPipeLink
	 *            the ruloPipeLink to set
	 */
	public void setRuloPipeLink(RuloPipeLink ruloPipeLink) {
		this.ruloPipeLink = ruloPipeLink;
	}

	/**
	 * @return the testDurum
	 */
	public Boolean getTestDurum() {
		return testDurum;
	}

	/**
	 * @param testDurum
	 *            the testDurum to set
	 */
	public void setTestDurum(Boolean testDurum) {
		this.testDurum = testDurum;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the genislikDis
	 */
	public BigDecimal getGenislikDis() {
		return genislikDis;
	}

	/**
	 * @param genislikDis
	 *            the genislikDis to set
	 */
	public void setGenislikDis(BigDecimal genislikDis) {
		this.genislikDis = genislikDis;
	}

	/**
	 * @return the genislikIc
	 */
	public BigDecimal getGenislikIc() {
		return genislikIc;
	}

	/**
	 * @param genislikIc
	 *            the genislikIc to set
	 */
	public void setGenislikIc(BigDecimal genislikIc) {
		this.genislikIc = genislikIc;
	}

	/**
	 * @return the yukseklikDis
	 */
	public BigDecimal getYukseklikDis() {
		return yukseklikDis;
	}

	/**
	 * @param yukseklikDis
	 *            the yukseklikDis to set
	 */
	public void setYukseklikDis(BigDecimal yukseklikDis) {
		this.yukseklikDis = yukseklikDis;
	}

	/**
	 * @return the yukseklikIc
	 */
	public BigDecimal getYukseklikIc() {
		return yukseklikIc;
	}

	/**
	 * @param yukseklikIc
	 *            the yukseklikIc to set
	 */
	public void setYukseklikIc(BigDecimal yukseklikIc) {
		this.yukseklikIc = yukseklikIc;
	}

	/**
	 * @return the kaynakKacikligi
	 */
	public BigDecimal getKaynakKacikligi() {
		return kaynakKacikligi;
	}

	/**
	 * @param kaynakKacikligi
	 *            the kaynakKacikligi to set
	 */
	public void setKaynakKacikligi(BigDecimal kaynakKacikligi) {
		this.kaynakKacikligi = kaynakKacikligi;
	}

	/**
	 * @return the sampleNo
	 */
	public String getSampleNo() {
		return sampleNo;
	}

	/**
	 * @param sampleNo
	 *            the sampleNo to set
	 */
	public void setSampleNo(String sampleNo) {
		this.sampleNo = sampleNo;
	}

	/**
	 * @return the birBes
	 */
	public String getBirBes() {
		return birBes;
	}

	/**
	 * @param birBes
	 *            the birBes to set
	 */
	public void setBirBes(String birBes) {
		this.birBes = birBes;
	}

	/**
	 * @return the altiBes
	 */
	public String getAltiBes() {
		return altiBes;
	}

	/**
	 * @param altiBes
	 *            the altiBes to set
	 */
	public void setAltiBes(String altiBes) {
		this.altiBes = altiBes;
	}

	/**
	 * @return the ikiBes
	 */
	public String getIkiBes() {
		return ikiBes;
	}

	/**
	 * @param ikiBes
	 *            the ikiBes to set
	 */
	public void setIkiBes(String ikiBes) {
		this.ikiBes = ikiBes;
	}

	/**
	 * @return the besBes
	 */
	public String getBesBes() {
		return besBes;
	}

	/**
	 * @param besBes
	 *            the besBes to set
	 */
	public void setBesBes(String besBes) {
		this.besBes = besBes;
	}

	/**
	 * @return the ucBes
	 */
	public String getUcBes() {
		return ucBes;
	}

	/**
	 * @param ucBes
	 *            the ucBes to set
	 */
	public void setUcBes(String ucBes) {
		this.ucBes = ucBes;
	}

	/**
	 * @return the dortBes
	 */
	public String getDortBes() {
		return dortBes;
	}

	/**
	 * @param dortBes
	 *            the dortBes to set
	 */
	public void setDortBes(String dortBes) {
		this.dortBes = dortBes;
	}

	/**
	 * @return the birBir
	 */
	public String getBirBir() {
		return birBir;
	}

	/**
	 * @param birBir
	 *            the birBir to set
	 */
	public void setBirBir(String birBir) {
		this.birBir = birBir;
	}

	/**
	 * @return the birUc
	 */
	public String getBirUc() {
		return birUc;
	}

	/**
	 * @param birUc
	 *            the birUc to set
	 */
	public void setBirUc(String birUc) {
		this.birUc = birUc;
	}

	/**
	 * @return the birDort
	 */
	public String getBirDort() {
		return birDort;
	}

	/**
	 * @param birDort
	 *            the birDort to set
	 */
	public void setBirDort(String birDort) {
		this.birDort = birDort;
	}

	/**
	 * @return the altiBir
	 */
	public String getAltiBir() {
		return altiBir;
	}

	/**
	 * @param altiBir
	 *            the altiBir to set
	 */
	public void setAltiBir(String altiBir) {
		this.altiBir = altiBir;
	}

	/**
	 * @return the altiUc
	 */
	public String getAltiUc() {
		return altiUc;
	}

	/**
	 * @param altiUc
	 *            the altiUc to set
	 */
	public void setAltiUc(String altiUc) {
		this.altiUc = altiUc;
	}

	/**
	 * @return the altiDort
	 */
	public String getAltiDort() {
		return altiDort;
	}

	/**
	 * @param altiDort
	 *            the altiDort to set
	 */
	public void setAltiDort(String altiDort) {
		this.altiDort = altiDort;
	}

	/**
	 * @return the ikiBir
	 */
	public String getIkiBir() {
		return ikiBir;
	}

	/**
	 * @param ikiBir
	 *            the ikiBir to set
	 */
	public void setIkiBir(String ikiBir) {
		this.ikiBir = ikiBir;
	}

	/**
	 * @return the ikiUc
	 */
	public String getIkiUc() {
		return ikiUc;
	}

	/**
	 * @param ikiUc
	 *            the ikiUc to set
	 */
	public void setIkiUc(String ikiUc) {
		this.ikiUc = ikiUc;
	}

	/**
	 * @return the ikiDort
	 */
	public String getIkiDort() {
		return ikiDort;
	}

	/**
	 * @param ikiDort
	 *            the ikiDort to set
	 */
	public void setIkiDort(String ikiDort) {
		this.ikiDort = ikiDort;
	}

	/**
	 * @return the besBir
	 */
	public String getBesBir() {
		return besBir;
	}

	/**
	 * @param besBir
	 *            the besBir to set
	 */
	public void setBesBir(String besBir) {
		this.besBir = besBir;
	}

	/**
	 * @return the besUc
	 */
	public String getBesUc() {
		return besUc;
	}

	/**
	 * @param besUc
	 *            the besUc to set
	 */
	public void setBesUc(String besUc) {
		this.besUc = besUc;
	}

	/**
	 * @return the besDort
	 */
	public String getBesDort() {
		return besDort;
	}

	/**
	 * @param besDort
	 *            the besDort to set
	 */
	public void setBesDort(String besDort) {
		this.besDort = besDort;
	}

	/**
	 * @return the ucBir
	 */
	public String getUcBir() {
		return ucBir;
	}

	/**
	 * @param ucBir
	 *            the ucBir to set
	 */
	public void setUcBir(String ucBir) {
		this.ucBir = ucBir;
	}

	/**
	 * @return the ucUc
	 */
	public String getUcUc() {
		return ucUc;
	}

	/**
	 * @param ucUc
	 *            the ucUc to set
	 */
	public void setUcUc(String ucUc) {
		this.ucUc = ucUc;
	}

	/**
	 * @return the ucDort
	 */
	public String getUcDort() {
		return ucDort;
	}

	/**
	 * @param ucDort
	 *            the ucDort to set
	 */
	public void setUcDort(String ucDort) {
		this.ucDort = ucDort;
	}

	/**
	 * @return the dortBir
	 */
	public String getDortBir() {
		return dortBir;
	}

	/**
	 * @param dortBir
	 *            the dortBir to set
	 */
	public void setDortBir(String dortBir) {
		this.dortBir = dortBir;
	}

	/**
	 * @return the dortUc
	 */
	public String getDortUc() {
		return dortUc;
	}

	/**
	 * @param dortUc
	 *            the dortUc to set
	 */
	public void setDortUc(String dortUc) {
		this.dortUc = dortUc;
	}

	/**
	 * @return the dortDort
	 */
	public String getDortDort() {
		return dortDort;
	}

	/**
	 * @param dortDort
	 *            the dortDort to set
	 */
	public void setDortDort(String dortDort) {
		this.dortDort = dortDort;
	}

	/**
	 * @return the altiIki
	 */
	public String getAltiIki() {
		return altiIki;
	}

	/**
	 * @param altiIki
	 *            the altiIki to set
	 */
	public void setAltiIki(String altiIki) {
		this.altiIki = altiIki;
	}

	/**
	 * @return the yediBir
	 */
	public String getYediBir() {
		return yediBir;
	}

	/**
	 * @param yediBir
	 *            the yediBir to set
	 */
	public void setYediBir(String yediBir) {
		this.yediBir = yediBir;
	}

	/**
	 * @return the birIki
	 */
	public String getBirIki() {
		return birIki;
	}

	/**
	 * @param birIki
	 *            the birIki to set
	 */
	public void setBirIki(String birIki) {
		this.birIki = birIki;
	}

	/**
	 * @return the besIki
	 */
	public String getBesIki() {
		return besIki;
	}

	/**
	 * @param besIki
	 *            the besIki to set
	 */
	public void setBesIki(String besIki) {
		this.besIki = besIki;
	}

	/**
	 * @return the yediIki
	 */
	public String getYediIki() {
		return yediIki;
	}

	/**
	 * @param yediIki
	 *            the yediIki to set
	 */
	public void setYediIki(String yediIki) {
		this.yediIki = yediIki;
	}

	/**
	 * @return the ikiIki
	 */
	public String getIkiIki() {
		return ikiIki;
	}

	/**
	 * @param ikiIki
	 *            the ikiIki to set
	 */
	public void setIkiIki(String ikiIki) {
		this.ikiIki = ikiIki;
	}

	/**
	 * @return the dortIki
	 */
	public String getDortIki() {
		return dortIki;
	}

	/**
	 * @param dortIki
	 *            the dortIki to set
	 */
	public void setDortIki(String dortIki) {
		this.dortIki = dortIki;
	}

	/**
	 * @return the yediUc
	 */
	public String getYediUc() {
		return yediUc;
	}

	/**
	 * @param yediUc
	 *            the yediUc to set
	 */
	public void setYediUc(String yediUc) {
		this.yediUc = yediUc;
	}

	/**
	 * @return the ucIki
	 */
	public String getUcIki() {
		return ucIki;
	}

	/**
	 * @param ucIki
	 *            the ucIki to set
	 */
	public void setUcIki(String ucIki) {
		this.ucIki = ucIki;
	}

	/**
	 * @return the partiNo
	 */
	public String getPartiNo() {
		return partiNo;
	}

	/**
	 * @param partiNo
	 *            the partiNo to set
	 */
	public void setPartiNo(String partiNo) {
		this.partiNo = partiNo;
	}

	/**
	 * @return the penetrasyonIc
	 */
	public BigDecimal getPenetrasyonIc() {
		return penetrasyonIc;
	}

	/**
	 * @param penetrasyonIc
	 *            the penetrasyonIc to set
	 */
	public void setPenetrasyonIc(BigDecimal penetrasyonIc) {
		this.penetrasyonIc = penetrasyonIc;
	}

	/**
	 * @return the penetrasyonDis
	 */
	public BigDecimal getPenetrasyonDis() {
		return penetrasyonDis;
	}

	/**
	 * @param penetrasyonDis
	 *            the penetrasyonDis to set
	 */
	public void setPenetrasyonDis(BigDecimal penetrasyonDis) {
		this.penetrasyonDis = penetrasyonDis;
	}

	/**
	 * @return the penetrasyon
	 */
	public BigDecimal getPenetrasyon() {
		return penetrasyon;
	}

	/**
	 * @param penetrasyon
	 *            the penetrasyon to set
	 */
	public void setPenetrasyon(BigDecimal penetrasyon) {
		this.penetrasyon = penetrasyon;
	}

}