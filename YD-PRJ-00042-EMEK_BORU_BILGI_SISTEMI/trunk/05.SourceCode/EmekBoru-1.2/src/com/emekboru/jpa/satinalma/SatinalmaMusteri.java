package com.emekboru.jpa.satinalma;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.Ulkeler;
import com.emekboru.jpa.stok.StokTanimlarMarka;

/**
 * The persistent class for the satinalma_musteri database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "SatinalmaMusteri.findAll", query = "SELECT r FROM SatinalmaMusteri r order by r.shortName") })
@Table(name = "satinalma_musteri")
public class SatinalmaMusteri implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SATINALMA_MUSTERI_ID_GENERATOR", sequenceName = "SATINALMA_MUSTERI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SATINALMA_MUSTERI_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(length = 2147483647)
	private String address;

	@Column(length = 2147483647, name = "city")
	private String city;

	@Column(name = "commercial_name", length = 2147483647)
	private String commercialName;

	@Column(name = "short_name", length = 2147483647)
	private String shortName;

	@Column(name = "ulke_id", insertable = false, updatable = false)
	private Integer ulkeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ulke_id", referencedColumnName = "ID", insertable = false)
	private Ulkeler ulkeler;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser user;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Transient
	private List<StokTanimlarMarka> markaList;

	@Column(name = "telefon")
	private String telefon;

	@Column(name = "fax")
	private String fax;

	@Column(name = "ilgili_kisi")
	private String ilgiliKisi;

	@Column(name = "mail")
	private String mail;

	@Column(length = 2147483647)
	private String note;

	public SatinalmaMusteri() {

		// ulkeler = new Ulkeler();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCommercialName() {
		return this.commercialName;
	}

	public void setCommercialName(String commercialName) {
		this.commercialName = commercialName;
	}

	public String getShortName() {
		return this.shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public Ulkeler getUlkeler() {
		return ulkeler;
	}

	public void setUlkeler(Ulkeler ulkeler) {
		this.ulkeler = ulkeler;
	}

	public Integer getEkleyenKullanici() {
		return ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public SystemUser getUser() {
		return user;
	}

	public void setUser(SystemUser user) {
		this.user = user;
	}

	public Timestamp getGuncellemeZamani() {
		return guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Timestamp getEklemeZamani() {
		return eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public List<StokTanimlarMarka> getMarkaList() {
		return markaList;
	}

	public void setMarkaList(List<StokTanimlarMarka> markaList) {
		this.markaList = markaList;
	}

	public String getIlgiliKisi() {
		return ilgiliKisi;
	}

	public void setIlgiliKisi(String ilgiliKisi) {
		this.ilgiliKisi = ilgiliKisi;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * @param mail
	 *            the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note
	 *            the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}

}