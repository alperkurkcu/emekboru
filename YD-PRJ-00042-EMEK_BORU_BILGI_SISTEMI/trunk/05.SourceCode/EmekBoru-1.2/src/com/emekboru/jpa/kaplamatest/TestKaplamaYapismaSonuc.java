package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.isolation.IsolationTestDefinition;

/**
 * The persistent class for the test_kaplama_yapisma_sonuc database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaYapismaSonuc.findAll", query = "SELECT r FROM TestKaplamaYapismaSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_kaplama_yapisma_sonuc")
public class TestKaplamaYapismaSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KAPLAMA_YAPISMA_SONUC_ID_GENERATOR", sequenceName = "TEST_KAPLAMA_YAPISMA_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KAPLAMA_YAPISMA_SONUC_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Column(name = "aciklama", length = 255)
	private String aciklama;

	// @Column(name = "destructive_test_id", nullable = false)
	// private Integer destructiveTestId;

	@Column(name = "dokuman_kayit_adi", length = 2147483647)
	private String dokumanKayitAdi;

	@Column(name = "dokuman_orijinal_adi", length = 2147483647)
	private String dokumanOrijinalAdi;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	// @Column(name = "global_id", nullable = false, insertable = false,
	// updatable = false)
	// private Integer globalId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "olculen_deger")
	private Integer olculenDeger;

	// @Column(name = "pipe_id", nullable = false)
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@Column(nullable = false)
	private Boolean sonuc;

	@Column(name = "test_sicakligi")
	private Integer testSicakligi;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi", nullable = false)
	private Date testTarihi;

	@Column(name = "test_bekleme_Suresi")
	private Integer testBeklemeSuresi;

	@Column(name = "band_type")
	private Integer bandType;

	@Column(name = "birim")
	private Integer birim;

	@Column(name = "test_hizi")
	private BigDecimal testHizi;

	@Column(name = "test_parcasi_olcusu")
	private BigDecimal testParcasiOlcusu;

	public TestKaplamaYapismaSonuc() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public String getDokumanKayitAdi() {
		return this.dokumanKayitAdi;
	}

	public void setDokumanKayitAdi(String dokumanKayitAdi) {
		this.dokumanKayitAdi = dokumanKayitAdi;
	}

	public String getDokumanOrijinalAdi() {
		return this.dokumanOrijinalAdi;
	}

	public void setDokumanOrijinalAdi(String dokumanOrijinalAdi) {
		this.dokumanOrijinalAdi = dokumanOrijinalAdi;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Integer getOlculenDeger() {
		return this.olculenDeger;
	}

	public void setOlculenDeger(Integer olculenDeger) {
		this.olculenDeger = olculenDeger;
	}

	public Boolean getSonuc() {
		return this.sonuc;
	}

	public void setSonuc(Boolean sonuc) {
		this.sonuc = sonuc;
	}

	public Integer getTestSicakligi() {
		return this.testSicakligi;
	}

	public void setTestSicakligi(Integer testSicakligi) {
		this.testSicakligi = testSicakligi;
	}

	public Date getTestTarihi() {
		return this.testTarihi;
	}

	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}

	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}

	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the testBeklemeSuresi
	 */
	public Integer getTestBeklemeSuresi() {
		return testBeklemeSuresi;
	}

	/**
	 * @param testBeklemeSuresi
	 *            the testBeklemeSuresi to set
	 */
	public void setTestBeklemeSuresi(Integer testBeklemeSuresi) {
		this.testBeklemeSuresi = testBeklemeSuresi;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the bandType
	 */
	public Integer getBandType() {
		return bandType;
	}

	/**
	 * @param bandType
	 *            the bandType to set
	 */
	public void setBandType(Integer bandType) {
		this.bandType = bandType;
	}

	/**
	 * @return the birim
	 */
	public Integer getBirim() {
		return birim;
	}

	/**
	 * @param birim
	 *            the birim to set
	 */
	public void setBirim(Integer birim) {
		this.birim = birim;
	}

	/**
	 * @return the testHizi
	 */
	public BigDecimal getTestHizi() {
		return testHizi;
	}

	/**
	 * @param testHizi
	 *            the testHizi to set
	 */
	public void setTestHizi(BigDecimal testHizi) {
		this.testHizi = testHizi;
	}

	/**
	 * @return the testParcasiOlcusu
	 */
	public BigDecimal getTestParcasiOlcusu() {
		return testParcasiOlcusu;
	}

	/**
	 * @param testParcasiOlcusu
	 *            the testParcasiOlcusu to set
	 */
	public void setTestParcasiOlcusu(BigDecimal testParcasiOlcusu) {
		this.testParcasiOlcusu = testParcasiOlcusu;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

}