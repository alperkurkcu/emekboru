package com.emekboru.jpa.kalibrasyon;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the kalibrasyon_otomatik_ultrasonik database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "KalibrasyonOtomatikUltrasonik.findAll", query = "SELECT r FROM KalibrasyonOtomatikUltrasonik r where r.onlineOffline=:prmOnlineOffline order by r.kalibrasyonZamani DESC") })
@Table(name = "kalibrasyon_otomatik_ultrasonik")
public class KalibrasyonOtomatikUltrasonik implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "KALIBRASYON_OTOMATIK_ULTRASONIK_ID_GENERATOR", sequenceName = "KALIBRASYON_OTOMATIK_ULTRASONIK_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "KALIBRASYON_OTOMATIK_ULTRASONIK_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "aciklama")
	private String aciklama;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "ilave_kazanc_degeri_alti")
	private String ilaveKazancDegeriAlti;

	@Column(name = "ilave_kazanc_degeri_bes")
	private String ilaveKazancDegeriBes;

	@Column(name = "ilave_kazanc_degeri_bir")
	private String ilaveKazancDegeriBir;

	@Column(name = "ilave_kazanc_degeri_dokuz")
	private String ilaveKazancDegeriDokuz;

	@Column(name = "ilave_kazanc_degeri_dort")
	private String ilaveKazancDegeriDort;

	@Column(name = "ilave_kazanc_degeri_iki")
	private String ilaveKazancDegeriIki;

	@Column(name = "ilave_kazanc_degeri_on")
	private String ilaveKazancDegeriOn;

	@Column(name = "ilave_kazanc_degeri_onalti")
	private String ilaveKazancDegeriOnalti;

	@Column(name = "ilave_kazanc_degeri_onbes")
	private String ilaveKazancDegeriOnbes;

	@Column(name = "ilave_kazanc_degeri_onbir")
	private String ilaveKazancDegeriOnbir;

	@Column(name = "ilave_kazanc_degeri_ondort")
	private String ilaveKazancDegeriOndort;

	@Column(name = "ilave_kazanc_degeri_oniki")
	private String ilaveKazancDegeriOniki;

	@Column(name = "ilave_kazanc_degeri_onuc")
	private String ilaveKazancDegeriOnuc;

	@Column(name = "ilave_kazanc_degeri_sekiz")
	private String ilaveKazancDegeriSekiz;

	@Column(name = "ilave_kazanc_degeri_uc")
	private String ilaveKazancDegeriUc;

	@Column(name = "ilave_kazanc_degeri_yedi")
	private String ilaveKazancDegeriYedi;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "kalibrasyon_zamani")
	private Date kalibrasyonZamani;

	@Column(name = "kalibrasyonu_yapan")
	private String kalibrasyonuYapan;

	@Column(name = "kayit_kazanc_degeri_alti")
	private String kayitKazancDegeriAlti;

	@Column(name = "kayit_kazanc_degeri_bes")
	private String kayitKazancDegeriBes;

	@Column(name = "kayit_kazanc_degeri_bir")
	private String kayitKazancDegeriBir;

	@Column(name = "kayit_kazanc_degeri_dokuz")
	private String kayitKazancDegeriDokuz;

	@Column(name = "kayit_kazanc_degeri_dort")
	private String kayitKazancDegeriDort;

	@Column(name = "kayit_kazanc_degeri_iki")
	private String kayitKazancDegeriIki;

	@Column(name = "kayit_kazanc_degeri_on")
	private String kayitKazancDegeriOn;

	@Column(name = "kayit_kazanc_degeri_onalti")
	private String kayitKazancDegeriOnalti;

	@Column(name = "kayit_kazanc_degeri_onbes")
	private String kayitKazancDegeriOnbes;

	@Column(name = "kayit_kazanc_degeri_onbir")
	private String kayitKazancDegeriOnbir;

	@Column(name = "kayit_kazanc_degeri_ondort")
	private String kayitKazancDegeriOndort;

	@Column(name = "kayit_kazanc_degeri_oniki")
	private String kayitKazancDegeriOniki;

	@Column(name = "kayit_kazanc_degeri_onuc")
	private String kayitKazancDegeriOnuc;

	@Column(name = "kayit_kazanc_degeri_sekiz")
	private String kayitKazancDegeriSekiz;

	@Column(name = "kayit_kazanc_degeri_uc")
	private String kayitKazancDegeriUc;

	@Column(name = "kayit_kazanc_degeri_yedi")
	private String kayitKazancDegeriYedi;

	@Column(name = "kayit_seviyesi_alti")
	private String kayitSeviyesiAlti;

	@Column(name = "kayit_seviyesi_bes")
	private String kayitSeviyesiBes;

	@Column(name = "kayit_seviyesi_bir")
	private String kayitSeviyesiBir;

	@Column(name = "kayit_seviyesi_dokuz")
	private String kayitSeviyesiDokuz;

	@Column(name = "kayit_seviyesi_dort")
	private String kayitSeviyesiDort;

	@Column(name = "kayit_seviyesi_iki")
	private String kayitSeviyesiIki;

	@Column(name = "kayit_seviyesi_on")
	private String kayitSeviyesiOn;

	@Column(name = "kayit_seviyesi_onalti")
	private String kayitSeviyesiOnalti;

	@Column(name = "kayit_seviyesi_onbes")
	private String kayitSeviyesiOnbes;

	@Column(name = "kayit_seviyesi_onbir")
	private String kayitSeviyesiOnbir;

	@Column(name = "kayit_seviyesi_ondort")
	private String kayitSeviyesiOndort;

	@Column(name = "kayit_seviyesi_oniki")
	private String kayitSeviyesiOniki;

	@Column(name = "kayit_seviyesi_onuc")
	private String kayitSeviyesiOnuc;

	@Column(name = "kayit_seviyesi_sekiz")
	private String kayitSeviyesiSekiz;

	@Column(name = "kayit_seviyesi_uc")
	private String kayitSeviyesiUc;

	@Column(name = "kayit_seviyesi_yedi")
	private String kayitSeviyesiYedi;

	@Column(name = "online_offline")
	private Integer onlineOffline;

	@Column(name = "prob_capi_frekansi_alti")
	private String probCapiFrekansiAlti;

	@Column(name = "prob_capi_frekansi_bes")
	private String probCapiFrekansiBes;

	@Column(name = "prob_capi_frekansi_bir")
	private String probCapiFrekansiBir;

	@Column(name = "prob_capi_frekansi_dokuz")
	private String probCapiFrekansiDokuz;

	@Column(name = "prob_capi_frekansi_dort")
	private String probCapiFrekansiDort;

	@Column(name = "prob_capi_frekansi_iki")
	private String probCapiFrekansiIki;

	@Column(name = "prob_capi_frekansi_on")
	private String probCapiFrekansiOn;

	@Column(name = "prob_capi_frekansi_onalti")
	private String probCapiFrekansiOnalti;

	@Column(name = "prob_capi_frekansi_onbes")
	private String probCapiFrekansiOnbes;

	@Column(name = "prob_capi_frekansi_onbir")
	private String probCapiFrekansiOnbir;

	@Column(name = "prob_capi_frekansi_ondort")
	private String probCapiFrekansiOndort;

	@Column(name = "prob_capi_frekansi_oniki")
	private String probCapiFrekansiOniki;

	@Column(name = "prob_capi_frekansi_onuc")
	private String probCapiFrekansiOnuc;

	@Column(name = "prob_capi_frekansi_sekiz")
	private String probCapiFrekansiSekiz;

	@Column(name = "prob_capi_frekansi_uc")
	private String probCapiFrekansiUc;

	@Column(name = "prob_capi_frekansi_yedi")
	private String probCapiFrekansiYedi;

	@Column(name = "referans_centik_delik_alti")
	private String referansCentikDelikAlti;

	@Column(name = "referans_centik_delik_bes")
	private String referansCentikDelikBes;

	@Column(name = "referans_centik_delik_bir")
	private String referansCentikDelikBir;

	@Column(name = "referans_centik_delik_dokuz")
	private String referansCentikDelikDokuz;

	@Column(name = "referans_centik_delik_dort")
	private String referansCentikDelikDort;

	@Column(name = "referans_centik_delik_iki")
	private String referansCentikDelikIki;

	@Column(name = "referans_centik_delik_on")
	private String referansCentikDelikOn;

	@Column(name = "referans_centik_delik_onalti")
	private String referansCentikDelikOnalti;

	@Column(name = "referans_centik_delik_onbes")
	private String referansCentikDelikOnbes;

	@Column(name = "referans_centik_delik_onbir")
	private String referansCentikDelikOnbir;

	@Column(name = "referans_centik_delik_ondort")
	private String referansCentikDelikOndort;

	@Column(name = "referans_centik_delik_oniki")
	private String referansCentikDelikOniki;

	@Column(name = "referans_centik_delik_onuc")
	private String referansCentikDelikOnuc;

	@Column(name = "referans_centik_delik_sekiz")
	private String referansCentikDelikSekiz;

	@Column(name = "referans_centik_delik_uc")
	private String referansCentikDelikUc;

	@Column(name = "referans_centik_delik_yedi")
	private String referansCentikDelikYedi;

	@Column(name = "referans_seviye_alti")
	private String referansSeviyeAlti;

	@Column(name = "referans_seviye_bes")
	private String referansSeviyeBes;

	@Column(name = "referans_seviye_bir")
	private String referansSeviyeBir;

	@Column(name = "referans_seviye_dokuz")
	private String referansSeviyeDokuz;

	@Column(name = "referans_seviye_dort")
	private String referansSeviyeDort;

	@Column(name = "referans_seviye_iki")
	private String referansSeviyeIki;

	@Column(name = "referans_seviye_on")
	private String referansSeviyeOn;

	@Column(name = "referans_seviye_onalti")
	private String referansSeviyeOnalti;

	@Column(name = "referans_seviye_onbes")
	private String referansSeviyeOnbes;

	@Column(name = "referans_seviye_onbir")
	private String referansSeviyeOnbir;

	@Column(name = "referans_seviye_ondort")
	private String referansSeviyeOndort;

	@Column(name = "referans_seviye_oniki")
	private String referansSeviyeOniki;

	@Column(name = "referans_seviye_onuc")
	private String referansSeviyeOnuc;

	@Column(name = "referans_seviye_sekiz")
	private String referansSeviyeSekiz;

	@Column(name = "referans_seviye_uc")
	private String referansSeviyeUc;

	@Column(name = "referans_seviye_yedi")
	private String referansSeviyeYedi;

	@Column(name = "kalibrasyon_mesafesi_alti")
	private String kalibrasyonMesafesiAlti;

	@Column(name = "kalibrasyon_mesafesi_bes")
	private String kalibrasyonMesafesiBes;

	@Column(name = "kalibrasyon_mesafesi_bir")
	private String kalibrasyonMesafesiBir;

	@Column(name = "kalibrasyon_mesafesi_dokuz")
	private String kalibrasyonMesafesiDokuz;

	@Column(name = "kalibrasyon_mesafesi_dort")
	private String kalibrasyonMesafesiDort;

	@Column(name = "kalibrasyon_mesafesi_iki")
	private String kalibrasyonMesafesiIki;

	@Column(name = "kalibrasyon_mesafesi_on")
	private String kalibrasyonMesafesiOn;

	@Column(name = "kalibrasyon_mesafesi_onalti")
	private String kalibrasyonMesafesiOnalti;

	@Column(name = "kalibrasyon_mesafesi_onbes")
	private String kalibrasyonMesafesiOnbes;

	@Column(name = "kalibrasyon_mesafesi_onbir")
	private String kalibrasyonMesafesiOnbir;

	@Column(name = "kalibrasyon_mesafesi_ondort")
	private String kalibrasyonMesafesiOndort;

	@Column(name = "kalibrasyon_mesafesi_oniki")
	private String kalibrasyonMesafesiOniki;

	@Column(name = "kalibrasyon_mesafesi_onuc")
	private String kalibrasyonMesafesiOnuc;

	@Column(name = "kalibrasyon_mesafesi_sekiz")
	private String kalibrasyonMesafesiSekiz;

	@Column(name = "kalibrasyon_mesafesi_uc")
	private String kalibrasyonMesafesiUc;

	@Column(name = "kalibrasyon_mesafesi_yedi")
	private String kalibrasyonMesafesiYedi;

	@Column(name = "tarama_hizi_alti")
	private String taramaHiziAlti;

	@Column(name = "tarama_hizi_bes")
	private String taramaHiziBes;

	@Column(name = "tarama_hizi_bir")
	private String taramaHiziBir;

	@Column(name = "tarama_hizi_dokuz")
	private String taramaHiziDokuz;

	@Column(name = "tarama_hizi_dort")
	private String taramaHiziDort;

	@Column(name = "tarama_hizi_iki")
	private String taramaHiziIki;

	@Column(name = "tarama_hizi_on")
	private String taramaHiziOn;

	@Column(name = "tarama_hizi_onalti")
	private String taramaHiziOnalti;

	@Column(name = "tarama_hizi_onbes")
	private String taramaHiziOnbes;

	@Column(name = "tarama_hizi_onbir")
	private String taramaHiziOnbir;

	@Column(name = "tarama_hizi_ondort")
	private String taramaHiziOndort;

	@Column(name = "tarama_hizi_oniki")
	private String taramaHiziOniki;

	@Column(name = "tarama_hizi_onuc")
	private String taramaHiziOnuc;

	@Column(name = "tarama_hizi_sekiz")
	private String taramaHiziSekiz;

	@Column(name = "tarama_hizi_uc")
	private String taramaHiziUc;

	@Column(name = "tarama_hizi_yedi")
	private String taramaHiziYedi;

	@Column(name = "cihaz")
	private String cihaz;

	@Column(name = "kalibrasyon_blogu_bir")
	private String kalibrasyonBloguBir;

	@Column(name = "kalibrasyon_blogu_iki")
	private String kalibrasyonBloguIki;

	@Column(name = "kalibrasyon_blogu_uc")
	private String kalibrasyonBloguUc;

	@Column(name = "kalibrasyon_blogu_dort")
	private String kalibrasyonBloguDort;

	@Column(name = "kalibrasyon_blogu_bes")
	private String kalibrasyonBloguBes;

	@Column(name = "kalibrasyon_blogu_alti")
	private String kalibrasyonBloguAlti;

	@Column(name = "kalibrasyon_blogu_yedi")
	private String kalibrasyonBloguYedi;

	@Column(name = "kalibrasyon_blogu_sekiz")
	private String kalibrasyonBloguSekiz;

	@Column(name = "kalibrasyon_blogu_dokuz")
	private String kalibrasyonBloguDokuz;

	@Column(name = "kalibrasyon_blogu_on")
	private String kalibrasyonBloguOn;

	@Column(name = "kalibrasyon_blogu_onbir")
	private String kalibrasyonBloguOnbir;

	@Column(name = "kalibrasyon_blogu_oniki")
	private String kalibrasyonBloguOniki;

	@Column(name = "kalibrasyon_blogu_onuc")
	private String kalibrasyonBloguOnuc;

	@Column(name = "kalibrasyon_blogu_ondort")
	private String kalibrasyonBloguOndort;

	@Column(name = "kalibrasyon_blogu_onbes")
	private String kalibrasyonBloguOnbes;

	@Column(name = "kalibrasyon_blogu_onalti")
	private String kalibrasyonBloguOnalti;

	public KalibrasyonOtomatikUltrasonik() {
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getIlaveKazancDegeriAlti() {
		return this.ilaveKazancDegeriAlti;
	}

	public void setIlaveKazancDegeriAlti(String ilaveKazancDegeriAlti) {
		this.ilaveKazancDegeriAlti = ilaveKazancDegeriAlti;
	}

	public String getIlaveKazancDegeriBes() {
		return this.ilaveKazancDegeriBes;
	}

	public void setIlaveKazancDegeriBes(String ilaveKazancDegeriBes) {
		this.ilaveKazancDegeriBes = ilaveKazancDegeriBes;
	}

	public String getIlaveKazancDegeriBir() {
		return this.ilaveKazancDegeriBir;
	}

	public void setIlaveKazancDegeriBir(String ilaveKazancDegeriBir) {
		this.ilaveKazancDegeriBir = ilaveKazancDegeriBir;
	}

	public String getIlaveKazancDegeriDokuz() {
		return this.ilaveKazancDegeriDokuz;
	}

	public void setIlaveKazancDegeriDokuz(String ilaveKazancDegeriDokuz) {
		this.ilaveKazancDegeriDokuz = ilaveKazancDegeriDokuz;
	}

	public String getIlaveKazancDegeriDort() {
		return this.ilaveKazancDegeriDort;
	}

	public void setIlaveKazancDegeriDort(String ilaveKazancDegeriDort) {
		this.ilaveKazancDegeriDort = ilaveKazancDegeriDort;
	}

	public String getIlaveKazancDegeriIki() {
		return this.ilaveKazancDegeriIki;
	}

	public void setIlaveKazancDegeriIki(String ilaveKazancDegeriIki) {
		this.ilaveKazancDegeriIki = ilaveKazancDegeriIki;
	}

	public String getIlaveKazancDegeriOn() {
		return this.ilaveKazancDegeriOn;
	}

	public void setIlaveKazancDegeriOn(String ilaveKazancDegeriOn) {
		this.ilaveKazancDegeriOn = ilaveKazancDegeriOn;
	}

	public String getIlaveKazancDegeriOnalti() {
		return this.ilaveKazancDegeriOnalti;
	}

	public void setIlaveKazancDegeriOnalti(String ilaveKazancDegeriOnalti) {
		this.ilaveKazancDegeriOnalti = ilaveKazancDegeriOnalti;
	}

	public String getIlaveKazancDegeriOnbes() {
		return this.ilaveKazancDegeriOnbes;
	}

	public void setIlaveKazancDegeriOnbes(String ilaveKazancDegeriOnbes) {
		this.ilaveKazancDegeriOnbes = ilaveKazancDegeriOnbes;
	}

	public String getIlaveKazancDegeriOnbir() {
		return this.ilaveKazancDegeriOnbir;
	}

	public void setIlaveKazancDegeriOnbir(String ilaveKazancDegeriOnbir) {
		this.ilaveKazancDegeriOnbir = ilaveKazancDegeriOnbir;
	}

	public String getIlaveKazancDegeriOndort() {
		return this.ilaveKazancDegeriOndort;
	}

	public void setIlaveKazancDegeriOndort(String ilaveKazancDegeriOndort) {
		this.ilaveKazancDegeriOndort = ilaveKazancDegeriOndort;
	}

	public String getIlaveKazancDegeriOniki() {
		return this.ilaveKazancDegeriOniki;
	}

	public void setIlaveKazancDegeriOniki(String ilaveKazancDegeriOniki) {
		this.ilaveKazancDegeriOniki = ilaveKazancDegeriOniki;
	}

	public String getIlaveKazancDegeriOnuc() {
		return this.ilaveKazancDegeriOnuc;
	}

	public void setIlaveKazancDegeriOnuc(String ilaveKazancDegeriOnuc) {
		this.ilaveKazancDegeriOnuc = ilaveKazancDegeriOnuc;
	}

	public String getIlaveKazancDegeriSekiz() {
		return this.ilaveKazancDegeriSekiz;
	}

	public void setIlaveKazancDegeriSekiz(String ilaveKazancDegeriSekiz) {
		this.ilaveKazancDegeriSekiz = ilaveKazancDegeriSekiz;
	}

	public String getIlaveKazancDegeriUc() {
		return this.ilaveKazancDegeriUc;
	}

	public void setIlaveKazancDegeriUc(String ilaveKazancDegeriUc) {
		this.ilaveKazancDegeriUc = ilaveKazancDegeriUc;
	}

	public String getIlaveKazancDegeriYedi() {
		return this.ilaveKazancDegeriYedi;
	}

	public void setIlaveKazancDegeriYedi(String ilaveKazancDegeriYedi) {
		this.ilaveKazancDegeriYedi = ilaveKazancDegeriYedi;
	}

	public String getKalibrasyonuYapan() {
		return this.kalibrasyonuYapan;
	}

	public void setKalibrasyonuYapan(String kalibrasyonuYapan) {
		this.kalibrasyonuYapan = kalibrasyonuYapan;
	}

	public String getKayitKazancDegeriAlti() {
		return this.kayitKazancDegeriAlti;
	}

	public void setKayitKazancDegeriAlti(String kayitKazancDegeriAlti) {
		this.kayitKazancDegeriAlti = kayitKazancDegeriAlti;
	}

	public String getKayitKazancDegeriBes() {
		return this.kayitKazancDegeriBes;
	}

	public void setKayitKazancDegeriBes(String kayitKazancDegeriBes) {
		this.kayitKazancDegeriBes = kayitKazancDegeriBes;
	}

	public String getKayitKazancDegeriBir() {
		return this.kayitKazancDegeriBir;
	}

	public void setKayitKazancDegeriBir(String kayitKazancDegeriBir) {
		this.kayitKazancDegeriBir = kayitKazancDegeriBir;
	}

	public String getKayitKazancDegeriDokuz() {
		return this.kayitKazancDegeriDokuz;
	}

	public void setKayitKazancDegeriDokuz(String kayitKazancDegeriDokuz) {
		this.kayitKazancDegeriDokuz = kayitKazancDegeriDokuz;
	}

	public String getKayitKazancDegeriDort() {
		return this.kayitKazancDegeriDort;
	}

	public void setKayitKazancDegeriDort(String kayitKazancDegeriDort) {
		this.kayitKazancDegeriDort = kayitKazancDegeriDort;
	}

	public String getKayitKazancDegeriIki() {
		return this.kayitKazancDegeriIki;
	}

	public void setKayitKazancDegeriIki(String kayitKazancDegeriIki) {
		this.kayitKazancDegeriIki = kayitKazancDegeriIki;
	}

	public String getKayitKazancDegeriOn() {
		return this.kayitKazancDegeriOn;
	}

	public void setKayitKazancDegeriOn(String kayitKazancDegeriOn) {
		this.kayitKazancDegeriOn = kayitKazancDegeriOn;
	}

	public String getKayitKazancDegeriOnalti() {
		return this.kayitKazancDegeriOnalti;
	}

	public void setKayitKazancDegeriOnalti(String kayitKazancDegeriOnalti) {
		this.kayitKazancDegeriOnalti = kayitKazancDegeriOnalti;
	}

	public String getKayitKazancDegeriOnbes() {
		return this.kayitKazancDegeriOnbes;
	}

	public void setKayitKazancDegeriOnbes(String kayitKazancDegeriOnbes) {
		this.kayitKazancDegeriOnbes = kayitKazancDegeriOnbes;
	}

	public String getKayitKazancDegeriOnbir() {
		return this.kayitKazancDegeriOnbir;
	}

	public void setKayitKazancDegeriOnbir(String kayitKazancDegeriOnbir) {
		this.kayitKazancDegeriOnbir = kayitKazancDegeriOnbir;
	}

	public String getKayitKazancDegeriOndort() {
		return this.kayitKazancDegeriOndort;
	}

	public void setKayitKazancDegeriOndort(String kayitKazancDegeriOndort) {
		this.kayitKazancDegeriOndort = kayitKazancDegeriOndort;
	}

	public String getKayitKazancDegeriOniki() {
		return this.kayitKazancDegeriOniki;
	}

	public void setKayitKazancDegeriOniki(String kayitKazancDegeriOniki) {
		this.kayitKazancDegeriOniki = kayitKazancDegeriOniki;
	}

	public String getKayitKazancDegeriOnuc() {
		return this.kayitKazancDegeriOnuc;
	}

	public void setKayitKazancDegeriOnuc(String kayitKazancDegeriOnuc) {
		this.kayitKazancDegeriOnuc = kayitKazancDegeriOnuc;
	}

	public String getKayitKazancDegeriSekiz() {
		return this.kayitKazancDegeriSekiz;
	}

	public void setKayitKazancDegeriSekiz(String kayitKazancDegeriSekiz) {
		this.kayitKazancDegeriSekiz = kayitKazancDegeriSekiz;
	}

	public String getKayitKazancDegeriUc() {
		return this.kayitKazancDegeriUc;
	}

	public void setKayitKazancDegeriUc(String kayitKazancDegeriUc) {
		this.kayitKazancDegeriUc = kayitKazancDegeriUc;
	}

	public String getKayitKazancDegeriYedi() {
		return this.kayitKazancDegeriYedi;
	}

	public void setKayitKazancDegeriYedi(String kayitKazancDegeriYedi) {
		this.kayitKazancDegeriYedi = kayitKazancDegeriYedi;
	}

	public String getKayitSeviyesiAlti() {
		return this.kayitSeviyesiAlti;
	}

	public void setKayitSeviyesiAlti(String kayitSeviyesiAlti) {
		this.kayitSeviyesiAlti = kayitSeviyesiAlti;
	}

	public String getKayitSeviyesiBes() {
		return this.kayitSeviyesiBes;
	}

	public void setKayitSeviyesiBes(String kayitSeviyesiBes) {
		this.kayitSeviyesiBes = kayitSeviyesiBes;
	}

	public String getKayitSeviyesiBir() {
		return this.kayitSeviyesiBir;
	}

	public void setKayitSeviyesiBir(String kayitSeviyesiBir) {
		this.kayitSeviyesiBir = kayitSeviyesiBir;
	}

	public String getKayitSeviyesiDokuz() {
		return this.kayitSeviyesiDokuz;
	}

	public void setKayitSeviyesiDokuz(String kayitSeviyesiDokuz) {
		this.kayitSeviyesiDokuz = kayitSeviyesiDokuz;
	}

	public String getKayitSeviyesiDort() {
		return this.kayitSeviyesiDort;
	}

	public void setKayitSeviyesiDort(String kayitSeviyesiDort) {
		this.kayitSeviyesiDort = kayitSeviyesiDort;
	}

	public String getKayitSeviyesiIki() {
		return this.kayitSeviyesiIki;
	}

	public void setKayitSeviyesiIki(String kayitSeviyesiIki) {
		this.kayitSeviyesiIki = kayitSeviyesiIki;
	}

	public String getKayitSeviyesiOn() {
		return this.kayitSeviyesiOn;
	}

	public void setKayitSeviyesiOn(String kayitSeviyesiOn) {
		this.kayitSeviyesiOn = kayitSeviyesiOn;
	}

	public String getKayitSeviyesiOnalti() {
		return this.kayitSeviyesiOnalti;
	}

	public void setKayitSeviyesiOnalti(String kayitSeviyesiOnalti) {
		this.kayitSeviyesiOnalti = kayitSeviyesiOnalti;
	}

	public String getKayitSeviyesiOnbes() {
		return this.kayitSeviyesiOnbes;
	}

	public void setKayitSeviyesiOnbes(String kayitSeviyesiOnbes) {
		this.kayitSeviyesiOnbes = kayitSeviyesiOnbes;
	}

	public String getKayitSeviyesiOnbir() {
		return this.kayitSeviyesiOnbir;
	}

	public void setKayitSeviyesiOnbir(String kayitSeviyesiOnbir) {
		this.kayitSeviyesiOnbir = kayitSeviyesiOnbir;
	}

	public String getKayitSeviyesiOndort() {
		return this.kayitSeviyesiOndort;
	}

	public void setKayitSeviyesiOndort(String kayitSeviyesiOndort) {
		this.kayitSeviyesiOndort = kayitSeviyesiOndort;
	}

	public String getKayitSeviyesiOniki() {
		return this.kayitSeviyesiOniki;
	}

	public void setKayitSeviyesiOniki(String kayitSeviyesiOniki) {
		this.kayitSeviyesiOniki = kayitSeviyesiOniki;
	}

	public String getKayitSeviyesiOnuc() {
		return this.kayitSeviyesiOnuc;
	}

	public void setKayitSeviyesiOnuc(String kayitSeviyesiOnuc) {
		this.kayitSeviyesiOnuc = kayitSeviyesiOnuc;
	}

	public String getKayitSeviyesiSekiz() {
		return this.kayitSeviyesiSekiz;
	}

	public void setKayitSeviyesiSekiz(String kayitSeviyesiSekiz) {
		this.kayitSeviyesiSekiz = kayitSeviyesiSekiz;
	}

	public String getKayitSeviyesiUc() {
		return this.kayitSeviyesiUc;
	}

	public void setKayitSeviyesiUc(String kayitSeviyesiUc) {
		this.kayitSeviyesiUc = kayitSeviyesiUc;
	}

	public String getKayitSeviyesiYedi() {
		return this.kayitSeviyesiYedi;
	}

	public void setKayitSeviyesiYedi(String kayitSeviyesiYedi) {
		this.kayitSeviyesiYedi = kayitSeviyesiYedi;
	}

	public Integer getOnlineOffline() {
		return this.onlineOffline;
	}

	public void setOnlineOffline(Integer onlineOffline) {
		this.onlineOffline = onlineOffline;
	}

	public String getProbCapiFrekansiAlti() {
		return this.probCapiFrekansiAlti;
	}

	public void setProbCapiFrekansiAlti(String probCapiFrekansiAlti) {
		this.probCapiFrekansiAlti = probCapiFrekansiAlti;
	}

	public String getProbCapiFrekansiBes() {
		return this.probCapiFrekansiBes;
	}

	public void setProbCapiFrekansiBes(String probCapiFrekansiBes) {
		this.probCapiFrekansiBes = probCapiFrekansiBes;
	}

	public String getProbCapiFrekansiBir() {
		return this.probCapiFrekansiBir;
	}

	public void setProbCapiFrekansiBir(String probCapiFrekansiBir) {
		this.probCapiFrekansiBir = probCapiFrekansiBir;
	}

	public String getProbCapiFrekansiDokuz() {
		return this.probCapiFrekansiDokuz;
	}

	public void setProbCapiFrekansiDokuz(String probCapiFrekansiDokuz) {
		this.probCapiFrekansiDokuz = probCapiFrekansiDokuz;
	}

	public String getProbCapiFrekansiDort() {
		return this.probCapiFrekansiDort;
	}

	public void setProbCapiFrekansiDort(String probCapiFrekansiDort) {
		this.probCapiFrekansiDort = probCapiFrekansiDort;
	}

	public String getProbCapiFrekansiIki() {
		return this.probCapiFrekansiIki;
	}

	public void setProbCapiFrekansiIki(String probCapiFrekansiIki) {
		this.probCapiFrekansiIki = probCapiFrekansiIki;
	}

	public String getProbCapiFrekansiOn() {
		return this.probCapiFrekansiOn;
	}

	public void setProbCapiFrekansiOn(String probCapiFrekansiOn) {
		this.probCapiFrekansiOn = probCapiFrekansiOn;
	}

	public String getProbCapiFrekansiOnalti() {
		return this.probCapiFrekansiOnalti;
	}

	public void setProbCapiFrekansiOnalti(String probCapiFrekansiOnalti) {
		this.probCapiFrekansiOnalti = probCapiFrekansiOnalti;
	}

	public String getProbCapiFrekansiOnbes() {
		return this.probCapiFrekansiOnbes;
	}

	public void setProbCapiFrekansiOnbes(String probCapiFrekansiOnbes) {
		this.probCapiFrekansiOnbes = probCapiFrekansiOnbes;
	}

	public String getProbCapiFrekansiOnbir() {
		return this.probCapiFrekansiOnbir;
	}

	public void setProbCapiFrekansiOnbir(String probCapiFrekansiOnbir) {
		this.probCapiFrekansiOnbir = probCapiFrekansiOnbir;
	}

	public String getProbCapiFrekansiOndort() {
		return this.probCapiFrekansiOndort;
	}

	public void setProbCapiFrekansiOndort(String probCapiFrekansiOndort) {
		this.probCapiFrekansiOndort = probCapiFrekansiOndort;
	}

	public String getProbCapiFrekansiOniki() {
		return this.probCapiFrekansiOniki;
	}

	public void setProbCapiFrekansiOniki(String probCapiFrekansiOniki) {
		this.probCapiFrekansiOniki = probCapiFrekansiOniki;
	}

	public String getProbCapiFrekansiOnuc() {
		return this.probCapiFrekansiOnuc;
	}

	public void setProbCapiFrekansiOnuc(String probCapiFrekansiOnuc) {
		this.probCapiFrekansiOnuc = probCapiFrekansiOnuc;
	}

	public String getProbCapiFrekansiSekiz() {
		return this.probCapiFrekansiSekiz;
	}

	public void setProbCapiFrekansiSekiz(String probCapiFrekansiSekiz) {
		this.probCapiFrekansiSekiz = probCapiFrekansiSekiz;
	}

	public String getProbCapiFrekansiUc() {
		return this.probCapiFrekansiUc;
	}

	public void setProbCapiFrekansiUc(String probCapiFrekansiUc) {
		this.probCapiFrekansiUc = probCapiFrekansiUc;
	}

	public String getProbCapiFrekansiYedi() {
		return this.probCapiFrekansiYedi;
	}

	public void setProbCapiFrekansiYedi(String probCapiFrekansiYedi) {
		this.probCapiFrekansiYedi = probCapiFrekansiYedi;
	}

	public String getReferansCentikDelikAlti() {
		return this.referansCentikDelikAlti;
	}

	public void setReferansCentikDelikAlti(String referansCentikDelikAlti) {
		this.referansCentikDelikAlti = referansCentikDelikAlti;
	}

	public String getReferansCentikDelikBes() {
		return this.referansCentikDelikBes;
	}

	public void setReferansCentikDelikBes(String referansCentikDelikBes) {
		this.referansCentikDelikBes = referansCentikDelikBes;
	}

	public String getReferansCentikDelikBir() {
		return this.referansCentikDelikBir;
	}

	public void setReferansCentikDelikBir(String referansCentikDelikBir) {
		this.referansCentikDelikBir = referansCentikDelikBir;
	}

	public String getReferansCentikDelikDokuz() {
		return this.referansCentikDelikDokuz;
	}

	public void setReferansCentikDelikDokuz(String referansCentikDelikDokuz) {
		this.referansCentikDelikDokuz = referansCentikDelikDokuz;
	}

	public String getReferansCentikDelikDort() {
		return this.referansCentikDelikDort;
	}

	public void setReferansCentikDelikDort(String referansCentikDelikDort) {
		this.referansCentikDelikDort = referansCentikDelikDort;
	}

	public String getReferansCentikDelikIki() {
		return this.referansCentikDelikIki;
	}

	public void setReferansCentikDelikIki(String referansCentikDelikIki) {
		this.referansCentikDelikIki = referansCentikDelikIki;
	}

	public String getReferansCentikDelikOn() {
		return this.referansCentikDelikOn;
	}

	public void setReferansCentikDelikOn(String referansCentikDelikOn) {
		this.referansCentikDelikOn = referansCentikDelikOn;
	}

	public String getReferansCentikDelikOnalti() {
		return this.referansCentikDelikOnalti;
	}

	public void setReferansCentikDelikOnalti(String referansCentikDelikOnalti) {
		this.referansCentikDelikOnalti = referansCentikDelikOnalti;
	}

	public String getReferansCentikDelikOnbes() {
		return this.referansCentikDelikOnbes;
	}

	public void setReferansCentikDelikOnbes(String referansCentikDelikOnbes) {
		this.referansCentikDelikOnbes = referansCentikDelikOnbes;
	}

	public String getReferansCentikDelikOnbir() {
		return this.referansCentikDelikOnbir;
	}

	public void setReferansCentikDelikOnbir(String referansCentikDelikOnbir) {
		this.referansCentikDelikOnbir = referansCentikDelikOnbir;
	}

	public String getReferansCentikDelikOndort() {
		return this.referansCentikDelikOndort;
	}

	public void setReferansCentikDelikOndort(String referansCentikDelikOndort) {
		this.referansCentikDelikOndort = referansCentikDelikOndort;
	}

	public String getReferansCentikDelikOniki() {
		return this.referansCentikDelikOniki;
	}

	public void setReferansCentikDelikOniki(String referansCentikDelikOniki) {
		this.referansCentikDelikOniki = referansCentikDelikOniki;
	}

	public String getReferansCentikDelikOnuc() {
		return this.referansCentikDelikOnuc;
	}

	public void setReferansCentikDelikOnuc(String referansCentikDelikOnuc) {
		this.referansCentikDelikOnuc = referansCentikDelikOnuc;
	}

	public String getReferansCentikDelikSekiz() {
		return this.referansCentikDelikSekiz;
	}

	public void setReferansCentikDelikSekiz(String referansCentikDelikSekiz) {
		this.referansCentikDelikSekiz = referansCentikDelikSekiz;
	}

	public String getReferansCentikDelikUc() {
		return this.referansCentikDelikUc;
	}

	public void setReferansCentikDelikUc(String referansCentikDelikUc) {
		this.referansCentikDelikUc = referansCentikDelikUc;
	}

	public String getReferansCentikDelikYedi() {
		return this.referansCentikDelikYedi;
	}

	public void setReferansCentikDelikYedi(String referansCentikDelikYedi) {
		this.referansCentikDelikYedi = referansCentikDelikYedi;
	}

	public String getReferansSeviyeAlti() {
		return this.referansSeviyeAlti;
	}

	public void setReferansSeviyeAlti(String referansSeviyeAlti) {
		this.referansSeviyeAlti = referansSeviyeAlti;
	}

	public String getReferansSeviyeBes() {
		return this.referansSeviyeBes;
	}

	public void setReferansSeviyeBes(String referansSeviyeBes) {
		this.referansSeviyeBes = referansSeviyeBes;
	}

	public String getReferansSeviyeBir() {
		return this.referansSeviyeBir;
	}

	public void setReferansSeviyeBir(String referansSeviyeBir) {
		this.referansSeviyeBir = referansSeviyeBir;
	}

	public String getReferansSeviyeDokuz() {
		return this.referansSeviyeDokuz;
	}

	public void setReferansSeviyeDokuz(String referansSeviyeDokuz) {
		this.referansSeviyeDokuz = referansSeviyeDokuz;
	}

	public String getReferansSeviyeDort() {
		return this.referansSeviyeDort;
	}

	public void setReferansSeviyeDort(String referansSeviyeDort) {
		this.referansSeviyeDort = referansSeviyeDort;
	}

	public String getReferansSeviyeIki() {
		return this.referansSeviyeIki;
	}

	public void setReferansSeviyeIki(String referansSeviyeIki) {
		this.referansSeviyeIki = referansSeviyeIki;
	}

	public String getReferansSeviyeOn() {
		return this.referansSeviyeOn;
	}

	public void setReferansSeviyeOn(String referansSeviyeOn) {
		this.referansSeviyeOn = referansSeviyeOn;
	}

	public String getReferansSeviyeOnalti() {
		return this.referansSeviyeOnalti;
	}

	public void setReferansSeviyeOnalti(String referansSeviyeOnalti) {
		this.referansSeviyeOnalti = referansSeviyeOnalti;
	}

	public String getReferansSeviyeOnbes() {
		return this.referansSeviyeOnbes;
	}

	public void setReferansSeviyeOnbes(String referansSeviyeOnbes) {
		this.referansSeviyeOnbes = referansSeviyeOnbes;
	}

	public String getReferansSeviyeOnbir() {
		return this.referansSeviyeOnbir;
	}

	public void setReferansSeviyeOnbir(String referansSeviyeOnbir) {
		this.referansSeviyeOnbir = referansSeviyeOnbir;
	}

	public String getReferansSeviyeOndort() {
		return this.referansSeviyeOndort;
	}

	public void setReferansSeviyeOndort(String referansSeviyeOndort) {
		this.referansSeviyeOndort = referansSeviyeOndort;
	}

	public String getReferansSeviyeOniki() {
		return this.referansSeviyeOniki;
	}

	public void setReferansSeviyeOniki(String referansSeviyeOniki) {
		this.referansSeviyeOniki = referansSeviyeOniki;
	}

	public String getReferansSeviyeOnuc() {
		return this.referansSeviyeOnuc;
	}

	public void setReferansSeviyeOnuc(String referansSeviyeOnuc) {
		this.referansSeviyeOnuc = referansSeviyeOnuc;
	}

	public String getReferansSeviyeSekiz() {
		return this.referansSeviyeSekiz;
	}

	public void setReferansSeviyeSekiz(String referansSeviyeSekiz) {
		this.referansSeviyeSekiz = referansSeviyeSekiz;
	}

	public String getReferansSeviyeUc() {
		return this.referansSeviyeUc;
	}

	public void setReferansSeviyeUc(String referansSeviyeUc) {
		this.referansSeviyeUc = referansSeviyeUc;
	}

	public String getReferansSeviyeYedi() {
		return this.referansSeviyeYedi;
	}

	public void setReferansSeviyeYedi(String referansSeviyeYedi) {
		this.referansSeviyeYedi = referansSeviyeYedi;
	}

	/**
	 * @return the eklemeZamani
	 */
	public Date getEklemeZamani() {
		return eklemeZamani;
	}

	/**
	 * @param eklemeZamani
	 *            the eklemeZamani to set
	 */
	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncellemeZamani
	 */
	public Date getGuncellemeZamani() {
		return guncellemeZamani;
	}

	/**
	 * @param guncellemeZamani
	 *            the guncellemeZamani to set
	 */
	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the kalibrasyonZamani
	 */
	public Date getKalibrasyonZamani() {
		return kalibrasyonZamani;
	}

	/**
	 * @param kalibrasyonZamani
	 *            the kalibrasyonZamani to set
	 */
	public void setKalibrasyonZamani(Date kalibrasyonZamani) {
		this.kalibrasyonZamani = kalibrasyonZamani;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the kalibrasyonMesafesiAlti
	 */
	public String getKalibrasyonMesafesiAlti() {
		return kalibrasyonMesafesiAlti;
	}

	/**
	 * @param kalibrasyonMesafesiAlti
	 *            the kalibrasyonMesafesiAlti to set
	 */
	public void setKalibrasyonMesafesiAlti(String kalibrasyonMesafesiAlti) {
		this.kalibrasyonMesafesiAlti = kalibrasyonMesafesiAlti;
	}

	/**
	 * @return the kalibrasyonMesafesiBes
	 */
	public String getKalibrasyonMesafesiBes() {
		return kalibrasyonMesafesiBes;
	}

	/**
	 * @param kalibrasyonMesafesiBes
	 *            the kalibrasyonMesafesiBes to set
	 */
	public void setKalibrasyonMesafesiBes(String kalibrasyonMesafesiBes) {
		this.kalibrasyonMesafesiBes = kalibrasyonMesafesiBes;
	}

	/**
	 * @return the kalibrasyonMesafesiBir
	 */
	public String getKalibrasyonMesafesiBir() {
		return kalibrasyonMesafesiBir;
	}

	/**
	 * @param kalibrasyonMesafesiBir
	 *            the kalibrasyonMesafesiBir to set
	 */
	public void setKalibrasyonMesafesiBir(String kalibrasyonMesafesiBir) {
		this.kalibrasyonMesafesiBir = kalibrasyonMesafesiBir;
	}

	/**
	 * @return the kalibrasyonMesafesiDokuz
	 */
	public String getKalibrasyonMesafesiDokuz() {
		return kalibrasyonMesafesiDokuz;
	}

	/**
	 * @param kalibrasyonMesafesiDokuz
	 *            the kalibrasyonMesafesiDokuz to set
	 */
	public void setKalibrasyonMesafesiDokuz(String kalibrasyonMesafesiDokuz) {
		this.kalibrasyonMesafesiDokuz = kalibrasyonMesafesiDokuz;
	}

	/**
	 * @return the kalibrasyonMesafesiDort
	 */
	public String getKalibrasyonMesafesiDort() {
		return kalibrasyonMesafesiDort;
	}

	/**
	 * @param kalibrasyonMesafesiDort
	 *            the kalibrasyonMesafesiDort to set
	 */
	public void setKalibrasyonMesafesiDort(String kalibrasyonMesafesiDort) {
		this.kalibrasyonMesafesiDort = kalibrasyonMesafesiDort;
	}

	/**
	 * @return the kalibrasyonMesafesiIki
	 */
	public String getKalibrasyonMesafesiIki() {
		return kalibrasyonMesafesiIki;
	}

	/**
	 * @param kalibrasyonMesafesiIki
	 *            the kalibrasyonMesafesiIki to set
	 */
	public void setKalibrasyonMesafesiIki(String kalibrasyonMesafesiIki) {
		this.kalibrasyonMesafesiIki = kalibrasyonMesafesiIki;
	}

	/**
	 * @return the kalibrasyonMesafesiOn
	 */
	public String getKalibrasyonMesafesiOn() {
		return kalibrasyonMesafesiOn;
	}

	/**
	 * @param kalibrasyonMesafesiOn
	 *            the kalibrasyonMesafesiOn to set
	 */
	public void setKalibrasyonMesafesiOn(String kalibrasyonMesafesiOn) {
		this.kalibrasyonMesafesiOn = kalibrasyonMesafesiOn;
	}

	/**
	 * @return the kalibrasyonMesafesiOnalti
	 */
	public String getKalibrasyonMesafesiOnalti() {
		return kalibrasyonMesafesiOnalti;
	}

	/**
	 * @param kalibrasyonMesafesiOnalti
	 *            the kalibrasyonMesafesiOnalti to set
	 */
	public void setKalibrasyonMesafesiOnalti(String kalibrasyonMesafesiOnalti) {
		this.kalibrasyonMesafesiOnalti = kalibrasyonMesafesiOnalti;
	}

	/**
	 * @return the kalibrasyonMesafesiOnbes
	 */
	public String getKalibrasyonMesafesiOnbes() {
		return kalibrasyonMesafesiOnbes;
	}

	/**
	 * @param kalibrasyonMesafesiOnbes
	 *            the kalibrasyonMesafesiOnbes to set
	 */
	public void setKalibrasyonMesafesiOnbes(String kalibrasyonMesafesiOnbes) {
		this.kalibrasyonMesafesiOnbes = kalibrasyonMesafesiOnbes;
	}

	/**
	 * @return the kalibrasyonMesafesiOnbir
	 */
	public String getKalibrasyonMesafesiOnbir() {
		return kalibrasyonMesafesiOnbir;
	}

	/**
	 * @param kalibrasyonMesafesiOnbir
	 *            the kalibrasyonMesafesiOnbir to set
	 */
	public void setKalibrasyonMesafesiOnbir(String kalibrasyonMesafesiOnbir) {
		this.kalibrasyonMesafesiOnbir = kalibrasyonMesafesiOnbir;
	}

	/**
	 * @return the kalibrasyonMesafesiOndort
	 */
	public String getKalibrasyonMesafesiOndort() {
		return kalibrasyonMesafesiOndort;
	}

	/**
	 * @param kalibrasyonMesafesiOndort
	 *            the kalibrasyonMesafesiOndort to set
	 */
	public void setKalibrasyonMesafesiOndort(String kalibrasyonMesafesiOndort) {
		this.kalibrasyonMesafesiOndort = kalibrasyonMesafesiOndort;
	}

	/**
	 * @return the kalibrasyonMesafesiOniki
	 */
	public String getKalibrasyonMesafesiOniki() {
		return kalibrasyonMesafesiOniki;
	}

	/**
	 * @param kalibrasyonMesafesiOniki
	 *            the kalibrasyonMesafesiOniki to set
	 */
	public void setKalibrasyonMesafesiOniki(String kalibrasyonMesafesiOniki) {
		this.kalibrasyonMesafesiOniki = kalibrasyonMesafesiOniki;
	}

	/**
	 * @return the kalibrasyonMesafesiOnuc
	 */
	public String getKalibrasyonMesafesiOnuc() {
		return kalibrasyonMesafesiOnuc;
	}

	/**
	 * @param kalibrasyonMesafesiOnuc
	 *            the kalibrasyonMesafesiOnuc to set
	 */
	public void setKalibrasyonMesafesiOnuc(String kalibrasyonMesafesiOnuc) {
		this.kalibrasyonMesafesiOnuc = kalibrasyonMesafesiOnuc;
	}

	/**
	 * @return the kalibrasyonMesafesiSekiz
	 */
	public String getKalibrasyonMesafesiSekiz() {
		return kalibrasyonMesafesiSekiz;
	}

	/**
	 * @param kalibrasyonMesafesiSekiz
	 *            the kalibrasyonMesafesiSekiz to set
	 */
	public void setKalibrasyonMesafesiSekiz(String kalibrasyonMesafesiSekiz) {
		this.kalibrasyonMesafesiSekiz = kalibrasyonMesafesiSekiz;
	}

	/**
	 * @return the kalibrasyonMesafesiUc
	 */
	public String getKalibrasyonMesafesiUc() {
		return kalibrasyonMesafesiUc;
	}

	/**
	 * @param kalibrasyonMesafesiUc
	 *            the kalibrasyonMesafesiUc to set
	 */
	public void setKalibrasyonMesafesiUc(String kalibrasyonMesafesiUc) {
		this.kalibrasyonMesafesiUc = kalibrasyonMesafesiUc;
	}

	/**
	 * @return the kalibrasyonMesafesiYedi
	 */
	public String getKalibrasyonMesafesiYedi() {
		return kalibrasyonMesafesiYedi;
	}

	/**
	 * @param kalibrasyonMesafesiYedi
	 *            the kalibrasyonMesafesiYedi to set
	 */
	public void setKalibrasyonMesafesiYedi(String kalibrasyonMesafesiYedi) {
		this.kalibrasyonMesafesiYedi = kalibrasyonMesafesiYedi;
	}

	/**
	 * @return the taramaHiziAlti
	 */
	public String getTaramaHiziAlti() {
		return taramaHiziAlti;
	}

	/**
	 * @param taramaHiziAlti
	 *            the taramaHiziAlti to set
	 */
	public void setTaramaHiziAlti(String taramaHiziAlti) {
		this.taramaHiziAlti = taramaHiziAlti;
	}

	/**
	 * @return the taramaHiziBes
	 */
	public String getTaramaHiziBes() {
		return taramaHiziBes;
	}

	/**
	 * @param taramaHiziBes
	 *            the taramaHiziBes to set
	 */
	public void setTaramaHiziBes(String taramaHiziBes) {
		this.taramaHiziBes = taramaHiziBes;
	}

	/**
	 * @return the taramaHiziBir
	 */
	public String getTaramaHiziBir() {
		return taramaHiziBir;
	}

	/**
	 * @param taramaHiziBir
	 *            the taramaHiziBir to set
	 */
	public void setTaramaHiziBir(String taramaHiziBir) {
		this.taramaHiziBir = taramaHiziBir;
	}

	/**
	 * @return the taramaHiziDokuz
	 */
	public String getTaramaHiziDokuz() {
		return taramaHiziDokuz;
	}

	/**
	 * @param taramaHiziDokuz
	 *            the taramaHiziDokuz to set
	 */
	public void setTaramaHiziDokuz(String taramaHiziDokuz) {
		this.taramaHiziDokuz = taramaHiziDokuz;
	}

	/**
	 * @return the taramaHiziDort
	 */
	public String getTaramaHiziDort() {
		return taramaHiziDort;
	}

	/**
	 * @param taramaHiziDort
	 *            the taramaHiziDort to set
	 */
	public void setTaramaHiziDort(String taramaHiziDort) {
		this.taramaHiziDort = taramaHiziDort;
	}

	/**
	 * @return the taramaHiziIki
	 */
	public String getTaramaHiziIki() {
		return taramaHiziIki;
	}

	/**
	 * @param taramaHiziIki
	 *            the taramaHiziIki to set
	 */
	public void setTaramaHiziIki(String taramaHiziIki) {
		this.taramaHiziIki = taramaHiziIki;
	}

	/**
	 * @return the taramaHiziOn
	 */
	public String getTaramaHiziOn() {
		return taramaHiziOn;
	}

	/**
	 * @param taramaHiziOn
	 *            the taramaHiziOn to set
	 */
	public void setTaramaHiziOn(String taramaHiziOn) {
		this.taramaHiziOn = taramaHiziOn;
	}

	/**
	 * @return the taramaHiziOnalti
	 */
	public String getTaramaHiziOnalti() {
		return taramaHiziOnalti;
	}

	/**
	 * @param taramaHiziOnalti
	 *            the taramaHiziOnalti to set
	 */
	public void setTaramaHiziOnalti(String taramaHiziOnalti) {
		this.taramaHiziOnalti = taramaHiziOnalti;
	}

	/**
	 * @return the taramaHiziOnbes
	 */
	public String getTaramaHiziOnbes() {
		return taramaHiziOnbes;
	}

	/**
	 * @param taramaHiziOnbes
	 *            the taramaHiziOnbes to set
	 */
	public void setTaramaHiziOnbes(String taramaHiziOnbes) {
		this.taramaHiziOnbes = taramaHiziOnbes;
	}

	/**
	 * @return the taramaHiziOnbir
	 */
	public String getTaramaHiziOnbir() {
		return taramaHiziOnbir;
	}

	/**
	 * @param taramaHiziOnbir
	 *            the taramaHiziOnbir to set
	 */
	public void setTaramaHiziOnbir(String taramaHiziOnbir) {
		this.taramaHiziOnbir = taramaHiziOnbir;
	}

	/**
	 * @return the taramaHiziOndort
	 */
	public String getTaramaHiziOndort() {
		return taramaHiziOndort;
	}

	/**
	 * @param taramaHiziOndort
	 *            the taramaHiziOndort to set
	 */
	public void setTaramaHiziOndort(String taramaHiziOndort) {
		this.taramaHiziOndort = taramaHiziOndort;
	}

	/**
	 * @return the taramaHiziOniki
	 */
	public String getTaramaHiziOniki() {
		return taramaHiziOniki;
	}

	/**
	 * @param taramaHiziOniki
	 *            the taramaHiziOniki to set
	 */
	public void setTaramaHiziOniki(String taramaHiziOniki) {
		this.taramaHiziOniki = taramaHiziOniki;
	}

	/**
	 * @return the taramaHiziOnuc
	 */
	public String getTaramaHiziOnuc() {
		return taramaHiziOnuc;
	}

	/**
	 * @param taramaHiziOnuc
	 *            the taramaHiziOnuc to set
	 */
	public void setTaramaHiziOnuc(String taramaHiziOnuc) {
		this.taramaHiziOnuc = taramaHiziOnuc;
	}

	/**
	 * @return the taramaHiziSekiz
	 */
	public String getTaramaHiziSekiz() {
		return taramaHiziSekiz;
	}

	/**
	 * @param taramaHiziSekiz
	 *            the taramaHiziSekiz to set
	 */
	public void setTaramaHiziSekiz(String taramaHiziSekiz) {
		this.taramaHiziSekiz = taramaHiziSekiz;
	}

	/**
	 * @return the taramaHiziUc
	 */
	public String getTaramaHiziUc() {
		return taramaHiziUc;
	}

	/**
	 * @param taramaHiziUc
	 *            the taramaHiziUc to set
	 */
	public void setTaramaHiziUc(String taramaHiziUc) {
		this.taramaHiziUc = taramaHiziUc;
	}

	/**
	 * @return the taramaHiziYedi
	 */
	public String getTaramaHiziYedi() {
		return taramaHiziYedi;
	}

	/**
	 * @param taramaHiziYedi
	 *            the taramaHiziYedi to set
	 */
	public void setTaramaHiziYedi(String taramaHiziYedi) {
		this.taramaHiziYedi = taramaHiziYedi;
	}

	/**
	 * @return the cihaz
	 */
	public String getCihaz() {
		return cihaz;
	}

	/**
	 * @param cihaz
	 *            the cihaz to set
	 */
	public void setCihaz(String cihaz) {
		this.cihaz = cihaz;
	}

	/**
	 * @return the kalibrasyonBloguBir
	 */
	public String getKalibrasyonBloguBir() {
		return kalibrasyonBloguBir;
	}

	/**
	 * @param kalibrasyonBloguBir
	 *            the kalibrasyonBloguBir to set
	 */
	public void setKalibrasyonBloguBir(String kalibrasyonBloguBir) {
		this.kalibrasyonBloguBir = kalibrasyonBloguBir;
	}

	/**
	 * @return the kalibrasyonBloguIki
	 */
	public String getKalibrasyonBloguIki() {
		return kalibrasyonBloguIki;
	}

	/**
	 * @param kalibrasyonBloguIki
	 *            the kalibrasyonBloguIki to set
	 */
	public void setKalibrasyonBloguIki(String kalibrasyonBloguIki) {
		this.kalibrasyonBloguIki = kalibrasyonBloguIki;
	}

	/**
	 * @return the kalibrasyonBloguUc
	 */
	public String getKalibrasyonBloguUc() {
		return kalibrasyonBloguUc;
	}

	/**
	 * @param kalibrasyonBloguUc
	 *            the kalibrasyonBloguUc to set
	 */
	public void setKalibrasyonBloguUc(String kalibrasyonBloguUc) {
		this.kalibrasyonBloguUc = kalibrasyonBloguUc;
	}

	/**
	 * @return the kalibrasyonBloguDort
	 */
	public String getKalibrasyonBloguDort() {
		return kalibrasyonBloguDort;
	}

	/**
	 * @param kalibrasyonBloguDort
	 *            the kalibrasyonBloguDort to set
	 */
	public void setKalibrasyonBloguDort(String kalibrasyonBloguDort) {
		this.kalibrasyonBloguDort = kalibrasyonBloguDort;
	}

	/**
	 * @return the kalibrasyonBloguBes
	 */
	public String getKalibrasyonBloguBes() {
		return kalibrasyonBloguBes;
	}

	/**
	 * @param kalibrasyonBloguBes
	 *            the kalibrasyonBloguBes to set
	 */
	public void setKalibrasyonBloguBes(String kalibrasyonBloguBes) {
		this.kalibrasyonBloguBes = kalibrasyonBloguBes;
	}

	/**
	 * @return the kalibrasyonBloguAlti
	 */
	public String getKalibrasyonBloguAlti() {
		return kalibrasyonBloguAlti;
	}

	/**
	 * @param kalibrasyonBloguAlti
	 *            the kalibrasyonBloguAlti to set
	 */
	public void setKalibrasyonBloguAlti(String kalibrasyonBloguAlti) {
		this.kalibrasyonBloguAlti = kalibrasyonBloguAlti;
	}

	/**
	 * @return the kalibrasyonBloguYedi
	 */
	public String getKalibrasyonBloguYedi() {
		return kalibrasyonBloguYedi;
	}

	/**
	 * @param kalibrasyonBloguYedi
	 *            the kalibrasyonBloguYedi to set
	 */
	public void setKalibrasyonBloguYedi(String kalibrasyonBloguYedi) {
		this.kalibrasyonBloguYedi = kalibrasyonBloguYedi;
	}

	/**
	 * @return the kalibrasyonBloguSekiz
	 */
	public String getKalibrasyonBloguSekiz() {
		return kalibrasyonBloguSekiz;
	}

	/**
	 * @param kalibrasyonBloguSekiz
	 *            the kalibrasyonBloguSekiz to set
	 */
	public void setKalibrasyonBloguSekiz(String kalibrasyonBloguSekiz) {
		this.kalibrasyonBloguSekiz = kalibrasyonBloguSekiz;
	}

	/**
	 * @return the kalibrasyonBloguDokuz
	 */
	public String getKalibrasyonBloguDokuz() {
		return kalibrasyonBloguDokuz;
	}

	/**
	 * @param kalibrasyonBloguDokuz
	 *            the kalibrasyonBloguDokuz to set
	 */
	public void setKalibrasyonBloguDokuz(String kalibrasyonBloguDokuz) {
		this.kalibrasyonBloguDokuz = kalibrasyonBloguDokuz;
	}

	/**
	 * @return the kalibrasyonBloguOn
	 */
	public String getKalibrasyonBloguOn() {
		return kalibrasyonBloguOn;
	}

	/**
	 * @param kalibrasyonBloguOn
	 *            the kalibrasyonBloguOn to set
	 */
	public void setKalibrasyonBloguOn(String kalibrasyonBloguOn) {
		this.kalibrasyonBloguOn = kalibrasyonBloguOn;
	}

	/**
	 * @return the kalibrasyonBloguOnbir
	 */
	public String getKalibrasyonBloguOnbir() {
		return kalibrasyonBloguOnbir;
	}

	/**
	 * @param kalibrasyonBloguOnbir
	 *            the kalibrasyonBloguOnbir to set
	 */
	public void setKalibrasyonBloguOnbir(String kalibrasyonBloguOnbir) {
		this.kalibrasyonBloguOnbir = kalibrasyonBloguOnbir;
	}

	/**
	 * @return the kalibrasyonBloguOniki
	 */
	public String getKalibrasyonBloguOniki() {
		return kalibrasyonBloguOniki;
	}

	/**
	 * @param kalibrasyonBloguOniki
	 *            the kalibrasyonBloguOniki to set
	 */
	public void setKalibrasyonBloguOniki(String kalibrasyonBloguOniki) {
		this.kalibrasyonBloguOniki = kalibrasyonBloguOniki;
	}

	/**
	 * @return the kalibrasyonBloguOnuc
	 */
	public String getKalibrasyonBloguOnuc() {
		return kalibrasyonBloguOnuc;
	}

	/**
	 * @param kalibrasyonBloguOnuc
	 *            the kalibrasyonBloguOnuc to set
	 */
	public void setKalibrasyonBloguOnuc(String kalibrasyonBloguOnuc) {
		this.kalibrasyonBloguOnuc = kalibrasyonBloguOnuc;
	}

	/**
	 * @return the kalibrasyonBloguOndort
	 */
	public String getKalibrasyonBloguOndort() {
		return kalibrasyonBloguOndort;
	}

	/**
	 * @param kalibrasyonBloguOndort
	 *            the kalibrasyonBloguOndort to set
	 */
	public void setKalibrasyonBloguOndort(String kalibrasyonBloguOndort) {
		this.kalibrasyonBloguOndort = kalibrasyonBloguOndort;
	}

	/**
	 * @return the kalibrasyonBloguOnbes
	 */
	public String getKalibrasyonBloguOnbes() {
		return kalibrasyonBloguOnbes;
	}

	/**
	 * @param kalibrasyonBloguOnbes
	 *            the kalibrasyonBloguOnbes to set
	 */
	public void setKalibrasyonBloguOnbes(String kalibrasyonBloguOnbes) {
		this.kalibrasyonBloguOnbes = kalibrasyonBloguOnbes;
	}

	/**
	 * @return the kalibrasyonBloguOnalti
	 */
	public String getKalibrasyonBloguOnalti() {
		return kalibrasyonBloguOnalti;
	}

	/**
	 * @param kalibrasyonBloguOnalti
	 *            the kalibrasyonBloguOnalti to set
	 */
	public void setKalibrasyonBloguOnalti(String kalibrasyonBloguOnalti) {
		this.kalibrasyonBloguOnalti = kalibrasyonBloguOnalti;
	}

}