package com.emekboru.jpa.rulo;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "rulo_ozellik_fiziksel_girdi")
public class RuloOzellikFizikselGirdi {
	@Id
	@SequenceGenerator(name = "rulo_ozellik_fiziksel_girdi_generator", sequenceName = "rulo_ozellik_fiziksel_girdi_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rulo_ozellik_fiziksel_girdi_generator")
	@Column(name = "rulo_ozellik_fiziksel_girdi_id")
	private Integer ruloOzellikFizikselId;

	@Column(name = "rulo_ozellik_fiziksel_akma")
	private Float ruloOzellikFizikselAkma;

	@Column(name = "rulo_ozellik_fiziksel_cekme")
	private Float ruloOzellikFizikselCekme;

	@Column(name = "rulo_ozellik_fiziksel_uzama")
	private Float ruloOzellikFizikselUzama;

	@Column(name = "rulo_ozellik_fiziksel_aciklama")
	private String ruloOzellikFizikselAciklama;

	@Temporal(TemporalType.DATE)
	@Column(name = "rulo_ozellik_fiziksel_tarih")
	private Date ruloOzellikFizikselTarih;

	@OneToOne(mappedBy = "ruloOzellikFizikselGirdi", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	private Rulo rulo;

	@Column(name = "durum")
	private Integer durum = 0;

	@Column(name = "islem_yapan_id")
	private int islemYapanId;

	@Column(name = "islem_yapan_isim")
	private String islemYapanIsim;

	public int getIslemYapanId() {
		return islemYapanId;
	}

	public void setIslemYapanId(int islemYapanId) {
		this.islemYapanId = islemYapanId;
	}

	public String getIslemYapanIsim() {
		return islemYapanIsim;
	}

	public void setIslemYapanIsim(String islemYapanIsim) {
		this.islemYapanIsim = islemYapanIsim;
	}

	public Integer getDurum() {
		return durum;
	}

	public void setDurum(Integer durum) {
		this.durum = durum;
	}

	public Rulo getRulo() {
		return rulo;
	}

	public void setRulo(Rulo rulo) {
		this.rulo = rulo;
	}

	public Integer getRuloOzellikFizikselId() {
		return ruloOzellikFizikselId;
	}

	public void setRuloOzellikFizikselId(Integer ruloOzellikFizikselId) {
		this.ruloOzellikFizikselId = ruloOzellikFizikselId;
	}

	public String getRuloOzellikFizikselAciklama() {
		return ruloOzellikFizikselAciklama;
	}

	public void setRuloOzellikFizikselAciklama(
			String ruloOzellikFizikselAciklama) {
		this.ruloOzellikFizikselAciklama = ruloOzellikFizikselAciklama;
	}

	public Date getRuloOzellikFizikselTarih() {
		return ruloOzellikFizikselTarih;
	}

	public void setRuloOzellikFizikselTarih(Date ruloOzellikFizikselTarih) {
		this.ruloOzellikFizikselTarih = ruloOzellikFizikselTarih;
	}

	public Float getRuloOzellikFizikselAkma() {
		return ruloOzellikFizikselAkma;
	}

	public void setRuloOzellikFizikselAkma(Float ruloOzellikFizikselAkma) {
		this.ruloOzellikFizikselAkma = ruloOzellikFizikselAkma;
	}

	public Float getRuloOzellikFizikselCekme() {
		return ruloOzellikFizikselCekme;
	}

	public void setRuloOzellikFizikselCekme(Float ruloOzellikFizikselCekme) {
		this.ruloOzellikFizikselCekme = ruloOzellikFizikselCekme;
	}

	public Float getRuloOzellikFizikselUzama() {
		return ruloOzellikFizikselUzama;
	}

	public void setRuloOzellikFizikselUzama(Float ruloOzellikFizikselUzama) {
		this.ruloOzellikFizikselUzama = ruloOzellikFizikselUzama;
	}

}
