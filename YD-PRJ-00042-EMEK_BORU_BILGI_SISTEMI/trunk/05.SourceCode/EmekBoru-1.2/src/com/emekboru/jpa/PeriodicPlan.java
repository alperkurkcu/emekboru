package com.emekboru.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the periodic_plan database table.
 * 
 */
@Entity
@Table(name="periodic_plan")
public class PeriodicPlan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PERIODIC_PLAN_PERIODICPLANID_GENERATOR", sequenceName="PERIODIC_PLAN_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PERIODIC_PLAN_PERIODICPLANID_GENERATOR")
	@Column(name="periodic_plan_id")
	private Integer periodicPlanId;

	@Column(name="year")
	private String year;

	@Column(name="plan_period")
	private String planPeriod;
	
	@Temporal(TemporalType.DATE)
	@Column(name="start_date")
	private Date startDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="end_date")
	private Date endDate;

	@Column(name="short_desc")
	private String shortDesc;
	
	@Temporal(TemporalType.DATE)
	@Column(name="jan_start_date")
	private Date janStartDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="jan_end_date")
	private Date janEndDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="feb_start_date")
	private Date febStartDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="feb_end_date")
	private Date febEndDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="mar_start_date")
	private Date marStartDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="mar_end_date")
	private Date marEndDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="apr_start_date")
	private Date aprStartDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="apr_end_date")
	private Date aprEndDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="may_start_date")
	private Date mayStartDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="may_end_date")
	private Date mayEndDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="jun_start_date")
	private Date junStartDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="jun_end_date")
	private Date junEndDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="jul_start_date")
	private Date julStartDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="jul_end_date")
	private Date julEndDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="aug_start_date")
	private Date augStartDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="aug_end_date")
	private Date augEndDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="sep_start_date")
	private Date sepStartDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="sep_end_date")
	private Date sepEndDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="oct_start_date")
	private Date octStartDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="oct_end_date")
	private Date octEndDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="nov_start_date")
	private Date novStartDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="nov_end_date")
	private Date novEndDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="dec_start_date")
	private Date decStartDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name="dec_end_date")
	private Date decEndDate;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="machine_id", referencedColumnName="machine_id")
	private Machine machine;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="machine_part_id", referencedColumnName="machine_part_id")
	private MachinePart machinePart;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="maintenance_plan_type_id", referencedColumnName="maintenance_plan_type_id")
	private MaintenancePlanType maintenancePlanType;

	public PeriodicPlan() {
		machine = new Machine();
		machinePart = new MachinePart();
		maintenancePlanType = new MaintenancePlanType();
	}

	public Integer getPeriodicPlanId() {
		return this.periodicPlanId;
	}

	public void setPeriodicPlanId(Integer periodicPlanId) {
		this.periodicPlanId = periodicPlanId;
	}

	public String getYear() {
		return this.year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getPlanPeriod() {
		return this.planPeriod;
	}

	public void setPlanPeriod(String planPeriod) {
		this.planPeriod = planPeriod;
	}
	
	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getShortDesc() {
		return this.shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public Date getJanStartDate() {
		return janStartDate;
	}

	public void setJanStartDate(Date janStartDate) {
		this.janStartDate = janStartDate;
	}

	public Date getJanEndDate() {
		return janEndDate;
	}

	public void setJanEndDate(Date janEndDate) {
		this.janEndDate = janEndDate;
	}

	public Date getFebStartDate() {
		return febStartDate;
	}

	public void setFebStartDate(Date febStartDate) {
		this.febStartDate = febStartDate;
	}

	public Date getFebEndDate() {
		return febEndDate;
	}

	public void setFebEndDate(Date febEndDate) {
		this.febEndDate = febEndDate;
	}

	public Date getMarStartDate() {
		return marStartDate;
	}

	public void setMarStartDate(Date marStartDate) {
		this.marStartDate = marStartDate;
	}

	public Date getMarEndDate() {
		return marEndDate;
	}

	public void setMarEndDate(Date marEndDate) {
		this.marEndDate = marEndDate;
	}

	public Date getAprStartDate() {
		return aprStartDate;
	}

	public void setAprStartDate(Date aprStartDate) {
		this.aprStartDate = aprStartDate;
	}

	public Date getAprEndDate() {
		return aprEndDate;
	}

	public void setAprEndDate(Date aprEndDate) {
		this.aprEndDate = aprEndDate;
	}

	public Date getMayStartDate() {
		return mayStartDate;
	}

	public void setMayStartDate(Date mayStartDate) {
		this.mayStartDate = mayStartDate;
	}

	public Date getMayEndDate() {
		return mayEndDate;
	}

	public void setMayEndDate(Date mayEndDate) {
		this.mayEndDate = mayEndDate;
	}

	public Date getJunStartDate() {
		return junStartDate;
	}

	public void setJunStartDate(Date junStartDate) {
		this.junStartDate = junStartDate;
	}

	public Date getJunEndDate() {
		return junEndDate;
	}

	public void setJunEndDate(Date junEndDate) {
		this.junEndDate = junEndDate;
	}

	public Date getJulStartDate() {
		return julStartDate;
	}

	public void setJulStartDate(Date julStartDate) {
		this.julStartDate = julStartDate;
	}

	public Date getJulEndDate() {
		return julEndDate;
	}

	public void setJulEndDate(Date julEndDate) {
		this.julEndDate = julEndDate;
	}

	public Date getAugStartDate() {
		return augStartDate;
	}

	public void setAugStartDate(Date augStartDate) {
		this.augStartDate = augStartDate;
	}

	public Date getAugEndDate() {
		return augEndDate;
	}

	public void setAugEndDate(Date augEndDate) {
		this.augEndDate = augEndDate;
	}

	public Date getSepStartDate() {
		return sepStartDate;
	}

	public void setSepStartDate(Date sepStartDate) {
		this.sepStartDate = sepStartDate;
	}

	public Date getSepEndDate() {
		return sepEndDate;
	}

	public void setSepEndDate(Date sepEndDate) {
		this.sepEndDate = sepEndDate;
	}

	public Date getOctStartDate() {
		return octStartDate;
	}

	public void setOctStartDate(Date octStartDate) {
		this.octStartDate = octStartDate;
	}

	public Date getOctEndDate() {
		return octEndDate;
	}

	public void setOctEndDate(Date octEndDate) {
		this.octEndDate = octEndDate;
	}

	public Date getNovStartDate() {
		return novStartDate;
	}

	public void setNovStartDate(Date novStartDate) {
		this.novStartDate = novStartDate;
	}

	public Date getNovEndDate() {
		return novEndDate;
	}

	public void setNovEndDate(Date novEndDate) {
		this.novEndDate = novEndDate;
	}

	public Date getDecStartDate() {
		return decStartDate;
	}

	public void setDecStartDate(Date decStartDate) {
		this.decStartDate = decStartDate;
	}

	public Date getDecEndDate() {
		return decEndDate;
	}

	public void setDecEndDate(Date decEndDate) {
		this.decEndDate = decEndDate;
	}

	public Machine getMachine() {
		return machine;
	}

	public void setMachine(Machine machine) {
		this.machine = machine;
	}

	public MachinePart getMachinePart() {
		return machinePart;
	}

	public void setMachinePart(MachinePart machinePart) {
		this.machinePart = machinePart;
	}

	public MaintenancePlanType getMaintenancePlanType() {
		return maintenancePlanType;
	}

	public void setMaintenancePlanType(MaintenancePlanType maintenancePlanType) {
		this.maintenancePlanType = maintenancePlanType;
	}
	
}