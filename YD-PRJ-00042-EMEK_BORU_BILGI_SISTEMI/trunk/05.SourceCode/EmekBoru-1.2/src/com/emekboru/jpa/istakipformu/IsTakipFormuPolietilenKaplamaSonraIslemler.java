package com.emekboru.jpa.istakipformu;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the is_takip_formu_polietilen_kaplama_sonra_islemler
 * database table.
 * 
 */
@Entity
@Table(name = "is_takip_formu_polietilen_kaplama_sonra_islemler")
public class IsTakipFormuPolietilenKaplamaSonraIslemler implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IS_TAKIP_FORMU_POLIETILEN_KAPLAMA_SONRA_ISLEMLER_ID_GENERATOR", sequenceName = "IS_TAKIP_FORMU_POLIETILEN_KAPLAMA_SONRA_ISLEMLER_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IS_TAKIP_FORMU_POLIETILEN_KAPLAMA_SONRA_ISLEMLER_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "ayar_sonrasi_ilk_boru_id")
	private Integer ayarSonrasiIlkBoruId;

	@Column(name = "ayar_sonrasi_ilk_boru_kalinlik_check")
	private Boolean ayarSonrasiIlkBoruKalinlikCheck;

	@Column(name = "ayar_sonrasi_ilk_boru_kalinlik_max")
	private Integer ayarSonrasiIlkBoruKalinlikMax;

	@Column(name = "ayar_sonrasi_ilk_boru_kalinlik_min")
	private Integer ayarSonrasiIlkBoruKalinlikMin;

	@Column(name = "ayar_sonrasi_ilk_boru_kalinlik_ort")
	private Integer ayarSonrasiIlkBoruKalinlikOrt;

	@Temporal(TemporalType.DATE)
	@Column(name = "ayar_sonrasi_ilk_boru_kalinlik_tarihi")
	private Date ayarSonrasiIlkBoruKalinlikTarihi;

	@Column(name = "cekme_test")
	private Integer cekmeTest;

	@Column(name = "cekme_test_check")
	private Boolean cekmeTestCheck;

	@Temporal(TemporalType.DATE)
	@Column(name = "cekme_test_tarihi")
	private Date cekmeTestTarihi;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "gorsel_kontrol")
	private Integer gorselKontrol;

	@Column(name = "gorsel_kontrol_check")
	private Boolean gorselKontrolCheck;

	@Temporal(TemporalType.DATE)
	@Column(name = "gorsel_kontrol_tarihi")
	private Date gorselKontrolTarihi;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "is_takip_form_id", insertable = false, updatable = false)
	private Integer isTakipFormId;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "is_takip_form_id", referencedColumnName = "ID", insertable = false, updatable = false)
	private IsTakipFormuPolietilenKaplama isTakipFormuPolietilenKaplama;

	@Column(name = "kaplama_hizi")
	private Integer kaplamaHizi;

	@Column(name = "kaplama_hizi_check")
	private Boolean kaplamaHiziCheck;

	@Temporal(TemporalType.DATE)
	@Column(name = "kaplama_hizi_tarihi")
	private Date kaplamaHiziTarihi;

	@Column(name = "ayar_sonrasi_ilk_boru_aciklama", length = 255)
	private String ayarSonrasiIlkBoruAciklama;

	@Column(name = "kalinlik_aciklama", length = 255)
	private String kalinlikAciklama;

	@Column(name = "cekme_testi_aciklama", length = 255)
	private String cekmeTestiAciklama;

	@Column(name = "gorsel_kontrol_aciklama", length = 255)
	private String gorselKontrolAciklama;

	@Column(name = "ayar_sonrasi_ilk_boru_check")
	private Boolean ayarSonrasiIlkBoruCheck;

	public IsTakipFormuPolietilenKaplamaSonraIslemler() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAyarSonrasiIlkBoruId() {
		return this.ayarSonrasiIlkBoruId;
	}

	public void setAyarSonrasiIlkBoruId(Integer ayarSonrasiIlkBoruId) {
		this.ayarSonrasiIlkBoruId = ayarSonrasiIlkBoruId;
	}

	public Boolean getAyarSonrasiIlkBoruKalinlikCheck() {
		return this.ayarSonrasiIlkBoruKalinlikCheck;
	}

	public void setAyarSonrasiIlkBoruKalinlikCheck(
			Boolean ayarSonrasiIlkBoruKalinlikCheck) {
		this.ayarSonrasiIlkBoruKalinlikCheck = ayarSonrasiIlkBoruKalinlikCheck;
	}

	public Integer getAyarSonrasiIlkBoruKalinlikMax() {
		return this.ayarSonrasiIlkBoruKalinlikMax;
	}

	public void setAyarSonrasiIlkBoruKalinlikMax(
			Integer ayarSonrasiIlkBoruKalinlikMax) {
		this.ayarSonrasiIlkBoruKalinlikMax = ayarSonrasiIlkBoruKalinlikMax;
	}

	public Integer getAyarSonrasiIlkBoruKalinlikMin() {
		return this.ayarSonrasiIlkBoruKalinlikMin;
	}

	public void setAyarSonrasiIlkBoruKalinlikMin(
			Integer ayarSonrasiIlkBoruKalinlikMin) {
		this.ayarSonrasiIlkBoruKalinlikMin = ayarSonrasiIlkBoruKalinlikMin;
	}

	public Integer getAyarSonrasiIlkBoruKalinlikOrt() {
		return this.ayarSonrasiIlkBoruKalinlikOrt;
	}

	public void setAyarSonrasiIlkBoruKalinlikOrt(
			Integer ayarSonrasiIlkBoruKalinlikOrt) {
		this.ayarSonrasiIlkBoruKalinlikOrt = ayarSonrasiIlkBoruKalinlikOrt;
	}

	public Date getAyarSonrasiIlkBoruKalinlikTarihi() {
		return this.ayarSonrasiIlkBoruKalinlikTarihi;
	}

	public void setAyarSonrasiIlkBoruKalinlikTarihi(
			Date ayarSonrasiIlkBoruKalinlikTarihi) {
		this.ayarSonrasiIlkBoruKalinlikTarihi = ayarSonrasiIlkBoruKalinlikTarihi;
	}

	public Integer getCekmeTest() {
		return this.cekmeTest;
	}

	public void setCekmeTest(Integer cekmeTest) {
		this.cekmeTest = cekmeTest;
	}

	public Boolean getCekmeTestCheck() {
		return this.cekmeTestCheck;
	}

	public void setCekmeTestCheck(Boolean cekmeTestCheck) {
		this.cekmeTestCheck = cekmeTestCheck;
	}

	public Date getCekmeTestTarihi() {
		return this.cekmeTestTarihi;
	}

	public void setCekmeTestTarihi(Date cekmeTestTarihi) {
		this.cekmeTestTarihi = cekmeTestTarihi;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getGorselKontrol() {
		return this.gorselKontrol;
	}

	public void setGorselKontrol(Integer gorselKontrol) {
		this.gorselKontrol = gorselKontrol;
	}

	public Boolean getGorselKontrolCheck() {
		return this.gorselKontrolCheck;
	}

	public void setGorselKontrolCheck(Boolean gorselKontrolCheck) {
		this.gorselKontrolCheck = gorselKontrolCheck;
	}

	public Date getGorselKontrolTarihi() {
		return this.gorselKontrolTarihi;
	}

	public void setGorselKontrolTarihi(Date gorselKontrolTarihi) {
		this.gorselKontrolTarihi = gorselKontrolTarihi;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getKaplamaHizi() {
		return this.kaplamaHizi;
	}

	public void setKaplamaHizi(Integer kaplamaHizi) {
		this.kaplamaHizi = kaplamaHizi;
	}

	public Boolean getKaplamaHiziCheck() {
		return this.kaplamaHiziCheck;
	}

	public void setKaplamaHiziCheck(Boolean kaplamaHiziCheck) {
		this.kaplamaHiziCheck = kaplamaHiziCheck;
	}

	public Date getKaplamaHiziTarihi() {
		return this.kaplamaHiziTarihi;
	}

	public void setKaplamaHiziTarihi(Date kaplamaHiziTarihi) {
		this.kaplamaHiziTarihi = kaplamaHiziTarihi;
	}

	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getAyarSonrasiIlkBoruAciklama() {
		return ayarSonrasiIlkBoruAciklama;
	}

	public void setAyarSonrasiIlkBoruAciklama(String ayarSonrasiIlkBoruAciklama) {
		this.ayarSonrasiIlkBoruAciklama = ayarSonrasiIlkBoruAciklama;
	}

	public String getKalinlikAciklama() {
		return kalinlikAciklama;
	}

	public void setKalinlikAciklama(String kalinlikAciklama) {
		this.kalinlikAciklama = kalinlikAciklama;
	}

	public String getCekmeTestiAciklama() {
		return cekmeTestiAciklama;
	}

	public void setCekmeTestiAciklama(String cekmeTestiAciklama) {
		this.cekmeTestiAciklama = cekmeTestiAciklama;
	}

	public String getGorselKontrolAciklama() {
		return gorselKontrolAciklama;
	}

	public void setGorselKontrolAciklama(String gorselKontrolAciklama) {
		this.gorselKontrolAciklama = gorselKontrolAciklama;
	}

	public Boolean getAyarSonrasiIlkBoruCheck() {
		return ayarSonrasiIlkBoruCheck;
	}

	public void setAyarSonrasiIlkBoruCheck(Boolean ayarSonrasiIlkBoruCheck) {
		this.ayarSonrasiIlkBoruCheck = ayarSonrasiIlkBoruCheck;
	}

	/**
	 * @return the isTakipFormuPolietilenKaplama
	 */
	public IsTakipFormuPolietilenKaplama getIsTakipFormuPolietilenKaplama() {
		return isTakipFormuPolietilenKaplama;
	}

	/**
	 * @param isTakipFormuPolietilenKaplama
	 *            the isTakipFormuPolietilenKaplama to set
	 */
	public void setIsTakipFormuPolietilenKaplama(
			IsTakipFormuPolietilenKaplama isTakipFormuPolietilenKaplama) {
		this.isTakipFormuPolietilenKaplama = isTakipFormuPolietilenKaplama;
	}

}