package com.emekboru.jpa.satinalma;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the satinalma_takip_dosyalar database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "SatinalmaTakipDosyalar.findAll", query = "SELECT r FROM SatinalmaTakipDosyalar r order by r.eklemeZamani DESC"),
		@NamedQuery(name = "SatinalmaTakipDosyalar.findAllByUserId", query = "SELECT r FROM SatinalmaTakipDosyalar r where r.ekleyenKullanici=:prmUserId order by r.eklemeZamani DESC") })
@Table(name = "satinalma_takip_dosyalar")
public class SatinalmaTakipDosyalar implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SATINALMA_TAKIP_DOSYALAR_ID_GENERATOR", sequenceName = "SATINALMA_TAKIP_DOSYALAR_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SATINALMA_TAKIP_DOSYALAR_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "dosya_takip_no", length = 255)
	private String dosyaTakipNo;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser user;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	// @Column(name = "malzeme_talep_id")
	// private Integer malzemeTalepId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "malzeme_talep_id", referencedColumnName = "ID", insertable = false)
	private SatinalmaMalzemeHizmetTalepFormu satinalmaMalzemeHizmetTalepFormu;

	@Transient
	public static final int PENDING = 0;
	@Transient
	public static final int APPROVED = 1;
	@Transient
	public static final int DECLINED = 2;

	public SatinalmaTakipDosyalar() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDosyaTakipNo() {
		return this.dosyaTakipNo;
	}

	public void setDosyaTakipNo(String dosyaTakipNo) {
		this.dosyaTakipNo = dosyaTakipNo;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	/**
	 * @return the user
	 */
	public SystemUser getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(SystemUser user) {
		this.user = user;
	}

	/**
	 * @return the satinalmaMalzemeHizmetTalepFormu
	 */
	public SatinalmaMalzemeHizmetTalepFormu getSatinalmaMalzemeHizmetTalepFormu() {
		return satinalmaMalzemeHizmetTalepFormu;
	}

	/**
	 * @param satinalmaMalzemeHizmetTalepFormu
	 *            the satinalmaMalzemeHizmetTalepFormu to set
	 */
	public void setSatinalmaMalzemeHizmetTalepFormu(
			SatinalmaMalzemeHizmetTalepFormu satinalmaMalzemeHizmetTalepFormu) {
		this.satinalmaMalzemeHizmetTalepFormu = satinalmaMalzemeHizmetTalepFormu;
	}

}