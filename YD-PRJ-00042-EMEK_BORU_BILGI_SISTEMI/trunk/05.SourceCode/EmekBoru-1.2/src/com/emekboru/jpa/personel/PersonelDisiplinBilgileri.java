package com.emekboru.jpa.personel;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the personel_disiplin_bilgileri database table.
 * 
 */
@Entity
@Table(name="personel_disiplin_bilgileri")
public class PersonelDisiplinBilgileri implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PERSONEL_DISIPLIN_BILGILERI_ID_GENERATOR", sequenceName="PERSONEL_DISIPLIN_BILGILERI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PERSONEL_DISIPLIN_BILGILERI_ID_GENERATOR")
	@Column(name="ID")
	private Integer id;

	@Column(name="ceza_puani")
	private Integer cezaPuani;

	@Column(name="disiplin_cezasi")
	private String disiplinCezasi;

	@Column(name="eklenme_tarihi")
	private Timestamp eklenmeTarihi;

	@Column(name="kimlik_id")
	private Integer kimlikId;

	@Column(name="kullanici_id_ekleyen")
	private Integer kullaniciIdEkleyen;

	@Column(name="kullanici_id_guncelleyen")
	private Integer kullaniciIdGuncelleyen;

	@Column(name="son_guncellenme_tarihi")
	private Timestamp sonGuncellenmeTarihi;

	@Column(name="sonuc")
	private String sonuc;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="tarih")
	private Date tarih;

	public PersonelDisiplinBilgileri() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCezaPuani() {
		return this.cezaPuani;
	}

	public void setCezaPuani(Integer cezaPuani) {
		this.cezaPuani = cezaPuani;
	}

	public String getDisiplinCezasi() {
		return this.disiplinCezasi;
	}

	public void setDisiplinCezasi(String disiplinCezasi) {
		this.disiplinCezasi = disiplinCezasi;
	}

	public Timestamp getEklenmeTarihi() {
		return this.eklenmeTarihi;
	}

	public void setEklenmeTarihi(Timestamp eklenmeTarihi) {
		this.eklenmeTarihi = eklenmeTarihi;
	}

	public Integer getKimlikId() {
		return this.kimlikId;
	}

	public void setKimlikId(Integer kimlikId) {
		this.kimlikId = kimlikId;
	}

	public Integer getKullaniciIdEkleyen() {
		return this.kullaniciIdEkleyen;
	}

	public void setKullaniciIdEkleyen(Integer kullaniciIdEkleyen) {
		this.kullaniciIdEkleyen = kullaniciIdEkleyen;
	}

	public Integer getKullaniciIdGuncelleyen() {
		return this.kullaniciIdGuncelleyen;
	}

	public void setKullaniciIdGuncelleyen(Integer kullaniciIdGuncelleyen) {
		this.kullaniciIdGuncelleyen = kullaniciIdGuncelleyen;
	}

	public Timestamp getSonGuncellenmeTarihi() {
		return this.sonGuncellenmeTarihi;
	}

	public void setSonGuncellenmeTarihi(Timestamp sonGuncellenmeTarihi) {
		this.sonGuncellenmeTarihi = sonGuncellenmeTarihi;
	}

	public String getSonuc() {
		return this.sonuc;
	}

	public void setSonuc(String sonuc) {
		this.sonuc = sonuc;
	}

	public Date getTarih() {
		return this.tarih;
	}

	public void setTarih(Date tarih) {
		this.tarih = tarih;
	}

}