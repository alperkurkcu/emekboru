package com.emekboru.jpa.istakipformu;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the is_takip_formu_imalat_once_islemler database
 * table.
 * 
 */
@Entity
@Table(name = "is_takip_formu_imalat_once_islemler")
public class IsTakipFormuImalatOnceIslemler implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IS_TAKIP_FORMU_IMALAT_ONCE_ISLEMLER_ID_GENERATOR", sequenceName = "IS_TAKIP_FORMU_IMALAT_ONCE_ISLEMLER_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IS_TAKIP_FORMU_IMALAT_ONCE_ISLEMLER_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "band_listesi_aciklama")
	private String bandListesiAciklama;

	@Column(name = "band_listesi_check")
	private Boolean bandListesiCheck;

	@Column(name = "band_listesi_kullanici")
	private Integer bandListesiKullanici;

	@Temporal(TemporalType.DATE)
	@Column(name = "band_listesi_tarih")
	private Date bandListesiTarih;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "engel_yok_aciklama")
	private String engelYokAciklama;

	@Column(name = "engel_yok_check")
	private Boolean engelYokCheck;

	@Column(name = "engel_yok_kullanici")
	private Integer engelYokKullanici;

	@Temporal(TemporalType.DATE)
	@Column(name = "engel_yok_tarih")
	private Date engelYokTarih;

	@Column(name = "gerceklesen_ayar_aciklama")
	private String gerceklesenAyarAciklama;

	@Column(name = "gerceklesen_ayar_check")
	private Boolean gerceklesenAyarCheck;

	@Column(name = "gerceklesen_ayar_kullanici")
	private Integer gerceklesenAyarKullanici;

	@Column(name = "gerceklesen_ayar_suresi")
	private Integer gerceklesenAyarSuresi;

	@Temporal(TemporalType.DATE)
	@Column(name = "gerceklesen_ayar_tarih")
	private Date gerceklesenAyarTarih;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name = "is_takip_form_id")
	// private Integer isTakipFormId;

	@OneToOne(mappedBy = "isTakipFormuImalatOnceIslemler", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	private IsTakipFormuImalat isTakipFormuImalat;

	@Column(name = "imalat_malzeme_aciklama")
	private String imalatMalzemeAciklama;

	@Column(name = "imalat_malzeme_check")
	private Boolean imalatMalzemeCheck;

	@Column(name = "imalat_malzeme_kullanici")
	private Integer imalatMalzemeKullanici;

	@Temporal(TemporalType.DATE)
	@Column(name = "imalat_malzeme_tarih")
	private Date imalatMalzemeTarih;

	@Column(name = "ongorulen_ayar_aciklama")
	private String ongorulenAyarAciklama;

	@Column(name = "ongorulen_ayar_check")
	private Boolean ongorulenAyarCheck;

	@Column(name = "ongorulen_ayar_kullanici")
	private Integer ongorulenAyarKullanici;

	@Column(name = "ongorulen_ayar_suresi")
	private Integer ongorulenAyarSuresi;

	@Temporal(TemporalType.DATE)
	@Column(name = "ongorulen_ayar_tarih")
	private Date ongorulenAyarTarih;

	@Column(name = "parametre_aciklama")
	private String parametreAciklama;

	@Column(name = "parametre_check")
	private Boolean parametreCheck;

	@Column(name = "parametre_kullanici")
	private Integer parametreKullanici;

	@Temporal(TemporalType.DATE)
	@Column(name = "parametre_tarih")
	private Date parametreTarih;

	public IsTakipFormuImalatOnceIslemler() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBandListesiAciklama() {
		return this.bandListesiAciklama;
	}

	public void setBandListesiAciklama(String bandListesiAciklama) {
		this.bandListesiAciklama = bandListesiAciklama;
	}

	public Boolean getBandListesiCheck() {
		return this.bandListesiCheck;
	}

	public void setBandListesiCheck(Boolean bandListesiCheck) {
		this.bandListesiCheck = bandListesiCheck;
	}

	public Integer getBandListesiKullanici() {
		return this.bandListesiKullanici;
	}

	public void setBandListesiKullanici(Integer bandListesiKullanici) {
		this.bandListesiKullanici = bandListesiKullanici;
	}

	public Date getBandListesiTarih() {
		return this.bandListesiTarih;
	}

	public void setBandListesiTarih(Date bandListesiTarih) {
		this.bandListesiTarih = bandListesiTarih;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public String getEngelYokAciklama() {
		return this.engelYokAciklama;
	}

	public void setEngelYokAciklama(String engelYokAciklama) {
		this.engelYokAciklama = engelYokAciklama;
	}

	public Boolean getEngelYokCheck() {
		return this.engelYokCheck;
	}

	public void setEngelYokCheck(Boolean engelYokCheck) {
		this.engelYokCheck = engelYokCheck;
	}

	public Integer getEngelYokKullanici() {
		return this.engelYokKullanici;
	}

	public void setEngelYokKullanici(Integer engelYokKullanici) {
		this.engelYokKullanici = engelYokKullanici;
	}

	public Date getEngelYokTarih() {
		return this.engelYokTarih;
	}

	public void setEngelYokTarih(Date engelYokTarih) {
		this.engelYokTarih = engelYokTarih;
	}

	public String getGerceklesenAyarAciklama() {
		return this.gerceklesenAyarAciklama;
	}

	public void setGerceklesenAyarAciklama(String gerceklesenAyarAciklama) {
		this.gerceklesenAyarAciklama = gerceklesenAyarAciklama;
	}

	public Boolean getGerceklesenAyarCheck() {
		return this.gerceklesenAyarCheck;
	}

	public void setGerceklesenAyarCheck(Boolean gerceklesenAyarCheck) {
		this.gerceklesenAyarCheck = gerceklesenAyarCheck;
	}

	public Integer getGerceklesenAyarKullanici() {
		return this.gerceklesenAyarKullanici;
	}

	public void setGerceklesenAyarKullanici(Integer gerceklesenAyarKullanici) {
		this.gerceklesenAyarKullanici = gerceklesenAyarKullanici;
	}

	public Integer getGerceklesenAyarSuresi() {
		return this.gerceklesenAyarSuresi;
	}

	public void setGerceklesenAyarSuresi(Integer gerceklesenAyarSuresi) {
		this.gerceklesenAyarSuresi = gerceklesenAyarSuresi;
	}

	public Date getGerceklesenAyarTarih() {
		return this.gerceklesenAyarTarih;
	}

	public void setGerceklesenAyarTarih(Date gerceklesenAyarTarih) {
		this.gerceklesenAyarTarih = gerceklesenAyarTarih;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public String getImalatMalzemeAciklama() {
		return this.imalatMalzemeAciklama;
	}

	public void setImalatMalzemeAciklama(String imalatMalzemeAciklama) {
		this.imalatMalzemeAciklama = imalatMalzemeAciklama;
	}

	public Boolean getImalatMalzemeCheck() {
		return this.imalatMalzemeCheck;
	}

	public void setImalatMalzemeCheck(Boolean imalatMalzemeCheck) {
		this.imalatMalzemeCheck = imalatMalzemeCheck;
	}

	public Integer getImalatMalzemeKullanici() {
		return this.imalatMalzemeKullanici;
	}

	public void setImalatMalzemeKullanici(Integer imalatMalzemeKullanici) {
		this.imalatMalzemeKullanici = imalatMalzemeKullanici;
	}

	public Date getImalatMalzemeTarih() {
		return this.imalatMalzemeTarih;
	}

	public void setImalatMalzemeTarih(Date imalatMalzemeTarih) {
		this.imalatMalzemeTarih = imalatMalzemeTarih;
	}

	public String getOngorulenAyarAciklama() {
		return this.ongorulenAyarAciklama;
	}

	public void setOngorulenAyarAciklama(String ongorulenAyarAciklama) {
		this.ongorulenAyarAciklama = ongorulenAyarAciklama;
	}

	public Boolean getOngorulenAyarCheck() {
		return this.ongorulenAyarCheck;
	}

	public void setOngorulenAyarCheck(Boolean ongorulenAyarCheck) {
		this.ongorulenAyarCheck = ongorulenAyarCheck;
	}

	public Integer getOngorulenAyarKullanici() {
		return this.ongorulenAyarKullanici;
	}

	public void setOngorulenAyarKullanici(Integer ongorulenAyarKullanici) {
		this.ongorulenAyarKullanici = ongorulenAyarKullanici;
	}

	public Integer getOngorulenAyarSuresi() {
		return this.ongorulenAyarSuresi;
	}

	public void setOngorulenAyarSuresi(Integer ongorulenAyarSuresi) {
		this.ongorulenAyarSuresi = ongorulenAyarSuresi;
	}

	public Date getOngorulenAyarTarih() {
		return this.ongorulenAyarTarih;
	}

	public void setOngorulenAyarTarih(Date ongorulenAyarTarih) {
		this.ongorulenAyarTarih = ongorulenAyarTarih;
	}

	public String getParametreAciklama() {
		return this.parametreAciklama;
	}

	public void setParametreAciklama(String parametreAciklama) {
		this.parametreAciklama = parametreAciklama;
	}

	public Boolean getParametreCheck() {
		return this.parametreCheck;
	}

	public void setParametreCheck(Boolean parametreCheck) {
		this.parametreCheck = parametreCheck;
	}

	public Integer getParametreKullanici() {
		return this.parametreKullanici;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the isTakipFormuImalat
	 */
	public IsTakipFormuImalat getIsTakipFormuImalat() {
		return isTakipFormuImalat;
	}

	/**
	 * @param isTakipFormuImalat
	 *            the isTakipFormuImalat to set
	 */
	public void setIsTakipFormuImalat(IsTakipFormuImalat isTakipFormuImalat) {
		this.isTakipFormuImalat = isTakipFormuImalat;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setParametreKullanici(Integer parametreKullanici) {
		this.parametreKullanici = parametreKullanici;
	}

	public Date getParametreTarih() {
		return this.parametreTarih;
	}

	public void setParametreTarih(Date parametreTarih) {
		this.parametreTarih = parametreTarih;
	}

}