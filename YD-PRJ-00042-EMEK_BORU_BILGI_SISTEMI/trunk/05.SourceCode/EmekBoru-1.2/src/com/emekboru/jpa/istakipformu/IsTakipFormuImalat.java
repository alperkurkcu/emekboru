package com.emekboru.jpa.istakipformu;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the is_takip_formu_imalat database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "IsTakipFormuImalat.findAll", query = "SELECT r FROM IsTakipFormuImalat r WHERE r.salesItem.itemId=:prmItemId") })
@Table(name = "is_takip_formu_imalat")
public class IsTakipFormuImalat implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IS_TAKIP_FORMU_IMALAT_ID_GENERATOR", sequenceName = "IS_TAKIP_FORMU_IMALAT_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IS_TAKIP_FORMU_IMALAT_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	// @Column(name="bitince_id")
	// private Integer bitinceId;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "bitince_id", referencedColumnName = "ID", insertable = false)
	private IsTakipFormuImalatBitinceIslemler isTakipFormuImalatBitinceIslemler;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name = "once_id")
	// private Integer onceId;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "once_id", referencedColumnName = "ID", insertable = false)
	private IsTakipFormuImalatOnceIslemler isTakipFormuImalatOnceIslemler;

	// @Column(name = "sonra_id")
	// private Integer sonraId;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "sonra_id", referencedColumnName = "ID", insertable = false)
	private IsTakipFormuImalatSonraIslemler isTakipFormuImalatSonraIslemler;

	// @Column(name = "sales_item_id")
	// private Integer salesItemId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	private SalesItem salesItem;

	public IsTakipFormuImalat() {

		isTakipFormuImalatBitinceIslemler = new IsTakipFormuImalatBitinceIslemler();
		isTakipFormuImalatOnceIslemler = new IsTakipFormuImalatOnceIslemler();
		isTakipFormuImalatSonraIslemler = new IsTakipFormuImalatSonraIslemler();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the isTakipFormuImalatOnceIslemler
	 */
	public IsTakipFormuImalatOnceIslemler getIsTakipFormuImalatOnceIslemler() {
		return isTakipFormuImalatOnceIslemler;
	}

	/**
	 * @param isTakipFormuImalatOnceIslemler
	 *            the isTakipFormuImalatOnceIslemler to set
	 */
	public void setIsTakipFormuImalatOnceIslemler(
			IsTakipFormuImalatOnceIslemler isTakipFormuImalatOnceIslemler) {
		this.isTakipFormuImalatOnceIslemler = isTakipFormuImalatOnceIslemler;
	}

	/**
	 * @return the isTakipFormuImalatSonraIslemler
	 */
	public IsTakipFormuImalatSonraIslemler getIsTakipFormuImalatSonraIslemler() {
		return isTakipFormuImalatSonraIslemler;
	}

	/**
	 * @param isTakipFormuImalatSonraIslemler
	 *            the isTakipFormuImalatSonraIslemler to set
	 */
	public void setIsTakipFormuImalatSonraIslemler(
			IsTakipFormuImalatSonraIslemler isTakipFormuImalatSonraIslemler) {
		this.isTakipFormuImalatSonraIslemler = isTakipFormuImalatSonraIslemler;
	}

	/**
	 * @return the salesItem
	 */
	public SalesItem getSalesItem() {
		return salesItem;
	}

	/**
	 * @param salesItem
	 *            the salesItem to set
	 */
	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the isTakipFormuImalatBitinceIslemler
	 */
	public IsTakipFormuImalatBitinceIslemler getIsTakipFormuImalatBitinceIslemler() {
		return isTakipFormuImalatBitinceIslemler;
	}

	/**
	 * @param isTakipFormuImalatBitinceIslemler
	 *            the isTakipFormuImalatBitinceIslemler to set
	 */
	public void setIsTakipFormuImalatBitinceIslemler(
			IsTakipFormuImalatBitinceIslemler isTakipFormuImalatBitinceIslemler) {
		this.isTakipFormuImalatBitinceIslemler = isTakipFormuImalatBitinceIslemler;
	}

}