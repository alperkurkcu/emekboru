package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;

/**
 * The persistent class for the test_kaplama_kumlama_malzemesi_tuz_miktari_sonuc
 * database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaKumlamaMalzemesiTuzMiktariSonuc.findAll", query = "SELECT r FROM TestKaplamaKumlamaMalzemesiTuzMiktariSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_kaplama_kumlama_malzemesi_tuz_miktari_sonuc")
public class TestKaplamaKumlamaMalzemesiTuzMiktariSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KAPLAMA_KUMLAMA_MALZEMESI_TUZ_MIKTARI_SONUC_ID_GENERATOR", sequenceName = "TEST_KAPLAMA_KUMLAMA_MALZEMESI_TUZ_MIKTARI_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KAPLAMA_KUMLAMA_MALZEMESI_TUZ_MIKTARI_SONUC_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Column(name = "abrasive_iletkenlik_degeri_1_1")
	private BigDecimal abrasiveIletkenlikDegeri11;

	@Column(name = "abrasive_iletkenlik_degeri_1_2")
	private BigDecimal abrasiveIletkenlikDegeri12;

	@Column(name = "abrasive_iletkenlik_degeri_1_3")
	private BigDecimal abrasiveIletkenlikDegeri13;

	@Column(name = "abrasive_iletkenlik_degeri_2_1")
	private BigDecimal abrasiveIletkenlikDegeri21;

	@Column(name = "abrasive_iletkenlik_degeri_2_2")
	private BigDecimal abrasiveIletkenlikDegeri22;

	@Column(name = "abrasive_iletkenlik_degeri_2_3")
	private BigDecimal abrasiveIletkenlikDegeri23;

	@Column(name = "abrasive_iletkenlik_degeri_3_1")
	private BigDecimal abrasiveIletkenlikDegeri31;

	@Column(name = "abrasive_iletkenlik_degeri_3_2")
	private BigDecimal abrasiveIletkenlikDegeri32;

	@Column(name = "abrasive_iletkenlik_degeri_3_3")
	private BigDecimal abrasiveIletkenlikDegeri33;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ekleme_zamani")
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	// @Column(name="global_id")
	// private Integer globalId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "numune_no_1")
	private BigDecimal numuneNo1;

	@Column(name = "numune_no_2")
	private BigDecimal numuneNo2;

	@Column(name = "numune_no_3")
	private BigDecimal numuneNo3;

	@Column(name = "ortalama_iletkenlik_degeri_1")
	private BigDecimal ortalamaIletkenlikDegeri1;

	@Column(name = "ortalama_iletkenlik_degeri_2")
	private BigDecimal ortalamaIletkenlikDegeri2;

	@Column(name = "ortalama_iletkenlik_degeri_3")
	private BigDecimal ortalamaIletkenlikDegeri3;

	// @Column(name="pipe_id")
	// private Integer pipeId;

	@Column(name = "sonuc_1")
	private BigDecimal sonuc1;

	@Column(name = "sonuc_2")
	private BigDecimal sonuc2;

	@Column(name = "sonuc_3")
	private BigDecimal sonuc3;

	@Column(name = "sonuc_yag_kontrol")
	private Integer sonucYagKontrol;

	@Column(name = "spesifikasyon_degeri_1")
	private BigDecimal spesifikasyonDegeri1;

	@Column(name = "spesifikasyon_degeri_2")
	private BigDecimal spesifikasyonDegeri2;

	@Column(name = "spesifikasyon_degeri_3")
	private BigDecimal spesifikasyonDegeri3;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi")
	private Date testTarihi;

	@Column(name = "vardiya")
	private Integer vardiya;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;

	/**
	 * @return the bagliTestId
	 */
	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;

	public TestKaplamaKumlamaMalzemesiTuzMiktariSonuc() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the abrasiveIletkenlikDegeri11
	 */
	public BigDecimal getAbrasiveIletkenlikDegeri11() {
		return abrasiveIletkenlikDegeri11;
	}

	/**
	 * @param abrasiveIletkenlikDegeri11
	 *            the abrasiveIletkenlikDegeri11 to set
	 */
	public void setAbrasiveIletkenlikDegeri11(
			BigDecimal abrasiveIletkenlikDegeri11) {
		this.abrasiveIletkenlikDegeri11 = abrasiveIletkenlikDegeri11;
	}

	/**
	 * @return the abrasiveIletkenlikDegeri12
	 */
	public BigDecimal getAbrasiveIletkenlikDegeri12() {
		return abrasiveIletkenlikDegeri12;
	}

	/**
	 * @param abrasiveIletkenlikDegeri12
	 *            the abrasiveIletkenlikDegeri12 to set
	 */
	public void setAbrasiveIletkenlikDegeri12(
			BigDecimal abrasiveIletkenlikDegeri12) {
		this.abrasiveIletkenlikDegeri12 = abrasiveIletkenlikDegeri12;
	}

	/**
	 * @return the abrasiveIletkenlikDegeri13
	 */
	public BigDecimal getAbrasiveIletkenlikDegeri13() {
		return abrasiveIletkenlikDegeri13;
	}

	/**
	 * @param abrasiveIletkenlikDegeri13
	 *            the abrasiveIletkenlikDegeri13 to set
	 */
	public void setAbrasiveIletkenlikDegeri13(
			BigDecimal abrasiveIletkenlikDegeri13) {
		this.abrasiveIletkenlikDegeri13 = abrasiveIletkenlikDegeri13;
	}

	/**
	 * @return the abrasiveIletkenlikDegeri21
	 */
	public BigDecimal getAbrasiveIletkenlikDegeri21() {
		return abrasiveIletkenlikDegeri21;
	}

	/**
	 * @param abrasiveIletkenlikDegeri21
	 *            the abrasiveIletkenlikDegeri21 to set
	 */
	public void setAbrasiveIletkenlikDegeri21(
			BigDecimal abrasiveIletkenlikDegeri21) {
		this.abrasiveIletkenlikDegeri21 = abrasiveIletkenlikDegeri21;
	}

	/**
	 * @return the abrasiveIletkenlikDegeri22
	 */
	public BigDecimal getAbrasiveIletkenlikDegeri22() {
		return abrasiveIletkenlikDegeri22;
	}

	/**
	 * @param abrasiveIletkenlikDegeri22
	 *            the abrasiveIletkenlikDegeri22 to set
	 */
	public void setAbrasiveIletkenlikDegeri22(
			BigDecimal abrasiveIletkenlikDegeri22) {
		this.abrasiveIletkenlikDegeri22 = abrasiveIletkenlikDegeri22;
	}

	/**
	 * @return the abrasiveIletkenlikDegeri23
	 */
	public BigDecimal getAbrasiveIletkenlikDegeri23() {
		return abrasiveIletkenlikDegeri23;
	}

	/**
	 * @param abrasiveIletkenlikDegeri23
	 *            the abrasiveIletkenlikDegeri23 to set
	 */
	public void setAbrasiveIletkenlikDegeri23(
			BigDecimal abrasiveIletkenlikDegeri23) {
		this.abrasiveIletkenlikDegeri23 = abrasiveIletkenlikDegeri23;
	}

	/**
	 * @return the abrasiveIletkenlikDegeri31
	 */
	public BigDecimal getAbrasiveIletkenlikDegeri31() {
		return abrasiveIletkenlikDegeri31;
	}

	/**
	 * @param abrasiveIletkenlikDegeri31
	 *            the abrasiveIletkenlikDegeri31 to set
	 */
	public void setAbrasiveIletkenlikDegeri31(
			BigDecimal abrasiveIletkenlikDegeri31) {
		this.abrasiveIletkenlikDegeri31 = abrasiveIletkenlikDegeri31;
	}

	/**
	 * @return the abrasiveIletkenlikDegeri32
	 */
	public BigDecimal getAbrasiveIletkenlikDegeri32() {
		return abrasiveIletkenlikDegeri32;
	}

	/**
	 * @param abrasiveIletkenlikDegeri32
	 *            the abrasiveIletkenlikDegeri32 to set
	 */
	public void setAbrasiveIletkenlikDegeri32(
			BigDecimal abrasiveIletkenlikDegeri32) {
		this.abrasiveIletkenlikDegeri32 = abrasiveIletkenlikDegeri32;
	}

	/**
	 * @return the abrasiveIletkenlikDegeri33
	 */
	public BigDecimal getAbrasiveIletkenlikDegeri33() {
		return abrasiveIletkenlikDegeri33;
	}

	/**
	 * @param abrasiveIletkenlikDegeri33
	 *            the abrasiveIletkenlikDegeri33 to set
	 */
	public void setAbrasiveIletkenlikDegeri33(
			BigDecimal abrasiveIletkenlikDegeri33) {
		this.abrasiveIletkenlikDegeri33 = abrasiveIletkenlikDegeri33;
	}

	/**
	 * @return the eklemeZamani
	 */
	public Date getEklemeZamani() {
		return eklemeZamani;
	}

	/**
	 * @param eklemeZamani
	 *            the eklemeZamani to set
	 */
	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	/**
	 * @return the ekleyenKullanici
	 */
	public Integer getEkleyenKullanici() {
		return ekleyenKullanici;
	}

	/**
	 * @param ekleyenKullanici
	 *            the ekleyenKullanici to set
	 */
	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	/**
	 * @return the guncellemeZamani
	 */
	public Date getGuncellemeZamani() {
		return guncellemeZamani;
	}

	/**
	 * @param guncellemeZamani
	 *            the guncellemeZamani to set
	 */
	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	/**
	 * @return the guncelleyenKullanici
	 */
	public Integer getGuncelleyenKullanici() {
		return guncelleyenKullanici;
	}

	/**
	 * @param guncelleyenKullanici
	 *            the guncelleyenKullanici to set
	 */
	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	/**
	 * @return the numuneNo1
	 */
	public BigDecimal getNumuneNo1() {
		return numuneNo1;
	}

	/**
	 * @param numuneNo1
	 *            the numuneNo1 to set
	 */
	public void setNumuneNo1(BigDecimal numuneNo1) {
		this.numuneNo1 = numuneNo1;
	}

	/**
	 * @return the numuneNo2
	 */
	public BigDecimal getNumuneNo2() {
		return numuneNo2;
	}

	/**
	 * @param numuneNo2
	 *            the numuneNo2 to set
	 */
	public void setNumuneNo2(BigDecimal numuneNo2) {
		this.numuneNo2 = numuneNo2;
	}

	/**
	 * @return the numuneNo3
	 */
	public BigDecimal getNumuneNo3() {
		return numuneNo3;
	}

	/**
	 * @param numuneNo3
	 *            the numuneNo3 to set
	 */
	public void setNumuneNo3(BigDecimal numuneNo3) {
		this.numuneNo3 = numuneNo3;
	}

	/**
	 * @return the ortalamaIletkenlikDegeri1
	 */
	public BigDecimal getOrtalamaIletkenlikDegeri1() {
		return ortalamaIletkenlikDegeri1;
	}

	/**
	 * @param ortalamaIletkenlikDegeri1
	 *            the ortalamaIletkenlikDegeri1 to set
	 */
	public void setOrtalamaIletkenlikDegeri1(
			BigDecimal ortalamaIletkenlikDegeri1) {
		this.ortalamaIletkenlikDegeri1 = ortalamaIletkenlikDegeri1;
	}

	/**
	 * @return the ortalamaIletkenlikDegeri2
	 */
	public BigDecimal getOrtalamaIletkenlikDegeri2() {
		return ortalamaIletkenlikDegeri2;
	}

	/**
	 * @param ortalamaIletkenlikDegeri2
	 *            the ortalamaIletkenlikDegeri2 to set
	 */
	public void setOrtalamaIletkenlikDegeri2(
			BigDecimal ortalamaIletkenlikDegeri2) {
		this.ortalamaIletkenlikDegeri2 = ortalamaIletkenlikDegeri2;
	}

	/**
	 * @return the ortalamaIletkenlikDegeri3
	 */
	public BigDecimal getOrtalamaIletkenlikDegeri3() {
		return ortalamaIletkenlikDegeri3;
	}

	/**
	 * @param ortalamaIletkenlikDegeri3
	 *            the ortalamaIletkenlikDegeri3 to set
	 */
	public void setOrtalamaIletkenlikDegeri3(
			BigDecimal ortalamaIletkenlikDegeri3) {
		this.ortalamaIletkenlikDegeri3 = ortalamaIletkenlikDegeri3;
	}

	/**
	 * @return the sonuc1
	 */
	public BigDecimal getSonuc1() {
		return sonuc1;
	}

	/**
	 * @param sonuc1
	 *            the sonuc1 to set
	 */
	public void setSonuc1(BigDecimal sonuc1) {
		this.sonuc1 = sonuc1;
	}

	/**
	 * @return the sonuc2
	 */
	public BigDecimal getSonuc2() {
		return sonuc2;
	}

	/**
	 * @param sonuc2
	 *            the sonuc2 to set
	 */
	public void setSonuc2(BigDecimal sonuc2) {
		this.sonuc2 = sonuc2;
	}

	/**
	 * @return the sonuc3
	 */
	public BigDecimal getSonuc3() {
		return sonuc3;
	}

	/**
	 * @param sonuc3
	 *            the sonuc3 to set
	 */
	public void setSonuc3(BigDecimal sonuc3) {
		this.sonuc3 = sonuc3;
	}

	/**
	 * @return the sonucYagKontrol
	 */
	public Integer getSonucYagKontrol() {
		return sonucYagKontrol;
	}

	/**
	 * @param sonucYagKontrol
	 *            the sonucYagKontrol to set
	 */
	public void setSonucYagKontrol(Integer sonucYagKontrol) {
		this.sonucYagKontrol = sonucYagKontrol;
	}

	/**
	 * @return the spesifikasyonDegeri1
	 */
	public BigDecimal getSpesifikasyonDegeri1() {
		return spesifikasyonDegeri1;
	}

	/**
	 * @param spesifikasyonDegeri1
	 *            the spesifikasyonDegeri1 to set
	 */
	public void setSpesifikasyonDegeri1(BigDecimal spesifikasyonDegeri1) {
		this.spesifikasyonDegeri1 = spesifikasyonDegeri1;
	}

	/**
	 * @return the spesifikasyonDegeri2
	 */
	public BigDecimal getSpesifikasyonDegeri2() {
		return spesifikasyonDegeri2;
	}

	/**
	 * @param spesifikasyonDegeri2
	 *            the spesifikasyonDegeri2 to set
	 */
	public void setSpesifikasyonDegeri2(BigDecimal spesifikasyonDegeri2) {
		this.spesifikasyonDegeri2 = spesifikasyonDegeri2;
	}

	/**
	 * @return the spesifikasyonDegeri3
	 */
	public BigDecimal getSpesifikasyonDegeri3() {
		return spesifikasyonDegeri3;
	}

	/**
	 * @param spesifikasyonDegeri3
	 *            the spesifikasyonDegeri3 to set
	 */
	public void setSpesifikasyonDegeri3(BigDecimal spesifikasyonDegeri3) {
		this.spesifikasyonDegeri3 = spesifikasyonDegeri3;
	}

	/**
	 * @return the testTarihi
	 */
	public Date getTestTarihi() {
		return testTarihi;
	}

	/**
	 * @param testTarihi
	 *            the testTarihi to set
	 */
	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	/**
	 * @return the vardiya
	 */
	public Integer getVardiya() {
		return vardiya;
	}

	/**
	 * @param vardiya
	 *            the vardiya to set
	 */
	public void setVardiya(Integer vardiya) {
		this.vardiya = vardiya;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the bagliGlobalId
	 */
	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}

	/**
	 * @param bagliGlobalId
	 *            the bagliGlobalId to set
	 */
	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}