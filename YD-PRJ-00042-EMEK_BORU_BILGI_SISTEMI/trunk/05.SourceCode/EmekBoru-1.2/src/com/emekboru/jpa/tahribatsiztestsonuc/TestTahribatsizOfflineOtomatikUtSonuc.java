package com.emekboru.jpa.tahribatsiztestsonuc;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.kalibrasyon.KalibrasyonOfflineBoruUcu;
import com.emekboru.jpa.kalibrasyon.KalibrasyonOtomatikUltrasonik;
import com.emekboru.jpa.kalibrasyon.KalibrasyonUltrasonikLaminasyon;
import com.emekboru.jpa.rulo.RuloPipeLink;

/**
 * The persistent class for the test_tahribatsiz_offline_otomatik_ut_sonuc
 * database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestTahribatsizOfflineOtomatikUtSonuc.findAll", query = "SELECT r FROM TestTahribatsizOfflineOtomatikUtSonuc r WHERE r.pipe.pipeId=:prmPipeId order by r.koordinatQ") })
@Table(name = "test_tahribatsiz_offline_otomatik_ut_sonuc")
public class TestTahribatsizOfflineOtomatikUtSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_TAHRIBATSIZ_OFFLINE_OTOMATIK_UT_SONUC_ID_GENERATOR", sequenceName = "TEST_TAHRIBATSIZ_OFFLINE_OTOMATIK_UT_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_TAHRIBATSIZ_OFFLINE_OTOMATIK_UT_SONUC_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(length = 255, name = "aciklama")
	private String aciklama;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "kaynak_laminasyon")
	private Integer kaynakLaminasyon;

	@Column(name = "koordinat_l", nullable = false)
	private Integer koordinatL;

	@Column(name = "koordinat_q", nullable = false)
	private Integer koordinatQ;

	// @Column(name = "pipe_id", nullable = false)
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@Column(name = "test_id")
	private Integer testId;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	private RuloPipeLink ruloPipeLink;

	// @Column(name = "kalibrasyon_offline_id")
	// private Integer kalibrasyonOfflineId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "kalibrasyon_offline_id", referencedColumnName = "ID", insertable = false)
	private KalibrasyonOtomatikUltrasonik kalibrasyonOffline;

	// @Column(name = "kalibrasyon_laminasyon_id")
	// private Integer kalibrasyonLaminasyonId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "kalibrasyon_laminasyon_id", referencedColumnName = "ID", insertable = false)
	private KalibrasyonUltrasonikLaminasyon kalibrasyonLaminasyon;

	// @Column(name = "kalibrasyon_boru_ucu_id")
	// private Integer kalibrasyonBoruUcuId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "kalibrasyon_boru_ucu_id", referencedColumnName = "ID", insertable = false)
	private KalibrasyonOfflineBoruUcu kalibrasyonBoruUcu;

	public TestTahribatsizOfflineOtomatikUtSonuc() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getKaynakLaminasyon() {
		return this.kaynakLaminasyon;
	}

	public void setKaynakLaminasyon(Integer kaynakLaminasyon) {
		this.kaynakLaminasyon = kaynakLaminasyon;
	}

	public Integer getKoordinatL() {
		return this.koordinatL;
	}

	public void setKoordinatL(Integer koordinatL) {
		this.koordinatL = koordinatL;
	}

	public Integer getKoordinatQ() {
		return this.koordinatQ;
	}

	public void setKoordinatQ(Integer koordinatQ) {
		this.koordinatQ = koordinatQ;
	}

	public Integer getTestId() {
		return this.testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the ruloPipeLink
	 */
	public RuloPipeLink getRuloPipeLink() {
		return ruloPipeLink;
	}

	/**
	 * @param ruloPipeLink
	 *            the ruloPipeLink to set
	 */
	public void setRuloPipeLink(RuloPipeLink ruloPipeLink) {
		this.ruloPipeLink = ruloPipeLink;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the kalibrasyonLaminasyon
	 */
	public KalibrasyonUltrasonikLaminasyon getKalibrasyonLaminasyon() {
		return kalibrasyonLaminasyon;
	}

	/**
	 * @param kalibrasyonLaminasyon
	 *            the kalibrasyonLaminasyon to set
	 */
	public void setKalibrasyonLaminasyon(
			KalibrasyonUltrasonikLaminasyon kalibrasyonLaminasyon) {
		this.kalibrasyonLaminasyon = kalibrasyonLaminasyon;
	}

	/**
	 * @return the kalibrasyonBoruUcu
	 */
	public KalibrasyonOfflineBoruUcu getKalibrasyonBoruUcu() {
		return kalibrasyonBoruUcu;
	}

	/**
	 * @param kalibrasyonBoruUcu
	 *            the kalibrasyonBoruUcu to set
	 */
	public void setKalibrasyonBoruUcu(
			KalibrasyonOfflineBoruUcu kalibrasyonBoruUcu) {
		this.kalibrasyonBoruUcu = kalibrasyonBoruUcu;
	}

	/**
	 * @return the kalibrasyonOffline
	 */
	public KalibrasyonOtomatikUltrasonik getKalibrasyonOffline() {
		return kalibrasyonOffline;
	}

	/**
	 * @param kalibrasyonOffline
	 *            the kalibrasyonOffline to set
	 */
	public void setKalibrasyonOffline(
			KalibrasyonOtomatikUltrasonik kalibrasyonOffline) {
		this.kalibrasyonOffline = kalibrasyonOffline;
	}

}