package com.emekboru.jpa;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.emekboru.jpa.employee.EmployeeActivityParticipation;
import com.emekboru.jpa.employee.EmployeeAddress;
import com.emekboru.jpa.employee.EmployeeAdvanceRequest;
import com.emekboru.jpa.employee.EmployeeBankAccount;
import com.emekboru.jpa.employee.EmployeeDayoff;
import com.emekboru.jpa.employee.EmployeeDiscipline;
import com.emekboru.jpa.employee.EmployeeEducation;
import com.emekboru.jpa.employee.EmployeeEmail;
import com.emekboru.jpa.employee.EmployeeHealth;
import com.emekboru.jpa.employee.EmployeeIdentity;
import com.emekboru.jpa.employee.EmployeePayment;
import com.emekboru.jpa.employee.EmployeePerformance;
import com.emekboru.jpa.employee.EmployeePhoneNumber;
import com.emekboru.jpa.employee.EmployeePreviousExperience;
import com.emekboru.jpa.employee.EmployeeVocationalFile;

/**
 * The persistent class for the employee database table.
 * 
 */
@NamedQueries({ @NamedQuery(name = "Employee.allActivePassive", query = "SELECT r FROM Employee r WHERE r.active =:prmActivePassive ORDER BY r.firstname ASC") })
@Entity
@Table(name = "employee")
public class Employee implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "employee_id")
	@SequenceGenerator(name = "EMPLOYEE_GENERATOR", sequenceName = "EMPLOYEE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMPLOYEE_GENERATOR")
	private Integer employeeId;

	@Column(name = "active")
	private Boolean active;

	@Column(name = "photo_path")
	private String photoPath;

	@Column(name = "address")
	private String address;

	@Column(name = "city")
	private String city;

	@Column(name = "country_code")
	private String countryCode;

	@Temporal(TemporalType.DATE)
	@Column(name = "date_of_birth")
	private Date dateOfBirth;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "department_id", referencedColumnName = "department_id", updatable = false)
	private Department department;

	@Column(name = "ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@Temporal(TemporalType.DATE)
	@Column(name = "end_date")
	private Date endDate;

	@Column(name = "firstname")
	private String firstname;

	@Column(name = "gender")
	private Boolean gender;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "lastname")
	private String lastname;

	@Column(name = "phone_number")
	private String phoneNumber;

	@Column(name = "postal_code")
	private String postalCode;

	@Column(name = "salary")
	private double salary;

	@Temporal(TemporalType.DATE)
	@Column(name = "start_date")
	private Date startDate;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ulke_id", referencedColumnName = "ID")
	private Ulkeler ulkeler;

	// @OneToMany(mappedBy = "employee")
	// private List<ToolsUsage> toolsUsages;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "employee")
	private List<EmployeeEventParticipation> employeeEventParticipations;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
	private List<LicenseRenewResponsible> licenseRenewResponsibles;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
	private List<OfferRelatedWith> offerRelatedWiths;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "employee")
	private List<SystemUser> users;

	// bi-directional many-to-one association to EmployeeAddress
	@OneToMany(mappedBy = "employee", orphanRemoval = true, cascade = CascadeType.ALL)
	private List<EmployeeAddress> employeeAddresses;

	// bi-directional many-to-one association to EmployeeBankAccount
	@OneToMany(mappedBy = "employee", orphanRemoval = true)
	private List<EmployeeBankAccount> employeeBankAccounts;

	// bi-directional many-to-one association to EmployeeEmail
	@OneToMany(mappedBy = "employee", orphanRemoval = true)
	private List<EmployeeEmail> employeeEmails;

	@OneToMany(mappedBy = "employee", orphanRemoval = true)
	private List<EmployeeEducation> employeeEducations;

	@OneToMany(mappedBy = "employee", orphanRemoval = true)
	private List<EmployeeVocationalFile> employeeVocationalFiles;

	// bi-directional one-to-one association to EmployeeIdentity

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "employee")
	@JoinColumn(name = "employee_id", referencedColumnName = "employee_id", insertable = false, updatable = false)
	private EmployeeIdentity employeeIdentity;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "employee")
	@JoinColumn(name = "employee_id", referencedColumnName = "employee_id", insertable = false, updatable = false)
	private EmployeePayment employeePayment;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "employee")
	@JoinColumn(name = "employee_id", referencedColumnName = "employee_id", insertable = false, updatable = false)
	private EmployeeHealth employeeHealth;

	// bi-directional many-to-one association to EmployeePhoneNumber
	@OneToMany(mappedBy = "employee", orphanRemoval = true)
	private List<EmployeePhoneNumber> employeePhoneNumbers;

	@OneToMany(mappedBy = "employee", orphanRemoval = true)
	private List<EmployeeDiscipline> employeeDisciplines;

	@OneToMany(mappedBy = "employee", orphanRemoval = true)
	private List<EmployeePreviousExperience> employeePreviousExperiences;

	@OneToMany(mappedBy = "employee", orphanRemoval = true)
	private List<EmployeePerformance> employeePerformances;

	@OneToMany(mappedBy = "employee", orphanRemoval = true)
	private List<EmployeeActivityParticipation> employeeActivitityParticipations;

	@Column(name = "reference")
	private String reference;

	@ManyToOne
	@JoinColumn(name = "supervisor_id", referencedColumnName = "employee_id", insertable = false)
	private Employee supervisor;

	// @OneToMany(mappedBy = "supervisor")
	// private List<Employee> subEmployees;

	@OneToMany(mappedBy = "employee", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<EmployeeAdvanceRequest> employeeAdvanceRequests;

	// @OneToMany(mappedBy = "supervisor", fetch = FetchType.EAGER, cascade =
	// CascadeType.ALL)
	// private List<EmployeeAdvanceRequest> employeeAdvanceRequestsToBeApproved;

	@OneToMany(mappedBy = "employee", orphanRemoval = true)
	private List<EmployeeDayoff> employeeDayoffs;

	// @OneToMany(mappedBy = "supervisor", orphanRemoval = true)
	// private List<EmployeeDayoff> subEmployeeDayoffRequests;

	@OneToMany(mappedBy = "deputy", orphanRemoval = true)
	private List<EmployeeDayoff> clientEmployeeDayoffRequests;

	@Transient
	private EmployeeAddress defaultAddress;
	@Transient
	private EmployeeEmail defaultEmail;
	@Transient
	private EmployeePhoneNumber defaultPhoneNumber;

	@Transient
	private int lastYearsDisciplinePenaltyPoints;

	@Transient
	private int dayoffDayCountOfLastYear;

	@Transient
	private Date lastAnniversary;

	public Employee() {
		department = new Department();
		ulkeler = new Ulkeler();
		employeeIdentity = new EmployeeIdentity();
	}

	@Column(name = "kalan_izin")
	private Integer kalanIzin;

	@Override
	public boolean equals(Object o) {
		return this.getEmployeeId() == ((Employee) o).getEmployeeId();
	}

	// public boolean equals(Object object) {
	// // Basic checks.
	// if (object == this)
	// return true;
	// if (object == null || getClass() != object.getClass())
	// return false;
	//
	// // All passed.
	// return true;
	// }

	public Integer getEmployeeId() {
		return this.employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Date getDateOfBirth() {
		return this.dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public Boolean getGender() {
		return this.gender;
	}

	public void setGender(Boolean gender) {
		this.gender = gender;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public double getSalary() {
		return this.salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public List<EmployeeAddress> getEmployeeAddresses() {
		return this.employeeAddresses;
	}

	public void setEmployeeAddresses(List<EmployeeAddress> employeeAddresses) {
		this.employeeAddresses = employeeAddresses;
	}

	public EmployeeAddress addEmployeeAddress(EmployeeAddress employeeAddress) {
		getEmployeeAddresses().add(employeeAddress);
		employeeAddress.setEmployee(this);

		return employeeAddress;
	}

	public EmployeeAddress removeEmployeeAddress(EmployeeAddress employeeAddress) {
		getEmployeeAddresses().remove(employeeAddress);
		employeeAddress.setEmployee(null);

		return employeeAddress;
	}

	public List<EmployeeBankAccount> getEmployeeBankAccounts() {
		return this.employeeBankAccounts;
	}

	public void setEmployeeBankAccounts(
			List<EmployeeBankAccount> employeeBankAccounts) {
		this.employeeBankAccounts = employeeBankAccounts;
	}

	public EmployeeBankAccount addEmployeeBankAccount(
			EmployeeBankAccount employeeBankAccount) {
		getEmployeeBankAccounts().add(employeeBankAccount);
		employeeBankAccount.setEmployee(this);

		return employeeBankAccount;
	}

	public EmployeeBankAccount removeEmployeeBankAccount(
			EmployeeBankAccount employeeBankAccount) {
		getEmployeeBankAccounts().remove(employeeBankAccount);
		employeeBankAccount.setEmployee(null);

		return employeeBankAccount;
	}

	public List<EmployeeEmail> getEmployeeEmails() {
		return this.employeeEmails;
	}

	public void setEmployeeEmails(List<EmployeeEmail> employeeEmails) {
		this.employeeEmails = employeeEmails;
	}

	public EmployeeEmail addEmployeeEmail(EmployeeEmail employeeEmail) {
		getEmployeeEmails().add(employeeEmail);
		employeeEmail.setEmployee(this);

		return employeeEmail;
	}

	public EmployeeEmail removeEmployeeEmail(EmployeeEmail employeeEmail) {
		getEmployeeEmails().remove(employeeEmail);
		employeeEmail.setEmployee(null);

		return employeeEmail;
	}

	public EmployeeIdentity getEmployeeIdentity() {
		return this.employeeIdentity;
	}

	public void setEmployeeIdentity(EmployeeIdentity employeeIdentity) {
		this.employeeIdentity = employeeIdentity;
	}

	public List<EmployeePhoneNumber> getEmployeePhoneNumbers() {
		return this.employeePhoneNumbers;
	}

	public void setEmployeePhoneNumbers(
			List<EmployeePhoneNumber> employeePhoneNumbers) {
		this.employeePhoneNumbers = employeePhoneNumbers;
	}

	public EmployeePhoneNumber addEmployeePhoneNumber(
			EmployeePhoneNumber employeePhoneNumber) {
		getEmployeePhoneNumbers().add(employeePhoneNumber);
		employeePhoneNumber.setEmployee(this);

		return employeePhoneNumber;
	}

	public EmployeePhoneNumber removeEmployeePhoneNumber(
			EmployeePhoneNumber employeePhoneNumber) {
		getEmployeePhoneNumbers().remove(employeePhoneNumber);
		employeePhoneNumber.setEmployee(null);

		return employeePhoneNumber;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Ulkeler getUlkeler() {
		return ulkeler;
	}

	public void setUlkeler(Ulkeler ulkeler) {
		this.ulkeler = ulkeler;
	}

	// public List<ToolsUsage> getToolsUsages() {
	// return toolsUsages;
	// }
	//
	// public void setToolsUsages(List<ToolsUsage> toolsUsages) {
	// this.toolsUsages = toolsUsages;
	// }

	public List<EmployeeEventParticipation> getEmployeeEventParticipations() {
		return employeeEventParticipations;
	}

	public void setEmployeeEventParticipations(
			List<EmployeeEventParticipation> employeeEventParticipations) {
		this.employeeEventParticipations = employeeEventParticipations;
	}

	public List<LicenseRenewResponsible> getLicenseRenewResponsibles() {
		return licenseRenewResponsibles;
	}

	public void setLicenseRenewResponsibles(
			List<LicenseRenewResponsible> licenseRenewResponsibles) {
		this.licenseRenewResponsibles = licenseRenewResponsibles;
	}

	public List<OfferRelatedWith> getOfferRelatedWiths() {
		return offerRelatedWiths;
	}

	public void setOfferRelatedWiths(List<OfferRelatedWith> offerRelatedWiths) {
		this.offerRelatedWiths = offerRelatedWiths;
	}

	public List<SystemUser> getUsers() {
		return users;
	}

	public void setUsers(List<SystemUser> users) {
		this.users = users;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public EmployeeAddress getDefaultAddress() {
		for (EmployeeAddress employeeAddress : employeeAddresses) {
			if (employeeAddress.getIsDefault() == true)
				defaultAddress = employeeAddress;
		}
		return defaultAddress;
	}

	public void setDefaultAddress(EmployeeAddress defaultAddress) {
		this.defaultAddress = defaultAddress;
	}

	public EmployeeEmail getDefaultEmail() {
		for (EmployeeEmail employeeEmail : employeeEmails) {
			if (employeeEmail.getIsDefault() == true)
				defaultEmail = employeeEmail;
		}
		return defaultEmail;
	}

	public void setDefaultEmail(EmployeeEmail defaultEmail) {
		this.defaultEmail = defaultEmail;
	}

	public EmployeePhoneNumber getDefaultPhoneNumber() {
		for (EmployeePhoneNumber employeePhoneNumber : employeePhoneNumbers) {
			if (employeePhoneNumber.getIsDefault() == true)
				defaultPhoneNumber = employeePhoneNumber;
		}
		return defaultPhoneNumber;
	}

	public void setDefaultPhoneNumber(EmployeePhoneNumber defaultPhoneNumber) {
		this.defaultPhoneNumber = defaultPhoneNumber;
	}

	public List<EmployeeEducation> getEmployeeEducations() {
		return employeeEducations;
	}

	public void setEmployeeEducations(List<EmployeeEducation> employeeEducations) {
		this.employeeEducations = employeeEducations;
	}

	public List<EmployeeVocationalFile> getEmployeeVocationalFiles() {
		return employeeVocationalFiles;
	}

	public void setEmployeeVocationalFiles(
			List<EmployeeVocationalFile> employeeVocationalFiles) {
		this.employeeVocationalFiles = employeeVocationalFiles;
	}

	public List<EmployeeDiscipline> getEmployeeDisciplines() {
		return employeeDisciplines;
	}

	public void setEmployeeDisciplines(
			List<EmployeeDiscipline> employeeDisciplines) {
		this.employeeDisciplines = employeeDisciplines;
	}

	public EmployeeHealth getEmployeeHealth() {
		return employeeHealth;
	}

	public void setEmployeeHealth(EmployeeHealth employeeHealth) {
		this.employeeHealth = employeeHealth;
	}

	public int getLastYearsDisciplinePenaltyPoints() {

		lastYearsDisciplinePenaltyPoints = 0;

		for (EmployeeDiscipline employeeDiscipline : this
				.getEmployeeDisciplines()) {
			if (employeeDiscipline.getIsLastYearsRecord())
				lastYearsDisciplinePenaltyPoints += employeeDiscipline
						.getPenaltyPoint();
		}

		return lastYearsDisciplinePenaltyPoints;
	}

	public void setLastYearsDisciplinePenaltyPoints(int lastYearsPenaltyPoints) {
		this.lastYearsDisciplinePenaltyPoints = lastYearsPenaltyPoints;
	}

	public EmployeePayment getEmployeePayment() {
		return employeePayment;
	}

	public void setEmployeePayment(EmployeePayment employeePayment) {
		this.employeePayment = employeePayment;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Employee getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(Employee supervisor) {
		this.supervisor = supervisor;
	}

	// public List<Employee> getSubEmployees() {
	// return subEmployees;
	// }
	//
	// public void setSubEmployees(List<Employee> subEmployees) {
	// this.subEmployees = subEmployees;
	// }

	public List<EmployeePreviousExperience> getEmployeePreviousExperiences() {
		return employeePreviousExperiences;
	}

	public void setEmployeePreviousExperiences(
			List<EmployeePreviousExperience> employeePreviousExperiences) {
		this.employeePreviousExperiences = employeePreviousExperiences;
	}

	public List<EmployeePerformance> getEmployeePerformances() {
		return employeePerformances;
	}

	public void setEmployeePerformances(
			List<EmployeePerformance> employeePerformances) {
		this.employeePerformances = employeePerformances;
	}

	public List<EmployeeActivityParticipation> getEmployeeActivitityParticipations() {
		return employeeActivitityParticipations;
	}

	public void setEmployeeActivitityParticipations(
			List<EmployeeActivityParticipation> employeeActivitityParticipations) {
		this.employeeActivitityParticipations = employeeActivitityParticipations;
	}

	public List<EmployeeAdvanceRequest> getEmployeeAdvanceRequests() {
		return employeeAdvanceRequests;
	}

	public void setEmployeeAdvanceRequests(
			List<EmployeeAdvanceRequest> employeeAdvanceRequests) {
		this.employeeAdvanceRequests = employeeAdvanceRequests;
	}

	// public List<EmployeeAdvanceRequest>
	// getEmployeeAdvanceRequestsToBeApproved() {
	// return employeeAdvanceRequestsToBeApproved;
	// }
	//
	// public void setEmployeeAdvanceRequestsToBeApproved(
	// List<EmployeeAdvanceRequest> employeeAdvanceRequestsToBeApproved) {
	// this.employeeAdvanceRequestsToBeApproved =
	// employeeAdvanceRequestsToBeApproved;
	// }

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

	public List<EmployeeDayoff> getEmployeeDayoffs() {
		return employeeDayoffs;
	}

	public void setEmployeeDayoffs(List<EmployeeDayoff> employeeDayoffs) {
		this.employeeDayoffs = employeeDayoffs;
	}

	// public List<EmployeeDayoff> getSubEmployeeDayoffRequests() {
	// return subEmployeeDayoffRequests;
	// }
	//
	// public void setSubEmployeeDayoffRequests(
	// List<EmployeeDayoff> subEmployeeDayoffRequests) {
	// this.subEmployeeDayoffRequests = subEmployeeDayoffRequests;
	// }

	public int getDayoffDayCountOfLastYear() {

		dayoffDayCountOfLastYear = 0;

		Date la = this.getLastAnniversary();

		for (EmployeeDayoff employeeDayoff : this.getEmployeeDayoffs()) {
			if (employeeDayoff.getStartDate().compareTo(la) > 0
					&& employeeDayoff.getHrApproval() == 1) {
				dayoffDayCountOfLastYear += employeeDayoff.getDayCount();
			}
		}

		return dayoffDayCountOfLastYear;
	}

	public void setDayoffDayCountOfLastYear(int dayoffDayCountOfLastYear) {
		this.dayoffDayCountOfLastYear = dayoffDayCountOfLastYear;
	}

	@SuppressWarnings("deprecation")
	public Date getLastAnniversary() {

		lastAnniversary = new Date();

		lastAnniversary = this.getStartDate();
		Date now = new Date();

		lastAnniversary.setYear(now.getYear());

		if (lastAnniversary.compareTo(now) > 0) {
			lastAnniversary.setYear(now.getYear() - 1);
		}

		return lastAnniversary;
	}

	public void setLastAnniversary(Date lastAnniversary) {
		this.lastAnniversary = lastAnniversary;
	}

	/**
	 * @return the kalanIzin
	 */
	public Integer getKalanIzin() {
		return kalanIzin;
	}

	/**
	 * @param kalanIzin
	 *            the kalanIzin to set
	 */
	public void setKalanIzin(Integer kalanIzin) {
		this.kalanIzin = kalanIzin;
	}

}