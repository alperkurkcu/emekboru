package com.emekboru.jpa.sales.events;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "sales_event_type")
public class SalesEventType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2951006381684665919L;

	@Id
	@Column(name = "type_id")
	private int typeId;

	@Column(name = "title")
	private String name;

	@OneToMany
	private List<SalesEvent> salesEvent;

	@Override
	public boolean equals(Object o) {
		return this.getTypeId() == ((SalesEventType) o).getTypeId();
	}

	public SalesEventType() {
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SalesEvent> getSalesEvent() {
		return salesEvent;
	}

	public void setSalesEvent(List<SalesEvent> salesEvent) {
		this.salesEvent = salesEvent;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
