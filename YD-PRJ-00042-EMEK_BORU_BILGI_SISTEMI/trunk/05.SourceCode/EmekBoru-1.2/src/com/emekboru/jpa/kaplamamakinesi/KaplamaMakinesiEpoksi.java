package com.emekboru.jpa.kaplamamakinesi;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the kaplama_makinesi_epoksi database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "KaplamaMakinesiEpoksi.findAllByPipeId", query = "SELECT r FROM KaplamaMakinesiEpoksi r WHERE r.pipe.pipeId=:prmPipeId order by r.eklemeZamani") })
@Table(name = "kaplama_makinesi_epoksi")
public class KaplamaMakinesiEpoksi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "KAPLAMA_MAKINESI_EPOKSI_ID_GENERATOR", sequenceName = "KAPLAMA_MAKINESI_EPOKSI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "KAPLAMA_MAKINESI_EPOKSI_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	private Boolean durum;

	@Column(name = "ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "kaplama_baslama_kullanici")
	private Integer kaplamaBaslamaKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "kaplama_baslama_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser kaplamaBaslamaUser;

	@Column(name = "kaplama_baslama_zamani")
	private Timestamp kaplamaBaslamaZamani;

	@Column(name = "kaplama_bitis_kullanici")
	private Integer kaplamaBitisKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "kaplama_bitis_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser kaplamaBitisUser;

	@Column(name = "kaplama_bitis_zamani")
	private Timestamp kaplamaBitisZamani;

	@Basic
	@Column(name = "pipe_id", nullable = false, insertable = false, updatable = false)
	private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@Column(name = "kaplama_yuzeyi")
	private Integer kaplamaYuzeyi;

	@Column(name = "kaplama_kat")
	private Integer kaplamaKat;

	public KaplamaMakinesiEpoksi() {

	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getDurum() {
		return this.durum;
	}

	public void setDurum(Boolean durum) {
		this.durum = durum;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Integer getKaplamaBaslamaKullanici() {
		return this.kaplamaBaslamaKullanici;
	}

	public void setKaplamaBaslamaKullanici(Integer kaplamaBaslamaKullanici) {
		this.kaplamaBaslamaKullanici = kaplamaBaslamaKullanici;
	}

	public Timestamp getKaplamaBaslamaZamani() {
		return this.kaplamaBaslamaZamani;
	}

	public void setKaplamaBaslamaZamani(Timestamp kaplamaBaslamaZamani) {
		this.kaplamaBaslamaZamani = kaplamaBaslamaZamani;
	}

	public Integer getKaplamaBitisKullanici() {
		return this.kaplamaBitisKullanici;
	}

	public void setKaplamaBitisKullanici(Integer kaplamaBitisKullanici) {
		this.kaplamaBitisKullanici = kaplamaBitisKullanici;
	}

	public Timestamp getKaplamaBitisZamani() {
		return this.kaplamaBitisZamani;
	}

	public void setKaplamaBitisZamani(Timestamp kaplamaBitisZamani) {
		this.kaplamaBitisZamani = kaplamaBitisZamani;
	}

	public SystemUser getKaplamaBaslamaUser() {
		return kaplamaBaslamaUser;
	}

	public void setKaplamaBaslamaUser(SystemUser kaplamaBaslamaUser) {
		this.kaplamaBaslamaUser = kaplamaBaslamaUser;
	}

	public SystemUser getKaplamaBitisUser() {
		return kaplamaBitisUser;
	}

	public void setKaplamaBitisUser(SystemUser kaplamaBitisUser) {
		this.kaplamaBitisUser = kaplamaBitisUser;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getKaplamaYuzeyi() {
		return kaplamaYuzeyi;
	}

	public void setKaplamaYuzeyi(Integer kaplamaYuzeyi) {
		this.kaplamaYuzeyi = kaplamaYuzeyi;
	}

	/**
	 * @return the kaplamaKat
	 */
	public Integer getKaplamaKat() {
		return kaplamaKat;
	}

	/**
	 * @param kaplamaKat
	 *            the kaplamaKat to set
	 */
	public void setKaplamaKat(Integer kaplamaKat) {
		this.kaplamaKat = kaplamaKat;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the eklemeZamani
	 */
	public Timestamp getEklemeZamani() {
		return eklemeZamani;
	}

	/**
	 * @param eklemeZamani
	 *            the eklemeZamani to set
	 */
	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	/**
	 * @return the guncellemeZamani
	 */
	public Timestamp getGuncellemeZamani() {
		return guncellemeZamani;
	}

	/**
	 * @param guncellemeZamani
	 *            the guncellemeZamani to set
	 */
	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

}