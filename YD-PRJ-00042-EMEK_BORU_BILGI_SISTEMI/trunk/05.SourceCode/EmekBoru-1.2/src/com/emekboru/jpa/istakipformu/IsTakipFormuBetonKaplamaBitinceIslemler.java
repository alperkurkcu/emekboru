package com.emekboru.jpa.istakipformu;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the is_takip_formu_beton_kaplama_bitince_islemler
 * database table.
 * 
 */
@Entity
@Table(name = "is_takip_formu_beton_kaplama_bitince_islemler")
public class IsTakipFormuBetonKaplamaBitinceIslemler implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IS_TAKIP_FORMU_BETON_KAPLAMA_BITINCE_ISLEMLER_ID_GENERATOR", sequenceName = "IS_TAKIP_FORMU_BETON_KAPLAMA_BITINCE_ISLEMLER_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IS_TAKIP_FORMU_BETON_KAPLAMA_BITINCE_ISLEMLER_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "gerceklesen_sarfiyat")
	private Integer gerceklesenSarfiyat;

	@Column(name = "gerceklesen_sarfiyat_cimento")
	private Integer gerceklesenSarfiyatCimento;

	@Column(name = "gerceklesen_sarfiyat_katki")
	private Integer gerceklesenSarfiyatKatki;

	@Column(name = "gerceklesen_sarfiyat_kum")
	private Integer gerceklesenSarfiyatKum;

	@Column(name = "gerceklesen_sarfiyat_su")
	private Integer gerceklesenSarfiyatSu;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name = "is_takip_form_id")
	// private Integer isTakipFormId;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "is_takip_form_id", referencedColumnName = "ID", insertable = false, updatable = false)
	private IsTakipFormuBetonKaplama isTakipFormuBetaonKaplama;

	@Column(name = "hedeflenen_sarfiyat")
	private Integer hedeflenenSarfiyat;

	@Column(name = "hedeflenen_sarfiyat_cimento")
	private Integer hedeflenenSarfiyatCimento;

	@Column(name = "hedeflenen_sarfiyat_katki")
	private Integer hedeflenenSarfiyatKatki;

	@Column(name = "hedeflenen_sarfiyat_kum")
	private Integer hedeflenenSarfiyatKum;

	@Column(name = "hedeflenen_sarfiyat_su")
	private Integer hedeflenenSarfiyatSu;

	public IsTakipFormuBetonKaplamaBitinceIslemler() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Integer getGerceklesenSarfiyat() {
		return this.gerceklesenSarfiyat;
	}

	public void setGerceklesenSarfiyat(Integer gerceklesenSarfiyat) {
		this.gerceklesenSarfiyat = gerceklesenSarfiyat;
	}

	public Integer getGerceklesenSarfiyatCimento() {
		return this.gerceklesenSarfiyatCimento;
	}

	public void setGerceklesenSarfiyatCimento(Integer gerceklesenSarfiyatCimento) {
		this.gerceklesenSarfiyatCimento = gerceklesenSarfiyatCimento;
	}

	public Integer getGerceklesenSarfiyatKatki() {
		return this.gerceklesenSarfiyatKatki;
	}

	public void setGerceklesenSarfiyatKatki(Integer gerceklesenSarfiyatKatki) {
		this.gerceklesenSarfiyatKatki = gerceklesenSarfiyatKatki;
	}

	public Integer getGerceklesenSarfiyatKum() {
		return this.gerceklesenSarfiyatKum;
	}

	public void setGerceklesenSarfiyatKum(Integer gerceklesenSarfiyatKum) {
		this.gerceklesenSarfiyatKum = gerceklesenSarfiyatKum;
	}

	public Integer getGerceklesenSarfiyatSu() {
		return this.gerceklesenSarfiyatSu;
	}

	public void setGerceklesenSarfiyatSu(Integer gerceklesenSarfiyatSu) {
		this.gerceklesenSarfiyatSu = gerceklesenSarfiyatSu;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Integer getHedeflenenSarfiyat() {
		return this.hedeflenenSarfiyat;
	}

	public void setHedeflenenSarfiyat(Integer hedeflenenSarfiyat) {
		this.hedeflenenSarfiyat = hedeflenenSarfiyat;
	}

	public Integer getHedeflenenSarfiyatCimento() {
		return this.hedeflenenSarfiyatCimento;
	}

	public void setHedeflenenSarfiyatCimento(Integer hedeflenenSarfiyatCimento) {
		this.hedeflenenSarfiyatCimento = hedeflenenSarfiyatCimento;
	}

	public Integer getHedeflenenSarfiyatKatki() {
		return this.hedeflenenSarfiyatKatki;
	}

	public void setHedeflenenSarfiyatKatki(Integer hedeflenenSarfiyatKatki) {
		this.hedeflenenSarfiyatKatki = hedeflenenSarfiyatKatki;
	}

	public Integer getHedeflenenSarfiyatKum() {
		return this.hedeflenenSarfiyatKum;
	}

	public void setHedeflenenSarfiyatKum(Integer hedeflenenSarfiyatKum) {
		this.hedeflenenSarfiyatKum = hedeflenenSarfiyatKum;
	}

	public Integer getHedeflenenSarfiyatSu() {
		return this.hedeflenenSarfiyatSu;
	}

	public void setHedeflenenSarfiyatSu(Integer hedeflenenSarfiyatSu) {
		this.hedeflenenSarfiyatSu = hedeflenenSarfiyatSu;
	}

	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	public IsTakipFormuBetonKaplama getIsTakipFormuBetaonKaplama() {
		return isTakipFormuBetaonKaplama;
	}

	public void setIsTakipFormuBetaonKaplama(
			IsTakipFormuBetonKaplama isTakipFormuBetaonKaplama) {
		this.isTakipFormuBetaonKaplama = isTakipFormuBetaonKaplama;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}