package com.emekboru.jpa.isemri;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the is_emri_polietilen_kaplama_kontrol_parametreleri
 * database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "IsEmriPolietilenKaplamaKontrolParametreleri.findAllByIsEmriId", query = "SELECT r FROM IsEmriPolietilenKaplamaKontrolParametreleri r WHERE r.isEmriPolietilenKaplama.id=:prmIsEmriId order by r.eklemeZamani") })
@Table(name = "is_emri_polietilen_kaplama_kontrol_parametreleri")
public class IsEmriPolietilenKaplamaKontrolParametreleri implements
		Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IS_EMRI_POLIETILEN_KAPLAMA_KONTROL_PARAMETRELERI_ID_GENERATOR", sequenceName = "IS_EMRI_POLIETILEN_KAPLAMA_KONTROL_PARAMETRELERI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IS_EMRI_POLIETILEN_KAPLAMA_KONTROL_PARAMETRELERI_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;
	//
	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	@Basic
	@Column(name = "is_emri_id", nullable = false, insertable = false, updatable = false)
	private Integer isEmriId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "is_emri_id", referencedColumnName = "ID", insertable = false)
	private IsEmriPolietilenKaplama isEmriPolietilenKaplama;

	@Column(name = "boya_miktari")
	private Integer boyaMiktari;

	@Column(name = "hava_debisi")
	private Integer havaDebisi;

	@Column(name = "program")
	private Integer program;

	@Column(name = "voltaj")
	private String voltaj;

	@Column(name = "amper")
	private String amper;

	@Column(name = "elektrod_temizleme_havasi")
	private Float elektrodTemizlemeHavasi;

	public IsEmriPolietilenKaplamaKontrolParametreleri() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	public IsEmriPolietilenKaplama getIsEmriPolietilenKaplama() {
		return isEmriPolietilenKaplama;
	}

	public void setIsEmriPolietilenKaplama(
			IsEmriPolietilenKaplama isEmriPolietilenKaplama) {
		this.isEmriPolietilenKaplama = isEmriPolietilenKaplama;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getBoyaMiktari() {
		return boyaMiktari;
	}

	public void setBoyaMiktari(Integer boyaMiktari) {
		this.boyaMiktari = boyaMiktari;
	}

	public Integer getHavaDebisi() {
		return havaDebisi;
	}

	public void setHavaDebisi(Integer havaDebisi) {
		this.havaDebisi = havaDebisi;
	}

	public Integer getProgram() {
		return program;
	}

	public void setProgram(Integer program) {
		this.program = program;
	}

	public String getVoltaj() {
		return voltaj;
	}

	public void setVoltaj(String voltaj) {
		this.voltaj = voltaj;
	}

	public String getAmper() {
		return amper;
	}

	public void setAmper(String amper) {
		this.amper = amper;
	}

	/**
	 * @return the elektrodTemizlemeHavasi
	 */
	public Float getElektrodTemizlemeHavasi() {
		return elektrodTemizlemeHavasi;
	}

	/**
	 * @param elektrodTemizlemeHavasi
	 *            the elektrodTemizlemeHavasi to set
	 */
	public void setElektrodTemizlemeHavasi(Float elektrodTemizlemeHavasi) {
		this.elektrodTemizlemeHavasi = elektrodTemizlemeHavasi;
	}

}