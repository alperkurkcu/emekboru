package com.emekboru.jpa.istakipformu;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the is_takip_formu_beton_kaplama_sonra_islemler
 * database table.
 * 
 */
@Entity
@Table(name = "is_takip_formu_beton_kaplama_sonra_islemler")
public class IsTakipFormuBetonKaplamaSonraIslemler implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IS_TAKIP_FORMU_BETON_KAPLAMA_SONRA_ISLEMLER_ID_GENERATOR", sequenceName = "IS_TAKIP_FORMU_EPOKSI_KAPLAMA_SONRA_ISLEMLER_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IS_TAKIP_FORMU_BETON_KAPLAMA_SONRA_ISLEMLER_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "ayar_sonrasi_ilk_boru_beton_kalinlik")
	private Integer ayarSonrasiIlkBoruBetonKalinlik;

	@Column(name = "ayar_sonrasi_ilk_boru_beton_kalinlik_boru_id")
	private Integer ayarSonrasiIlkBoruBetonKalinlikBoruId;

	@Temporal(TemporalType.DATE)
	@Column(name = "ayar_sonrasi_ilk_boru_beton_kalinlik_tarihi")
	private Date ayarSonrasiIlkBoruBetonKalinlikTarihi;

	@Column(name = "ayar_sonrasi_ilk_boru_id")
	private Integer ayarSonrasiIlkBoruId;

	@Column(name = "ayar_sonrasi_ilk_boru_yas_kalinlik_check")
	private Boolean ayarSonrasiIlkBoruYasKalinlikCheck;

	@Temporal(TemporalType.DATE)
	@Column(name = "ayar_sonrasi_ilk_boru_yas_kalinlik_tarihi")
	private Date ayarSonrasiIlkBoruYasKalinlikTarihi;

	@Column(name = "beton_hizi")
	private Integer betonHizi;

	@Column(name = "beton_hizi_check")
	private Boolean betonHiziCheck;

	@Temporal(TemporalType.DATE)
	@Column(name = "beton_hizi_tarihi")
	private Date betonHiziTarihi;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name = "is_takip_form_id")
	// private Integer isTakipFormId;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "is_takip_form_id", referencedColumnName = "ID", insertable = false, updatable = false)
	private IsTakipFormuBetonKaplama isTakipFormuBetonKaplama;

	@Column(name = "ayar_sonrasi_ilk_boru_yas_kalinlik_aciklama", length = 255)
	private String ayarSonrasiIlkBoruYasKalinlikAciklama;

	@Column(name = "ayar_sonrasi_ilk_boru_beton_kalinlik_aciklama", length = 255)
	private String ayarSonrasiIlkBoruBetonKalinlikAciklama;

	@Column(name = "beton_hizi_aciklama", length = 255)
	private String betonHiziAciklama;

	@Column(name = "ayar_sonrasi_ilk_boru_beton_kalinlik_check")
	private Boolean ayarSonrasiIlkBoruBetonKalinlikCheck;

	public IsTakipFormuBetonKaplamaSonraIslemler() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAyarSonrasiIlkBoruBetonKalinlik() {
		return this.ayarSonrasiIlkBoruBetonKalinlik;
	}

	public void setAyarSonrasiIlkBoruBetonKalinlik(
			Integer ayarSonrasiIlkBoruBetonKalinlik) {
		this.ayarSonrasiIlkBoruBetonKalinlik = ayarSonrasiIlkBoruBetonKalinlik;
	}

	public Integer getAyarSonrasiIlkBoruBetonKalinlikBoruId() {
		return this.ayarSonrasiIlkBoruBetonKalinlikBoruId;
	}

	public void setAyarSonrasiIlkBoruBetonKalinlikBoruId(
			Integer ayarSonrasiIlkBoruBetonKalinlikBoruId) {
		this.ayarSonrasiIlkBoruBetonKalinlikBoruId = ayarSonrasiIlkBoruBetonKalinlikBoruId;
	}

	public Date getAyarSonrasiIlkBoruBetonKalinlikTarihi() {
		return this.ayarSonrasiIlkBoruBetonKalinlikTarihi;
	}

	public void setAyarSonrasiIlkBoruBetonKalinlikTarihi(
			Date ayarSonrasiIlkBoruBetonKalinlikTarihi) {
		this.ayarSonrasiIlkBoruBetonKalinlikTarihi = ayarSonrasiIlkBoruBetonKalinlikTarihi;
	}

	public Integer getAyarSonrasiIlkBoruId() {
		return this.ayarSonrasiIlkBoruId;
	}

	public void setAyarSonrasiIlkBoruId(Integer ayarSonrasiIlkBoruId) {
		this.ayarSonrasiIlkBoruId = ayarSonrasiIlkBoruId;
	}

	public Boolean getAyarSonrasiIlkBoruYasKalinlikCheck() {
		return this.ayarSonrasiIlkBoruYasKalinlikCheck;
	}

	public void setAyarSonrasiIlkBoruYasKalinlikCheck(
			Boolean ayarSonrasiIlkBoruYasKalinlikCheck) {
		this.ayarSonrasiIlkBoruYasKalinlikCheck = ayarSonrasiIlkBoruYasKalinlikCheck;
	}

	public Date getAyarSonrasiIlkBoruYasKalinlikTarihi() {
		return this.ayarSonrasiIlkBoruYasKalinlikTarihi;
	}

	public void setAyarSonrasiIlkBoruYasKalinlikTarihi(
			Date ayarSonrasiIlkBoruYasKalinlikTarihi) {
		this.ayarSonrasiIlkBoruYasKalinlikTarihi = ayarSonrasiIlkBoruYasKalinlikTarihi;
	}

	public Integer getBetonHizi() {
		return this.betonHizi;
	}

	public void setBetonHizi(Integer betonHizi) {
		this.betonHizi = betonHizi;
	}

	public Boolean getBetonHiziCheck() {
		return this.betonHiziCheck;
	}

	public void setBetonHiziCheck(Boolean betonHiziCheck) {
		this.betonHiziCheck = betonHiziCheck;
	}

	public Date getBetonHiziTarihi() {
		return this.betonHiziTarihi;
	}

	public void setBetonHiziTarihi(Date betonHiziTarihi) {
		this.betonHiziTarihi = betonHiziTarihi;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	public IsTakipFormuBetonKaplama getIsTakipFormuBetonKaplama() {
		return isTakipFormuBetonKaplama;
	}

	public void setIsTakipFormuBetonKaplama(
			IsTakipFormuBetonKaplama isTakipFormuBetonKaplama) {
		this.isTakipFormuBetonKaplama = isTakipFormuBetonKaplama;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getAyarSonrasiIlkBoruYasKalinlikAciklama() {
		return ayarSonrasiIlkBoruYasKalinlikAciklama;
	}

	public void setAyarSonrasiIlkBoruYasKalinlikAciklama(
			String ayarSonrasiIlkBoruYasKalinlikAciklama) {
		this.ayarSonrasiIlkBoruYasKalinlikAciklama = ayarSonrasiIlkBoruYasKalinlikAciklama;
	}

	public String getAyarSonrasiIlkBoruBetonKalinlikAciklama() {
		return ayarSonrasiIlkBoruBetonKalinlikAciklama;
	}

	public void setAyarSonrasiIlkBoruBetonKalinlikAciklama(
			String ayarSonrasiIlkBoruBetonKalinlikAciklama) {
		this.ayarSonrasiIlkBoruBetonKalinlikAciklama = ayarSonrasiIlkBoruBetonKalinlikAciklama;
	}

	public String getBetonHiziAciklama() {
		return betonHiziAciklama;
	}

	public void setBetonHiziAciklama(String betonHiziAciklama) {
		this.betonHiziAciklama = betonHiziAciklama;
	}

	public Boolean getAyarSonrasiIlkBoruBetonKalinlikCheck() {
		return ayarSonrasiIlkBoruBetonKalinlikCheck;
	}

	public void setAyarSonrasiIlkBoruBetonKalinlikCheck(
			Boolean ayarSonrasiIlkBoruBetonKalinlikCheck) {
		this.ayarSonrasiIlkBoruBetonKalinlikCheck = ayarSonrasiIlkBoruBetonKalinlikCheck;
	}

}