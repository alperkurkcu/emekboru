package com.emekboru.jpa;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiAmg1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiAmg1Spec;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiBkm1KatiMadde;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiBkm1KuruMadde;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiBkm1Ph;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiEpoksiTozEea1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiEpoksiTozEjs1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiEpoksiTozEkz1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiEpoksiTozEni1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiEpoksiTozEta1Sonuc;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiEpoksiTozEty1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiMfr1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiMfr1Spec;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiOit1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiPdd1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiPdd1Spec;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiSertlik;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiSertlikSpec;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeUreticiKst1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeUreticiMfr1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeUreticiOit1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeUreticiPdd1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeUreticiPkd1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeUreticiPks1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeUreticiPku1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeUreticiYst1;

/**
 * The persistent class for the coat_raw_material database table.
 * 
 */
@Entity
@Table(name = "coat_raw_material")
public class CoatRawMaterial implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "coat_material_id")
	@SequenceGenerator(name = "COAT_RAW_MATERIAL_GENERATOR", sequenceName = "COAT_RAW_MATERIAL_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COAT_RAW_MATERIAL_GENERATOR")
	private Integer coatMaterialId;

	@Column(name = "miktari")
	private double miktari;

	@Column(name = "remaining_amount")
	private double remainingAmount;

	@Column(name = "parti_no")
	private String partiNo;

	@Basic
	@Column(name = "coat_material_type_id", insertable = false, updatable = false)
	private Integer coatMaterialTypeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coat_material_type_id", referencedColumnName = "coat_material_type_id", insertable = false)
	private CoatMaterialType coatMaterialType;

	private String urun;

	@Temporal(TemporalType.DATE)
	@Column(name = "entrance_date")
	private Date entranceDate;

	private String marka;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeGirdiEpoksiTozEta1Sonuc> testHammaddeGirdiEpoksiTozEta1Sonucs;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeGirdiEpoksiTozEni1> testHammaddeGirdiEpoksiTozEni1s;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeGirdiEpoksiTozEjs1> testHammaddeGirdiEpoksiTozEjs1s;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeGirdiEpoksiTozEea1> testHammaddeGirdiEpoksiTozEea1s;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeGirdiEpoksiTozEkz1> testHammaddeGirdiEpoksiTozEkz1s;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeGirdiEpoksiTozEty1> testHammaddeGirdiEpoksiTozEty1s;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeGirdiMfr1> testHammaddeGirdiMfr1s;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeGirdiOit1> testHammaddeGirdiOit1s;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeGirdiPdd1> testHammaddeGirdiPdd1s;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeGirdiBkm1Ph> testHammaddeGirdiBkm1Phs;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeGirdiBkm1KatiMadde> testHammaddeGirdiBkm1KatiMaddes;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeGirdiBkm1KuruMadde> testHammaddeGirdiBkm1KuruMaddes;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeGirdiSertlik> testHammaddeGirdiSertliks;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeGirdiAmg1> testHammaddeGirdiAmg1s;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeGirdiMfr1Spec> testHammaddeGirdiMfr1Specs;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeGirdiPdd1Spec> testHammaddeGirdiPdd1Specs;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeGirdiSertlikSpec> testHammaddeGirdiSertlikSpecs;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeGirdiAmg1Spec> testHammaddeGirdiAmg1Specs;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeUreticiMfr1> testHammaddeUreticiMfr1s;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeUreticiOit1> testHammaddeUreticiOit1s;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeUreticiPdd1> testHammaddeUreticiPdd1s;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeUreticiYst1> testHammaddeUreticiYst1s;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeUreticiKst1> testHammaddeUreticiKst1s;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeUreticiPkd1> testHammaddeUreticiPkd1s;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeUreticiPks1> testHammaddeUreticiPks1s;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "coatRawMaterial", cascade = CascadeType.ALL)
	private List<TestHammaddeUreticiPku1> testHammaddeUreticiPku1s;

	public CoatRawMaterial() {
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof CoatRawMaterial
				&& ((CoatRawMaterial) obj).getCoatMaterialId() == this
						.getCoatMaterialId())
			return true;
		return false;
	}

	public Integer getCoatMaterialId() {
		return this.coatMaterialId;
	}

	public void setCoatMaterialId(Integer coatMaterialId) {
		this.coatMaterialId = coatMaterialId;
	}

	public double getMiktari() {
		return this.miktari;
	}

	public void setMiktari(double miktari) {
		this.miktari = miktari;
	}

	public String getPartiNo() {
		return this.partiNo;
	}

	public void setPartiNo(String partiNo) {
		this.partiNo = partiNo;
	}

	public String getUrun() {
		return this.urun;
	}

	public void setUrun(String urun) {
		this.urun = urun;
	}

	public Integer getCoatMaterialTypeId() {
		return coatMaterialTypeId;
	}

	public void setCoatMaterialTypeId(Integer coatMaterialTypeId) {
		this.coatMaterialTypeId = coatMaterialTypeId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public double getRemainingAmount() {
		return remainingAmount;
	}

	public void setRemainingAmount(double remainingAmount) {
		this.remainingAmount = remainingAmount;
	}

	public Date getEntranceDate() {
		return entranceDate;
	}

	public void setEntranceDate(Date entranceDate) {
		this.entranceDate = entranceDate;
	}

	public String getMarka() {
		return marka;
	}

	public void setMarka(String marka) {
		this.marka = marka;
	}

	/**
	 * @return the testHammaddeGirdiEpoksiTozEta1Sonucs
	 */
	public List<TestHammaddeGirdiEpoksiTozEta1Sonuc> getTestHammaddeGirdiEpoksiTozEta1Sonucs() {
		return testHammaddeGirdiEpoksiTozEta1Sonucs;
	}

	/**
	 * @param testHammaddeGirdiEpoksiTozEta1Sonucs
	 *            the testHammaddeGirdiEpoksiTozEta1Sonucs to set
	 */
	public void setTestHammaddeGirdiEpoksiTozEta1Sonucs(
			List<TestHammaddeGirdiEpoksiTozEta1Sonuc> testHammaddeGirdiEpoksiTozEta1Sonucs) {
		this.testHammaddeGirdiEpoksiTozEta1Sonucs = testHammaddeGirdiEpoksiTozEta1Sonucs;
	}

	/**
	 * @return the testHammaddeGirdiEpoksiTozEni1s
	 */
	public List<TestHammaddeGirdiEpoksiTozEni1> getTestHammaddeGirdiEpoksiTozEni1s() {
		return testHammaddeGirdiEpoksiTozEni1s;
	}

	/**
	 * @param testHammaddeGirdiEpoksiTozEni1s
	 *            the testHammaddeGirdiEpoksiTozEni1s to set
	 */
	public void setTestHammaddeGirdiEpoksiTozEni1s(
			List<TestHammaddeGirdiEpoksiTozEni1> testHammaddeGirdiEpoksiTozEni1s) {
		this.testHammaddeGirdiEpoksiTozEni1s = testHammaddeGirdiEpoksiTozEni1s;
	}

	/**
	 * @return the testHammaddeGirdiEpoksiTozEjs1s
	 */
	public List<TestHammaddeGirdiEpoksiTozEjs1> getTestHammaddeGirdiEpoksiTozEjs1s() {
		return testHammaddeGirdiEpoksiTozEjs1s;
	}

	/**
	 * @param testHammaddeGirdiEpoksiTozEjs1s
	 *            the testHammaddeGirdiEpoksiTozEjs1s to set
	 */
	public void setTestHammaddeGirdiEpoksiTozEjs1s(
			List<TestHammaddeGirdiEpoksiTozEjs1> testHammaddeGirdiEpoksiTozEjs1s) {
		this.testHammaddeGirdiEpoksiTozEjs1s = testHammaddeGirdiEpoksiTozEjs1s;
	}

	/**
	 * @return the testHammaddeGirdiEpoksiTozEea1s
	 */
	public List<TestHammaddeGirdiEpoksiTozEea1> getTestHammaddeGirdiEpoksiTozEea1s() {
		return testHammaddeGirdiEpoksiTozEea1s;
	}

	/**
	 * @param testHammaddeGirdiEpoksiTozEea1s
	 *            the testHammaddeGirdiEpoksiTozEea1s to set
	 */
	public void setTestHammaddeGirdiEpoksiTozEea1s(
			List<TestHammaddeGirdiEpoksiTozEea1> testHammaddeGirdiEpoksiTozEea1s) {
		this.testHammaddeGirdiEpoksiTozEea1s = testHammaddeGirdiEpoksiTozEea1s;
	}

	/**
	 * @return the testHammaddeGirdiEpoksiTozEkz1s
	 */
	public List<TestHammaddeGirdiEpoksiTozEkz1> getTestHammaddeGirdiEpoksiTozEkz1s() {
		return testHammaddeGirdiEpoksiTozEkz1s;
	}

	/**
	 * @param testHammaddeGirdiEpoksiTozEkz1s
	 *            the testHammaddeGirdiEpoksiTozEkz1s to set
	 */
	public void setTestHammaddeGirdiEpoksiTozEkz1s(
			List<TestHammaddeGirdiEpoksiTozEkz1> testHammaddeGirdiEpoksiTozEkz1s) {
		this.testHammaddeGirdiEpoksiTozEkz1s = testHammaddeGirdiEpoksiTozEkz1s;
	}

	/**
	 * @return the testHammaddeGirdiMfr1s
	 */
	public List<TestHammaddeGirdiMfr1> getTestHammaddeGirdiMfr1s() {
		return testHammaddeGirdiMfr1s;
	}

	/**
	 * @param testHammaddeGirdiMfr1s
	 *            the testHammaddeGirdiMfr1s to set
	 */
	public void setTestHammaddeGirdiMfr1s(
			List<TestHammaddeGirdiMfr1> testHammaddeGirdiMfr1s) {
		this.testHammaddeGirdiMfr1s = testHammaddeGirdiMfr1s;
	}

	/**
	 * @return the testHammaddeGirdiOit1s
	 */
	public List<TestHammaddeGirdiOit1> getTestHammaddeGirdiOit1s() {
		return testHammaddeGirdiOit1s;
	}

	/**
	 * @param testHammaddeGirdiOit1s
	 *            the testHammaddeGirdiOit1s to set
	 */
	public void setTestHammaddeGirdiOit1s(
			List<TestHammaddeGirdiOit1> testHammaddeGirdiOit1s) {
		this.testHammaddeGirdiOit1s = testHammaddeGirdiOit1s;
	}

	/**
	 * @return the testHammaddeGirdiPdd1s
	 */
	public List<TestHammaddeGirdiPdd1> getTestHammaddeGirdiPdd1s() {
		return testHammaddeGirdiPdd1s;
	}

	/**
	 * @param testHammaddeGirdiPdd1s
	 *            the testHammaddeGirdiPdd1s to set
	 */
	public void setTestHammaddeGirdiPdd1s(
			List<TestHammaddeGirdiPdd1> testHammaddeGirdiPdd1s) {
		this.testHammaddeGirdiPdd1s = testHammaddeGirdiPdd1s;
	}

	/**
	 * @return the testHammaddeGirdiEpoksiTozEty1s
	 */
	public List<TestHammaddeGirdiEpoksiTozEty1> getTestHammaddeGirdiEpoksiTozEty1s() {
		return testHammaddeGirdiEpoksiTozEty1s;
	}

	/**
	 * @param testHammaddeGirdiEpoksiTozEty1s
	 *            the testHammaddeGirdiEpoksiTozEty1s to set
	 */
	public void setTestHammaddeGirdiEpoksiTozEty1s(
			List<TestHammaddeGirdiEpoksiTozEty1> testHammaddeGirdiEpoksiTozEty1s) {
		this.testHammaddeGirdiEpoksiTozEty1s = testHammaddeGirdiEpoksiTozEty1s;
	}

	/**
	 * @return the testHammaddeGirdiMfr1Specs
	 */
	public List<TestHammaddeGirdiMfr1Spec> getTestHammaddeGirdiMfr1Specs() {
		return testHammaddeGirdiMfr1Specs;
	}

	/**
	 * @param testHammaddeGirdiMfr1Specs
	 *            the testHammaddeGirdiMfr1Specs to set
	 */
	public void setTestHammaddeGirdiMfr1Specs(
			List<TestHammaddeGirdiMfr1Spec> testHammaddeGirdiMfr1Specs) {
		this.testHammaddeGirdiMfr1Specs = testHammaddeGirdiMfr1Specs;
	}

	/**
	 * @return the testHammaddeGirdiPdd1Specs
	 */
	public List<TestHammaddeGirdiPdd1Spec> getTestHammaddeGirdiPdd1Specs() {
		return testHammaddeGirdiPdd1Specs;
	}

	/**
	 * @param testHammaddeGirdiPdd1Specs
	 *            the testHammaddeGirdiPdd1Specs to set
	 */
	public void setTestHammaddeGirdiPdd1Specs(
			List<TestHammaddeGirdiPdd1Spec> testHammaddeGirdiPdd1Specs) {
		this.testHammaddeGirdiPdd1Specs = testHammaddeGirdiPdd1Specs;
	}

	/**
	 * @return the testHammaddeUreticiMfr1s
	 */
	public List<TestHammaddeUreticiMfr1> getTestHammaddeUreticiMfr1s() {
		return testHammaddeUreticiMfr1s;
	}

	/**
	 * @param testHammaddeUreticiMfr1s
	 *            the testHammaddeUreticiMfr1s to set
	 */
	public void setTestHammaddeUreticiMfr1s(
			List<TestHammaddeUreticiMfr1> testHammaddeUreticiMfr1s) {
		this.testHammaddeUreticiMfr1s = testHammaddeUreticiMfr1s;
	}

	/**
	 * @return the testHammaddeUreticiOit1s
	 */
	public List<TestHammaddeUreticiOit1> getTestHammaddeUreticiOit1s() {
		return testHammaddeUreticiOit1s;
	}

	/**
	 * @param testHammaddeUreticiOit1s
	 *            the testHammaddeUreticiOit1s to set
	 */
	public void setTestHammaddeUreticiOit1s(
			List<TestHammaddeUreticiOit1> testHammaddeUreticiOit1s) {
		this.testHammaddeUreticiOit1s = testHammaddeUreticiOit1s;
	}

	/**
	 * @return the testHammaddeUreticiPdd1s
	 */
	public List<TestHammaddeUreticiPdd1> getTestHammaddeUreticiPdd1s() {
		return testHammaddeUreticiPdd1s;
	}

	/**
	 * @param testHammaddeUreticiPdd1s
	 *            the testHammaddeUreticiPdd1s to set
	 */
	public void setTestHammaddeUreticiPdd1s(
			List<TestHammaddeUreticiPdd1> testHammaddeUreticiPdd1s) {
		this.testHammaddeUreticiPdd1s = testHammaddeUreticiPdd1s;
	}

	/**
	 * @return the coatMaterialType
	 */
	public CoatMaterialType getCoatMaterialType() {
		return coatMaterialType;
	}

	/**
	 * @param coatMaterialType
	 *            the coatMaterialType to set
	 */
	public void setCoatMaterialType(CoatMaterialType coatMaterialType) {
		this.coatMaterialType = coatMaterialType;
	}

	/**
	 * @return the testHammaddeUreticiYst1s
	 */
	public List<TestHammaddeUreticiYst1> getTestHammaddeUreticiYst1s() {
		return testHammaddeUreticiYst1s;
	}

	/**
	 * @param testHammaddeUreticiYst1s
	 *            the testHammaddeUreticiYst1s to set
	 */
	public void setTestHammaddeUreticiYst1s(
			List<TestHammaddeUreticiYst1> testHammaddeUreticiYst1s) {
		this.testHammaddeUreticiYst1s = testHammaddeUreticiYst1s;
	}

	/**
	 * @return the testHammaddeUreticiKst1s
	 */
	public List<TestHammaddeUreticiKst1> getTestHammaddeUreticiKst1s() {
		return testHammaddeUreticiKst1s;
	}

	/**
	 * @param testHammaddeUreticiKst1s
	 *            the testHammaddeUreticiKst1s to set
	 */
	public void setTestHammaddeUreticiKst1s(
			List<TestHammaddeUreticiKst1> testHammaddeUreticiKst1s) {
		this.testHammaddeUreticiKst1s = testHammaddeUreticiKst1s;
	}

	/**
	 * @return the testHammaddeUreticiPkd1s
	 */
	public List<TestHammaddeUreticiPkd1> getTestHammaddeUreticiPkd1s() {
		return testHammaddeUreticiPkd1s;
	}

	/**
	 * @param testHammaddeUreticiPkd1s
	 *            the testHammaddeUreticiPkd1s to set
	 */
	public void setTestHammaddeUreticiPkd1s(
			List<TestHammaddeUreticiPkd1> testHammaddeUreticiPkd1s) {
		this.testHammaddeUreticiPkd1s = testHammaddeUreticiPkd1s;
	}

	/**
	 * @return the testHammaddeUreticiPks1s
	 */
	public List<TestHammaddeUreticiPks1> getTestHammaddeUreticiPks1s() {
		return testHammaddeUreticiPks1s;
	}

	/**
	 * @param testHammaddeUreticiPks1s
	 *            the testHammaddeUreticiPks1s to set
	 */
	public void setTestHammaddeUreticiPks1s(
			List<TestHammaddeUreticiPks1> testHammaddeUreticiPks1s) {
		this.testHammaddeUreticiPks1s = testHammaddeUreticiPks1s;
	}

	/**
	 * @return the testHammaddeUreticiPku1s
	 */
	public List<TestHammaddeUreticiPku1> getTestHammaddeUreticiPku1s() {
		return testHammaddeUreticiPku1s;
	}

	/**
	 * @param testHammaddeUreticiPku1s
	 *            the testHammaddeUreticiPku1s to set
	 */
	public void setTestHammaddeUreticiPku1s(
			List<TestHammaddeUreticiPku1> testHammaddeUreticiPku1s) {
		this.testHammaddeUreticiPku1s = testHammaddeUreticiPku1s;
	}

	/**
	 * @return the testHammaddeGirdiBkm1Phs
	 */
	public List<TestHammaddeGirdiBkm1Ph> getTestHammaddeGirdiBkm1Phs() {
		return testHammaddeGirdiBkm1Phs;
	}

	/**
	 * @param testHammaddeGirdiBkm1Phs
	 *            the testHammaddeGirdiBkm1Phs to set
	 */
	public void setTestHammaddeGirdiBkm1Phs(
			List<TestHammaddeGirdiBkm1Ph> testHammaddeGirdiBkm1Phs) {
		this.testHammaddeGirdiBkm1Phs = testHammaddeGirdiBkm1Phs;
	}

	/**
	 * @return the testHammaddeGirdiBkm1KatiMaddes
	 */
	public List<TestHammaddeGirdiBkm1KatiMadde> getTestHammaddeGirdiBkm1KatiMaddes() {
		return testHammaddeGirdiBkm1KatiMaddes;
	}

	/**
	 * @param testHammaddeGirdiBkm1KatiMaddes
	 *            the testHammaddeGirdiBkm1KatiMaddes to set
	 */
	public void setTestHammaddeGirdiBkm1KatiMaddes(
			List<TestHammaddeGirdiBkm1KatiMadde> testHammaddeGirdiBkm1KatiMaddes) {
		this.testHammaddeGirdiBkm1KatiMaddes = testHammaddeGirdiBkm1KatiMaddes;
	}

	/**
	 * @return the testHammaddeGirdiBkm1KuruMaddes
	 */
	public List<TestHammaddeGirdiBkm1KuruMadde> getTestHammaddeGirdiBkm1KuruMaddes() {
		return testHammaddeGirdiBkm1KuruMaddes;
	}

	/**
	 * @param testHammaddeGirdiBkm1KuruMaddes
	 *            the testHammaddeGirdiBkm1KuruMaddes to set
	 */
	public void setTestHammaddeGirdiBkm1KuruMaddes(
			List<TestHammaddeGirdiBkm1KuruMadde> testHammaddeGirdiBkm1KuruMaddes) {
		this.testHammaddeGirdiBkm1KuruMaddes = testHammaddeGirdiBkm1KuruMaddes;
	}

	/**
	 * @return the testHammaddeGirdiSertliks
	 */
	public List<TestHammaddeGirdiSertlik> getTestHammaddeGirdiSertliks() {
		return testHammaddeGirdiSertliks;
	}

	/**
	 * @param testHammaddeGirdiSertliks
	 *            the testHammaddeGirdiSertliks to set
	 */
	public void setTestHammaddeGirdiSertliks(
			List<TestHammaddeGirdiSertlik> testHammaddeGirdiSertliks) {
		this.testHammaddeGirdiSertliks = testHammaddeGirdiSertliks;
	}

	/**
	 * @return the testHammaddeGirdiSertlikSpecs
	 */
	public List<TestHammaddeGirdiSertlikSpec> getTestHammaddeGirdiSertlikSpecs() {
		return testHammaddeGirdiSertlikSpecs;
	}

	/**
	 * @param testHammaddeGirdiSertlikSpecs
	 *            the testHammaddeGirdiSertlikSpecs to set
	 */
	public void setTestHammaddeGirdiSertlikSpecs(
			List<TestHammaddeGirdiSertlikSpec> testHammaddeGirdiSertlikSpecs) {
		this.testHammaddeGirdiSertlikSpecs = testHammaddeGirdiSertlikSpecs;
	}

	/**
	 * @return the testHammaddeGirdiAmg1s
	 */
	public List<TestHammaddeGirdiAmg1> getTestHammaddeGirdiAmg1s() {
		return testHammaddeGirdiAmg1s;
	}

	/**
	 * @param testHammaddeGirdiAmg1s
	 *            the testHammaddeGirdiAmg1s to set
	 */
	public void setTestHammaddeGirdiAmg1s(
			List<TestHammaddeGirdiAmg1> testHammaddeGirdiAmg1s) {
		this.testHammaddeGirdiAmg1s = testHammaddeGirdiAmg1s;
	}

	/**
	 * @return the testHammaddeGirdiAmg1Specs
	 */
	public List<TestHammaddeGirdiAmg1Spec> getTestHammaddeGirdiAmg1Specs() {
		return testHammaddeGirdiAmg1Specs;
	}

	/**
	 * @param testHammaddeGirdiAmg1Specs
	 *            the testHammaddeGirdiAmg1Specs to set
	 */
	public void setTestHammaddeGirdiAmg1Specs(
			List<TestHammaddeGirdiAmg1Spec> testHammaddeGirdiAmg1Specs) {
		this.testHammaddeGirdiAmg1Specs = testHammaddeGirdiAmg1Specs;
	}

}