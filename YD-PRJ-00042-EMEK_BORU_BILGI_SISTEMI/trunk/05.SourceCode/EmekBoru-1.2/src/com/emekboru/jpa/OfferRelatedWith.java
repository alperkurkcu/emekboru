package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "offer_related_with")
public class OfferRelatedWith implements Serializable {

	private static final long serialVersionUID = -2613469935813767717L;

	@Id
	@Column(name = "tender_related_with_id")
	private Integer offerRelatedWithId;

	@Column(name = "signature_privilege")
	private Boolean signaturePrivilege;

	@Column(name = "responsible")
	private Boolean responsible;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "employee_id", referencedColumnName = "employee_id")
	private Employee employee;

	public OfferRelatedWith() {

	}

	/**
	 * GETTERS AND SETTERS
	 */
	public Integer getOfferRelatedWithId() {
		return offerRelatedWithId;
	}

	public void setOfferRelatedWithId(Integer offerRelatedWithId) {
		this.offerRelatedWithId = offerRelatedWithId;
	}

	public Boolean getSignaturePrivilege() {
		return signaturePrivilege;
	}

	public void setSignaturePrivilege(Boolean signaturePrivilege) {
		this.signaturePrivilege = signaturePrivilege;
	}

	public Boolean getResponsible() {
		return responsible;
	}

	public void setResponsible(Boolean responsible) {
		this.responsible = responsible;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
