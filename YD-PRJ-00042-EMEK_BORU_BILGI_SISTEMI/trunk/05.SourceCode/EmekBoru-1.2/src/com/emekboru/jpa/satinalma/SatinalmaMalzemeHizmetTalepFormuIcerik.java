package com.emekboru.jpa.satinalma;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.stok.StokTanimlarBirim;
import com.emekboru.jpa.stok.StokUrunKartlari;

/**
 * The persistent class for the satinalma_malzeme_hizmet_talep_formu_icerik
 * database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "SatinalmaMalzemeHizmetTalepFormuIcerik.findAll", query = "SELECT r FROM SatinalmaMalzemeHizmetTalepFormuIcerik r where r.satinalmaMalzemeHizmetTalepFormu.id=:prmFormId order by r.id"),
		@NamedQuery(name = "SatinalmaMalzemeHizmetTalepFormuIcerik.findAllTrue", query = "SELECT r FROM SatinalmaMalzemeHizmetTalepFormuIcerik r where r.satinalmaMalzemeHizmetTalepFormu.id=:prmFormId and r.onayliUrun =true order by r.id") })
@Table(name = "satinalma_malzeme_hizmet_talep_formu_icerik")
public class SatinalmaMalzemeHizmetTalepFormuIcerik implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SATINALMA_MALZEME_HIZMET_TALEP_FORMU_ICERIK_ID_GENERATOR", sequenceName = "SATINALMA_MALZEME_HIZMET_TALEP_FORMU_ICERIK_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SATINALMA_MALZEME_HIZMET_TALEP_FORMU_ICERIK_ID_GENERATOR")
	@Column(name = "ID")
	private Integer id;

	@Column(name = "bakim")
	private Boolean bakim;

	// @Column(name = "birim")
	// private Integer birim;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "birim", referencedColumnName = "ID", insertable = false)
	private StokTanimlarBirim stokTanimlarBirim;

	@Column(name = "demirbas")
	private Boolean demirbas;

	@Column(name = "ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser user;

	// @Column(name = "form_id")
	// private Integer formId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "form_id", referencedColumnName = "ID", insertable = false)
	private SatinalmaMalzemeHizmetTalepFormu satinalmaMalzemeHizmetTalepFormu;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Temporal(TemporalType.DATE)
	@Column(name = "ihtiyac_suresi", nullable = false)
	private Date ihtiyacSuresi;

	// @Column(name = "malzeme_tanim")
	// private String malzemeTanim;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "malzeme_id", referencedColumnName = "ID", insertable = false)
	private StokUrunKartlari stokUrunKartlari;

	@Column(name = "miktar")
	private Integer miktar;

	@Column(name = "sarf")
	private Boolean sarf;

	@Column(name = "uretim")
	private Boolean uretim;

	@Column(name = "sira_no")
	private Integer siraNo;

	@Column(name = "siparis_miktari")
	private Integer siparisMiktari;

	@Column(name = "kalan")
	private Integer kalan;

	@Column(name = "dis_alim_aciklama")
	private String disAlimAciklama;

	@Column(name = "onayli_urun")
	private Boolean onayliUrun = true;

	// @Column(name = "tedarikci_id")
	// private Integer tedarikciId;

	@Column(name = "teslim_tarihi")
	private String teslimTarihi;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "tedarikci_id", referencedColumnName = "ID", insertable = false)
	private SatinalmaMusteri satinalmaTedarikci;

	public SatinalmaMalzemeHizmetTalepFormuIcerik() {

		stokTanimlarBirim = new StokTanimlarBirim();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getBakim() {
		return this.bakim;
	}

	public void setBakim(Boolean bakim) {
		this.bakim = bakim;
	}

	public Boolean getDemirbas() {
		return this.demirbas;
	}

	public void setDemirbas(Boolean demirbas) {
		this.demirbas = demirbas;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Boolean getSarf() {
		return this.sarf;
	}

	public void setSarf(Boolean sarf) {
		this.sarf = sarf;
	}

	public Boolean getUretim() {
		return this.uretim;
	}

	public void setUretim(Boolean uretim) {
		this.uretim = uretim;
	}

	public SystemUser getUser() {
		return user;
	}

	public void setUser(SystemUser user) {
		this.user = user;
	}

	public SatinalmaMalzemeHizmetTalepFormu getSatinalmaMalzemeHizmetTalepFormu() {
		return satinalmaMalzemeHizmetTalepFormu;
	}

	public void setSatinalmaMalzemeHizmetTalepFormu(
			SatinalmaMalzemeHizmetTalepFormu satinalmaMalzemeHizmetTalepFormu) {
		this.satinalmaMalzemeHizmetTalepFormu = satinalmaMalzemeHizmetTalepFormu;
	}

	public Date getIhtiyacSuresi() {
		return ihtiyacSuresi;
	}

	public void setIhtiyacSuresi(Date ihtiyacSuresi) {
		this.ihtiyacSuresi = ihtiyacSuresi;
	}

	public Integer getSiraNo() {
		return siraNo;
	}

	public void setSiraNo(Integer siraNo) {
		this.siraNo = siraNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getMiktar() {
		return miktar;
	}

	public void setMiktar(Integer miktar) {
		this.miktar = miktar;
	}

	public Integer getSiparisMiktari() {
		return siparisMiktari;
	}

	public void setSiparisMiktari(Integer siparisMiktari) {
		this.siparisMiktari = siparisMiktari;
	}

	public StokTanimlarBirim getStokTanimlarBirim() {
		return stokTanimlarBirim;
	}

	public void setStokTanimlarBirim(StokTanimlarBirim stokTanimlarBirim) {
		this.stokTanimlarBirim = stokTanimlarBirim;
	}

	public StokUrunKartlari getStokUrunKartlari() {
		return stokUrunKartlari;
	}

	public void setStokUrunKartlari(StokUrunKartlari stokUrunKartlari) {
		this.stokUrunKartlari = stokUrunKartlari;
	}

	public Integer getKalan() {
		return kalan;
	}

	public void setKalan(Integer kalan) {
		this.kalan = kalan;
	}

	/**
	 * @return the disAlimAciklama
	 */
	public String getDisAlimAciklama() {
		return disAlimAciklama;
	}

	/**
	 * @param disAlimAciklama
	 *            the disAlimAciklama to set
	 */
	public void setDisAlimAciklama(String disAlimAciklama) {
		this.disAlimAciklama = disAlimAciklama;
	}

	/**
	 * @return the onayliUrun
	 */
	public Boolean getOnayliUrun() {
		return onayliUrun;
	}

	/**
	 * @param onayliUrun
	 *            the onayliUrun to set
	 */
	public void setOnayliUrun(Boolean onayliUrun) {
		this.onayliUrun = onayliUrun;
	}

	/**
	 * @return the teslimTarihi
	 */
	public String getTeslimTarihi() {
		return teslimTarihi;
	}

	/**
	 * @param teslimTarihi
	 *            the teslimTarihi to set
	 */
	public void setTeslimTarihi(String teslimTarihi) {
		this.teslimTarihi = teslimTarihi;
	}

	/**
	 * @return the satinalmaTedarikci
	 */
	public SatinalmaMusteri getSatinalmaTedarikci() {
		return satinalmaTedarikci;
	}

	/**
	 * @param satinalmaTedarikci
	 *            the satinalmaTedarikci to set
	 */
	public void setSatinalmaTedarikci(SatinalmaMusteri satinalmaTedarikci) {
		this.satinalmaTedarikci = satinalmaTedarikci;
	}

}