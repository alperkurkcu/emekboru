package com.emekboru.jpa.kalibrasyon;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the kalibrasyon_floroskopik database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "KalibrasyonFloroskopik.findAll", query = "SELECT r FROM KalibrasyonFloroskopik r order by r.kalibrasyonZamani DESC") })
@Table(name = "kalibrasyon_floroskopik")
public class KalibrasyonFloroskopik implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "KALIBRASYON_FLOROSKOPIK_ID_GENERATOR", sequenceName = "KALIBRASYON_FLOROSKOPIK_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "KALIBRASYON_FLOROSKOPIK_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "aciklama")
	private String aciklama;

	@Column(name = "buyume_orani")
	private String buyumeOrani;

	@Column(name = "cihaz")
	private String cihaz;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "goruntu_kalite_belirteci")
	private String goruntuKaliteBelirteci;

	@Column(name = "kalite_numarasi")
	private String kaliteNumarasi;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "kalibrasyon_zamani")
	private Date kalibrasyonZamani;

	@Column(name = "kalibrasyonu_yapan")
	private String kalibrasyonuYapan;

	@Column(name = "muayene_hizi")
	private String muayeneHizi;

	@Column(name = "odak_boyutu")
	private String odakBoyutu;

	private String odd;

	@Column(name = "snr_norm")
	private String snrNorm;

	private String sod;

	@Column(name = "tel_numarasi")
	private String telNumarasi;

	@Column(name = "tup_akimi")
	private String tupAkimi;

	@Column(name = "tup_voltaji")
	private String tupVoltaji;

	@Column(name = "cift_tel_numarasi")
	private String ciftTelNumarasi;

	public KalibrasyonFloroskopik() {
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public String getBuyumeOrani() {
		return this.buyumeOrani;
	}

	public void setBuyumeOrani(String buyumeOrani) {
		this.buyumeOrani = buyumeOrani;
	}

	public String getCihaz() {
		return this.cihaz;
	}

	public void setCihaz(String cihaz) {
		this.cihaz = cihaz;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public String getGoruntuKaliteBelirteci() {
		return this.goruntuKaliteBelirteci;
	}

	public void setGoruntuKaliteBelirteci(String goruntuKaliteBelirteci) {
		this.goruntuKaliteBelirteci = goruntuKaliteBelirteci;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getKalibrasyonuYapan() {
		return this.kalibrasyonuYapan;
	}

	public void setKalibrasyonuYapan(String kalibrasyonuYapan) {
		this.kalibrasyonuYapan = kalibrasyonuYapan;
	}

	public String getMuayeneHizi() {
		return this.muayeneHizi;
	}

	public void setMuayeneHizi(String muayeneHizi) {
		this.muayeneHizi = muayeneHizi;
	}

	public String getOdakBoyutu() {
		return this.odakBoyutu;
	}

	public void setOdakBoyutu(String odakBoyutu) {
		this.odakBoyutu = odakBoyutu;
	}

	public String getOdd() {
		return this.odd;
	}

	public void setOdd(String odd) {
		this.odd = odd;
	}

	public String getSnrNorm() {
		return this.snrNorm;
	}

	public void setSnrNorm(String snrNorm) {
		this.snrNorm = snrNorm;
	}

	public String getSod() {
		return this.sod;
	}

	public void setSod(String sod) {
		this.sod = sod;
	}

	public String getTelNumarasi() {
		return this.telNumarasi;
	}

	public void setTelNumarasi(String telNumarasi) {
		this.telNumarasi = telNumarasi;
	}

	public String getTupAkimi() {
		return this.tupAkimi;
	}

	public void setTupAkimi(String tupAkimi) {
		this.tupAkimi = tupAkimi;
	}

	public String getTupVoltaji() {
		return this.tupVoltaji;
	}

	public void setTupVoltaji(String tupVoltaji) {
		this.tupVoltaji = tupVoltaji;
	}

	/**
	 * @return the eklemeZamani
	 */
	public Date getEklemeZamani() {
		return eklemeZamani;
	}

	/**
	 * @param eklemeZamani
	 *            the eklemeZamani to set
	 */
	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncellemeZamani
	 */
	public Date getGuncellemeZamani() {
		return guncellemeZamani;
	}

	/**
	 * @param guncellemeZamani
	 *            the guncellemeZamani to set
	 */
	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the kalibrasyonZamani
	 */
	public Date getKalibrasyonZamani() {
		return kalibrasyonZamani;
	}

	/**
	 * @param kalibrasyonZamani
	 *            the kalibrasyonZamani to set
	 */
	public void setKalibrasyonZamani(Date kalibrasyonZamani) {
		this.kalibrasyonZamani = kalibrasyonZamani;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the kaliteNumarasi
	 */
	public String getKaliteNumarasi() {
		return kaliteNumarasi;
	}

	/**
	 * @param kaliteNumarasi
	 *            the kaliteNumarasi to set
	 */
	public void setKaliteNumarasi(String kaliteNumarasi) {
		this.kaliteNumarasi = kaliteNumarasi;
	}

	/**
	 * @return the ciftTelNumarasi
	 */
	public String getCiftTelNumarasi() {
		return ciftTelNumarasi;
	}

	/**
	 * @param ciftTelNumarasi
	 *            the ciftTelNumarasi to set
	 */
	public void setCiftTelNumarasi(String ciftTelNumarasi) {
		this.ciftTelNumarasi = ciftTelNumarasi;
	}

}