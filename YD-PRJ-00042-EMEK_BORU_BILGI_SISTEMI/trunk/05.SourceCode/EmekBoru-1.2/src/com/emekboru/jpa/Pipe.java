package com.emekboru.jpa;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiBeton;
import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiDisKumlama;
import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiEpoksi;
import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiFircalama;
import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiIcKumlama;
import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiMuflama;
import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiPolietilen;
import com.emekboru.jpa.rulo.RuloPipeLink;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpa.shipment.Shipment;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizFloroskopikSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizGozOlcuSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizHidrostatik;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizKesim;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizManuelUtSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizManyetikParcacikSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizOfflineOtomatikUtSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizOtomatikUtSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizTamir;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizTorna;

/**
 * The persistent class for the pipe database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "Pipe.getMaxPipeId", query = "select max(o.pipeId) from Pipe o"),
		@NamedQuery(name = "Pipe.Total", query = "SELECT COUNT(o) FROM Pipe o where o.salesItem.itemId=:prmItemId"),
		@NamedQuery(name = "Pipe.getMaxPipeIndex", query = "select max(o.pipeIndex) from Pipe o where o.salesItem.itemId=:prmItemId"),
		@NamedQuery(name = "Pipe.findByItemIdASC", query = "select o from Pipe o where o.salesItem.itemId=:prmItemId order by o.pipeId ASC"),
		@NamedQuery(name = "Pipe.findByItemId", query = "select o from Pipe o where o.salesItem.itemId=:prmItemId order by o.pipeId DESC"),
		@NamedQuery(name = "Pipe.findByItemIdAndDestructiveTest", query = "select o from Pipe o where o.salesItem.itemId=:prmItemId and o.tahribatliTestSonuc='t' order by o.pipeId DESC"),
		@NamedQuery(name = "Pipe.findByBarkodNo", query = "select o from Pipe o where o.pipeBarkodNo=:prmBarkodNo"),
		@NamedQuery(name = "Pipe.findByPipeId", query = "select o from Pipe o where o.pipeId=:prmPipeId"),
		@NamedQuery(name = "Pipe.findYesterdaysProduction", query = "select o from Pipe o where o.eklemeZamani=:prmDate order by o.pipeId DESC"),
		@NamedQuery(name = "Pipe.bandEkli", query = "select p from Pipe p where p.pipeIndex=:prmIndex and p.salesItem.itemId=:prmItemId"),
		@NamedQuery(name = "Pipe.findReleaseCountByItemId", query = "select DISTINCT o.releaseNoteTarihi, count(DISTINCT o) from Pipe o where o.salesItem.itemId=:prmItemId and o.releaseNoteTarihi is not null GROUP BY o.releaseNoteTarihi order by o.releaseNoteTarihi ASC"),
		@NamedQuery(name = "Pipe.findUcBirCountByItemId", query = "select DISTINCT o.ucBirTarihi, count(DISTINCT o) from Pipe o where o.salesItem.itemId=:prmItemId and o.releaseNoteTarihi is not null and o.ucBirTarihi is not null GROUP BY o.releaseNoteTarihi,o.ucBirTarihi order by o.releaseNoteTarihi ASC,o.ucBirTarihi ASC"),
		@NamedQuery(name = "Pipe.findByItemIdMachineId", query = "select o from Pipe o, MachinePipeLink m where m.pipe.salesItem.itemId=:prmItemId and m.machine.machineId=:prmMachineId and m.pipe.pipeId=o.pipeId order by o.pipeId DESC"),
		@NamedQuery(name = "Pipe.findByItemIdIndexId", query = "select o from Pipe o where o.salesItem.itemId=:prmItemId and o.pipeIndex=:prmIndexId order by o.pipeId DESC") })
@Table(name = "pipe")
public class Pipe implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "pipe_id", nullable = false)
	@SequenceGenerator(name = "PIPE_GENERATOR", sequenceName = "PIPE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PIPE_GENERATOR")
	private Integer pipeId;

	@Column(name = "aucu_alin")
	private double aucuAlin;

	@Column(name = "aucu_boru")
	private double aucuBoru;

	@Column(name = "aucu_kaynak")
	private double aucuKaynak;

	@Column(name = "agirligi")
	private double agirligi;

	@Column(name = "bucu_alin")
	private double bucuAlin;

	@Column(name = "bucu_boru")
	private double bucuBoru;

	@Column(name = "bucu_kaynak")
	private double bucuKaynak;

	private double boy;

	@Column(name = "cap_a_ucu")
	private double capAUcu;

	@Column(name = "cap_b_ucu")
	private double capBUcu;

	@Column(name = "cap_max")
	private double capMax;

	@Column(name = "cap_min")
	private double capMin;

	@Column(name = "dogrusalik")
	private double dogrusalik;

	@Column(name = "et_kalinligi")
	private double etKalinligi;

	@Column(name = "kaynak_boy")
	private double kaynakBoy;

	@Column(name = "kaynak_dikici")
	private double kaynakDikici;

	@Column(name = "location")
	private String location;

	@Column(name = "lot_no")
	private int lotNo;

	@Column(name = "magnetic_kalici")
	private double magneticKalici;

	private Boolean marked;

	private String note;

	@Column(name = "ovallik_a_max")
	private double ovallikAMax;

	@Column(name = "ovallik_a_min")
	private double ovallikAMin;

	@Column(name = "ovallik_b_max")
	private double ovallikBMax;

	@Column(name = "ovallik_b_min")
	private double ovallikBMin;

	@Column(name = "ovallik_body_max")
	private double ovallikBodyMax;

	@Column(name = "ovallik_body_min")
	private double ovallikBodyMin;

	@Column(name = "pipe_index")
	private int pipeIndex;

	@Column(name = "radial_kacik")
	private double radialKacik;

	private int sonuc;

	private int status;

	@Column(name = "dis_cap")
	private double disCap;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "production_date")
	private Date productionDate;

	// @JoinColumn(name = "order_id", referencedColumnName = "order_id",
	// insertable = false)
	// @ManyToOne(fetch = FetchType.LAZY)
	// private Order order;

	// entegre
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private SalesItem salesItem;
	// entegra

	@OneToMany(mappedBy = "pipe", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<EdwPipeLink> edwPipeLinks;

	@Column(name = "band_eki")
	private Boolean bandEki;

	@Column(name = "kaplama_test_sonuc")
	private Boolean kaplamaTestSonuc;

	@Column(name = "tahribatli_test_sonuc")
	private Boolean tahribatliTestSonuc;

	@Column(name = "tahribatsiz_test_sonuc")
	private Boolean tahribatsizTestSonuc;

	@OneToMany(mappedBy = "pipe", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<PipeMachineRuloLink> pmrLinks;

	@OneToMany(mappedBy = "pipe", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<MachinePipeLink> mpLink;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "pipe_barkod_no")
	private String pipeBarkodNo;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private TestTahribatsizTorna testTahribatsizTorna;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private TestTahribatsizHidrostatik testTahribatsizHidrostatik;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private TestTahribatsizKesim testTahribatsizKesimSonuc;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private TestTahribatsizGozOlcuSonuc testTahribatsizGozOlcuSonuc;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@OneToOne(fetch = FetchType.EAGER)
	private RuloPipeLink ruloPipeLink;

	// test sonucları icin
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@OneToMany(fetch = FetchType.LAZY)
	private List<TestAgirlikDusurmeSonuc> testAgirlikDusurmeSonuclar;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@OneToMany(fetch = FetchType.LAZY)
	private List<TestBukmeSonuc> testBukmeSonuclar;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@OneToMany(fetch = FetchType.LAZY)
	private List<TestCekmeSonuc> testCekmeSonuclar;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@OneToMany(fetch = FetchType.LAZY)
	private List<TestCentikDarbeSonuc> testCentikDarbeSonuclar;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@OneToMany(fetch = FetchType.LAZY)
	private List<TestKimyasalAnalizSonuc> testKimyasalAnalizSonuclar;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@OneToMany(fetch = FetchType.LAZY)
	private List<TestKaynakSonuc> testKaynakSonuclar;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@OneToMany(fetch = FetchType.LAZY)
	private List<TestTahribatsizTamir> testTahribatsizTamirler;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@OneToMany(fetch = FetchType.LAZY)
	private List<TestTahribatsizOtomatikUtSonuc> testTahribatsizOtomatikUtSonuclar;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@OneToMany(fetch = FetchType.LAZY)
	private List<TestTahribatsizManuelUtSonuc> testTahribatsizManuelUtSonuclar;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@OneToMany(fetch = FetchType.LAZY)
	private List<TestTahribatsizFloroskopikSonuc> testTahribatsizFloroskopikSonuclar;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@OneToMany(fetch = FetchType.LAZY)
	private List<TestTahribatsizOfflineOtomatikUtSonuc> testTahribatsizOfflineOtomatikUtSonuclar;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@OneToMany(fetch = FetchType.LAZY)
	private List<TestTahribatsizKesim> testTahribatsizKesimSonuclar;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@OneToMany(fetch = FetchType.LAZY)
	private List<TestTahribatsizHidrostatik> testTahribatsizHidrostatikler;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@OneToMany(fetch = FetchType.LAZY)
	private List<TestTahribatsizGozOlcuSonuc> testTahribatsizGozOlcuSonuclar;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@OneToMany(fetch = FetchType.LAZY)
	private List<TestTahribatsizManyetikParcacikSonuc> testTahribatsizManyetikParcacikSonuclar;

	// kaplama için
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private KaplamaMakinesiIcKumlama kaplamaMakinesiIcKumlama;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private KaplamaMakinesiDisKumlama kaplamaMakinesiDisKumlama;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private KaplamaMakinesiEpoksi kaplamaMakinesiEpoksi;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private KaplamaMakinesiPolietilen kaplamaMakinesiPolietilen;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private KaplamaMakinesiBeton kaplamaMakinesiBeton;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private KaplamaMakinesiFircalama kaplamaMakinesiFircalama;

	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private KaplamaMakinesiMuflama kaplamaMakinesiMuflama;

	// @JoinColumn(name = "shipment_id", referencedColumnName = "id", insertable
	// = false)
	// @ManyToOne(fetch = FetchType.LAZY)
	// private Shipment shipment;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "shipment_id", referencedColumnName = "id", insertable = false)
	private Shipment shipment;

	// @ManyToOne
	// @JoinColumn(name = "shipment_id", insertable = false)
	// private Shipment shipment;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "release_note_tarihi", nullable = false)
	private Date releaseNoteTarihi;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "uc_bir_tarihi", nullable = false)
	private Date ucBirTarihi;

	@Column(name = "pipe_index_old")
	private int pipeIndexOld;

	@Column(name = "pipe_index_guncelleyen_kullanici")
	private Integer pipeIndexGuncelleyenKullanici;

	public Pipe() {
	}

	@Override
	public boolean equals(Object obj) {

		if (!(obj instanceof Pipe)) {
			return false;
		}
		if (((Pipe) obj).pipeId == this.pipeId) {
			return true;
		}
		return false;
	}

	public Integer getPipeId() {
		return pipeId;
	}

	public void setPipeId(Integer pipeId) {
		this.pipeId = pipeId;
	}

	public double getAucuAlin() {
		return this.aucuAlin;
	}

	public void setAucuAlin(double aucuAlin) {
		this.aucuAlin = aucuAlin;
	}

	public double getAucuBoru() {
		return this.aucuBoru;
	}

	public void setAucuBoru(double aUcuBoru) {
		this.aucuBoru = aUcuBoru;
	}

	public double getAucuKaynak() {
		return this.aucuKaynak;
	}

	public void setAucuKaynak(double aUcuKaynak) {
		this.aucuKaynak = aUcuKaynak;
	}

	public double getAgirligi() {
		return this.agirligi;
	}

	public void setAgirligi(double agirligi) {
		this.agirligi = agirligi;
	}

	public double getBucuAlin() {
		return this.bucuAlin;
	}

	public void setBucuAlin(double bUcuAlin) {
		this.bucuAlin = bUcuAlin;
	}

	public double getBucuBoru() {
		return this.bucuBoru;
	}

	public void setBucuBoru(double bUcuBoru) {
		this.bucuBoru = bUcuBoru;
	}

	public double getBucuKaynak() {
		return this.bucuKaynak;
	}

	public void setBucuKaynak(double bUcuKaynak) {
		this.bucuKaynak = bUcuKaynak;
	}

	public double getBoy() {
		return this.boy;
	}

	public void setBoy(double boy) {
		this.boy = boy;
	}

	public double getCapAUcu() {
		return this.capAUcu;
	}

	public void setCapAUcu(double capAUcu) {
		this.capAUcu = capAUcu;
	}

	public double getCapBUcu() {
		return this.capBUcu;
	}

	public void setCapBUcu(double capBUcu) {
		this.capBUcu = capBUcu;
	}

	public double getCapMax() {
		return this.capMax;
	}

	public void setCapMax(double capMax) {
		this.capMax = capMax;
	}

	public double getCapMin() {
		return this.capMin;
	}

	public void setCapMin(double capMin) {
		this.capMin = capMin;
	}

	public double getDogrusalik() {
		return this.dogrusalik;
	}

	public void setDogrusalik(double dogrusalik) {
		this.dogrusalik = dogrusalik;
	}

	public double getEtKalinligi() {
		return this.etKalinligi;
	}

	public void setEtKalinligi(double etKalinligi) {
		this.etKalinligi = etKalinligi;
	}

	public double getKaynakBoy() {
		return this.kaynakBoy;
	}

	public void setKaynakBoy(double kaynakBoy) {
		this.kaynakBoy = kaynakBoy;
	}

	public double getKaynakDikici() {
		return this.kaynakDikici;
	}

	public void setKaynakDikici(double kaynakDikici) {
		this.kaynakDikici = kaynakDikici;
	}

	public int getLotNo() {
		return this.lotNo;
	}

	public void setLotNo(int lotNo) {
		this.lotNo = lotNo;
	}

	public double getMagneticKalici() {
		return this.magneticKalici;
	}

	public void setMagneticKalici(double magneticKalici) {
		this.magneticKalici = magneticKalici;
	}

	public Boolean getMarked() {
		return this.marked;
	}

	public void setMarked(Boolean marked) {
		this.marked = marked;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public double getOvallikAMax() {
		return this.ovallikAMax;
	}

	public void setOvallikAMax(double ovallikAMax) {
		this.ovallikAMax = ovallikAMax;
	}

	public double getOvallikAMin() {
		return this.ovallikAMin;
	}

	public void setOvallikAMin(double ovallikAMin) {
		this.ovallikAMin = ovallikAMin;
	}

	public double getOvallikBMax() {
		return this.ovallikBMax;
	}

	public void setOvallikBMax(double ovallikBMax) {
		this.ovallikBMax = ovallikBMax;
	}

	public double getOvallikBMin() {
		return this.ovallikBMin;
	}

	public void setOvallikBMin(double ovallikBMin) {
		this.ovallikBMin = ovallikBMin;
	}

	public double getOvallikBodyMax() {
		return this.ovallikBodyMax;
	}

	public void setOvallikBodyMax(double ovallikBodyMax) {
		this.ovallikBodyMax = ovallikBodyMax;
	}

	public double getOvallikBodyMin() {
		return this.ovallikBodyMin;
	}

	public void setOvallikBodyMin(double ovallikBodyMin) {
		this.ovallikBodyMin = ovallikBodyMin;
	}

	public int getPipeIndex() {
		return this.pipeIndex;
	}

	public void setPipeIndex(int pipeIndex) {
		this.pipeIndex = pipeIndex;
	}

	public double getRadialKacik() {
		return this.radialKacik;
	}

	public void setRadialKacik(double radialKacik) {
		this.radialKacik = radialKacik;
	}

	public int getSonuc() {
		return this.sonuc;
	}

	public void setSonuc(int sonuc) {
		this.sonuc = sonuc;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public double getDisCap() {
		return disCap;
	}

	public void setDisCap(double disCap) {
		this.disCap = disCap;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public List<EdwPipeLink> getEdwPipeLinks() {
		if (edwPipeLinks == null) {
			edwPipeLinks = new ArrayList<EdwPipeLink>(0);
		}
		return edwPipeLinks;
	}

	public void setEdwPipeLinks(List<EdwPipeLink> edwPipeLinks) {
		this.edwPipeLinks = edwPipeLinks;
	}

	public List<PipeMachineRuloLink> getPmrLinks() {
		if (pmrLinks == null) {
			pmrLinks = new ArrayList<PipeMachineRuloLink>(0);
		}
		return pmrLinks;
	}

	public void setPmrLinks(List<PipeMachineRuloLink> pmrLinks) {
		this.pmrLinks = pmrLinks;
	}

	public Date getProductionDate() {
		return productionDate;
	}

	public void setProductionDate(Date productionDate) {
		this.productionDate = productionDate;
	}

	public List<MachinePipeLink> getMpLink() {
		return mpLink;
	}

	public void setMpLink(List<MachinePipeLink> mpLink) {
		this.mpLink = mpLink;
	}

	public Boolean getBandEki() {
		return bandEki;
	}

	public void setBandEki(Boolean bandEki) {
		this.bandEki = bandEki;
	}

	public Boolean getKaplamaTestSonuc() {
		return kaplamaTestSonuc;
	}

	public void setKaplamaTestSonuc(Boolean kaplamaTestSonuc) {
		this.kaplamaTestSonuc = kaplamaTestSonuc;
	}

	public Boolean getTahribatliTestSonuc() {
		return tahribatliTestSonuc;
	}

	public void setTahribatliTestSonuc(Boolean tahribatliTestSonuc) {
		this.tahribatliTestSonuc = tahribatliTestSonuc;
	}

	public Boolean getTahribatsizTestSonuc() {
		return tahribatsizTestSonuc;
	}

	public void setTahribatsizTestSonuc(Boolean tahribatsizTestSonuc) {
		this.tahribatsizTestSonuc = tahribatsizTestSonuc;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public Timestamp getEklemeZamani() {
		return eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getPipeBarkodNo() {
		return pipeBarkodNo;
	}

	public void setPipeBarkodNo(String pipeBarkodNo) {
		this.pipeBarkodNo = pipeBarkodNo;
	}

	public TestTahribatsizTorna getTestTahribatsizTorna() {
		return testTahribatsizTorna;
	}

	public void setTestTahribatsizTorna(
			TestTahribatsizTorna testTahribatsizTorna) {
		this.testTahribatsizTorna = testTahribatsizTorna;
	}

	public TestTahribatsizHidrostatik getTestTahribatsizHidrostatik() {
		return testTahribatsizHidrostatik;
	}

	public void setTestTahribatsizHidrostatik(
			TestTahribatsizHidrostatik testTahribatsizHidrostatik) {
		this.testTahribatsizHidrostatik = testTahribatsizHidrostatik;
	}

	public TestTahribatsizGozOlcuSonuc getTestTahribatsizGozOlcuSonuc() {
		return testTahribatsizGozOlcuSonuc;
	}

	public void setTestTahribatsizGozOlcuSonuc(
			TestTahribatsizGozOlcuSonuc testTahribatsizGozOlcuSonuc) {
		this.testTahribatsizGozOlcuSonuc = testTahribatsizGozOlcuSonuc;
	}

	public RuloPipeLink getRuloPipeLink() {
		return ruloPipeLink;
	}

	public void setRuloPipeLink(RuloPipeLink ruloPipeLink) {
		this.ruloPipeLink = ruloPipeLink;
	}

	public List<TestAgirlikDusurmeSonuc> getTestAgirlikDusurmeSonuclar() {
		return testAgirlikDusurmeSonuclar;
	}

	public void setTestAgirlikDusurmeSonuclar(
			List<TestAgirlikDusurmeSonuc> testAgirlikDusurmeSonuclar) {
		this.testAgirlikDusurmeSonuclar = testAgirlikDusurmeSonuclar;
	}

	public List<TestBukmeSonuc> getTestBukmeSonuclar() {
		return testBukmeSonuclar;
	}

	public void setTestBukmeSonuclar(List<TestBukmeSonuc> testBukmeSonuclar) {
		this.testBukmeSonuclar = testBukmeSonuclar;
	}

	public List<TestCekmeSonuc> getTestCekmeSonuclar() {
		return testCekmeSonuclar;
	}

	public void setTestCekmeSonuclar(List<TestCekmeSonuc> testCekmeSonuclar) {
		this.testCekmeSonuclar = testCekmeSonuclar;
	}

	public List<TestCentikDarbeSonuc> getTestCentikDarbeSonuclar() {
		return testCentikDarbeSonuclar;
	}

	public void setTestCentikDarbeSonuclar(
			List<TestCentikDarbeSonuc> testCentikDarbeSonuclar) {
		this.testCentikDarbeSonuclar = testCentikDarbeSonuclar;
	}

	public List<TestKimyasalAnalizSonuc> getTestKimyasalAnalizSonuclar() {
		return testKimyasalAnalizSonuclar;
	}

	public void setTestKimyasalAnalizSonuclar(
			List<TestKimyasalAnalizSonuc> testKimyasalAnalizSonuclar) {
		this.testKimyasalAnalizSonuclar = testKimyasalAnalizSonuclar;
	}

	public List<TestKaynakSonuc> getTestKaynakSonuclar() {
		return testKaynakSonuclar;
	}

	public void setTestKaynakSonuclar(List<TestKaynakSonuc> testKaynakSonuclar) {
		this.testKaynakSonuclar = testKaynakSonuclar;
	}

	public List<TestTahribatsizTamir> getTestTahribatsizTamirler() {
		return testTahribatsizTamirler;
	}

	public void setTestTahribatsizTamirler(
			List<TestTahribatsizTamir> testTahribatsizTamirler) {
		this.testTahribatsizTamirler = testTahribatsizTamirler;
	}

	public List<TestTahribatsizManuelUtSonuc> getTestTahribatsizManuelUtSonuclar() {
		return testTahribatsizManuelUtSonuclar;
	}

	public void setTestTahribatsizManuelUtSonuclar(
			List<TestTahribatsizManuelUtSonuc> testTahribatsizManuelUtSonuclar) {
		this.testTahribatsizManuelUtSonuclar = testTahribatsizManuelUtSonuclar;
	}

	public List<TestTahribatsizFloroskopikSonuc> getTestTahribatsizFloroskopikSonuclar() {
		return testTahribatsizFloroskopikSonuclar;
	}

	public void setTestTahribatsizFloroskopikSonuclar(
			List<TestTahribatsizFloroskopikSonuc> testTahribatsizFloroskopikSonuclar) {
		this.testTahribatsizFloroskopikSonuclar = testTahribatsizFloroskopikSonuclar;
	}

	public Shipment getShipment() {
		return shipment;
	}

	public void setShipment(Shipment shipment) {
		this.shipment = shipment;
	}

	/**
	 * @return the testTahribatsizKesimSonuclar
	 */
	public List<TestTahribatsizKesim> getTestTahribatsizKesimSonuclar() {
		return testTahribatsizKesimSonuclar;
	}

	/**
	 * @param testTahribatsizKesimSonuclar
	 *            the testTahribatsizKesimSonuclar to set
	 */
	public void setTestTahribatsizKesimSonuclar(
			List<TestTahribatsizKesim> testTahribatsizKesimSonuclar) {
		this.testTahribatsizKesimSonuclar = testTahribatsizKesimSonuclar;
	}

	/**
	 * @return the testTahribatsizKesimSonuc
	 */
	public TestTahribatsizKesim getTestTahribatsizKesimSonuc() {
		return testTahribatsizKesimSonuc;
	}

	/**
	 * @param testTahribatsizKesimSonuc
	 *            the testTahribatsizKesimSonuc to set
	 */
	public void setTestTahribatsizKesimSonuc(
			TestTahribatsizKesim testTahribatsizKesimSonuc) {
		this.testTahribatsizKesimSonuc = testTahribatsizKesimSonuc;
	}

	/**
	 * @return the releaseNoteTarihi
	 */
	public Date getReleaseNoteTarihi() {
		return releaseNoteTarihi;
	}

	/**
	 * @param releaseNoteTarihi
	 *            the releaseNoteTarihi to set
	 */
	public void setReleaseNoteTarihi(Date releaseNoteTarihi) {
		this.releaseNoteTarihi = releaseNoteTarihi;
	}

	/**
	 * @return the kaplamaMakinesiIcKumlama
	 */
	public KaplamaMakinesiIcKumlama getKaplamaMakinesiIcKumlama() {
		return kaplamaMakinesiIcKumlama;
	}

	/**
	 * @param kaplamaMakinesiIcKumlama
	 *            the kaplamaMakinesiIcKumlama to set
	 */
	public void setKaplamaMakinesiIcKumlama(
			KaplamaMakinesiIcKumlama kaplamaMakinesiIcKumlama) {
		this.kaplamaMakinesiIcKumlama = kaplamaMakinesiIcKumlama;
	}

	/**
	 * @return the kaplamaMakinesiDisKumlama
	 */
	public KaplamaMakinesiDisKumlama getKaplamaMakinesiDisKumlama() {
		return kaplamaMakinesiDisKumlama;
	}

	/**
	 * @param kaplamaMakinesiDisKumlama
	 *            the kaplamaMakinesiDisKumlama to set
	 */
	public void setKaplamaMakinesiDisKumlama(
			KaplamaMakinesiDisKumlama kaplamaMakinesiDisKumlama) {
		this.kaplamaMakinesiDisKumlama = kaplamaMakinesiDisKumlama;
	}

	/**
	 * @return the kaplamaMakinesiEpoksi
	 */
	public KaplamaMakinesiEpoksi getKaplamaMakinesiEpoksi() {
		return kaplamaMakinesiEpoksi;
	}

	/**
	 * @param kaplamaMakinesiEpoksi
	 *            the kaplamaMakinesiEpoksi to set
	 */
	public void setKaplamaMakinesiEpoksi(
			KaplamaMakinesiEpoksi kaplamaMakinesiEpoksi) {
		this.kaplamaMakinesiEpoksi = kaplamaMakinesiEpoksi;
	}

	/**
	 * @return the kaplamaMakinesiPolietilen
	 */
	public KaplamaMakinesiPolietilen getKaplamaMakinesiPolietilen() {
		return kaplamaMakinesiPolietilen;
	}

	/**
	 * @param kaplamaMakinesiPolietilen
	 *            the kaplamaMakinesiPolietilen to set
	 */
	public void setKaplamaMakinesiPolietilen(
			KaplamaMakinesiPolietilen kaplamaMakinesiPolietilen) {
		this.kaplamaMakinesiPolietilen = kaplamaMakinesiPolietilen;
	}

	/**
	 * @return the kaplamaMakinesiBeton
	 */
	public KaplamaMakinesiBeton getKaplamaMakinesiBeton() {
		return kaplamaMakinesiBeton;
	}

	/**
	 * @param kaplamaMakinesiBeton
	 *            the kaplamaMakinesiBeton to set
	 */
	public void setKaplamaMakinesiBeton(
			KaplamaMakinesiBeton kaplamaMakinesiBeton) {
		this.kaplamaMakinesiBeton = kaplamaMakinesiBeton;
	}

	/**
	 * @return the kaplamaMakinesiFircalama
	 */
	public KaplamaMakinesiFircalama getKaplamaMakinesiFircalama() {
		return kaplamaMakinesiFircalama;
	}

	/**
	 * @param kaplamaMakinesiFircalama
	 *            the kaplamaMakinesiFircalama to set
	 */
	public void setKaplamaMakinesiFircalama(
			KaplamaMakinesiFircalama kaplamaMakinesiFircalama) {
		this.kaplamaMakinesiFircalama = kaplamaMakinesiFircalama;
	}

	/**
	 * @return the kaplamaMakinesiMuflama
	 */
	public KaplamaMakinesiMuflama getKaplamaMakinesiMuflama() {
		return kaplamaMakinesiMuflama;
	}

	/**
	 * @param kaplamaMakinesiMuflama
	 *            the kaplamaMakinesiMuflama to set
	 */
	public void setKaplamaMakinesiMuflama(
			KaplamaMakinesiMuflama kaplamaMakinesiMuflama) {
		this.kaplamaMakinesiMuflama = kaplamaMakinesiMuflama;
	}

	/**
	 * @return the testTahribatsizOtomatikUtSonuclar
	 */
	public List<TestTahribatsizOtomatikUtSonuc> getTestTahribatsizOtomatikUtSonuclar() {
		return testTahribatsizOtomatikUtSonuclar;
	}

	/**
	 * @param testTahribatsizOtomatikUtSonuclar
	 *            the testTahribatsizOtomatikUtSonuclar to set
	 */
	public void setTestTahribatsizOtomatikUtSonuclar(
			List<TestTahribatsizOtomatikUtSonuc> testTahribatsizOtomatikUtSonuclar) {
		this.testTahribatsizOtomatikUtSonuclar = testTahribatsizOtomatikUtSonuclar;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the testTahribatsizGozOlcuSonuclar
	 */
	public List<TestTahribatsizGozOlcuSonuc> getTestTahribatsizGozOlcuSonuclar() {
		return testTahribatsizGozOlcuSonuclar;
	}

	/**
	 * @param testTahribatsizGozOlcuSonuclar
	 *            the testTahribatsizGozOlcuSonuclar to set
	 */
	public void setTestTahribatsizGozOlcuSonuclar(
			List<TestTahribatsizGozOlcuSonuc> testTahribatsizGozOlcuSonuclar) {
		this.testTahribatsizGozOlcuSonuclar = testTahribatsizGozOlcuSonuclar;
	}

	/**
	 * @return the testTahribatsizHidrostatikler
	 */
	public List<TestTahribatsizHidrostatik> getTestTahribatsizHidrostatikler() {
		return testTahribatsizHidrostatikler;
	}

	/**
	 * @param testTahribatsizHidrostatikler
	 *            the testTahribatsizHidrostatikler to set
	 */
	public void setTestTahribatsizHidrostatikler(
			List<TestTahribatsizHidrostatik> testTahribatsizHidrostatikler) {
		this.testTahribatsizHidrostatikler = testTahribatsizHidrostatikler;
	}

	/**
	 * @return the testTahribatsizOfflineOtomatikUtSonuclar
	 */
	public List<TestTahribatsizOfflineOtomatikUtSonuc> getTestTahribatsizOfflineOtomatikUtSonuclar() {
		return testTahribatsizOfflineOtomatikUtSonuclar;
	}

	/**
	 * @param testTahribatsizOfflineOtomatikUtSonuclar
	 *            the testTahribatsizOfflineOtomatikUtSonuclar to set
	 */
	public void setTestTahribatsizOfflineOtomatikUtSonuclar(
			List<TestTahribatsizOfflineOtomatikUtSonuc> testTahribatsizOfflineOtomatikUtSonuclar) {
		this.testTahribatsizOfflineOtomatikUtSonuclar = testTahribatsizOfflineOtomatikUtSonuclar;
	}

	/**
	 * @return the testTahribatsizManyetikParcacikSonuclar
	 */
	public List<TestTahribatsizManyetikParcacikSonuc> getTestTahribatsizManyetikParcacikSonuclar() {
		return testTahribatsizManyetikParcacikSonuclar;
	}

	/**
	 * @param testTahribatsizManyetikParcacikSonuclar
	 *            the testTahribatsizManyetikParcacikSonuclar to set
	 */
	public void setTestTahribatsizManyetikParcacikSonuclar(
			List<TestTahribatsizManyetikParcacikSonuc> testTahribatsizManyetikParcacikSonuclar) {
		this.testTahribatsizManyetikParcacikSonuclar = testTahribatsizManyetikParcacikSonuclar;
	}

	/**
	 * @return the ucBirTarihi
	 */
	public Date getUcBirTarihi() {
		return ucBirTarihi;
	}

	/**
	 * @param ucBirTarihi
	 *            the ucBirTarihi to set
	 */
	public void setUcBirTarihi(Date ucBirTarihi) {
		this.ucBirTarihi = ucBirTarihi;
	}

	/**
	 * @return the pipeIndexOld
	 */
	public int getPipeIndexOld() {
		return pipeIndexOld;
	}

	/**
	 * @param pipeIndexOld
	 *            the pipeIndexOld to set
	 */
	public void setPipeIndexOld(int pipeIndexOld) {
		this.pipeIndexOld = pipeIndexOld;
	}

	/**
	 * @return the pipeIndexGuncelleyenKullanici
	 */
	public Integer getPipeIndexGuncelleyenKullanici() {
		return pipeIndexGuncelleyenKullanici;
	}

	/**
	 * @param pipeIndexGuncelleyenKullanici
	 *            the pipeIndexGuncelleyenKullanici to set
	 */
	public void setPipeIndexGuncelleyenKullanici(
			Integer pipeIndexGuncelleyenKullanici) {
		this.pipeIndexGuncelleyenKullanici = pipeIndexGuncelleyenKullanici;
	}

}