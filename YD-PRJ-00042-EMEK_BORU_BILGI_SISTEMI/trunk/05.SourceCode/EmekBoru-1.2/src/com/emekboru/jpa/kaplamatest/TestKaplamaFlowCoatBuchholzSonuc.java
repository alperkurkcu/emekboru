package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.isolation.IsolationTestDefinition;

/**
 * The persistent class for the test_kaplama_flow_coat_buchholz_sonuc database
 * table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaFlowCoatBuchholzSonuc.findAll", query = "SELECT r FROM TestKaplamaFlowCoatBuchholzSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_kaplama_flow_coat_buchholz_sonuc")
public class TestKaplamaFlowCoatBuchholzSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KAPLAMA_FLOW_COAT_BUCHHOLZ_SONUC_ID_GENERATOR", sequenceName = "TEST_KAPLAMA_FLOW_COAT_BUCHHOLZ_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KAPLAMA_FLOW_COAT_BUCHHOLZ_SONUC_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "aritmetik_mean")
	private BigDecimal aritmetikMean;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name = "global_id")
	// private Integer globalId;

	// @Column(name = "isolation_test_id")
	// private Integer isolationTestId;

	// @Column(name = "pipe_id")
	// private Integer pipeId;

	private String sonuc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi", nullable = false)
	private Date testTarihi;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@Column(name = "veri_1")
	private BigDecimal veri1;

	@Column(name = "veri_2")
	private BigDecimal veri2;

	@Column(name = "veri_3")
	private BigDecimal veri3;

	@Column(name = "veri_4")
	private BigDecimal veri4;

	@Column(name = "veri_5")
	private BigDecimal veri5;

	public TestKaplamaFlowCoatBuchholzSonuc() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	/**
	 * @return the aritmetikMean
	 */
	public BigDecimal getAritmetikMean() {
		return aritmetikMean;
	}

	/**
	 * @param aritmetikMean
	 *            the aritmetikMean to set
	 */
	public void setAritmetikMean(BigDecimal aritmetikMean) {
		this.aritmetikMean = aritmetikMean;
	}

	/**
	 * @return the testTarihi
	 */
	public Date getTestTarihi() {
		return testTarihi;
	}

	/**
	 * @param testTarihi
	 *            the testTarihi to set
	 */
	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	/**
	 * @return the bagliGlobalId
	 */
	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}

	/**
	 * @param bagliGlobalId
	 *            the bagliGlobalId to set
	 */
	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	/**
	 * @return the bagliTestId
	 */
	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the veri1
	 */
	public BigDecimal getVeri1() {
		return veri1;
	}

	/**
	 * @param veri1
	 *            the veri1 to set
	 */
	public void setVeri1(BigDecimal veri1) {
		this.veri1 = veri1;
	}

	/**
	 * @return the veri2
	 */
	public BigDecimal getVeri2() {
		return veri2;
	}

	/**
	 * @param veri2
	 *            the veri2 to set
	 */
	public void setVeri2(BigDecimal veri2) {
		this.veri2 = veri2;
	}

	/**
	 * @return the veri3
	 */
	public BigDecimal getVeri3() {
		return veri3;
	}

	/**
	 * @param veri3
	 *            the veri3 to set
	 */
	public void setVeri3(BigDecimal veri3) {
		this.veri3 = veri3;
	}

	/**
	 * @return the veri4
	 */
	public BigDecimal getVeri4() {
		return veri4;
	}

	/**
	 * @param veri4
	 *            the veri4 to set
	 */
	public void setVeri4(BigDecimal veri4) {
		this.veri4 = veri4;
	}

	/**
	 * @return the veri5
	 */
	public BigDecimal getVeri5() {
		return veri5;
	}

	/**
	 * @param veri5
	 *            the veri5 to set
	 */
	public void setVeri5(BigDecimal veri5) {
		this.veri5 = veri5;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the sonuc
	 */
	public String getSonuc() {
		return sonuc;
	}

	/**
	 * @param sonuc
	 *            the sonuc to set
	 */
	public void setSonuc(String sonuc) {
		this.sonuc = sonuc;
	}

}