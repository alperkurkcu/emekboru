package com.emekboru.jpa;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
		@NamedQuery(name = "YetkiLoginLog.findAll", query = "select o from YetkiLoginLog o"),
		@NamedQuery(name = "YetkiLoginLog.updateTimeOutOlanlarLogOut", query = "update  YetkiLoginLog o  set cikisZamani=:cikisZamani  where  o.sessionId=:sessionId")

})
@Table(name = "yetki_login_log")
public class YetkiLoginLog implements Serializable {
	@Id
	@SequenceGenerator(name = "yetki_login_log_generator", sequenceName = "yetki_login_log_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "yetki_login_log_generator")
	@Column(name = "id")
	private Integer id;

	@Column(name = "user_name")
	private String userName;
	@Column(name = "session_id")
	private String sessionId;
	@Column(name = "cikis_zamani")
	private Timestamp cikisZamani;
	@Column(name = "giris_zamani", nullable = false)
	private Timestamp girisZamani;

	public YetkiLoginLog() {

	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public Timestamp getCikisZamani() {
		return cikisZamani;
	}

	public void setCikisZamani(Timestamp cikisZamani) {
		this.cikisZamani = cikisZamani;
	}

	public Timestamp getGirisZamani() {
		return girisZamani;
	}

	public void setGirisZamani(Timestamp girisZamani) {
		this.girisZamani = girisZamani;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
