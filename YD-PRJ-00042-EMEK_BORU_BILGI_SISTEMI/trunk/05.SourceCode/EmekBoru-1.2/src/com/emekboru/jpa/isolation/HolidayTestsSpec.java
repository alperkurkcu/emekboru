package com.emekboru.jpa.isolation;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.Order;
import com.emekboru.jpa.sales.order.SalesItem;

@Entity
@Table(name = "holidaytestsspec")
public class HolidayTestsSpec extends AbstractTestsSpec {
	@Column(name = "testDegeri")
	private String testDegeri;

	@Id
	@Column(name = "id")
	@SequenceGenerator(name = "AUTOMATIC_ISOLATION_TEST_GENERATOR", sequenceName = "AUTOMATIC_ISOLATION_TEST_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AUTOMATIC_ISOLATION_TEST_GENERATOR")
	private Integer id;

	@OneToOne
	@JoinColumn(name = "testtype", referencedColumnName = "isolationtype", insertable = false)
	private IsolationTestDefinition isolationTestDefinition;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "order_id", referencedColumnName = "order_id", insertable = false)
	private Order order;

	@ManyToOne
	@JoinColumns({
			@JoinColumn(name = "order_id", referencedColumnName = "order_order_id", insertable = false, updatable = false),
			@JoinColumn(name = "testtype", referencedColumnName = "isolationtype", insertable = false, updatable = false) })
	private IsolationTestDefinition mainIsolationDefinition;

	// entegrasyon
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	private SalesItem salesItem;

	@ManyToOne
	@JoinColumns({
			@JoinColumn(name = "sales_item_id", referencedColumnName = "sales_item_id", insertable = false, updatable = false),
			@JoinColumn(name = "testtype", referencedColumnName = "isolationtype", insertable = false, updatable = false) })
	private IsolationTestDefinition salesIsolationDefinition;
	// entegrasyon

	@Column(name = "aciklama")
	private String aciklama;

	public String getAciklama() {
		return aciklama;
	}

	public Integer getId() {
		return id;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public IsolationTestDefinition getIsolationTestDefinition() {
		return isolationTestDefinition;
	}

	public void setIsolationTestDefinition(
			IsolationTestDefinition isolationTestDefinition) {
		this.isolationTestDefinition = isolationTestDefinition;
	}

	public String getTestDegeri() {
		return testDegeri;
	}

	public void setTestDegeri(String testDegeri) {
		this.testDegeri = testDegeri;
	}

	public IsolationTestDefinition getMainIsolationDefinition() {
		return mainIsolationDefinition;
	}

	public void setMainIsolationDefinition(
			IsolationTestDefinition mainIsolationDefinition) {
		this.mainIsolationDefinition = mainIsolationDefinition;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public IsolationTestDefinition getSalesIsolationDefinition() {
		return salesIsolationDefinition;
	}

	public void setSalesIsolationDefinition(
			IsolationTestDefinition salesIsolationDefinition) {
		this.salesIsolationDefinition = salesIsolationDefinition;
	}

}
