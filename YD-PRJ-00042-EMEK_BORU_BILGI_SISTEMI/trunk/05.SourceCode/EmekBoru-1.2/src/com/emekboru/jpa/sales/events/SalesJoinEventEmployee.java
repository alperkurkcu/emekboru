package com.emekboru.jpa.sales.events;

import java.io.Serializable;

import javax.persistence.*;

import com.emekboru.jpa.Employee;

@Entity
@Table(name = "sales_join_event_employee")
public class SalesJoinEventEmployee implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2417798066073640176L;

	@Id
	@Column(name = "join_id")
	@SequenceGenerator(name = "SALES_JOIN_EVENT_EMPLOYEE_GENERATOR", sequenceName = "SALES_JOIN_EVENT_EMPLOYEE_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_JOIN_EVENT_EMPLOYEE_GENERATOR")
	private int joinId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "employee_id", referencedColumnName = "employee_id", updatable = false)
	private Employee employee;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "event_id", referencedColumnName = "event_id", updatable = false)
	private SalesEvent event;

	public SalesJoinEventEmployee() {
		employee = new Employee();
		event = new SalesEvent();
	}

	/**
	 * GETTERS AND SETTERS
	 */

	public int getJoinId() {
		return joinId;
	}

	public SalesEvent getEvent() {
		return event;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setJoinId(Integer joinId) {
		this.joinId = joinId;
	}

	public void setEvent(SalesEvent event) {
		this.event = event;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
