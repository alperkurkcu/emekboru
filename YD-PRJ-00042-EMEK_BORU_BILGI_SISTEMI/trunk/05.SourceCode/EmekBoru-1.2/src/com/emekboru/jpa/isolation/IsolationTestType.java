package com.emekboru.jpa.isolation;

public enum IsolationTestType {

	/**
	 * IC ve DIS KAPLAMA TEST KODLARI
	 */

	// iç kaplama
	LSC1("Kumlanmış Boruların Yüzey Temizliği", "LSC1", 2123), LSP1(
			"Kumlanmış Boruların Yüzey Pürüzlülüğü", "LSP1", 2124), LSD1(
			"Kumlanmış Boruların Toz Miktarı", "LSD1", 2125), LSS1(
			"Kumlanmış Boruların Tuz Miktarı", "LSS1", 2126), LAS1(
			"Kumlama Malzemesindeki Tuz Miktarı", "LAS1", 2127), LAC1(
			"Kumlama Ortam Şartları", "LAC1", 2128), LBT1("Bükme Testi",
			"LBT1", 2129), LCT1("Kaplama Kalınlığı", "LCT1", 2130), LHT1(
			"Holiday / Pinhole Testi", "LHT1", 2131), LIT1("Darbe Testi",
			"LIT1", 2132), LAT1("Yapışma Cross Cut Testi", "LAT1", 2133), LIT2(
			"Delici Uca Karşı Direnç Testi", "LIT2", 2134), LCD1(
			"Katodik Soyulma Testi", "LCD1", 2135), LHT2("Sertlik Testi",
			"LHT2", 2136), LFI1("Nihai Kontrol", "LFI1", 2137), BCB1(
			"Beton Eğme-Basma Testi", "BCB1", 2138), LBZ1("Buchholz Testi",
			"LBZ1", 2139), EXC1("Toz Epoksi X-Cut Testi", "EXC1", 2140), BSC1(
			"Kum/Çimento(S/C)-Su/Çimento(W/C) Oranı Tayini", "BSC1", 2141), POY1(
			"Pull-Off Yapışma Testi", "POY1", 2142), BYH1(
			"Beton Yüzey Hazırlığı", "BYH1", 2143), BGK1("Beton Göz Kontrol",
			"BGK1", 2144), BKK1("Boya Sonrası Yüzey Kontrol Testi", "BKK1",
			2145), LKB1("Kaplamasız Bölge Kontrolü", "LKB1", 2146), LKK1(
			"Kumlama malzemesi, boyut, şekil ve özellik kontrolü", "LKK1", 2148), LKT1(
			"Boya Kaplama Tamir Formu", "LKT1", 2149), LSK1(
			"Kaplamadan Önce Yüzey Kontrolü", "LSK1", 2150), LDT1(
			"Daldırma Testi", "LDT1", 2151), FPT1("Pinhole Test", "FPT1", 2152), FBH1(
			"Bucholz Test", "FBH1", 2153), FFT1("Film Thickness Test", "FFT1",
			2154), FCT1("Cure Test", "FCT1", 2155), FBT1("Bend Test", "FBT1",
			2156), FWT1("Water Test", "FWT1", 2157), FAT1(
			"Bond (Adhesion) Test", "FAT1", 2158), FST1("Stripping Test",
			"FST1", 2159), LGB("Gas Blistering Testi", "LGB", 2160), LHB(
			"Hydraulic Blistering Testi", "LHB", 2161), LMP(
			"Density, Viscosity and Temperature of Mixed Wet Paint Testi ",
			"LMP", 2162),

	// dış kaplama
	CSC1("Kumlanmış Boruların Yüzey Temizliği", "CSC1", 2005), CSP1(
			"Kumlanmış Boruların Yüzey Pürüzlülüğü", "CSP1", 2006), CSD1(
			"Kumlanmış Boruların Toz Miktarı", "CSD1", 2007), CSS1(
			"Kumlanmış Boruların Tuz Miktarı", "CSS1", 2008), CAS1(
			"Kumlama Malzemesindeki Tuz Miktarı", "CAS1", 2009), CAC1(
			"Kumlama Ortam Şartları", "CAC1", 2010), CKT1(
			"Kaplama Tamir Test Raporu", "CKT1", 2011), CBT1("Bükme Testi",
			"CBT1", 2012), CDS1("Toz Boya Kaplamanın DSC Testi", "CDS1", 2013), CCT1(
			"MPQT Kaplama Kalınlığı", "CCT1", 2014), OHT1(
			"Online Holiday Testi", "OHT1", 2015), CIT1("Darbe Testi", "CIT1",
			2016), CAT1("Yapışma Testi", "CAT1", 2017), CIT2(
			"Delici Uca Karşı Direnç Testi", "CIT2", 2018), CET1(
			"Polietilen Uzama Testi", "CET1", 2019), CCD1(
			"Katodik Soyulma Testi", "CCD1", 2020), CHT2("Sertlik Testi",
			"CHT2", 2021), CFI1("Nihai Kontrol", "CFI1", 2022), DKK1(
			"Dış Kaplama Kalınlığı Testi", "DKK1", 2023),

	CKK1("Kumlama malzemesi, boyut, şekil ve özellik kontrolü", "CKK1", 2025),

	KSK1("Kaplama Uygulama Sıcaklık Kontrolü", "KSK1", 2030), CPP1(
			"Polipropilen-Polietilen Process Bozulma Kontrolü", "CPP1", 2031), CPP2(
			"Yapıştırıcı Process Bozulma Kontrolü", "CPP2", 2032), CKB1(
			"Kaplamasız Bölge Kontrolü", "CKB1", 2033), CKT2(
			"Kaplama Tamiri Kontrolü", "CKT2", 2034), CGK1("Göz Kontrol",
			"CGK1", 2035), CED("Kaplama Elektrik Direnci Testi", "CED", 2039), CTY(
			"Thermal Ageing Testi", "CTY", 2040), CST(
			"Storage Condition - UV Direnci Testi", "CST", 2041), CUV(
			"UV Ageing Testi", "CUV", 2042);

	private String label;
	private String code;
	private Integer globalId;

	private IsolationTestType(String label, String code, Integer globalId) {
		this.label = label;
		this.code = code;
		this.globalId = globalId;
	}

	public String getLabel() {
		return label;
	}

	public String getCode() {
		return code;
	}

	public Integer getGlobalId() {
		return globalId;
	}
}
