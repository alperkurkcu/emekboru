package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.isolation.IsolationTestDefinition;

/**
 * The persistent class for the test_kaplama_sertlik_sonuc database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaSertlikSonuc.findAll", query = "SELECT r FROM TestKaplamaSertlikSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_kaplama_sertlik_sonuc")
public class TestKaplamaSertlikSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KAPLAMA_SERTLIK_SONUC_ID_GENERATOR", sequenceName = "TEST_KAPLAMA_SERTLIK_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KAPLAMA_SERTLIK_SONUC_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Column(name = "aciklama", length = 255)
	private String aciklama;

	// @Column(name="destructive_test_id", nullable=false)
	// private Integer destructiveTestId;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name="pipe_id", nullable=false)
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@Column(nullable = false)
	private BigDecimal sonuc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi", nullable = false)
	private Date testTarihi;

	@Column(name = "test_no_a")
	private BigDecimal testNoA;

	@Column(name = "test_no_b")
	private BigDecimal testNoB;

	@Column(name = "test_no_c")
	private BigDecimal testNoC;

	@Column(name = "test_no_d")
	private BigDecimal testNoD;

	@Column(name = "test_no_e")
	private BigDecimal testNoE;

	@Column(name = "sertlik_degeri_a")
	private BigDecimal sertlikDegeriA;

	@Column(name = "sertlik_degeri_b")
	private BigDecimal sertlikDegeriB;

	@Column(name = "sertlik_degeri_c")
	private BigDecimal sertlikDegeriC;

	@Column(name = "sertlik_degeri_d")
	private BigDecimal sertlikDegeriD;

	@Column(name = "sertlik_degeri_e")
	private BigDecimal sertlikDegeriE;

	@Column(name = "ortalama_sertlik")
	private BigDecimal ortalamaSertlik;

	@Column(name = "kalinlik")
	private BigDecimal kalinlik;

	@Column(name = "sicaklik")
	private BigDecimal sicaklik;

	@Column(name = "bagil_nem")
	private BigDecimal bagilNem;

	@Column(name = "katman_sayisi")
	private Integer katmanSayisi;

	public TestKaplamaSertlikSonuc() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Date getTestTarihi() {
		return this.testTarihi;
	}

	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}

	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}

	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the sonuc
	 */
	public BigDecimal getSonuc() {
		return sonuc;
	}

	/**
	 * @param sonuc
	 *            the sonuc to set
	 */
	public void setSonuc(BigDecimal sonuc) {
		this.sonuc = sonuc;
	}

	/**
	 * @return the testNoA
	 */
	public BigDecimal getTestNoA() {
		return testNoA;
	}

	/**
	 * @param testNoA
	 *            the testNoA to set
	 */
	public void setTestNoA(BigDecimal testNoA) {
		this.testNoA = testNoA;
	}

	/**
	 * @return the testNoB
	 */
	public BigDecimal getTestNoB() {
		return testNoB;
	}

	/**
	 * @param testNoB
	 *            the testNoB to set
	 */
	public void setTestNoB(BigDecimal testNoB) {
		this.testNoB = testNoB;
	}

	/**
	 * @return the testNoC
	 */
	public BigDecimal getTestNoC() {
		return testNoC;
	}

	/**
	 * @param testNoC
	 *            the testNoC to set
	 */
	public void setTestNoC(BigDecimal testNoC) {
		this.testNoC = testNoC;
	}

	/**
	 * @return the testNoD
	 */
	public BigDecimal getTestNoD() {
		return testNoD;
	}

	/**
	 * @param testNoD
	 *            the testNoD to set
	 */
	public void setTestNoD(BigDecimal testNoD) {
		this.testNoD = testNoD;
	}

	/**
	 * @return the testNoE
	 */
	public BigDecimal getTestNoE() {
		return testNoE;
	}

	/**
	 * @param testNoE
	 *            the testNoE to set
	 */
	public void setTestNoE(BigDecimal testNoE) {
		this.testNoE = testNoE;
	}

	/**
	 * @return the sertlikDegeriA
	 */
	public BigDecimal getSertlikDegeriA() {
		return sertlikDegeriA;
	}

	/**
	 * @param sertlikDegeriA
	 *            the sertlikDegeriA to set
	 */
	public void setSertlikDegeriA(BigDecimal sertlikDegeriA) {
		this.sertlikDegeriA = sertlikDegeriA;
	}

	/**
	 * @return the sertlikDegeriB
	 */
	public BigDecimal getSertlikDegeriB() {
		return sertlikDegeriB;
	}

	/**
	 * @param sertlikDegeriB
	 *            the sertlikDegeriB to set
	 */
	public void setSertlikDegeriB(BigDecimal sertlikDegeriB) {
		this.sertlikDegeriB = sertlikDegeriB;
	}

	/**
	 * @return the sertlikDegeriC
	 */
	public BigDecimal getSertlikDegeriC() {
		return sertlikDegeriC;
	}

	/**
	 * @param sertlikDegeriC
	 *            the sertlikDegeriC to set
	 */
	public void setSertlikDegeriC(BigDecimal sertlikDegeriC) {
		this.sertlikDegeriC = sertlikDegeriC;
	}

	/**
	 * @return the sertlikDegeriD
	 */
	public BigDecimal getSertlikDegeriD() {
		return sertlikDegeriD;
	}

	/**
	 * @param sertlikDegeriD
	 *            the sertlikDegeriD to set
	 */
	public void setSertlikDegeriD(BigDecimal sertlikDegeriD) {
		this.sertlikDegeriD = sertlikDegeriD;
	}

	/**
	 * @return the sertlikDegeriE
	 */
	public BigDecimal getSertlikDegeriE() {
		return sertlikDegeriE;
	}

	/**
	 * @param sertlikDegeriE
	 *            the sertlikDegeriE to set
	 */
	public void setSertlikDegeriE(BigDecimal sertlikDegeriE) {
		this.sertlikDegeriE = sertlikDegeriE;
	}

	/**
	 * @return the ortalamaSertlik
	 */
	public BigDecimal getOrtalamaSertlik() {
		return ortalamaSertlik;
	}

	/**
	 * @param ortalamaSertlik
	 *            the ortalamaSertlik to set
	 */
	public void setOrtalamaSertlik(BigDecimal ortalamaSertlik) {
		this.ortalamaSertlik = ortalamaSertlik;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the eklemeZamani
	 */
	public Timestamp getEklemeZamani() {
		return eklemeZamani;
	}

	/**
	 * @param eklemeZamani
	 *            the eklemeZamani to set
	 */
	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncellemeZamani
	 */
	public Timestamp getGuncellemeZamani() {
		return guncellemeZamani;
	}

	/**
	 * @param guncellemeZamani
	 *            the guncellemeZamani to set
	 */
	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the kalinlik
	 */
	public BigDecimal getKalinlik() {
		return kalinlik;
	}

	/**
	 * @param kalinlik
	 *            the kalinlik to set
	 */
	public void setKalinlik(BigDecimal kalinlik) {
		this.kalinlik = kalinlik;
	}

	/**
	 * @return the sicaklik
	 */
	public BigDecimal getSicaklik() {
		return sicaklik;
	}

	/**
	 * @param sicaklik
	 *            the sicaklik to set
	 */
	public void setSicaklik(BigDecimal sicaklik) {
		this.sicaklik = sicaklik;
	}

	/**
	 * @return the bagilNem
	 */
	public BigDecimal getBagilNem() {
		return bagilNem;
	}

	/**
	 * @param bagilNem
	 *            the bagilNem to set
	 */
	public void setBagilNem(BigDecimal bagilNem) {
		this.bagilNem = bagilNem;
	}

	/**
	 * @return the katmanSayisi
	 */
	public Integer getKatmanSayisi() {
		return katmanSayisi;
	}

	/**
	 * @param katmanSayisi
	 *            the katmanSayisi to set
	 */
	public void setKatmanSayisi(Integer katmanSayisi) {
		this.katmanSayisi = katmanSayisi;
	}

}