package com.emekboru.jpa.rulo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQueries({ @NamedQuery(name = "RuloTestKimyasalUretici.getByDokumNo", query = "SELECT r FROM RuloTestKimyasalUretici r WHERE r.rulo.ruloDokumNo=:prmDokumNo") })
@Table(name = "rulo_test_kimyasal_uretici")
public class RuloTestKimyasalUretici implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "rulo_test_kimyasal_uretici_generator", sequenceName = "rulo_test_kimyasal_uretici_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rulo_test_kimyasal_uretici_generator")
	@Column(name = "rulo_test_kimyasal_uretici_id")
	private Integer ruloTestKimyasalUreticiId;

	@Column(name = "rulo_test_kimyasal_uretici_ti")
	private Float ruloTestKimyasalUreticiTi;

	@Column(name = "rulo_test_kimyasal_uretici_v")
	private Float ruloTestKimyasalUreticiV;

	@Column(name = "rulo_test_kimyasal_uretici_nb")
	private Float ruloTestKimyasalUreticiNb;

	@Column(name = "rulo_test_kimyasal_uretici_b")
	private Float ruloTestKimyasalUreticiB;

	@Column(name = "rulo_test_kimyasal_uretici_co")
	private Float ruloTestKimyasalUreticiCo;

	@Column(name = "rulo_test_kimyasal_uretici_n")
	private Float ruloTestKimyasalUreticiN;

	@Column(name = "rulo_test_kimyasal_uretici_ce1")
	private Float ruloTestKimyasalUreticiCe1;

	@Column(name = "rulo_test_kimyasal_uretici_ce2")
	private Float ruloTestKimyasalUreticiCe2;

	@Column(name = "rulo_test_kimyasal_uretici_ae")
	private Float ruloTestKimyasalUreticiAe;

	@Column(name = "rulo_test_kimyasal_uretici_c")
	private Float ruloTestKimyasalUreticiC;

	@Column(name = "rulo_test_kimyasal_uretici_si")
	private Float ruloTestKimyasalUreticiSi;

	@Column(name = "rulo_test_kimyasal_uretici_mn")
	private Float ruloTestKimyasalUreticiMn;

	@Column(name = "rulo_test_kimyasal_uretici_p")
	private Float ruloTestKimyasalUreticiP;

	@Column(name = "rulo_test_kimyasal_uretici_s")
	private Float ruloTestKimyasalUreticiS;

	@Column(name = "rulo_test_kimyasal_uretici_cr")
	private Float ruloTestKimyasalUreticiCr;

	@Column(name = "rulo_test_kimyasal_uretici_ni")
	private Float ruloTestKimyasalUreticiNi;

	@Column(name = "rulo_test_kimyasal_uretici_mo")
	private Float ruloTestKimyasalUreticiMo;

	@Column(name = "rulo_test_kimyasal_uretici_cu")
	private Float ruloTestKimyasalUreticiCu;

	@Column(name = "rulo_test_kimyasal_uretici_al")
	private Float ruloTestKimyasalUreticiAl;

	@Column(name = "rulo_test_kimyasal_uretici_as")
	private Float ruloTestKimyasalUreticiAs;

	@Column(name = "rulo_test_kimyasal_uretici_sn")
	private Float ruloTestKimyasalUreticiSn;

	@Column(name = "rulo_test_kimyasal_uretici_sb")
	private Float ruloTestKimyasalUreticiSb;

	@Column(name = "rulo_test_kimyasal_uretici_w")
	private Float ruloTestKimyasalUreticiW;

	@Column(name = "rulo_test_kimyasal_uretici_pb")
	private Float ruloTestKimyasalUreticiPb;

	@Column(name = "rulo_test_kimyasal_uretici_ca")
	private Float ruloTestKimyasalUreticiCa;

	@Column(name = "rulo_test_kimyasal_uretici_aciklama")
	private String ruloTestKimyasalUreticiAciklama;

	@Temporal(TemporalType.DATE)
	@Column(name = "rulo_test_kimyasal_uretici_tarih")
	private Date ruloTestKimyasalUreticiTarih;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "rulo_id", nullable = false)
	private Rulo rulo;

	@Column(name = "durum")
	private Integer durum = 0;

	@Column(name = "islem_yapan_id")
	private int islemYapanId;

	@Column(name = "islem_yapan_isim")
	private String islemYapanIsim;

	@Column(name = "ti_")
	private String tiOnEk;
	@Column(name = "nb_")
	private String nbOnEk;
	@Column(name = "mo_")
	private String moOnEk;
	@Column(name = "ni_")
	private String niOnEk;
	@Column(name = "s_")
	private String sOnEk;
	@Column(name = "cu_")
	private String cuOnEk;
	@Column(name = "al_")
	private String alOnEk;
	@Column(name = "c_")
	private String cOnEk;
	@Column(name = "cr_")
	private String crOnEk;
	@Column(name = "b_")
	private String bOnEk;
	@Column(name = "v_")
	private String vOnEk;
	@Column(name = "co_")
	private String coOnEk;

	@Column(name = "si_")
	private String siOnEk;
	@Column(name = "w_")
	private String wOnEk;
	@Column(name = "as_")
	private String asOnEk;
	@Column(name = "sb_")
	private String sbOnEk;
	@Column(name = "sn_")
	private String snOnEk;
	@Column(name = "pb_")
	private String pbOnEk;
	@Column(name = "ca_")
	private String caOnEk;
	@Column(name = "n_")
	private String nOnEk;

	public String getTiOnEk() {
		return tiOnEk;
	}

	public void setTiOnEk(String tiOnEk) {
		this.tiOnEk = tiOnEk;
	}

	public String getNbOnEk() {
		return nbOnEk;
	}

	public void setNbOnEk(String nbOnEk) {
		this.nbOnEk = nbOnEk;
	}

	public String getMoOnEk() {
		return moOnEk;
	}

	public void setMoOnEk(String moOnEk) {
		this.moOnEk = moOnEk;
	}

	public String getNiOnEk() {
		return niOnEk;
	}

	public void setNiOnEk(String niOnEk) {
		this.niOnEk = niOnEk;
	}

	public String getsOnEk() {
		return sOnEk;
	}

	public void setsOnEk(String sOnEk) {
		this.sOnEk = sOnEk;
	}

	public String getCuOnEk() {
		return cuOnEk;
	}

	public void setCuOnEk(String cuOnEk) {
		this.cuOnEk = cuOnEk;
	}

	public String getAlOnEk() {
		return alOnEk;
	}

	public void setAlOnEk(String alOnEk) {
		this.alOnEk = alOnEk;
	}

	public String getcOnEk() {
		return cOnEk;
	}

	public void setcOnEk(String cOnEk) {
		this.cOnEk = cOnEk;
	}

	public String getCrOnEk() {
		return crOnEk;
	}

	public void setCrOnEk(String crOnEk) {
		this.crOnEk = crOnEk;
	}

	public String getbOnEk() {
		return bOnEk;
	}

	public void setbOnEk(String bOnEk) {
		this.bOnEk = bOnEk;
	}

	public String getvOnEk() {
		return vOnEk;
	}

	public void setvOnEk(String vOnEk) {
		this.vOnEk = vOnEk;
	}

	public String getCoOnEk() {
		return coOnEk;
	}

	public void setCoOnEk(String coOnEk) {
		this.coOnEk = coOnEk;
	}

	public int getIslemYapanId() {
		return islemYapanId;
	}

	public void setIslemYapanId(int islemYapanId) {
		this.islemYapanId = islemYapanId;
	}

	public String getIslemYapanIsim() {
		return islemYapanIsim;
	}

	public void setIslemYapanIsim(String islemYapanIsim) {
		this.islemYapanIsim = islemYapanIsim;
	}

	public Integer getDurum() {
		return durum;
	}

	public void setDurum(Integer durum) {
		this.durum = durum;
	}

	public Rulo getRulo() {
		return rulo;
	}

	public void setRulo(Rulo rulo) {
		this.rulo = rulo;
	}

	public Integer getRuloTestKimyasalUreticiId() {
		return ruloTestKimyasalUreticiId;
	}

	public void setRuloTestKimyasalUreticiId(Integer ruloTestKimyasalUreticiId) {
		this.ruloTestKimyasalUreticiId = ruloTestKimyasalUreticiId;
	}

	public String getRuloTestKimyasalUreticiAciklama() {
		return ruloTestKimyasalUreticiAciklama;
	}

	public void setRuloTestKimyasalUreticiAciklama(
			String ruloTestKimyasalUreticiAciklama) {
		this.ruloTestKimyasalUreticiAciklama = ruloTestKimyasalUreticiAciklama;
	}

	public Date getRuloTestKimyasalUreticiTarih() {
		return ruloTestKimyasalUreticiTarih;
	}

	public void setRuloTestKimyasalUreticiTarih(
			Date ruloTestKimyasalUreticiTarih) {
		this.ruloTestKimyasalUreticiTarih = ruloTestKimyasalUreticiTarih;
	}

	public Float getRuloTestKimyasalUreticiTi() {
		return ruloTestKimyasalUreticiTi;
	}

	public void setRuloTestKimyasalUreticiTi(Float ruloTestKimyasalUreticiTi) {
		this.ruloTestKimyasalUreticiTi = ruloTestKimyasalUreticiTi;
	}

	public Float getRuloTestKimyasalUreticiV() {
		return ruloTestKimyasalUreticiV;
	}

	public void setRuloTestKimyasalUreticiV(Float ruloTestKimyasalUreticiV) {
		this.ruloTestKimyasalUreticiV = ruloTestKimyasalUreticiV;
	}

	public Float getRuloTestKimyasalUreticiNb() {
		return ruloTestKimyasalUreticiNb;
	}

	public void setRuloTestKimyasalUreticiNb(Float ruloTestKimyasalUreticiNb) {
		this.ruloTestKimyasalUreticiNb = ruloTestKimyasalUreticiNb;
	}

	public Float getRuloTestKimyasalUreticiB() {
		return ruloTestKimyasalUreticiB;
	}

	public void setRuloTestKimyasalUreticiB(Float ruloTestKimyasalUreticiB) {
		this.ruloTestKimyasalUreticiB = ruloTestKimyasalUreticiB;
	}

	public Float getRuloTestKimyasalUreticiCo() {
		return ruloTestKimyasalUreticiCo;
	}

	public void setRuloTestKimyasalUreticiCo(Float ruloTestKimyasalUreticiCo) {
		this.ruloTestKimyasalUreticiCo = ruloTestKimyasalUreticiCo;
	}

	public Float getRuloTestKimyasalUreticiN() {
		return ruloTestKimyasalUreticiN;
	}

	public void setRuloTestKimyasalUreticiN(Float ruloTestKimyasalUreticiN) {
		this.ruloTestKimyasalUreticiN = ruloTestKimyasalUreticiN;
	}

	public Float getRuloTestKimyasalUreticiCe1() {
		return ruloTestKimyasalUreticiCe1;
	}

	public void setRuloTestKimyasalUreticiCe1(Float ruloTestKimyasalUreticiCe1) {
		this.ruloTestKimyasalUreticiCe1 = ruloTestKimyasalUreticiCe1;
	}

	public Float getRuloTestKimyasalUreticiCe2() {
		return ruloTestKimyasalUreticiCe2;
	}

	public void setRuloTestKimyasalUreticiCe2(Float ruloTestKimyasalUreticiCe2) {
		this.ruloTestKimyasalUreticiCe2 = ruloTestKimyasalUreticiCe2;
	}

	public Float getRuloTestKimyasalUreticiAe() {
		return ruloTestKimyasalUreticiAe;
	}

	public void setRuloTestKimyasalUreticiAe(Float ruloTestKimyasalUreticiAe) {
		this.ruloTestKimyasalUreticiAe = ruloTestKimyasalUreticiAe;
	}

	public Float getRuloTestKimyasalUreticiC() {
		return ruloTestKimyasalUreticiC;
	}

	public void setRuloTestKimyasalUreticiC(Float ruloTestKimyasalUreticiC) {
		this.ruloTestKimyasalUreticiC = ruloTestKimyasalUreticiC;
	}

	public Float getRuloTestKimyasalUreticiSi() {
		return ruloTestKimyasalUreticiSi;
	}

	public void setRuloTestKimyasalUreticiSi(Float ruloTestKimyasalUreticiSi) {
		this.ruloTestKimyasalUreticiSi = ruloTestKimyasalUreticiSi;
	}

	public Float getRuloTestKimyasalUreticiMn() {
		return ruloTestKimyasalUreticiMn;
	}

	public void setRuloTestKimyasalUreticiMn(Float ruloTestKimyasalUreticiMn) {
		this.ruloTestKimyasalUreticiMn = ruloTestKimyasalUreticiMn;
	}

	public Float getRuloTestKimyasalUreticiP() {
		return ruloTestKimyasalUreticiP;
	}

	public void setRuloTestKimyasalUreticiP(Float ruloTestKimyasalUreticiP) {
		this.ruloTestKimyasalUreticiP = ruloTestKimyasalUreticiP;
	}

	public Float getRuloTestKimyasalUreticiS() {
		return ruloTestKimyasalUreticiS;
	}

	public void setRuloTestKimyasalUreticiS(Float ruloTestKimyasalUreticiS) {
		this.ruloTestKimyasalUreticiS = ruloTestKimyasalUreticiS;
	}

	public Float getRuloTestKimyasalUreticiCr() {
		return ruloTestKimyasalUreticiCr;
	}

	public void setRuloTestKimyasalUreticiCr(Float ruloTestKimyasalUreticiCr) {
		this.ruloTestKimyasalUreticiCr = ruloTestKimyasalUreticiCr;
	}

	public Float getRuloTestKimyasalUreticiNi() {
		return ruloTestKimyasalUreticiNi;
	}

	public void setRuloTestKimyasalUreticiNi(Float ruloTestKimyasalUreticiNi) {
		this.ruloTestKimyasalUreticiNi = ruloTestKimyasalUreticiNi;
	}

	public Float getRuloTestKimyasalUreticiMo() {
		return ruloTestKimyasalUreticiMo;
	}

	public void setRuloTestKimyasalUreticiMo(Float ruloTestKimyasalUreticiMo) {
		this.ruloTestKimyasalUreticiMo = ruloTestKimyasalUreticiMo;
	}

	public Float getRuloTestKimyasalUreticiCu() {
		return ruloTestKimyasalUreticiCu;
	}

	public void setRuloTestKimyasalUreticiCu(Float ruloTestKimyasalUreticiCu) {
		this.ruloTestKimyasalUreticiCu = ruloTestKimyasalUreticiCu;
	}

	public Float getRuloTestKimyasalUreticiAl() {
		return ruloTestKimyasalUreticiAl;
	}

	public void setRuloTestKimyasalUreticiAl(Float ruloTestKimyasalUreticiAl) {
		this.ruloTestKimyasalUreticiAl = ruloTestKimyasalUreticiAl;
	}

	/**
	 * @return the ruloTestKimyasalUreticiAs
	 */
	public Float getRuloTestKimyasalUreticiAs() {
		return ruloTestKimyasalUreticiAs;
	}

	/**
	 * @param ruloTestKimyasalUreticiAs
	 *            the ruloTestKimyasalUreticiAs to set
	 */
	public void setRuloTestKimyasalUreticiAs(Float ruloTestKimyasalUreticiAs) {
		this.ruloTestKimyasalUreticiAs = ruloTestKimyasalUreticiAs;
	}

	/**
	 * @return the ruloTestKimyasalUreticiSn
	 */
	public Float getRuloTestKimyasalUreticiSn() {
		return ruloTestKimyasalUreticiSn;
	}

	/**
	 * @param ruloTestKimyasalUreticiSn
	 *            the ruloTestKimyasalUreticiSn to set
	 */
	public void setRuloTestKimyasalUreticiSn(Float ruloTestKimyasalUreticiSn) {
		this.ruloTestKimyasalUreticiSn = ruloTestKimyasalUreticiSn;
	}

	/**
	 * @return the ruloTestKimyasalUreticiW
	 */
	public Float getRuloTestKimyasalUreticiW() {
		return ruloTestKimyasalUreticiW;
	}

	/**
	 * @param ruloTestKimyasalUreticiW
	 *            the ruloTestKimyasalUreticiW to set
	 */
	public void setRuloTestKimyasalUreticiW(Float ruloTestKimyasalUreticiW) {
		this.ruloTestKimyasalUreticiW = ruloTestKimyasalUreticiW;
	}

	/**
	 * @return the ruloTestKimyasalUreticiCa
	 */
	public Float getRuloTestKimyasalUreticiCa() {
		return ruloTestKimyasalUreticiCa;
	}

	/**
	 * @param ruloTestKimyasalUreticiCa
	 *            the ruloTestKimyasalUreticiCa to set
	 */
	public void setRuloTestKimyasalUreticiCa(Float ruloTestKimyasalUreticiCa) {
		this.ruloTestKimyasalUreticiCa = ruloTestKimyasalUreticiCa;
	}

	/**
	 * @return the ruloTestKimyasalUreticiSb
	 */
	public Float getRuloTestKimyasalUreticiSb() {
		return ruloTestKimyasalUreticiSb;
	}

	/**
	 * @param ruloTestKimyasalUreticiSb
	 *            the ruloTestKimyasalUreticiSb to set
	 */
	public void setRuloTestKimyasalUreticiSb(Float ruloTestKimyasalUreticiSb) {
		this.ruloTestKimyasalUreticiSb = ruloTestKimyasalUreticiSb;
	}

	/**
	 * @return the ruloTestKimyasalUreticiPb
	 */
	public Float getRuloTestKimyasalUreticiPb() {
		return ruloTestKimyasalUreticiPb;
	}

	/**
	 * @param ruloTestKimyasalUreticiPb
	 *            the ruloTestKimyasalUreticiPb to set
	 */
	public void setRuloTestKimyasalUreticiPb(Float ruloTestKimyasalUreticiPb) {
		this.ruloTestKimyasalUreticiPb = ruloTestKimyasalUreticiPb;
	}

	/**
	 * @return the siOnEk
	 */
	public String getSiOnEk() {
		return siOnEk;
	}

	/**
	 * @param siOnEk
	 *            the siOnEk to set
	 */
	public void setSiOnEk(String siOnEk) {
		this.siOnEk = siOnEk;
	}

	/**
	 * @return the wOnEk
	 */
	public String getwOnEk() {
		return wOnEk;
	}

	/**
	 * @param wOnEk
	 *            the wOnEk to set
	 */
	public void setwOnEk(String wOnEk) {
		this.wOnEk = wOnEk;
	}

	/**
	 * @return the asOnEk
	 */
	public String getAsOnEk() {
		return asOnEk;
	}

	/**
	 * @param asOnEk
	 *            the asOnEk to set
	 */
	public void setAsOnEk(String asOnEk) {
		this.asOnEk = asOnEk;
	}

	/**
	 * @return the sbOnEk
	 */
	public String getSbOnEk() {
		return sbOnEk;
	}

	/**
	 * @param sbOnEk
	 *            the sbOnEk to set
	 */
	public void setSbOnEk(String sbOnEk) {
		this.sbOnEk = sbOnEk;
	}

	/**
	 * @return the snOnEk
	 */
	public String getSnOnEk() {
		return snOnEk;
	}

	/**
	 * @param snOnEk
	 *            the snOnEk to set
	 */
	public void setSnOnEk(String snOnEk) {
		this.snOnEk = snOnEk;
	}

	/**
	 * @return the pbOnEk
	 */
	public String getPbOnEk() {
		return pbOnEk;
	}

	/**
	 * @param pbOnEk
	 *            the pbOnEk to set
	 */
	public void setPbOnEk(String pbOnEk) {
		this.pbOnEk = pbOnEk;
	}

	/**
	 * @return the caOnEk
	 */
	public String getCaOnEk() {
		return caOnEk;
	}

	/**
	 * @param caOnEk
	 *            the caOnEk to set
	 */
	public void setCaOnEk(String caOnEk) {
		this.caOnEk = caOnEk;
	}

	/**
	 * @return the nOnEk
	 */
	public String getnOnEk() {
		return nOnEk;
	}

	/**
	 * @param nOnEk
	 *            the nOnEk to set
	 */
	public void setnOnEk(String nOnEk) {
		this.nOnEk = nOnEk;
	}

}
