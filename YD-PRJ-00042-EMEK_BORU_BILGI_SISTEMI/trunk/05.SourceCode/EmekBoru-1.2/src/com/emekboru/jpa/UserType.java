package com.emekboru.jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the user_type database table.
 * 
 */
@Entity
@Table(name="user_type")
public class UserType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="user_type_id")
	@SequenceGenerator(name="USER_TYPE_GENERATOR", sequenceName="USER_TYPE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="USER_TYPE_GENERATOR")	
	private Integer userTypeId;

	@Column(name="title")
	private String title;

	@Column(name="type")
	private String type;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userType")
    private List<SystemUser> users;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "userType", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<OperationPrivilege> opPrivileges;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "userType", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<GeneralPrivilege> generalPrivileges;

    public UserType() {
    	
    	generalPrivileges = new ArrayList<GeneralPrivilege>(1);
    	opPrivileges = new ArrayList<OperationPrivilege>(1);
    }

	public Integer getUserTypeId() {
		return this.userTypeId;
	}

	public void setUserTypeId(Integer userTypeId) {
		this.userTypeId = userTypeId;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<SystemUser> getUsers() {
		return users;
	}

	public void setUsers(List<SystemUser> users) {
		this.users = users;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<OperationPrivilege> getOpPrivileges() {
		return opPrivileges;
	}

	public void setOpPrivileges(List<OperationPrivilege> opPrivileges) {
		this.opPrivileges = opPrivileges;
	}

	public List<GeneralPrivilege> getGeneralPrivileges() {
		return generalPrivileges;
	}

	public void setGeneralPrivileges(List<GeneralPrivilege> generalPrivileges) {
		this.generalPrivileges = generalPrivileges;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}