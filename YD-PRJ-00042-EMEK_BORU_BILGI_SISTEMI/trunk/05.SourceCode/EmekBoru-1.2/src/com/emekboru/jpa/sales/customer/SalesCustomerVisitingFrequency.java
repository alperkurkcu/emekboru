package com.emekboru.jpa.sales.customer;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "sales_customer_visiting_frequency")
public class SalesCustomerVisitingFrequency implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8115562420628625777L;

	@Id
	@Column(name = "frequency_id")
	private int frequencyId;

	@Column(name = "frequency")
	private String frequency;

	@OneToMany
	private List<SalesCRMVisit> salesCRMVisit;

	public SalesCustomerVisitingFrequency() {

	}

	@Override
	public boolean equals(Object o) {
		return this.getFrequencyId() == ((SalesCustomerVisitingFrequency) o)
				.getFrequencyId();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public int getFrequencyId() {
		return frequencyId;
	}

	public void setFrequencyId(int frequencyId) {
		this.frequencyId = frequencyId;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public List<SalesCRMVisit> getSalesCRMVisit() {
		return salesCRMVisit;
	}

	public void setSalesCRMVisit(List<SalesCRMVisit> salesCRMVisit) {
		this.salesCRMVisit = salesCRMVisit;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
