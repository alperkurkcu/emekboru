package com.emekboru.jpa.kaplamagirditest;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the test_hammadde_girdi_bkm1_kati_madde database
 * table.
 * 
 */
@Entity
@Table(name = "test_hammadde_girdi_bkm1_kati_madde")
public class TestHammaddeGirdiBkm1KatiMadde implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_HAMMADDE_GIRDI_BKM1_KATI_MADDE_ID_GENERATOR", sequenceName = "TEST_HAMMADDE_GIRDI_BKM1_KATI_MADDE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_HAMMADDE_GIRDI_BKM1_KATI_MADDE_ID_GENERATOR")
	@Column(name = "ID")
	private Integer id;

	// @Column(name = "coat_material_id")
	// private Integer coatMaterialId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coat_material_id", referencedColumnName = "coat_material_id", insertable = false)
	private CoatRawMaterial coatRawMaterial;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "bos_kap_agirligi")
	private String bosKapAgirligi;

	@Column(name = "kapsul_kalan_numune_agirligi")
	private String kapsulKalanNumuneAgirligi;

	@Column(name = "kapsul_numune_agirligi")
	private String kapsulNumuneAgirligi;

	@Column(name = "kuru_madde_icerigi")
	private String kuruMaddeIcerigi;
	
	@Column(name = "rapor_no")
	private String raporNo;

	public TestHammaddeGirdiBkm1KatiMadde() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBosKapAgirligi() {
		return this.bosKapAgirligi;
	}

	public void setBosKapAgirligi(String bosKapAgirligi) {
		this.bosKapAgirligi = bosKapAgirligi;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getKapsulKalanNumuneAgirligi() {
		return this.kapsulKalanNumuneAgirligi;
	}

	public void setKapsulKalanNumuneAgirligi(String kapsulKalanNumuneAgirligi) {
		this.kapsulKalanNumuneAgirligi = kapsulKalanNumuneAgirligi;
	}

	public String getKapsulNumuneAgirligi() {
		return this.kapsulNumuneAgirligi;
	}

	public void setKapsulNumuneAgirligi(String kapsulNumuneAgirligi) {
		this.kapsulNumuneAgirligi = kapsulNumuneAgirligi;
	}

	public String getKuruMaddeIcerigi() {
		return this.kuruMaddeIcerigi;
	}

	public void setKuruMaddeIcerigi(String kuruMaddeIcerigi) {
		this.kuruMaddeIcerigi = kuruMaddeIcerigi;
	}

	/**
	 * @return the coatRawMaterial
	 */
	public CoatRawMaterial getCoatRawMaterial() {
		return coatRawMaterial;
	}

	/**
	 * @param coatRawMaterial
	 *            the coatRawMaterial to set
	 */
	public void setCoatRawMaterial(CoatRawMaterial coatRawMaterial) {
		this.coatRawMaterial = coatRawMaterial;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the raporNo
	 */
	public String getRaporNo() {
		return raporNo;
	}

	/**
	 * @param raporNo the raporNo to set
	 */
	public void setRaporNo(String raporNo) {
		this.raporNo = raporNo;
	}

}