package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;

/**
 * The persistent class for the test_kaplama_kumlanmıs_boru_tuz_miktari_sonuc
 * database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaKumlanmisBoruTuzMiktariSonuc.findAll", query = "SELECT r FROM TestKaplamaKumlanmisBoruTuzMiktariSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_kaplama_kumlanmis_boru_tuz_miktari_sonuc")
public class TestKaplamaKumlanmisBoruTuzMiktariSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KAPLAMA_KUMLANMIS_BORU_TUZ_MIKTARI_SONUC_ID_GENERATOR", sequenceName = "TEST_KAPLAMA_KUMLANMIS_BORU_TUZ_MIKTARI_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KAPLAMA_KUMLANMIS_BORU_TUZ_MIKTARI_SONUC_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ekleme_zamani")
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	// @Column(name="global_id")
	// private Integer globalId;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	// @Column(name="pipe_id")
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	private Integer sonuc;

	@Column(name = "test_gereksinimi_birimi")
	private Integer testGereksinimiBirimi;

	@Column(name = "test_gereksinimi")
	private BigDecimal testGereksinimi;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi")
	private Date testTarihi;

	@Column(name = "tuz_miktari")
	private BigDecimal tuzMiktari;

	@Column(name = "tuz_miktari_birimi")
	private Integer tuzMiktariBirimi;

	@Column(name = "vardiya")
	private Integer vardiya;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;

	public TestKaplamaKumlanmisBoruTuzMiktariSonuc() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getSonuc() {
		return this.sonuc;
	}

	public void setSonuc(Integer sonuc) {
		this.sonuc = sonuc;
	}

	public Date getTestTarihi() {
		return this.testTarihi;
	}

	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	public Integer getTuzMiktariBirimi() {
		return this.tuzMiktariBirimi;
	}

	public void setTuzMiktariBirimi(Integer tuzMiktariBirimi) {
		this.tuzMiktariBirimi = tuzMiktariBirimi;
	}

	public Integer getVardiya() {
		return this.vardiya;
	}

	public void setVardiya(Integer vardiya) {
		this.vardiya = vardiya;
	}

	/**
	 * @return the bagliGlobalId
	 */
	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}

	/**
	 * @param bagliGlobalId
	 *            the bagliGlobalId to set
	 */
	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	/**
	 * @return the bagliTestId
	 */
	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the testGereksinimiBirimi
	 */
	public Integer getTestGereksinimiBirimi() {
		return testGereksinimiBirimi;
	}

	/**
	 * @param testGereksinimiBirimi
	 *            the testGereksinimiBirimi to set
	 */
	public void setTestGereksinimiBirimi(Integer testGereksinimiBirimi) {
		this.testGereksinimiBirimi = testGereksinimiBirimi;
	}

	/**
	 * @return the testGereksinimi
	 */
	public BigDecimal getTestGereksinimi() {
		return testGereksinimi;
	}

	/**
	 * @param testGereksinimi
	 *            the testGereksinimi to set
	 */
	public void setTestGereksinimi(BigDecimal testGereksinimi) {
		this.testGereksinimi = testGereksinimi;
	}

	/**
	 * @return the tuzMiktari
	 */
	public BigDecimal getTuzMiktari() {
		return tuzMiktari;
	}

	/**
	 * @param tuzMiktari
	 *            the tuzMiktari to set
	 */
	public void setTuzMiktari(BigDecimal tuzMiktari) {
		this.tuzMiktari = tuzMiktari;
	}

}