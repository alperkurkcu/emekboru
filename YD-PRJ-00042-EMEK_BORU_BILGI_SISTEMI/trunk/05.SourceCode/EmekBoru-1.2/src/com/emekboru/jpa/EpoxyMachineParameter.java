package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the epoxy_machine_parameters database table.
 * 
 */
@Entity
@Table(name="epoxy_machine_parameters")
public class EpoxyMachineParameter implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="parameters_id")
	@SequenceGenerator(name="EPOXY_MACHINE_PARAMETERS_GENERATOR", sequenceName="EPOXY_MACHINE_PARAMETERS_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="EPOXY_MACHINE_PARAMETERS_GENERATOR")	
	private Integer parametersId;

	@Column(name="araba_hizi")
	private Integer arabaHizi;

	@Column(name="basinc")
	private double basinc;

	@Column(name="machine_id")
	private Integer machineId;

	@Column(name="meme_capi_1")
	private Integer memeCapi1;

	@Column(name="meme_capi_2")
	private Integer memeCapi2;

    public EpoxyMachineParameter() {
    }

	public Integer getParametersId() {
		return this.parametersId;
	}

	public void setParametersId(Integer parametersId) {
		this.parametersId = parametersId;
	}

	public Integer getArabaHizi() {
		return this.arabaHizi;
	}

	public void setArabaHizi(Integer arabaHizi) {
		this.arabaHizi = arabaHizi;
	}

	public double getBasinc() {
		return this.basinc;
	}

	public void setBasinc(double basinc) {
		this.basinc = basinc;
	}

	public Integer getMachineId() {
		return this.machineId;
	}

	public void setMachineId(Integer machineId) {
		this.machineId = machineId;
	}

	public Integer getMemeCapi1() {
		return this.memeCapi1;
	}

	public void setMemeCapi1(Integer memeCapi1) {
		this.memeCapi1 = memeCapi1;
	}

	public Integer getMemeCapi2() {
		return this.memeCapi2;
	}

	public void setMemeCapi2(Integer memeCapi2) {
		this.memeCapi2 = memeCapi2;
	}

}