package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.isolation.IsolationTestDefinition;

/**
 * The persistent class for the test_kaplama_toz_boya_sonuc database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaTozBoyaSonuc.findAll", query = "SELECT r FROM TestKaplamaTozBoyaSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_kaplama_toz_boya_sonuc")
public class TestKaplamaTozBoyaSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KAPLAMA_TOZ_BOYA_SONUC_ID_GENERATOR", sequenceName = "TEST_KAPLAMA_TOZ_BOYA_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KAPLAMA_TOZ_BOYA_SONUC_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Column(name = "aciklama", length = 255)
	private String aciklama;

	private BigDecimal c;

	private BigDecimal dh;

	private BigDecimal dh1;

	@Column(name = "dokuman_kayit_adi")
	private String dokumanKayitAdi;

	@Column(name = "dokuman_orijinal_adi")
	private String dokumanOrijinalAdi;

	@Column(name = "dsc_sonuc")
	private String dscSonuc;

	private BigDecimal dtg;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name="global_id")
	// private Integer globalId;

	//
	// @Column(name = "isolation_test_id")
	// private Integer isolationTestId;

	@Column(name = "kabul_degeri")
	private BigDecimal kabulDegeri;

	@Column(name = "manifacturer_dh")
	private BigDecimal manifacturerDh;

	@Column(name = "manifacturer_ta1")
	private BigDecimal manifacturerTa1;

	@Column(name = "manifacturer_ta2")
	private BigDecimal manifacturerTa2;

	@Column(name = "numune_agirligi")
	private Integer numuneAgirligi;

	@Column(name = "parti_no")
	private Integer partiNo;

	// @Column(name="pipe_id")
	// private Integer pipeId;

	private BigDecimal ta1;

	private BigDecimal ta2;

	private BigDecimal ta3;

	private BigDecimal ta4;

	@Column(name = "test_sonuc")
	private String testSonuc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi")
	private Date testTarihi;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;

	public TestKaplamaTozBoyaSonuc() {
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the aciklama
	 */
	public String getAciklama() {
		return aciklama;
	}

	/**
	 * @param aciklama
	 *            the aciklama to set
	 */
	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	/**
	 * @return the dokumanKayitAdi
	 */
	public String getDokumanKayitAdi() {
		return dokumanKayitAdi;
	}

	/**
	 * @param dokumanKayitAdi
	 *            the dokumanKayitAdi to set
	 */
	public void setDokumanKayitAdi(String dokumanKayitAdi) {
		this.dokumanKayitAdi = dokumanKayitAdi;
	}

	/**
	 * @return the dokumanOrijinalAdi
	 */
	public String getDokumanOrijinalAdi() {
		return dokumanOrijinalAdi;
	}

	/**
	 * @param dokumanOrijinalAdi
	 *            the dokumanOrijinalAdi to set
	 */
	public void setDokumanOrijinalAdi(String dokumanOrijinalAdi) {
		this.dokumanOrijinalAdi = dokumanOrijinalAdi;
	}

	/**
	 * @return the numuneAgirligi
	 */
	public Integer getNumuneAgirligi() {
		return numuneAgirligi;
	}

	/**
	 * @param numuneAgirligi
	 *            the numuneAgirligi to set
	 */
	public void setNumuneAgirligi(Integer numuneAgirligi) {
		this.numuneAgirligi = numuneAgirligi;
	}

	/**
	 * @return the partiNo
	 */
	public Integer getPartiNo() {
		return partiNo;
	}

	/**
	 * @param partiNo
	 *            the partiNo to set
	 */
	public void setPartiNo(Integer partiNo) {
		this.partiNo = partiNo;
	}

	/**
	 * @return the testTarihi
	 */
	public Date getTestTarihi() {
		return testTarihi;
	}

	/**
	 * @param testTarihi
	 *            the testTarihi to set
	 */
	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the bagliGlobalId
	 */
	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}

	/**
	 * @param bagliGlobalId
	 *            the bagliGlobalId to set
	 */
	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	/**
	 * @return the bagliTestId
	 */
	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the eklemeZamani
	 */
	public Timestamp getEklemeZamani() {
		return eklemeZamani;
	}

	/**
	 * @param eklemeZamani
	 *            the eklemeZamani to set
	 */
	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncellemeZamani
	 */
	public Timestamp getGuncellemeZamani() {
		return guncellemeZamani;
	}

	/**
	 * @param guncellemeZamani
	 *            the guncellemeZamani to set
	 */
	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the c
	 */
	public BigDecimal getC() {
		return c;
	}

	/**
	 * @param c
	 *            the c to set
	 */
	public void setC(BigDecimal c) {
		this.c = c;
	}

	/**
	 * @return the dh
	 */
	public BigDecimal getDh() {
		return dh;
	}

	/**
	 * @param dh
	 *            the dh to set
	 */
	public void setDh(BigDecimal dh) {
		this.dh = dh;
	}

	/**
	 * @return the dh1
	 */
	public BigDecimal getDh1() {
		return dh1;
	}

	/**
	 * @param dh1
	 *            the dh1 to set
	 */
	public void setDh1(BigDecimal dh1) {
		this.dh1 = dh1;
	}

	/**
	 * @return the dtg
	 */
	public BigDecimal getDtg() {
		return dtg;
	}

	/**
	 * @param dtg
	 *            the dtg to set
	 */
	public void setDtg(BigDecimal dtg) {
		this.dtg = dtg;
	}

	/**
	 * @return the kabulDegeri
	 */
	public BigDecimal getKabulDegeri() {
		return kabulDegeri;
	}

	/**
	 * @param kabulDegeri
	 *            the kabulDegeri to set
	 */
	public void setKabulDegeri(BigDecimal kabulDegeri) {
		this.kabulDegeri = kabulDegeri;
	}

	/**
	 * @return the manifacturerDh
	 */
	public BigDecimal getManifacturerDh() {
		return manifacturerDh;
	}

	/**
	 * @param manifacturerDh
	 *            the manifacturerDh to set
	 */
	public void setManifacturerDh(BigDecimal manifacturerDh) {
		this.manifacturerDh = manifacturerDh;
	}

	/**
	 * @return the manifacturerTa1
	 */
	public BigDecimal getManifacturerTa1() {
		return manifacturerTa1;
	}

	/**
	 * @param manifacturerTa1
	 *            the manifacturerTa1 to set
	 */
	public void setManifacturerTa1(BigDecimal manifacturerTa1) {
		this.manifacturerTa1 = manifacturerTa1;
	}

	/**
	 * @return the manifacturerTa2
	 */
	public BigDecimal getManifacturerTa2() {
		return manifacturerTa2;
	}

	/**
	 * @param manifacturerTa2
	 *            the manifacturerTa2 to set
	 */
	public void setManifacturerTa2(BigDecimal manifacturerTa2) {
		this.manifacturerTa2 = manifacturerTa2;
	}

	/**
	 * @return the ta1
	 */
	public BigDecimal getTa1() {
		return ta1;
	}

	/**
	 * @param ta1
	 *            the ta1 to set
	 */
	public void setTa1(BigDecimal ta1) {
		this.ta1 = ta1;
	}

	/**
	 * @return the ta2
	 */
	public BigDecimal getTa2() {
		return ta2;
	}

	/**
	 * @param ta2
	 *            the ta2 to set
	 */
	public void setTa2(BigDecimal ta2) {
		this.ta2 = ta2;
	}

	/**
	 * @return the ta3
	 */
	public BigDecimal getTa3() {
		return ta3;
	}

	/**
	 * @param ta3
	 *            the ta3 to set
	 */
	public void setTa3(BigDecimal ta3) {
		this.ta3 = ta3;
	}

	/**
	 * @return the ta4
	 */
	public BigDecimal getTa4() {
		return ta4;
	}

	/**
	 * @param ta4
	 *            the ta4 to set
	 */
	public void setTa4(BigDecimal ta4) {
		this.ta4 = ta4;
	}

	/**
	 * @return the dscSonuc
	 */
	public String getDscSonuc() {
		return dscSonuc;
	}

	/**
	 * @param dscSonuc
	 *            the dscSonuc to set
	 */
	public void setDscSonuc(String dscSonuc) {
		this.dscSonuc = dscSonuc;
	}

	/**
	 * @return the testSonuc
	 */
	public String getTestSonuc() {
		return testSonuc;
	}

	/**
	 * @param testSonuc
	 *            the testSonuc to set
	 */
	public void setTestSonuc(String testSonuc) {
		this.testSonuc = testSonuc;
	}

}