package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;

/**
 * The persistent class for the test_kaplama_kumlanmis_boru_toz_miktari_sonuc
 * database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaKumlanmisBoruTozMiktariSonuc.findAll", query = "SELECT r FROM TestKaplamaKumlanmisBoruTozMiktariSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_kaplama_kumlanmis_boru_toz_miktari_sonuc")
public class TestKaplamaKumlanmisBoruTozMiktariSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KAPLAMA_KUMLANMIS_BORU_TOZ_MIKTARI_SONUC_ID_GENERATOR", sequenceName = "TEST_KAPLAMA_KUMLANMIS_BORU_TOZ_MIKTARI_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KAPLAMA_KUMLANMIS_BORU_TOZ_MIKTARI_SONUC_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ekleme_zamani")
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	// @Column(name="global_id")
	// private Integer globalId;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	// @Column(name="isolation_test_id")
	// private Integer isolationTestId;

	// @Column(name="pipe_id")
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;

	private Boolean sonuc;

	@Column(name = "test_bolgesi_a")
	private Integer testBolgesiA;

	@Column(name = "test_bolgesi_b")
	private Integer testBolgesiB;

	@Column(name = "test_bolgesi_c")
	private Integer testBolgesiC;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi")
	private Date testTarihi;

	@Column(name = "toz_miktari_a")
	private Integer tozMiktariA;

	@Column(name = "toz_miktari_b")
	private Integer tozMiktariB;

	@Column(name = "toz_miktari_c")
	private Integer tozMiktariC;

	public TestKaplamaKumlanmisBoruTozMiktariSonuc() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	/**
	 * @return the bagliTestId
	 */
	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the bagliGlobalId
	 */
	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}

	/**
	 * @param bagliGlobalId
	 *            the bagliGlobalId to set
	 */
	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Boolean getSonuc() {
		return this.sonuc;
	}

	public void setSonuc(Boolean sonuc) {
		this.sonuc = sonuc;
	}

	public Integer getTestBolgesiA() {
		return this.testBolgesiA;
	}

	public void setTestBolgesiA(Integer testBolgesiA) {
		this.testBolgesiA = testBolgesiA;
	}

	public Integer getTestBolgesiB() {
		return this.testBolgesiB;
	}

	public void setTestBolgesiB(Integer testBolgesiB) {
		this.testBolgesiB = testBolgesiB;
	}

	public Integer getTestBolgesiC() {
		return this.testBolgesiC;
	}

	public void setTestBolgesiC(Integer testBolgesiC) {
		this.testBolgesiC = testBolgesiC;
	}

	public Date getTestTarihi() {
		return this.testTarihi;
	}

	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	public Integer getTozMiktariA() {
		return this.tozMiktariA;
	}

	public void setTozMiktariA(Integer tozMiktariA) {
		this.tozMiktariA = tozMiktariA;
	}

	public Integer getTozMiktariB() {
		return this.tozMiktariB;
	}

	public void setTozMiktariB(Integer tozMiktariB) {
		this.tozMiktariB = tozMiktariB;
	}

	public Integer getTozMiktariC() {
		return this.tozMiktariC;
	}

	public void setTozMiktariC(Integer tozMiktariC) {
		this.tozMiktariC = tozMiktariC;
	}

}