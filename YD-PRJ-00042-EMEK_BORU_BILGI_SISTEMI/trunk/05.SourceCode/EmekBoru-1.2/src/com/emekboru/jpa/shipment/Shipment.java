package com.emekboru.jpa.shipment;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.emekboru.jpa.Pipe;

/**
 * The persistent class for the shipment database table.
 * 
 */
@Entity
@Table(name = "shipment")
@NamedQuery(name = "Shipment.findAll", query = "SELECT s FROM Shipment s")
public class Shipment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SHIPMENT_ID_GENERATOR", sequenceName = "SHIPMENT_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SHIPMENT_ID_GENERATOR")
	@Column(name = "id")
	private Integer id;

	@Column(name = "status")
	private Integer status;

	@Transient
	public static final int NEWLY_CREATED = 0;
	@Transient
	public static final int PREPARED = 1;

	@Column(name = "ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "shipment_number")
	private String shipmentNumber;

	// bi-directional many-to-one association to ShipmentDriver
	// @ManyToOne
	// @JoinColumn(name = "driver_id")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "driver_id", referencedColumnName = "id", insertable = false)
	private ShipmentDriver shipmentDriver;

	// bi-directional many-to-one association to ShipmentTruck
	// @ManyToOne
	// @JoinColumn(name = "truck_id")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "truck_id", referencedColumnName = "id", insertable = false)
	private ShipmentTruck shipmentTruck;

	// bi-directional many-to-one association to ShipmentTrailer
	// @ManyToOne
	// @JoinColumn(name = "trailer_id")
	// private ShipmentTrailer shipmentTrailer;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "trailer_id", referencedColumnName = "id", insertable = false)
	private ShipmentTrailer shipmentTrailer;

	// bi-directional many-to-one association to EmployeeAddress
	@OneToMany(mappedBy = "shipment", orphanRemoval = false, cascade = CascadeType.ALL)
	private List<Pipe> pipes;

	// @OneToMany(mappedBy = "shipment", orphanRemoval = true)
	// private List<Pipe> pipes;

	public Shipment() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public ShipmentDriver getShipmentDriver() {
		return this.shipmentDriver;
	}

	public void setShipmentDriver(ShipmentDriver shipmentDriver) {
		this.shipmentDriver = shipmentDriver;
	}

	public ShipmentTruck getShipmentTruck() {
		return this.shipmentTruck;
	}

	public void setShipmentTruck(ShipmentTruck shipmentTruck) {
		this.shipmentTruck = shipmentTruck;
	}

	public ShipmentTrailer getShipmentTrailer() {
		return this.shipmentTrailer;
	}

	public void setShipmentTrailer(ShipmentTrailer shipmentTrailer) {
		this.shipmentTrailer = shipmentTrailer;
	}

	public List<Pipe> getPipes() {
		return pipes;
	}

	public void setPipes(List<Pipe> pipes) {
		this.pipes = pipes;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getShipmentNumber() {
		return shipmentNumber;
	}

	public void setShipmentNumber(String shipmentNumber) {
		this.shipmentNumber = shipmentNumber;
	}

}