package com.emekboru.jpa.kaplamagirditest;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the test_hammadde_girdi_epoksi_toz_eni1 database
 * table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestHammaddeGirdiEpoksiTozEni1.findAll", query = "SELECT r FROM TestHammaddeGirdiEpoksiTozEni1 r WHERE r.coatRawMaterial.coatMaterialId=:prmMaterialId") })
@Table(name = "test_hammadde_girdi_epoksi_toz_eni1")
public class TestHammaddeGirdiEpoksiTozEni1 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_HAMMADDE_GIRDI_EPOKSI_TOZ_ENI1_ID_GENERATOR", sequenceName = "TEST_HAMMADDE_GIRDI_EPOKSI_TOZ_ENI1_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_HAMMADDE_GIRDI_EPOKSI_TOZ_ENI1_ID_GENERATOR")
	@Column(name = "ID")
	private Integer id;

	// @Column(name = "coat_material_id")
	// private Integer coatMaterialId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coat_material_id", referencedColumnName = "coat_material_id", insertable = false)
	private CoatRawMaterial coatRawMaterial;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "ilk_kutle")
	private String ilkKutle;

	@Column(name = "nem_icerigi")
	private String nemIcerigi;

	@Column(name = "numune_kalibi_kutlesi")
	private String numuneKalibiKutlesi;

	@Column(name = "parti_no")
	private String partiNo;

	@Column(name = "son_kutle")
	private String sonKutle;

	private Boolean sonuc;

	private String standard;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi", nullable = false)
	private Date testTarihi;

	@Column(name = "uretici_spec")
	private String ureticiSpec;
	
	@Column(name = "rapor_no")
	private String raporNo;

	public TestHammaddeGirdiEpoksiTozEni1() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Boolean getSonuc() {
		return this.sonuc;
	}

	public void setSonuc(Boolean sonuc) {
		this.sonuc = sonuc;
	}

	public String getStandard() {
		return this.standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getUreticiSpec() {
		return this.ureticiSpec;
	}

	public void setUreticiSpec(String ureticiSpec) {
		this.ureticiSpec = ureticiSpec;
	}

	/**
	 * @return the coatRawMaterial
	 */
	public CoatRawMaterial getCoatRawMaterial() {
		return coatRawMaterial;
	}

	/**
	 * @param coatRawMaterial
	 *            the coatRawMaterial to set
	 */
	public void setCoatRawMaterial(CoatRawMaterial coatRawMaterial) {
		this.coatRawMaterial = coatRawMaterial;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the testTarihi
	 */
	public Date getTestTarihi() {
		return testTarihi;
	}

	/**
	 * @param testTarihi
	 *            the testTarihi to set
	 */
	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	/**
	 * @return the ilkKutle
	 */
	public String getIlkKutle() {
		return ilkKutle;
	}

	/**
	 * @param ilkKutle
	 *            the ilkKutle to set
	 */
	public void setIlkKutle(String ilkKutle) {
		this.ilkKutle = ilkKutle;
	}

	/**
	 * @return the nemIcerigi
	 */
	public String getNemIcerigi() {
		return nemIcerigi;
	}

	/**
	 * @param nemIcerigi
	 *            the nemIcerigi to set
	 */
	public void setNemIcerigi(String nemIcerigi) {
		this.nemIcerigi = nemIcerigi;
	}

	/**
	 * @return the numuneKalibiKutlesi
	 */
	public String getNumuneKalibiKutlesi() {
		return numuneKalibiKutlesi;
	}

	/**
	 * @param numuneKalibiKutlesi
	 *            the numuneKalibiKutlesi to set
	 */
	public void setNumuneKalibiKutlesi(String numuneKalibiKutlesi) {
		this.numuneKalibiKutlesi = numuneKalibiKutlesi;
	}

	/**
	 * @return the partiNo
	 */
	public String getPartiNo() {
		return partiNo;
	}

	/**
	 * @param partiNo
	 *            the partiNo to set
	 */
	public void setPartiNo(String partiNo) {
		this.partiNo = partiNo;
	}

	/**
	 * @return the sonKutle
	 */
	public String getSonKutle() {
		return sonKutle;
	}

	/**
	 * @param sonKutle
	 *            the sonKutle to set
	 */
	public void setSonKutle(String sonKutle) {
		this.sonKutle = sonKutle;
	}

	/**
	 * @return the raporNo
	 */
	public String getRaporNo() {
		return raporNo;
	}

	/**
	 * @param raporNo the raporNo to set
	 */
	public void setRaporNo(String raporNo) {
		this.raporNo = raporNo;
	}

}