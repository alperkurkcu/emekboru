package com.emekboru.jpa.sales.order;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.emekboru.jpa.AgirlikTestsSpec;
import com.emekboru.jpa.BoruBoyutsalKontrolTolerans;
import com.emekboru.jpa.BoruKaliteKaplamaTurleri;
import com.emekboru.jpa.BukmeTestsSpec;
import com.emekboru.jpa.CekmeTestsSpec;
import com.emekboru.jpa.CentikDarbeTestsSpec;
import com.emekboru.jpa.ChemicalRequirement;
import com.emekboru.jpa.DestructiveTestsSpec;
import com.emekboru.jpa.HidrostaticTestsSpec;
import com.emekboru.jpa.KaynakMakroTestSpec;
import com.emekboru.jpa.KaynakSertlikTestsSpec;
import com.emekboru.jpa.NondestructiveTestsSpec;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.PlannedIsolation;
import com.emekboru.jpa.QualitySpec;
import com.emekboru.jpa.isemri.IsEmriBetonKaplama;
import com.emekboru.jpa.isemri.IsEmriDisKumlama;
import com.emekboru.jpa.isemri.IsEmriEpoksiKaplama;
import com.emekboru.jpa.isemri.IsEmriIcKumlama;
import com.emekboru.jpa.isemri.IsEmriPolietilenKaplama;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatestspecs.KaliteKaplamaMarkalama;
import com.emekboru.jpa.kaplamatestspecs.KaplamaKaliteCutBackKoruma;
import com.emekboru.jpa.kaplamatestspecs.KaplamaKaliteKaynakAgziKoruma;
import com.emekboru.jpa.rulo.Rulo;

@NamedQueries({
		@NamedQuery(name = "totalSalesItem", query = "SELECT COUNT(u) FROM SalesItem u"),
		@NamedQuery(name = "SalesItem.findAll", query = "SELECT u FROM SalesItem u order by u.proposal.proposalId, u.itemNo"),
		@NamedQuery(name = "SalesItem.findAllOrdered", query = "SELECT u FROM SalesItem u, SalesOrder o where u.proposal=o.proposal order by u.proposal.proposalId ASC, u.itemId ASC"),
		@NamedQuery(name = "SalesItem.findItemNo", query = "SELECT u FROM SalesItem u where u.proposal.proposalId=:prmProposalId"),
		@NamedQuery(name = "SalesItem.totalAll", query = "SELECT COUNT(o) FROM SalesItem o") })
@Entity
@Table(name = "sales_item")
public class SalesItem implements Serializable, Cloneable {

	private static final long serialVersionUID = -1480125279981328287L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_ITEM_GENERATOR")
	@SequenceGenerator(name = "SALES_ITEM_GENERATOR", sequenceName = "SALES_ITEM_SEQUENCE", allocationSize = 1)
	@Column(name = "item_id")
	private int itemId;

	@Column(name = "item_no")
	private String itemNo;

	@Column(name = "diameter")
	private double diameter;

	@Column(name = "thickness")
	private double thickness;

	@Column(name = "quality")
	private String quality;

	@Column(name = "pipe_length")
	private double pipeLength;

	@Column(name = "total_length_m")
	private double totalLengthM;

	@Column(name = "unit_weight")
	private double unitWeight;

	@Column(name = "total_tonnage")
	private double totalTonnage;

	@Column(name = "manufacturing_standard")
	private String manufacturingStandard;

	@Column(name = "isolation_info")
	private String isolation;

	@Column(name = "transportation")
	private String transportation;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "proposal", referencedColumnName = "proposal_id")
	private SalesProposal proposal;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "currency_unit", referencedColumnName = "currency_id")
	private SalesCurrency currencyUnit;

	@Column(name = "parity_dollar")
	private double parityDollar;

	@Column(name = "parity_euro")
	private double parityEuro;

	@Column(name = "parity_euro_dollar")
	private double parityEuroDollar;

	@Column(name = "plate")
	private double plate;

	@Column(name = "plate_transportation")
	private double plateTransportation;

	@Column(name = "wastage_percentage")
	private double wastagePercentage;

	@Column(name = "labor")
	private double labor;

	@Column(name = "covering")
	private double covering;

	@Column(name = "transportation_unit_price")
	private double transportationUnitPrice;

	@Column(name = "transportation_type")
	private Integer transportationType;

	@Column(name = "transportation_vehicle")
	private Integer transportationVehicle;

	@Column(name = "transportation_type_1")
	private double transportationType1;

	@Column(name = "transportation_type_2")
	private double transportationType2;

	@Column(name = "transportation_type_3")
	private double transportationType3;

	@Column(name = "transportation_type_4")
	private double transportationType4;

	@Column(name = "transportation_type_5")
	private double transportationType5;

	@Column(name = "transportation_type_6")
	private double transportationType6;

	@Column(name = "transportation_type_7")
	private double transportationType7;

	@Column(name = "transportation_type_8")
	private double transportationType8;

	@Column(name = "unit_price_m")
	private double unitPriceM;

	@Column(name = "unit_price_ton")
	private double unitPriceTon;

	@Column(name = "commission")
	private double commission;

	@Column(name = "commission_price")
	private double commissionPrice;

	@Column(name = "insurance")
	private double insurance;

	@Column(name = "insurance_price")
	private double insurancePrice;

	@Column(name = "total_price")
	private double totalPrice;

	@Column(name = "total_price_tl")
	private double totalPriceTL;

	@Column(name = "total_price_euro")
	private double totalPriceEuro;

	@Transient
	private String unitPriceString;

	@Transient
	private String transportationPriceString;

	// entegrasyon
	@OneToOne(mappedBy = "salesItem", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private QualitySpec qualitySpec;

	@OneToOne(mappedBy = "salesItem", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private BoruBoyutsalKontrolTolerans boyutsalKontrol;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesItem", fetch = FetchType.LAZY)
	private List<DestructiveTestsSpec> destructiveTestsSpecs;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesItem", fetch = FetchType.LAZY)
	private List<NondestructiveTestsSpec> nondestructiveTestsSpecs;

	@OneToOne(mappedBy = "salesItem", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	private ChemicalRequirement chemicalRequirements;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "salesItem", cascade = CascadeType.PERSIST, orphanRemoval = true)
	private List<PlannedIsolation> plannedIsolation;

	// coating
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesItem")
	private List<IsolationTestDefinition> isolationTestDefinitions;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesItem")
	private List<AgirlikTestsSpec> agirlikTestsSpecs;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesItem")
	private List<BukmeTestsSpec> bukmeTestsSpecs;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesItem")
	private List<CekmeTestsSpec> cekmeTestsSpecs;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesItem")
	private List<CentikDarbeTestsSpec> centikDarbeTestsSpecs;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesItem")
	private List<HidrostaticTestsSpec> hidrostaticTestsSpecs;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesItem")
	private List<KaynakMakroTestSpec> kaynakMakroTestSpecs;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesItem")
	private List<KaynakSertlikTestsSpec> kaynakSertlikTestsSpecs;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "selectedSalesItemPipeRulo")
	private Rulo selectedRulo;

	@Transient
	private PlannedIsolation internalIsolation;

	@Transient
	private PlannedIsolation externalIsolation;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "salesItem")
	private List<Pipe> pipes;

	@Column(name = "polyethylene_type")
	private String polyethyleneType;

	@Column(name = "third_party")
	private String thirdParty;

	@Column(name = "kaynak_agzi_koruma")
	private String kaynakAgziKoruma;

	@Column(name = "roll_plate_inventory_tonnage")
	private double rollPlateInventoryTonnage;

	@Column(name = "roll_plate_order_tonnage ")
	private double rollPlateOrderTonnage;

	@Temporal(TemporalType.DATE)
	@Column(name = "roll_plate_order_approximate_arrival_date")
	private Date rollPlateOrderApproximateArrivalDate;

	@Column(name = "material_quality")
	private String materialQuality;

	@Column(name = "istavroz")
	private String istavroz;

	@Column(name = "total_length_tolerance")
	private String totalLengthTolerance;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "delivery_type", referencedColumnName = "delivery_id")
	private SalesDelivery deliveryType;

	@Column(name = "packing_list_unit_weight")
	private String packingListUnitWeight;

	@Column(name = "pipe_type")
	private String pipeType;

	@Column(name = "internal_covering_type")
	private String internalCoveringType;

	@Column(name = "external_covering_type")
	private String externalCoveringType;

	@Column(name = "internal_covering_standard")
	private String internalCoveringStandard;

	@Column(name = "external_covering_standard")
	private String externalCoveringStandard;

	@Column(name = "pipe_hole")
	private String pipeHole;

	@Column(name = "marking")
	private String marking;

	@Temporal(TemporalType.DATE)
	@Column(name = "delivery_date")
	private Date deliveryDate;

	@Column(name = "duration")
	private String duration;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "item_id", referencedColumnName = "sales_item_id", insertable = false, updatable = false)
	private IsEmriIcKumlama isEmriIcKumlama;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "item_id", referencedColumnName = "sales_item_id", insertable = false, updatable = false)
	private IsEmriDisKumlama isEmriDisKumlama;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "item_id", referencedColumnName = "sales_item_id", insertable = false, updatable = false)
	private IsEmriPolietilenKaplama isEmriPolietilenKaplama;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "item_id", referencedColumnName = "sales_item_id", insertable = false, updatable = false)
	private IsEmriEpoksiKaplama isEmriEpoksiKaplama;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "item_id", referencedColumnName = "sales_item_id", insertable = false, updatable = false)
	private IsEmriBetonKaplama isEmriBetonKaplama;

	@Column(name = "kazik_boy_tolerans")
	private String kazikBoyTolerans;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "item_id", referencedColumnName = "sales_item_id", insertable = false, updatable = false)
	private BoruKaliteKaplamaTurleri boruKaliteKaplamaTurleri;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "item_id", referencedColumnName = "sales_item_id", insertable = false, updatable = false)
	private KaliteKaplamaMarkalama kaliteKaplamaMarkalama;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "item_id", referencedColumnName = "sales_item_id", insertable = false, updatable = false)
	private KaplamaKaliteKaynakAgziKoruma kaplamaKaliteKaynakAgziKoruma;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "item_id", referencedColumnName = "sales_item_id", insertable = false, updatable = false)
	private KaplamaKaliteCutBackKoruma kaplamaKaliteCutBackKoruma;

	// bi-directional many-to-one association to EmployeeAddress
	@OneToMany(mappedBy = "reservedSalesItem", orphanRemoval = true, cascade = CascadeType.ALL)
	private List<Rulo> reservedRulos;

	@Column(name = "extra_price")
	private double extraPrice;

	@Column(name = "boru_boy_tolerans")
	private String boruBoyTolerans;

	@Column(name = "fca_tir_tonaj")
	private double fcaTirTonaj;

	@Column(name = "fca_tir_adedi")
	private double fcaTirAdedi;

	@Column(name = "fca_tir_ucreti")
	private double fcaTirUcreti;

	@Column(name = "fca_birim_ucret")
	private double fcaBirimUcret;

	@Column(name = "fca_tir_kapasite")
	private double fcaTirKapasite;

	@Column(name = "fob_konteyner_ucreti")
	private double fobKonteynerUcreti;

	@Column(name = "fob_liman_ucreti")
	private double fobLimanUcreti;

	@Column(name = "fob_birim_ucret")
	private double fobBirimUcret;

	@Column(name = "fob_konteyner_sayisi")
	private double fobKonteynerSayisi;

	@Column(name = "fob_ucreti")
	private double fobUcreti;

	@Column(name = "cfr_konteyner_ucreti")
	private double cfrKonteynerUcreti;

	@Column(name = "cfr_birim_ucret")
	private double cfrBirimUcret;

	@Column(name = "cfr_konteyner_sayisi")
	private double cfrKonteynerSayisi;

	@Column(name = "cfr_ucreti")
	private double cfrUcreti;

	@Column(name = "cfr_navlun_ucreti")
	private double cfrNavlunUcreti;

	@Column(name = "cfr_navlun_metre_ucreti")
	private double cfrNavlunMetreUcreti;

	@Column(name = "ship_carry_type")
	private Integer shipCarryType;

	public SalesItem() {
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public int getItemId() {
		return itemId;
	}

	public String getItemNo() {
		return itemNo;
	}

	public void setItemNo(String itemNo) {
		this.itemNo = itemNo;
	}

	public double getDiameter() {
		return diameter;
	}

	public void setDiameter(double diameter) {
		this.diameter = diameter;
	}

	public double getThickness() {
		return thickness;
	}

	public void setThickness(double thickness) {
		this.thickness = thickness;
	}

	public String getQuality() {
		return quality;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	public double getPipeLength() {
		return pipeLength;
	}

	public void setPipeLength(double pipeLength) {
		this.pipeLength = pipeLength;
	}

	public String getManufacturingStandard() {
		return manufacturingStandard;
	}

	public void setManufacturingStandard(String manufacturingStandard) {
		this.manufacturingStandard = manufacturingStandard;
	}

	public String getIsolation() {
		return isolation;
	}

	public void setIsolation(String isolation) {
		this.isolation = isolation;
	}

	public double getTotalLengthM() {
		return totalLengthM;
	}

	public void setTotalLengthM(double totalLengthM) {
		this.totalLengthM = totalLengthM;
	}

	public double getUnitWeight() {
		return unitWeight;
	}

	public void setUnitWeight(double unitWeight) {
		this.unitWeight = unitWeight;
	}

	public double getTotalTonnage() {
		return totalTonnage;
	}

	public void setTotalTonnage(double totalTonnage) {
		this.totalTonnage = totalTonnage;
	}

	public String getTransportation() {
		return transportation;
	}

	public void setTransportation(String transportation) {
		this.transportation = transportation;
	}

	public SalesProposal getProposal() {
		return proposal;
	}

	public void setProposal(SalesProposal proposal) {
		this.proposal = proposal;
	}

	public SalesCurrency getCurrencyUnit() {
		return currencyUnit;
	}

	public void setCurrencyUnit(SalesCurrency currencyUnit) {
		this.currencyUnit = currencyUnit;
	}

	public double getParityDollar() {
		return parityDollar;
	}

	public void setParityDollar(double parityDollar) {
		this.parityDollar = parityDollar;
	}

	public double getParityEuro() {
		return parityEuro;
	}

	public void setParityEuro(double parityEuro) {
		this.parityEuro = parityEuro;
	}

	public double getPlate() {
		return plate;
	}

	public void setPlate(double plate) {
		this.plate = plate;
	}

	public double getPlateTransportation() {
		return plateTransportation;
	}

	public void setPlateTransportation(double plateTransportation) {
		this.plateTransportation = plateTransportation;
	}

	public double getWastagePercentage() {
		return wastagePercentage;
	}

	public void setWastagePercentage(double wastagePercentage) {
		this.wastagePercentage = wastagePercentage;
	}

	public double getLabor() {
		return labor;
	}

	public void setLabor(double labor) {
		this.labor = labor;
	}

	public double getCovering() {
		return covering;
	}

	public void setCovering(double covering) {
		this.covering = covering;
	}

	public double getTransportationUnitPrice() {
		return transportationUnitPrice;
	}

	public void setTransportationUnitPrice(double transportationUnitPrice) {
		this.transportationUnitPrice = transportationUnitPrice;
	}

	public double getTransportationType1() {
		return transportationType1;
	}

	public void setTransportationType1(double transportationType1) {
		this.transportationType1 = transportationType1;
	}

	public double getTransportationType2() {
		return transportationType2;
	}

	public void setTransportationType2(double transportationType2) {
		this.transportationType2 = transportationType2;
	}

	public double getTransportationType3() {
		return transportationType3;
	}

	public void setTransportationType3(double transportationType3) {
		this.transportationType3 = transportationType3;
	}

	public double getTransportationType4() {
		return transportationType4;
	}

	public void setTransportationType4(double transportationType4) {
		this.transportationType4 = transportationType4;
	}

	public double getTransportationType5() {
		return transportationType5;
	}

	public void setTransportationType5(double transportationType5) {
		this.transportationType5 = transportationType5;
	}

	public double getTransportationType6() {
		return transportationType6;
	}

	public void setTransportationType6(double transportationType6) {
		this.transportationType6 = transportationType6;
	}

	public double getTransportationType7() {
		return transportationType7;
	}

	public void setTransportationType7(double transportationType7) {
		this.transportationType7 = transportationType7;
	}

	public double getTransportationType8() {
		return transportationType8;
	}

	public void setTransportationType8(double transportationType8) {
		this.transportationType8 = transportationType8;
	}

	public double getUnitPriceM() {
		return unitPriceM;
	}

	public void setUnitPriceM(double unitPriceM) {
		this.unitPriceM = unitPriceM;
	}

	public double getUnitPriceTon() {
		return unitPriceTon;
	}

	public void setUnitPriceTon(double unitPriceTon) {
		this.unitPriceTon = unitPriceTon;
	}

	public double getCommission() {
		return commission;
	}

	public void setCommission(double commission) {
		this.commission = commission;
	}

	public double getCommissionPrice() {
		return commissionPrice;
	}

	public void setCommissionPrice(double commissionPrice) {
		this.commissionPrice = commissionPrice;
	}

	public double getInsurance() {
		return insurance;
	}

	public void setInsurance(double insurance) {
		this.insurance = insurance;
	}

	public double getInsurancePrice() {
		return insurancePrice;
	}

	public void setInsurancePrice(double insurancePrice) {
		this.insurancePrice = insurancePrice;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getUnitPriceString() {
		try {
			unitPriceString = String.valueOf(this.getUnitPriceM()).replace(".",
					",");
		} catch (Exception e) {
			unitPriceString = "0,0";
		}
		return unitPriceString;
	}

	public void setUnitPriceString(String unitPriceString) {
		this.unitPriceString = unitPriceString;
	}

	public String getTransportationPriceString() {
		try {
			transportationPriceString = String.valueOf(
					this.getTransportationUnitPrice()).replace(".", ",");
		} catch (Exception e) {
			transportationPriceString = "0,0";
		}
		return transportationPriceString;
	}

	public void setTransportationPriceString(String transportationPriceString) {
		this.transportationPriceString = transportationPriceString;
	}

	public QualitySpec getQualitySpec() {
		return qualitySpec;
	}

	public void setQualitySpec(QualitySpec qualitySpec) {
		this.qualitySpec = qualitySpec;
	}

	public BoruBoyutsalKontrolTolerans getBoyutsalKontrol() {
		return boyutsalKontrol;
	}

	public void setBoyutsalKontrol(BoruBoyutsalKontrolTolerans boyutsalKontrol) {
		this.boyutsalKontrol = boyutsalKontrol;
	}

	public List<DestructiveTestsSpec> getDestructiveTestsSpecs() {
		return destructiveTestsSpecs;
	}

	public void setDestructiveTestsSpecs(
			List<DestructiveTestsSpec> destructiveTestsSpecs) {
		this.destructiveTestsSpecs = destructiveTestsSpecs;
	}

	public List<NondestructiveTestsSpec> getNondestructiveTestsSpecs() {
		return nondestructiveTestsSpecs;
	}

	public void setNondestructiveTestsSpecs(
			List<NondestructiveTestsSpec> nondestructiveTestsSpecs) {
		this.nondestructiveTestsSpecs = nondestructiveTestsSpecs;
	}

	public ChemicalRequirement getChemicalRequirements() {
		return chemicalRequirements;
	}

	public void setChemicalRequirements(ChemicalRequirement chemicalRequirements) {
		this.chemicalRequirements = chemicalRequirements;
	}

	public List<PlannedIsolation> getPlannedIsolation() {
		return plannedIsolation;
	}

	public void setPlannedIsolation(List<PlannedIsolation> plannedIsolation) {
		this.plannedIsolation = plannedIsolation;
	}

	public List<IsolationTestDefinition> getIsolationTestDefinitions() {
		return isolationTestDefinitions;
	}

	public void setIsolationTestDefinitions(
			List<IsolationTestDefinition> isolationTestDefinitions) {
		this.isolationTestDefinitions = isolationTestDefinitions;
	}

	public List<AgirlikTestsSpec> getAgirlikTestsSpecs() {
		return agirlikTestsSpecs;
	}

	public void setAgirlikTestsSpecs(List<AgirlikTestsSpec> agirlikTestsSpecs) {
		this.agirlikTestsSpecs = agirlikTestsSpecs;
	}

	public List<BukmeTestsSpec> getBukmeTestsSpecs() {
		return bukmeTestsSpecs;
	}

	public void setBukmeTestsSpecs(List<BukmeTestsSpec> bukmeTestsSpecs) {
		this.bukmeTestsSpecs = bukmeTestsSpecs;
	}

	public List<CekmeTestsSpec> getCekmeTestsSpecs() {
		return cekmeTestsSpecs;
	}

	public void setCekmeTestsSpecs(List<CekmeTestsSpec> cekmeTestsSpecs) {
		this.cekmeTestsSpecs = cekmeTestsSpecs;
	}

	public List<CentikDarbeTestsSpec> getCentikDarbeTestsSpecs() {
		return centikDarbeTestsSpecs;
	}

	public void setCentikDarbeTestsSpecs(
			List<CentikDarbeTestsSpec> centikDarbeTestsSpecs) {
		this.centikDarbeTestsSpecs = centikDarbeTestsSpecs;
	}

	public List<HidrostaticTestsSpec> getHidrostaticTestsSpecs() {
		return hidrostaticTestsSpecs;
	}

	public void setHidrostaticTestsSpecs(
			List<HidrostaticTestsSpec> hidrostaticTestsSpecs) {
		this.hidrostaticTestsSpecs = hidrostaticTestsSpecs;
	}

	public List<KaynakMakroTestSpec> getKaynakMakroTestSpecs() {
		return kaynakMakroTestSpecs;
	}

	public void setKaynakMakroTestSpecs(
			List<KaynakMakroTestSpec> kaynakMakroTestSpecs) {
		this.kaynakMakroTestSpecs = kaynakMakroTestSpecs;
	}

	public List<KaynakSertlikTestsSpec> getKaynakSertlikTestsSpecs() {
		return kaynakSertlikTestsSpecs;
	}

	public void setKaynakSertlikTestsSpecs(
			List<KaynakSertlikTestsSpec> kaynakSertlikTestsSpecs) {
		this.kaynakSertlikTestsSpecs = kaynakSertlikTestsSpecs;
	}

	public Rulo getSelectedRulo() {
		return selectedRulo;
	}

	public void setSelectedRulo(Rulo selectedRulo) {
		this.selectedRulo = selectedRulo;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public PlannedIsolation getInternalIsolation() {
		return internalIsolation;
	}

	public void setInternalIsolation(PlannedIsolation internalIsolation) {
		this.internalIsolation = internalIsolation;
	}

	public PlannedIsolation getExternalIsolation() {
		return externalIsolation;
	}

	public void setExternalIsolation(PlannedIsolation externalIsolation) {
		this.externalIsolation = externalIsolation;
	}

	public List<Pipe> getPipes() {
		return pipes;
	}

	public void setPipes(List<Pipe> pipes) {
		this.pipes = pipes;
	}

	public String getPolyethyleneType() {
		return polyethyleneType;
	}

	public void setPolyethyleneType(String polyethyleneType) {
		this.polyethyleneType = polyethyleneType;
	}

	public String getThirdParty() {
		return thirdParty;
	}

	public void setThirdParty(String thirdParty) {
		this.thirdParty = thirdParty;
	}

	public String getKaynakAgziKoruma() {
		return kaynakAgziKoruma;
	}

	public void setKaynakAgziKoruma(String kaynakAgziKoruma) {
		this.kaynakAgziKoruma = kaynakAgziKoruma;
	}

	public double getRollPlateInventoryTonnage() {
		return rollPlateInventoryTonnage;
	}

	public void setRollPlateInventoryTonnage(double rollPlateInventoryTonnage) {
		this.rollPlateInventoryTonnage = rollPlateInventoryTonnage;
	}

	public double getRollPlateOrderTonnage() {
		return rollPlateOrderTonnage;
	}

	public void setRollPlateOrderTonnage(double rollPlateOrderTonnage) {
		this.rollPlateOrderTonnage = rollPlateOrderTonnage;
	}

	public Date getRollPlateOrderApproximateArrivalDate() {
		return rollPlateOrderApproximateArrivalDate;
	}

	public void setRollPlateOrderApproximateArrivalDate(
			Date rollPlateOrderApproximateArrivalDate) {
		this.rollPlateOrderApproximateArrivalDate = rollPlateOrderApproximateArrivalDate;
	}

	public String getMaterialQuality() {
		return materialQuality;
	}

	public void setMaterialQuality(String materialQuality) {
		this.materialQuality = materialQuality;
	}

	public String getIstavroz() {
		return istavroz;
	}

	public void setIstavroz(String istavroz) {
		this.istavroz = istavroz;
	}

	public String getTotalLengthTolerance() {
		return totalLengthTolerance;
	}

	public void setTotalLengthTolerance(String totalLengthTolerance) {
		this.totalLengthTolerance = totalLengthTolerance;
	}

	public SalesDelivery getDeliveryType() {
		return deliveryType;
	}

	public void setDeliveryType(SalesDelivery deliveryType) {
		this.deliveryType = deliveryType;
	}

	public String getPackingListUnitWeight() {
		return packingListUnitWeight;
	}

	public void setPackingListUnitWeight(String packingListUnitWeight) {
		this.packingListUnitWeight = packingListUnitWeight;
	}

	public String getPipeType() {
		return pipeType;
	}

	public void setPipeType(String pipeType) {
		this.pipeType = pipeType;
	}

	public String getInternalCoveringType() {
		return internalCoveringType;
	}

	public void setInternalCoveringType(String internalCoveringType) {
		this.internalCoveringType = internalCoveringType;
	}

	public String getExternalCoveringType() {
		return externalCoveringType;
	}

	public void setExternalCoveringType(String externalCoveringType) {
		this.externalCoveringType = externalCoveringType;
	}

	public String getInternalCoveringStandard() {
		return internalCoveringStandard;
	}

	public void setInternalCoveringStandard(String internalStandard) {
		this.internalCoveringStandard = internalStandard;
	}

	public String getExternalCoveringStandard() {
		return externalCoveringStandard;
	}

	public void setExternalCoveringStandard(String externalStandard) {
		this.externalCoveringStandard = externalStandard;
	}

	public String getPipeHole() {
		return pipeHole;
	}

	public void setPipeHole(String pipeHole) {
		this.pipeHole = pipeHole;
	}

	public String getMarking() {
		return marking;
	}

	public void setMarking(String marking) {
		this.marking = marking;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public IsEmriIcKumlama getIsEmriIcKumlama() {
		return isEmriIcKumlama;
	}

	public void setIsEmriIcKumlama(IsEmriIcKumlama isEmriIcKumlama) {
		this.isEmriIcKumlama = isEmriIcKumlama;
	}

	public IsEmriDisKumlama getIsEmriDisKumlama() {
		return isEmriDisKumlama;
	}

	public void setIsEmriDisKumlama(IsEmriDisKumlama isEmriDisKumlama) {
		this.isEmriDisKumlama = isEmriDisKumlama;
	}

	public IsEmriPolietilenKaplama getIsEmriPolietilenKaplama() {
		return isEmriPolietilenKaplama;
	}

	public void setIsEmriPolietilenKaplama(
			IsEmriPolietilenKaplama isEmriPolietilenKaplama) {
		this.isEmriPolietilenKaplama = isEmriPolietilenKaplama;
	}

	public IsEmriEpoksiKaplama getIsEmriEpoksiKaplama() {
		return isEmriEpoksiKaplama;
	}

	public void setIsEmriEpoksiKaplama(IsEmriEpoksiKaplama isEmriEpoksiKaplama) {
		this.isEmriEpoksiKaplama = isEmriEpoksiKaplama;
	}

	public IsEmriBetonKaplama getIsEmriBetonKaplama() {
		return isEmriBetonKaplama;
	}

	public void setIsEmriBetonKaplama(IsEmriBetonKaplama isEmriBetonKaplama) {
		this.isEmriBetonKaplama = isEmriBetonKaplama;
	}

	public String getKazikBoyTolerans() {
		return kazikBoyTolerans;
	}

	public void setKazikBoyTolerans(String kazikBoyTolerans) {
		this.kazikBoyTolerans = kazikBoyTolerans;
	}

	public BoruKaliteKaplamaTurleri getBoruKaliteKaplamaTurleri() {
		return boruKaliteKaplamaTurleri;
	}

	public void setBoruKaliteKaplamaTurleri(
			BoruKaliteKaplamaTurleri boruKaliteKaplamaTurleri) {
		this.boruKaliteKaplamaTurleri = boruKaliteKaplamaTurleri;
	}

	/**
	 * @return the kaliteKaplamaMarkalama
	 */
	public KaliteKaplamaMarkalama getKaliteKaplamaMarkalama() {
		return kaliteKaplamaMarkalama;
	}

	/**
	 * @param kaliteKaplamaMarkalama
	 *            the kaliteKaplamaMarkalama to set
	 */
	public void setKaliteKaplamaMarkalama(
			KaliteKaplamaMarkalama kaliteKaplamaMarkalama) {
		this.kaliteKaplamaMarkalama = kaliteKaplamaMarkalama;
	}

	/**
	 * @return the kaplamaKaliteKaynakAgziKoruma
	 */
	public KaplamaKaliteKaynakAgziKoruma getKaplamaKaliteKaynakAgziKoruma() {
		return kaplamaKaliteKaynakAgziKoruma;
	}

	/**
	 * @param kaplamaKaliteKaynakAgziKoruma
	 *            the kaplamaKaliteKaynakAgziKoruma to set
	 */
	public void setKaplamaKaliteKaynakAgziKoruma(
			KaplamaKaliteKaynakAgziKoruma kaplamaKaliteKaynakAgziKoruma) {
		this.kaplamaKaliteKaynakAgziKoruma = kaplamaKaliteKaynakAgziKoruma;
	}

	/**
	 * @return the kaplamaKaliteCutBackKoruma
	 */
	public KaplamaKaliteCutBackKoruma getKaplamaKaliteCutBackKoruma() {
		return kaplamaKaliteCutBackKoruma;
	}

	/**
	 * @param kaplamaKaliteCutBackKoruma
	 *            the kaplamaKaliteCutBackKoruma to set
	 */
	public void setKaplamaKaliteCutBackKoruma(
			KaplamaKaliteCutBackKoruma kaplamaKaliteCutBackKoruma) {
		this.kaplamaKaliteCutBackKoruma = kaplamaKaliteCutBackKoruma;
	}

	public double getParityEuroDollar() {
		return parityEuroDollar;
	}

	public void setParityEuroDollar(double parityEuroDollar) {
		this.parityEuroDollar = parityEuroDollar;
	}

	public double getTotalPriceTL() {
		return totalPriceTL;
	}

	public void setTotalPriceTL(double totalPriceTL) {
		this.totalPriceTL = totalPriceTL;
	}

	public double getTotalPriceEuro() {
		return totalPriceEuro;
	}

	public void setTotalPriceEuro(double totalPriceEuro) {
		this.totalPriceEuro = totalPriceEuro;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

	public List<Rulo> getReservedRulos() {
		return reservedRulos;
	}

	public void setReservedRulos(List<Rulo> reservedRulos) {
		this.reservedRulos = reservedRulos;
	}

	public double getExtraPrice() {
		return extraPrice;
	}

	public void setExtraPrice(double extraPrice) {
		this.extraPrice = extraPrice;
	}

	/**
	 * @return the boruBoyTolerans
	 */
	public String getBoruBoyTolerans() {
		return boruBoyTolerans;
	}

	/**
	 * @param boruBoyTolerans
	 *            the boruBoyTolerans to set
	 */
	public void setBoruBoyTolerans(String boruBoyTolerans) {
		this.boruBoyTolerans = boruBoyTolerans;
	}

	/**
	 * @return the transportationType
	 */
	public Integer getTransportationType() {
		return transportationType;
	}

	/**
	 * @param transportationType
	 *            the transportationType to set
	 */
	public void setTransportationType(Integer transportationType) {
		this.transportationType = transportationType;
	}

	/**
	 * @return the fcaTirTonaj
	 */
	public double getFcaTirTonaj() {
		return fcaTirTonaj;
	}

	/**
	 * @param fcaTirTonaj
	 *            the fcaTirTonaj to set
	 */
	public void setFcaTirTonaj(double fcaTirTonaj) {
		this.fcaTirTonaj = fcaTirTonaj;
	}

	/**
	 * @return the fcaTirAdedi
	 */
	public double getFcaTirAdedi() {
		return fcaTirAdedi;
	}

	/**
	 * @param fcaTirAdedi
	 *            the fcaTirAdedi to set
	 */
	public void setFcaTirAdedi(double fcaTirAdedi) {
		this.fcaTirAdedi = fcaTirAdedi;
	}

	/**
	 * @return the fcaTirUcreti
	 */
	public double getFcaTirUcreti() {
		return fcaTirUcreti;
	}

	/**
	 * @param fcaTirUcreti
	 *            the fcaTirUcreti to set
	 */
	public void setFcaTirUcreti(double fcaTirUcreti) {
		this.fcaTirUcreti = fcaTirUcreti;
	}

	/**
	 * @return the fcaBirimUcret
	 */
	public double getFcaBirimUcret() {
		return fcaBirimUcret;
	}

	/**
	 * @param fcaBirimUcret
	 *            the fcaBirimUcret to set
	 */
	public void setFcaBirimUcret(double fcaBirimUcret) {
		this.fcaBirimUcret = fcaBirimUcret;
	}

	/**
	 * @return the fcaTirKapasite
	 */
	public double getFcaTirKapasite() {
		return fcaTirKapasite;
	}

	/**
	 * @param fcaTirKapasite
	 *            the fcaTirKapasite to set
	 */
	public void setFcaTirKapasite(double fcaTirKapasite) {
		this.fcaTirKapasite = fcaTirKapasite;
	}

	/**
	 * @return the fobKonteynerUcreti
	 */
	public double getFobKonteynerUcreti() {
		return fobKonteynerUcreti;
	}

	/**
	 * @param fobKonteynerUcreti
	 *            the fobKonteynerUcreti to set
	 */
	public void setFobKonteynerUcreti(double fobKonteynerUcreti) {
		this.fobKonteynerUcreti = fobKonteynerUcreti;
	}

	/**
	 * @return the fobBirimUcret
	 */
	public double getFobBirimUcret() {
		return fobBirimUcret;
	}

	/**
	 * @param fobBirimUcret
	 *            the fobBirimUcret to set
	 */
	public void setFobBirimUcret(double fobBirimUcret) {
		this.fobBirimUcret = fobBirimUcret;
	}

	/**
	 * @return the fobKonteynerSayisi
	 */
	public double getFobKonteynerSayisi() {
		return fobKonteynerSayisi;
	}

	/**
	 * @param fobKonteynerSayisi
	 *            the fobKonteynerSayisi to set
	 */
	public void setFobKonteynerSayisi(double fobKonteynerSayisi) {
		this.fobKonteynerSayisi = fobKonteynerSayisi;
	}

	/**
	 * @return the fobUcreti
	 */
	public double getFobUcreti() {
		return fobUcreti;
	}

	/**
	 * @param fobUcreti
	 *            the fobUcreti to set
	 */
	public void setFobUcreti(double fobUcreti) {
		this.fobUcreti = fobUcreti;
	}

	/**
	 * @return the cfrKonteynerUcreti
	 */
	public double getCfrKonteynerUcreti() {
		return cfrKonteynerUcreti;
	}

	/**
	 * @param cfrKonteynerUcreti
	 *            the cfrKonteynerUcreti to set
	 */
	public void setCfrKonteynerUcreti(double cfrKonteynerUcreti) {
		this.cfrKonteynerUcreti = cfrKonteynerUcreti;
	}

	/**
	 * @return the cfrBirimUcret
	 */
	public double getCfrBirimUcret() {
		return cfrBirimUcret;
	}

	/**
	 * @param cfrBirimUcret
	 *            the cfrBirimUcret to set
	 */
	public void setCfrBirimUcret(double cfrBirimUcret) {
		this.cfrBirimUcret = cfrBirimUcret;
	}

	/**
	 * @return the cfrKonteynerSayisi
	 */
	public double getCfrKonteynerSayisi() {
		return cfrKonteynerSayisi;
	}

	/**
	 * @param cfrKonteynerSayisi
	 *            the cfrKonteynerSayisi to set
	 */
	public void setCfrKonteynerSayisi(double cfrKonteynerSayisi) {
		this.cfrKonteynerSayisi = cfrKonteynerSayisi;
	}

	/**
	 * @return the cfrUcreti
	 */
	public double getCfrUcreti() {
		return cfrUcreti;
	}

	/**
	 * @param cfrUcreti
	 *            the cfrUcreti to set
	 */
	public void setCfrUcreti(double cfrUcreti) {
		this.cfrUcreti = cfrUcreti;
	}

	/**
	 * @return the transportationVehicle
	 */
	public Integer getTransportationVehicle() {
		return transportationVehicle;
	}

	/**
	 * @param transportationVehicle
	 *            the transportationVehicle to set
	 */
	public void setTransportationVehicle(Integer transportationVehicle) {
		this.transportationVehicle = transportationVehicle;
	}

	/**
	 * @return the shipCarryType
	 */
	public Integer getShipCarryType() {
		return shipCarryType;
	}

	/**
	 * @param shipCarryType
	 *            the shipCarryType to set
	 */
	public void setShipCarryType(Integer shipCarryType) {
		this.shipCarryType = shipCarryType;
	}

	/**
	 * @return the fobLimanUcreti
	 */
	public double getFobLimanUcreti() {
		return fobLimanUcreti;
	}

	/**
	 * @param fobLimanUcreti
	 *            the fobLimanUcreti to set
	 */
	public void setFobLimanUcreti(double fobLimanUcreti) {
		this.fobLimanUcreti = fobLimanUcreti;
	}

	/**
	 * @return the cfrNavlunUcreti
	 */
	public double getCfrNavlunUcreti() {
		return cfrNavlunUcreti;
	}

	/**
	 * @param cfrNavlunUcreti
	 *            the cfrNavlunUcreti to set
	 */
	public void setCfrNavlunUcreti(double cfrNavlunUcreti) {
		this.cfrNavlunUcreti = cfrNavlunUcreti;
	}

	/**
	 * @return the cfrNavlunMetreUcreti
	 */
	public double getCfrNavlunMetreUcreti() {
		return cfrNavlunMetreUcreti;
	}

	/**
	 * @param cfrNavlunMetreUcreti
	 *            the cfrNavlunMetreUcreti to set
	 */
	public void setCfrNavlunMetreUcreti(double cfrNavlunMetreUcreti) {
		this.cfrNavlunMetreUcreti = cfrNavlunMetreUcreti;
	}

}
