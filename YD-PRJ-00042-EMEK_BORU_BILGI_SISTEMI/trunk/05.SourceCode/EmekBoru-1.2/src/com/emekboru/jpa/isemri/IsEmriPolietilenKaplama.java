package com.emekboru.jpa.isemri;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the is_emri_polietilen_kaplama database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "IsEmriPolietilenKaplama.findAllByItemId", query = "SELECT r FROM IsEmriPolietilenKaplama r WHERE r.salesItem.itemId=:prmItemId order by r.eklemeZamani") })
@Table(name = "is_emri_polietilen_kaplama")
public class IsEmriPolietilenKaplama implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IS_EMRI_POLIETILEN_KAPLAMA_ID_GENERATOR", sequenceName = "IS_EMRI_POLIETILEN_KAPLAMA_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IS_EMRI_POLIETILEN_KAPLAMA_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	@Basic
	@Column(name = "sales_item_id", nullable = false, updatable = false, insertable = false)
	private Integer salesItemId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	private SalesItem salesItem;

	// @Column(name = "coat_material_epoksi_id")
	// private Integer coatMaterialId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coat_material_epoksi_id", referencedColumnName = "coat_material_id", insertable = false)
	private CoatRawMaterial coatRawMaterialEpoksi;

	// @Column(name = "coat_material_yapistirici_id")
	// private Integer coatMaterialId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coat_material_yapistirici_id", referencedColumnName = "coat_material_id", insertable = false)
	private CoatRawMaterial coatRawMaterialYapistirici;

	// @Column(name = "coat_material_polietilen_id")
	// private Integer coatMaterialId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coat_material_polietilen_id", referencedColumnName = "coat_material_id", insertable = false)
	private CoatRawMaterial coatRawMaterialPolietilen;

	@Column(name = "hedeflenen_malzeme_epoksi")
	private Double hedeflenenMalzemeEpoksi;

	@Column(name = "hedeflenen_malzeme_yapistirici")
	private Double hedeflenenMalzemeYapistirici;

	@Column(name = "hedeflenen_malzeme_polietilen")
	private Double hedeflenenMalzemePolietilen;

	@Column(name = "hatve")
	private String hatve;

	@Column(name = "intercoat_suresi")
	private Integer intercoatSuresi;

	@Column(name = "kontrol_parametre_id", insertable = false, updatable = false)
	private Integer kontrolParametreId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "kontrol_parametre_id", referencedColumnName = "ID", insertable = false)
	private IsEmriPolietilenKaplamaKontrolParametreleri isEmriPolietilenKaplamaKontrolParametreleri;

	@Column(name = "yapistirici_sicaklik_id", insertable = false, updatable = false)
	private Integer yapistiriciSicaklikId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "yapistirici_sicaklik_id", referencedColumnName = "ID", insertable = false)
	private IsEmriPolietilenKaplamaYapistiriciSicaklik isEmriPolietilenKaplamaYapistiriciSicaklik;

	@Column(name = "polietilen_sicaklik_id", insertable = false, updatable = false)
	private Integer polietilenSicaklikId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "polietilen_sicaklik_id", referencedColumnName = "ID", insertable = false)
	private IsEmriPolietilenKaplamaPolietilenSicaklik isEmriPolietilenKaplamaPolietilenSicaklik;

	public IsEmriPolietilenKaplama() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public CoatRawMaterial getCoatRawMaterialEpoksi() {
		return coatRawMaterialEpoksi;
	}

	public void setCoatRawMaterialEpoksi(CoatRawMaterial coatRawMaterialEpoksi) {
		this.coatRawMaterialEpoksi = coatRawMaterialEpoksi;
	}

	public CoatRawMaterial getCoatRawMaterialYapistirici() {
		return coatRawMaterialYapistirici;
	}

	public void setCoatRawMaterialYapistirici(
			CoatRawMaterial coatRawMaterialYapistirici) {
		this.coatRawMaterialYapistirici = coatRawMaterialYapistirici;
	}

	public CoatRawMaterial getCoatRawMaterialPolietilen() {
		return coatRawMaterialPolietilen;
	}

	public void setCoatRawMaterialPolietilen(
			CoatRawMaterial coatRawMaterialPolietilen) {
		this.coatRawMaterialPolietilen = coatRawMaterialPolietilen;
	}

	public Double getHedeflenenMalzemeEpoksi() {
		return hedeflenenMalzemeEpoksi;
	}

	public void setHedeflenenMalzemeEpoksi(Double hedeflenenMalzemeEpoksi) {
		this.hedeflenenMalzemeEpoksi = hedeflenenMalzemeEpoksi;
	}

	public Double getHedeflenenMalzemeYapistirici() {
		return hedeflenenMalzemeYapistirici;
	}

	public void setHedeflenenMalzemeYapistirici(
			Double hedeflenenMalzemeYapistirici) {
		this.hedeflenenMalzemeYapistirici = hedeflenenMalzemeYapistirici;
	}

	public Double getHedeflenenMalzemePolietilen() {
		return hedeflenenMalzemePolietilen;
	}

	public void setHedeflenenMalzemePolietilen(
			Double hedeflenenMalzemePolietilen) {
		this.hedeflenenMalzemePolietilen = hedeflenenMalzemePolietilen;
	}

	public String getHatve() {
		return hatve;
	}

	public void setHatve(String hatve) {
		this.hatve = hatve;
	}

	public Integer getIntercoatSuresi() {
		return intercoatSuresi;
	}

	public void setIntercoatSuresi(Integer intercoatSuresi) {
		this.intercoatSuresi = intercoatSuresi;
	}

	public IsEmriPolietilenKaplamaKontrolParametreleri getIsEmriPolietilenKaplamaKontrolParametreleri() {
		return isEmriPolietilenKaplamaKontrolParametreleri;
	}

	public void setIsEmriPolietilenKaplamaKontrolParametreleri(
			IsEmriPolietilenKaplamaKontrolParametreleri isEmriPolietilenKaplamaKontrolParametreleri) {
		this.isEmriPolietilenKaplamaKontrolParametreleri = isEmriPolietilenKaplamaKontrolParametreleri;
	}

	public IsEmriPolietilenKaplamaPolietilenSicaklik getIsEmriPolietilenKaplamaPolietilenSicaklik() {
		return isEmriPolietilenKaplamaPolietilenSicaklik;
	}

	public void setIsEmriPolietilenKaplamaPolietilenSicaklik(
			IsEmriPolietilenKaplamaPolietilenSicaklik isEmriPolietilenKaplamaPolietilenSicaklik) {
		this.isEmriPolietilenKaplamaPolietilenSicaklik = isEmriPolietilenKaplamaPolietilenSicaklik;
	}

	public IsEmriPolietilenKaplamaYapistiriciSicaklik getIsEmriPolietilenKaplamaYapistiriciSicaklik() {
		return isEmriPolietilenKaplamaYapistiriciSicaklik;
	}

	public void setIsEmriPolietilenKaplamaYapistiriciSicaklik(
			IsEmriPolietilenKaplamaYapistiriciSicaklik isEmriPolietilenKaplamaYapistiriciSicaklik) {
		this.isEmriPolietilenKaplamaYapistiriciSicaklik = isEmriPolietilenKaplamaYapistiriciSicaklik;
	}

}