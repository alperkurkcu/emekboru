package com.emekboru.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the pipe_coat database table.
 * 
 */
@Entity
@Table(name="pipe_coat")
public class PipeCoat implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="pipe_coat_id")
	@SequenceGenerator(name="PIPE_COAT_GENERATOR", sequenceName="PIPE_COAT_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PIPE_COAT_GENERATOR")	
	private Integer pipeCoatId;

	@Column(name="cement_parameters_id")
	private Integer cementParametersId;

    @Temporal( TemporalType.TIMESTAMP)
	private Date date;

	@Column(name="goz_kontrol")
	private String gozKontrol;

	@Column(name="parameters_id")
	private Integer parametersId;

	@Column(name="pipe_id")
	private Integer pipeId;

	@Column(name="polietilen_parameters_id")
	private Integer polietilenParametersId;

	@Column(name="repair_holiday_test")
	private String repairHolidayTest;

	@Column(name="side")
	private Integer side;

	private String type;

	@Column(name="yas_cap_a")
	private double yasCapA;

	@Column(name="yas_cap_b")
	private double yasCapB;

    public PipeCoat() {
    }

	public Integer getPipeCoatId() {
		return this.pipeCoatId;
	}

	public void setPipeCoatId(Integer pipeCoatId) {
		this.pipeCoatId = pipeCoatId;
	}

	public Integer getCementParametersId() {
		return this.cementParametersId;
	}

	public void setCementParametersId(Integer cementParametersId) {
		this.cementParametersId = cementParametersId;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getGozKontrol() {
		return this.gozKontrol;
	}

	public void setGozKontrol(String gozKontrol) {
		this.gozKontrol = gozKontrol;
	}

	public Integer getParametersId() {
		return this.parametersId;
	}

	public void setParametersId(Integer parametersId) {
		this.parametersId = parametersId;
	}

	public Integer getPipeId() {
		return this.pipeId;
	}

	public void setPipeId(Integer pipeId) {
		this.pipeId = pipeId;
	}

	public Integer getPolietilenParametersId() {
		return this.polietilenParametersId;
	}

	public void setPolietilenParametersId(Integer polietilenParametersId) {
		this.polietilenParametersId = polietilenParametersId;
	}

	public String getRepairHolidayTest() {
		return this.repairHolidayTest;
	}

	public void setRepairHolidayTest(String repairHolidayTest) {
		this.repairHolidayTest = repairHolidayTest;
	}

	public Integer getSide() {
		return this.side;
	}

	public void setSide(Integer side) {
		this.side = side;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getYasCapA() {
		return this.yasCapA;
	}

	public void setYasCapA(double yasCapA) {
		this.yasCapA = yasCapA;
	}

	public double getYasCapB() {
		return this.yasCapB;
	}

	public void setYasCapB(double yasCapB) {
		this.yasCapB = yasCapB;
	}

}