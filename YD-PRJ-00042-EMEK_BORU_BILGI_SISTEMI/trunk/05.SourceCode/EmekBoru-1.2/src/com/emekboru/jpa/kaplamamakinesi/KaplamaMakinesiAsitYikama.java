package com.emekboru.jpa.kaplamamakinesi;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the kaplama_makinesi_asit_yikama database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "KaplamaMakinesiAsitYikama.findAllByPipeId", query = "SELECT r FROM KaplamaMakinesiAsitYikama r WHERE r.pipe.pipeId=:prmPipeId order by r.eklemeZamani") })
@Table(name = "kaplama_makinesi_asit_yikama")
public class KaplamaMakinesiAsitYikama implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "KAPLAMA_MAKINESI_ASIT_YIKAMA_ID_GENERATOR", sequenceName = "KAPLAMA_MAKINESI_ASIT_YIKAMA_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "KAPLAMA_MAKINESI_ASIT_YIKAMA_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	private Boolean durum;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "kaplama_baslama_kullanici")
	private Integer kaplamaBaslamaKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "kaplama_baslama_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser kaplamaBaslamaUser;

	@Column(name = "kaplama_baslama_zamani")
	private Timestamp kaplamaBaslamaZamani;

	@Column(name = "kaplama_bitis_kullanici")
	private Integer kaplamaBitisKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "kaplama_bitis_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser kaplamaBitisUser;

	@Column(name = "kaplama_bitis_zamani")
	private Timestamp kaplamaBitisZamani;

	@Basic
	@Column(name = "pipe_id", nullable = false, insertable = false, updatable = false)
	private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	private String konsantrasyon;

	@Column(name = "sicaklik_a")
	private BigDecimal sicaklikA;

	@Column(name = "sicaklik_b")
	private BigDecimal sicaklikB;

	@Column(name = "sicaklik_c")
	private BigDecimal sicaklikC;

	public KaplamaMakinesiAsitYikama() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getDurum() {
		return this.durum;
	}

	public void setDurum(Boolean durum) {
		this.durum = durum;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Integer getKaplamaBaslamaKullanici() {
		return this.kaplamaBaslamaKullanici;
	}

	public void setKaplamaBaslamaKullanici(Integer kaplamaBaslamaKullanici) {
		this.kaplamaBaslamaKullanici = kaplamaBaslamaKullanici;
	}

	public Timestamp getKaplamaBaslamaZamani() {
		return this.kaplamaBaslamaZamani;
	}

	public void setKaplamaBaslamaZamani(Timestamp kaplamaBaslamaZamani) {
		this.kaplamaBaslamaZamani = kaplamaBaslamaZamani;
	}

	public Integer getKaplamaBitisKullanici() {
		return this.kaplamaBitisKullanici;
	}

	public void setKaplamaBitisKullanici(Integer kaplamaBitisKullanici) {
		this.kaplamaBitisKullanici = kaplamaBitisKullanici;
	}

	public Timestamp getKaplamaBitisZamani() {
		return this.kaplamaBitisZamani;
	}

	public void setKaplamaBitisZamani(Timestamp kaplamaBitisZamani) {
		this.kaplamaBitisZamani = kaplamaBitisZamani;
	}

	public String getKonsantrasyon() {
		return this.konsantrasyon;
	}

	public void setKonsantrasyon(String konsantrasyon) {
		this.konsantrasyon = konsantrasyon;
	}

	public Integer getPipeId() {
		return this.pipeId;
	}

	public void setPipeId(Integer pipeId) {
		this.pipeId = pipeId;
	}

	/**
	 * @return the kaplamaBaslamaUser
	 */
	public SystemUser getKaplamaBaslamaUser() {
		return kaplamaBaslamaUser;
	}

	/**
	 * @param kaplamaBaslamaUser
	 *            the kaplamaBaslamaUser to set
	 */
	public void setKaplamaBaslamaUser(SystemUser kaplamaBaslamaUser) {
		this.kaplamaBaslamaUser = kaplamaBaslamaUser;
	}

	/**
	 * @return the kaplamaBitisUser
	 */
	public SystemUser getKaplamaBitisUser() {
		return kaplamaBitisUser;
	}

	/**
	 * @param kaplamaBitisUser
	 *            the kaplamaBitisUser to set
	 */
	public void setKaplamaBitisUser(SystemUser kaplamaBitisUser) {
		this.kaplamaBitisUser = kaplamaBitisUser;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the sicaklikA
	 */
	public BigDecimal getSicaklikA() {
		return sicaklikA;
	}

	/**
	 * @param sicaklikA
	 *            the sicaklikA to set
	 */
	public void setSicaklikA(BigDecimal sicaklikA) {
		this.sicaklikA = sicaklikA;
	}

	/**
	 * @return the sicaklikB
	 */
	public BigDecimal getSicaklikB() {
		return sicaklikB;
	}

	/**
	 * @param sicaklikB
	 *            the sicaklikB to set
	 */
	public void setSicaklikB(BigDecimal sicaklikB) {
		this.sicaklikB = sicaklikB;
	}

	/**
	 * @return the sicaklikC
	 */
	public BigDecimal getSicaklikC() {
		return sicaklikC;
	}

	/**
	 * @param sicaklikC
	 *            the sicaklikC to set
	 */
	public void setSicaklikC(BigDecimal sicaklikC) {
		this.sicaklikC = sicaklikC;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}