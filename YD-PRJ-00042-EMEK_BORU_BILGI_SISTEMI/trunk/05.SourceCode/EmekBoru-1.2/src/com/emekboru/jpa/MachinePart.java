package com.emekboru.jpa;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the machine_part database table.
 * 
 */
@Entity
@NamedQuery(name = "MachinePart.findAllByMachineId", query = "SELECT m FROM MachinePart m WHERE m.machine.machineId = :machineId")
@Table(name = "machine_part")
public class MachinePart implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "MACHINE_PART_MACHINEPARTID_GENERATOR", sequenceName = "MACHINE_PART_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MACHINE_PART_MACHINEPARTID_GENERATOR")
	@Column(name = "machine_part_id", unique = true, nullable = false)
	private Integer machinePartId;

	@Column(name = "part_name", length = 32)
	private String partName;

	@Column(name = "short_desc", length = 120)
	private String shortDesc;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "machine_id", referencedColumnName = "machine_id")
	private Machine machine;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "machine_part_category_id", referencedColumnName = "machine_part_category_id")
	private MachinePartCategory machinePartCategory;

	@OneToMany(mappedBy = "machinePart", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<MaintenancePlan> maintenancePlans;

	public MachinePart() {

		machine = new Machine();
		machinePartCategory = new MachinePartCategory();
	}

	// setters getters

	public Integer getMachinePartId() {
		return this.machinePartId;
	}

	public void setMachinePartId(Integer machinePartId) {
		this.machinePartId = machinePartId;
	}

	public String getPartName() {
		return this.partName;
	}

	public void setPartName(String partName) {
		this.partName = partName;
	}

	public String getShortDesc() {
		return this.shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public Machine getMachine() {
		return machine;
	}

	public void setMachine(Machine machine) {
		this.machine = machine;
	}

	public MachinePartCategory getMachinePartCategory() {
		return machinePartCategory;
	}

	public void setMachinePartCategory(MachinePartCategory machinePartCategory) {
		this.machinePartCategory = machinePartCategory;
	}

	public List<MaintenancePlan> getMaintenancePlans() {
		return maintenancePlans;
	}

	public void setMaintenancePlans(List<MaintenancePlan> maintenancePlans) {
		this.maintenancePlans = maintenancePlans;
	}

}