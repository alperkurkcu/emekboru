package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the polietilen_machine_parameters database table.
 * 
 */
@Entity
@Table(name="polietilen_machine_parameters")
public class PolietilenMachineParameter implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="polietilen_parameters_id")
	@SequenceGenerator(name="POLIETILEN_MACHINE_PARAMETERS_GENERATOR", sequenceName="POLIETILEN_MACHINE_PARAMETERS_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="POLIETILEN_MACHINE_PARAMETERS_GENERATOR")	
	private Integer polietilenParametersId;

	@Column(name="boru_hizi")
	private double boruHizi;

	private Integer cene;

	@Column(name="hatve_max")
	private Integer hatveMax;

	@Column(name="hatve_min")
	private Integer hatveMin;

	@Column(name="konv_acisi")
	private Integer konvAcisi;

	@Column(name="machine_id")
	private Integer machineId;

	@Column(name="pe_film_sicakligi_max")
	private Integer peFilmSicakligiMax;

	@Column(name="pe_film_sicakligi_min")
	private Integer peFilmSicakligiMin;

	@Column(name="pe_vida_a")
	private double peVidaA;

	@Column(name="pe_vida_b")
	private double peVidaB;

	@Column(name="polietilen_vida_a")
	private Integer polietilenVidaA;

	@Column(name="polietilen_vida_b")
	private Integer polietilenVidaB;

	@Column(name="yapis_film_sicakligi_max")
	private Integer yapisFilmSicakligiMax;

	@Column(name="yapis_film_sicakligi_min")
	private Integer yapisFilmSicakligiMin;

	@Column(name="yapistirci_vida")
	private double yapistirciVida;

	@Column(name="yapistirici")
	private Integer yapistirici;

    public PolietilenMachineParameter() {
    }

	public Integer getPolietilenParametersId() {
		return this.polietilenParametersId;
	}

	public void setPolietilenParametersId(Integer polietilenParametersId) {
		this.polietilenParametersId = polietilenParametersId;
	}

	public double getBoruHizi() {
		return this.boruHizi;
	}

	public void setBoruHizi(double boruHizi) {
		this.boruHizi = boruHizi;
	}

	public Integer getCene() {
		return this.cene;
	}

	public void setCene(Integer cene) {
		this.cene = cene;
	}

	public Integer getHatveMax() {
		return this.hatveMax;
	}

	public void setHatveMax(Integer hatveMax) {
		this.hatveMax = hatveMax;
	}

	public Integer getHatveMin() {
		return this.hatveMin;
	}

	public void setHatveMin(Integer hatveMin) {
		this.hatveMin = hatveMin;
	}

	public Integer getKonvAcisi() {
		return this.konvAcisi;
	}

	public void setKonvAcisi(Integer konvAcisi) {
		this.konvAcisi = konvAcisi;
	}

	public Integer getMachineId() {
		return this.machineId;
	}

	public void setMachineId(Integer machineId) {
		this.machineId = machineId;
	}

	public Integer getPeFilmSicakligiMax() {
		return this.peFilmSicakligiMax;
	}

	public void setPeFilmSicakligiMax(Integer peFilmSicakligiMax) {
		this.peFilmSicakligiMax = peFilmSicakligiMax;
	}

	public Integer getPeFilmSicakligiMin() {
		return this.peFilmSicakligiMin;
	}

	public void setPeFilmSicakligiMin(Integer peFilmSicakligiMin) {
		this.peFilmSicakligiMin = peFilmSicakligiMin;
	}

	public double getPeVidaA() {
		return this.peVidaA;
	}

	public void setPeVidaA(double peVidaA) {
		this.peVidaA = peVidaA;
	}

	public double getPeVidaB() {
		return this.peVidaB;
	}

	public void setPeVidaB(double peVidaB) {
		this.peVidaB = peVidaB;
	}

	public Integer getPolietilenVidaA() {
		return this.polietilenVidaA;
	}

	public void setPolietilenVidaA(Integer polietilenVidaA) {
		this.polietilenVidaA = polietilenVidaA;
	}

	public Integer getPolietilenVidaB() {
		return this.polietilenVidaB;
	}

	public void setPolietilenVidaB(Integer polietilenVidaB) {
		this.polietilenVidaB = polietilenVidaB;
	}

	public Integer getYapisFilmSicakligiMax() {
		return this.yapisFilmSicakligiMax;
	}

	public void setYapisFilmSicakligiMax(Integer yapisFilmSicakligiMax) {
		this.yapisFilmSicakligiMax = yapisFilmSicakligiMax;
	}

	public Integer getYapisFilmSicakligiMin() {
		return this.yapisFilmSicakligiMin;
	}

	public void setYapisFilmSicakligiMin(Integer yapisFilmSicakligiMin) {
		this.yapisFilmSicakligiMin = yapisFilmSicakligiMin;
	}

	public double getYapistirciVida() {
		return this.yapistirciVida;
	}

	public void setYapistirciVida(double yapistirciVida) {
		this.yapistirciVida = yapistirciVida;
	}

	public Integer getYapistirici() {
		return this.yapistirici;
	}

	public void setYapistirici(Integer yapistirici) {
		this.yapistirici = yapistirici;
	}

}