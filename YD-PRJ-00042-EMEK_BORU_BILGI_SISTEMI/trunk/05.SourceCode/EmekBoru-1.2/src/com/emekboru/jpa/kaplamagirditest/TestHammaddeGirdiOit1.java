package com.emekboru.jpa.kaplamagirditest;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the test_hammadde_girdi_oit1 database table.
 * 
 */
@Entity
@Table(name = "test_hammadde_girdi_oit1")
public class TestHammaddeGirdiOit1 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_HAMMADDE_GIRDI_OIT1_ID_GENERATOR", sequenceName = "TEST_HAMMADDE_GIRDI_OIT1_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_HAMMADDE_GIRDI_OIT1_ID_GENERATOR")
	@Column(name = "ID")
	private Integer id;

	// @Column(name = "coat_material_id")
	// private Integer coatMaterialId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coat_material_id", referencedColumnName = "coat_material_id", insertable = false)
	private CoatRawMaterial coatRawMaterial;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "numune_hazirlama_methodu")
	private String numuneHazirlamaMethodu;

	@Column(name = "oksidasyon_iduksiyon_zamani")
	private String oksidasyonIduksiyonZamani;

	@Column(name = "parti_no")
	private Integer partiNo;

	private Boolean sonuc;

	private String standard;

	@Column(name = "test_sicakligi")
	private String testSicakligi;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi", nullable = false)
	private Date testTarihi;

	@Column(name = "uretici_spec")
	private String ureticiSpec;

	@Column(name = "degerlendirme_standard")
	private String degerlendirmeStandard;

	@Column(name = "rapor_no")
	private String raporNo;

	public TestHammaddeGirdiOit1() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getNumuneHazirlamaMethodu() {
		return this.numuneHazirlamaMethodu;
	}

	public void setNumuneHazirlamaMethodu(String numuneHazirlamaMethodu) {
		this.numuneHazirlamaMethodu = numuneHazirlamaMethodu;
	}

	public Integer getPartiNo() {
		return this.partiNo;
	}

	public void setPartiNo(Integer partiNo) {
		this.partiNo = partiNo;
	}

	public Boolean getSonuc() {
		return this.sonuc;
	}

	public void setSonuc(Boolean sonuc) {
		this.sonuc = sonuc;
	}

	public String getStandard() {
		return this.standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getUreticiSpec() {
		return this.ureticiSpec;
	}

	public void setUreticiSpec(String ureticiSpec) {
		this.ureticiSpec = ureticiSpec;
	}

	/**
	 * @return the coatRawMaterial
	 */
	public CoatRawMaterial getCoatRawMaterial() {
		return coatRawMaterial;
	}

	/**
	 * @param coatRawMaterial
	 *            the coatRawMaterial to set
	 */
	public void setCoatRawMaterial(CoatRawMaterial coatRawMaterial) {
		this.coatRawMaterial = coatRawMaterial;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the oksidasyonIduksiyonZamani
	 */
	public String getOksidasyonIduksiyonZamani() {
		return oksidasyonIduksiyonZamani;
	}

	/**
	 * @param oksidasyonIduksiyonZamani
	 *            the oksidasyonIduksiyonZamani to set
	 */
	public void setOksidasyonIduksiyonZamani(String oksidasyonIduksiyonZamani) {
		this.oksidasyonIduksiyonZamani = oksidasyonIduksiyonZamani;
	}

	/**
	 * @return the testSicakligi
	 */
	public String getTestSicakligi() {
		return testSicakligi;
	}

	/**
	 * @param testSicakligi
	 *            the testSicakligi to set
	 */
	public void setTestSicakligi(String testSicakligi) {
		this.testSicakligi = testSicakligi;
	}

	/**
	 * @return the testTarihi
	 */
	public Date getTestTarihi() {
		return testTarihi;
	}

	/**
	 * @param testTarihi
	 *            the testTarihi to set
	 */
	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	/**
	 * @return the degerlendirmeStandard
	 */
	public String getDegerlendirmeStandard() {
		return degerlendirmeStandard;
	}

	/**
	 * @param degerlendirmeStandard
	 *            the degerlendirmeStandard to set
	 */
	public void setDegerlendirmeStandard(String degerlendirmeStandard) {
		this.degerlendirmeStandard = degerlendirmeStandard;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the raporNo
	 */
	public String getRaporNo() {
		return raporNo;
	}

	/**
	 * @param raporNo
	 *            the raporNo to set
	 */
	public void setRaporNo(String raporNo) {
		this.raporNo = raporNo;
	}
}