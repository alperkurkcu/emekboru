package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.isolation.IsolationTestDefinition;

/**
 * The persistent class for the
 * test_kaplama_kumlanmis_boru_yuzey_puruzlulugu_sonuc database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc.findAll", query = "SELECT r FROM TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_kaplama_kumlanmis_boru_yuzey_puruzlulugu_sonuc")
public class TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc implements
		Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KAPLAMA_KUMLANMIS_BORU_YUZEY_PURUZLULUGU_SONUC_ID_GENERATOR", sequenceName = "TEST_KAPLAMA_KUMLANMIS_BORU_YUZEY_PURUZLULUGU_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KAPLAMA_KUMLANMIS_BORU_YUZEY_PURUZLULUGU_SONUC_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "a_ucu_max")
	private BigDecimal aUcuMax;

	@Column(name = "a_ucu_min")
	private BigDecimal aUcuMin;

	@Column(name = "a_ucu_ort")
	private BigDecimal aUcuOrt;

	@Column(name = "a_ucu_sonuc")
	private Boolean aUcuSonuc;

	@Column(name = "b_ucu_max")
	private BigDecimal bUcuMax;

	@Column(name = "b_ucu_min")
	private BigDecimal bUcuMin;

	@Column(name = "b_ucu_ort")
	private BigDecimal bUcuOrt;

	@Column(name = "b_ucu_sonuc")
	private Boolean bUcuSonuc;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "kullanilan_yontem")
	private Integer kullanilanYontem;

	@Column(name = "orta_ucu_max")
	private BigDecimal ortaUcuMax;

	@Column(name = "orta_ucu_min")
	private BigDecimal ortaUcuMin;

	@Column(name = "orta_ucu_ort")
	private BigDecimal ortaUcuOrt;

	@Column(name = "orta_ucu_sonuc")
	private Boolean ortaUcuSonuc;

	// @Column(name="pipe_id")
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi", nullable = false)
	private Date testTarihi;

	public TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc() {
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the aUcuSonuc
	 */
	public Boolean getaUcuSonuc() {
		return aUcuSonuc;
	}

	/**
	 * @param aUcuSonuc
	 *            the aUcuSonuc to set
	 */
	public void setaUcuSonuc(Boolean aUcuSonuc) {
		this.aUcuSonuc = aUcuSonuc;
	}

	/**
	 * @return the bUcuSonuc
	 */
	public Boolean getbUcuSonuc() {
		return bUcuSonuc;
	}

	/**
	 * @param bUcuSonuc
	 *            the bUcuSonuc to set
	 */
	public void setbUcuSonuc(Boolean bUcuSonuc) {
		this.bUcuSonuc = bUcuSonuc;
	}

	/**
	 * @return the eklemeZamani
	 */
	public Date getEklemeZamani() {
		return eklemeZamani;
	}

	/**
	 * @return the ekleyenKullanici
	 */
	public Integer getEkleyenKullanici() {
		return ekleyenKullanici;
	}

	/**
	 * @param ekleyenKullanici
	 *            the ekleyenKullanici to set
	 */
	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	/**
	 * @return the guncellemeZamani
	 */
	public Date getGuncellemeZamani() {
		return guncellemeZamani;
	}

	/**
	 * @return the guncelleyenKullanici
	 */
	public Integer getGuncelleyenKullanici() {
		return guncelleyenKullanici;
	}

	/**
	 * @param guncelleyenKullanici
	 *            the guncelleyenKullanici to set
	 */
	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	/**
	 * @return the kullanilanYontem
	 */
	public Integer getKullanilanYontem() {
		return kullanilanYontem;
	}

	/**
	 * @param kullanilanYontem
	 *            the kullanilanYontem to set
	 */
	public void setKullanilanYontem(Integer kullanilanYontem) {
		this.kullanilanYontem = kullanilanYontem;
	}

	/**
	 * @return the ortaUcuSonuc
	 */
	public Boolean getOrtaUcuSonuc() {
		return ortaUcuSonuc;
	}

	/**
	 * @param ortaUcuSonuc
	 *            the ortaUcuSonuc to set
	 */
	public void setOrtaUcuSonuc(Boolean ortaUcuSonuc) {
		this.ortaUcuSonuc = ortaUcuSonuc;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the bagliGlobalId
	 */
	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}

	/**
	 * @param bagliGlobalId
	 *            the bagliGlobalId to set
	 */
	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	/**
	 * @return the bagliTestId
	 */
	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the testTarihi
	 */
	public Date getTestTarihi() {
		return testTarihi;
	}

	/**
	 * @param testTarihi
	 *            the testTarihi to set
	 */
	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @param eklemeZamani
	 *            the eklemeZamani to set
	 */
	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	/**
	 * @param guncellemeZamani
	 *            the guncellemeZamani to set
	 */
	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	/**
	 * @return the aUcuMax
	 */
	public BigDecimal getaUcuMax() {
		return aUcuMax;
	}

	/**
	 * @param aUcuMax
	 *            the aUcuMax to set
	 */
	public void setaUcuMax(BigDecimal aUcuMax) {
		this.aUcuMax = aUcuMax;
	}

	/**
	 * @return the aUcuMin
	 */
	public BigDecimal getaUcuMin() {
		return aUcuMin;
	}

	/**
	 * @param aUcuMin
	 *            the aUcuMin to set
	 */
	public void setaUcuMin(BigDecimal aUcuMin) {
		this.aUcuMin = aUcuMin;
	}

	/**
	 * @return the aUcuOrt
	 */
	public BigDecimal getaUcuOrt() {
		return aUcuOrt;
	}

	/**
	 * @param aUcuOrt
	 *            the aUcuOrt to set
	 */
	public void setaUcuOrt(BigDecimal aUcuOrt) {
		this.aUcuOrt = aUcuOrt;
	}

	/**
	 * @return the bUcuMax
	 */
	public BigDecimal getbUcuMax() {
		return bUcuMax;
	}

	/**
	 * @param bUcuMax
	 *            the bUcuMax to set
	 */
	public void setbUcuMax(BigDecimal bUcuMax) {
		this.bUcuMax = bUcuMax;
	}

	/**
	 * @return the bUcuMin
	 */
	public BigDecimal getbUcuMin() {
		return bUcuMin;
	}

	/**
	 * @param bUcuMin
	 *            the bUcuMin to set
	 */
	public void setbUcuMin(BigDecimal bUcuMin) {
		this.bUcuMin = bUcuMin;
	}

	/**
	 * @return the bUcuOrt
	 */
	public BigDecimal getbUcuOrt() {
		return bUcuOrt;
	}

	/**
	 * @param bUcuOrt
	 *            the bUcuOrt to set
	 */
	public void setbUcuOrt(BigDecimal bUcuOrt) {
		this.bUcuOrt = bUcuOrt;
	}

	/**
	 * @return the ortaUcuMax
	 */
	public BigDecimal getOrtaUcuMax() {
		return ortaUcuMax;
	}

	/**
	 * @param ortaUcuMax
	 *            the ortaUcuMax to set
	 */
	public void setOrtaUcuMax(BigDecimal ortaUcuMax) {
		this.ortaUcuMax = ortaUcuMax;
	}

	/**
	 * @return the ortaUcuMin
	 */
	public BigDecimal getOrtaUcuMin() {
		return ortaUcuMin;
	}

	/**
	 * @param ortaUcuMin
	 *            the ortaUcuMin to set
	 */
	public void setOrtaUcuMin(BigDecimal ortaUcuMin) {
		this.ortaUcuMin = ortaUcuMin;
	}

	/**
	 * @return the ortaUcuOrt
	 */
	public BigDecimal getOrtaUcuOrt() {
		return ortaUcuOrt;
	}

	/**
	 * @param ortaUcuOrt
	 *            the ortaUcuOrt to set
	 */
	public void setOrtaUcuOrt(BigDecimal ortaUcuOrt) {
		this.ortaUcuOrt = ortaUcuOrt;
	}

}