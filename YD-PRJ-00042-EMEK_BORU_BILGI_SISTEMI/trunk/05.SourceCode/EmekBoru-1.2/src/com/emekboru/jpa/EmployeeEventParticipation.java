package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "employee_event_participation")
public class EmployeeEventParticipation implements Serializable{

	private static final long serialVersionUID = 7745974733603954468L;
	
	@Id
	@Column(name = "employee_event_participation_id")
	@SequenceGenerator(name = "EMPLOYEE_EVENT_PARTICIPATION_GENERATOR", sequenceName = "EMPLOYEE_EVENT_PARTICIPATION_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMPLOYEE_EVENT_PARTICIPATION_GENERATOR")
	private Integer employeeEventParticipationId;
	
	@Column(name = "participated")
	private Boolean participated;

	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "employee_id", referencedColumnName = "employee_id", updatable = false)
	private Employee employee;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "event_id", referencedColumnName = "event_id", updatable = false)
	private EventEb event;
	
	public EmployeeEventParticipation(){
		
		event = new EventEb();
		employee = new Employee();
	}

	/**
	 * 		GETTERS AND SETTERS
	 */
	public Integer getEmployeeEventParticipationId() {
		return employeeEventParticipationId;
	}

	public void setEmployeeEventParticipationId(Integer employeeEventParticipationId) {
		this.employeeEventParticipationId = employeeEventParticipationId;
	}

	public Boolean getParticipated() {
		return participated;
	}

	public void setParticipated(Boolean participated) {
		this.participated = participated;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public EventEb getEvent() {
		return event;
	}

	public void setEvent(EventEb event) {
		this.event = event;
	}
	
}
