package com.emekboru.jpa.isemri;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the is_emri_epoksi_kaplama database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "IsEmriEpoksiKaplama.findAllByItemId", query = "SELECT r FROM IsEmriEpoksiKaplama r WHERE r.salesItem.itemId=:prmItemId order by r.eklemeZamani") })
@Table(name = "is_emri_epoksi_kaplama")
public class IsEmriEpoksiKaplama implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IS_EMRI_EPOKSI_KAPLAMA_ID_GENERATOR", sequenceName = "IS_EMRI_EPOKSI_KAPLAMA_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IS_EMRI_EPOKSI_KAPLAMA_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "araba_hizi")
	private Integer arabaHizi;

	@Column(name = "basinc")
	private Double basinc;

	@Column(name = "donus_hizi")
	private Integer donusHizi;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "hedeflenen_malzeme")
	private Double hedeflenenMalzeme;

	@Column(name = "meme_capi")
	private Integer memeCapi;

	@Basic
	@Column(name = "sales_item_id", nullable = false, insertable = false, updatable = false)
	private Integer salesItemId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	private SalesItem salesItem;

	// @Column(name = "coat_material_id")
	// private Integer coatMaterialId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coat_material_id", referencedColumnName = "coat_material_id", insertable = false)
	private CoatRawMaterial coatRawMaterial;

	public IsEmriEpoksiKaplama() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getArabaHizi() {
		return this.arabaHizi;
	}

	public void setArabaHizi(Integer arabaHizi) {
		this.arabaHizi = arabaHizi;
	}

	public Integer getDonusHizi() {
		return this.donusHizi;
	}

	public void setDonusHizi(Integer donusHizi) {
		this.donusHizi = donusHizi;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getMemeCapi() {
		return this.memeCapi;
	}

	public void setMemeCapi(Integer memeCapi) {
		this.memeCapi = memeCapi;
	}

	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Double getBasinc() {
		return basinc;
	}

	public void setBasinc(Double basinc) {
		this.basinc = basinc;
	}

	public Double getHedeflenenMalzeme() {
		return hedeflenenMalzeme;
	}

	public void setHedeflenenMalzeme(Double hedeflenenMalzeme) {
		this.hedeflenenMalzeme = hedeflenenMalzeme;
	}

	public CoatRawMaterial getCoatRawMaterial() {
		return coatRawMaterial;
	}

	public void setCoatRawMaterial(CoatRawMaterial coatRawMaterial) {
		this.coatRawMaterial = coatRawMaterial;
	}

}