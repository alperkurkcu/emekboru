package com.emekboru.jpa;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.isolation.IsolationTestDefinition;

/**
 * The persistent class for the inceleme_grubu database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "IncelemeGrubu.findAll", query = "SELECT r FROM IncelemeGrubu r order by r.adi") })
@Table(name = "inceleme_grubu")
public class IncelemeGrubu implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "INCELEME_GRUBU_ID_GENERATOR", sequenceName = "INCELEME_GRUBU_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "INCELEME_GRUBU_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Column(name = "adi", nullable = false, length = 25)
	private String adi;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "tanimi", nullable = false, length = 25)
	private String tanimi;

	// @OneToOne
	// @JoinColumn(name = "ID", referencedColumnName = "diagnose_group",
	// insertable = false, updatable = false)
	// private DestructiveTestsSpec salesDestructiveTestSpec;

	// @OneToOne
	// @JoinColumn(name = "ID", referencedColumnName = "incelemegrubu",
	// insertable = false, updatable = false)
	// private IsolationTestDefinition salesIsolationDefinition;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "incelemeGrubu", cascade = CascadeType.ALL)
	private List<IsolationTestDefinition> salesIsolationDefinition;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "incelemeGrubu", cascade = CascadeType.ALL)
	private List<DestructiveTestsSpec> salesDestructiveTestSpec;

	public IncelemeGrubu() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAdi() {
		return this.adi;
	}

	public void setAdi(String adi) {
		this.adi = adi;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getTanimi() {
		return this.tanimi;
	}

	public void setTanimi(String tanimi) {
		this.tanimi = tanimi;
	}

}