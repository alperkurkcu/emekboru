package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.isolation.IsolationTestDefinition;

/**
 * The persistent class for the test_kaplama_kalinligi_sonuc database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaKalinligiSonuc.findAll", query = "SELECT r FROM TestKaplamaKalinligiSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_kaplama_kalinligi_sonuc")
public class TestKaplamaKalinligiSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KAPLAMA_KALINLIGI_SONUC_ID_GENERATOR", sequenceName = "TEST_KAPLAMA_KALINLIGI_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KAPLAMA_KALINLIGI_SONUC_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Column(name = "aciklama", length = 255)
	private String aciklama;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	// @Column(name = "global_id", nullable = false)
	// private Integer globalId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "ic_kaplama_mi")
	private Boolean IcKaplamaMi;

	@Column(name = "kalinlik_min")
	private BigDecimal KalinlikMin;

	@Column(name = "kalinlik_max")
	private BigDecimal KalinlikMax;

	@Column(name = "kalinlik_ort")
	private BigDecimal KalinlikOrt;

	@Column(name = "vardiya")
	private int vardiya;

	@Column(name = "boya_sistemi")
	private int BoyaSistemi;

	@Column(name = "film_a")
	private BigDecimal FilmA;

	@Column(name = "film_b")
	private BigDecimal FilmB;

	@Column(name = "film_c")
	private BigDecimal FilmC;

	@Column(name = "film_d")
	private BigDecimal FilmD;

	@Column(name = "film_e")
	private BigDecimal FilmE;

	@Column(name = "dis_kaplama_mi")
	private Boolean DisKaplamaMi;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@Column(nullable = false)
	private Integer sonuc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi", nullable = false)
	private Date testTarihi;

	public TestKaplamaKalinligiSonuc() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	/**
	 * @return the vardiya
	 */
	public int getVardiya() {
		return vardiya;
	}

	/**
	 * @param vardiya
	 *            the vardiya to set
	 */
	public void setVardiya(int vardiya) {
		this.vardiya = vardiya;
	}

	/**
	 * @return the boyaSistemi
	 */
	public int getBoyaSistemi() {
		return BoyaSistemi;
	}

	/**
	 * @param boyaSistemi
	 *            the boyaSistemi to set
	 */
	public void setBoyaSistemi(int boyaSistemi) {
		BoyaSistemi = boyaSistemi;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getTestTarihi() {
		return this.testTarihi;
	}

	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}

	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}

	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the kalinlikMin
	 */
	public BigDecimal getKalinlikMin() {
		return KalinlikMin;
	}

	/**
	 * @param kalinlikMin
	 *            the kalinlikMin to set
	 */
	public void setKalinlikMin(BigDecimal kalinlikMin) {
		KalinlikMin = kalinlikMin;
	}

	/**
	 * @return the kalinlikMax
	 */
	public BigDecimal getKalinlikMax() {
		return KalinlikMax;
	}

	/**
	 * @param kalinlikMax
	 *            the kalinlikMax to set
	 */
	public void setKalinlikMax(BigDecimal kalinlikMax) {
		KalinlikMax = kalinlikMax;
	}

	/**
	 * @return the kalinlikOrt
	 */
	public BigDecimal getKalinlikOrt() {
		return KalinlikOrt;
	}

	/**
	 * @param kalinlikOrt
	 *            the kalinlikOrt to set
	 */
	public void setKalinlikOrt(BigDecimal kalinlikOrt) {
		KalinlikOrt = kalinlikOrt;
	}

	/**
	 * @return the icKaplamaMi
	 */
	public Boolean getIcKaplamaMi() {
		return IcKaplamaMi;
	}

	/**
	 * @param icKaplamaMi
	 *            the icKaplamaMi to set
	 */
	public void setIcKaplamaMi(Boolean icKaplamaMi) {
		IcKaplamaMi = icKaplamaMi;
	}

	/**
	 * @return the disKaplamaMi
	 */
	public Boolean getDisKaplamaMi() {
		return DisKaplamaMi;
	}

	/**
	 * @param disKaplamaMi
	 *            the disKaplamaMi to set
	 */
	public void setDisKaplamaMi(Boolean disKaplamaMi) {
		DisKaplamaMi = disKaplamaMi;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the filmA
	 */
	public BigDecimal getFilmA() {
		return FilmA;
	}

	/**
	 * @param filmA
	 *            the filmA to set
	 */
	public void setFilmA(BigDecimal filmA) {
		FilmA = filmA;
	}

	/**
	 * @return the filmB
	 */
	public BigDecimal getFilmB() {
		return FilmB;
	}

	/**
	 * @param filmB
	 *            the filmB to set
	 */
	public void setFilmB(BigDecimal filmB) {
		FilmB = filmB;
	}

	/**
	 * @return the filmC
	 */
	public BigDecimal getFilmC() {
		return FilmC;
	}

	/**
	 * @param filmC
	 *            the filmC to set
	 */
	public void setFilmC(BigDecimal filmC) {
		FilmC = filmC;
	}

	/**
	 * @return the filmD
	 */
	public BigDecimal getFilmD() {
		return FilmD;
	}

	/**
	 * @param filmD
	 *            the filmD to set
	 */
	public void setFilmD(BigDecimal filmD) {
		FilmD = filmD;
	}

	/**
	 * @return the filmE
	 */
	public BigDecimal getFilmE() {
		return FilmE;
	}

	/**
	 * @param filmE
	 *            the filmE to set
	 */
	public void setFilmE(BigDecimal filmE) {
		FilmE = filmE;
	}

	/**
	 * @return the sonuc
	 */
	public Integer getSonuc() {
		return sonuc;
	}

	/**
	 * @param sonuc
	 *            the sonuc to set
	 */
	public void setSonuc(Integer sonuc) {
		this.sonuc = sonuc;
	}

}