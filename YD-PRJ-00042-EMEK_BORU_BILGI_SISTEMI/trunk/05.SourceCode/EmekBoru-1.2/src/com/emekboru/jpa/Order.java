package com.emekboru.jpa;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.emekboru.config.Config;
import com.emekboru.config.Status;
import com.emekboru.jpa.isolation.IsolationTestDefinition;

@NamedQueries({
		@NamedQuery(name = "totalOrder", query = "SELECT COUNT(u) FROM Order u"),
		@NamedQuery(name = "Order.findAll", query = "SELECT u FROM Order u where u.ended=:prmEnded order by u.orderYear, u.orderNumber") })
@Entity
@Table(name = "order_c")
public class Order implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "order_id")
	@SequenceGenerator(name = "ORDER_GENERATOR", sequenceName = "ORDER_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ORDER_GENERATOR")
	private int orderId;

	private double boy;

	@Column(name = "dis_cap")
	private double disCap;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_date")
	private Date endDate;

	@Column(name = "estimated_duration_time")
	private int estimatedDurationTime;

	@Column(name = "et_kalinligi")
	private double etKalinligi;

	@Column(name = "kalite")
	private String kalite;

	@Column(name = "miktar")
	private double miktar;

	@Column(name = "note")
	private String note;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_date")
	private Date startDate;

	@Column(name = "tezgah_hizi")
	private String tezgahHizi;

	@Column(name = "toplan_metraj")
	private double toplanMetraj;

	@Column(name = "pipe_type")
	private String pipeType;

	@Column(name = "isletme_basinci")
	private double isletmeBasinci;

	@Column(name = "status_id")
	private int statusId;

	@Column(name = "order_year")
	private int orderYear;

	@Column(name = "order_number")
	private int orderNumber;

	@Column(name = "tender_offer_id")
	private int tenderOfferId;

	@Column(name = "internal")
	private boolean internal;

	@Column(name = "external")
	private boolean external;

	@Column(name = "ended")
	private boolean ended;

	@Transient
	private int customerId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "customer_id", referencedColumnName = "customer_id")
	private Customer customer;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "order", cascade = CascadeType.PERSIST, orphanRemoval = true)
	private List<PlannedIsolation> plannedIsolation;

	// coating
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "order")
	private List<IsolationTestDefinition> isolationTestDefinitions;

	// @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	// "order")
	// private List<AgirlikTestsSpec> agirlikTestsSpecs;

	// @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	// "order")
	// private List<BukmeTestsSpec> bukmeTestsSpecs;
	//
	// @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	// "order")
	// private List<CekmeTestsSpec> cekmeTestsSpecs;

	// @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	// "order")
	// private List<CentikDarbeTestsSpec> centikDarbeTestsSpecs;

	// @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	// "order")
	// private List<DestructiveTestsSpec> destructiveTestsSpecs;

	// @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	// "order")
	// private List<HidrostaticTestsSpec> hidrostaticTestsSpecs;

	// @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	// "order")
	// private List<KaynakMakroTestSpec> kaynakMakroTestSpecs;

	// @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	// "order")
	// private List<KaynakSertlikTestsSpec> kaynakSertlikTestsSpecs;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "order")
	private List<NondestructiveTestsSpec> nondestructiveTestsSpecs;

	// @OneToMany(cascade = CascadeType.ALL, mappedBy = "order")
	// private List<Pipe> pipes;

	// @OneToOne(mappedBy = "order", cascade = CascadeType.ALL, fetch =
	// FetchType.EAGER, orphanRemoval = true)
	// private BukmeTestsSpec bukme;

	// we keep just the employee id. The person responsible for the orderItem.
	// it maybe the same or different with the order's responsible.
	@Column(name = "responsible_employee_id")
	private int responsibleEmployee;

	@Transient
	private Status status;

	@Transient
	private PlannedIsolation internalIsolation;

	@Transient
	private PlannedIsolation externalIsolation;

	@Column(name = "sub_by_username")
	private String username;

	@Transient
	private String orderYearAndOrderNumber;

	// @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy =
	// "selectedPipeRulo")
	// private Rulo selectedRulo;

	public Order() {
		customer = new Customer();
		// qualitySpec = new QualitySpec();
	}

	public Status getStatus(Config config) {

		status = config.getOrder().findStatus(statusId);
		return status;
	}

	@Override
	public boolean equals(Object obj) {

		if (!(obj instanceof Order))
			return false;

		if (orderId == ((Order) obj).getOrderId())
			return true;

		return false;
	}

	public boolean isInternal() {
		return internal;
	}

	public void setInternal(boolean internal) {
		this.internal = internal;
	}

	public boolean isExternal() {
		return external;
	}

	public void setExternal(boolean external) {
		this.external = external;
	}

	public double getBoy() {
		return this.boy;
	}

	public void setBoy(double boy) {
		this.boy = boy;
	}

	public double getDisCap() {
		return this.disCap;
	}

	public void setDisCap(double disCap) {
		this.disCap = disCap;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getEstimatedDurationTime() {
		return this.estimatedDurationTime;
	}

	public void setEstimatedDurationTime(Integer estimatedDurationTime) {
		this.estimatedDurationTime = estimatedDurationTime;
	}

	public double getEtKalinligi() {
		return this.etKalinligi;
	}

	public void setEtKalinligi(double etKalinligi) {
		this.etKalinligi = etKalinligi;
	}

	public String getKalite() {
		return this.kalite;
	}

	public void setKalite(String kalite) {
		this.kalite = kalite;
	}

	public double getMiktar() {
		return this.miktar;
	}

	public void setMiktar(double miktar) {
		this.miktar = miktar;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getTezgahHizi() {
		return this.tezgahHizi;
	}

	public void setTezgahHizi(String tezgahHizi) {
		this.tezgahHizi = tezgahHizi;
	}

	public double getToplanMetraj() {
		return this.toplanMetraj;
	}

	public void setToplanMetraj(double toplanMetraj) {
		this.toplanMetraj = toplanMetraj;
	}

	public double getIsletmeBasinci() {
		return isletmeBasinci;
	}

	public void setIsletmeBasinci(double isletmeBasinci) {
		this.isletmeBasinci = isletmeBasinci;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getResponsibleEmployee() {
		return responsibleEmployee;
	}

	public void setResponsibleEmployee(int responsibleEmployee) {
		this.responsibleEmployee = responsibleEmployee;
	}

	public String getPipeType() {
		return pipeType;
	}

	public void setPipeType(String pipeType) {
		this.pipeType = pipeType;
	}

	public List<PlannedIsolation> getPlannedIsolation() {
		return plannedIsolation;
	}

	public void setPlannedIsolation(List<PlannedIsolation> plannedIsolation) {
		this.plannedIsolation = plannedIsolation;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public int getOrderYear() {
		return orderYear;
	}

	public void setOrderYear(int orderYear) {
		this.orderYear = orderYear;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public int getTenderOfferId() {
		return tenderOfferId;
	}

	public void setTenderOfferId(int tenderOfferId) {
		this.tenderOfferId = tenderOfferId;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public void setEstimatedDurationTime(int estimatedDurationTime) {
		this.estimatedDurationTime = estimatedDurationTime;
	}

	// public QualitySpec getQualitySpec() {
	// return qualitySpec;
	// }
	//
	// public void setQualitySpec(QualitySpec qualitySpec) {
	// this.qualitySpec = qualitySpec;
	// }

	// public List<DestructiveTestsSpec> getDestructiveTestsSpecs() {
	// return destructiveTestsSpecs;
	// }
	//
	// public void setDestructiveTestsSpecs(
	// List<DestructiveTestsSpec> destructiveTestsSpecs) {
	// this.destructiveTestsSpecs = destructiveTestsSpecs;
	// }

	public List<NondestructiveTestsSpec> getNondestructiveTestsSpecs() {
		return nondestructiveTestsSpecs;
	}

	public void setNondestructiveTestsSpecs(
			List<NondestructiveTestsSpec> nondestructiveTestsSpecs) {
		this.nondestructiveTestsSpecs = nondestructiveTestsSpecs;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public PlannedIsolation getInternalIsolation() {
		return internalIsolation;
	}

	public void setInternalIsolation(PlannedIsolation internalIsolation) {
		this.internalIsolation = internalIsolation;
	}

	public PlannedIsolation getExternalIsolation() {
		return externalIsolation;
	}

	public void setExternalIsolation(PlannedIsolation externalIsolation) {
		this.externalIsolation = externalIsolation;
	}

	public boolean isEnded() {
		return ended;
	}

	public void setEnded(boolean ended) {
		this.ended = ended;
	}

	public List<IsolationTestDefinition> getIsolationTestDefinitions() {
		return isolationTestDefinitions;
	}

	public void setIsolationTestDefinitions(
			List<IsolationTestDefinition> isolationTestDefinitions) {
		this.isolationTestDefinitions = isolationTestDefinitions;
	}

	public String getOrderYearAndOrderNumber() {
		return this.getOrderYear() + "/" + this.getOrderNumber();
	}

	public void setOrderYearAndOrderNumber(String orderYearAndOrderNumber) {
		this.orderYearAndOrderNumber = orderYearAndOrderNumber;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

}