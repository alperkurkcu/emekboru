package com.emekboru.jpa.rulo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "rulo_ozellik_boyutsal")
public class RuloOzellikBoyutsal {

	@Id
	@SequenceGenerator(name = "rulo_ozellik_boyutsal_generator", sequenceName = "rulo_ozellik_boyutsal_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rulo_ozellik_boyutsal_generator")
	@Column(name = "rulo_boyutsal_ozellik_id")
	private Integer ruloOzellikBoyutsalId;

	@Column(name = "rulo_boyutsal_ozellik_et_kalinligi")
	private Float ruloOzellikBoyutsalEtKalinligi;

	@Column(name = "rulo_boyutsal_ozellik_olculen_et_kalinligi")
	private Float ruloOzellikBoyutsalOlculenEtKalinligi;

	@Column(name = "rulo_boyutsal_ozellik_genislik")
	private Float ruloOzellikBoyutsalGenislik;

	@Column(name = "rulo_boyutsal_ozellik_olculen_genislik")
	private Float ruloOzellikBoyutsalOlculenGenislik;

	@Column(name = "rulo_boyutsal_ozellik_miktar")
	private Float ruloOzellikBoyutsalMiktar;

	@Column(name = "rulo_boyutsal_ozellik_kalan_miktar")
	private Float ruloOzellikBoyutsalKalanMiktar;

	@OneToOne(mappedBy = "ruloOzellikBoyutsal", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	private Rulo rulo;

	@Column(name = "durum")
	private Integer durum = 0;

	@Column(name = "islem_yapan_id")
	private int islemYapanId;

	@Column(name = "islem_yapan_isim")
	private String islemYapanIsim;

	public int getIslemYapanId() {
		return islemYapanId;
	}

	public void setIslemYapanId(int islemYapanId) {
		this.islemYapanId = islemYapanId;
	}

	public String getIslemYapanIsim() {
		return islemYapanIsim;
	}

	public void setIslemYapanIsim(String islemYapanIsim) {
		this.islemYapanIsim = islemYapanIsim;
	}

	public Integer getDurum() {
		return durum;
	}

	public void setDurum(Integer durum) {
		this.durum = durum;
	}

	public Rulo getRulo() {
		return rulo;
	}

	public void setRulo(Rulo rulo) {
		this.rulo = rulo;
	}

	public Integer getRuloOzellikBoyutsalId() {
		return ruloOzellikBoyutsalId;
	}

	public void setRuloOzellikBoyutsalId(Integer ruloOzellikBoyutsalId) {
		this.ruloOzellikBoyutsalId = ruloOzellikBoyutsalId;
	}

	public Float getRuloOzellikBoyutsalEtKalinligi() {
		return ruloOzellikBoyutsalEtKalinligi;
	}

	public void setRuloOzellikBoyutsalEtKalinligi(
			Float ruloOzellikBoyutsalEtKalinligi) {
		this.ruloOzellikBoyutsalEtKalinligi = ruloOzellikBoyutsalEtKalinligi;
	}

	public Float getRuloOzellikBoyutsalOlculenEtKalinligi() {
		return ruloOzellikBoyutsalOlculenEtKalinligi;
	}

	public void setRuloOzellikBoyutsalOlculenEtKalinligi(
			Float ruloOzellikBoyutsalOlculenEtKalinligi) {
		this.ruloOzellikBoyutsalOlculenEtKalinligi = ruloOzellikBoyutsalOlculenEtKalinligi;
	}

	public Float getRuloOzellikBoyutsalGenislik() {
		return ruloOzellikBoyutsalGenislik;
	}

	public void setRuloOzellikBoyutsalGenislik(Float ruloOzellikBoyutsalGenislik) {
		this.ruloOzellikBoyutsalGenislik = ruloOzellikBoyutsalGenislik;
	}

	public Float getRuloOzellikBoyutsalOlculenGenislik() {
		return ruloOzellikBoyutsalOlculenGenislik;
	}

	public void setRuloOzellikBoyutsalOlculenGenislik(
			Float ruloOzellikBoyutsalOlculenGenislik) {
		this.ruloOzellikBoyutsalOlculenGenislik = ruloOzellikBoyutsalOlculenGenislik;
	}

	public Float getRuloOzellikBoyutsalMiktar() {
		return ruloOzellikBoyutsalMiktar;
	}

	public void setRuloOzellikBoyutsalMiktar(Float ruloOzellikBoyutsalMiktar) {
		this.ruloOzellikBoyutsalMiktar = ruloOzellikBoyutsalMiktar;
	}

	public Float getRuloOzellikBoyutsalKalanMiktar() {
		return ruloOzellikBoyutsalKalanMiktar;
	}

	public void setRuloOzellikBoyutsalKalanMiktar(
			Float ruloOzellikBoyutsalKalanMiktar) {
		this.ruloOzellikBoyutsalKalanMiktar = ruloOzellikBoyutsalKalanMiktar;
	}

}
