package com.emekboru.jpa;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the machine database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "Machine.findAll", query = "SELECT m FROM Machine m") })
@Table(name = "machine")
public class Machine implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "machine_id")
	@SequenceGenerator(name = "MACHINE_GENERATOR", sequenceName = "MACHINE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MACHINE_GENERATOR")
	private int machineId;

	private String brand;

	@Temporal(TemporalType.DATE)
	@Column(name = "incoming_date")
	private Date incomingDate;

	private String name;

	private String model;

	private Double value;

	@Column(name = "description")
	private String description;

	private Integer status;

	@Column(name = "serial_number")
	private String serialNumber;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "machine_type_id", referencedColumnName = "machine_type_id")
	private MachineType machineType;

	@OneToMany(mappedBy = "machine", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<MachinePart> machineParts;

	@OneToMany(mappedBy = "machine", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<MachineEdwLink> machineEdwLinks;

	@OneToMany(mappedBy = "machine", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<MachineRuloLink> machineRuloLinks;

	@OneToMany(mappedBy = "machine", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<MachinePipeLink> machinePipeLinks;

	@Column(name = "type")
	private Integer type;

	public Machine() {
		machineType = new MachineType();
	}

	@Override
	public boolean equals(Object obj) {

		if (!(obj instanceof Machine))
			return false;

		if (this.machineId == ((Machine) obj).machineId)
			return true;

		return false;
	}

	/**
	 * GETTERS AND SETTERS
	 */
	public int getMachineId() {
		return this.machineId;
	}

	public void setMachineId(int machineId) {
		this.machineId = machineId;
	}

	public String getBrand() {
		return this.brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public Date getIncomingDate() {
		return this.incomingDate;
	}

	public void setIncomingDate(Date incomingDate) {
		this.incomingDate = incomingDate;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public MachineType getMachineType() {
		return machineType;
	}

	public void setMachineType(MachineType machineType) {
		this.machineType = machineType;
	}

	public List<MachinePart> getMachineParts() {
		return machineParts;
	}

	public void setMachineParts(List<MachinePart> machineParts) {
		this.machineParts = machineParts;
	}

	public List<MachineEdwLink> getMachineEdwLinks() {
		return machineEdwLinks;
	}

	public void setMachineEdwLinks(List<MachineEdwLink> machineEdwLinks) {
		this.machineEdwLinks = machineEdwLinks;
	}

	public List<MachineRuloLink> getMachineRuloLinks() {
		return machineRuloLinks;
	}

	public void setMachineRuloLinks(List<MachineRuloLink> machineRuloLinks) {
		this.machineRuloLinks = machineRuloLinks;
	}

	public List<MachinePipeLink> getMachinePipeLinks() {
		return machinePipeLinks;
	}

	public void setMachinePipeLinks(List<MachinePipeLink> machinePipeLinks) {
		this.machinePipeLinks = machinePipeLinks;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

}