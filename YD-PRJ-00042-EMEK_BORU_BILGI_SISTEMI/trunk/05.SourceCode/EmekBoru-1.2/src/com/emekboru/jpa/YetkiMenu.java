package com.emekboru.jpa;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the yetki_menu database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "YetkiMenu.findAll", query = "select o from YetkiMenu o order by o.ekranTanimi ASC"),
		@NamedQuery(name = "YetkiMenu.findByMenuDurum", query = "select o from YetkiMenu o where o.menuDurum='A' order by o.menuSira "),
		@NamedQuery(name = "YetkiMenu.findAllPages", query = "select o from YetkiMenu o where o.isSubMenu='f' ") })
@Table(name = "yetki_menu")
public class YetkiMenu implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "YETKI_MENU_GENERATOR", sequenceName = "yetki_menu_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "YETKI_MENU_GENERATOR")
	@Column(name = "ID")
	private Integer id;

	@Column(name = "action_sinif_metot")
	private String actionSinifMetot;

	@Column(name = "action_link_ad")
	private String actionLinkAd;

	@Column(name = "bas_tar")
	private Timestamp basTar;

	@Column(name = "bit_tar")
	private Timestamp bitTar;

	@Column(name = "ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@Column(name = "ekran_tanimi")
	private String ekranTanimi;

	// @Column(name="pageAbbr")
	// private String pageAbbr;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "menu_durum")
	private boolean menuDurum = true;

	@Column(name = "menu_resim")
	private String menuResim;

	@Column(name = "menu_sira")
	private Integer menuSira;

	@Column(name = "modul_no")
	private Integer modulNo;

	@Column(name = "parent_menu_id")
	private Integer parentMenuId;

	@Column(name = "is_submenu")
	private boolean isSubMenu;

	public YetkiMenu() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getActionSinifMetot() {
		return actionSinifMetot;
	}

	public void setActionSinifMetot(String actionSinifMetot) {
		this.actionSinifMetot = actionSinifMetot;
	}

	public Timestamp getBasTar() {
		return basTar;
	}

	public void setBasTar(Timestamp basTar) {
		this.basTar = basTar;
	}

	public Timestamp getBitTar() {
		return bitTar;
	}

	public void setBitTar(Timestamp bitTar) {
		this.bitTar = bitTar;
	}

	public Timestamp getEklemeZamani() {
		return eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public String getEkranTanimi() {
		return ekranTanimi;
	}

	public void setEkranTanimi(String ekranTanimi) {
		this.ekranTanimi = ekranTanimi;
	}

	public Timestamp getGuncellemeZamani() {
		return guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public boolean isMenuDurum() {
		return menuDurum;
	}

	public void setMenuDurum(boolean menuDurum) {
		this.menuDurum = menuDurum;
	}

	public String getMenuResim() {
		return menuResim;
	}

	public void setMenuResim(String menuResim) {
		this.menuResim = menuResim;
	}

	public Integer getMenuSira() {
		return menuSira;
	}

	public void setMenuSira(Integer menuSira) {
		this.menuSira = menuSira;
	}

	public Integer getModulNo() {
		return modulNo;
	}

	public void setModulNo(Integer modulNo) {
		this.modulNo = modulNo;
	}

	public Integer getParentMenuId() {
		return parentMenuId;
	}

	public void setParentMenuId(Integer parentMenuId) {
		this.parentMenuId = parentMenuId;
	}

	public boolean isSubMenu() {
		return isSubMenu;
	}

	public void setSubMenu(boolean isSubMenu) {
		this.isSubMenu = isSubMenu;
	}

	public String getActionLinkAd() {
		return actionLinkAd;
	}

	public void setActionLinkAd(String actionLinkAd) {
		this.actionLinkAd = actionLinkAd;
	}

}