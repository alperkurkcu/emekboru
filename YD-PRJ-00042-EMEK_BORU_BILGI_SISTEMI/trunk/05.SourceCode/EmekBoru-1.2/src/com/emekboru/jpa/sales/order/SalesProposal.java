package com.emekboru.jpa.sales.order;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.emekboru.jpa.Employee;
import com.emekboru.jpa.sales.customer.SalesCustomer;

@Entity
@Table(name = "sales_proposal")
public class SalesProposal implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4116751507959392629L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_PROPOSAL_GENERATOR")
	@SequenceGenerator(name = "SALES_PROPOSAL_GENERATOR", sequenceName = "SALES_PROPOSAL_SEQUENCE", allocationSize = 1)
	@Column(name = "proposal_id")
	private int proposalId;

	@Column(name = "proposal_number")
	private String proposalNumber;

	@Column(name = "reference_number")
	private String referenceNumber;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "customer", referencedColumnName = "sales_customer_id")
	private SalesCustomer customer;

	@Temporal(TemporalType.DATE)
	@Column(name = "request_date")
	private Date requestDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "form_date")
	private Date formDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "post_date")
	private Date postDate;

	@Column(name = "validity_period")
	private String validityPeriod;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "currency_unit", referencedColumnName = "currency_id")
	private SalesCurrency currencyUnit;

	@Column(name = "requested_documents")
	private String requestedDocuments;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "status", referencedColumnName = "proposal_status_id")
	private SalesProposalStatus status;

	@Column(name = "notes")
	private String notes;

	@Column(name = "approval_one")
	private boolean approval1;

	@Column(name = "approval_two")
	private boolean approval2;

	@Column(name = "proposal_language")
	private String proposalLanguage;

	@Column(name = "revision_number")
	private String revisionNumber;

	@Column(name = "revision_notes")
	private String revisionNotes;

	/* JOIN FUNCTIONS */
	@Transient
	private List<Employee> employeeList;
	/* end of JOIN FUNCTIONS */

	@OneToMany(mappedBy = "proposal")
	private List<SalesItem> itemList;

	// entegrasyon

	@OneToOne(mappedBy = "proposal")
	private SalesOrder salesOrder;

	// entegrasyon

	@Column(name = "documents")
	private String documents;

	@Transient
	private List<String> fileNames;

	@Transient
	private int fileNumber;

	public SalesProposal() {
		approval1 = false;
		approval2 = false;
		fileNumber = 0;
	}

	@Override
	public boolean equals(Object o) {
		return this.getProposalId() == ((SalesProposal) o).getProposalId();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public int getProposalId() {
		return proposalId;
	}

	public String getProposalNumber() {
		return proposalNumber;
	}

	public void setProposalNumber(String proposalNumber) {
		this.proposalNumber = proposalNumber;
	}

	public SalesCustomer getCustomer() {
		return customer;
	}

	public void setCustomer(SalesCustomer customer) {
		this.customer = customer;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Date getFormDate() {
		return formDate;
	}

	public void setFormDate(Date formDate) {
		this.formDate = formDate;
	}

	public Date getPostDate() {
		return postDate;
	}

	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}

	public String getValidityPeriod() {
		return validityPeriod;
	}

	public void setValidityPeriod(String validityPeriod) {
		this.validityPeriod = validityPeriod;
	}

	public SalesCurrency getCurrencyUnit() {
		return currencyUnit;
	}

	public void setCurrencyUnit(SalesCurrency currencyUnit) {
		this.currencyUnit = currencyUnit;
	}

	public String getRequestedDocuments() {
		return requestedDocuments;
	}

	public void setRequestedDocuments(String requestedDocuments) {
		this.requestedDocuments = requestedDocuments;
	}

	public SalesProposalStatus getStatus() {
		return status;
	}

	public void setStatus(SalesProposalStatus status) {
		this.status = status;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public boolean getApproval1() {
		return approval1;
	}

	public void setApproval1(boolean approval1) {
		this.approval1 = approval1;
	}

	public boolean getApproval2() {
		return approval2;
	}

	public void setApproval2(boolean approval2) {
		this.approval2 = approval2;
	}

	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}

	public List<SalesItem> getItemList() {
		return itemList;
	}

	public void setItemList(List<SalesItem> itemList) {
		this.itemList = itemList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setProposalId(int proposalId) {
		this.proposalId = proposalId;
	}

	public SalesOrder getSalesOrder() {
		return salesOrder;
	}

	public void setSalesOrder(SalesOrder salesOrder) {
		this.salesOrder = salesOrder;
	}

	public String getDocuments() {
		return documents;
	}

	public void setDocuments(String documents) {
		this.documents = documents;
	}

	public List<String> getFileNames() {
		try {
			String[] names = documents.split("//");
			fileNames = new ArrayList<String>();

			for (int i = 1; i < names.length; i++) {
				fileNames.add(names[i]);
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return fileNames;
	}

	public void setFileNames(List<String> fileNames) {
		this.fileNames = fileNames;
	}

	public int getFileNumber() {
		try {
			String[] names = documents.split("//");

			fileNumber = names.length - 1;
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return fileNumber;
	}

	public String getProposalLanguage() {
		return proposalLanguage;
	}

	public void setProposalLanguage(String proposalLanguage) {
		this.proposalLanguage = proposalLanguage;
	}

	public String getRevisionNumber() {
		return revisionNumber;
	}

	public void setRevisionNumber(String revisionNumber) {
		this.revisionNumber = revisionNumber;
	}

	public String getRevisionNotes() {
		return revisionNotes;
	}

	public void setRevisionNotes(String revisionNotes) {
		this.revisionNotes = revisionNotes;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
}
