package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "other_cost_item")
public class OtherCostItem implements Serializable {

	private static final long serialVersionUID = -6889426897193906204L;

	@Id
	@Column(name = "other_cost_item_id")
	private Integer otherCostItemId;

	@Column(name = "name")
	private String name;

	@Column(name = "short_desc")
	private String shortDesc;

	@Column(name = "cost")
	private Double cost;

	@Column(name = "as_percentage")
	private Boolean asPercentage;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "currency_code", referencedColumnName = "currency_code", insertable = false, updatable = false)
	private Currency currency;

	public OtherCostItem() {

	}

	/**
	 * GETTERS AND SETTERS
	 */
	public Integer getOtherCostItemId() {
		return otherCostItemId;
	}

	public void setOtherCostItemId(Integer otherCostItemId) {
		this.otherCostItemId = otherCostItemId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	public Boolean getAsPercentage() {
		return asPercentage;
	}

	public void setAsPercentage(Boolean asPercentage) {
		this.asPercentage = asPercentage;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
