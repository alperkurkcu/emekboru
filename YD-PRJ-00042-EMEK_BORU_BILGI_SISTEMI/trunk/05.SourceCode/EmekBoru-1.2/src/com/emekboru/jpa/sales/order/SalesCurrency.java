package com.emekboru.jpa.sales.order;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "sales_currency")
public class SalesCurrency implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5650891833117086353L;

	@Id
	@Column(name = "currency_id")
	private int currencyId;

	@Column(name = "title")
	private String name;

	@OneToMany
	private List<SalesProposal> proposalList;

	@OneToMany
	private List<SalesItem> itemList;

	public SalesCurrency() {
	}

	@Override
	public boolean equals(Object o) {
		return this.getCurrencyId() == ((SalesCurrency) o).getCurrencyId();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public int getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(int currencyId) {
		this.currencyId = currencyId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SalesProposal> getProposalList() {
		return proposalList;
	}

	public void setProposalList(List<SalesProposal> proposalList) {
		this.proposalList = proposalList;
	}

	public List<SalesItem> getItemList() {
		return itemList;
	}

	public void setItemList(List<SalesItem> itemList) {
		this.itemList = itemList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
