package com.emekboru.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the repairs database table.
 * 
 */
@Entity
@Table(name="repairs")
public class Repair implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="repair_id")
	private Integer repairId;

    @Temporal( TemporalType.DATE)
	private Date date;

	private String description;

	@Column(name="pipe_coat_id")
	private Integer pipeCoatId;

    public Repair() {
    }

	public Integer getRepairId() {
		return this.repairId;
	}

	public void setRepairId(Integer repairId) {
		this.repairId = repairId;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getPipeCoatId() {
		return this.pipeCoatId;
	}

	public void setPipeCoatId(Integer pipeCoatId) {
		this.pipeCoatId = pipeCoatId;
	}

}