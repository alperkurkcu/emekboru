package com.emekboru.jpa.listeners;

import javax.persistence.PostLoad;

import com.emekboru.jpa.Order;
import com.emekboru.jpa.PlannedIsolation;
import com.emekboru.utils.IsolationType;

public class OrderListener {

	@PostLoad
	public void onLoad(Order order) {

		for (PlannedIsolation i : order.getPlannedIsolation()) {

			if (i.getInternalExternal() == IsolationType.IC_KAPLAMA.getType())
				order.setInternalIsolation(i);

			if (i.getInternalExternal() == IsolationType.DIS_KAPLAMA.getType())
				order.setExternalIsolation(i);
		}

	}
}
