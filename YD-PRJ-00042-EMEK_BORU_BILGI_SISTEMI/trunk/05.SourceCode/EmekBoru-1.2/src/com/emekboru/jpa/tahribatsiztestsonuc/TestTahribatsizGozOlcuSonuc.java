package com.emekboru.jpa.tahribatsiztestsonuc;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.rulo.RuloPipeLink;

/**
 * The persistent class for the test_tahribatsiz_goz_olcu_sonuc database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "TestTahribatsizGozOlcuSonuc.findAll", query = "SELECT r FROM TestTahribatsizGozOlcuSonuc r WHERE r.pipe.pipeId=:prmPipeId"),
		@NamedQuery(name = "TestTahribatsizGozOlcuSonuc.getByDokumNo", query = "SELECT r FROM TestTahribatsizGozOlcuSonuc r WHERE r.pipe.ruloPipeLink.rulo.ruloDokumNo=:prmDokumNo") })
@Table(name = "test_tahribatsiz_goz_olcu_sonuc")
public class TestTahribatsizGozOlcuSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_TAHRIBATSIZ_GOZ_OLCU_SONUC_ID_GENERATOR", sequenceName = "TEST_TAHRIBATSIZ_GOZ_OLCU_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_TAHRIBATSIZ_GOZ_OLCU_SONUC_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "a_ucu_alin_yuksekligi")
	private BigDecimal aUcuAlinYuksekligi;

	@Column(name = "a_ucu_boru_ucu_dik")
	private BigDecimal aUcuBoruUcuDik;

	@Column(name = "a_ucu_dis_cap")
	private BigDecimal aUcuDisCap;

	@Column(name = "a_ucu_ic_cap_cevresel")
	private BigDecimal aUcuIcCapCevresel;

	@Column(name = "a_ucu_ic_cap")
	private BigDecimal aUcuIcCap;

	@Column(name = "a_ucu_kaynak_agizi_acisi")
	private BigDecimal aUcuKaynakAgiziAcisi;

	@Column(name = "a_ucu_ovallik_cap_max")
	private BigDecimal aUcuOvallikCapMax;

	@Column(name = "a_ucu_ovallik_cap_min")
	private BigDecimal aUcuOvallikCapMin;

	@Column(length = 255, name = "aciklama")
	private String aciklama;

	@Column(name = "b_ucu_alin_yuksekligi")
	private BigDecimal bUcuAlinYuksekligi;

	@Column(name = "b_ucu_boru_ucu_dik")
	private BigDecimal bUcuBoruUcuDik;

	@Column(name = "b_ucu_dis_cap")
	private BigDecimal bUcuDisCap;

	@Column(name = "b_ucu_ic_cap")
	private BigDecimal bUcuIcCap;

	@Column(name = "b_ucu_ic_cap_cevresel")
	private BigDecimal bUcuIcCapCevresel;

	@Column(name = "b_ucu_kaynak_agizi_acisi")
	private BigDecimal bUcuKaynakAgiziAcisi;

	@Column(name = "b_ucu_ovallik_cap_max")
	private BigDecimal bUcuOvallikCapMax;

	@Column(name = "b_ucu_ovallik_cap_min")
	private BigDecimal bUcuOvallikCapMin;

	@Column(name = "boru_agirlik")
	private BigDecimal boruAgirlik;

	@Column(name = "boru_uzunluk")
	private BigDecimal boruUzunluk;

	@Column(name = "dogrusallik")
	private BigDecimal dogrusallik;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	// @Column(name = "et_kalinligi")
	// private BigDecimal etKalinligi;

	@Column(name = "govde_dis_cap_max")
	private BigDecimal govdeDisCapMax;

	@Column(name = "govde_dis_cap_min")
	private BigDecimal govdeDisCapMin;

	@Column(name = "govde_ovallik_cap_max")
	private BigDecimal govdeOvallikCapMax;

	@Column(name = "govde_ovallik_cap_min")
	private BigDecimal govdeOvallikCapMin;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "kalici_manyetizma")
	private BigDecimal kaliciManyetizma;

	@Basic
	@Column(name = "pipe_id", nullable = false, insertable = false, updatable = false)
	private Integer pipeId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@Column(name = "radyal_kaciklik")
	private BigDecimal radyalKaciklik;

	@Column(name = "ic_kaynak_dikisi_yuksekligi_min")
	private BigDecimal icKaynakDikisiYuksekligiMin;

	@Column(name = "ic_kaynak_dikisi_yuksekligi_max")
	private BigDecimal icKaynakDikisiYuksekligiMax;

	@Column(name = "dis_kaynak_dikisi_yuksekligi_min")
	private BigDecimal disKaynakDikisiYuksekligiMin;

	@Column(name = "dis_kaynak_dikisi_yuksekligi_max")
	private BigDecimal disKaynakDikisiYuksekligiMax;

	private Integer result;

	@Column(name = "test_id")
	private Integer testId;

	// @Column(name = "manuel_ut_id")
	// private Integer manuelUtId;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "manuel_ut_id", referencedColumnName = "ID", insertable = false, updatable = true)
	private TestTahribatsizManuelUtSonuc testTahribatsizManuelUtSonuc;

	// @Column(name = "fl_id")
	// private Integer flId;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fl_id", referencedColumnName = "ID", insertable = false, updatable = true)
	private TestTahribatsizFloroskopikSonuc testTahribatsizFloroskopikSonuc;

	@Column(name = "durum")
	private Boolean durum;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	private RuloPipeLink ruloPipeLink;

	@Column(name = "et_kalinligi_min")
	private BigDecimal etKalinligiMin;

	@Column(name = "et_kalinligi_max")
	private BigDecimal etKalinligiMax;

	@Column(name = "dent")
	private BigDecimal dent;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "boru_kabul_tarihi")
	private Date boruKabulTarihi;

	public TestTahribatsizGozOlcuSonuc() {
	}

	// getters setters

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the aUcuAlinYuksekligi
	 */
	public BigDecimal getaUcuAlinYuksekligi() {
		return aUcuAlinYuksekligi;
	}

	/**
	 * @param aUcuAlinYuksekligi
	 *            the aUcuAlinYuksekligi to set
	 */
	public void setaUcuAlinYuksekligi(BigDecimal aUcuAlinYuksekligi) {
		this.aUcuAlinYuksekligi = aUcuAlinYuksekligi;
	}

	/**
	 * @return the aUcuBoruUcuDik
	 */
	public BigDecimal getaUcuBoruUcuDik() {
		return aUcuBoruUcuDik;
	}

	/**
	 * @param aUcuBoruUcuDik
	 *            the aUcuBoruUcuDik to set
	 */
	public void setaUcuBoruUcuDik(BigDecimal aUcuBoruUcuDik) {
		this.aUcuBoruUcuDik = aUcuBoruUcuDik;
	}

	/**
	 * @return the aUcuDisCap
	 */
	public BigDecimal getaUcuDisCap() {
		return aUcuDisCap;
	}

	/**
	 * @param aUcuDisCap
	 *            the aUcuDisCap to set
	 */
	public void setaUcuDisCap(BigDecimal aUcuDisCap) {
		this.aUcuDisCap = aUcuDisCap;
	}

	/**
	 * @return the aUcuKaynakAgiziAcisi
	 */
	public BigDecimal getaUcuKaynakAgiziAcisi() {
		return aUcuKaynakAgiziAcisi;
	}

	/**
	 * @param aUcuKaynakAgiziAcisi
	 *            the aUcuKaynakAgiziAcisi to set
	 */
	public void setaUcuKaynakAgiziAcisi(BigDecimal aUcuKaynakAgiziAcisi) {
		this.aUcuKaynakAgiziAcisi = aUcuKaynakAgiziAcisi;
	}

	/**
	 * @return the aUcuOvallikCapMax
	 */
	public BigDecimal getaUcuOvallikCapMax() {
		return aUcuOvallikCapMax;
	}

	/**
	 * @param aUcuOvallikCapMax
	 *            the aUcuOvallikCapMax to set
	 */
	public void setaUcuOvallikCapMax(BigDecimal aUcuOvallikCapMax) {
		this.aUcuOvallikCapMax = aUcuOvallikCapMax;
	}

	/**
	 * @return the aUcuOvallikCapMin
	 */
	public BigDecimal getaUcuOvallikCapMin() {
		return aUcuOvallikCapMin;
	}

	/**
	 * @param aUcuOvallikCapMin
	 *            the aUcuOvallikCapMin to set
	 */
	public void setaUcuOvallikCapMin(BigDecimal aUcuOvallikCapMin) {
		this.aUcuOvallikCapMin = aUcuOvallikCapMin;
	}

	/**
	 * @return the aciklama
	 */
	public String getAciklama() {
		return aciklama;
	}

	/**
	 * @param aciklama
	 *            the aciklama to set
	 */
	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	/**
	 * @return the bUcuAlinYuksekligi
	 */
	public BigDecimal getbUcuAlinYuksekligi() {
		return bUcuAlinYuksekligi;
	}

	/**
	 * @param bUcuAlinYuksekligi
	 *            the bUcuAlinYuksekligi to set
	 */
	public void setbUcuAlinYuksekligi(BigDecimal bUcuAlinYuksekligi) {
		this.bUcuAlinYuksekligi = bUcuAlinYuksekligi;
	}

	/**
	 * @return the bUcuBoruUcuDik
	 */
	public BigDecimal getbUcuBoruUcuDik() {
		return bUcuBoruUcuDik;
	}

	/**
	 * @param bUcuBoruUcuDik
	 *            the bUcuBoruUcuDik to set
	 */
	public void setbUcuBoruUcuDik(BigDecimal bUcuBoruUcuDik) {
		this.bUcuBoruUcuDik = bUcuBoruUcuDik;
	}

	/**
	 * @return the bUcuDisCap
	 */
	public BigDecimal getbUcuDisCap() {
		return bUcuDisCap;
	}

	/**
	 * @param bUcuDisCap
	 *            the bUcuDisCap to set
	 */
	public void setbUcuDisCap(BigDecimal bUcuDisCap) {
		this.bUcuDisCap = bUcuDisCap;
	}

	/**
	 * @return the bUcuKaynakAgiziAcisi
	 */
	public BigDecimal getbUcuKaynakAgiziAcisi() {
		return bUcuKaynakAgiziAcisi;
	}

	/**
	 * @param bUcuKaynakAgiziAcisi
	 *            the bUcuKaynakAgiziAcisi to set
	 */
	public void setbUcuKaynakAgiziAcisi(BigDecimal bUcuKaynakAgiziAcisi) {
		this.bUcuKaynakAgiziAcisi = bUcuKaynakAgiziAcisi;
	}

	/**
	 * @return the bUcuOvallikCapMax
	 */
	public BigDecimal getbUcuOvallikCapMax() {
		return bUcuOvallikCapMax;
	}

	/**
	 * @param bUcuOvallikCapMax
	 *            the bUcuOvallikCapMax to set
	 */
	public void setbUcuOvallikCapMax(BigDecimal bUcuOvallikCapMax) {
		this.bUcuOvallikCapMax = bUcuOvallikCapMax;
	}

	/**
	 * @return the bUcuOvallikCapMin
	 */
	public BigDecimal getbUcuOvallikCapMin() {
		return bUcuOvallikCapMin;
	}

	/**
	 * @param bUcuOvallikCapMin
	 *            the bUcuOvallikCapMin to set
	 */
	public void setbUcuOvallikCapMin(BigDecimal bUcuOvallikCapMin) {
		this.bUcuOvallikCapMin = bUcuOvallikCapMin;
	}

	/**
	 * @return the boruAgirlik
	 */
	public BigDecimal getBoruAgirlik() {
		return boruAgirlik;
	}

	/**
	 * @param boruAgirlik
	 *            the boruAgirlik to set
	 */
	public void setBoruAgirlik(BigDecimal boruAgirlik) {
		this.boruAgirlik = boruAgirlik;
	}

	/**
	 * @return the boruUzunluk
	 */
	public BigDecimal getBoruUzunluk() {
		return boruUzunluk;
	}

	/**
	 * @param boruUzunluk
	 *            the boruUzunluk to set
	 */
	public void setBoruUzunluk(BigDecimal boruUzunluk) {
		this.boruUzunluk = boruUzunluk;
	}

	/**
	 * @return the dogrusallik
	 */
	public BigDecimal getDogrusallik() {
		return dogrusallik;
	}

	/**
	 * @param dogrusallik
	 *            the dogrusallik to set
	 */
	public void setDogrusallik(BigDecimal dogrusallik) {
		this.dogrusallik = dogrusallik;
	}

	/**
	 * @return the eklemeZamani
	 */
	public Timestamp getEklemeZamani() {
		return eklemeZamani;
	}

	/**
	 * @param eklemeZamani
	 *            the eklemeZamani to set
	 */
	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the govdeDisCapMax
	 */
	public BigDecimal getGovdeDisCapMax() {
		return govdeDisCapMax;
	}

	/**
	 * @param govdeDisCapMax
	 *            the govdeDisCapMax to set
	 */
	public void setGovdeDisCapMax(BigDecimal govdeDisCapMax) {
		this.govdeDisCapMax = govdeDisCapMax;
	}

	/**
	 * @return the govdeDisCapMin
	 */
	public BigDecimal getGovdeDisCapMin() {
		return govdeDisCapMin;
	}

	/**
	 * @param govdeDisCapMin
	 *            the govdeDisCapMin to set
	 */
	public void setGovdeDisCapMin(BigDecimal govdeDisCapMin) {
		this.govdeDisCapMin = govdeDisCapMin;
	}

	/**
	 * @return the govdeOvallikCapMax
	 */
	public BigDecimal getGovdeOvallikCapMax() {
		return govdeOvallikCapMax;
	}

	/**
	 * @param govdeOvallikCapMax
	 *            the govdeOvallikCapMax to set
	 */
	public void setGovdeOvallikCapMax(BigDecimal govdeOvallikCapMax) {
		this.govdeOvallikCapMax = govdeOvallikCapMax;
	}

	/**
	 * @return the govdeOvallikCapMin
	 */
	public BigDecimal getGovdeOvallikCapMin() {
		return govdeOvallikCapMin;
	}

	/**
	 * @param govdeOvallikCapMin
	 *            the govdeOvallikCapMin to set
	 */
	public void setGovdeOvallikCapMin(BigDecimal govdeOvallikCapMin) {
		this.govdeOvallikCapMin = govdeOvallikCapMin;
	}

	/**
	 * @return the guncellemeZamani
	 */
	public Timestamp getGuncellemeZamani() {
		return guncellemeZamani;
	}

	/**
	 * @param guncellemeZamani
	 *            the guncellemeZamani to set
	 */
	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the kaliciManyetizma
	 */
	public BigDecimal getKaliciManyetizma() {
		return kaliciManyetizma;
	}

	/**
	 * @param kaliciManyetizma
	 *            the kaliciManyetizma to set
	 */
	public void setKaliciManyetizma(BigDecimal kaliciManyetizma) {
		this.kaliciManyetizma = kaliciManyetizma;
	}

	/**
	 * @return the pipeId
	 */
	public Integer getPipeId() {
		return pipeId;
	}

	/**
	 * @param pipeId
	 *            the pipeId to set
	 */
	public void setPipeId(Integer pipeId) {
		this.pipeId = pipeId;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the radyalKaciklik
	 */
	public BigDecimal getRadyalKaciklik() {
		return radyalKaciklik;
	}

	/**
	 * @param radyalKaciklik
	 *            the radyalKaciklik to set
	 */
	public void setRadyalKaciklik(BigDecimal radyalKaciklik) {
		this.radyalKaciklik = radyalKaciklik;
	}

	/**
	 * @return the icKaynakDikisiYuksekligiMin
	 */
	public BigDecimal getIcKaynakDikisiYuksekligiMin() {
		return icKaynakDikisiYuksekligiMin;
	}

	/**
	 * @param icKaynakDikisiYuksekligiMin
	 *            the icKaynakDikisiYuksekligiMin to set
	 */
	public void setIcKaynakDikisiYuksekligiMin(
			BigDecimal icKaynakDikisiYuksekligiMin) {
		this.icKaynakDikisiYuksekligiMin = icKaynakDikisiYuksekligiMin;
	}

	/**
	 * @return the icKaynakDikisiYuksekligiMax
	 */
	public BigDecimal getIcKaynakDikisiYuksekligiMax() {
		return icKaynakDikisiYuksekligiMax;
	}

	/**
	 * @param icKaynakDikisiYuksekligiMax
	 *            the icKaynakDikisiYuksekligiMax to set
	 */
	public void setIcKaynakDikisiYuksekligiMax(
			BigDecimal icKaynakDikisiYuksekligiMax) {
		this.icKaynakDikisiYuksekligiMax = icKaynakDikisiYuksekligiMax;
	}

	/**
	 * @return the disKaynakDikisiYuksekligiMin
	 */
	public BigDecimal getDisKaynakDikisiYuksekligiMin() {
		return disKaynakDikisiYuksekligiMin;
	}

	/**
	 * @param disKaynakDikisiYuksekligiMin
	 *            the disKaynakDikisiYuksekligiMin to set
	 */
	public void setDisKaynakDikisiYuksekligiMin(
			BigDecimal disKaynakDikisiYuksekligiMin) {
		this.disKaynakDikisiYuksekligiMin = disKaynakDikisiYuksekligiMin;
	}

	/**
	 * @return the disKaynakDikisiYuksekligiMax
	 */
	public BigDecimal getDisKaynakDikisiYuksekligiMax() {
		return disKaynakDikisiYuksekligiMax;
	}

	/**
	 * @param disKaynakDikisiYuksekligiMax
	 *            the disKaynakDikisiYuksekligiMax to set
	 */
	public void setDisKaynakDikisiYuksekligiMax(
			BigDecimal disKaynakDikisiYuksekligiMax) {
		this.disKaynakDikisiYuksekligiMax = disKaynakDikisiYuksekligiMax;
	}

	/**
	 * @return the result
	 */
	public Integer getResult() {
		return result;
	}

	/**
	 * @param result
	 *            the result to set
	 */
	public void setResult(Integer result) {
		this.result = result;
	}

	/**
	 * @return the testId
	 */
	public Integer getTestId() {
		return testId;
	}

	/**
	 * @param testId
	 *            the testId to set
	 */
	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	/**
	 * @return the testTahribatsizManuelUtSonuc
	 */
	public TestTahribatsizManuelUtSonuc getTestTahribatsizManuelUtSonuc() {
		return testTahribatsizManuelUtSonuc;
	}

	/**
	 * @param testTahribatsizManuelUtSonuc
	 *            the testTahribatsizManuelUtSonuc to set
	 */
	public void setTestTahribatsizManuelUtSonuc(
			TestTahribatsizManuelUtSonuc testTahribatsizManuelUtSonuc) {
		this.testTahribatsizManuelUtSonuc = testTahribatsizManuelUtSonuc;
	}

	/**
	 * @return the testTahribatsizFloroskopikSonuc
	 */
	public TestTahribatsizFloroskopikSonuc getTestTahribatsizFloroskopikSonuc() {
		return testTahribatsizFloroskopikSonuc;
	}

	/**
	 * @param testTahribatsizFloroskopikSonuc
	 *            the testTahribatsizFloroskopikSonuc to set
	 */
	public void setTestTahribatsizFloroskopikSonuc(
			TestTahribatsizFloroskopikSonuc testTahribatsizFloroskopikSonuc) {
		this.testTahribatsizFloroskopikSonuc = testTahribatsizFloroskopikSonuc;
	}

	/**
	 * @return the durum
	 */
	public Boolean getDurum() {
		return durum;
	}

	/**
	 * @param durum
	 *            the durum to set
	 */
	public void setDurum(Boolean durum) {
		this.durum = durum;
	}

	/**
	 * @return the ruloPipeLink
	 */
	public RuloPipeLink getRuloPipeLink() {
		return ruloPipeLink;
	}

	/**
	 * @param ruloPipeLink
	 *            the ruloPipeLink to set
	 */
	public void setRuloPipeLink(RuloPipeLink ruloPipeLink) {
		this.ruloPipeLink = ruloPipeLink;
	}

	/**
	 * @return the etKalinligiMin
	 */
	public BigDecimal getEtKalinligiMin() {
		return etKalinligiMin;
	}

	/**
	 * @param etKalinligiMin
	 *            the etKalinligiMin to set
	 */
	public void setEtKalinligiMin(BigDecimal etKalinligiMin) {
		this.etKalinligiMin = etKalinligiMin;
	}

	/**
	 * @return the etKalinligiMax
	 */
	public BigDecimal getEtKalinligiMax() {
		return etKalinligiMax;
	}

	/**
	 * @param etKalinligiMax
	 *            the etKalinligiMax to set
	 */
	public void setEtKalinligiMax(BigDecimal etKalinligiMax) {
		this.etKalinligiMax = etKalinligiMax;
	}

	/**
	 * @return the dent
	 */
	public BigDecimal getDent() {
		return dent;
	}

	/**
	 * @param dent
	 *            the dent to set
	 */
	public void setDent(BigDecimal dent) {
		this.dent = dent;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the aUcuIcCapCevresel
	 */
	public BigDecimal getaUcuIcCapCevresel() {
		return aUcuIcCapCevresel;
	}

	/**
	 * @param aUcuIcCapCevresel
	 *            the aUcuIcCapCevresel to set
	 */
	public void setaUcuIcCapCevresel(BigDecimal aUcuIcCapCevresel) {
		this.aUcuIcCapCevresel = aUcuIcCapCevresel;
	}

	/**
	 * @return the aUcuIcCap
	 */
	public BigDecimal getaUcuIcCap() {
		return aUcuIcCap;
	}

	/**
	 * @param aUcuIcCap
	 *            the aUcuIcCap to set
	 */
	public void setaUcuIcCap(BigDecimal aUcuIcCap) {
		this.aUcuIcCap = aUcuIcCap;
	}

	/**
	 * @return the bUcuIcCap
	 */
	public BigDecimal getbUcuIcCap() {
		return bUcuIcCap;
	}

	/**
	 * @param bUcuIcCap
	 *            the bUcuIcCap to set
	 */
	public void setbUcuIcCap(BigDecimal bUcuIcCap) {
		this.bUcuIcCap = bUcuIcCap;
	}

	/**
	 * @return the bUcuIcCapCevresel
	 */
	public BigDecimal getbUcuIcCapCevresel() {
		return bUcuIcCapCevresel;
	}

	/**
	 * @param bUcuIcCapCevresel
	 *            the bUcuIcCapCevresel to set
	 */
	public void setbUcuIcCapCevresel(BigDecimal bUcuIcCapCevresel) {
		this.bUcuIcCapCevresel = bUcuIcCapCevresel;
	}

	/**
	 * @return the boruKabulTarihi
	 */
	public Date getBoruKabulTarihi() {
		return boruKabulTarihi;
	}

	/**
	 * @param boruKabulTarihi
	 *            the boruKabulTarihi to set
	 */
	public void setBoruKabulTarihi(Date boruKabulTarihi) {
		this.boruKabulTarihi = boruKabulTarihi;
	}

}