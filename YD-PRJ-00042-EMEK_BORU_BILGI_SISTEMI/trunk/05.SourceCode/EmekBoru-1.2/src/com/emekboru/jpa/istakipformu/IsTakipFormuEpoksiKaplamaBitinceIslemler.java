package com.emekboru.jpa.istakipformu;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the is_takip_formu_epoksi_kaplama_bitince_islemler
 * database table.
 * 
 */
@Entity
@Table(name = "is_takip_formu_epoksi_kaplama_bitince_islemler")
public class IsTakipFormuEpoksiKaplamaBitinceIslemler implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IS_TAKIP_FORMU_EPOKSI_KAPLAMA_BITINCE_ISLEMLER_ID_GENERATOR", sequenceName = "IS_TAKIP_FORMU_EPOKSI_KAPLAMA_BITINCE_ISLEMLER_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IS_TAKIP_FORMU_EPOKSI_KAPLAMA_BITINCE_ISLEMLER_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", insertable = false, updatable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici", insertable = false, updatable = false)
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name = "is_takip_form_id")
	// private Integer isTakipFormId;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "is_takip_form_id", referencedColumnName = "ID", insertable = false, updatable = false)
	private IsTakipFormuEpoksiKaplama isTakipFormuEpoksiKaplama;

	@Column(name = "hedeflenen_sarfiyat_bir")
	private Integer hedeflenenSarfiyatBir;

	@Column(name = "hedeflenen_sarfiyat_iki")
	private Integer hedeflenenSarfiyatIki;

	@Column(name = "hedeflenen_sarfiyat_uc")
	private Integer hedeflenenSarfiyatUc;

	@Column(name = "gerceklesen_sarfiyat_bir")
	private Integer gerceklesenSarfiyatBir;

	@Column(name = "gerceklesen_sarfiyat_iki")
	private Integer gerceklesenSarfiyatIki;

	@Column(name = "gerceklesen_sarfiyat_uc")
	private Integer gerceklesenSarfiyatUc;

	public IsTakipFormuEpoksiKaplamaBitinceIslemler() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	public IsTakipFormuEpoksiKaplama getIsTakipFormuEpoksiKaplama() {
		return isTakipFormuEpoksiKaplama;
	}

	public void setIsTakipFormuEpoksiKaplama(
			IsTakipFormuEpoksiKaplama isTakipFormuEpoksiKaplama) {
		this.isTakipFormuEpoksiKaplama = isTakipFormuEpoksiKaplama;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getHedeflenenSarfiyatBir() {
		return hedeflenenSarfiyatBir;
	}

	public void setHedeflenenSarfiyatBir(Integer hedeflenenSarfiyatBir) {
		this.hedeflenenSarfiyatBir = hedeflenenSarfiyatBir;
	}

	public Integer getHedeflenenSarfiyatIki() {
		return hedeflenenSarfiyatIki;
	}

	public void setHedeflenenSarfiyatIki(Integer hedeflenenSarfiyatIki) {
		this.hedeflenenSarfiyatIki = hedeflenenSarfiyatIki;
	}

	public Integer getHedeflenenSarfiyatUc() {
		return hedeflenenSarfiyatUc;
	}

	public void setHedeflenenSarfiyatUc(Integer hedeflenenSarfiyatUc) {
		this.hedeflenenSarfiyatUc = hedeflenenSarfiyatUc;
	}

	public Integer getGerceklesenSarfiyatBir() {
		return gerceklesenSarfiyatBir;
	}

	public void setGerceklesenSarfiyatBir(Integer gerceklesenSarfiyatBir) {
		this.gerceklesenSarfiyatBir = gerceklesenSarfiyatBir;
	}

	public Integer getGerceklesenSarfiyatIki() {
		return gerceklesenSarfiyatIki;
	}

	public void setGerceklesenSarfiyatIki(Integer gerceklesenSarfiyatIki) {
		this.gerceklesenSarfiyatIki = gerceklesenSarfiyatIki;
	}

	public Integer getGerceklesenSarfiyatUc() {
		return gerceklesenSarfiyatUc;
	}

	public void setGerceklesenSarfiyatUc(Integer gerceklesenSarfiyatUc) {
		this.gerceklesenSarfiyatUc = gerceklesenSarfiyatUc;
	}

}