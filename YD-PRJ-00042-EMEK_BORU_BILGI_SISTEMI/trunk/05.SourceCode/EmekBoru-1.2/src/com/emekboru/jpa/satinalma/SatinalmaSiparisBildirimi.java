package com.emekboru.jpa.satinalma;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * The persistent class for the satinalma_siparis_bildirimi database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "SatinalmaSiparisBildirimi.findAll", query = "SELECT r FROM SatinalmaSiparisBildirimi r order by r.eklemeZamani DESC") })
@Table(name = "satinalma_siparis_bildirimi")
public class SatinalmaSiparisBildirimi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SATINALMA_SIPARIS_BILDIRIMI_ID_GENERATOR", sequenceName = "SATINALMA_SIPARIS_BILDIRIMI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SATINALMA_SIPARIS_BILDIRIMI_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(length = 6, name = "aciklama")
	private String aciklama;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(length = 6, name = "iban")
	private String iban;

	@Column(name = "odeme_vadesi", length = 6)
	private String odemeVadesi;

	// @Column(name = "siparis_mektubu_id", nullable = false)
	// private Integer siparisMektubuId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "siparis_mektubu_id", referencedColumnName = "ID", insertable = false)
	private SatinalmaSiparisMektubu satinalmaSiparisMektubu;

	@Column(name = "documents")
	private String documents;

	@Transient
	private List<String> fileNames;

	@Transient
	private int fileNumber;

	public SatinalmaSiparisBildirimi() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getIban() {
		return this.iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getOdemeVadesi() {
		return this.odemeVadesi;
	}

	public void setOdemeVadesi(String odemeVadesi) {
		this.odemeVadesi = odemeVadesi;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the satinalmaSiparisMektubu
	 */
	public SatinalmaSiparisMektubu getSatinalmaSiparisMektubu() {
		return satinalmaSiparisMektubu;
	}

	/**
	 * @param satinalmaSiparisMektubu
	 *            the satinalmaSiparisMektubu to set
	 */
	public void setSatinalmaSiparisMektubu(
			SatinalmaSiparisMektubu satinalmaSiparisMektubu) {
		this.satinalmaSiparisMektubu = satinalmaSiparisMektubu;
	}

	/**
	 * @return the fileNames
	 */
	public List<String> getFileNames() {
		try {
			String[] names = documents.split("//");
			fileNames = new ArrayList<String>();

			for (int i = 1; i < names.length; i++) {
				fileNames.add(names[i]);
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return fileNames;
	}

	/**
	 * @param fileNames
	 *            the fileNames to set
	 */
	public void setFileNames(List<String> fileNames) {
		this.fileNames = fileNames;
	}

	/**
	 * @return the fileNumber
	 */
	public int getFileNumber() {
		try {
			String[] names = documents.split("//");

			fileNumber = names.length - 1;
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return fileNumber;
	}

	/**
	 * @return the documents
	 */
	public String getDocuments() {
		return documents;
	}

	/**
	 * @param documents
	 *            the documents to set
	 */
	public void setDocuments(String documents) {
		this.documents = documents;
	}

	/**
	 * @param fileNumber
	 *            the fileNumber to set
	 */
	public void setFileNumber(int fileNumber) {
		this.fileNumber = fileNumber;
	}

}