package com.emekboru.jpa.employee;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.emekboru.jpa.Employee;
import com.emekboru.jpa.common.CommonDayoffType;

/**
 * The persistent class for the employee_dayoff database table.
 * 
 */
@Entity
@Table(name = "employee_dayoff")
@NamedQuery(name = "EmployeeDayoff.findAll", query = "SELECT e FROM EmployeeDayoff e")
public class EmployeeDayoff implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "EMPLOYEE_DAYOFF_ID_GENERATOR", sequenceName = "EMPLOYEE_DAYOFF_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMPLOYEE_DAYOFF_ID_GENERATOR")
	@Column(name = "id")
	private Integer id;

	@Column(name = "active")
	private Boolean active;

	@Column(name = "address")
	private String address;

	@Column(name = "day_count")
	private Integer dayCount;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "deputy_id", referencedColumnName = "employee_id", insertable = false)
	private Employee deputy;

	@Column(name = "description")
	private String description;

	@Column(name = "ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	// bi-directional many-to-one association to Employee
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "employee_id", referencedColumnName = "employee_id", insertable = false)
	private Employee employee;

	@Temporal(TemporalType.DATE)
	@Column(name = "end_date")
	private Date endDate;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "hr_approval")
	private Integer hrApproval;

	@Column(name = "phone")
	private String phone;

	@Temporal(TemporalType.DATE)
	@Column(name = "request_date")
	private Date requestDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "start_date")
	private Date startDate;

	@Column(name = "supervisor_approval")
	private Integer supervisorApproval;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "supervisor_id", referencedColumnName = "employee_id", insertable = false)
	private Employee supervisor;

	// bi-directional many-to-one association to CommonDayoffType
	@ManyToOne
	@JoinColumn(name = "dayoff_type_id")
	private CommonDayoffType commonDayoffType;

	@Column(name = "bm_approval")
	private Integer bmApproval;

	@Transient
	public static final int PENDING = 0;
	@Transient
	public static final int APPROVED = 1;
	@Transient
	public static final int DECLINED = 2;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "second_supervisor_id", referencedColumnName = "employee_id", insertable = false)
	private Employee secondSupervisor;

	@Temporal(TemporalType.DATE)
	@Column(name = "work_start_date")
	private Date workStartDate;

	@Column(name = "second_supervisor_approval")
	private Integer secondSupervisorApproval;

	public EmployeeDayoff() {
		this.commonDayoffType = new CommonDayoffType();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getDayCount() {
		return this.dayCount;
	}

	public void setDayCount(Integer dayCount) {
		this.dayCount = dayCount;
	}

	public Employee getDeputy() {
		return deputy;
	}

	public void setDeputy(Employee deputy) {
		this.deputy = deputy;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getRequestDate() {
		return this.requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Employee getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(Employee supervisor) {
		this.supervisor = supervisor;
	}

	public CommonDayoffType getCommonDayoffType() {
		return this.commonDayoffType;
	}

	public void setCommonDayoffType(CommonDayoffType commonDayoffType) {
		this.commonDayoffType = commonDayoffType;
	}

	public Integer getHrApproval() {
		return hrApproval;
	}

	public void setHrApproval(Integer hrApproval) {
		this.hrApproval = hrApproval;
	}

	public Integer getSupervisorApproval() {
		return supervisorApproval;
	}

	public void setSupervisorApproval(Integer supervisorApproval) {
		this.supervisorApproval = supervisorApproval;
	}

	/**
	 * @return the bmApproval
	 */
	public Integer getBmApproval() {
		return bmApproval;
	}

	/**
	 * @param bmApproval
	 *            the bmApproval to set
	 */
	public void setBmApproval(Integer bmApproval) {
		this.bmApproval = bmApproval;
	}

	/**
	 * @return the secondSupervisor
	 */
	public Employee getSecondSupervisor() {
		return secondSupervisor;
	}

	/**
	 * @param secondSupervisor
	 *            the secondSupervisor to set
	 */
	public void setSecondSupervisor(Employee secondSupervisor) {
		this.secondSupervisor = secondSupervisor;
	}

	/**
	 * @return the workStartDate
	 */
	public Date getWorkStartDate() {
		return workStartDate;
	}

	/**
	 * @param workStartDate
	 *            the workStartDate to set
	 */
	public void setWorkStartDate(Date workStartDate) {
		this.workStartDate = workStartDate;
	}

	/**
	 * @return the secondSupervisorApproval
	 */
	public Integer getSecondSupervisorApproval() {
		return secondSupervisorApproval;
	}

	/**
	 * @param secondSupervisorApproval
	 *            the secondSupervisorApproval to set
	 */
	public void setSecondSupervisorApproval(Integer secondSupervisorApproval) {
		this.secondSupervisorApproval = secondSupervisorApproval;
	}

}