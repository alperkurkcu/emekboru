package com.emekboru.jpa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.emekboru.jpa.rulo.RuloPipeLink;

/**
 * The persistent class for the test_boru_cekme_sonuc database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "TestCekmeSonuc.findAll", query = "SELECT r FROM TestCekmeSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId"),
		@NamedQuery(name = "TestCekmeSonuc.getBySalesItemGlobalId", query = "SELECT r FROM TestCekmeSonuc r WHERE r.pipe.salesItem.itemId=:prmSalesItem and r.bagliGlobalId.globalId=:prmGlobalId order by r.partiNo"),
		@NamedQuery(name = "TestCekmeSonuc.getBySalesItem", query = "SELECT r FROM TestCekmeSonuc r WHERE r.pipe.salesItem.itemId=:prmSalesItem order by r.partiNo"),
		@NamedQuery(name = "TestCekmeSonuc.getByDokumNo", query = "SELECT r FROM TestCekmeSonuc r WHERE r.pipe.ruloPipeLink.rulo.ruloDokumNo=:prmDokumNo"),
		@NamedQuery(name = "TestCekmeSonuc.getByDokumNoSalesItem", query = "SELECT r FROM TestCekmeSonuc r WHERE r.ruloPipeLink.rulo.ruloDokumNo=:prmDokumNo and r.pipe.salesItem.itemId=:prmItemId order by r.pipe.pipeBarkodNo, r.bagliTestId.code") })
@Table(name = "test_cekme_sonuc")
public class TestCekmeSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_CEKME_SONUC_ID_GENERATOR", sequenceName = "TEST_CEKME_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_CEKME_SONUC_ID_GENERATOR")
	@Column(name = "ID")
	private Integer id;

	@Column(name = "aciklama")
	private String aciklama;

	@Column(name = "akma_cekme_oran")
	private BigDecimal akmaCekmeOran;

	@Column(name = "akma_dayanci")
	private BigDecimal akmaDayanci;

	@Column(name = "akma_yuku")
	private BigDecimal akmaYuku;

	private BigDecimal alan;

	@Column(name = "cekme_dayanci")
	private BigDecimal cekmeDayanci;

	// @Column(name = "global_id")
	// private Integer globalId;

	@Column(name = "cekme_yuku")
	private BigDecimal cekmeYuku;

	// @Column(name="destructive_test_id")
	// private Integer destructiveTestId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Column(name = "et_kalinlik")
	private BigDecimal etKalinlik;

	@Column(name = "genislik")
	private BigDecimal genislik;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "kopma_bolgesi")
	private Integer kopmaBolgesi;

	@Column(name = "parti_no")
	private String partiNo;

	// @Column(name="pipe_id")
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private DestructiveTestsSpec bagliGlobalId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "destructive_test_id", referencedColumnName = "test_id", insertable = false)
	private DestructiveTestsSpec bagliTestId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	private Boolean sonuc;

	@Column(name = "uzama", precision = 3, scale = 2)
	private BigDecimal uzama;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi", nullable = false)
	private Date testTarihi;

	@Column(name = "documents")
	private String documents;

	@Transient
	private List<String> fileNames;

	@Transient
	private int fileNumber;

	@Column(name = "test_durum")
	private Boolean testDurum;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", nullable = false, referencedColumnName = "pipe_id", insertable = false, updatable = false)
	private RuloPipeLink ruloPipeLink;

	@Column(name = "sample_no")
	private String sampleNo;

	@Column(name = "numune_yeri")
	private Integer numuneYeri;

	@Column(name = "numune_yonu")
	private Integer numuneYonu;

	public TestCekmeSonuc() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Date getEklemeZamani() {
		return eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getGuncellemeZamani() {
		return guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public DestructiveTestsSpec getBagliGlobalId() {
		return bagliGlobalId;
	}

	public void setBagliGlobalId(DestructiveTestsSpec bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	public DestructiveTestsSpec getBagliTestId() {
		return bagliTestId;
	}

	public void setBagliTestId(DestructiveTestsSpec bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public Boolean getSonuc() {
		return sonuc;
	}

	public void setSonuc(Boolean sonuc) {
		this.sonuc = sonuc;
	}

	public Date getTestTarihi() {
		return testTarihi;
	}

	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	public String getDocuments() {
		return documents;
	}

	public void setDocuments(String documents) {
		this.documents = documents;
	}

	public List<String> getFileNames() {
		try {
			String[] names = documents.split("//");
			fileNames = new ArrayList<String>();

			for (int i = 1; i < names.length; i++) {
				fileNames.add(names[i]);
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return fileNames;
	}

	public void setFileNames(List<String> fileNames) {
		this.fileNames = fileNames;
	}

	public int getFileNumber() {
		try {
			String[] names = documents.split("//");

			fileNumber = names.length - 1;
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return fileNumber;
	}

	/**
	 * @return the testDurum
	 */
	public Boolean getTestDurum() {
		return testDurum;
	}

	/**
	 * @param testDurum
	 *            the testDurum to set
	 */
	public void setTestDurum(Boolean testDurum) {
		this.testDurum = testDurum;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the ruloPipeLink
	 */
	public RuloPipeLink getRuloPipeLink() {
		return ruloPipeLink;
	}

	/**
	 * @param ruloPipeLink
	 *            the ruloPipeLink to set
	 */
	public void setRuloPipeLink(RuloPipeLink ruloPipeLink) {
		this.ruloPipeLink = ruloPipeLink;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the akmaCekmeOran
	 */
	public BigDecimal getAkmaCekmeOran() {
		return akmaCekmeOran;
	}

	/**
	 * @param akmaCekmeOran
	 *            the akmaCekmeOran to set
	 */
	public void setAkmaCekmeOran(BigDecimal akmaCekmeOran) {
		this.akmaCekmeOran = akmaCekmeOran;
	}

	/**
	 * @return the akmaDayanci
	 */
	public BigDecimal getAkmaDayanci() {
		return akmaDayanci;
	}

	/**
	 * @param akmaDayanci
	 *            the akmaDayanci to set
	 */
	public void setAkmaDayanci(BigDecimal akmaDayanci) {
		this.akmaDayanci = akmaDayanci;
	}

	/**
	 * @return the akmaYuku
	 */
	public BigDecimal getAkmaYuku() {
		return akmaYuku;
	}

	/**
	 * @param akmaYuku
	 *            the akmaYuku to set
	 */
	public void setAkmaYuku(BigDecimal akmaYuku) {
		this.akmaYuku = akmaYuku;
	}

	/**
	 * @return the alan
	 */
	public BigDecimal getAlan() {
		return alan;
	}

	/**
	 * @param alan
	 *            the alan to set
	 */
	public void setAlan(BigDecimal alan) {
		this.alan = alan;
	}

	/**
	 * @return the cekmeDayanci
	 */
	public BigDecimal getCekmeDayanci() {
		return cekmeDayanci;
	}

	/**
	 * @param cekmeDayanci
	 *            the cekmeDayanci to set
	 */
	public void setCekmeDayanci(BigDecimal cekmeDayanci) {
		this.cekmeDayanci = cekmeDayanci;
	}

	/**
	 * @return the cekmeYuku
	 */
	public BigDecimal getCekmeYuku() {
		return cekmeYuku;
	}

	/**
	 * @param cekmeYuku
	 *            the cekmeYuku to set
	 */
	public void setCekmeYuku(BigDecimal cekmeYuku) {
		this.cekmeYuku = cekmeYuku;
	}

	/**
	 * @return the etKalinlik
	 */
	public BigDecimal getEtKalinlik() {
		return etKalinlik;
	}

	/**
	 * @param etKalinlik
	 *            the etKalinlik to set
	 */
	public void setEtKalinlik(BigDecimal etKalinlik) {
		this.etKalinlik = etKalinlik;
	}

	/**
	 * @return the genislik
	 */
	public BigDecimal getGenislik() {
		return genislik;
	}

	/**
	 * @param genislik
	 *            the genislik to set
	 */
	public void setGenislik(BigDecimal genislik) {
		this.genislik = genislik;
	}

	/**
	 * @return the uzama
	 */
	public BigDecimal getUzama() {
		return uzama;
	}

	/**
	 * @param uzama
	 *            the uzama to set
	 */
	public void setUzama(BigDecimal uzama) {
		this.uzama = uzama;
	}

	/**
	 * @return the sampleNo
	 */
	public String getSampleNo() {
		return sampleNo;
	}

	/**
	 * @param sampleNo
	 *            the sampleNo to set
	 */
	public void setSampleNo(String sampleNo) {
		this.sampleNo = sampleNo;
	}

	/**
	 * @return the partiNo
	 */
	public String getPartiNo() {
		return partiNo;
	}

	/**
	 * @param partiNo
	 *            the partiNo to set
	 */
	public void setPartiNo(String partiNo) {
		this.partiNo = partiNo;
	}

	/**
	 * @return the kopmaBolgesi
	 */
	public Integer getKopmaBolgesi() {
		return kopmaBolgesi;
	}

	/**
	 * @param kopmaBolgesi
	 *            the kopmaBolgesi to set
	 */
	public void setKopmaBolgesi(Integer kopmaBolgesi) {
		this.kopmaBolgesi = kopmaBolgesi;
	}

	/**
	 * @return the numuneYeri
	 */
	public Integer getNumuneYeri() {
		return numuneYeri;
	}

	/**
	 * @param numuneYeri
	 *            the numuneYeri to set
	 */
	public void setNumuneYeri(Integer numuneYeri) {
		this.numuneYeri = numuneYeri;
	}

	/**
	 * @return the numuneYonu
	 */
	public Integer getNumuneYonu() {
		return numuneYonu;
	}

	/**
	 * @param numuneYonu
	 *            the numuneYonu to set
	 */
	public void setNumuneYonu(Integer numuneYonu) {
		this.numuneYonu = numuneYonu;
	}
}