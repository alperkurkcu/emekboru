package com.emekboru.jpa.sales.order;

import java.io.Serializable;

import javax.persistence.*;

import com.emekboru.jpa.Employee;

@Entity
@Table(name = "sales_join_proposal_employee")
public class SalesJoinProposalEmployee implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1369285745454769428L;

	@Id
	@Column(name = "join_id")
	@SequenceGenerator(name = "SALES_JOIN_PROPOSAL_EMPLOYEE_GENERATOR", sequenceName = "SALES_JOIN_PROPOSAL_EMPLOYEE_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_JOIN_PROPOSAL_EMPLOYEE_GENERATOR")
	private int joinId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "employee_id", referencedColumnName = "employee_id", updatable = false)
	private Employee employee;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "proposal_id", referencedColumnName = "proposal_id", updatable = false)
	private SalesProposal proposal;

	public SalesJoinProposalEmployee() {
		employee = new Employee();
		proposal = new SalesProposal();
	}

	/**
	 * GETTERS AND SETTERS
	 */

	public int getJoinId() {
		return joinId;
	}

	public SalesProposal getProposal() {
		return proposal;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setJoinId(Integer joinId) {
		this.joinId = joinId;
	}

	public void setProposal(SalesProposal proposal) {
		this.proposal = proposal;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
