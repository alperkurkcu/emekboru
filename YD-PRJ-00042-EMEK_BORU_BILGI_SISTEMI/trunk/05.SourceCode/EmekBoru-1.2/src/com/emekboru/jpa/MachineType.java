package com.emekboru.jpa;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="machine_type")
public class MachineType implements Serializable{

	private static final long serialVersionUID = 5150808050386698133L;
	
	@Id
	@Column(name="machine_type_id")
	@SequenceGenerator(name="MACHINE_TYPE_GENERATOR", sequenceName="MACHINE_TYPE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MACHINE_TYPE_GENERATOR")
	private Integer machineTypeId;
	
	@Column(name="short_desc")
	private String shortDesc;
	
	@Column(name="display_name")
	private String displayName;
	
	@Column(name="is_manufacturing")
	private boolean isManufacturing;
	
	@Column(name="is_coating")
	private boolean isCoating;
	
	@Column(name="is_spiral")
	private boolean isSpiral;
	
	@Column(name="type_name")
	private String typeName;
	
	
	
	@OneToMany(mappedBy = "machineType", fetch=FetchType.LAZY)
	private List<Machine> machines;

	
	/**
	 * GETTERS AND SETTERS
	 */
	public Integer getMachineTypeId() {
		return machineTypeId;
	}

	public void setMachineTypeId(Integer machineTypeId) {
		this.machineTypeId = machineTypeId;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}
	
	public List<Machine> getMachines() {
		return machines;
	}

	public void setMachines(List<Machine> machines) {
		this.machines = machines;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public boolean isManufacturing() {
		return isManufacturing;
	}

	public void setManufacturing(boolean isManufacturing) {
		this.isManufacturing = isManufacturing;
	}

	public boolean isCoating() {
		return isCoating;
	}

	public void setCoating(boolean isCoating) {
		this.isCoating = isCoating;
	}

	public boolean isSpiral() {
		return isSpiral;
	}

	public void setSpiral(boolean isSpiral) {
		this.isSpiral = isSpiral;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	
}
