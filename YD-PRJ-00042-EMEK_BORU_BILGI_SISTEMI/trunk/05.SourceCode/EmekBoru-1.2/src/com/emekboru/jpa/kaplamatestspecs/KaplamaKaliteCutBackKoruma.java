package com.emekboru.jpa.kaplamatestspecs;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the kaplama_kalite_cut_back_koruma database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "KaplamaKaliteCutBackKoruma.findAll", query = "SELECT r FROM KaplamaKaliteCutBackKoruma r WHERE r.salesItem.itemId=:prmItemId order by r.eklemeZamani") })
@Table(name = "kaplama_kalite_cut_back_koruma")
public class KaplamaKaliteCutBackKoruma implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "KAPLAMA_KALITE_CUT_BACK_KORUMA_ID_GENERATOR", sequenceName = "KAPLAMA_KALITE_CUT_BACK_KORUMA_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "KAPLAMA_KALITE_CUT_BACK_KORUMA_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(nullable = false, length = 3000, name = "aciklama")
	private String aciklama;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Basic
	@Column(name = "sales_item_id", insertable = false, updatable = false)
	private Integer salesItemId;

	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private SalesItem salesItem;

	public KaplamaKaliteCutBackKoruma() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	/**
	 * @return the salesItem
	 */
	public SalesItem getSalesItem() {
		return salesItem;
	}

	/**
	 * @param salesItem
	 *            the salesItem to set
	 */
	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

}