package com.emekboru.jpa.sales.order;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sales_pipe_hole")
public class SalesPipeHole implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8170150326955112801L;

	@Id
	@Column(name = "pipe_id")
	private int pipeHoleId;

	@Column(name = "title")
	private String name;


	public SalesPipeHole() {
	}

	@Override
	public boolean equals(Object o) {
		return this.getPipeHoleId() == ((SalesPipeHole) o).getPipeHoleId();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public int getPipeHoleId() {
		return pipeHoleId;
	}

	public void setPipeHole(int pipeHoleId) {
		this.pipeHoleId = pipeHoleId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
