package com.emekboru.jpa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.emekboru.jpa.rulo.RuloPipeLink;

@Entity
@NamedQueries({
		@NamedQuery(name = "TestKimyasalAnalizSonuc.findAll", query = "SELECT r FROM TestKimyasalAnalizSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId"),
		@NamedQuery(name = "TestKimyasalAnalizSonuc.findByPipeId", query = "SELECT r FROM TestKimyasalAnalizSonuc r WHERE r.pipe.pipeId=:prmPipeId"),
		@NamedQuery(name = "TestKimyasalAnalizSonuc.getBySalesItem", query = "SELECT r FROM TestKimyasalAnalizSonuc r WHERE r.pipe.salesItem.itemId=:prmSalesItem order by r.partiNo"),
		@NamedQuery(name = "TestKimyasalAnalizSonuc.getByDokumNo", query = "SELECT r FROM TestKimyasalAnalizSonuc r WHERE r.pipe.ruloPipeLink.rulo.ruloDokumNo=:prmDokumNo"),
		@NamedQuery(name = "TestKimyasalAnalizSonuc.getByDokumNoSalesItem", query = "SELECT r FROM TestKimyasalAnalizSonuc r WHERE r.ruloPipeLink.rulo.ruloDokumNo=:prmDokumNo and r.pipe.salesItem.itemId=:prmItemId order by r.pipe.pipeBarkodNo") })
@Table(name = "test_kimyasal_analiz_sonuc")
public class TestKimyasalAnalizSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KIMYASAL_ANALIZ_SONUC_GENERATOR", sequenceName = "TEST_KIMYASAL_ANALIZ_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KIMYASAL_ANALIZ_SONUC_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Column(name = "aciklama")
	private String aciklama;

	private BigDecimal ae;

	private BigDecimal al;

	@Column(name = "al_signum")
	private String alSignum;

	private BigDecimal b;

	@Column(name = "b_signum")
	private String bSignum;

	private BigDecimal c;

	@Column(name = "c_signum")
	private String cSignum;

	private BigDecimal ce1;

	private BigDecimal ce2;

	private BigDecimal co;

	@Column(name = "co_signum")
	private String coSignum;

	private BigDecimal cr;

	@Column(name = "cr_signum")
	private String crSignum;

	private BigDecimal cu;

	@Column(name = "cu_signum")
	private String cuSignum;

	private BigDecimal w;

	@Column(name = "w_signum")
	private String wSignum;

	@Column(name = "as_value")
	private BigDecimal as;

	@Column(name = "as_signum")
	private String asSignum;

	private BigDecimal sb;

	@Column(name = "sb_signum")
	private String sbSignum;

	private BigDecimal sn;

	@Column(name = "sn_signum")
	private String snSignum;

	private BigDecimal pb;

	@Column(name = "pb_signum")
	private String pbSignum;

	private BigDecimal ca;

	@Column(name = "ca_signum")
	private String caSignum;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi", nullable = false)
	private Date testTarihi;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	private BigDecimal mn;

	private BigDecimal mo;

	@Column(name = "mo_signum")
	private String moSignum;

	private BigDecimal n;

	@Column(name = "n_signum")
	private String nSignum;

	private BigDecimal nb;

	@Column(name = "nb_signum")
	private String nbSignum;

	@Column(name = "ni")
	private BigDecimal ni;

	@Column(name = "ni_signum")
	private String niSignum;

	private BigDecimal p;

	@Column(name = "parti_no")
	private String partiNo;

	// @Column(name = "pipe_id")
	// private Integer pipeId;

	// @Column(name = "global_id")
	// private Integer globalId;

	// @Column(name = "destructive_test_id")
	// private Integer destructiveTestId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private DestructiveTestsSpec bagliGlobalId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "destructive_test_id", referencedColumnName = "test_id", insertable = false)
	private DestructiveTestsSpec bagliTestId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	private BigDecimal s;

	@Column(name = "s_signum")
	private String sSignum;

	@Column(name = "si")
	private BigDecimal si;

	private Boolean sonuc;

	@Column(name = "ti")
	private BigDecimal ti;

	@Column(name = "ti_signum")
	private String tiSignum;

	private BigDecimal v;

	@Column(name = "v_signum")
	private String vSignum;

	@Column(name = "documents")
	private String documents;

	@Transient
	private List<String> fileNames;

	@Transient
	private int fileNumber;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", nullable = false, referencedColumnName = "pipe_id", insertable = false, updatable = false)
	private RuloPipeLink ruloPipeLink;

	@Column(name = "sample_no")
	private String sampleNo;

	public TestKimyasalAnalizSonuc() {
	}

	// setters getters

	public Integer getId() {
		return this.id;
	}

	public String getwSignum() {
		return wSignum;
	}

	public void setwSignum(String wSignum) {
		this.wSignum = wSignum;
	}

	public String getAsSignum() {
		return asSignum;
	}

	public void setAsSignum(String asSignum) {
		this.asSignum = asSignum;
	}

	public String getSbSignum() {
		return sbSignum;
	}

	public void setSbSignum(String sbSignum) {
		this.sbSignum = sbSignum;
	}

	public String getSnSignum() {
		return snSignum;
	}

	public void setSnSignum(String snSignum) {
		this.snSignum = snSignum;
	}

	/**
	 * @return the pbSignum
	 */
	public String getPbSignum() {
		return pbSignum;
	}

	/**
	 * @param pbSignum
	 *            the pbSignum to set
	 */
	public void setPbSignum(String pbSignum) {
		this.pbSignum = pbSignum;
	}

	/**
	 * @return the vSignum
	 */
	public String getvSignum() {
		return vSignum;
	}

	/**
	 * @param vSignum
	 *            the vSignum to set
	 */
	public void setvSignum(String vSignum) {
		this.vSignum = vSignum;
	}

	public String getCaSignum() {
		return caSignum;
	}

	public void setCaSignum(String caSignum) {
		this.caSignum = caSignum;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public String getbSignum() {
		return bSignum;
	}

	public void setbSignum(String bSignum) {
		this.bSignum = bSignum;
	}

	public String getcSignum() {
		return cSignum;
	}

	public void setcSignum(String cSignum) {
		this.cSignum = cSignum;
	}

	public String getsSignum() {
		return sSignum;
	}

	public void setsSignum(String sSignum) {
		this.sSignum = sSignum;
	}

	public DestructiveTestsSpec getBagliGlobalId() {
		return bagliGlobalId;
	}

	public void setBagliGlobalId(DestructiveTestsSpec bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	public DestructiveTestsSpec getBagliTestId() {
		return bagliTestId;
	}

	public void setBagliTestId(DestructiveTestsSpec bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public Date getEklemeZamani() {
		return eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Date getGuncellemeZamani() {
		return guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Date getTestTarihi() {
		return testTarihi;
	}

	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	public String getAlSignum() {
		return alSignum;
	}

	public void setAlSignum(String alSignum) {
		this.alSignum = alSignum;
	}

	public String getCoSignum() {
		return coSignum;
	}

	public void setCoSignum(String coSignum) {
		this.coSignum = coSignum;
	}

	public String getCrSignum() {
		return crSignum;
	}

	public void setCrSignum(String crSignum) {
		this.crSignum = crSignum;
	}

	public String getCuSignum() {
		return cuSignum;
	}

	public void setCuSignum(String cuSignum) {
		this.cuSignum = cuSignum;
	}

	public Integer getEkleyenKullanici() {
		return ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Integer getGuncelleyenKullanici() {
		return guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getMoSignum() {
		return moSignum;
	}

	public void setMoSignum(String moSignum) {
		this.moSignum = moSignum;
	}

	public String getNbSignum() {
		return nbSignum;
	}

	public void setNbSignum(String nbSignum) {
		this.nbSignum = nbSignum;
	}

	public String getNiSignum() {
		return niSignum;
	}

	public void setNiSignum(String niSignum) {
		this.niSignum = niSignum;
	}

	public Boolean getSonuc() {
		return sonuc;
	}

	public void setSonuc(Boolean sonuc) {
		this.sonuc = sonuc;
	}

	public String getTiSignum() {
		return tiSignum;
	}

	public void setTiSignum(String tiSignum) {
		this.tiSignum = tiSignum;
	}

	public String getDocuments() {
		return documents;
	}

	public void setDocuments(String documents) {
		this.documents = documents;
	}

	public List<String> getFileNames() {
		try {
			String[] names = documents.split("//");
			fileNames = new ArrayList<String>();

			for (int i = 1; i < names.length; i++) {
				fileNames.add(names[i]);
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return fileNames;
	}

	public void setFileNames(List<String> fileNames) {
		this.fileNames = fileNames;
	}

	public int getFileNumber() {
		try {
			String[] names = documents.split("//");

			fileNumber = names.length - 1;
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return fileNumber;
	}

	/**
	 * @return the nSignum
	 */
	public String getnSignum() {
		return nSignum;
	}

	/**
	 * @param nSignum
	 *            the nSignum to set
	 */
	public void setnSignum(String nSignum) {
		this.nSignum = nSignum;
	}

	/**
	 * @return the ruloPipeLink
	 */
	public RuloPipeLink getRuloPipeLink() {
		return ruloPipeLink;
	}

	/**
	 * @param ruloPipeLink
	 *            the ruloPipeLink to set
	 */
	public void setRuloPipeLink(RuloPipeLink ruloPipeLink) {
		this.ruloPipeLink = ruloPipeLink;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the ae
	 */
	public BigDecimal getAe() {
		return ae;
	}

	/**
	 * @param ae
	 *            the ae to set
	 */
	public void setAe(BigDecimal ae) {
		this.ae = ae;
	}

	/**
	 * @return the al
	 */
	public BigDecimal getAl() {
		return al;
	}

	/**
	 * @param al
	 *            the al to set
	 */
	public void setAl(BigDecimal al) {
		this.al = al;
	}

	/**
	 * @return the b
	 */
	public BigDecimal getB() {
		return b;
	}

	/**
	 * @param b
	 *            the b to set
	 */
	public void setB(BigDecimal b) {
		this.b = b;
	}

	/**
	 * @return the c
	 */
	public BigDecimal getC() {
		return c;
	}

	/**
	 * @param c
	 *            the c to set
	 */
	public void setC(BigDecimal c) {
		this.c = c;
	}

	/**
	 * @return the ce1
	 */
	public BigDecimal getCe1() {
		return ce1;
	}

	/**
	 * @param ce1
	 *            the ce1 to set
	 */
	public void setCe1(BigDecimal ce1) {
		this.ce1 = ce1;
	}

	/**
	 * @return the ce2
	 */
	public BigDecimal getCe2() {
		return ce2;
	}

	/**
	 * @param ce2
	 *            the ce2 to set
	 */
	public void setCe2(BigDecimal ce2) {
		this.ce2 = ce2;
	}

	/**
	 * @return the co
	 */
	public BigDecimal getCo() {
		return co;
	}

	/**
	 * @param co
	 *            the co to set
	 */
	public void setCo(BigDecimal co) {
		this.co = co;
	}

	/**
	 * @return the cr
	 */
	public BigDecimal getCr() {
		return cr;
	}

	/**
	 * @param cr
	 *            the cr to set
	 */
	public void setCr(BigDecimal cr) {
		this.cr = cr;
	}

	/**
	 * @return the cu
	 */
	public BigDecimal getCu() {
		return cu;
	}

	/**
	 * @param cu
	 *            the cu to set
	 */
	public void setCu(BigDecimal cu) {
		this.cu = cu;
	}

	/**
	 * @return the w
	 */
	public BigDecimal getW() {
		return w;
	}

	/**
	 * @param w
	 *            the w to set
	 */
	public void setW(BigDecimal w) {
		this.w = w;
	}

	/**
	 * @return the as
	 */
	public BigDecimal getAs() {
		return as;
	}

	/**
	 * @param as
	 *            the as to set
	 */
	public void setAs(BigDecimal as) {
		this.as = as;
	}

	/**
	 * @return the sb
	 */
	public BigDecimal getSb() {
		return sb;
	}

	/**
	 * @param sb
	 *            the sb to set
	 */
	public void setSb(BigDecimal sb) {
		this.sb = sb;
	}

	/**
	 * @return the sn
	 */
	public BigDecimal getSn() {
		return sn;
	}

	/**
	 * @param sn
	 *            the sn to set
	 */
	public void setSn(BigDecimal sn) {
		this.sn = sn;
	}

	/**
	 * @return the pb
	 */
	public BigDecimal getPb() {
		return pb;
	}

	/**
	 * @param pb
	 *            the pb to set
	 */
	public void setPb(BigDecimal pb) {
		this.pb = pb;
	}

	/**
	 * @return the ca
	 */
	public BigDecimal getCa() {
		return ca;
	}

	/**
	 * @param ca
	 *            the ca to set
	 */
	public void setCa(BigDecimal ca) {
		this.ca = ca;
	}

	/**
	 * @return the mn
	 */
	public BigDecimal getMn() {
		return mn;
	}

	/**
	 * @param mn
	 *            the mn to set
	 */
	public void setMn(BigDecimal mn) {
		this.mn = mn;
	}

	/**
	 * @return the mo
	 */
	public BigDecimal getMo() {
		return mo;
	}

	/**
	 * @param mo
	 *            the mo to set
	 */
	public void setMo(BigDecimal mo) {
		this.mo = mo;
	}

	/**
	 * @return the n
	 */
	public BigDecimal getN() {
		return n;
	}

	/**
	 * @param n
	 *            the n to set
	 */
	public void setN(BigDecimal n) {
		this.n = n;
	}

	/**
	 * @return the nb
	 */
	public BigDecimal getNb() {
		return nb;
	}

	/**
	 * @param nb
	 *            the nb to set
	 */
	public void setNb(BigDecimal nb) {
		this.nb = nb;
	}

	/**
	 * @return the ni
	 */
	public BigDecimal getNi() {
		return ni;
	}

	/**
	 * @param ni
	 *            the ni to set
	 */
	public void setNi(BigDecimal ni) {
		this.ni = ni;
	}

	/**
	 * @return the p
	 */
	public BigDecimal getP() {
		return p;
	}

	/**
	 * @param p
	 *            the p to set
	 */
	public void setP(BigDecimal p) {
		this.p = p;
	}

	/**
	 * @return the s
	 */
	public BigDecimal getS() {
		return s;
	}

	/**
	 * @param s
	 *            the s to set
	 */
	public void setS(BigDecimal s) {
		this.s = s;
	}

	/**
	 * @return the si
	 */
	public BigDecimal getSi() {
		return si;
	}

	/**
	 * @param si
	 *            the si to set
	 */
	public void setSi(BigDecimal si) {
		this.si = si;
	}

	/**
	 * @return the ti
	 */
	public BigDecimal getTi() {
		return ti;
	}

	/**
	 * @param ti
	 *            the ti to set
	 */
	public void setTi(BigDecimal ti) {
		this.ti = ti;
	}

	/**
	 * @return the v
	 */
	public BigDecimal getV() {
		return v;
	}

	/**
	 * @param v
	 *            the v to set
	 */
	public void setV(BigDecimal v) {
		this.v = v;
	}

	/**
	 * @return the sampleNo
	 */
	public String getSampleNo() {
		return sampleNo;
	}

	/**
	 * @param sampleNo
	 *            the sampleNo to set
	 */
	public void setSampleNo(String sampleNo) {
		this.sampleNo = sampleNo;
	}

	/**
	 * @return the partiNo
	 */
	public String getPartiNo() {
		return partiNo;
	}

	/**
	 * @param partiNo
	 *            the partiNo to set
	 */
	public void setPartiNo(String partiNo) {
		this.partiNo = partiNo;
	}

}