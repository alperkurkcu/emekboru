package com.emekboru.jpa.istakipformu;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the is_takip_formu_kumlama_devam_islemler database
 * table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "IsTakipFormuKumlamaDevamIslemler.findAll", query = "SELECT r FROM IsTakipFormuKumlamaDevamIslemler r WHERE r.salesItem.itemId=:prmItemId and r.icDis=:prmIcDis") })
@Table(name = "is_takip_formu_kumlama_devam_islemler")
public class IsTakipFormuKumlamaDevamIslemler implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IS_TAKIP_FORMU_KUMLAMA_DEVAM_ISLEMLER_ID_GENERATOR", sequenceName = "IS_TAKIP_FORMU_KUMLAMA_DEVAM_ISLEMLER_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IS_TAKIP_FORMU_KUMLAMA_DEVAM_ISLEMLER_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(length = 2147483647, name = "aciklamalar")
	private String aciklamalar;

	@Column(name = "ciglenme_noktasi")
	private float ciglenmeNoktasi;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name = "sales_item_id", nullable = false)
	// private Integer salesItemId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	private SalesItem salesItem;

	@Column(name = "kumlama_hizi")
	private Integer kumlamaHizi;

	@Column(name = "malzeme_sarfiyat")
	private Integer malzemeSarfiyat;

	private float rh;

	// @Column(name = "pipe_id", nullable = false)
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@Column(length = 32)
	private String puruzluluk;

	@Column(name = "toz_testi")
	private Integer tozTesti;

	@Column(name = "vardiya")
	private Integer vardiya;

	@Column(name = "yuzey_kalitesi")
	private float yuzeyKalitesi;

	@Column(name = "yuzey_sicakligi")
	private float yuzeySicakligi;

	@Column(name = "ic_dis", nullable = false)
	private Integer icDis;

	public IsTakipFormuKumlamaDevamIslemler() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklamalar() {
		return this.aciklamalar;
	}

	public void setAciklamalar(String aciklamalar) {
		this.aciklamalar = aciklamalar;
	}

	public float getCiglenmeNoktasi() {
		return this.ciglenmeNoktasi;
	}

	public void setCiglenmeNoktasi(float ciglenmeNoktasi) {
		this.ciglenmeNoktasi = ciglenmeNoktasi;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getKumlamaHizi() {
		return this.kumlamaHizi;
	}

	public void setKumlamaHizi(Integer kumlamaHizi) {
		this.kumlamaHizi = kumlamaHizi;
	}

	public Integer getMalzemeSarfiyat() {
		return this.malzemeSarfiyat;
	}

	public void setMalzemeSarfiyat(Integer malzemeSarfiyat) {
		this.malzemeSarfiyat = malzemeSarfiyat;
	}

	public String getPuruzluluk() {
		return this.puruzluluk;
	}

	public void setPuruzluluk(String puruzluluk) {
		this.puruzluluk = puruzluluk;
	}

	public Integer getTozTesti() {
		return this.tozTesti;
	}

	public void setTozTesti(Integer tozTesti) {
		this.tozTesti = tozTesti;
	}

	public Integer getVardiya() {
		return this.vardiya;
	}

	public void setVardiya(Integer vardiya) {
		this.vardiya = vardiya;
	}

	public float getYuzeyKalitesi() {
		return this.yuzeyKalitesi;
	}

	public void setYuzeyKalitesi(float yuzeyKalitesi) {
		this.yuzeyKalitesi = yuzeyKalitesi;
	}

	public float getYuzeySicakligi() {
		return this.yuzeySicakligi;
	}

	public void setYuzeySicakligi(float yuzeySicakligi) {
		this.yuzeySicakligi = yuzeySicakligi;
	}

	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public Integer getIcDis() {
		return icDis;
	}

	public void setIcDis(Integer icDis) {
		this.icDis = icDis;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public float getRh() {
		return rh;
	}

	public void setRh(float rh) {
		this.rh = rh;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

}