package com.emekboru.jpa.kaplamagirditest;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the test_hammadde_girdi_epoksi_toz_eta1_sonuc
 * database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestHammaddeGirdiEpoksiTozEta1Sonuc.findAll", query = "SELECT r FROM TestHammaddeGirdiEpoksiTozEta1Sonuc r WHERE r.coatRawMaterial.coatMaterialId=:prmMaterialId") })
@Table(name = "test_hammadde_girdi_epoksi_toz_eta1_sonuc")
public class TestHammaddeGirdiEpoksiTozEta1Sonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_HAMMADDE_GIRDI_EPOKSI_TOZ_ETA1_SONUC_ID_GENERATOR", sequenceName = "TEST_HAMMADDE_GIRDI_EPOKSI_TOZ_ETA1_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_HAMMADDE_GIRDI_EPOKSI_TOZ_ETA1_SONUC_ID_GENERATOR")
	@Column(name = "ID")
	private Integer id;

	// @Column(name = "coat_material_id")
	// private Integer coatMaterialId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coat_material_id", referencedColumnName = "coat_material_id", insertable = false)
	private CoatRawMaterial coatRawMaterial;

	@Column(name = "dh")
	private String dh;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "kurlenmis_film")
	private String kurlenmisFilm;

	@Column(name = "numune_agirligi")
	private String numuneAgirligi;

	@Column(name = "parti_no")
	private String partiNo;

	private Boolean sonuc;

	private String standard;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi", nullable = false)
	private Date testTarihi;

	@Column(name = "toz_boya")
	private String tozBoya;

	@Column(name = "uretici_degeri_tg1")
	private String ureticiDegeriTg1;

	@Column(name = "uretici_degeri_tg2")
	private String ureticiDegeriTg2;

	@Column(name = "uretici_degeri_dh")
	private String ureticiDegeridh;

	@Column(name = "rapor_no")
	private String raporNo;

	public TestHammaddeGirdiEpoksiTozEta1Sonuc() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Boolean getSonuc() {
		return this.sonuc;
	}

	public void setSonuc(Boolean sonuc) {
		this.sonuc = sonuc;
	}

	public String getStandard() {
		return this.standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	/**
	 * @return the coatRawMaterial
	 */
	public CoatRawMaterial getCoatRawMaterial() {
		return coatRawMaterial;
	}

	/**
	 * @param coatRawMaterial
	 *            the coatRawMaterial to set
	 */
	public void setCoatRawMaterial(CoatRawMaterial coatRawMaterial) {
		this.coatRawMaterial = coatRawMaterial;
	}

	/**
	 * @return the dh
	 */
	public String getDh() {
		return dh;
	}

	/**
	 * @param dh
	 *            the dh to set
	 */
	public void setDh(String dh) {
		this.dh = dh;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the kurlenmisFilm
	 */
	public String getKurlenmisFilm() {
		return kurlenmisFilm;
	}

	/**
	 * @param kurlenmisFilm
	 *            the kurlenmisFilm to set
	 */
	public void setKurlenmisFilm(String kurlenmisFilm) {
		this.kurlenmisFilm = kurlenmisFilm;
	}

	/**
	 * @return the numuneAgirligi
	 */
	public String getNumuneAgirligi() {
		return numuneAgirligi;
	}

	/**
	 * @param numuneAgirligi
	 *            the numuneAgirligi to set
	 */
	public void setNumuneAgirligi(String numuneAgirligi) {
		this.numuneAgirligi = numuneAgirligi;
	}

	/**
	 * @return the partiNo
	 */
	public String getPartiNo() {
		return partiNo;
	}

	/**
	 * @param partiNo
	 *            the partiNo to set
	 */
	public void setPartiNo(String partiNo) {
		this.partiNo = partiNo;
	}

	/**
	 * @return the testTarihi
	 */
	public Date getTestTarihi() {
		return testTarihi;
	}

	/**
	 * @param testTarihi
	 *            the testTarihi to set
	 */
	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	/**
	 * @return the tozBoya
	 */
	public String getTozBoya() {
		return tozBoya;
	}

	/**
	 * @param tozBoya
	 *            the tozBoya to set
	 */
	public void setTozBoya(String tozBoya) {
		this.tozBoya = tozBoya;
	}

	/**
	 * @return the ureticiDegeriTg1
	 */
	public String getUreticiDegeriTg1() {
		return ureticiDegeriTg1;
	}

	/**
	 * @param ureticiDegeriTg1
	 *            the ureticiDegeriTg1 to set
	 */
	public void setUreticiDegeriTg1(String ureticiDegeriTg1) {
		this.ureticiDegeriTg1 = ureticiDegeriTg1;
	}

	/**
	 * @return the ureticiDegeriTg2
	 */
	public String getUreticiDegeriTg2() {
		return ureticiDegeriTg2;
	}

	/**
	 * @param ureticiDegeriTg2
	 *            the ureticiDegeriTg2 to set
	 */
	public void setUreticiDegeriTg2(String ureticiDegeriTg2) {
		this.ureticiDegeriTg2 = ureticiDegeriTg2;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the ureticiDegeridh
	 */
	public String getUreticiDegeridh() {
		return ureticiDegeridh;
	}

	/**
	 * @param ureticiDegeridh
	 *            the ureticiDegeridh to set
	 */
	public void setUreticiDegeridh(String ureticiDegeridh) {
		this.ureticiDegeridh = ureticiDegeridh;
	}

	/**
	 * @return the raporNo
	 */
	public String getRaporNo() {
		return raporNo;
	}

	/**
	 * @param raporNo
	 *            the raporNo to set
	 */
	public void setRaporNo(String raporNo) {
		this.raporNo = raporNo;
	}
}