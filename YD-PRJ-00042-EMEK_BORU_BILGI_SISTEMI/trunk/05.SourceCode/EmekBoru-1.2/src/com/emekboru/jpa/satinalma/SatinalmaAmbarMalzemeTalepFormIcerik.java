package com.emekboru.jpa.satinalma;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Department;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpa.stok.StokTanimlarBirim;
import com.emekboru.jpa.stok.StokUrunKartlari;

/**
 * The persistent class for the satinalma_ambar_malzeme_talep_form_icerik
 * database table.
 **/
@Entity
@NamedQueries({ @NamedQuery(name = "SatinalmaAmbarMalzemeTalepFormIcerik.findAll", query = "SELECT r FROM SatinalmaAmbarMalzemeTalepFormIcerik r where r.satinalmaAmbarMalzemeTalepForm.id=:prmFormId order by r.id") })
@Table(name = "satinalma_ambar_malzeme_talep_form_icerik")
public class SatinalmaAmbarMalzemeTalepFormIcerik implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SATINALMA_AMBAR_MALZEME_TALEP_FORM_ICERIK_ID_GENERATOR", sequenceName = "SATINALMA_AMBAR_MALZEME_TALEP_FORM_ICERIK_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SATINALMA_AMBAR_MALZEME_TALEP_FORM_ICERIK_ID_GENERATOR")
	@Column(name = "ID")
	private Integer id;

	// @Column(name = "form_id")
	// private Integer formId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "form_id", referencedColumnName = "ID", insertable = false)
	private SatinalmaAmbarMalzemeTalepForm satinalmaAmbarMalzemeTalepForm;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "bakim")
	private Boolean bakim;

	// @Column(name = "birim")
	// private Integer birim;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "birim", referencedColumnName = "ID", insertable = false)
	private StokTanimlarBirim stokTanimlarBirim;

	@Column(name = "demirbas")
	private Boolean demirbas;

	@Temporal(TemporalType.DATE)
	@Column(name = "ihtiyac_suresi", nullable = false)
	private Date ihtiyacSuresi;

	// @Column(name = "malzeme_tanim")
	// private String malzemeTanim;

	// @Column(name = "malzeme_id")
	// private Integer malzemeId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "malzeme_id", referencedColumnName = "ID", insertable = false)
	private StokUrunKartlari stokUrunKartlari;

	@Column(name = "miktar")
	private Integer miktar;

	private Boolean sarf;

	@Column(name = "uretim")
	private Boolean uretim;

	@Column(name = "sira_no")
	private Integer siraNo;

	@Column(name = "kalan")
	private Integer kalan;

	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private SalesItem salesItem;

	// @Column(name = "sarf_merkezi")
	// private Integer sarfMerkezi;

	@JoinColumn(name = "sarf_merkezi", referencedColumnName = "id", insertable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private SatinalmaSarfMerkezi satinalmaSarfMerkezi;

	// @Column(name = "departman_id")
	// private Integer departmanId;

	@JoinColumn(name = "departman_id", referencedColumnName = "department_id", insertable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private Department department;

	public SatinalmaAmbarMalzemeTalepFormIcerik() {

		stokTanimlarBirim = new StokTanimlarBirim();
		department = new Department();
		satinalmaSarfMerkezi = new SatinalmaSarfMerkezi();
	}

	// setters getters
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public SatinalmaAmbarMalzemeTalepForm getSatinalmaAmbarMalzemeTalepForm() {
		return satinalmaAmbarMalzemeTalepForm;
	}

	public void setSatinalmaAmbarMalzemeTalepForm(
			SatinalmaAmbarMalzemeTalepForm satinalmaAmbarMalzemeTalepForm) {
		this.satinalmaAmbarMalzemeTalepForm = satinalmaAmbarMalzemeTalepForm;
	}

	public Timestamp getEklemeZamani() {
		return eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Boolean getBakim() {
		return bakim;
	}

	public void setBakim(Boolean bakim) {
		this.bakim = bakim;
	}

	// public String getBirim() {
	// return birim;
	// }
	//
	// public void setBirim(String birim) {
	// this.birim = birim;
	// }

	public Boolean getDemirbas() {
		return demirbas;
	}

	public void setDemirbas(Boolean demirbas) {
		this.demirbas = demirbas;
	}

	public Date getIhtiyacSuresi() {
		return ihtiyacSuresi;
	}

	public void setIhtiyacSuresi(Date ihtiyacSuresi) {
		this.ihtiyacSuresi = ihtiyacSuresi;
	}

	public Boolean getSarf() {
		return sarf;
	}

	public void setSarf(Boolean sarf) {
		this.sarf = sarf;
	}

	public Boolean getUretim() {
		return uretim;
	}

	public void setUretim(Boolean uretim) {
		this.uretim = uretim;
	}

	public Integer getSiraNo() {
		return siraNo;
	}

	public void setSiraNo(Integer siraNo) {
		this.siraNo = siraNo;
	}

	public StokUrunKartlari getStokUrunKartlari() {
		return stokUrunKartlari;
	}

	public void setStokUrunKartlari(StokUrunKartlari stokUrunKartlari) {
		this.stokUrunKartlari = stokUrunKartlari;
	}

	public StokTanimlarBirim getStokTanimlarBirim() {
		return stokTanimlarBirim;
	}

	public void setStokTanimlarBirim(StokTanimlarBirim stokTanimlarBirim) {
		this.stokTanimlarBirim = stokTanimlarBirim;
	}

	public Integer getKalan() {
		return kalan;
	}

	public void setKalan(Integer kalan) {
		this.kalan = kalan;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public Integer getMiktar() {
		return miktar;
	}

	public void setMiktar(Integer miktar) {
		this.miktar = miktar;
	}

	public SatinalmaSarfMerkezi getSatinalmaSarfMerkezi() {
		return satinalmaSarfMerkezi;
	}

	public void setSatinalmaSarfMerkezi(
			SatinalmaSarfMerkezi satinalmaSarfMerkezi) {
		this.satinalmaSarfMerkezi = satinalmaSarfMerkezi;
	}

}