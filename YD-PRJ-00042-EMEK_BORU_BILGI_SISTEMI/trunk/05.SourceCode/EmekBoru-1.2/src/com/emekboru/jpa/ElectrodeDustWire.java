package com.emekboru.jpa;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.emekboru.config.Config;
import com.emekboru.config.Materials.Statuses;
import com.emekboru.jpa.sales.customer.SalesCustomer;

@Entity
@NamedQueries({
		@NamedQuery(name = "ElectrodeDustWire.findDust", query = "select o from ElectrodeDustWire o where (o.salesItemId=0 or o.salesItemId=:prmItemId) and o.type=2 order by o.kalite desc, o.electrodeDustWireId"),
		@NamedQuery(name = "ElectrodeDustWire.findWire", query = "select o from ElectrodeDustWire o where (o.salesItemId=0 or o.salesItemId=:prmItemId) and o.type=3 order by o.kalite desc, o.electrodeDustWireId") })
@Table(name = "electrode_dust_wire")
public class ElectrodeDustWire implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "electrode_dust_wire_id")
	@SequenceGenerator(name = "ELECTRODE_DUST_WIRE_GENERATOR", sequenceName = "ELECTRODE_DUST_WIRE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ELECTRODE_DUST_WIRE_GENERATOR")
	private int electrodeDustWireId;

	private double cap;

	@Column(name = "cinsi")
	private String cinsi;

	@Temporal(TemporalType.DATE)
	@Column(name = "entrance_date")
	private Date entranceDate;

	@Column(name = "kalite")
	private String kalite;

	@Column(name = "location")
	private String location;

	@Column(name = "miktari")
	private double miktari;

	@Column(name = "part_number")
	private String partNumber;

	@Column(name = "remaining_amount")
	private double remainingAmount;

	private int type;

	@Column(name = "status_id")
	private int statusId;

	private String note;

	// // set it when allocating material to an order
	// @Column(name = "order_id")
	// private int orderId;

	@Column(name = "fatura_number")
	private String faturaNumber;

	@Column(name = "irsaliye_number")
	private String irsaliyeNumber;

	@Column(name = "trade_name")
	private String tradeName;

	@OneToMany(mappedBy = "edw", fetch = FetchType.LAZY)
	private List<SoldEdw> soldTo;

	@OneToMany(mappedBy = "edw", fetch = FetchType.LAZY)
	private List<EdwPipeLink> edwPipeLinks;

	@OneToMany(mappedBy = "edw", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<MachineEdwLink> machineEdwLinks;

	// @ManyToOne(fetch = FetchType.EAGER)
	// @JoinColumn(name = "customer_id", referencedColumnName = "customer_id",
	// insertable = false)
	// private Customer customer;

	// entegre
	// set it when allocating material to an order
	@Column(name = "sales_item_id")
	private int salesItemId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sales_customer_id", referencedColumnName = "sales_customer_id", insertable = false)
	private SalesCustomer salesCustomer;
	// entegre

	@Transient
	private Statuses status;

	public ElectrodeDustWire() {

		salesCustomer = new SalesCustomer();
	}

	public Statuses getStatus(Config config) {

		status = config.getMaterials().findStatusById(statusId);
		return status;
	}

	public void setStatus(Config config, int statusId) {

		status = config.getMaterials().findStatusById(statusId);
		this.statusId = statusId;
	}

	@Override
	public boolean equals(Object obj) {

		if (!(obj instanceof ElectrodeDustWire)) {
			return false;
		}

		if (this == obj) {
			return true;
		}

		if (electrodeDustWireId == (((ElectrodeDustWire) obj).electrodeDustWireId)) {
			return true;
		}

		return false;
	}

	/**
	 * *************************************************************************
	 * *** GETTERS AND SETTERS SECTION ********************
	 * /********************
	 * *********************************************************
	 */
	public int getElectrodeDustWireId() {
		return this.electrodeDustWireId;
	}

	public void setElectrodeDustWireId(int electrodeDustWireId) {
		this.electrodeDustWireId = electrodeDustWireId;
	}

	public double getCap() {
		return this.cap;
	}

	public void setCap(double cap) {
		this.cap = cap;
	}

	public String getCinsi() {
		return this.cinsi;
	}

	public void setCinsi(String cinsi) {
		this.cinsi = cinsi;
	}

	public Date getEntranceDate() {
		return this.entranceDate;
	}

	public void setEntranceDate(Date entranceDate) {
		this.entranceDate = entranceDate;
	}

	public String getKalite() {
		return this.kalite;
	}

	public void setKalite(String kalite) {
		this.kalite = kalite;
	}

	public double getMiktari() {
		return this.miktari;
	}

	public void setMiktari(double miktari) {
		this.miktari = miktari;
	}

	public String getPartNumber() {
		return this.partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public double getRemainingAmount() {
		return this.remainingAmount;
	}

	public void setRemainingAmount(double remainingAmount) {
		this.remainingAmount = remainingAmount;
	}

	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public Statuses getStatus() {
		return status;
	}

	public void setStatus(Statuses status) {

		statusId = status.getId();
		this.status = status;
	}

	public void setType(int type) {
		this.type = type;
	}

	public List<SoldEdw> getSoldTo() {
		return soldTo;
	}

	public void setSoldTo(List<SoldEdw> soldTo) {
		this.soldTo = soldTo;
	}

	public String getTradeName() {
		return tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	public List<EdwPipeLink> getEdwPipeLinks() {
		return edwPipeLinks;
	}

	public void setEdwPipeLinks(List<EdwPipeLink> edwPipeLinks) {
		this.edwPipeLinks = edwPipeLinks;
	}

	public String getFaturaNumber() {
		return faturaNumber;
	}

	public void setFaturaNumber(String faturaNumber) {
		this.faturaNumber = faturaNumber;
	}

	public String getIrsaliyeNumber() {
		return irsaliyeNumber;
	}

	public void setIrsaliyeNumber(String irsaliyeNumber) {
		this.irsaliyeNumber = irsaliyeNumber;
	}

	public List<MachineEdwLink> getMachineEdwLinks() {
		return machineEdwLinks;
	}

	public void setMachineEdwLinks(List<MachineEdwLink> machineEdwLinks) {
		this.machineEdwLinks = machineEdwLinks;
	}

	public SalesCustomer getSalesCustomer() {
		return salesCustomer;
	}

	public void setSalesCustomer(SalesCustomer salesCustomer) {
		this.salesCustomer = salesCustomer;
	}

	public int getSalesItemId() {
		return salesItemId;
	}

	public void setSalesItemId(int salesItemId) {
		this.salesItemId = salesItemId;
	}

}