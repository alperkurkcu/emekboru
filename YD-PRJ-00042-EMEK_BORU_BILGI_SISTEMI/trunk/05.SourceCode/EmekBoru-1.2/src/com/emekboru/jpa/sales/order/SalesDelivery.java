package com.emekboru.jpa.sales.order;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "sales_delivery")
public class SalesDelivery implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6506263122829829763L;

	@Id
	@Column(name = "delivery_id")
	private int deliveryId;

	@Column(name = "title")
	private String name;

	@OneToMany
	private List<SalesOrder> orderList;

	public SalesDelivery() {
	}

	@Override
	public boolean equals(Object o) {
		return this.getDeliveryId() == ((SalesDelivery) o).getDeliveryId();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public int getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(int deliveryId) {
		this.deliveryId = deliveryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SalesOrder> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<SalesOrder> orderList) {
		this.orderList = orderList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
