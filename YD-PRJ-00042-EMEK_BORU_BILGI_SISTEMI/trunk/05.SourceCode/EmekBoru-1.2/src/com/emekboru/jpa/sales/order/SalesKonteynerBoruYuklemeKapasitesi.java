package com.emekboru.jpa.sales.order;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the sales_konteyner_boru_yukleme_kapasitesi database
 * table.
 * 
 */
@Entity
@Table(name = "sales_konteyner_boru_yukleme_kapasitesi")
public class SalesKonteynerBoruYuklemeKapasitesi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SALES_KONTEYNER_BORU_YUKLEME_KAPASITESI_ID_GENERATOR", sequenceName = "SALES_KONTEYNER_BORU_YUKLEME_KAPASITESI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_KONTEYNER_BORU_YUKLEME_KAPASITESI_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "boru_capi")
	private float boruCapi;

	@Column(name = "hc_kapasite")
	private Integer hcKapasite;

	@Column(name = "ot_kapasite")
	private Integer otKapasite;

	public boolean equals(Object object) {
		// Basic checks.
		if (object == this)
			return true;
		if (object == null || getClass() != object.getClass())
			return false;

		// All passed.
		return true;
	}

	public SalesKonteynerBoruYuklemeKapasitesi() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public float getBoruCapi() {
		return this.boruCapi;
	}

	public void setBoruCapi(float boruCapi) {
		this.boruCapi = boruCapi;
	}

	public Integer getHcKapasite() {
		return this.hcKapasite;
	}

	public void setHcKapasite(Integer hcKapasite) {
		this.hcKapasite = hcKapasite;
	}

	public Integer getOtKapasite() {
		return this.otKapasite;
	}

	public void setOtKapasite(Integer otKapasite) {
		this.otKapasite = otKapasite;
	}

}