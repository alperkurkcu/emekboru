package com.emekboru.jpa.tahribatsiztestsonuc;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.Employee;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the test_tahribatsiz_torna database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "TestTahribatsizTorna.findAll", query = "SELECT r FROM TestTahribatsizTorna r WHERE r.pipe.pipeId=:prmPipeId order by r.eklemeZamani"),
		@NamedQuery(name = "TestTahribatsizTorna.getBySalesItem", query = "SELECT r FROM TestTahribatsizTorna r WHERE r.pipe.salesItem.itemId=:prmSalesItem") })
@Table(name = "test_tahribatsiz_torna")
public class TestTahribatsizTorna implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_TAHRIBATSIZ_TORNA_ID_GENERATOR", sequenceName = "TEST_TAHRIBATSIZ_TORNA_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_TAHRIBATSIZ_TORNA_ID_GENERATOR")
	@Column(name = "ID", unique = true, nullable = false)
	private Integer id;

	private Boolean durum;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser user;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenUser;

	// @Column(name = "manuel_ut_id")
	// private Integer manuelUtId;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "manuel_ut_id", referencedColumnName = "ID", insertable = false, updatable = true)
	private TestTahribatsizManuelUtSonuc testTahribatsizManuelUtSonuc;

	// @Column(name = "fl_id")
	// private Integer flId;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fl_id", referencedColumnName = "ID", insertable = false, updatable = true)
	private TestTahribatsizFloroskopikSonuc testTahribatsizFloroskopikSonuc;

	@Column(name = "muayene_baslama_zamani")
	private Timestamp muayeneBaslamaZamani;

	// @Column(name = "muayene_baslama_kullanici")
	// private Integer muayeneBaslamaKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "muayene_baslama_kullanici", referencedColumnName = "ID")
	private SystemUser muayeneBaslamaUser;

	@Basic
	@Column(name = "pipe_id", insertable = false, updatable = false)
	private Integer pipeId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@Column(name = "muayene_bitis_zamani")
	private Timestamp muayeneBitisZamani;

	// @Column(name = "muayene_bitis_kullanici")
	// private Integer muayeneBitisKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "muayene_bitis_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser muayeneBitisUser;

	@Column(name = "islem_yapilan_uc")
	private Integer islemYapilanUc;

	// @Column(name = "yardimci_kullanici", nullable = false)
	// private Integer yardimciKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "yardımci_kullanici", referencedColumnName = "employee_id", insertable = false)
	private Employee yardimciKullanici;

	@Column(name = "kullanilan_elmas_adedi")
	private Integer kullanilanElmasAdedi;

	public TestTahribatsizTorna() {

	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getDurum() {
		return this.durum;
	}

	public void setDurum(Boolean durum) {
		this.durum = durum;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public TestTahribatsizManuelUtSonuc getTestTahribatsizManuelUtSonuc() {
		return testTahribatsizManuelUtSonuc;
	}

	public void setTestTahribatsizManuelUtSonuc(
			TestTahribatsizManuelUtSonuc testTahribatsizManuelUtSonuc) {
		this.testTahribatsizManuelUtSonuc = testTahribatsizManuelUtSonuc;
	}

	public SystemUser getUser() {
		return user;
	}

	public void setUser(SystemUser user) {
		this.user = user;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public Timestamp getMuayeneBaslamaZamani() {
		return muayeneBaslamaZamani;
	}

	public void setMuayeneBaslamaZamani(Timestamp muayeneBaslamaZamani) {
		this.muayeneBaslamaZamani = muayeneBaslamaZamani;
	}

	public SystemUser getMuayeneBaslamaUser() {
		return muayeneBaslamaUser;
	}

	public void setMuayeneBaslamaUser(SystemUser muayeneBaslamaUser) {
		this.muayeneBaslamaUser = muayeneBaslamaUser;
	}

	public Timestamp getMuayeneBitisZamani() {
		return muayeneBitisZamani;
	}

	public void setMuayeneBitisZamani(Timestamp muayeneBitisZamani) {
		this.muayeneBitisZamani = muayeneBitisZamani;
	}

	public SystemUser getMuayeneBitisUser() {
		return muayeneBitisUser;
	}

	public void setMuayeneBitisUser(SystemUser muayeneBitisUser) {
		this.muayeneBitisUser = muayeneBitisUser;
	}

	public TestTahribatsizFloroskopikSonuc getTestTahribatsizFloroskopikSonuc() {
		return testTahribatsizFloroskopikSonuc;
	}

	public void setTestTahribatsizFloroskopikSonuc(
			TestTahribatsizFloroskopikSonuc testTahribatsizFloroskopikSonuc) {
		this.testTahribatsizFloroskopikSonuc = testTahribatsizFloroskopikSonuc;
	}

	public Integer getIslemYapilanUc() {
		return islemYapilanUc;
	}

	public void setIslemYapilanUc(Integer islemYapilanUc) {
		this.islemYapilanUc = islemYapilanUc;
	}

	public SystemUser getGuncelleyenUser() {
		return guncelleyenUser;
	}

	public void setGuncelleyenUser(SystemUser guncelleyenUser) {
		this.guncelleyenUser = guncelleyenUser;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the yardimciKullanici
	 */
	public Employee getYardimciKullanici() {
		return yardimciKullanici;
	}

	/**
	 * @param yardimciKullanici
	 *            the yardimciKullanici to set
	 */
	public void setYardimciKullanici(Employee yardimciKullanici) {
		this.yardimciKullanici = yardimciKullanici;
	}

	/**
	 * @return the kullanilanElmasAdedi
	 */
	public Integer getKullanilanElmasAdedi() {
		return kullanilanElmasAdedi;
	}

	/**
	 * @param kullanilanElmasAdedi
	 *            the kullanilanElmasAdedi to set
	 */
	public void setKullanilanElmasAdedi(Integer kullanilanElmasAdedi) {
		this.kullanilanElmasAdedi = kullanilanElmasAdedi;
	}

}