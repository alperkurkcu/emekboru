package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="maintenance_executed_activities")
public class MaintenanceExecutedActivities implements Serializable {

	private static final long serialVersionUID = -3746201528806920066L;

	
	@Id
	@Column(name="maintenance_executed_activities_id")
	private Integer maintenanceExecutedActivitiesId;

	@Column(name="result")
	private Boolean result;
	
	@Column(name = "outsourced")
	private Boolean outsourced;
	
	@Column(name = "outsourced_to")
	private String outsourcedTo;
	
	@Column(name = "result_short_desc")
	private String resultShortDesc;
	
	@Column(name = "result_long_desc")
	private String resultLongDesc;
	
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "maintenance_plan_executed_id", referencedColumnName = "maintenance_plan_executed_id")
	private MaintenancePlanExecuted maintenancePlanExecuted;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "maintenance_activity_id", referencedColumnName = "maintenance_activity_id")
	private MaintenanceActivity 	maintenanceActivity;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "employee_id", referencedColumnName = "employee_id")
	private Employee employee;
	
	public MaintenanceExecutedActivities(){
		
	}

	/**
	 * 		GETTERS AND SETTERS
	 */
	public Integer getMaintenanceExecutedActivitiesId() {
		return maintenanceExecutedActivitiesId;
	}

	public void setMaintenanceExecutedActivitiesId(
			Integer maintenanceExecutedActivitiesId) {
		this.maintenanceExecutedActivitiesId = maintenanceExecutedActivitiesId;
	}

	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}

	public Boolean getOutsourced() {
		return outsourced;
	}

	public void setOutsourced(Boolean outsourced) {
		this.outsourced = outsourced;
	}

	public String getOutsourcedTo() {
		return outsourcedTo;
	}

	public void setOutsourcedTo(String outsourcedTo) {
		this.outsourcedTo = outsourcedTo;
	}

	public String getResultShortDesc() {
		return resultShortDesc;
	}

	public void setResultShortDesc(String resultShortDesc) {
		this.resultShortDesc = resultShortDesc;
	}

	public String getResultLongDesc() {
		return resultLongDesc;
	}

	public void setResultLongDesc(String resultLongDesc) {
		this.resultLongDesc = resultLongDesc;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public MaintenancePlanExecuted getMaintenancePlanExecuted() {
		return maintenancePlanExecuted;
	}

	public void setMaintenancePlanExecuted(
			MaintenancePlanExecuted maintenancePlanExecuted) {
		this.maintenancePlanExecuted = maintenancePlanExecuted;
	}

	public MaintenanceActivity getMaintenanceActivity() {
		return maintenanceActivity;
	}

	public void setMaintenanceActivity(MaintenanceActivity maintenanceActivity) {
		this.maintenanceActivity = maintenanceActivity;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
}
