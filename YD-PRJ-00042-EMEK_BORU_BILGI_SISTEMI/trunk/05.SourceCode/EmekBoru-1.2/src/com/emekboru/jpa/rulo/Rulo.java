package com.emekboru.jpa.rulo;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.emekboru.config.Config;
import com.emekboru.config.Materials.Statuses;
import com.emekboru.jpa.MachineRuloLink;
import com.emekboru.jpa.sales.customer.SalesCustomer;
import com.emekboru.jpa.sales.order.SalesItem;

@NamedQueries({
		@NamedQuery(name = "inRuloYil", query = "SELECT r FROM Rulo r WHERE r.ruloYil = :ruloYil ORDER BY r.ruloKimlikId DESC"),
		@NamedQuery(name = "total", query = "SELECT COUNT(u) FROM Rulo u"),
		@NamedQuery(name = "Rulo.loadFirstRulo", query = "SELECT u FROM Rulo u WHERE u.ruloId=(SELECT MAX(r.ruloId) FROM Rulo r)"),
		@NamedQuery(name = "Rulo.findByRuloId", query = "SELECT r FROM Rulo r WHERE r.ruloId =:prmRuloId"),
		@NamedQuery(name = "Rulo.findByItemIdGreaterThanZero", query = "select u from Rulo u where u.salesItemId>:prmItemId order by u.ruloYil, u.ruloKimlikId"),
		@NamedQuery(name = "Rulo.findByItemIdOrAll", query = "select u from Rulo u where u.salesItemId is null or u.salesItemId<=0 or u.salesItemId=:prmItemId order by u.salesItemId, u.ruloYil, u.ruloKimlikId "),
		@NamedQuery(name = "Rulo.loadAllocatedRulos", query = "select u from Rulo u where u.salesItemId=:prmItemId order by u.salesItemId, u.ruloYil, u.ruloKimlikId"),
		@NamedQuery(name = "Rulo.findByDokumAndEtKalinligi", query = "select r from Rulo r, RuloOzellikBoyutsal rb where r.ruloDokumNo=:prmDokumNo and rb.ruloOzellikBoyutsalEtKalinligi=:prmEtKalinligi and r.ruloOzellikBoyutsal.ruloOzellikBoyutsalId=rb.ruloOzellikBoyutsalId order by r.ruloId"),
		@NamedQuery(name = "Rulo.loadAvaliableRulosForSpiralMachine", query = "select u from Rulo u where u.salesItemId=:prmItemId or u.salesItemId is null order by u.salesItemId, u.ruloYil, u.ruloKimlikId"),
		@NamedQuery(name = "Rulo.findByYilKimlik", query = "select u from Rulo u where u.ruloYil=:prmRuloYil and u.ruloKimlikId=:prmRuloKimlikId"),
		@NamedQuery(name = "Rulo.totalSalesItem", query = "SELECT COUNT(r) from Rulo r WHERE r.ruloOzellikBoyutsal.ruloOzellikBoyutsalKalanMiktar>0 AND r.ruloOzellikBoyutsal.ruloOzellikBoyutsalEtKalinligi BETWEEN :paramrmThicknessMin AND :paramrmThicknessMax and r.ruloDurum not in (4,5,7,8,9)") })
@Entity
@Table(name = "rulo")
public class Rulo {
	public static final int RULO_GENEL_DURUM_BEKLEMEDE = 0;
	public static final int RULO_GENEL_DURUM_KABUL = 1;
	public static final int RULO_GENEL_DURUM_RED = 2;

	public static final int RULO_TEST_DURUM_GIRILMEMIS = 0;
	public static final int RULO_TEST_DURUM_KABUL = 1;
	public static final int RULO_TEST_DURUM_RED = 2;

	public static String STATUS_ENTERED = "Entered";
	public static String STATUS_WAITING = "Waiting";
	public static String STATUS_POS_CHECKED = "PositivelyChecked";
	public static String STATUS_NEG_CHECKED = "NegativelyChecked";
	public static String STATUS_IN_PRODUCTION = "InProduction";
	public static String STATUS_FREE = "Free";
	public static String STATUS_SOLD = "Sold";
	public static String STATUS_WASTED = "Wasted";
	public static String STATUS_DEPLETED = "Depleted";
	public static String STATUS_ALLOCATED = "Allocated";

	@Id
	@SequenceGenerator(name = "rulo_generator", sequenceName = "rulo_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rulo_generator")
	@Column(name = "rulo_id")
	private Integer ruloId;

	@Column(name = "rulo_stok_no")
	private String ruloStokNo;

	@Column(name = "rulo_bobin_no")
	private String ruloBobinNo;

	@Column(name = "rulo_etiket_no")
	private String ruloEtiketNo;

	@Column(name = "rulo_dokum_no")
	private String ruloDokumNo;

	@Column(name = "rulo_barkod_no")
	private String ruloBarkodNo;

	@Column(name = "rulo_kalite")
	private String ruloKalite;

	@Column(name = "rulo_yer")
	private String ruloYer;

	@Temporal(TemporalType.DATE)
	@Column(name = "rulo_tarih")
	private Date ruloTarih;

	@Temporal(TemporalType.DATE)
	@Column(name = "reservation_date")
	private Date reservationDate;

	@Column(name = "rulo_durum")
	private Integer ruloDurum;

	@Column(name = "rulo_genel_durum")
	private Integer ruloGenelDurum;

	@Column(name = "rulo_fatura_no")
	private String ruloFaturaNo;

	@Column(name = "rulo_irsaliye_no")
	private String ruloIrsaliyeNo;

	@Column(name = "rulo_kimlik_id")
	private String ruloKimlikId;

	@Column(name = "rulo_not")
	private String ruloNot;

	@Column(name = "islem_yapan_id")
	private int islemYapanId;

	@Transient
	private String ruloYilAndRuloKimlikId;

	@Column(name = "islem_yapan_isim")
	private String islemYapanIsim;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "rulo_boyutsal_ozellik_id", referencedColumnName = "rulo_boyutsal_ozellik_id", insertable = false)
	private RuloOzellikBoyutsal ruloOzellikBoyutsal;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "rulo_ozellik_fiziksel_girdi_id", referencedColumnName = "rulo_ozellik_fiziksel_girdi_id", insertable = false)
	private RuloOzellikFizikselGirdi ruloOzellikFizikselGirdi;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "rulo_ozellik_fiziksel_ure_id", referencedColumnName = "rulo_ozellik_fiziksel_ure_id", insertable = false)
	private RuloOzellikFizikselUretici ruloOzellikFizikselUretici;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "rulo", cascade = CascadeType.ALL)
	private List<RuloTestCekmeBoyuna> ruloTestCekmeBoyuna;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "rulo", cascade = CascadeType.ALL)
	private List<RuloTestCekmeEnine> ruloTestCekmeEnine;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "rulo", cascade = CascadeType.ALL)
	private List<RuloTestCentikDarbeBoyuna> ruloTestCentikDarbeBoyuna;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "rulo", cascade = CascadeType.ALL)
	private List<RuloTestCentikDarbeEnine> ruloTestCentikDarbeEnine;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "rulo", cascade = CascadeType.ALL)
	private List<RuloTestCentikDarbeUretici> ruloTestCentikDarbeUretici;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "rulo", cascade = CascadeType.ALL)
	private List<RuloTestKimyasalGirdi> ruloTestKimyasalGirdi;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "rulo", cascade = CascadeType.ALL)
	private List<RuloTestKimyasalUretici> ruloTestKimyasalUretici;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "rulo", cascade = CascadeType.ALL)
	private List<RuloTestDwttUretici> ruloTestDwttUretici;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "rulo", cascade = CascadeType.ALL)
	private List<RuloTestDwttGirdi> ruloTestDwttGirdi;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "rulo", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<MachineRuloLink> machineRuloLinks;

	@Column(name = "rulo_yil")
	private String ruloYil;

	@Transient
	private Integer salesCustomerId;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "sales_customer_id", referencedColumnName = "sales_customer_id", insertable = false)
	private SalesCustomer salesCustomer;

	@Column(name = "sales_item_id")
	private Integer salesItemId;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false, updatable = false)
	private SalesItem selectedSalesItemPipeRulo;

	// bi-directional many-to-one association to SalesItem
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "reserved_sales_item_id", referencedColumnName = "item_id", insertable = false)
	private SalesItem reservedSalesItem;

	@Transient
	private Statuses status;

	public Rulo() {

		ruloOzellikBoyutsal = new RuloOzellikBoyutsal();
		ruloOzellikFizikselGirdi = new RuloOzellikFizikselGirdi();
		ruloOzellikFizikselUretici = new RuloOzellikFizikselUretici();
		this.salesCustomer = new SalesCustomer();
	}

	// setters getters

	public Integer getRuloId() {
		return ruloId;
	}

	public void setRuloId(Integer ruloId) {
		this.ruloId = ruloId;
	}

	public String getRuloStokNo() {
		return ruloStokNo;
	}

	public void setRuloStokNo(String ruloStokNo) {
		this.ruloStokNo = ruloStokNo;
	}

	public String getRuloBobinNo() {
		return ruloBobinNo;
	}

	public void setRuloBobinNo(String ruloBobinNo) {
		this.ruloBobinNo = ruloBobinNo;
	}

	public String getRuloEtiketNo() {
		return ruloEtiketNo;
	}

	public void setRuloEtiketNo(String ruloEtiketNo) {
		this.ruloEtiketNo = ruloEtiketNo;
	}

	public String getRuloDokumNo() {
		return ruloDokumNo;
	}

	public void setRuloDokumNo(String ruloDokumNo) {
		this.ruloDokumNo = ruloDokumNo;
	}

	public String getRuloBarkodNo() {
		return ruloBarkodNo;
	}

	public void setRuloBarkodNo(String ruloBarkodNo) {
		this.ruloBarkodNo = ruloBarkodNo;
	}

	public String getRuloKalite() {
		return ruloKalite;
	}

	public void setRuloKalite(String ruloKalite) {
		this.ruloKalite = ruloKalite;
	}

	public String getRuloYer() {
		return ruloYer;
	}

	public void setRuloYer(String ruloYer) {
		this.ruloYer = ruloYer;
	}

	public Date getRuloTarih() {
		return ruloTarih;
	}

	public void setRuloTarih(Date ruloTarih) {
		this.ruloTarih = ruloTarih;
	}

	public Integer getRuloDurum() {
		return ruloDurum;
	}

	public void setRuloDurum(Integer ruloDurum) {
		this.ruloDurum = ruloDurum;
	}

	public String getRuloFaturaNo() {
		return ruloFaturaNo;
	}

	public void setRuloFaturaNo(String ruloFaturaNo) {
		this.ruloFaturaNo = ruloFaturaNo;
	}

	public String getRuloIrsaliyeNo() {
		return ruloIrsaliyeNo;
	}

	public void setRuloIrsaliyeNo(String ruloIrsaliyeNo) {
		this.ruloIrsaliyeNo = ruloIrsaliyeNo;
	}

	public String getRuloKimlikId() {
		return ruloKimlikId;
	}

	public void setRuloKimlikId(String ruloKimlikId) {
		this.ruloKimlikId = ruloKimlikId;
	}

	public RuloOzellikBoyutsal getRuloOzellikBoyutsal() {
		return ruloOzellikBoyutsal;
	}

	public void setRuloOzellikBoyutsal(RuloOzellikBoyutsal ruloOzellikBoyutsal) {
		this.ruloOzellikBoyutsal = ruloOzellikBoyutsal;
	}

	public RuloOzellikFizikselGirdi getRuloOzellikFizikselGirdi() {
		return ruloOzellikFizikselGirdi;
	}

	public void setRuloOzellikFizikselGirdi(
			RuloOzellikFizikselGirdi ruloOzellikFizikselGirdi) {
		this.ruloOzellikFizikselGirdi = ruloOzellikFizikselGirdi;
	}

	public RuloOzellikFizikselUretici getRuloOzellikFizikselUretici() {
		return ruloOzellikFizikselUretici;
	}

	public void setRuloOzellikFizikselUretici(
			RuloOzellikFizikselUretici ruloOzellikFizikselUretici) {
		this.ruloOzellikFizikselUretici = ruloOzellikFizikselUretici;
	}

	public List<RuloTestCekmeBoyuna> getRuloTestCekmeBoyuna() {
		return ruloTestCekmeBoyuna;
	}

	public void setRuloTestCekmeBoyuna(
			List<RuloTestCekmeBoyuna> ruloTestCekmeBoyuna) {
		this.ruloTestCekmeBoyuna = ruloTestCekmeBoyuna;
	}

	public List<RuloTestCekmeEnine> getRuloTestCekmeEnine() {
		return ruloTestCekmeEnine;
	}

	public void setRuloTestCekmeEnine(
			List<RuloTestCekmeEnine> ruloTestCekmeEnine) {
		this.ruloTestCekmeEnine = ruloTestCekmeEnine;
	}

	public List<RuloTestCentikDarbeBoyuna> getRuloTestCentikDarbeBoyuna() {
		return ruloTestCentikDarbeBoyuna;
	}

	public void setRuloTestCentikDarbeBoyuna(
			List<RuloTestCentikDarbeBoyuna> ruloTestCentikDarbeBoyuna) {
		this.ruloTestCentikDarbeBoyuna = ruloTestCentikDarbeBoyuna;
	}

	public List<RuloTestCentikDarbeEnine> getRuloTestCentikDarbeEnine() {
		return ruloTestCentikDarbeEnine;
	}

	public void setRuloTestCentikDarbeEnine(
			List<RuloTestCentikDarbeEnine> ruloTestCentikDarbeEnine) {
		this.ruloTestCentikDarbeEnine = ruloTestCentikDarbeEnine;
	}

	public List<RuloTestCentikDarbeUretici> getRuloTestCentikDarbeUretici() {
		return ruloTestCentikDarbeUretici;
	}

	public void setRuloTestCentikDarbeUretici(
			List<RuloTestCentikDarbeUretici> ruloTestCentikDarbeUretici) {
		this.ruloTestCentikDarbeUretici = ruloTestCentikDarbeUretici;
	}

	public List<RuloTestKimyasalGirdi> getRuloTestKimyasalGirdi() {
		return ruloTestKimyasalGirdi;
	}

	public void setRuloTestKimyasalGirdi(
			List<RuloTestKimyasalGirdi> ruloTestKimyasalGirdi) {
		this.ruloTestKimyasalGirdi = ruloTestKimyasalGirdi;
	}

	public List<RuloTestKimyasalUretici> getRuloTestKimyasalUretici() {
		return ruloTestKimyasalUretici;
	}

	public void setRuloTestKimyasalUretici(
			List<RuloTestKimyasalUretici> ruloTestKimyasalUretici) {
		this.ruloTestKimyasalUretici = ruloTestKimyasalUretici;
	}

	public int getIslemYapanId() {
		return islemYapanId;
	}

	public void setIslemYapanId(int islemYapanId) {
		this.islemYapanId = islemYapanId;
	}

	public String getIslemYapanIsim() {
		return islemYapanIsim;
	}

	public void setIslemYapanIsim(String islemYapanIsim) {
		this.islemYapanIsim = islemYapanIsim;
	}

	public String getRuloYilAndRuloKimlikId() {
		ruloYilAndRuloKimlikId = getRuloYil() + "/" + getRuloKimlikId();
		return ruloYilAndRuloKimlikId;
	}

	public void setRuloYilAndRuloKimlikId(String ruloYilAndRuloKimlikId) {
		this.ruloYilAndRuloKimlikId = ruloYilAndRuloKimlikId;
	}

	public String getRuloNot() {
		return ruloNot;
	}

	public void setRuloNot(String ruloNot) {
		this.ruloNot = ruloNot;
	}

	public Statuses getStatus() {
		return status;
	}

	public void setStatus(Statuses status) {
		this.status = status;
	}

	public Integer getRuloGenelDurum() {
		return ruloGenelDurum;
	}

	public void setRuloGenelDurum(Integer ruloGenelDurum) {
		this.ruloGenelDurum = ruloGenelDurum;
	}

	public void setStatuses(Statuses s) {
		status = s;
		ruloDurum = s.getId();
	}

	public Statuses getStatus(Config config) {
		if (status == null)
			status = config.getMaterials().findStatusById(ruloDurum);
		return status;
	}

	public String getRuloYil() {
		return ruloYil;
	}

	public void setRuloYil(String ruloYil) {
		this.ruloYil = ruloYil;
	}

	public List<MachineRuloLink> getMachineRuloLinks() {
		return machineRuloLinks;
	}

	public void setMachineRuloLinks(List<MachineRuloLink> machineRuloLinks) {
		this.machineRuloLinks = machineRuloLinks;
	}

	public Integer getSalesItemId() {
		return salesItemId;
	}

	public void setSalesItemId(Integer salesItemId) {
		this.salesItemId = salesItemId;
	}

	public SalesItem getSelectedSalesItemPipeRulo() {
		return selectedSalesItemPipeRulo;
	}

	public void setSelectedSalesItemPipeRulo(SalesItem selectedSalesItemPipeRulo) {
		this.selectedSalesItemPipeRulo = selectedSalesItemPipeRulo;
	}

	public SalesCustomer getSalesCustomer() {
		return salesCustomer;
	}

	public void setSalesCustomer(SalesCustomer salesCustomer) {
		this.salesCustomer = salesCustomer;
	}

	public Integer getSalesCustomerId() {
		return salesCustomerId;
	}

	public void setSalesCustomerId(Integer salesCustomerId) {
		this.salesCustomerId = salesCustomerId;
	}

	public SalesItem getReservedSalesItem() {
		return reservedSalesItem;
	}

	public void setReservedSalesItem(SalesItem reservedSalesItem) {
		this.reservedSalesItem = reservedSalesItem;
	}

	public Date getReservationDate() {
		return reservationDate;
	}

	public void setReservationDate(Date reservationDate) {
		this.reservationDate = reservationDate;
	}

	/**
	 * @return the ruloTestDwttUretici
	 */
	public List<RuloTestDwttUretici> getRuloTestDwttUretici() {
		return ruloTestDwttUretici;
	}

	/**
	 * @param ruloTestDwttUretici
	 *            the ruloTestDwttUretici to set
	 */
	public void setRuloTestDwttUretici(
			List<RuloTestDwttUretici> ruloTestDwttUretici) {
		this.ruloTestDwttUretici = ruloTestDwttUretici;
	}

	/**
	 * @return the ruloTestDwttGirdi
	 */
	public List<RuloTestDwttGirdi> getRuloTestDwttGirdi() {
		return ruloTestDwttGirdi;
	}

	/**
	 * @param ruloTestDwttGirdi
	 *            the ruloTestDwttGirdi to set
	 */
	public void setRuloTestDwttGirdi(List<RuloTestDwttGirdi> ruloTestDwttGirdi) {
		this.ruloTestDwttGirdi = ruloTestDwttGirdi;
	}

}
