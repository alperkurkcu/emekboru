package com.emekboru.jpa;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "event_type")
public class EventTypeEb implements Serializable{

	private static final long serialVersionUID = -3972081883988089777L;
	
	@Id
	@Column(name="event_type_id")
	@SequenceGenerator(name = "EVENT_TYPE_GENERATOR", sequenceName = "EVENT_TYPE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EVENT_TYPE_GENERATOR")
	private Integer eventTypeId;
	
	@Column(name="name")
	private String name;
	
	@Column(name="short_desc")
	private String shortDesc;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "eventType")
	private List<EventEb> events;
	
	public EventTypeEb(){
		
	}

	@Override
	public boolean equals(Object o) {
		if(o instanceof EventTypeEb){
			
			if(((EventTypeEb) o).getEventTypeId().equals(this.getEventTypeId()))
				return true;
		}
		return false;
	}

	/**
	 * 		GETTERS AND SETTERS
	 */
	public Integer getEventTypeId() {
		return eventTypeId;
	}

	public void setEventTypeId(Integer eventTypeId) {
		this.eventTypeId = eventTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<EventEb> getEvents() {
		return events;
	}

	public void setEvents(List<EventEb> events) {
		this.events = events;
	}
	
}
