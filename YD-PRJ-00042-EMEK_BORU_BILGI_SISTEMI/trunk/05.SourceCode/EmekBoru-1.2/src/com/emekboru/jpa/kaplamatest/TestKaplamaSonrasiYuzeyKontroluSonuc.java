package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.isolation.IsolationTestDefinition;

/**
 * The persistent class for the test_kaplama_sonrasi_yuzey_kontrolu_sonuc
 * database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaSonrasiYuzeyKontroluSonuc.findAll", query = "SELECT r FROM TestKaplamaSonrasiYuzeyKontroluSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_kaplama_sonrasi_yuzey_kontrolu_sonuc")
public class TestKaplamaSonrasiYuzeyKontroluSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KAPLAMA_SONRASI_YUZEY_KONTROLU_SONUC_ID_GENERATOR", sequenceName = "TEST_KAPLAMA_SONRASI_YUZEY_KONTROLU_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KAPLAMA_SONRASI_YUZEY_KONTROLU_SONUC_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ekleme_zamani")
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	// @Column(name="global_id")
	// private Integer globalId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name="pipe_id")
	// private Integer pipeId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi")
	private Date testTarihi;

	@Column(name = "vardiya")
	private Integer vardiya;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;

	@Column(name = "goz_kontrol")
	private Integer gozKontrol;

	// @Column(name = "isolation_test_id")
	// private Integer isolationTestId;

	@Column(name = "red_ince")
	private Boolean redInce;

	@Column(name = "rz_degeri")
	private String rzDegeri;

	@Column(name = "tamir_akma")
	private Boolean tamirAkma;

	@Column(name = "tamir_damlama")
	private Boolean tamirDamlama;

	@Column(name = "tamir_grit")
	private Boolean tamirGrit;

	@Column(name = "tamir_meme_izi")
	private Boolean tamirMemeIzi;

	@Column(name = "tamir_tarak_izi")
	private Boolean tamirTarakIzi;

	@Column(name = "sonuc")
	private Integer sonuc;

	public TestKaplamaSonrasiYuzeyKontroluSonuc() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Integer getGozKontrol() {
		return this.gozKontrol;
	}

	public void setGozKontrol(Integer gozKontrol) {
		this.gozKontrol = gozKontrol;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Boolean getRedInce() {
		return this.redInce;
	}

	public void setRedInce(Boolean redInce) {
		this.redInce = redInce;
	}

	public String getRzDegeri() {
		return this.rzDegeri;
	}

	public void setRzDegeri(String rzDegeri) {
		this.rzDegeri = rzDegeri;
	}

	public Boolean getTamirAkma() {
		return this.tamirAkma;
	}

	public void setTamirAkma(Boolean tamirAkma) {
		this.tamirAkma = tamirAkma;
	}

	public Boolean getTamirDamlama() {
		return this.tamirDamlama;
	}

	public void setTamirDamlama(Boolean tamirDamlama) {
		this.tamirDamlama = tamirDamlama;
	}

	public Boolean getTamirGrit() {
		return this.tamirGrit;
	}

	public void setTamirGrit(Boolean tamirGrit) {
		this.tamirGrit = tamirGrit;
	}

	public Boolean getTamirMemeIzi() {
		return this.tamirMemeIzi;
	}

	public void setTamirMemeIzi(Boolean tamirMemeIzi) {
		this.tamirMemeIzi = tamirMemeIzi;
	}

	public Boolean getTamirTarakIzi() {
		return this.tamirTarakIzi;
	}

	public void setTamirTarakIzi(Boolean tamirTarakIzi) {
		this.tamirTarakIzi = tamirTarakIzi;
	}

	public Integer getVardiya() {
		return this.vardiya;
	}

	public void setVardiya(Integer vardiya) {
		this.vardiya = vardiya;
	}

	/**
	 * @return the eklemeZamani
	 */
	public Date getEklemeZamani() {
		return eklemeZamani;
	}

	/**
	 * @param eklemeZamani
	 *            the eklemeZamani to set
	 */
	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	/**
	 * @return the guncellemeZamani
	 */
	public Date getGuncellemeZamani() {
		return guncellemeZamani;
	}

	/**
	 * @param guncellemeZamani
	 *            the guncellemeZamani to set
	 */
	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	/**
	 * @return the testTarihi
	 */
	public Date getTestTarihi() {
		return testTarihi;
	}

	/**
	 * @param testTarihi
	 *            the testTarihi to set
	 */
	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the bagliTestId
	 */
	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the bagliGlobalId
	 */
	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}

	/**
	 * @param bagliGlobalId
	 *            the bagliGlobalId to set
	 */
	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the sonuc
	 */
	public Integer getSonuc() {
		return sonuc;
	}

	/**
	 * @param sonuc
	 *            the sonuc to set
	 */
	public void setSonuc(Integer sonuc) {
		this.sonuc = sonuc;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

}