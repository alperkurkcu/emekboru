package com.emekboru.jpa.common;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.employee.EmployeeDayoff;

/**
 * The persistent class for the common_dayoff_type database table.
 * 
 */
@Entity
@Table(name = "common_dayoff_type")
@NamedQuery(name = "CommonDayoffType.findAll", query = "SELECT c FROM CommonDayoffType c")
public class CommonDayoffType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "COMMON_DAYOFF_TYPE_ID_GENERATOR", sequenceName = "EMPLOYEE_DAYOFF_TYPE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMMON_DAYOFF_TYPE_ID_GENERATOR")
	@Column(name = "id")
	private Integer id;

	@Column(name = "description")
	private String description;

	@Column(name = "ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "name")
	private String name;

	// bi-directional many-to-one association to EmployeeDayoff
	@OneToMany(mappedBy = "commonDayoffType")
	private List<EmployeeDayoff> employeeDayoffs;

	public CommonDayoffType() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<EmployeeDayoff> getEmployeeDayoffs() {
		return this.employeeDayoffs;
	}

	public void setEmployeeDayoffs(List<EmployeeDayoff> employeeDayoffs) {
		this.employeeDayoffs = employeeDayoffs;
	}

	public EmployeeDayoff addEmployeeDayoff(EmployeeDayoff employeeDayoff) {
		getEmployeeDayoffs().add(employeeDayoff);
		employeeDayoff.setCommonDayoffType(this);

		return employeeDayoff;
	}

	public EmployeeDayoff removeEmployeeDayoff(EmployeeDayoff employeeDayoff) {
		getEmployeeDayoffs().remove(employeeDayoff);
		employeeDayoff.setCommonDayoffType(null);

		return employeeDayoff;
	}

}