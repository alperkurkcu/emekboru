package com.emekboru.jpa.tahribatsiztestsonuc;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.kalibrasyon.KalibrasyonFloroskopik;

/**
 * The persistent class for the test_tahribatsiz_floroskopik_sonuc database
 * table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "TestTahribatsizFloroskopikSonuc.findAll", query = "SELECT r FROM TestTahribatsizFloroskopikSonuc r WHERE r.pipe.pipeId=:prmPipeId order by r.koordinatQ"),
		@NamedQuery(name = "TestTahribatsizFloroskopikSonuc.findByKoordinats", query = "SELECT r FROM TestTahribatsizFloroskopikSonuc r WHERE r.koordinatQ=:prmQ and r.koordinatL=:prmL"),
		@NamedQuery(name = "TestTahribatsizFloroskopikSonuc.findByKoordinatsPipeId", query = "SELECT r FROM TestTahribatsizFloroskopikSonuc r WHERE r.koordinatQ=:prmQ and r.koordinatL=:prmL and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_tahribatsiz_floroskopik_sonuc")
public class TestTahribatsizFloroskopikSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_TAHRIBATSIZ_FLOROSKOPIK_SONUC_ID_GENERATOR", sequenceName = "TEST_TAHRIBATSIZ_FLOROSKOPIK_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_TAHRIBATSIZ_FLOROSKOPIK_SONUC_ID_GENERATOR")
	@Column(name = "ID", unique = true, nullable = false)
	private Integer id;

	private Boolean aa;

	private Boolean ab;

	private Boolean ad;

	private Boolean ba;

	private Boolean bb;

	private Boolean c;

	private Boolean d;

	private Boolean de;

	private Boolean e;

	private Boolean f;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@Column(name = "koordinat_q")
	private Integer koordinatQ;

	@Column(name = "koordinat_l")
	private Integer koordinatL;

	private Integer result;

	@Column(name = "tamir_kesim")
	private Integer tamirKesim;

	@Column(name = "test_id", nullable = false)
	private Integer testId;

	@Column(name = "aciklama", length = 255)
	private String aciklama;

	@Column(name = "belirti_aciklama", length = 255)
	private String BelirtiAciklama;

	@Column(name = "kaynak_islemi", nullable = false, length = 20)
	private String kaynakIslemi;

	@Column(name = "kaynak_kacikligi", nullable = false)
	private Boolean kaynakKacikligi;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser user;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	// sonuc ekranlarına bilgiler getirebilmek için

	@OneToOne(mappedBy = "testTahribatsizFloroskopikSonuc")
	private TestTahribatsizTamir testTahribatsizTamir;

	@OneToOne(mappedBy = "testTahribatsizFloroskopikSonuc")
	private TestTahribatsizTorna testTahribatsizTorna;

	@OneToOne(mappedBy = "testTahribatsizFloroskopikSonuc")
	private TestTahribatsizHidrostatik testTahribatsizHidrostatik;

	@OneToOne(mappedBy = "testTahribatsizFloroskopikSonuc")
	private TestTahribatsizGozOlcuSonuc testTahribatsizGozOlcuSonuc;

	@Column(name = "tamir_kabul_zamani")
	private Timestamp tamirKabulZamani;

	@Column(name = "tamir_kabul_kullanici")
	private Integer tamirKabulKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "tamir_kabul_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser tamirKabulEmployee;

	@Column(name = "boru_ucu")
	private Integer boruUcu;

	// @Column(name = "kalibrasyon_id")
	// private Integer kalibrasyonId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "kalibrasyon_id", referencedColumnName = "ID", insertable = false)
	private KalibrasyonFloroskopik kalibrasyon;

	public TestTahribatsizFloroskopikSonuc() {

	}

	// setters getters

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getAa() {
		return this.aa;
	}

	public void setAa(Boolean aa) {
		this.aa = aa;
	}

	public Boolean getAb() {
		return this.ab;
	}

	public void setAb(Boolean ab) {
		this.ab = ab;
	}

	public Boolean getAd() {
		return this.ad;
	}

	public void setAd(Boolean ad) {
		this.ad = ad;
	}

	public Boolean getBa() {
		return this.ba;
	}

	public void setBa(Boolean ba) {
		this.ba = ba;
	}

	public Boolean getBb() {
		return this.bb;
	}

	public void setBb(Boolean bb) {
		this.bb = bb;
	}

	public Boolean getDe() {
		return this.de;
	}

	public void setDe(Boolean de) {
		this.de = de;
	}

	public Boolean getF() {
		return this.f;
	}

	public void setF(Boolean f) {
		this.f = f;
	}

	public Integer getTestId() {
		return this.testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public Integer getKoordinatQ() {
		return koordinatQ;
	}

	public void setKoordinatQ(Integer koordinatQ) {
		this.koordinatQ = koordinatQ;
	}

	public Integer getKoordinatL() {
		return koordinatL;
	}

	public void setKoordinatL(Integer koordinatL) {
		this.koordinatL = koordinatL;
	}

	public String getKaynakIslemi() {
		return kaynakIslemi;
	}

	public void setKaynakIslemi(String kaynakIslemi) {
		this.kaynakIslemi = kaynakIslemi;
	}

	public Boolean getC() {
		return c;
	}

	public void setC(Boolean c) {
		this.c = c;
	}

	public Boolean getD() {
		return d;
	}

	public void setD(Boolean d) {
		this.d = d;
	}

	public Boolean getE() {
		return e;
	}

	public void setE(Boolean e) {
		this.e = e;
	}

	public String getBelirtiAciklama() {
		return BelirtiAciklama;
	}

	public void setBelirtiAciklama(String belirtiAciklama) {
		BelirtiAciklama = belirtiAciklama;
	}

	public Timestamp getEklemeZamani() {
		return eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Timestamp getGuncellemeZamani() {
		return guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public SystemUser getUser() {
		return user;
	}

	public void setUser(SystemUser user) {
		this.user = user;
	}

	public Boolean getKaynakKacikligi() {
		return kaynakKacikligi;
	}

	public void setKaynakKacikligi(Boolean kaynakKacikligi) {
		this.kaynakKacikligi = kaynakKacikligi;
	}

	public TestTahribatsizTamir getTestTahribatsizTamir() {
		return testTahribatsizTamir;
	}

	public void setTestTahribatsizTamir(
			TestTahribatsizTamir testTahribatsizTamir) {
		this.testTahribatsizTamir = testTahribatsizTamir;
	}

	public TestTahribatsizTorna getTestTahribatsizTorna() {
		return testTahribatsizTorna;
	}

	public void setTestTahribatsizTorna(
			TestTahribatsizTorna testTahribatsizTorna) {
		this.testTahribatsizTorna = testTahribatsizTorna;
	}

	public TestTahribatsizHidrostatik getTestTahribatsizHidrostatik() {
		return testTahribatsizHidrostatik;
	}

	public void setTestTahribatsizHidrostatik(
			TestTahribatsizHidrostatik testTahribatsizHidrostatik) {
		this.testTahribatsizHidrostatik = testTahribatsizHidrostatik;
	}

	public TestTahribatsizGozOlcuSonuc getTestTahribatsizGozOlcuSonuc() {
		return testTahribatsizGozOlcuSonuc;
	}

	public void setTestTahribatsizGozOlcuSonuc(
			TestTahribatsizGozOlcuSonuc testTahribatsizGozOlcuSonuc) {
		this.testTahribatsizGozOlcuSonuc = testTahribatsizGozOlcuSonuc;
	}

	public Integer getResult() {
		return result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}

	public Integer getTamirKesim() {
		return tamirKesim;
	}

	public void setTamirKesim(Integer tamirKesim) {
		this.tamirKesim = tamirKesim;
	}

	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	public Timestamp getTamirKabulZamani() {
		return tamirKabulZamani;
	}

	public void setTamirKabulZamani(Timestamp tamirKabulZamani) {
		this.tamirKabulZamani = tamirKabulZamani;
	}

	public Integer getTamirKabulKullanici() {
		return tamirKabulKullanici;
	}

	public void setTamirKabulKullanici(Integer tamirKabulKullanici) {
		this.tamirKabulKullanici = tamirKabulKullanici;
	}

	public SystemUser getTamirKabulEmployee() {
		return tamirKabulEmployee;
	}

	public void setTamirKabulEmployee(SystemUser tamirKabulEmployee) {
		this.tamirKabulEmployee = tamirKabulEmployee;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the boruUcu
	 */
	public Integer getBoruUcu() {
		return boruUcu;
	}

	/**
	 * @param boruUcu
	 *            the boruUcu to set
	 */
	public void setBoruUcu(Integer boruUcu) {
		this.boruUcu = boruUcu;
	}

	/**
	 * @return the kalibrasyon
	 */
	public KalibrasyonFloroskopik getKalibrasyon() {
		return kalibrasyon;
	}

	/**
	 * @param kalibrasyon
	 *            the kalibrasyon to set
	 */
	public void setKalibrasyon(KalibrasyonFloroskopik kalibrasyon) {
		this.kalibrasyon = kalibrasyon;
	}

}