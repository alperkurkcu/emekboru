package com.emekboru.jpa.sales.order;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sales_pipe_type")
public class SalesPipeType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2594658776137891590L;

	@Id
	@Column(name = "pipe_type_id")
	private int pipeTypeId;

	@Column(name = "title")
	private String name;

	public SalesPipeType() {
	}

	@Override
	public boolean equals(Object o) {
		return this.getPipeTypeId() == ((SalesPipeType) o).getPipeTypeId();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public int getPipeTypeId() {
		return pipeTypeId;
	}

	public void setPipeTypeId(int pipeTypeId) {
		this.pipeTypeId = pipeTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
