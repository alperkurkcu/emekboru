package com.emekboru.jpa.common;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.employee.EmployeePayment;

/**
 * The persistent class for the common_payment_type database table.
 * 
 */
@Entity
@Table(name = "common_payment_type")
@NamedQuery(name = "CommonPaymentType.findAll", query = "SELECT c FROM CommonPaymentType c")
public class CommonPaymentType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "COMMON_PAYMENT_TYPE_ID_GENERATOR", sequenceName = "EMPLOYEE_PAYMENT_TYPE_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMMON_PAYMENT_TYPE_ID_GENERATOR")
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "description")
	private String description;

	@Column(name = "ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "name")
	private String name;

	// bi-directional many-to-one association to EmployeePayment
	@OneToMany(mappedBy = "commonPaymentType")
	private List<EmployeePayment> employeePayments;

	public CommonPaymentType() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<EmployeePayment> getEmployeePayments() {
		return this.employeePayments;
	}

	public void setEmployeePayments(List<EmployeePayment> employeePayments) {
		this.employeePayments = employeePayments;
	}

	public EmployeePayment addEmployeePayment(EmployeePayment employeePayment) {
		getEmployeePayments().add(employeePayment);
		employeePayment.setCommonPaymentType(this);

		return employeePayment;
	}

	public EmployeePayment removeEmployeePayment(EmployeePayment employeePayment) {
		getEmployeePayments().remove(employeePayment);
		employeePayment.setCommonPaymentType(null);

		return employeePayment;
	}

}