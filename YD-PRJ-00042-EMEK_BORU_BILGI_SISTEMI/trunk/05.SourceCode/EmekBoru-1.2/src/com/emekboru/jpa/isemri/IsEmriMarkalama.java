package com.emekboru.jpa.isemri;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the is_emri_markalama database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "IsEmriMarkalama.findAllByItemId", query = "SELECT r FROM IsEmriMarkalama r WHERE r.salesItem.itemId=:prmItemId order by r.eklemeZamani") })
@Table(name = "is_emri_markalama")
public class IsEmriMarkalama implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IS_EMRI_MARKALAMA_ID_GENERATOR", sequenceName = "IS_EMRI_MARKALAMA_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IS_EMRI_MARKALAMA_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "aciklama")
	private String aciklama;

	@Column(name = "documents")
	private String documents;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	@Basic
	@Column(name = "sales_item_id", insertable = false, updatable = false)
	private Integer salesItemId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	private SalesItem salesItem;

	public IsEmriMarkalama() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public String getDocuments() {
		return this.documents;
	}

	public void setDocuments(String documents) {
		this.documents = documents;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the salesItem
	 */
	public SalesItem getSalesItem() {
		return salesItem;
	}

	/**
	 * @param salesItem
	 *            the salesItem to set
	 */
	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}