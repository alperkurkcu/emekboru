package com.emekboru.jpa.satinalma;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the satinalma_teklif_isteme database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "SatinalmaTeklifIsteme.findAll", query = "SELECT r FROM SatinalmaTeklifIsteme r order by r.id"),
		@NamedQuery(name = "SatinalmaTeklifIsteme.findAllTedarikci", query = "SELECT DISTINCT(r.satinalmaMusteri.id) FROM SatinalmaTeklifIsteme r where r.satinalmaTakipDosyalar.id=:prmDosyaId"),
		@NamedQuery(name = "SatinalmaTeklifIsteme.findByDosyaId", query = "SELECT r FROM SatinalmaTeklifIsteme r where r.satinalmaTakipDosyalar.id=:prmDosyaId order by r.id") })
@Table(name = "satinalma_teklif_isteme")
public class SatinalmaTeklifIsteme implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SATINALMA_TEKLIF_ISTEME_ID_GENERATOR", sequenceName = "SATINALMA_TEKLIF_ISTEME_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SATINALMA_TEKLIF_ISTEME_ID_GENERATOR")
	@Column(name = "ID")
	private Integer id;

	@Column(name = "ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser user;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "teklif_tarihi")
	private Timestamp teklifTarihi;

	@Column(name = "gonderme_tarihi")
	private Timestamp gondermeTarihi;

	@Column(name = "teklif_sayi")
	private Integer teklifSayi;

	@Column(name = "nakliye")
	private String nakliye;

	@Column(name = "nakliye_masraflari")
	private Integer nakliyeMasraflari;

	@Column(name = "belgeler")
	private String belgeler;

	@Column(name = "odeme")
	private Integer odeme;

	@Column(name = "hesap_no")
	private String hesapNo;

	@Column(name = "damga_vergisi")
	private Integer damgaVergisi;

	@Column(name = "kur")
	private Integer kur;

	@Column(name = "kesin_teminat")
	private Integer kesinTeminat;

	@Column(name = "cezai_sartlar")
	private String cezaiSartlar;

	// @Column(name = "durum_id")
	// private Integer durumId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "durum_id", referencedColumnName = "ID", insertable = false)
	private SatinalmaDurum satinalmaDurum;

	// @OneToOne(fetch = FetchType.EAGER)
	// @JoinColumn(name = "ID", referencedColumnName = "ID", insertable = false,
	// updatable = false)
	// private SatinalmaTeklifIstemeIcerik satinalmaTeklifIstemeIcerik;

	// @Column(name = "malzeme_id")
	// private Integer malzemeId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "malzeme_id", referencedColumnName = "ID", insertable = false)
	private SatinalmaMalzemeHizmetTalepFormu satinalmaMalzemeHizmetTalepFormu;

	// @Column(name = "dosya_id")
	// private Integer dosyaId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "dosya_id", referencedColumnName = "ID", insertable = false)
	private SatinalmaTakipDosyalar satinalmaTakipDosyalar;

	// @Column(name = "musteri_id")
	// private Integer musteriId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "musteri_id", referencedColumnName = "ID", insertable = false)
	private SatinalmaMusteri satinalmaMusteri;

	public SatinalmaTeklifIsteme() {

		// satinalmaDurum = new SatinalmaDurum();
		// satinalmaMusteri = new SatinalmaMusteri();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Timestamp getTeklifTarihi() {
		return this.teklifTarihi;
	}

	public void setTeklifTarihi(Timestamp teklifTarihi) {
		this.teklifTarihi = teklifTarihi;
	}

	public SystemUser getUser() {
		return user;
	}

	public void setUser(SystemUser user) {
		this.user = user;
	}

	public Integer getTeklifSayi() {
		return teklifSayi;
	}

	public void setTeklifSayi(Integer teklifSayi) {
		this.teklifSayi = teklifSayi;
	}

	public String getNakliye() {
		return nakliye;
	}

	public void setNakliye(String nakliye) {
		this.nakliye = nakliye;
	}

	public Integer getNakliyeMasraflari() {
		return nakliyeMasraflari;
	}

	public void setNakliyeMasraflari(Integer nakliyeMasraflari) {
		this.nakliyeMasraflari = nakliyeMasraflari;
	}

	public String getBelgeler() {
		return belgeler;
	}

	public void setBelgeler(String belgeler) {
		this.belgeler = belgeler;
	}

	public Integer getOdeme() {
		return odeme;
	}

	public void setOdeme(Integer odeme) {
		this.odeme = odeme;
	}

	public String getHesapNo() {
		return hesapNo;
	}

	public void setHesapNo(String hesapNo) {
		this.hesapNo = hesapNo;
	}

	public Integer getDamgaVergisi() {
		return damgaVergisi;
	}

	public void setDamgaVergisi(Integer damgaVergisi) {
		this.damgaVergisi = damgaVergisi;
	}

	public Integer getKur() {
		return kur;
	}

	public void setKur(Integer kur) {
		this.kur = kur;
	}

	public String getCezaiSartlar() {
		return cezaiSartlar;
	}

	public void setCezaiSartlar(String cezaiSartlar) {
		this.cezaiSartlar = cezaiSartlar;
	}

	public Integer getKesinTeminat() {
		return kesinTeminat;
	}

	public void setKesinTeminat(Integer kesinTeminat) {
		this.kesinTeminat = kesinTeminat;
	}

	public SatinalmaDurum getSatinalmaDurum() {
		return satinalmaDurum;
	}

	public void setSatinalmaDurum(SatinalmaDurum satinalmaDurum) {
		this.satinalmaDurum = satinalmaDurum;
	}

	public SatinalmaMalzemeHizmetTalepFormu getSatinalmaMalzemeHizmetTalepFormu() {
		return satinalmaMalzemeHizmetTalepFormu;
	}

	public void setSatinalmaMalzemeHizmetTalepFormu(
			SatinalmaMalzemeHizmetTalepFormu satinalmaMalzemeHizmetTalepFormu) {
		this.satinalmaMalzemeHizmetTalepFormu = satinalmaMalzemeHizmetTalepFormu;
	}

	public Timestamp getGondermeTarihi() {
		return gondermeTarihi;
	}

	public void setGondermeTarihi(Timestamp gondermeTarihi) {
		this.gondermeTarihi = gondermeTarihi;
	}

	public SatinalmaMusteri getSatinalmaMusteri() {
		return satinalmaMusteri;
	}

	public void setSatinalmaMusteri(SatinalmaMusteri satinalmaMusteri) {
		this.satinalmaMusteri = satinalmaMusteri;
	}

	/**
	 * @return the satinalmaTakipDosyalar
	 */
	public SatinalmaTakipDosyalar getSatinalmaTakipDosyalar() {
		return satinalmaTakipDosyalar;
	}

	/**
	 * @param satinalmaTakipDosyalar
	 *            the satinalmaTakipDosyalar to set
	 */
	public void setSatinalmaTakipDosyalar(
			SatinalmaTakipDosyalar satinalmaTakipDosyalar) {
		this.satinalmaTakipDosyalar = satinalmaTakipDosyalar;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}