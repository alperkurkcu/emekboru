package com.emekboru.jpa;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the lk_yetki_rol database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "LkYetkiRol.findAll", query = "select o from LkYetkiRol o"),
		@NamedQuery(name = "LkYetkiRol.findByAdAciklamaAndMenuId", query = "select o from LkYetkiRol o where o.ad=:prmAd and o.aciklama=:prmAciklama"),
		@NamedQuery(name = "LkYetkiRol.findByMenuId", query = "select o from LkYetkiRol o where o.menu.id=:prmMenuId"),
		@NamedQuery(name = "LkYetkiRol.findBySubMenu", query = "select o from LkYetkiRol o where o.menu.isSubMenu=:prmIsSubMenu")
})
@Table(name = "lk_yetki_rol")
public class LkYetkiRol implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@SequenceGenerator(name = "LK_YETKI_ROL_GENERATOR", sequenceName = "LK_YETKI_ROL_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LK_YETKI_ROL_GENERATOR")
	private Integer id;
	@Column(name = "ACIKLAMA")
	private String aciklama;
	@Column(name = "AD")
	private String ad;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_yetki_menu", referencedColumnName = "ID")
	private YetkiMenu menu;

	@Column(name = "ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	public LkYetkiRol() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public String getAd() {
		return this.ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public YetkiMenu getMenu() {
		return menu;
	}

	public void setMenu(YetkiMenu menu) {
		this.menu = menu;
	}

}