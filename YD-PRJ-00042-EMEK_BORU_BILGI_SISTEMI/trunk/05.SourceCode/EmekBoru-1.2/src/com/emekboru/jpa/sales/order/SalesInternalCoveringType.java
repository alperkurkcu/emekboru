package com.emekboru.jpa.sales.order;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sales_internal_covering_type")
public class SalesInternalCoveringType implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 4063358279846598229L;

	@Id
	@Column(name = "type_id")
	private int typeId;

	@Column(name = "title")
	private String name;

	public SalesInternalCoveringType() {
	}

	@Override
	public boolean equals(Object o) {
		return this.getTypeId() == ((SalesInternalCoveringType) o).getTypeId();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
