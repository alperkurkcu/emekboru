package com.emekboru.jpa.satinalma;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.stok.StokTanimlarBirim;

/**
 * The persistent class for the satinalma_teklif_isteme_icerik database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "SatinalmaTeklifIstemeIcerik.findAll", query = "SELECT r FROM SatinalmaTeklifIstemeIcerik r where r.satinalmaTeklifIsteme.id=:prmFormId order by r.id") })
@Table(name = "satinalma_teklif_isteme_icerik")
public class SatinalmaTeklifIstemeIcerik implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SATINALMA_TEKLIF_ISTEME_ICERIK_ID_GENERATOR", sequenceName = "SATINALMA_TEKLIF_ISTEME_ICERIK_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SATINALMA_TEKLIF_ISTEME_ICERIK_ID_GENERATOR")
	@Column(name = "ID")
	private Integer id;

	@Column(name = "birim", insertable = false, updatable = false)
	private String birim;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "birim", referencedColumnName = "ID", insertable = false)
	private StokTanimlarBirim stokTanimlarBirim;

	@Column(name = "birim_fiyat")
	private double birimFiyat;

	@Column(name = "ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser user;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "malzeme_tanim")
	private String malzemeTanim;

	@Column(name = "miktar")
	private String miktar;

	@Column(name = "teklif_id", insertable = false, updatable = false)
	private Integer teklifId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "teklif_id", referencedColumnName = "ID", insertable = false)
	private SatinalmaTeklifIsteme satinalmaTeklifIsteme;

	@Column(name = "teslim_tarihi", nullable = false)
	private String teslimTarihi;

	@Column(name = "sira_no")
	private Integer siraNo;

	@Column(name = "tutar")
	private double tutar;

	@Column(name = "kdv")
	private Integer kdv = 18;

	@Transient
	private double toplam;

	@Transient
	private double toplamTutar;

	@Column(name = "takip_dosya_icerik_id", insertable = false, updatable = false)
	private String takipDosyaIcerikId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "takip_dosya_icerik_id", referencedColumnName = "ID", insertable = false)
	private SatinalmaTakipDosyalarIcerik satinalmaTakipDosyalarIcerik;

	public SatinalmaTeklifIstemeIcerik() {

		stokTanimlarBirim = new StokTanimlarBirim();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the birimFiyat
	 */
	public double getBirimFiyat() {
		return birimFiyat;
	}

	/**
	 * @param birimFiyat
	 *            the birimFiyat to set
	 */
	public void setBirimFiyat(double birimFiyat) {
		this.birimFiyat = birimFiyat;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getMalzemeTanim() {
		return this.malzemeTanim;
	}

	public void setMalzemeTanim(String malzemeTanim) {
		this.malzemeTanim = malzemeTanim;
	}

	public String getMiktar() {
		return this.miktar;
	}

	public void setMiktar(String miktar) {
		this.miktar = miktar;
	}

	public SystemUser getUser() {
		return user;
	}

	public void setUser(SystemUser user) {
		this.user = user;
	}

	public SatinalmaTeklifIsteme getSatinalmaTeklifIsteme() {
		return satinalmaTeklifIsteme;
	}

	public void setSatinalmaTeklifIsteme(
			SatinalmaTeklifIsteme satinalmaTeklifIsteme) {
		this.satinalmaTeklifIsteme = satinalmaTeklifIsteme;
	}

	public Integer getSiraNo() {
		return siraNo;
	}

	public void setSiraNo(Integer siraNo) {
		this.siraNo = siraNo;
	}

	/**
	 * @return the tutar
	 */
	public double getTutar() {
		return tutar;
	}

	/**
	 * @param tutar
	 *            the tutar to set
	 */
	public void setTutar(double tutar) {
		this.tutar = tutar;
	}

	public Integer getKdv() {
		return kdv;
	}

	public void setKdv(Integer kdv) {
		this.kdv = kdv;
	}

	/**
	 * @return the toplam
	 */
	public double getToplam() {
		return toplam;
	}

	/**
	 * @param toplam
	 *            the toplam to set
	 */
	public void setToplam(double toplam) {
		this.toplam = toplam;
	}

	/**
	 * @return the toplamTutar
	 */
	public double getToplamTutar() {
		return toplamTutar;
	}

	/**
	 * @param toplamTutar
	 *            the toplamTutar to set
	 */
	public void setToplamTutar(double toplamTutar) {
		this.toplamTutar = toplamTutar;
	}

	public StokTanimlarBirim getStokTanimlarBirim() {
		return stokTanimlarBirim;
	}

	public void setStokTanimlarBirim(StokTanimlarBirim stokTanimlarBirim) {
		this.stokTanimlarBirim = stokTanimlarBirim;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the satinalmaTakipDosyalarIcerik
	 */
	public SatinalmaTakipDosyalarIcerik getSatinalmaTakipDosyalarIcerik() {
		return satinalmaTakipDosyalarIcerik;
	}

	/**
	 * @param satinalmaTakipDosyalarIcerik
	 *            the satinalmaTakipDosyalarIcerik to set
	 */
	public void setSatinalmaTakipDosyalarIcerik(
			SatinalmaTakipDosyalarIcerik satinalmaTakipDosyalarIcerik) {
		this.satinalmaTakipDosyalarIcerik = satinalmaTakipDosyalarIcerik;
	}

	/**
	 * @return the teslimTarihi
	 */
	public String getTeslimTarihi() {
		return teslimTarihi;
	}

	/**
	 * @param teslimTarihi
	 *            the teslimTarihi to set
	 */
	public void setTeslimTarihi(String teslimTarihi) {
		this.teslimTarihi = teslimTarihi;
	}

}