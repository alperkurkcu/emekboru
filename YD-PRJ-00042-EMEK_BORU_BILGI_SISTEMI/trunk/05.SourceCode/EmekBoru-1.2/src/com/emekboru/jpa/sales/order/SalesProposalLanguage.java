package com.emekboru.jpa.sales.order;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "sales_proposal_language")
public class SalesProposalLanguage implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 6929094701804388842L;

	@Id
	@Column(name = "language_id")
	private int languageId;

	@Column(name = "language_name")
	private String languageName;

	public SalesProposalLanguage() {

	}

	@Override
	public boolean equals(Object o) {
		return this.getLanguageId() == ((SalesProposalLanguage) o)
				.getLanguageId();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public int getLanguageId() {
		return languageId;
	}

	public void setLanguageId(int languageId) {
		this.languageId = languageId;
	}

	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
