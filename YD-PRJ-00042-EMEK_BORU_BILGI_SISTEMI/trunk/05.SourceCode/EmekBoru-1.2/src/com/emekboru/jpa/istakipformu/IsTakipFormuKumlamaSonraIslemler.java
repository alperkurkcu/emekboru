package com.emekboru.jpa.istakipformu;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the is_takip_formu_kumlama_sonra_islemler database
 * table.
 * 
 */
@Entity
@Table(name = "is_takip_formu_kumlama_sonra_islemler")
public class IsTakipFormuKumlamaSonraIslemler implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IS_TAKIP_FORMU_KUMLAMA_SONRA_ISLEMLER_ID_GENERATOR", sequenceName = "IS_TAKIP_FORMU_KUMLAMA_SONRA_ISLEMLER_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IS_TAKIP_FORMU_KUMLAMA_SONRA_ISLEMLER_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "ayar_sonrasi_ilk_boru_check")
	private Boolean ayarSonrasiIlkBoruCheck;

	@Column(name = "ayar_sonrasi_ilk_boru_id")
	private Integer ayarSonrasiIlkBoruId;

	@Temporal(TemporalType.DATE)
	@Column(name = "ayar_sonrasi_ilk_boru_tarihi")
	private Date ayarSonrasiIlkBoruTarihi;

	@Column(name = "yuzey_check")
	private Boolean yuzeyCheck;

	@Temporal(TemporalType.DATE)
	@Column(name = "yuzey_tarihi")
	private Date yuzeyTarihi;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name = "is_takip_form_id")
	// private Integer isTakipFormId;

	@OneToOne(mappedBy = "isTakipFormuKumlamaSonraIslemler", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	private IsTakipFormuKumlama isTakipFormuKumlama;

	@Column(name = "kumlama_hizi")
	private Integer kumlamaHizi;

	@Column(name = "kumlama_hizi_check")
	private Boolean kumlamaHiziCheck;

	@Temporal(TemporalType.DATE)
	@Column(name = "kumlama_hizi_tarihi")
	private Date kumlamaHiziTarihi;

	@Column(name = "toz_testi")
	private Integer tozTesti;

	@Column(name = "toz_testi_check")
	private Boolean tozTestiCheck;

	@Temporal(TemporalType.DATE)
	@Column(name = "toz_testi_tarihi")
	private Date tozTestiTarihi;

	@Column(name = "yuzey_kalitesi")
	private Integer yuzeyKalitesi;

	@Column(name = "yuzey_kalitesi_check")
	private Boolean yuzeyKalitesiCheck;

	@Temporal(TemporalType.DATE)
	@Column(name = "yuzey_kalitesi_tarihi")
	private Date yuzeyKalitesiTarihi;

	@Column(name = "yuzey_puruzlulugu")
	private Integer yuzeyPuruzlulugu;

	@Column(name = "yuzey_puruzlulugu_check")
	private Boolean yuzeyPuruzluluguCheck;

	@Temporal(TemporalType.DATE)
	@Column(name = "yuzey_puruzlulugu_tarihi")
	private Date yuzeyPuruzluluguTarihi;

	@Column(name = "yuzey_aciklama", length = 255)
	private String yuzeyAciklama;

	@Column(name = "ayar_sonrasi_ilk_boru_aciklama", length = 255)
	private String ayarSonrasiIlkBoruAciklama;

	@Column(name = "toz_testi_aciklama", length = 255)
	private String tozTestiAciklama;

	@Column(name = "yuzey_puruzlulugu_aciklama", length = 255)
	private String yuzeyPuruzluluguAciklama;

	@Column(name = "yuzey_kalitesi_aciklama", length = 255)
	private String yuzeyKalitesiAciklama;

	@Column(name = "kumlama_hizi_aciklama", length = 255)
	private String kumlamaHiziAciklama;

	public IsTakipFormuKumlamaSonraIslemler() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getAyarSonrasiIlkBoruCheck() {
		return this.ayarSonrasiIlkBoruCheck;
	}

	public void setAyarSonrasiIlkBoruCheck(Boolean ayarSonrasiIlkBoruCheck) {
		this.ayarSonrasiIlkBoruCheck = ayarSonrasiIlkBoruCheck;
	}

	public Integer getAyarSonrasiIlkBoruId() {
		return this.ayarSonrasiIlkBoruId;
	}

	public void setAyarSonrasiIlkBoruId(Integer ayarSonrasiIlkBoruId) {
		this.ayarSonrasiIlkBoruId = ayarSonrasiIlkBoruId;
	}

	public Date getAyarSonrasiIlkBoruTarihi() {
		return this.ayarSonrasiIlkBoruTarihi;
	}

	public void setAyarSonrasiIlkBoruTarihi(Date ayarSonrasiIlkBoruTarihi) {
		this.ayarSonrasiIlkBoruTarihi = ayarSonrasiIlkBoruTarihi;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getKumlamaHizi() {
		return this.kumlamaHizi;
	}

	public void setKumlamaHizi(Integer kumlamaHizi) {
		this.kumlamaHizi = kumlamaHizi;
	}

	public Boolean getKumlamaHiziCheck() {
		return this.kumlamaHiziCheck;
	}

	public void setKumlamaHiziCheck(Boolean kumlamaHiziCheck) {
		this.kumlamaHiziCheck = kumlamaHiziCheck;
	}

	public Date getKumlamaHiziTarihi() {
		return this.kumlamaHiziTarihi;
	}

	public void setKumlamaHiziTarihi(Date kumlamaHiziTarihi) {
		this.kumlamaHiziTarihi = kumlamaHiziTarihi;
	}

	public Integer getTozTesti() {
		return this.tozTesti;
	}

	public void setTozTesti(Integer tozTesti) {
		this.tozTesti = tozTesti;
	}

	public Boolean getTozTestiCheck() {
		return this.tozTestiCheck;
	}

	public void setTozTestiCheck(Boolean tozTestiCheck) {
		this.tozTestiCheck = tozTestiCheck;
	}

	public Date getTozTestiTarihi() {
		return this.tozTestiTarihi;
	}

	public void setTozTestiTarihi(Date tozTestiTarihi) {
		this.tozTestiTarihi = tozTestiTarihi;
	}

	public Integer getYuzeyKalitesi() {
		return this.yuzeyKalitesi;
	}

	public void setYuzeyKalitesi(Integer yuzeyKalitesi) {
		this.yuzeyKalitesi = yuzeyKalitesi;
	}

	public Boolean getYuzeyKalitesiCheck() {
		return this.yuzeyKalitesiCheck;
	}

	public void setYuzeyKalitesiCheck(Boolean yuzeyKalitesiCheck) {
		this.yuzeyKalitesiCheck = yuzeyKalitesiCheck;
	}

	public Date getYuzeyKalitesiTarihi() {
		return this.yuzeyKalitesiTarihi;
	}

	public void setYuzeyKalitesiTarihi(Date yuzeyKalitesiTarihi) {
		this.yuzeyKalitesiTarihi = yuzeyKalitesiTarihi;
	}

	public Integer getYuzeyPuruzlulugu() {
		return this.yuzeyPuruzlulugu;
	}

	public void setYuzeyPuruzlulugu(Integer yuzeyPuruzlulugu) {
		this.yuzeyPuruzlulugu = yuzeyPuruzlulugu;
	}

	public Boolean getYuzeyPuruzluluguCheck() {
		return this.yuzeyPuruzluluguCheck;
	}

	public void setYuzeyPuruzluluguCheck(Boolean yuzeyPuruzluluguCheck) {
		this.yuzeyPuruzluluguCheck = yuzeyPuruzluluguCheck;
	}

	public Date getYuzeyPuruzluluguTarihi() {
		return this.yuzeyPuruzluluguTarihi;
	}

	public void setYuzeyPuruzluluguTarihi(Date yuzeyPuruzluluguTarihi) {
		this.yuzeyPuruzluluguTarihi = yuzeyPuruzluluguTarihi;
	}

	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	public IsTakipFormuKumlama getIsTakipFormuKumlama() {
		return isTakipFormuKumlama;
	}

	public void setIsTakipFormuKumlama(IsTakipFormuKumlama isTakipFormuKumlama) {
		this.isTakipFormuKumlama = isTakipFormuKumlama;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Boolean getYuzeyCheck() {
		return yuzeyCheck;
	}

	public void setYuzeyCheck(Boolean yuzeyCheck) {
		this.yuzeyCheck = yuzeyCheck;
	}

	public Date getYuzeyTarihi() {
		return yuzeyTarihi;
	}

	public void setYuzeyTarihi(Date yuzeyTarihi) {
		this.yuzeyTarihi = yuzeyTarihi;
	}

	public String getYuzeyAciklama() {
		return yuzeyAciklama;
	}

	public void setYuzeyAciklama(String yuzeyAciklama) {
		this.yuzeyAciklama = yuzeyAciklama;
	}

	public String getAyarSonrasiIlkBoruAciklama() {
		return ayarSonrasiIlkBoruAciklama;
	}

	public void setAyarSonrasiIlkBoruAciklama(String ayarSonrasiIlkBoruAciklama) {
		this.ayarSonrasiIlkBoruAciklama = ayarSonrasiIlkBoruAciklama;
	}

	public String getTozTestiAciklama() {
		return tozTestiAciklama;
	}

	public void setTozTestiAciklama(String tozTestiAciklama) {
		this.tozTestiAciklama = tozTestiAciklama;
	}

	public String getYuzeyPuruzluluguAciklama() {
		return yuzeyPuruzluluguAciklama;
	}

	public void setYuzeyPuruzluluguAciklama(String yuzeyPuruzluluguAciklama) {
		this.yuzeyPuruzluluguAciklama = yuzeyPuruzluluguAciklama;
	}

	public String getYuzeyKalitesiAciklama() {
		return yuzeyKalitesiAciklama;
	}

	public void setYuzeyKalitesiAciklama(String yuzeyKalitesiAciklama) {
		this.yuzeyKalitesiAciklama = yuzeyKalitesiAciklama;
	}

	public String getKumlamaHiziAciklama() {
		return kumlamaHiziAciklama;
	}

	public void setKumlamaHiziAciklama(String kumlamaHiziAciklama) {
		this.kumlamaHiziAciklama = kumlamaHiziAciklama;
	}

}