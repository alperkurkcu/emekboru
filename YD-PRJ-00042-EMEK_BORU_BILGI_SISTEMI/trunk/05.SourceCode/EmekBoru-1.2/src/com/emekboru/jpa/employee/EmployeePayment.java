package com.emekboru.jpa.employee;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.Employee;
import com.emekboru.jpa.common.CommonPaymentType;

/**
 * The persistent class for the employee_payment database table.
 * 
 */
@Entity
@Table(name = "employee_payment")
@NamedQuery(name = "EmployeePayment.findAll", query = "SELECT e FROM EmployeePayment e")
public class EmployeePayment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "EMPLOYEE_PAYMENT_ID_GENERATOR", sequenceName = "EMPLOYEE_PAYMENT_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMPLOYEE_PAYMENT_ID_GENERATOR")
	@Column(name = "id")
	private Integer id;

	@Column(name = "description")
	private String description;

	@Column(name = "ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@Column(name = "employee_id", insertable = false, updatable = false)
	private Integer employeeId;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "employee_id", referencedColumnName = "employee_id", insertable = false)
	private Employee employee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "hay_degree")
	private Integer hayDegree;

	@Column(name = "payment_amount")
	private float paymentAmount;

	@JoinColumn(name = "payment_type_id", referencedColumnName = "id", insertable = false)
	private CommonPaymentType commonPaymentType;

	public EmployeePayment() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Integer getHayDegree() {
		return this.hayDegree;
	}

	public void setHayDegree(Integer hayDegree) {
		this.hayDegree = hayDegree;
	}

	public float getPaymentAmount() {
		return this.paymentAmount;
	}

	public void setPaymentAmount(float paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public CommonPaymentType getCommonPaymentType() {
		return this.commonPaymentType;
	}

	public void setCommonPaymentType(CommonPaymentType commonPaymentType) {
		this.commonPaymentType = commonPaymentType;
	}

}