package com.emekboru.jpa.sales.documents;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "sales_document_type")
public class SalesDocumentType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2950502406175815722L;

	@Id
	@Column(name = "type_id")
	private int typeId;

	@Column(name = "title")
	private String name;

	@OneToMany
	private List<SalesDocument> salesDocument;

	@Override
	public boolean equals(Object o) {
		return this.getTypeId() == ((SalesDocumentType) o).getTypeId();
	}

	public SalesDocumentType() {
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SalesDocument> getSalesDocument() {
		return salesDocument;
	}

	public void setSalesDocument(List<SalesDocument> salesDocument) {
		this.salesDocument = salesDocument;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
