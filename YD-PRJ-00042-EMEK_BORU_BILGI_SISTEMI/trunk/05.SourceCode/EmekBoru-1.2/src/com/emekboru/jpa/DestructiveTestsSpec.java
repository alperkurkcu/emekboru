package com.emekboru.jpa;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.config.Test;
import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the destructive_tests_spec database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "DestructiveTestsSpec.findByTestId", query = "select o from DestructiveTestsSpec o where o.testId=:prmTestId"),
		@NamedQuery(name = "DestructiveTestsSpec.findByItemId", query = "select n from DestructiveTestsSpec n where n.salesItem.itemId=:prmItemId"),
		@NamedQuery(name = "DestructiveTestsSpec.findByGlobalIdItemId", query = "select n from DestructiveTestsSpec n where n.globalId=:prmGlobalId and n.salesItem.itemId=:prmItemId") })
@Table(name = "destructive_tests_spec")
public class DestructiveTestsSpec implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "DESTRUCTIVE_TESTS_SPEC_GENERATOR", sequenceName = "DESTRUCTIVE_TESTS_SPEC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DESTRUCTIVE_TESTS_SPEC_GENERATOR")
	@Column(name = "test_id")
	private Integer testId;

	@Column(name = "assess_standard")
	private String assessStandard;

	@Column(name = "code")
	private String code;

	@Column(name = "completed")
	private Boolean completed;

	// @Column(name = "diagnose_group", updatable = false)
	// private Integer diagnoseGroup;

	@Column(name = "explanation")
	private String explanation;

	@Column(name = "frequency")
	private float frequency;

	@Column(name = "global_id")
	private Integer globalId;

	@Column(name = "name")
	private String name;

	@Column(name = "standard")
	private String standard;

	@Column(name = "has_specs")
	private Boolean hasSpecs;

	@Column(name = "has_sonuc")
	private Boolean hasSonuc;

	@Basic
	@Column(name = "sales_item_id", insertable = false, updatable = false)
	private Integer salesItemId;

	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private SalesItem salesItem;

	// destructive test specs
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesDestructiveTestSpecs")
	private ChemicalRequirement kimyasalTestsSpecs;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesDestructiveTestSpecs")
	private KaynakSertlikTestsSpec kaynakSertlikTestsSpecs;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesDestructiveTestSpecs")
	private KaynakMakroTestSpec kaynakMakroTestSpecs;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesDestructiveTestSpecs")
	private AgirlikTestsSpec agirlikTestsSpecs;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesDestructiveTestSpecs")
	private CekmeTestsSpec cekmeTestsSpecs;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesDestructiveTestSpecs")
	private BukmeTestsSpec bukmeTestsSpecs;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "salesDestructiveTestSpecs")
	private CentikDarbeTestsSpec centikDarbeTestsSpecs;

	@JoinColumn(name = "diagnose_group", referencedColumnName = "ID", insertable = false)
	@ManyToOne(fetch = FetchType.EAGER)
	private IncelemeGrubu incelemeGrubu;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	public DestructiveTestsSpec() {

		incelemeGrubu = new IncelemeGrubu();
	}

	public DestructiveTestsSpec(Test test) {

		code = test.getCode();
		globalId = test.getGlobalId();
		name = test.getDisplayName();

	}

	public DestructiveTestsSpec(DestructiveTestsSpec t) {

		testId = t.testId;
		assessStandard = t.assessStandard;
		code = t.code;
		completed = t.completed;
		explanation = t.explanation;
		frequency = t.frequency;
		globalId = t.globalId;
		name = t.name;
		standard = t.standard;
		// order = t.order;
		salesItem = t.salesItem;
		hasSpecs = t.hasSpecs;
		hasSonuc = t.hasSonuc;
		incelemeGrubu = t.incelemeGrubu;
		salesItem = t.salesItem;
	}

	public void reset() {

		testId = 0;
		assessStandard = "";
		code = "";
		completed = true;
		explanation = "";
		frequency = 0;
		globalId = 0;
		name = "";
		standard = "";
		hasSpecs = false;
		hasSonuc = false;
		incelemeGrubu = new IncelemeGrubu();
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof DestructiveTestsSpec)) {
			return false;
		}
		DestructiveTestsSpec other = (DestructiveTestsSpec) object;
		if ((this.globalId == null && other.globalId != null)
				|| (this.globalId != null && !this.globalId
						.equals(other.globalId))) {
			return false;
		}
		return true;
	}

	// setters getters

	public Integer getTestId() {
		return testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	public String getAssessStandard() {
		return assessStandard;
	}

	public void setAssessStandard(String assessStandard) {
		this.assessStandard = assessStandard;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Boolean getCompleted() {
		return completed;
	}

	public void setCompleted(Boolean completed) {
		this.completed = completed;
	}

	public String getExplanation() {
		return explanation;
	}

	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}

	public float getFrequency() {
		return frequency;
	}

	public void setFrequency(float frequency) {
		this.frequency = frequency;
	}

	public Integer getGlobalId() {
		return globalId;
	}

	public void setGlobalId(Integer globalId) {
		this.globalId = globalId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public Boolean getHasSpecs() {
		return hasSpecs;
	}

	public void setHasSpecs(Boolean hasSpecs) {
		this.hasSpecs = hasSpecs;
	}

	public Boolean getHasSonuc() {
		return hasSonuc;
	}

	public void setHasSonuc(Boolean hasSonuc) {
		this.hasSonuc = hasSonuc;
	}

	// public Order getOrder() {
	// return order;
	// }
	//
	// public void setOrder(Order order) {
	// this.order = order;
	// }

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public KaynakSertlikTestsSpec getKaynakSertlikTestsSpecs() {
		return kaynakSertlikTestsSpecs;
	}

	public void setKaynakSertlikTestsSpecs(
			KaynakSertlikTestsSpec kaynakSertlikTestsSpecs) {
		this.kaynakSertlikTestsSpecs = kaynakSertlikTestsSpecs;
	}

	public KaynakMakroTestSpec getKaynakMakroTestSpecs() {
		return kaynakMakroTestSpecs;
	}

	public void setKaynakMakroTestSpecs(KaynakMakroTestSpec kaynakMakroTestSpecs) {
		this.kaynakMakroTestSpecs = kaynakMakroTestSpecs;
	}

	public AgirlikTestsSpec getAgirlikTestsSpecs() {
		return agirlikTestsSpecs;
	}

	public void setAgirlikTestsSpecs(AgirlikTestsSpec agirlikTestsSpecs) {
		this.agirlikTestsSpecs = agirlikTestsSpecs;
	}

	public CekmeTestsSpec getCekmeTestsSpecs() {
		return cekmeTestsSpecs;
	}

	public void setCekmeTestsSpecs(CekmeTestsSpec cekmeTestsSpecs) {
		this.cekmeTestsSpecs = cekmeTestsSpecs;
	}

	public BukmeTestsSpec getBukmeTestsSpecs() {
		return bukmeTestsSpecs;
	}

	public void setBukmeTestsSpecs(BukmeTestsSpec bukmeTestsSpecs) {
		this.bukmeTestsSpecs = bukmeTestsSpecs;
	}

	public CentikDarbeTestsSpec getCentikDarbeTestsSpecs() {
		return centikDarbeTestsSpecs;
	}

	public void setCentikDarbeTestsSpecs(
			CentikDarbeTestsSpec centikDarbeTestsSpecs) {
		this.centikDarbeTestsSpecs = centikDarbeTestsSpecs;
	}

	public IncelemeGrubu getIncelemeGrubu() {
		return incelemeGrubu;
	}

	public void setIncelemeGrubu(IncelemeGrubu incelemeGrubu) {
		this.incelemeGrubu = incelemeGrubu;
	}

	public Timestamp getEklemeZamani() {
		return eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	/**
	 * @return the kimyasalTestsSpecs
	 */
	public ChemicalRequirement getKimyasalTestsSpecs() {
		return kimyasalTestsSpecs;
	}

	/**
	 * @param kimyasalTestsSpecs
	 *            the kimyasalTestsSpecs to set
	 */
	public void setKimyasalTestsSpecs(ChemicalRequirement kimyasalTestsSpecs) {
		this.kimyasalTestsSpecs = kimyasalTestsSpecs;
	}

}