package com.emekboru.jpa.istakipformu;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the is_takip_formu_beton_kaplama_devam_islemler
 * database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "IsTakipFormuBetonKaplamaDevamIslemler.findAll", query = "SELECT r FROM IsTakipFormuBetonKaplamaDevamIslemler r WHERE r.salesItem.itemId=:prmItemId") })
@Table(name = "is_takip_formu_beton_kaplama_devam_islemler")
public class IsTakipFormuBetonKaplamaDevamIslemler implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IS_TAKIP_FORMU_BETON_KAPLAMA_DEVAM_ISLEMLER_ID_GENERATOR", sequenceName = "IS_TAKIP_FORMU_BETON_KAPLAMA_DEVAM_ISLEMLER_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IS_TAKIP_FORMU_BETON_KAPLAMA_DEVAM_ISLEMLER_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "bos_tartim")
	private Integer bosTartim;

	@Column(name = "cimento")
	private Integer cimento;

	@Column(name = "dolu_tartim")
	private Integer doluTartim;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

//	@Column(name = "ekleyen_kullanici", nullable = false)
//	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	private Integer fark;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

//	@Column(name = "guncelleyen_kullanici")
//	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "harc_sicakligi")
	private float harcSicakligi;

	@Column(name = "kalinlik")
	private Integer kalinlik;

	@Column(name = "kaplama_hizi")
	private Integer kaplamaHizi;

	@Column(name = "katki_malzeme")
	private Integer katkiMalzeme;

	private Integer kum;

	@Column(name = "nem_rh")
	private float nemRh;

	// @Column(name = "pipe_id", nullable = false)
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@Column(name = "rampa_beton")
	private float rampaBeton;

	@Column(name = "rampa_sikma")
	private float rampaSikma;

	@Column(name = "rampa_suresi")
	private Integer rampaSuresi;

	// @Column(name = "sales_item_id", nullable = false)
	// private Integer salesItemId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	private SalesItem salesItem;

	private Integer su;

	@Column(name = "vardiya")
	private Integer vardiya;

	public IsTakipFormuBetonKaplamaDevamIslemler() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBosTartim() {
		return this.bosTartim;
	}

	public void setBosTartim(Integer bosTartim) {
		this.bosTartim = bosTartim;
	}

	public Integer getCimento() {
		return this.cimento;
	}

	public void setCimento(Integer cimento) {
		this.cimento = cimento;
	}

	public Integer getDoluTartim() {
		return this.doluTartim;
	}

	public void setDoluTartim(Integer doluTartim) {
		this.doluTartim = doluTartim;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	// public Integer getEkleyenKullanici() {
	// return this.ekleyenKullanici;
	// }
	//
	// public void setEkleyenKullanici(Integer ekleyenKullanici) {
	// this.ekleyenKullanici = ekleyenKullanici;
	// }

	public Integer getFark() {
		return this.fark;
	}

	public void setFark(Integer fark) {
		this.fark = fark;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	// public Integer getGuncelleyenKullanici() {
	// return this.guncelleyenKullanici;
	// }
	//
	// public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
	// this.guncelleyenKullanici = guncelleyenKullanici;
	// }

	public float getHarcSicakligi() {
		return this.harcSicakligi;
	}

	public void setHarcSicakligi(float harcSicakligi) {
		this.harcSicakligi = harcSicakligi;
	}

	public Integer getKalinlik() {
		return this.kalinlik;
	}

	public void setKalinlik(Integer kalinlik) {
		this.kalinlik = kalinlik;
	}

	public Integer getKaplamaHizi() {
		return this.kaplamaHizi;
	}

	public void setKaplamaHizi(Integer kaplamaHizi) {
		this.kaplamaHizi = kaplamaHizi;
	}

	public Integer getKatkiMalzeme() {
		return this.katkiMalzeme;
	}

	public void setKatkiMalzeme(Integer katkiMalzeme) {
		this.katkiMalzeme = katkiMalzeme;
	}

	public Integer getKum() {
		return this.kum;
	}

	public void setKum(Integer kum) {
		this.kum = kum;
	}

	public float getNemRh() {
		return this.nemRh;
	}

	public void setNemRh(float nemRh) {
		this.nemRh = nemRh;
	}

	public float getRampaBeton() {
		return this.rampaBeton;
	}

	public void setRampaBeton(float rampaBeton) {
		this.rampaBeton = rampaBeton;
	}

	public float getRampaSikma() {
		return this.rampaSikma;
	}

	public void setRampaSikma(float rampaSikma) {
		this.rampaSikma = rampaSikma;
	}

	public Integer getRampaSuresi() {
		return this.rampaSuresi;
	}

	public void setRampaSuresi(Integer rampaSuresi) {
		this.rampaSuresi = rampaSuresi;
	}

	public Integer getSu() {
		return this.su;
	}

	public void setSu(Integer su) {
		this.su = su;
	}

	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getVardiya() {
		return vardiya;
	}

	public void setVardiya(Integer vardiya) {
		this.vardiya = vardiya;
	}

}