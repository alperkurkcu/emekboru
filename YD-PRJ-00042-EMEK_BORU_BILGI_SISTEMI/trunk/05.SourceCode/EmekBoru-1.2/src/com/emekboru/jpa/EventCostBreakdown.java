package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "event_cost_breakdown")
public class EventCostBreakdown implements Serializable{

	private static final long serialVersionUID = 2523023188441591530L;

	
	@Id
	@Column(name = "event_cost_breakdown_id")
	@SequenceGenerator(name = "EVENT_COST_BREAKDOWN_GENERATOR", sequenceName = "EVENT_COST_BREAKDOWN_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EVENT_COST_BREAKDOWN_GENERATOR")
	private Integer eventCostBreakdownId;
	
	@Column(name = "amount_planned")
	private Double amountPlanned;
	
	@Column(name = "amount_paid")
	private Double amountPaid;
	
	@Column(name = "tl_exchange_rate")
	private Double tlExchangeRate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "event_id", referencedColumnName = "event_id")
	private EventEb event;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cost_type_id", referencedColumnName = "cost_type_id")
	private CostType costType;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currency_code", referencedColumnName = "currency_code")
	private Currency currency;
	
	public EventCostBreakdown(){
		
		costType = new CostType();
		currency = new Currency();
	}

	/**
	 * 		GETTERS AND SETTERS
	 */
	public Integer getEventCostBreakdownId() {
		return eventCostBreakdownId;
	}

	public void setEventCostBreakdownId(Integer eventCostBreakdownId) {
		this.eventCostBreakdownId = eventCostBreakdownId;
	}

	public Double getAmountPlanned() {
		return amountPlanned;
	}

	public void setAmountPlanned(Double amountPlanned) {
		this.amountPlanned = amountPlanned;
	}

	public Double getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(Double amountPaid) {
		this.amountPaid = amountPaid;
	}

	public Double getTlExchangeRate() {
		return tlExchangeRate;
	}

	public void setTlExchangeRate(Double tlExchangeRate) {
		this.tlExchangeRate = tlExchangeRate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public EventEb getEvent() {
		return event;
	}

	public void setEvent(EventEb event) {
		this.event = event;
	}

	public CostType getCostType() {
		return costType;
	}

	public void setCostType(CostType costType) {
		this.costType = costType;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	
}
