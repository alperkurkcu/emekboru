package com.emekboru.jpa.sales.events;

import java.io.Serializable;

import javax.persistence.*;

import com.emekboru.jpa.sales.customer.SalesContactPeople;

@Entity
@Table(name = "sales_join_event_contact_people")
public class SalesJoinEventContactPeople implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6416681400938503664L;

	@Id
	@Column(name = "join_id")
	@SequenceGenerator(name = "SALES_JOIN_EVENT_CONTACT_PEOPLE_GENERATOR", sequenceName = "SALES_JOIN_EVENT_CONTACT_PEOPLE_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_JOIN_EVENT_CONTACT_PEOPLE_GENERATOR")
	private int joinId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "contact_people_id", referencedColumnName = "sales_contact_person_id", updatable = false)
	private SalesContactPeople contactPeople;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "event_id", referencedColumnName = "event_id", updatable = false)
	private SalesEvent event;

	public SalesJoinEventContactPeople() {
		contactPeople = new SalesContactPeople();
		event = new SalesEvent();
	}

	/**
	 * GETTERS AND SETTERS
	 */

	public int getJoinId() {
		return joinId;
	}

	public SalesEvent getEvent() {
		return event;
	}

	public SalesContactPeople getContactPeople() {
		return contactPeople;
	}

	public void setJoinId(Integer joinId) {
		this.joinId = joinId;
	}

	public void setEvent(SalesEvent event) {
		this.event = event;
	}

	public void setContactPeople(SalesContactPeople contactPeople) {
		this.contactPeople = contactPeople;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
