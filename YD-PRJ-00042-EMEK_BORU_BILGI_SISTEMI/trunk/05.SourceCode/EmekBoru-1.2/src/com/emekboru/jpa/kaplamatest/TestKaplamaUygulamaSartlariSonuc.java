package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;

 


/**
 * The persistent class for the test_kaplama_uygulama_sartlari_sonuc database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaUygulamaSartlariSonuc.findAll", query = "SELECT r FROM TestKaplamaUygulamaSartlariSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name="test_kaplama_uygulama_sartlari_sonuc")
public class TestKaplamaUygulamaSartlariSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TEST_KAPLAMA_UYGULAMA_SARTLARI_SONUC_ID_GENERATOR", sequenceName="TEST_KAPLAMA_UYGULAMA_SARTLARI_SONUC_SEQ",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TEST_KAPLAMA_UYGULAMA_SARTLARI_SONUC_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="ekleme_zamani")
	private Date eklemeZamani;

	@Column(name="ekleyen_kullanici")
	private Integer ekleyenKullanici;

//	@Column(name="global_id")
//	private Integer globalId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name="guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	//@Column(name="pipe_id")
	//private Integer pipeId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="test_tarihi")
	private Date testTarihi;

	@Column(name="vardiya")
	private Integer vardiya;

	@Column(name="yuzey_kontrol")
	private Integer yuzeyKontrol;

	public TestKaplamaUygulamaSartlariSonuc() {
	}
	
	
	
	/**
	 * @return the bagliGlobalId
	 */
	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}



	/**
	 * @param bagliGlobalId the bagliGlobalId to set
	 */
	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}



	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getTestTarihi() {
		return this.testTarihi;
	}

	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	public Integer getVardiya() {
		return this.vardiya;
	}

	public void setVardiya(Integer vardiya) {
		this.vardiya = vardiya;
	}

	public Integer getYuzeyKontrol() {
		return this.yuzeyKontrol;
	}

	public void setYuzeyKontrol(Integer yuzeyKontrol) {
		this.yuzeyKontrol = yuzeyKontrol;
	}



	/**
	 * @return the bagliTestId
	 */
	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}



	/**
	 * @param bagliTestId the bagliTestId to set
	 */
	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

}