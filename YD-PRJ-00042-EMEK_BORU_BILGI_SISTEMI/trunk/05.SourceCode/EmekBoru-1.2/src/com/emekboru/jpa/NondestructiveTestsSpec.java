package com.emekboru.jpa;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.config.Test;
import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the nondestructive_tests_spec database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "NondestructiveTestsSpec.findByItemIdGlobalId", query = "select n from NondestructiveTestsSpec n where n.salesItem.itemId=:prmItemId and n.globalId=:prmGlobalId"),
		@NamedQuery(name = "NondestructiveTestsSpec.findByItemId", query = "select n from NondestructiveTestsSpec n where n.salesItem.itemId=:prmItemId") })
@Table(name = "nondestructive_tests_spec")
public class NondestructiveTestsSpec implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "NONDESTRUCTIVE_TESTS_SPEC_GENERATOR", sequenceName = "NONDESTRUCTIVE_TESTS_SPEC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NONDESTRUCTIVE_TESTS_SPEC_GENERATOR")
	@Column(name = "test_id")
	private Integer testId;

	@Column(name = "asses_standard")
	private String assesStandard;

	@Column(name = "calibration")
	private String calibration;

	@Column(name = "completed")
	private Boolean completed;

	@Column(name = "explanation")
	private String explanation;

	@Column(name = "frequency")
	private float frequency;

	@Column(name = "global_id")
	private Integer globalId;

	@Column(name = "name")
	private String name;

	@Column(name = "code")
	private String code;

	@Column(name = "standard")
	private String standard;

	@JoinColumn(name = "order_id", referencedColumnName = "order_id", insertable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private Order order;

	// entegrasyon
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private SalesItem salesItem;

	// entegrasyon

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	public NondestructiveTestsSpec() {
	}

	public NondestructiveTestsSpec(Test t) {

		globalId = t.getGlobalId();
		name = t.getDisplayName();
		code = t.getCode();
	}

	public NondestructiveTestsSpec(NondestructiveTestsSpec t) {

		testId = t.testId;
		code = t.code;
		completed = t.completed;
		explanation = t.explanation;
		frequency = t.frequency;
		globalId = t.globalId;
		name = t.name;
		standard = t.standard;
		order = t.order;
		salesItem = t.salesItem;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof NondestructiveTestsSpec)) {
			return false;
		}
		NondestructiveTestsSpec other = (NondestructiveTestsSpec) object;
		if ((this.globalId == null && other.globalId != null)
				|| (this.globalId != null && !this.globalId
						.equals(other.globalId))) {
			return false;
		}
		return true;
	}

	// setters getters

	public Integer getTestId() {
		return this.testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	public String getAssesStandard() {
		return this.assesStandard;
	}

	public void setAssesStandard(String assesStandard) {
		this.assesStandard = assesStandard;
	}

	public String getCalibration() {
		return this.calibration;
	}

	public void setCalibration(String calibration) {
		this.calibration = calibration;
	}

	public Boolean getCompleted() {
		return this.completed;
	}

	public void setCompleted(Boolean completed) {
		this.completed = completed;
	}

	public String getExplanation() {
		return this.explanation;
	}

	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}

	public float getFrequency() {
		return this.frequency;
	}

	public void setFrequency(float frequency) {
		this.frequency = frequency;
	}

	public Integer getGlobalId() {
		return this.globalId;
	}

	public void setGlobalId(Integer globalId) {
		this.globalId = globalId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStandard() {
		return this.standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public Timestamp getEklemeZamani() {
		return eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

}