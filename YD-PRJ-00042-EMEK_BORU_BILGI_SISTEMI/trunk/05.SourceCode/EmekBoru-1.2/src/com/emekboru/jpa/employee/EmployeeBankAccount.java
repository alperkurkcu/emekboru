package com.emekboru.jpa.employee;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.Employee;

/**
 * The persistent class for the employee_bank_account database table.
 * 
 */
@Entity
@Table(name = "employee_bank_account")
@NamedQuery(name = "EmployeeBankAccount.findAll", query = "SELECT e FROM EmployeeBankAccount e")
public class EmployeeBankAccount implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "EMPLOYEE_BANK_ACCOUNT_EMPLOYEEBANKACCOUNTID_GENERATOR", sequenceName = "EMPLOYEE_BANK_ACCOUNT_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMPLOYEE_BANK_ACCOUNT_EMPLOYEEBANKACCOUNTID_GENERATOR")
	@Column(name = "employee_bank_account_id")
	private Integer employeeBankAccountId;

	@Column(name = "description")
	private String description;

	@Column(name = "ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name = "is_default")
	private Boolean isDefault;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "iban")
	private String iban;

	// bi-directional many-to-one association to Employee
	@ManyToOne
	@JoinColumn(name = "employee_id", insertable = false)
	private Employee employee;

	public EmployeeBankAccount() {
	}

	public Integer getEmployeeBankAccountId() {
		return this.employeeBankAccountId;
	}

	public void setEmployeeBankAccountId(Integer employeeBankAccountId) {
		this.employeeBankAccountId = employeeBankAccountId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getIban() {
		return this.iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public Employee getEmployee() {
		return this.employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

}