package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.config.Test;
import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the cekme_tests_spec database table.
 * 
 */

@NamedQueries({
		@NamedQuery(name = "CTS.kontrol", query = "SELECT r.testId FROM CekmeTestsSpec r WHERE r.globalId = :prmGlobalId and r.salesItem.itemId=:prmItemId"),
		@NamedQuery(name = "CTS.seciliCekmeTestTanim", query = "SELECT r FROM CekmeTestsSpec r WHERE r.globalId = :prmGlobalId and r.salesItem.itemId=:prmItemId"),
		@NamedQuery(name = "CTS.findByItemId", query = "SELECT r FROM CekmeTestsSpec r WHERE r.salesItem.itemId=:prmItemId order by r.globalId") })
@Entity
@Table(name = "cekme_tests_spec")
public class CekmeTestsSpec implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final int KAYNAKLI = 1013;
	public static final int MALZEME_ENINE = 1014;
	public static final int MALZEME_BOYUNA = 1015;
	public static final int BANT_EKI_KAYNAGI = 1016;
	public static final int TAM_KAYNAK = 1017;

	@Id
	@SequenceGenerator(name = "CEKME_TESTS_SPEC_GENERATOR", sequenceName = "CEKME_TESTS_SPEC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CEKME_TESTS_SPEC_GENERATOR")
	@Column(name = "test_id")
	private Integer testId;

	@Column(name = "akma_dayanci_max")
	private Float akmaDayanciMax;

	@Column(name = "akma_dayanci_min")
	private Float akmaDayanciMin;

	@Column(name = "cekme_dayanci_max")
	private Float cekmeDayanciMax;

	@Column(name = "cekme_dayanci_min")
	private Float cekmeDayanciMin;

	@Column(name = "completed")
	private Boolean completed;

	@Column(name = "explanation")
	private String explanation;

	@Column(name = "global_id")
	private Integer globalId;

	@Column(name = "name")
	private String name;

	@Column(name = "code")
	private String code;

	@Column(name = "uzama_max")
	private Float uzamaMax;

	@Column(name = "uzama_min")
	private Float uzamaMin;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	private SalesItem salesItem;

	@ManyToOne
	@JoinColumns({
			@JoinColumn(name = "sales_item_id", referencedColumnName = "sales_item_id", insertable = false, updatable = false),
			@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false, updatable = false) })
	private DestructiveTestsSpec salesDestructiveTestSpecs;

	@Column(name = "akma_bolu_cekme_max")
	private Float akmaBoluCekmeMax;

	@Column(name = "akma_bolu_cekme_min")
	private Float akmaBoluCekmeMin;

	public CekmeTestsSpec() {
	}

	public CekmeTestsSpec(Test test) {

		globalId = test.getGlobalId();
		name = test.getDisplayName();
		code = test.getCode();
		// completed = true;
		completed = false;
	}

	public void reset() {

		testId = 0;
		akmaDayanciMax = (float) 0.0;
		akmaDayanciMin = (float) 0.0;
		cekmeDayanciMax = (float) 0.0;
		cekmeDayanciMin = (float) 0.0;
		// completed = true;
		completed = false;
		explanation = "";
		uzamaMax = (float) 0.0;
		uzamaMin = (float) 0.0;
	}

	// GETTERS AND SETTERS
	public Integer getTestId() {
		return this.testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	public Boolean getCompleted() {
		return completed;
	}

	public void setCompleted(Boolean completed) {
		this.completed = completed;
	}

	public String getExplanation() {
		return explanation;
	}

	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}

	public Integer getGlobalId() {
		return globalId;
	}

	public void setGlobalId(Integer globalId) {
		this.globalId = globalId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public DestructiveTestsSpec getSalesDestructiveTestSpecs() {
		return salesDestructiveTestSpecs;
	}

	public void setSalesDestructiveTestSpecs(
			DestructiveTestsSpec salesDestructiveTestSpecs) {
		this.salesDestructiveTestSpecs = salesDestructiveTestSpecs;
	}

	public Float getAkmaDayanciMax() {
		return akmaDayanciMax;
	}

	public void setAkmaDayanciMax(Float akmaDayanciMax) {
		this.akmaDayanciMax = akmaDayanciMax;
	}

	public Float getAkmaDayanciMin() {
		return akmaDayanciMin;
	}

	public void setAkmaDayanciMin(Float akmaDayanciMin) {
		this.akmaDayanciMin = akmaDayanciMin;
	}

	public Float getCekmeDayanciMax() {
		return cekmeDayanciMax;
	}

	public void setCekmeDayanciMax(Float cekmeDayanciMax) {
		this.cekmeDayanciMax = cekmeDayanciMax;
	}

	public Float getCekmeDayanciMin() {
		return cekmeDayanciMin;
	}

	public void setCekmeDayanciMin(Float cekmeDayanciMin) {
		this.cekmeDayanciMin = cekmeDayanciMin;
	}

	public Float getUzamaMax() {
		return uzamaMax;
	}

	public void setUzamaMax(Float uzamaMax) {
		this.uzamaMax = uzamaMax;
	}

	public Float getUzamaMin() {
		return uzamaMin;
	}

	public void setUzamaMin(Float uzamaMin) {
		this.uzamaMin = uzamaMin;
	}

	public Float getAkmaBoluCekmeMax() {
		return akmaBoluCekmeMax;
	}

	public void setAkmaBoluCekmeMax(Float akmaBoluCekmeMax) {
		this.akmaBoluCekmeMax = akmaBoluCekmeMax;
	}

	public Float getAkmaBoluCekmeMin() {
		return akmaBoluCekmeMin;
	}

	public void setAkmaBoluCekmeMin(Float akmaBoluCekmeMin) {
		this.akmaBoluCekmeMin = akmaBoluCekmeMin;
	}

}