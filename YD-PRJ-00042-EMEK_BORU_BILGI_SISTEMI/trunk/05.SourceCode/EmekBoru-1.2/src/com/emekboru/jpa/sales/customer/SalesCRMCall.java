package com.emekboru.jpa.sales.customer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.emekboru.jpa.Employee;

@Entity
@Table(name = "sales_crm_call")
public class SalesCRMCall implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6900844515512729166L;

	@Id
	@Column(name = "sales_crm_call_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_CRM_CALL_GENERATOR")
	@SequenceGenerator(name = "SALES_CRM_CALL_GENERATOR", sequenceName = "SALES_CRM_CALL_SEQUENCE", allocationSize = 1)
	private int salesCRMCallId;

	@Column(name = "meeting_agenda")
	private String meetingAgenda;

	@Temporal(TemporalType.TIME)
	@Column(name = "call_time")
	private Date callTime;

	@Temporal(TemporalType.DATE)
	@Column(name = "call_date")
	private Date callDate;

	@Column(name = "notes")
	private String notes;

	/* JOIN FUNCTIONS */
	@Transient
	private List<Employee> participantList;

	@Transient
	private List<SalesContactPeople> contactPeopleList;
	/* end of JOIN FUNCTIONS */

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_customer_id", referencedColumnName = "sales_customer_id")
	private SalesCustomer salesCustomer;

	public SalesCRMCall() {

		participantList = new ArrayList<Employee>();
	}

	@Override
	public boolean equals(Object o) {
		return this.getSalesCRMCallId() == ((SalesCRMCall) o)
				.getSalesCRMCallId();
	}

	/**
	 * GETTERS
	 */
	public int getSalesCRMCallId() {
		return salesCRMCallId;
	}

	public String getMeetingAgenda() {
		return meetingAgenda;
	}

	public Date getCallTime() {
		return callTime;
	}

	public Date getCallDate() {
		return callDate;
	}

	public String getNotes() {
		return notes;
	}

	public SalesCustomer getSalesCustomer() {
		return salesCustomer;
	}

	public List<Employee> getParticipantList() {
		return participantList;
	}

	public List<SalesContactPeople> getContactPeopleList() {
		return contactPeopleList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * SETTERS
	 */
	public void setMeetingAgenda(String meetingAgenda) {
		this.meetingAgenda = meetingAgenda;
	}

	public void setCallTime(Date callTime) {
		this.callTime = callTime;
	}

	public void setCallDate(Date callDate) {
		this.callDate = callDate;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public void setSalesCustomer(SalesCustomer salesCustomer) {
		this.salesCustomer = salesCustomer;
	}

	public void setParticipantList(List<Employee> participantList) {
		this.participantList = participantList;
	}

	public void setContactPeopleList(List<SalesContactPeople> contactPeopleList) {
		this.contactPeopleList = contactPeopleList;
	}

}