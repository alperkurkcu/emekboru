package com.emekboru.jpa.sales.order;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQueries({ @NamedQuery(name = "SalesOrder.findOrderByProposalId", query = "SELECT r FROM SalesOrder r WHERE r.proposal.proposalId =:prmProposalId") })
@Table(name = "sales_order")
public class SalesOrder implements Serializable {

	private static final long serialVersionUID = 5147271525679128117L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_ORDER_GENERATOR")
	@SequenceGenerator(name = "SALES_ORDER_GENERATOR", sequenceName = "SALES_ORDER_SEQUENCE", allocationSize = 1)
	@Column(name = "order_id")
	private int orderId;

	@Column(name = "order_no")
	private String orderNo;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "proposal", referencedColumnName = "proposal_id")
	private SalesProposal proposal;

	@Temporal(TemporalType.DATE)
	@Column(name = "form_date")
	private Date formDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "delivery_date")
	private Date deliveryDate;

	@Column(name = "is_in_scope_one")
	private boolean isInScope1;

	@Column(name = "is_in_scope_two")
	private boolean isInScope2;

	@Column(name = "is_in_scope_three")
	private boolean isInScope3;

	@Column(name = "is_in_scope_four")
	private boolean isInScope4;

	@Column(name = "is_in_scope_five")
	private boolean isInScope5;

	@Column(name = "approximate_amount_for_transportation")
	private double approximateAmountForTransportation;

	@Column(name = "approximate_amount_detail_for_transportation")
	private String approximateAmountDetailForTransportation;

	@Column(name = "approximate_amount")
	private double approximateAmount;

	@Column(name = "approximate_amount_detail")
	private String approximateAmountDetail;

	/*
	 * @OneToMany(mappedBy = "order_id") private List<SalesPayment> paymentList;
	 */

	public SalesOrder() {
	}

	@Override
	public boolean equals(Object o) {
		return this.getOrderId() == ((SalesOrder) o).getOrderId();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public SalesProposal getProposal() {
		return proposal;
	}

	public void setProposal(SalesProposal proposal) {
		this.proposal = proposal;
	}

	public Date getFormDate() {
		return formDate;
	}

	public void setFormDate(Date formDate) {
		this.formDate = formDate;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public boolean getIsInScope1() {
		return isInScope1;
	}

	public void setIsInScope1(boolean isInScope1) {
		this.isInScope1 = isInScope1;
	}

	public boolean getIsInScope2() {
		return isInScope2;
	}

	public void setIsInScope2(boolean isInScope2) {
		this.isInScope2 = isInScope2;
	}

	public boolean getIsInScope3() {
		return isInScope3;
	}

	public void setIsInScope3(boolean isInScope3) {
		this.isInScope3 = isInScope3;
	}

	public boolean getIsInScope4() {
		return isInScope4;
	}

	public void setIsInScope4(boolean isInScope4) {
		this.isInScope4 = isInScope4;
	}

	public boolean getIsInScope5() {
		return isInScope5;
	}

	public void setIsInScope5(boolean isInScope5) {
		this.isInScope5 = isInScope5;
	}

	public double getApproximateAmountForTransportation() {
		return approximateAmountForTransportation;
	}

	public void setApproximateAmountForTransportation(
			double approximateAmountForTransportation) {
		this.approximateAmountForTransportation = approximateAmountForTransportation;
	}

	public String getApproximateAmountDetailForTransportation() {
		return approximateAmountDetailForTransportation;
	}

	public void setApproximateAmountDetailForTransportation(
			String approximateAmountDetailForTransportation) {
		this.approximateAmountDetailForTransportation = approximateAmountDetailForTransportation;
	}

	public double getApproximateAmount() {
		return approximateAmount;
	}

	public void setApproximateAmount(double approximateAmount) {
		this.approximateAmount = approximateAmount;
	}

	public String getApproximateAmountDetail() {
		return approximateAmountDetail;
	}

	public void setApproximateAmountDetail(String approximateAmountDetail) {
		this.approximateAmountDetail = approximateAmountDetail;
	}

	/*
	 * public List<SalesPayment> getPaymentList() { return paymentList; }
	 * 
	 * public void setPaymentList(List<SalesPayment> paymentList) {
	 * this.paymentList = paymentList; }
	 */

	// public String getIstavroz() {
	// return istavroz;
	// }
	//
	// public void setIstavroz(String istavroz) {
	// this.istavroz = istavroz;
	// }

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
