package com.emekboru.jpa.machines;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Machine;
import com.emekboru.jpa.MachineType;
import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the machine_duruslar_ariza database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "MachineDuruslarAriza.findAll", query = "SELECT m FROM MachineDuruslarAriza m order by m.id"),
		@NamedQuery(name = "MachineDuruslarAriza.sorgula", query = "SELECT m FROM MachineDuruslarAriza m WHERE m.baslamaZamani BETWEEN :prmBaslamaZamani AND :prmBitisZamani AND m.machineType.machineTypeId=:prmMachineTypeId order by m.id"),
		@NamedQuery(name = "MachineDuruslarAriza.findYesterdaysAriza", query = "SELECT m FROM MachineDuruslarAriza m WHERE m.baslamaZamani BETWEEN :prmDate1 AND :prmDate2 AND m.machine.machineId=:prmMachineId order by m.id") })
@Table(name = "machine_duruslar_ariza")
public class MachineDuruslarAriza implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "MACHINE_DURUSLAR_ARIZA_ID_GENERATOR", sequenceName = "MACHINE_DURUSLAR_ARIZA_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MACHINE_DURUSLAR_ARIZA_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(length = 255, name = "aciklama")
	private String aciklama;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "baslama_zamani", nullable = false)
	private Date baslamaZamani;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "bitis_zamani")
	private Date bitisZamani;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name = "machine_id", nullable = false)
	// private Integer machineId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "machine_id", referencedColumnName = "machine_id", insertable = false)
	private Machine machine;

	// @Column(name = "machine_type_id", nullable = false)
	// private Integer machineTypeId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "machine_type_id", referencedColumnName = "machine_type_id", insertable = false)
	private MachineType machineType;

	@Column(nullable = false, name = "vardiya")
	private Integer vardiya;

	public MachineDuruslarAriza() {

		machine = new Machine();
		machineType = new MachineType();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Integer getVardiya() {
		return this.vardiya;
	}

	public void setVardiya(Integer vardiya) {
		this.vardiya = vardiya;
	}

	/**
	 * @return the machine
	 */
	public Machine getMachine() {
		return machine;
	}

	/**
	 * @param machine
	 *            the machine to set
	 */
	public void setMachine(Machine machine) {
		this.machine = machine;
	}

	/**
	 * @return the machineType
	 */
	public MachineType getMachineType() {
		return machineType;
	}

	/**
	 * @param machineType
	 *            the machineType to set
	 */
	public void setMachineType(MachineType machineType) {
		this.machineType = machineType;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the baslamaZamani
	 */
	public Date getBaslamaZamani() {
		return baslamaZamani;
	}

	/**
	 * @param baslamaZamani
	 *            the baslamaZamani to set
	 */
	public void setBaslamaZamani(Date baslamaZamani) {
		this.baslamaZamani = baslamaZamani;
	}

	/**
	 * @return the bitisZamani
	 */
	public Date getBitisZamani() {
		return bitisZamani;
	}

	/**
	 * @param bitisZamani
	 *            the bitisZamani to set
	 */
	public void setBitisZamani(Date bitisZamani) {
		this.bitisZamani = bitisZamani;
	}

}