package com.emekboru.jpa.istakipformu;

import static javax.persistence.FetchType.EAGER;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the is_takip_formu_epoksi_kaplama_devam_islemler
 * database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "IsTakipFormuEpoksiKaplamaDevamIslemler.findAll", query = "SELECT r FROM IsTakipFormuEpoksiKaplamaDevamIslemler r WHERE r.salesItem.itemId=:prmItemId") })
@Table(name = "is_takip_formu_epoksi_kaplama_devam_islemler")
public class IsTakipFormuEpoksiKaplamaDevamIslemler implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IS_TAKIP_FORMU_EPOKSI_KAPLAMA_DEVAM_ISLEMLER_ID_GENERATOR", sequenceName = "IS_TAKIP_FORMU_EPOKSI_KAPLAMA_DEVAM_ISLEMLER_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IS_TAKIP_FORMU_EPOKSI_KAPLAMA_DEVAM_ISLEMLER_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(length = 2147483647, name = "aciklamalar")
	private String aciklamalar;

	@Column(name = "boru_sicakligi", length = 32)
	private String boruSicakligi;

	@Column(name = "boya_sicakligi")
	private Integer boyaSicakligi;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name = "sales_item_id", nullable = false)
	// private Integer salesItemId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	private SalesItem salesItem;

	@Column(name = "kaplama_hizi")
	private float kaplamaHizi;

	@Column(name = "kuru_kalinlik", length = 24)
	private String kuruKalinlik;

	@Column(name = "malzeme_sarfiyat")
	private Integer malzemeSarfiyat;

	// @Column(name = "pipe_id", nullable = false)
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@Column(nullable = false, name = "vardiya")
	private Integer vardiya;

	@Column(name = "yas_kalinlik", length = 32)
	private String yasKalinlik;

	@Column(name = "ortam_sicakligi", length = 32)
	private String ortamSicakligi;

	@Column(name = "bagil_nem", length = 32)
	private String bagilNem;

	@Column(name = "ciglenme_sicakligi", length = 32)
	private String ciglenmeSicakligi;

	public IsTakipFormuEpoksiKaplamaDevamIslemler() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklamalar() {
		return this.aciklamalar;
	}

	public void setAciklamalar(String aciklamalar) {
		this.aciklamalar = aciklamalar;
	}

	public String getBoruSicakligi() {
		return this.boruSicakligi;
	}

	public void setBoruSicakligi(String boruSicakligi) {
		this.boruSicakligi = boruSicakligi;
	}

	public Integer getBoyaSicakligi() {
		return this.boyaSicakligi;
	}

	public void setBoyaSicakligi(Integer boyaSicakligi) {
		this.boyaSicakligi = boyaSicakligi;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public float getKaplamaHizi() {
		return this.kaplamaHizi;
	}

	public void setKaplamaHizi(float kaplamaHizi) {
		this.kaplamaHizi = kaplamaHizi;
	}

	public String getKuruKalinlik() {
		return this.kuruKalinlik;
	}

	public void setKuruKalinlik(String kuruKalinlik) {
		this.kuruKalinlik = kuruKalinlik;
	}

	public Integer getMalzemeSarfiyat() {
		return this.malzemeSarfiyat;
	}

	public void setMalzemeSarfiyat(Integer malzemeSarfiyat) {
		this.malzemeSarfiyat = malzemeSarfiyat;
	}

	public Integer getVardiya() {
		return this.vardiya;
	}

	public void setVardiya(Integer vardiya) {
		this.vardiya = vardiya;
	}

	public String getYasKalinlik() {
		return this.yasKalinlik;
	}

	public void setYasKalinlik(String yasKalinlik) {
		this.yasKalinlik = yasKalinlik;
	}

	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the ortamSicakligi
	 */
	public String getOrtamSicakligi() {
		return ortamSicakligi;
	}

	/**
	 * @param ortamSicakligi the ortamSicakligi to set
	 */
	public void setOrtamSicakligi(String ortamSicakligi) {
		this.ortamSicakligi = ortamSicakligi;
	}

	/**
	 * @return the bagilNem
	 */
	public String getBagilNem() {
		return bagilNem;
	}

	/**
	 * @param bagilNem the bagilNem to set
	 */
	public void setBagilNem(String bagilNem) {
		this.bagilNem = bagilNem;
	}

	/**
	 * @return the ciglenmeSicakligi
	 */
	public String getCiglenmeSicakligi() {
		return ciglenmeSicakligi;
	}

	/**
	 * @param ciglenmeSicakligi the ciglenmeSicakligi to set
	 */
	public void setCiglenmeSicakligi(String ciglenmeSicakligi) {
		this.ciglenmeSicakligi = ciglenmeSicakligi;
	}

}