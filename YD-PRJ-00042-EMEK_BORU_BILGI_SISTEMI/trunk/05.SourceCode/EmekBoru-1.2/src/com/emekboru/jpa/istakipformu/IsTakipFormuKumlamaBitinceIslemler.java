package com.emekboru.jpa.istakipformu;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the is_takip_formu_kumlama_bitince_islemler database
 * table.
 * 
 */
@Entity
@Table(name = "is_takip_formu_kumlama_bitince_islemler")
public class IsTakipFormuKumlamaBitinceIslemler implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IS_TAKIP_FORMU_KUMLAMA_BITINCE_ISLEMLER_ID_GENERATOR", sequenceName = "IS_TAKIP_FORMU_KUMLAMA_BITINCE_ISLEMLER_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IS_TAKIP_FORMU_KUMLAMA_BITINCE_ISLEMLER_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "gerceklesen_sarfiyat")
	private Integer gerceklesenSarfiyat;

	@Column(name = "gerceklesen_sarfiyat_check")
	private Boolean gerceklesenSarfiyatCheck;

	@Temporal(TemporalType.DATE)
	@Column(name = "gerceklesen_sarfiyat_tarihi")
	private Date gerceklesenSarfiyatTarihi;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name = "is_takip_form_id")
	// private Integer isTakipFormId;

	@OneToOne(mappedBy = "isTakipFormuKumlamaBitinceIslemler", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	private IsTakipFormuKumlama isTakipFormuKumlama;

	@Column(name = "hedeflenen_sarfiyat")
	private Integer hedeflenenSarfiyat;

	@Column(name = "hedeflenen_sarfiyat_check")
	private Boolean hedeflenenSarfiyatCheck;

	@Temporal(TemporalType.DATE)
	@Column(name = "hedeflenen_sarfiyat_tarihi")
	private Date hedeflenenSarfiyatTarihi;

	public IsTakipFormuKumlamaBitinceIslemler() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getGerceklesenSarfiyat() {
		return this.gerceklesenSarfiyat;
	}

	public void setGerceklesenSarfiyat(Integer gerceklesenSarfiyat) {
		this.gerceklesenSarfiyat = gerceklesenSarfiyat;
	}

	public Boolean getGerceklesenSarfiyatCheck() {
		return this.gerceklesenSarfiyatCheck;
	}

	public void setGerceklesenSarfiyatCheck(Boolean gerceklesenSarfiyatCheck) {
		this.gerceklesenSarfiyatCheck = gerceklesenSarfiyatCheck;
	}

	public Date getGerceklesenSarfiyatTarihi() {
		return this.gerceklesenSarfiyatTarihi;
	}

	public void setGerceklesenSarfiyatTarihi(Date gerceklesenSarfiyatTarihi) {
		this.gerceklesenSarfiyatTarihi = gerceklesenSarfiyatTarihi;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getHedeflenenSarfiyat() {
		return this.hedeflenenSarfiyat;
	}

	public void setHedeflenenSarfiyat(Integer hedeflenenSarfiyat) {
		this.hedeflenenSarfiyat = hedeflenenSarfiyat;
	}

	public Boolean getHedeflenenSarfiyatCheck() {
		return this.hedeflenenSarfiyatCheck;
	}

	public void setHedeflenenSarfiyatCheck(Boolean hedeflenenSarfiyatCheck) {
		this.hedeflenenSarfiyatCheck = hedeflenenSarfiyatCheck;
	}

	public Date getHedeflenenSarfiyatTarihi() {
		return this.hedeflenenSarfiyatTarihi;
	}

	public void setHedeflenenSarfiyatTarihi(Date hedeflenenSarfiyatTarihi) {
		this.hedeflenenSarfiyatTarihi = hedeflenenSarfiyatTarihi;
	}

	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	public IsTakipFormuKumlama getIsTakipFormuKumlama() {
		return isTakipFormuKumlama;
	}

	public void setIsTakipFormuKumlama(IsTakipFormuKumlama isTakipFormuKumlama) {
		this.isTakipFormuKumlama = isTakipFormuKumlama;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}