package com.emekboru.jpa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.rulo.RuloPipeLink;

/**
 * The persistent class for the test_centik_darbe_sonuc database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "TestCentikDarbeSonuc.findAll", query = "SELECT r FROM TestCentikDarbeSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId"),
		@NamedQuery(name = "TestCentikDarbeSonuc.getBySalesItem", query = "SELECT r FROM TestCentikDarbeSonuc r WHERE r.pipe.salesItem.itemId=:prmSalesItem order by r.partiNo"),
		@NamedQuery(name = "TestCentikDarbeSonuc.getBySalesItemGlobalId", query = "SELECT r FROM TestCentikDarbeSonuc r WHERE r.pipe.salesItem.itemId=:prmSalesItem and r.bagliGlobalId.globalId=:prmGlobalId order by r.partiNo"),
		@NamedQuery(name = "TestCentikDarbeSonuc.getByDokumNo", query = "SELECT r FROM TestCentikDarbeSonuc r WHERE r.pipe.ruloPipeLink.rulo.ruloDokumNo=:prmDokumNo"),
		@NamedQuery(name = "TestCentikDarbeSonuc.getByDokumNoSalesItem", query = "SELECT r FROM TestCentikDarbeSonuc r WHERE r.ruloPipeLink.rulo.ruloDokumNo=:prmDokumNo and r.pipe.salesItem.itemId=:prmItemId order by r.pipe.pipeBarkodNo, r.bagliTestId.code") })
@Table(name = "test_centik_darbe_sonuc")
public class TestCentikDarbeSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_CENTIK_DARBE_SONUC_ID_GENERATOR", sequenceName = "TEST_CENTIK_DARBE_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_CENTIK_DARBE_SONUC_ID_GENERATOR")
	@Column(name = "ID")
	private Integer id;

	@Column(name = "aciklama")
	private String aciklama;

	// @Column(name="destructive_test_id")
	// private Integer destructiveTestId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi", nullable = false)
	private Date testTarihi;

	@Column(name = "enerji_ikinci")
	private BigDecimal enerjiIkinci;

	@Column(name = "enerji_ilk")
	private BigDecimal enerjiIlk;

	@Column(name = "enerji_ucuncu")
	private BigDecimal enerjiUcuncu;

	@Column(name = "ortalama_enerji")
	private BigDecimal ortalamaEnerji;

	// @Column(name="global_id")
	// private Integer globalId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "kesme_alani_ikinci")
	private BigDecimal kesmeAlaniIkinci;

	@Column(name = "kesme_alani_ilk")
	private BigDecimal kesmeAlaniIlk;

	@Column(name = "kesme_alani_ucuncu")
	private BigDecimal kesmeAlaniUcuncu;

	@Column(name = "ortalama_kesme_alani")
	private BigDecimal ortalamaKesmeAlani;

	@Column(name = "parti_no")
	private String partiNo;

	// @Column(name="pipe_id")
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private DestructiveTestsSpec bagliGlobalId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "destructive_test_id", referencedColumnName = "test_id", insertable = false)
	private DestructiveTestsSpec bagliTestId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	private Boolean sonuc;

	@Column(name = "test_durum")
	private Boolean testDurum;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", nullable = false, referencedColumnName = "pipe_id", insertable = false, updatable = false)
	private RuloPipeLink ruloPipeLink;

	@Column(name = "sample_no")
	private String sampleNo;

	@Column(name = "numune_yeri")
	private Integer numuneYeri;

	@Column(name = "numune_yonu")
	private Integer numuneYonu;

	@Column(name = "centik_yeri")
	private Integer centikYeri;

	public TestCentikDarbeSonuc() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getEklemeZamani() {
		return eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Date getTestTarihi() {
		return testTarihi;
	}

	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	public Date getGuncellemeZamani() {
		return guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public DestructiveTestsSpec getBagliGlobalId() {
		return bagliGlobalId;
	}

	public void setBagliGlobalId(DestructiveTestsSpec bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	public DestructiveTestsSpec getBagliTestId() {
		return bagliTestId;
	}

	public void setBagliTestId(DestructiveTestsSpec bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public Boolean getSonuc() {
		return sonuc;
	}

	public void setSonuc(Boolean sonuc) {
		this.sonuc = sonuc;
	}

	/**
	 * @return the testDurum
	 */
	public Boolean getTestDurum() {
		return testDurum;
	}

	/**
	 * @param testDurum
	 *            the testDurum to set
	 */
	public void setTestDurum(Boolean testDurum) {
		this.testDurum = testDurum;
	}

	/**
	 * @return the ruloPipeLink
	 */
	public RuloPipeLink getRuloPipeLink() {
		return ruloPipeLink;
	}

	/**
	 * @param ruloPipeLink
	 *            the ruloPipeLink to set
	 */
	public void setRuloPipeLink(RuloPipeLink ruloPipeLink) {
		this.ruloPipeLink = ruloPipeLink;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the enerjiIkinci
	 */
	public BigDecimal getEnerjiIkinci() {
		return enerjiIkinci;
	}

	/**
	 * @param enerjiIkinci
	 *            the enerjiIkinci to set
	 */
	public void setEnerjiIkinci(BigDecimal enerjiIkinci) {
		this.enerjiIkinci = enerjiIkinci;
	}

	/**
	 * @return the enerjiIlk
	 */
	public BigDecimal getEnerjiIlk() {
		return enerjiIlk;
	}

	/**
	 * @param enerjiIlk
	 *            the enerjiIlk to set
	 */
	public void setEnerjiIlk(BigDecimal enerjiIlk) {
		this.enerjiIlk = enerjiIlk;
	}

	/**
	 * @return the enerjiUcuncu
	 */
	public BigDecimal getEnerjiUcuncu() {
		return enerjiUcuncu;
	}

	/**
	 * @param enerjiUcuncu
	 *            the enerjiUcuncu to set
	 */
	public void setEnerjiUcuncu(BigDecimal enerjiUcuncu) {
		this.enerjiUcuncu = enerjiUcuncu;
	}

	/**
	 * @return the ortalamaEnerji
	 */
	public BigDecimal getOrtalamaEnerji() {
		return ortalamaEnerji;
	}

	/**
	 * @param ortalamaEnerji
	 *            the ortalamaEnerji to set
	 */
	public void setOrtalamaEnerji(BigDecimal ortalamaEnerji) {
		this.ortalamaEnerji = ortalamaEnerji;
	}

	/**
	 * @return the kesmeAlaniIkinci
	 */
	public BigDecimal getKesmeAlaniIkinci() {
		return kesmeAlaniIkinci;
	}

	/**
	 * @param kesmeAlaniIkinci
	 *            the kesmeAlaniIkinci to set
	 */
	public void setKesmeAlaniIkinci(BigDecimal kesmeAlaniIkinci) {
		this.kesmeAlaniIkinci = kesmeAlaniIkinci;
	}

	/**
	 * @return the kesmeAlaniIlk
	 */
	public BigDecimal getKesmeAlaniIlk() {
		return kesmeAlaniIlk;
	}

	/**
	 * @param kesmeAlaniIlk
	 *            the kesmeAlaniIlk to set
	 */
	public void setKesmeAlaniIlk(BigDecimal kesmeAlaniIlk) {
		this.kesmeAlaniIlk = kesmeAlaniIlk;
	}

	/**
	 * @return the kesmeAlaniUcuncu
	 */
	public BigDecimal getKesmeAlaniUcuncu() {
		return kesmeAlaniUcuncu;
	}

	/**
	 * @param kesmeAlaniUcuncu
	 *            the kesmeAlaniUcuncu to set
	 */
	public void setKesmeAlaniUcuncu(BigDecimal kesmeAlaniUcuncu) {
		this.kesmeAlaniUcuncu = kesmeAlaniUcuncu;
	}

	/**
	 * @return the ortalamaKesmeAlani
	 */
	public BigDecimal getOrtalamaKesmeAlani() {
		return ortalamaKesmeAlani;
	}

	/**
	 * @param ortalamaKesmeAlani
	 *            the ortalamaKesmeAlani to set
	 */
	public void setOrtalamaKesmeAlani(BigDecimal ortalamaKesmeAlani) {
		this.ortalamaKesmeAlani = ortalamaKesmeAlani;
	}

	/**
	 * @return the sampleNo
	 */
	public String getSampleNo() {
		return sampleNo;
	}

	/**
	 * @param sampleNo
	 *            the sampleNo to set
	 */
	public void setSampleNo(String sampleNo) {
		this.sampleNo = sampleNo;
	}

	/**
	 * @return the partiNo
	 */
	public String getPartiNo() {
		return partiNo;
	}

	/**
	 * @param partiNo
	 *            the partiNo to set
	 */
	public void setPartiNo(String partiNo) {
		this.partiNo = partiNo;
	}

	/**
	 * @return the numuneYeri
	 */
	public Integer getNumuneYeri() {
		return numuneYeri;
	}

	/**
	 * @param numuneYeri
	 *            the numuneYeri to set
	 */
	public void setNumuneYeri(Integer numuneYeri) {
		this.numuneYeri = numuneYeri;
	}

	/**
	 * @return the numuneYonu
	 */
	public Integer getNumuneYonu() {
		return numuneYonu;
	}

	/**
	 * @param numuneYonu
	 *            the numuneYonu to set
	 */
	public void setNumuneYonu(Integer numuneYonu) {
		this.numuneYonu = numuneYonu;
	}

	/**
	 * @return the centikYeri
	 */
	public Integer getCentikYeri() {
		return centikYeri;
	}

	/**
	 * @param centikYeri
	 *            the centikYeri to set
	 */
	public void setCentikYeri(Integer centikYeri) {
		this.centikYeri = centikYeri;
	}

}