package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The persistent class for the transportation_cost database table.
 * 
 */
@Entity
@Table(name = "transportation_cost")
public class TransportationCost implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "transportation_cost_id")
	private Integer transportationCostId;

	@Column(name = "pipe_cost")
	private float pipe_cost;

	@Column(name = "weight_unit_cost")
	private float weightUnitCost;

	@Column(name = "currency_code")
	private String currencyCode;

	@Column(name = "internal_external")
	private Boolean internalExternal;

	@Column(name = "tender_offer_item_id")
	private Integer tenderOfferItemId;

	@Column(name = "truck_capacity")
	private Integer truckCapacity;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "currency_code", referencedColumnName = "currency_code", insertable = false, updatable = false)
	private Currency currency;

	public TransportationCost() {
	}

	/**
	 * GETTERS AND SETTERS
	 */
	public Integer getTransportationCostId() {
		return this.transportationCostId;
	}

	public void setTransportationCostId(Integer transportationCostId) {
		this.transportationCostId = transportationCostId;
	}

	public String getCurrencyCode() {
		return this.currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Boolean getInternalExternal() {
		return this.internalExternal;
	}

	public void setInternalExternal(Boolean internalExternal) {
		this.internalExternal = internalExternal;
	}

	public Integer getTenderOfferItemId() {
		return this.tenderOfferItemId;
	}

	public void setTenderOfferItemId(Integer tenderOfferItemId) {
		this.tenderOfferItemId = tenderOfferItemId;
	}

	public Integer getTruckCapacity() {
		return this.truckCapacity;
	}

	public void setTruckCapacity(Integer truckCapacity) {
		this.truckCapacity = truckCapacity;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public float getPipe_cost() {
		return pipe_cost;
	}

	public void setPipe_cost(float pipe_cost) {
		this.pipe_cost = pipe_cost;
	}

	public float getWeightUnitCost() {
		return weightUnitCost;
	}

	public void setWeightUnitCost(float weightUnitCost) {
		this.weightUnitCost = weightUnitCost;
	}

}