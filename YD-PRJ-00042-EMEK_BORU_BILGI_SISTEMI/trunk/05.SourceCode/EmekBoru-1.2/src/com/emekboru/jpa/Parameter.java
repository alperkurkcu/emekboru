package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "parameter")
public class Parameter implements Serializable {

	private static final long serialVersionUID = -1995197672329027696L;

	@Id
	@Column(name = "parameter_id")
	private Integer parameterId;

	@Column(name = "name")
	private String name;

	@Column(name = "short_desc")
	private String shortDesc;

	public Parameter() {

	}

	/**
	 * GETTERS AND SETTERS
	 */
	public Integer getParameterId() {
		return parameterId;
	}

	public void setParameterId(Integer parameterId) {
		this.parameterId = parameterId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
