package com.emekboru.jpa.kaplamatestspecs;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Order;

/**
 * The persistent class for the kaplama_test_spec_yuzey_kontrol_test database
 * table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "KTSYKT.silmeKontrolu", query = "SELECT r.id FROM KaplamaTestSpecYuzeyKontrolTest r WHERE r.testtype = :prmTestType and r.order.orderId=:prmOrderId"),
		@NamedQuery(name = "KTSYKT.seciliYuzeyKontrolTestTanim", query = "SELECT r FROM KaplamaTestSpecYuzeyKontrolTest r WHERE r.testtype = :prmTestType and r.order.orderId=:prmOrderId") })
@Table(name = "kaplama_test_spec_yuzey_kontrol_test")
public class KaplamaTestSpecYuzeyKontrolTest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "KAPLAMA_TEST_SPEC_YUZEY_KONTROL_TEST_ID_GENERATOR", sequenceName = "KAPLAMA_TEST_SPEC_YUZEY_KONTROL_TEST_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "KAPLAMA_TEST_SPEC_YUZEY_KONTROL_TEST_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(nullable = false, length = 255)
	private String aciklama;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	// @Column(name = "order_id")
	// private Integer orderId;

	@JoinColumn(name = "order_id", referencedColumnName = "order_id", insertable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private Order order;

	private Integer testtype;

	@Column(length = 255)
	private String yuzeyprofili;

	@Column(length = 255)
	private String yuzeytemizligi;

	public KaplamaTestSpecYuzeyKontrolTest() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Integer getTesttype() {
		return this.testtype;
	}

	public void setTesttype(Integer testtype) {
		this.testtype = testtype;
	}

	public String getYuzeyprofili() {
		return this.yuzeyprofili;
	}

	public void setYuzeyprofili(String yuzeyprofili) {
		this.yuzeyprofili = yuzeyprofili;
	}

	public String getYuzeytemizligi() {
		return this.yuzeytemizligi;
	}

	public void setYuzeytemizligi(String yuzeytemizligi) {
		this.yuzeytemizligi = yuzeytemizligi;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

}