package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.primefaces.model.MenuModel;

/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "SystemUser.findAll", query = "select o from SystemUser o"),
		@NamedQuery(name = "SystemUser.findByKullaniciAdiSifre", query = "select o from SystemUser o where o.username=:username and o.password=:password"),
		@NamedQuery(name = "SystemUser.findByUserId", query = "select o from SystemUser o where o.id=:prmSystemUserId"),
		@NamedQuery(name = "SystemUser.findByActivePassive", query = "select o from SystemUser o where o.isActive=:prmActivePasive") })
@Table(name = "system_user")
public class SystemUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String username;

	private String password;

	@Column(name = "admin")
	private boolean admin;

	@Column(name = "is_active")
	private boolean isActive;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "employee_id", referencedColumnName = "employee_id")
	private Employee employee;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_type_id", referencedColumnName = "user_type_id")
	private UserType userType;

	@SequenceGenerator(name = "SYSTEM_USER_ID_GENERATOR", sequenceName = "SYSTEM_USER_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SYSTEM_USER_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Transient
	private MenuModel userMenu;

	@Override
	public boolean equals(Object o) {
		return this.getId() == ((SystemUser) o).getId();
	}

	public SystemUser() {

		userType = new UserType();
		employee = new Employee();
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public MenuModel getUserMenu() {
		return userMenu;
	}

	public void setUserMenu(MenuModel userMenu) {
		this.userMenu = userMenu;
	}

}