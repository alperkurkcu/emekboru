package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class OrderOffersPK implements Serializable{

	private static final long serialVersionUID = -4401459592019066718L;

	@Column(name="material_offer_id", insertable = false, updatable = false)
	private int materialOfferId;

	@Column(name="material_ordered_id", insertable = false, updatable = false)
	private int materialOrderedId;

	public OrderOffersPK()
	{
		
	}
	public int getMaterialOfferId() {
		return materialOfferId;
	}

	public void setMaterialOfferId(int materialOfferId) {
		this.materialOfferId = materialOfferId;
	}

	public int getMaterialOrderedId() {
		return materialOrderedId;
	}

	public void setMaterialOrderedId(int materialOrderedId) {
		this.materialOrderedId = materialOrderedId;
	}
}
