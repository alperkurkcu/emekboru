package com.emekboru.jpa.istakipformu;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the
 * is_takip_formu_polietilen_kaplama_bitince_islemler database table.
 * 
 */
@Entity
@Table(name = "is_takip_formu_polietilen_kaplama_bitince_islemler")
public class IsTakipFormuPolietilenKaplamaBitinceIslemler implements
		Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IS_TAKIP_FORMU_POLIETILEN_KAPLAMA_BITINCE_ISLEMLER_ID_GENERATOR", sequenceName = "IS_TAKIP_FORMU_POLIETILEN_KAPLAMA_BITINCE_ISLEMLER_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IS_TAKIP_FORMU_POLIETILEN_KAPLAMA_BITINCE_ISLEMLER_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "gerceklesen_sarfiyat_pe")
	private Integer gerceklesenSarfiyatPe;

	@Column(name = "gerceklesen_sarfiyat_tep")
	private Integer gerceklesenSarfiyatTep;

	@Column(name = "gerceklesen_sarfiyat_yp")
	private Integer gerceklesenSarfiyatYp;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "hedeflenen_sarfiyat_pe")
	private Integer hedeflenenSarfiyatPe;

	@Column(name = "hedeflenen_sarfiyat_tep")
	private Integer hedeflenenSarfiyatTep;

	@Column(name = "hedeflenen_sarfiyat_yp")
	private Integer hedeflenenSarfiyatYp;

	// @Column(name = "is_takip_form_id", nullable = false)
	// private Integer isTakipFormId;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "is_takip_form_id", referencedColumnName = "ID", insertable = false, updatable = false)
	private IsTakipFormuPolietilenKaplama isTakipFormuPolietilenKaplama;

	public IsTakipFormuPolietilenKaplamaBitinceIslemler() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getGerceklesenSarfiyatPe() {
		return this.gerceklesenSarfiyatPe;
	}

	public void setGerceklesenSarfiyatPe(Integer gerceklesenSarfiyatPe) {
		this.gerceklesenSarfiyatPe = gerceklesenSarfiyatPe;
	}

	public Integer getGerceklesenSarfiyatTep() {
		return this.gerceklesenSarfiyatTep;
	}

	public void setGerceklesenSarfiyatTep(Integer gerceklesenSarfiyatTep) {
		this.gerceklesenSarfiyatTep = gerceklesenSarfiyatTep;
	}

	public Integer getGerceklesenSarfiyatYp() {
		return this.gerceklesenSarfiyatYp;
	}

	public void setGerceklesenSarfiyatYp(Integer gerceklesenSarfiyatYp) {
		this.gerceklesenSarfiyatYp = gerceklesenSarfiyatYp;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getHedeflenenSarfiyatPe() {
		return this.hedeflenenSarfiyatPe;
	}

	public void setHedeflenenSarfiyatPe(Integer hedeflenenSarfiyatPe) {
		this.hedeflenenSarfiyatPe = hedeflenenSarfiyatPe;
	}

	public Integer getHedeflenenSarfiyatTep() {
		return this.hedeflenenSarfiyatTep;
	}

	public void setHedeflenenSarfiyatTep(Integer hedeflenenSarfiyatTep) {
		this.hedeflenenSarfiyatTep = hedeflenenSarfiyatTep;
	}

	public Integer getHedeflenenSarfiyatYp() {
		return this.hedeflenenSarfiyatYp;
	}

	public void setHedeflenenSarfiyatYp(Integer hedeflenenSarfiyatYp) {
		this.hedeflenenSarfiyatYp = hedeflenenSarfiyatYp;
	}

	public IsTakipFormuPolietilenKaplama getIsTakipFormuPolietilenKaplama() {
		return isTakipFormuPolietilenKaplama;
	}

	public void setIsTakipFormuPolietilenKaplama(
			IsTakipFormuPolietilenKaplama isTakipFormuPolietilenKaplama) {
		this.isTakipFormuPolietilenKaplama = isTakipFormuPolietilenKaplama;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

}