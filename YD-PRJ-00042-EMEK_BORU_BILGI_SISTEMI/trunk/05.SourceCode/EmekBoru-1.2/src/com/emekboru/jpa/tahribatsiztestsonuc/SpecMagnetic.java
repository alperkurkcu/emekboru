package com.emekboru.jpa.tahribatsiztestsonuc;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the spec_magnetic database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "SpecMagnetic.findAll", query = "SELECT r FROM SpecMagnetic r WHERE r.pipe.pipeId=:prmPipeId") })
@Table(name = "spec_magnetic")
public class SpecMagnetic implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SPEC_MAGNETIC_ID_GENERATOR", sequenceName = "SPEC_MAGNETIC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SPEC_MAGNETIC_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "aciklama")
	private String aciklama;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "isil_islem_oncesi")
	private Boolean isilIslemOncesi;

	@Column(name = "isil_islem_sonrasi")
	private Boolean isilIslemSonrasi;

	@Column(name = "isil_islem_yok")
	private Boolean isilIslemYok;

	@Column(name = "yuzey_diger")
	private Boolean yuzeyDiger;

	@Column(name = "yuzey_islenmis")
	private Boolean yuzeyIslenmis;

	@Column(name = "yuzey_kaynak")
	private Boolean yuzeyKaynak;

	@Column(name = "yuzey_kumlanmis")
	private Boolean yuzeyKumlanmis;

	@Column(name = "yuzey_sicaligi_arada")
	private Boolean yuzeySicaligiArada;

	@Column(name = "yuzey_sicaligi_buyuk")
	private Boolean yuzeySicaligiBuyuk;

	@Column(name = "yuzey_sicaligi_kucuk")
	private Boolean yuzeySicaligiKucuk;

	@Column(name = "yuzey_taslanmis")
	private Boolean yuzeyTaslanmis;

	// @Column(name = "pipe_id", nullable = false)
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	public SpecMagnetic() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Boolean getIsilIslemOncesi() {
		return this.isilIslemOncesi;
	}

	public void setIsilIslemOncesi(Boolean isilIslemOncesi) {
		this.isilIslemOncesi = isilIslemOncesi;
	}

	public Boolean getIsilIslemSonrasi() {
		return this.isilIslemSonrasi;
	}

	public void setIsilIslemSonrasi(Boolean isilIslemSonrasi) {
		this.isilIslemSonrasi = isilIslemSonrasi;
	}

	public Boolean getIsilIslemYok() {
		return this.isilIslemYok;
	}

	public void setIsilIslemYok(Boolean isilIslemYok) {
		this.isilIslemYok = isilIslemYok;
	}

	public Boolean getYuzeyDiger() {
		return this.yuzeyDiger;
	}

	public void setYuzeyDiger(Boolean yuzeyDiger) {
		this.yuzeyDiger = yuzeyDiger;
	}

	public Boolean getYuzeyIslenmis() {
		return this.yuzeyIslenmis;
	}

	public void setYuzeyIslenmis(Boolean yuzeyIslenmis) {
		this.yuzeyIslenmis = yuzeyIslenmis;
	}

	public Boolean getYuzeyKaynak() {
		return this.yuzeyKaynak;
	}

	public void setYuzeyKaynak(Boolean yuzeyKaynak) {
		this.yuzeyKaynak = yuzeyKaynak;
	}

	public Boolean getYuzeyKumlanmis() {
		return this.yuzeyKumlanmis;
	}

	public void setYuzeyKumlanmis(Boolean yuzeyKumlanmis) {
		this.yuzeyKumlanmis = yuzeyKumlanmis;
	}

	public Boolean getYuzeySicaligiArada() {
		return this.yuzeySicaligiArada;
	}

	public void setYuzeySicaligiArada(Boolean yuzeySicaligiArada) {
		this.yuzeySicaligiArada = yuzeySicaligiArada;
	}

	public Boolean getYuzeySicaligiBuyuk() {
		return this.yuzeySicaligiBuyuk;
	}

	public void setYuzeySicaligiBuyuk(Boolean yuzeySicaligiBuyuk) {
		this.yuzeySicaligiBuyuk = yuzeySicaligiBuyuk;
	}

	public Boolean getYuzeySicaligiKucuk() {
		return this.yuzeySicaligiKucuk;
	}

	public void setYuzeySicaligiKucuk(Boolean yuzeySicaligiKucuk) {
		this.yuzeySicaligiKucuk = yuzeySicaligiKucuk;
	}

	public Boolean getYuzeyTaslanmis() {
		return this.yuzeyTaslanmis;
	}

	public void setYuzeyTaslanmis(Boolean yuzeyTaslanmis) {
		this.yuzeyTaslanmis = yuzeyTaslanmis;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the eklemeZamani
	 */
	public Date getEklemeZamani() {
		return eklemeZamani;
	}

	/**
	 * @param eklemeZamani
	 *            the eklemeZamani to set
	 */
	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncellemeZamani
	 */
	public Date getGuncellemeZamani() {
		return guncellemeZamani;
	}

	/**
	 * @param guncellemeZamani
	 *            the guncellemeZamani to set
	 */
	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

}