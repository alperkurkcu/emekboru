package com.emekboru.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="maintenance_plan")
public class MaintenancePlan implements Serializable{

	private static final long serialVersionUID = -6503068819857311523L;

	@Id
	@SequenceGenerator(name = "MAINTENANCE_PLAN_MAINTENANCEPLANID_GENERATOR", sequenceName = "MAINTENANCE_PLAN_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MAINTENANCE_PLAN_MAINTENANCEPLANID_GENERATOR")
	@Column(name="maintenance_plan_id")
	private Integer maintenancePlanId;
	
	@Column(name="short_desc")
	private String shortDesc;
	
	@Column(name="year")
	private Integer year;
	
	@Temporal(TemporalType.DATE)
	@Column(name="planned_execution_day")
	private Date plannedExecutionDay;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="machine_part_id", referencedColumnName="machine_part_id")
	private MachinePart machinePart;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="maintenance_plan_type_id", referencedColumnName="maintenance_plan_type_id")
	private MaintenancePlanType maintenancePlanType;
	
	
	public MaintenancePlan(){
		
	}

	/**
	 * GETTERS AND SETTERS
	 */
	public Integer getMaintenancePlanId() {
		return maintenancePlanId;
	}


	public void setMaintenancePlanId(Integer maintenancePlanId) {
		this.maintenancePlanId = maintenancePlanId;
	}


	public String getShortDesc() {
		return shortDesc;
	}


	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}


	public Date getPlannedExecutionDay() {
		return plannedExecutionDay;
	}


	public void setPlannedExecutionDay(Date plannedExecutionDay) {
		this.plannedExecutionDay = plannedExecutionDay;
	}


	public MachinePart getMachinePart() {
		return machinePart;
	}


	public void setMachinePart(MachinePart machinePart) {
		this.machinePart = machinePart;
	}


	public MaintenancePlanType getMaintenancePlanType() {
		return maintenancePlanType;
	}


	public void setMaintenancePlanType(MaintenancePlanType maintenancePlanType) {
		this.maintenancePlanType = maintenancePlanType;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
