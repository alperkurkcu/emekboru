package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.isolation.IsolationTestDefinition;

/**
 * The persistent class for the test_kaplama_delici_uc_direnc_sonuc database
 * table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaDeliciUcDirencSonuc.findAll", query = "SELECT r FROM TestKaplamaDeliciUcDirencSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_kaplama_delici_uc_direnc_sonuc")
public class TestKaplamaDeliciUcDirencSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KAPLAMA_DELICI_UC_DIRENC_SONUC_ID_GENERATOR", sequenceName = "TEST_KAPLAMA_DELICI_UC_DIRENC_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KAPLAMA_DELICI_UC_DIRENC_SONUC_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Column(name = "aciklama", length = 255)
	private String aciklama;

	@Column(name = "art_ortalama")
	private BigDecimal artOrtalama;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name = "global_id")
	// private Integer globalId;

	@Column(name = "holiday", nullable = false)
	private Boolean holiday;

	// @Column(name = "isolation_test_id")
	// private Integer isolationTestId;

	@Column(name = "permotasyon_der_a")
	private BigDecimal permotasyonDerA;

	@Column(name = "permotasyon_der_b")
	private BigDecimal permotasyonDerB;

	@Column(name = "permotasyon_der_c")
	private BigDecimal permotasyonDerC;

	@Column(name = "pinhole", nullable = false)
	private Boolean pinhole;

	// @Column(name = "pipe_id")
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	private String sonuc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_bas_tarihi")
	private Date testBasTarihi;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_bit_tarihi")
	private Date testBitTarihi;

	@Column(name = "test_num_boyutlari")
	private String testNumBoyutlari;

	@Column(name = "test_sicakligi")
	private Integer testSicakligi;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi")
	private Date testTarihi;

	@Column(name = "standard_penetrasyon_degeri")
	private BigDecimal standartPenetrasyonDegeri;

	public TestKaplamaDeliciUcDirencSonuc() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Boolean getHoliday() {
		return this.holiday;
	}

	public void setHoliday(Boolean holiday) {
		this.holiday = holiday;
	}

	public Boolean getPinhole() {
		return this.pinhole;
	}

	public void setPinhole(Boolean pinhole) {
		this.pinhole = pinhole;
	}

	public Date getTestBasTarihi() {
		return this.testBasTarihi;
	}

	public void setTestBasTarihi(Date testBasTarihi) {
		this.testBasTarihi = testBasTarihi;
	}

	public Date getTestBitTarihi() {
		return this.testBitTarihi;
	}

	public void setTestBitTarihi(Date testBitTarihi) {
		this.testBitTarihi = testBitTarihi;
	}

	public String getTestNumBoyutlari() {
		return this.testNumBoyutlari;
	}

	public void setTestNumBoyutlari(String testNumBoyutlari) {
		this.testNumBoyutlari = testNumBoyutlari;
	}

	public Integer getTestSicakligi() {
		return this.testSicakligi;
	}

	public void setTestSicakligi(Integer testSicakligi) {
		this.testSicakligi = testSicakligi;
	}

	/**
	 * @return the bagliGlobalId
	 */
	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}

	/**
	 * @param bagliGlobalId
	 *            the bagliGlobalId to set
	 */
	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	/**
	 * @return the bagliTestId
	 */
	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the eklemeZamani
	 */
	public Date getEklemeZamani() {
		return eklemeZamani;
	}

	/**
	 * @param eklemeZamani
	 *            the eklemeZamani to set
	 */
	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncellemeZamani
	 */
	public Date getGuncellemeZamani() {
		return guncellemeZamani;
	}

	/**
	 * @param guncellemeZamani
	 *            the guncellemeZamani to set
	 */
	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the testTarihi
	 */
	public Date getTestTarihi() {
		return testTarihi;
	}

	/**
	 * @param testTarihi
	 *            the testTarihi to set
	 */
	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	/**
	 * @return the artOrtalama
	 */
	public BigDecimal getArtOrtalama() {
		return artOrtalama;
	}

	/**
	 * @param artOrtalama
	 *            the artOrtalama to set
	 */
	public void setArtOrtalama(BigDecimal artOrtalama) {
		this.artOrtalama = artOrtalama;
	}

	/**
	 * @return the permotasyonDerA
	 */
	public BigDecimal getPermotasyonDerA() {
		return permotasyonDerA;
	}

	/**
	 * @param permotasyonDerA
	 *            the permotasyonDerA to set
	 */
	public void setPermotasyonDerA(BigDecimal permotasyonDerA) {
		this.permotasyonDerA = permotasyonDerA;
	}

	/**
	 * @return the permotasyonDerB
	 */
	public BigDecimal getPermotasyonDerB() {
		return permotasyonDerB;
	}

	/**
	 * @param permotasyonDerB
	 *            the permotasyonDerB to set
	 */
	public void setPermotasyonDerB(BigDecimal permotasyonDerB) {
		this.permotasyonDerB = permotasyonDerB;
	}

	/**
	 * @return the permotasyonDerC
	 */
	public BigDecimal getPermotasyonDerC() {
		return permotasyonDerC;
	}

	/**
	 * @param permotasyonDerC
	 *            the permotasyonDerC to set
	 */
	public void setPermotasyonDerC(BigDecimal permotasyonDerC) {
		this.permotasyonDerC = permotasyonDerC;
	}

	/**
	 * @return the standartPenetrasyonDegeri
	 */
	public BigDecimal getStandartPenetrasyonDegeri() {
		return standartPenetrasyonDegeri;
	}

	/**
	 * @param standartPenetrasyonDegeri
	 *            the standartPenetrasyonDegeri to set
	 */
	public void setStandartPenetrasyonDegeri(
			BigDecimal standartPenetrasyonDegeri) {
		this.standartPenetrasyonDegeri = standartPenetrasyonDegeri;
	}

	/**
	 * @return the sonuc
	 */
	public String getSonuc() {
		return sonuc;
	}

	/**
	 * @param sonuc
	 *            the sonuc to set
	 */
	public void setSonuc(String sonuc) {
		this.sonuc = sonuc;
	}

}