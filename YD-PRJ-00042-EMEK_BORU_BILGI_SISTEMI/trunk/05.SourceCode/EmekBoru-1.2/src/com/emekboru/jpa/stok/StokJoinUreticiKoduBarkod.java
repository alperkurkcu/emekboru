package com.emekboru.jpa.stok;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the stok_join_uretici_kodu_barkod database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "StokJoinUreticiKoduBarkod.findAllByUreticiJoinId", query = "SELECT c FROM StokJoinUreticiKoduBarkod c where c.stokJoinMarkaUreticiKodu.joinId=:prmUreticiJoinId order by c.barkod"),
		@NamedQuery(name = "StokJoinUreticiKoduBarkod.findAllByUreticiJoinIdMarkaId", query = "SELECT c FROM StokJoinUreticiKoduBarkod c where c.stokTanimlarMarka.id=:prmMarkaId and c.stokJoinMarkaUreticiKodu.joinId=:prmUrunJoinId order by c.barkod"),
		@NamedQuery(name = "StokJoinUreticiKoduBarkod.findAllByUrunId", query = "SELECT c FROM StokJoinUreticiKoduBarkod c where c.stokJoinMarkaUreticiKodu.stokUrunKartlari.id=:prmUrunId") })
@Table(name = "stok_join_uretici_kodu_barkod")
public class StokJoinUreticiKoduBarkod implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "STOK_JOIN_URETICI_KODU_BARKOD_JOINID_GENERATOR", sequenceName = "STOK_JOIN_URETICI_KODU_BARKOD_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STOK_JOIN_URETICI_KODU_BARKOD_JOINID_GENERATOR")
	@Column(name = "join_id", unique = true, nullable = false)
	private Integer joinId;

	@Column(nullable = false, length = 32)
	private String barkod;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	// @Column(name = "marka_id", nullable = false)
	// private Integer markaId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "marka_id", referencedColumnName = "ID", insertable = false)
	private StokTanimlarMarka stokTanimlarMarka;

	@Basic
	@Column(name = "uretici_join_id", nullable = false, insertable = false, updatable = false)
	private Integer ureticiJoinId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "uretici_join_id", referencedColumnName = "join_id", insertable = false)
	private StokJoinMarkaUreticiKodu stokJoinMarkaUreticiKodu;

	public StokJoinUreticiKoduBarkod() {

		stokJoinMarkaUreticiKodu = new StokJoinMarkaUreticiKodu();
		stokTanimlarMarka = new StokTanimlarMarka();
	}

	public Integer getJoinId() {
		return this.joinId;
	}

	public void setJoinId(Integer joinId) {
		this.joinId = joinId;
	}

	public String getBarkod() {
		return this.barkod;
	}

	public void setBarkod(String barkod) {
		this.barkod = barkod;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public StokJoinMarkaUreticiKodu getStokJoinMarkaUreticiKodu() {
		return stokJoinMarkaUreticiKodu;
	}

	public void setStokJoinMarkaUreticiKodu(
			StokJoinMarkaUreticiKodu stokJoinMarkaUreticiKodu) {
		this.stokJoinMarkaUreticiKodu = stokJoinMarkaUreticiKodu;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public StokTanimlarMarka getStokTanimlarMarka() {
		return stokTanimlarMarka;
	}

	public void setStokTanimlarMarka(StokTanimlarMarka stokTanimlarMarka) {
		this.stokTanimlarMarka = stokTanimlarMarka;
	}

}