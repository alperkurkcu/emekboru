package com.emekboru.jpa.employee;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.emekboru.jpa.Employee;

/**
 * The persistent class for the employee_discipline database table.
 * 
 */
@Entity
@Table(name = "employee_discipline")
@NamedQuery(name = "EmployeeDiscipline.findAll", query = "SELECT e FROM EmployeeDiscipline e")
public class EmployeeDiscipline implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "EMPLOYEE_DISCIPLINE_ID_GENERATOR", sequenceName = "EMPLOYEE_DISCIPLINE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMPLOYEE_DISCIPLINE_ID_GENERATOR")
	@Column(name = "id")
	private Integer id;

	@Temporal(TemporalType.DATE)
	@Column(name = "date")
	private Date date;

	@Column(name = "discipline_penalty")
	private String disciplinePenalty;

	@Column(name = "ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@ManyToOne
	@JoinColumn(name = "employee_id", insertable = false)
	private Employee employee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "penalty_point")
	private Integer penaltyPoint;

	@Column(name = "result")
	private String result;

	@Transient
	private Boolean isLastYearsRecord;

	public EmployeeDiscipline() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDisciplinePenalty() {
		return this.disciplinePenalty;
	}

	public void setDisciplinePenalty(String disciplinePenalty) {
		this.disciplinePenalty = disciplinePenalty;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Integer getPenaltyPoint() {
		return this.penaltyPoint;
	}

	public void setPenaltyPoint(Integer penaltyPoint) {
		this.penaltyPoint = penaltyPoint;
	}

	public String getResult() {
		return this.result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Boolean getIsLastYearsRecord() {

		Calendar now = Calendar.getInstance();
		Calendar penaltyDate = Calendar.getInstance();

		penaltyDate.setTime(date);

		int penaltyYear = penaltyDate.get(Calendar.YEAR);
		int currentYear = now.get(Calendar.YEAR);

		isLastYearsRecord = penaltyYear == currentYear;

		return isLastYearsRecord;
	}

	public void setIsLastYearsRecord(Boolean isLastYearsRecord) {
		this.isLastYearsRecord = isLastYearsRecord;
	}

}