package com.emekboru.jpa.kalibrasyon;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the kalibrasyon_manuel_ut database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "KalibrasyonManuelUt.findAll", query = "SELECT r FROM KalibrasyonManuelUt r order by r.kalibrasyonZamani DESC") })
@Table(name = "kalibrasyon_manuel_ut")
public class KalibrasyonManuelUt implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "KALIBRASYON_MANUEL_UT_ID_GENERATOR", sequenceName = "KALIBRASYON_MANUEL_UT_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "KALIBRASYON_MANUEL_UT_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "aciklama")
	private String aciklama;

	@Column(name = "alfa_bir")
	private String alfaBir;

	@Column(name = "alfa_iki")
	private String alfaIki;

	@Column(name = "alfa_uc")
	private String alfaUc;

	@Column(name = "degerlendirme_metodu_bir")
	private String degerlendirmeMetoduBir;

	@Column(name = "degerlendirme_metodu_iki")
	private String degerlendirmeMetoduIki;

	@Column(name = "degerlendirme_metodu_uc")
	private String degerlendirmeMetoduUc;

	@Column(name = "delta_v_bir")
	private String deltaVBir;

	@Column(name = "delta_v_iki")
	private String deltaVIki;

	@Column(name = "delta_v_uc")
	private String deltaVUc;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "f_bir")
	private String fBir;

	@Column(name = "f_iki")
	private String fIki;

	@Column(name = "f_uc")
	private String fUc;

	@Column(name = "k_bir")
	private String kBir;

	@Column(name = "k_iki")
	private String kIki;

	@Column(name = "k_uc")
	private String kUc;

	@Column(name = "kalibrasyon_yansiticisi_bir")
	private String kalibrasyonYansiticisiBir;

	@Column(name = "kalibrasyon_yansiticisi_iki")
	private String kalibrasyonYansiticisiIki;

	@Column(name = "kalibrasyon_yansiticisi_uc")
	private String kalibrasyonYansiticisiUc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "kalibrasyon_zamani")
	private Date kalibrasyonZamani;

	@Column(name = "kalibrasyonu_yapan")
	private String kalibrasyonuYapan;

	@Column(name = "kayit_siniri_bir")
	private String kayitSiniriBir;

	@Column(name = "kayit_siniri_iki")
	private String kayitSiniriIki;

	@Column(name = "kayit_siniri_uc")
	private String kayitSiniriUc;

	@Column(name = "sb_bir")
	private String sbBir;

	@Column(name = "sb_iki")
	private String sbIki;

	@Column(name = "sb_uc")
	private String sbUc;

	@Column(name = "type_bir")
	private String typeBir;

	@Column(name = "type_iki")
	private String typeIki;

	@Column(name = "type_uc")
	private String typeUc;

	@Column(name = "vj_bir")
	private String vjBir;

	@Column(name = "vj_iki")
	private String vjIki;

	@Column(name = "vj_uc")
	private String vjUc;

	@Column(name = "vr_bir")
	private String vrBir;

	@Column(name = "vr_iki")
	private String vrIki;

	@Column(name = "vr_uc")
	private String vrUc;

	@Column(name = "x_bir")
	private String xBir;

	@Column(name = "x_iki")
	private String xIki;

	@Column(name = "x_uc")
	private String xUc;

	@Column(name = "kalibrasyon_blogu_bir")
	private String kalibrasyonBloguBir;

	@Column(name = "kalibrasyon_blogu_iki")
	private String kalibrasyonBloguIki;

	@Column(name = "kalibrasyon_blogu_uc")
	private String kalibrasyonBloguUc;

	@Column(name = "test_teknigi")
	private String testTeknigi;

	@Column(name = "ekipman_standard")
	private String ekipmanStandard;

	@Column(name = "cihaz_adi")
	private String cihazAdi;

	@Column(name = "cihaz_no")
	private String cihazNo;

	public KalibrasyonManuelUt() {
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public String getAlfaBir() {
		return this.alfaBir;
	}

	public void setAlfaBir(String alfaBir) {
		this.alfaBir = alfaBir;
	}

	public String getAlfaIki() {
		return this.alfaIki;
	}

	public void setAlfaIki(String alfaIki) {
		this.alfaIki = alfaIki;
	}

	public String getAlfaUc() {
		return this.alfaUc;
	}

	public void setAlfaUc(String alfaUc) {
		this.alfaUc = alfaUc;
	}

	public String getDegerlendirmeMetoduBir() {
		return this.degerlendirmeMetoduBir;
	}

	public void setDegerlendirmeMetoduBir(String degerlendirmeMetoduBir) {
		this.degerlendirmeMetoduBir = degerlendirmeMetoduBir;
	}

	public String getDegerlendirmeMetoduIki() {
		return this.degerlendirmeMetoduIki;
	}

	public void setDegerlendirmeMetoduIki(String degerlendirmeMetoduIki) {
		this.degerlendirmeMetoduIki = degerlendirmeMetoduIki;
	}

	public String getDegerlendirmeMetoduUc() {
		return this.degerlendirmeMetoduUc;
	}

	public void setDegerlendirmeMetoduUc(String degerlendirmeMetoduUc) {
		this.degerlendirmeMetoduUc = degerlendirmeMetoduUc;
	}

	public String getDeltaVBir() {
		return this.deltaVBir;
	}

	public void setDeltaVBir(String deltaVBir) {
		this.deltaVBir = deltaVBir;
	}

	public String getDeltaVIki() {
		return this.deltaVIki;
	}

	public void setDeltaVIki(String deltaVIki) {
		this.deltaVIki = deltaVIki;
	}

	public String getDeltaVUc() {
		return this.deltaVUc;
	}

	public void setDeltaVUc(String deltaVUc) {
		this.deltaVUc = deltaVUc;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public String getFBir() {
		return this.fBir;
	}

	public void setFBir(String fBir) {
		this.fBir = fBir;
	}

	public String getFIki() {
		return this.fIki;
	}

	public void setFIki(String fIki) {
		this.fIki = fIki;
	}

	public String getFUc() {
		return this.fUc;
	}

	public void setFUc(String fUc) {
		this.fUc = fUc;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getKBir() {
		return this.kBir;
	}

	public void setKBir(String kBir) {
		this.kBir = kBir;
	}

	public String getKIki() {
		return this.kIki;
	}

	public void setKIki(String kIki) {
		this.kIki = kIki;
	}

	public String getKUc() {
		return this.kUc;
	}

	public void setKUc(String kUc) {
		this.kUc = kUc;
	}

	public String getKalibrasyonYansiticisiBir() {
		return this.kalibrasyonYansiticisiBir;
	}

	public void setKalibrasyonYansiticisiBir(String kalibrasyonYansiticisiBir) {
		this.kalibrasyonYansiticisiBir = kalibrasyonYansiticisiBir;
	}

	public String getKalibrasyonYansiticisiIki() {
		return this.kalibrasyonYansiticisiIki;
	}

	public void setKalibrasyonYansiticisiIki(String kalibrasyonYansiticisiIki) {
		this.kalibrasyonYansiticisiIki = kalibrasyonYansiticisiIki;
	}

	public String getKalibrasyonYansiticisiUc() {
		return this.kalibrasyonYansiticisiUc;
	}

	public void setKalibrasyonYansiticisiUc(String kalibrasyonYansiticisiUc) {
		this.kalibrasyonYansiticisiUc = kalibrasyonYansiticisiUc;
	}

	public String getKalibrasyonuYapan() {
		return this.kalibrasyonuYapan;
	}

	public void setKalibrasyonuYapan(String kalibrasyonuYapan) {
		this.kalibrasyonuYapan = kalibrasyonuYapan;
	}

	public String getKayitSiniriBir() {
		return this.kayitSiniriBir;
	}

	public void setKayitSiniriBir(String kayitSiniriBir) {
		this.kayitSiniriBir = kayitSiniriBir;
	}

	public String getKayitSiniriIki() {
		return this.kayitSiniriIki;
	}

	public void setKayitSiniriIki(String kayitSiniriIki) {
		this.kayitSiniriIki = kayitSiniriIki;
	}

	public String getKayitSiniriUc() {
		return this.kayitSiniriUc;
	}

	public void setKayitSiniriUc(String kayitSiniriUc) {
		this.kayitSiniriUc = kayitSiniriUc;
	}

	public String getSbBir() {
		return this.sbBir;
	}

	public void setSbBir(String sbBir) {
		this.sbBir = sbBir;
	}

	public String getSbIki() {
		return this.sbIki;
	}

	public void setSbIki(String sbIki) {
		this.sbIki = sbIki;
	}

	public String getSbUc() {
		return this.sbUc;
	}

	public void setSbUc(String sbUc) {
		this.sbUc = sbUc;
	}

	public String getTypeBir() {
		return this.typeBir;
	}

	public void setTypeBir(String typeBir) {
		this.typeBir = typeBir;
	}

	public String getTypeIki() {
		return this.typeIki;
	}

	public void setTypeIki(String typeIki) {
		this.typeIki = typeIki;
	}

	public String getTypeUc() {
		return this.typeUc;
	}

	public void setTypeUc(String typeUc) {
		this.typeUc = typeUc;
	}

	public String getVjBir() {
		return this.vjBir;
	}

	public void setVjBir(String vjBir) {
		this.vjBir = vjBir;
	}

	public String getVjIki() {
		return this.vjIki;
	}

	public void setVjIki(String vjIki) {
		this.vjIki = vjIki;
	}

	public String getVjUc() {
		return this.vjUc;
	}

	public void setVjUc(String vjUc) {
		this.vjUc = vjUc;
	}

	public String getVrBir() {
		return this.vrBir;
	}

	public void setVrBir(String vrBir) {
		this.vrBir = vrBir;
	}

	public String getVrIki() {
		return this.vrIki;
	}

	public void setVrIki(String vrIki) {
		this.vrIki = vrIki;
	}

	public String getVrUc() {
		return this.vrUc;
	}

	public void setVrUc(String vrUc) {
		this.vrUc = vrUc;
	}

	public String getXBir() {
		return this.xBir;
	}

	public void setXBir(String xBir) {
		this.xBir = xBir;
	}

	public String getXIki() {
		return this.xIki;
	}

	public void setXIki(String xIki) {
		this.xIki = xIki;
	}

	public String getXUc() {
		return this.xUc;
	}

	public void setXUc(String xUc) {
		this.xUc = xUc;
	}

	/**
	 * @return the eklemeZamani
	 */
	public Date getEklemeZamani() {
		return eklemeZamani;
	}

	/**
	 * @param eklemeZamani
	 *            the eklemeZamani to set
	 */
	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncellemeZamani
	 */
	public Date getGuncellemeZamani() {
		return guncellemeZamani;
	}

	/**
	 * @param guncellemeZamani
	 *            the guncellemeZamani to set
	 */
	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the kalibrasyonZamani
	 */
	public Date getKalibrasyonZamani() {
		return kalibrasyonZamani;
	}

	/**
	 * @param kalibrasyonZamani
	 *            the kalibrasyonZamani to set
	 */
	public void setKalibrasyonZamani(Date kalibrasyonZamani) {
		this.kalibrasyonZamani = kalibrasyonZamani;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the kalibrasyonBloguBir
	 */
	public String getKalibrasyonBloguBir() {
		return kalibrasyonBloguBir;
	}

	/**
	 * @param kalibrasyonBloguBir
	 *            the kalibrasyonBloguBir to set
	 */
	public void setKalibrasyonBloguBir(String kalibrasyonBloguBir) {
		this.kalibrasyonBloguBir = kalibrasyonBloguBir;
	}

	/**
	 * @return the kalibrasyonBloguIki
	 */
	public String getKalibrasyonBloguIki() {
		return kalibrasyonBloguIki;
	}

	/**
	 * @param kalibrasyonBloguIki
	 *            the kalibrasyonBloguIki to set
	 */
	public void setKalibrasyonBloguIki(String kalibrasyonBloguIki) {
		this.kalibrasyonBloguIki = kalibrasyonBloguIki;
	}

	/**
	 * @return the kalibrasyonBloguUc
	 */
	public String getKalibrasyonBloguUc() {
		return kalibrasyonBloguUc;
	}

	/**
	 * @param kalibrasyonBloguUc
	 *            the kalibrasyonBloguUc to set
	 */
	public void setKalibrasyonBloguUc(String kalibrasyonBloguUc) {
		this.kalibrasyonBloguUc = kalibrasyonBloguUc;
	}

	/**
	 * @return the testTeknigi
	 */
	public String getTestTeknigi() {
		return testTeknigi;
	}

	/**
	 * @param testTeknigi
	 *            the testTeknigi to set
	 */
	public void setTestTeknigi(String testTeknigi) {
		this.testTeknigi = testTeknigi;
	}

	/**
	 * @return the ekipmanStandard
	 */
	public String getEkipmanStandard() {
		return ekipmanStandard;
	}

	/**
	 * @param ekipmanStandard
	 *            the ekipmanStandard to set
	 */
	public void setEkipmanStandard(String ekipmanStandard) {
		this.ekipmanStandard = ekipmanStandard;
	}

	/**
	 * @return the cihazAdi
	 */
	public String getCihazAdi() {
		return cihazAdi;
	}

	/**
	 * @param cihazAdi
	 *            the cihazAdi to set
	 */
	public void setCihazAdi(String cihazAdi) {
		this.cihazAdi = cihazAdi;
	}

	/**
	 * @return the cihazNo
	 */
	public String getCihazNo() {
		return cihazNo;
	}

	/**
	 * @param cihazNo
	 *            the cihazNo to set
	 */
	public void setCihazNo(String cihazNo) {
		this.cihazNo = cihazNo;
	}

}