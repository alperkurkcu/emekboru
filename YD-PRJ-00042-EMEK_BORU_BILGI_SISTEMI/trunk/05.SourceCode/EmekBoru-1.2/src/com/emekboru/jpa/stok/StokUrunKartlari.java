package com.emekboru.jpa.stok;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the stok_urun_kartlari database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "StokUrunKartlari.findAll", query = "SELECT r FROM StokUrunKartlari r order by r.urunKodu"),
		@NamedQuery(name = "StokUrunKartlari.findAllGirisCikis", query = "SELECT r, s FROM StokUrunKartlari r, StokGirisCikis s where r.id=s.stokUrunKartlari.id order by r.urunKodu"),
		@NamedQuery(name = "StokUrunKartlari.findAllBarkodlu", query = "SELECT DISTINCT ukb FROM StokJoinUreticiKoduBarkod ukb, StokUrunKartlari suk where ukb.stokJoinMarkaUreticiKodu.stokUrunKartlari.id=suk.id order by ukb.stokJoinMarkaUreticiKodu.stokUrunKartlari.urunKodu ASC"),
		@NamedQuery(name = "StokUrunKartlari.findAllBarkodluByUrunId", query = "SELECT DISTINCT ukb FROM StokJoinUreticiKoduBarkod ukb, StokJoinUrunMarka um, StokUrunKartlari suk where ukb.stokTanimlarMarka.id=um.stokTanimlarMarka.id and um.stokUrunKartlari.id=suk.id and ukb.stokJoinMarkaUreticiKodu.stokUrunKartlari.id=:prmUrunId") })
@Table(name = "stok_urun_kartlari")
public class StokUrunKartlari implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "STOK_URUN_KARTLARI_ID_GENERATOR", sequenceName = "STOK_URUN_KARTLARI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STOK_URUN_KARTLARI_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "barkod_kodu", nullable = false)
	private String barkodKodu;

	@Basic
	@Column(name = "birim_id", nullable = false, insertable = false, updatable = false)
	private Integer birimId;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "birim_id", referencedColumnName = "ID", insertable = false)
	private StokTanimlarBirim stokTanimlarBirim;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Column(name = "goz_no", nullable = false)
	private String gozNo;

	@Basic
	@Column(name = "grup_id", nullable = false, insertable = false, updatable = false)
	private Integer grupId;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "grup_id", referencedColumnName = "ID", insertable = false)
	private StokTanimlarUrunGrubu stokTanimlarUrunGrubu;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Basic
	@Column(name = "marka_id", nullable = false, insertable = false, updatable = false)
	private Integer markaId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "marka_id", referencedColumnName = "ID", insertable = false)
	private StokTanimlarMarka stokTanimlarMarka;

	@Column(name = "max_stok", nullable = false)
	private Integer maxStok;

	@Column(name = "min_stok", nullable = false)
	private Integer minStok;

	// @Column(name = "raf_id", nullable = false)
	// private Integer rafId;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "raf_id", referencedColumnName = "ID", insertable = false)
	private StokTanimlarRaf stokTanimlarRaf;

	@Column(name = "sarf_yeri_id", nullable = false)
	private Integer sarfYeriId;

	// @ManyToOne(fetch = FetchType.LAZY)
	// @JoinColumn(name = "sarf_yeri_id", referencedColumnName = "ID",
	// insertable = false)
	// private StokTanimlarSarfYeri stokTanimlarSarfYeri;

	@Basic
	@Column(name = "uretici_kodu", nullable = false, length = 32, insertable = false, updatable = false)
	private Integer ureticiKodu;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "uretici_kodu", referencedColumnName = "join_id", insertable = false)
	private StokJoinUreticiKoduBarkod stokJoinUreticiKoduBarkod;

	@OneToMany(mappedBy = "stokUrunKartlari", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<StokJoinMarkaUreticiKodu> stokJoinMarkaUreticiKodlar;

	@Column(name = "urun_adi", nullable = false, length = 32)
	private String urunAdi;

	@Column(name = "urun_kodu", nullable = false)
	private String urunKodu;

	@OneToMany(mappedBy = "stokUrunKartlari", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<StokJoinUrunMarka> stokJoinUrunMarkalar;

	// @OneToMany(mappedBy = "stokUrunKartlari", cascade = CascadeType.ALL,
	// fetch = FetchType.EAGER, orphanRemoval = true)
	// private List<StokJoinMarkaBarkod> stokJoinMarkaBarkodlar;

	@Column(name = "photo_path")
	private String photoPath;

	@Column(name = "aciklama")
	private String aciklama;

	@Column(name = "ada_no", nullable = false)
	private Integer adaNo;

	@Column(name = "aktif_pasif")
	private boolean aktifPasif;

	public StokUrunKartlari() {

		stokTanimlarUrunGrubu = new StokTanimlarUrunGrubu();
		stokTanimlarBirim = new StokTanimlarBirim();
		stokTanimlarRaf = new StokTanimlarRaf();
		stokJoinUrunMarkalar = new ArrayList<StokJoinUrunMarka>(0);
		stokJoinMarkaUreticiKodlar = new ArrayList<StokJoinMarkaUreticiKodu>(0);
		// stokJoinMarkaBarkodlar = new ArrayList<StokJoinMarkaBarkod>(0);
		// stokTanimlarMarka = new StokTanimlarMarka();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBarkodKodu() {
		return barkodKodu;
	}

	public void setBarkodKodu(String barkodKodu) {
		this.barkodKodu = barkodKodu;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	/**
	 * @return the gozNo
	 */
	public String getGozNo() {
		return gozNo;
	}

	/**
	 * @param gozNo
	 *            the gozNo to set
	 */
	public void setGozNo(String gozNo) {
		this.gozNo = gozNo;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Integer getMaxStok() {
		return this.maxStok;
	}

	public void setMaxStok(Integer maxStok) {
		this.maxStok = maxStok;
	}

	public Integer getMinStok() {
		return this.minStok;
	}

	public void setMinStok(Integer minStok) {
		this.minStok = minStok;
	}

	public String getUrunAdi() {
		return this.urunAdi;
	}

	public void setUrunAdi(String urunAdi) {
		this.urunAdi = urunAdi;
	}

	public StokTanimlarBirim getStokTanimlarBirim() {
		return stokTanimlarBirim;
	}

	public void setStokTanimlarBirim(StokTanimlarBirim stokTanimlarBirim) {
		this.stokTanimlarBirim = stokTanimlarBirim;
	}

	public StokTanimlarUrunGrubu getStokTanimlarUrunGrubu() {
		return stokTanimlarUrunGrubu;
	}

	public void setStokTanimlarUrunGrubu(
			StokTanimlarUrunGrubu stokTanimlarUrunGrubu) {
		this.stokTanimlarUrunGrubu = stokTanimlarUrunGrubu;
	}

	public StokTanimlarRaf getStokTanimlarRaf() {
		return stokTanimlarRaf;
	}

	public void setStokTanimlarRaf(StokTanimlarRaf stokTanimlarRaf) {
		this.stokTanimlarRaf = stokTanimlarRaf;
	}

	public StokTanimlarMarka getStokTanimlarMarka() {
		return stokTanimlarMarka;
	}

	public void setStokTanimlarMarka(StokTanimlarMarka stokTanimlarMarka) {
		this.stokTanimlarMarka = stokTanimlarMarka;
	}

	public Integer getSarfYeriId() {
		return sarfYeriId;
	}

	public void setSarfYeriId(Integer sarfYeriId) {
		this.sarfYeriId = sarfYeriId;
	}

	public List<StokJoinUrunMarka> getStokJoinUrunMarkalar() {
		return stokJoinUrunMarkalar;
	}

	public void setStokJoinUrunMarkalar(
			List<StokJoinUrunMarka> stokJoinUrunMarkalar) {
		this.stokJoinUrunMarkalar = stokJoinUrunMarkalar;
	}

	public List<StokJoinMarkaUreticiKodu> getStokJoinMarkaUreticiKodlar() {
		return stokJoinMarkaUreticiKodlar;
	}

	public void setStokJoinMarkaUreticiKodlar(
			List<StokJoinMarkaUreticiKodu> stokJoinMarkaUreticiKodlar) {
		this.stokJoinMarkaUreticiKodlar = stokJoinMarkaUreticiKodlar;
	}

	public StokJoinUreticiKoduBarkod getStokJoinUreticiKoduBarkod() {
		return stokJoinUreticiKoduBarkod;
	}

	public void setStokJoinUreticiKoduBarkod(
			StokJoinUreticiKoduBarkod stokJoinUreticiKoduBarkod) {
		this.stokJoinUreticiKoduBarkod = stokJoinUreticiKoduBarkod;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

	/**
	 * @return the aciklama
	 */
	public String getAciklama() {
		return aciklama;
	}

	/**
	 * @param aciklama
	 *            the aciklama to set
	 */
	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	/**
	 * @return the adaNo
	 */
	public Integer getAdaNo() {
		return adaNo;
	}

	/**
	 * @param adaNo
	 *            the adaNo to set
	 */
	public void setAdaNo(Integer adaNo) {
		this.adaNo = adaNo;
	}

	/**
	 * @return the aktifPasif
	 */
	public boolean isAktifPasif() {
		return aktifPasif;
	}

	/**
	 * @param aktifPasif
	 *            the aktifPasif to set
	 */
	public void setAktifPasif(boolean aktifPasif) {
		this.aktifPasif = aktifPasif;
	}

	/**
	 * @return the urunKodu
	 */
	public String getUrunKodu() {
		return urunKodu;
	}

	/**
	 * @param urunKodu
	 *            the urunKodu to set
	 */
	public void setUrunKodu(String urunKodu) {
		this.urunKodu = urunKodu;
	}

}