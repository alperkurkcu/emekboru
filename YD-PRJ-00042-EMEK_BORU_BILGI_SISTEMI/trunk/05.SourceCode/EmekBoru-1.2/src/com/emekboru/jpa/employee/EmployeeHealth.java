package com.emekboru.jpa.employee;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.Employee;
import com.emekboru.jpa.common.CommonBloodGroup;
import com.emekboru.jpa.common.CommonHealthStatus;

/**
 * The persistent class for the employee_health database table.
 * 
 */
@Entity
@Table(name = "employee_health")
@NamedQuery(name = "EmployeeHealth.findAll", query = "SELECT e FROM EmployeeHealth e")
public class EmployeeHealth implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "EMPLOYEE_HEALTH_ID_GENERATOR", sequenceName = "EMPLOYEE_HEALTH_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMPLOYEE_HEALTH_ID_GENERATOR")
	@Column(name = "id")
	private Integer id;

	@Column(name = "description")
	private String description;

	@Column(name = "ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "employee_id", referencedColumnName = "employee_id", insertable = false)
	private Employee employee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "health_condition_description")
	private String healthConditionDescription;

	@JoinColumn(name = "blood_group_id", referencedColumnName = "id", insertable = false)
	private CommonBloodGroup commonBloodGroup;

	@JoinColumn(name = "health_status_id", referencedColumnName = "id", insertable = false)
	private CommonHealthStatus commonHealthStatus;

	public EmployeeHealth() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getHealthConditionDescription() {
		return this.healthConditionDescription;
	}

	public void setHealthConditionDescription(String healthConditionDescription) {
		this.healthConditionDescription = healthConditionDescription;
	}

	public CommonBloodGroup getCommonBloodGroup() {
		return this.commonBloodGroup;
	}

	public void setCommonBloodGroup(CommonBloodGroup commonBloodGroup) {
		this.commonBloodGroup = commonBloodGroup;
	}

	public CommonHealthStatus getCommonHealthStatus() {
		return this.commonHealthStatus;
	}

	public void setCommonHealthStatus(CommonHealthStatus commonHealthStatus) {
		this.commonHealthStatus = commonHealthStatus;
	}

}