package com.emekboru.jpa.tahribatsiztestsonuc;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.kalibrasyon.KalibrasyonOtomatikUltrasonik;
import com.emekboru.jpa.kalibrasyon.KalibrasyonUltrasonikLaminasyon;
import com.emekboru.jpa.rulo.RuloPipeLink;

/**
 * The persistent class for the test_tahribatsiz_otomatik_ut_sonuc database
 * table.
 * 
 */

@Entity
@NamedQueries({ @NamedQuery(name = "TestTahribatsizOtomatikUtSonuc.findAll", query = "SELECT r FROM TestTahribatsizOtomatikUtSonuc r WHERE r.pipe.pipeId=:prmPipeId order by r.koordinatQ") })
@Table(name = "test_tahribatsiz_otomatik_ut_sonuc")
public class TestTahribatsizOtomatikUtSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_TAHRIBATSIZ_OTOMATIK_UT_SONUC_ID_GENERATOR", sequenceName = "TEST_TAHRIBATSIZ_OTOMATIK_UT_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_TAHRIBATSIZ_OTOMATIK_UT_SONUC_ID_GENERATOR")
	@Column(name = "ID", unique = true, nullable = false)
	private Integer id;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "test_id")
	private Integer testId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@Column(name = "kaynak_laminasyon", nullable = false)
	private Integer kaynakLaminasyon;

	@Column(name = "koordinat_q")
	private Integer koordinatQ;

	@Column(name = "koordinat_l")
	private Integer koordinatL;

	// kazik borusu kontrolu
	@Transient
	private Boolean kazikKontrol = false;

	@Column(name = "aciklama", length = 255)
	private String aciklama;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false, updatable = false)
	private RuloPipeLink ruloPipeLink;

	// @Column(name = "kalibrasyon_online_id")
	// private Integer kalibrasyonOnlineId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "kalibrasyon_online_id", referencedColumnName = "ID", insertable = false)
	private KalibrasyonOtomatikUltrasonik kalibrasyonOnline;

	// @Column(name = "kalibrasyon_laminasyon_id")
	// private Integer kalibrasyonLaminasyonId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "kalibrasyon_laminasyon_id", referencedColumnName = "ID", insertable = false)
	private KalibrasyonUltrasonikLaminasyon kalibrasyonLaminasyon;

	public TestTahribatsizOtomatikUtSonuc() {

	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public Integer getTestId() {
		return testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	public Integer getKaynakLaminasyon() {
		return kaynakLaminasyon;
	}

	public void setKaynakLaminasyon(Integer kaynakLaminasyon) {
		this.kaynakLaminasyon = kaynakLaminasyon;
	}

	public Integer getKoordinatQ() {
		return koordinatQ;
	}

	public void setKoordinatQ(Integer koordinatQ) {
		this.koordinatQ = koordinatQ;
	}

	public Integer getKoordinatL() {
		return koordinatL;
	}

	public void setKoordinatL(Integer koordinatL) {
		this.koordinatL = koordinatL;
	}

	public Timestamp getEklemeZamani() {
		return eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Timestamp getGuncellemeZamani() {
		return guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Boolean getKazikKontrol() {
		return kazikKontrol;
	}

	public void setKazikKontrol(Boolean kazikKontrol) {
		this.kazikKontrol = kazikKontrol;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public RuloPipeLink getRuloPipeLink() {
		return ruloPipeLink;
	}

	public void setRuloPipeLink(RuloPipeLink ruloPipeLink) {
		this.ruloPipeLink = ruloPipeLink;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the kalibrasyonOnline
	 */
	public KalibrasyonOtomatikUltrasonik getKalibrasyonOnline() {
		return kalibrasyonOnline;
	}

	/**
	 * @param kalibrasyonOnline
	 *            the kalibrasyonOnline to set
	 */
	public void setKalibrasyonOnline(
			KalibrasyonOtomatikUltrasonik kalibrasyonOnline) {
		this.kalibrasyonOnline = kalibrasyonOnline;
	}

	/**
	 * @return the kalibrasyonLaminasyon
	 */
	public KalibrasyonUltrasonikLaminasyon getKalibrasyonLaminasyon() {
		return kalibrasyonLaminasyon;
	}

	/**
	 * @param kalibrasyonLaminasyon
	 *            the kalibrasyonLaminasyon to set
	 */
	public void setKalibrasyonLaminasyon(
			KalibrasyonUltrasonikLaminasyon kalibrasyonLaminasyon) {
		this.kalibrasyonLaminasyon = kalibrasyonLaminasyon;
	}

}