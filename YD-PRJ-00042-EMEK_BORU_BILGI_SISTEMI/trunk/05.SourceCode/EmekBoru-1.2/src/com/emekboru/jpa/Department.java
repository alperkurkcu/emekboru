package com.emekboru.jpa;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the department database table.
 * 
 */
@Entity
@Table(name = "department")
public class Department implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "DEPARTMENT_GENERATOR", sequenceName = "DEPARTMENT_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DEPARTMENT_GENERATOR")
	@Column(name = "department_id")
	private Integer departmentId;

	@Column(name = "description")
	private String description;

	private String name;

	private String address;

	@Column(name = "city")
	private String city;

	@Column(name = "phone_number")
	private String phoneNumber;

	@Column(name = "fax_number")
	private String faxNumber;

	@Column(name = "postal_code")
	private String postalCode;

	@Column(name = "email")
	private String email;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ulke_id", referencedColumnName = "ID")
	private Ulkeler ulkeler;

	@OneToMany(mappedBy = "department", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Employee> employees;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "department_manager_employee_id", referencedColumnName = "employee_id", insertable = false)
	private Employee departmentManagerEmployee;

	public Department() {
		ulkeler = new Ulkeler();
	}

	public Integer getDepartmentId() {
		return this.departmentId;
	}

	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String departmentDesc) {
		this.description = departmentDesc;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public Ulkeler getUlkeler() {
		return ulkeler;
	}

	public void setUlkeler(Ulkeler ulkeler) {
		this.ulkeler = ulkeler;
	}

	/**
	 * @return the departmentManagerEmployee
	 */
	public Employee getDepartmentManagerEmployee() {
		return departmentManagerEmployee;
	}

	/**
	 * @param departmentManagerEmployee
	 *            the departmentManagerEmployee to set
	 */
	public void setDepartmentManagerEmployee(Employee departmentManagerEmployee) {
		this.departmentManagerEmployee = departmentManagerEmployee;
	}

}