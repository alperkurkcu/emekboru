package com.emekboru.jpa.rulo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "rulo_test_centik_darbe_enine")
public class RuloTestCentikDarbeEnine {
	@Id
	@SequenceGenerator(name = "rulo_centik_darbe_enine_generator", sequenceName = "rulo_centik_darbe_enine_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rulo_centik_darbe_enine_generator")
	@Column(name = "rulo_centik_darbe_id")
	private Integer ruloTestCentikDarbeId;

	@Column(name = "rulo_centik_darbe_enerji_1")
	private Float ruloTestCentikDarbeEnerji1;

	@Column(name = "rulo_centik_darbe_enerji_2")
	private Float ruloTestCentikDarbeEnerji2;

	@Column(name = "rulo_centik_darbe_enerji_3")
	private Float ruloTestCentikDarbeEnerji3;

	@Column(name = "rulo_centik_darbe_ortalama_enerji")
	private Float ruloTestCentikDarbeOrtalamaEnerji;

	@Column(name = "rulo_centik_darbe_aciklama")
	private String ruloTestCentikDarbeAciklama;

	@Temporal(TemporalType.DATE)
	@Column(name = "rulo_centik_darbe_tarih")
	private Date ruloTestCentikDarbeTarih;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "rulo_id", nullable = false)
	private Rulo rulo;

	@Column(name = "durum")
	private Integer durum = 0;

	@Column(name = "rulo_centik_darbe_sicaklik")
	private Float ruloTestCentikDarbeSicaklik;

	@Column(name = "islem_yapan_id")
	private int islemYapanId;

	@Column(name = "islem_yapan_isim")
	private String islemYapanIsim;

	// setters getters
	public int getIslemYapanId() {
		return islemYapanId;
	}

	public void setIslemYapanId(int islemYapanId) {
		this.islemYapanId = islemYapanId;
	}

	public String getIslemYapanIsim() {
		return islemYapanIsim;
	}

	public void setIslemYapanIsim(String islemYapanIsim) {
		this.islemYapanIsim = islemYapanIsim;
	}

	public Integer getDurum() {
		return durum;
	}

	public void setDurum(Integer durum) {
		this.durum = durum;
	}

	public Rulo getRulo() {
		return rulo;
	}

	public void setRulo(Rulo rulo) {
		this.rulo = rulo;
	}

	public Integer getRuloTestCentikDarbeId() {
		return ruloTestCentikDarbeId;
	}

	public void setRuloTestCentikDarbeId(Integer ruloTestCentikDarbeId) {
		this.ruloTestCentikDarbeId = ruloTestCentikDarbeId;
	}

	public String getRuloTestCentikDarbeAciklama() {
		return ruloTestCentikDarbeAciklama;
	}

	public void setRuloTestCentikDarbeAciklama(
			String ruloTestCentikDarbeAciklama) {
		this.ruloTestCentikDarbeAciklama = ruloTestCentikDarbeAciklama;
	}

	public Date getRuloTestCentikDarbeTarih() {
		return ruloTestCentikDarbeTarih;
	}

	public void setRuloTestCentikDarbeTarih(Date ruloTestCentikDarbeTarih) {
		this.ruloTestCentikDarbeTarih = ruloTestCentikDarbeTarih;
	}

	public Float getRuloTestCentikDarbeEnerji1() {
		return ruloTestCentikDarbeEnerji1;
	}

	public void setRuloTestCentikDarbeEnerji1(Float ruloTestCentikDarbeEnerji1) {
		this.ruloTestCentikDarbeEnerji1 = ruloTestCentikDarbeEnerji1;
	}

	public Float getRuloTestCentikDarbeEnerji2() {
		return ruloTestCentikDarbeEnerji2;
	}

	public void setRuloTestCentikDarbeEnerji2(Float ruloTestCentikDarbeEnerji2) {
		this.ruloTestCentikDarbeEnerji2 = ruloTestCentikDarbeEnerji2;
	}

	public Float getRuloTestCentikDarbeEnerji3() {
		return ruloTestCentikDarbeEnerji3;
	}

	public void setRuloTestCentikDarbeEnerji3(Float ruloTestCentikDarbeEnerji3) {
		this.ruloTestCentikDarbeEnerji3 = ruloTestCentikDarbeEnerji3;
	}

	public Float getRuloTestCentikDarbeOrtalamaEnerji() {
		return ruloTestCentikDarbeOrtalamaEnerji;
	}

	public void setRuloTestCentikDarbeOrtalamaEnerji(
			Float ruloTestCentikDarbeOrtalamaEnerji) {
		this.ruloTestCentikDarbeOrtalamaEnerji = ruloTestCentikDarbeOrtalamaEnerji;
	}

	public Float getRuloTestCentikDarbeSicaklik() {
		return ruloTestCentikDarbeSicaklik;
	}

	public void setRuloTestCentikDarbeSicaklik(Float ruloTestCentikDarbeSicaklik) {
		this.ruloTestCentikDarbeSicaklik = ruloTestCentikDarbeSicaklik;
	}

}
