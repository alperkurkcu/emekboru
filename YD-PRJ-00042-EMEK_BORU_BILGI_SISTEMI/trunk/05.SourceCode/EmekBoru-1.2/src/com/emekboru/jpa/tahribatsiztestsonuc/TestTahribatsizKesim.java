package com.emekboru.jpa.tahribatsiztestsonuc;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the test_tahribatsiz_kesim database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestTahribatsizKesim.findAll", query = "SELECT r FROM TestTahribatsizKesim r WHERE r.pipe.pipeId=:prmPipeId order by r.eklemeZamani") })
@Table(name = "test_tahribatsiz_kesim")
public class TestTahribatsizKesim implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_TAHRIBATSIZ_KESIM_ID_GENERATOR", sequenceName = "TEST_TAHRIBATSIZ_KESIM_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_TAHRIBATSIZ_KESIM_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(length = 255, name = "aciklama")
	private String aciklama;

	private Boolean durum;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser user;

	// @Column(name = "manuel_ut_id")
	// private Integer manuelUtId;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "manuel_ut_id", referencedColumnName = "ID", insertable = false)
	private TestTahribatsizManuelUtSonuc testTahribatsizManuelUtSonuc;

	// @Column(name = "fl_id")
	// private Integer flId;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fl_id", referencedColumnName = "ID", insertable = false, updatable = true)
	private TestTahribatsizFloroskopikSonuc testTahribatsizFloroskopikSonuc;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@Column(name = "muayene_baslama_kullanici")
	private Integer muayeneBaslamaKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "muayene_baslama_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser muayeneBaslamaUser;

	@Column(name = "muayene_baslama_zamani")
	private Timestamp muayeneBaslamaZamani;

	@Column(name = "muayene_bitis_kullanici")
	private Integer muayeneBitisKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "muayene_bitis_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser muayeneBitisUser;

	@Column(name = "muayene_bitis_zamani")
	private Timestamp muayeneBitisZamani;

	@Basic
	@Column(name = "pipe_id", nullable = false, insertable = false, updatable = false)
	private Integer pipeId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	public TestTahribatsizKesim() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Boolean getDurum() {
		return this.durum;
	}

	public void setDurum(Boolean durum) {
		this.durum = durum;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getMuayeneBaslamaKullanici() {
		return this.muayeneBaslamaKullanici;
	}

	public void setMuayeneBaslamaKullanici(Integer muayeneBaslamaKullanici) {
		this.muayeneBaslamaKullanici = muayeneBaslamaKullanici;
	}

	public Timestamp getMuayeneBaslamaZamani() {
		return this.muayeneBaslamaZamani;
	}

	public void setMuayeneBaslamaZamani(Timestamp muayeneBaslamaZamani) {
		this.muayeneBaslamaZamani = muayeneBaslamaZamani;
	}

	public Integer getMuayeneBitisKullanici() {
		return this.muayeneBitisKullanici;
	}

	public void setMuayeneBitisKullanici(Integer muayeneBitisKullanici) {
		this.muayeneBitisKullanici = muayeneBitisKullanici;
	}

	public Timestamp getMuayeneBitisZamani() {
		return this.muayeneBitisZamani;
	}

	public void setMuayeneBitisZamani(Timestamp muayeneBitisZamani) {
		this.muayeneBitisZamani = muayeneBitisZamani;
	}

	public SystemUser getUser() {
		return user;
	}

	public void setUser(SystemUser user) {
		this.user = user;
	}

	public TestTahribatsizManuelUtSonuc getTestTahribatsizManuelUtSonuc() {
		return testTahribatsizManuelUtSonuc;
	}

	public void setTestTahribatsizManuelUtSonuc(
			TestTahribatsizManuelUtSonuc testTahribatsizManuelUtSonuc) {
		this.testTahribatsizManuelUtSonuc = testTahribatsizManuelUtSonuc;
	}

	public TestTahribatsizFloroskopikSonuc getTestTahribatsizFloroskopikSonuc() {
		return testTahribatsizFloroskopikSonuc;
	}

	public void setTestTahribatsizFloroskopikSonuc(
			TestTahribatsizFloroskopikSonuc testTahribatsizFloroskopikSonuc) {
		this.testTahribatsizFloroskopikSonuc = testTahribatsizFloroskopikSonuc;
	}

	public SystemUser getMuayeneBaslamaUser() {
		return muayeneBaslamaUser;
	}

	public void setMuayeneBaslamaUser(SystemUser muayeneBaslamaUser) {
		this.muayeneBaslamaUser = muayeneBaslamaUser;
	}

	public SystemUser getMuayeneBitisUser() {
		return muayeneBitisUser;
	}

	public void setMuayeneBitisUser(SystemUser muayeneBitisUser) {
		this.muayeneBitisUser = muayeneBitisUser;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}