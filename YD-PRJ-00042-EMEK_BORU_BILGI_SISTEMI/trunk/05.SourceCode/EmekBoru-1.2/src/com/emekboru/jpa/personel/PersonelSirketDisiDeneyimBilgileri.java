package com.emekboru.jpa.personel;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the personel_sirket_disi_deneyim_bilgileri database table.
 * 
 */
@Entity
@Table(name="personel_sirket_disi_deneyim_bilgileri")
public class PersonelSirketDisiDeneyimBilgileri implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PERSONEL_SIRKET_DISI_DENEYIM_BILGILERI_ID_GENERATOR", sequenceName="PERSONEL_SIRKET_DISI_DENEYIM_BILGILERI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PERSONEL_SIRKET_DISI_DENEYIM_BILGILERI_ID_GENERATOR")
	@Column(name="ID")
	private Integer id;

	@Column(name="ayrilma_nedeni")
	private String ayrilmaNedeni;

	@Column(name="baslangic_tarihi")
	private Timestamp baslangicTarihi;

	@Column(name="bitis_tarihi")
	private Timestamp bitisTarihi;

	@Column(name="calistigi_firma")
	private String calistigiFirma;

	@Column(name="eklenme_tarihi")
	private Timestamp eklenmeTarihi;
	@Column(name="gorevi")
	private String gorevi;

	@Column(name="kimlik_id")
	private Integer kimlikId;

	@Column(name="kullanici_id_ekleyen")
	private Integer kullaniciIdEkleyen;

	@Column(name="kullanici_id_guncelleyen")
	private Integer kullaniciIdGuncelleyen;

	@Column(name="son_guncellenme_tarihi")
	private Timestamp sonGuncellenmeTarihi;

	public PersonelSirketDisiDeneyimBilgileri() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAyrilmaNedeni() {
		return this.ayrilmaNedeni;
	}

	public void setAyrilmaNedeni(String ayrilmaNedeni) {
		this.ayrilmaNedeni = ayrilmaNedeni;
	}

	public Timestamp getBaslangicTarihi() {
		return this.baslangicTarihi;
	}

	public void setBaslangicTarihi(Timestamp baslangicTarihi) {
		this.baslangicTarihi = baslangicTarihi;
	}

	public Timestamp getBitisTarihi() {
		return this.bitisTarihi;
	}

	public void setBitisTarihi(Timestamp bitisTarihi) {
		this.bitisTarihi = bitisTarihi;
	}

	public String getCalistigiFirma() {
		return this.calistigiFirma;
	}

	public void setCalistigiFirma(String calistigiFirma) {
		this.calistigiFirma = calistigiFirma;
	}

	public Timestamp getEklenmeTarihi() {
		return this.eklenmeTarihi;
	}

	public void setEklenmeTarihi(Timestamp eklenmeTarihi) {
		this.eklenmeTarihi = eklenmeTarihi;
	}

	public String getGorevi() {
		return this.gorevi;
	}

	public void setGorevi(String gorevi) {
		this.gorevi = gorevi;
	}

	public Integer getKimlikId() {
		return this.kimlikId;
	}

	public void setKimlikId(Integer kimlikId) {
		this.kimlikId = kimlikId;
	}

	public Integer getKullaniciIdEkleyen() {
		return this.kullaniciIdEkleyen;
	}

	public void setKullaniciIdEkleyen(Integer kullaniciIdEkleyen) {
		this.kullaniciIdEkleyen = kullaniciIdEkleyen;
	}

	public Integer getKullaniciIdGuncelleyen() {
		return this.kullaniciIdGuncelleyen;
	}

	public void setKullaniciIdGuncelleyen(Integer kullaniciIdGuncelleyen) {
		this.kullaniciIdGuncelleyen = kullaniciIdGuncelleyen;
	}

	public Timestamp getSonGuncellenmeTarihi() {
		return this.sonGuncellenmeTarihi;
	}

	public void setSonGuncellenmeTarihi(Timestamp sonGuncellenmeTarihi) {
		this.sonGuncellenmeTarihi = sonGuncellenmeTarihi;
	}

}