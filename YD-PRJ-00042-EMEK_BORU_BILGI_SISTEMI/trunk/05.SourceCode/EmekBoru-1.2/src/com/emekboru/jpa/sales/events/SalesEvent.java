package com.emekboru.jpa.sales.events;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.emekboru.jpa.Employee;
import com.emekboru.jpa.sales.SalesStatus;
import com.emekboru.jpa.sales.customer.SalesContactPeople;
import com.emekboru.jpa.sales.customer.SalesCustomer;

@Entity
@Table(name = "sales_event")
public class SalesEvent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3794909456593006805L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_EVENT_GENERATOR")
	@SequenceGenerator(name = "SALES_EVENT_GENERATOR", sequenceName = "SALES_EVENT_SEQUENCE", allocationSize = 1)
	@Column(name = "event_id")
	private int eventId;

	@Column(name = "title")
	private String title;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "event_type", referencedColumnName = "type_id")
	private SalesEventType eventType;

	@Column(name = "event_location")
	private String location;

	@Temporal(TemporalType.DATE)
	@Column(name = "event_date")
	private Date eventDate;

	@Temporal(TemporalType.TIME)
	@Column(name = "event_time")
	private Date time;

	@Column(name = "total")
	private String total;

	@Column(name = "items")
	private String items;

	@Column(name = "kazanim")
	private String kazanim;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "status", referencedColumnName = "status_id")
	private SalesStatus status;

	@Column(name = "preparation")
	private String preparation;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "the_customer", referencedColumnName = "sales_customer_id")
	private SalesCustomer theCustomer;

	/* JOIN FUNCTIONS */
	@Transient
	private List<Employee> participantList;

	@Transient
	private List<SalesContactPeople> contactPeopleList;

	/* end of JOIN FUNCTIONS */

	@Override
	public boolean equals(Object o) {
		return this.getEventId() == ((SalesEvent) o).getEventId();
	}

	public SalesEvent() {
		title = "-";
		participantList = new ArrayList<Employee>();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public int getEventId() {
		return eventId;
	}

	public String getTitle() {
		return title;
	}

	public String getLocation() {
		return location;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public Date getTime() {
		return time;
	}

	public String getTotal() {
		return total;
	}

	public String getItems() {
		return items;
	}

	public String getKazanim() {
		return kazanim;
	}

	public SalesStatus getStatus() {
		return status;
	}

	public SalesEventType getEventType() {
		return eventType;
	}

	public String getPreparation() {
		return preparation;
	}

	public List<Employee> getParticipantList() {
		return participantList;
	}

	public List<SalesContactPeople> getContactPeopleList() {
		return contactPeopleList;
	}

	public SalesCustomer getTheCustomer() {
		return theCustomer;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public void setItems(String items) {
		this.items = items;
	}

	public void setKazanim(String kazanim) {
		this.kazanim = kazanim;
	}

	public void setStatus(SalesStatus status) {
		this.status = status;
	}

	public void setEventType(SalesEventType eventType) {
		this.eventType = eventType;
	}

	public void setPreparation(String preparation) {
		this.preparation = preparation;
	}

	public void setParticipantList(List<Employee> participantList) {
		this.participantList = participantList;
	}

	public void setContactPeopleList(List<SalesContactPeople> contactPeopleList) {
		this.contactPeopleList = contactPeopleList;
	}

	public void setTheCustomer(SalesCustomer theCustomer) {
		this.theCustomer = theCustomer;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
