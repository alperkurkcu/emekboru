package com.emekboru.jpa.machines;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.Machine;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the machine_sales_pipe_no database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "MachineSalesPipeNo.findByMachineSalesItem", query = "SELECT r FROM MachineSalesPipeNo r WHERE r.machine.machineId=:prmMachineId and r.salesItem.itemId=:prmItemId and r.durum=true"),
		@NamedQuery(name = "MachineSalesPipeNo.findBySalesItem", query = "SELECT r FROM MachineSalesPipeNo r WHERE r.salesItem.itemId=:prmItemId and r.durum=true") })
@Table(name = "machine_sales_pipe_no")
public class MachineSalesPipeNo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "MACHINE_SALES_PIPE_NO_ID_GENERATOR", sequenceName = "MACHINE_SALES_PIPE_NO_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MACHINE_SALES_PIPE_NO_ID_GENERATOR")
	@Column(name = "ID", unique = true, nullable = false)
	private Integer id;

	@Column(name = "first_pipe_no")
	private Integer firstPipeNo;

	@Column(name = "last_pipe_no")
	private Integer lastPipeNo;

	// @Column(name = "machine_id")
	// private Integer machineId;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "machine_id", referencedColumnName = "machine_id", insertable = false)
	private Machine machine;

	// @Column(name = "sales_item_id")
	// private Integer salesItemId;

	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private SalesItem salesItem;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "durum")
	private boolean durum;

	public MachineSalesPipeNo() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFirstPipeNo() {
		return this.firstPipeNo;
	}

	public void setFirstPipeNo(Integer firstPipeNo) {
		this.firstPipeNo = firstPipeNo;
	}

	public Integer getLastPipeNo() {
		return this.lastPipeNo;
	}

	public void setLastPipeNo(Integer lastPipeNo) {
		this.lastPipeNo = lastPipeNo;
	}

	/**
	 * @return the eklemeZamani
	 */
	public Timestamp getEklemeZamani() {
		return eklemeZamani;
	}

	/**
	 * @param eklemeZamani
	 *            the eklemeZamani to set
	 */
	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	/**
	 * @return the ekleyenKullanici
	 */
	public Integer getEkleyenKullanici() {
		return ekleyenKullanici;
	}

	/**
	 * @param ekleyenKullanici
	 *            the ekleyenKullanici to set
	 */
	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncellemeZamani
	 */
	public Timestamp getGuncellemeZamani() {
		return guncellemeZamani;
	}

	/**
	 * @param guncellemeZamani
	 *            the guncellemeZamani to set
	 */
	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	/**
	 * @return the guncelleyenKullanici
	 */
	public Integer getGuncelleyenKullanici() {
		return guncelleyenKullanici;
	}

	/**
	 * @param guncelleyenKullanici
	 *            the guncelleyenKullanici to set
	 */
	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the machine
	 */
	public Machine getMachine() {
		return machine;
	}

	/**
	 * @param machine
	 *            the machine to set
	 */
	public void setMachine(Machine machine) {
		this.machine = machine;
	}

	/**
	 * @return the salesItem
	 */
	public SalesItem getSalesItem() {
		return salesItem;
	}

	/**
	 * @param salesItem
	 *            the salesItem to set
	 */
	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	/**
	 * @return the durum
	 */
	public boolean isDurum() {
		return durum;
	}

	/**
	 * @param durum
	 *            the durum to set
	 */
	public void setDurum(boolean durum) {
		this.durum = durum;
	}

}