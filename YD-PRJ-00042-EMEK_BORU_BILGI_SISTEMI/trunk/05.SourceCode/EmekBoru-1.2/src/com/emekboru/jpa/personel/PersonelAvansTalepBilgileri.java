package com.emekboru.jpa.personel;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the personel_avans_talep_bilgileri database table.
 * 
 */
@Entity
@Table(name="personel_avans_talep_bilgileri")
public class PersonelAvansTalepBilgileri implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PERSONEL_AVANS_TALEP_BILGILERI_ID_GENERATOR", sequenceName="PERSONEL_AVANS_TALEP_BILGILERI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PERSONEL_AVANS_TALEP_BILGILERI_ID_GENERATOR")
	@Column(name="ID")
	private Integer id;
	
	@Column(name="aktif")
	private Boolean aktif;

	@Column(name="avans_miktari")
	private float avansMiktari;

	private Boolean durumu;

	@Column(name="eklenme_tarihi")
	private Timestamp eklenmeTarihi;

	@Column(name="kimlik_id")
	private Integer kimlikId;

	@Column(name="kimlik_id_onay_veren")
	private Integer kimlikIdOnayVeren;

	@Column(name="kullanici_id_ekleyen")
	private Integer kullaniciIdEkleyen;

	@Column(name="kullanici_id_guncelleyen")
	private Integer kullaniciIdGuncelleyen;
	
	@Column(name="nedeni")
	private String nedeni;

	@Column(name="son_guncellenme_tarihi")
	private Timestamp sonGuncellenmeTarihi;

	@Column(name="taksit_sayisi")
	private Integer taksitSayisi;

	@Column(name = "talep_tarihi")
	private Timestamp talepTarihi;

	public PersonelAvansTalepBilgileri() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getAktif() {
		return this.aktif;
	}

	public void setAktif(Boolean aktif) {
		this.aktif = aktif;
	}

	public float getAvansMiktari() {
		return this.avansMiktari;
	}

	public void setAvansMiktari(float avansMiktari) {
		this.avansMiktari = avansMiktari;
	}

	public Boolean getDurumu() {
		return this.durumu;
	}

	public void setDurumu(Boolean durumu) {
		this.durumu = durumu;
	}

	public Timestamp getEklenmeTarihi() {
		return this.eklenmeTarihi;
	}

	public void setEklenmeTarihi(Timestamp eklenmeTarihi) {
		this.eklenmeTarihi = eklenmeTarihi;
	}

	public Integer getKimlikId() {
		return this.kimlikId;
	}

	public void setKimlikId(Integer kimlikId) {
		this.kimlikId = kimlikId;
	}

	public Integer getKimlikIdOnayVeren() {
		return this.kimlikIdOnayVeren;
	}

	public void setKimlikIdOnayVeren(Integer kimlikIdOnayVeren) {
		this.kimlikIdOnayVeren = kimlikIdOnayVeren;
	}

	public Integer getKullaniciIdEkleyen() {
		return this.kullaniciIdEkleyen;
	}

	public void setKullaniciIdEkleyen(Integer kullaniciIdEkleyen) {
		this.kullaniciIdEkleyen = kullaniciIdEkleyen;
	}

	public Integer getKullaniciIdGuncelleyen() {
		return this.kullaniciIdGuncelleyen;
	}

	public void setKullaniciIdGuncelleyen(Integer kullaniciIdGuncelleyen) {
		this.kullaniciIdGuncelleyen = kullaniciIdGuncelleyen;
	}

	public String getNedeni() {
		return this.nedeni;
	}

	public void setNedeni(String nedeni) {
		this.nedeni = nedeni;
	}

	public Timestamp getSonGuncellenmeTarihi() {
		return this.sonGuncellenmeTarihi;
	}

	public void setSonGuncellenmeTarihi(Timestamp sonGuncellenmeTarihi) {
		this.sonGuncellenmeTarihi = sonGuncellenmeTarihi;
	}

	public Integer getTaksitSayisi() {
		return this.taksitSayisi;
	}

	public void setTaksitSayisi(Integer taksitSayisi) {
		this.taksitSayisi = taksitSayisi;
	}

	public Timestamp getTalepTarihi() {
		return this.talepTarihi;
	}

	public void setTalepTarihi(Timestamp talepTarihi) {
		this.talepTarihi = talepTarihi;
	}

}