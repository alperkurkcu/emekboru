package com.emekboru.jpa.istakipformu;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the is_takip_formu_polietilen_kaplama_once_islemler
 * database table.
 * 
 */
@Entity
@Table(name = "is_takip_formu_polietilen_kaplama_once_islemler")
public class IsTakipFormuPolietilenKaplamaOnceIslemler implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IS_TAKIP_FORMU_POLIETILEN_KAPLAMA_ONCE_ISLEMLER_ID_GENERATOR", sequenceName = "IS_TAKIP_FORMU_POLIETILEN_KAPLAMA_ONCE_ISLEMLER_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IS_TAKIP_FORMU_POLIETILEN_KAPLAMA_ONCE_ISLEMLER_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "boru_listesi_check")
	private Boolean boruListesiCheck;

	@Column(name = "boru_listesi_kullanici")
	private Integer boruListesiKullanici;

	@Temporal(TemporalType.DATE)
	@Column(name = "boru_listesi_tarih")
	private Date boruListesiTarih;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "engel_yok_check")
	private Boolean engelYokCheck;

	@Column(name = "engel_yok_kullanici")
	private Integer engelYokKullanici;

	@Temporal(TemporalType.DATE)
	@Column(name = "engel_yok_tarih")
	private Date engelYokTarih;

	@Column(name = "gerceklesen_ayar_check")
	private Boolean gerceklesenAyarCheck;

	@Column(name = "gerceklesen_ayar_kullanici")
	private Integer gerceklesenAyarKullanici;

	@Column(name = "gerceklesen_ayar_suresi")
	private Integer gerceklesenAyarSuresi;

	@Temporal(TemporalType.DATE)
	@Column(name = "gerceklesen_ayar_tarih")
	private Date gerceklesenAyarTarih;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name = "is_takip_form_id")
	// private Integer isTakipFormId;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "is_takip_form_id", referencedColumnName = "ID", insertable = false, updatable = false)
	private IsTakipFormuPolietilenKaplama isTakipFormuPolietilenKaplama;

	@Column(name = "malzeme_check")
	private Boolean malzemeCheck;

	@Column(name = "malzeme_kullanici")
	private Integer malzemeKullanici;

	@Temporal(TemporalType.DATE)
	@Column(name = "malzeme_tarih")
	private Date malzemeTarih;

	@Column(name = "ongorulen_ayar_check")
	private Boolean ongorulenAyarCheck;

	@Column(name = "ongorulen_ayar_kullanici")
	private Integer ongorulenAyarKullanici;

	@Column(name = "ongorulen_ayar_suresi")
	private Integer ongorulenAyarSuresi;

	@Temporal(TemporalType.DATE)
	@Column(name = "ongorulen_ayar_tarih")
	private Date ongorulenAyarTarih;

	@Column(name = "parametre_check")
	private Boolean parametreCheck;

	@Column(name = "parametre_kullanici")
	private Integer parametreKullanici;

	@Temporal(TemporalType.DATE)
	@Column(name = "parametre_tarih")
	private Date parametreTarih;

	@Column(name = "engel_yok_aciklama", length = 255)
	private String engelYokAciklama;

	@Column(name = "boru_listesi_aciklama", length = 255)
	private String boruListesiAciklama;

	@Column(name = "malzeme_aciklama", length = 255)
	private String malzemeAciklama;

	@Column(name = "parametre_aciklama", length = 255)
	private String parametreAciklama;

	@Column(name = "ongorulen_ayar_aciklama", length = 255)
	private String ongorulenAyarAciklama;

	@Column(name = "gerceklesen_ayar_aciklama", length = 255)
	private String gerceklesenAyarAciklama;

	public IsTakipFormuPolietilenKaplamaOnceIslemler() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getBoruListesiCheck() {
		return this.boruListesiCheck;
	}

	public void setBoruListesiCheck(Boolean boruListesiCheck) {
		this.boruListesiCheck = boruListesiCheck;
	}

	public Integer getBoruListesiKullanici() {
		return this.boruListesiKullanici;
	}

	public void setBoruListesiKullanici(Integer boruListesiKullanici) {
		this.boruListesiKullanici = boruListesiKullanici;
	}

	public Date getBoruListesiTarih() {
		return this.boruListesiTarih;
	}

	public void setBoruListesiTarih(Date boruListesiTarih) {
		this.boruListesiTarih = boruListesiTarih;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Boolean getEngelYokCheck() {
		return this.engelYokCheck;
	}

	public void setEngelYokCheck(Boolean engelYokCheck) {
		this.engelYokCheck = engelYokCheck;
	}

	public Integer getEngelYokKullanici() {
		return this.engelYokKullanici;
	}

	public void setEngelYokKullanici(Integer engelYokKullanici) {
		this.engelYokKullanici = engelYokKullanici;
	}

	public Date getEngelYokTarih() {
		return this.engelYokTarih;
	}

	public void setEngelYokTarih(Date engelYokTarih) {
		this.engelYokTarih = engelYokTarih;
	}

	public Boolean getGerceklesenAyarCheck() {
		return this.gerceklesenAyarCheck;
	}

	public void setGerceklesenAyarCheck(Boolean gerceklesenAyarCheck) {
		this.gerceklesenAyarCheck = gerceklesenAyarCheck;
	}

	public Integer getGerceklesenAyarKullanici() {
		return this.gerceklesenAyarKullanici;
	}

	public void setGerceklesenAyarKullanici(Integer gerceklesenAyarKullanici) {
		this.gerceklesenAyarKullanici = gerceklesenAyarKullanici;
	}

	public Integer getGerceklesenAyarSuresi() {
		return this.gerceklesenAyarSuresi;
	}

	public void setGerceklesenAyarSuresi(Integer gerceklesenAyarSuresi) {
		this.gerceklesenAyarSuresi = gerceklesenAyarSuresi;
	}

	public Date getGerceklesenAyarTarih() {
		return this.gerceklesenAyarTarih;
	}

	public void setGerceklesenAyarTarih(Date gerceklesenAyarTarih) {
		this.gerceklesenAyarTarih = gerceklesenAyarTarih;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Boolean getMalzemeCheck() {
		return this.malzemeCheck;
	}

	public void setMalzemeCheck(Boolean malzemeCheck) {
		this.malzemeCheck = malzemeCheck;
	}

	public Integer getMalzemeKullanici() {
		return this.malzemeKullanici;
	}

	public void setMalzemeKullanici(Integer malzemeKullanici) {
		this.malzemeKullanici = malzemeKullanici;
	}

	public Date getMalzemeTarih() {
		return this.malzemeTarih;
	}

	public void setMalzemeTarih(Date malzemeTarih) {
		this.malzemeTarih = malzemeTarih;
	}

	public Boolean getOngorulenAyarCheck() {
		return this.ongorulenAyarCheck;
	}

	public void setOngorulenAyarCheck(Boolean ongorulenAyarCheck) {
		this.ongorulenAyarCheck = ongorulenAyarCheck;
	}

	public Integer getOngorulenAyarKullanici() {
		return this.ongorulenAyarKullanici;
	}

	public void setOngorulenAyarKullanici(Integer ongorulenAyarKullanici) {
		this.ongorulenAyarKullanici = ongorulenAyarKullanici;
	}

	public Integer getOngorulenAyarSuresi() {
		return this.ongorulenAyarSuresi;
	}

	public void setOngorulenAyarSuresi(Integer ongorulenAyarSuresi) {
		this.ongorulenAyarSuresi = ongorulenAyarSuresi;
	}

	public Date getOngorulenAyarTarih() {
		return this.ongorulenAyarTarih;
	}

	public void setOngorulenAyarTarih(Date ongorulenAyarTarih) {
		this.ongorulenAyarTarih = ongorulenAyarTarih;
	}

	public Boolean getParametreCheck() {
		return this.parametreCheck;
	}

	public void setParametreCheck(Boolean parametreCheck) {
		this.parametreCheck = parametreCheck;
	}

	public Integer getParametreKullanici() {
		return this.parametreKullanici;
	}

	public void setParametreKullanici(Integer parametreKullanici) {
		this.parametreKullanici = parametreKullanici;
	}

	public Date getParametreTarih() {
		return this.parametreTarih;
	}

	public void setParametreTarih(Date parametreTarih) {
		this.parametreTarih = parametreTarih;
	}

	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	public IsTakipFormuPolietilenKaplama getIsTakipFormuPolietilenKaplama() {
		return isTakipFormuPolietilenKaplama;
	}

	public void setIsTakipFormuPolietilenKaplama(
			IsTakipFormuPolietilenKaplama isTakipFormuPolietilenKaplama) {
		this.isTakipFormuPolietilenKaplama = isTakipFormuPolietilenKaplama;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getEngelYokAciklama() {
		return engelYokAciklama;
	}

	public void setEngelYokAciklama(String engelYokAciklama) {
		this.engelYokAciklama = engelYokAciklama;
	}

	public String getBoruListesiAciklama() {
		return boruListesiAciklama;
	}

	public void setBoruListesiAciklama(String boruListesiAciklama) {
		this.boruListesiAciklama = boruListesiAciklama;
	}

	public String getMalzemeAciklama() {
		return malzemeAciklama;
	}

	public void setMalzemeAciklama(String malzemeAciklama) {
		this.malzemeAciklama = malzemeAciklama;
	}

	public String getParametreAciklama() {
		return parametreAciklama;
	}

	public void setParametreAciklama(String parametreAciklama) {
		this.parametreAciklama = parametreAciklama;
	}

	public String getOngorulenAyarAciklama() {
		return ongorulenAyarAciklama;
	}

	public void setOngorulenAyarAciklama(String ongorulenAyarAciklama) {
		this.ongorulenAyarAciklama = ongorulenAyarAciklama;
	}

	public String getGerceklesenAyarAciklama() {
		return gerceklesenAyarAciklama;
	}

	public void setGerceklesenAyarAciklama(String gerceklesenAyarAciklama) {
		this.gerceklesenAyarAciklama = gerceklesenAyarAciklama;
	}

}