package com.emekboru.jpa;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "document_type")
public class DocumentType implements Serializable{

	private static final long serialVersionUID = 3241987501942032467L;

	
	@Id
	@Column(name = "document_type_id")
	private Integer documentTypeId;
	
	@OneToMany(fetch = FetchType.LAZY)
	private List<OfferRequiredDocument> offerRequiredDocuments;
	
	public DocumentType(){
		
	}

	/**
	 * 		GETTERS AND SETTERS
	 */
	public Integer getDocumentTypeId() {
		return documentTypeId;
	}

	public void setDocumentTypeId(Integer documentTypeId) {
		this.documentTypeId = documentTypeId;
	}

	public List<OfferRequiredDocument> getOfferRequiredDocuments() {
		return offerRequiredDocuments;
	}

	public void setOfferRequiredDocuments(
			List<OfferRequiredDocument> offerRequiredDocuments) {
		this.offerRequiredDocuments = offerRequiredDocuments;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
