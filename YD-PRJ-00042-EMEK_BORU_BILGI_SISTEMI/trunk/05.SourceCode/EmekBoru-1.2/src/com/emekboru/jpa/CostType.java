package com.emekboru.jpa;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "cost_type")
public class CostType  implements Serializable{

	private static final long serialVersionUID = 6294689615726078515L;

	
	@Id
	@Column(name = "cost_type_id")
	@SequenceGenerator(name = "COST_TYPE_GENERATOR", sequenceName = "COST_TYPE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COST_TYPE_GENERATOR")
	private Integer costTypeId;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "short_desc")
	private String shortDesc;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "costType")
	private List<EventCostBreakdown> eventCostBreakdowns;
	
	public CostType(){
		
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof CostType){
			
			if(((CostType) obj).getCostTypeId().equals(this.getCostTypeId()))
				return true;
		}
		return false;
	}


	/**
	 * 		GETTERS AND SETTERS
	 */
	public Integer getCostTypeId() {
		return costTypeId;
	}

	public void setCostTypeId(Integer costTypeId) {
		this.costTypeId = costTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<EventCostBreakdown> getEventCostBreakdowns() {
		return eventCostBreakdowns;
	}

	public void setEventCostBreakdowns(List<EventCostBreakdown> eventCostBreakdowns) {
		this.eventCostBreakdowns = eventCostBreakdowns;
	}
}
