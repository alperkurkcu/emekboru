package com.emekboru.jpa.kaplamagirditest;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the test_hammadde_girdi_epoksi_toz_ekz1 database
 * table.
 * 
 */
@Entity
@Table(name = "test_hammadde_girdi_epoksi_toz_ekz1")
public class TestHammaddeGirdiEpoksiTozEkz1 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_HAMMADDE_GIRDI_EPOKSI_TOZ_EKZ1_ID_GENERATOR", sequenceName = "TEST_HAMMADDE_GIRDI_EPOKSI_TOZ_EKZ1_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_HAMMADDE_GIRDI_EPOKSI_TOZ_EKZ1_ID_GENERATOR")
	@Column(name = "ID")
	private Integer id;

	// @Column(name = "coat_material_id")
	// private Integer coatMaterialId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coat_material_id", referencedColumnName = "coat_material_id", insertable = false)
	private CoatRawMaterial coatRawMaterial;

	private String dtg;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "kur_zamani")
	private String kurZamani;

	@Column(name = "parti_no")
	private String partiNo;

	private Boolean sonuc;

	private String standard;

	@Column(name = "test_sicakligi")
	private String testSicakligi;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi", nullable = false)
	private Date testTarihi;

	private String tg1;

	private String tg2;

	@Column(name = "uretici_spec")
	private String ureticiSpec;

	@Column(name = "rapor_no")
	private String raporNo;

	public TestHammaddeGirdiEpoksiTozEkz1() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Boolean getSonuc() {
		return this.sonuc;
	}

	public void setSonuc(Boolean sonuc) {
		this.sonuc = sonuc;
	}

	public String getStandard() {
		return this.standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getUreticiSpec() {
		return this.ureticiSpec;
	}

	public void setUreticiSpec(String ureticiSpec) {
		this.ureticiSpec = ureticiSpec;
	}

	/**
	 * @return the coatRawMaterial
	 */
	public CoatRawMaterial getCoatRawMaterial() {
		return coatRawMaterial;
	}

	/**
	 * @param coatRawMaterial
	 *            the coatRawMaterial to set
	 */
	public void setCoatRawMaterial(CoatRawMaterial coatRawMaterial) {
		this.coatRawMaterial = coatRawMaterial;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the testTarihi
	 */
	public Date getTestTarihi() {
		return testTarihi;
	}

	/**
	 * @param testTarihi
	 *            the testTarihi to set
	 */
	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	/**
	 * @return the dtg
	 */
	public String getDtg() {
		return dtg;
	}

	/**
	 * @param dtg
	 *            the dtg to set
	 */
	public void setDtg(String dtg) {
		this.dtg = dtg;
	}

	/**
	 * @return the kurZamani
	 */
	public String getKurZamani() {
		return kurZamani;
	}

	/**
	 * @param kurZamani
	 *            the kurZamani to set
	 */
	public void setKurZamani(String kurZamani) {
		this.kurZamani = kurZamani;
	}

	/**
	 * @return the partiNo
	 */
	public String getPartiNo() {
		return partiNo;
	}

	/**
	 * @param partiNo
	 *            the partiNo to set
	 */
	public void setPartiNo(String partiNo) {
		this.partiNo = partiNo;
	}

	/**
	 * @return the testSicakligi
	 */
	public String getTestSicakligi() {
		return testSicakligi;
	}

	/**
	 * @param testSicakligi
	 *            the testSicakligi to set
	 */
	public void setTestSicakligi(String testSicakligi) {
		this.testSicakligi = testSicakligi;
	}

	/**
	 * @return the tg1
	 */
	public String getTg1() {
		return tg1;
	}

	/**
	 * @param tg1
	 *            the tg1 to set
	 */
	public void setTg1(String tg1) {
		this.tg1 = tg1;
	}

	/**
	 * @return the tg2
	 */
	public String getTg2() {
		return tg2;
	}

	/**
	 * @param tg2
	 *            the tg2 to set
	 */
	public void setTg2(String tg2) {
		this.tg2 = tg2;
	}

	/**
	 * @return the raporNo
	 */
	public String getRaporNo() {
		return raporNo;
	}

	/**
	 * @param raporNo
	 *            the raporNo to set
	 */
	public void setRaporNo(String raporNo) {
		this.raporNo = raporNo;
	}

}