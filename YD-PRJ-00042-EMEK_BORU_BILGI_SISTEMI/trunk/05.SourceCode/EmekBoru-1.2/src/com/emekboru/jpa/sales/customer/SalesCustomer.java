package com.emekboru.jpa.sales.customer;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.emekboru.jpa.ElectrodeDustWire;
import com.emekboru.jpa.SoldEdw;
import com.emekboru.jpa.Ulkeler;
import com.emekboru.jpa.rulo.Rulo;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpa.sales.order.SalesProposal;

@Entity
@Table(name = "sales_customer")
public class SalesCustomer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3108773605511262260L;

	@Id
	@Column(name = "sales_customer_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_CUSTOMER_GENERATOR")
	@SequenceGenerator(name = "SALES_CUSTOMER_GENERATOR", sequenceName = "SALES_CUSTOMER_SEQUENCE", allocationSize = 1)
	private int salesCustomerId;

	@Column(name = "short_name")
	private String shortName;

	@Column(name = "commercial_name")
	private String commercialName;

	@Column(name = "code")
	private String code;

	/* JOIN FUNCTIONS */
	@Transient
	private List<SalesCustomerFunctionType> functionTypeList;
	/* end of JOIN FUNCTIONS */

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "classification", referencedColumnName = "class_id")
	private SalesCustomerClass classification;

	@Column(name = "address")
	private String address;

	@Column(name = "city")
	private String city;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ulke_id", referencedColumnName = "ID")
	private Ulkeler ulkeler;

	@Column(name = "tax_office")
	private String taxOffice;

	@Column(name = "tax_number")
	private String taxNumber;

	@Column(name = "record_date")
	private String recordDate;

	@Column(name = "user_last_update")
	private String userLastUpdate;

	@OneToMany(mappedBy = "customer")
	private List<SalesProposal> proposalList;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "salesCustomer")
	private List<ElectrodeDustWire> edws;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "salesCustomer")
	private List<SoldEdw> soldEdws;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "salesCustomer")
	private List<Rulo> rulos;

	@Transient
	private List<SalesItem> salesItems;
	
	@Column(name = "domestic")
	private boolean domestic = true;

	@Column(name = "abroad")
	private boolean abroad = false;

	public SalesCustomer() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		recordDate = sdf.format(cal.getTime());
		ulkeler = new Ulkeler();
	}

	@Override
	public boolean equals(Object o) {
		return this.getSalesCustomerId() == ((SalesCustomer) o)
				.getSalesCustomerId();
	}

	/**
	 * GETTERS
	 */
	public int getSalesCustomerId() {
		return salesCustomerId;
	}

	public String getShortName() {
		return shortName;
	}

	public String getCommercialName() {
		return commercialName;
	}

	public String getCode() {
		return code;
	}

	public SalesCustomerClass getClassification() {
		return classification;
	}

	public String getAddress() {
		return address;
	}

	public String getCity() {
		return city;
	}

	public String getTaxOffice() {
		return taxOffice;
	}

	public String getTaxNumber() {
		return taxNumber;
	}

	public String getRecordDate() {
		return recordDate;
	}

	public String getUserLastUpdate() {
		return userLastUpdate;
	}

	public boolean getDomestic() {
		return domestic;
	}

	public boolean getAbroad() {
		return abroad;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * SETTERS
	 */
	public void setSalesCustomerId(int salesCustomerId) {
		this.salesCustomerId = salesCustomerId;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public void setCommercialName(String commercialName) {
		this.commercialName = commercialName;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setClassification(SalesCustomerClass classification) {
		this.classification = classification;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setTaxOffice(String taxOffice) {
		this.taxOffice = taxOffice;
	}

	public void setTaxNumber(String taxNumber) {
		this.taxNumber = taxNumber;
	}

	public void setRecordDate(String recordDate) {
		this.recordDate = recordDate;
	}

	public void setUserLastUpdate(String userLastUpdate) {
		this.userLastUpdate = userLastUpdate;
	}



	/* ONE-TO-MANY */
	public List<SalesProposal> getProposalList() {
		return proposalList;
	}

	public void setProposalList(List<SalesProposal> proposalList) {
		this.proposalList = proposalList;
	}

	public Ulkeler getUlkeler() {
		return ulkeler;
	}

	public void setUlkeler(Ulkeler ulkeler) {
		this.ulkeler = ulkeler;
	}

	public List<SalesCustomerFunctionType> getFunctionTypeList() {
		return functionTypeList;
	}

	public void setFunctionTypeList(
			List<SalesCustomerFunctionType> functionTypeList) {
		this.functionTypeList = functionTypeList;
	}

	public List<ElectrodeDustWire> getEdws() {
		return edws;
	}

	public void setEdws(List<ElectrodeDustWire> edws) {
		this.edws = edws;
	}

	public List<SoldEdw> getSoldEdws() {
		return soldEdws;
	}

	public void setSoldEdws(List<SoldEdw> soldEdws) {
		this.soldEdws = soldEdws;
	}

	public List<Rulo> getRulos() {
		return rulos;
	}

	public void setRulos(List<Rulo> rulos) {
		this.rulos = rulos;
	}

	public List<SalesItem> getSalesItems() {
		return salesItems;
	}

	public void setSalesItems(List<SalesItem> salesItems) {
		this.salesItems = salesItems;
	}

	public boolean isDomestic() {
		return domestic;
	}

	public void setDomestic(boolean domestic) {
		this.domestic = domestic;
	}

	public boolean isAbroad() {
		return abroad;
	}

	public void setAbroad(boolean abroad) {
		this.abroad = abroad;
	}

}