package com.emekboru.jpa.sales.order;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "sales_payment")
public class SalesPayment implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 2892202471630210915L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_PAYMENT_GENERATOR")
	@SequenceGenerator(name = "SALES_PAYMENT_GENERATOR", sequenceName = "SALES_PAYMENT_SEQUENCE", allocationSize = 1)
	@Column(name = "payment_id")
	private int paymentId;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "payment_date")
	private Date paymentDate;

	@Column(name = "amount")
	private double amount;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "order_id", referencedColumnName = "order_id")
	private SalesOrder order;

	public SalesPayment() {
	}

	@Override
	public boolean equals(Object o) {
		return this.getPaymentId() == ((SalesPayment) o).getPaymentId();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public int getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(int paymentId) {
		this.paymentId = paymentId;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public SalesOrder getOrder() {
		return order;
	}

	public void setOrder(SalesOrder order) {
		this.order = order;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
