package com.emekboru.jpa.sales.order;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sales_external_covering_standard")
public class SalesExternalCoveringStandard implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1277446524038267073L;

	@Id
	@Column(name = "standard_id")
	private int standardId;

	@Column(name = "title")
	private String name;

	public SalesExternalCoveringStandard() {
	}

	@Override
	public boolean equals(Object o) {
		return this.getStandardId() == ((SalesExternalCoveringStandard) o).getStandardId();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public int getStandardId() {
		return standardId;
	}

	public void setStandardId(int standardId) {
		this.standardId = standardId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
