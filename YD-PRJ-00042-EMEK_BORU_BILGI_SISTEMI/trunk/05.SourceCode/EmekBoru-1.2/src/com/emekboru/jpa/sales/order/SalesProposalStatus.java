package com.emekboru.jpa.sales.order;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "sales_proposal_status")
public class SalesProposalStatus implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 4672842004774442473L;

	@Id
	@Column(name = "proposal_status_id")
	private int statusId;

	@Column(name = "title")
	private String name;

	@OneToMany
	private List<SalesProposal> proposalList;

	public SalesProposalStatus() {
	}

	@Override
	public boolean equals(Object o) {
		return this.getStatusId() == ((SalesProposalStatus) o).getStatusId();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SalesProposal> getProposalList() {
		return proposalList;
	}

	public void setProposalList(List<SalesProposal> proposalList) {
		this.proposalList = proposalList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
