package com.emekboru.jpa.personel;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the personel_saglik_bilgileri database table.
 * 
 */
@Entity
@Table(name="personel_saglik_bilgileri")
public class PersonelSaglikBilgileri implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PERSONEL_SAGLIK_BILGILERI_ID_GENERATOR", sequenceName="PERSONEL_SAGLIK_BILGILERI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PERSONEL_SAGLIK_BILGILERI_ID_GENERATOR")
	@Column(name="ID")
	private Integer id;
	@Column(name="aciklama")
	private String aciklama;

	@Column(name="eklenme_tarihi")
	private Timestamp eklenmeTarihi;

	@Column(name="kan_grubu_id")
	private Integer kanGrubuId;

	@Column(name="kimlik_id")
	private Integer kimlikId;

	@Column(name="kullanici_id_ekleyen")
	private Integer kullaniciIdEkleyen;

	@Column(name="kullanici_id_guncelleyen")
	private Integer kullaniciIdGuncelleyen;

	@Column(name="ozur_id")
	private Integer ozurId;

	@Column(name="saglik_durumu")
	private String saglikDurumu;

	@Column(name="son_guncellenme_tarihi")
	private Timestamp sonGuncellenmeTarihi;

	public PersonelSaglikBilgileri() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Timestamp getEklenmeTarihi() {
		return this.eklenmeTarihi;
	}

	public void setEklenmeTarihi(Timestamp eklenmeTarihi) {
		this.eklenmeTarihi = eklenmeTarihi;
	}

	public Integer getKanGrubuId() {
		return this.kanGrubuId;
	}

	public void setKanGrubuId(Integer kanGrubuId) {
		this.kanGrubuId = kanGrubuId;
	}

	public Integer getKimlikId() {
		return this.kimlikId;
	}

	public void setKimlikId(Integer kimlikId) {
		this.kimlikId = kimlikId;
	}

	public Integer getKullaniciIdEkleyen() {
		return this.kullaniciIdEkleyen;
	}

	public void setKullaniciIdEkleyen(Integer kullaniciIdEkleyen) {
		this.kullaniciIdEkleyen = kullaniciIdEkleyen;
	}

	public Integer getKullaniciIdGuncelleyen() {
		return this.kullaniciIdGuncelleyen;
	}

	public void setKullaniciIdGuncelleyen(Integer kullaniciIdGuncelleyen) {
		this.kullaniciIdGuncelleyen = kullaniciIdGuncelleyen;
	}

	public Integer getOzurId() {
		return this.ozurId;
	}

	public void setOzurId(Integer ozurId) {
		this.ozurId = ozurId;
	}

	public String getSaglikDurumu() {
		return this.saglikDurumu;
	}

	public void setSaglikDurumu(String saglikDurumu) {
		this.saglikDurumu = saglikDurumu;
	}

	public Timestamp getSonGuncellenmeTarihi() {
		return this.sonGuncellenmeTarihi;
	}

	public void setSonGuncellenmeTarihi(Timestamp sonGuncellenmeTarihi) {
		this.sonGuncellenmeTarihi = sonGuncellenmeTarihi;
	}

}