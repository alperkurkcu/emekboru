package com.emekboru.jpa.sales.order;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sales_internal_covering_standard")
public class SalesInternalCoveringStandard implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4062662562778659513L;

	@Id
	@Column(name = "standard_id")
	private int standardId;

	@Column(name = "title")
	private String name;

	public SalesInternalCoveringStandard() {
	}

	@Override
	public boolean equals(Object o) {
		return this.getStandardId() == ((SalesInternalCoveringStandard) o).getStandardId();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public int getStandardId() {
		return standardId;
	}

	public void setStandardId(int standardId) {
		this.standardId = standardId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
