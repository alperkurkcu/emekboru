package com.emekboru.jpa.sales.customer;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "sales_contact_people")
public class SalesContactPeople implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3679352597756347584L;

	@Id
	@Column(name = "sales_contact_person_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_CONTACT_PEOPLE_GENERATOR")
	@SequenceGenerator(name = "SALES_CONTACT_PEOPLE_GENERATOR", sequenceName = "SALES_CONTACT_PEOPLE_SEQUENCE", allocationSize = 1)
	private int salesContactPeopleId;

	@Column(name = "firstname")
	private String name;

	@Column(name = "surname")
	private String surname;

	@Column(name = "job")
	private String job;

	@Column(name = "phone")
	private String phone;

	@Column(name = "mobile")
	private String mobile;

	@Column(name = "email")
	private String email;

	@Column(name = "personal_characteristic")
	private String personalCharacteristic;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_customer_id", referencedColumnName = "sales_customer_id")
	private SalesCustomer salesCustomer;

	public SalesContactPeople() {
	}

	@Override
	public boolean equals(Object o) {
		return this.getSalesContactPeopleId() == ((SalesContactPeople) o)
				.getSalesContactPeopleId();
	}

	/**
	 * GETTERS
	 */
	public int getSalesContactPeopleId() {
		return salesContactPeopleId;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getJob() {
		return job;
	}

	public String getPhone() {
		return phone;
	}

	public String getMobile() {
		return mobile;
	}

	public String getEmail() {
		return email;
	}

	public String getPersonalCharacteristic() {
		return personalCharacteristic;
	}

	public SalesCustomer getSalesCustomer() {
		return salesCustomer;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * SETTERS
	 */
	public void setName(String name) {
		this.name = name;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPersonalCharacteristic(String personalCharacteristic) {
		this.personalCharacteristic = personalCharacteristic;
	}

	public void setSalesCustomer(SalesCustomer salesCustomer) {
		this.salesCustomer = salesCustomer;
	}
}
