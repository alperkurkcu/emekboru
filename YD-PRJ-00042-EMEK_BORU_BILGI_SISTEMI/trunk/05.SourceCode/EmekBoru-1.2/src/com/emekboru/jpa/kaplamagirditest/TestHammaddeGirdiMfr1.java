package com.emekboru.jpa.kaplamagirditest;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the test_hammadde_girdi_mfr1 database table.
 * 
 */
@Entity
@Table(name = "test_hammadde_girdi_mfr1")
public class TestHammaddeGirdiMfr1 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_HAMMADDE_GIRDI_MFR1_ID_GENERATOR", sequenceName = "TEST_HAMMADDE_GIRDI_MFR1_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_HAMMADDE_GIRDI_MFR1_ID_GENERATOR")
	@Column(name = "ID")
	private Integer id;

	// @Column(name = "coat_material_id")
	// private Integer coatMaterialId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coat_material_id", referencedColumnName = "coat_material_id", insertable = false)
	private CoatRawMaterial coatRawMaterial;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "kesilen_parca_kutlesi_1")
	private String kesilenParcaKutlesi1;

	@Column(name = "kesilen_parca_kutlesi_2")
	private String kesilenParcaKutlesi2;

	@Column(name = "kesilen_parca_kutlesi_3")
	private String kesilenParcaKutlesi3;

	@Column(name = "kesilen_parca_kutlesi_4")
	private String kesilenParcaKutlesi4;

	@Column(name = "kesilen_parca_kutlesi_5")
	private String kesilenParcaKutlesi5;

	@Column(name = "kutlesel_akis_hizi_1")
	private String kutleselAkisHizi1;

	@Column(name = "kutlesel_akis_hizi_2")
	private String kutleselAkisHizi2;

	@Column(name = "kutlesel_akis_hizi_3")
	private String kutleselAkisHizi3;

	@Column(name = "kutlesel_akis_hizi_4")
	private String kutleselAkisHizi4;

	@Column(name = "kutlesel_akis_hizi_5")
	private String kutleselAkisHizi5;

	@Column(name = "kutlesel_akis_hizi_ortalama")
	private String kutleselAkisHiziOrtalama;

	@Column(name = "parti_no")
	private String partiNo;

	private Boolean sonuc;

	private String standard;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi", nullable = false)
	private Date testTarihi;

	@Column(name = "uretici_spec")
	private String ureticiSpec;

	@Column(name = "degerlendirme_standard")
	private String degerlendirmeStandard;
	
	@Column(name = "rapor_no")
	private String raporNo;

	public TestHammaddeGirdiMfr1() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	/**
	 * @return the coatRawMaterial
	 */
	public CoatRawMaterial getCoatRawMaterial() {
		return coatRawMaterial;
	}

	/**
	 * @param coatRawMaterial
	 *            the coatRawMaterial to set
	 */
	public void setCoatRawMaterial(CoatRawMaterial coatRawMaterial) {
		this.coatRawMaterial = coatRawMaterial;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the sonuc
	 */
	public Boolean getSonuc() {
		return sonuc;
	}

	/**
	 * @param sonuc
	 *            the sonuc to set
	 */
	public void setSonuc(Boolean sonuc) {
		this.sonuc = sonuc;
	}

	/**
	 * @return the standard
	 */
	public String getStandard() {
		return standard;
	}

	/**
	 * @param standard
	 *            the standard to set
	 */
	public void setStandard(String standard) {
		this.standard = standard;
	}

	/**
	 * @return the testTarihi
	 */
	public Date getTestTarihi() {
		return testTarihi;
	}

	/**
	 * @param testTarihi
	 *            the testTarihi to set
	 */
	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	/**
	 * @return the ureticiSpec
	 */
	public String getUreticiSpec() {
		return ureticiSpec;
	}

	/**
	 * @param ureticiSpec
	 *            the ureticiSpec to set
	 */
	public void setUreticiSpec(String ureticiSpec) {
		this.ureticiSpec = ureticiSpec;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the kesilenParcaKutlesi1
	 */
	public String getKesilenParcaKutlesi1() {
		return kesilenParcaKutlesi1;
	}

	/**
	 * @param kesilenParcaKutlesi1
	 *            the kesilenParcaKutlesi1 to set
	 */
	public void setKesilenParcaKutlesi1(String kesilenParcaKutlesi1) {
		this.kesilenParcaKutlesi1 = kesilenParcaKutlesi1;
	}

	/**
	 * @return the kesilenParcaKutlesi2
	 */
	public String getKesilenParcaKutlesi2() {
		return kesilenParcaKutlesi2;
	}

	/**
	 * @param kesilenParcaKutlesi2
	 *            the kesilenParcaKutlesi2 to set
	 */
	public void setKesilenParcaKutlesi2(String kesilenParcaKutlesi2) {
		this.kesilenParcaKutlesi2 = kesilenParcaKutlesi2;
	}

	/**
	 * @return the kesilenParcaKutlesi3
	 */
	public String getKesilenParcaKutlesi3() {
		return kesilenParcaKutlesi3;
	}

	/**
	 * @param kesilenParcaKutlesi3
	 *            the kesilenParcaKutlesi3 to set
	 */
	public void setKesilenParcaKutlesi3(String kesilenParcaKutlesi3) {
		this.kesilenParcaKutlesi3 = kesilenParcaKutlesi3;
	}

	/**
	 * @return the kesilenParcaKutlesi4
	 */
	public String getKesilenParcaKutlesi4() {
		return kesilenParcaKutlesi4;
	}

	/**
	 * @param kesilenParcaKutlesi4
	 *            the kesilenParcaKutlesi4 to set
	 */
	public void setKesilenParcaKutlesi4(String kesilenParcaKutlesi4) {
		this.kesilenParcaKutlesi4 = kesilenParcaKutlesi4;
	}

	/**
	 * @return the kesilenParcaKutlesi5
	 */
	public String getKesilenParcaKutlesi5() {
		return kesilenParcaKutlesi5;
	}

	/**
	 * @param kesilenParcaKutlesi5
	 *            the kesilenParcaKutlesi5 to set
	 */
	public void setKesilenParcaKutlesi5(String kesilenParcaKutlesi5) {
		this.kesilenParcaKutlesi5 = kesilenParcaKutlesi5;
	}

	/**
	 * @return the kutleselAkisHizi1
	 */
	public String getKutleselAkisHizi1() {
		return kutleselAkisHizi1;
	}

	/**
	 * @param kutleselAkisHizi1
	 *            the kutleselAkisHizi1 to set
	 */
	public void setKutleselAkisHizi1(String kutleselAkisHizi1) {
		this.kutleselAkisHizi1 = kutleselAkisHizi1;
	}

	/**
	 * @return the kutleselAkisHizi2
	 */
	public String getKutleselAkisHizi2() {
		return kutleselAkisHizi2;
	}

	/**
	 * @param kutleselAkisHizi2
	 *            the kutleselAkisHizi2 to set
	 */
	public void setKutleselAkisHizi2(String kutleselAkisHizi2) {
		this.kutleselAkisHizi2 = kutleselAkisHizi2;
	}

	/**
	 * @return the kutleselAkisHizi3
	 */
	public String getKutleselAkisHizi3() {
		return kutleselAkisHizi3;
	}

	/**
	 * @param kutleselAkisHizi3
	 *            the kutleselAkisHizi3 to set
	 */
	public void setKutleselAkisHizi3(String kutleselAkisHizi3) {
		this.kutleselAkisHizi3 = kutleselAkisHizi3;
	}

	/**
	 * @return the kutleselAkisHizi4
	 */
	public String getKutleselAkisHizi4() {
		return kutleselAkisHizi4;
	}

	/**
	 * @param kutleselAkisHizi4
	 *            the kutleselAkisHizi4 to set
	 */
	public void setKutleselAkisHizi4(String kutleselAkisHizi4) {
		this.kutleselAkisHizi4 = kutleselAkisHizi4;
	}

	/**
	 * @return the kutleselAkisHizi5
	 */
	public String getKutleselAkisHizi5() {
		return kutleselAkisHizi5;
	}

	/**
	 * @param kutleselAkisHizi5
	 *            the kutleselAkisHizi5 to set
	 */
	public void setKutleselAkisHizi5(String kutleselAkisHizi5) {
		this.kutleselAkisHizi5 = kutleselAkisHizi5;
	}

	/**
	 * @return the kutleselAkisHiziOrtalama
	 */
	public String getKutleselAkisHiziOrtalama() {
		return kutleselAkisHiziOrtalama;
	}

	/**
	 * @param kutleselAkisHiziOrtalama
	 *            the kutleselAkisHiziOrtalama to set
	 */
	public void setKutleselAkisHiziOrtalama(String kutleselAkisHiziOrtalama) {
		this.kutleselAkisHiziOrtalama = kutleselAkisHiziOrtalama;
	}

	/**
	 * @return the partiNo
	 */
	public String getPartiNo() {
		return partiNo;
	}

	/**
	 * @param partiNo
	 *            the partiNo to set
	 */
	public void setPartiNo(String partiNo) {
		this.partiNo = partiNo;
	}

	/**
	 * @return the degerlendirmeStandard
	 */
	public String getDegerlendirmeStandard() {
		return degerlendirmeStandard;
	}

	/**
	 * @param degerlendirmeStandard
	 *            the degerlendirmeStandard to set
	 */
	public void setDegerlendirmeStandard(String degerlendirmeStandard) {
		this.degerlendirmeStandard = degerlendirmeStandard;
	}

	/**
	 * @return the raporNo
	 */
	public String getRaporNo() {
		return raporNo;
	}

	/**
	 * @param raporNo the raporNo to set
	 */
	public void setRaporNo(String raporNo) {
		this.raporNo = raporNo;
	}

}