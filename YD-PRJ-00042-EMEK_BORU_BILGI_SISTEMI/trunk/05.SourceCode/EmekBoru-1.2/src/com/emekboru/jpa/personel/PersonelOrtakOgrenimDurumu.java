package com.emekboru.jpa.personel;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the personel_ortak_ogrenim_durumu database table.
 * 
 */
@Entity
@Table(name = "personel_ortak_ogrenim_durumu")
public class PersonelOrtakOgrenimDurumu implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "PERSONEL_ORTAK_OGRENIM_DURUMU_ID_GENERATOR", sequenceName = "PERSONEL_ORTAK_OGRENIM_DURUMU_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PERSONEL_ORTAK_OGRENIM_DURUMU_ID_GENERATOR")
	@Column(name = "ID")
	private Integer id;

	@Column(name = "eklenme_tarihi")
	private Timestamp eklenmeTarihi;

	@Column(name = "kullanici_id_ekleyen")
	private Integer kullaniciIdEkleyen;

	@Column(name = "kullanici_id_guncelleyen")
	private Integer kullaniciIdGuncelleyen;

	@Column(name = "ogrenim_durumu_adi")
	private String ogrenimDurumuAdi;

	@Column(name = "son_guncellenme_tarihi")
	private Timestamp sonGuncellenmeTarihi;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "personelOrtakOgrenimDurumu", cascade = CascadeType.ALL)
	private List<PersonelEgitimBilgileri> personelEgitimBilgileri;

	public PersonelOrtakOgrenimDurumu() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getEklenmeTarihi() {
		return this.eklenmeTarihi;
	}

	public void setEklenmeTarihi(Timestamp eklenmeTarihi) {
		this.eklenmeTarihi = eklenmeTarihi;
	}

	public Integer getKullaniciIdEkleyen() {
		return this.kullaniciIdEkleyen;
	}

	public void setKullaniciIdEkleyen(Integer kullaniciIdEkleyen) {
		this.kullaniciIdEkleyen = kullaniciIdEkleyen;
	}

	public Integer getKullaniciIdGuncelleyen() {
		return this.kullaniciIdGuncelleyen;
	}

	public void setKullaniciIdGuncelleyen(Integer kullaniciIdGuncelleyen) {
		this.kullaniciIdGuncelleyen = kullaniciIdGuncelleyen;
	}

	public String getOgrenimDurumuAdi() {
		return this.ogrenimDurumuAdi;
	}

	public void setOgrenimDurumuAdi(String ogrenimDurumuAdi) {
		this.ogrenimDurumuAdi = ogrenimDurumuAdi;
	}

	public Timestamp getSonGuncellenmeTarihi() {
		return this.sonGuncellenmeTarihi;
	}

	public void setSonGuncellenmeTarihi(Timestamp sonGuncellenmeTarihi) {
		this.sonGuncellenmeTarihi = sonGuncellenmeTarihi;
	}

	public List<PersonelEgitimBilgileri> getPersonelEgitimBilgileri() {
		return personelEgitimBilgileri;
	}

	public void setPersonelEgitimBilgileri(
			List<PersonelEgitimBilgileri> personelEgitimBilgileri) {
		this.personelEgitimBilgileri = personelEgitimBilgileri;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}