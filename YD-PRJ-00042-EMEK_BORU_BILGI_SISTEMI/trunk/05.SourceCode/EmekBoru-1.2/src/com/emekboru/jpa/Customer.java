package com.emekboru.jpa;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the company database table.
 */

@Entity
@Table(name = "customer")
public class Customer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "customer_id")
	@SequenceGenerator(name = "COMPANY_GENERATOR", sequenceName = "COMPANY_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMPANY_GENERATOR")
	private int customerId;

	private String address;

	@Column(name = "business_id")
	private Integer businessId;

	@Column(name = "city")
	private String city;

	@Column(name = "e_mail")
	private String eMail;

	@Column(name = "fax_number")
	private String faxNumber;

	private String name;

	@Temporal(TemporalType.DATE)
	@Column(name = "partners_since")
	private Date partnersSince;

	@Column(name = "phone_number")
	private String phoneNumber;

	@Column(name = "postal_code")
	private String postalCode;

	@Column(name = "website")
	private String website;

	@Column(name = "business_type")
	private String businessType;

	@Column(name = "is_government")
	private boolean isGovernment;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ulke_id", referencedColumnName = "ID")
	private Ulkeler ulkeler;

	// @OneToMany(fetch = FetchType.LAZY, mappedBy = "customer")
	// private List<SoldEdw> soldEdws;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "customer")
	private List<Order> orders;

	public Customer() {

		ulkeler = new Ulkeler();
	}

	/**
	 * GETTERS AND SETTERS
	 */
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getBusinessId() {
		return this.businessId;
	}

	public void setBusinessId(Integer businessId) {
		this.businessId = businessId;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEMail() {
		return this.eMail;
	}

	public void setEMail(String eMail) {
		this.eMail = eMail;
	}

	public String getFaxNumber() {
		return this.faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getPartnersSince() {
		return this.partnersSince;
	}

	public void setPartnersSince(Date partnersSince) {
		this.partnersSince = partnersSince;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public boolean getIsGovernment() {
		return isGovernment;
	}

	public void setIsGovernment(boolean isGovernment) {
		this.isGovernment = isGovernment;
	}

	public void setGovernment(boolean isGovernment) {
		this.isGovernment = isGovernment;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	public Ulkeler getUlkeler() {
		return ulkeler;
	}

	public void setUlkeler(Ulkeler ulkeler) {
		this.ulkeler = ulkeler;
	}

}