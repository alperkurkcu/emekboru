package com.emekboru.jpa.stok;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the stok_tanimlar_depo_tanimlari database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "StokTanimlarDepoTanimlari.findAll", query = "SELECT r FROM StokTanimlarDepoTanimlari r order by r.depoAdi") })
@Table(name = "stok_tanimlar_depo_tanimlari")
public class StokTanimlarDepoTanimlari implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "STOK_TANIMLAR_DEPO_TANIMLARI_ID_GENERATOR", sequenceName = "STOK_TANIMLAR_DEPO_TANIMLARI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STOK_TANIMLAR_DEPO_TANIMLARI_ID_GENERATOR")
	@Column(name = "ID")
	private Integer id;

	@Column(name = "depo_adi")
	private String depoAdi;

	// @Column(name = "depo_tipi")
	// private Integer depoTipi;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	public StokTanimlarDepoTanimlari() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDepoAdi() {
		return this.depoAdi;
	}

	public void setDepoAdi(String depoAdi) {
		this.depoAdi = depoAdi;
	}

	public Timestamp getEklemeZamani() {
		return eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

}