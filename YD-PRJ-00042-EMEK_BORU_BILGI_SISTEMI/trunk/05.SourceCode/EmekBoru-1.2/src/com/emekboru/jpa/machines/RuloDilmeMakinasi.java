package com.emekboru.jpa.machines;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.rulo.Rulo;

/**
 * The persistent class for the rulo_dilme_makinasi database table.
 * 
 */
@Entity
@Table(name = "rulo_dilme_makinasi")
public class RuloDilmeMakinasi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "RULO_DILME_MAKINASI_ID_GENERATOR", sequenceName = "RULO_DILME_MAKINASI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RULO_DILME_MAKINASI_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name = "rulo_id")
	// private Integer ruloId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "rulo_id", referencedColumnName = "rulo_id", insertable = false)
	private Rulo rulo;

	@Column(name = "rulo_dilme_sayisi")
	private Integer ruloDilmeSayisi;

	@Column(name = "tumu")
	private Boolean tumu;

	@Column(name = "rulo_dilinecek_miktar")
	private Integer ruloDilinecekMiktar;

	@Column(name = "dilme_ebadi_bir")
	private Integer dilmeEbadiBir;

	@Column(name = "dilme_ebadi_iki")
	private Integer dilmeEbadiIki;

	@Column(name = "dilme_ebadi_uc")
	private Integer dilmeEbadiUc;

	@Column(name = "dilme_ebadi_dort")
	private Integer dilmeEbadiDort;

	public RuloDilmeMakinasi() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	public Rulo getRulo() {
		return rulo;
	}

	public void setRulo(Rulo rulo) {
		this.rulo = rulo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getRuloDilmeSayisi() {
		return ruloDilmeSayisi;
	}

	public void setRuloDilmeSayisi(Integer ruloDilmeSayisi) {
		this.ruloDilmeSayisi = ruloDilmeSayisi;
	}

	public Boolean getTumu() {
		return tumu;
	}

	public void setTumu(Boolean tumu) {
		this.tumu = tumu;
	}

	public Integer getRuloDilinecekMiktar() {
		return ruloDilinecekMiktar;
	}

	public void setRuloDilinecekMiktar(Integer ruloDilinecekMiktar) {
		this.ruloDilinecekMiktar = ruloDilinecekMiktar;
	}

	public Integer getDilmeEbadiBir() {
		return dilmeEbadiBir;
	}

	public void setDilmeEbadiBir(Integer dilmeEbadiBir) {
		this.dilmeEbadiBir = dilmeEbadiBir;
	}

	public Integer getDilmeEbadiIki() {
		return dilmeEbadiIki;
	}

	public void setDilmeEbadiIki(Integer dilmeEbadiIki) {
		this.dilmeEbadiIki = dilmeEbadiIki;
	}

	public Integer getDilmeEbadiUc() {
		return dilmeEbadiUc;
	}

	public void setDilmeEbadiUc(Integer dilmeEbadiUc) {
		this.dilmeEbadiUc = dilmeEbadiUc;
	}

	public Integer getDilmeEbadiDort() {
		return dilmeEbadiDort;
	}

	public void setDilmeEbadiDort(Integer dilmeEbadiDort) {
		this.dilmeEbadiDort = dilmeEbadiDort;
	}

}