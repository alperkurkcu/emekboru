package com.emekboru.jpa.common;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.employee.EmployeeHealth;

/**
 * The persistent class for the common_blood_group database table.
 * 
 */
@Entity
@Table(name = "common_blood_group")
@NamedQuery(name = "CommonBloodGroup.findAll", query = "SELECT c FROM CommonBloodGroup c")
public class CommonBloodGroup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "COMMON_BLOOD_GROUP_ID_GENERATOR", sequenceName = "EMPLOYEE_DISCIPLINE_SEQ")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMMON_BLOOD_GROUP_ID_GENERATOR")
	@Column(name = "id")
	private Integer id;

	@Column(name = "description")
	private String description;

	@Column(name = "ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "name")
	private String name;

	// bi-directional many-to-one association to EmployeeHealth
	@OneToMany(mappedBy = "commonBloodGroup")
	private List<EmployeeHealth> employeeHealths;

	public CommonBloodGroup() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<EmployeeHealth> getEmployeeHealths() {
		return this.employeeHealths;
	}

	public void setEmployeeHealths(List<EmployeeHealth> employeeHealths) {
		this.employeeHealths = employeeHealths;
	}

	public EmployeeHealth addEmployeeHealth(EmployeeHealth employeeHealth) {
		getEmployeeHealths().add(employeeHealth);
		employeeHealth.setCommonBloodGroup(this);

		return employeeHealth;
	}

	public EmployeeHealth removeEmployeeHealth(EmployeeHealth employeeHealth) {
		getEmployeeHealths().remove(employeeHealth);
		employeeHealth.setCommonBloodGroup(null);

		return employeeHealth;
	}

}