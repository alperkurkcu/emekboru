package com.emekboru.jpa.satinalma;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the satinalma_sarf_merkezi database table.
 * 
 */
@Entity
@Table(name="satinalma_sarf_merkezi")
@NamedQuery(name="SatinalmaSarfMerkezi.findAll", query="SELECT s FROM SatinalmaSarfMerkezi s")
public class SatinalmaSarfMerkezi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SATINALMA_SARF_MERKEZI_ID_GENERATOR", sequenceName="SATINALMA_SARF_MERKEZI_SEQ", allocationSize= 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SATINALMA_SARF_MERKEZI_ID_GENERATOR")
	@Column(name="id")
	private Integer id;

	@Column(name="description")
	private String description;

	@Column(name="ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name="ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@Column(name="guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name="guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name="name")
	private String name;

	public SatinalmaSarfMerkezi() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}