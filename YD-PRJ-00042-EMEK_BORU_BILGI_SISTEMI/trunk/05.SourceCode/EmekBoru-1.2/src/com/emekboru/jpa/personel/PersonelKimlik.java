package com.emekboru.jpa.personel;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the personel_kimlik database table.
 * 
 */
@Entity
@Table(name = "personel_kimlik")
public class PersonelKimlik implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "PERSONEL_KIMLIK_ID_GENERATOR", sequenceName = "PERSONEL_KIMLIK_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PERSONEL_KIMLIK_ID_GENERATOR")
	@Column(name = "ID")
	private Integer id;

	@Column(name = "adi")
	private String adi;

	@Column(name = "aktif")
	private Boolean aktif;

	@Column(name = "ana_adi")
	private String anaAdi;

	@Column(name = "baba_adi")
	private String babaAdi;

	@Column(name = "banka_aciklama")
	private String bankaAciklama;

	@Column(name = "banka_bilgileri")
	private String bankaBilgileri;

	@Column(name = "calisma_durumu_id")
	private Integer calismaDurumuId;

	@Column(name = "cinsiyet_id")
	private Integer cinsiyetId;

	@Temporal(TemporalType.DATE)
	@Column(name = "dogum_tarihi")
	private Date dogumTarihi;

	@Column(name = "eklenme_tarihi")
	private Timestamp eklenmeTarihi;

	@Column(name = "kullanici_id_ekleyen")
	private Integer kullaniciIdEkleyen;

	@Column(name = "kullanici_id_guncelleyen")
	private Integer kullaniciIdGuncelleyen;

	@Column(name = "referans_bilgileri")
	private String referansBilgileri;

	@Column(name = "son_guncellenme_tarihi")
	private Timestamp sonGuncellenmeTarihi;

	@Column(name = "soyadi")
	private String soyadi;

	@Column(name = "tc_kimlik_numarasi")
	private String tcKimlikNumarasi;

	@Column(name = "varsayilan_adres")
	private String varsayilanAdres;

	@Column(name = "varsayilan_email")
	private String varsayilanEmail;

	@Column(name = "varsayilan_tel")
	private String varsayilanTel;

//	// bi-directional many-to-one association to PersonelKimlikAdre
//	@OneToMany(mappedBy = "personelKimlik", orphanRemoval = true)
//	private List<PersonelKimlikAdres> personelKimlikAdres;
//
//	// bi-directional many-to-one association to PersonelKimlikEmail
//	@OneToMany(mappedBy = "personelKimlik", orphanRemoval = true)
//	private List<PersonelKimlikEmail> personelKimlikEmails;
//
//	// bi-directional many-to-one association to PersonelKimlikTelefon
//	@OneToMany(mappedBy = "personelKimlik", orphanRemoval = true)
//	private List<PersonelKimlikTelefon> personelKimlikTelefons;

	// @Transient
	// private PersonelKimlikAdres defaultAdres;
	// @Transient
	// private PersonelKimlikEmail defaultEmail;
	// @Transient
	// private PersonelKimlikTelefon defaultTelefon;

	public PersonelKimlik() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAdi() {
		return this.adi;
	}

	public void setAdi(String adi) {
		this.adi = adi;
	}

	public Boolean getAktif() {
		return this.aktif;
	}

	public void setAktif(Boolean aktif) {
		this.aktif = aktif;
	}

	public String getAnaAdi() {
		return this.anaAdi;
	}

	public void setAnaAdi(String anaAdi) {
		this.anaAdi = anaAdi;
	}

	public String getBabaAdi() {
		return this.babaAdi;
	}

	public void setBabaAdi(String babaAdi) {
		this.babaAdi = babaAdi;
	}

	public String getBankaAciklama() {
		return this.bankaAciklama;
	}

	public void setBankaAciklama(String bankaAciklama) {
		this.bankaAciklama = bankaAciklama;
	}

	public String getBankaBilgileri() {
		return this.bankaBilgileri;
	}

	public void setBankaBilgileri(String bankaBilgileri) {
		this.bankaBilgileri = bankaBilgileri;
	}

	public Integer getCalismaDurumuId() {
		return this.calismaDurumuId;
	}

	public void setCalismaDurumuId(Integer calismaDurumuId) {
		this.calismaDurumuId = calismaDurumuId;
	}

	public Integer getCinsiyetId() {
		return this.cinsiyetId;
	}

	public void setCinsiyetId(Integer cinsiyetId) {
		this.cinsiyetId = cinsiyetId;
	}

	public Date getDogumTarihi() {
		return this.dogumTarihi;
	}

	public void setDogumTarihi(Date dogumTarihi) {
		this.dogumTarihi = dogumTarihi;
	}

	public Timestamp getEklenmeTarihi() {
		return this.eklenmeTarihi;
	}

	public void setEklenmeTarihi(Timestamp eklenmeTarihi) {
		this.eklenmeTarihi = eklenmeTarihi;
	}

	public Integer getKullaniciIdEkleyen() {
		return this.kullaniciIdEkleyen;
	}

	public void setKullaniciIdEkleyen(Integer kullaniciIdEkleyen) {
		this.kullaniciIdEkleyen = kullaniciIdEkleyen;
	}

	public Integer getKullaniciIdGuncelleyen() {
		return this.kullaniciIdGuncelleyen;
	}

	public void setKullaniciIdGuncelleyen(Integer kullaniciIdGuncelleyen) {
		this.kullaniciIdGuncelleyen = kullaniciIdGuncelleyen;
	}

	public String getReferansBilgileri() {
		return this.referansBilgileri;
	}

	public void setReferansBilgileri(String referansBilgileri) {
		this.referansBilgileri = referansBilgileri;
	}

	public Timestamp getSonGuncellenmeTarihi() {
		return this.sonGuncellenmeTarihi;
	}

	public void setSonGuncellenmeTarihi(Timestamp sonGuncellenmeTarihi) {
		this.sonGuncellenmeTarihi = sonGuncellenmeTarihi;
	}

	public String getSoyadi() {
		return this.soyadi;
	}

	public void setSoyadi(String soyadi) {
		this.soyadi = soyadi;
	}

	public String getTcKimlikNumarasi() {
		return this.tcKimlikNumarasi;
	}

	public void setTcKimlikNumarasi(String tcKimlikNumarasi) {
		this.tcKimlikNumarasi = tcKimlikNumarasi;
	}

	public String getVarsayilanAdres() {
		return this.varsayilanAdres;
	}

	public void setVarsayilanAdres(String varsayilanAdres) {
		this.varsayilanAdres = varsayilanAdres;
	}

	public String getVarsayilanEmail() {
		return this.varsayilanEmail;
	}

	public void setVarsayilanEmail(String varsayilanEmail) {
		this.varsayilanEmail = varsayilanEmail;
	}

	public String getVarsayilanTel() {
		return this.varsayilanTel;
	}

	public void setVarsayilanTel(String varsayilanTel) {
		this.varsayilanTel = varsayilanTel;
	}

//	public List<PersonelKimlikAdres> getPersonelKimlikAdres() {
//		return this.personelKimlikAdres;
//	}
//
//	public void setPersonelKimlikAdres(
//			List<PersonelKimlikAdres> personelKimlikAdres) {
//		this.personelKimlikAdres = personelKimlikAdres;
//	}
//
//	public List<PersonelKimlikEmail> getPersonelKimlikEmails() {
//		return this.personelKimlikEmails;
//	}
//
//	public void setPersonelKimlikEmails(
//			List<PersonelKimlikEmail> personelKimlikEmails) {
//		this.personelKimlikEmails = personelKimlikEmails;
//	}
//
//	public List<PersonelKimlikTelefon> getPersonelKimlikTelefons() {
//		return this.personelKimlikTelefons;
//	}
//
//	public void setPersonelKimlikTelefons(
//			List<PersonelKimlikTelefon> personelKimlikTelefons) {
//		this.personelKimlikTelefons = personelKimlikTelefons;
//	}
//
//	/**
//	 * @return the defaultAdres
//	 */
//	public PersonelKimlikAdres getDefaultAdres() {
//		for (PersonelKimlikAdres adres : personelKimlikAdres) {
//			if (adres.getVarsayilanMi() == true)
//				defaultAdres = adres;
//		}
//		return defaultAdres;
//	}
//
//	/**
//	 * @param defaultAdres
//	 *            the defaultAdres to set
//	 */
//	public void setDefaultAdres(PersonelKimlikAdres defaultAdres) {
//		this.defaultAdres = defaultAdres;
//	}
//
//	/**
//	 * @return the defaultEmail
//	 */
//	public PersonelKimlikEmail getDefaultEmail() {
//		for (PersonelKimlikEmail email : personelKimlikEmails) {
//			if (email.getVarsayilanMi() == true)
//				defaultEmail = email;
//		}
//		return defaultEmail;
//	}
//
//	/**
//	 * @param defaultEmail
//	 *            the defaultEmail to set
//	 */
//	public void setDefaultEmail(PersonelKimlikEmail defaultEmail) {
//		this.defaultEmail = defaultEmail;
//	}
//
//	/**
//	 * @return the defaultTelefon
//	 */
//	public PersonelKimlikTelefon getDefaultTelefon() {
//		for (PersonelKimlikTelefon tel : personelKimlikTelefons) {
//			if (tel.getVarsayilanMi() == true)
//				defaultTelefon = tel;
//		}
//		return defaultTelefon;
//	}
//
//	/**
//	 * @param defaultTelefon
//	 *            the defaultTelefon to set
//	 */
//	public void setDefaultTelefon(PersonelKimlikTelefon defaultTelefon) {
//		this.defaultTelefon = defaultTelefon;
//	}

}