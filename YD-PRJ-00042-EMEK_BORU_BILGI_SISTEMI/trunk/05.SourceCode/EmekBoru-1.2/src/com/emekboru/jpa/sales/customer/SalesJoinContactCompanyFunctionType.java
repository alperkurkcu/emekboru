package com.emekboru.jpa.sales.customer;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "sales_join_contact_company_function_type")
public class SalesJoinContactCompanyFunctionType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4048012944388987365L;

	@Id
	@Column(name = "join_id")
	@SequenceGenerator(name = "SALES_JOIN_CONTACT_COMPANY_FUNCTION_TYPE_GENERATOR", sequenceName = "SALES_JOIN_CONTACT_COMPANY_FUNCTION_TYPE_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_JOIN_CONTACT_COMPANY_FUNCTION_TYPE_GENERATOR")
	private int joinId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "function_id", referencedColumnName = "function_id", updatable = false)
	private SalesCustomerFunctionType functionType;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "contact_company_id", referencedColumnName = "sales_contact_company_id", updatable = false)
	private SalesContactCompany contactCompany;

	public SalesJoinContactCompanyFunctionType() {
		functionType = new SalesCustomerFunctionType();
		contactCompany = new SalesContactCompany();
	}

	/**
	 * GETTERS AND SETTERS
	 */

	public int getJoinId() {
		return joinId;
	}

	public SalesCustomerFunctionType getFunctionType() {
		return functionType;
	}

	public SalesContactCompany getContactCompany() {
		return contactCompany;
	}

	public void setJoinId(Integer joinId) {
		this.joinId = joinId;
	}

	public void setFunctionType(SalesCustomerFunctionType functionType) {
		this.functionType = functionType;
	}

	public void setContactCompany(SalesContactCompany contactCompany) {
		this.contactCompany = contactCompany;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
