package com.emekboru.jpa;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the yetki_menu_kullanici_erisim database table.
 * 
 */

@Entity
@NamedQueries({
	@NamedQuery(name = "YetkiMenuKullaniciErisim.findAll", query = "select o from YetkiMenuKullaniciErisim o"),
	@NamedQuery(name = "YetkiMenuKullaniciErisim.findById", query = "select o from YetkiMenuKullaniciErisim o where o.id=:prmYetkiMenuKullaniciErisimId"),
	@NamedQuery(name = "YetkiMenuKullaniciErisim.findByMenuId", query = "select o from YetkiMenuKullaniciErisim o where o.yetkiMenu.id=:prmYetkiMenuId"),
	@NamedQuery(name = "YetkiMenuKullaniciErisim.findByUserId", query = "select o from YetkiMenuKullaniciErisim o where o.systemUser.id=:prmUserId"),
	@NamedQuery(name = "YetkiMenuKullaniciErisim.findUserMenuRolesByUserIdAndMenuId", query = "select o from YetkiMenuKullaniciErisim o where o.systemUser.id=:prmUserId and o.yetkiMenu.id=:prmMenuId"),
	@NamedQuery(name = "YetkiMenuKullaniciErisim.findUserMenuRolesByUserIdAndMenuIdANdRoleId", query = "select o from YetkiMenuKullaniciErisim o where o.systemUser.id=:prmUserId and o.yetkiMenu.id=:prmMenuId and o.lkYetkiRol.id=:prmRoleId")

})
@Table(name="yetki_menu_kullanici_erisim")
public class YetkiMenuKullaniciErisim implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="YETKI_MENU_KULLANICI_ERISIM_GENERATOR",sequenceName="yetki_menu_kullanici_erisim_seq", allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="YETKI_MENU_KULLANICI_ERISIM_GENERATOR")
	@Column(name="ID")
	private Integer id;

	@Column(name="ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name="ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@Column(name="guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name="guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_rol", referencedColumnName = "ID")
	private LkYetkiRol lkYetkiRol;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_system_user", referencedColumnName = "ID")
	private SystemUser systemUser;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_yetki_menu", referencedColumnName = "ID")
	private YetkiMenu yetkiMenu;
	

	public YetkiMenuKullaniciErisim() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public LkYetkiRol getLkYetkiRol() {
		return lkYetkiRol;
	}

	public void setLkYetkiRol(LkYetkiRol lkYetkiRol) {
		this.lkYetkiRol = lkYetkiRol;
	}

	public SystemUser getSystemUser() {
		return systemUser;
	}

	public void setSystemUser(SystemUser systemUser) {
		this.systemUser = systemUser;
	}

	public YetkiMenu getYetkiMenu() {
		return yetkiMenu;
	}

	public void setYetkiMenu(YetkiMenu yetkiMenu) {
		this.yetkiMenu = yetkiMenu;
	}

}