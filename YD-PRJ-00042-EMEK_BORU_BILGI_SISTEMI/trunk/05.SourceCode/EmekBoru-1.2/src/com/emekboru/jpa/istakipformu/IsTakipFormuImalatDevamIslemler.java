package com.emekboru.jpa.istakipformu;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the is_takip_formu_imalat_devam_islemler database
 * table.
 * 
 */
@Entity
@Table(name = "is_takip_formu_imalat_devam_islemler")
public class IsTakipFormuImalatDevamIslemler implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IS_TAKIP_FORMU_IMALAT_DEVAM_ISLEMLER_ID_GENERATOR", sequenceName = "IS_TAKIP_FORMU_IMALAT_DEVAM_ISLEMLER_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IS_TAKIP_FORMU_IMALAT_DEVAM_ISLEMLER_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(length = 2147483647, name = "aciklamalar")
	private String aciklamalar;

	@Column(name = "boru_boyu")
	private String boruBoyu;

	private Boolean check;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name = "sales_item_id", nullable = false)
	// private Integer salesItemId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	private SalesItem salesItem;

	private String fl;

	private String ht;

	// private Integer kullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser kullaniciEmployee;

	private String mut;

	@Column(name = "pipe_id")
	private Integer pipeId;

	@Temporal(TemporalType.DATE)
	private Date tarih;

	@Column(name = "torna_dik_sap")
	private String tornaDikSap;

	@Column(name = "torna_kok_y")
	private String tornaKokY;

	public IsTakipFormuImalatDevamIslemler() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklamalar() {
		return this.aciklamalar;
	}

	public void setAciklamalar(String aciklamalar) {
		this.aciklamalar = aciklamalar;
	}

	public String getBoruBoyu() {
		return this.boruBoyu;
	}

	public void setBoruBoyu(String boruBoyu) {
		this.boruBoyu = boruBoyu;
	}

	public Boolean getCheck() {
		return this.check;
	}

	public void setCheck(Boolean check) {
		this.check = check;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public String getFl() {
		return this.fl;
	}

	public void setFl(String fl) {
		this.fl = fl;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public String getHt() {
		return this.ht;
	}

	public void setHt(String ht) {
		this.ht = ht;
	}

	public String getMut() {
		return this.mut;
	}

	public void setMut(String mut) {
		this.mut = mut;
	}

	public Integer getPipeId() {
		return this.pipeId;
	}

	public void setPipeId(Integer pipeId) {
		this.pipeId = pipeId;
	}

	public Date getTarih() {
		return this.tarih;
	}

	public void setTarih(Date tarih) {
		this.tarih = tarih;
	}

	public String getTornaDikSap() {
		return this.tornaDikSap;
	}

	public void setTornaDikSap(String tornaDikSap) {
		this.tornaDikSap = tornaDikSap;
	}

	public String getTornaKokY() {
		return this.tornaKokY;
	}

	public void setTornaKokY(String tornaKokY) {
		this.tornaKokY = tornaKokY;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the salesItem
	 */
	public SalesItem getSalesItem() {
		return salesItem;
	}

	/**
	 * @param salesItem
	 *            the salesItem to set
	 */
	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	/**
	 * @return the kullaniciEmployee
	 */
	public SystemUser getKullaniciEmployee() {
		return kullaniciEmployee;
	}

	/**
	 * @param kullaniciEmployee
	 *            the kullaniciEmployee to set
	 */
	public void setKullaniciEmployee(SystemUser kullaniciEmployee) {
		this.kullaniciEmployee = kullaniciEmployee;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}