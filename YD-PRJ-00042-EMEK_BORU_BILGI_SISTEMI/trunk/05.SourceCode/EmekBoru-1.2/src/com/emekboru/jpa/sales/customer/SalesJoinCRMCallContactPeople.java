package com.emekboru.jpa.sales.customer;

import java.io.Serializable;

import javax.persistence.*;

import com.emekboru.jpa.sales.customer.SalesContactPeople;

@Entity
@Table(name = "sales_join_crm_call_contact_people")
public class SalesJoinCRMCallContactPeople implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4155216010500223012L;

	@Id
	@Column(name = "join_id")
	@SequenceGenerator(name = "SALES_JOIN_CRM_CALL_CONTACT_PEOPLE_GENERATOR", sequenceName = "SALES_JOIN_CRM_CALL_CONTACT_PEOPLE_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_JOIN_CRM_CALL_CONTACT_PEOPLE_GENERATOR")
	private int joinId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "contact_people_id", referencedColumnName = "sales_contact_person_id", updatable = false)
	private SalesContactPeople contactPeople;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "call_id", referencedColumnName = "sales_crm_call_id", updatable = false)
	private SalesCRMCall call;

	public SalesJoinCRMCallContactPeople() {
		contactPeople = new SalesContactPeople();
		call = new SalesCRMCall();
	}

	/**
	 * GETTERS AND SETTERS
	 */

	public int getJoinId() {
		return joinId;
	}

	public SalesCRMCall getCall() {
		return call;
	}

	public SalesContactPeople getContactPeople() {
		return contactPeople;
	}

	public void setJoinId(Integer joinId) {
		this.joinId = joinId;
	}

	public void setCall(SalesCRMCall call) {
		this.call = call;
	}

	public void setContactPeople(SalesContactPeople contactPeople) {
		this.contactPeople = contactPeople;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
