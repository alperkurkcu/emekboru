package com.emekboru.jpa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.emekboru.jpa.rulo.RuloPipeLink;

/**
 * The persistent class for the test_agirlik_dusurme_sonuc database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "TestAgirlikDusurmeSonuc.findAll", query = "SELECT r FROM TestAgirlikDusurmeSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId"),
		@NamedQuery(name = "TestAgirlikDusurmeSonuc.getBySalesItemGlobalId", query = "SELECT r FROM TestAgirlikDusurmeSonuc r WHERE r.pipe.salesItem.itemId=:prmSalesItem and r.bagliGlobalId.globalId=:prmGlobalId order by r.partiNo"),
		@NamedQuery(name = "TestAgirlikDusurmeSonuc.getByDokumNo", query = "SELECT r FROM TestAgirlikDusurmeSonuc r WHERE r.pipe.ruloPipeLink.rulo.ruloDokumNo=:prmDokumNo order by r.pipe.pipeBarkodNo, r.bagliTestId.code") })
@Table(name = "test_agirlik_dusurme_sonuc")
public class TestAgirlikDusurmeSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_AGIRLIK_DUSURME_SONUC_ID_GENERATOR", sequenceName = "TEST_AGIRLIK_DUSURME_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_AGIRLIK_DUSURME_SONUC_ID_GENERATOR")
	@Column(name = "ID")
	private Integer id;

	@Column(name = "aciklama")
	private String aciklama;

	// @Column(name = "destructive_test_id")
	// private Integer destructiveTestId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	// @Column(name = "global_id")
	// private Integer globalId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "kesme_alani_ikinci")
	private BigDecimal kesmeAlaniIkinci;

	@Column(name = "kesme_alani_ilk")
	private BigDecimal kesmeAlaniIlk;

	@Column(name = "ortalama_kesme_alani")
	private BigDecimal ortalamaKesmeAlani;

	@Column(name = "parti_no")
	private String partiNo;

	// @Column(name = "pipe_id")
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private DestructiveTestsSpec bagliGlobalId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "destructive_test_id", referencedColumnName = "test_id", insertable = false)
	private DestructiveTestsSpec bagliTestId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	private Boolean sonuc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi", nullable = false)
	private Date testTarihi;

	@Column(name = "documents")
	private String documents;

	@Transient
	private List<String> fileNames;

	@Transient
	private int fileNumber;

	@Column(name = "test_durum")
	private Boolean testDurum;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", nullable = false, referencedColumnName = "pipe_id", insertable = false, updatable = false)
	private RuloPipeLink ruloPipeLink;

	@Column(name = "sample_no")
	private String sampleNo;

	@Column(name = "numune_boyutu")
	private String numuneBoyutu;

	public TestAgirlikDusurmeSonuc() {
	}

	// setters getters
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	/*
	 * public Integer getKesmeAlaniIkinci() { return this.kesmeAlaniIkinci; }
	 * 
	 * public void setKesmeAlaniIkinci(Integer kesmeAlaniIkinci) {
	 * this.kesmeAlaniIkinci = kesmeAlaniIkinci; }
	 * 
	 * public Integer getKesmeAlaniIlk() { return this.kesmeAlaniIlk; }
	 * 
	 * public void setKesmeAlaniIlk(Integer kesmeAlaniIlk) { this.kesmeAlaniIlk
	 * = kesmeAlaniIlk; }
	 * 
	 * public Integer getOrtalamaKesmeAlani() { return this.ortalamaKesmeAlani;
	 * }
	 * 
	 * public void setOrtalamaKesmeAlani(Integer ortalamaKesmeAlani) {
	 * this.ortalamaKesmeAlani = ortalamaKesmeAlani; }
	 */

	public Boolean getSonuc() {
		return this.sonuc;
	}

	public void setSonuc(Boolean sonuc) {
		this.sonuc = sonuc;
	}

	public DestructiveTestsSpec getBagliTestId() {
		return bagliTestId;
	}

	public void setBagliTestId(DestructiveTestsSpec bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public DestructiveTestsSpec getBagliGlobalId() {
		return bagliGlobalId;
	}

	public void setBagliGlobalId(DestructiveTestsSpec bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	public Date getEklemeZamani() {
		return eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Date getGuncellemeZamani() {
		return guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Date getTestTarihi() {
		return testTarihi;
	}

	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	public String getDocuments() {
		return documents;
	}

	public void setDocuments(String documents) {
		this.documents = documents;
	}

	public List<String> getFileNames() {
		try {
			String[] names = documents.split("//");
			fileNames = new ArrayList<String>();

			for (int i = 1; i < names.length; i++) {
				fileNames.add(names[i]);
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return fileNames;
	}

	public void setFileNames(List<String> fileNames) {
		this.fileNames = fileNames;
	}

	public int getFileNumber() {
		try {
			String[] names = documents.split("//");

			fileNumber = names.length - 1;
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return fileNumber;
	}

	/**
	 * @return the testDurum
	 */
	public Boolean getTestDurum() {
		return testDurum;
	}

	/**
	 * @param testDurum
	 *            the testDurum to set
	 */
	public void setTestDurum(Boolean testDurum) {
		this.testDurum = testDurum;
	}

	/**
	 * @return the ruloPipeLink
	 */
	public RuloPipeLink getRuloPipeLink() {
		return ruloPipeLink;
	}

	/**
	 * @param ruloPipeLink
	 *            the ruloPipeLink to set
	 */
	public void setRuloPipeLink(RuloPipeLink ruloPipeLink) {
		this.ruloPipeLink = ruloPipeLink;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the sampleNo
	 */
	public String getSampleNo() {
		return sampleNo;
	}

	/**
	 * @param sampleNo
	 *            the sampleNo to set
	 */
	public void setSampleNo(String sampleNo) {
		this.sampleNo = sampleNo;
	}

	/**
	 * @return the numuneBoyutu
	 */
	public String getNumuneBoyutu() {
		return numuneBoyutu;
	}

	/**
	 * @param numuneBoyutu
	 *            the numuneBoyutu to set
	 */
	public void setNumuneBoyutu(String numuneBoyutu) {
		this.numuneBoyutu = numuneBoyutu;
	}

	/**
	 * @return the partiNo
	 */
	public String getPartiNo() {
		return partiNo;
	}

	/**
	 * @param partiNo
	 *            the partiNo to set
	 */
	public void setPartiNo(String partiNo) {
		this.partiNo = partiNo;
	}

	/**
	 * @return the kesmeAlaniIkinci
	 */
	public BigDecimal getKesmeAlaniIkinci() {
		return kesmeAlaniIkinci;
	}

	/**
	 * @param kesmeAlaniIkinci
	 *            the kesmeAlaniIkinci to set
	 */
	public void setKesmeAlaniIkinci(BigDecimal kesmeAlaniIkinci) {
		this.kesmeAlaniIkinci = kesmeAlaniIkinci;
	}

	/**
	 * @return the kesmeAlaniIlk
	 */
	public BigDecimal getKesmeAlaniIlk() {
		return kesmeAlaniIlk;
	}

	/**
	 * @param kesmeAlaniIlk
	 *            the kesmeAlaniIlk to set
	 */
	public void setKesmeAlaniIlk(BigDecimal kesmeAlaniIlk) {
		this.kesmeAlaniIlk = kesmeAlaniIlk;
	}

	/**
	 * @return the ortalamaKesmeAlani
	 */
	public BigDecimal getOrtalamaKesmeAlani() {
		return ortalamaKesmeAlani;
	}

	/**
	 * @param ortalamaKesmeAlani
	 *            the ortalamaKesmeAlani to set
	 */
	public void setOrtalamaKesmeAlani(BigDecimal ortalamaKesmeAlani) {
		this.ortalamaKesmeAlani = ortalamaKesmeAlani;
	}

}