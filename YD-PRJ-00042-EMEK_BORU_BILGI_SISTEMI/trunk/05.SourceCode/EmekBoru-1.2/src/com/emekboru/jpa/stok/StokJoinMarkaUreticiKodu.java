package com.emekboru.jpa.stok;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the stok_join_marka_uretici_kodu database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "StokJoinMarkaUreticiKodu.findAllByUrunIdMarkaId", query = "SELECT c FROM StokJoinMarkaUreticiKodu c where c.stokUrunKartlari.id=:prmUrunId and c.stokTanimlarMarka.id=:prmMarkaId order by c.ureticiKodu") })
@Table(name = "stok_join_marka_uretici_kodu")
public class StokJoinMarkaUreticiKodu implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "STOK_JOIN_MARKA_URETICI_KODU_JOINID_GENERATOR", sequenceName = "STOK_JOIN_MARKA_URETICI_KODU_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STOK_JOIN_MARKA_URETICI_KODU_JOINID_GENERATOR")
	@Column(name = "join_id", unique = true, nullable = false)
	private Integer joinId;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	// @Column(name = "marka_id", nullable = false)
	// private Integer markaId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "marka_id", referencedColumnName = "ID", insertable = false)
	private StokTanimlarMarka stokTanimlarMarka;

	@Column(name = "uretici_kodu", nullable = false)
	private String ureticiKodu;

	// @Column(name = "urun_id", nullable = false)
	// private Integer urunId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "urun_id", referencedColumnName = "ID", insertable = false)
	private StokUrunKartlari stokUrunKartlari;

	public StokJoinMarkaUreticiKodu() {

		stokUrunKartlari = new StokUrunKartlari();
		stokTanimlarMarka = new StokTanimlarMarka();
	}

	public Integer getJoinId() {
		return this.joinId;
	}

	public void setJoinId(Integer joinId) {
		this.joinId = joinId;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public StokTanimlarMarka getStokTanimlarMarka() {
		return stokTanimlarMarka;
	}

	public void setStokTanimlarMarka(StokTanimlarMarka stokTanimlarMarka) {
		this.stokTanimlarMarka = stokTanimlarMarka;
	}

	public String getUreticiKodu() {
		return ureticiKodu;
	}

	public void setUreticiKodu(String ureticiKodu) {
		this.ureticiKodu = ureticiKodu;
	}

	public StokUrunKartlari getStokUrunKartlari() {
		return stokUrunKartlari;
	}

	public void setStokUrunKartlari(StokUrunKartlari stokUrunKartlari) {
		this.stokUrunKartlari = stokUrunKartlari;
	}

}