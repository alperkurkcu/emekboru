package com.emekboru.jpa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the machine_edw_link database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "MachineEdwLink.findDustByMachineId", query = "SELECT mel FROM MachineEdwLink mel, ElectrodeDustWire edw WHERE mel.machine.machineId =:prmMachineId AND edw.type = '2' AND mel.ended = 'f' AND edw.electrodeDustWireId = mel.electrodeDustWireId ORDER BY mel.id"),
		@NamedQuery(name = "MachineEdwLink.findWireByMachineId", query = "SELECT mel FROM MachineEdwLink mel, ElectrodeDustWire edw WHERE mel.machine.machineId =:prmMachineId AND edw.type = '3' AND mel.ended = 'f' AND edw.electrodeDustWireId = mel.electrodeDustWireId ORDER BY mel.id"),
		@NamedQuery(name = "MachineEdwLink.findDustByMachineIdByDurum", query = "SELECT mel FROM MachineEdwLink mel, ElectrodeDustWire edw WHERE mel.machine.machineId =:prmMachineId AND edw.type = '2' AND mel.ended = 'f' AND edw.electrodeDustWireId = mel.electrodeDustWireId AND mel.icDis=:prmIcDis ORDER BY mel.id"),
		@NamedQuery(name = "MachineEdwLink.findWireByMachineIdByDurum", query = "SELECT mel FROM MachineEdwLink mel, ElectrodeDustWire edw WHERE mel.machine.machineId =:prmMachineId AND edw.type = '3' AND mel.ended = 'f' AND edw.electrodeDustWireId = mel.electrodeDustWireId AND mel.icDis=:prmIcDis AND mel.acDc=:prmAcDc ORDER BY mel.id"),
		@NamedQuery(name = "MachineEdwLink.findAllWireByMachineId", query = "SELECT mel FROM MachineEdwLink mel, ElectrodeDustWire edw WHERE mel.machine.machineId =:prmMachineId AND edw.type = '3' AND mel.ended = 'f' AND edw.electrodeDustWireId = mel.electrodeDustWireId ORDER BY mel.icDis desc") })
@Table(name = "machine_edw_link")
public class MachineEdwLink implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@SequenceGenerator(name = "MACHINE_EDW_LINK_GENERATOR", sequenceName = "MACHINE_EDW_LINK_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MACHINE_EDW_LINK_GENERATOR")
	private int id;

	@Column(name = "electrode_dust_wire_id")
	private Integer electrodeDustWireId;

	@Column(name = "amount_consumed")
	private double amountConsumed;

	@Column(name = "ended")
	private boolean ended;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "end_date")
	private Date endDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_date")
	private Date startDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "machine_id", referencedColumnName = "machine_id", insertable = false)
	private Machine machine;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "electrode_dust_wire_id", referencedColumnName = "electrode_dust_wire_id", insertable = false, updatable = false)
	private ElectrodeDustWire edw;

	@OneToMany(mappedBy = "machineEdwLink", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<EdwPipeLink> edwLinks;

	@Column(name = "ic_dis")
	private Integer icDis;

	@Column(name = "ac_dc")
	private Integer acDc;

	@Column(name = "volt")
	private BigDecimal volt;

	@Column(name = "akim")
	private BigDecimal akim;

	public MachineEdwLink() {
	}

	@Override
	public boolean equals(Object obj) {

		if (!(obj instanceof MachineEdwLink))
			return false;

		if (this.id == ((MachineEdwLink) obj).id)
			return true;

		return false;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getAmountConsumed() {
		return this.amountConsumed;
	}

	public void setAmountConsumed(double amountConsumed) {
		this.amountConsumed = amountConsumed;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Machine getMachine() {
		return machine;
	}

	public void setMachine(Machine machine) {
		this.machine = machine;
	}

	public ElectrodeDustWire getEdw() {
		return edw;
	}

	public void setEdw(ElectrodeDustWire edw) {
		this.edw = edw;
	}

	public boolean isEnded() {
		return ended;
	}

	public void setEnded(boolean ended) {
		this.ended = ended;
	}

	public List<EdwPipeLink> getEdwLinks() {
		return edwLinks;
	}

	public void setPmcLinks(List<EdwPipeLink> edwLinks) {
		this.edwLinks = edwLinks;
	}

	public Integer getElectrodeDustWireId() {
		return electrodeDustWireId;
	}

	public void setElectrodeDustWireId(Integer electrodeDustWireId) {
		this.electrodeDustWireId = electrodeDustWireId;
	}

	/**
	 * @return the icDis
	 */
	public Integer getIcDis() {
		return icDis;
	}

	/**
	 * @param icDis
	 *            the icDis to set
	 */
	public void setIcDis(Integer icDis) {
		this.icDis = icDis;
	}

	/**
	 * @return the acDc
	 */
	public Integer getAcDc() {
		return acDc;
	}

	/**
	 * @param acDc
	 *            the acDc to set
	 */
	public void setAcDc(Integer acDc) {
		this.acDc = acDc;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the akim
	 */
	public BigDecimal getAkim() {
		return akim;
	}

	/**
	 * @param akim
	 *            the akim to set
	 */
	public void setAkim(BigDecimal akim) {
		this.akim = akim;
	}

	/**
	 * @return the volt
	 */
	public BigDecimal getVolt() {
		return volt;
	}

	/**
	 * @param volt
	 *            the volt to set
	 */
	public void setVolt(BigDecimal volt) {
		this.volt = volt;
	}

}