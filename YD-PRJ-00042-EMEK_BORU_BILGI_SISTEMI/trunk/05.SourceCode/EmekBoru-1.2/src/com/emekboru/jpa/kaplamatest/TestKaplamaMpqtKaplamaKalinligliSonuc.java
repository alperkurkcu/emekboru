package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.isolation.IsolationTestDefinition;

/**
 * The persistent class for the test_kaplama_mpqt_kaplama_kalinligli_sonuc
 * database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaMpqtKaplamaKalinligliSonuc.findAll", query = "SELECT r FROM TestKaplamaMpqtKaplamaKalinligliSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_kaplama_mpqt_kaplama_kalinligli_sonuc")
public class TestKaplamaMpqtKaplamaKalinligliSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KAPLAMA_MPQT_KAPLAMA_KALINLIGLI_SONUC_ID_GENERATOR", sequenceName = "TEST_KAPLAMA_MPQT_KAPLAMA_KALINLIGLI_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KAPLAMA_MPQT_KAPLAMA_KALINLIGLI_SONUC_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ekleme_zamani")
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	// @Column(name="global_id")
	// private Integer globalId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "epoksi")
	private Boolean epoksi;

	@Column(name = "epoksi_max")
	private BigDecimal epoksiMax;

	@Column(name = "epoksi_min")
	private BigDecimal epoksiMin;

	@Column(name = "epoksi_ort")
	private BigDecimal epoksiOrt;

	@Column(name = "epoksi_sonuc")
	private Boolean epoksiSonuc;

	// @Column(name = "global_id")
	// private Integer globalId;

	// @Column(name = "isolation_test_id")
	// private Integer isolationTestId;

	// @Column(name = "pipe_id")
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;

	@Column(name = "polietilen_polipropilen")
	private Boolean polietilenPolipropilen;

	@Column(name = "polietilen_polipropilen_max")
	private BigDecimal polietilenPolipropilenMax;

	@Column(name = "polietilen_polipropilen_min")
	private BigDecimal polietilenPolipropilenMin;

	@Column(name = "polietilen_polipropilen_ort")
	private BigDecimal polietilenPolipropilenOrt;

	@Column(name = "polietilen_polipropilen_secim")
	private Boolean polietilenPolipropilenSecim;

	@Column(name = "polietilen_polipropilen_sonuc")
	private Boolean polietilenPolipropilenSonuc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi")
	private Date testTarihi;

	@Column(name = "yapistirici")
	private Boolean yapistirici;

	@Column(name = "yapistirici_max")
	private BigDecimal yapistiriciMax;

	@Column(name = "yapistirici_min")
	private BigDecimal yapistiriciMin;

	@Column(name = "yapistirici_ort")
	private BigDecimal yapistiriciOrt;

	@Column(name = "yapistirici_sonuc")
	private Boolean yapistiriciSonuc;

	public TestKaplamaMpqtKaplamaKalinligliSonuc() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Boolean getEpoksi() {
		return this.epoksi;
	}

	public void setEpoksi(Boolean epoksi) {
		this.epoksi = epoksi;
	}

	public Boolean getEpoksiSonuc() {
		return this.epoksiSonuc;
	}

	public void setEpoksiSonuc(Boolean epoksiSonuc) {
		this.epoksiSonuc = epoksiSonuc;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Boolean getPolietilenPolipropilen() {
		return this.polietilenPolipropilen;
	}

	public void setPolietilenPolipropilen(Boolean polietilenPolipropilen) {
		this.polietilenPolipropilen = polietilenPolipropilen;
	}

	public Boolean getPolietilenPolipropilenSecim() {
		return this.polietilenPolipropilenSecim;
	}

	public void setPolietilenPolipropilenSecim(
			Boolean polietilenPolipropilenSecim) {
		this.polietilenPolipropilenSecim = polietilenPolipropilenSecim;
	}

	public Boolean getPolietilenPolipropilenSonuc() {
		return this.polietilenPolipropilenSonuc;
	}

	public void setPolietilenPolipropilenSonuc(
			Boolean polietilenPolipropilenSonuc) {
		this.polietilenPolipropilenSonuc = polietilenPolipropilenSonuc;
	}

	public Boolean getYapistirici() {
		return this.yapistirici;
	}

	public void setYapistirici(Boolean yapistirici) {
		this.yapistirici = yapistirici;
	}

	public Boolean getYapistiriciSonuc() {
		return this.yapistiriciSonuc;
	}

	public void setYapistiriciSonuc(Boolean yapistiriciSonuc) {
		this.yapistiriciSonuc = yapistiriciSonuc;
	}

	/**
	 * @return the eklemeZamani
	 */
	public Date getEklemeZamani() {
		return eklemeZamani;
	}

	/**
	 * @param eklemeZamani
	 *            the eklemeZamani to set
	 */
	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	/**
	 * @return the guncellemeZamani
	 */
	public Date getGuncellemeZamani() {
		return guncellemeZamani;
	}

	/**
	 * @param guncellemeZamani
	 *            the guncellemeZamani to set
	 */
	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the bagliGlobalId
	 */
	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}

	/**
	 * @param bagliGlobalId
	 *            the bagliGlobalId to set
	 */
	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	/**
	 * @return the bagliTestId
	 */
	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the testTarihi
	 */
	public Date getTestTarihi() {
		return testTarihi;
	}

	/**
	 * @param testTarihi
	 *            the testTarihi to set
	 */
	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the epoksiMax
	 */
	public BigDecimal getEpoksiMax() {
		return epoksiMax;
	}

	/**
	 * @param epoksiMax
	 *            the epoksiMax to set
	 */
	public void setEpoksiMax(BigDecimal epoksiMax) {
		this.epoksiMax = epoksiMax;
	}

	/**
	 * @return the epoksiMin
	 */
	public BigDecimal getEpoksiMin() {
		return epoksiMin;
	}

	/**
	 * @param epoksiMin
	 *            the epoksiMin to set
	 */
	public void setEpoksiMin(BigDecimal epoksiMin) {
		this.epoksiMin = epoksiMin;
	}

	/**
	 * @return the epoksiOrt
	 */
	public BigDecimal getEpoksiOrt() {
		return epoksiOrt;
	}

	/**
	 * @param epoksiOrt
	 *            the epoksiOrt to set
	 */
	public void setEpoksiOrt(BigDecimal epoksiOrt) {
		this.epoksiOrt = epoksiOrt;
	}

	/**
	 * @return the polietilenPolipropilenMax
	 */
	public BigDecimal getPolietilenPolipropilenMax() {
		return polietilenPolipropilenMax;
	}

	/**
	 * @param polietilenPolipropilenMax
	 *            the polietilenPolipropilenMax to set
	 */
	public void setPolietilenPolipropilenMax(
			BigDecimal polietilenPolipropilenMax) {
		this.polietilenPolipropilenMax = polietilenPolipropilenMax;
	}

	/**
	 * @return the polietilenPolipropilenMin
	 */
	public BigDecimal getPolietilenPolipropilenMin() {
		return polietilenPolipropilenMin;
	}

	/**
	 * @param polietilenPolipropilenMin
	 *            the polietilenPolipropilenMin to set
	 */
	public void setPolietilenPolipropilenMin(
			BigDecimal polietilenPolipropilenMin) {
		this.polietilenPolipropilenMin = polietilenPolipropilenMin;
	}

	/**
	 * @return the polietilenPolipropilenOrt
	 */
	public BigDecimal getPolietilenPolipropilenOrt() {
		return polietilenPolipropilenOrt;
	}

	/**
	 * @param polietilenPolipropilenOrt
	 *            the polietilenPolipropilenOrt to set
	 */
	public void setPolietilenPolipropilenOrt(
			BigDecimal polietilenPolipropilenOrt) {
		this.polietilenPolipropilenOrt = polietilenPolipropilenOrt;
	}

	/**
	 * @return the yapistiriciMax
	 */
	public BigDecimal getYapistiriciMax() {
		return yapistiriciMax;
	}

	/**
	 * @param yapistiriciMax
	 *            the yapistiriciMax to set
	 */
	public void setYapistiriciMax(BigDecimal yapistiriciMax) {
		this.yapistiriciMax = yapistiriciMax;
	}

	/**
	 * @return the yapistiriciMin
	 */
	public BigDecimal getYapistiriciMin() {
		return yapistiriciMin;
	}

	/**
	 * @param yapistiriciMin
	 *            the yapistiriciMin to set
	 */
	public void setYapistiriciMin(BigDecimal yapistiriciMin) {
		this.yapistiriciMin = yapistiriciMin;
	}

	/**
	 * @return the yapistiriciOrt
	 */
	public BigDecimal getYapistiriciOrt() {
		return yapistiriciOrt;
	}

	/**
	 * @param yapistiriciOrt
	 *            the yapistiriciOrt to set
	 */
	public void setYapistiriciOrt(BigDecimal yapistiriciOrt) {
		this.yapistiriciOrt = yapistiriciOrt;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

}