package com.emekboru.jpa.kaplamamakinesi;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the kaplama_makinesi_fircalama database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "KaplamaMakinesiFircalama.findAllByPipeId", query = "SELECT r FROM KaplamaMakinesiFircalama r WHERE r.pipe.pipeId=:prmPipeId order by r.eklemeZamani") })
@Table(name = "kaplama_makinesi_fircalama")
public class KaplamaMakinesiFircalama implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "KAPLAMA_MAKINESI_FIRCALAMA_ID_GENERATOR", sequenceName = "KAPLAMA_MAKINESI_FIRCALAMA_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "KAPLAMA_MAKINESI_FIRCALAMA_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	private Boolean durum;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "kaplama_baslama_kullanici")
	private Integer kaplamaBaslamaKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "kaplama_baslama_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser kaplamaBaslamaUser;

	@Column(name = "kaplama_baslama_zamani")
	private Timestamp kaplamaBaslamaZamani;

	@Column(name = "kaplama_bitis_kullanici")
	private Integer kaplamaBitisKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "kaplama_bitis_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser kaplamaBitisUser;

	@Column(name = "kaplama_bitis_zamani")
	private Timestamp kaplamaBitisZamani;

	@Basic
	@Column(name = "pipe_id", nullable = false, insertable = false, updatable = false)
	private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	public KaplamaMakinesiFircalama() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getDurum() {
		return this.durum;
	}

	public void setDurum(Boolean durum) {
		this.durum = durum;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Integer getKaplamaBaslamaKullanici() {
		return this.kaplamaBaslamaKullanici;
	}

	public void setKaplamaBaslamaKullanici(Integer kaplamaBaslamaKullanici) {
		this.kaplamaBaslamaKullanici = kaplamaBaslamaKullanici;
	}

	public Timestamp getKaplamaBaslamaZamani() {
		return this.kaplamaBaslamaZamani;
	}

	public void setKaplamaBaslamaZamani(Timestamp kaplamaBaslamaZamani) {
		this.kaplamaBaslamaZamani = kaplamaBaslamaZamani;
	}

	public Integer getKaplamaBitisKullanici() {
		return this.kaplamaBitisKullanici;
	}

	public void setKaplamaBitisKullanici(Integer kaplamaBitisKullanici) {
		this.kaplamaBitisKullanici = kaplamaBitisKullanici;
	}

	public Timestamp getKaplamaBitisZamani() {
		return this.kaplamaBitisZamani;
	}

	public void setKaplamaBitisZamani(Timestamp kaplamaBitisZamani) {
		this.kaplamaBitisZamani = kaplamaBitisZamani;
	}

	public SystemUser getKaplamaBaslamaUser() {
		return kaplamaBaslamaUser;
	}

	public void setKaplamaBaslamaUser(SystemUser kaplamaBaslamaUser) {
		this.kaplamaBaslamaUser = kaplamaBaslamaUser;
	}

	public SystemUser getKaplamaBitisUser() {
		return kaplamaBitisUser;
	}

	public void setKaplamaBitisUser(SystemUser kaplamaBitisUser) {
		this.kaplamaBitisUser = kaplamaBitisUser;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}