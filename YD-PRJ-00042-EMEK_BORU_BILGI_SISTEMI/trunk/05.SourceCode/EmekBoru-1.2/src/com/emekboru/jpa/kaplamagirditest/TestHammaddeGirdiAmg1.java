package com.emekboru.jpa.kaplamagirditest;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the test_hammadde_girdi_amg1 database table.
 * 
 */
@Entity
@Table(name = "test_hammadde_girdi_amg1")
public class TestHammaddeGirdiAmg1 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_HAMMADDE_GIRDI_AMG1_ID_GENERATOR", sequenceName = "TEST_HAMMADDE_GIRDI_AMG1_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_HAMMADDE_GIRDI_AMG1_ID_GENERATOR")
	@Column(name = "ID")
	private Integer id;

	// @Column(name = "coat_material_id")
	// private Integer coatMaterialId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coat_material_id", referencedColumnName = "coat_material_id", insertable = false)
	private CoatRawMaterial coatRawMaterial;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "agirlik")
	private String agirlik;

	// @Column(name="coat_material_id")
	// private Integer coatMaterialId;

	@Column(name = "degerlendirme_standard")
	private String degerlendirmeStandard;

	@Column(name = "parti_no")
	private String partiNo;

	@Column(name = "rapor_no")
	private String raporNo;

	private Boolean sonuc;

	private String standard;

	@Column(name = "tane_boyutu")
	private String taneBoyutu;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi", nullable = false)
	private Date testTarihi;

	@Column(name = "toplam_oran")
	private String toplamOran;

	@Column(name = "yuzde_oran")
	private String yuzdeOran;

	public TestHammaddeGirdiAmg1() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAgirlik() {
		return this.agirlik;
	}

	public void setAgirlik(String agirlik) {
		this.agirlik = agirlik;
	}

	public String getDegerlendirmeStandard() {
		return this.degerlendirmeStandard;
	}

	public void setDegerlendirmeStandard(String degerlendirmeStandard) {
		this.degerlendirmeStandard = degerlendirmeStandard;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getPartiNo() {
		return this.partiNo;
	}

	public void setPartiNo(String partiNo) {
		this.partiNo = partiNo;
	}

	public String getRaporNo() {
		return this.raporNo;
	}

	public void setRaporNo(String raporNo) {
		this.raporNo = raporNo;
	}

	public Boolean getSonuc() {
		return this.sonuc;
	}

	public void setSonuc(Boolean sonuc) {
		this.sonuc = sonuc;
	}

	public String getStandard() {
		return this.standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getTaneBoyutu() {
		return this.taneBoyutu;
	}

	public void setTaneBoyutu(String taneBoyutu) {
		this.taneBoyutu = taneBoyutu;
	}

	public String getToplamOran() {
		return this.toplamOran;
	}

	public void setToplamOran(String toplamOran) {
		this.toplamOran = toplamOran;
	}

	public String getYuzdeOran() {
		return this.yuzdeOran;
	}

	public void setYuzdeOran(String yuzdeOran) {
		this.yuzdeOran = yuzdeOran;
	}

	/**
	 * @return the coatRawMaterial
	 */
	public CoatRawMaterial getCoatRawMaterial() {
		return coatRawMaterial;
	}

	/**
	 * @param coatRawMaterial
	 *            the coatRawMaterial to set
	 */
	public void setCoatRawMaterial(CoatRawMaterial coatRawMaterial) {
		this.coatRawMaterial = coatRawMaterial;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the testTarihi
	 */
	public Date getTestTarihi() {
		return testTarihi;
	}

	/**
	 * @param testTarihi
	 *            the testTarihi to set
	 */
	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

}