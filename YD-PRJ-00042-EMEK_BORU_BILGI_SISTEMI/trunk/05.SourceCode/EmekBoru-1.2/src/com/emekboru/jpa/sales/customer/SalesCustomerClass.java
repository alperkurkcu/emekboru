package com.emekboru.jpa.sales.customer;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "sales_customer_class")
public class SalesCustomerClass implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7252266479748805877L;

	@Id
	@Column(name = "class_id")
	private int classId;

	@Column(name = "title")
	private String name;

	@OneToMany(mappedBy = "classification")
	private List<SalesCustomer> salesCustomer;

	@OneToMany(mappedBy = "classification")
	private List<SalesContactCompany> salesContactCompany;

	public SalesCustomerClass() {

	}

	@Override
	public boolean equals(Object o) {
		return this.getClassId() == ((SalesCustomerClass) o).getClassId();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public int getClassId() {
		return classId;
	}

	public void setClassId(int classId) {
		this.classId = classId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SalesCustomer> getSalesCustomer() {
		return salesCustomer;
	}

	public void setSalesCustomer(List<SalesCustomer> salesCustomer) {
		this.salesCustomer = salesCustomer;
	}

	public List<SalesContactCompany> getSalesContactCompany() {
		return salesContactCompany;
	}

	public void setSalesContactCompany(
			List<SalesContactCompany> salesContactCompany) {
		this.salesContactCompany = salesContactCompany;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
