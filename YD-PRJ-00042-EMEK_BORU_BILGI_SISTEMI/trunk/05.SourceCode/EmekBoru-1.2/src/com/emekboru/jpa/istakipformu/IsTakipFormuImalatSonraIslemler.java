package com.emekboru.jpa.istakipformu;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the is_takip_formu_imalat_sonra_islemler database
 * table.
 * 
 */
@Entity
@Table(name = "is_takip_formu_imalat_sonra_islemler")
public class IsTakipFormuImalatSonraIslemler implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IS_TAKIP_FORMU_IMALAT_SONRA_ISLEMLER_ID_GENERATOR", sequenceName = "IS_TAKIP_FORMU_IMALAT_SONRA_ISLEMLER_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IS_TAKIP_FORMU_IMALAT_SONRA_ISLEMLER_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "ayar_sonrasi_ilk_boru_aciklama")
	private String ayarSonrasiIlkBoruAciklama;

	@Column(name = "ayar_sonrasi_ilk_boru_check")
	private Boolean ayarSonrasiIlkBoruCheck;

	@Column(name = "ayar_sonrasi_ilk_boru_id")
	private Integer ayarSonrasiIlkBoruId;

	@Temporal(TemporalType.DATE)
	@Column(name = "ayar_sonrasi_ilk_boru_tarihi")
	private Date ayarSonrasiIlkBoruTarihi;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name = "is_takip_form_id")
	// private Integer isTakipFormId;

	@OneToOne(mappedBy = "isTakipFormuImalatSonraIslemler", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	private IsTakipFormuImalat isTakipFormuImalat;

	@Column(name = "gk_aciklama")
	private String gkAciklama;

	@Column(name = "gk_boru_id")
	private Integer gkBoruId;

	@Column(name = "gk_check")
	private Boolean gkCheck;

	@Column(name = "gk_kaynak_yuksekligi")
	private String gkKaynakYuksekligi;

	@Temporal(TemporalType.DATE)
	@Column(name = "gk_tarihi")
	private Date gkTarihi;

	@Column(name = "hidro_aciklama")
	private String hidroAciklama;

	@Column(name = "hidro_boru_id")
	private Integer hidroBoruId;

	@Column(name = "hidro_check")
	private Boolean hidroCheck;

	@Temporal(TemporalType.DATE)
	@Column(name = "hidro_tarihi")
	private Date hidroTarihi;

	@Column(name = "kalinti_maynetiklesme_aciklama")
	private String kalintiMaynetiklesmeAciklama;

	@Column(name = "kalinti_maynetiklesme_boru_id")
	private Integer kalintiMaynetiklesmeBoruId;

	@Column(name = "kalinti_maynetiklesme_check")
	private Boolean kalintiMaynetiklesmeCheck;

	@Temporal(TemporalType.DATE)
	@Column(name = "kalinti_maynetiklesme_tarihi")
	private Date kalintiMaynetiklesmeTarihi;

	@Column(name = "kalinti_maynetiklesme_veri_1")
	private String kalintiMaynetiklesmeVeri1;

	@Column(name = "kalinti_maynetiklesme_veri_2")
	private String kalintiMaynetiklesmeVeri2;

	@Column(name = "kaynak_agzi_aciklama")
	private String kaynakAgziAciklama;

	@Column(name = "kaynak_agzi_alin_yuksekligi")
	private String kaynakAgziAlinYuksekligi;

	@Column(name = "kaynak_agzi_boru_id")
	private Integer kaynakAgziBoruId;

	@Column(name = "kaynak_agzi_check")
	private Boolean kaynakAgziCheck;

	@Column(name = "kaynak_agzi_diklikten_sapma")
	private String kaynakAgziDikliktenSapma;

	@Temporal(TemporalType.DATE)
	@Column(name = "kaynak_agzi_tarihi")
	private Date kaynakAgziTarihi;

	@Column(name = "makro_test_aciklama")
	private String makroTestAciklama;

	@Column(name = "makro_test_check")
	private Boolean makroTestCheck;

	@Temporal(TemporalType.DATE)
	@Column(name = "makro_test_tarihi")
	private Date makroTestTarihi;

	@Column(name = "ring_expansion_aciklama")
	private String ringExpansionAciklama;

	@Column(name = "ring_expansion_check")
	private Boolean ringExpansionCheck;

	@Column(name = "ring_expansion_puruzlulugu")
	private Integer ringExpansionPuruzlulugu;

	@Temporal(TemporalType.DATE)
	@Column(name = "ring_expansion_tarihi")
	private Date ringExpansionTarihi;

	public IsTakipFormuImalatSonraIslemler() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAyarSonrasiIlkBoruAciklama() {
		return this.ayarSonrasiIlkBoruAciklama;
	}

	public void setAyarSonrasiIlkBoruAciklama(String ayarSonrasiIlkBoruAciklama) {
		this.ayarSonrasiIlkBoruAciklama = ayarSonrasiIlkBoruAciklama;
	}

	public Boolean getAyarSonrasiIlkBoruCheck() {
		return this.ayarSonrasiIlkBoruCheck;
	}

	public void setAyarSonrasiIlkBoruCheck(Boolean ayarSonrasiIlkBoruCheck) {
		this.ayarSonrasiIlkBoruCheck = ayarSonrasiIlkBoruCheck;
	}

	public Integer getAyarSonrasiIlkBoruId() {
		return this.ayarSonrasiIlkBoruId;
	}

	public void setAyarSonrasiIlkBoruId(Integer ayarSonrasiIlkBoruId) {
		this.ayarSonrasiIlkBoruId = ayarSonrasiIlkBoruId;
	}

	public Date getAyarSonrasiIlkBoruTarihi() {
		return this.ayarSonrasiIlkBoruTarihi;
	}

	public void setAyarSonrasiIlkBoruTarihi(Date ayarSonrasiIlkBoruTarihi) {
		this.ayarSonrasiIlkBoruTarihi = ayarSonrasiIlkBoruTarihi;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public String getGkAciklama() {
		return this.gkAciklama;
	}

	public void setGkAciklama(String gkAciklama) {
		this.gkAciklama = gkAciklama;
	}

	public Integer getGkBoruId() {
		return this.gkBoruId;
	}

	public void setGkBoruId(Integer gkBoruId) {
		this.gkBoruId = gkBoruId;
	}

	public Boolean getGkCheck() {
		return this.gkCheck;
	}

	public void setGkCheck(Boolean gkCheck) {
		this.gkCheck = gkCheck;
	}

	public String getGkKaynakYuksekligi() {
		return this.gkKaynakYuksekligi;
	}

	public void setGkKaynakYuksekligi(String gkKaynakYuksekligi) {
		this.gkKaynakYuksekligi = gkKaynakYuksekligi;
	}

	public Date getGkTarihi() {
		return this.gkTarihi;
	}

	public void setGkTarihi(Date gkTarihi) {
		this.gkTarihi = gkTarihi;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public String getHidroAciklama() {
		return this.hidroAciklama;
	}

	public void setHidroAciklama(String hidroAciklama) {
		this.hidroAciklama = hidroAciklama;
	}

	public Integer getHidroBoruId() {
		return this.hidroBoruId;
	}

	public void setHidroBoruId(Integer hidroBoruId) {
		this.hidroBoruId = hidroBoruId;
	}

	public Boolean getHidroCheck() {
		return this.hidroCheck;
	}

	public void setHidroCheck(Boolean hidroCheck) {
		this.hidroCheck = hidroCheck;
	}

	public Date getHidroTarihi() {
		return this.hidroTarihi;
	}

	public void setHidroTarihi(Date hidroTarihi) {
		this.hidroTarihi = hidroTarihi;
	}

	public String getKalintiMaynetiklesmeAciklama() {
		return this.kalintiMaynetiklesmeAciklama;
	}

	public void setKalintiMaynetiklesmeAciklama(
			String kalintiMaynetiklesmeAciklama) {
		this.kalintiMaynetiklesmeAciklama = kalintiMaynetiklesmeAciklama;
	}

	public Integer getKalintiMaynetiklesmeBoruId() {
		return this.kalintiMaynetiklesmeBoruId;
	}

	public void setKalintiMaynetiklesmeBoruId(Integer kalintiMaynetiklesmeBoruId) {
		this.kalintiMaynetiklesmeBoruId = kalintiMaynetiklesmeBoruId;
	}

	public Boolean getKalintiMaynetiklesmeCheck() {
		return this.kalintiMaynetiklesmeCheck;
	}

	public void setKalintiMaynetiklesmeCheck(Boolean kalintiMaynetiklesmeCheck) {
		this.kalintiMaynetiklesmeCheck = kalintiMaynetiklesmeCheck;
	}

	public Date getKalintiMaynetiklesmeTarihi() {
		return this.kalintiMaynetiklesmeTarihi;
	}

	public void setKalintiMaynetiklesmeTarihi(Date kalintiMaynetiklesmeTarihi) {
		this.kalintiMaynetiklesmeTarihi = kalintiMaynetiklesmeTarihi;
	}

	public String getKalintiMaynetiklesmeVeri1() {
		return this.kalintiMaynetiklesmeVeri1;
	}

	public void setKalintiMaynetiklesmeVeri1(String kalintiMaynetiklesmeVeri1) {
		this.kalintiMaynetiklesmeVeri1 = kalintiMaynetiklesmeVeri1;
	}

	public String getKalintiMaynetiklesmeVeri2() {
		return this.kalintiMaynetiklesmeVeri2;
	}

	public void setKalintiMaynetiklesmeVeri2(String kalintiMaynetiklesmeVeri2) {
		this.kalintiMaynetiklesmeVeri2 = kalintiMaynetiklesmeVeri2;
	}

	public String getKaynakAgziAciklama() {
		return this.kaynakAgziAciklama;
	}

	public void setKaynakAgziAciklama(String kaynakAgziAciklama) {
		this.kaynakAgziAciklama = kaynakAgziAciklama;
	}

	public String getKaynakAgziAlinYuksekligi() {
		return this.kaynakAgziAlinYuksekligi;
	}

	public void setKaynakAgziAlinYuksekligi(String kaynakAgziAlinYuksekligi) {
		this.kaynakAgziAlinYuksekligi = kaynakAgziAlinYuksekligi;
	}

	public Integer getKaynakAgziBoruId() {
		return this.kaynakAgziBoruId;
	}

	public void setKaynakAgziBoruId(Integer kaynakAgziBoruId) {
		this.kaynakAgziBoruId = kaynakAgziBoruId;
	}

	public Boolean getKaynakAgziCheck() {
		return this.kaynakAgziCheck;
	}

	public void setKaynakAgziCheck(Boolean kaynakAgziCheck) {
		this.kaynakAgziCheck = kaynakAgziCheck;
	}

	public String getKaynakAgziDikliktenSapma() {
		return this.kaynakAgziDikliktenSapma;
	}

	public void setKaynakAgziDikliktenSapma(String kaynakAgziDikliktenSapma) {
		this.kaynakAgziDikliktenSapma = kaynakAgziDikliktenSapma;
	}

	public Date getKaynakAgziTarihi() {
		return this.kaynakAgziTarihi;
	}

	public void setKaynakAgziTarihi(Date kaynakAgziTarihi) {
		this.kaynakAgziTarihi = kaynakAgziTarihi;
	}

	public String getMakroTestAciklama() {
		return this.makroTestAciklama;
	}

	public void setMakroTestAciklama(String makroTestAciklama) {
		this.makroTestAciklama = makroTestAciklama;
	}

	public Boolean getMakroTestCheck() {
		return this.makroTestCheck;
	}

	public void setMakroTestCheck(Boolean makroTestCheck) {
		this.makroTestCheck = makroTestCheck;
	}

	public Date getMakroTestTarihi() {
		return this.makroTestTarihi;
	}

	public void setMakroTestTarihi(Date makroTestTarihi) {
		this.makroTestTarihi = makroTestTarihi;
	}

	public String getRingExpansionAciklama() {
		return this.ringExpansionAciklama;
	}

	public void setRingExpansionAciklama(String ringExpansionAciklama) {
		this.ringExpansionAciklama = ringExpansionAciklama;
	}

	public Boolean getRingExpansionCheck() {
		return this.ringExpansionCheck;
	}

	public void setRingExpansionCheck(Boolean ringExpansionCheck) {
		this.ringExpansionCheck = ringExpansionCheck;
	}

	public Integer getRingExpansionPuruzlulugu() {
		return this.ringExpansionPuruzlulugu;
	}

	public void setRingExpansionPuruzlulugu(Integer ringExpansionPuruzlulugu) {
		this.ringExpansionPuruzlulugu = ringExpansionPuruzlulugu;
	}

	public Date getRingExpansionTarihi() {
		return this.ringExpansionTarihi;
	}

	public void setRingExpansionTarihi(Date ringExpansionTarihi) {
		this.ringExpansionTarihi = ringExpansionTarihi;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the isTakipFormuImalat
	 */
	public IsTakipFormuImalat getIsTakipFormuImalat() {
		return isTakipFormuImalat;
	}

	/**
	 * @param isTakipFormuImalat
	 *            the isTakipFormuImalat to set
	 */
	public void setIsTakipFormuImalat(IsTakipFormuImalat isTakipFormuImalat) {
		this.isTakipFormuImalat = isTakipFormuImalat;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}