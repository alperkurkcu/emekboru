package com.emekboru.jpa;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@NamedQueries({ @NamedQuery(name = "MachinePartCategory.findAll", query = "SELECT c FROM MachinePartCategory c") })
@Table(name="machine_part_category")
public class MachinePartCategory implements Serializable{


	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "machine_part_category_id")
	@SequenceGenerator(name = "MACHINE_PART_CATEGORY_GENERATOR", sequenceName = "MACHINE_PART_CATEGORY_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MACHINE_PART_CATEGORY_GENERATOR")
	private Integer machinePartCategoryId;

	@Column(name = "category_name")
	private String categoryName;

	@Column(name = "short_desc")
	private String shortDesc;

	@OneToMany(mappedBy = "machinePartCategory", fetch = FetchType.LAZY)
	private List<MachinePart> machineParts;

	public MachinePartCategory() {

	}

	/**
	 * GETTERS AND SETTERS
	 */
	public Integer getMachinePartCategoryId() {
		return machinePartCategoryId;
	}

	public void setMachinePartCategoryId(Integer machinePartCategoryId) {
		this.machinePartCategoryId = machinePartCategoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public List<MachinePart> getMachineParts() {
		return machineParts;
	}

	public void setMachineParts(List<MachinePart> machineParts) {
		this.machineParts = machineParts;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
