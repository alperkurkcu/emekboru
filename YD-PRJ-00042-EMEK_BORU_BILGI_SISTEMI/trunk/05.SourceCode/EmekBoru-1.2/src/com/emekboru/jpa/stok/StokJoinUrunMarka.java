package com.emekboru.jpa.stok;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the stok_join_urun_marka database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "StokJoinUrunMarka.findAllByUrunId", query = "SELECT c FROM StokJoinUrunMarka c where c.stokUrunKartlari.id=:prmUrunId order by c.stokTanimlarMarka.id"),
		@NamedQuery(name = "StokJoinUrunMarka.findAllByUrunIdMarkaId", query = "SELECT c FROM StokJoinUrunMarka c where c.stokUrunKartlari.id=:prmUrunId and c.stokTanimlarMarka.id=:prmMarkaId order by c.stokTanimlarMarka.id") })
@Table(name = "stok_join_urun_marka")
public class StokJoinUrunMarka implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "STOK_JOIN_URUN_MARKA_JOINID_GENERATOR", sequenceName = "STOK_JOIN_URUN_MARKA_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STOK_JOIN_URUN_MARKA_JOINID_GENERATOR")
	@Column(name = "join_id", unique = true, nullable = false)
	private Integer joinId;

	// @Column(name = "marka_id", nullable = false)
	// private Integer markaId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "marka_id", referencedColumnName = "ID", insertable = false)
	private StokTanimlarMarka stokTanimlarMarka;

	// @Column(name = "urun_id", nullable = false)
	// private Integer urunId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "urun_id", referencedColumnName = "ID", insertable = false)
	private StokUrunKartlari stokUrunKartlari;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	public StokJoinUrunMarka() {

		stokTanimlarMarka = new StokTanimlarMarka();
		stokUrunKartlari = new StokUrunKartlari();
	}

	public Integer getJoinId() {
		return this.joinId;
	}

	public void setJoinId(Integer joinId) {
		this.joinId = joinId;
	}

	public Timestamp getEklemeZamani() {
		return eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public StokTanimlarMarka getStokTanimlarMarka() {
		return stokTanimlarMarka;
	}

	public void setStokTanimlarMarka(StokTanimlarMarka stokTanimlarMarka) {
		this.stokTanimlarMarka = stokTanimlarMarka;
	}

	public StokUrunKartlari getStokUrunKartlari() {
		return stokUrunKartlari;
	}

	public void setStokUrunKartlari(StokUrunKartlari stokUrunKartlari) {
		this.stokUrunKartlari = stokUrunKartlari;
	}

}