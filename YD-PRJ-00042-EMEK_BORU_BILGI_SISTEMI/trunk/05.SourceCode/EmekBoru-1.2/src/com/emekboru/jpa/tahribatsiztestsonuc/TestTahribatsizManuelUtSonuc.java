package com.emekboru.jpa.tahribatsiztestsonuc;

import static javax.persistence.CascadeType.REFRESH;
import static javax.persistence.FetchType.EAGER;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.kalibrasyon.KalibrasyonManuelUt;

/**
 * The persistent class for the test_tahribatsiz_manuel_ut_sonuc database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "TestTahribatsizManuelUtSonuc.findAll", query = "SELECT r FROM TestTahribatsizManuelUtSonuc r WHERE r.pipe.pipeId=:prmPipeId order by r.koordinatQ"),
		@NamedQuery(name = "TestTahribatsizManuelUtSonuc.findByKoordinats", query = "SELECT r FROM TestTahribatsizManuelUtSonuc r WHERE r.koordinatQ=:prmQ and r.koordinatL=:prmL"),
		@NamedQuery(name = "TestTahribatsizManuelUtSonuc.findByKoordinatsPipeId", query = "SELECT r FROM TestTahribatsizManuelUtSonuc r WHERE r.koordinatQ=:prmQ and r.koordinatL=:prmL and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_tahribatsiz_manuel_ut_sonuc")
public class TestTahribatsizManuelUtSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_TAHRIBATSIZ_MANUEL_UT_SONUC_ID_GENERATOR", sequenceName = "TEST_TAHRIBATSIZ_MANUEL_UT_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_TAHRIBATSIZ_MANUEL_UT_SONUC_ID_GENERATOR")
	@Column(name = "ID", unique = true, nullable = false)
	private Integer id;

	@Column(name = "aciklama", length = 255)
	private String aciklama;

	@Column(name = "degerlendirme", nullable = false)
	private Integer degerlendirme;

	@Column(name = "kaynak_laminasyon", nullable = false)
	private Integer kaynakLaminasyon;

	@Column(name = "kaynak_islemi", nullable = false, length = 20)
	private String kaynakIslemi;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@Column(name = "tamir_sonrasi_degerlendirme", length = 255)
	private String tamirSonrasiDegerlendirme;

	@OneToOne(mappedBy = "testTahribatsizManuelUtSonuc", fetch = EAGER, cascade = REFRESH)
	private TestTahribatsizTamir testTahribatsizTamir;

	@OneToOne(mappedBy = "testTahribatsizManuelUtSonuc", fetch = EAGER, cascade = REFRESH)
	private TestTahribatsizTorna testTahribatsizTorna;

	@OneToOne(mappedBy = "testTahribatsizManuelUtSonuc", fetch = EAGER, cascade = REFRESH)
	private TestTahribatsizHidrostatik testTahribatsizHidrostatik;

	@OneToOne(mappedBy = "testTahribatsizManuelUtSonuc", fetch = EAGER, cascade = REFRESH)
	private TestTahribatsizGozOlcuSonuc testTahribatsizGozOlcuSonuc;

	private String vu;

	private boolean b;

	private boolean e;

	private boolean d;

	private boolean h;

	@Column(name = "test_id")
	private Integer testId;

	@Column(name = "koordinat_q")
	private Integer koordinatQ;

	@Column(name = "koordinat_l")
	private Integer koordinatL;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "derinlik")
	private Integer derinlik;

	@Column(name = "tamir_sonrasi_durum", nullable = false)
	private Integer tamirSonrasiDurum;

	@Column(name = "tamir_kabul_zamani")
	private Timestamp tamirKabulZamani;

	@Column(name = "tamir_kabul_kullanici")
	private Integer tamirKabulKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "tamir_kabul_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser tamirKabulEmployee;

	@Column(name = "gaz_Kanali")
	private boolean gazKanali;

	private boolean curuf;

	@Column(name = "yetersiz_nufuziyet")
	private boolean yetersizNufuziyet;

	private boolean catlak;

	@Column(name = "yanma_olugu")
	private boolean yanmaOlugu;

	@Column(name = "yetersiz_ergime")
	private boolean yetersizErgime;

	// @Column(name = "kalibrasyon_id")
	// private Integer kalibrasyonId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "kalibrasyon_id", referencedColumnName = "ID", insertable = false)
	private KalibrasyonManuelUt kalibrasyon;

	public TestTahribatsizManuelUtSonuc() {

	}

	// setters getters

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Integer getKaynakLaminasyon() {
		return kaynakLaminasyon;
	}

	public void setKaynakLaminasyon(Integer kaynakLaminasyon) {
		this.kaynakLaminasyon = kaynakLaminasyon;
	}

	public String getKaynakIslemi() {
		return kaynakIslemi;
	}

	public void setKaynakIslemi(String kaynakIslemi) {
		this.kaynakIslemi = kaynakIslemi;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public String getTamirSonrasiDegerlendirme() {
		return tamirSonrasiDegerlendirme;
	}

	public void setTamirSonrasiDegerlendirme(String tamirSonrasiDegerlendirme) {
		this.tamirSonrasiDegerlendirme = tamirSonrasiDegerlendirme;
	}

	/**
	 * @return the vu
	 */
	public String getVu() {
		return vu;
	}

	/**
	 * @param vu
	 *            the vu to set
	 */
	public void setVu(String vu) {
		this.vu = vu;
	}

	public Integer getTestId() {
		return testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	public Integer getKoordinatQ() {
		return koordinatQ;
	}

	public void setKoordinatQ(Integer koordinatQ) {
		this.koordinatQ = koordinatQ;
	}

	public Integer getKoordinatL() {
		return koordinatL;
	}

	public void setKoordinatL(Integer koordinatL) {
		this.koordinatL = koordinatL;
	}

	public boolean isB() {
		return b;
	}

	public void setB(boolean b) {
		this.b = b;
	}

	public boolean isE() {
		return e;
	}

	public void setE(boolean e) {
		this.e = e;
	}

	public boolean isD() {
		return d;
	}

	public void setD(boolean d) {
		this.d = d;
	}

	public boolean isH() {
		return h;
	}

	public void setH(boolean h) {
		this.h = h;
	}

	public Timestamp getEklemeZamani() {
		return eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Timestamp getGuncellemeZamani() {
		return guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public TestTahribatsizTamir getTestTahribatsizTamir() {
		return testTahribatsizTamir;
	}

	public void setTestTahribatsizTamir(
			TestTahribatsizTamir testTahribatsizTamir) {
		this.testTahribatsizTamir = testTahribatsizTamir;
	}

	public TestTahribatsizTorna getTestTahribatsizTorna() {
		return testTahribatsizTorna;
	}

	public void setTestTahribatsizTorna(
			TestTahribatsizTorna testTahribatsizTorna) {
		this.testTahribatsizTorna = testTahribatsizTorna;
	}

	public TestTahribatsizHidrostatik getTestTahribatsizHidrostatik() {
		return testTahribatsizHidrostatik;
	}

	public void setTestTahribatsizHidrostatik(
			TestTahribatsizHidrostatik testTahribatsizHidrostatik) {
		this.testTahribatsizHidrostatik = testTahribatsizHidrostatik;
	}

	public Integer getDerinlik() {
		return derinlik;
	}

	public void setDerinlik(Integer derinlik) {
		this.derinlik = derinlik;
	}

	public TestTahribatsizGozOlcuSonuc getTestTahribatsizGozOlcuSonuc() {
		return testTahribatsizGozOlcuSonuc;
	}

	public void setTestTahribatsizGozOlcuSonuc(
			TestTahribatsizGozOlcuSonuc testTahribatsizGozOlcuSonuc) {
		this.testTahribatsizGozOlcuSonuc = testTahribatsizGozOlcuSonuc;
	}

	public Integer getDegerlendirme() {
		return degerlendirme;
	}

	public void setDegerlendirme(Integer degerlendirme) {
		this.degerlendirme = degerlendirme;
	}

	public Integer getTamirSonrasiDurum() {
		return tamirSonrasiDurum;
	}

	public void setTamirSonrasiDurum(Integer tamirSonrasiDurum) {
		this.tamirSonrasiDurum = tamirSonrasiDurum;
	}

	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	public Timestamp getTamirKabulZamani() {
		return tamirKabulZamani;
	}

	public void setTamirKabulZamani(Timestamp tamirKabulZamani) {
		this.tamirKabulZamani = tamirKabulZamani;
	}

	public Integer getTamirKabulKullanici() {
		return tamirKabulKullanici;
	}

	public void setTamirKabulKullanici(Integer tamirKabulKullanici) {
		this.tamirKabulKullanici = tamirKabulKullanici;
	}

	public SystemUser getTamirKabulEmployee() {
		return tamirKabulEmployee;
	}

	public void setTamirKabulEmployee(SystemUser tamirKabulEmployee) {
		this.tamirKabulEmployee = tamirKabulEmployee;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public boolean isGazKanali() {
		return gazKanali;
	}

	public void setGazKanali(boolean gazKanali) {
		this.gazKanali = gazKanali;
	}

	public boolean isCuruf() {
		return curuf;
	}

	public void setCuruf(boolean curuf) {
		this.curuf = curuf;
	}

	public boolean isYetersizNufuziyet() {
		return yetersizNufuziyet;
	}

	public void setYetersizNufuziyet(boolean yetersizNufuziyet) {
		this.yetersizNufuziyet = yetersizNufuziyet;
	}

	public boolean isCatlak() {
		return catlak;
	}

	public void setCatlak(boolean catlak) {
		this.catlak = catlak;
	}

	public boolean isYanmaOlugu() {
		return yanmaOlugu;
	}

	public void setYanmaOlugu(boolean yanmaOlugu) {
		this.yanmaOlugu = yanmaOlugu;
	}

	public boolean isYetersizErgime() {
		return yetersizErgime;
	}

	public void setYetersizErgime(boolean yetersizErgime) {
		this.yetersizErgime = yetersizErgime;
	}

	/**
	 * @return the kalibrasyon
	 */
	public KalibrasyonManuelUt getKalibrasyon() {
		return kalibrasyon;
	}

	/**
	 * @param kalibrasyon
	 *            the kalibrasyon to set
	 */
	public void setKalibrasyon(KalibrasyonManuelUt kalibrasyon) {
		this.kalibrasyon = kalibrasyon;
	}
}