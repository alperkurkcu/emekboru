package com.emekboru.jpa.rulo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "rulo_test_cekme_enine")
public class RuloTestCekmeEnine {

	@Id
	@SequenceGenerator(name = "rulo_test_cekme_enine_generator", sequenceName = "rulo_test_cekme_enine_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rulo_test_cekme_enine_generator")
	@Column(name = "rulo_cekme_testi_enine_id")
	private Integer ruloTestCekmeEnineId;

	@Column(name = "rulo_cekme_testi_enine_genislik")
	private Float ruloTestCekmeEnineGenislik;

	@Column(name = "rulo_cekme_testi_enine_et_kalinligi")
	private Float ruloTestCekmeEnineEtKalinligi;

	@Column(name = "rulo_cekme_testi_enine_alan")
	private Float ruloTestCekmeEnineAlan;

	@Column(name = "rulo_cekme_testi_enine_akma_yuku")
	private Float ruloTestCekmeEnineAkmaYuku;

	@Column(name = "rulo_cekme_testi_enine_cekme_yuku")
	private Float ruloTestCekmeEnineCekmeYuku;

	@Column(name = "rulo_cekme_testi_enine_akma_dayanci")
	private Float ruloTestCekmeEnineAkmaDayanci;

	@Column(name = "rulo_cekme_testi_enine_cekme_dayanci")
	private Float ruloTestCekmeEnineCekmeDayanci;

	@Column(name = "rulo_cekme_testi_enine_akma_cekme")
	private Float ruloTestCekmeEnineAkmaCekme;

	@Column(name = "rulo_cekme_testi_enine_uzama")
	private Float ruloTestCekmeEnineUzama;

	@Temporal(TemporalType.DATE)
	@Column(name = "rulo_cekme_testi_enine_tarih")
	private Date ruloTestCekmeEnineTarih;

	@Column(name = "rulo_cekme_testi_enine_aciklama")
	private String ruloTestCekmeEnineAciklama;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "rulo_id", nullable = false)
	private Rulo rulo;

	@Column(name = "durum")
	private Integer durum = 0;

	@Column(name = "islem_yapan_id")
	private int islemYapanId;

	@Column(name = "islem_yapan_isim")
	private String islemYapanIsim;

	// setters getters
	public int getIslemYapanId() {
		return islemYapanId;
	}

	public void setIslemYapanId(int islemYapanId) {
		this.islemYapanId = islemYapanId;
	}

	public String getIslemYapanIsim() {
		return islemYapanIsim;
	}

	public void setIslemYapanIsim(String islemYapanIsim) {
		this.islemYapanIsim = islemYapanIsim;
	}

	public Integer getDurum() {
		return durum;
	}

	public void setDurum(Integer durum) {
		this.durum = durum;
	}

	public Rulo getRulo() {
		return rulo;
	}

	public void setRulo(Rulo rulo) {
		this.rulo = rulo;
	}

	public Integer getRuloTestCekmeEnineId() {
		return ruloTestCekmeEnineId;
	}

	public void setRuloTestCekmeEnineId(Integer ruloTestCekmeEnineId) {
		this.ruloTestCekmeEnineId = ruloTestCekmeEnineId;
	}

	public Date getRuloTestCekmeEnineTarih() {
		return ruloTestCekmeEnineTarih;
	}

	public void setRuloTestCekmeEnineTarih(Date ruloTestCekmeEnineTarih) {
		this.ruloTestCekmeEnineTarih = ruloTestCekmeEnineTarih;
	}

	public String getRuloTestCekmeEnineAciklama() {
		return ruloTestCekmeEnineAciklama;
	}

	public void setRuloTestCekmeEnineAciklama(String ruloTestCekmeEnineAciklama) {
		this.ruloTestCekmeEnineAciklama = ruloTestCekmeEnineAciklama;
	}

	public Float getRuloTestCekmeEnineGenislik() {
		return ruloTestCekmeEnineGenislik;
	}

	public void setRuloTestCekmeEnineGenislik(Float ruloTestCekmeEnineGenislik) {
		this.ruloTestCekmeEnineGenislik = ruloTestCekmeEnineGenislik;
	}

	public Float getRuloTestCekmeEnineEtKalinligi() {
		return ruloTestCekmeEnineEtKalinligi;
	}

	public void setRuloTestCekmeEnineEtKalinligi(
			Float ruloTestCekmeEnineEtKalinligi) {
		this.ruloTestCekmeEnineEtKalinligi = ruloTestCekmeEnineEtKalinligi;
	}

	public Float getRuloTestCekmeEnineAlan() {
		return ruloTestCekmeEnineAlan;
	}

	public void setRuloTestCekmeEnineAlan(Float ruloTestCekmeEnineAlan) {
		this.ruloTestCekmeEnineAlan = ruloTestCekmeEnineAlan;
	}

	public Float getRuloTestCekmeEnineAkmaYuku() {
		return ruloTestCekmeEnineAkmaYuku;
	}

	public void setRuloTestCekmeEnineAkmaYuku(Float ruloTestCekmeEnineAkmaYuku) {
		this.ruloTestCekmeEnineAkmaYuku = ruloTestCekmeEnineAkmaYuku;
	}

	public Float getRuloTestCekmeEnineCekmeYuku() {
		return ruloTestCekmeEnineCekmeYuku;
	}

	public void setRuloTestCekmeEnineCekmeYuku(Float ruloTestCekmeEnineCekmeYuku) {
		this.ruloTestCekmeEnineCekmeYuku = ruloTestCekmeEnineCekmeYuku;
	}

	public Float getRuloTestCekmeEnineAkmaDayanci() {
		return ruloTestCekmeEnineAkmaDayanci;
	}

	public void setRuloTestCekmeEnineAkmaDayanci(
			Float ruloTestCekmeEnineAkmaDayanci) {
		this.ruloTestCekmeEnineAkmaDayanci = ruloTestCekmeEnineAkmaDayanci;
	}

	public Float getRuloTestCekmeEnineCekmeDayanci() {
		return ruloTestCekmeEnineCekmeDayanci;
	}

	public void setRuloTestCekmeEnineCekmeDayanci(
			Float ruloTestCekmeEnineCekmeDayanci) {
		this.ruloTestCekmeEnineCekmeDayanci = ruloTestCekmeEnineCekmeDayanci;
	}

	public Float getRuloTestCekmeEnineAkmaCekme() {
		return ruloTestCekmeEnineAkmaCekme;
	}

	public void setRuloTestCekmeEnineAkmaCekme(Float ruloTestCekmeEnineAkmaCekme) {
		this.ruloTestCekmeEnineAkmaCekme = ruloTestCekmeEnineAkmaCekme;
	}

	public Float getRuloTestCekmeEnineUzama() {
		return ruloTestCekmeEnineUzama;
	}

	public void setRuloTestCekmeEnineUzama(Float ruloTestCekmeEnineUzama) {
		this.ruloTestCekmeEnineUzama = ruloTestCekmeEnineUzama;
	}

}
