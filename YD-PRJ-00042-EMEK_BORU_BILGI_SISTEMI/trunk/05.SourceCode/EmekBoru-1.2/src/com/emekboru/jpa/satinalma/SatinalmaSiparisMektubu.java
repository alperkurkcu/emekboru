package com.emekboru.jpa.satinalma;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the satinalma_siparis_mektubu database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "SatinalmaSiparisMektubu.findAll", query = "SELECT r FROM SatinalmaSiparisMektubu r order by r.id"),
		@NamedQuery(name = "SatinalmaSiparisMektubu.findByKararId", query = "SELECT r FROM SatinalmaSiparisMektubu r where r.satinalmaSatinalmaKarari.id=:prmDosyaId order by r.id") })
@Table(name = "satinalma_siparis_mektubu")
public class SatinalmaSiparisMektubu implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SATINALMA_SIPARIS_MEKTUBU_ID_GENERATOR", sequenceName = "SATINALMA_SIPARIS_MEKTUBU_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SATINALMA_SIPARIS_MEKTUBU_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	// @Column(name = "satinalma_karar_id", nullable = false)
	// private Integer satinalmaKararId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "satinalma_karar_id", referencedColumnName = "ID")
	private SatinalmaSatinalmaKarari satinalmaSatinalmaKarari;

	// @Column(name = "tedarikci_firma_id", nullable = false)
	// private Integer tedarikciFirmaId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "tedarikci_firma_id", referencedColumnName = "ID")
	private SatinalmaMusteri satinalmaMusteri;

	@Column(length = 2147483647, name = "aciklama")
	private String aciklama;

	@Column(name = "toplam")
	private double toplam;

	@Column(name = "toplam_kdv")
	private double toplamKdv;

	@Column(name = "toplam_tutar")
	private double toplamTutar;

	public SatinalmaSiparisMektubu() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	/**
	 * @return the satinalmaSatinalmaKarari
	 */
	public SatinalmaSatinalmaKarari getSatinalmaSatinalmaKarari() {
		return satinalmaSatinalmaKarari;
	}

	/**
	 * @param satinalmaSatinalmaKarari
	 *            the satinalmaSatinalmaKarari to set
	 */
	public void setSatinalmaSatinalmaKarari(
			SatinalmaSatinalmaKarari satinalmaSatinalmaKarari) {
		this.satinalmaSatinalmaKarari = satinalmaSatinalmaKarari;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the satinalmaMusteri
	 */
	public SatinalmaMusteri getSatinalmaMusteri() {
		return satinalmaMusteri;
	}

	/**
	 * @param satinalmaMusteri
	 *            the satinalmaMusteri to set
	 */
	public void setSatinalmaMusteri(SatinalmaMusteri satinalmaMusteri) {
		this.satinalmaMusteri = satinalmaMusteri;
	}

	/**
	 * @return the aciklama
	 */
	public String getAciklama() {
		return aciklama;
	}

	/**
	 * @param aciklama
	 *            the aciklama to set
	 */
	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	/**
	 * @return the toplam
	 */
	public double getToplam() {
		return toplam;
	}

	/**
	 * @param toplam
	 *            the toplam to set
	 */
	public void setToplam(double toplam) {
		this.toplam = toplam;
	}

	/**
	 * @return the toplamKdv
	 */
	public double getToplamKdv() {
		return toplamKdv;
	}

	/**
	 * @param toplamKdv
	 *            the toplamKdv to set
	 */
	public void setToplamKdv(double toplamKdv) {
		this.toplamKdv = toplamKdv;
	}

	/**
	 * @return the toplamTutar
	 */
	public double getToplamTutar() {
		return toplamTutar;
	}

	/**
	 * @param toplamTutar
	 *            the toplamTutar to set
	 */
	public void setToplamTutar(double toplamTutar) {
		this.toplamTutar = toplamTutar;
	}

}