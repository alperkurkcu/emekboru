package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.isolation.IsolationTestDefinition;

/**
 * The persistent class for the test_kaplama_process_bozulma_sonuc database
 * table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaProcessBozulmaSonuc.findAll", query = "SELECT r FROM TestKaplamaProcessBozulmaSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_kaplama_process_bozulma_sonuc")
public class TestKaplamaProcessBozulmaSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KAPLAMA_PROCESS_BOZULMA_SONUC_ID_GENERATOR", sequenceName = "TEST_KAPLAMA_PROCESS_BOZULMA_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KAPLAMA_PROCESS_BOZULMA_SONUC_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "extrude_erime_akis_hizi_1")
	private BigDecimal extrudeErimeAkisHizi1;

	@Column(name = "extrude_erime_akis_hizi_2")
	private BigDecimal extrudeErimeAkisHizi2;

	@Column(name = "extrude_erime_akis_hizi_3")
	private BigDecimal extrudeErimeAkisHizi3;

	@Column(name = "extrude_erime_akis_hizi_4")
	private BigDecimal extrudeErimeAkisHizi4;

	@Column(name = "extrude_erime_akis_hizi_5")
	private BigDecimal extrudeErimeAkisHizi5;

	@Column(name = "extrude_kesilen_parca_kutlesi_1")
	private BigDecimal extrudeKesilenParcaKutlesi1;

	@Column(name = "extrude_kesilen_parca_kutlesi_2")
	private BigDecimal extrudeKesilenParcaKutlesi2;

	@Column(name = "extrude_kesilen_parca_kutlesi_3")
	private BigDecimal extrudeKesilenParcaKutlesi3;

	@Column(name = "extrude_kesilen_parca_kutlesi_4")
	private BigDecimal extrudeKesilenParcaKutlesi4;

	@Column(name = "extrude_kesilen_parca_kutlesi_5")
	private BigDecimal extrudeKesilenParcaKutlesi5;

	@Column(name = "extrude_ortalama_erime_akis_hizi")
	private BigDecimal extrudeOrtalamaErimeAkisHizi;

	@Column(name = "extrude_sonuc")
	private String extrudeSonuc;

	@Column(name = "extrude_spesifikasyon_degeri")
	private BigDecimal extrudeSpesifikasyonDegeri;

	// @Column(name="global_id")
	// private Integer globalId;

	@Column(name = "hammadde_erime_akis_hizi_1")
	private BigDecimal hammaddeErimeAkisHizi1;

	@Column(name = "hammadde_erime_akis_hizi_2")
	private BigDecimal hammaddeErimeAkisHizi2;

	@Column(name = "hammadde_erime_akis_hizi_3")
	private BigDecimal hammaddeErimeAkisHizi3;

	@Column(name = "hammadde_erime_akis_hizi_4")
	private BigDecimal hammaddeErimeAkisHizi4;

	@Column(name = "hammadde_erime_akis_hizi_5")
	private BigDecimal hammaddeErimeAkisHizi5;

	@Column(name = "hammadde_kesilen_parca_kutlesi_1")
	private BigDecimal hammaddeKesilenParcaKutlesi1;

	@Column(name = "hammadde_kesilen_parca_kutlesi_2")
	private BigDecimal hammaddeKesilenParcaKutlesi2;

	@Column(name = "hammadde_kesilen_parca_kutlesi_3")
	private BigDecimal hammaddeKesilenParcaKutlesi3;

	@Column(name = "hammadde_kesilen_parca_kutlesi_4")
	private BigDecimal hammaddeKesilenParcaKutlesi4;

	@Column(name = "hammadde_kesilen_parca_kutlesi_5")
	private BigDecimal hammaddeKesilenParcaKutlesi5;

	@Column(name = "hammadde_ortalama_erime_akis_hizi")
	private BigDecimal hammaddeOrtalamaErimeAkisHizi;

	@Column(name = "hammadde_sonuc")
	private String hammaddeSonuc;

	@Column(name = "hammadde_uretici_degeri")
	private BigDecimal hammaddeUreticiDegeri;

	// @Column(name="pipe_id")
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi")
	private Date testTarihi;

	@Column(name = "vardiya")
	private Integer vardiya;

	@Column(name = "extrude_on_isitma")
	private BigDecimal extrudeOnIsitma;

	@Column(name = "extrude_sicaklik_yuk")
	private BigDecimal extrudeSicaklikYuk;

	@Column(name = "extrude_numune_kutle")
	private BigDecimal extrudeNumuneKutle;

	@Column(name = "extrude_kesilme_zamani")
	private BigDecimal extrudeKesilmeZamani;

	@Column(name = "hammadde_on_isitma")
	private BigDecimal hammaddeOnIsitma;

	@Column(name = "hammadde_sicaklik_yuk")
	private BigDecimal hammaddeSicaklikYuk;

	@Column(name = "hammadde_numune_kutle")
	private BigDecimal hammaddeNumuneKutle;

	@Column(name = "hammadde_kesilme_zamani")
	private BigDecimal hammaddeKesilmeZamani;

	public TestKaplamaProcessBozulmaSonuc() {
	}

	/**
	 * @return the eklemeZamani
	 */
	public Timestamp getEklemeZamani() {
		return eklemeZamani;
	}

	/**
	 * @param eklemeZamani
	 *            the eklemeZamani to set
	 */
	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncellemeZamani
	 */
	public Timestamp getGuncellemeZamani() {
		return guncellemeZamani;
	}

	/**
	 * @param guncellemeZamani
	 *            the guncellemeZamani to set
	 */
	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the extrudeErimeAkisHizi1
	 */
	public BigDecimal getExtrudeErimeAkisHizi1() {
		return extrudeErimeAkisHizi1;
	}

	/**
	 * @param extrudeErimeAkisHizi1
	 *            the extrudeErimeAkisHizi1 to set
	 */
	public void setExtrudeErimeAkisHizi1(BigDecimal extrudeErimeAkisHizi1) {
		this.extrudeErimeAkisHizi1 = extrudeErimeAkisHizi1;
	}

	/**
	 * @return the extrudeErimeAkisHizi2
	 */
	public BigDecimal getExtrudeErimeAkisHizi2() {
		return extrudeErimeAkisHizi2;
	}

	/**
	 * @param extrudeErimeAkisHizi2
	 *            the extrudeErimeAkisHizi2 to set
	 */
	public void setExtrudeErimeAkisHizi2(BigDecimal extrudeErimeAkisHizi2) {
		this.extrudeErimeAkisHizi2 = extrudeErimeAkisHizi2;
	}

	/**
	 * @return the extrudeErimeAkisHizi3
	 */
	public BigDecimal getExtrudeErimeAkisHizi3() {
		return extrudeErimeAkisHizi3;
	}

	/**
	 * @param extrudeErimeAkisHizi3
	 *            the extrudeErimeAkisHizi3 to set
	 */
	public void setExtrudeErimeAkisHizi3(BigDecimal extrudeErimeAkisHizi3) {
		this.extrudeErimeAkisHizi3 = extrudeErimeAkisHizi3;
	}

	/**
	 * @return the extrudeErimeAkisHizi4
	 */
	public BigDecimal getExtrudeErimeAkisHizi4() {
		return extrudeErimeAkisHizi4;
	}

	/**
	 * @param extrudeErimeAkisHizi4
	 *            the extrudeErimeAkisHizi4 to set
	 */
	public void setExtrudeErimeAkisHizi4(BigDecimal extrudeErimeAkisHizi4) {
		this.extrudeErimeAkisHizi4 = extrudeErimeAkisHizi4;
	}

	/**
	 * @return the extrudeErimeAkisHizi5
	 */
	public BigDecimal getExtrudeErimeAkisHizi5() {
		return extrudeErimeAkisHizi5;
	}

	/**
	 * @param extrudeErimeAkisHizi5
	 *            the extrudeErimeAkisHizi5 to set
	 */
	public void setExtrudeErimeAkisHizi5(BigDecimal extrudeErimeAkisHizi5) {
		this.extrudeErimeAkisHizi5 = extrudeErimeAkisHizi5;
	}

	/**
	 * @return the extrudeKesilenParcaKutlesi1
	 */
	public BigDecimal getExtrudeKesilenParcaKutlesi1() {
		return extrudeKesilenParcaKutlesi1;
	}

	/**
	 * @param extrudeKesilenParcaKutlesi1
	 *            the extrudeKesilenParcaKutlesi1 to set
	 */
	public void setExtrudeKesilenParcaKutlesi1(
			BigDecimal extrudeKesilenParcaKutlesi1) {
		this.extrudeKesilenParcaKutlesi1 = extrudeKesilenParcaKutlesi1;
	}

	/**
	 * @return the extrudeKesilenParcaKutlesi2
	 */
	public BigDecimal getExtrudeKesilenParcaKutlesi2() {
		return extrudeKesilenParcaKutlesi2;
	}

	/**
	 * @param extrudeKesilenParcaKutlesi2
	 *            the extrudeKesilenParcaKutlesi2 to set
	 */
	public void setExtrudeKesilenParcaKutlesi2(
			BigDecimal extrudeKesilenParcaKutlesi2) {
		this.extrudeKesilenParcaKutlesi2 = extrudeKesilenParcaKutlesi2;
	}

	/**
	 * @return the extrudeKesilenParcaKutlesi3
	 */
	public BigDecimal getExtrudeKesilenParcaKutlesi3() {
		return extrudeKesilenParcaKutlesi3;
	}

	/**
	 * @param extrudeKesilenParcaKutlesi3
	 *            the extrudeKesilenParcaKutlesi3 to set
	 */
	public void setExtrudeKesilenParcaKutlesi3(
			BigDecimal extrudeKesilenParcaKutlesi3) {
		this.extrudeKesilenParcaKutlesi3 = extrudeKesilenParcaKutlesi3;
	}

	/**
	 * @return the extrudeKesilenParcaKutlesi4
	 */
	public BigDecimal getExtrudeKesilenParcaKutlesi4() {
		return extrudeKesilenParcaKutlesi4;
	}

	/**
	 * @param extrudeKesilenParcaKutlesi4
	 *            the extrudeKesilenParcaKutlesi4 to set
	 */
	public void setExtrudeKesilenParcaKutlesi4(
			BigDecimal extrudeKesilenParcaKutlesi4) {
		this.extrudeKesilenParcaKutlesi4 = extrudeKesilenParcaKutlesi4;
	}

	/**
	 * @return the extrudeKesilenParcaKutlesi5
	 */
	public BigDecimal getExtrudeKesilenParcaKutlesi5() {
		return extrudeKesilenParcaKutlesi5;
	}

	/**
	 * @param extrudeKesilenParcaKutlesi5
	 *            the extrudeKesilenParcaKutlesi5 to set
	 */
	public void setExtrudeKesilenParcaKutlesi5(
			BigDecimal extrudeKesilenParcaKutlesi5) {
		this.extrudeKesilenParcaKutlesi5 = extrudeKesilenParcaKutlesi5;
	}

	/**
	 * @return the extrudeOrtalamaErimeAkisHizi
	 */
	public BigDecimal getExtrudeOrtalamaErimeAkisHizi() {
		return extrudeOrtalamaErimeAkisHizi;
	}

	/**
	 * @param extrudeOrtalamaErimeAkisHizi
	 *            the extrudeOrtalamaErimeAkisHizi to set
	 */
	public void setExtrudeOrtalamaErimeAkisHizi(
			BigDecimal extrudeOrtalamaErimeAkisHizi) {
		this.extrudeOrtalamaErimeAkisHizi = extrudeOrtalamaErimeAkisHizi;
	}

	/**
	 * @return the extrudeSpesifikasyonDegeri
	 */
	public BigDecimal getExtrudeSpesifikasyonDegeri() {
		return extrudeSpesifikasyonDegeri;
	}

	/**
	 * @param extrudeSpesifikasyonDegeri
	 *            the extrudeSpesifikasyonDegeri to set
	 */
	public void setExtrudeSpesifikasyonDegeri(
			BigDecimal extrudeSpesifikasyonDegeri) {
		this.extrudeSpesifikasyonDegeri = extrudeSpesifikasyonDegeri;
	}

	/**
	 * @return the hammaddeErimeAkisHizi1
	 */
	public BigDecimal getHammaddeErimeAkisHizi1() {
		return hammaddeErimeAkisHizi1;
	}

	/**
	 * @param hammaddeErimeAkisHizi1
	 *            the hammaddeErimeAkisHizi1 to set
	 */
	public void setHammaddeErimeAkisHizi1(BigDecimal hammaddeErimeAkisHizi1) {
		this.hammaddeErimeAkisHizi1 = hammaddeErimeAkisHizi1;
	}

	/**
	 * @return the hammaddeErimeAkisHizi2
	 */
	public BigDecimal getHammaddeErimeAkisHizi2() {
		return hammaddeErimeAkisHizi2;
	}

	/**
	 * @param hammaddeErimeAkisHizi2
	 *            the hammaddeErimeAkisHizi2 to set
	 */
	public void setHammaddeErimeAkisHizi2(BigDecimal hammaddeErimeAkisHizi2) {
		this.hammaddeErimeAkisHizi2 = hammaddeErimeAkisHizi2;
	}

	/**
	 * @return the hammaddeErimeAkisHizi3
	 */
	public BigDecimal getHammaddeErimeAkisHizi3() {
		return hammaddeErimeAkisHizi3;
	}

	/**
	 * @param hammaddeErimeAkisHizi3
	 *            the hammaddeErimeAkisHizi3 to set
	 */
	public void setHammaddeErimeAkisHizi3(BigDecimal hammaddeErimeAkisHizi3) {
		this.hammaddeErimeAkisHizi3 = hammaddeErimeAkisHizi3;
	}

	/**
	 * @return the hammaddeErimeAkisHizi4
	 */
	public BigDecimal getHammaddeErimeAkisHizi4() {
		return hammaddeErimeAkisHizi4;
	}

	/**
	 * @param hammaddeErimeAkisHizi4
	 *            the hammaddeErimeAkisHizi4 to set
	 */
	public void setHammaddeErimeAkisHizi4(BigDecimal hammaddeErimeAkisHizi4) {
		this.hammaddeErimeAkisHizi4 = hammaddeErimeAkisHizi4;
	}

	/**
	 * @return the hammaddeErimeAkisHizi5
	 */
	public BigDecimal getHammaddeErimeAkisHizi5() {
		return hammaddeErimeAkisHizi5;
	}

	/**
	 * @param hammaddeErimeAkisHizi5
	 *            the hammaddeErimeAkisHizi5 to set
	 */
	public void setHammaddeErimeAkisHizi5(BigDecimal hammaddeErimeAkisHizi5) {
		this.hammaddeErimeAkisHizi5 = hammaddeErimeAkisHizi5;
	}

	/**
	 * @return the hammaddeKesilenParcaKutlesi1
	 */
	public BigDecimal getHammaddeKesilenParcaKutlesi1() {
		return hammaddeKesilenParcaKutlesi1;
	}

	/**
	 * @param hammaddeKesilenParcaKutlesi1
	 *            the hammaddeKesilenParcaKutlesi1 to set
	 */
	public void setHammaddeKesilenParcaKutlesi1(
			BigDecimal hammaddeKesilenParcaKutlesi1) {
		this.hammaddeKesilenParcaKutlesi1 = hammaddeKesilenParcaKutlesi1;
	}

	/**
	 * @return the hammaddeKesilenParcaKutlesi2
	 */
	public BigDecimal getHammaddeKesilenParcaKutlesi2() {
		return hammaddeKesilenParcaKutlesi2;
	}

	/**
	 * @param hammaddeKesilenParcaKutlesi2
	 *            the hammaddeKesilenParcaKutlesi2 to set
	 */
	public void setHammaddeKesilenParcaKutlesi2(
			BigDecimal hammaddeKesilenParcaKutlesi2) {
		this.hammaddeKesilenParcaKutlesi2 = hammaddeKesilenParcaKutlesi2;
	}

	/**
	 * @return the hammaddeKesilenParcaKutlesi3
	 */
	public BigDecimal getHammaddeKesilenParcaKutlesi3() {
		return hammaddeKesilenParcaKutlesi3;
	}

	/**
	 * @param hammaddeKesilenParcaKutlesi3
	 *            the hammaddeKesilenParcaKutlesi3 to set
	 */
	public void setHammaddeKesilenParcaKutlesi3(
			BigDecimal hammaddeKesilenParcaKutlesi3) {
		this.hammaddeKesilenParcaKutlesi3 = hammaddeKesilenParcaKutlesi3;
	}

	/**
	 * @return the hammaddeKesilenParcaKutlesi4
	 */
	public BigDecimal getHammaddeKesilenParcaKutlesi4() {
		return hammaddeKesilenParcaKutlesi4;
	}

	/**
	 * @param hammaddeKesilenParcaKutlesi4
	 *            the hammaddeKesilenParcaKutlesi4 to set
	 */
	public void setHammaddeKesilenParcaKutlesi4(
			BigDecimal hammaddeKesilenParcaKutlesi4) {
		this.hammaddeKesilenParcaKutlesi4 = hammaddeKesilenParcaKutlesi4;
	}

	/**
	 * @return the hammaddeKesilenParcaKutlesi5
	 */
	public BigDecimal getHammaddeKesilenParcaKutlesi5() {
		return hammaddeKesilenParcaKutlesi5;
	}

	/**
	 * @param hammaddeKesilenParcaKutlesi5
	 *            the hammaddeKesilenParcaKutlesi5 to set
	 */
	public void setHammaddeKesilenParcaKutlesi5(
			BigDecimal hammaddeKesilenParcaKutlesi5) {
		this.hammaddeKesilenParcaKutlesi5 = hammaddeKesilenParcaKutlesi5;
	}

	/**
	 * @return the hammaddeOrtalamaErimeAkisHizi
	 */
	public BigDecimal getHammaddeOrtalamaErimeAkisHizi() {
		return hammaddeOrtalamaErimeAkisHizi;
	}

	/**
	 * @param hammaddeOrtalamaErimeAkisHizi
	 *            the hammaddeOrtalamaErimeAkisHizi to set
	 */
	public void setHammaddeOrtalamaErimeAkisHizi(
			BigDecimal hammaddeOrtalamaErimeAkisHizi) {
		this.hammaddeOrtalamaErimeAkisHizi = hammaddeOrtalamaErimeAkisHizi;
	}

	/**
	 * @return the extrudeSonuc
	 */
	public String getExtrudeSonuc() {
		return extrudeSonuc;
	}

	/**
	 * @param extrudeSonuc
	 *            the extrudeSonuc to set
	 */
	public void setExtrudeSonuc(String extrudeSonuc) {
		this.extrudeSonuc = extrudeSonuc;
	}

	/**
	 * @return the hammaddeSonuc
	 */
	public String getHammaddeSonuc() {
		return hammaddeSonuc;
	}

	/**
	 * @param hammaddeSonuc
	 *            the hammaddeSonuc to set
	 */
	public void setHammaddeSonuc(String hammaddeSonuc) {
		this.hammaddeSonuc = hammaddeSonuc;
	}

	/**
	 * @return the hammaddeUreticiDegeri
	 */
	public BigDecimal getHammaddeUreticiDegeri() {
		return hammaddeUreticiDegeri;
	}

	/**
	 * @param hammaddeUreticiDegeri
	 *            the hammaddeUreticiDegeri to set
	 */
	public void setHammaddeUreticiDegeri(BigDecimal hammaddeUreticiDegeri) {
		this.hammaddeUreticiDegeri = hammaddeUreticiDegeri;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the bagliTestId
	 */
	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the testTarihi
	 */
	public Date getTestTarihi() {
		return testTarihi;
	}

	/**
	 * @param testTarihi
	 *            the testTarihi to set
	 */
	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	/**
	 * @return the vardiya
	 */
	public Integer getVardiya() {
		return vardiya;
	}

	/**
	 * @param vardiya
	 *            the vardiya to set
	 */
	public void setVardiya(Integer vardiya) {
		this.vardiya = vardiya;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the bagliGlobalId
	 */
	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}

	/**
	 * @param bagliGlobalId
	 *            the bagliGlobalId to set
	 */
	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the extrudeOnIsitma
	 */
	public BigDecimal getExtrudeOnIsitma() {
		return extrudeOnIsitma;
	}

	/**
	 * @param extrudeOnIsitma
	 *            the extrudeOnIsitma to set
	 */
	public void setExtrudeOnIsitma(BigDecimal extrudeOnIsitma) {
		this.extrudeOnIsitma = extrudeOnIsitma;
	}

	/**
	 * @return the extrudeSicaklikYuk
	 */
	public BigDecimal getExtrudeSicaklikYuk() {
		return extrudeSicaklikYuk;
	}

	/**
	 * @param extrudeSicaklikYuk
	 *            the extrudeSicaklikYuk to set
	 */
	public void setExtrudeSicaklikYuk(BigDecimal extrudeSicaklikYuk) {
		this.extrudeSicaklikYuk = extrudeSicaklikYuk;
	}

	/**
	 * @return the extrudeNumuneKutle
	 */
	public BigDecimal getExtrudeNumuneKutle() {
		return extrudeNumuneKutle;
	}

	/**
	 * @param extrudeNumuneKutle
	 *            the extrudeNumuneKutle to set
	 */
	public void setExtrudeNumuneKutle(BigDecimal extrudeNumuneKutle) {
		this.extrudeNumuneKutle = extrudeNumuneKutle;
	}

	/**
	 * @return the extrudeKesilmeZamani
	 */
	public BigDecimal getExtrudeKesilmeZamani() {
		return extrudeKesilmeZamani;
	}

	/**
	 * @param extrudeKesilmeZamani
	 *            the extrudeKesilmeZamani to set
	 */
	public void setExtrudeKesilmeZamani(BigDecimal extrudeKesilmeZamani) {
		this.extrudeKesilmeZamani = extrudeKesilmeZamani;
	}

	/**
	 * @return the hammaddeOnIsitma
	 */
	public BigDecimal getHammaddeOnIsitma() {
		return hammaddeOnIsitma;
	}

	/**
	 * @param hammaddeOnIsitma
	 *            the hammaddeOnIsitma to set
	 */
	public void setHammaddeOnIsitma(BigDecimal hammaddeOnIsitma) {
		this.hammaddeOnIsitma = hammaddeOnIsitma;
	}

	/**
	 * @return the hammaddeSicaklikYuk
	 */
	public BigDecimal getHammaddeSicaklikYuk() {
		return hammaddeSicaklikYuk;
	}

	/**
	 * @param hammaddeSicaklikYuk
	 *            the hammaddeSicaklikYuk to set
	 */
	public void setHammaddeSicaklikYuk(BigDecimal hammaddeSicaklikYuk) {
		this.hammaddeSicaklikYuk = hammaddeSicaklikYuk;
	}

	/**
	 * @return the hammaddeNumuneKutle
	 */
	public BigDecimal getHammaddeNumuneKutle() {
		return hammaddeNumuneKutle;
	}

	/**
	 * @param hammaddeNumuneKutle
	 *            the hammaddeNumuneKutle to set
	 */
	public void setHammaddeNumuneKutle(BigDecimal hammaddeNumuneKutle) {
		this.hammaddeNumuneKutle = hammaddeNumuneKutle;
	}

	/**
	 * @return the hammaddeKesilmeZamani
	 */
	public BigDecimal getHammaddeKesilmeZamani() {
		return hammaddeKesilmeZamani;
	}

	/**
	 * @param hammaddeKesilmeZamani
	 *            the hammaddeKesilmeZamani to set
	 */
	public void setHammaddeKesilmeZamani(BigDecimal hammaddeKesilmeZamani) {
		this.hammaddeKesilmeZamani = hammaddeKesilmeZamani;
	}

}