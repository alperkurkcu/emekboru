package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.isolation.IsolationTestDefinition;

/**
 * The persistent class for the test_kaplama_kumlama_ortam_sartlari_sonuc
 * database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaKumlamaOrtamSartlariSonuc.findAll", query = "SELECT r FROM TestKaplamaKumlamaOrtamSartlariSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_kaplama_kumlama_ortam_sartlari_sonuc")
public class TestKaplamaKumlamaOrtamSartlariSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KAPLAMA_KUMLAMA_ORTAM_SARTLARI_SONUC_ID_GENERATOR", sequenceName = "TEST_KAPLAMA_KUMLAMA_ORTAM_SARTLARI_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KAPLAMA_KUMLAMA_ORTAM_SARTLARI_SONUC_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Column(name = "bagil_nem")
	private String bagilNem;

	@Column(name = "boru_sicakligi_once")
	private String boruSicakligiOnce;

	@Column(name = "boru_sicakligi_sonra")
	private String boruSicakligiSonra;

	@Column(name = "celik_yuzey_ciylenme_sicakligi")
	private String celikYuzeyCiylenmeSicakligi;

	@Column(name = "ciylenme_sicakligi")
	private String ciylenmeSicakligi;

	@Column(name = "dewpoint_sicakligi")
	private String dewpointSicakligi;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ekleme_zamani")
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	// @Column(name="global_id")
	// private Integer globalId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "hava_sicakligi")
	private String havaSicakligi;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "kumlama_oncesi_tarih_saat")
	private Date kumlamaOncesiTarihSaat;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "kumlama_sonrasi_tarih_saat")
	private Date kumlamaSonrasiTarihSaat;

	@Column(name = "kumlamadan_once")
	private Boolean kumlamadanOnce;

	@Column(name = "kumlamadan_sonra")
	private Boolean kumlamadanSonra;

	// @Column(name="pipe_id")
	// private Integer pipeId;

	@Column(name = "sonuc_1")
	private Boolean sonuc1;

	@Column(name = "sonuc_2")
	private Boolean sonuc2;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi")
	private Date testTarihi;

	@Column(name = "vardiya")
	private Integer vardiya;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;

	public TestKaplamaKumlamaOrtamSartlariSonuc() {
	}

	/**
	 * @return the bagliTestId
	 */
	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Date getKumlamaOncesiTarihSaat() {
		return this.kumlamaOncesiTarihSaat;
	}

	public void setKumlamaOncesiTarihSaat(Date kumlamaOncesiTarihSaat) {
		this.kumlamaOncesiTarihSaat = kumlamaOncesiTarihSaat;
	}

	public Date getKumlamaSonrasiTarihSaat() {
		return this.kumlamaSonrasiTarihSaat;
	}

	public void setKumlamaSonrasiTarihSaat(Date kumlamaSonrasiTarihSaat) {
		this.kumlamaSonrasiTarihSaat = kumlamaSonrasiTarihSaat;
	}

	public Boolean getKumlamadanOnce() {
		return this.kumlamadanOnce;
	}

	public void setKumlamadanOnce(Boolean kumlamadanOnce) {
		this.kumlamadanOnce = kumlamadanOnce;
	}

	public Boolean getKumlamadanSonra() {
		return this.kumlamadanSonra;
	}

	public void setKumlamadanSonra(Boolean kumlamadanSonra) {
		this.kumlamadanSonra = kumlamadanSonra;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the bagliGlobalId
	 */
	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}

	/**
	 * @param bagliGlobalId
	 *            the bagliGlobalId to set
	 */
	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Boolean getSonuc1() {
		return this.sonuc1;
	}

	public void setSonuc1(Boolean sonuc1) {
		this.sonuc1 = sonuc1;
	}

	public Boolean getSonuc2() {
		return this.sonuc2;
	}

	public void setSonuc2(Boolean sonuc2) {
		this.sonuc2 = sonuc2;
	}

	public Date getTestTarihi() {
		return this.testTarihi;
	}

	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	public Integer getVardiya() {
		return this.vardiya;
	}

	public void setVardiya(Integer vardiya) {
		this.vardiya = vardiya;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the bagilNem
	 */
	public String getBagilNem() {
		return bagilNem;
	}

	/**
	 * @param bagilNem
	 *            the bagilNem to set
	 */
	public void setBagilNem(String bagilNem) {
		this.bagilNem = bagilNem;
	}

	/**
	 * @return the boruSicakligiOnce
	 */
	public String getBoruSicakligiOnce() {
		return boruSicakligiOnce;
	}

	/**
	 * @param boruSicakligiOnce
	 *            the boruSicakligiOnce to set
	 */
	public void setBoruSicakligiOnce(String boruSicakligiOnce) {
		this.boruSicakligiOnce = boruSicakligiOnce;
	}

	/**
	 * @return the boruSicakligiSonra
	 */
	public String getBoruSicakligiSonra() {
		return boruSicakligiSonra;
	}

	/**
	 * @param boruSicakligiSonra
	 *            the boruSicakligiSonra to set
	 */
	public void setBoruSicakligiSonra(String boruSicakligiSonra) {
		this.boruSicakligiSonra = boruSicakligiSonra;
	}

	/**
	 * @return the celikYuzeyCiylenmeSicakligi
	 */
	public String getCelikYuzeyCiylenmeSicakligi() {
		return celikYuzeyCiylenmeSicakligi;
	}

	/**
	 * @param celikYuzeyCiylenmeSicakligi
	 *            the celikYuzeyCiylenmeSicakligi to set
	 */
	public void setCelikYuzeyCiylenmeSicakligi(
			String celikYuzeyCiylenmeSicakligi) {
		this.celikYuzeyCiylenmeSicakligi = celikYuzeyCiylenmeSicakligi;
	}

	/**
	 * @return the ciylenmeSicakligi
	 */
	public String getCiylenmeSicakligi() {
		return ciylenmeSicakligi;
	}

	/**
	 * @param ciylenmeSicakligi
	 *            the ciylenmeSicakligi to set
	 */
	public void setCiylenmeSicakligi(String ciylenmeSicakligi) {
		this.ciylenmeSicakligi = ciylenmeSicakligi;
	}

	/**
	 * @return the dewpointSicakligi
	 */
	public String getDewpointSicakligi() {
		return dewpointSicakligi;
	}

	/**
	 * @param dewpointSicakligi
	 *            the dewpointSicakligi to set
	 */
	public void setDewpointSicakligi(String dewpointSicakligi) {
		this.dewpointSicakligi = dewpointSicakligi;
	}

	/**
	 * @return the havaSicakligi
	 */
	public String getHavaSicakligi() {
		return havaSicakligi;
	}

	/**
	 * @param havaSicakligi
	 *            the havaSicakligi to set
	 */
	public void setHavaSicakligi(String havaSicakligi) {
		this.havaSicakligi = havaSicakligi;
	}

}