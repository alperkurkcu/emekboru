package com.emekboru.jpa.isemri;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Machine;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the is_emri_imalat database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "IsEmriImalat.findAllByItemId", query = "SELECT r FROM IsEmriImalat r WHERE r.salesItem.itemId=:prmItemId order by r.eklemeZamani") })
@Table(name = "is_emri_imalat")
public class IsEmriImalat implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IS_EMRI_IMALAT_ID_GENERATOR", sequenceName = "IS_EMRI_IMALAT_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IS_EMRI_IMALAT_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "aciklama")
	private String aciklama;

	@Column(name = "birim_agirlik")
	private String birimAgirlik;

	@Column(name = "dis_ac_amper")
	private String disAcAmper;

	@Column(name = "dis_ac_voltaj")
	private String disAcVoltaj;

	@Column(name = "dis_dc_amper")
	private String disDcAmper;

	@Column(name = "dis_dc_voltaj")
	private String disDcVoltaj;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;
	//
	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "hedeflenen_ayar_suresi")
	private String hedeflenenAyarSuresi;

	@Column(name = "helis_acisi")
	private String helisAcisi;

	// @Column(name = "ht_kontrol_kullanici")
	// private Integer htKontrolKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ht_kontrol_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser htKontrolEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "ht_kontrol_zamani")
	private Date htKontrolZamani;

	@Column(name = "ic_ac_amper")
	private String icAcAmper;

	@Column(name = "ic_ac_voltaj")
	private String icAcVoltaj;

	@Column(name = "ic_dc_amper")
	private String icDcAmper;

	@Column(name = "ic_dc_voltaj")
	private String icDcVoltaj;

	@Column(name = "ilave_torna_payi")
	private String ilaveTornaPayi;

	// @Column(name = "klemp_kontrol_kullanici")
	// private Integer klempKontrolKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "klemp_kontrol_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser klempKontrolEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "klemp_kontrol_zamani")
	private Date klempKontrolZamani;

	@Column(name = "lazer_sistemi")
	private String lazerSistemi;

	// @Column(name = "machine_id")
	// private Integer machineId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "machine_id", referencedColumnName = "machine_id", insertable = false)
	private Machine machine;

	@Column(name = "sac_genisligi")
	private String sacGenisligi;

	// @Column(name = "sales_item_id")
	// private Integer salesItemId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	private SalesItem salesItem;

	@Column(name = "tezgah_hizi")
	private String tezgahHizi;

	// @Column(name = "tezgah_kontrol_kullanici")
	// private Integer tezgahKontrolKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tezgah_kontrol_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser tezgahKontrolEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "tezgah_kontrol_zamani")
	private Date tezgahKontrolZamani;

	@Column(name = "toz_ilave_miktari")
	private String tozIlaveMiktari;

	@Column(name = "vardiyada_veri_1")
	private String vardiyadaVeri1;

	@Column(name = "vardiyada_veri_2")
	private String vardiyadaVeri2;

	@Column(name = "vardiyada_veri_3")
	private String vardiyadaVeri3;

	public IsEmriImalat() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public String getBirimAgirlik() {
		return this.birimAgirlik;
	}

	public void setBirimAgirlik(String birimAgirlik) {
		this.birimAgirlik = birimAgirlik;
	}

	public String getDisAcAmper() {
		return this.disAcAmper;
	}

	public void setDisAcAmper(String disAcAmper) {
		this.disAcAmper = disAcAmper;
	}

	public String getDisAcVoltaj() {
		return this.disAcVoltaj;
	}

	public void setDisAcVoltaj(String disAcVoltaj) {
		this.disAcVoltaj = disAcVoltaj;
	}

	public String getDisDcAmper() {
		return this.disDcAmper;
	}

	public void setDisDcAmper(String disDcAmper) {
		this.disDcAmper = disDcAmper;
	}

	public String getDisDcVoltaj() {
		return this.disDcVoltaj;
	}

	public void setDisDcVoltaj(String disDcVoltaj) {
		this.disDcVoltaj = disDcVoltaj;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public String getHedeflenenAyarSuresi() {
		return this.hedeflenenAyarSuresi;
	}

	public void setHedeflenenAyarSuresi(String hedeflenenAyarSuresi) {
		this.hedeflenenAyarSuresi = hedeflenenAyarSuresi;
	}

	public String getHelisAcisi() {
		return this.helisAcisi;
	}

	public void setHelisAcisi(String helisAcisi) {
		this.helisAcisi = helisAcisi;
	}

	public Date getHtKontrolZamani() {
		return this.htKontrolZamani;
	}

	public void setHtKontrolZamani(Date htKontrolZamani) {
		this.htKontrolZamani = htKontrolZamani;
	}

	public String getIcAcAmper() {
		return this.icAcAmper;
	}

	public void setIcAcAmper(String icAcAmper) {
		this.icAcAmper = icAcAmper;
	}

	public String getIcAcVoltaj() {
		return this.icAcVoltaj;
	}

	public void setIcAcVoltaj(String icAcVoltaj) {
		this.icAcVoltaj = icAcVoltaj;
	}

	public String getIcDcAmper() {
		return this.icDcAmper;
	}

	public void setIcDcAmper(String icDcAmper) {
		this.icDcAmper = icDcAmper;
	}

	public String getIcDcVoltaj() {
		return this.icDcVoltaj;
	}

	public void setIcDcVoltaj(String icDcVoltaj) {
		this.icDcVoltaj = icDcVoltaj;
	}

	public String getIlaveTornaPayi() {
		return this.ilaveTornaPayi;
	}

	public void setIlaveTornaPayi(String ilaveTornaPayi) {
		this.ilaveTornaPayi = ilaveTornaPayi;
	}

	public Date getKlempKontrolZamani() {
		return this.klempKontrolZamani;
	}

	public void setKlempKontrolZamani(Date klempKontrolZamani) {
		this.klempKontrolZamani = klempKontrolZamani;
	}

	public String getLazerSistemi() {
		return this.lazerSistemi;
	}

	public void setLazerSistemi(String lazerSistemi) {
		this.lazerSistemi = lazerSistemi;
	}

	public String getSacGenisligi() {
		return this.sacGenisligi;
	}

	public void setSacGenisligi(String sacGenisligi) {
		this.sacGenisligi = sacGenisligi;
	}

	public String getTezgahHizi() {
		return this.tezgahHizi;
	}

	public void setTezgahHizi(String tezgahHizi) {
		this.tezgahHizi = tezgahHizi;
	}

	public Date getTezgahKontrolZamani() {
		return this.tezgahKontrolZamani;
	}

	public void setTezgahKontrolZamani(Date tezgahKontrolZamani) {
		this.tezgahKontrolZamani = tezgahKontrolZamani;
	}

	public String getTozIlaveMiktari() {
		return this.tozIlaveMiktari;
	}

	public void setTozIlaveMiktari(String tozIlaveMiktari) {
		this.tozIlaveMiktari = tozIlaveMiktari;
	}

	public String getVardiyadaVeri1() {
		return this.vardiyadaVeri1;
	}

	public void setVardiyadaVeri1(String vardiyadaVeri1) {
		this.vardiyadaVeri1 = vardiyadaVeri1;
	}

	public String getVardiyadaVeri2() {
		return this.vardiyadaVeri2;
	}

	public void setVardiyadaVeri2(String vardiyadaVeri2) {
		this.vardiyadaVeri2 = vardiyadaVeri2;
	}

	public String getVardiyadaVeri3() {
		return this.vardiyadaVeri3;
	}

	public void setVardiyadaVeri3(String vardiyadaVeri3) {
		this.vardiyadaVeri3 = vardiyadaVeri3;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the htKontrolEmployee
	 */
	public SystemUser getHtKontrolEmployee() {
		return htKontrolEmployee;
	}

	/**
	 * @param htKontrolEmployee
	 *            the htKontrolEmployee to set
	 */
	public void setHtKontrolEmployee(SystemUser htKontrolEmployee) {
		this.htKontrolEmployee = htKontrolEmployee;
	}

	/**
	 * @return the klempKontrolEmployee
	 */
	public SystemUser getKlempKontrolEmployee() {
		return klempKontrolEmployee;
	}

	/**
	 * @param klempKontrolEmployee
	 *            the klempKontrolEmployee to set
	 */
	public void setKlempKontrolEmployee(SystemUser klempKontrolEmployee) {
		this.klempKontrolEmployee = klempKontrolEmployee;
	}

	/**
	 * @return the machine
	 */
	public Machine getMachine() {
		return machine;
	}

	/**
	 * @param machine
	 *            the machine to set
	 */
	public void setMachine(Machine machine) {
		this.machine = machine;
	}

	/**
	 * @return the salesItem
	 */
	public SalesItem getSalesItem() {
		return salesItem;
	}

	/**
	 * @param salesItem
	 *            the salesItem to set
	 */
	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	/**
	 * @return the tezgahKontrolEmployee
	 */
	public SystemUser getTezgahKontrolEmployee() {
		return tezgahKontrolEmployee;
	}

	/**
	 * @param tezgahKontrolEmployee
	 *            the tezgahKontrolEmployee to set
	 */
	public void setTezgahKontrolEmployee(SystemUser tezgahKontrolEmployee) {
		this.tezgahKontrolEmployee = tezgahKontrolEmployee;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}