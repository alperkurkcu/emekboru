package com.emekboru.jpa.sales;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.emekboru.jpa.sales.customer.SalesCRMVisit;

@Entity
@Table(name = "sales_status")
public class SalesStatus implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3081230025866237481L;

	@Id
	@Column(name = "status_id")
	private int statusId;

	@Column(name = "status")
	private String status;

	@OneToMany
	private List<SalesCRMVisit> salesCRMVisit;

	public SalesStatus() {
	}

	@Override
	public boolean equals(Object o) {
		return this.getStatusId() == ((SalesStatus) o).getStatusId();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<SalesCRMVisit> getSalesCRMVisit() {
		return salesCRMVisit;
	}

	public void setSalesCRMVisit(List<SalesCRMVisit> salesCRMVisit) {
		this.salesCRMVisit = salesCRMVisit;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
