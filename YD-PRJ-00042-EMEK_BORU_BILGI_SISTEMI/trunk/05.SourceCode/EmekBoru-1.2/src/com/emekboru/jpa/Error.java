package com.emekboru.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the errors database table.
 * 
 */
@Entity
@Table(name="errors")
public class Error implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="error_id")
	@SequenceGenerator(name="ERROR_GENERATOR", sequenceName="ERROR_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ERROR_GENERATOR")	
	private Integer errorId;

    @Temporal( TemporalType.TIMESTAMP)
	private Date date;

	@Column(name="duration")
	private double duration;

	@Column(name="machine_id")
	private Integer machineId;

	private String note;

    public Error() {
    }

	public Integer getErrorId() {
		return this.errorId;
	}

	public void setErrorId(Integer errorId) {
		this.errorId = errorId;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getDuration() {
		return this.duration;
	}

	public void setDuration(double duration) {
		this.duration = duration;
	}

	public Integer getMachineId() {
		return this.machineId;
	}

	public void setMachineId(Integer machineId) {
		this.machineId = machineId;
	}

	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}
}