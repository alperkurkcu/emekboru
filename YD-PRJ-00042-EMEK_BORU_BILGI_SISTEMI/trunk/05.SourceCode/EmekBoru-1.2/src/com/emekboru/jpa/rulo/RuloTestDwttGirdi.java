package com.emekboru.jpa.rulo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the rulo_test_dwtt_girdi database table.
 * 
 */
@Entity
@Table(name = "rulo_test_dwtt_girdi")
public class RuloTestDwttGirdi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "RULO_TEST_DWTT_GIRDI_RULODWTTGIRDIID_GENERATOR", sequenceName = "RULO_TEST_DWTT_GIRDI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RULO_TEST_DWTT_GIRDI_RULODWTTGIRDIID_GENERATOR")
	@Column(name = "rulo_dwtt_girdi_id", unique = true, nullable = false)
	private Integer ruloDwttGirdiId;

	@Column(name = "durum")
	private Integer durum = 0;

	@Column(name = "islem_yapan_id")
	private Integer islemYapanId;

	@Column(name = "islem_yapan_isim", length = 2147483647)
	private String islemYapanIsim;

	@Column(name = "rulo_dwtt_girdi_aciklama", length = 2147483647)
	private String ruloDwttGirdiAciklama;

	@Column(name = "rulo_dwtt_girdi_kirilma_alani_bir")
	private float ruloDwttGirdiKirilmaAlaniBir;

	@Column(name = "rulo_dwtt_girdi_kirilma_alani_iki")
	private float ruloDwttGirdiKirilmaAlaniIki;

	@Column(name = "rulo_dwtt_girdi_ortalama_kirilma_alani")
	private float ruloDwttGirdiOrtalamaKirilmaAlani;

	@Column(name = "rulo_dwtt_girdi_sicaklik")
	private float ruloDwttGirdiSicaklik;

	@Temporal(TemporalType.DATE)
	@Column(name = "rulo_dwtt_girdi_tarih")
	private Date ruloDwttGirdiTarih;

	// @Column(name = "rulo_id")
	// private Integer ruloId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "rulo_id", nullable = false)
	private Rulo rulo;

	public RuloTestDwttGirdi() {
	}

	public Integer getRuloDwttGirdiId() {
		return this.ruloDwttGirdiId;
	}

	public void setRuloDwttGirdiId(Integer ruloDwttGirdiId) {
		this.ruloDwttGirdiId = ruloDwttGirdiId;
	}

	public Integer getDurum() {
		return this.durum;
	}

	public void setDurum(Integer durum) {
		this.durum = durum;
	}

	public Integer getIslemYapanId() {
		return this.islemYapanId;
	}

	public void setIslemYapanId(Integer islemYapanId) {
		this.islemYapanId = islemYapanId;
	}

	public String getIslemYapanIsim() {
		return this.islemYapanIsim;
	}

	public void setIslemYapanIsim(String islemYapanIsim) {
		this.islemYapanIsim = islemYapanIsim;
	}

	public String getRuloDwttGirdiAciklama() {
		return this.ruloDwttGirdiAciklama;
	}

	public void setRuloDwttGirdiAciklama(String ruloDwttGirdiAciklama) {
		this.ruloDwttGirdiAciklama = ruloDwttGirdiAciklama;
	}

	public float getRuloDwttGirdiKirilmaAlaniBir() {
		return this.ruloDwttGirdiKirilmaAlaniBir;
	}

	public void setRuloDwttGirdiKirilmaAlaniBir(
			float ruloDwttGirdiKirilmaAlaniBir) {
		this.ruloDwttGirdiKirilmaAlaniBir = ruloDwttGirdiKirilmaAlaniBir;
	}

	public float getRuloDwttGirdiKirilmaAlaniIki() {
		return this.ruloDwttGirdiKirilmaAlaniIki;
	}

	public void setRuloDwttGirdiKirilmaAlaniIki(
			float ruloDwttGirdiKirilmaAlaniIki) {
		this.ruloDwttGirdiKirilmaAlaniIki = ruloDwttGirdiKirilmaAlaniIki;
	}

	public float getRuloDwttGirdiOrtalamaKirilmaAlani() {
		return this.ruloDwttGirdiOrtalamaKirilmaAlani;
	}

	public void setRuloDwttGirdiOrtalamaKirilmaAlani(
			float ruloDwttGirdiOrtalamaKirilmaAlani) {
		this.ruloDwttGirdiOrtalamaKirilmaAlani = ruloDwttGirdiOrtalamaKirilmaAlani;
	}

	public float getRuloDwttGirdiSicaklik() {
		return this.ruloDwttGirdiSicaklik;
	}

	public void setRuloDwttGirdiSicaklik(float ruloDwttGirdiSicaklik) {
		this.ruloDwttGirdiSicaklik = ruloDwttGirdiSicaklik;
	}

	/**
	 * @return the rulo
	 */
	public Rulo getRulo() {
		return rulo;
	}

	/**
	 * @param rulo
	 *            the rulo to set
	 */
	public void setRulo(Rulo rulo) {
		this.rulo = rulo;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the ruloDwttGirdiTarih
	 */
	public Date getRuloDwttGirdiTarih() {
		return ruloDwttGirdiTarih;
	}

	/**
	 * @param ruloDwttGirdiTarih
	 *            the ruloDwttGirdiTarih to set
	 */
	public void setRuloDwttGirdiTarih(Date ruloDwttGirdiTarih) {
		this.ruloDwttGirdiTarih = ruloDwttGirdiTarih;
	}

}