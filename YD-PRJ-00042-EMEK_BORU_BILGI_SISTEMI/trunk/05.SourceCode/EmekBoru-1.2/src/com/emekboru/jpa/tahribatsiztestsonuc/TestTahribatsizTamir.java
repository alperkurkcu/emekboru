package com.emekboru.jpa.tahribatsiztestsonuc;

import static javax.persistence.CascadeType.REFRESH;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.emekboru.jpa.Employee;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the test_tahribatsiz_tamir database table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "TestTahribatsizTamir.findAll", query = "SELECT r FROM TestTahribatsizTamir r WHERE r.pipe.pipeId=:prmPipeId order by r.eklemeZamani"),
		@NamedQuery(name = "TestTahribatsizTamir.findByPipeIdManuelId", query = "SELECT r FROM TestTahribatsizTamir r WHERE r.pipe.pipeId=:prmPipeId and r.testTahribatsizManuelUtSonuc.id=:prmManuelId order by r.eklemeZamani") })
@Table(name = "test_tahribatsiz_tamir")
public class TestTahribatsizTamir implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_TAHRIBATSIZ_TAMIR_ID_GENERATOR", sequenceName = "TEST_TAHRIBATSIZ_TAMIR_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_TAHRIBATSIZ_TAMIR_ID_GENERATOR")
	@Column(name = "ID", unique = true, nullable = false)
	private Integer id;

	private Boolean durum;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser user;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	// @Column(name = "manuel_ut_id")
	// private Integer manuelUtId;

	@OneToOne(fetch = FetchType.EAGER, cascade = REFRESH)
	@JoinColumn(name = "manuel_ut_id", referencedColumnName = "ID", insertable = false, updatable = true)
	private TestTahribatsizManuelUtSonuc testTahribatsizManuelUtSonuc;

	// @Column(name = "fl_id")
	// private Integer flId;

	@OneToOne(fetch = FetchType.EAGER, cascade = REFRESH)
	@JoinColumn(name = "fl_id", referencedColumnName = "ID", insertable = false, updatable = true)
	private TestTahribatsizFloroskopikSonuc testTahribatsizFloroskopikSonuc;

	@Column(name = "muayene_baslama_zamani")
	private Timestamp muayeneBaslamaZamani;

	@Column(name = "muayene_baslama_kullanici")
	private Integer muayeneBaslamaKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "muayene_baslama_kullanici", referencedColumnName = "employee_id", insertable = false, updatable = false)
	private Employee muayeneBaslamaUser;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@Column(name = "muayene_bitis_zamani")
	private Timestamp muayeneBitisZamani;

	@Column(name = "muayene_bitis_kullanici")
	private Integer muayeneBitisKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "muayene_bitis_kullanici", referencedColumnName = "employee_id", insertable = false, updatable = false)
	private Employee muayeneBitisUser;

	@Column(name = "muayene_bitis_zamani_kalan")
	private Timestamp muayeneBitisZamaniKalan;

	@Column(name = "muayene_bitis_kullanici_kalan")
	private Integer muayeneBitisKullaniciKalan;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "muayene_bitis_kullanici_kalan", referencedColumnName = "employee_id", insertable = false, updatable = false)
	private Employee muayeneBitisUserKalan;

	@Transient
	private Boolean yarimKaldi = false;

	@Transient
	private Boolean yarimTamamlandi = false;

	@Column(name = "muayene_bitis_kaynak_uzunluk")
	private Integer muayeneBitisKaynakUzunluk;

	@Column(name = "muayene_bitis_kalan_kaynak_uzunluk")
	private Integer muayeneBitisKalanKaynakUzunluk;

	@Column(name = "muayene_bitis_kaynak_agirlik")
	private Integer muayeneBitisKaynakAgirlik;

	@Column(name = "muayene_bitis_kalan_kaynak_agirlik")
	private Integer muayeneBitisKalanKaynakAgirlik;

	// @Column(name = "yardimci_kullanici", nullable = false)
	// private Integer yardimciKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "yardımci_kullanici", referencedColumnName = "employee_id", insertable = false)
	private Employee yardimciKullanici;

	public TestTahribatsizTamir() {

	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getDurum() {
		return this.durum;
	}

	public void setDurum(Boolean durum) {
		this.durum = durum;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public TestTahribatsizManuelUtSonuc getTestTahribatsizManuelUtSonuc() {
		return testTahribatsizManuelUtSonuc;
	}

	public void setTestTahribatsizManuelUtSonuc(
			TestTahribatsizManuelUtSonuc testTahribatsizManuelUtSonuc) {
		this.testTahribatsizManuelUtSonuc = testTahribatsizManuelUtSonuc;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public SystemUser getUser() {
		return user;
	}

	public void setUser(SystemUser user) {
		this.user = user;
	}

	public Timestamp getMuayeneBaslamaZamani() {
		return muayeneBaslamaZamani;
	}

	public void setMuayeneBaslamaZamani(Timestamp muayeneBaslamaZamani) {
		this.muayeneBaslamaZamani = muayeneBaslamaZamani;
	}

	public Integer getMuayeneBaslamaKullanici() {
		return muayeneBaslamaKullanici;
	}

	public void setMuayeneBaslamaKullanici(Integer muayeneBaslamaKullanici) {
		this.muayeneBaslamaKullanici = muayeneBaslamaKullanici;
	}

	public Timestamp getMuayeneBitisZamani() {
		return muayeneBitisZamani;
	}

	public void setMuayeneBitisZamani(Timestamp muayeneBitisZamani) {
		this.muayeneBitisZamani = muayeneBitisZamani;
	}

	public Integer getMuayeneBitisKullanici() {
		return muayeneBitisKullanici;
	}

	public void setMuayeneBitisKullanici(Integer muayeneBitisKullanici) {
		this.muayeneBitisKullanici = muayeneBitisKullanici;
	}

	public TestTahribatsizFloroskopikSonuc getTestTahribatsizFloroskopikSonuc() {
		return testTahribatsizFloroskopikSonuc;
	}

	public void setTestTahribatsizFloroskopikSonuc(
			TestTahribatsizFloroskopikSonuc testTahribatsizFloroskopikSonuc) {
		this.testTahribatsizFloroskopikSonuc = testTahribatsizFloroskopikSonuc;
	}

	public Employee getMuayeneBaslamaUser() {
		return muayeneBaslamaUser;
	}

	public void setMuayeneBaslamaUser(Employee muayeneBaslamaUser) {
		this.muayeneBaslamaUser = muayeneBaslamaUser;
	}

	public Employee getMuayeneBitisUser() {
		return muayeneBitisUser;
	}

	public void setMuayeneBitisUser(Employee muayeneBitisUser) {
		this.muayeneBitisUser = muayeneBitisUser;
	}

	public Timestamp getMuayeneBitisZamaniKalan() {
		return muayeneBitisZamaniKalan;
	}

	public void setMuayeneBitisZamaniKalan(Timestamp muayeneBitisZamaniKalan) {
		this.muayeneBitisZamaniKalan = muayeneBitisZamaniKalan;
	}

	public Integer getMuayeneBitisKullaniciKalan() {
		return muayeneBitisKullaniciKalan;
	}

	public void setMuayeneBitisKullaniciKalan(Integer muayeneBitisKullaniciKalan) {
		this.muayeneBitisKullaniciKalan = muayeneBitisKullaniciKalan;
	}

	public Employee getMuayeneBitisUserKalan() {
		return muayeneBitisUserKalan;
	}

	public void setMuayeneBitisUserKalan(Employee muayeneBitisUserKalan) {
		this.muayeneBitisUserKalan = muayeneBitisUserKalan;
	}

	public Boolean getYarimKaldi() {
		return yarimKaldi;
	}

	public void setYarimKaldi(Boolean yarimKaldi) {
		this.yarimKaldi = yarimKaldi;
	}

	public Boolean getYarimTamamlandi() {
		return yarimTamamlandi;
	}

	public void setYarimTamamlandi(Boolean yarimTamamlandi) {
		this.yarimTamamlandi = yarimTamamlandi;
	}

	public Integer getMuayeneBitisKaynakUzunluk() {
		return muayeneBitisKaynakUzunluk;
	}

	public void setMuayeneBitisKaynakUzunluk(Integer muayeneBitisKaynakUzunluk) {
		this.muayeneBitisKaynakUzunluk = muayeneBitisKaynakUzunluk;
	}

	public Integer getMuayeneBitisKalanKaynakUzunluk() {
		return muayeneBitisKalanKaynakUzunluk;
	}

	public void setMuayeneBitisKalanKaynakUzunluk(
			Integer muayeneBitisKalanKaynakUzunluk) {
		this.muayeneBitisKalanKaynakUzunluk = muayeneBitisKalanKaynakUzunluk;
	}

	public Integer getMuayeneBitisKaynakAgirlik() {
		return muayeneBitisKaynakAgirlik;
	}

	public void setMuayeneBitisKaynakAgirlik(Integer muayeneBitisKaynakAgirlik) {
		this.muayeneBitisKaynakAgirlik = muayeneBitisKaynakAgirlik;
	}

	public Integer getMuayeneBitisKalanKaynakAgirlik() {
		return muayeneBitisKalanKaynakAgirlik;
	}

	public void setMuayeneBitisKalanKaynakAgirlik(
			Integer muayeneBitisKalanKaynakAgirlik) {
		this.muayeneBitisKalanKaynakAgirlik = muayeneBitisKalanKaynakAgirlik;
	}

	/**
	 * @return the yardimciKullanici
	 */
	public Employee getYardimciKullanici() {
		return yardimciKullanici;
	}

	/**
	 * @param yardimciKullanici
	 *            the yardimciKullanici to set
	 */
	public void setYardimciKullanici(Employee yardimciKullanici) {
		this.yardimciKullanici = yardimciKullanici;
	}

}