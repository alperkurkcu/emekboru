package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;

/**
 * The persistent class for the kaplama_kalite_takip_formu database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "KaplamaKaliteTakipFormu.findAll", query = "SELECT r FROM KaplamaKaliteTakipFormu r WHERE r.pipe.pipeId=:prmPipeId order by r.eklemeZamani") })
@Table(name = "kaplama_kalite_takip_formu")
public class KaplamaKaliteTakipFormu implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "KAPLAMA_KALITE_TAKIP_FORMU_ID_GENERATOR", sequenceName = "KAPLAMA_KALITE_TAKIP_FORMU_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "KAPLAMA_KALITE_TAKIP_FORMU_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "pipe_id", nullable = false, insertable = false, updatable = false)
	private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "muayene_tarih", nullable = false)
	private Date muayeneTarih;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "sevk_tarih", nullable = false)
	private Date sevkTarih;

	@Column(name = "sevke_uygun", nullable = false)
	private Boolean sevkeUygun;

	@Column(name = "ic_boya_durum", nullable = false)
	private Integer icBoyaDurum;

	@Column(name = "ic_boya_rapor_no", nullable = false)
	private Integer icBoyaRaporNo;

	@Column(name = "ic_boya_aciklama", nullable = false)
	private String icBoyaAciklama;

	@Column(name = "dis_polietilen_durum", nullable = false)
	private Integer disBoyaDurum;

	@Column(name = "dis_polietilen_rapor_no", nullable = false)
	private Integer disBoyaRaporNo;

	@Column(name = "dis_polietilen_aciklama", nullable = false)
	private String disPolietilenAciklama;

	@Column(name = "ic_beton_durum", nullable = false)
	private Integer icBetonDurum;

	@Column(name = "ic_beton_rapor_no", nullable = false)
	private Integer icBetonRaporNo;

	@Column(name = "ic_beton_aciklama", nullable = false)
	private String icBetonAciklama;

	@Column(name = "dis_boya_durum", nullable = false)
	private Integer disPolietilenDurum;

	@Column(name = "dis_boya_rapor_no", nullable = false)
	private Integer disPolietilenRaporNo;

	@Column(name = "dis_boya_aciklama", nullable = false)
	private String disBoyaAciklama;

	public KaplamaKaliteTakipFormu() {

		icBoyaDurum = -1;
		disBoyaDurum = -1;
		icBetonDurum = -1;
		disPolietilenDurum = -1;

	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Boolean getSevkeUygun() {
		return sevkeUygun;
	}

	public void setSevkeUygun(Boolean sevkeUygun) {
		this.sevkeUygun = sevkeUygun;
	}

	public Integer getDisBoyaDurum() {
		return disBoyaDurum;
	}

	public void setDisBoyaDurum(Integer disBoyaDurum) {
		this.disBoyaDurum = disBoyaDurum;
	}

	public Integer getDisBoyaRaporNo() {
		return disBoyaRaporNo;
	}

	public void setDisBoyaRaporNo(Integer disBoyaRaporNo) {
		this.disBoyaRaporNo = disBoyaRaporNo;
	}

	public String getDisPolietilenAciklama() {
		return disPolietilenAciklama;
	}

	public void setDisPolietilenAciklama(String disPolietilenAciklama) {
		this.disPolietilenAciklama = disPolietilenAciklama;
	}

	public Integer getIcBetonDurum() {
		return icBetonDurum;
	}

	public void setIcBetonDurum(Integer icBetonDurum) {
		this.icBetonDurum = icBetonDurum;
	}

	public Integer getIcBetonRaporNo() {
		return icBetonRaporNo;
	}

	public void setIcBetonRaporNo(Integer icBetonRaporNo) {
		this.icBetonRaporNo = icBetonRaporNo;
	}

	public String getIcBetonAciklama() {
		return icBetonAciklama;
	}

	public void setIcBetonAciklama(String icBetonAciklama) {
		this.icBetonAciklama = icBetonAciklama;
	}

	public Integer getDisPolietilenDurum() {
		return disPolietilenDurum;
	}

	public void setDisPolietilenDurum(Integer disPolietilenDurum) {
		this.disPolietilenDurum = disPolietilenDurum;
	}

	public Integer getDisPolietilenRaporNo() {
		return disPolietilenRaporNo;
	}

	public void setDisPolietilenRaporNo(Integer disPolietilenRaporNo) {
		this.disPolietilenRaporNo = disPolietilenRaporNo;
	}

	public String getDisBoyaAciklama() {
		return disBoyaAciklama;
	}

	public void setDisBoyaAciklama(String disBoyaAciklama) {
		this.disBoyaAciklama = disBoyaAciklama;
	}

	public Date getMuayeneTarih() {
		return muayeneTarih;
	}

	public void setMuayeneTarih(Date muayeneTarih) {
		this.muayeneTarih = muayeneTarih;
	}

	public Date getSevkTarih() {
		return sevkTarih;
	}

	public void setSevkTarih(Date sevkTarih) {
		this.sevkTarih = sevkTarih;
	}

	/**
	 * @return the icBoyaDurum
	 */
	public Integer getIcBoyaDurum() {
		return icBoyaDurum;
	}

	/**
	 * @param icBoyaDurum
	 *            the icBoyaDurum to set
	 */
	public void setIcBoyaDurum(Integer icBoyaDurum) {
		this.icBoyaDurum = icBoyaDurum;
	}

	/**
	 * @return the icBoyaRaporNo
	 */
	public Integer getIcBoyaRaporNo() {
		return icBoyaRaporNo;
	}

	/**
	 * @param icBoyaRaporNo
	 *            the icBoyaRaporNo to set
	 */
	public void setIcBoyaRaporNo(Integer icBoyaRaporNo) {
		this.icBoyaRaporNo = icBoyaRaporNo;
	}

	/**
	 * @return the icBoyaAciklama
	 */
	public String getIcBoyaAciklama() {
		return icBoyaAciklama;
	}

	/**
	 * @param icBoyaAciklama
	 *            the icBoyaAciklama to set
	 */
	public void setIcBoyaAciklama(String icBoyaAciklama) {
		this.icBoyaAciklama = icBoyaAciklama;
	}
}