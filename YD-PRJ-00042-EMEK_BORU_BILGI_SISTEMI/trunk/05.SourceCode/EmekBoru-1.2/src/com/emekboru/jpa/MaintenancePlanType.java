package com.emekboru.jpa;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="maintenance_plan_type")
public class MaintenancePlanType implements Serializable{

	private static final long serialVersionUID = 2690128241890682371L;

	@Id
	@Column(name="maintenance_plan_type_id")
	@SequenceGenerator(name="MAINTENANCE_PLAN_TYPE_GENERATOR", sequenceName="MAINTENANCE_PLAN_TYPE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MAINTENANCE_PLAN_TYPE_GENERATOR")
	private Integer maintenancePlanTypeId;
	
	@Column(name="plan_name")
	private String planName;
	
	@Column(name="short_desc")
	private String shortDesc;
	
	@Column(name="frequency_in_weeks")
	private Integer frequencyInWeeks;
	
	@OneToMany(mappedBy = "maintenancePlanType", fetch=FetchType.LAZY)
	private List<MaintenancePlan> maintenancePlans;
	
	public MaintenancePlanType(){
		
	}

	/**
	 * GETTERS AND SETTERS
	 */
	public Integer getMaintenancePlanTypeId() {
		return maintenancePlanTypeId;
	}

	public void setMaintenancePlanTypeId(Integer maintenancePlanTypeId) {
		this.maintenancePlanTypeId = maintenancePlanTypeId;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public Integer getFrequencyInWeeks() {
		return frequencyInWeeks;
	}

	public void setFrequencyInWeeks(Integer frequencyInWeeks) {
		this.frequencyInWeeks = frequencyInWeeks;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<MaintenancePlan> getMaintenancePlans() {
		return maintenancePlans;
	}

	public void setMaintenancePlans(List<MaintenancePlan> maintenancePlans) {
		this.maintenancePlans = maintenancePlans;
	}
}
