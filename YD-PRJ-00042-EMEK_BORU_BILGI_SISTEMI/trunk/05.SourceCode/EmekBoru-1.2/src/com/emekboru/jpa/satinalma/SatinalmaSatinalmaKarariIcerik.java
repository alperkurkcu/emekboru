package com.emekboru.jpa.satinalma;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * The persistent class for the satinalma_satinalma_karari_icerik database
 * table.
 * 
 */
@Entity
@NamedQueries({
		@NamedQuery(name = "SatinalmaSatinalmaKarariIcerik.findKabulEdilenByKararId", query = "SELECT r FROM SatinalmaSatinalmaKarariIcerik r WHERE r.kabulEdilen=true and r.satinalmaSatinalmaKarari.id=:prmKararId"),
		@NamedQuery(name = "SatinalmaSatinalmaKarariIcerik.teklifIstemeByKararId", query = "SELECT DISTINCT t FROM SatinalmaSatinalmaKarariIcerik r, SatinalmaTeklifIsteme t WHERE r.kabulEdilen=true and r.satinalmaSatinalmaKarari.id=:prmKararId and r.satinalmaTeklifIsteme.id=t.id"),
		@NamedQuery(name = "SatinalmaSatinalmaKarariIcerik.kararIcerikByKararId", query = "SELECT r FROM SatinalmaSatinalmaKarariIcerik r, SatinalmaTeklifIsteme t WHERE r.kabulEdilen=true and r.satinalmaSatinalmaKarari.id=:prmKararId and r.satinalmaTeklifIsteme.id=t.id") })
@Table(name = "satinalma_satinalma_karari_icerik")
public class SatinalmaSatinalmaKarariIcerik implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SATINALMA_SATINALMA_KARARI_ICERIK_ID_GENERATOR", sequenceName = "SATINALMA_SATINALMA_KARARI_ICERIK_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SATINALMA_SATINALMA_KARARI_ICERIK_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	// @Column(name = "dosya_no", nullable = false)
	// private Integer dosyaNo;

	// @ManyToOne(fetch = FetchType.EAGER)
	// @JoinColumn(name = "dosya_no", referencedColumnName = "ID", insertable =
	// false, nullable = false)
	// private SatinalmaTakipDosyalar satinalmaTakipDosyalar;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "ilk_teklif_birim_fiyat")
	private double ilkTeklifBirimFiyat;

	// @Column(name = "karar_id", nullable = false)
	// private Integer kararId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "karar_id", referencedColumnName = "ID", insertable = false, nullable = false)
	private SatinalmaSatinalmaKarari satinalmaSatinalmaKarari;

	private float kur;

	@Column(name = "nihai_teklif_birim_fiyat")
	private double nihaiTeklifBirimFiyat;

	@Column(name = "odeme_aciklama", length = 2147483647)
	private String odemeAciklama;

	// @Column(name = "teklif_icerik_no", nullable = false)
	// private Integer teklifIcerikNo;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "teklif_icerik_no", referencedColumnName = "ID", insertable = false, nullable = false)
	private SatinalmaTeklifIstemeIcerik satinalmaTeklifIstemeIcerik;

	// @Column(name = "teklif_no", nullable = false)
	// private Integer teklifNo;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "teklif_no", referencedColumnName = "ID", insertable = false, nullable = false)
	private SatinalmaTeklifIsteme satinalmaTeklifIsteme;

	@Column(name = "teslim_suresi", length = 2147483647)
	private String teslimSuresi;

	@Column(name = "teslim_yeri_nakliye_aciklama", length = 2147483647)
	private String teslimYeriNakliyeAciklama;

	@Column(name = "kabul_edilen")
	private boolean kabulEdilen;

	@Transient
	private double quantityIlk;

	@Transient
	private double quantityNihai;

	public SatinalmaSatinalmaKarariIcerik() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	/**
	 * @return the ilkTeklifBirimFiyat
	 */
	public double getIlkTeklifBirimFiyat() {
		return ilkTeklifBirimFiyat;
	}

	/**
	 * @param ilkTeklifBirimFiyat
	 *            the ilkTeklifBirimFiyat to set
	 */
	public void setIlkTeklifBirimFiyat(double ilkTeklifBirimFiyat) {
		this.ilkTeklifBirimFiyat = ilkTeklifBirimFiyat;
	}

	public float getKur() {
		return this.kur;
	}

	public void setKur(float kur) {
		this.kur = kur;
	}

	/**
	 * @return the nihaiTeklifBirimFiyat
	 */
	public double getNihaiTeklifBirimFiyat() {
		return nihaiTeklifBirimFiyat;
	}

	/**
	 * @param nihaiTeklifBirimFiyat
	 *            the nihaiTeklifBirimFiyat to set
	 */
	public void setNihaiTeklifBirimFiyat(double nihaiTeklifBirimFiyat) {
		this.nihaiTeklifBirimFiyat = nihaiTeklifBirimFiyat;
	}

	public String getOdemeAciklama() {
		return this.odemeAciklama;
	}

	public void setOdemeAciklama(String odemeAciklama) {
		this.odemeAciklama = odemeAciklama;
	}

	public String getTeslimSuresi() {
		return this.teslimSuresi;
	}

	public void setTeslimSuresi(String teslimSuresi) {
		this.teslimSuresi = teslimSuresi;
	}

	public String getTeslimYeriNakliyeAciklama() {
		return this.teslimYeriNakliyeAciklama;
	}

	public void setTeslimYeriNakliyeAciklama(String teslimYeriNakliyeAciklama) {
		this.teslimYeriNakliyeAciklama = teslimYeriNakliyeAciklama;
	}

	/**
	 * @return the satinalmaTeklifIsteme
	 */
	public SatinalmaTeklifIsteme getSatinalmaTeklifIsteme() {
		return satinalmaTeklifIsteme;
	}

	/**
	 * @param satinalmaTeklifIsteme
	 *            the satinalmaTeklifIsteme to set
	 */
	public void setSatinalmaTeklifIsteme(
			SatinalmaTeklifIsteme satinalmaTeklifIsteme) {
		this.satinalmaTeklifIsteme = satinalmaTeklifIsteme;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the satinalmaSatinalmaKarari
	 */
	public SatinalmaSatinalmaKarari getSatinalmaSatinalmaKarari() {
		return satinalmaSatinalmaKarari;
	}

	/**
	 * @param satinalmaSatinalmaKarari
	 *            the satinalmaSatinalmaKarari to set
	 */
	public void setSatinalmaSatinalmaKarari(
			SatinalmaSatinalmaKarari satinalmaSatinalmaKarari) {
		this.satinalmaSatinalmaKarari = satinalmaSatinalmaKarari;
	}

	/**
	 * @return the satinalmaTeklifIstemeIcerik
	 */
	public SatinalmaTeklifIstemeIcerik getSatinalmaTeklifIstemeIcerik() {
		return satinalmaTeklifIstemeIcerik;
	}

	/**
	 * @param satinalmaTeklifIstemeIcerik
	 *            the satinalmaTeklifIstemeIcerik to set
	 */
	public void setSatinalmaTeklifIstemeIcerik(
			SatinalmaTeklifIstemeIcerik satinalmaTeklifIstemeIcerik) {
		this.satinalmaTeklifIstemeIcerik = satinalmaTeklifIstemeIcerik;
	}

	/**
	 * @return the kabulEdilen
	 */
	public boolean isKabulEdilen() {
		return kabulEdilen;
	}

	/**
	 * @param kabulEdilen
	 *            the kabulEdilen to set
	 */
	public void setKabulEdilen(boolean kabulEdilen) {
		this.kabulEdilen = kabulEdilen;
	}

	/**
	 * @return the quantityIlk
	 */
	public double getQuantityIlk() {
		return quantityIlk;
	}

	/**
	 * @param quantityIlk
	 *            the quantityIlk to set
	 */
	public void setQuantityIlk(double quantityIlk) {
		this.quantityIlk = quantityIlk;
	}

	/**
	 * @return the quantityNihai
	 */
	public double getQuantityNihai() {
		return quantityNihai;
	}

	/**
	 * @param quantityNihai
	 *            the quantityNihai to set
	 */
	public void setQuantityNihai(double quantityNihai) {
		this.quantityNihai = quantityNihai;
	}

}