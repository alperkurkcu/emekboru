package com.emekboru.jpa.listeners;

import java.text.DecimalFormat;

import javax.ejb.EJB;
import javax.persistence.PostLoad;

import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.EventCostBreakdown;
import com.emekboru.jpa.EventEb;


public class EventEbListener {
	
	@EJB
	private ConfigBean config;
	
	@PostLoad
	public void onPersist(EventEb event){
		
		System.out.println("EventEbListener.onPersist()");
		double totalCost 	= 0;
		double plannedCost 	= 0;
		for (EventCostBreakdown cost : event.getEventCostBreakdowns()) {
			totalCost 	+= (cost.getAmountPaid()*cost.getTlExchangeRate());
			plannedCost += (cost.getAmountPlanned()*cost.getTlExchangeRate());
		}
		DecimalFormat df = new DecimalFormat("#,##");
		event.setEstimatedCost(Double.parseDouble(df.format(plannedCost)));
		event.setTotalCost(Double.parseDouble(df.format(totalCost)));
	}

	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

}
