package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the operation_privileges database table.
 * 
 */
@Entity
@Table(name="operation_privileges")
public class OperationPrivilege implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="privilege_id")
	@SequenceGenerator(name="OP_PRIVILEGE_GENERATOR", sequenceName="OP_PRIVILEGE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="OP_PRIVILEGE_GENERATOR")	
	private Integer privilegeId;

	@Column(name="ambient_condition_test_p")
	private Boolean ambientConditionTestP;

	@Column(name="cathodic_test_p")
	private Boolean cathodicTestP;

	@Column(name="cement_mortart_test_p")
	private Boolean cementMortartTestP;

	@Column(name="charpy_impact_test_p")
	private Boolean charpyImpactTestP;

	@Column(name="cut_test_p")
	private Boolean cutTestP;

	@Column(name="entrance_test_p")
	private Boolean entranceTestP;

	@Column(name="epoxy_dsc_test_p")
	private Boolean epoxyDscTestP;

	@Column(name="epoxy_lining_test_p")
	private Boolean epoxyLiningTestP;

	@Column(name="etilen_propilen_coat_test_p")
	private Boolean etilenPropilenCoatTestP;

	@Column(name="etilen_propilen_hardness_test_p")
	private Boolean etilenPropilenHardnessTestP;

	@Column(name="etilen_propilen_resistance_test_p")
	private Boolean etilenPropilenResistanceTestP;

	@Column(name="etilen_propilen_test_p")
	private Boolean etilenPropilenTestP;

	@Column(name="fluoroscopic_test_p")
	private Boolean fluoroscopicTestP;

	@Column(name="fusion_bond_epoxy_p")
	private Boolean fusionBondEpoxyP;

	@Column(name="guided_bend_test_p")
	private Boolean guidedBendTestP;

	@Column(name="hot_water_test_p")
	private Boolean hotWaterTestP;

	@Column(name="hydrostatic_test_p")
	private Boolean hydrostaticTestP;

	@Column(name="liquid_epoxy_test_p")
	private Boolean liquidEpoxyTestP;

	@Column(name="magentic_particle_test_p")
	private Boolean magenticParticleTestP;

	@Column(name="magnetic_penetrant_test_p")
	private Boolean magneticPenetrantTestP;

	@Column(name="manual_ultrasonic_test_p")
	private Boolean manualUltrasonicTestP;

	@Column(name="radiographic_test_p")
	private Boolean radiographicTestP;

	@Column(name="salt_content_test_p")
	private Boolean saltContentTestP;

	@Column(name="sandblasting_p")
	private Boolean sandblastingP;

	@Column(name="surface_blast_cleaning_test_p")
	private Boolean surfaceBlastCleaningTestP;

	@Column(name="surface_profile_test_p")
	private Boolean surfaceProfileTestP;

	@Column(name="tensile_test_p")
	private Boolean tensileTestP;

	@Column(name="toughness_test_p")
	private Boolean toughnessTestP;

	@Column(name="weld_macrographic_test_p")
	private Boolean weldMacrographicTestP;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="user_type_id", referencedColumnName="user_type_id", updatable= false)
	private UserType userType;
	
    public OperationPrivilege() {
    	
    }

	public Boolean getAmbientConditionTestP() {
		return this.ambientConditionTestP;
	}

	public void setAmbientConditionTestP(Boolean ambientConditionTestP) {
		this.ambientConditionTestP = ambientConditionTestP;
	}

	public Boolean getCathodicTestP() {
		return this.cathodicTestP;
	}

	public void setCathodicTestP(Boolean cathodicTestP) {
		this.cathodicTestP = cathodicTestP;
	}

	public Boolean getCementMortartTestP() {
		return this.cementMortartTestP;
	}

	public void setCementMortartTestP(Boolean cementMortartTestP) {
		this.cementMortartTestP = cementMortartTestP;
	}

	public Boolean getCharpyImpactTestP() {
		return this.charpyImpactTestP;
	}

	public void setCharpyImpactTestP(Boolean charpyImpactTestP) {
		this.charpyImpactTestP = charpyImpactTestP;
	}

	public Boolean getCutTestP() {
		return this.cutTestP;
	}

	public void setCutTestP(Boolean cutTestP) {
		this.cutTestP = cutTestP;
	}

	public Boolean getEntranceTestP() {
		return this.entranceTestP;
	}

	public void setEntranceTestP(Boolean entranceTestP) {
		this.entranceTestP = entranceTestP;
	}

	public Boolean getEpoxyDscTestP() {
		return this.epoxyDscTestP;
	}

	public void setEpoxyDscTestP(Boolean epoxyDscTestP) {
		this.epoxyDscTestP = epoxyDscTestP;
	}

	public Boolean getEpoxyLiningTestP() {
		return this.epoxyLiningTestP;
	}

	public void setEpoxyLiningTestP(Boolean epoxyLiningTestP) {
		this.epoxyLiningTestP = epoxyLiningTestP;
	}

	public Boolean getEtilenPropilenCoatTestP() {
		return this.etilenPropilenCoatTestP;
	}

	public void setEtilenPropilenCoatTestP(Boolean etilenPropilenCoatTestP) {
		this.etilenPropilenCoatTestP = etilenPropilenCoatTestP;
	}

	public Boolean getEtilenPropilenHardnessTestP() {
		return this.etilenPropilenHardnessTestP;
	}

	public void setEtilenPropilenHardnessTestP(Boolean etilenPropilenHardnessTestP) {
		this.etilenPropilenHardnessTestP = etilenPropilenHardnessTestP;
	}

	public Boolean getEtilenPropilenResistanceTestP() {
		return this.etilenPropilenResistanceTestP;
	}

	public void setEtilenPropilenResistanceTestP(Boolean etilenPropilenResistanceTestP) {
		this.etilenPropilenResistanceTestP = etilenPropilenResistanceTestP;
	}

	public Boolean getEtilenPropilenTestP() {
		return this.etilenPropilenTestP;
	}

	public void setEtilenPropilenTestP(Boolean etilenPropilenTestP) {
		this.etilenPropilenTestP = etilenPropilenTestP;
	}

	public Boolean getFluoroscopicTestP() {
		return this.fluoroscopicTestP;
	}

	public void setFluoroscopicTestP(Boolean fluoroscopicTestP) {
		this.fluoroscopicTestP = fluoroscopicTestP;
	}

	public Boolean getFusionBondEpoxyP() {
		return this.fusionBondEpoxyP;
	}

	public void setFusionBondEpoxyP(Boolean fusionBondEpoxyP) {
		this.fusionBondEpoxyP = fusionBondEpoxyP;
	}

	public Boolean getGuidedBendTestP() {
		return this.guidedBendTestP;
	}

	public void setGuidedBendTestP(Boolean guidedBendTestP) {
		this.guidedBendTestP = guidedBendTestP;
	}

	public Boolean getHotWaterTestP() {
		return this.hotWaterTestP;
	}

	public void setHotWaterTestP(Boolean hotWaterTestP) {
		this.hotWaterTestP = hotWaterTestP;
	}

	public Boolean getHydrostaticTestP() {
		return this.hydrostaticTestP;
	}

	public void setHydrostaticTestP(Boolean hydrostaticTestP) {
		this.hydrostaticTestP = hydrostaticTestP;
	}

	public Boolean getLiquidEpoxyTestP() {
		return this.liquidEpoxyTestP;
	}

	public void setLiquidEpoxyTestP(Boolean liquidEpoxyTestP) {
		this.liquidEpoxyTestP = liquidEpoxyTestP;
	}

	public Boolean getMagenticParticleTestP() {
		return this.magenticParticleTestP;
	}

	public void setMagenticParticleTestP(Boolean magenticParticleTestP) {
		this.magenticParticleTestP = magenticParticleTestP;
	}

	public Boolean getMagneticPenetrantTestP() {
		return this.magneticPenetrantTestP;
	}

	public void setMagneticPenetrantTestP(Boolean magneticPenetrantTestP) {
		this.magneticPenetrantTestP = magneticPenetrantTestP;
	}

	public Boolean getManualUltrasonicTestP() {
		return this.manualUltrasonicTestP;
	}

	public void setManualUltrasonicTestP(Boolean manualUltrasonicTestP) {
		this.manualUltrasonicTestP = manualUltrasonicTestP;
	}

	public Boolean getRadiographicTestP() {
		return this.radiographicTestP;
	}

	public void setRadiographicTestP(Boolean radiographicTestP) {
		this.radiographicTestP = radiographicTestP;
	}

	public Boolean getSaltContentTestP() {
		return this.saltContentTestP;
	}

	public void setSaltContentTestP(Boolean saltContentTestP) {
		this.saltContentTestP = saltContentTestP;
	}

	public Boolean getSandblastingP() {
		return this.sandblastingP;
	}

	public void setSandblastingP(Boolean sandblastingP) {
		this.sandblastingP = sandblastingP;
	}

	public Boolean getSurfaceBlastCleaningTestP() {
		return this.surfaceBlastCleaningTestP;
	}

	public void setSurfaceBlastCleaningTestP(Boolean surfaceBlastCleaningTestP) {
		this.surfaceBlastCleaningTestP = surfaceBlastCleaningTestP;
	}

	public Boolean getSurfaceProfileTestP() {
		return this.surfaceProfileTestP;
	}

	public void setSurfaceProfileTestP(Boolean surfaceProfileTestP) {
		this.surfaceProfileTestP = surfaceProfileTestP;
	}

	public Boolean getTensileTestP() {
		return this.tensileTestP;
	}

	public void setTensileTestP(Boolean tensileTestP) {
		this.tensileTestP = tensileTestP;
	}

	public Boolean getToughnessTestP() {
		return this.toughnessTestP;
	}

	public void setToughnessTestP(Boolean toughnessTestP) {
		this.toughnessTestP = toughnessTestP;
	}

	public Boolean getWeldMacrographicTestP() {
		return this.weldMacrographicTestP;
	}

	public void setWeldMacrographicTestP(Boolean weldMacrographicTestP) {
		this.weldMacrographicTestP = weldMacrographicTestP;
	}

	public Integer getPrivilegeId() {
		return privilegeId;
	}

	public void setPrivilegeId(Integer privilegeId) {
		this.privilegeId = privilegeId;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}