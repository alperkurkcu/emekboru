package com.emekboru.jpa.sales.order;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the sales_kamyon_tir_yukleme_kapasitesi database
 * table.
 * 
 */
@Entity
@Table(name = "sales_kamyon_tir_yukleme_kapasitesi")
public class SalesKamyonTirYuklemeKapasitesi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SALES_KAMYON_TIR_YUKLEME_KAPASITESI_ID_GENERATOR", sequenceName = "SALES_KAMYON_TIR_YUKLEME_KAPASITESI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_KAMYON_TIR_YUKLEME_KAPASITESI_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "boru_capi")
	private float boruCapi;

	@Column(name = "kamyon_kapasite")
	private Integer kamyonKapasite;

	@Column(name = "tir_kapasite")
	private Integer tirKapasite;

	public boolean equals(Object object) {
		// Basic checks.
		if (object == this)
			return true;
		if (object == null || getClass() != object.getClass())
			return false;

		// All passed.
		return true;
	}

	public SalesKamyonTirYuklemeKapasitesi() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public float getBoruCapi() {
		return this.boruCapi;
	}

	public void setBoruCapi(float boruCapi) {
		this.boruCapi = boruCapi;
	}

	public Integer getKamyonKapasite() {
		return this.kamyonKapasite;
	}

	public void setKamyonKapasite(Integer kamyonKapasite) {
		this.kamyonKapasite = kamyonKapasite;
	}

	public Integer getTirKapasite() {
		return this.tirKapasite;
	}

	public void setTirKapasite(Integer tirKapasite) {
		this.tirKapasite = tirKapasite;
	}

}