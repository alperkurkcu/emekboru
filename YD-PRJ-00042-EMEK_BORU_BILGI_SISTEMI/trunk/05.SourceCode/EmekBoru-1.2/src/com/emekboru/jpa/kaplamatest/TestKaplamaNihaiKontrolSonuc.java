package com.emekboru.jpa.kaplamatest;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.isolation.IsolationTestDefinition;

/**
 * The persistent class for the test_kaplama_nihai_kontrol_sonuc database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "TestKaplamaNihaiKontrolSonuc.findAll", query = "SELECT r FROM TestKaplamaNihaiKontrolSonuc r WHERE r.bagliGlobalId.globalId =:prmGlobalId and r.pipe.pipeId=:prmPipeId") })
@Table(name = "test_kaplama_nihai_kontrol_sonuc")
public class TestKaplamaNihaiKontrolSonuc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_KAPLAMA_NIHAI_KONTROL_SONUC_ID_GENERATOR", sequenceName = "TEST_KAPLAMA_NIHAI_KONTROL_SONUC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_KAPLAMA_NIHAI_KONTROL_SONUC_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Column(name = "boya_dis")
	private Boolean boyaDis;

	@Column(name = "boya_ic")
	private Boolean boyaIc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ekleme_zamani")
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	// @Column(name = "global_id")
	// private Integer globalId;

	@Column(name = "gorsel_kontrol")
	private Integer gorselKontrol;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "kaynak_agzi")
	private Boolean kaynakAgzi;

	@Column(name = "kaynak_agzi_koruma")
	private Boolean kaynakAgziKoruma;

	private Boolean markalama;

	// @Column(name = "pipe_id")
	// private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false)
	private IsolationTestDefinition bagliGlobalId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "isolation_test_id", referencedColumnName = "ID", insertable = false)
	private IsolationTestDefinition bagliTestId;

	@Column(name = "red_gorsel_kontrol")
	private Boolean redGorselKontrol;

	@Column(name = "red_kaynak_agiz")
	private Boolean redKaynakAgiz;

	@Column(name = "red_kaynak_agzi_koruma")
	private Boolean redKaynakAgziKoruma;

	@Column(name = "red_markalama")
	private Boolean redMarkalama;

	@Column(name = "red_vernik")
	private Boolean redVernik;

	private Boolean sonuc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi")
	private Date testTarihi;

	@Column(name = "vardiya")
	private Integer vardiya;

	@Column(name = "vernik")
	private Boolean vernik;

	@Column(name = "dis_kaplama")
	private Integer disKaplama;

	public TestKaplamaNihaiKontrolSonuc() {
	}

	/**
	 * @return the bagliGlobalId
	 */
	public IsolationTestDefinition getBagliGlobalId() {
		return bagliGlobalId;
	}

	/**
	 * @param bagliGlobalId
	 *            the bagliGlobalId to set
	 */
	public void setBagliGlobalId(IsolationTestDefinition bagliGlobalId) {
		this.bagliGlobalId = bagliGlobalId;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getBoyaDis() {
		return this.boyaDis;
	}

	public void setBoyaDis(Boolean boyaDis) {
		this.boyaDis = boyaDis;
	}

	public Boolean getBoyaIc() {
		return this.boyaIc;
	}

	public void setBoyaIc(Boolean boyaIc) {
		this.boyaIc = boyaIc;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Integer getGorselKontrol() {
		return this.gorselKontrol;
	}

	public void setGorselKontrol(Integer gorselKontrol) {
		this.gorselKontrol = gorselKontrol;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public Boolean getKaynakAgzi() {
		return this.kaynakAgzi;
	}

	public void setKaynakAgzi(Boolean kaynakAgzi) {
		this.kaynakAgzi = kaynakAgzi;
	}

	public Boolean getKaynakAgziKoruma() {
		return this.kaynakAgziKoruma;
	}

	public void setKaynakAgziKoruma(Boolean kaynakAgziKoruma) {
		this.kaynakAgziKoruma = kaynakAgziKoruma;
	}

	public Boolean getMarkalama() {
		return this.markalama;
	}

	public void setMarkalama(Boolean markalama) {
		this.markalama = markalama;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Boolean getRedGorselKontrol() {
		return this.redGorselKontrol;
	}

	public void setRedGorselKontrol(Boolean redGorselKontrol) {
		this.redGorselKontrol = redGorselKontrol;
	}

	public Boolean getRedKaynakAgiz() {
		return this.redKaynakAgiz;
	}

	public void setRedKaynakAgiz(Boolean redKaynakAgiz) {
		this.redKaynakAgiz = redKaynakAgiz;
	}

	public Boolean getRedKaynakAgziKoruma() {
		return this.redKaynakAgziKoruma;
	}

	public void setRedKaynakAgziKoruma(Boolean redKaynakAgziKoruma) {
		this.redKaynakAgziKoruma = redKaynakAgziKoruma;
	}

	public Boolean getRedMarkalama() {
		return this.redMarkalama;
	}

	public void setRedMarkalama(Boolean redMarkalama) {
		this.redMarkalama = redMarkalama;
	}

	public Boolean getRedVernik() {
		return this.redVernik;
	}

	public void setRedVernik(Boolean redVernik) {
		this.redVernik = redVernik;
	}

	public Boolean getSonuc() {
		return this.sonuc;
	}

	public void setSonuc(Boolean sonuc) {
		this.sonuc = sonuc;
	}

	public Date getTestTarihi() {
		return this.testTarihi;
	}

	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	public Integer getVardiya() {
		return this.vardiya;
	}

	public void setVardiya(Integer vardiya) {
		this.vardiya = vardiya;
	}

	public Boolean getVernik() {
		return this.vernik;
	}

	public void setVernik(Boolean vernik) {
		this.vernik = vernik;
	}

	/**
	 * @return the bagliTestId
	 */
	public IsolationTestDefinition getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(IsolationTestDefinition bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the disKaplama
	 */
	public Integer getDisKaplama() {
		return disKaplama;
	}

	/**
	 * @param disKaplama
	 *            the disKaplama to set
	 */
	public void setDisKaplama(Integer disKaplama) {
		this.disKaplama = disKaplama;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

}