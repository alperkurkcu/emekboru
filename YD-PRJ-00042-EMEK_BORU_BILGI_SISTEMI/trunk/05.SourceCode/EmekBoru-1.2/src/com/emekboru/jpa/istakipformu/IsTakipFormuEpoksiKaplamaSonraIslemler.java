package com.emekboru.jpa.istakipformu;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the is_takip_formu_epoksi_kaplama_sonra_islemler
 * database table.
 * 
 */
@Entity
@Table(name = "is_takip_formu_epoksi_kaplama_sonra_islemler")
public class IsTakipFormuEpoksiKaplamaSonraIslemler implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IS_TAKIP_FORMU_EPOKSI_KAPLAMA_SONRA_ISLEMLER_ID_GENERATOR", sequenceName = "IS_TAKIP_FORMU_EPOKSI_KAPLAMA_SONRA_ISLEMLER_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IS_TAKIP_FORMU_EPOKSI_KAPLAMA_SONRA_ISLEMLER_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "ayar_sonrasi_ilk_boru_epoksi_kalinlik")
	private Integer ayarSonrasiIlkBoruEpoksiKalinlik;

	@Column(name = "ayar_sonrasi_ilk_boru_epoksi_kalinlik_boru_id")
	private Integer ayarSonrasiIlkBoruEpoksiKalinlikBoruId;

	@Temporal(TemporalType.DATE)
	@Column(name = "ayar_sonrasi_ilk_boru_epoksi_kalinlik_tarihi")
	private Date ayarSonrasiIlkBoruEpoksiKalinlikTarihi;

	@Column(name = "ayar_sonrasi_ilk_boru_id")
	private Integer ayarSonrasiIlkBoruId;

	@Column(name = "ayar_sonrasi_ilk_boru_yas_kalinlik_check")
	private Boolean ayarSonrasiIlkBoruYasKalinlikCheck;

	@Temporal(TemporalType.DATE)
	@Column(name = "ayar_sonrasi_ilk_boru_yas_kalinlik_tarihi")
	private Date ayarSonrasiIlkBoruYasKalinlikTarihi;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	// @Column(name = "ekleyen_kullanici", nullable = false)
	// private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	// @Column(name = "guncelleyen_kullanici")
	// private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name = "is_takip_form_id")
	// private Integer isTakipFormId;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "is_takip_form_id", referencedColumnName = "ID", insertable = false, updatable = false)
	private IsTakipFormuEpoksiKaplama isTakipFormuEpoksiKaplama;

	@Column(name = "epoksi_hizi")
	private Integer epoksiHizi;

	@Column(name = "epoksi_hizi_check")
	private Boolean epoksiHiziCheck;

	@Temporal(TemporalType.DATE)
	@Column(name = "epoksi_hizi_tarihi")
	private Date epoksiHiziTarihi;

	@Column(name = "ayar_sonrasi_ilk_boru_yas_kalinlik_aciklama", length = 255)
	private String ayarSonrasiIlkBoruYasKalinlikAciklama;

	@Column(name = "ayar_sonrasi_ilk_boru_epoksi_kalinlik_aciklama", length = 255)
	private String ayarSonrasiIlkBoruEpoksiKalinlikAciklama;

	@Column(name = "epoksi_hizi_aciklama", length = 255)
	private String epoksiHiziAciklama;

	@Column(name = "ayar_sonrasi_ilk_boru_epoksi_kalinlik_check")
	private Boolean ayarSonrasiIlkBoruEpoksiKalinlikCheck;

	public IsTakipFormuEpoksiKaplamaSonraIslemler() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAyarSonrasiIlkBoruEpoksiKalinlik() {
		return this.ayarSonrasiIlkBoruEpoksiKalinlik;
	}

	public void setAyarSonrasiIlkBoruEpoksiKalinlik(
			Integer ayarSonrasiIlkBoruEpoksiKalinlik) {
		this.ayarSonrasiIlkBoruEpoksiKalinlik = ayarSonrasiIlkBoruEpoksiKalinlik;
	}

	public Integer getAyarSonrasiIlkBoruEpoksiKalinlikBoruId() {
		return this.ayarSonrasiIlkBoruEpoksiKalinlikBoruId;
	}

	public void setAyarSonrasiIlkBoruEpoksiKalinlikBoruId(
			Integer ayarSonrasiIlkBoruEpoksiKalinlikBoruId) {
		this.ayarSonrasiIlkBoruEpoksiKalinlikBoruId = ayarSonrasiIlkBoruEpoksiKalinlikBoruId;
	}

	public Date getAyarSonrasiIlkBoruEpoksiKalinlikTarihi() {
		return this.ayarSonrasiIlkBoruEpoksiKalinlikTarihi;
	}

	public void setAyarSonrasiIlkBoruEpoksiKalinlikTarihi(
			Date ayarSonrasiIlkBoruEpoksiKalinlikTarihi) {
		this.ayarSonrasiIlkBoruEpoksiKalinlikTarihi = ayarSonrasiIlkBoruEpoksiKalinlikTarihi;
	}

	public Integer getAyarSonrasiIlkBoruId() {
		return this.ayarSonrasiIlkBoruId;
	}

	public void setAyarSonrasiIlkBoruId(Integer ayarSonrasiIlkBoruId) {
		this.ayarSonrasiIlkBoruId = ayarSonrasiIlkBoruId;
	}

	public Boolean getAyarSonrasiIlkBoruYasKalinlikCheck() {
		return this.ayarSonrasiIlkBoruYasKalinlikCheck;
	}

	public void setAyarSonrasiIlkBoruYasKalinlikCheck(
			Boolean ayarSonrasiIlkBoruYasKalinlikCheck) {
		this.ayarSonrasiIlkBoruYasKalinlikCheck = ayarSonrasiIlkBoruYasKalinlikCheck;
	}

	public Date getAyarSonrasiIlkBoruYasKalinlikTarihi() {
		return this.ayarSonrasiIlkBoruYasKalinlikTarihi;
	}

	public void setAyarSonrasiIlkBoruYasKalinlikTarihi(
			Date ayarSonrasiIlkBoruYasKalinlikTarihi) {
		this.ayarSonrasiIlkBoruYasKalinlikTarihi = ayarSonrasiIlkBoruYasKalinlikTarihi;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEpoksiHizi() {
		return this.epoksiHizi;
	}

	public void setEpoksiHizi(Integer epoksiHizi) {
		this.epoksiHizi = epoksiHizi;
	}

	public Boolean getEpoksiHiziCheck() {
		return this.epoksiHiziCheck;
	}

	public void setEpoksiHiziCheck(Boolean epoksiHiziCheck) {
		this.epoksiHiziCheck = epoksiHiziCheck;
	}

	public Date getEpoksiHiziTarihi() {
		return this.epoksiHiziTarihi;
	}

	public void setEpoksiHiziTarihi(Date epoksiHiziTarihi) {
		this.epoksiHiziTarihi = epoksiHiziTarihi;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}
	
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	public IsTakipFormuEpoksiKaplama getIsTakipFormuEpoksiKaplama() {
		return isTakipFormuEpoksiKaplama;
	}

	public void setIsTakipFormuEpoksiKaplama(
			IsTakipFormuEpoksiKaplama isTakipFormuEpoksiKaplama) {
		this.isTakipFormuEpoksiKaplama = isTakipFormuEpoksiKaplama;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getAyarSonrasiIlkBoruYasKalinlikAciklama() {
		return ayarSonrasiIlkBoruYasKalinlikAciklama;
	}

	public void setAyarSonrasiIlkBoruYasKalinlikAciklama(
			String ayarSonrasiIlkBoruYasKalinlikAciklama) {
		this.ayarSonrasiIlkBoruYasKalinlikAciklama = ayarSonrasiIlkBoruYasKalinlikAciklama;
	}

	public String getAyarSonrasiIlkBoruEpoksiKalinlikAciklama() {
		return ayarSonrasiIlkBoruEpoksiKalinlikAciklama;
	}

	public void setAyarSonrasiIlkBoruEpoksiKalinlikAciklama(
			String ayarSonrasiIlkBoruEpoksiKalinlikAciklama) {
		this.ayarSonrasiIlkBoruEpoksiKalinlikAciklama = ayarSonrasiIlkBoruEpoksiKalinlikAciklama;
	}

	public String getEpoksiHiziAciklama() {
		return epoksiHiziAciklama;
	}

	public void setEpoksiHiziAciklama(String epoksiHiziAciklama) {
		this.epoksiHiziAciklama = epoksiHiziAciklama;
	}

	public Boolean getAyarSonrasiIlkBoruEpoksiKalinlikCheck() {
		return ayarSonrasiIlkBoruEpoksiKalinlikCheck;
	}

	public void setAyarSonrasiIlkBoruEpoksiKalinlikCheck(
			Boolean ayarSonrasiIlkBoruEpoksiKalinlikCheck) {
		this.ayarSonrasiIlkBoruEpoksiKalinlikCheck = ayarSonrasiIlkBoruEpoksiKalinlikCheck;
	}

}