package com.emekboru.jpa.kalibrasyon;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the kalibrasyon_offline_boru_ucu database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "KalibrasyonOfflineBoruUcu.findAll", query = "SELECT r FROM KalibrasyonOfflineBoruUcu r order by r.kalibrasyonZamani DESC") })
@Table(name = "kalibrasyon_offline_boru_ucu")
public class KalibrasyonOfflineBoruUcu implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "KALIBRASYON_OFFLINE_BORU_UCU_ID_GENERATOR", sequenceName = "KALIBRASYON_OFFLINE_BORU_UCU_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "KALIBRASYON_OFFLINE_BORU_UCU_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	@Column(name = "aciklama")
	private String aciklama;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "ilave_kazanc_degeri_bir")
	private String ilaveKazancDegeriBir;

	@Column(name = "ilave_kazanc_degeri_iki")
	private String ilaveKazancDegeriIki;

	@Column(name = "ilave_kazanc_degeri_uc")
	private String ilaveKazancDegeriUc;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "kalibrasyon_zamani")
	private Date kalibrasyonZamani;

	@Column(name = "kalibrasyonu_yapan")
	private String kalibrasyonuYapan;

	@Column(name = "kayit_kazanc_degeri_bir")
	private String kayitKazancDegeriBir;

	@Column(name = "kayit_kazanc_degeri_iki")
	private String kayitKazancDegeriIki;

	@Column(name = "kayit_kazanc_degeri_uc")
	private String kayitKazancDegeriUc;

	@Column(name = "kayit_seviyesi_bir")
	private String kayitSeviyesiBir;

	@Column(name = "kayit_seviyesi_iki")
	private String kayitSeviyesiIki;

	@Column(name = "kayit_seviyesi_uc")
	private String kayitSeviyesiUc;

	@Column(name = "prob_capi_frekansi_bir")
	private String probCapiFrekansiBir;

	@Column(name = "prob_capi_frekansi_iki")
	private String probCapiFrekansiIki;

	@Column(name = "prob_capi_frekansi_uc")
	private String probCapiFrekansiUc;

	@Column(name = "referans_centik_delik_bir")
	private String referansCentikDelikBir;

	@Column(name = "referans_centik_delik_iki")
	private String referansCentikDelikIki;

	@Column(name = "referans_centik_delik_uc")
	private String referansCentikDelikUc;

	@Column(name = "referans_seviye_bir")
	private String referansSeviyeBir;

	@Column(name = "referans_seviye_iki")
	private String referansSeviyeIki;

	@Column(name = "referans_seviye_uc")
	private String referansSeviyeUc;

	@Column(name = "tarama_hizi_bir")
	private String taramaHiziBir;

	@Column(name = "tarama_hizi_iki")
	private String taramaHiziIki;

	@Column(name = "tarama_hizi_uc")
	private String taramaHiziUc;

	@Column(name = "tarama_alani_bir")
	private String taramaAlaniBir;

	@Column(name = "tarama_alani_iki")
	private String taramaAlaniIki;

	@Column(name = "tarama_alani_uc")
	private String taramaAlaniUc;

	@Column(name = "kalibrasyon_mesafesi_bir")
	private String kalibrasyonMesafesiBir;

	@Column(name = "kalibrasyon_mesafesi_iki")
	private String kalibrasyonMesafesiIki;

	@Column(name = "kalibrasyon_mesafesi_uc")
	private String kalibrasyonMesafesiUc;

	@Column(name = "cihaz")
	private String cihaz;

	@Column(name = "kalibrasyon_blogu_bir")
	private String kalibrasyonBloguBir;

	@Column(name = "kalibrasyon_blogu_iki")
	private String kalibrasyonBloguIki;

	@Column(name = "kalibrasyon_blogu_uc")
	private String kalibrasyonBloguUc;

	public KalibrasyonOfflineBoruUcu() {
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getIlaveKazancDegeriBir() {
		return this.ilaveKazancDegeriBir;
	}

	public void setIlaveKazancDegeriBir(String ilaveKazancDegeriBir) {
		this.ilaveKazancDegeriBir = ilaveKazancDegeriBir;
	}

	public String getIlaveKazancDegeriIki() {
		return this.ilaveKazancDegeriIki;
	}

	public void setIlaveKazancDegeriIki(String ilaveKazancDegeriIki) {
		this.ilaveKazancDegeriIki = ilaveKazancDegeriIki;
	}

	public String getIlaveKazancDegeriUc() {
		return this.ilaveKazancDegeriUc;
	}

	public void setIlaveKazancDegeriUc(String ilaveKazancDegeriUc) {
		this.ilaveKazancDegeriUc = ilaveKazancDegeriUc;
	}

	public String getKalibrasyonuYapan() {
		return this.kalibrasyonuYapan;
	}

	public void setKalibrasyonuYapan(String kalibrasyonuYapan) {
		this.kalibrasyonuYapan = kalibrasyonuYapan;
	}

	public String getKayitKazancDegeriBir() {
		return this.kayitKazancDegeriBir;
	}

	public void setKayitKazancDegeriBir(String kayitKazancDegeriBir) {
		this.kayitKazancDegeriBir = kayitKazancDegeriBir;
	}

	public String getKayitKazancDegeriIki() {
		return this.kayitKazancDegeriIki;
	}

	public void setKayitKazancDegeriIki(String kayitKazancDegeriIki) {
		this.kayitKazancDegeriIki = kayitKazancDegeriIki;
	}

	public String getKayitKazancDegeriUc() {
		return this.kayitKazancDegeriUc;
	}

	public void setKayitKazancDegeriUc(String kayitKazancDegeriUc) {
		this.kayitKazancDegeriUc = kayitKazancDegeriUc;
	}

	public String getKayitSeviyesiBir() {
		return this.kayitSeviyesiBir;
	}

	public void setKayitSeviyesiBir(String kayitSeviyesiBir) {
		this.kayitSeviyesiBir = kayitSeviyesiBir;
	}

	public String getKayitSeviyesiIki() {
		return this.kayitSeviyesiIki;
	}

	public void setKayitSeviyesiIki(String kayitSeviyesiIki) {
		this.kayitSeviyesiIki = kayitSeviyesiIki;
	}

	public String getKayitSeviyesiUc() {
		return this.kayitSeviyesiUc;
	}

	public void setKayitSeviyesiUc(String kayitSeviyesiUc) {
		this.kayitSeviyesiUc = kayitSeviyesiUc;
	}

	public String getProbCapiFrekansiBir() {
		return this.probCapiFrekansiBir;
	}

	public void setProbCapiFrekansiBir(String probCapiFrekansiBir) {
		this.probCapiFrekansiBir = probCapiFrekansiBir;
	}

	public String getProbCapiFrekansiIki() {
		return this.probCapiFrekansiIki;
	}

	public void setProbCapiFrekansiIki(String probCapiFrekansiIki) {
		this.probCapiFrekansiIki = probCapiFrekansiIki;
	}

	public String getProbCapiFrekansiUc() {
		return this.probCapiFrekansiUc;
	}

	public void setProbCapiFrekansiUc(String probCapiFrekansiUc) {
		this.probCapiFrekansiUc = probCapiFrekansiUc;
	}

	public String getReferansCentikDelikBir() {
		return this.referansCentikDelikBir;
	}

	public void setReferansCentikDelikBir(String referansCentikDelikBir) {
		this.referansCentikDelikBir = referansCentikDelikBir;
	}

	public String getReferansCentikDelikIki() {
		return this.referansCentikDelikIki;
	}

	public void setReferansCentikDelikIki(String referansCentikDelikIki) {
		this.referansCentikDelikIki = referansCentikDelikIki;
	}

	public String getReferansCentikDelikUc() {
		return this.referansCentikDelikUc;
	}

	public void setReferansCentikDelikUc(String referansCentikDelikUc) {
		this.referansCentikDelikUc = referansCentikDelikUc;
	}

	public String getReferansSeviyeBir() {
		return this.referansSeviyeBir;
	}

	public void setReferansSeviyeBir(String referansSeviyeBir) {
		this.referansSeviyeBir = referansSeviyeBir;
	}

	public String getReferansSeviyeIki() {
		return this.referansSeviyeIki;
	}

	public void setReferansSeviyeIki(String referansSeviyeIki) {
		this.referansSeviyeIki = referansSeviyeIki;
	}

	public String getReferansSeviyeUc() {
		return this.referansSeviyeUc;
	}

	public void setReferansSeviyeUc(String referansSeviyeUc) {
		this.referansSeviyeUc = referansSeviyeUc;
	}

	/**
	 * @return the eklemeZamani
	 */
	public Date getEklemeZamani() {
		return eklemeZamani;
	}

	/**
	 * @param eklemeZamani
	 *            the eklemeZamani to set
	 */
	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncellemeZamani
	 */
	public Date getGuncellemeZamani() {
		return guncellemeZamani;
	}

	/**
	 * @param guncellemeZamani
	 *            the guncellemeZamani to set
	 */
	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the kalibrasyonZamani
	 */
	public Date getKalibrasyonZamani() {
		return kalibrasyonZamani;
	}

	/**
	 * @param kalibrasyonZamani
	 *            the kalibrasyonZamani to set
	 */
	public void setKalibrasyonZamani(Date kalibrasyonZamani) {
		this.kalibrasyonZamani = kalibrasyonZamani;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the taramaHiziBir
	 */
	public String getTaramaHiziBir() {
		return taramaHiziBir;
	}

	/**
	 * @param taramaHiziBir
	 *            the taramaHiziBir to set
	 */
	public void setTaramaHiziBir(String taramaHiziBir) {
		this.taramaHiziBir = taramaHiziBir;
	}

	/**
	 * @return the taramaHiziIki
	 */
	public String getTaramaHiziIki() {
		return taramaHiziIki;
	}

	/**
	 * @param taramaHiziIki
	 *            the taramaHiziIki to set
	 */
	public void setTaramaHiziIki(String taramaHiziIki) {
		this.taramaHiziIki = taramaHiziIki;
	}

	/**
	 * @return the taramaHiziUc
	 */
	public String getTaramaHiziUc() {
		return taramaHiziUc;
	}

	/**
	 * @param taramaHiziUc
	 *            the taramaHiziUc to set
	 */
	public void setTaramaHiziUc(String taramaHiziUc) {
		this.taramaHiziUc = taramaHiziUc;
	}

	/**
	 * @return the taramaAlaniBir
	 */
	public String getTaramaAlaniBir() {
		return taramaAlaniBir;
	}

	/**
	 * @param taramaAlaniBir
	 *            the taramaAlaniBir to set
	 */
	public void setTaramaAlaniBir(String taramaAlaniBir) {
		this.taramaAlaniBir = taramaAlaniBir;
	}

	/**
	 * @return the taramaAlaniIki
	 */
	public String getTaramaAlaniIki() {
		return taramaAlaniIki;
	}

	/**
	 * @param taramaAlaniIki
	 *            the taramaAlaniIki to set
	 */
	public void setTaramaAlaniIki(String taramaAlaniIki) {
		this.taramaAlaniIki = taramaAlaniIki;
	}

	/**
	 * @return the taramaAlaniUc
	 */
	public String getTaramaAlaniUc() {
		return taramaAlaniUc;
	}

	/**
	 * @param taramaAlaniUc
	 *            the taramaAlaniUc to set
	 */
	public void setTaramaAlaniUc(String taramaAlaniUc) {
		this.taramaAlaniUc = taramaAlaniUc;
	}

	/**
	 * @return the kalibrasyonMesafesiBir
	 */
	public String getKalibrasyonMesafesiBir() {
		return kalibrasyonMesafesiBir;
	}

	/**
	 * @param kalibrasyonMesafesiBir
	 *            the kalibrasyonMesafesiBir to set
	 */
	public void setKalibrasyonMesafesiBir(String kalibrasyonMesafesiBir) {
		this.kalibrasyonMesafesiBir = kalibrasyonMesafesiBir;
	}

	/**
	 * @return the kalibrasyonMesafesiIki
	 */
	public String getKalibrasyonMesafesiIki() {
		return kalibrasyonMesafesiIki;
	}

	/**
	 * @param kalibrasyonMesafesiIki
	 *            the kalibrasyonMesafesiIki to set
	 */
	public void setKalibrasyonMesafesiIki(String kalibrasyonMesafesiIki) {
		this.kalibrasyonMesafesiIki = kalibrasyonMesafesiIki;
	}

	/**
	 * @return the kalibrasyonMesafesiUc
	 */
	public String getKalibrasyonMesafesiUc() {
		return kalibrasyonMesafesiUc;
	}

	/**
	 * @param kalibrasyonMesafesiUc
	 *            the kalibrasyonMesafesiUc to set
	 */
	public void setKalibrasyonMesafesiUc(String kalibrasyonMesafesiUc) {
		this.kalibrasyonMesafesiUc = kalibrasyonMesafesiUc;
	}

	/**
	 * @return the cihaz
	 */
	public String getCihaz() {
		return cihaz;
	}

	/**
	 * @param cihaz
	 *            the cihaz to set
	 */
	public void setCihaz(String cihaz) {
		this.cihaz = cihaz;
	}

	/**
	 * @return the kalibrasyonBloguBir
	 */
	public String getKalibrasyonBloguBir() {
		return kalibrasyonBloguBir;
	}

	/**
	 * @param kalibrasyonBloguBir the kalibrasyonBloguBir to set
	 */
	public void setKalibrasyonBloguBir(String kalibrasyonBloguBir) {
		this.kalibrasyonBloguBir = kalibrasyonBloguBir;
	}

	/**
	 * @return the kalibrasyonBloguIki
	 */
	public String getKalibrasyonBloguIki() {
		return kalibrasyonBloguIki;
	}

	/**
	 * @param kalibrasyonBloguIki the kalibrasyonBloguIki to set
	 */
	public void setKalibrasyonBloguIki(String kalibrasyonBloguIki) {
		this.kalibrasyonBloguIki = kalibrasyonBloguIki;
	}

	/**
	 * @return the kalibrasyonBloguUc
	 */
	public String getKalibrasyonBloguUc() {
		return kalibrasyonBloguUc;
	}

	/**
	 * @param kalibrasyonBloguUc the kalibrasyonBloguUc to set
	 */
	public void setKalibrasyonBloguUc(String kalibrasyonBloguUc) {
		this.kalibrasyonBloguUc = kalibrasyonBloguUc;
	}

}