package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the general_privileges database table.
 * 
 */
@Entity
@Table(name="general_privileges")
public class GeneralPrivilege implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="general_privilege_id")
	@SequenceGenerator(name="GENERAL_PRIVILEGE_GENERATOR", sequenceName="GENERAL_PRIVILEGE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="GENERAL_PRIVILEGE_GENERATOR")	
	private Integer generalPrivilegeId;

	@Column(name="coat_material_p")
	private Integer coatMaterialP;

	@Column(name="company_p")
	private Integer companyP;

	@Column(name="department_p")
	private Integer departmentP;

	@Column(name="location_p")
	private Integer locationP;

	@Column(name="machine_p")
	private Integer machineP;

	@Column(name="manufacturing_material_p")
	private Integer manufacturingMaterialP;

	@Column(name="order_p")
	private Integer orderP;

	@Column(name="user_p")
	private Integer userP;
	
	@Column(name="employee_p")
	private Integer employeeP;
	
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="user_type_id", referencedColumnName="user_type_id", updatable= false)
	private UserType userType;
	

    public GeneralPrivilege() {
    	
    }

	public Integer getCoatMaterialP() {
		return this.coatMaterialP;
	}

	public void setCoatMaterialP(Integer  coatMaterialP) {
		this.coatMaterialP = coatMaterialP;
	}

	public Integer  getCompanyP() {
		return this.companyP;
	}

	public void setCompanyP(Integer  company) {
		this.companyP = company;
	}

	public Integer  getDepartmentP() {
		return this.departmentP;
	}

	public void setDepartmentP(Integer  departmentP) {
		this.departmentP = departmentP;
	}

	public Integer  getLocationP() {
		return this.locationP;
	}

	public void setLocationP(Integer  location) {
		this.locationP = location;
	}

	public Integer  getMachineP() {
		return this.machineP;
	}

	public void setMachineP(Integer  machineP) {
		this.machineP = machineP;
	}

	public Integer  getManufacturingMaterialP() {
		return this.manufacturingMaterialP;
	}

	public void setManufacturingMaterialP(Integer  manufacturingMaterialP) {
		this.manufacturingMaterialP = manufacturingMaterialP;
	}

	public Integer  getOrderP() {
		return this.orderP;
	}

	public void setOrderP(Integer  orderP) {
		this.orderP = orderP;
	}

	public Integer  getUserP() {
		return this.userP;
	}

	public void setUserP(Integer  userP) {
		this.userP = userP;
	}

	public Integer  getEmployeeP() {
		return employeeP;
	}

	public void setEmployeeP(Integer  employeeP) {
		this.employeeP = employeeP;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getGeneralPrivilegeId() {
		return generalPrivilegeId;
	}

	public void setGeneralPrivilegeId(Integer generalPrivilegeId) {
		this.generalPrivilegeId = generalPrivilegeId;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

}