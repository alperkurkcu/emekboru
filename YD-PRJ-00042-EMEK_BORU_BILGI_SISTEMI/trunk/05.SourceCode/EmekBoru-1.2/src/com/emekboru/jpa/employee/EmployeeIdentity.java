package com.emekboru.jpa.employee;

import java.io.Serializable;

import javax.persistence.*;

import com.emekboru.jpa.Employee;

import java.sql.Timestamp;

/**
 * The persistent class for the employee_identity database table.
 * 
 */
@Entity
@Table(name = "employee_identity")
@NamedQuery(name = "EmployeeIdentity.findAll", query = "SELECT e FROM EmployeeIdentity e")
public class EmployeeIdentity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "EMPLOYEE_IDENTITY_EMPLOYEEIDENTITYID_GENERATOR", sequenceName = "EMPLOYEE_IDENTITY_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EMPLOYEE_IDENTITY_EMPLOYEEIDENTITYID_GENERATOR")
	@Column(name = "employee_identity_id")
	private Integer employeeIdentityId;

	@Column(name = "ekleme_zamani")
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici")
	private Integer ekleyenKullanici;

	@Column(name = "father_name")
	private String fatherName;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "mother_name")
	private String motherName;

	@Column(name="tckn")
	private String tckn;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "employee_id", referencedColumnName = "employee_id", insertable = false)
	private Employee employee;

	public EmployeeIdentity() {
	}

	public Integer getEmployeeIdentityId() {
		return this.employeeIdentityId;
	}

	public void setEmployeeIdentityId(Integer employeeIdentityId) {
		this.employeeIdentityId = employeeIdentityId;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public String getFatherName() {
		return this.fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getMotherName() {
		return this.motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public String getTckn() {
		return this.tckn;
	}

	public void setTckn(String tckn) {
		this.tckn = tckn;
	}

	public Employee getEmployee() {
		return this.employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

}