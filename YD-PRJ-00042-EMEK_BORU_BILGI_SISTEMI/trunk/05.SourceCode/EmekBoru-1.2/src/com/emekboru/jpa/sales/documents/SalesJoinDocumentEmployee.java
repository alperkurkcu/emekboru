package com.emekboru.jpa.sales.documents;

import java.io.Serializable;

import javax.persistence.*;

import com.emekboru.jpa.Employee;

@Entity
@Table(name = "sales_join_document_employee")
public class SalesJoinDocumentEmployee implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -386309865998115859L;

	@Id
	@Column(name = "join_id")
	@SequenceGenerator(name = "SALES_JOIN_DOCUMENT_EMPLOYEE_GENERATOR", sequenceName = "SALES_JOIN_DOCUMENT_EMPLOYEE_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_JOIN_DOCUMENT_EMPLOYEE_GENERATOR")
	private int joinId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "employee_id", referencedColumnName = "employee_id", updatable = false)
	private Employee employee;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "document_id", referencedColumnName = "document_id", updatable = false)
	private SalesDocument document;

	public SalesJoinDocumentEmployee() {
		employee = new Employee();
		document = new SalesDocument();
	}

	/**
	 * GETTERS AND SETTERS
	 */

	public int getJoinId() {
		return joinId;
	}

	public SalesDocument getDocument() {
		return document;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setJoinId(Integer joinId) {
		this.joinId = joinId;
	}

	public void setDocument(SalesDocument document) {
		this.document = document;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
