package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "pipe_machine_rulo_link")
public class PipeMachineRuloLink implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@SequenceGenerator(name = "PIPE_MACHINE_RULO_LINK_GENERATOR", sequenceName = "PIPE_MACHINE_RULO_LINK_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PIPE_MACHINE_RULO_LINK_GENERATOR")
	private int id;

	@Basic
	@Column(name = "machine_rulo_link_id", insertable = false, updatable = false)
	private Integer machineRuloLinkId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "machine_rulo_link_id", referencedColumnName = "id", insertable = false)
	private MachineRuloLink machineRuloLink;

	@Basic
	@Column(name = "pipe_id", insertable = false, updatable = false)
	private Integer pipeId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	public PipeMachineRuloLink() {

	}

	@Override
	public boolean equals(Object obj) {

		if (!(obj instanceof PipeMachineRuloLink))
			return false;
		if (this.id == ((PipeMachineRuloLink) obj).id)
			return true;
		return false;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public MachineRuloLink getMachineRuloLink() {
		return machineRuloLink;
	}

	public void setMachineRuloLink(MachineRuloLink machineRuloLink) {
		this.machineRuloLink = machineRuloLink;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}
}
