package com.emekboru.jpa.istakipformu;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the is_takip_formu_beton_kaplama database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "IsTakipFormuBetonKaplama.findAll", query = "SELECT r FROM IsTakipFormuBetonKaplama r WHERE r.salesItem.itemId=:prmItemId") })
@Table(name = "is_takip_formu_beton_kaplama")
public class IsTakipFormuBetonKaplama implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "IS_TAKIP_FORMU_BETON_KAPLAMA_ID_GENERATOR", sequenceName = "IS_TAKIP_FORMU_BETON_KAPLAMA_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IS_TAKIP_FORMU_BETON_KAPLAMA_ID_GENERATOR")
	@Column(unique = true, nullable = false, name = "ID")
	private Integer id;

	// @Column(name="bitince_id")
	// private Integer bitinceId;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "bitince_id", referencedColumnName = "ID", insertable = false)
	private IsTakipFormuBetonKaplamaBitinceIslemler isTakipFormuBetonKaplamaBitinceIslemler;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	// @Column(name = "once_id")
	// private Integer onceId;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "once_id", referencedColumnName = "ID", insertable = false)
	private IsTakipFormuBetonKaplamaOnceIslemler isTakipFormuBetonKaplamaOnceIslemler;

	// @Column(name = "sonra_id")
	// private Integer sonraId;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "sonra_id", referencedColumnName = "ID", insertable = false)
	private IsTakipFormuBetonKaplamaSonraIslemler isTakipFormuBetonKaplamaSonraIslemler;

	// @Column(name = "sales_item_id")
	// private Integer salesItemId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	private SalesItem salesItem;

	public IsTakipFormuBetonKaplama() {

		isTakipFormuBetonKaplamaBitinceIslemler = new IsTakipFormuBetonKaplamaBitinceIslemler();
		isTakipFormuBetonKaplamaOnceIslemler = new IsTakipFormuBetonKaplamaOnceIslemler();
		isTakipFormuBetonKaplamaSonraIslemler = new IsTakipFormuBetonKaplamaSonraIslemler();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public IsTakipFormuBetonKaplamaBitinceIslemler getIsTakipFormuBetonKaplamaBitinceIslemler() {
		return isTakipFormuBetonKaplamaBitinceIslemler;
	}

	public void setIsTakipFormuBetonKaplamaBitinceIslemler(
			IsTakipFormuBetonKaplamaBitinceIslemler isTakipFormuBetonKaplamaBitinceIslemler) {
		this.isTakipFormuBetonKaplamaBitinceIslemler = isTakipFormuBetonKaplamaBitinceIslemler;
	}

	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	public IsTakipFormuBetonKaplamaOnceIslemler getIsTakipFormuBetonKaplamaOnceIslemler() {
		return isTakipFormuBetonKaplamaOnceIslemler;
	}

	public void setIsTakipFormuBetonKaplamaOnceIslemler(
			IsTakipFormuBetonKaplamaOnceIslemler isTakipFormuBetonKaplamaOnceIslemler) {
		this.isTakipFormuBetonKaplamaOnceIslemler = isTakipFormuBetonKaplamaOnceIslemler;
	}

	public IsTakipFormuBetonKaplamaSonraIslemler getIsTakipFormuBetonKaplamaSonraIslemler() {
		return isTakipFormuBetonKaplamaSonraIslemler;
	}

	public void setIsTakipFormuBetonKaplamaSonraIslemler(
			IsTakipFormuBetonKaplamaSonraIslemler isTakipFormuBetonKaplamaSonraIslemler) {
		this.isTakipFormuBetonKaplamaSonraIslemler = isTakipFormuBetonKaplamaSonraIslemler;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}