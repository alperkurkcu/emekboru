package com.emekboru.jpa.personel;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the personel_ortak_ozur_orani database table.
 * 
 */
@Entity
@Table(name="personel_ortak_ozur_orani")
public class PersonelOrtakOzurOrani implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PERSONEL_ORTAK_OZUR_ORANI_ID_GENERATOR", sequenceName="PERSONEL_ORTAK_OZUR_ORANI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PERSONEL_ORTAK_OZUR_ORANI_ID_GENERATOR")
	@Column(name="ID")
	private Integer id;

	@Column(name="eklenme_tarihi")
	private Timestamp eklenmeTarihi;

	@Column(name="kullanici_id_ekleyen")
	private Integer kullaniciIdEkleyen;

	@Column(name="kullanici_id_guncelleyen")
	private Integer kullaniciIdGuncelleyen;

	@Column(name="personel_ozur_adi")
	private String personelOzurAdi;

	@Column(name="son_guncellenme_tarihi")
	private Timestamp sonGuncellenmeTarihi;

	public PersonelOrtakOzurOrani() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getEklenmeTarihi() {
		return this.eklenmeTarihi;
	}

	public void setEklenmeTarihi(Timestamp eklenmeTarihi) {
		this.eklenmeTarihi = eklenmeTarihi;
	}

	public Integer getKullaniciIdEkleyen() {
		return this.kullaniciIdEkleyen;
	}

	public void setKullaniciIdEkleyen(Integer kullaniciIdEkleyen) {
		this.kullaniciIdEkleyen = kullaniciIdEkleyen;
	}

	public Integer getKullaniciIdGuncelleyen() {
		return this.kullaniciIdGuncelleyen;
	}

	public void setKullaniciIdGuncelleyen(Integer kullaniciIdGuncelleyen) {
		this.kullaniciIdGuncelleyen = kullaniciIdGuncelleyen;
	}

	public String getPersonelOzurAdi() {
		return this.personelOzurAdi;
	}

	public void setPersonelOzurAdi(String personelOzurAdi) {
		this.personelOzurAdi = personelOzurAdi;
	}

	public Timestamp getSonGuncellenmeTarihi() {
		return this.sonGuncellenmeTarihi;
	}

	public void setSonGuncellenmeTarihi(Timestamp sonGuncellenmeTarihi) {
		this.sonGuncellenmeTarihi = sonGuncellenmeTarihi;
	}

}