package com.emekboru.jpa;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@NamedQueries({ @NamedQuery(name = "EdwPipeLink.findByPrmPipeId", query = "SELECT mel FROM EdwPipeLink mel WHERE mel.pipe.pipeId =:prmPipeId") })
@Table(name = "edw_pipe_link")
public class EdwPipeLink implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id")
	@SequenceGenerator(name = "EDW_PIPE_LINK_GENERATOR", sequenceName = "EDW_PIPE_LINK_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EDW_PIPE_LINK_GENERATOR")
	private Integer id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "electrode_dust_wire_id", referencedColumnName = "electrode_dust_wire_id", insertable = false, updatable = false)
	private ElectrodeDustWire edw;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "electrode_dust_wire_id", referencedColumnName = "electrode_dust_wire_id")
	private MachineEdwLink machineEdwLink;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pipe_id", referencedColumnName = "pipe_id", insertable = false)
	private Pipe pipe;

	@Column(name = "akim")
	private BigDecimal akim;

	@Column(name = "volt")
	private BigDecimal volt;

	@Column(name = "ic_dis")
	private Integer icDis;

	@Column(name = "ac_dc")
	private Integer acDc;

	public EdwPipeLink() {

	}

	@Override
	public boolean equals(Object obj) {

		if (!(obj instanceof EdwPipeLink))
			return false;
		if (this.id == ((EdwPipeLink) obj).id)
			return true;
		return false;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ElectrodeDustWire getEdw() {
		return edw;
	}

	public void setEdw(ElectrodeDustWire edw) {
		this.edw = edw;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public MachineEdwLink getMachineEdwLink() {
		return machineEdwLink;
	}

	public void MachineEdwLink(MachineEdwLink machineEdwLink) {
		this.machineEdwLink = machineEdwLink;
	}

	/**
	 * @return the akim
	 */
	public BigDecimal getAkim() {
		return akim;
	}

	/**
	 * @param akim
	 *            the akim to set
	 */
	public void setAkim(BigDecimal akim) {
		this.akim = akim;
	}

	/**
	 * @return the volt
	 */
	public BigDecimal getVolt() {
		return volt;
	}

	/**
	 * @param volt
	 *            the volt to set
	 */
	public void setVolt(BigDecimal volt) {
		this.volt = volt;
	}

	/**
	 * @return the icDis
	 */
	public Integer getIcDis() {
		return icDis;
	}

	/**
	 * @param icDis
	 *            the icDis to set
	 */
	public void setIcDis(Integer icDis) {
		this.icDis = icDis;
	}

	/**
	 * @return the acDc
	 */
	public Integer getAcDc() {
		return acDc;
	}

	/**
	 * @param acDc
	 *            the acDc to set
	 */
	public void setAcDc(Integer acDc) {
		this.acDc = acDc;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

}
