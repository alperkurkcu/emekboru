package com.emekboru.jpa;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.sales.customer.SalesContactCompany;
import com.emekboru.jpa.sales.customer.SalesCustomer;

/**
 * The persistent class for the ulkeler database table.
 * 
 */
@Entity
@NamedQueries({ @NamedQuery(name = "Ulkeler.findAll", query = "SELECT r FROM Ulkeler r order by r.id") })
@Table(name = "ulkeler")
public class Ulkeler implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "ULKELER_ID_GENERATOR", sequenceName = "ULKELER_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ULKELER_ID_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Integer id;

	@Temporal(TemporalType.DATE)
	@Column(name = "ekleme_zamani", nullable = false)
	private Date eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@Temporal(TemporalType.DATE)
	@Column(name = "guncelleme_zamani")
	private Date guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@Column(name = "ulke_ismi", nullable = false, length = 32)
	private String ulkeIsmi;

	@Column(name = "ulke_kodu", nullable = false, length = 4)
	private String ulkeKodu;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "ulkeler", cascade = CascadeType.ALL)
	private List<Customer> customers;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "ulkeler", cascade = CascadeType.ALL)
	private List<Department> departments;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "ulkeler", cascade = CascadeType.ALL)
	private List<Employee> employee;

	@OneToMany(mappedBy = "ulkeler", cascade = CascadeType.ALL)
	private List<SalesContactCompany> salesContactCompanies;

	@OneToMany(mappedBy = "ulkeler", cascade = CascadeType.ALL)
	private List<SalesCustomer> salesCustomers;

	public Ulkeler() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Date eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Date getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Date guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getUlkeIsmi() {
		return this.ulkeIsmi;
	}

	public void setUlkeIsmi(String ulkeIsmi) {
		this.ulkeIsmi = ulkeIsmi;
	}

	public String getUlkeKodu() {
		return this.ulkeKodu;
	}

	public void setUlkeKodu(String ulkeKodu) {
		this.ulkeKodu = ulkeKodu;
	}

	public List<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

	public List<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}

	public List<Employee> getEmployee() {
		return employee;
	}

	public void setEmployee(List<Employee> employee) {
		this.employee = employee;
	}

	public List<SalesContactCompany> getSalesContactCompanies() {
		return salesContactCompanies;
	}

	public void setSalesContactCompanies(
			List<SalesContactCompany> salesContactCompanies) {
		this.salesContactCompanies = salesContactCompanies;
	}

	public List<SalesCustomer> getSalesCustomers() {
		return salesCustomers;
	}

	public void setSalesCustomers(List<SalesCustomer> salesCustomers) {
		this.salesCustomers = salesCustomers;
	}

}