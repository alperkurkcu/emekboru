package com.emekboru.jpa.kaplamagirditest;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.SystemUser;

/**
 * The persistent class for the test_hammadde_girdi_epoksi_toz_ety1 database
 * table.
 * 
 */
@Entity
@Table(name = "test_hammadde_girdi_epoksi_toz_ety1")
public class TestHammaddeGirdiEpoksiTozEty1 implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "TEST_HAMMADDE_GIRDI_EPOKSI_TOZ_ETY1_ID_GENERATOR", sequenceName = "TEST_HAMMADDE_GIRDI_EPOKSI_TOZ_ETY1_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TEST_HAMMADDE_GIRDI_EPOKSI_TOZ_ETY1_ID_GENERATOR")
	@Column(name = "ID")
	private Integer id;

	// @Column(name = "coat_material_id")
	// private Integer coatMaterialId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coat_material_id", referencedColumnName = "coat_material_id", insertable = false)
	private CoatRawMaterial coatRawMaterial;

	@Column(name = "degerlendirme_standard")
	private String degerlendirmeStandard;

	@Column(name = "ekleme_zamani", nullable = false)
	private Timestamp eklemeZamani;

	@Column(name = "ekleyen_kullanici", nullable = false)
	private Integer ekleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ekleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser ekleyenEmployee;

	@Column(name = "guncelleme_zamani")
	private Timestamp guncellemeZamani;

	@Column(name = "guncelleyen_kullanici")
	private Integer guncelleyenKullanici;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "guncelleyen_kullanici", referencedColumnName = "ID", insertable = false, updatable = false)
	private SystemUser guncelleyenEmployee;

	@Column(name = "ortalama_yogunluk")
	private String ortalamaYogunluk;

	@Column(name = "parti_no")
	private String partiNo;

	private Boolean sonuc;

	private String standard;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "test_tarihi", nullable = false)
	private Date testTarihi;

	@Column(name = "uretici_spec")
	private String ureticiSpec;

	@Column(name = "yogunluk_1")
	private String yogunluk1;

	@Column(name = "yogunluk_2")
	private String yogunluk2;

	@Column(name = "yogunluk_3")
	private String yogunluk3;

	@Column(name = "yogunluk_4")
	private String yogunluk4;

	@Column(name = "yogunluk_5")
	private String yogunluk5;

	@Column(name = "rapor_no")
	private String raporNo;

	public TestHammaddeGirdiEpoksiTozEty1() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDegerlendirmeStandard() {
		return this.degerlendirmeStandard;
	}

	public void setDegerlendirmeStandard(String degerlendirmeStandard) {
		this.degerlendirmeStandard = degerlendirmeStandard;
	}

	public Timestamp getEklemeZamani() {
		return this.eklemeZamani;
	}

	public void setEklemeZamani(Timestamp eklemeZamani) {
		this.eklemeZamani = eklemeZamani;
	}

	public Integer getEkleyenKullanici() {
		return this.ekleyenKullanici;
	}

	public void setEkleyenKullanici(Integer ekleyenKullanici) {
		this.ekleyenKullanici = ekleyenKullanici;
	}

	public Timestamp getGuncellemeZamani() {
		return this.guncellemeZamani;
	}

	public void setGuncellemeZamani(Timestamp guncellemeZamani) {
		this.guncellemeZamani = guncellemeZamani;
	}

	public Integer getGuncelleyenKullanici() {
		return this.guncelleyenKullanici;
	}

	public void setGuncelleyenKullanici(Integer guncelleyenKullanici) {
		this.guncelleyenKullanici = guncelleyenKullanici;
	}

	public String getOrtalamaYogunluk() {
		return this.ortalamaYogunluk;
	}

	public void setOrtalamaYogunluk(String ortalamaYogunluk) {
		this.ortalamaYogunluk = ortalamaYogunluk;
	}

	public String getPartiNo() {
		return this.partiNo;
	}

	public void setPartiNo(String partiNo) {
		this.partiNo = partiNo;
	}

	public Boolean getSonuc() {
		return this.sonuc;
	}

	public void setSonuc(Boolean sonuc) {
		this.sonuc = sonuc;
	}

	public String getStandard() {
		return this.standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getUreticiSpec() {
		return this.ureticiSpec;
	}

	public void setUreticiSpec(String ureticiSpec) {
		this.ureticiSpec = ureticiSpec;
	}

	public String getYogunluk1() {
		return this.yogunluk1;
	}

	public void setYogunluk1(String yogunluk1) {
		this.yogunluk1 = yogunluk1;
	}

	public String getYogunluk2() {
		return this.yogunluk2;
	}

	public void setYogunluk2(String yogunluk2) {
		this.yogunluk2 = yogunluk2;
	}

	public String getYogunluk3() {
		return this.yogunluk3;
	}

	public void setYogunluk3(String yogunluk3) {
		this.yogunluk3 = yogunluk3;
	}

	public String getYogunluk4() {
		return this.yogunluk4;
	}

	public void setYogunluk4(String yogunluk4) {
		this.yogunluk4 = yogunluk4;
	}

	public String getYogunluk5() {
		return this.yogunluk5;
	}

	public void setYogunluk5(String yogunluk5) {
		this.yogunluk5 = yogunluk5;
	}

	/**
	 * @return the coatRawMaterial
	 */
	public CoatRawMaterial getCoatRawMaterial() {
		return coatRawMaterial;
	}

	/**
	 * @param coatRawMaterial
	 *            the coatRawMaterial to set
	 */
	public void setCoatRawMaterial(CoatRawMaterial coatRawMaterial) {
		this.coatRawMaterial = coatRawMaterial;
	}

	/**
	 * @return the ekleyenEmployee
	 */
	public SystemUser getEkleyenEmployee() {
		return ekleyenEmployee;
	}

	/**
	 * @param ekleyenEmployee
	 *            the ekleyenEmployee to set
	 */
	public void setEkleyenEmployee(SystemUser ekleyenEmployee) {
		this.ekleyenEmployee = ekleyenEmployee;
	}

	/**
	 * @return the guncelleyenEmployee
	 */
	public SystemUser getGuncelleyenEmployee() {
		return guncelleyenEmployee;
	}

	/**
	 * @param guncelleyenEmployee
	 *            the guncelleyenEmployee to set
	 */
	public void setGuncelleyenEmployee(SystemUser guncelleyenEmployee) {
		this.guncelleyenEmployee = guncelleyenEmployee;
	}

	/**
	 * @return the testTarihi
	 */
	public Date getTestTarihi() {
		return testTarihi;
	}

	/**
	 * @param testTarihi
	 *            the testTarihi to set
	 */
	public void setTestTarihi(Date testTarihi) {
		this.testTarihi = testTarihi;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the raporNo
	 */
	public String getRaporNo() {
		return raporNo;
	}

	/**
	 * @param raporNo
	 *            the raporNo to set
	 */
	public void setRaporNo(String raporNo) {
		this.raporNo = raporNo;
	}

}