/**
 * 
 */
package com.emekboru.jpa.istakipformu;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isTakipFormuOrtak")
@ViewScoped
public class IsTakipFormuOrtak {

	@EJB
	private ConfigBean configBean;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	// Dış Kumlama
	public void goToIsTakipFormuDisKumlama() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().IS_TAKIP_FORMU_DIS_KUMLAMA_PAGE);
	}

	// İç Kumlama
	public void goToIsTakipFormuIcKumlama() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().IS_TAKIP_FORMU_IC_KUMLAMA_PAGE);
	}

	// Epoksi
	public void goToIsTakipFormuEpoksi() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().IS_TAKIP_FORMU_EPOKSI_PAGE);
	}

	// Polietilen
	public void goToIsTakipFormuPolietilen() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().IS_TAKIP_FORMU_POLIETILEN_PAGE);
	}

	// Beton
	public void goToIsTakipFormuBeton() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().IS_TAKIP_FORMU_BETON_PAGE);
	}

	// setters getters

	public ConfigBean getConfigBean() {
		return configBean;
	}

	public void setConfigBean(ConfigBean configBean) {
		this.configBean = configBean;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

}
