package com.emekboru.jpa.personel;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the personel_izin_talep_bilgileri database table.
 * 
 */
@Entity
@Table(name="personel_izin_talep_bilgileri")
public class PersonelIzinTalepBilgileri implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PERSONEL_IZIN_TALEP_BILGILERI_ID_GENERATOR", sequenceName="PERSONEL_IZIN_TALEP_BILGILERI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PERSONEL_IZIN_TALEP_BILGILERI_ID_GENERATOR")
	@Column(name="ID")
	private Integer id;
	@Column(name="	")
	private String aciklama;
	@Column(name="aktif")
	private Boolean aktif;

	@Column(name="baslangic_tarihi")
	private Timestamp baslangicTarihi;

	@Column(name="bitis_tarihi")
	private Timestamp bitisTarihi;

	@Column(name="eklenme_tarihi")
	private Timestamp eklenmeTarihi;

	@Column(name="izin_adres")
	private String izinAdres;

	@Column(name="izin_gunu_sayisi")
	private Integer izinGunuSayisi;

	@Column(name="izin_telefon")
	private String izinTelefon;

	@Column(name="izin_turu_id")
	private Integer izinTuruId;

	@Column(name="kimik_id_vekalet_kisi")
	private Integer kimikIdVekaletKisi;

	@Column(name="kimlik_id")
	private Integer kimlikId;

	@Column(name="kimlik_id_onaylayan")
	private Integer kimlikIdOnaylayan;

	@Column(name="kullanici_id_ekleyen")
	private Integer kullaniciIdEkleyen;

	@Column(name="kullanici_id_guncelleyen")
	private Integer kullaniciIdGuncelleyen;

	@Column(name="son_guncellenme_tarihi")
	private Timestamp sonGuncellenmeTarihi;

	public PersonelIzinTalepBilgileri() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public Boolean getAktif() {
		return this.aktif;
	}

	public void setAktif(Boolean aktif) {
		this.aktif = aktif;
	}

	public Timestamp getBaslangicTarihi() {
		return this.baslangicTarihi;
	}

	public void setBaslangicTarihi(Timestamp baslangicTarihi) {
		this.baslangicTarihi = baslangicTarihi;
	}

	public Timestamp getBitisTarihi() {
		return this.bitisTarihi;
	}

	public void setBitisTarihi(Timestamp bitisTarihi) {
		this.bitisTarihi = bitisTarihi;
	}

	public Timestamp getEklenmeTarihi() {
		return this.eklenmeTarihi;
	}

	public void setEklenmeTarihi(Timestamp eklenmeTarihi) {
		this.eklenmeTarihi = eklenmeTarihi;
	}

	public String getIzinAdres() {
		return this.izinAdres;
	}

	public void setIzinAdres(String izinAdres) {
		this.izinAdres = izinAdres;
	}

	public Integer getIzinGunuSayisi() {
		return this.izinGunuSayisi;
	}

	public void setIzinGunuSayisi(Integer izinGunuSayisi) {
		this.izinGunuSayisi = izinGunuSayisi;
	}

	public String getIzinTelefon() {
		return this.izinTelefon;
	}

	public void setIzinTelefon(String izinTelefon) {
		this.izinTelefon = izinTelefon;
	}

	public Integer getIzinTuruId() {
		return this.izinTuruId;
	}

	public void setIzinTuruId(Integer izinTuruId) {
		this.izinTuruId = izinTuruId;
	}

	public Integer getKimikIdVekaletKisi() {
		return this.kimikIdVekaletKisi;
	}

	public void setKimikIdVekaletKisi(Integer kimikIdVekaletKisi) {
		this.kimikIdVekaletKisi = kimikIdVekaletKisi;
	}

	public Integer getKimlikId() {
		return this.kimlikId;
	}

	public void setKimlikId(Integer kimlikId) {
		this.kimlikId = kimlikId;
	}

	public Integer getKimlikIdOnaylayan() {
		return this.kimlikIdOnaylayan;
	}

	public void setKimlikIdOnaylayan(Integer kimlikIdOnaylayan) {
		this.kimlikIdOnaylayan = kimlikIdOnaylayan;
	}

	public Integer getKullaniciIdEkleyen() {
		return this.kullaniciIdEkleyen;
	}

	public void setKullaniciIdEkleyen(Integer kullaniciIdEkleyen) {
		this.kullaniciIdEkleyen = kullaniciIdEkleyen;
	}

	public Integer getKullaniciIdGuncelleyen() {
		return this.kullaniciIdGuncelleyen;
	}

	public void setKullaniciIdGuncelleyen(Integer kullaniciIdGuncelleyen) {
		this.kullaniciIdGuncelleyen = kullaniciIdGuncelleyen;
	}

	public Timestamp getSonGuncellenmeTarihi() {
		return this.sonGuncellenmeTarihi;
	}

	public void setSonGuncellenmeTarihi(Timestamp sonGuncellenmeTarihi) {
		this.sonGuncellenmeTarihi = sonGuncellenmeTarihi;
	}

}