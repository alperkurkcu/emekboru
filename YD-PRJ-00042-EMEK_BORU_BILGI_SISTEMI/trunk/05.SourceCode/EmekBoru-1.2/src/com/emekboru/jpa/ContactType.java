package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "contact_type")
public class ContactType implements Serializable {

	private static final long serialVersionUID = 67394491652394276L;

	@Id
	@Column(name = "contact_type_id")
	@SequenceGenerator(name = "CONTACT_TYPE_GENERATOR", sequenceName = "CONTACT_TYPE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONTACT_TYPE_GENERATOR")
	private Integer contactTypeId;

	@Column(name = "name")
	private String name;

	@Column(name = "short_desc")
	private String shortDesc;

	public ContactType() {

	}

	@Override
	public boolean equals(Object obj) {

		if (obj instanceof ContactType) {

			if (((ContactType) obj).getContactTypeId().equals(
					this.contactTypeId))
				return true;
		}
		return false;
	}

	/**
	 * GETTERS AND SETTERS
	 */
	public Integer getContactTypeId() {
		return contactTypeId;
	}

	public void setContactTypeId(Integer contactTypeId) {
		this.contactTypeId = contactTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
