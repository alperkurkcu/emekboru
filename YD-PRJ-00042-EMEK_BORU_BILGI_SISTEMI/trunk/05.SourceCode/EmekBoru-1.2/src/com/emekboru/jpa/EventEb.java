package com.emekboru.jpa;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.emekboru.jpa.listeners.EventEbListener;



@Entity
@Table(name = "event")
@EntityListeners(value = EventEbListener.class)
public class EventEb implements Serializable{

	private static final long serialVersionUID = 8452253460529081319L;
	
	
	@Id
	@Column(name = "event_id")
	@SequenceGenerator(name = "EVENT_GENERATOR", sequenceName = "EVENT_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "EVENT_GENERATOR")
	private Integer eventId;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "planned_date")
	private Date plannedDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "realization_date")
	private Date realizationDate;
	
	@Column(name = "note")
	private String note;
	
	@Transient
	private double totalCost;
	
	@Transient
	private double estimatedCost;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "event", cascade = CascadeType.ALL)
	private List<EventCostBreakdown> eventCostBreakdowns;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "event", cascade = CascadeType.ALL)
	private List<EmployeeEventParticipation> employeeEventParticipations;
	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "event_type_id", referencedColumnName = "event_type_id")
	private EventTypeEb eventType;
	
	public EventEb(){
		eventType = new EventTypeEb();
	}

	/**
	 * 		GETTERS AND SETTERS
	 */
	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public Date getPlannedDate() {
		return plannedDate;
	}

	public void setPlannedDate(Date plannedDate) {
		this.plannedDate = plannedDate;
	}

	public Date getRealizationDate() {
		return realizationDate;
	}

	public void setRealizationDate(Date realizationDate) {
		this.realizationDate = realizationDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
 
	public List<EventCostBreakdown> getEventCostBreakdowns() {
		return eventCostBreakdowns;
	}

	public void setEventCostBreakdowns(List<EventCostBreakdown> eventCostBreakdowns) {
		this.eventCostBreakdowns = eventCostBreakdowns;
	}

	public List<EmployeeEventParticipation> getEmployeeEventParticipations() {
		return employeeEventParticipations;
	}

	public void setEmployeeEventParticipations(
			List<EmployeeEventParticipation> employeeEventParticipations) {
		this.employeeEventParticipations = employeeEventParticipations;
	}

	public EventTypeEb getEventType() {
		return eventType;
	}

	public void setEventType(EventTypeEb eventType) {
		this.eventType = eventType;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}

	public double getEstimatedCost() {
		return estimatedCost;
	}

	public void setEstimatedCost(double estimatedCost) {
		this.estimatedCost = estimatedCost;
	}

}
