package com.emekboru.jpa.personel;

import java.io.Serializable;

import javax.persistence.*;

import com.emekboru.jpa.Employee;

import java.sql.Timestamp;


/**
 * The persistent class for the personel_kimlik_telefon database table.
 * 
 */
@Entity
@Table(name="personel_kimlik_telefon")
public class PersonelKimlikTelefon implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PERSONEL_KIMLIK_TELEFON_ID_GENERATOR", sequenceName="PERSONEL_KIMLIK_TELEFON_SEQ", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PERSONEL_KIMLIK_TELEFON_ID_GENERATOR")
	@Column(name="ID")
	private Integer id;

	@Column(name="eklenme_tarihi")
	private Timestamp eklenmeTarihi;

//	@Column(name="kimlik_id")
//	private Integer kimlikId;

	@Column(name="kullanici_id_ekleyen")
	private Integer kullaniciIdEkleyen;

	@Column(name="kullanici_id_guncelleyen")
	private Integer kullaniciIdGuncelleyen;

	private String numara;

	@Column(name="son_guncellenme_tarihi")
	private Timestamp sonGuncellenmeTarihi;

	@Column(name="telefon_turu_id")
	private Integer telefonTuruId;

	@Column(name="varsayilan_mi")
	private Boolean varsayilanMi;

	// bi-directional many-to-one association to Employee
	@ManyToOne
	@JoinColumn(name = "kimlik_id", insertable = false)
	private Employee employee;

	public PersonelKimlikTelefon() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getEklenmeTarihi() {
		return this.eklenmeTarihi;
	}

	public void setEklenmeTarihi(Timestamp eklenmeTarihi) {
		this.eklenmeTarihi = eklenmeTarihi;
	}

//	public Integer getKimlikId() {
//		return this.kimlikId;
//	}
//
//	public void setKimlikId(Integer kimlikId) {
//		this.kimlikId = kimlikId;
//	}

	public Integer getKullaniciIdEkleyen() {
		return this.kullaniciIdEkleyen;
	}

	public void setKullaniciIdEkleyen(Integer kullaniciIdEkleyen) {
		this.kullaniciIdEkleyen = kullaniciIdEkleyen;
	}

	public Integer getKullaniciIdGuncelleyen() {
		return this.kullaniciIdGuncelleyen;
	}

	public void setKullaniciIdGuncelleyen(Integer kullaniciIdGuncelleyen) {
		this.kullaniciIdGuncelleyen = kullaniciIdGuncelleyen;
	}

	public String getNumara() {
		return this.numara;
	}

	public void setNumara(String numara) {
		this.numara = numara;
	}

	public Timestamp getSonGuncellenmeTarihi() {
		return this.sonGuncellenmeTarihi;
	}

	public void setSonGuncellenmeTarihi(Timestamp sonGuncellenmeTarihi) {
		this.sonGuncellenmeTarihi = sonGuncellenmeTarihi;
	}

	public Integer getTelefonTuruId() {
		return this.telefonTuruId;
	}

	public void setTelefonTuruId(Integer telefonTuruId) {
		this.telefonTuruId = telefonTuruId;
	}

	public Boolean getVarsayilanMi() {
		if (this.varsayilanMi == null) return false;
		return this.varsayilanMi;
	}

	public void setVarsayilanMi(Boolean varsayilanMi) {
		this.varsayilanMi = varsayilanMi;
	}

	/**
	 * @return the employee
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 * @param employee the employee to set
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}