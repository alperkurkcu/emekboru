package com.emekboru.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.emekboru.config.Test;
import com.emekboru.jpa.sales.order.SalesItem;

/**
 * The persistent class for the kaynak_makrografik_tests_spec database table.
 * 
 */
@NamedQueries({
		@NamedQuery(name = "KMTS.silmeKontrolu", query = "SELECT r.testId FROM KaynakMakroTestSpec r WHERE r.globalId = :prmGlobalId and r.salesItem.itemId=:prmItemId"),
		@NamedQuery(name = "KMTS.seciliKaynakMakroTestTanim", query = "SELECT r FROM KaynakMakroTestSpec r WHERE r.globalId = :prmGlobalId and r.salesItem.itemId=:prmItemId"),
		@NamedQuery(name = "KMTS.findByItemId", query = "SELECT r FROM KaynakMakroTestSpec r WHERE r.salesItem.itemId=:prmItemId") })
@Entity
@Table(name = "kaynak_makrografik_tests_spec")
public class KaynakMakroTestSpec implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final int KAYNAK_MACRO_TEST = 1031;

	@Id
	@SequenceGenerator(name = "KAYNAK_MAKROGRAFIK_TESTS_SPEC_GENERATOR", sequenceName = "KAYNAK_MAKROGRAFIK_TESTS_SPEC_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "KAYNAK_MAKROGRAFIK_TESTS_SPEC_GENERATOR")
	@Column(name = "test_id")
	private Integer testId;

	@Column(name = "dis_genislik")
	private Float disGenislik;

	@Column(name = "dis_yukseklik")
	private Float disYukseklik;

	@Column(name = "explanation")
	private String explanation;

	@Column(name = "global_id")
	private Integer globalId;

	@Column(name = "ic_genislik")
	private Float icGenislik;

	@Column(name = "ic_yukseklik")
	private Float icYukseklik;

	@Column(name = "kaynak_kacikligi")
	private Float kaynakKacikligi;

	@Column(name = "name")
	private String name;

	@Column(name = "code")
	private String code;

	@Column(name = "penetrasyon")
	private Float penetrasyon;

	// @JoinColumn(name = "order_id", referencedColumnName = "order_id",
	// insertable = false)
	// @ManyToOne(fetch = FetchType.LAZY)
	// private Order order;
	//
	// @ManyToOne
	// @JoinColumns({
	// @JoinColumn(name = "order_id", referencedColumnName = "order_id",
	// insertable = false, updatable = false),
	// @JoinColumn(name = "global_id", referencedColumnName = "global_id",
	// insertable = false, updatable = false) })
	// private DestructiveTestsSpec mainDestructiveTestSpecs;

	// entegrasyon
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_item_id", referencedColumnName = "item_id", insertable = false)
	private SalesItem salesItem;

	@ManyToOne
	@JoinColumns({
			@JoinColumn(name = "sales_item_id", referencedColumnName = "sales_item_id", insertable = false, updatable = false),
			@JoinColumn(name = "global_id", referencedColumnName = "global_id", insertable = false, updatable = false) })
	private DestructiveTestsSpec salesDestructiveTestSpecs;

	// entegrasyon

	public KaynakMakroTestSpec() {

	}

	public KaynakMakroTestSpec(Test test) {

		globalId = test.getGlobalId();
		name = test.getDisplayName();
		code = test.getCode();
	}

	public void reset() {

		testId = 0;
		explanation = "";
		// disGenislik = (float) 0.0;
		disGenislik = null;
		disYukseklik = null;
		icGenislik = null;
		icYukseklik = null;
		kaynakKacikligi = null;
		penetrasyon = null;
	}

	// GETTERS AND SETTERS
	public Integer getTestId() {
		return this.testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	public String getExplanation() {
		return this.explanation;
	}

	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}

	public Integer getGlobalId() {
		return this.globalId;
	}

	public void setGlobalId(Integer globalId) {
		this.globalId = globalId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public DestructiveTestsSpec getSalesDestructiveTestSpecs() {
		return salesDestructiveTestSpecs;
	}

	public void setSalesDestructiveTestSpecs(
			DestructiveTestsSpec salesDestructiveTestSpecs) {
		this.salesDestructiveTestSpecs = salesDestructiveTestSpecs;
	}

	public Float getDisGenislik() {
		return disGenislik;
	}

	public void setDisGenislik(Float disGenislik) {
		this.disGenislik = disGenislik;
	}

	public Float getDisYukseklik() {
		return disYukseklik;
	}

	public void setDisYukseklik(Float disYukseklik) {
		this.disYukseklik = disYukseklik;
	}

	public Float getIcGenislik() {
		return icGenislik;
	}

	public void setIcGenislik(Float icGenislik) {
		this.icGenislik = icGenislik;
	}

	public Float getIcYukseklik() {
		return icYukseklik;
	}

	public void setIcYukseklik(Float icYukseklik) {
		this.icYukseklik = icYukseklik;
	}

	public Float getKaynakKacikligi() {
		return kaynakKacikligi;
	}

	public void setKaynakKacikligi(Float kaynakKacikligi) {
		this.kaynakKacikligi = kaynakKacikligi;
	}

	public Float getPenetrasyon() {
		return penetrasyon;
	}

	public void setPenetrasyon(Float penetrasyon) {
		this.penetrasyon = penetrasyon;
	}

}