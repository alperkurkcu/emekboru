package com.emekboru.jpa.sales.documents;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.emekboru.jpa.Employee;

@Entity
@Table(name = "sales_document")
public class SalesDocument implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8384087290421826868L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_DOCUMENT_GENERATOR")
	@SequenceGenerator(name = "SALES_DOCUMENT_GENERATOR", sequenceName = "SALES_DOCUMENT_SEQUENCE", allocationSize = 1)
	@Column(name = "document_id")
	private int documentId;

	@Column(name = "title")
	private String title;

	@Column(name = "document_name")
	private String name;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "document_type", referencedColumnName = "type_id")
	private SalesDocumentType documentType;

	@Temporal(TemporalType.DATE)
	@Column(name = "start_date")
	private Date startDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "end_date")
	private Date endDate;

	@Column(name = "document_language")
	private String language;

	/* JOIN FUNCTIONS */
	@Transient
	private List<Employee> responsibleList;

	/* end of JOIN FUNCTIONS */

	/* FILE UPLOAD-DOWNLOAD FUNCTIONS */
	@Transient
	private List<String> fileNames;

	@Transient
	private int fileNumber;

	/* end of FILE UPLOAD-DOWNLOAD FUNCTIONS */

	@Override
	public boolean equals(Object o) {
		return this.getDocumentId() == ((SalesDocument) o).getDocumentId();
	}

	public SalesDocument() {
		title = "-";
		fileNumber = 0;
		responsibleList = new ArrayList<Employee>();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public int getDocumentId() {
		return documentId;
	}

	public String getTitle() {
		return title;
	}

	public String getName() {
		return name;
	}

	public String getLanguage() {
		return language;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public SalesDocumentType getDocumentType() {
		return documentType;
	}

	public List<Employee> getResponsibleList() {
		return responsibleList;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public void setDocumentType(SalesDocumentType documentType) {
		this.documentType = documentType;
	}

	public void setResponsibleList(List<Employee> responsibleList) {
		this.responsibleList = responsibleList;
	}

	public List<String> getFileNames() {
		try {
			String[] names = name.split("//");
			fileNames = new ArrayList<String>();

			for (int i = 1; i < names.length; i++) {
				fileNames.add(names[i]);
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return fileNames;
	}

	public void setFileNames(List<String> fileNames) {
		this.fileNames = fileNames;
	}

	public int getFileNumber() {
		try {
			String[] names = name.split("//");

			fileNumber = names.length - 1;
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return fileNumber;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
