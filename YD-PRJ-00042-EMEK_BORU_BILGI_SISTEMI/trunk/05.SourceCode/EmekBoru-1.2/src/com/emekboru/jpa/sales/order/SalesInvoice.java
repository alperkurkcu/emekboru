package com.emekboru.jpa.sales.order;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "sales_invoice")
public class SalesInvoice implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5040315332558833256L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SALES_INVOICE_GENERATOR")
	@SequenceGenerator(name = "SALES_INVOICE_GENERATOR", sequenceName = "SALES_INVOICE_SEQUENCE", allocationSize = 1)
	@Column(name = "invoice_id")
	private int invoiceId;

	@Column(name = "invoice_no")
	private String invoiceNo;

	@Temporal(TemporalType.TIME)
	@Column(name = "invoice_date")
	private Date invoiceDate;

	@Column(name = "total_amount")
	private int totalAmount;

	@Column(name = "paid")
	private int paid;

	@Column(name = "rest")
	private int rest;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "order_no", referencedColumnName = "order_id")
	private SalesOrder order;

	public SalesInvoice() {
	}
	/*
	 * GETTERS AND SETTERS
	 */

	public int getInvoiceId() {
		return invoiceId;
	}

	public SalesOrder getOrder() {
		return order;
	}

	public void setOrder(SalesOrder orderNo) {
		this.order = orderNo;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public int getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(int totalAmount) {
		this.totalAmount = totalAmount;
	}

	public int getPaid() {
		return paid;
	}

	public void setPaid(int paid) {
		this.paid = paid;
	}

	public int getRest() {
		return rest;
	}

	public void setRest(int rest) {
		this.rest = rest;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
