package com.emekboru.jpa.personel;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the personel_egitim_bilgileri database table.
 * 
 */
@Entity
@Table(name = "personel_egitim_bilgileri")
public class PersonelEgitimBilgileri implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "PERSONEL_EGITIM_BILGILERI_ID_GENERATOR", sequenceName = "PERSONEL_EGITIM_BILGILERI_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PERSONEL_EGITIM_BILGILERI_ID_GENERATOR")
	@Column(name = "ID")
	private Integer id;
	@Column(name = "aciklama")
	private String aciklama;

	private String bolum;

	@Column(name = "eklenme_tarihi")
	private Timestamp eklenmeTarihi;

	@Column(name = "kimlik_id")
	private Integer kimlikId;

	@Column(name = "kullanici_id_ekleyen")
	private Integer kullaniciIdEkleyen;

	@Column(name = "kullanici_id_guncelleyen")
	private Integer kullaniciIdGuncelleyen;

	@Column(name = "mevzun_oldugu_okul")
	private String mevzunOlduguOkul;

	@Column(name = "mevzuniyet_yili")
	private Integer mevzuniyetYili;

	// @Column(name="ogrenim_durumu_id")
	// private Integer ogrenimDurumuId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ogrenim_durumu_id", referencedColumnName = "ID")
	private PersonelOrtakOgrenimDurumu personelOrtakOgrenimDurumu;

	@Column(name = "son_guncellenme_tarihi")
	private Timestamp sonGuncellenmeTarihi;

	public PersonelEgitimBilgileri() {

		personelOrtakOgrenimDurumu = new PersonelOrtakOgrenimDurumu();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAciklama() {
		return this.aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public String getBolum() {
		return this.bolum;
	}

	public void setBolum(String bolum) {
		this.bolum = bolum;
	}

	public Timestamp getEklenmeTarihi() {
		return this.eklenmeTarihi;
	}

	public void setEklenmeTarihi(Timestamp eklenmeTarihi) {
		this.eklenmeTarihi = eklenmeTarihi;
	}

	public Integer getKimlikId() {
		return this.kimlikId;
	}

	public void setKimlikId(Integer kimlikId) {
		this.kimlikId = kimlikId;
	}

	public Integer getKullaniciIdEkleyen() {
		return this.kullaniciIdEkleyen;
	}

	public void setKullaniciIdEkleyen(Integer kullaniciIdEkleyen) {
		this.kullaniciIdEkleyen = kullaniciIdEkleyen;
	}

	public Integer getKullaniciIdGuncelleyen() {
		return this.kullaniciIdGuncelleyen;
	}

	public void setKullaniciIdGuncelleyen(Integer kullaniciIdGuncelleyen) {
		this.kullaniciIdGuncelleyen = kullaniciIdGuncelleyen;
	}

	public String getMevzunOlduguOkul() {
		return this.mevzunOlduguOkul;
	}

	public void setMevzunOlduguOkul(String mevzunOlduguOkul) {
		this.mevzunOlduguOkul = mevzunOlduguOkul;
	}

	public Integer getMevzuniyetYili() {
		return this.mevzuniyetYili;
	}

	public void setMevzuniyetYili(Integer mevzuniyetYili) {
		this.mevzuniyetYili = mevzuniyetYili;
	}

	// public Integer getOgrenimDurumuId() {
	// return this.ogrenimDurumuId;
	// }
	//
	// public void setOgrenimDurumuId(Integer ogrenimDurumuId) {
	// this.ogrenimDurumuId = ogrenimDurumuId;
	// }

	public Timestamp getSonGuncellenmeTarihi() {
		return this.sonGuncellenmeTarihi;
	}

	public void setSonGuncellenmeTarihi(Timestamp sonGuncellenmeTarihi) {
		this.sonGuncellenmeTarihi = sonGuncellenmeTarihi;
	}

	/**
	 * @return the personelOrtakOgrenimDurumu
	 */
	public PersonelOrtakOgrenimDurumu getPersonelOrtakOgrenimDurumu() {
		return personelOrtakOgrenimDurumu;
	}

	/**
	 * @param personelOrtakOgrenimDurumu
	 *            the personelOrtakOgrenimDurumu to set
	 */
	public void setPersonelOrtakOgrenimDurumu(
			PersonelOrtakOgrenimDurumu personelOrtakOgrenimDurumu) {
		this.personelOrtakOgrenimDurumu = personelOrtakOgrenimDurumu;
	}

}