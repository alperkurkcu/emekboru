package com.emekboru.jpaman;

import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.emekboru.jpa.LkYetkiRol;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.YetkiMenu;
import com.emekboru.jpa.YetkiMenuKullaniciErisim;
import com.emekboru.utils.YetkiTreeNode;
import com.emekboru.utils.YetkiTreeNodeType;

public class YetkiMenuKullaniciErisimManager extends
		CommonQueries<YetkiMenuKullaniciErisim> {

	/*
	 * public static List<YetkiMenuKullaniciErisim> findAllUserYetkiMenuErisim()
	 * { return findAll(YetkiMenuKullaniciErisim.class); }
	 */

	public static HashMap<Integer, YetkiMenuKullaniciErisim> findUserYetkiMenuErisim(
			Integer userId) {
		HashMap<Integer, YetkiMenuKullaniciErisim> menuErisimHashMap = new HashMap<Integer, YetkiMenuKullaniciErisim>();
		List<YetkiMenuKullaniciErisim> kullaniciErisimList = getYetkiMenuKullaniciErisimByUserId(userId);
		HashMap<Integer, YetkiMenu> sayfaMap = new HashMap<Integer, YetkiMenu>();
		for (int i = 0; i < kullaniciErisimList.size(); ++i) {
			if (!sayfaMap.containsKey(kullaniciErisimList.get(i).getYetkiMenu()
					.getId())) {
				menuErisimHashMap.put(kullaniciErisimList.get(i).getId(),
						kullaniciErisimList.get(i));
				sayfaMap.put(kullaniciErisimList.get(i).getYetkiMenu().getId(),
						kullaniciErisimList.get(i).getYetkiMenu());
			}
		}
		return menuErisimHashMap;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List<YetkiMenuKullaniciErisim> getAllYetkiMenuKullaniciErisimList() {
		List result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("YetkiMenuKullaniciErisim.findAll")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	// @SuppressWarnings({ "rawtypes", "unchecked" })
	// public static List<YetkiMenuKullaniciErisim>
	// getYetkiMenuKullaniciErisimByUserId(
	// Integer id) {
	// List result = null;
	// EntityManager em = Factory.getEntityManager();
	// try {
	// result = em
	// .createNamedQuery("YetkiMenuKullaniciErisim.findByUserId")
	// .setParameter("prmUserId", id).getResultList();
	// } catch (Exception ex) {
	// ex.printStackTrace();
	// }
	// return result;
	// }

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List<YetkiMenuKullaniciErisim> getYetkiMenuKullaniciErisimByUserId(
			Integer id) {
		List result = null;
		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();

		if (tx.isActive()) {
			manager.close();
		}

		try {
			tx.begin();
			result = manager
					.createNamedQuery("YetkiMenuKullaniciErisim.findByUserId")
					.setParameter("prmUserId", id).getResultList();
			tx.commit();
			manager.close();
			return result;
		} catch (Exception e) {

			System.err
					.println("yetkiMenuKullaniciErisimManager.getYetkiMenuKullaniciErisimByUserId-HATA");
			e.printStackTrace();
			manager.flush();
			tx.rollback();
			return result;
		}
	}

	public static YetkiTreeNode getAllYetkiMenuKullaniciErisimTree(
			boolean userSelectable, boolean pageSelectable,
			boolean roleSelectable) {
		UserManager userManager = new UserManager();
		// List<SystemUser> usersList = userManager.findAll(SystemUser.class);
		List<SystemUser> usersList = userManager.findAllOrdered(
				SystemUser.class, "username");
		YetkiTreeNode root = new YetkiTreeNode("Root", null);
		YetkiTreeNode userNode = null;
		YetkiTreeNode yetkiNode = null;
		YetkiTreeNode roleNode = null;
		List<YetkiMenuKullaniciErisim> yetkiListesi = null;
		List<LkYetkiRol> menuRolList = null;
		HashMap<Integer, Integer> userMenuMap = new HashMap<Integer, Integer>();
		for (SystemUser systemUser : usersList) {
			userNode = new YetkiTreeNode(systemUser.getEmployee()
					.getFirstname()
					+ " "
					+ systemUser.getEmployee().getLastname(), root);
			userNode.setObjectId(systemUser.getId());
			userNode.setObjectType(YetkiTreeNodeType.USER);
			userNode.setSelectable(userSelectable);
			yetkiListesi = YetkiMenuKullaniciErisimManager
					.getYetkiMenuKullaniciErisimByUserId(systemUser.getId());
			userMenuMap = addUserMenusToMap(yetkiListesi);
			for (int pageId = 0; pageId < yetkiListesi.size(); pageId++) {
				if (userMenuMap.containsKey(yetkiListesi.get(pageId)
						.getYetkiMenu().getId())) {
					yetkiNode = new YetkiTreeNode(yetkiListesi.get(pageId)
							.getYetkiMenu().getEkranTanimi(), userNode);
					yetkiNode.setObjectId(yetkiListesi.get(pageId)
							.getYetkiMenu().getId());
					yetkiNode.setObjectType(YetkiTreeNodeType.PAGE);
					yetkiNode.setSelectable(pageSelectable);
					// yetkiNode.setSelectable(false);
					menuRolList = LkYetkiRolManager
							.getRoleByMenuId(yetkiListesi.get(pageId)
									.getYetkiMenu().getId());
					for (int rolId = 0; rolId < menuRolList.size(); rolId++) {
						roleNode = new YetkiTreeNode(menuRolList.get(rolId)
								.getAd(), yetkiNode);
						roleNode.setObjectId(menuRolList.get(rolId).getId());
						roleNode.setObjectType(YetkiTreeNodeType.ROLE);
						roleNode.setSelectable(roleSelectable);
						// roleNode.setSelectable(false);
						roleNode.setUserId(systemUser.getId());
						roleNode.setPageId(yetkiListesi.get(pageId)
								.getYetkiMenu().getId());
						roleNode.setRoleId(menuRolList.get(rolId).getId());
					}
					userMenuMap.remove(yetkiListesi.get(pageId).getYetkiMenu()
							.getId());
				}
			}
		}
		return root;
	}

	private static HashMap<Integer, Integer> addUserMenusToMap(
			List<YetkiMenuKullaniciErisim> yetkiListesi) {
		HashMap<Integer, Integer> userMenuMap = new HashMap<Integer, Integer>();

		for (int i = 0; i < yetkiListesi.size(); i++) {
			Integer menuId = yetkiListesi.get(i).getYetkiMenu().getId();
			if (!userMenuMap.containsKey(menuId)) {
				userMenuMap.put(menuId, 1);
			}
		}

		return userMenuMap;
	}

	@SuppressWarnings("unused")
	private static boolean isNodeExists(YetkiTreeNode userNode, Integer childId) {

		for (int i = 0; i < userNode.getChildCount(); i++) {
			YetkiTreeNode node = (YetkiTreeNode) userNode.getChildren().get(i);
			if (node.getObjectId() == childId)
				return true;
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public static List<YetkiMenuKullaniciErisim> getDefinedUserMenuRolesByUserIdAndMenuId(
			Integer userId, Integer menuId) {
		List<YetkiMenuKullaniciErisim> result = null;

		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"YetkiMenuKullaniciErisim.findUserMenuRolesByUserIdAndMenuId")
					.setParameter("prmUserId", userId)
					.setParameter("prmMenuId", menuId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	public static void addOrUpdateYetkiMenuKullaniciErisim(
			YetkiMenuKullaniciErisim kullaniciErisim) {
		EntityManager em = getEntityManager();

		em.getTransaction().begin();
		if (kullaniciErisim.getId() != null) {
			em.merge(kullaniciErisim);
		} else {
			em.persist(kullaniciErisim);

		}
		em.getTransaction().commit();
	}

	public static void addOrUpdateYetkiMenuKullaniciErisim(
			List<YetkiMenuKullaniciErisim> kullaniciErisimList) {
		EntityManager em = getEntityManager();
		em.getTransaction().begin();
		em.persist(kullaniciErisimList);
		em.getTransaction().commit();
	}

	private static EntityManager getEntityManager() {
		return Factory.getEntityManager();
	}

	public static void removeUserRole(Integer userId, Integer pageId,
			Integer roleId) {

		EntityManager em = Factory.getEntityManager();
		YetkiMenuKullaniciErisim userMenuKullaniciErisim = null;
		em.getTransaction().begin();

		try {
			userMenuKullaniciErisim = (YetkiMenuKullaniciErisim) em
					.createNamedQuery(
							"YetkiMenuKullaniciErisim.findUserMenuRolesByUserIdAndMenuIdANdRoleId")
					.setParameter("prmUserId", userId)
					.setParameter("prmMenuId", pageId)
					.setParameter("prmRoleId", roleId).getSingleResult();

		} catch (Exception ex) {
			ex.printStackTrace();
			em.getTransaction().commit();

		}

		if (userMenuKullaniciErisim != null) {
			em.remove(userMenuKullaniciErisim);
		}
		em.getTransaction().commit();

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List<YetkiMenuKullaniciErisim> getYetkiMenuKullaniciErisimByMenuId(
			Integer menuId) {
		List result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("YetkiMenuKullaniciErisim.findByMenuId")
					.setParameter("prmYetkiMenuId", menuId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
