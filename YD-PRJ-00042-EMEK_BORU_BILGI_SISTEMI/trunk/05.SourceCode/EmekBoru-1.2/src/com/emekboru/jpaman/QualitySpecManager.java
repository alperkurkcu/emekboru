package com.emekboru.jpaman;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.QualitySpec;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.utils.IsolationType;

public class QualitySpecManager extends CommonQueries<QualitySpec> {
	/*
	 * @SuppressWarnings("unchecked") public List<QualitySpec>
	 * seciliQualitySpec(Integer orderId) {
	 * 
	 * List<QualitySpec> result = new ArrayList<QualitySpec>();
	 * 
	 * EntityManager manager = Factory.getInstance().createEntityManager();
	 * manager.getEntityManagerFactory().getCache().evictAll();
	 * manager.getTransaction().begin(); Query findQuerie =
	 * manager.createNamedQuery("QS.seciliQualitySpec");
	 * findQuerie.setParameter("prmOrderId", orderId); result =
	 * findQuerie.getResultList(); manager.getTransaction().commit();
	 * manager.close(); return result;
	 * 
	 * }
	 * 
	 * @SuppressWarnings({ "unchecked" }) public static
	 * List<IsolationTestDefinition> getAllInternalIsolationTestDefinitions(
	 * Order order) {
	 * 
	 * List<IsolationTestDefinition> result = null; EntityManager em =
	 * Factory.getEntityManager(); try { result = em .createNamedQuery(
	 * "IsolationTestDefinition.findIsolationByOrderIdAndIsolationType")
	 * .setParameter(1, IsolationType.IC_KAPLAMA) .setParameter(2,
	 * order).getResultList();
	 * 
	 * } catch (Exception ex) { ex.printStackTrace(); } return result; }
	 * 
	 * 
	 * @SuppressWarnings({ "unchecked" }) public static
	 * List<IsolationTestDefinition> getAllExternalIsolationTestDefinitions(
	 * Order order) {
	 * 
	 * List<IsolationTestDefinition> result = null; EntityManager em =
	 * Factory.getEntityManager(); try { result = em .createNamedQuery(
	 * "IsolationTestDefinition.findIsolationByOrderIdAndIsolationType")
	 * .setParameter(1, IsolationType.DIS_KAPLAMA) .setParameter(2,
	 * order).getResultList();
	 * 
	 * } catch (Exception ex) { ex.printStackTrace(); } return result; }
	 */

	@SuppressWarnings("unchecked")
	public List<QualitySpec> seciliQualitySpec(Integer salesItemId) {

		List<QualitySpec> result = new ArrayList<QualitySpec>();

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query findQuerie = manager.createNamedQuery("QS.seciliQualitySpec");
		findQuerie.setParameter("prmItemId", salesItemId);
		result = findQuerie.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return result;

	}

	@SuppressWarnings({ "unchecked" })
	public static List<IsolationTestDefinition> getAllInternalIsolationTestDefinitions(
			SalesItem salesItem) {

		List<IsolationTestDefinition> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"IsolationTestDefinition.findIsolationByItemIdAndIsolationType")
					.setParameter(1, IsolationType.IC_KAPLAMA)
					.setParameter(2, salesItem).getResultList();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings({ "unchecked" })
	public static List<IsolationTestDefinition> getAllExternalIsolationTestDefinitions(
			SalesItem salesItem) {

		List<IsolationTestDefinition> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"IsolationTestDefinition.findIsolationByItemIdAndIsolationType")
					.setParameter(1, IsolationType.DIS_KAPLAMA)
					.setParameter(2, salesItem).getResultList();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
