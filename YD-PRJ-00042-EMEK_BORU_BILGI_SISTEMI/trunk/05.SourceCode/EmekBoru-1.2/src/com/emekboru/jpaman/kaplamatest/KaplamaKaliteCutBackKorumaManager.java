/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatestspecs.KaplamaKaliteCutBackKoruma;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KaplamaKaliteCutBackKorumaManager extends
		CommonQueries<KaplamaKaliteCutBackKoruma> {

	@SuppressWarnings("unchecked")
	public static List<KaplamaKaliteCutBackKoruma> getAllKaplamaKaliteCutBackKoruma(
			Integer itemId) {

		List<KaplamaKaliteCutBackKoruma> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("KaplamaKaliteCutBackKoruma.findAll")
					.setParameter("prmItemId", itemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
