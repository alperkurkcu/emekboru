/**
 * 
 */
package com.emekboru.jpaman.istakipformu;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.istakipformu.IsTakipFormuPolietilenKaplama;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class IsTakipFormuPolietilenKaplamaManager extends
		CommonQueries<IsTakipFormuPolietilenKaplama> {

	@SuppressWarnings("unchecked")
	public static List<IsTakipFormuPolietilenKaplama> getAllIsTakipFormuPolietilenKaplama(
			Integer itemId) {

		List<IsTakipFormuPolietilenKaplama> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("IsTakipFormuPolietilenKaplama.findAll")
					.setParameter("prmItemId", itemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;

	}
}
