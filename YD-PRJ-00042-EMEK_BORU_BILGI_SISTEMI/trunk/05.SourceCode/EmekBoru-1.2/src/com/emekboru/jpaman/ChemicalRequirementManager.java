package com.emekboru.jpaman;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.ChemicalRequirement;

public class ChemicalRequirementManager extends
		CommonQueries<ChemicalRequirement> {

	// itemId sipari� kalemidir.
	public Integer kontrol(Integer itemId) {
		Integer count = 0;

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query findQuerie = manager.createNamedQuery("CR.kontrol");
		findQuerie.setParameter("prmItemId", itemId);
		// testId yi count a esitleyip hem testId cekm�s oluyorum hemde
		// i�erisinde item var m� yok mu kontrol edyorum
		if (findQuerie.getResultList().isEmpty()) {
			return count;
		} else {
			Number result = (Number) findQuerie.getSingleResult();
			if (result.equals(null)) {

			} else {
				count = result.intValue();
				if (count.equals(null)) {
					count = 0;
				}
			}
		}
		manager.getTransaction().commit();
		manager.close();
		return count;
	}

	@SuppressWarnings("unchecked")
	public List<ChemicalRequirement> seciliChemicalRequirementTanim(
			Integer prmItemId) {

		List<ChemicalRequirement> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("CR.seciliChemicalRequirementTanim")
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
