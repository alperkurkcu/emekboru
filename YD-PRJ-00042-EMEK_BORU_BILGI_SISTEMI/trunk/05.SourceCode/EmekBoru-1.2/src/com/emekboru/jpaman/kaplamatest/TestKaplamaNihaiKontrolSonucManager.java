package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaNihaiKontrolSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class TestKaplamaNihaiKontrolSonucManager extends
		CommonQueries<TestKaplamaNihaiKontrolSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaNihaiKontrolSonuc> getAllTestKaplamaNihaiKontrolSonuc(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaNihaiKontrolSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("TestKaplamaNihaiKontrolSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
