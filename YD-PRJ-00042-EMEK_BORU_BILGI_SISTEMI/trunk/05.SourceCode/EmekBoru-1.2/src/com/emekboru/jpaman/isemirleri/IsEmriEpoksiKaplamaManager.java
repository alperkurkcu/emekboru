package com.emekboru.jpaman.isemirleri;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.isemri.IsEmriEpoksiKaplama;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * 
 */

/**
 * @author Alper K
 * 
 */
public class IsEmriEpoksiKaplamaManager extends
		CommonQueries<IsEmriEpoksiKaplama> {

	@SuppressWarnings("unchecked")
	public static List<IsEmriEpoksiKaplama> getAllIsEmriEpoksiKaplama(
			Integer prmItemId) {

		List<IsEmriEpoksiKaplama> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("IsEmriEpoksiKaplama.findAllByItemId")
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
