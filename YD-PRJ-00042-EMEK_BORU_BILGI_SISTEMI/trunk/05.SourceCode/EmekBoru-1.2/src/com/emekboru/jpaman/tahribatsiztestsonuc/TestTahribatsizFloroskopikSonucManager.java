/**
 * 
 */
package com.emekboru.jpaman.tahribatsiztestsonuc;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizFloroskopikSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
public class TestTahribatsizFloroskopikSonucManager extends
		CommonQueries<TestTahribatsizFloroskopikSonuc> {

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizFloroskopikSonuc> getAllTestTahribatsizFloroskopikSonuc(
			Integer pipeId) {

		List<TestTahribatsizFloroskopikSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("TestTahribatsizFloroskopikSonuc.findAll")
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizFloroskopikSonuc> findByKoordinatsPipeId(
			Integer koordinatQ, Integer koordinatL, Integer pipeId) {

		List<TestTahribatsizFloroskopikSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestTahribatsizFloroskopikSonuc.findByKoordinatsPipeId")
					.setParameter("prmPipeId", pipeId)
					.setParameter("prmQ", koordinatQ)
					.setParameter("prmL", koordinatL).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizFloroskopikSonuc> dunUretilmisBorularIcin(
			Integer prmMachineId) {

		List<TestTahribatsizFloroskopikSonuc> result = null;
		EntityManager em = Factory.getEntityManager();

		String tarihIlk = UtilInsCore.getYesterday().toString()
				.substring(0, 10)
				+ " 08:00:00";
		String tarihSon = UtilInsCore.getTarihZaman().toString()
				.substring(0, 10)
				+ " 07:59:59";

		Timestamp tsIlk = Timestamp.valueOf(tarihIlk);
		Timestamp tsSon = Timestamp.valueOf(tarihSon);

		try {
			String query = "SELECT o FROM TestTahribatsizFloroskopikSonuc mel WHERE mel.pipe.pipeId in (select o.pipe.pipeId from MachinePipeLink o where o.pipe.eklemeZamani BETWEEN :prmDate1 AND :prmDate2 and o.machine.machineId=:prmMachineId) order by mel.pipe.pipeId";
			Query index = em.createQuery(query);
			index.setParameter("prmDate1", tsIlk)
					.setParameter("prmDate2", tsSon)
					.setParameter("prmMachineId", prmMachineId);
			result = index.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizFloroskopikSonuc> tumUretilmisBorularIcin(
			Integer prmSalesItemId, Integer prmMachineId) {

		List<TestTahribatsizFloroskopikSonuc> result = null;
		EntityManager em = Factory.getEntityManager();

		try {
			String query = "SELECT mel FROM TestTahribatsizFloroskopikSonuc mel WHERE mel.pipe.pipeId in (select o.pipe.pipeId from MachinePipeLink o where o.machine.machineId=:prmMachineId and o.pipe.salesItem.itemId=:prmSalesItemId) order by mel.pipe.pipeId";
			Query index = em.createQuery(query);
			index.setParameter("prmSalesItemId", prmSalesItemId).setParameter(
					"prmMachineId", prmMachineId);
			result = index.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizFloroskopikSonuc> findBetweenTwoParameters(
			String prmFirstBarcode, String prmSecondBarcode) {

		List<TestTahribatsizFloroskopikSonuc> result = null;
		EntityManager em = Factory.getEntityManager();

		try {
			try {
				String query = "SELECT r FROM TestTahribatsizFloroskopikSonuc r WHERE r.pipe.pipeId in (select p.pipeId from Pipe p where p.pipeBarkodNo BETWEEN :prmFirstBarcode AND :prmSecondBarcode ) order by r.pipe.pipeId ASC";
				result = em.createQuery(query)
						.setParameter("prmFirstBarcode", prmFirstBarcode)
						.setParameter("prmSecondBarcode", prmSecondBarcode)
						.getResultList();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			return result;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizFloroskopikSonuc> getByItemDate(
			Integer prmItemId, Date prmDateA, Date prmDateB) {

		List<TestTahribatsizFloroskopikSonuc> result = null;

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();

		final String TIME_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
		final SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT);

		try {
			String query = new String();
			query = "SELECT r FROM TestTahribatsizFloroskopikSonuc r WHERE r.pipe.pipeId in (select p.pipeId from Pipe p where p.salesItem.itemId=:prmItemId)";

			query = query + " AND r.eklemeZamani BETWEEN '"
					+ sdf.format(prmDateA) + "' AND '" + sdf.format(prmDateB)
					+ "'";

			query = query + " OR r.guncellemeZamani BETWEEN '"
					+ sdf.format(prmDateA) + "' AND '" + sdf.format(prmDateB)
					+ "'";

			query = query + " ORDER BY r.pipe.pipeId ASC, r.koordinatQ ASC";

			System.out.println("final query: " + query);

			Query findQuery = manager.createQuery(query).setParameter(
					"prmItemId", prmItemId);
			result = findQuery.getResultList();

			manager.getTransaction().commit();
			manager.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (result.size() == 0)
			return null;
		return result;
	}
}
