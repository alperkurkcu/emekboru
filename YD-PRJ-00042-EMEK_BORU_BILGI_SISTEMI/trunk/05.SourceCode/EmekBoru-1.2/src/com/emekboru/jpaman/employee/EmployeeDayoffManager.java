/**
 * 
 */
package com.emekboru.jpaman.employee;

import java.util.List;

import javax.faces.bean.ManagedProperty;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.Employee;
import com.emekboru.jpa.employee.EmployeeDayoff;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author kursat
 * 
 */
public class EmployeeDayoffManager extends CommonQueries<EmployeeDayoff> {

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userCredentialsBean;

	public EmployeeDayoffManager() {
		super();
		userCredentialsBean = new UserCredentialsBean();
	}

	public List<EmployeeDayoff> findAll() {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String name = EmployeeDayoff.class.getName();
		String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
		String query = "SELECT c FROM " + simpleClassName
				+ " c order by c.id DESC";
		Query findQuery = manager.createQuery(query);

		@SuppressWarnings("unchecked")
		List<EmployeeDayoff> returnedList = findQuery.getResultList();

		tx.commit();
		manager.close();
		return returnedList;
	}

	public List<EmployeeDayoff> findAllByEmployee(Employee employee) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String name = EmployeeDayoff.class.getName();
		String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
		String query = "SELECT c FROM " + simpleClassName
				+ " c WHERE c.active = true AND c.employee.employeeId = "
				+ employee.getEmployeeId() + "order by c.id DESC";
		Query findQuery = manager.createQuery(query);

		@SuppressWarnings("unchecked")
		List<EmployeeDayoff> returnedList = findQuery.getResultList();

		tx.commit();
		manager.close();
		return returnedList;
	}

	public List<EmployeeDayoff> findAllBySupervisors(Employee employee) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String name = EmployeeDayoff.class.getName();
		String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
		String query = "SELECT c FROM " + simpleClassName
				+ " c WHERE c.active = true AND c.supervisor.employeeId = "
				+ employee.getEmployeeId()
				+ " OR c.secondSupervisor.employeeId = "
				+ employee.getEmployeeId() + "order by c.id DESC";
		Query findQuery = manager.createQuery(query);

		@SuppressWarnings("unchecked")
		List<EmployeeDayoff> returnedList = findQuery.getResultList();

		tx.commit();
		manager.close();
		return returnedList;
	}

	public List<EmployeeDayoff> findAllByBM(Employee employee) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String name = EmployeeDayoff.class.getName();
		String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
		String query = "SELECT c FROM "
				+ simpleClassName
				+ " c WHERE c.active = true and c.employee.department.departmentManagerEmployee.employeeId = "
				+ employee.getEmployeeId() + "order by c.id DESC";
		Query findQuery = manager.createQuery(query);

		@SuppressWarnings("unchecked")
		List<EmployeeDayoff> returnedList = findQuery.getResultList();

		tx.commit();
		manager.close();
		return returnedList;
	}
}
