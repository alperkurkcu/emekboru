package com.emekboru.jpaman.employee;

import com.emekboru.jpa.employee.EmployeePhoneNumber;
import com.emekboru.jpaman.CommonQueries;

public class EmployeePhoneNumberManager extends
		CommonQueries<EmployeePhoneNumber> {
	
	/* (non-Javadoc)
	 * @see com.emekboru.jpaman.CommonQueries#enterNew(java.lang.Object)
	 */
	@Override
	public boolean enterNew(EmployeePhoneNumber object) {
		object.setIsDefault(false);
		return super.enterNew(object);
	}

}
