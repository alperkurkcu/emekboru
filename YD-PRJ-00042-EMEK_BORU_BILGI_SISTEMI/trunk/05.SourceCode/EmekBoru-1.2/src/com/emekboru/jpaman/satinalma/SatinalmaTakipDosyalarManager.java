/**
 * 
 */
package com.emekboru.jpaman.satinalma;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.satinalma.SatinalmaTakipDosyalar;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class SatinalmaTakipDosyalarManager extends
		CommonQueries<SatinalmaTakipDosyalar> {

	@SuppressWarnings("unchecked")
	public List<SatinalmaTakipDosyalar> getAllSatinalmaTakipDosyalar() {

		List<SatinalmaTakipDosyalar> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("SatinalmaTakipDosyalar.findAll")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaTakipDosyalar> getPersonalSatinalmaTakipDosyalar(
			Integer prmUserId) {

		List<SatinalmaTakipDosyalar> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("SatinalmaTakipDosyalar.findAllByUserId")
					.setParameter("prmUserId", prmUserId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings({ "unused", "unchecked" })
	public String getOtomatikSayi(Integer prmEmployeeId) {

		List<SatinalmaTakipDosyalar> result = null;
		Integer sayi = null;
		String sonuc = null;
		String sonSayi = null;
		Integer dateYear1 = null;
		Integer dateYear2 = null;

		DateFormat dateFormat = new SimpleDateFormat("yyyy");
		Date date = new Date();

		dateYear1 = date.getYear() + 1900;
		dateYear2 = date.getYear() + 1901;

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		try {
			String query = new String();
			query = "SELECT c FROM SatinalmaTakipDosyalar c WHERE c.id=(Select max(d.satinalmaTakipDosyalar.id) from SatinalmaTakipDosyalarIcerik d where d.gorevliPersonel.employeeId=:prmEmployeeId and d.takipDosyaId IS NOT NULL) and c.eklemeZamani BETWEEN '"
					+ dateYear1 + ".01.01' AND '" + dateYear2 + ".01.01'";

			Query findQuery = manager.createQuery(query);
			result = findQuery.setParameter("prmEmployeeId", prmEmployeeId)
					.getResultList();
			manager.getTransaction().commit();
			manager.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		if (result.size() == 0) {

			if (prmEmployeeId == 138) {
				return sonuc = "EMK-" + dateFormat.format(date) + "/"
						+ (new DecimalFormat("0000000").format(1000));
			} else {// if (employeeId == 140) {
				return sonuc = "EMK-" + dateFormat.format(date) + "/"
						+ (new DecimalFormat("0000000").format(2000));
			}

		} else {
			if (prmEmployeeId == 138) {
				sayi = Integer.parseInt((result.get(0).getDosyaTakipNo())
						.substring(9, 16)) + 1;
				if (sayi == 3000) {
					return sonuc = "EMK-"
							+ dateFormat.format(date)
							+ "/"
							+ (new DecimalFormat("0000000").format(sayi + 1000));
				} else if (sayi == 5000) {
					return sonuc = "EMK-"
							+ dateFormat.format(date)
							+ "/"
							+ (new DecimalFormat("0000000").format(sayi + 1000));
				} else {
					return sonuc = "EMK-" + dateFormat.format(date) + "/"
							+ (new DecimalFormat("0000000").format(sayi));
				}
			} else {// if (employeeId == 140) {
				sayi = Integer.parseInt((result.get(0).getDosyaTakipNo())
						.substring(9, 16)) + 1;
				if (sayi == 2000) {
					return sonuc = "EMK-"
							+ dateFormat.format(date)
							+ "/"
							+ (new DecimalFormat("0000000").format(sayi + 1000));
				} else if (sayi == 4000) {
					return sonuc = "EMK-"
							+ dateFormat.format(date)
							+ "/"
							+ (new DecimalFormat("0000000").format(sayi + 1000));
				} else {
					return sonuc = "EMK-" + dateFormat.format(date) + "/"
							+ (new DecimalFormat("0000000").format(sayi));
				}
			}
		}
	}

}
