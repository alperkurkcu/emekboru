/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaFlowCoatFilmThicknessSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaFlowCoatFilmThicknessSonucManager extends
		CommonQueries<TestKaplamaFlowCoatFilmThicknessSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaFlowCoatFilmThicknessSonuc> getAllTestKaplamaFlowCoatFilmThicknessSonuc(
			int globalId, Integer pipeId) {

		List<TestKaplamaFlowCoatFilmThicknessSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestKaplamaFlowCoatFilmThicknessSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
