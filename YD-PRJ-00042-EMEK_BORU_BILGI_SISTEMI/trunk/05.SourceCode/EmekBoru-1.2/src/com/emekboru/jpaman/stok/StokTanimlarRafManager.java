/**
 * 
 */
package com.emekboru.jpaman.stok;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.stok.StokTanimlarRaf;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class StokTanimlarRafManager extends CommonQueries<StokTanimlarRaf> {

	@SuppressWarnings("unchecked")
	public List<StokTanimlarRaf> getAllStokTanimlarRaf() {

		List<StokTanimlarRaf> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("StokTanimlarRaf.findAll")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
