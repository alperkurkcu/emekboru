/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaFlowCoatBondSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaFlowCoatBondSonucManager extends
		CommonQueries<TestKaplamaFlowCoatBondSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaFlowCoatBondSonuc> getAllTestKaplamaFlowCoatBondSonuc(
			int globalId, Integer pipeId) {

		List<TestKaplamaFlowCoatBondSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("TestKaplamaFlowCoatBondSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
