package com.emekboru.jpaman.sales.order;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.primefaces.model.SortOrder;

import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class SalesItemManager extends CommonQueries<SalesItem> {

	@SuppressWarnings("unchecked")
	public List<SalesItem> loadSalesItems() {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query findQuerie = manager.createNamedQuery("SalesItem.findAll");
		List<SalesItem> resultList = findQuerie.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return resultList;
	}

	public Integer findItemNo(Integer proposalId) {

		Integer count = 1;
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query findQuerie = manager.createNamedQuery("SalesItem.findItemNo");
		findQuerie.setParameter("prmProposalId", proposalId);

		if (findQuerie.getResultList().isEmpty()) {
			return count;
		} else {
			Number result = (Number) findQuerie.getResultList().size();
			if (result.equals(null)) {

			} else {
				count = result.intValue() + 1;
				if (count.equals(null)) {
					count = 1;
				}
			}
		}
		manager.getTransaction().commit();
		manager.close();
		return count;
	}

	@SuppressWarnings("unchecked")
	public List<String> findDistinctValuesOfColumn(String columnName) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String name = SalesItem.class.getName();
		String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
		String query = "SELECT DISTINCT c." + columnName + " FROM "
				+ simpleClassName + " c ";
		Query findQuery = manager.createQuery(query);
		List<String> returnedList = findQuery.getResultList();
		tx.commit();
		manager.close();
		return returnedList;
	}

	@SuppressWarnings("unchecked")
	public List<SalesItem> loadSalesItemsOrdered() {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query findQuerie = manager.createNamedQuery("SalesItem.findAllOrdered");
		List<SalesItem> resultList = findQuerie.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return resultList;
	}

	@SuppressWarnings("unchecked")
	public List<SalesItem> findRangeFilter(int start, int pagesize,
			ArrayList<String> alanlar, ArrayList<String> veriler,
			String sortField, SortOrder sortOrder) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		String hsql = "select r from SalesItem r where 1=1 ";
		if (alanlar != null && alanlar.size() > 0) {
			for (int i = 0; i < alanlar.size(); i++) {
				if (alanlar.get(i) != null && !alanlar.get(i).equals(""))
					hsql += "and r." + alanlar.get(i) + " like '%"
							+ veriler.get(i) + "%' ";
			}
		}
		String siralama;
		if (sortField != null && !sortField.equals("")) {
			if (sortOrder.ordinal() == 1) {
				siralama = " desc";
			} else {
				siralama = " asc";
			}
			hsql += "order by r." + sortField + siralama;
		} else
			hsql += "order by r.proposal.proposalId, r.itemNo ";

		System.out.println(hsql);

		Query query = manager.createQuery(hsql);
		query.setMaxResults(pagesize);
		query.setFirstResult(start);
		List<SalesItem> list = query.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return list;
	}

	public int countTotalRecord() {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query query = manager.createNamedQuery("SalesItem.totalAll");
		Number result = (Number) query.getSingleResult();
		int rr = result.intValue();
		manager.getTransaction().commit();
		manager.close();
		return rr;
	}
}
