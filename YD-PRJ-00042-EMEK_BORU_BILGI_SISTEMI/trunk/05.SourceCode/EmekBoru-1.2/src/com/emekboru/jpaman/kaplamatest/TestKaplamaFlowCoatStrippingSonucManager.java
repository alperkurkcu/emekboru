/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaFlowCoatStrippingSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaFlowCoatStrippingSonucManager extends
		CommonQueries<TestKaplamaFlowCoatStrippingSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaFlowCoatStrippingSonuc> getAllTestKaplamaFlowCoatStrippingSonuc(
			int globalId, Integer pipeId) {

		List<TestKaplamaFlowCoatStrippingSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestKaplamaFlowCoatStrippingSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
