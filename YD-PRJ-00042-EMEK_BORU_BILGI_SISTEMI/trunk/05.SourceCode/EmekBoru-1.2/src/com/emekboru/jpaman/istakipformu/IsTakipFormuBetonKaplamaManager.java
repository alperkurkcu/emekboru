/**
 * 
 */
package com.emekboru.jpaman.istakipformu;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.istakipformu.IsTakipFormuBetonKaplama;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class IsTakipFormuBetonKaplamaManager extends
		CommonQueries<IsTakipFormuBetonKaplama> {

	@SuppressWarnings("unchecked")
	public static List<IsTakipFormuBetonKaplama> getAllIsTakipFormuBetonKaplama(
			Integer itemId) {

		List<IsTakipFormuBetonKaplama> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("IsTakipFormuBetonKaplama.findAll")
					.setParameter("prmItemId", itemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
