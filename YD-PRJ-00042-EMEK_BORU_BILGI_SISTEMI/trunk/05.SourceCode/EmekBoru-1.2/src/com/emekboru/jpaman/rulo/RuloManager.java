package com.emekboru.jpaman.rulo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.primefaces.model.SortOrder;

import com.emekboru.config.Config;
import com.emekboru.jpa.Machine;
import com.emekboru.jpa.MachineRuloLink;
import com.emekboru.jpa.rulo.Rulo;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class RuloManager extends CommonQueries<Rulo> {
	@SuppressWarnings("unchecked")
	public List<Rulo> getAll() {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		List<Rulo> rulos = manager.createQuery("select r from Rulo r")
				.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return rulos;
	}

	@SuppressWarnings("unchecked")
	public List<Rulo> getByRuloId(Integer ruloId) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query query = manager.createNamedQuery("Rulo.findByRuloId");
		query.setParameter("prmRuloId", ruloId);
		List<Rulo> rulos = query.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return rulos;
	}

	@SuppressWarnings("unchecked")
	public List<Rulo> getByDokumEtKalinligi(String dokumNo, Float etKalinligi) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query query = manager
				.createNamedQuery("Rulo.findByDokumAndEtKalinligi");
		query.setParameter("prmDokumNo", dokumNo).setParameter(
				"prmEtKalinligi", etKalinligi);
		List<Rulo> rulos = query.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return rulos;
	}

	@SuppressWarnings("unchecked")
	public List<Rulo> getYear(String ruloYil) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query query = manager.createNamedQuery("inRuloYil");
		query.setParameter("ruloYil", ruloYil);
		List<Rulo> rulos = query.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return rulos;
	}

	@SuppressWarnings("unchecked")
	public List<Rulo> findRange(int start, int pagesize) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query query = manager
				.createQuery("select r from Rulo r order by r.ruloId desc");
		query.setMaxResults(pagesize);
		query.setFirstResult(start);
		List<Rulo> rulos = query.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return rulos;
	}

	@SuppressWarnings("unchecked")
	public List<Rulo> findRangeFilter(int start, int pagesize,
			ArrayList<String> alanlar, ArrayList<String> veriler,
			String sortField, SortOrder sortOrder) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		String hsql = "select r from Rulo r ";
		if (alanlar != null && alanlar.size() > 0) {
			hsql += "where ";
			for (int i = 0; i < alanlar.size(); i++) {
				if (alanlar.get(i) != null && !alanlar.get(i).equals("")
						&& !alanlar.get(i).equals("ruloYilAndRuloKimlikId"))
					hsql += "r." + alanlar.get(i) + " like '%" + veriler.get(i)
							+ "%' ";
				else if (alanlar.get(i) != null && !alanlar.get(i).equals("")
						&& alanlar.get(i).equals("ruloYilAndRuloKimlikId")) {
					hsql += " CONCAT(r.ruloYil,CONCAT('/',r.ruloKimlikId )) like '%"
							+ veriler.get(i) + "%' ";
				}
			}
		}
		String siralama;
		if (sortField != null && !sortField.equals("")
				&& !sortField.equals("ruloYilAndRuloKimlikId")) {
			if (sortOrder.ordinal() == 1) {
				siralama = " desc";
			} else {
				siralama = " asc";
			}
			hsql += "order by r." + sortField + siralama;
		} else if (sortField != null && !sortField.equals("")
				&& sortField.equals("ruloYilAndRuloKimlikId")) {
			if (sortOrder.ordinal() == 1) {
				siralama = " desc";
			} else {
				siralama = " asc";
			}
			hsql += "order by r.ruloYil " + siralama + " , r.ruloKimlikId"
					+ siralama;
		} else
			hsql += "order by r.ruloId desc";

		Query query = manager.createQuery(hsql);
		query.setMaxResults(pagesize);
		query.setFirstResult(start);
		List<Rulo> rulos = query.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return rulos;
	}

	public int countTotalRecord() {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query query = manager.createNamedQuery("total");
		Number result = (Number) query.getSingleResult();
		int rr = result.intValue();
		manager.getTransaction().commit();
		manager.close();
		return rr;
	}

	// return rulos that are being processed in one specific machine
	// they should have a not-finished MachineRuloLink
	@SuppressWarnings("unchecked")
	public List<Rulo> findInMachine(int machineId) {

		List<Rulo> rulos = new ArrayList<Rulo>();
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query findQuerie = manager
				.createNamedQuery("MachineRuloLink.findByMachineIdAndEnded");
		findQuerie.setParameter("prmMachineId", machineId);
		findQuerie.setParameter("prmEnded", false);
		List<MachineRuloLink> links = findQuerie.getResultList();
		System.out.println("How many RuloLinks: " + links.size());
		for (MachineRuloLink l : links) {
			rulos.add(l.getRulo());
		}

		return rulos;

	}

	@SuppressWarnings("unchecked")
	public MachineRuloLink findCurrentLinkInMachine(Machine machine, Rulo rulo) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query findQuerie = manager
				.createNamedQuery("MachineRuloLink.findByMachineIdEndedRuloId");
		findQuerie.setParameter("prmMachineId", machine.getMachineId());
		findQuerie.setParameter("prmEnded", false);
		findQuerie.setParameter("prmRuloId", rulo.getRuloId());
		List<MachineRuloLink> result = findQuerie.getResultList();

		if (result.size() > 0) {
			return result.get(0);
		}

		return null;

	}

	public List<Rulo> findAllocatedRulos(Config config) {
		// TODO Auto-generated method stub
		return null;
	}

	public void deleteMachineRuloLink(Rulo rulo) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();

		// pipe_machine_rulo_link delete
		// Query pmrl = manager
		// .createNativeQuery("DELETE FROM pipe_machine_rulo_link where machine_rulo_link_id=?");
		// pmrl.setParameter(1, rulo.getMachineRuloLinks().get(0).getPmrLinks()
		// .get(0).getMachineRuloLink().getId());

		// machine_rulo_link delete
		// Query query = manager
		// .createNativeQuery("DELETE FROM machine_rulo_link where rulo_id=? and machine_id=?");
		// query.setParameter(1, rulo.getRuloId());
		// query.setParameter(2, rulo.getMachineRuloLinks().get(0).getMachine()
		// .getMachineId());
		Query query = manager
				.createNativeQuery("DELETE FROM machine_rulo_link where rulo_id=?");
		query.setParameter(1, rulo.getRuloId());
		query.executeUpdate();
		manager.getTransaction().commit();
		manager.close();
	}

	@SuppressWarnings("unchecked")
	public List<Rulo> getLoadRulos(SalesItem salesItem) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query findQuerie = manager.createNamedQuery("Rulo.loadAllocatedRulos");
		findQuerie.setParameter("prmItemId", salesItem.getItemId());
		List<Rulo> resultList = findQuerie.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return resultList;
	}

	@SuppressWarnings("unchecked")
	public List<Rulo> getAvailableRulos() {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query findQuerie = manager
				.createQuery("select r from Rulo r, RuloOzellikBoyutsal t where t.ruloOzellikBoyutsalKalanMiktar>0 and r.ruloOzellikBoyutsal = t.ruloOzellikBoyutsalId and (r.orderId=0 or r.orderId is null) order by r.ruloId");
		List<Rulo> resultList = findQuerie.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return resultList;
	}

	// siparişin rulolarını getiren metod
	@SuppressWarnings("unchecked")
	public List<Rulo> getAvailableRulosForSpiralMachine(SalesItem salesItem) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query findQuerie = manager
				.createQuery("SELECT DISTINCT r from Rulo r "
						+ "WHERE r.ruloOzellikBoyutsal.ruloOzellikBoyutsalKalanMiktar>0 AND "
						+ "r.ruloOzellikBoyutsal.ruloOzellikBoyutsalEtKalinligi BETWEEN :paramrmThicknessMin AND :paramrmThicknessMax and "
						+ "r.ruloDurum not in (4,5,7,8,9)"
						+ " order by r.ruloId");
		findQuerie.setParameter("paramrmThicknessMin",
				salesItem.getThickness() - 2);
		findQuerie.setParameter("paramrmThicknessMax",
				salesItem.getThickness() + 2);
		List<Rulo> resultList = findQuerie.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return resultList;
		// int itemId = 0;
		// EntityManager manager = Factory.getInstance().createEntityManager();
		// manager.getEntityManagerFactory().getCache().evictAll();
		// manager.getTransaction().begin();
		// if (salesItem != null) {
		// itemId = salesItem.getItemId();
		// }
		// Query findQuerie = manager
		// .createNamedQuery("Rulo.loadAvaliableRulosForSpiralMachine");
		// findQuerie.setParameter("prmItemId", itemId);
		// List<Rulo> resultList = findQuerie.getResultList();
		// manager.getTransaction().commit();
		// manager.close();
		// return resultList;
	}

	@SuppressWarnings("unchecked")
	public Rulo getLastRulo() {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query findQuerie = manager.createNamedQuery("Rulo.loadFirstRulo");
		List<Rulo> resultList = findQuerie.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return resultList.get(0);
	}

	// @SuppressWarnings("unchecked")
	// public List<Rulo> getSalesItemAvailableRulos(SalesItem salesItem) {
	//
	// EntityManager manager = Factory.getInstance().createEntityManager();
	// manager.getEntityManagerFactory().getCache().evictAll();
	// manager.getTransaction().begin();
	// Query findQuerie = manager
	// .createQuery("SELECT r from Rulo r, RuloOzellikBoyutsal t "
	// + "WHERE t.ruloOzellikBoyutsalKalanMiktar>0 AND "
	// + "(r.orderId=0 or r.orderId is null) AND "
	// +
	// "r.thickness BETWEEN (:paramrmThickness+0.2) AND (:paramrmThickness-0.2) "
	// + "order by r.ruloId");
	// findQuerie.setParameter("paramrmThickness", salesItem.getThickness());
	// List<Rulo> resultList = findQuerie.getResultList();
	// manager.getTransaction().commit();
	// manager.close();
	// return resultList;
	// }

	@SuppressWarnings("unchecked")
	public Rulo findByYilKimlik(String prmRuloYil, String prmRuloKimlikId) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query findQuerie = manager.createNamedQuery("Rulo.findByYilKimlik");
		findQuerie.setParameter("prmRuloYil", prmRuloYil);
		findQuerie.setParameter("prmRuloKimlikId", prmRuloKimlikId);
		List<Rulo> resultList = findQuerie.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return resultList.get(0);
	}

	@SuppressWarnings("unchecked")
	public List<Rulo> findRangeFilter(int start, int pagesize,
			ArrayList<String> alanlar, ArrayList<String> veriler,
			String sortField, SortOrder sortOrder, SalesItem prmSalesItem) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query query = null;
		if (prmSalesItem.getItemId() > 0) {
			String hsql = "SELECT DISTINCT r from Rulo r "
					+ "WHERE r.ruloOzellikBoyutsal.ruloOzellikBoyutsalKalanMiktar>0 AND "
					+ "r.ruloOzellikBoyutsal.ruloOzellikBoyutsalEtKalinligi BETWEEN :paramrmThicknessMin AND :paramrmThicknessMax and "
					+ "r.ruloDurum not in (4,5,7,8,9)";
			if (alanlar != null && alanlar.size() > 0) {
				for (int i = 0; i < alanlar.size(); i++) {
					if (alanlar.get(i) != null && !alanlar.get(i).equals("")
							&& !alanlar.get(i).equals("ruloYilAndRuloKimlikId"))
						hsql += "and r." + alanlar.get(i) + " like '%"
								+ veriler.get(i) + "%' ";
					else if (alanlar.get(i) != null
							&& !alanlar.get(i).equals("")
							&& alanlar.get(i).equals("ruloYilAndRuloKimlikId")) {
						hsql += "and CONCAT(r.ruloYil,CONCAT('/',r.ruloKimlikId )) like '%"
								+ veriler.get(i) + "%' ";
					}
				}
			}
			String siralama;
			if (sortField != null && !sortField.equals("")
					&& !sortField.equals("ruloYilAndRuloKimlikId")) {
				if (sortOrder.ordinal() == 1) {
					siralama = " desc";
				} else {
					siralama = " asc";
				}
				hsql += "order by r." + sortField + siralama;
			} else if (sortField != null && !sortField.equals("")
					&& sortField.equals("ruloYilAndRuloKimlikId")) {
				if (sortOrder.ordinal() == 1) {
					siralama = " desc";
				} else {
					siralama = " asc";
				}
				hsql += "order by r.ruloYil " + siralama + " , r.ruloKimlikId"
						+ siralama;
			} else
				hsql += "order by r.ruloId desc";

			query = manager.createQuery(hsql);
			query.setMaxResults(pagesize);
			query.setFirstResult(start);
			query.setParameter("paramrmThicknessMin",
					prmSalesItem.getThickness() - 2);
			query.setParameter("paramrmThicknessMax",
					prmSalesItem.getThickness() + 2);
		} else {
			String hsql = "select r from Rulo r ";
			if (alanlar != null && alanlar.size() > 0) {
				hsql += "where ";
				for (int i = 0; i < alanlar.size(); i++) {
					if (alanlar.get(i) != null && !alanlar.get(i).equals("")
							&& !alanlar.get(i).equals("ruloYilAndRuloKimlikId"))
						hsql += "r." + alanlar.get(i) + " like '%"
								+ veriler.get(i) + "%' ";
					else if (alanlar.get(i) != null
							&& !alanlar.get(i).equals("")
							&& alanlar.get(i).equals("ruloYilAndRuloKimlikId")) {
						hsql += " CONCAT(r.ruloYil,CONCAT('/',r.ruloKimlikId )) like '%"
								+ veriler.get(i) + "%' ";
					}
				}
			}
			String siralama;
			if (sortField != null && !sortField.equals("")
					&& !sortField.equals("ruloYilAndRuloKimlikId")) {
				if (sortOrder.ordinal() == 1) {
					siralama = " desc";
				} else {
					siralama = " asc";
				}
				hsql += "order by r." + sortField + siralama;
			} else if (sortField != null && !sortField.equals("")
					&& sortField.equals("ruloYilAndRuloKimlikId")) {
				if (sortOrder.ordinal() == 1) {
					siralama = " desc";
				} else {
					siralama = " asc";
				}
				hsql += "order by r.ruloYil " + siralama + " , r.ruloKimlikId"
						+ siralama;
			} else
				hsql += "order by r.ruloId desc";

			query = manager.createQuery(hsql);
			query.setMaxResults(pagesize);
			query.setFirstResult(start);
		}
		List<Rulo> rulos = query.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return rulos;
	}

	public int countTotalRecord(SalesItem prmSalesItem) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query query = manager.createNamedQuery("Rulo.totalSalesItem");
		query.setParameter("paramrmThicknessMin",
				prmSalesItem.getThickness() - 2);
		query.setParameter("paramrmThicknessMax",
				prmSalesItem.getThickness() + 2);
		Number result = (Number) query.getSingleResult();
		int rr = result.intValue();
		manager.getTransaction().commit();
		manager.close();
		return rr;
	}
}
