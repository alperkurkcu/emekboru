/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatestspecs.KaliteKaplamaMarkalama;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KaliteKaplamaMarkalamaManager extends
		CommonQueries<KaliteKaplamaMarkalama> {

	@SuppressWarnings("unchecked")
	public static List<KaliteKaplamaMarkalama> getAllKaliteKaplamaMarkalama(
			Integer itemId) {

		List<KaliteKaplamaMarkalama> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("KaliteKaplamaMarkalama.findAll")
					.setParameter("prmItemId", itemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
