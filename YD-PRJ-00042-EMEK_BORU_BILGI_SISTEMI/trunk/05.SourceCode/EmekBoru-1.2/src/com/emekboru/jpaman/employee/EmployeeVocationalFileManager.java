/**
 * 
 */
package com.emekboru.jpaman.employee;

import java.sql.Timestamp;

import javax.faces.bean.ManagedProperty;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.employee.EmployeeVocationalFile;
import com.emekboru.jpaman.CommonQueries;

/**
 * @author kursat
 * 
 */
public class EmployeeVocationalFileManager extends
		CommonQueries<com.emekboru.jpa.employee.EmployeeVocationalFile> {

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userCredentialsBean;

	public EmployeeVocationalFileManager() {
		super();
		userCredentialsBean = new UserCredentialsBean();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.emekboru.jpaman.CommonQueries#enterNew(java.lang.Object)
	 */
	@Override
	public boolean enterNew(EmployeeVocationalFile employeeVocationalFile) {

		Integer ekleyenKullanici = userCredentialsBean.getUser().getId();
		Timestamp eklemeZamani = new java.sql.Timestamp(
				new java.util.Date().getTime());

		employeeVocationalFile.setEklemeZamani(eklemeZamani);
		employeeVocationalFile.setEkleyenKullanici(ekleyenKullanici);

		// TODO Auto-generated method stub
		return super.enterNew(employeeVocationalFile);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.emekboru.jpaman.CommonQueries#updateEntity(java.lang.Object)
	 */
	@Override
	public void updateEntity(EmployeeVocationalFile employeeVocationalFile) {

		Integer guncelleyenKullanici = userCredentialsBean.getUser().getId();
		Timestamp guncellemeZamani = new java.sql.Timestamp(
				new java.util.Date().getTime());

		employeeVocationalFile.setGuncellemeZamani(guncellemeZamani);
		employeeVocationalFile.setGuncelleyenKullanici(guncelleyenKullanici);

		super.updateEntity(employeeVocationalFile);
	}

}
