/**
 * 
 */
package com.emekboru.jpaman.isemirleri;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.isemri.IsEmriPolietilenKaplama;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class IsEmriPolietilenKaplamaManager extends
		CommonQueries<IsEmriPolietilenKaplama> {

	@SuppressWarnings("unchecked")
	public static List<IsEmriPolietilenKaplama> getAllIsEmriPolietilenKaplama(
			Integer prmItemId) {

		List<IsEmriPolietilenKaplama> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("IsEmriPolietilenKaplama.findAllByItemId")
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
