package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaUygulamaSartlariSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class TestKaplamaUygulamaSartlariSonucManager extends
		CommonQueries<TestKaplamaUygulamaSartlariSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaUygulamaSartlariSonuc> getAllTestKaplamaUygulamaSartlariSonuc(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaUygulamaSartlariSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestKaplamaUygulamaSartlariSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
