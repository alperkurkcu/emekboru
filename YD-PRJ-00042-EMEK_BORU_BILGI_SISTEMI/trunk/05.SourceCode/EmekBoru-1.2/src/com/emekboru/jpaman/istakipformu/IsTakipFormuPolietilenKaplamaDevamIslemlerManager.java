/**
 * 
 */
package com.emekboru.jpaman.istakipformu;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.istakipformu.IsTakipFormuPolietilenKaplamaDevamIslemler;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class IsTakipFormuPolietilenKaplamaDevamIslemlerManager extends
		CommonQueries<IsTakipFormuPolietilenKaplamaDevamIslemler> {

	@SuppressWarnings("unchecked")
	public static List<IsTakipFormuPolietilenKaplamaDevamIslemler> getAllIsTakipFormuPolietilenKaplamaDevamIslemler(
			Integer itemId) {

		List<IsTakipFormuPolietilenKaplamaDevamIslemler> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"IsTakipFormuPolietilenKaplamaDevamIslemler.findAll")
					.setParameter("prmItemId", itemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
