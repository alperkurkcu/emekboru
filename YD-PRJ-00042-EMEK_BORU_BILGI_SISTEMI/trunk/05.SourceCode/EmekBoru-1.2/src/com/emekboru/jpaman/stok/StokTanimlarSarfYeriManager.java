/**
 * 
 */
package com.emekboru.jpaman.stok;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.stok.StokTanimlarSarfYeri;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class StokTanimlarSarfYeriManager extends
		CommonQueries<StokTanimlarSarfYeri> {

	@SuppressWarnings("unchecked")
	public List<StokTanimlarSarfYeri> getAllStokTanimlarSarfYeri() {

		List<StokTanimlarSarfYeri> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("StokTanimlarSarfYeri.findAll")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
