/**
 * 
 */
package com.emekboru.jpaman.stok;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.stok.StokJoinUreticiKoduBarkod;
import com.emekboru.jpa.stok.StokUrunKartlari;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class StokJoinUreticiKoduBarkodManager extends
		CommonQueries<StokJoinUreticiKoduBarkod> {

	@SuppressWarnings("unchecked")
	public List<StokJoinUreticiKoduBarkod> getAllStokJoinUreticiKoduBarkod(
			Integer ureticiJoinId) {

		List<StokJoinUreticiKoduBarkod> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"StokJoinUreticiKoduBarkod.findAllByUreticiJoinId")
					.setParameter("prmUreticiJoinId", ureticiJoinId)
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<StokJoinUreticiKoduBarkod> findAllByUreticiJoinIdMarkaId(
			Integer prmMarkaId, Integer prmUrunJoinId) {

		List<StokJoinUreticiKoduBarkod> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"StokJoinUreticiKoduBarkod.findAllByUreticiJoinIdMarkaId")
					.setParameter("prmMarkaId", prmMarkaId)
					.setParameter("prmUrunJoinId", prmUrunJoinId)
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	List<StokUrunKartlari> result = null;

	@SuppressWarnings({ "unchecked" })
	public List<StokUrunKartlari> getAllStokUrunKartlariBarkodlu() {
			
		Thread thread = new Thread() {
			@Override
			public void run() {

				EntityManager em = Factory.getEntityManager();
				result = em.createNamedQuery("StokUrunKartlari.findAllBarkodlu")
						.getResultList();
			}
		};
		thread.run();
		
		return result;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<StokUrunKartlari> getAllStokUrunKartlariBarkodluByUrunId(
			Integer prmUrunId) {
		List result = null;

		try {
			EntityManager em = Factory.getEntityManager();
			result = em
					.createNamedQuery(
							"StokUrunKartlari.findAllBarkodluByUrunId")
					.setParameter("prmUrunId", prmUrunId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<StokJoinUreticiKoduBarkod> findAllByUrunId(Integer prmUrunId) {

		List<StokJoinUreticiKoduBarkod> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"StokJoinUreticiKoduBarkod.findAllByUrunId")
					.setParameter("prmUrunId", prmUrunId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
