/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatestspecs.KaplamaKaliteKaynakAgziKoruma;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KaplamaKaliteKaynakAgziKorumaManager extends
		CommonQueries<KaplamaKaliteKaynakAgziKoruma> {

	@SuppressWarnings("unchecked")
	public static List<KaplamaKaliteKaynakAgziKoruma> getAllKaplamaKaliteKaynakAgziKoruma(
			Integer itemId) {

		List<KaplamaKaliteKaynakAgziKoruma> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("KaplamaKaliteKaynakAgziKoruma.findAll")
					.setParameter("prmItemId", itemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
