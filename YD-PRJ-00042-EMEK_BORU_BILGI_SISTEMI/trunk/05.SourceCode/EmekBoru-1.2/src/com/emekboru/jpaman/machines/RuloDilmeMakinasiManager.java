/**
 * 
 */
package com.emekboru.jpaman.machines;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.machines.RuloDilmeMakinasi;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class RuloDilmeMakinasiManager extends CommonQueries<RuloDilmeMakinasi> {

	@SuppressWarnings("unchecked")
	public static List<RuloDilmeMakinasi> getAllRuloDilmeMakinasi(Integer ruloId) {

		List<RuloDilmeMakinasi> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("RuloDilmeMakinasi.findAll")
					.setParameter("prmRuloId", ruloId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
