package com.emekboru.jpaman;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.EdwPipeLink;
import com.emekboru.utils.UtilInsCore;

public class EdwPipeLinkManager extends CommonQueries<EdwPipeLink> {

	@SuppressWarnings("unchecked")
	public void insertEdwPipeLink(EdwPipeLink edwPipeLink) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();

		int num = 0;
		Query index = manager
				.createNativeQuery("select max(id) from edw_pipe_link");
		List<Integer> getMaxIndex = index.getResultList();

		if (getMaxIndex.get(0) == null) {
			getMaxIndex.add(0, 0);
		} else
			num = getMaxIndex.get(0).intValue();

		if (edwPipeLink.getAkim() != null && edwPipeLink.getVolt() != null) {
			Query query = manager
					.createNativeQuery("INSERT INTO edw_pipe_link (electrode_dust_wire_id, pipe_id, id, akim, volt, ac_dc, ic_dis) "
							+ " VALUES(?,?,?,?,?,?,?)");
			query.setParameter(1, edwPipeLink.getEdw().getElectrodeDustWireId());
			query.setParameter(2, edwPipeLink.getPipe().getPipeId());
			query.setParameter(3, num + 1);
			query.setParameter(4, edwPipeLink.getAkim().floatValue());
			query.setParameter(5, edwPipeLink.getVolt().floatValue());
			query.setParameter(6, edwPipeLink.getAcDc().floatValue());
			query.setParameter(7, edwPipeLink.getIcDis().floatValue());
			query.executeUpdate();
		} else {
			Query query = manager
					.createNativeQuery("INSERT INTO edw_pipe_link (electrode_dust_wire_id, pipe_id, id) "
							+ " VALUES(?,?,?)");
			query.setParameter(1, edwPipeLink.getEdw().getElectrodeDustWireId());
			query.setParameter(2, edwPipeLink.getPipe().getPipeId());
			query.setParameter(3, num + 1);
			query.executeUpdate();
		}

		manager.getTransaction().commit();
		manager.close();
	}

	@SuppressWarnings("unchecked")
	public static List<EdwPipeLink> findByPrmPipeId(Integer prmPipeId) {

		List<EdwPipeLink> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("EdwPipeLink.findByPrmPipeId")
					.setParameter("prmPipeId", prmPipeId).getResultList();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<EdwPipeLink> dunUretilmisBorularIcin(Integer prmMachineId) {

		List<EdwPipeLink> result = null;
		EntityManager em = Factory.getEntityManager();

		String tarihIlk = UtilInsCore.getYesterday().toString()
				.substring(0, 10)
				+ " 08:00:00";
		String tarihSon = UtilInsCore.getTarihZaman().toString()
				.substring(0, 10)
				+ " 07:59:59";

		Timestamp tsIlk = Timestamp.valueOf(tarihIlk);
		Timestamp tsSon = Timestamp.valueOf(tarihSon);

		try {
			String query = "SELECT mel FROM EdwPipeLink mel WHERE mel.pipe.pipeId in (select o.pipe.pipeId from MachinePipeLink o where o.pipe.eklemeZamani BETWEEN :prmDate1 AND :prmDate2 and o.machine.machineId=:prmMachineId) order by mel.pipe.pipeId";
			Query index = em.createQuery(query);
			index.setParameter("prmDate1", tsIlk)
					.setParameter("prmDate2", tsSon)
					.setParameter("prmMachineId", prmMachineId);
			result = index.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<EdwPipeLink> tumUretilmisBorularIcin(Integer prmSalesItemId,
			Integer prmMachineId) {

		List<EdwPipeLink> result = null;
		EntityManager em = Factory.getEntityManager();

		try {
			String query = "SELECT mel FROM EdwPipeLink mel WHERE mel.pipe.pipeId in (select o.pipe.pipeId from MachinePipeLink o where o.machine.machineId=:prmMachineId and o.pipe.salesItem.itemId=:prmSalesItemId) order by mel.pipe.pipeId";
			Query index = em.createQuery(query);
			index.setParameter("prmSalesItemId", prmSalesItemId).setParameter(
					"prmMachineId", prmMachineId);
			result = index.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
