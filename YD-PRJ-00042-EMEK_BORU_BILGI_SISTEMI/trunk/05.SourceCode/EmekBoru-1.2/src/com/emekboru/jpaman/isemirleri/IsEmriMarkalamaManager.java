/**
 * 
 */
package com.emekboru.jpaman.isemirleri;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.isemri.IsEmriMarkalama;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class IsEmriMarkalamaManager extends CommonQueries<IsEmriMarkalama> {

	@SuppressWarnings("unchecked")
	public static List<IsEmriMarkalama> getAllIsEmriMarkalama(Integer prmItemId) {

		List<IsEmriMarkalama> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("IsEmriMarkalama.findAllByItemId")
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
