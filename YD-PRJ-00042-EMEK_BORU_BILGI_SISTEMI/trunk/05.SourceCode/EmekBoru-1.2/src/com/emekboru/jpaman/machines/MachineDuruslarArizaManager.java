/**
 * 
 */
package com.emekboru.jpaman.machines;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.machines.MachineDuruslarAriza;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
public class MachineDuruslarArizaManager extends
		CommonQueries<MachineDuruslarAriza> {

	@SuppressWarnings("unchecked")
	public static List<MachineDuruslarAriza> getAllMachineDuruslarAriza() {

		List<MachineDuruslarAriza> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("MachineDuruslarAriza.findAll")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<MachineDuruslarAriza> getSorguSonuc(
			MachineDuruslarAriza machineDuruslarArizaAramaForm) {

		List<MachineDuruslarAriza> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("MachineDuruslarAriza.sorgula")
					.setParameter("prmBaslamaZamani",
							machineDuruslarArizaAramaForm.getBaslamaZamani())
					.setParameter("prmBitisZamani",
							machineDuruslarArizaAramaForm.getBitisZamani())
					.setParameter(
							"prmMachineTypeId",
							machineDuruslarArizaAramaForm.getMachine()
									.getMachineType()).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<MachineDuruslarAriza> findYesterdaysDayShiftAriza(
			Integer prmMachineId) {

		List<MachineDuruslarAriza> result = null;
		EntityManager em = Factory.getEntityManager();

		String tarihIlk = UtilInsCore.getYesterday().toString()
				.substring(0, 10)
				+ " 08:00:00";
		String tarihSon = UtilInsCore.getYesterday().toString()
				.substring(0, 10)
				+ " 19:59:59";

		Timestamp tsIlk = Timestamp.valueOf(tarihIlk);
		Timestamp tsSon = Timestamp.valueOf(tarihSon);

		System.out.println(tsIlk);
		System.out.println(tsSon);

		try {
			result = em
					.createNamedQuery(
							"MachineDuruslarAriza.findYesterdaysAriza")
					.setParameter("prmDate1", tsIlk)
					.setParameter("prmDate2", tsSon)
					.setParameter("prmMachineId", prmMachineId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<MachineDuruslarAriza> findYesterdaysNightShiftAriza(
			Integer prmMachineId) {

		List<MachineDuruslarAriza> result = null;
		EntityManager em = Factory.getEntityManager();

		String tarihIlk = UtilInsCore.getYesterday().toString()
				.substring(0, 10)
				+ " 20:00:00";
		String tarihSon = UtilInsCore.getYesterday().toString()
				.substring(0, 10)
				+ " 07:59:59";

		Timestamp tsIlk = Timestamp.valueOf(tarihIlk);
		Timestamp tsSon = Timestamp.valueOf(tarihSon);

		System.out.println(tsIlk);
		System.out.println(tsSon);

		try {
			result = em
					.createNamedQuery(
							"MachineDuruslarAriza.findYesterdaysAriza")
					.setParameter("prmDate1", tsIlk)
					.setParameter("prmDate2", tsSon)
					.setParameter("prmMachineId", prmMachineId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
