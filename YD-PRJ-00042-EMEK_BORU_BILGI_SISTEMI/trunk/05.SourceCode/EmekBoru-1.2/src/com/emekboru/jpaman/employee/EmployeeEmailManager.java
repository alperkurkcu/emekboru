package com.emekboru.jpaman.employee;

import com.emekboru.jpa.employee.EmployeeEmail;
import com.emekboru.jpaman.CommonQueries;

public class EmployeeEmailManager extends CommonQueries<EmployeeEmail> {
	/* (non-Javadoc)
	 * @see com.emekboru.jpaman.CommonQueries#enterNew(java.lang.Object)
	 */
	@Override
	public boolean enterNew(EmployeeEmail object) {
		object.setIsDefault(false);
		return super.enterNew(object);
	}

}
