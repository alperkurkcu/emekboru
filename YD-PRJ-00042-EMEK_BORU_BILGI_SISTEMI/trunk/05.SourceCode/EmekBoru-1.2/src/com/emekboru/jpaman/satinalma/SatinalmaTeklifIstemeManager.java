/**
 * 
 */
package com.emekboru.jpaman.satinalma;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.satinalma.SatinalmaTeklifIsteme;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class SatinalmaTeklifIstemeManager extends
		CommonQueries<SatinalmaTeklifIsteme> {

	@SuppressWarnings("unchecked")
	public List<SatinalmaTeklifIsteme> getAllSatinalmaTeklifIsteme() {

		List<SatinalmaTeklifIsteme> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("SatinalmaTeklifIsteme.findAll")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaTeklifIsteme> findByDosyaId(Integer prmDosyaId) {

		List<SatinalmaTeklifIsteme> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("SatinalmaTeklifIsteme.findByDosyaId")
					.setParameter("prmDosyaId", prmDosyaId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings({ "unchecked" })
	public Integer getOtomatikSayi(Integer prmDosyaId) {

		List<Integer> result = null;
		Integer sayi = 1;
		// String sonuc = null;

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		try {
			String query = new String();
			query = "SELECT max(c.teklifSayi) FROM SatinalmaTeklifIsteme c WHERE c.satinalmaTakipDosyalar.id=:prmDosyaId";
			Query findQuery = manager.createQuery(query);
			result = findQuery.setParameter("prmDosyaId", prmDosyaId)
					.getResultList();
			manager.getTransaction().commit();
			manager.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (result.get(0) == null)
			return sayi;
		sayi = result.get(0) + 1;
		return sayi;

	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaTeklifIsteme> satinalmaKarariListDoldur() {

		List<SatinalmaTeklifIsteme> result = null;
		EntityManager em = Factory.getEntityManager();
		try {

			String query = new String();
			// query =
			// "SELECT sti FROM SatinalmaTeklifIsteme sti INNER JOIN SatinalmaTeklifIstemeIcerik ON SatinalmaTeklifIstemeIcerik.satinalmaTeklifIsteme.id = sti.id "
			// +
			// "INNER JOIN SatinalmaTakipDosyalarIcerik ON SatinalmaTeklifIstemeIcerik.satinalmaTakipDosyalarIcerik.id = SatinalmaTakipDosyalarIcerik.id "
			// +
			// "INNER JOIN SatinalmaMalzemeHizmetTalepFormuIcerik ON SatinalmaTakipDosyalarIcerik.satinalmaMalzemeHizmetTalepFormuIcerik.id = SatinalmaMalzemeHizmetTalepFormuIcerik.id "
			// +
			// "INNER JOIN stokUrunKartlari ON stokUrunKartlari.id = SatinalmaMalzemeHizmetTalepFormuIcerik.stokUrunKartlari.id "
			// +
			// "INNER JOIN SatinalmaSatinalmaKarariIcerik ON SatinalmaSatinalmaKarariIcerik.satinalmaTeklifIstemeIcerik.id = SatinalmaTeklifIstemeIcerik.id "
			// +
			// "AND SatinalmaSatinalmaKarariIcerik.satinalmaTeklifIsteme.id = sti.id AND SatinalmaSatinalmaKarariIcerik.satinalmaTeklifIstemeIcerik.id = SatinalmaTeklifIstemeIcerik.id";

			query = "SELECT sti FROM SatinalmaTeklifIsteme sti, SatinalmaTeklifIstemeIcerik stii WHERE stii.satinalmaTeklifIsteme.id = sti.id";

			Query findQuery = em.createQuery(query);
			result = findQuery.getResultList();

			// result = em.createNamedQuery(
			// "SatinalmaTeklifIsteme.satinalmaKarariListDoldur")
			// .getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaTeklifIsteme> findAllTedarikci(
			Integer prmDosyaId) {

		List<SatinalmaTeklifIsteme> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("SatinalmaTeklifIsteme.findAllTedarikci")
					.setParameter("prmDosyaId", prmDosyaId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
