/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaDeliciUcDirencSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaDeliciUcDirencSonucManager extends
		CommonQueries<TestKaplamaDeliciUcDirencSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaDeliciUcDirencSonuc> getAllDeliciUcDirencSonucTest(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaDeliciUcDirencSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("TestKaplamaDeliciUcDirencSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
