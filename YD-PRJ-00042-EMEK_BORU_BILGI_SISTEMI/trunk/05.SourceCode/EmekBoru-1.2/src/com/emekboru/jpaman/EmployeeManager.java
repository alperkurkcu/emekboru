package com.emekboru.jpaman;

import java.sql.Timestamp;
import java.util.List;

import javax.faces.bean.ManagedProperty;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.Employee;

public class EmployeeManager extends CommonQueries<Employee> {

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userCredentialsBean;

	public EmployeeManager() {
		super();
		userCredentialsBean = new UserCredentialsBean();
	}

	@SuppressWarnings("unchecked")
	public List<Employee> notHandled(int eventId) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query query = manager.createNamedQuery("Employee.notHandled");
		query.setParameter("eventId", eventId);
		List<Employee> companyList = (List<Employee>) query.getResultList();
		manager.close();
		return companyList;

	}

	@Override
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.emekboru.jpaman.CommonQueries#enterNew(java.lang.Object)
	 */
	public boolean enterNew(Employee employee) {

		Integer ekleyenKullanici = userCredentialsBean.getUser().getId();
		Timestamp eklemeZamani = new java.sql.Timestamp(
				new java.util.Date().getTime());

		employee.setEklemeZamani(eklemeZamani);
		employee.setEkleyenKullanici(ekleyenKullanici);

		employee.getEmployeeIdentity().setEklemeZamani(eklemeZamani);
		employee.getEmployeeIdentity().setEkleyenKullanici(ekleyenKullanici);

		employee.getEmployeeIdentity().setEmployee(employee);
		return super.enterNew(employee);
	}

	public List<Employee> findByAllActive() {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String query = new String();
		query = "SELECT c FROM Employee c WHERE c.active =1";

		Query findQuery = manager.createQuery(query);
		List<Employee> returnedList = findQuery.getResultList();
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		return returnedList;
	}

	@SuppressWarnings("unchecked")
	public List<Employee> findByActivePassive(boolean prmActivePassive) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		Query query = manager.createNamedQuery("Employee.allActivePassive");
		query.setParameter("prmActivePassive", prmActivePassive);
		List<Employee> returnedList = query.getResultList();
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		return returnedList;
	}
}
