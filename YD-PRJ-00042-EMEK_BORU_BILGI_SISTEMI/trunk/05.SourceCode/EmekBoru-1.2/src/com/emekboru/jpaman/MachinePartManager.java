package com.emekboru.jpaman;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.MachinePart;

public class MachinePartManager extends CommonQueries<MachinePart> {

	@SuppressWarnings("unchecked")
	public static List<MachinePart> findByMachineId(Integer machineId) {

		List<MachinePart> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("MachinePart.findAllByMachineId")
					.setParameter("machineId", machineId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
}
