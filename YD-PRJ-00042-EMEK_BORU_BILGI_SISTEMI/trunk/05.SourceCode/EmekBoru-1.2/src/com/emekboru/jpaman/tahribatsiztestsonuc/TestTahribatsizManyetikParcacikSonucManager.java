/**
 * 
 */
package com.emekboru.jpaman.tahribatsiztestsonuc;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizManyetikParcacikSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
public class TestTahribatsizManyetikParcacikSonucManager extends
		CommonQueries<TestTahribatsizManyetikParcacikSonuc> {

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizManyetikParcacikSonuc> getAllTestTahribatsizManyetikParcacikSonuc(
			Integer pipeId) {

		List<TestTahribatsizManyetikParcacikSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestTahribatsizManyetikParcacikSonuc.findAll")
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizManyetikParcacikSonuc> dunUretilmisBorularIcin(
			Integer prmMachineId) {

		List<TestTahribatsizManyetikParcacikSonuc> result = null;
		EntityManager em = Factory.getEntityManager();

		String tarihIlk = UtilInsCore.getYesterday().toString()
				.substring(0, 10)
				+ " 08:00:00";
		String tarihSon = UtilInsCore.getTarihZaman().toString()
				.substring(0, 10)
				+ " 07:59:59";

		Timestamp tsIlk = Timestamp.valueOf(tarihIlk);
		Timestamp tsSon = Timestamp.valueOf(tarihSon);

		try {
			String query = "SELECT o FROM TestTahribatsizManyetikParcacikSonuc mel WHERE mel.pipe.pipeId in (select o.pipe.pipeId from MachinePipeLink o where o.pipe.eklemeZamani BETWEEN :prmDate1 AND :prmDate2 and o.machine.machineId=:prmMachineId) order by mel.pipe.pipeId";
			Query index = em.createQuery(query);
			index.setParameter("prmDate1", tsIlk)
					.setParameter("prmDate2", tsSon)
					.setParameter("prmMachineId", prmMachineId);
			result = index.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizManyetikParcacikSonuc> tumUretilmisBorularIcin(
			Integer prmSalesItemId, Integer prmMachineId) {

		List<TestTahribatsizManyetikParcacikSonuc> result = null;
		EntityManager em = Factory.getEntityManager();

		try {
			String query = "SELECT mel FROM TestTahribatsizManyetikParcacikSonuc mel WHERE mel.pipe.pipeId in (select o.pipe.pipeId from MachinePipeLink o where o.machine.machineId=:prmMachineId and o.pipe.salesItem.itemId=:prmSalesItemId) order by mel.pipe.pipeId";
			Query index = em.createQuery(query);
			index.setParameter("prmSalesItemId", prmSalesItemId).setParameter(
					"prmMachineId", prmMachineId);
			result = index.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizManyetikParcacikSonuc> findBetweenTwoParameters(
			String prmFirstBarcode, String prmSecondBarcode) {

		List<TestTahribatsizManyetikParcacikSonuc> result = null;
		EntityManager em = Factory.getEntityManager();

		try {
			try {
				String query = "SELECT r FROM TestTahribatsizManyetikParcacikSonuc r WHERE r.pipe.pipeId in (select p.pipeId from Pipe p where p.pipeBarkodNo BETWEEN :prmFirstBarcode AND :prmSecondBarcode ) order by r.pipe.pipeId ASC";
				result = em.createQuery(query)
						.setParameter("prmFirstBarcode", prmFirstBarcode)
						.setParameter("prmSecondBarcode", prmSecondBarcode)
						.getResultList();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			return result;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
