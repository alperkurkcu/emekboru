package com.emekboru.jpaman.sales.order;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import com.emekboru.jpa.sales.order.SalesProposal;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class SalesProposalManager extends CommonQueries<SalesProposal> {

	@SuppressWarnings("unchecked")
	public List<SalesProposal> findAllLikeOrderBy(Class<SalesProposal> clazz, String column,
			String likeAttrbute, String orderBy) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String likeColumn = new String(column);
		String likeAtt = new String(likeAttrbute);

		String name = clazz.getName();
		String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
		String query = "SELECT c FROM " + simpleClassName + " c WHERE c."
				+ likeColumn + " LIKE '%" + likeAtt + "%' ORDER BY c." + likeColumn + " " + orderBy;
		Query findQuery = manager.createQuery(query);
		List<SalesProposal> returnedList = findQuery.getResultList();
		tx.commit();
		manager.close();
		return returnedList;
	}
}