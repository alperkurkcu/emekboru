package com.emekboru.jpaman;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.TestCentikDarbeSonuc;
import com.emekboru.jpa.sales.order.SalesItem;

public class TestCentikDarbeSonucManager extends
		CommonQueries<TestCentikDarbeSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestCentikDarbeSonuc> getAllTestCentikDarbeSonuc(
			Integer globalId, Integer pipeId) {
		List<TestCentikDarbeSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestCentikDarbeSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<TestCentikDarbeSonuc> getBySalesItemGlobalId(
			SalesItem prmSalesItem, int prmGlobalId) {

		List<TestCentikDarbeSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestCentikDarbeSonuc.getBySalesItemGlobalId")
					.setParameter("prmSalesItem", prmSalesItem.getItemId())
					.setParameter("prmGlobalId", prmGlobalId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<TestCentikDarbeSonuc> getBySalesItem(
			SalesItem prmSalesItem) {

		List<TestCentikDarbeSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestCentikDarbeSonuc.getBySalesItem")
					.setParameter("prmSalesItem", prmSalesItem.getItemId())
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<TestCentikDarbeSonuc> getByDokumNo(String prmDokumNo) {

		List<TestCentikDarbeSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestCentikDarbeSonuc.getByDokumNo")
					.setParameter("prmDokumNo", prmDokumNo).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<TestCentikDarbeSonuc> getByDokumNoSalesItem(
			String prmDokumNo, Integer prmItemId) {

		List<TestCentikDarbeSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestCentikDarbeSonuc.getByDokumNoSalesItem")
					.setParameter("prmDokumNo", prmDokumNo)
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
