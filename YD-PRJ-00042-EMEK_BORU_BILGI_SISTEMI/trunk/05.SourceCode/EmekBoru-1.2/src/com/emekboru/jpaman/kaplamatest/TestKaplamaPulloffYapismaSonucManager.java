/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaPulloffYapismaSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaPulloffYapismaSonucManager extends
		CommonQueries<TestKaplamaPulloffYapismaSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaPulloffYapismaSonuc> getAllTestKaplamaPulloffYapismaSonuc(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaPulloffYapismaSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("TestKaplamaPulloffYapismaSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
