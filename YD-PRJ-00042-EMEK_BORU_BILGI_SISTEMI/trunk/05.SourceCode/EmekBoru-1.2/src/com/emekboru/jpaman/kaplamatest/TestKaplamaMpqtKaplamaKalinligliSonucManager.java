/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaMpqtKaplamaKalinligliSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaMpqtKaplamaKalinligliSonucManager extends
		CommonQueries<TestKaplamaMpqtKaplamaKalinligliSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaMpqtKaplamaKalinligliSonuc> getAllTestKaplamaMpqtKaplamaKalinligliSonuc(
			int globalId, Integer pipeId) {

		List<TestKaplamaMpqtKaplamaKalinligliSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestKaplamaMpqtKaplamaKalinligliSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
