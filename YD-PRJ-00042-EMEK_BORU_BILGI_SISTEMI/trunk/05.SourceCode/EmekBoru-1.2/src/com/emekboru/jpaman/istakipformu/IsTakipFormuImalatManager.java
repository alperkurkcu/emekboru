/**
 * 
 */
package com.emekboru.jpaman.istakipformu;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.istakipformu.IsTakipFormuImalat;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class IsTakipFormuImalatManager extends
		CommonQueries<IsTakipFormuImalat> {

	@SuppressWarnings("unchecked")
	public static List<IsTakipFormuImalat> getAllIsTakipFormuImalat(
			Integer itemId) {

		List<IsTakipFormuImalat> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("IsTakipFormuImalat.findAll")
					.setParameter("prmItemId", itemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
