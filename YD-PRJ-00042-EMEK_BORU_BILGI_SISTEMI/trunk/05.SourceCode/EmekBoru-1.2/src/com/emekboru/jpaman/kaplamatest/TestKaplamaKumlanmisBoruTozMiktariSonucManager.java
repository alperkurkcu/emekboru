package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaKumlanmisBoruTozMiktariSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class TestKaplamaKumlanmisBoruTozMiktariSonucManager extends
		CommonQueries<TestKaplamaKumlanmisBoruTozMiktariSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaKumlanmisBoruTozMiktariSonuc> getAllTestKaplamaKumlanmisBoruTozMiktariSonuc(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaKumlanmisBoruTozMiktariSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestKaplamaKumlanmisBoruTozMiktariSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
