/**
 * 
 */
package com.emekboru.jpaman.satinalma;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.satinalma.SatinalmaTeklifIstemeIcerik;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class SatinalmaTeklifIstemeIcerikManager extends
		CommonQueries<SatinalmaTeklifIstemeIcerik> {

	@SuppressWarnings("unchecked")
	public List<SatinalmaTeklifIstemeIcerik> getAllSatinalmaTeklifIstemeIcerik(
			Integer formId) {

		List<SatinalmaTeklifIstemeIcerik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("SatinalmaTeklifIstemeIcerik.findAll")
					.setParameter("prmFormId", formId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
