/**
 * 
 */
package com.emekboru.jpaman.kalibrasyon;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kalibrasyon.KalibrasyonFloroskopik;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KalibrasyonFloroskopikManager extends
		CommonQueries<KalibrasyonFloroskopik> {

	@SuppressWarnings("unchecked")
	public static List<KalibrasyonFloroskopik> getAllKalibrasyon() {

		List<KalibrasyonFloroskopik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("KalibrasyonFloroskopik.findAll")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
