/**
 * 
 */
package com.emekboru.jpaman.kalibrasyon;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kalibrasyon.KalibrasyonManyetik;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KalibrasyonManyetikManager extends
		CommonQueries<KalibrasyonManyetik> {

	@SuppressWarnings("unchecked")
	public static List<KalibrasyonManyetik> getAllKalibrasyon() {

		List<KalibrasyonManyetik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("KalibrasyonManyetik.findAll")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<KalibrasyonManyetik> getAllKalibrasyonByType(
			Integer prmType) {

		List<KalibrasyonManyetik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("KalibrasyonManyetik.findAllByType")
					.setParameter("prmType", prmType).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
