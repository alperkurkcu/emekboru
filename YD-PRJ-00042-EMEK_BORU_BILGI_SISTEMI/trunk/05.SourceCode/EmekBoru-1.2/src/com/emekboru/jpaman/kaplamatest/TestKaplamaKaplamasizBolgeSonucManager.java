package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaKaplamasizBolgeSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class TestKaplamaKaplamasizBolgeSonucManager extends
		CommonQueries<TestKaplamaKaplamasizBolgeSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaKaplamasizBolgeSonuc> getAllTestKaplamaKaplamasizBolgeSonuc(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaKaplamasizBolgeSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("TestKaplamaKaplamasizBolgeSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
