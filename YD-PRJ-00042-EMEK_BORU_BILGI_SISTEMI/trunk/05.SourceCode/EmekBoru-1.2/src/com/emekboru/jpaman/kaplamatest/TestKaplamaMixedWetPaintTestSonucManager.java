/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaMixedWetPaintTestSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaMixedWetPaintTestSonucManager extends
		CommonQueries<TestKaplamaMixedWetPaintTestSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaMixedWetPaintTestSonuc> getAllMixedWetPaintTestSonuc(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaMixedWetPaintTestSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestKaplamaMixedWetPaintTestSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
