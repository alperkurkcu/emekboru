package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaBukmeSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class TestKaplamaBukmeSonucManager extends
		CommonQueries<TestKaplamaBukmeSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaBukmeSonuc> getAllKaplamaBukmeSonucTest(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaBukmeSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestKaplamaBukmeSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
