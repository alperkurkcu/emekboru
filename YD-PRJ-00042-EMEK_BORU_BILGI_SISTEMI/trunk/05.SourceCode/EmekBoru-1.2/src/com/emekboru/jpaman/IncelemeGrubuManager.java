package com.emekboru.jpaman;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.IncelemeGrubu;

public class IncelemeGrubuManager extends CommonQueries<IncelemeGrubu> {

	@SuppressWarnings("unchecked")
	public static List<IncelemeGrubu> getAllIncelemeGrubu() {

		List<IncelemeGrubu> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("IncelemeGrubu.findAll")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
