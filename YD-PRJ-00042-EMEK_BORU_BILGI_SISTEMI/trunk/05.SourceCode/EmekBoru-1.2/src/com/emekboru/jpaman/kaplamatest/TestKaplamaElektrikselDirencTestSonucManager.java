/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaElektrikselDirencTestSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaElektrikselDirencTestSonucManager extends
		CommonQueries<TestKaplamaElektrikselDirencTestSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaElektrikselDirencTestSonuc> getAllElektrikselDirencTestSonuc(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaElektrikselDirencTestSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestKaplamaElektrikselDirencTestSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
