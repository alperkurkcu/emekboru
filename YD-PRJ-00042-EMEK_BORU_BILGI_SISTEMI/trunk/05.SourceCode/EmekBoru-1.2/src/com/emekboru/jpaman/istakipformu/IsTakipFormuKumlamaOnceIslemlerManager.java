/**
 * 
 */
package com.emekboru.jpaman.istakipformu;

import com.emekboru.jpa.istakipformu.IsTakipFormuKumlamaOnceIslemler;
import com.emekboru.jpaman.CommonQueries;

/**
 * @author Alper K
 * 
 */
public class IsTakipFormuKumlamaOnceIslemlerManager extends
		CommonQueries<IsTakipFormuKumlamaOnceIslemler> {

}
