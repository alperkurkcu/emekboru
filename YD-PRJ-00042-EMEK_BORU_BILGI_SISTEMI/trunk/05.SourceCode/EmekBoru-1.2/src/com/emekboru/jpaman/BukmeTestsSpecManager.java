package com.emekboru.jpaman;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.BukmeTestsSpec;

public class BukmeTestsSpecManager extends CommonQueries<BukmeTestsSpec> {

	// itemId sipari� kalemidir.
	public Integer kontrol(Integer prmGlobalId, Integer prmItemId) {

		Integer count = 0;

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query findQuerie = manager.createNamedQuery("BTS.kontrol");
		findQuerie.setParameter("prmGlobalId", prmGlobalId);
		findQuerie.setParameter("prmItemId", prmItemId);
		// testId yi count a esitleyip hem testId cekm�s oluyorum hemde
		// i�erisinde item var m� yok mu kontrol ediyorum
		if (findQuerie.getResultList().isEmpty()) {
			return count;
		} else {
			Number result = (Number) findQuerie.getSingleResult();
			if (result.equals(null)) {

			} else {
				count = result.intValue();
				if (count.equals(null)) {
					count = 0;
				}
			}
		}
		manager.getTransaction().commit();
		manager.close();
		return count;
	}

	@SuppressWarnings("unchecked")
	public List<BukmeTestsSpec> seciliBukmeTestTanim(Integer prmGlobalId,
			Integer prmItemId) {

		List<BukmeTestsSpec> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("BTS.seciliBukmeTestTanim")
					.setParameter("prmGlobalId", prmGlobalId)
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;

	}

	@SuppressWarnings("unchecked")
	public List<BukmeTestsSpec> findByItemId(Integer prmItemId) {

		List<BukmeTestsSpec> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("BTS.findByItemId")
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;

	}

}
