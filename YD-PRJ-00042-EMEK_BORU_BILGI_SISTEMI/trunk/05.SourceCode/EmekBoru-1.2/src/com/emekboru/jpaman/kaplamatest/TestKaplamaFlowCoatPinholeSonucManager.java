/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaFlowCoatPinholeSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaFlowCoatPinholeSonucManager extends
		CommonQueries<TestKaplamaFlowCoatPinholeSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaFlowCoatPinholeSonuc> getAllTestKaplamaFlowCoatPinholeSonuc(
			int globalId, Integer pipeId) {

		List<TestKaplamaFlowCoatPinholeSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestKaplamaFlowCoatPinholeSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
