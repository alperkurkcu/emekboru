/**
 * 
 */
package com.emekboru.jpaman.satinalma;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.satinalma.SatinalmaSiparisMektubu;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class SatinalmaSiparisMektubuManager extends
		CommonQueries<SatinalmaSiparisMektubu> {

	@SuppressWarnings("unchecked")
	public List<SatinalmaSiparisMektubu> findByKararId(Integer prmKararId) {

		List<SatinalmaSiparisMektubu> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("SatinalmaSiparisMektubu.findByKararId")
					.setParameter("prmDosyaId", prmKararId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
