/**
 * 
 */
package com.emekboru.jpaman.coatingmaterials;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.coatingmaterials.KaplamaMalzemeKullanmaIcKumlama;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KaplamaMalzemeKullanmaIcKumlamaManager extends
		CommonQueries<KaplamaMalzemeKullanmaIcKumlama> {

	@SuppressWarnings("unchecked")
	public static List<KaplamaMalzemeKullanmaIcKumlama> getAllKaplamaMalzemeKullanmaIcKumlama() {

		List<KaplamaMalzemeKullanmaIcKumlama> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery(
					"KaplamaMalzemeKullanmaIcKumlama.findAll").getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
