package com.emekboru.jpaman;

public class WhereAnyArgs<T> {

	private String firstFieldName;
	private Boolean firstIsPk;
	private String secondFieldName;
	private Boolean secondIsPk;
	private String thirdFieldName;
	private Boolean thirdIsPk;
	private T thirdFieldValue;
	
	public WhereAnyArgs(Builder<T> builder){
		this.firstFieldName 	= builder.firstFieldName;
		this.firstIsPk 			= builder.firstIsPk;
		this.secondFieldName 	= builder.secondFieldName;
		this.secondIsPk 		= builder.secondIsPk;
		this.thirdFieldName 	= builder.thirdFieldName;
		this.thirdIsPk 			= builder.thirdIsPk;
		this.thirdFieldValue 	= builder.thirdFieldValue;
	}
	
	public static class Builder<T>{
		
		private String firstFieldName;
		private Boolean firstIsPk;
		private String secondFieldName;
		private Boolean secondIsPk;
		private String thirdFieldName;
		private Boolean thirdIsPk;
		private T thirdFieldValue;
		
		public Builder<T> setFirstFieldName(String firstFieldName) {
			this.firstFieldName = firstFieldName;
			return this;
		}

		public Builder<T> setFirstIsPk(Boolean firstIsPk) {
			this.firstIsPk = firstIsPk;
			return this;
		}

		public Builder<T> setSecondFieldName(String secondFieldName) {
			this.secondFieldName = secondFieldName;
			return this;
		}

		public Builder<T> setSecondIsPk(Boolean secondIsPk) {
			this.secondIsPk = secondIsPk;
			return this;
		}

		public Builder<T> setThirdFieldName(String thirdFieldName) {
			this.thirdFieldName = thirdFieldName;
			return this;
		}

		public Builder<T> setThirdIsPk(Boolean thirdIsPk) {
			this.thirdIsPk = thirdIsPk;
			return this;
		}

		public Builder<T> setThirdFieldValue(T thirdFieldValue) {
			this.thirdFieldValue = thirdFieldValue;
			return this;
		}
		
		public WhereAnyArgs<T> build(){
			return new WhereAnyArgs<T>(this);
		}
	}


	public String getFirstFieldName() {
		return firstFieldName;
	}

	public Boolean getFirstIsPk() {
		return firstIsPk;
	}

	public String getSecondFieldName() {
		return secondFieldName;
	}

	public Boolean getSecondIsPk() {
		return secondIsPk;
	}

	public String getThirdFieldName() {
		return thirdFieldName;
	}

	public Boolean getThirdIsPk() {
		return thirdIsPk;
	}

	public T getThirdFieldValue() {
		return thirdFieldValue;
	}
	
}
