package com.emekboru.jpaman.employee;

import com.emekboru.jpa.employee.EmployeeAddress;
import com.emekboru.jpaman.CommonQueries;

public class EmployeeAddressManager extends CommonQueries<EmployeeAddress> {
	
	/* (non-Javadoc)
	 * @see com.emekboru.jpaman.CommonQueries#enterNew(java.lang.Object)
	 */
	@Override
	public boolean enterNew(EmployeeAddress object) {
		object.setIsDefault(false);
		return super.enterNew(object);
	}

}
