/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaFlowCoatBuchholzSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaFlowCoatBuchholzSonucManager extends
		CommonQueries<TestKaplamaFlowCoatBuchholzSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaFlowCoatBuchholzSonuc> getAllTestKaplamaFlowCoatBuchholzSonuc(
			int globalId, Integer pipeId) {

		List<TestKaplamaFlowCoatBuchholzSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestKaplamaFlowCoatBuchholzSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
