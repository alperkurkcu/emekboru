package com.emekboru.jpaman;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.TestCekmeSonuc;
import com.emekboru.jpa.sales.order.SalesItem;

public class TestCekmeSonucManager extends CommonQueries<TestCekmeSonuc> {

	@SuppressWarnings({ "unchecked" })
	public static List<TestCekmeSonuc> getAllTestCekmeSonuc(Integer globalId,
			Integer pipeId) {

		List<TestCekmeSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestCekmeSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<TestCekmeSonuc> getBySalesItemGlobalId(
			SalesItem prmSalesItem, int prmGlobalId) {

		List<TestCekmeSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("TestCekmeSonuc.getBySalesItemGlobalId")
					.setParameter("prmSalesItem", prmSalesItem.getItemId())
					.setParameter("prmGlobalId", prmGlobalId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<TestCekmeSonuc> getBySalesItem(SalesItem prmSalesItem) {

		List<TestCekmeSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestCekmeSonuc.getBySalesItem")
					.setParameter("prmSalesItem", prmSalesItem.getItemId())
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<TestCekmeSonuc> getByDokumNo(String prmDokumNo) {

		List<TestCekmeSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestCekmeSonuc.getByDokumNo")
					.setParameter("prmDokumNo", prmDokumNo).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<TestCekmeSonuc> getByDokumNoSalesItem(String prmDokumNo,
			Integer prmItemId) {

		List<TestCekmeSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("TestCekmeSonuc.getByDokumNoSalesItem")
					.setParameter("prmDokumNo", prmDokumNo)
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
