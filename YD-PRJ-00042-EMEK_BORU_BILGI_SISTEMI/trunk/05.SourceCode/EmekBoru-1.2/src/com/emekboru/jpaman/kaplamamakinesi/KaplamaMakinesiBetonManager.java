/**
 * 
 */
package com.emekboru.jpaman.kaplamamakinesi;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiBeton;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KaplamaMakinesiBetonManager extends
		CommonQueries<KaplamaMakinesiBeton> {

	@SuppressWarnings("unchecked")
	public static List<KaplamaMakinesiBeton> getAllKaplamaMakinesiBeton(
			Integer prmPipeId) {

		List<KaplamaMakinesiBeton> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("KaplamaMakinesiBeton.findAllByPipeId")
					.setParameter("prmPipeId", prmPipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
