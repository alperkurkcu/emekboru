/**
 * 
 */
package com.emekboru.jpaman.satinalma;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.satinalma.SatinalmaOdemeBildirimiIcerik;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class SatinalmaOdemeBildirimiIcerikManager extends
		CommonQueries<SatinalmaOdemeBildirimiIcerik> {

	@SuppressWarnings("unchecked")
	public List<SatinalmaOdemeBildirimiIcerik> findByBildirimId(
			Integer prmBildirimId) {

		List<SatinalmaOdemeBildirimiIcerik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaOdemeBildirimiIcerik.findByBildirimId")
					.setParameter("prmBildirimId", prmBildirimId)
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
