package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaKumlanmisBoruTuzMiktariSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class TestKaplamaKumlanmisBoruTuzMiktariSonucManager extends
		CommonQueries<TestKaplamaKumlanmisBoruTuzMiktariSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaKumlanmisBoruTuzMiktariSonuc> getAllTestKaplamaKumlanmisBoruTuzMiktariSonuc(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaKumlanmisBoruTuzMiktariSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestKaplamaKumlanmisBoruTuzMiktariSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
