/**
 * 
 */
package com.emekboru.jpaman.satinalma;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.satinalma.SatinalmaMalzemeHizmetTalepFormuIcerik;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class SatinalmaMalzemeHizmetTalepFormuIcerikManager extends
		CommonQueries<SatinalmaMalzemeHizmetTalepFormuIcerik> {

	@SuppressWarnings({ "unchecked" })
	public List<SatinalmaMalzemeHizmetTalepFormuIcerik> getAllSatinalmaMalzemeHizmetTalepFormuIcerik(
			Integer formId) {

		List<SatinalmaMalzemeHizmetTalepFormuIcerik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaMalzemeHizmetTalepFormuIcerik.findAll")
					.setParameter("prmFormId", formId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings({ "unchecked" })
	public List<SatinalmaMalzemeHizmetTalepFormuIcerik> getAllSatinalmaMalzemeHizmetTalepFormuIcerikTrue(
			Integer formId) {

		List<SatinalmaMalzemeHizmetTalepFormuIcerik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaMalzemeHizmetTalepFormuIcerik.findAllTrue")
					.setParameter("prmFormId", formId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
