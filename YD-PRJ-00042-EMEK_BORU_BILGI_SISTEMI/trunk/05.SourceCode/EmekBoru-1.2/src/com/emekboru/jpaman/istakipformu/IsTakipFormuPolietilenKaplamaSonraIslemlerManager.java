/**
 * 
 */
package com.emekboru.jpaman.istakipformu;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.istakipformu.IsTakipFormuPolietilenKaplamaSonraIslemler;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class IsTakipFormuPolietilenKaplamaSonraIslemlerManager extends
		CommonQueries<IsTakipFormuPolietilenKaplamaSonraIslemler> {

	@SuppressWarnings("unchecked")
	public static List<IsTakipFormuPolietilenKaplamaSonraIslemler> getAllIsTakipFormuPolietilenKaplamaSonraIslemler(
			Integer itemId) {

		List<IsTakipFormuPolietilenKaplamaSonraIslemler> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"IsTakipFormuPolietilenKaplamaSonraIslemler.findAllByItemId")
					.setParameter("prmItemId", itemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
