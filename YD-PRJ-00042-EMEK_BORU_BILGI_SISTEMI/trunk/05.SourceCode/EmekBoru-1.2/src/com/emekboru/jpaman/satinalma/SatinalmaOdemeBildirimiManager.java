/**
 * 
 */
package com.emekboru.jpaman.satinalma;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.satinalma.SatinalmaOdemeBildirimi;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class SatinalmaOdemeBildirimiManager extends
		CommonQueries<SatinalmaOdemeBildirimi> {

	@SuppressWarnings("unchecked")
	public List<SatinalmaOdemeBildirimi> getAllSatinalmaOdemeBildirimi() {

		List<SatinalmaOdemeBildirimi> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("SatinalmaOdemeBildirimi.findAll")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
