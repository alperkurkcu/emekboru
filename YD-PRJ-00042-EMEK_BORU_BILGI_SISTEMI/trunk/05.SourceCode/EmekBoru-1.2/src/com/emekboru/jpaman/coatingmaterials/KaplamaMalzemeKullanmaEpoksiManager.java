/**
 * 
 */
package com.emekboru.jpaman.coatingmaterials;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.coatingmaterials.KaplamaMalzemeKullanmaEpoksi;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KaplamaMalzemeKullanmaEpoksiManager extends
		CommonQueries<KaplamaMalzemeKullanmaEpoksi> {

	@SuppressWarnings("unchecked")
	public static List<KaplamaMalzemeKullanmaEpoksi> getAllKaplamaMalzemeKullanmaEpoksi() {

		List<KaplamaMalzemeKullanmaEpoksi> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("KaplamaMalzemeKullanmaEpoksi.findAll")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
