/**
 * 
 */
package com.emekboru.jpaman.employee;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import com.emekboru.jpa.Employee;
import com.emekboru.jpa.employee.EmployeeAdvanceRequest;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author kursat
 * 
 */
public class EmployeeAdvanceManager extends
		CommonQueries<EmployeeAdvanceRequest> {

	public List<EmployeeAdvanceRequest> findAllAdvanceRequestsOfEmployee(
			Employee employee) {

		return super.findByField(EmployeeAdvanceRequest.class, "employee.employeeId",
				employee.getEmployeeId());
	}

	public List<EmployeeAdvanceRequest> findAdvanceRequestsOfSubworkersOfEmployee(
			Employee employee) {

		return super.findByField(EmployeeAdvanceRequest.class, "supervisor.employeeId",
				employee.getEmployeeId());
	}

	public List<EmployeeAdvanceRequest> findAdvanceRequestsOfSubworkersOfSubworkersOfEmployee(
			Employee employee) {

		return super.findByField(EmployeeAdvanceRequest.class,
				"supervisorsSupervisor.employeeId", employee.getEmployeeId());
	}

	public List<EmployeeAdvanceRequest> findAllAdvanceRequestsToEmployee(
			Employee employee) {

		List<EmployeeAdvanceRequest> allRequestsList = new ArrayList<EmployeeAdvanceRequest>();
		List<EmployeeAdvanceRequest> subworkersRequestsList = this
				.findAdvanceRequestsOfSubworkersOfEmployee(employee);
		List<EmployeeAdvanceRequest> subworkersOfSubworkersRequestsList = this
				.findAdvanceRequestsOfSubworkersOfSubworkersOfEmployee(employee);

		allRequestsList.addAll(subworkersRequestsList);
		allRequestsList.addAll(subworkersOfSubworkersRequestsList);

		return allRequestsList;
	}
	
	public List<EmployeeAdvanceRequest> findAll() {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String name = EmployeeAdvanceRequest.class.getName();
		String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
		String query = "SELECT c FROM " + simpleClassName + " c ";
		Query findQuery = manager.createQuery(query);
		
		@SuppressWarnings("unchecked")
		List<EmployeeAdvanceRequest> returnedList = findQuery.getResultList();
		tx.commit();
		manager.close();
		return returnedList;
	}
}
