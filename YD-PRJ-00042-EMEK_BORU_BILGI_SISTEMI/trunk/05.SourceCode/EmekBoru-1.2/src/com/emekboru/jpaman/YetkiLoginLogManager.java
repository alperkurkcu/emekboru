package com.emekboru.jpaman;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.YetkiLoginLog;
import com.emekboru.utils.UtilInsCore;

public class YetkiLoginLogManager extends CommonQueries<YetkiLoginLog> {

	@SuppressWarnings("unused")
	public void updateTimeOutOlanlarLogOut(String sessionId) throws Throwable {
		try {
			EntityManager em = Factory.getInstance().createEntityManager();
			em.getTransaction().begin();
			Query query = em
					.createNamedQuery("YetkiLoginLog.updateTimeOutOlanlarLogOut");
			query.setParameter("cikisZamani", UtilInsCore.getTarihZaman());
			query.setParameter("sessionId", sessionId);
			int a = query.executeUpdate();
			em.getTransaction().commit();
			em.close();

		} catch (RuntimeException rte) {
			throw rte;
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}

}
