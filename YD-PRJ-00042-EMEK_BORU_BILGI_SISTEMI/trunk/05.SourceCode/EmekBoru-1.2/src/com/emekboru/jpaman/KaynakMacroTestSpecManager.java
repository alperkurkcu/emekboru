package com.emekboru.jpaman;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.KaynakMakroTestSpec;

public class KaynakMacroTestSpecManager extends
		CommonQueries<KaynakMakroTestSpec> {

	// itemId sipari� kalemidir.
	@SuppressWarnings("unchecked")
	public Integer silmeKontrolu(Integer globalId, Integer itemId) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query findQuerie = manager.createNamedQuery("KMTS.silmeKontrolu");
		findQuerie.setParameter("prmGlobalId", globalId);
		findQuerie.setParameter("prmItemId", itemId);
		List<KaynakMakroTestSpec> resultList = findQuerie.getResultList();
		Integer count = resultList.size();
		if (count.equals(null)) {
			count = 0;
		}
		manager.getTransaction().commit();
		manager.close();
		return count;
	}

	@SuppressWarnings("unchecked")
	public List<KaynakMakroTestSpec> seciliKaynakMakroTestTanim(
			Integer globalId, Integer prmItemId) {

		List<KaynakMakroTestSpec> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("KMTS.seciliKaynakMakroTestTanim")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;

	}

	@SuppressWarnings("unchecked")
	public List<KaynakMakroTestSpec> findByItemId(Integer prmItemId) {

		List<KaynakMakroTestSpec> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("KMTS.findByItemId")
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;

	}

}
