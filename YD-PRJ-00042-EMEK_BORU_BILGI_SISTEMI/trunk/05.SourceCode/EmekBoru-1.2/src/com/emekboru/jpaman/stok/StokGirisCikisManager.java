/**
 * 
 */
package com.emekboru.jpaman.stok;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedProperty;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.primefaces.model.SortOrder;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.stok.StokGirisCikis;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class StokGirisCikisManager extends CommonQueries<StokGirisCikis> {

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	@SuppressWarnings("unchecked")
	public List<StokGirisCikis> getAllStokGirisCikis() {

		List<StokGirisCikis> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("StokGirisCikis.findAll")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("HATA - " + userBean.getUsername()
					+ " - StokGirisCikisManager.getAllStokGirisCikis");
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<StokGirisCikis> findLimited() {

		List<StokGirisCikis> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("StokGirisCikis.findAll")
					.setFirstResult(0).setMaxResults(100).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<Long> getGiris(Integer joinId) {

		List<Long> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			// result = em.createNamedQuery("StokGirisCikis.findGiris")
			// .setParameter("prmUrunId", urunId).getResultList();
			result = em.createNamedQuery("StokGirisCikis.findGirisJoinId")
					.setParameter("prmJoinId", joinId).getResultList();
			if (result.isEmpty())
				result.add((long) 0);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<Long> getCikis(Integer joinId) {

		List<Long> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			// result = em.createNamedQuery("StokGirisCikis.findCikis")
			// .setParameter("prmUrunId", urunId).getResultList();
			result = em.createNamedQuery("StokGirisCikis.findCikisJoinId")
					.setParameter("prmJoinId", joinId).getResultList();
			if (result.isEmpty())
				result.add((long) 0);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<StokGirisCikis> getStokGirisCikisListesi(String tarihIlk,
			String tarihSon, Integer depo, String islemDurumu,
			Integer teslimEden, Integer teslimAlan) {

		List<StokGirisCikis> result = null;

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		try {
			String query = new String();
			query = "SELECT c FROM StokGirisCikis c WHERE 1=1";
			if (tarihIlk != null && tarihSon != null) {
				query = query + " AND c.eklemeZamani BETWEEN '" + tarihIlk
						+ "' AND '" + tarihSon + "'";
			}
			if (depo != null) {
				query = query + " AND c.stokTanimlarDepoTanimlari.id=" + depo;
			}
			if (islemDurumu != null) {
				query = query + " AND c.islemDurumu=" + islemDurumu;
			}
			if (teslimEden != null) {
				query = query + " AND c.teslimEdenKullanici.employeeId="
						+ teslimEden;
			}
			if (teslimAlan != null) {
				query = query + " AND c.teslimAlanKullanici.employeeId="
						+ teslimAlan;
			}
			query = query + " ORDER BY c.eklemeZamani ";

			System.out.println("final query: " + query);

			Query findQuery = manager.createQuery(query);
			result = findQuery.getResultList();
			manager.getTransaction().commit();
			manager.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (result.size() == 0)
			return null;
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<StokGirisCikis> getHareketListesi(Integer projeId,
			Integer evrakNo, String urunAdi, String urunKodu, String tarihIlk,
			String tarihSon, Integer depo, String islemDurumu, Integer sarfId,
			Integer urunGrubu) {

		List<StokGirisCikis> result = null;

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		try {
			String query = new String();
			query = "SELECT c FROM StokGirisCikis c WHERE 1=1";
			if (tarihIlk != null && tarihSon != null) {
				query = query + " AND c.eklemeZamani BETWEEN '" + tarihIlk
						+ "0:00' AND '" + tarihSon + "0:00'";
			}
			if (depo != null) {
				query = query + " AND c.stokTanimlarDepoTanimlari.id=" + depo;
			}
			if (islemDurumu != null) {
				query = query + " AND c.islemDurumu=" + islemDurumu;
			}
			query = query + " ORDER BY c.eklemeZamani ";

			System.out.println("final query: " + query);

			Query findQuery = manager.createQuery(query);
			result = findQuery.getResultList();
			manager.getTransaction().commit();
			manager.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (result.size() == 0)
			return null;
		return result;
	}

	public List<StokGirisCikis> getDurumRaporu(String tarihIlk,
			String tarihSon, Integer depo, String islemDurumu,
			Integer teslimEden, Integer teslimAlan) {

		List<StokGirisCikis> result = null;

		return result;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getKritikStokManuelSQL() {

		List<Object[]> result = null;

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		String query = new String();

		try {
			// query =
			// "select A.GIRIS, A.ADI, A.DEPO, A.GRUBU, A.TOPLAM_GIRIS,COALESCE(B.TOPLAM_CIKIS, 0) AS TOPLAM_CIKIS,A.TOPLAM_GIRIS - COALESCE(B.TOPLAM_CIKIS, 0) AS TOPLAM, A.MIN_STOK, A.MAX_STOK, A.MIN_STOK - (A.TOPLAM_GIRIS - COALESCE(B.TOPLAM_CIKIS, 0)) AS MIN_GEREK, A.MAX_STOK - (A.TOPLAM_GIRIS - COALESCE(B.TOPLAM_CIKIS, 0)) AS MAX_GEREK, A.BIRIM from "
			// +
			// "(select s2.urun_kodu AS GIRIS, s2.urun_adi AS ADI, s3.depo_adi AS DEPO, s4.urun_grubu AS GRUBU, SUM(s1.miktar) AS TOPLAM_GIRIS, s2.min_stok, s2.max_stok, s5.birim AS BIRIM from stok_giris_cikis s1, stok_urun_kartlari s2, stok_tanimlar_depo_tanimlari s3,stok_tanimlar_urun_grubu s4, stok_tanimlar_birim s5 where s2.id=s1.urun_id and s1.depo = s3.id and s2.grup_id=s4.id and s2.birim_id=s5.id and s1.islem_durumu=1 GROUP BY s2.urun_kodu, s2.urun_adi , s3.depo_adi, s4.urun_grubu, s2.min_stok, s2.max_stok, s5.birim ORDER BY s2.urun_kodu"
			// + ") A LEFT JOIN "
			// +
			// "(select s2.urun_kodu AS CIKIS, s2.urun_adi AS ADI, s3.depo_adi AS DEPO, s4.urun_grubu AS GRUBU, SUM(s1.miktar) AS TOPLAM_CIKIS, s2.min_stok, s2.max_stok, s5.birim AS BIRIM from stok_giris_cikis s1, stok_urun_kartlari s2, stok_tanimlar_depo_tanimlari s3,stok_tanimlar_urun_grubu s4, stok_tanimlar_birim s5 where s2.id=s1.urun_id and s1.depo = s3.id and s2.grup_id=s4.id and s2.birim_id=s5.id and s1.islem_durumu=0 GROUP BY s2.urun_kodu, s2.urun_adi , s3.depo_adi, s4.urun_grubu, s2.min_stok, s2.max_stok, s5.birim ORDER BY s2.urun_kodu"
			// + ") B ON A.GIRIS = B.CIKIS";
			query = "select A.GIRIS, A.ADI, A.DEPO, A.GRUBU, A.TOPLAM_GIRIS,COALESCE(B.TOPLAM_CIKIS, 0) AS TOPLAM_CIKIS,A.TOPLAM_GIRIS - COALESCE(B.TOPLAM_CIKIS, 0) AS TOPLAM, A.MIN_STOK, A.MAX_STOK, A.MIN_STOK - (A.TOPLAM_GIRIS - COALESCE(B.TOPLAM_CIKIS, 0)) AS MIN_GEREK, A.MAX_STOK - (A.TOPLAM_GIRIS - COALESCE(B.TOPLAM_CIKIS, 0)) AS MAX_GEREK, A.BIRIM, A.ADA, A.RAF, A.GOZ from "
					+ "(select s2.urun_kodu AS GIRIS, s2.urun_adi AS ADI, s3.depo_adi AS DEPO, s4.urun_grubu AS GRUBU, SUM(s1.miktar) AS TOPLAM_GIRIS, s2.min_stok, s2.max_stok, s5.birim AS BIRIM, s2.ada_no AS ADA, s6.raf AS RAF, s2.goz_no AS GOZ from stok_giris_cikis s1, stok_urun_kartlari s2, stok_tanimlar_depo_tanimlari s3,stok_tanimlar_urun_grubu s4, stok_tanimlar_birim s5, stok_tanimlar_raf s6 where s2.id=s1.urun_id and s1.depo = s3.id and s2.grup_id=s4.id and s2.birim_id=s5.id and s1.islem_durumu='1' AND s2.raf_id = s6.id GROUP BY s2.urun_kodu, s2.urun_adi , s3.depo_adi, s4.urun_grubu, s2.min_stok, s2.max_stok, s5.birim, s6.raf,	s2.ada_no, s2.goz_no ORDER BY s2.urun_kodu"
					+ ") A LEFT JOIN "
					+ "(select s2.urun_kodu AS CIKIS, s2.urun_adi AS ADI, s3.depo_adi AS DEPO, s4.urun_grubu AS GRUBU, SUM(s1.miktar) AS TOPLAM_CIKIS, s2.min_stok, s2.max_stok, s5.birim AS BIRIM, s2.ada_no AS ADA, s6.raf AS RAF, s2.goz_no AS GOZ from stok_giris_cikis s1, stok_urun_kartlari s2, stok_tanimlar_depo_tanimlari s3,stok_tanimlar_urun_grubu s4, stok_tanimlar_birim s5, stok_tanimlar_raf s6 where s2.id=s1.urun_id and s1.depo = s3.id and s2.grup_id=s4.id and s2.birim_id=s5.id and s1.islem_durumu='0' AND s2.raf_id = s6.id GROUP BY s2.urun_kodu, s2.urun_adi , s3.depo_adi, s4.urun_grubu, s2.min_stok, s2.max_stok, s5.birim , s6.raf, s2.ada_no, s2.goz_no ORDER BY s2.urun_kodu"
					+ ") B ON A.GIRIS = B.CIKIS";
			Query findQuery = manager.createNativeQuery(query);
			result = findQuery.getResultList();
			manager.getTransaction().commit();
			manager.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<StokGirisCikis> getKritikStok() {

		List<StokGirisCikis> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("StokGirisCikis.kritikStok")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public Long getUrunGiris(Integer urunId) {

		List<Long> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("StokGirisCikis.urunGirisCikisMktari")
					.setParameter("urunKartiId", urunId)
					.setParameter("islemDurumu", "1").getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		if (result.get(0) == null)
			return (long) 0;

		return (long) result.get(0);
	}

	@SuppressWarnings("unchecked")
	public Long getUrunCikis(Integer urunId) {

		List<Long> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("StokGirisCikis.urunGirisCikisMktari")
					.setParameter("urunKartiId", urunId)
					.setParameter("islemDurumu", "0").getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		if (result.get(0) == null)
			return (long) 0;

		return result.get(0);
	}

	@SuppressWarnings("unchecked")
	public List<StokGirisCikis> silmeKontrol(Integer prmUrunId) {

		List<StokGirisCikis> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("StokGirisCikis.findByUrunId")
					.setParameter("prmUrunId", prmUrunId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getStokHareketListManualSQL(String tarihIlk,
			String tarihSon) {

		List<Object[]> result = null;

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		String query = new String();

		try {
			query = "select A.GIRIS, A.ADI, A.DEPO, A.GRUBU, A.TOPLAM_GIRIS,COALESCE(B.TOPLAM_CIKIS, 0) AS TOPLAM_CIKIS,A.TOPLAM_GIRIS - COALESCE(B.TOPLAM_CIKIS, 0) AS TOPLAM, A.MIN_STOK, A.MAX_STOK, A.MIN_STOK - (A.TOPLAM_GIRIS - COALESCE(B.TOPLAM_CIKIS, 0)) AS MIN_GEREK, A.MAX_STOK - (A.TOPLAM_GIRIS - COALESCE(B.TOPLAM_CIKIS, 0)) AS MAX_GEREK, A.BIRIM, A.ADA, A.RAF, A.GOZ from "
					+ "(select s2.urun_kodu AS GIRIS, s2.urun_adi AS ADI, s3.depo_adi AS DEPO, s4.urun_grubu AS GRUBU, SUM(s1.miktar) AS TOPLAM_GIRIS, s2.min_stok, s2.max_stok, s5.birim AS BIRIM, s2.ada_no AS ADA, s6.raf AS RAF, s2.goz_no AS GOZ from stok_giris_cikis s1, stok_urun_kartlari s2, stok_tanimlar_depo_tanimlari s3,stok_tanimlar_urun_grubu s4, stok_tanimlar_birim s5, stok_tanimlar_raf s6 where s2.id=s1.urun_id and s1.depo = s3.id and s2.grup_id=s4.id and s2.birim_id=s5.id and s1.islem_durumu='1' AND s2.raf_id = s6.id AND s1.ekleme_zamani BETWEEN '"
					+ tarihIlk
					+ "' AND '"
					+ tarihSon
					+ "' GROUP BY s2.urun_kodu, s2.urun_adi , s3.depo_adi, s4.urun_grubu, s2.min_stok, s2.max_stok, s5.birim, s6.raf,	s2.ada_no, s2.goz_no ORDER BY s2.urun_kodu"
					+ ") A LEFT JOIN "
					+ "(select s2.urun_kodu AS CIKIS, s2.urun_adi AS ADI, s3.depo_adi AS DEPO, s4.urun_grubu AS GRUBU, SUM(s1.miktar) AS TOPLAM_CIKIS, s2.min_stok, s2.max_stok, s5.birim AS BIRIM, s2.ada_no AS ADA, s6.raf AS RAF, s2.goz_no AS GOZ from stok_giris_cikis s1, stok_urun_kartlari s2, stok_tanimlar_depo_tanimlari s3,stok_tanimlar_urun_grubu s4, stok_tanimlar_birim s5, stok_tanimlar_raf s6 where s2.id=s1.urun_id and s1.depo = s3.id and s2.grup_id=s4.id and s2.birim_id=s5.id and s1.islem_durumu='0' AND s2.raf_id = s6.id AND s1.ekleme_zamani BETWEEN '"
					+ tarihIlk
					+ "' AND '"
					+ tarihSon
					+ "' GROUP BY s2.urun_kodu, s2.urun_adi , s3.depo_adi, s4.urun_grubu, s2.min_stok, s2.max_stok, s5.birim , s6.raf, s2.ada_no, s2.goz_no ORDER BY s2.urun_kodu"
					+ ") B ON A.GIRIS = B.CIKIS";
			Query findQuery = manager.createNativeQuery(query);
			result = findQuery.getResultList();
			manager.getTransaction().commit();
			manager.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;

	}

	@SuppressWarnings("unchecked")
	public List<StokGirisCikis> findRangeFilter(int start, int pagesize,
			ArrayList<String> alanlar, ArrayList<String> veriler,
			String sortField, SortOrder sortOrder) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		String hsql = "select r from StokGirisCikis r where 1=1 ";
		if (alanlar != null && alanlar.size() > 0) {
			for (int i = 0; i < alanlar.size(); i++) {
				if (alanlar.get(i) != null && !alanlar.get(i).equals(""))
					hsql += "and r." + alanlar.get(i) + " like '%"
							+ veriler.get(i) + "%' ";
			}
		}
		String siralama;
		if (sortField != null && !sortField.equals("")) {
			if (sortOrder.ordinal() == 1) {
				siralama = " desc";
			} else {
				siralama = " asc";
			}
			hsql += "order by r." + sortField + siralama;
		} else
			hsql += "order by r.id desc";

		System.out.println(hsql);

		Query query = manager.createQuery(hsql);
		query.setMaxResults(pagesize);
		query.setFirstResult(start);
		List<StokGirisCikis> list = query.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return list;
	}

	public int countTotalRecord() {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query query = manager.createNamedQuery("StokGirisCikis.totalAll");
		Number result = (Number) query.getSingleResult();
		int rr = result.intValue();
		manager.getTransaction().commit();
		manager.close();
		return rr;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

}
