/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaBetonGozKontrolSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author YD-PC
 *
 */
public class TestKaplamaBetonGozKontrolSonucManager extends
CommonQueries<TestKaplamaBetonGozKontrolSonuc> {
	
	@SuppressWarnings("unchecked")
	public static List<TestKaplamaBetonGozKontrolSonuc> getAllTestKaplamaBetonGozKontrolSonuc(Integer globalId,
			Integer pipeId) {

		List<TestKaplamaBetonGozKontrolSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestKaplamaBetonGozKontrolSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
