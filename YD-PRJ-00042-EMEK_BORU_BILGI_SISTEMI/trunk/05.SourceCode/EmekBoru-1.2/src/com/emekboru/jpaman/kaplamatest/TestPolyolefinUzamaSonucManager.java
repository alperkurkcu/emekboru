package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestPolyolefinUzamaSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class TestPolyolefinUzamaSonucManager extends
		CommonQueries<TestPolyolefinUzamaSonuc> {
	@SuppressWarnings("unchecked")
	public static List<TestPolyolefinUzamaSonuc> getAllPolyolefinUzamaTestSonuc(
			int globalId, Integer pipeId) {

		List<TestPolyolefinUzamaSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestPolyolefinUzamaSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
