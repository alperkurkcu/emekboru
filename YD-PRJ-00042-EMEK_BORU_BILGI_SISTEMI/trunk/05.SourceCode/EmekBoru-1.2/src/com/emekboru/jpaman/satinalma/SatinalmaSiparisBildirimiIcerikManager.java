/**
 * 
 */
package com.emekboru.jpaman.satinalma;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.satinalma.SatinalmaSiparisBildirimiIcerik;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class SatinalmaSiparisBildirimiIcerikManager extends
		CommonQueries<SatinalmaSiparisBildirimiIcerik> {

	@SuppressWarnings("unchecked")
	public List<SatinalmaSiparisBildirimiIcerik> findByBildirimId(
			Integer prmBildirimId) {

		List<SatinalmaSiparisBildirimiIcerik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaSiparisBildirimiIcerik.findByBildirimId")
					.setParameter("prmBildirimId", prmBildirimId)
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
