/**
 * 
 */
package com.emekboru.jpaman.tahribatsiztestsonuc;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizGozOlcuSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
public class TestTahribatsizGozOlcuSonucManager extends
		CommonQueries<TestTahribatsizGozOlcuSonuc> {

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizGozOlcuSonuc> getAllTestTahribatsizGozOlcuSonuc(
			Integer prmPipeId) {

		List<TestTahribatsizGozOlcuSonuc> result = null;
		EntityManager em = Factory.getInstance().createEntityManager();
		try {
			result = em.createNamedQuery("TestTahribatsizGozOlcuSonuc.findAll")
					.setParameter("prmPipeId", prmPipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizGozOlcuSonuc> getAllTestTahribatsizGozOlcuSonucByItemId(
			Integer prmItemId) {

		List<TestTahribatsizGozOlcuSonuc> result = null;
		EntityManager em = Factory.getInstance().createEntityManager();
		try {
			String query = "SELECT r FROM TestTahribatsizGozOlcuSonuc r WHERE r.pipe.pipeId in (select p.pipeId from Pipe p where p.salesItem.itemId=:prmItemId)";
			result = em.createQuery(query).setParameter("prmItemId", prmItemId)
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizGozOlcuSonuc> findByItemIdSonuc(Integer prmItemId) {

		List<TestTahribatsizGozOlcuSonuc> result = null;
		EntityManager em = Factory.getInstance().createEntityManager();
		try {
			String query = "SELECT r FROM TestTahribatsizGozOlcuSonuc r WHERE r.pipe.pipeId in (select p.pipeId from Pipe p where p.salesItem.itemId=:prmItemId) and r.result=1 order by r.pipe.pipeId ASC";
			result = em.createQuery(query).setParameter("prmItemId", prmItemId)
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizGozOlcuSonuc> getByItemDate(Integer prmItemId,
			Date prmDateA, Date prmDateB, Integer prmSonuc) {

		List<TestTahribatsizGozOlcuSonuc> result = null;

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();

		final String TIME_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
		final SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT);

		try {
			String query = new String();
			query = "SELECT r FROM TestTahribatsizGozOlcuSonuc r WHERE r.pipe.pipeId in (select p.pipeId from Pipe p where p.salesItem.itemId=:prmItemId)";

			query = query + " AND r.boruKabulTarihi BETWEEN '"
					+ sdf.format(prmDateA) + "' AND '" + sdf.format(prmDateB)
					+ "'";

			if (prmSonuc == 1) {
				query = query + " AND r.result=1";
			} else if (prmSonuc == 0) {
				query = query + " AND r.result!=-1";
			}

			query = query + " ORDER BY r.eklemeZamani ";

			System.out.println("final query: " + query);

			Query findQuery = manager.createQuery(query).setParameter(
					"prmItemId", prmItemId);
			result = findQuery.getResultList();

			manager.getTransaction().commit();
			manager.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (result.size() == 0)
			return null;
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizGozOlcuSonuc> findByItemIdSonucAll(
			Integer prmItemId) {

		List<TestTahribatsizGozOlcuSonuc> result = null;
		EntityManager em = Factory.getInstance().createEntityManager();
		try {
			String query = "SELECT r FROM TestTahribatsizGozOlcuSonuc r WHERE r.pipe.pipeId in (select p.pipeId from Pipe p where p.salesItem.itemId=:prmItemId) and r.result!=-1 order by r.pipe.pipeId ASC";
			result = em.createQuery(query).setParameter("prmItemId", prmItemId)
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizGozOlcuSonuc> findByItemIdSonucNoReleaseDate(
			Integer prmItemId) {

		List<TestTahribatsizGozOlcuSonuc> result = null;
		EntityManager em = Factory.getInstance().createEntityManager();
		try {
			String query = "SELECT r FROM TestTahribatsizGozOlcuSonuc r WHERE r.pipe.pipeId in (select p.pipeId from Pipe p where p.salesItem.itemId=:prmItemId and p.releaseNoteTarihi is null) and r.result=1 order by r.pipe.pipeId ASC";
			result = em.createQuery(query).setParameter("prmItemId", prmItemId)
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizGozOlcuSonuc> findByItemIdSonucYesReleaseDate(
			Integer prmItemId) {

		List<TestTahribatsizGozOlcuSonuc> result = null;
		EntityManager em = Factory.getInstance().createEntityManager();
		try {
			String query = "SELECT r FROM TestTahribatsizGozOlcuSonuc r WHERE r.pipe.pipeId in (select p.pipeId from Pipe p where p.salesItem.itemId=:prmItemId  and p.releaseNoteTarihi is not null) and r.result=1 order by r.pipe.pipeId ASC";
			result = em.createQuery(query).setParameter("prmItemId", prmItemId)
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizGozOlcuSonuc> findByItemIdSonucNoUcBirDate(
			Integer prmItemId) {

		List<TestTahribatsizGozOlcuSonuc> result = null;
		EntityManager em = Factory.getInstance().createEntityManager();
		try {
			String query = "SELECT r FROM TestTahribatsizGozOlcuSonuc r WHERE r.pipe.pipeId in (select p.pipeId from Pipe p where p.salesItem.itemId=:prmItemId and p.ucBirTarihi is null) and r.result=1 order by r.pipe.pipeId ASC";
			result = em.createQuery(query).setParameter("prmItemId", prmItemId)
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizGozOlcuSonuc> findByItemIdSonucYesUcBirDate(
			Integer prmItemId) {

		List<TestTahribatsizGozOlcuSonuc> result = null;
		EntityManager em = Factory.getInstance().createEntityManager();
		try {
			String query = "SELECT r FROM TestTahribatsizGozOlcuSonuc r WHERE r.pipe.pipeId in (select p.pipeId from Pipe p where p.salesItem.itemId=:prmItemId  and p.ucBirTarihi is not null) and r.result=1 order by r.pipe.pipeId ASC";
			result = em.createQuery(query).setParameter("prmItemId", prmItemId)
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizGozOlcuSonuc> getByDokumNo(String prmDokumNo) {

		List<TestTahribatsizGozOlcuSonuc> result = null;
		EntityManager em = Factory.getInstance().createEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestTahribatsizGozOlcuSonuc.getByDokumNo")
					.setParameter("prmDokumNo", prmDokumNo).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizGozOlcuSonuc> dunUretilmisBorularIcin(
			Integer prmMachineId) {

		List<TestTahribatsizGozOlcuSonuc> result = null;
		EntityManager em = Factory.getEntityManager();

		String tarihIlk = UtilInsCore.getYesterday().toString()
				.substring(0, 10)
				+ " 08:00:00";
		String tarihSon = UtilInsCore.getTarihZaman().toString()
				.substring(0, 10)
				+ " 07:59:59";

		Timestamp tsIlk = Timestamp.valueOf(tarihIlk);
		Timestamp tsSon = Timestamp.valueOf(tarihSon);

		try {
			String query = "SELECT o FROM TestTahribatsizGozOlcuSonuc mel WHERE mel.pipe.pipeId in (select o.pipe.pipeId from MachinePipeLink o where o.pipe.eklemeZamani BETWEEN :prmDate1 AND :prmDate2 and o.machine.machineId=:prmMachineId) order by mel.pipe.pipeId";
			Query index = em.createQuery(query);
			index.setParameter("prmDate1", tsIlk)
					.setParameter("prmDate2", tsSon)
					.setParameter("prmMachineId", prmMachineId);
			result = index.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizGozOlcuSonuc> tumUretilmisBorularIcin(
			Integer prmSalesItemId, Integer prmMachineId) {

		List<TestTahribatsizGozOlcuSonuc> result = null;
		EntityManager em = Factory.getEntityManager();

		try {
			String query = "SELECT mel FROM TestTahribatsizGozOlcuSonuc mel WHERE mel.pipe.pipeId in (select o.pipe.pipeId from MachinePipeLink o where o.machine.machineId=:prmMachineId and o.pipe.salesItem.itemId=:prmSalesItemId) and mel.boruUzunluk >0 order by mel.pipe.pipeId";
			Query index = em.createQuery(query);
			index.setParameter("prmSalesItemId", prmSalesItemId).setParameter(
					"prmMachineId", prmMachineId);
			result = index.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
