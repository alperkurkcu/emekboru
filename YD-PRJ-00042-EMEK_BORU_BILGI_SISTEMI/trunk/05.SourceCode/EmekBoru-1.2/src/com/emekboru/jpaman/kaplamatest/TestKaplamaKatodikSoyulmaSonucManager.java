/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaKatodikSoyulmaSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaKatodikSoyulmaSonucManager extends
		CommonQueries<TestKaplamaKatodikSoyulmaSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaKatodikSoyulmaSonuc> getAllKatodikSoyulmaSonucTest(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaKatodikSoyulmaSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("TestKaplamaKatodikSoyulmaSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
