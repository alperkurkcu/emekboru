/**
 * 
 */
package com.emekboru.jpaman.satinalma;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedProperty;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.primefaces.model.SortOrder;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.satinalma.SatinalmaAmbarMalzemeTalepForm;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class SatinalmaAmbarMalzemeTalepFormManager extends
		CommonQueries<SatinalmaAmbarMalzemeTalepForm> {

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	@SuppressWarnings("unchecked")
	public List<SatinalmaAmbarMalzemeTalepForm> findLimited() {

		List<SatinalmaAmbarMalzemeTalepForm> result = null;

		try {
			EntityManager em = Factory.getEntityManager();
			result = em
					.createNamedQuery("SatinalmaAmbarMalzemeTalepForm.findAll")
					.setMaxResults(50).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
			result = null;
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaAmbarMalzemeTalepForm> getAllSatinalmaAmbarMalzemeTalepForm() {

		List<SatinalmaAmbarMalzemeTalepForm> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery(
					"SatinalmaAmbarMalzemeTalepForm.findAll").getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out
					.println("HATA - "
							+ userBean.getUsername()
							+ " - SatinalmaAmbarMalzemeTalepFormManager.getAllSatinalmaAmbarMalzemeTalepForm");
			result = null;
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaAmbarMalzemeTalepForm> getUserSatinalmaAmbarMalzemeTalepForm(
			Integer employeeId) {

		List<SatinalmaAmbarMalzemeTalepForm> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaAmbarMalzemeTalepForm.findByUserId")
					.setParameter("prmEmployeeId", employeeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
			result = null;
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaAmbarMalzemeTalepForm> getOnayBekleyenlerSatinalmaAmbarMalzemeTalepForm(
			Integer employeeId) {

		List<SatinalmaAmbarMalzemeTalepForm> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaAmbarMalzemeTalepForm.findOnayBekleyenler")
					.setParameter("prmEmployeeId", employeeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
			result = null;
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaAmbarMalzemeTalepForm> getOnayGecmisSatinalmaAmbarMalzemeTalepForm(
			Integer employeeId) {

		List<SatinalmaAmbarMalzemeTalepForm> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaAmbarMalzemeTalepForm.findHistory")
					.setParameter("prmEmployeeId", employeeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
			result = null;
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaAmbarMalzemeTalepForm> getSatinalmaAmbarMalzemeTalepForm(
			String prmFormNo) {

		List<SatinalmaAmbarMalzemeTalepForm> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaAmbarMalzemeTalepForm.findByFormNo")
					.setParameter("prmFormNo", prmFormNo).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
			result = null;
		}
		return result;
	}

	@SuppressWarnings({ "unused", "unchecked", "deprecation" })
	public String getOtomatikSayi() {

		List<SatinalmaAmbarMalzemeTalepForm> result = null;
		Integer sayi = null;
		String sonuc = null;
		String sonSayi = null;
		Integer dateYear1 = null;
		Integer dateYear2 = null;

		DateFormat dateFormat = new SimpleDateFormat("yyyy");
		Date date = new Date();

		dateYear1 = date.getYear() + 1900;
		dateYear2 = date.getYear() + 1901;

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		try {
			String query = new String();
			query = "SELECT c FROM SatinalmaAmbarMalzemeTalepForm c WHERE c.id=(Select max(d.id) from SatinalmaAmbarMalzemeTalepForm d) and c.eklemeZamani BETWEEN '"
					+ dateYear1 + ".01.01' AND '" + dateYear2 + ".01.01'";

			Query findQuery = manager.createQuery(query);
			result = findQuery.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (result.size() == 0) {
			return sonuc = "ATF-" + dateFormat.format(date) + "/"
					+ (new DecimalFormat("0000000").format(1));
		} else {
			sayi = Integer.parseInt((result.get(0).getFormSayisi()).substring(
					9, 16)) + 1;
			return sonuc = "ATF-" + dateFormat.format(date) + "/"
					+ (new DecimalFormat("0000000").format(sayi));
		}
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<SatinalmaAmbarMalzemeTalepForm> findByYear() {

		List<SatinalmaAmbarMalzemeTalepForm> result = null;
		EntityManager em = Factory.getEntityManager();

		Integer prmYear = null;
		Integer prmMonth = null;
		Date date = new Date();

		Calendar beginCalendar = Calendar.getInstance();
		beginCalendar.setTime(date);
		beginCalendar.add(Calendar.MONTH, -2);

		prmYear = beginCalendar.getTime().getYear() + 1900;
		prmMonth = beginCalendar.getTime().getMonth();

		try {
			String query = new String();
			query = "SELECT r FROM SatinalmaAmbarMalzemeTalepForm r where r.eklemeZamani > '"
					+ prmYear
					+ "."
					+ prmMonth
					+ ".31' AND r.satinalmaDurum.id <> 14 order by r.eklemeZamani DESC";

			Query findQuery = em.createQuery(query);
			result = findQuery.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<Integer> findEmpity() {

		List<Integer> result = null;
		EntityManager em = Factory.getEntityManager();
		try {

			Query index = em
					.createNativeQuery("select s.id from satinalma_ambar_malzeme_talep_form s LEFT JOIN satinalma_ambar_malzeme_talep_form_icerik i on s.id=i.form_id where i.form_id is null");
			result = index.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
			result = null;
		}
		return result;
	}

	public void deleteEmpity(Integer silinecek) {
		EntityManager em = Factory.getEntityManager();
		Factory.beginTransaction();
		Query query;
		try {
			query = em
					.createNativeQuery("DELETE FROM satinalma_ambar_malzeme_talep_form where id="
							+ silinecek);
			query.executeUpdate();
			Factory.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaAmbarMalzemeTalepForm> getOpenedFinishedSatinalmaAmbarMalzemeTalepForm() {

		List<SatinalmaAmbarMalzemeTalepForm> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery(
					"SatinalmaAmbarMalzemeTalepForm.findOpenedFinished")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
			result = null;
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaAmbarMalzemeTalepForm> findRangeFilter(int start,
			int pagesize, ArrayList<String> alanlar, ArrayList<String> veriler,
			String sortField, SortOrder sortOrder, int prmEmployeeId,
			boolean prmOlder) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();

		Query query = null;
		String hsql = null;
		if (prmOlder) {
			hsql = "SELECT r FROM SatinalmaAmbarMalzemeTalepForm r where r.supervisor.employeeId=:prmEmployeeId and r.tamamlanma=true and r.supervisorApproval!=0 ";
			if (alanlar != null && alanlar.size() > 0) {
				for (int i = 0; i < alanlar.size(); i++) {
					if (alanlar.get(i) != null && !alanlar.get(i).equals(""))
						hsql += "and upper(r." + alanlar.get(i) + ") like '%"
								+ veriler.get(i).toUpperCase() + "%' ";
				}
			}
			String siralama;
			if (sortField != null && !sortField.equals("")) {
				if (sortOrder.ordinal() == 1) {
					siralama = " desc";
				} else {
					siralama = " asc";
				}
				hsql += "order by r." + sortField + siralama;
			} else
				hsql += "order by r.id DESC";

			query = manager.createQuery(hsql);
			query.setMaxResults(pagesize);
			query.setFirstResult(start);
			query.setParameter("prmEmployeeId", prmEmployeeId);
		} else if (prmEmployeeId > 0) {

			hsql = "SELECT r FROM SatinalmaAmbarMalzemeTalepForm r where r.ekleyenKullanici=:prmEmployeeId ";
			if (alanlar != null && alanlar.size() > 0) {
				for (int i = 0; i < alanlar.size(); i++) {
					if (alanlar.get(i) != null && !alanlar.get(i).equals(""))
						hsql += "and r." + alanlar.get(i) + " like '%"
								+ veriler.get(i).toUpperCase() + "%' ";
				}
			}
			String siralama;
			if (sortField != null && !sortField.equals("")) {
				if (sortOrder.ordinal() == 1) {
					siralama = " desc";
				} else {
					siralama = " asc";
				}
				hsql += "order by r." + sortField + siralama;
			} else
				hsql += "order by r.id DESC";

			query = manager.createQuery(hsql);
			query.setMaxResults(pagesize);
			query.setFirstResult(start);
			query.setParameter("prmEmployeeId", prmEmployeeId);

		} else {
			hsql = "select r from SatinalmaAmbarMalzemeTalepForm r where 1=1 ";
			if (alanlar != null && alanlar.size() > 0) {
				for (int i = 0; i < alanlar.size(); i++) {
					if (alanlar.get(i) != null && !alanlar.get(i).equals(""))
						hsql += "and r." + alanlar.get(i) + " like '%"
								+ veriler.get(i).toUpperCase() + "%' ";
				}
			}
			String siralama;
			if (sortField != null && !sortField.equals("")) {
				if (sortOrder.ordinal() == 1) {
					siralama = " desc";
				} else {
					siralama = " asc";
				}
				hsql += "order by r." + sortField + siralama;
			} else
				hsql += "order by r.id DESC";

			query = manager.createQuery(hsql);
			query.setMaxResults(pagesize);
			query.setFirstResult(start);
		}

		List<SatinalmaAmbarMalzemeTalepForm> list = query.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return list;
	}

	public int countTotalRecord(int prmEmployeeId, boolean prmOlder) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query query = null;
		if (prmOlder) {
			query = manager
					.createNamedQuery("SatinalmaAmbarMalzemeTalepForm.totalAllHistory");
			query.setParameter("prmEmployeeId", prmEmployeeId);
		} else if (prmEmployeeId > 0) {
			query = manager
					.createNamedQuery("SatinalmaAmbarMalzemeTalepForm.totalAllByEmployeeId");
			query.setParameter("prmEmployeeId", prmEmployeeId);
		} else {
			query = manager
					.createNamedQuery("SatinalmaAmbarMalzemeTalepForm.totalAll");
		}
		Number result = (Number) query.getSingleResult();
		int rr = result.intValue();
		manager.getTransaction().commit();
		manager.close();
		return rr;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}
}
