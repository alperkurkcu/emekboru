/**
 * 
 */
package com.emekboru.jpaman.tahribatsiztestsonuc;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.tahribatsiztestsonuc.SpecPenetrant;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class SpecPenetrantManager extends CommonQueries<SpecPenetrant> {

	@SuppressWarnings("unchecked")
	public static List<SpecPenetrant> getAllSpecPenetrant(Integer pipeId) {

		List<SpecPenetrant> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("SpecPenetrant.findAll")
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
