package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaUygulamaSicaklikSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class TestKaplamaUygulamaSicaklikSonucManager extends
		CommonQueries<TestKaplamaUygulamaSicaklikSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaUygulamaSicaklikSonuc> getAllTestKaplamaUygulamaSicaklikSonuc(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaUygulamaSicaklikSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestKaplamaUygulamaSicaklikSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
