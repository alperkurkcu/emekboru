/**
 * 
 */
package com.emekboru.jpaman.isemirleri;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.isemri.IsEmriPolietilenKaplamaYapistiriciSicaklik;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class IsEmriPolietilenKaplamaYapistiriciSicaklikManager extends
		CommonQueries<IsEmriPolietilenKaplamaYapistiriciSicaklik> {

	@SuppressWarnings("unchecked")
	public static List<IsEmriPolietilenKaplamaYapistiriciSicaklik> getAllIsEmriPolietilenKaplamaYapistiriciSicaklik(
			Integer prmIsEmriId) {

		List<IsEmriPolietilenKaplamaYapistiriciSicaklik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"IsEmriPolietilenKaplamaYapistiriciSicaklik.findAllByIsEmriId")
					.setParameter("prmIsEmriId", prmIsEmriId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
