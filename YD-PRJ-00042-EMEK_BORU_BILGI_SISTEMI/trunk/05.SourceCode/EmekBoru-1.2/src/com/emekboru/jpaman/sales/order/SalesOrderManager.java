package com.emekboru.jpaman.sales.order;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.sales.order.SalesOrder;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class SalesOrderManager extends CommonQueries<SalesOrder> {

	@SuppressWarnings("unchecked")
	public SalesOrder getOrderByProposal(Integer proposalId) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query findQuerie = manager
				.createNamedQuery("SalesOrder.findOrderByProposalId");
		findQuerie.setParameter("prmProposalId", proposalId);
		List<SalesOrder> result = findQuerie.getResultList();

		if (result.size() > 0) {
			return result.get(0);
		} else
			return null;

	}

}
