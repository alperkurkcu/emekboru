package com.emekboru.jpaman;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.apache.log4j.Logger;

@SuppressWarnings("rawtypes")
public class CommonQueries<T> {

	public boolean enterNew(T object) {
		EntityManager entityManager = Persistence.createEntityManagerFactory(
				"emekboru").createEntityManager();
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			entityManager.persist(object);
			entityManager.flush();
			transaction.commit();
			entityManager.clear();
			entityManager.close();
			return true;
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("-HATA-CommonQueries.enterNew ",
					e);
			entityManager.flush();
			transaction.rollback();
			entityManager.clear();
			entityManager.close();
			return false;
		}
	}

	public void updateEntity(T toUpdate) {

		EntityManager entityManager = Persistence.createEntityManagerFactory(
				"emekboru").createEntityManager();
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			toUpdate = entityManager.merge(toUpdate);
			entityManager.flush();
			entityManager.getTransaction().commit();
			entityManager.clear();
			entityManager.close();
		} catch (Exception e) {
			Logger.getLogger(getClass()).error(
					"-HATA-CommonQueries.updateEntity ", e);
			entityManager.flush();
			transaction.rollback();
			entityManager.clear();
			entityManager.close();
		}
		return;
	}

	// just delete an object from the database
	public boolean delete(T object) {

		EntityManager entityManager = Persistence.createEntityManagerFactory(
				"emekboru").createEntityManager();
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			object = entityManager.merge(object);
			entityManager.remove(object);
			entityManager.flush();
			entityManager.getTransaction().commit();
			entityManager.clear();
			entityManager.close();
			return true;
		} catch (Exception e) {
			Logger.getLogger(getClass())
					.error("-HATA-CommonQueries.delete ", e);
			entityManager.flush();
			transaction.rollback();
			entityManager.clear();
			entityManager.close();
			return false;
		}
	}

	public void refreshEntity(T entity) {

		EntityManager entityManager = Persistence.createEntityManagerFactory(
				"emekboru").createEntityManager();
		EntityTransaction transaction = entityManager.getTransaction();
		try {
			transaction.begin();
			entityManager.refresh(entity);
			entityManager.flush();
			entityManager.getTransaction().commit();
			entityManager.clear();
			entityManager.close();
		} catch (Exception e) {
			Logger.getLogger(getClass()).error(
					"-HATA-CommonQueries.refreshEntity ", e);
			entityManager.flush();
			transaction.rollback();
			entityManager.clear();
			entityManager.close();
		}
		return;
	}

	public List<T> refreshCollection(List<T> entityCollection) {

		EntityManager entityManager = Persistence.createEntityManagerFactory(
				"emekboru").createEntityManager();
		EntityTransaction transaction = entityManager.getTransaction();
		List<T> result = new ArrayList<T>();

		try {
			transaction.begin();

			if (entityCollection != null && !entityCollection.isEmpty()) {
				entityManager.getEntityManagerFactory().getCache()
						.evict(entityCollection.get(0).getClass());
				T mergedEntity;
				for (T entity : entityCollection) {
					mergedEntity = entityManager.merge(entity);
					entityManager.refresh(mergedEntity);
					result.add(mergedEntity);
				}
			}
			entityManager.flush();
			entityManager.getTransaction().commit();
			entityManager.clear();
			entityManager.close();
			return result;
		} catch (Exception e) {
			Logger.getLogger(getClass()).error(
					"-HATA-CommonQueries.refreshCollection ", e);
			entityManager.flush();
			transaction.rollback();
			entityManager.clear();
			entityManager.close();
			return result;
		}
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll(Class<T> clazz) {

		EntityManager entityManager = Persistence.createEntityManagerFactory(
				"emekboru").createEntityManager();
		EntityTransaction transaction = entityManager.getTransaction();
		List<T> returnedList = null;

		try {
			transaction.begin();
			String name = clazz.getName();
			String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
			String query = "SELECT c FROM " + simpleClassName + " c ";
			Query findQuery = entityManager.createQuery(query);
			returnedList = findQuery.getResultList();
			entityManager.flush();
			transaction.commit();
			entityManager.clear();
			entityManager.close();
			return returnedList;
		} catch (Exception e) {
			Logger.getLogger(getClass()).error("-HATA-CommonQueries.findAll ",
					e);
			entityManager.flush();
			transaction.rollback();
			entityManager.clear();
			entityManager.close();
			return returnedList;
		}
	}

	@SuppressWarnings("unchecked")
	public <E> List<T> findByField(Class<T> clazz, String searchBy,
			E searchByValue) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String searchByAtt = new String(searchBy);

		int lastIdx = searchByAtt.lastIndexOf('.');
		if (lastIdx > 0)
			searchByAtt = searchByAtt.substring(lastIdx + 1);

		String name = clazz.getName();
		String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
		String query = new String();
		query = "SELECT c FROM " + simpleClassName + " c WHERE c." + searchBy
				+ "=:" + searchByAtt;

		Query findQuery = manager.createQuery(query);
		findQuery.setParameter(searchByAtt, searchByValue);
		List<T> returnedList = findQuery.getResultList();
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		return returnedList;
	}

	@SuppressWarnings("unchecked")
	public <E> List<T> findByFieldOrderBy(Class<T> clazz, String searchBy,
			E searchByValue, String orderBy) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String searchByAtt = new String(searchBy);

		int lastIdx = searchByAtt.lastIndexOf('.');
		if (lastIdx > 0)
			searchByAtt = searchByAtt.substring(lastIdx + 1);

		String name = clazz.getName();
		String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
		String query = new String();
		query = "SELECT c FROM " + simpleClassName + " c WHERE c." + searchBy
				+ "=:" + searchByAtt + " ORDER BY c." + orderBy + " ASC";

		Query findQuery = manager.createQuery(query);
		findQuery.setParameter(searchByAtt, searchByValue);
		List<T> returnedList = findQuery.getResultList();
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		return returnedList;
	}

	public T loadObject(Class<T> clazz, Object id) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();

		tx.begin();
		manager.getEntityManagerFactory().getCache().evictAll();
		T t = manager.find(clazz, id);
		// manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		return t;
	}

	@SuppressWarnings("unchecked")
	public List<T> findAllOrdered(Class<T> clazz, String orderBy) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String orderByAtt = new String(orderBy);
		int lastIdx = orderByAtt.lastIndexOf('.');
		if (lastIdx > 0)
			orderByAtt = orderByAtt.substring(lastIdx + 1);

		String name = clazz.getName();
		String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
		String query = "SELECT c FROM " + simpleClassName + " c ORDER BY c."
				+ orderByAtt + " ASC";
		Query findQuery = manager.createQuery(query);
		List<T> returnedList = findQuery.getResultList();
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		return returnedList;
	}

	@SuppressWarnings("unchecked")
	public List<T> findAllOrderBy(Class<T> clazz, String orderBy) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String orderByAtt = new String(orderBy);

		String name = clazz.getName();
		String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
		String query = "SELECT c FROM " + simpleClassName + " c ORDER BY c."
				+ orderByAtt + " DESC";
		Query findQuery = manager.createQuery(query);
		List<T> returnedList = findQuery.getResultList();
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		return returnedList;
	}

	@SuppressWarnings("unchecked")
	public List<T> findAllOrderByASC(Class<T> clazz, String orderBy) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String orderByAtt = new String(orderBy);

		String name = clazz.getName();
		String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
		String query = "SELECT c FROM " + simpleClassName + " c ORDER BY c."
				+ orderByAtt + " ASC";
		Query findQuery = manager.createQuery(query);
		List<T> returnedList = findQuery.getResultList();
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		return returnedList;
	}

	@SuppressWarnings("unchecked")
	public List<T> findAllOrderByDESCASC(Class<T> clazz, String orderBy1,
			String orderBy2) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String name = clazz.getName();
		String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
		String query = "SELECT c FROM " + simpleClassName + " c ORDER BY c."
				+ orderBy1 + " DESC," + orderBy2 + " ASC";
		Query findQuery = manager.createQuery(query);
		List<T> returnedList = findQuery.getResultList();
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		return returnedList;
	}

	public <E> boolean deleteByField(Class<T> clazz, String deleteBy,
			E deleteByValue) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String deleteByAtt = new String(deleteBy);
		int lastIdx = deleteByAtt.lastIndexOf('.');
		if (lastIdx > 0)
			deleteByAtt = deleteByAtt.substring(lastIdx + 1);

		String name = clazz.getName();
		String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
		String query = new String();
		query = "DELETE FROM " + simpleClassName + " c WHERE c." + deleteBy
				+ "=:" + deleteByAtt;
		Query deleteQuery = manager.createQuery(query);
		deleteQuery.setParameter(deleteByAtt, deleteByValue);
		int deleted = deleteQuery.executeUpdate();
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		if (deleted == 0)
			return false;
		return true;
	}

	public <K> List<?> findAllByField(Class<T> clazz, String findByField,
			String className, String secondField, K secondValue) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String name = clazz.getName();
		String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
		String query = new String();
		query = "SELECT c FROM " + simpleClassName + " c WHERE c."
				+ findByField + "=" + " ANY(SELECT l.id." + findByField
				+ " FROM " + className + " l WHERE l.id." + secondField + "=:"
				+ secondField + ")";
		Query findQuery = manager.createQuery(query);
		findQuery.setParameter(secondField, secondValue);
		List<?> returnedList = findQuery.getResultList();
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		return returnedList;
	}

	// Return a list of object that are between the selected value(for example
	// between stardDate and EndDate)
	public <E> List<?> findTypeBetweenTwoValues(Class<T> clazz,
			String attributeName, E startValue, E endValue, Integer type) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String name = clazz.getName();
		String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
		String query = new String();
		query = "SELECT c FROM " + simpleClassName
				+ " c WHERE c.coatMaterialTypeId=:coatMaterialTypeId AND "
				+ "c." + attributeName + ">=:start" + " AND " + "c."
				+ attributeName + "<=:end";
		Query findQuery = manager.createQuery(query);
		findQuery.setParameter("start", startValue);
		findQuery.setParameter("end", endValue);
		findQuery.setParameter("coatMaterialTypeId", type);
		List<?> returnedList = findQuery.getResultList();
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		return returnedList;
	}

	public boolean deleteByRuloPK(Class<T> clazz, int ruloId, int ruloYear,
			boolean isPkField) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String name = clazz.getName();
		String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
		String query = new String();
		query = "DELETE FROM " + simpleClassName
				+ " c WHERE c.ruloId=:ruloId AND c.ruloYear=:ruloYear";
		if (isPkField)
			query = "DELETE FROM "
					+ simpleClassName
					+ " c WHERE c.id.ruloId=:ruloId AND c.id.ruloYear=:ruloYear";
		Query deleteQuery = manager.createQuery(query);
		deleteQuery.setParameter("ruloId", ruloId);
		deleteQuery.setParameter("ruloYear", ruloYear);
		int deleted = deleteQuery.executeUpdate();
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		if (deleted == 0)
			return false;
		return true;
	}

	// find the maxximum value of a field FieldName by searching by another
	// field SearchFieldName
	@SuppressWarnings("unchecked")
	public <E> T findMax(Class<T> clazz, String fieldName,
			String searchFieldName, E searchFieldValue, boolean isPkField) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		String name = clazz.getName();
		String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
		String query = new String();
		query = "SELECT MAX(c." + fieldName + ") FROM " + simpleClassName
				+ " c WHERE c." + searchFieldName + "=:" + searchFieldName;
		if (isPkField)
			query = "SELECT MAX(c." + fieldName + ") FROM " + simpleClassName
					+ " c WHERE c.id." + searchFieldName + "=:"
					+ searchFieldName;
		Query findQuery = manager.createQuery(query);
		findQuery.setParameter(searchFieldName, searchFieldValue);
		List<T> returnedList = findQuery.getResultList();
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		if (returnedList.size() == 0)
			return null;
		return returnedList.get(0);
	}

	@SuppressWarnings("unchecked")
	public List<T> selectFromWhereQuerie(Class<T> classOfObject,
			ArrayList<WhereClauseArgs> conditionList) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		String querie = createWhereQuerie(classOfObject, conditionList);
		Query jpqlQuerie = manager.createQuery(querie);
		String fieldName = new String();

		for (int i = 0; i < conditionList.size(); i++) {
			WhereClauseArgs arg = conditionList.get(i);
			int lastIndexOfPoint = arg.getFieldName().lastIndexOf('.');
			if (lastIndexOfPoint > 0)
				fieldName = arg.getFieldName().substring(lastIndexOfPoint + 1);
			else
				fieldName = arg.getFieldName();

			if (arg.getOrderOfOccurance() != null)
				jpqlQuerie.setParameter(fieldName
						+ arg.getOrderOfOccurance().toString(), arg.getValue());
			else
				jpqlQuerie.setParameter(fieldName, arg.getValue());
		}
		System.out.println("QUERY : " + jpqlQuerie.toString());
		List<T> resultList = jpqlQuerie.getResultList();
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		return resultList;
	}

	@SuppressWarnings("unchecked")
	public List<T> selectFromWhereQuerieOrderDesc(Class<T> classOfObject,
			ArrayList<WhereClauseArgs> conditionList, String orderedBy) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		String querie = createWhereQuerieOrderedDesc(classOfObject,
				conditionList, orderedBy);
		System.out.println("this is querie after WHERE : " + querie);
		Query jpqlQuerie = manager.createQuery(querie);
		for (int i = 0; i < conditionList.size(); i++) {
			WhereClauseArgs arg = conditionList.get(i);
			if (arg.getOrderOfOccurance() != null)
				jpqlQuerie.setParameter(arg.getFieldName()
						+ arg.getOrderOfOccurance().toString(), arg.getValue());
			else
				jpqlQuerie.setParameter(arg.getFieldName(), arg.getValue());
		}
		List<T> resultList = jpqlQuerie.getResultList();
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		return resultList;
	}

	@SuppressWarnings("unchecked")
	public <E> List<?> selectFromNotAnyQuery(Class<T> classOfObject,
			Class<E> classOfAny, WhereAnyArgs<T> arg, boolean distinct) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		String className = classOfObject.toString().substring(
				classOfObject.toString().lastIndexOf('.') + 1);
		String anyClassName = classOfAny.toString().substring(
				classOfObject.toString().lastIndexOf('.') + 1);
		String query = new String();
		if (distinct)
			query = JpqlClauses.SELECT_DISTINCT.concat("t")
					.concat(JpqlClauses.FROM).concat(className).concat(" t")
					.concat(JpqlClauses.WHERE);
		else
			query = JpqlClauses.SELECT.concat("t").concat(JpqlClauses.FROM)
					.concat(className).concat(" t").concat(JpqlClauses.WHERE);

		if (arg.getFirstIsPk())
			query = query.concat("t.id.");
		else
			query = query.concat("t.");

		query = query.concat(arg.getFirstFieldName()).concat(
				JpqlComparativeClauses.NQ_NOT_EQUAL);
		query = query.concat(" ALL(SELECT ");
		if (arg.getSecondIsPk())
			query = query.concat("m.id.");
		else
			query = query.concat("m.");
		query = query + arg.getSecondFieldName() + JpqlClauses.FROM
				+ anyClassName + " m" + JpqlClauses.WHERE;

		if (arg.getThirdIsPk())
			query = query.concat("m.id.");
		else
			query = query.concat("m.");

		query = query + arg.getThirdFieldName()
				+ JpqlComparativeClauses.NQ_EQUAL + arg.getThirdFieldName()
				+ ")";
		System.out.println("this is the final query: " + query);
		Query jpqlQuery = manager.createQuery(query);
		jpqlQuery.setParameter(arg.getThirdFieldName(),
				arg.getThirdFieldValue());
		List<T> list = jpqlQuery.getResultList();
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		return list;

	}

	@SuppressWarnings("unchecked")
	public <E> List<?> selectFromAnyQuery(Class<T> classOfObject,
			Class<E> classOfAny, WhereAnyArgs<T> arg, String compClause,
			boolean distinct) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		String className = classOfObject.toString().substring(
				classOfObject.toString().lastIndexOf('.') + 1);
		String anyClassName = classOfAny.toString().substring(
				classOfObject.toString().lastIndexOf('.') + 1);
		String query = new String();
		if (distinct)
			query = JpqlClauses.SELECT_DISTINCT.concat("t")
					.concat(JpqlClauses.FROM).concat(className).concat(" t")
					.concat(JpqlClauses.WHERE);
		else
			query = JpqlClauses.SELECT.concat("t").concat(JpqlClauses.FROM)
					.concat(className).concat(" t").concat(JpqlClauses.WHERE);

		if (arg.getFirstIsPk())
			query = query.concat("t.id.");
		else
			query = query.concat("t.");

		query = query.concat(arg.getFirstFieldName()).concat(
				JpqlComparativeClauses.NQ_EQUAL);

		query = query.concat(" ANY(SELECT ");
		if (arg.getSecondIsPk())
			query = query.concat("m.id.");
		else
			query = query.concat("m.");
		query = query + arg.getSecondFieldName() + JpqlClauses.FROM
				+ anyClassName + " m" + JpqlClauses.WHERE;

		if (arg.getThirdIsPk())
			query = query.concat("m.id.");
		else
			query = query.concat("m.");

		query = query + arg.getThirdFieldName() + compClause
				+ arg.getThirdFieldName() + ")";
		System.out.println("this is the final query: " + query);
		Query jpqlQuery = manager.createQuery(query);
		jpqlQuery.setParameter(arg.getThirdFieldName(),
				arg.getThirdFieldValue());
		List<T> list = jpqlQuery.getResultList();
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		return list;

	}

	@SuppressWarnings("unchecked")
	public List<T> selectFromWhereQuerieOrderDescMaxResult(
			Class<T> classOfObject, ArrayList<WhereClauseArgs> conditionList,
			String orderedBy, Integer maxResult) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		String querie = createWhereQuerieOrderedDesc(classOfObject,
				conditionList, orderedBy);
		System.out.println("this is querie after WHERE : " + querie);
		Query jpqlQuerie = manager.createQuery(querie);
		for (int i = 0; i < conditionList.size(); i++) {
			WhereClauseArgs arg = conditionList.get(i);
			if (arg.getOrderOfOccurance() != null)
				jpqlQuerie.setParameter(arg.getFieldName()
						+ arg.getOrderOfOccurance().toString(), arg.getValue());
			else
				jpqlQuerie.setParameter(arg.getFieldName(), arg.getValue());
		}
		List<T> resultList = jpqlQuerie.setMaxResults(maxResult)
				.getResultList();
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		return resultList;
	}

	@SuppressWarnings("unchecked")
	public List<T> selectFromWhereQuerieOrderAsc(Class<T> classOfObject,
			ArrayList<WhereClauseArgs> conditionList, String orderedBy) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		String querie = createWhereQuerieOrderedAsc(classOfObject,
				conditionList, orderedBy);
		System.out.println("this is querie after WHERE : " + querie);
		Query jpqlQuerie = manager.createQuery(querie);
		for (int i = 0; i < conditionList.size(); i++) {
			WhereClauseArgs arg = conditionList.get(i);
			if (arg.getOrderOfOccurance() != null)
				jpqlQuerie.setParameter(arg.getFieldName()
						+ arg.getOrderOfOccurance().toString(), arg.getValue());
			else
				jpqlQuerie.setParameter(arg.getFieldName(), arg.getValue());
		}
		List<T> resultList = jpqlQuerie.getResultList();
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		return resultList;
	}

	@SuppressWarnings("unchecked")
	public List<T> selectFromWhereQuerieOrderAscMaxResult(
			Class<T> classOfObject, ArrayList<WhereClauseArgs> conditionList,
			String orderedBy, Integer maxResult) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		String querie = createWhereQuerieOrderedAsc(classOfObject,
				conditionList, orderedBy);
		Query jpqlQuerie = manager.createQuery(querie);
		for (int i = 0; i < conditionList.size(); i++) {
			WhereClauseArgs arg = conditionList.get(i);
			if (arg.getOrderOfOccurance() != null)
				jpqlQuerie.setParameter(arg.getFieldName()
						+ arg.getOrderOfOccurance().toString(), arg.getValue());
			else
				jpqlQuerie.setParameter(arg.getFieldName(), arg.getValue());
		}
		List<T> resultList = jpqlQuerie.setMaxResults(maxResult)
				.getResultList();
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		return resultList;
	}

	// create the query for complex select parameters (student.course.courseId =
	// courseId)
	@SuppressWarnings("unchecked")
	public List<T> selectFromWhereQuerieComplexName(Class<T> classOfObject,
			ArrayList<WhereClauseArgs> conditionList) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		String querie = createWhereQuerieComplexName(classOfObject,
				conditionList);
		System.out.println("final query : " + querie);
		Query jpqlQuerie = manager.createQuery(querie);
		for (int i = 0; i < conditionList.size(); i++) {
			WhereClauseArgs arg = conditionList.get(i);
			int firstIndex = arg.getFieldName().lastIndexOf('.');
			String paramName = new String();
			if (firstIndex > 0)
				paramName = arg.getFieldName().substring(firstIndex + 1);
			else
				paramName = arg.getFieldName();

			if (arg.getOrderOfOccurance() != null)
				jpqlQuerie.setParameter(paramName
						+ arg.getOrderOfOccurance().toString(), arg.getValue());
			else
				jpqlQuerie.setParameter(paramName, arg.getValue());
		}
		List<T> resultList = jpqlQuerie.getResultList();
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		return resultList;
	}

	public String createWhereQuerie(Class<T> classOfObject,
			ArrayList<WhereClauseArgs> conditionList) {

		String querie = new String();
		String objectClassName = classOfObject.toString().substring(
				classOfObject.toString().lastIndexOf('.') + 1);
		querie = JpqlClauses.SELECT.concat("t").concat(JpqlClauses.FROM)
				.concat(objectClassName).concat(" t ")
				.concat(JpqlClauses.WHERE);
		for (int i = 0; i < conditionList.size(); i++) {
			WhereClauseArgs arg = conditionList.get(i);
			String fieldName = new String();
			int lastIndexOfPoint = arg.getFieldName().lastIndexOf('.');
			if (lastIndexOfPoint > 0)
				fieldName = arg.getFieldName().substring(lastIndexOfPoint + 1);
			else
				fieldName = arg.getFieldName();
			if (arg.getOrderOfOccurance() != null) {
				if (arg.isComplexKey()) {

					querie = querie
							.concat("t.")
							.concat("id.")
							.concat(fieldName)
							.concat(arg.getComparativeClause())
							.concat(arg.getFieldName()
									+ arg.getOrderOfOccurance().toString());
					if (i < conditionList.size() - 1)
						querie = querie.concat(JpqlClauses.AND);
				} else {
					querie = querie
							.concat("t.")
							.concat(arg.getFieldName().toString())
							.concat(arg.getComparativeClause())
							.concat(fieldName
									+ arg.getOrderOfOccurance().toString());
					if (i < conditionList.size() - 1)
						querie = querie.concat(JpqlClauses.AND);
				}
			} else {
				if (arg.isComplexKey()) {
					querie = querie.concat("t.").concat("id.")
							.concat(arg.getFieldName())
							.concat(arg.getComparativeClause())
							.concat(arg.getFieldName());
					if (i < conditionList.size() - 1)
						querie = querie.concat(JpqlClauses.AND);
				} else {
					querie = querie.concat("t.")
							.concat(arg.getFieldName().toString())
							.concat(arg.getComparativeClause())
							.concat(fieldName);
					if (i < conditionList.size() - 1)
						querie = querie.concat(JpqlClauses.AND);
				}
			}
		}
		return querie;
	}

	public String createWhereQuerieComplexName(Class<T> classOfObject,
			ArrayList<WhereClauseArgs> conditionList) {

		String querie = new String();
		String objectClassName = classOfObject.toString().substring(
				classOfObject.toString().lastIndexOf('.') + 1);
		querie = JpqlClauses.SELECT.concat("t").concat(JpqlClauses.FROM)
				.concat(objectClassName).concat(" t ");
		if (conditionList != null) {
			querie = querie.concat(JpqlClauses.WHERE);
			for (int i = 0; i < conditionList.size(); i++) {
				WhereClauseArgs arg = conditionList.get(i);
				System.out.println("is OR clause: " + arg.isNextOrClause());
				int firstIndex = arg.getFieldName().lastIndexOf('.');
				String paramName = new String();
				// if the name is for example programName than just take that
				// name
				// if the name is complex such as course.program.programName,
				// than just take programName
				if (firstIndex > 0)
					paramName = arg.getFieldName().substring(firstIndex + 1);
				else
					paramName = arg.getFieldName();

				if (arg.getOrderOfOccurance() != null) {
					if (arg.isComplexKey())
						querie = querie
								.concat("t.")
								.concat("id.")
								.concat(arg.getFieldName())
								.concat(arg.getComparativeClause())
								.concat(arg.getFieldName()
										+ arg.getOrderOfOccurance().toString());
					else
						querie = querie
								.concat("t.")
								.concat(arg.getFieldName().toString())
								.concat(arg.getComparativeClause())
								.concat(paramName
										+ arg.getOrderOfOccurance().toString());
				} else {
					if (arg.isComplexKey())
						querie = querie.concat("t.").concat("id.")
								.concat(arg.getFieldName())
								.concat(arg.getComparativeClause())
								.concat(arg.getFieldName());
					else
						querie = querie.concat("t.")
								.concat(arg.getFieldName().toString())
								.concat(arg.getComparativeClause())
								.concat(paramName.toString());
				}
				if (i < conditionList.size() - 1) {// if we have selected that
													// the query should be OR
													// then we add OR instead of
													// AND
					if (arg.isNextOrClause())
						querie = querie.concat(JpqlClauses.OR);
					else
						querie = querie.concat(JpqlClauses.AND);
				}
			}
		}
		return querie;
	}

	public String createWhereQuerieOrderedDesc(Class<T> classOfObject,
			ArrayList<WhereClauseArgs> conditionList, String orderedBy) {
		String querie = createWhereQuerie(classOfObject, conditionList);
		querie = querie.concat(JpqlClauses.ORDER_BY + "t." + orderedBy
				+ JpqlClauses.DESC);
		return querie;
	}

	public String createWhereQuerieOrderedAsc(Class<T> classOfObject,
			ArrayList<WhereClauseArgs> conditionList, String orderedBy) {
		String querie = createWhereQuerie(classOfObject, conditionList);
		querie = querie.concat(JpqlClauses.ORDER_BY + "t." + orderedBy
				+ JpqlClauses.ASC);
		return querie;
	}

	@SuppressWarnings("unchecked")
	public List<T> findAllLike(Class<T> clazz, String column,
			String likeAttrbute) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String likeColumn = new String(column);
		String likeAtt = new String(likeAttrbute);

		String name = clazz.getName();
		String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
		String query = "SELECT c FROM " + simpleClassName + " c WHERE c."
				+ likeColumn + " LIKE '%" + likeAtt + "%'";
		Query findQuery = manager.createQuery(query);
		List<T> returnedList = findQuery.getResultList();
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		return returnedList;
	}

	@SuppressWarnings("unchecked")
	public List<String> findDistinctValuesOfColumn(Class<T> clazz,
			String columnName) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String name = clazz.getName();
		String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
		String query = "SELECT DISTINCT c." + columnName + " FROM "
				+ simpleClassName + " c ";
		Query findQuery = manager.createQuery(query);
		List<String> returnedList = findQuery.getResultList();
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		return returnedList;
	}

	public <E> List<?> findBetweenTwoParameters(Class<T> clazz,
			String attributeName, String startValue, String endValue) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String name = clazz.getName();
		String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
		String query = new String();
		query = "SELECT c FROM " + simpleClassName + " c WHERE c."
				+ attributeName + " BETWEEN :start AND :end";
		Query findQuery = manager.createQuery(query);
		findQuery.setParameter("start", startValue);
		findQuery.setParameter("end", endValue);
		List<?> returnedList = findQuery.getResultList();
		manager.flush();
		tx.commit();
		manager.clear();
		manager.close();
		return returnedList;
	}

}
