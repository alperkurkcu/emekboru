/**
 * 
 */
package com.emekboru.jpaman.stok;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.stok.StokJoinMarkaUreticiKodu;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class StokJoinMarkaUreticiKoduManager extends
		CommonQueries<StokJoinMarkaUreticiKodu> {

	@SuppressWarnings("unchecked")
	public List<StokJoinMarkaUreticiKodu> getAllStokJoinMarkaUreticiKodu(
			Integer urunId, Integer markaId) {

		List<StokJoinMarkaUreticiKodu> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"StokJoinMarkaUreticiKodu.findAllByUrunIdMarkaId")
					.setParameter("prmUrunId", urunId)
					.setParameter("prmMarkaId", markaId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
