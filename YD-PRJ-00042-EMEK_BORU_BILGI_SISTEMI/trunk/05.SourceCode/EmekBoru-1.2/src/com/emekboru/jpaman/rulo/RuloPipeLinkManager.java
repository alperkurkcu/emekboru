/**
 * 
 */
package com.emekboru.jpaman.rulo;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.rulo.RuloPipeLink;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class RuloPipeLinkManager extends CommonQueries<RuloPipeLink> {

	@SuppressWarnings("unchecked")
	public static List<RuloPipeLink> findByPipeId(Integer prmPipeId) {

		List<RuloPipeLink> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("RuloPipeLink.findByPipeId")
					.setParameter("prmPipeId", prmPipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
