/**
 * 
 */
package com.emekboru.jpaman.kaplamamakinesi;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiEpoksi;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KaplamaMakinesiEpoksiManager extends
		CommonQueries<KaplamaMakinesiEpoksi> {

	@SuppressWarnings("unchecked")
	public static List<KaplamaMakinesiEpoksi> getAllKaplamaMakinesiEpoksi(
			Integer prmPipeId) {

		List<KaplamaMakinesiEpoksi> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("KaplamaMakinesiEpoksi.findAllByPipeId")
					.setParameter("prmPipeId", prmPipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<KaplamaMakinesiEpoksi> getByDateVardiya(String prmDateA,
			String prmDateB, Integer prmVardiya) {

		List<KaplamaMakinesiEpoksi> result = null;

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		try {
			String query = new String();
			query = "SELECT r FROM KaplamaMakinesiEpoksi r WHERE 1=1";

			if (prmVardiya == 1) {
				query = query + " AND r.eklemeZamani BETWEEN '" + prmDateA
						+ " 08:00:00' AND '" + prmDateA + " 19:59:59'";
			} else if (prmVardiya == 0) {
				query = query + " AND r.eklemeZamani BETWEEN '" + prmDateA
						+ " 20:00:00' AND '" + prmDateB + " 07:59:59'";
			}

			query = query + " ORDER BY r.eklemeZamani ";

			System.out.println("final query: " + query);

			Query findQuery = manager.createQuery(query);
			result = findQuery.getResultList();
			manager.getTransaction().commit();
			manager.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (result.size() == 0)
			return null;
		return result;
	}

}
