/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaTozEpoksiXcutSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaTozEpoksiXcutSonucManager extends
		CommonQueries<TestKaplamaTozEpoksiXcutSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaTozEpoksiXcutSonuc> getAllTestKaplamaTozEpoksiXcutSonuc(
			Integer prmGlobalId, Integer prmPipeId) {
		List<TestKaplamaTozEpoksiXcutSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("TestKaplamaTozEpoksiXcutSonuc.findAll")
					.setParameter("prmGlobalId", prmGlobalId)
					.setParameter("prmPipeId", prmPipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
