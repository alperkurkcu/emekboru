package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaKumlamaMalzemesiTuzMiktariSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class TestKaplamaKumlamaMalzemesiTuzMiktariSonucManager extends
		CommonQueries<TestKaplamaKumlamaMalzemesiTuzMiktariSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaKumlamaMalzemesiTuzMiktariSonuc> getAllTestKaplamaKumlamaMalzemesiTuzMiktariSonuc(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaKumlamaMalzemesiTuzMiktariSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestKaplamaKumlamaMalzemesiTuzMiktariSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
