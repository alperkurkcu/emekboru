/**
 * 
 */
package com.emekboru.jpaman.coatingmaterials;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.coatingmaterials.KaplamaMalzemeKullanmaAsitYikama;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KaplamaMalzemeKullanmaAsitYikamaManager extends
		CommonQueries<KaplamaMalzemeKullanmaAsitYikama> {

	@SuppressWarnings("unchecked")
	public static List<KaplamaMalzemeKullanmaAsitYikama> getAllKaplamaMalzemeKullanmaAsitYikama() {

		List<KaplamaMalzemeKullanmaAsitYikama> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery(
					"KaplamaMalzemeKullanmaAsitYikama.findAll").getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
