package com.emekboru.jpaman;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.DestructiveTestsSpec;

public class DestructiveTestsSpecManager extends
		CommonQueries<DestructiveTestsSpec> {

	@SuppressWarnings("unchecked")
	public void testSonucKontrol(Integer prmTestId, Boolean durum) {
		EntityManager em = Factory.getEntityManager();
		DestructiveTestsSpec test = new DestructiveTestsSpec();
		Query itemIndexQuery = em
				.createNamedQuery("DestructiveTestsSpec.findByTestId");
		itemIndexQuery.setParameter("prmTestId", prmTestId);
		List<DestructiveTestsSpec> getList = itemIndexQuery.getResultList();
		test = getList.get(0);
		test.setHasSonuc(durum);
		updateEntity(test);
	}

	@SuppressWarnings("unchecked")
	public static List<DestructiveTestsSpec> findByItemId(Integer prmItemId) {

		List<DestructiveTestsSpec> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("DestructiveTestsSpec.findByItemId")
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<DestructiveTestsSpec> findByGlobalIdItemId(
			Integer prmGlobalId, Integer prmItemId) {

		List<DestructiveTestsSpec> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"DestructiveTestsSpec.findByGlobalIdItemId")
					.setParameter("prmGlobalId", prmGlobalId)
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}