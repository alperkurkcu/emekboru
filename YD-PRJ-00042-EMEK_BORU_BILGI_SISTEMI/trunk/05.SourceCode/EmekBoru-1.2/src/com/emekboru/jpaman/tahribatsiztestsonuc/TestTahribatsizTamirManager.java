/**
 * 
 */
package com.emekboru.jpaman.tahribatsiztestsonuc;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizTamir;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
public class TestTahribatsizTamirManager extends
		CommonQueries<TestTahribatsizTamir> {

	@SuppressWarnings("unchecked")
	public static List<TestTahribatsizTamir> getAllTestTahribatsizTamir(
			Integer pipeId) {

		List<TestTahribatsizTamir> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestTahribatsizTamir.findAll")
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static Boolean tamirKontrol(Integer pipeId, Integer manuelId) {

		Boolean kontrol = false;
		List<TestTahribatsizTamir> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestTahribatsizTamir.findByPipeIdManuelId")
					.setParameter("prmPipeId", pipeId)
					.setParameter("prmManuelId", manuelId).getResultList();
			if (result.size() > 0) {
				kontrol = true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return kontrol;
	}

	@SuppressWarnings("unchecked")
	public static List<TestTahribatsizTamir> tamirBul(Integer pipeId,
			Integer manuelId) {

		List<TestTahribatsizTamir> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestTahribatsizTamir.findByPipeIdManuelId")
					.setParameter("prmPipeId", pipeId)
					.setParameter("prmManuelId", manuelId).getResultList();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<TestTahribatsizTamir> getByDateVardiya(String prmDateA,
			String prmDateB, Integer prmVardiya) {

		List<TestTahribatsizTamir> result = null;

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		try {
			String query = new String();
			query = "SELECT r FROM TestTahribatsizTamir r WHERE 1=1";

			if (prmVardiya == 1) {
				query = query + " AND r.eklemeZamani BETWEEN '" + prmDateA
						+ " 08:00:00' AND '" + prmDateA + " 19:59:59'";
			} else if (prmVardiya == 0) {
				query = query + " AND r.eklemeZamani BETWEEN '" + prmDateA
						+ " 20:00:00' AND '" + prmDateB + " 07:59:59'";
			}

			query = query + " ORDER BY r.eklemeZamani ";

			System.out.println("final query: " + query);

			Query findQuery = manager.createQuery(query);
			result = findQuery.getResultList();
			manager.getTransaction().commit();
			manager.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (result.size() == 0)
			return null;
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<TestTahribatsizTamir> getByDate(String prmDateA,
			String prmDateB) {

		List<TestTahribatsizTamir> result = null;

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		try {
			String query = new String();
			query = "SELECT r FROM TestTahribatsizTamir r WHERE r.eklemeZamani BETWEEN '"
					+ prmDateA
					+ " 00:00:00' AND '"
					+ prmDateB
					+ " 23:59:59' ORDER BY r.eklemeZamani ";

			System.out.println("final query: " + query);

			Query findQuery = manager.createQuery(query);
			result = findQuery.getResultList();
			manager.getTransaction().commit();
			manager.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (result.size() == 0)
			return null;
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizTamir> dunUretilmisBorularIcin(
			Integer prmMachineId) {

		List<TestTahribatsizTamir> result = null;
		EntityManager em = Factory.getEntityManager();

		String tarihIlk = UtilInsCore.getYesterday().toString()
				.substring(0, 10)
				+ " 08:00:00";
		String tarihSon = UtilInsCore.getTarihZaman().toString()
				.substring(0, 10)
				+ " 07:59:59";

		Timestamp tsIlk = Timestamp.valueOf(tarihIlk);
		Timestamp tsSon = Timestamp.valueOf(tarihSon);

		try {
			String query = "SELECT o FROM TestTahribatsizTamir mel WHERE mel.pipe.pipeId in (select o.pipe.pipeId from MachinePipeLink o where o.pipe.eklemeZamani BETWEEN :prmDate1 AND :prmDate2 and o.machine.machineId=:prmMachineId) order by mel.pipe.pipeId";
			Query index = em.createQuery(query);
			index.setParameter("prmDate1", tsIlk)
					.setParameter("prmDate2", tsSon)
					.setParameter("prmMachineId", prmMachineId);
			result = index.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizTamir> tumUretilmisBorularIcin(
			Integer prmSalesItemId, Integer prmMachineId) {

		List<TestTahribatsizTamir> result = null;
		EntityManager em = Factory.getEntityManager();

		try {
			String query = "SELECT mel FROM TestTahribatsizTamir mel WHERE mel.pipe.pipeId in (select o.pipe.pipeId from MachinePipeLink o where o.machine.machineId=:prmMachineId and o.pipe.salesItem.itemId=:prmSalesItemId) order by mel.pipe.pipeId";
			Query index = em.createQuery(query);
			index.setParameter("prmSalesItemId", prmSalesItemId).setParameter(
					"prmMachineId", prmMachineId);
			result = index.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
