package com.emekboru.jpaman;

public class JpqlClauses {

	public static final String SELECT = "SELECT ";
	public static final String SELECT_DISTINCT = "SELECT DISTINCT ";
	public static final String UPDATE = "UPDATE ";
	public static final String DELETE = "DELETE ";
	public static final String ANY = "ANY";
	public static final String ALL = " ALL";
	public static final String SOME = " SOME ";
	public static final String AND = " AND ";
	public static final String WHERE = " WHERE ";
	public static final String OR = " OR ";
	public static final String BETWEEN = " BETWEEN ";
	public static final String NOT_BETWEEN = " NOT BETWEEN ";
	public static final String LIKE = " LIKE ";
	public static final String NOT_LIKE = " NOT LIKE ";
	public static final String EQUAL = " EQUAL ";
	public static final String NOT_EQUAL = " NOT EQUAL ";
	public static final String IS_EMPTY = " IS EMPTY ";
	public static final String IS_NOT_EMPTY = " IS NOT EMPTY ";
	public static final String ORDER_BY = " ORDER BY ";
	public static final String ASC = " ASC";
	public static final String DESC = " DESC";
	public static final String IS_NULL = " IS NULL ";
	public static final String IS_NOT_NULL = " IS NOT NULL ";
	public static final String NOT = " NOT ";
	public static final String IN = " IN ";
	public static final String MEMBER_OF = " MEMBER OF";
	public static final String NOT_MEMBER_OF = " NOT MEMBER OF ";
	public static final String GROUP_BY = " GROUP BY ";
	public static final String HAVING = " HAVING ";
	public static final String FROM = " FROM ";
}
