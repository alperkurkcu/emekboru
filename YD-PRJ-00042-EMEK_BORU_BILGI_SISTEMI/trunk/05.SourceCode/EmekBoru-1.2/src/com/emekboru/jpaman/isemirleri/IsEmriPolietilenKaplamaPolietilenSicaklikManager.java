/**
 * 
 */
package com.emekboru.jpaman.isemirleri;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.isemri.IsEmriPolietilenKaplamaPolietilenSicaklik;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class IsEmriPolietilenKaplamaPolietilenSicaklikManager extends
		CommonQueries<IsEmriPolietilenKaplamaPolietilenSicaklik> {

	@SuppressWarnings("unchecked")
	public static List<IsEmriPolietilenKaplamaPolietilenSicaklik> getAllIsEmriPolietilenKaplamaPolietilenSicaklik(
			Integer prmIsEmriId) {

		List<IsEmriPolietilenKaplamaPolietilenSicaklik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"IsEmriPolietilenKaplamaPolietilenSicaklik.findAllByIsEmriId")
					.setParameter("prmIsEmriId", prmIsEmriId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;

	}
}
