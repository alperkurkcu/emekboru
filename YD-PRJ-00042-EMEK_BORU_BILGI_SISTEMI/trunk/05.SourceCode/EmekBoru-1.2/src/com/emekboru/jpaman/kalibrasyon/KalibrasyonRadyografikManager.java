/**
 * 
 */
package com.emekboru.jpaman.kalibrasyon;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kalibrasyon.KalibrasyonRadyografik;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KalibrasyonRadyografikManager extends
		CommonQueries<KalibrasyonRadyografik> {

	@SuppressWarnings("unchecked")
	public static List<KalibrasyonRadyografik> getAllKalibrasyon() {

		List<KalibrasyonRadyografik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("KalibrasyonRadyografik.findAll")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
