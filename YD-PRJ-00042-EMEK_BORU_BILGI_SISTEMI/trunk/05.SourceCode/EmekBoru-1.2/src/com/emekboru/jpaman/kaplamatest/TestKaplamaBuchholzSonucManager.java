package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaBuchholzSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class TestKaplamaBuchholzSonucManager extends
		CommonQueries<TestKaplamaBuchholzSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaBuchholzSonuc> getAllTestKaplamaBuchholzSonuc(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaBuchholzSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestKaplamaBuchholzSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
