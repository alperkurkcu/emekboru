package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaTozBoyaSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class TestKaplamaTozBoyaSonucManager extends
		CommonQueries<TestKaplamaTozBoyaSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaTozBoyaSonuc> getAllKaplamaTozBoyaSonucTest(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaTozBoyaSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestKaplamaTozBoyaSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
