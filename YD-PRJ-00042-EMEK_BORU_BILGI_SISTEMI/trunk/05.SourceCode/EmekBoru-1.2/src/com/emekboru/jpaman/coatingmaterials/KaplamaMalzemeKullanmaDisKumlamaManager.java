/**
 * 
 */
package com.emekboru.jpaman.coatingmaterials;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.coatingmaterials.KaplamaMalzemeKullanmaDisKumlama;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KaplamaMalzemeKullanmaDisKumlamaManager extends
		CommonQueries<KaplamaMalzemeKullanmaDisKumlama> {

	@SuppressWarnings("unchecked")
	public static List<KaplamaMalzemeKullanmaDisKumlama> getAllKaplamaMalzemeKullanmaDisKumlama() {

		List<KaplamaMalzemeKullanmaDisKumlama> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery(
					"KaplamaMalzemeKullanmaDisKumlama.findAll").getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
