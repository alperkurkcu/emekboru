/**
 * 
 */
package com.emekboru.jpaman.istakipformu;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.istakipformu.IsTakipFormuEpoksiKaplama;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class IsTakipFormuEpoksiKaplamaManager extends
		CommonQueries<IsTakipFormuEpoksiKaplama> {

	@SuppressWarnings("unchecked")
	public static List<IsTakipFormuEpoksiKaplama> getAllIsTakipFormuEpoksiKaplama(
			Integer itemId) {

		List<IsTakipFormuEpoksiKaplama> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("IsTakipFormuEpoksiKaplama.findAll")
					.setParameter("prmItemId", itemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
