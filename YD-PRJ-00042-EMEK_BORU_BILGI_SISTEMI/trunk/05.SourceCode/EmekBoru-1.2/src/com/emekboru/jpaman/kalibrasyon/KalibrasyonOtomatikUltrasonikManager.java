/**
 * 
 */
package com.emekboru.jpaman.kalibrasyon;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kalibrasyon.KalibrasyonOtomatikUltrasonik;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KalibrasyonOtomatikUltrasonikManager extends
		CommonQueries<KalibrasyonOtomatikUltrasonik> {

	@SuppressWarnings("unchecked")
	public static List<KalibrasyonOtomatikUltrasonik> getAllKalibrasyon(
			Integer prmOnlineOffline) {

		List<KalibrasyonOtomatikUltrasonik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("KalibrasyonOtomatikUltrasonik.findAll")
					.setParameter("prmOnlineOffline", prmOnlineOffline)
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
