package com.emekboru.jpaman;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.ElectrodeDustWire;
import com.emekboru.jpa.Machine;
import com.emekboru.jpa.MachineEdwLink;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.messages.ElectrodeDustWireType;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class ElectrodeDustWireManager extends CommonQueries<ElectrodeDustWire> {

	// Integer orderId = 0;
	Integer itemId = 0;

	// find EDW that are still on the machines (which have the END_DATE null)
	public List<ElectrodeDustWire> findCurrentYearEDW(Integer type,
			Date entranceDate) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query findQuerie = manager
				.createNamedQuery("ElectrodeDustWire.findCurrentYear");
		findQuerie.setParameter("type", type);
		findQuerie.setParameter("entranceDate", entranceDate);
		List<ElectrodeDustWire> list = findQuerie.getResultList();
		return list;
	}

	// find THE MINIMUM ENTRANCE DATE for a single type (E, D, or W)
	public List<Date> findMinYearForType(Integer type) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query findQuerie = manager
				.createNamedQuery("ElectrodeDustWire.findMinYearForType");
		findQuerie.setParameter("type", type);
		List<Date> list = findQuerie.getResultList();
		return list;
	}

	// find THE MAXIMUM ENTRANCE DATE for a single type (E, D, or W)
	public List<Date> findMaxYearForType(Integer type) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query findQuerie = manager
				.createNamedQuery("ElectrodeDustWire.findMaxYearForType");
		findQuerie.setParameter("type", type);
		List<Date> list = findQuerie.getResultList();
		return list;
	}

	// find THE MAXIMUM ENTRANCE DATE for a single type (E, D, or W)
	public List<ElectrodeDustWire> findTypeBetweenDates(Integer type,
			Date startDate, Date endDate) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query findQuerie = manager
				.createNamedQuery("ElectrodeDustWire.findTypeBetweenDates");
		findQuerie.setParameter("type", type);
		findQuerie.setParameter("startDate", startDate);
		findQuerie.setParameter("endDate", endDate);
		List<ElectrodeDustWire> list = findQuerie.getResultList();
		return list;
	}

	public List<ElectrodeDustWire> findRecent(int maxResult, int type) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query findQuerie = manager
				.createNamedQuery("ElectrodeDustWire.findRecent");
		findQuerie.setParameter("type", type);
		List<ElectrodeDustWire> list = findQuerie.setMaxResults(maxResult)
				.getResultList();
		return list;
	}

	// load only dusts that are allocated to certain ORDERS
	public List<ElectrodeDustWire> findAllocatedDusts(SalesItem salesItem) {

		if (salesItem != null) {
			itemId = salesItem.getItemId();
		}
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query findQuerie = manager
				.createNamedQuery("ElectrodeDustWire.findDust");
		findQuerie.setParameter("prmItemId", itemId);
		List<ElectrodeDustWire> list = findQuerie.getResultList();
		return list;
	}

	// load only WIRES that are allocated to certain ORDERS
	public List<ElectrodeDustWire> findAllocatedWires(SalesItem salesItem) {

		if (salesItem != null) {
			itemId = salesItem.getItemId();
		}
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query findQuerie = manager
				.createNamedQuery("ElectrodeDustWire.findWire");
		findQuerie.setParameter("prmItemId", itemId);
		List<ElectrodeDustWire> list = findQuerie.getResultList();
		return list;
	}

	// find Dusts that are processed in a certain machine
	// public List<ElectrodeDustWire> findDustsInMachine(int machineId) {
	//
	// List<ElectrodeDustWire> dusts = new ArrayList<ElectrodeDustWire>();
	// CommonQueries<MachineEdwLink> linkq = new
	// CommonQueries<MachineEdwLink>();
	// WhereClauseArgs arg1 = new WhereClauseArgs.Builder()
	// .setFieldName("ended").setValue(false).build();
	// WhereClauseArgs arg2 = new WhereClauseArgs.Builder()
	// .setFieldName("machine.machineId").setValue(machineId).build();
	// WhereClauseArgs arg3 = new WhereClauseArgs.Builder()
	// .setFieldName("edw.type").setValue(ElectrodeDustWireType.DUST)
	// .build();
	// ArrayList<WhereClauseArgs> condList = new ArrayList<WhereClauseArgs>();
	// condList.add(arg1);
	// condList.add(arg2);
	// condList.add(arg3);
	// List<MachineEdwLink> links = linkq.selectFromWhereQuerie(
	// MachineEdwLink.class, condList);
	// for (MachineEdwLink l : links) {
	// dusts.add(l.getEdw());
	// }
	//
	// return dusts;
	//
	// }
	public List<ElectrodeDustWire> findDustsInMachine(int machineId) {

		List<ElectrodeDustWire> dusts = new ArrayList<ElectrodeDustWire>();
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();

		Query findQuerie = manager
				.createNamedQuery("MachineEdwLink.findDustByMachineId");
		findQuerie.setParameter("prmMachineId", machineId);
		List<MachineEdwLink> links = findQuerie.getResultList();

		for (MachineEdwLink l : links) {
			dusts.add(l.getEdw());
		}
		return dusts;
	}

	// deneme

	public List<ElectrodeDustWire> findDustByMachineIdByDurum(int prmMachineId,
			int prmIcDis) {

		List<ElectrodeDustWire> dusts = new ArrayList<ElectrodeDustWire>();
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();

		Query findQuerie = manager
				.createNamedQuery("MachineEdwLink.findDustByMachineIdByDurum");
		findQuerie.setParameter("prmMachineId", prmMachineId).setParameter(
				"prmIcDis", prmIcDis);
		List<MachineEdwLink> links = findQuerie.getResultList();

		for (MachineEdwLink l : links) {
			dusts.add(l.getEdw());
		}
		return dusts;
	}

	// deneme

	// find Dusts that are processed in a certain machine
	// public List<ElectrodeDustWire> findWiresInMachine(int machineId) {
	//
	// List<ElectrodeDustWire> wires = new ArrayList<ElectrodeDustWire>();
	// CommonQueries<MachineEdwLink> linkq = new
	// CommonQueries<MachineEdwLink>();
	// WhereClauseArgs arg1 = new WhereClauseArgs.Builder()
	// .setFieldName("ended").setValue(false).build();
	// WhereClauseArgs arg2 = new WhereClauseArgs.Builder()
	// .setFieldName("machine.machineId").setValue(machineId).build();
	// WhereClauseArgs arg3 = new WhereClauseArgs.Builder()
	// .setFieldName("edw.type").setValue(ElectrodeDustWireType.WIRE)
	// .build();
	// ArrayList<WhereClauseArgs> condList = new ArrayList<WhereClauseArgs>();
	// condList.add(arg1);
	// condList.add(arg2);
	// condList.add(arg3);
	// List<MachineEdwLink> links = linkq.selectFromWhereQuerie(
	// MachineEdwLink.class, condList);
	// for (MachineEdwLink l : links) {
	// wires.add(l.getEdw());
	// }
	//
	// return wires;
	// }

	public List<ElectrodeDustWire> findWiresInMachine(int machineId) {

		List<ElectrodeDustWire> wires = new ArrayList<ElectrodeDustWire>();
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();

		Query findQuerie = manager
				.createNamedQuery("MachineEdwLink.findWireByMachineId");
		findQuerie.setParameter("prmMachineId", machineId);
		List<MachineEdwLink> links = findQuerie.getResultList();

		for (MachineEdwLink l : links) {
			wires.add(l.getEdw());
		}
		return wires;
	}

	// deneme

	public List<ElectrodeDustWire> findWireByMachineIdByDurum(int prmMachineId,
			int prmIcDis, int prmAcDc) {

		List<ElectrodeDustWire> wires = new ArrayList<ElectrodeDustWire>();
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();

		Query findQuerie = manager
				.createNamedQuery("MachineEdwLink.findWireByMachineIdByDurum");
		findQuerie.setParameter("prmMachineId", prmMachineId)
				.setParameter("prmIcDis", prmIcDis)
				.setParameter("prmAcDc", prmAcDc);
		List<MachineEdwLink> links = findQuerie.getResultList();

		for (MachineEdwLink l : links) {
			wires.add(l.getEdw());
		}
		return wires;
	}

	// deneme

	// find the MachineEdwLink element for the given machine and the
	// given Dust (this should be still in production ended=false)
	public MachineEdwLink findCurrentDustInMachine(Machine machine,
			ElectrodeDustWire dust) {

		WhereClauseArgs arg1 = new WhereClauseArgs.Builder()
				.setFieldName("machine.machineId")
				.setValue(machine.getMachineId()).build();
		WhereClauseArgs arg2 = new WhereClauseArgs.Builder()
				.setFieldName("edw.electrodeDustWireId")
				.setValue(dust.getElectrodeDustWireId()).build();
		WhereClauseArgs arg3 = new WhereClauseArgs.Builder()
				.setFieldName("edw.type").setValue(ElectrodeDustWireType.DUST)
				.build();
		WhereClauseArgs arg4 = new WhereClauseArgs.Builder()
				.setFieldName("ended").setValue(false).build();

		ArrayList<WhereClauseArgs> condList = new ArrayList<WhereClauseArgs>();
		condList.add(arg1);
		condList.add(arg2);
		condList.add(arg3);
		condList.add(arg4);
		CommonQueries<MachineEdwLink> q = new CommonQueries<MachineEdwLink>();
		List<MachineEdwLink> result = q.selectFromWhereQuerie(
				MachineEdwLink.class, condList);

		if (result.size() > 0) {
			return result.get(0);
		}
		return null;
	}

	// find the MachineEdwLink element for the given machine and the
	// given Dust (this should be still in production ended=false)
	public MachineEdwLink findCurrentWireInMachine(Machine machine,
			ElectrodeDustWire wire) {

		WhereClauseArgs arg1 = new WhereClauseArgs.Builder()
				.setFieldName("machine.machineId")
				.setValue(machine.getMachineId()).build();
		WhereClauseArgs arg2 = new WhereClauseArgs.Builder()
				.setFieldName("edw.electrodeDustWireId")
				.setValue(wire.getElectrodeDustWireId()).build();
		WhereClauseArgs arg3 = new WhereClauseArgs.Builder()
				.setFieldName("edw.type").setValue(ElectrodeDustWireType.WIRE)
				.build();
		WhereClauseArgs arg4 = new WhereClauseArgs.Builder()
				.setFieldName("ended").setValue(false).build();

		ArrayList<WhereClauseArgs> condList = new ArrayList<WhereClauseArgs>();
		condList.add(arg1);
		condList.add(arg2);
		condList.add(arg3);
		condList.add(arg4);
		CommonQueries<MachineEdwLink> q = new CommonQueries<MachineEdwLink>();
		List<MachineEdwLink> result = q.selectFromWhereQuerie(
				MachineEdwLink.class, condList);

		if (result.size() > 0) {
			return result.get(0);
		}
		return null;
	}
}
