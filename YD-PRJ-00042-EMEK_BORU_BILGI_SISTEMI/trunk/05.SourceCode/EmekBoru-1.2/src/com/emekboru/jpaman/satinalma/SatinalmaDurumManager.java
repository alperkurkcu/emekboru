/**
 * 
 */
package com.emekboru.jpaman.satinalma;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.satinalma.SatinalmaDurum;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class SatinalmaDurumManager extends CommonQueries<SatinalmaDurum> {

	@SuppressWarnings("unchecked")
	public List<SatinalmaDurum> getAllSatinalmaDurum(Integer formId) {

		List<SatinalmaDurum> result = null;
		Integer prmFormId = 0;
		EntityManager em = Factory.getEntityManager();

		try {
			if (formId == 1) {
				prmFormId = formId;
			} else if (formId == 2) {
				prmFormId = formId;
			} else if (formId == 3) {
				prmFormId = formId;
			} else if (formId == 4) {
				prmFormId = formId;
			} else if (formId == 5) {
				prmFormId = formId;
			} else if (formId == 6) {
				prmFormId = formId;
			} else if (formId == 7) {
				prmFormId = formId;
			}
			result = em.createNamedQuery("SatinalmaDurum.findByFormId")
					.setParameter("prmFormId", prmFormId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
