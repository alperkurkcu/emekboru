/**
 * 
 */
package com.emekboru.jpaman.stok;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import com.emekboru.jpa.stok.StokUrunKartlari;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class StokUrunKartlariManager extends CommonQueries<StokUrunKartlari> {

	List<StokUrunKartlari> result = null;

	@SuppressWarnings("unchecked")
	public List<StokUrunKartlari> getAllStokUrunKartlari() {

		Thread thread = new Thread() {
			@Override
			public void run() {

				EntityManager em = Factory.getEntityManager();
				result = em.createNamedQuery("StokUrunKartlari.findAll")
						.getResultList();
			}
		};
		thread.run();

		return result;
	}

	@SuppressWarnings("unchecked")
	public List<StokUrunKartlari> getAllStokUrunKartlariGirisCikis(
			Integer prmUrunId) {

		List<StokUrunKartlari> result = null;
		try {
			EntityManager em = Factory.getEntityManager();
			result = em.createNamedQuery("StokUrunKartlari.findAllGirisCikis")
					.setParameter("prmUrunId", prmUrunId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<StokUrunKartlari> findAllLike(String column, String likeAttrbute) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String likeColumn = new String(column);
		String likeAtt = new String(likeAttrbute);

		String name = StokUrunKartlari.class.getName();
		String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
		String query = "SELECT c FROM " + simpleClassName + " c WHERE upper(c."
				+ likeColumn + ") LIKE '%" + likeAtt + "%'";
		Query findQuery = manager.createQuery(query);
		List<StokUrunKartlari> returnedList = findQuery.getResultList();
		tx.commit();
		manager.close();
		return returnedList;
	}

	// @SuppressWarnings("unchecked")
	// public static List<StokUrunKartlari> getKritikStok() {
	//
	// List<StokUrunKartlari> result = null;
	// EntityManager em = Factory.getEntityManager();
	// try {
	// result = em.createNamedQuery("StokUrunKartlari.kritikStokGiris")
	// .getResultList();
	// } catch (Exception ex) {
	// ex.printStackTrace();
	// }
	// return result;
	// }

	@SuppressWarnings("unchecked")
	public List<String> findFirstOfColumnLike(String columnName, String like,
			String orderBy) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String name = StokUrunKartlari.class.getName();
		String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
		String query = "SELECT c." + columnName + " FROM " + simpleClassName
				+ " c WHERE c." + columnName + " LIKE '" + like
				+ "%' ORDER BY c." + columnName + " " + orderBy;
		Query findQuery = manager.createQuery(query).setMaxResults(1);
		List<String> returnedList = findQuery.getResultList();
		tx.commit();
		manager.close();
		return returnedList;
	}

	@SuppressWarnings("unchecked")
	public List<StokUrunKartlari> findByBarkodAktif(String likeAttrbute) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String likeAtt = new String(likeAttrbute);

		String query = "SELECT c FROM StokUrunKartlari c WHERE c.barkodKodu LIKE '%"
				+ likeAtt + "%' and c.aktifPasif!=true";
		Query findQuery = manager.createQuery(query);
		List<StokUrunKartlari> returnedList = findQuery.getResultList();
		tx.commit();
		manager.close();
		return returnedList;
	}

	@SuppressWarnings("unchecked")
	public List<StokUrunKartlari> findByUrunAdiAktif(String likeAttrbute) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String likeAtt = new String(likeAttrbute);

		String query = "SELECT c FROM StokUrunKartlari c WHERE c.urunAdi LIKE '%"
				+ likeAtt + "%' and c.aktifPasif!=true";
		Query findQuery = manager.createQuery(query);
		List<StokUrunKartlari> returnedList = findQuery.getResultList();
		tx.commit();
		manager.close();
		return returnedList;
	}

}
