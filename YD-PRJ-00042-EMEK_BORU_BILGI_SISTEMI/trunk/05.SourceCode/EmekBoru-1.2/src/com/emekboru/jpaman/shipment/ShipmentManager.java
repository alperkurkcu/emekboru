package com.emekboru.jpaman.shipment;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.shipment.Shipment;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;
import com.emekboru.jpaman.PipeManager;

public class ShipmentManager extends CommonQueries<Shipment> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.emekboru.jpaman.CommonQueries#updateEntity(java.lang.Object)
	 */
	@Override
	public void updateEntity(Shipment toUpdate) {

		PipeManager pipeManager = new PipeManager();

		for (Pipe pipe : toUpdate.getPipes()) {
			pipe.setShipment(toUpdate);
			pipeManager.updateEntity(pipe);
		}

		super.updateEntity(toUpdate);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.emekboru.jpaman.CommonQueries#delete(java.lang.Object)
	 */
	@Override
	public boolean delete(Shipment object) {
		PipeManager pipeManager = new PipeManager();

		for (Pipe pipe : object.getPipes()) {
			pipe.setShipment(null);
			pipeManager.updateEntity(pipe);
		}

		object.getPipes().clear();

		return super.delete(object);
	}

	@SuppressWarnings("unchecked")
	public String findMaxLike(String fieldName, String searchFieldName,
			String searchFieldValue) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		String name = Shipment.class.getName();
		String simpleClassName = name.substring(name.lastIndexOf('.') + 1);
		String query = new String();
		query = "SELECT MAX(c." + fieldName + ") FROM " + simpleClassName
				+ " c WHERE c." + searchFieldName + " LIKE :" + searchFieldName;

		Query findQuery = manager.createQuery(query);
		findQuery.setParameter(searchFieldName, "%" + searchFieldValue + "%");
		List<String> returnedList = findQuery.getResultList();
		tx.commit();
		manager.close();
		if (returnedList.size() == 0)
			return null;
		return returnedList.get(0);
	}
}
