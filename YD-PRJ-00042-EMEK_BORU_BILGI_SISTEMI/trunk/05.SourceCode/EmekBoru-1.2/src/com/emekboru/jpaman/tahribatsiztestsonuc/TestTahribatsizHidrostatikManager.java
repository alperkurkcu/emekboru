/**
 * 
 */
package com.emekboru.jpaman.tahribatsiztestsonuc;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizHidrostatik;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
public class TestTahribatsizHidrostatikManager extends
		CommonQueries<TestTahribatsizHidrostatik> {

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizHidrostatik> getAllTestTahribatsizHidrostatik(
			Integer prmPipeId) {

		List<TestTahribatsizHidrostatik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestTahribatsizHidrostatik.findAll")
					.setParameter("prmPipeId", prmPipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizHidrostatik> getBySalesItem(
			SalesItem prmSalesItem) {

		List<TestTahribatsizHidrostatik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestTahribatsizHidrostatik.getBySalesItem")
					.setParameter("prmSalesItem", prmSalesItem.getItemId())
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizHidrostatik> getByItemDate(Integer prmPipeId,
			String prmDateA, String prmDateB, Integer prmVardiya) {

		List<TestTahribatsizHidrostatik> result = null;

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		try {
			String query = new String();
			query = "SELECT r FROM TestTahribatsizHidrostatik r WHERE 1=1 and r.pipe.pipeId=:prmPipeId";

			if (prmVardiya == 1) {
				query = query + " AND r.eklemeZamani BETWEEN '" + prmDateA
						+ " 08:00:00' AND '" + prmDateA + " 19:59:59'";
			} else if (prmVardiya == 0) {
				query = query + " AND r.eklemeZamani BETWEEN '" + prmDateA
						+ " 20:00:00' AND '" + prmDateB + " 07:59:59'";
			}

			query = query + " ORDER BY r.eklemeZamani ";

			System.out.println("final query: " + query);

			Query findQuery = manager.createQuery(query).setParameter(
					"prmPipeId", prmPipeId);
			result = findQuery.getResultList();
			manager.getTransaction().commit();
			manager.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (result.size() == 0)
			return null;
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizHidrostatik> dunUretilmisBorularIcin(
			Integer prmMachineId) {

		List<TestTahribatsizHidrostatik> result = null;
		EntityManager em = Factory.getEntityManager();

		String tarihIlk = UtilInsCore.getYesterday().toString()
				.substring(0, 10)
				+ " 08:00:00";
		String tarihSon = UtilInsCore.getTarihZaman().toString()
				.substring(0, 10)
				+ " 07:59:59";

		Timestamp tsIlk = Timestamp.valueOf(tarihIlk);
		Timestamp tsSon = Timestamp.valueOf(tarihSon);

		try {
			String query = "SELECT o FROM TestTahribatsizHidrostatik mel WHERE mel.pipe.pipeId in (select o.pipe.pipeId from MachinePipeLink o where o.pipe.eklemeZamani BETWEEN :prmDate1 AND :prmDate2 and o.machine.machineId=:prmMachineId) order by mel.pipe.pipeId";
			Query index = em.createQuery(query);
			index.setParameter("prmDate1", tsIlk)
					.setParameter("prmDate2", tsSon)
					.setParameter("prmMachineId", prmMachineId);
			result = index.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizHidrostatik> tumUretilmisBorularIcin(
			Integer prmSalesItemId, Integer prmMachineId) {

		List<TestTahribatsizHidrostatik> result = null;
		EntityManager em = Factory.getEntityManager();

		try {
			String query = "SELECT mel FROM TestTahribatsizHidrostatik mel WHERE mel.pipe.pipeId in (select o.pipe.pipeId from MachinePipeLink o where o.machine.machineId=:prmMachineId and o.pipe.salesItem.itemId=:prmSalesItemId) order by mel.pipe.pipeId";
			Query index = em.createQuery(query);
			index.setParameter("prmSalesItemId", prmSalesItemId).setParameter(
					"prmMachineId", prmMachineId);
			result = index.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
