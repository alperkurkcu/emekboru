package com.emekboru.jpaman;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.Machine;
import com.emekboru.jpa.MachinePipeLink;
import com.emekboru.jpa.Pipe;
import com.emekboru.utils.UtilInsCore;

public class MachinePipeLinkManager extends CommonQueries<MachinePipeLink> {

	/**
	 * For the given pipe find the MachinePipeLink which is not completed yet.
	 * The parameter isEnded = false
	 * 
	 * @param pipe
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public MachinePipeLink findNotEnded(Pipe pipe, Machine machine) {

		ArrayList<WhereClauseArgs> list = new ArrayList<WhereClauseArgs>();
		WhereClauseArgs arg1 = new WhereClauseArgs.Builder()
				.setFieldName("pipe.pipeId").setValue(pipe.getPipeId()).build();
		WhereClauseArgs arg2 = new WhereClauseArgs.Builder()
				.setFieldName("machine.machineId")
				.setValue(machine.getMachineId()).build();
		WhereClauseArgs arg3 = new WhereClauseArgs.Builder()
				.setFieldName("ended").setValue(false).build();
		list.add(arg1);
		list.add(arg2);
		list.add(arg3);
		List<MachinePipeLink> results = selectFromWhereQuerieComplexName(
				MachinePipeLink.class, list);
		if (results.size() == 0)
			return null;
		return results.get(0);
	}

	@SuppressWarnings("unchecked")
	public List<MachinePipeLink> findYesterdaysDayShiftProduction(
			Integer prmMachineId) {

		List<MachinePipeLink> result = null;
		EntityManager em = Factory.getEntityManager();

		String tarihIlk = UtilInsCore.getYesterday().toString()
				.substring(0, 10)
				+ " 08:00:00";
		String tarihSon = UtilInsCore.getYesterday().toString()
				.substring(0, 10)
				+ " 19:59:59";

		Timestamp tsIlk = Timestamp.valueOf(tarihIlk);
		Timestamp tsSon = Timestamp.valueOf(tarihSon);

		System.out.println(tsIlk);
		System.out.println(tsSon);

		try {
			result = em
					.createNamedQuery(
							"MachinePipeLink.findYesterdaysProduction")
					.setParameter("prmDate1", tsIlk)
					.setParameter("prmDate2", tsSon)
					.setParameter("prmMachineId", prmMachineId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<MachinePipeLink> findYesterdaysNightShiftProduction(
			Integer prmMachineId) {

		List<MachinePipeLink> result = null;
		EntityManager em = Factory.getEntityManager();

		String tarihIlk = UtilInsCore.getYesterday().toString()
				.substring(0, 10)
				+ " 20:00:00";
		String tarihSon = UtilInsCore.getYesterday().toString()
				.substring(0, 10)
				+ " 07:59:59";

		Timestamp tsIlk = Timestamp.valueOf(tarihIlk);
		Timestamp tsSon = Timestamp.valueOf(tarihSon);

		System.out.println(tsIlk);
		System.out.println(tsSon);

		try {
			result = em
					.createNamedQuery(
							"MachinePipeLink.findYesterdaysProduction")
					.setParameter("prmDate1", tsIlk)
					.setParameter("prmDate2", tsSon)
					.setParameter("prmMachineId", prmMachineId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<MachinePipeLink> findYesterdaysProduction(Integer prmMachineId) {

		List<MachinePipeLink> result = null;
		EntityManager em = Factory.getEntityManager();

		String tarihIlk = UtilInsCore.getYesterday().toString()
				.substring(0, 10)
				+ " 08:00:00";
		String tarihSon = UtilInsCore.getTarihZaman().toString()
				.substring(0, 10)
				+ " 07:59:59";

		Timestamp tsIlk = Timestamp.valueOf(tarihIlk);
		Timestamp tsSon = Timestamp.valueOf(tarihSon);

		System.out.println(tsIlk);
		System.out.println(tsSon);

		try {
			result = em
					.createNamedQuery(
							"MachinePipeLink.findYesterdaysProduction")
					.setParameter("prmDate1", tsIlk)
					.setParameter("prmDate2", tsSon)
					.setParameter("prmMachineId", prmMachineId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<MachinePipeLink> findByPipeId(int prmPipeId) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query itemIndexQuery = manager
				.createNamedQuery("MachinePipeLink.findByPipeId");
		itemIndexQuery.setParameter("prmPipeId", prmPipeId);
		List<MachinePipeLink> getList = itemIndexQuery.getResultList();
		return getList;
	}

	@SuppressWarnings("unchecked")
	public List<MachinePipeLink> findByItemId(int prmItemId, int prmMachineId) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query itemIndexQuery = manager
				.createNamedQuery("MachinePipeLink.findByItemId");
		itemIndexQuery.setParameter("prmItemId", prmItemId).setParameter(
				"prmMachineId", prmMachineId);
		List<MachinePipeLink> getList = itemIndexQuery.getResultList();
		return getList;
	}
	
	@SuppressWarnings("unchecked")
	public List<MachinePipeLink> findByItemIdMachineIdLimited(int prmItemId, int prmMachineId) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query itemIndexQuery = manager
				.createNamedQuery("MachinePipeLink.findByItemId");
		itemIndexQuery.setParameter("prmItemId", prmItemId).setParameter(
				"prmMachineId", prmMachineId);
		itemIndexQuery.setFirstResult(0);
		itemIndexQuery.setMaxResults(3);
		List<MachinePipeLink> getList = itemIndexQuery.getResultList();
		return getList;
	}
}
