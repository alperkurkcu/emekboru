package com.emekboru.jpaman;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.TestKimyasalAnalizSonuc;
import com.emekboru.jpa.sales.order.SalesItem;

public class TestKimyasalAnalizSonucManager extends
		CommonQueries<TestKimyasalAnalizSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKimyasalAnalizSonuc> getAllKimyasalAnalizSonucTest(
			int globalId, Integer pipeId) {

		List<TestKimyasalAnalizSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestKimyasalAnalizSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<TestKimyasalAnalizSonuc> getBySalesItem(
			SalesItem prmSalesItem) {

		List<TestKimyasalAnalizSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("TestKimyasalAnalizSonuc.getBySalesItem")
					.setParameter("prmSalesItem", prmSalesItem.getItemId())
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<TestKimyasalAnalizSonuc> getByDokumNo(String prmDokumNo) {

		List<TestKimyasalAnalizSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("TestKimyasalAnalizSonuc.getByDokumNo")
					.setParameter("prmDokumNo", prmDokumNo).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<TestKimyasalAnalizSonuc> getByDokumNoSalesItem(
			String prmDokumNo, Integer prmItemId) {

		List<TestKimyasalAnalizSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestKimyasalAnalizSonuc.getByDokumNoSalesItem")
					.setParameter("prmDokumNo", prmDokumNo)
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
