/**
 * 
 */
package com.emekboru.jpaman.machines;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.machines.PlazmaSarfMalzemeleriKullanimi;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class PlazmaSarfMalzemeleriKullanimiManager extends
		CommonQueries<PlazmaSarfMalzemeleriKullanimi> {

	@SuppressWarnings("unchecked")
	public static List<PlazmaSarfMalzemeleriKullanimi> getAllSarfMalzemeleri() {

		List<PlazmaSarfMalzemeleriKullanimi> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery(
					"PlazmaSarfMalzemeleriKullanimi.findAll").getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
