/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;
import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaKumlamaOrtamSartlariSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author YD-PC
 *
 */
public class TestKaplamaKumlamaOrtamSartlariSonucManager extends
CommonQueries<TestKaplamaKumlamaOrtamSartlariSonuc> {
	
	@SuppressWarnings("unchecked")
	public static List<TestKaplamaKumlamaOrtamSartlariSonuc> getAllTestKaplamaKumlamaOrtamSartlariSonuc(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaKumlamaOrtamSartlariSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestKaplamaKumlamaOrtamSartlariSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
