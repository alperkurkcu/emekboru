package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaBetonYuzeyHazirligiSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class TestKaplamaBetonYuzeyHazirligiSonucManager extends
		CommonQueries<TestKaplamaBetonYuzeyHazirligiSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaBetonYuzeyHazirligiSonuc> getAllTestKaplamaBetonYuzeyHazirligiSonuc(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaBetonYuzeyHazirligiSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestKaplamaBetonYuzeyHazirligiSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
