package com.emekboru.jpaman.rulo;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.rulo.RuloTestKimyasalGirdi;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class RuloTestKimyasalGirdiManager extends
		CommonQueries<RuloTestKimyasalGirdi> {

	public void update(RuloTestKimyasalGirdi ruloTestKimyasalGirdi) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();

		manager.merge(ruloTestKimyasalGirdi);
		manager.flush();
		manager.getTransaction().commit();
		manager.close();
	}

	@SuppressWarnings("unchecked")
	public static List<RuloTestKimyasalGirdi> getByDokumNo(String prmDokumNo) {

		List<RuloTestKimyasalGirdi> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("RuloTestKimyasalGirdi.getByDokumNo")
					.setParameter("prmDokumNo", prmDokumNo).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
