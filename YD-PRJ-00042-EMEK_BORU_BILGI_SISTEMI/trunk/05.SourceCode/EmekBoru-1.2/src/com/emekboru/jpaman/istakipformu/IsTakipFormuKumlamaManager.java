/**
 * 
 */
package com.emekboru.jpaman.istakipformu;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.istakipformu.IsTakipFormuKumlama;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class IsTakipFormuKumlamaManager extends
		CommonQueries<IsTakipFormuKumlama> {

	@SuppressWarnings("unchecked")
	public static List<IsTakipFormuKumlama> getAllIsTakipFormuKumlama(
			Integer itemId, Integer icDis) {

		List<IsTakipFormuKumlama> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("IsTakipFormuKumlama.findAll")
					.setParameter("prmItemId", itemId)
					.setParameter("prmIcDis", icDis).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
