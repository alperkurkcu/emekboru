/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaGasBlisteringTestSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaGasBlisteringTestSonucManager extends
		CommonQueries<TestKaplamaGasBlisteringTestSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaGasBlisteringTestSonuc> getAllGasBlisteringTestSonuc(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaGasBlisteringTestSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestKaplamaGasBlisteringTestSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
