package com.emekboru.jpaman;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.KaynakSertlikTestsSpec;

public class KaynakSertlikTestSpecManager extends
		CommonQueries<KaynakSertlikTestsSpec> {

	// itemId sipari� kalemidir.
	@SuppressWarnings("unchecked")
	public Integer kontrol(Integer globalId, Integer itemId) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query findQuerie = manager.createNamedQuery("KSTS.kontrol");
		findQuerie.setParameter("prmGlobalId", globalId);
		findQuerie.setParameter("prmItemId", itemId);
		List<KaynakSertlikTestsSpec> resultList = findQuerie.getResultList();
		Integer count = resultList.size();
		if (count.equals(null)) {
			count = 0;
		}
		manager.getTransaction().commit();
		manager.close();
		return count;
	}

	@SuppressWarnings("unchecked")
	public List<KaynakSertlikTestsSpec> seciliKaynakSertlikTestTanim(
			Integer globalId, Integer prmItemId) {

		List<KaynakSertlikTestsSpec> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("KSTS.seciliKaynakSertlikTestTanim")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;

	}

	@SuppressWarnings("unchecked")
	public List<KaynakSertlikTestsSpec> findByItemId(Integer prmItemId) {

		List<KaynakSertlikTestsSpec> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("KSTS.findByItemId")
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;

	}

}
