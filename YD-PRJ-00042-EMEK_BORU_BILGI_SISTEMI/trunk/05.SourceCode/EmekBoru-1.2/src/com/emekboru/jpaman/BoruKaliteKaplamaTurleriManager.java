/**
 * 
 */
package com.emekboru.jpaman;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.BoruKaliteKaplamaTurleri;

/**
 * @author Alper K
 * 
 */
public class BoruKaliteKaplamaTurleriManager extends
		CommonQueries<BoruKaliteKaplamaTurleri> {

	@SuppressWarnings("unchecked")
	public static List<BoruKaliteKaplamaTurleri> getAllBoruKaliteKaplamaTurleri(
			Integer itemId) {

		List<BoruKaliteKaplamaTurleri> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("BoruKaliteKaplamaTurleri.findAll")
					.setParameter("prmItemId", itemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
