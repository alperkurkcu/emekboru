package com.emekboru.jpaman;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.NondestructiveTestsSpec;

public class NondestructiveTestsSpecManager extends
		CommonQueries<NondestructiveTestsSpec> {

	@SuppressWarnings("unchecked")
	public static List<NondestructiveTestsSpec> findByItemIdGlobalId(
			Integer itemId, Integer globalId) {

		List<NondestructiveTestsSpec> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"NondestructiveTestsSpec.findByItemIdGlobalId")
					.setParameter("prmItemId", itemId)
					.setParameter("prmGlobalId", globalId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<NondestructiveTestsSpec> findByItemId(Integer itemId) {

		List<NondestructiveTestsSpec> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("NondestructiveTestsSpec.findByItemId")
					.setParameter("prmItemId", itemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}