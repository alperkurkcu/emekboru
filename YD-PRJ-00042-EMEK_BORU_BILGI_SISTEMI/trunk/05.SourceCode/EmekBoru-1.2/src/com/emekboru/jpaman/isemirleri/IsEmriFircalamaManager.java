/**
 * 
 */
package com.emekboru.jpaman.isemirleri;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.isemri.IsEmriFircalama;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class IsEmriFircalamaManager extends CommonQueries<IsEmriFircalama> {

	@SuppressWarnings("unchecked")
	public static List<IsEmriFircalama> getAllIsEmriFircalama(Integer prmItemId) {

		List<IsEmriFircalama> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("IsEmriFircalama.findAllByItemId")
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
