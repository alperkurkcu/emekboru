/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaFlowCoatCureSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaFlowCoatCureSonucManager extends
		CommonQueries<TestKaplamaFlowCoatCureSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaFlowCoatCureSonuc> getAllTestKaplamaFlowCoatCureSonuc(
			int globalId, Integer pipeId) {

		List<TestKaplamaFlowCoatCureSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("TestKaplamaFlowCoatCureSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
