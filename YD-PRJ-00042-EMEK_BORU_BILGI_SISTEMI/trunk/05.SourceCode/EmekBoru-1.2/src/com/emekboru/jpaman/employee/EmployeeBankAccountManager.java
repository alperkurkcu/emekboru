/**
 * 
 */
package com.emekboru.jpaman.employee;

import java.sql.Timestamp;

import javax.faces.bean.ManagedProperty;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.employee.EmployeeBankAccount;
import com.emekboru.jpaman.CommonQueries;

/**
 * @author kursat
 * 
 */
public class EmployeeBankAccountManager extends
		CommonQueries<EmployeeBankAccount> {

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userCredentialsBean;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.emekboru.jpaman.CommonQueries#enterNew(java.lang.Object)
	 */

	public EmployeeBankAccountManager() {
		super();
		userCredentialsBean = new UserCredentialsBean();
	}

	@Override
	public boolean enterNew(EmployeeBankAccount employeeBankAccount) {

		Integer ekleyenKullanici = userCredentialsBean.getUser().getId();
		Timestamp eklemeZamani = new java.sql.Timestamp(
				new java.util.Date().getTime());
		
		if (employeeBankAccount.getEmployee().getEmployeeBankAccounts().size() <= 0)
			employeeBankAccount.setIsDefault(true);
		else
			employeeBankAccount.setIsDefault(false);

		employeeBankAccount.setEkleyenKullanici(ekleyenKullanici);
		employeeBankAccount.setEklemeZamani(eklemeZamani);

		return super.enterNew(employeeBankAccount);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.emekboru.jpaman.CommonQueries#updateEntity(java.lang.Object)
	 */
	@Override
	public void updateEntity(EmployeeBankAccount employeeBankAccount) {
		Integer guncelleyenKullanici = userCredentialsBean.getUser().getId();
		Timestamp guncellemeZamani = new java.sql.Timestamp(
				new java.util.Date().getTime());

		employeeBankAccount.setGuncelleyenKullanici(guncelleyenKullanici);
		employeeBankAccount.setGuncellemeZamani(guncellemeZamani);

		super.updateEntity(employeeBankAccount);
	}

}
