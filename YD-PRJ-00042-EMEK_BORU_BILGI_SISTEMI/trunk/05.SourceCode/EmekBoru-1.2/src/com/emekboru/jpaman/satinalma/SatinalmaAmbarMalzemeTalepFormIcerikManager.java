/**
 * 
 */
package com.emekboru.jpaman.satinalma;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.satinalma.SatinalmaAmbarMalzemeTalepFormIcerik;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class SatinalmaAmbarMalzemeTalepFormIcerikManager extends
		CommonQueries<SatinalmaAmbarMalzemeTalepFormIcerik> {

	@SuppressWarnings({ "unchecked" })
	public List<SatinalmaAmbarMalzemeTalepFormIcerik> getAllSatinalmaAmbarMalzemeTalepFormIcerik(
			Integer formId) {

		List<SatinalmaAmbarMalzemeTalepFormIcerik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaAmbarMalzemeTalepFormIcerik.findAll")
					.setParameter("prmFormId", formId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
