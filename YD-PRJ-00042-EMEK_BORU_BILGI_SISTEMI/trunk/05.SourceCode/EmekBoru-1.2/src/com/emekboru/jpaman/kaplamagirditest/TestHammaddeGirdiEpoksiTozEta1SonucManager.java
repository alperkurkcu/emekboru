/**
 * 
 */
package com.emekboru.jpaman.kaplamagirditest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiEpoksiTozEta1Sonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestHammaddeGirdiEpoksiTozEta1SonucManager extends
		CommonQueries<TestHammaddeGirdiEpoksiTozEta1Sonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestHammaddeGirdiEpoksiTozEta1Sonuc> getAllTestHammaddeGirdiEpoksiTozEta1Sonuc(
			Integer prmMaterialId) {

		List<TestHammaddeGirdiEpoksiTozEta1Sonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestHammaddeGirdiEpoksiTozEta1Sonuc.findAll")
					.setParameter("prmMaterialId", prmMaterialId)
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
