/**
 * 
 */
package com.emekboru.jpaman.isemirleri;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.isemri.IsEmriBetonKaplama;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class IsEmriBetonKaplamaManager extends
		CommonQueries<IsEmriBetonKaplama> {

	@SuppressWarnings("unchecked")
	public static List<IsEmriBetonKaplama> getAllIsEmriBetonKaplama(
			Integer prmItemId) {

		List<IsEmriBetonKaplama> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("IsEmriBetonKaplama.findAllByItemId")
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
