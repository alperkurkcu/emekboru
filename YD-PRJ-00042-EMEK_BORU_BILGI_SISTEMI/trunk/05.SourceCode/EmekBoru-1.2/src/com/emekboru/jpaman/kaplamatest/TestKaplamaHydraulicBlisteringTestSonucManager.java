/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaHydraulicBlisteringTestSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaHydraulicBlisteringTestSonucManager extends
		CommonQueries<TestKaplamaHydraulicBlisteringTestSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaHydraulicBlisteringTestSonuc> getAllHydraulicBlisteringTestSonuc(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaHydraulicBlisteringTestSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestKaplamaHydraulicBlisteringTestSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
