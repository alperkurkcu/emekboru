package com.emekboru.jpaman;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.emekboru.jpa.SystemUser;

public class UserManager extends CommonQueries<SystemUser> {

	@SuppressWarnings("unchecked")
	public List<SystemUser> userLogin(String username, String password) {

		List<SystemUser> userList = null;
		// EntityManager em = Factory.getEntityManager();
		EntityManager em = Persistence.createEntityManagerFactory("emekboru")
				.createEntityManager();

		try {
			userList = em
					.createNamedQuery("SystemUser.findByKullaniciAdiSifre")
					.setParameter("username", username)
					.setParameter("password", password).getResultList();
			return userList;

		} catch (Exception ex) {
			ex.printStackTrace();
			System.err.println("-HATA-UserManager.userLogin");
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SystemUser> findAllUsers() {

		List<SystemUser> userList = null;
		// EntityManager em = Factory.getEntityManager();
		EntityManager em = Persistence.createEntityManagerFactory("emekboru")
				.createEntityManager();

		try {
			userList = em.createNamedQuery("SystemUser.findAll")
					.getResultList();
			return userList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println("-HATA-UserManager.findAllUsers");
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<SystemUser> findAllUsersActivePassive(boolean prmActivePasive) {

		List<SystemUser> userList = null;
		EntityManager em = Factory.getEntityManager();

		try {
			Query query = em.createNamedQuery("SystemUser.findByActivePassive");
			query.setParameter("prmActivePasive", prmActivePasive);
			userList = query.getResultList();
			return userList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println("-HATA-UserManager.findAllUsers");
			return null;
		}
	}

	public SystemUser findBySystemUserId(Integer id) {

		SystemUser user = null;
		// EntityManager em = Factory.getEntityManager();
		EntityManager em = Persistence.createEntityManagerFactory("emekboru")
				.createEntityManager();

		try {
			user = (SystemUser) em.createNamedQuery("SystemUser.findByUserId")
					.setParameter("prmSystemUserId", id).getSingleResult();
			return user;
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("-HATA-UserManager.findBySystemUserId");
			return null;
		}
	}

}
