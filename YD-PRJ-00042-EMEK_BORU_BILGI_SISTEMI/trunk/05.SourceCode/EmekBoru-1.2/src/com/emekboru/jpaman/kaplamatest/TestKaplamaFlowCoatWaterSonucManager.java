/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaFlowCoatWaterSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaFlowCoatWaterSonucManager extends
		CommonQueries<TestKaplamaFlowCoatWaterSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaFlowCoatWaterSonuc> getAllTestKaplamaFlowCoatWaterSonuc(
			int globalId, Integer pipeId) {

		List<TestKaplamaFlowCoatWaterSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("TestKaplamaFlowCoatWaterSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
