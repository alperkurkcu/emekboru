/**
 * 
 */
package com.emekboru.jpaman.tahribatsiztestsonuc;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizPenetrantSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestTahribatsizPenetrantSonucManager extends
		CommonQueries<TestTahribatsizPenetrantSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestTahribatsizPenetrantSonuc> getAllTestTahribatsizPenetrantSonuc(
			Integer pipeId) {

		List<TestTahribatsizPenetrantSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("TestTahribatsizPenetrantSonuc.findAll")
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizPenetrantSonuc> findBetweenTwoParameters(
			String prmFirstBarcode, String prmSecondBarcode) {

		List<TestTahribatsizPenetrantSonuc> result = null;
		EntityManager em = Factory.getEntityManager();

		try {
			try {
				String query = "SELECT r FROM TestTahribatsizPenetrantSonuc r WHERE r.pipe.pipeId in (select p.pipeId from Pipe p where p.pipeBarkodNo BETWEEN :prmFirstBarcode AND :prmSecondBarcode ) order by r.pipe.pipeId ASC";
				result = em.createQuery(query)
						.setParameter("prmFirstBarcode", prmFirstBarcode)
						.setParameter("prmSecondBarcode", prmSecondBarcode)
						.getResultList();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			return result;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
