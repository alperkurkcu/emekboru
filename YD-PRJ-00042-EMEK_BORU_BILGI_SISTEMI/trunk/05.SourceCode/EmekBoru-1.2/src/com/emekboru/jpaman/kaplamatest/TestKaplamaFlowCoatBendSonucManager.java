/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaFlowCoatBendSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaFlowCoatBendSonucManager extends
		CommonQueries<TestKaplamaFlowCoatBendSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaFlowCoatBendSonuc> getAllTestKaplamaFlowCoatBendSonuc(
			int globalId, Integer pipeId) {

		List<TestKaplamaFlowCoatBendSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("TestKaplamaFlowCoatBendSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
