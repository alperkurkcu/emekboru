/**
 * 
 */
package com.emekboru.jpaman.satinalma;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.satinalma.SatinalmaSatinalmaKarariIcerik;
import com.emekboru.jpa.satinalma.SatinalmaTeklifIsteme;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class SatinalmaSatinalmaKarariIcerikManager extends
		CommonQueries<SatinalmaSatinalmaKarariIcerik> {

	@SuppressWarnings("unchecked")
	public List<SatinalmaSatinalmaKarariIcerik> satinalmaKarariListDoldur(
			Integer prmKararId) {

		List<SatinalmaSatinalmaKarariIcerik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {

			String query = new String();
			// query =
			// "SELECT sti FROM SatinalmaTeklifIsteme sti INNER JOIN SatinalmaTeklifIstemeIcerik ON SatinalmaTeklifIstemeIcerik.satinalmaTeklifIsteme.id = sti.id "
			// +
			// "INNER JOIN SatinalmaTakipDosyalarIcerik ON SatinalmaTeklifIstemeIcerik.satinalmaTakipDosyalarIcerik.id = SatinalmaTakipDosyalarIcerik.id "
			// +
			// "INNER JOIN SatinalmaMalzemeHizmetTalepFormuIcerik ON SatinalmaTakipDosyalarIcerik.satinalmaMalzemeHizmetTalepFormuIcerik.id = SatinalmaMalzemeHizmetTalepFormuIcerik.id "
			// +
			// "INNER JOIN stokUrunKartlari ON stokUrunKartlari.id = SatinalmaMalzemeHizmetTalepFormuIcerik.stokUrunKartlari.id "
			// +
			// "INNER JOIN SatinalmaSatinalmaKarariIcerik ON SatinalmaSatinalmaKarariIcerik.satinalmaTeklifIstemeIcerik.id = SatinalmaTeklifIstemeIcerik.id "
			// +
			// "AND SatinalmaSatinalmaKarariIcerik.satinalmaTeklifIsteme.id = sti.id AND SatinalmaSatinalmaKarariIcerik.satinalmaTeklifIstemeIcerik.id = SatinalmaTeklifIstemeIcerik.id";

			// query =
			// "SELECT ski FROM SatinalmaSatinalmaKarariIcerik ski, SatinalmaTeklifIsteme sti, SatinalmaTeklifIstemeIcerik stii WHERE ski.satinalmaTeklifIstemeIcerik.id=stii.id AND stii.satinalmaTeklifIsteme.id = sti.id order by sti.satinalmaMusteri.commercialName";

			query = "SELECT ski FROM SatinalmaSatinalmaKarariIcerik ski WHERE ski.satinalmaSatinalmaKarari.id=:prmKararId order by ski.satinalmaTeklifIsteme.satinalmaMusteri.id";

			Query findQuery = em.createQuery(query);
			result = findQuery.setParameter("prmKararId", prmKararId)
					.getResultList();

			// result = em.createNamedQuery(
			// "SatinalmaTeklifIsteme.satinalmaKarariListDoldur")
			// .getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaSatinalmaKarariIcerik> satinalmaKarariListDoldurByFirma(
			Integer prmKararId, Integer prmFirmaId) {

		List<SatinalmaSatinalmaKarariIcerik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {

			String query = new String();

			query = "SELECT ski FROM SatinalmaSatinalmaKarariIcerik ski WHERE ski.satinalmaSatinalmaKarari.id=:prmKararId and ski.satinalmaTeklifIsteme.satinalmaMusteri.id =:prmFirmaId order by ski.id";

			Query findQuery = em.createQuery(query);
			result = findQuery.setParameter("prmKararId", prmKararId)
					.setParameter("prmFirmaId", prmFirmaId).getResultList();

			// result = em.createNamedQuery(
			// "SatinalmaTeklifIsteme.satinalmaKarariListDoldur")
			// .getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaSatinalmaKarariIcerik> findKabulEdilenByKararId(
			Integer prmKararId) {

		List<SatinalmaSatinalmaKarariIcerik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaSatinalmaKarariIcerik.findKabulEdilenByKararId")
					.setParameter("prmKararId", prmKararId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaTeklifIsteme> teklifIstemeByKararId(Integer prmKararId) {

		List<SatinalmaTeklifIsteme> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaSatinalmaKarariIcerik.teklifIstemeByKararId")
					.setParameter("prmKararId", prmKararId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaSatinalmaKarariIcerik> kararIcerikByKararId(
			Integer prmKararId) {

		List<SatinalmaSatinalmaKarariIcerik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaSatinalmaKarariIcerik.kararIcerikByKararId")
					.setParameter("prmKararId", prmKararId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
