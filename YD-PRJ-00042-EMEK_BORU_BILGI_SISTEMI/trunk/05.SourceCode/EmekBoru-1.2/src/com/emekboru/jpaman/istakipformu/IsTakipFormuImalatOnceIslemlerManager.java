/**
 * 
 */
package com.emekboru.jpaman.istakipformu;

import com.emekboru.jpa.istakipformu.IsTakipFormuImalatOnceIslemler;
import com.emekboru.jpaman.CommonQueries;

/**
 * @author Alper K
 * 
 */
public class IsTakipFormuImalatOnceIslemlerManager extends
		CommonQueries<IsTakipFormuImalatOnceIslemler> {

}
