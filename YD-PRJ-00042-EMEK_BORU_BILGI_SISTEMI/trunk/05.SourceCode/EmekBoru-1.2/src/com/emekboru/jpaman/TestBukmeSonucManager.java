package com.emekboru.jpaman;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.TestBukmeSonuc;
import com.emekboru.jpa.sales.order.SalesItem;

public class TestBukmeSonucManager extends CommonQueries<TestBukmeSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestBukmeSonuc> getAllBukmeTestSonuc(int globalId,
			Integer pipeId) {

		List<TestBukmeSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestBukmeSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<TestBukmeSonuc> getBySalesItem(SalesItem prmSalesItem,
			int prmGlobalId) {

		List<TestBukmeSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestBukmeSonuc.getBySalesItem")
					.setParameter("prmSalesItem", prmSalesItem.getItemId())
					.setParameter("prmGlobalId", prmGlobalId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<TestBukmeSonuc> getByDokumNo(String prmDokumNo) {

		List<TestBukmeSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestBukmeSonuc.getByDokumNo")
					.setParameter("prmDokumNo", prmDokumNo).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<TestBukmeSonuc> getByDokumNoSalesItem(String prmDokumNo,
			Integer prmItemId) {

		List<TestBukmeSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("TestBukmeSonuc.getByDokumNoSalesItem")
					.setParameter("prmDokumNo", prmDokumNo)
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
