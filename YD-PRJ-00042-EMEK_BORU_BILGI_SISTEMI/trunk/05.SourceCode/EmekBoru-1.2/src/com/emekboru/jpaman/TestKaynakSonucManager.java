package com.emekboru.jpaman;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.TestKaynakSonuc;
import com.emekboru.jpa.sales.order.SalesItem;

public class TestKaynakSonucManager extends CommonQueries<TestKaynakSonuc> {

	@SuppressWarnings({ "unchecked" })
	public static List<TestKaynakSonuc> getAllTestCekmeSonuc(int globalId,
			Integer pipeId) {

		List<TestKaynakSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestKaynakSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<TestKaynakSonuc> getBySalesItem(SalesItem prmSalesItem,
			int prmGlobalId) {

		List<TestKaynakSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestKaynakSonuc.getBySalesItem")
					.setParameter("prmSalesItem", prmSalesItem.getItemId())
					.setParameter("prmGlobalId", prmGlobalId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<TestKaynakSonuc> getByDokumNoGlobalId(String prmDokumNo,
			int prmGlobalId) {

		List<TestKaynakSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("TestKaynakSonuc.getByDokumNoGlobalId")
					.setParameter("prmDokumNo", prmDokumNo)
					.setParameter("prmGlobalId", prmGlobalId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
