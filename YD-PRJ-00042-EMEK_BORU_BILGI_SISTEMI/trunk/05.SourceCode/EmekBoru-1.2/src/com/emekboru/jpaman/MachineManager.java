package com.emekboru.jpaman;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.Machine;
import com.emekboru.jpa.MachineRuloLink;

@SuppressWarnings({ "unchecked" })
public class MachineManager extends CommonQueries<Machine> {

	// public List<Machine> findSpiralMachines() {
	//
	// ArrayList<WhereClauseArgs> conditionList = new
	// ArrayList<WhereClauseArgs>();
	// WhereClauseArgs arg1 = new WhereClauseArgs.Builder()
	// .setFieldName("machineType.isSpiral").setValue(true).build();
	// conditionList.add(arg1);
	// return selectFromWhereQuerie(Machine.class, conditionList);
	// }
	//
	// public List<Machine> findManufacturingMachines() {
	//
	// ArrayList<WhereClauseArgs> conditionList = new
	// ArrayList<WhereClauseArgs>();
	// WhereClauseArgs arg1 = new WhereClauseArgs.Builder()
	// .setFieldName("machineType.isManufacturing").setValue(true)
	// .build();
	// conditionList.add(arg1);
	// return selectFromWhereQuerie(Machine.class, conditionList);
	// }
	//
	// public List<Machine> findCoatingMachines() {
	//
	// ArrayList<WhereClauseArgs> conditionList = new
	// ArrayList<WhereClauseArgs>();
	// WhereClauseArgs arg1 = new WhereClauseArgs.Builder()
	// .setFieldName("machineType.isCoating").setValue(true).build();
	// conditionList.add(arg1);
	// return selectFromWhereQuerie(Machine.class, conditionList);
	// }

	// alper - makinedeki ruloya orderId setleme
	public List<MachineRuloLink> loadMachineRulo(Integer machineId) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query findQuerie = manager
				.createNamedQuery("MachineRuloLink.findByMachineIdAndEnded");
		findQuerie.setParameter("prmMachineId", machineId);
		findQuerie.setParameter("prmEnded", false);
		List<MachineRuloLink> resultList = findQuerie.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return resultList;
	}

	// alper - makinedeki ruloya orderId setleme

	public List<Machine> spiralMachine() {

		List<Machine> result = new ArrayList<Machine>();

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query findQuerie = manager
				.createNamedQuery("Machine.activeSpiralMachine");
		result = findQuerie.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return result;

	}

}
