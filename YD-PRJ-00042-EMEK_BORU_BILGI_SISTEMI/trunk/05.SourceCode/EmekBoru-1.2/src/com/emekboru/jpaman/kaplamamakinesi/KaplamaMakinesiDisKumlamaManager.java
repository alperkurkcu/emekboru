/**
 * 
 */
package com.emekboru.jpaman.kaplamamakinesi;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiDisKumlama;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KaplamaMakinesiDisKumlamaManager extends
		CommonQueries<KaplamaMakinesiDisKumlama> {

	@SuppressWarnings("unchecked")
	public static List<KaplamaMakinesiDisKumlama> getAllKaplamaMakinesiDisKumlama(
			Integer prmPipeId) {

		List<KaplamaMakinesiDisKumlama> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"KaplamaMakinesiDisKumlama.findAllByPipeId")
					.setParameter("prmPipeId", prmPipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
