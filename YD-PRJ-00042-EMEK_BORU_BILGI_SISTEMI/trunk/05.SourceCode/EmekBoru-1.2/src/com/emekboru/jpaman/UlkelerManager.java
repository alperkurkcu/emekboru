package com.emekboru.jpaman;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.Ulkeler;

public class UlkelerManager {

	public static void addOrUpdateUlkeler(Ulkeler ulkeler) {

		EntityManager em = Factory.getEntityManager();
		em.getTransaction().begin();
		if (ulkeler.getId() == null)
			em.persist(ulkeler);
		else {
			em.merge(ulkeler);
		}
		em.getTransaction().commit();
		// em.close();
	}

	@SuppressWarnings("unchecked")
	public static List<Ulkeler> getAllUlkeler() {

		List<Ulkeler> result = null;
		EntityManager em = Factory.getEntityManager();
		// em.getTransaction().begin();
		try {
			result = em.createNamedQuery("Ulkeler.findAll").getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		// em.close();
		return result;
	}

	public static void deleteUlkeler(Ulkeler ulkelerForm) {

		EntityManager em = Factory.getEntityManager();
		em.getTransaction().begin();
		if (ulkelerForm.getId() == null) {

		} else {
			em.remove(ulkelerForm);
		}
		em.getTransaction().commit();
		//em.close();
	}

}
