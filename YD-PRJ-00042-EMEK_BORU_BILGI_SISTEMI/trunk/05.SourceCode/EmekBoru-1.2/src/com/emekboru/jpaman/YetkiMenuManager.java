package com.emekboru.jpaman;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.YetkiMenu;
import com.emekboru.utils.YetkiTreeNode;

public class YetkiMenuManager extends CommonQueries<YetkiMenu> {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List<YetkiMenu> getAllYetkiMenuList() {
		List result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("YetkiMenu.findAll").getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	public static void addOrUpdateLkYetkiMenu(YetkiMenu yetkiMenu) {
		EntityManager em = Factory.getEntityManager();
		em.getTransaction().begin();
		if (yetkiMenu.getId() != null) {
			em.merge(yetkiMenu);
		} else {
			em.persist(yetkiMenu);
		}
		em.getTransaction().commit();
	}

	public static YetkiMenu getYetkiMenuById(Integer id) {
		EntityManager em = Factory.getEntityManager();
		return em.find(YetkiMenu.class, id);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List<YetkiMenu> getAllPages() {

		List result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("YetkiMenu.findAllPages")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	public static YetkiTreeNode getAllYetkiMenuTree(boolean allSelectable) {
		List<YetkiMenu> yetkiListesi = getAllYetkiMenuList();
		HashMap<Integer, YetkiTreeNode> menuHm = addPagesToHashMap(
				yetkiListesi, allSelectable);

		List<YetkiTreeNode> menuList = new ArrayList<YetkiTreeNode>();
		YetkiTreeNode root = new YetkiTreeNode("Root", null);
		for (int i = 0; i < yetkiListesi.size(); i++) {
			YetkiMenu aa = (YetkiMenu) yetkiListesi.get(i);
			if (aa.getParentMenuId() != null) {
				if (menuHm.containsKey(aa.getParentMenuId())) {// eger
																// parentmenu
																// varsa
					YetkiTreeNode m = (YetkiTreeNode) menuHm.get(aa
							.getParentMenuId());
					m.getChildren().add((YetkiTreeNode) menuHm.get(aa.getId()));
				} else {
					menuHm.remove(aa.getId());
				}
			} else {
				YetkiTreeNode node = menuHm.get(aa.getId());
				node.setParent(root);
				menuList.add(node);
			}
		}
		return root;
	}

	private static HashMap<Integer, YetkiTreeNode> addPagesToHashMap(
			List<YetkiMenu> yetkiListesi, boolean allSelectable) {
		HashMap<Integer, YetkiTreeNode> menuHm = new HashMap<Integer, YetkiTreeNode>();
		for (int i = 0; i < yetkiListesi.size(); i++) {
			if (!menuHm.containsKey(yetkiListesi.get(i).getId())) {
				YetkiTreeNode yetkiTreeNode = new YetkiTreeNode(yetkiListesi
						.get(i).getEkranTanimi(), null);

				if (!yetkiListesi.get(i).isSubMenu()) {
					if (!allSelectable)
						yetkiTreeNode.setSelectable(false);
					else
						yetkiTreeNode.setSelectable(true);
				}

				yetkiTreeNode.setObjectId(yetkiListesi.get(i).getId());
				menuHm.put(yetkiListesi.get(i).getId(), yetkiTreeNode);
			}
		}
		return menuHm;
	}

	public static YetkiMenu getYetkiMenuByAbbr(String sayfaStr) {
		// TODO Auto-generated method stub
		return null;
	}

}
