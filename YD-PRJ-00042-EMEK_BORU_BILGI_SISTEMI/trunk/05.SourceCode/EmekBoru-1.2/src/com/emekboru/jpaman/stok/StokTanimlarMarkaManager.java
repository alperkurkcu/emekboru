/**
 * 
 */
package com.emekboru.jpaman.stok;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.stok.StokTanimlarMarka;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class StokTanimlarMarkaManager extends CommonQueries<StokTanimlarMarka> {

	@SuppressWarnings("unchecked")
	public List<StokTanimlarMarka> getAllStokTanimlarMarka() {

		List<StokTanimlarMarka> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("StokTanimlarMarka.findAll")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
