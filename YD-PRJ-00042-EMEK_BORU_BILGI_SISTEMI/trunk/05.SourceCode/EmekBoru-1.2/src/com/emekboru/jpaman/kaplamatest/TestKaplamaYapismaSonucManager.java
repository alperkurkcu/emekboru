package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaYapismaSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class TestKaplamaYapismaSonucManager extends
		CommonQueries<TestKaplamaYapismaSonuc> {
	@SuppressWarnings("unchecked")
	public static List<TestKaplamaYapismaSonuc> getAllKaplamaYapismaSonucTest(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaYapismaSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestKaplamaYapismaSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
