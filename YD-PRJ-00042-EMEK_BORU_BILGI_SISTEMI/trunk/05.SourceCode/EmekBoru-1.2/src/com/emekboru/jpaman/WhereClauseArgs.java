package com.emekboru.jpaman;


public class WhereClauseArgs<T> {

	private String fieldName;
	private String comparativeClause;
	private T value;
	private boolean isComplexKey;
	private boolean nextOrClause;
	/*
	 * in case we use the same field more than once in a query we have to give its number of occurancies
	 * of that field in the query because when we add the named query parameter we must have different parameters
	 * names in the right side, for example: WHERE a.param>param1 and a.param<param2 (if we dont do this
	 * we would have WHERE a.param>param AND a.param<param). 
	 * If it happens that we dont want to use AND but OR clause we have to set the andOrClause to TRUE
	 */
	private Integer orderOfOccurance;
	
	public WhereClauseArgs(Builder<T> builder){
		this.fieldName 			= builder.fieldName;
		this.comparativeClause 	= builder.comparativeClause;
		this.value 				= builder.value;
		this.isComplexKey 		= builder.isComplexKey;
		this.nextOrClause 		= builder.nextOrClause;
		this.orderOfOccurance	= builder.orderOfOccurance;
	}

	public static class Builder<T>{

		private String fieldName;
		private String comparativeClause;
		private T value;
		private boolean isComplexKey;
		private boolean nextOrClause;
		private Integer orderOfOccurance;
		
		public  Builder(){
			this.comparativeClause = JpqlComparativeClauses.NQ_EQUAL;
			this.isComplexKey = false;
		}
		public Builder<T> setFieldName(String fieldName) {
			this.fieldName = fieldName;
			return this;
		}
		
		public Builder<T> setComparativeClause(String comparativeClause) {
			this.comparativeClause = comparativeClause;
			return this;
		}
		
		public Builder<T> setValue(T value) {
			this.value = value;
			return this;
		}
		
		public Builder<T> setComplexKey(boolean isComplexKey) {
			this.isComplexKey = isComplexKey;
			return this;
		}
		
		public Builder<T> setNextOrClause(boolean nextOrClause) {
			this.nextOrClause = nextOrClause;
			return this;
		}
		
		public Builder<T> setOrderOfOccurance(int orderOfOccurance) {
			this.orderOfOccurance = orderOfOccurance;
			return this;
		}
		
		public WhereClauseArgs<T> build(){
			return new WhereClauseArgs<T>(this);
		}
	}
	
	public String getFieldName() {
		return fieldName;
	}

	public String getComparativeClause() {
		return comparativeClause;
	}

	public boolean isComplexKey() {
		return isComplexKey;
	}

	public T getValue() {
		return value;
	}

	public Integer getOrderOfOccurance() {
		return orderOfOccurance;
	}

	public boolean isNextOrClause() {
		return nextOrClause;
	}

}
