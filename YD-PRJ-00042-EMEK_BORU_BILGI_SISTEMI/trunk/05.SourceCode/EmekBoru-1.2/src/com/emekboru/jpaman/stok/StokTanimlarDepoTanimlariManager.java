/**
 * 
 */
package com.emekboru.jpaman.stok;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.stok.StokTanimlarDepoTanimlari;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class StokTanimlarDepoTanimlariManager extends
		CommonQueries<StokTanimlarDepoTanimlari> {

	@SuppressWarnings("unchecked")
	public List<StokTanimlarDepoTanimlari> getAllStokTanimlarDepoTanimlari() {

		List<StokTanimlarDepoTanimlari> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("StokTanimlarDepoTanimlari.findAll")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
