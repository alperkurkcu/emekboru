/**
 * 
 */
package com.emekboru.jpaman.isemirleri;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.isemri.IsEmriDisKumlama;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class IsEmriDisKumlamaManager extends CommonQueries<IsEmriDisKumlama> {

	@SuppressWarnings("unchecked")
	public static List<IsEmriDisKumlama> getAllIsEmriDisKumlama(
			Integer prmItemId) {

		List<IsEmriDisKumlama> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("IsEmriDisKumlama.findAllByItemId")
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
