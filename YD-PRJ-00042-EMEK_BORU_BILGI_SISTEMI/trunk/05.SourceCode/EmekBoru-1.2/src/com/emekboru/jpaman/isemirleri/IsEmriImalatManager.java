/**
 * 
 */
package com.emekboru.jpaman.isemirleri;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.isemri.IsEmriImalat;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class IsEmriImalatManager extends CommonQueries<IsEmriImalat> {

	@SuppressWarnings("unchecked")
	public static List<IsEmriImalat> getAllIsEmriImalat(Integer prmItemId) {

		List<IsEmriImalat> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("IsEmriImalat.findAllByItemId")
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
