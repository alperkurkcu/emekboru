/**
 * 
 */
package com.emekboru.jpaman.satinalma;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.satinalma.SatinalmaMalzemeHizmetTalepFormu;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class SatinalmaMalzemeHizmetTalepFormuManager extends
		CommonQueries<SatinalmaMalzemeHizmetTalepFormu> {

	@SuppressWarnings("unchecked")
	public List<SatinalmaMalzemeHizmetTalepFormu> getAllSatinalmaMalzemeHizmetTalepFormu() {

		List<SatinalmaMalzemeHizmetTalepFormu> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery(
					"SatinalmaMalzemeHizmetTalepFormu.findAll").getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaMalzemeHizmetTalepFormu> getUserSatinalmaMalzemeHizmetTalepFormu(
			Integer prmEmployeeId) {

		List<SatinalmaMalzemeHizmetTalepFormu> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaMalzemeHizmetTalepFormu.findByUserId")
					.setParameter("prmEmployeeId", prmEmployeeId)
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaMalzemeHizmetTalepFormu> getOnayBekleyenlerSatinalmaMalzemeHizmetTalepFormu(
			Integer prmEmployeeId) {

		List<SatinalmaMalzemeHizmetTalepFormu> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaMalzemeHizmetTalepFormu.findOnayBekleyenler")
					.setParameter("prmEmployeeId", prmEmployeeId)
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaMalzemeHizmetTalepFormu> getGMOnayBekleyenlerSatinalmaMalzemeHizmetTalepFormu(
			Integer prmEmployeeId) {

		List<SatinalmaMalzemeHizmetTalepFormu> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaMalzemeHizmetTalepFormu.findGMOnayBekleyenler")
					.setParameter("prmEmployeeId", prmEmployeeId)
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaMalzemeHizmetTalepFormu> getOnayGecmislerSatinalmaMalzemeHizmetTalepFormu(
			Integer prmEmployeeId) {

		List<SatinalmaMalzemeHizmetTalepFormu> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaMalzemeHizmetTalepFormu.findHistory")
					.setParameter("prmEmployeeId", prmEmployeeId)
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaMalzemeHizmetTalepFormu> getTumSatinalmayaDusenlerSatinalmaMalzemeHizmetTalepFormu() {

		List<SatinalmaMalzemeHizmetTalepFormu> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery(
					"SatinalmaMalzemeHizmetTalepFormu.findSatinalmayaDusenler")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaMalzemeHizmetTalepFormu> findYonlendirilenler(
			Integer prmUserId) {

		List<SatinalmaMalzemeHizmetTalepFormu> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaMalzemeHizmetTalepFormu.findYonlendirilenler")
					.setParameter("prmUserId", prmUserId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings({ "unused", "unchecked" })
	public String getOtomatikSayi() {

		List<SatinalmaMalzemeHizmetTalepFormu> result = null;
		Integer sayi = null;
		String sonuc = null;
		String sonSayi = null;
		Integer dateYear1 = null;
		Integer dateYear2 = null;

		DateFormat dateFormat = new SimpleDateFormat("yyyy");
		Date date = new Date();

		dateYear1 = date.getYear() + 1900;
		dateYear2 = date.getYear() + 1901;

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		try {
			String query = new String();
			query = "SELECT c FROM SatinalmaMalzemeHizmetTalepFormu c WHERE c.id=(Select max(d.id) from SatinalmaMalzemeHizmetTalepFormu d) and c.eklemeZamani BETWEEN '"
					+ dateYear1 + ".01.01' AND '" + dateYear2 + ".01.01'";

			Query findQuery = manager.createQuery(query);
			result = findQuery.getResultList();
			manager.getTransaction().commit();
			manager.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (result.size() == 0) {
			return sonuc = "MTF-" + dateFormat.format(date) + "/"
					+ (new DecimalFormat("0000000").format(1));
		} else {
			sayi = Integer.parseInt((result.get(0).getFormSayisi()).substring(
					9, 16)) + 1;
			return sonuc = "MTF-" + dateFormat.format(date) + "/"
					+ (new DecimalFormat("0000000").format(sayi));
		}
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaMalzemeHizmetTalepFormu> getAllSatinalmaMalzemeHizmetTalepFormuByFormId(
			Integer formId) {

		List<SatinalmaMalzemeHizmetTalepFormu> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaMalzemeHizmetTalepFormu.findAllByFormId")
					.setParameter("prmFormId", formId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
