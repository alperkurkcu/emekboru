package com.emekboru.jpaman;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.primefaces.model.SortOrder;

import com.emekboru.jpa.Machine;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.rulo.Rulo;
import com.emekboru.utils.UtilInsCore;

@SuppressWarnings({ "unchecked" })
public class PipeManager extends CommonQueries<Pipe> {

	private static String PIPES_IN_MACHINES_QUERY = "SELECT p from Pipe p WHERE p.pipeId=: (SELECT "
			+ " link.pipe.pipeId FROM MachinePipeLink link "
			+ "WHERE link.machine.machineId=:machineId "
			+ "AND link.ended=false)";

	private static String LAST_PIPE_IN_MACHINES_QUERY = "SELECT p from Pipe p WHERE p.pipeId = "
			+ "(SELECT link1.pipe.pipeId FROM MachinePipeLink link1 WHERE link1.machine.machineId=:machineId AND link1.ended=false and "
			+ "link1.id=(select max(link2.id) from MachinePipeLink link2))";

	public List<Integer> getItemMaxPipeIndex(Integer itemId) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		// pipeQueries.xml de
		Query itemIndexQuery = manager.createNamedQuery("Pipe.getMaxPipeIndex");
		itemIndexQuery.setParameter("prmItemId", itemId);
		List<Integer> getItemIndexes = itemIndexQuery.setMaxResults(1)
				.getResultList();
		return getItemIndexes;
	}

	public List<Pipe> findByItemId(int itemId) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query itemIndexQuery = manager.createNamedQuery("Pipe.findByItemId");
		itemIndexQuery.setParameter("prmItemId", itemId);
		List<Pipe> getList = itemIndexQuery.getResultList();
		return getList;
	}

	public List<Pipe> findByItemIdASC(int itemId) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query itemIndexQuery = manager.createNamedQuery("Pipe.findByItemIdASC");
		itemIndexQuery.setParameter("prmItemId", itemId);
		List<Pipe> getList = itemIndexQuery.getResultList();
		return getList;
	}

	public List<Pipe> findBandEkli(int prmIndex, int prmItemId) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query itemIndexQuery = manager.createNamedQuery("Pipe.bandEkli");
		itemIndexQuery.setParameter("prmIndex", prmIndex).setParameter(
				"prmItemId", prmItemId);
		List<Pipe> getList = itemIndexQuery.getResultList();
		return getList;
	}

	public List<Pipe> findByItemIdAnDDestructiveTest(int itemId) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query itemIndexQuery = manager
				.createNamedQuery("Pipe.findByItemIdAndDestructiveTest");
		itemIndexQuery.setParameter("prmItemId", itemId);
		List<Pipe> getList = itemIndexQuery.getResultList();
		return getList;
	}

	public List<Pipe> findByBarkodNo(String pipeBarkodNo) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query itemIndexQuery = manager.createNamedQuery("Pipe.findByBarkodNo");
		itemIndexQuery.setParameter("prmBarkodNo", pipeBarkodNo);
		List<Pipe> getList = itemIndexQuery.getResultList();
		return getList;
	}

	public List<Pipe> pipesInMachine(Machine machine) {

		List<Pipe> result = new ArrayList<Pipe>();
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query query = manager.createQuery(PIPES_IN_MACHINES_QUERY);
		query.setParameter("machineId", machine.getMachineId());
		result = query.getResultList();
		manager.getTransaction().commit();
		return result;
	}

	public List<Pipe> allPipesInMachine(Machine machine, int gunFarki) {

		Calendar cal = Calendar.getInstance();
		Date today = cal.getTime();
		String prmDate1 = new SimpleDateFormat("yyyy.MM.dd").format(today);
		cal.add(Calendar.DATE, -gunFarki);
		Date todayBefore7days = cal.getTime();
		String prmDate2 = new SimpleDateFormat("yyyy.MM.dd")
				.format(todayBefore7days);

		String ALL_PIPES_IN_MACHINES_QUERY = "SELECT p from Pipe p WHERE p.pipeId IN (SELECT "
				+ " link.pipe.pipeId FROM MachinePipeLink link "
				+ "WHERE link.machine.machineId=:prmMachineId "
				+ "AND link.ended=false) and p.eklemeZamani BETWEEN '"
				+ prmDate2
				+ "' AND '"
				+ prmDate1
				+ "' order by p.eklemeZamani desc";

		List<Pipe> result = new ArrayList<Pipe>();
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query query = manager.createQuery(ALL_PIPES_IN_MACHINES_QUERY);
		query.setParameter("prmMachineId", machine.getMachineId());
		result = query.getResultList();
		manager.getTransaction().commit();
		return result;
	}

	public List<Pipe> allPipesInMachine(Machine machine, Date startDate,
			Date endDate) {

		String prmDate1 = new SimpleDateFormat("yyyy.MM.dd").format(endDate);

		String prmDate2 = new SimpleDateFormat("yyyy.MM.dd").format(startDate);

		String ALL_PIPES_IN_MACHINES_QUERY = "SELECT p from Pipe p WHERE p.pipeId IN (SELECT "
				+ " link.pipe.pipeId FROM MachinePipeLink link "
				+ "WHERE link.machine.machineId=:prmMachineId "
				+ "AND link.ended=false) and p.eklemeZamani BETWEEN '"
				+ prmDate2
				+ "' AND '"
				+ prmDate1
				+ "' order by p.eklemeZamani desc";

		List<Pipe> result = new ArrayList<Pipe>();
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query query = manager.createQuery(ALL_PIPES_IN_MACHINES_QUERY);
		query.setParameter("prmMachineId", machine.getMachineId());
		result = query.getResultList();
		manager.getTransaction().commit();
		return result;
	}

	public List<Pipe> lastPipeInMachine(Machine machine) {

		List<Pipe> result = new ArrayList<Pipe>();
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query query = manager.createQuery(LAST_PIPE_IN_MACHINES_QUERY);
		query.setParameter("machineId", machine.getMachineId());
		result = query.getResultList();
		manager.getTransaction().commit();
		return result;
	}

	public List<Integer> getMaxPipeId() {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query query = manager.createNamedQuery("Pipe.getMaxPipeId");
		List<Integer> getItemIndexes = query.getResultList();
		if (getItemIndexes.get(0) == null) {
			getItemIndexes.add(0, 0);
		}
		return getItemIndexes;
	}

	public void insertPipe(Pipe pipe) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query query = manager
				.createNativeQuery("INSERT INTO pipe VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		// query.setParameter(1, pipe.getOrder().getOrderId());
		query.setParameter(1, 0);
		query.setParameter(2, pipe.getPipeId());
		query.setParameter(3, pipe.getDisCap());
		query.setParameter(4, pipe.getAgirligi());
		query.setParameter(5, pipe.getBoy());
		query.setParameter(6, pipe.getKaynakBoy());
		query.setParameter(7, pipe.getRadialKacik());
		query.setParameter(8, pipe.getEtKalinligi());
		query.setParameter(9, pipe.getMagneticKalici());
		query.setParameter(10, pipe.getKaynakDikici());
		query.setParameter(11, pipe.getDogrusalik());
		query.setParameter(12, pipe.getNote());
		query.setParameter(13, pipe.getCapAUcu());
		query.setParameter(14, pipe.getCapBUcu());
		query.setParameter(15, pipe.getCapMin());
		query.setParameter(16, pipe.getCapMax());
		query.setParameter(17, pipe.getOvallikAMin());
		query.setParameter(18, pipe.getOvallikAMax());
		query.setParameter(19, pipe.getOvallikBMin());
		query.setParameter(20, pipe.getOvallikBMax());
		query.setParameter(21, pipe.getOvallikBodyMin());
		query.setParameter(22, pipe.getOvallikBodyMax());
		query.setParameter(23, pipe.getAucuKaynak());
		query.setParameter(24, pipe.getAucuAlin());
		query.setParameter(25, pipe.getAucuBoru());
		query.setParameter(26, pipe.getBucuKaynak());
		query.setParameter(27, pipe.getBucuAlin());
		query.setParameter(28, pipe.getBucuBoru());
		query.setParameter(29, false);
		query.setParameter(30, pipe.getPipeIndex());
		query.setParameter(31, pipe.getStatus());
		query.setParameter(32, pipe.getSonuc());
		query.setParameter(33, pipe.getLotNo());
		query.setParameter(34, false);
		query.setParameter(35, pipe.getLocation());
		query.setParameter(36, pipe.getProductionDate());
		query.setParameter(37, pipe.getBandEki());
		query.setParameter(38, false);
		query.setParameter(39, false);
		query.setParameter(40, false);
		query.setParameter(41, pipe.getSalesItem().getItemId());
		query.setParameter(42, pipe.getEklemeZamani());
		query.setParameter(43, pipe.getEkleyenKullanici());
		// query.setParameter(44, null);
		// query.setParameter(45, null);
		query.executeUpdate();
		// manager.merge(pipe);
		manager.getTransaction().commit();
		manager.close();
	}

	public void insertRuloPipeLink(Pipe pipe, Rulo rulo) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		int num = 0;
		Query index = manager
				.createNativeQuery("select max(link_id) from rulo_pipe_link");
		List<Integer> getMaxIndex = index.getResultList();

		if (getMaxIndex.get(0) == null) {
			getMaxIndex.add(0, 0);
		} else
			num = getMaxIndex.get(0).intValue();

		Query query = manager
				.createNativeQuery("INSERT INTO rulo_pipe_link VALUES(?,?,?,?)");
		query.setParameter(1, num + 1);
		query.setParameter(2, rulo.getRuloId());
		query.setParameter(3, rulo.getRuloYil());
		query.setParameter(4, pipe.getPipeId());
		query.executeUpdate();
		manager.getTransaction().commit();
		manager.close();
	}

	public void testYapilanBoruEkle(Integer pipeId, Integer type, boolean sonuc) {

		EntityManager entityManager = Persistence.createEntityManagerFactory(
				"emekboru").createEntityManager();
		EntityTransaction transaction = entityManager.getTransaction();

		Pipe pipe = new Pipe();
		Query itemIndexQuery = entityManager
				.createNamedQuery("Pipe.findByPipeId");
		itemIndexQuery.setParameter("prmPipeId", pipeId);
		List<Pipe> getList = itemIndexQuery.getResultList();
		pipe = getList.get(0);

		if (type == 1) {// 1=kaplama
			pipe.setKaplamaTestSonuc(sonuc);
		} else if (type == 2) {// 2=tahribatlı
			pipe.setTahribatliTestSonuc(sonuc);
		} else if (type == 3) {// 3=tahribatsız
			pipe.setTahribatsizTestSonuc(sonuc);
		}

		try {
			transaction.begin();
			entityManager.merge(pipe);
			entityManager.flush();
			entityManager.getTransaction().commit();
			entityManager.clear();
			entityManager.close();
		} catch (Exception e) {
			Logger.getLogger(getClass()).error(
					"-HATA-PipeManager.testYapilanBoruEkle ", e);
			entityManager.flush();
			transaction.rollback();
			entityManager.clear();
			entityManager.close();
		}
	}

	public void updatePipe(Pipe pipe) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query query = manager
				.createNativeQuery("UPDATE pipe SET pipe_barkod_no='"
						+ pipe.getPipeBarkodNo() + "' where pipe_id="
						+ pipe.getPipeId());
		query.executeUpdate();
		manager.getTransaction().commit();
		manager.close();
	}

	public List<Pipe> findYesterdaysProduction() {

		List<Pipe> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("Pipe.findYesterdaysProduction")
					.setParameter("prmDate", UtilInsCore.getTarihZaman())
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	public List<Pipe> findReleaseCountByItemId(int itemId) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query itemIndexQuery = manager
				.createNamedQuery("Pipe.findReleaseCountByItemId");
		itemIndexQuery.setParameter("prmItemId", itemId);
		List<Pipe> getList = itemIndexQuery.getResultList();
		return getList;
	}

	public List<Pipe> findReleasePipesByReleaseTarihi(Date startDate,
			Date endDate) {

		String prmDate2;
		String prmDate1 = new SimpleDateFormat("yyyy/MM/dd HH:00:00")
				.format(startDate);
		if (endDate == null) {
			prmDate2 = prmDate1;

		} else {
			prmDate2 = new SimpleDateFormat("yyyy/MM/dd HH:00:00")
					.format(endDate);

		}

		String QUERY = "select p from Pipe p where p.releaseNoteTarihi='"
				+ prmDate1 + "' or p.releaseNoteTarihi='" + prmDate2
				+ "' order by p.pipeIndex ASC";

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query itemIndexQuery = manager.createQuery(QUERY);
		List<Pipe> getList = itemIndexQuery.getResultList();
		return getList;
	}

	public List<Pipe> findUcBirCountByItemId(int itemId) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query itemIndexQuery = manager
				.createNamedQuery("Pipe.findUcBirCountByItemId");
		itemIndexQuery.setParameter("prmItemId", itemId);
		List<Pipe> getList = itemIndexQuery.getResultList();
		return getList;
	}

	public List<Pipe> findUcBirPipesByUcBirTarihi(Date startDate, Date endDate) {

		String prmDate2;
		String prmDate1 = new SimpleDateFormat("yyyy/MM/dd HH:00:00")
				.format(startDate);
		if (endDate == null) {
			prmDate2 = prmDate1;

		} else {
			prmDate2 = new SimpleDateFormat("yyyy/MM/dd HH:00:00")
					.format(endDate);

		}

		String QUERY = "select p from Pipe p where p.ucBirTarihi='" + prmDate1
				+ "' or p.ucBirTarihi='" + prmDate2
				+ "' order by p.pipeIndex ASC";

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query itemIndexQuery = manager.createQuery(QUERY);
		List<Pipe> getList = itemIndexQuery.getResultList();
		return getList;
	}

	public List<Pipe> findByItemIdMachineId(int prmItemId, int prmMachineId) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query itemIndexQuery = manager
				.createNamedQuery("Pipe.findByItemIdMachineId");
		itemIndexQuery.setParameter("prmItemId", prmItemId).setParameter(
				"prmMachineId", prmMachineId);
		List<Pipe> getList = itemIndexQuery.getResultList();
		return getList;
	}

	public List<Pipe> findByItemIdMachineIdLast100(int prmItemId,
			int prmMachineId) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query itemIndexQuery = manager
				.createNamedQuery("Pipe.findByItemIdMachineId");
		itemIndexQuery.setParameter("prmItemId", prmItemId).setParameter(
				"prmMachineId", prmMachineId);
		itemIndexQuery.setFirstResult(0);
		itemIndexQuery.setMaxResults(150);
		List<Pipe> getList = itemIndexQuery.getResultList();
		return getList;
	}

	public List<Pipe> findByItemIdIndexId(int prmItemId, int prmIndexId) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query itemIndexQuery = manager
				.createNamedQuery("Pipe.findByItemIdIndexId");
		itemIndexQuery.setParameter("prmItemId", prmItemId).setParameter(
				"prmIndexId", prmIndexId);
		List<Pipe> getList = itemIndexQuery.getResultList();
		return getList;
	}

	public static List<Pipe> getByDateVardiya(String prmDateA, String prmDateB,
			Integer prmVardiya) {

		List<Pipe> result = null;

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		try {
			String query = new String();
			query = "SELECT r FROM Pipe r WHERE 1=1";

			if (prmVardiya == 1) {
				query = query + " AND r.eklemeZamani BETWEEN '" + prmDateA
						+ " 08:00:00' AND '" + prmDateA + " 19:59:59'";
			} else if (prmVardiya == 0) {
				query = query + " AND r.eklemeZamani BETWEEN '" + prmDateA
						+ " 20:00:00' AND '" + prmDateB + " 07:59:59'";
			}

			query = query + " ORDER BY r.eklemeZamani ";

			System.out.println("final query: " + query);

			Query findQuery = manager.createQuery(query);
			result = findQuery.getResultList();
			manager.getTransaction().commit();
			manager.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (result.size() == 0)
			return null;
		return result;
	}

	public List<Pipe> findRangeFilter(int start, int pagesize,
			ArrayList<String> alanlar, ArrayList<String> veriler,
			String sortField, SortOrder sortOrder, int prmItemId) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		String hsql = "select r from Pipe r where r.salesItem.itemId=:prmItemId ";
		if (alanlar != null && alanlar.size() > 0) {
			for (int i = 0; i < alanlar.size(); i++) {
				if (alanlar.get(i) != null && !alanlar.get(i).equals(""))
					hsql += "and r." + alanlar.get(i) + " like '%"
							+ veriler.get(i) + "%' ";
			}
		}
		String siralama;
		if (sortField != null && !sortField.equals("")) {
			if (sortOrder.ordinal() == 1) {
				siralama = " desc";
			} else {
				siralama = " asc";
			}
			hsql += "order by r." + sortField + siralama;
		} else
			hsql += "order by r.pipeId desc";

		Query query = manager.createQuery(hsql);
		query.setMaxResults(pagesize);
		query.setFirstResult(start);
		query.setParameter("prmItemId", prmItemId);
		List<Pipe> list = query.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return list;
	}

	public int countTotalRecord(int prmItemId) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query query = manager.createNamedQuery("Pipe.Total");
		query.setParameter("prmItemId", prmItemId);
		Number result = (Number) query.getSingleResult();
		int rr = result.intValue();
		manager.getTransaction().commit();
		manager.close();
		return rr;
	}

}
