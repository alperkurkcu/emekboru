/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.KaplamaKaliteTakipFormu;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KaplamaKaliteTakipFormuManager extends
		CommonQueries<KaplamaKaliteTakipFormu> {

	@SuppressWarnings("unchecked")
	public static List<KaplamaKaliteTakipFormu> getAllKaplamaKaliteTakipFormu(
			Integer pipeId) {

		List<KaplamaKaliteTakipFormu> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("KaplamaKaliteTakipFormu.findAll")
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
