package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaSertlikSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class TestKaplamaSertlikSonucManager extends
		CommonQueries<TestKaplamaSertlikSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaSertlikSonuc> getAllKaplamaSertlikSonucTest(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaSertlikSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestKaplamaSertlikSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
