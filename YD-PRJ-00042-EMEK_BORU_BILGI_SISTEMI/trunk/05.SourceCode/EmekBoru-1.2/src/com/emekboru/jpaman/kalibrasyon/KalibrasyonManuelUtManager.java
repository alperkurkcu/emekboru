/**
 * 
 */
package com.emekboru.jpaman.kalibrasyon;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kalibrasyon.KalibrasyonManuelUt;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KalibrasyonManuelUtManager extends
		CommonQueries<KalibrasyonManuelUt> {

	@SuppressWarnings("unchecked")
	public static List<KalibrasyonManuelUt> getAllKalibrasyon() {

		List<KalibrasyonManuelUt> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("KalibrasyonManuelUt.findAll")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
