package com.emekboru.jpaman;

public class JpqlComparativeClauses {

	//FOR NAMED QUERIES
	public static final String NQ_GREATER_THEN = ">:";
	public static final String NQ_LESS_THEN = "<:";
	public static final String NQ_NOT_EQUAL = "<>:";
	public static final String NQ_GREATER_THEN_EQUAL = ">=:";
	public static final String NQ_SMALLER_THEN_EQUAL = "<=:";
	public static final String NQ_EQUAL = "=:";

	//FOR NORMAL QUERIES
	public static final String GREATER_THEN = ">";
	public static final String LESS_THEN = "<";
	public static final String NOT_EQUAL = "<>";
	public static final String EQUAL = "=";
	public static final String GREATER_THEN_EQUAL = ">=";
	public static final String SMALLER_THEN_EQUAL = ">=";
}
