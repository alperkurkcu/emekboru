/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaSonrasiYuzeyKontroluSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaSonrasiYuzeyKontroluSonucManager extends
		CommonQueries<TestKaplamaSonrasiYuzeyKontroluSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaSonrasiYuzeyKontroluSonuc> getAllTestKaplamaSonrasiYuzeyKontroluSonuc(
			int globalId, Integer pipeId) {

		List<TestKaplamaSonrasiYuzeyKontroluSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestKaplamaSonrasiYuzeyKontroluSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
