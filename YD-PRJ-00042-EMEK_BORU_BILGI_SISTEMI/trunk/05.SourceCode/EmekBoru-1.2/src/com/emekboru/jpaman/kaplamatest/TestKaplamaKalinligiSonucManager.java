package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaKalinligiSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class TestKaplamaKalinligiSonucManager extends
		CommonQueries<TestKaplamaKalinligiSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaKalinligiSonuc> getAllKaplamaKalinligiSonucTest(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaKalinligiSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestKaplamaKalinligiSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaKalinligiSonuc> findByItemIdSonuc(
			Integer prmItemId) {

		List<TestKaplamaKalinligiSonuc> result = null;
		EntityManager em = Factory.getInstance().createEntityManager();
		try {
			String query = "SELECT r FROM TestKaplamaKalinligiSonuc r WHERE r.pipe.pipeId in (select p.pipeId from Pipe p where p.salesItem.itemId=:prmItemId) order by r.pipe.pipeId ASC";
			result = em.createQuery(query).setParameter("prmItemId", prmItemId)
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
