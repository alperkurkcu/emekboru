/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaThermalAgeingTestSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaThermalAgeingTestSonucManager extends
		CommonQueries<TestKaplamaThermalAgeingTestSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaThermalAgeingTestSonuc> getAllThermalAgeingTestSonuc(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaThermalAgeingTestSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestKaplamaThermalAgeingTestSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
