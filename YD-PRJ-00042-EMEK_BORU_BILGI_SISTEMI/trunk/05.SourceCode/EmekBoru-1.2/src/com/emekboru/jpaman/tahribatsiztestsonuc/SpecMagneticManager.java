/**
 * 
 */
package com.emekboru.jpaman.tahribatsiztestsonuc;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.tahribatsiztestsonuc.SpecMagnetic;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class SpecMagneticManager extends CommonQueries<SpecMagnetic> {

	@SuppressWarnings("unchecked")
	public static List<SpecMagnetic> getAllSpecMagnetic(Integer pipeId) {

		List<SpecMagnetic> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("SpecMagnetic.findAll")
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
