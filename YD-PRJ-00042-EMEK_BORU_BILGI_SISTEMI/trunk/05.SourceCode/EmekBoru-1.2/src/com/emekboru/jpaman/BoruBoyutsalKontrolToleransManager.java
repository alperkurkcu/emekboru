package com.emekboru.jpaman;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.BoruBoyutsalKontrolTolerans;

public class BoruBoyutsalKontrolToleransManager extends
		CommonQueries<BoruBoyutsalKontrolTolerans> {

	@SuppressWarnings("unchecked")
	public List<BoruBoyutsalKontrolTolerans> seciliBoyutsalKontrol(
			Integer salesItemId) {

		List<BoruBoyutsalKontrolTolerans> result = new ArrayList<BoruBoyutsalKontrolTolerans>();

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query findQuerie = manager
				.createNamedQuery("BoruBoyutsalKontrolTolerans.seciliBoyutsalKontrol");
		findQuerie.setParameter("prmItemId", salesItemId);
		result = findQuerie.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return result;

	}
}
