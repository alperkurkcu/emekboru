package com.emekboru.jpaman;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.Customer;


public class CustomerManager extends CommonQueries<Customer>{

	@SuppressWarnings("unchecked")
	public List<Customer> notHandled(Integer empId){
		
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query query = manager.createNamedQuery("Customer.notHandled");
		query.setParameter("employeeId", empId);
		List<Customer> companyList = (List<Customer>)query.getResultList();
		manager.close();
		return companyList;
		
	}
	
	public Customer getCustomerByCustormerId(int customerId) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Customer customer = manager.find(Customer.class, customerId);
		manager.getTransaction().commit();
		manager.close();
		return customer;
	}
	
}
