/**
 * 
 */
package com.emekboru.jpaman.kaplamamakinesi;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiPolietilen;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KaplamaMakinesiPolietilenManager extends
		CommonQueries<KaplamaMakinesiPolietilen> {

	@SuppressWarnings("unchecked")
	public static List<KaplamaMakinesiPolietilen> getAllKaplamaMakinesiPolietilen(
			Integer prmPipeId) {

		List<KaplamaMakinesiPolietilen> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"KaplamaMakinesiPolietilen.findAllByPipeId")
					.setParameter("prmPipeId", prmPipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
