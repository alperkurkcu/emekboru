package com.emekboru.jpaman;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.AgirlikTestsSpec;

public class AgirlikTestsSpecManager extends CommonQueries<AgirlikTestsSpec> {

	// itemId sipari� kalemidir.
	@SuppressWarnings("unchecked")
	public Integer kontrol(Integer globalId, Integer itemId) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query findQuerie = manager.createNamedQuery("ATS.silmeKontrolu");
		findQuerie.setParameter("prmGlobalId", globalId);
		findQuerie.setParameter("prmItemId", itemId);
		List<AgirlikTestsSpec> resultList = findQuerie.getResultList();
		Integer count = resultList.size();
		if (count.equals(null)) {
			count = 0;
		}
		manager.getTransaction().commit();
		manager.close();
		return count;
	}

	@SuppressWarnings("unchecked")
	public List<AgirlikTestsSpec> seciliAgirlikTestTanim(Integer globalId,
			Integer prmItemId) {

		List<AgirlikTestsSpec> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("ATS.seciliAgirlikTestTanim")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;

	}

	@SuppressWarnings("unchecked")
	public List<AgirlikTestsSpec> findByItemId(Integer prmItemId) {

		List<AgirlikTestsSpec> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("ATS.findByItemId")
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;

	}

}
