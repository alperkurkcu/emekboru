/**
 * 
 */
package com.emekboru.jpaman.kalibrasyon;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kalibrasyon.KalibrasyonPenetrant;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KalibrasyonPenetrantManager extends
		CommonQueries<KalibrasyonPenetrant> {

	@SuppressWarnings("unchecked")
	public static List<KalibrasyonPenetrant> getAllKalibrasyon() {

		List<KalibrasyonPenetrant> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("KalibrasyonPenetrant.findAll")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
