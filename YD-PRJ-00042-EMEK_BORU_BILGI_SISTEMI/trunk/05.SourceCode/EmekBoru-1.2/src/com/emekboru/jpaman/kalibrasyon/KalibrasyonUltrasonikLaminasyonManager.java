/**
 * 
 */
package com.emekboru.jpaman.kalibrasyon;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kalibrasyon.KalibrasyonUltrasonikLaminasyon;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KalibrasyonUltrasonikLaminasyonManager extends
		CommonQueries<KalibrasyonUltrasonikLaminasyon> {

	@SuppressWarnings("unchecked")
	public static List<KalibrasyonUltrasonikLaminasyon> getAllKalibrasyon(
			Integer prmOnlineOffline) {

		List<KalibrasyonUltrasonikLaminasyon> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("KalibrasyonUltrasonikLaminasyon.findAll")
					.setParameter("prmOnlineOffline", prmOnlineOffline)
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
