/**
 * 
 */
package com.emekboru.jpaman.employee;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.faces.bean.ManagedProperty;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.employee.EmployeeBankAccount;
import com.emekboru.jpa.employee.EmployeeDiscipline;
import com.emekboru.jpaman.CommonQueries;

/**
 * @author kursat
 * 
 */
public class EmployeeDisciplineManager extends
		CommonQueries<EmployeeDiscipline> {

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userCredentialsBean;

	public EmployeeDisciplineManager() {
		super();
		userCredentialsBean = new UserCredentialsBean();
	}

	public List<EmployeeDiscipline> findAllSortedByDate(
			Class<EmployeeBankAccount> clazz) {

		List<EmployeeDiscipline> listOfEmployeeDisciplines = this
				.findAll(EmployeeDiscipline.class);

		Collections.sort(listOfEmployeeDisciplines,
				new Comparator<EmployeeDiscipline>() {

					@Override
					public int compare(EmployeeDiscipline employeeDiscipline1,
							EmployeeDiscipline employeeDiscipline2) {
						return employeeDiscipline1.getDate().compareTo(employeeDiscipline2.getDate());
					}
				});

		return listOfEmployeeDisciplines;
	}
}
