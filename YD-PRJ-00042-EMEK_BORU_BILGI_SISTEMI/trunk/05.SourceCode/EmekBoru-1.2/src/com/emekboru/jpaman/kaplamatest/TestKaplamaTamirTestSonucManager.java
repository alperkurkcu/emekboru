package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaTamirTestSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class TestKaplamaTamirTestSonucManager extends
		CommonQueries<TestKaplamaTamirTestSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaTamirTestSonuc> getAllTestKaplamaTamirTestSonuc(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaTamirTestSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestKaplamaTamirTestSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
