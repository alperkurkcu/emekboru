/**
 * 
 */
package com.emekboru.jpaman.coatingmaterials;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.coatingmaterials.KaplamaMalzemeKullanmaBeton;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KaplamaMalzemeKullanmaBetonManager extends
		CommonQueries<KaplamaMalzemeKullanmaBeton> {

	@SuppressWarnings("unchecked")
	public static List<KaplamaMalzemeKullanmaBeton> getAllKaplamaMalzemeKullanmaBeton() {

		List<KaplamaMalzemeKullanmaBeton> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("KaplamaMalzemeKullanmaBeton.findAll")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
