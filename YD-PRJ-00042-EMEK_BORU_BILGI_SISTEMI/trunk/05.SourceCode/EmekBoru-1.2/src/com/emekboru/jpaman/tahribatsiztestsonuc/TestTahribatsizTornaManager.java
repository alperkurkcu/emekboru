/**
 * 
 */
package com.emekboru.jpaman.tahribatsiztestsonuc;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizTorna;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestTahribatsizTornaManager extends
		CommonQueries<TestTahribatsizTorna> {

	@SuppressWarnings("unchecked")
	public static List<TestTahribatsizTorna> getAllTestTahribatsizTorna(
			Integer pipeId) {

		List<TestTahribatsizTorna> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestTahribatsizTorna.findAll")
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<TestTahribatsizTorna> getBySalesItem(
			SalesItem prmSalesItem) {

		List<TestTahribatsizTorna> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestTahribatsizTorna.getBySalesItem")
					.setParameter("prmSalesItem", prmSalesItem.getItemId())
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<TestTahribatsizTorna> getByPipeDateVardiya(
			Integer prmPipeId, String prmDateA, String prmDateB,
			Integer prmVardiya) {

		List<TestTahribatsizTorna> result = null;

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		try {
			String query = new String();
			query = "SELECT r FROM TestTahribatsizTorna r WHERE 1=1 and r.pipe.pipeId=:prmPipeId";

			if (prmVardiya == 1) {
				query = query + " AND r.eklemeZamani BETWEEN '" + prmDateA
						+ " 08:00:00' AND '" + prmDateA + " 19:59:59'";
			} else if (prmVardiya == 0) {
				query = query + " AND r.eklemeZamani BETWEEN '" + prmDateA
						+ " 20:00:00' AND '" + prmDateB + " 07:59:59'";
			}

			query = query + " ORDER BY r.eklemeZamani ";

			System.out.println("final query: " + query);

			Query findQuery = manager.createQuery(query).setParameter(
					"prmPipeId", prmPipeId);
			result = findQuery.getResultList();
			manager.getTransaction().commit();
			manager.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (result.size() == 0)
			return null;
		return result;
	}
}
