package com.emekboru.jpaman.rulo;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.rulo.RuloTestKimyasalUretici;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class RuloTestKimyasalUreticiManager extends
		CommonQueries<RuloTestKimyasalUretici> {

	@SuppressWarnings("unchecked")
	public static List<RuloTestKimyasalUretici> getByDokumNo(String prmDokumNo) {

		List<RuloTestKimyasalUretici> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("RuloTestKimyasalUretici.getByDokumNo")
					.setParameter("prmDokumNo", prmDokumNo).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
