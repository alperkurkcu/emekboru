package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaKumlamaOncesiOnIsitmaSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class TestKaplamaKumlamaOncesiOnIsitmaSonucManager extends
		CommonQueries<TestKaplamaKumlamaOncesiOnIsitmaSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaKumlamaOncesiOnIsitmaSonuc> getAllTestKaplamaKumlamaOncesiOnIsitmaSonuc(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaKumlamaOncesiOnIsitmaSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestKaplamaKumlamaOncesiOnIsitmaSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
