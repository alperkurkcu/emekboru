package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class TestKaplamaKumlanmisBoruYuzeyTemizligiSonucManager extends
		CommonQueries<TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc> getAllTestKaplamaKumlanmisBoruYuzeyTemizligiSonuc(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc> findByItemIdSonuc(
			Integer prmItemId) {

		List<TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc> result = null;
		EntityManager em = Factory.getInstance().createEntityManager();
		try {
			String query = "SELECT r FROM TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc r WHERE r.pipe.pipeId in (select p.pipeId from Pipe p where p.salesItem.itemId=:prmItemId) order by r.pipe.pipeId ASC";
			result = em.createQuery(query).setParameter("prmItemId", prmItemId)
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
