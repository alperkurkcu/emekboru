/**
 * 
 */
package com.emekboru.jpaman.istakipformu;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.istakipformu.IsTakipFormuBetonKaplamaDevamIslemler;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class IsTakipFormuBetonKaplamaDevamIslemlerManager extends
		CommonQueries<IsTakipFormuBetonKaplamaDevamIslemler> {

	@SuppressWarnings("unchecked")
	public static List<IsTakipFormuBetonKaplamaDevamIslemler> getAllIsTakipFormuBetonKaplamaDevamIslemler(
			Integer itemId) {

		List<IsTakipFormuBetonKaplamaDevamIslemler> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"IsTakipFormuBetonKaplamaDevamIslemler.findAll")
					.setParameter("prmItemId", itemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
