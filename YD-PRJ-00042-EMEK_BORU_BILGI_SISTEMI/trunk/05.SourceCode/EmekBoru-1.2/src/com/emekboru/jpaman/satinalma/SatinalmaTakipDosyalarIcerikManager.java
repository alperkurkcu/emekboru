/**
 * 
 */
package com.emekboru.jpaman.satinalma;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.satinalma.SatinalmaTakipDosyalarIcerik;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class SatinalmaTakipDosyalarIcerikManager extends
		CommonQueries<SatinalmaTakipDosyalarIcerik> {

	@SuppressWarnings("unchecked")
	public List<SatinalmaTakipDosyalarIcerik> getAllSatinalmaTakipDosyalarIcerik() {

		List<SatinalmaTakipDosyalarIcerik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("SatinalmaTakipDosyalarIcerik.findAll")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaTakipDosyalarIcerik> getAllSatinalmaTakipDosyalarIcerikByMalzemeFormId(
			Integer formId) {

		List<SatinalmaTakipDosyalarIcerik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaTakipDosyalarIcerik.findAllByFormId")
					.setParameter("prmFormId", formId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaTakipDosyalarIcerik> getAllSatinalmaMalzemeHizmetTalepFormuByGorevliId(
			Integer prmUserId) {

		List<SatinalmaTakipDosyalarIcerik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaTakipDosyalarIcerik.findAllByUserId")
					.setParameter("prmUserId", prmUserId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaTakipDosyalarIcerik> findByFormIdUserId(
			Integer prmUserId, Integer prmFormId) {

		List<SatinalmaTakipDosyalarIcerik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaTakipDosyalarIcerik.findByFormIdUserId")
					.setParameter("prmUserId", prmUserId)
					.setParameter("prmFormId", prmFormId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaTakipDosyalarIcerik> findByDosyaIdFormId(
			Integer prmDosyaId, Integer prmFormId) {

		List<SatinalmaTakipDosyalarIcerik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaTakipDosyalarIcerik.findAllByDosyaIdFormId")
					.setParameter("prmFormId", prmFormId)
					.setParameter("prmDosyaId", prmDosyaId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaTakipDosyalarIcerik> getAllSatinalmaTakipDosyalarIcerikByDosyaId(
			Integer prmDosyaId) {

		List<SatinalmaTakipDosyalarIcerik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaTakipDosyalarIcerik.findAllByDosyaId")
					.setParameter("prmDosyaId", prmDosyaId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
