/**
 * 
 */
package com.emekboru.jpaman.tahribatsiztestsonuc;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizFloroskopikSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizManuelUtSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizManyetikParcacikSonuc;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
public class TestTahribatsizKoordinatManager {

	public static final Integer ManuelStation = 1;
	public static final Integer FloroscopicStation = 2;
	public static final Integer FloroscopicTamir = 1;
	public static final Integer ManuelTamir = 2;

	public void tumTablolaraKoordinatEkleme(Integer Q, Integer L, Pipe pipe,
			String type, UserCredentialsBean userBean, String kaynakIslemi,
			String kaynakLaminasyon, Integer tamirKesim) {

		TestTahribatsizManuelUtSonucManager manuelManager = new TestTahribatsizManuelUtSonucManager();
		TestTahribatsizFloroskopikSonucManager floroskopikManager = new TestTahribatsizFloroskopikSonucManager();
		TestTahribatsizManyetikParcacikSonucManager manyetikManager = new TestTahribatsizManyetikParcacikSonucManager();

		if (type == "o") {

			TestTahribatsizManuelUtSonuc manuel = new TestTahribatsizManuelUtSonuc();
			try {
				manuel.setKoordinatQ(Q);
				manuel.setKoordinatL(L);
				manuel.setEklemeZamani(new java.sql.Timestamp(
						new java.util.Date().getTime()));
				manuel.setEkleyenEmployee(userBean.getUser());
				manuel.setPipe(pipe);
				manuel.setKaynakLaminasyon(Integer.parseInt(kaynakLaminasyon));
				manuel.setKaynakIslemi(kaynakIslemi);
				manuelManager.enterNew(manuel);
			} catch (Exception e) {
				e.printStackTrace();
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"HATA, MANUEL KOORDİNATI EKLENEMEDİ!", null));
				return;
			}
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"MANUEL KOORDİNATI OLARAK DA EKLENDİ!", null));
		} else if (type.equals("m")) {

			FacesContext context = FacesContext.getCurrentInstance();

			if (tamirKesim == 2) {// tamir ise
				TestTahribatsizFloroskopikSonuc floroskopik = new TestTahribatsizFloroskopikSonuc();

				floroskopik.setKoordinatQ(Q);
				floroskopik.setKoordinatL(L);
				floroskopik.setEklemeZamani(new java.sql.Timestamp(
						new java.util.Date().getTime()));
				floroskopik.setUser(userBean.getUser());
				floroskopik.setPipe(pipe);
				floroskopik.setKaynakIslemi(kaynakIslemi);
				floroskopik.setTamirKesim(1);

				try {
					floroskopikManager.enterNew(floroskopik);
				} catch (Exception e) {
					e.printStackTrace();
					context.addMessage(null, new FacesMessage(
							FacesMessage.SEVERITY_FATAL,
							"HATA, FLOROSKOPİ TAMİR KOORDİNATI EKLENEMEDİ!",
							null));
					return;
				}
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"FLOROSKOPİ TAMİR KOORDİNATI OLARAK DA EKLENDİ!", null));
			} else if ((kaynakIslemi.equals("2") || kaynakIslemi.equals("4"))) {
				TestTahribatsizFloroskopikSonuc floroskopik = new TestTahribatsizFloroskopikSonuc();

				floroskopik.setKoordinatQ(Q);
				floroskopik.setKoordinatL(L);
				floroskopik.setEklemeZamani(new java.sql.Timestamp(
						new java.util.Date().getTime()));
				floroskopik.setUser(userBean.getUser());
				floroskopik.setPipe(pipe);
				floroskopik.setKaynakIslemi(kaynakIslemi);

				try {
					floroskopikManager.enterNew(floroskopik);
				} catch (Exception e) {
					e.printStackTrace();
					context.addMessage(null, new FacesMessage(
							FacesMessage.SEVERITY_FATAL,
							"HATA, FLOROSKOPİ SMAW KOORDİNATI EKLENEMEDİ!",
							null));
					return;
				}

				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"FLOROSKOPİ SMAW KOORDİNATI OLARAK DA EKLENDİ!", null));
			}

			TestTahribatsizManyetikParcacikSonuc manyetik = new TestTahribatsizManyetikParcacikSonuc();

			manyetik.setKoordinatQ(Q);
			manyetik.setKoordinatL(L);
			manyetik.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			manyetik.setEkleyenEmployee(userBean.getUser());
			manyetik.setPipe(pipe);
			manyetik.setHataTipi(3);

			try {
				manyetikManager.enterNew(manyetik);
			} catch (Exception e) {
				e.printStackTrace();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"HATA, MANYETİK KOORDİNATI EKLENEMEDİ!", null));
				return;
			}
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"MANYETİK KOORDİNATI OLARAK DA EKLENDİ!", null));
		} else if (type.equals("f") && tamirKesim == 1) {

			TestTahribatsizManuelUtSonuc manuel = new TestTahribatsizManuelUtSonuc();
			try {
				manuel.setKoordinatQ(Q);
				manuel.setKoordinatL(L);
				manuel.setEklemeZamani(new java.sql.Timestamp(
						new java.util.Date().getTime()));
				manuel.setEkleyenEmployee(userBean.getUser());
				manuel.setPipe(pipe);
				manuel.setKaynakIslemi(kaynakIslemi);
				manuel.setDegerlendirme(2);
				manuelManager.enterNew(manuel);
			} catch (Exception e) {
				e.printStackTrace();
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"HATA, MANUEL TAMİR KOORDİNATI EKLENEMEDİ!", null));
				return;
			}
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"MANUEL TAMİR KOORDİNATI OLARAK DA EKLENDİ!", null));
		} else if (type.equals("f")
				&& (kaynakIslemi.equals("2") || kaynakIslemi.equals("4"))) {

			TestTahribatsizManuelUtSonuc manuel = new TestTahribatsizManuelUtSonuc();
			try {
				manuel.setKoordinatQ(Q);
				manuel.setKoordinatL(L);
				manuel.setEklemeZamani(new java.sql.Timestamp(
						new java.util.Date().getTime()));
				manuel.setEkleyenEmployee(userBean.getUser());
				manuel.setPipe(pipe);
				manuel.setKaynakIslemi(kaynakIslemi);
				manuelManager.enterNew(manuel);
			} catch (Exception e) {
				e.printStackTrace();
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"HATA, MANUEL SMAW KOORDİNATI EKLENEMEDİ!", null));
				return;
			}
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"MANUEL SMAW KOORDİNATI OLARAK DA EKLENDİ!", null));
		}
	}

	public void tumTablolardaKoordinatGuncelleme(Integer ilkQ, Integer ilkL,
			Integer sonQ, Integer sonL, Pipe pipe, UserCredentialsBean userBean) {

		TestTahribatsizManuelUtSonuc manuel = new TestTahribatsizManuelUtSonuc();
		// TestTahribatsizFloroskopikSonuc floroskopik = new
		// TestTahribatsizFloroskopikSonuc();

		// manuel bulma
		try {
			TestTahribatsizManuelUtSonucManager manuelFindManager = new TestTahribatsizManuelUtSonucManager();
			manuel = manuelFindManager.findByKoordinatsPipeId(ilkQ, ilkL,
					pipe.getPipeId()).get(0);
		} catch (Exception e) {

			System.out
					.println(" testTahribatsizKoordinatManager.tumTablolardaKoordinatGuncelleme-HATA manuelUT");
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"MANUEL UT GÜNCELLEME HATASI, TEKRAR DENEYİNİZ!"));
			return;
		}

		manuel.setKoordinatQ(sonQ);
		manuel.setKoordinatL(sonL);
		manuel.setGuncelleyenEmployee(userBean.getUser());
		manuel.setGuncellemeZamani(UtilInsCore.getTarihZaman());

		TestTahribatsizManuelUtSonucManager manuelUpdateManager = new TestTahribatsizManuelUtSonucManager();
		manuelUpdateManager.updateEntity(manuel);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"MANUEL UT KOORDİNATI GÜNCELLENMİŞTİR!"));

		// // floroskopik bulma
		// try {
		// TestTahribatsizFloroskopikSonucManager floroskopikFindManager = new
		// TestTahribatsizFloroskopikSonucManager();
		// floroskopik = floroskopikFindManager.findByKoordinatsPipeId(ilkQ,
		// ilkL, pipe.getPipeId()).get(0);
		// } catch (Exception e) {
		//
		// System.out
		// .println(" testTahribatsizKoordinatManager.tumTablolardaKoordinatGuncelleme-HATA floroskopikUT");
		// context.addMessage(null, new FacesMessage(
		// "FLOROSKOPİK UT GÜNCELLEME HATASI, TEKRAR DENEYİNİZ!"));
		// return;
		// }
		//
		// floroskopik.setKoordinatQ(sonQ);
		// floroskopik.setKoordinatL(sonL);
		// floroskopik.setGuncelleyenEmployee(userBean.getUser());
		// floroskopik.setGuncellemeZamani(UtilInsCore.getTarihZaman());
		//
		// TestTahribatsizFloroskopikSonucManager floroskopikUpdateManager = new
		// TestTahribatsizFloroskopikSonucManager();
		// floroskopikUpdateManager.updateEntity(floroskopik);
		// context.addMessage(null, new FacesMessage(
		// "FLOROSKOPİK UT KOORDİNATI GÜNCELLENMİŞTİR!"));
	}

	public boolean coordinateCheck(Integer coordinateQ, Integer coordinateL,
			Integer stationType, Integer prmPipeId) {
		Integer count = 0;
		if (stationType == ManuelStation) {
			TestTahribatsizManuelUtSonucManager manuelManager = new TestTahribatsizManuelUtSonucManager();
			count = manuelManager.findByKoordinatsPipeId(coordinateQ,
					coordinateL, prmPipeId).size();
			if (count > 0)
				return true;

		} else if (stationType == FloroscopicStation) {
			TestTahribatsizFloroskopikSonucManager floroManager = new TestTahribatsizFloroskopikSonucManager();
			count = floroManager.findByKoordinatsPipeId(coordinateQ,
					coordinateL, prmPipeId).size();
			if (count > 0)
				return true;
		}
		return false;
	}

	public void tablolardaDurumGuncelleme(Integer coordinateQ,
			Integer coordinateL, Integer prmPipeId,
			UserCredentialsBean userBean, Integer prmDegerlendirme,
			Integer stationType) {

		if (stationType == ManuelStation) {
			TestTahribatsizManuelUtSonucManager manuelManager = new TestTahribatsizManuelUtSonucManager();
			TestTahribatsizManuelUtSonuc manuel = new TestTahribatsizManuelUtSonuc();
			manuel = manuelManager.findByKoordinatsPipeId(coordinateQ,
					coordinateL, prmPipeId).get(0);
			manuel.setDegerlendirme(ManuelTamir);
			manuel.setGuncelleyenEmployee(userBean.getUser());
			manuel.setGuncellemeZamani(UtilInsCore.getTarihZaman());
			manuelManager.updateEntity(manuel);
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"MANUEL UT KOORDİNATI GÜNCELLENMİŞTİR!"));

		} else if (stationType == FloroscopicStation) {
			TestTahribatsizFloroskopikSonucManager floroManager = new TestTahribatsizFloroskopikSonucManager();
			TestTahribatsizFloroskopikSonuc floros = new TestTahribatsizFloroskopikSonuc();
			floros = floroManager.findByKoordinatsPipeId(coordinateQ,
					coordinateL, prmPipeId).get(0);
			floros.setTamirKesim(FloroscopicTamir);
			floros.setGuncelleyenEmployee(userBean.getUser());
			floros.setGuncellemeZamani(UtilInsCore.getTarihZaman());
			floroManager.updateEntity(floros);
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"FLOROSKOPİK UT KOORDİNATI GÜNCELLENMİŞTİR!"));
		}

	}
}
