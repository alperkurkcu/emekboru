/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaGozKontrolSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author YD-PC
 * 
 */
public class TestKaplamaGozKontrolSonucManager extends
		CommonQueries<TestKaplamaGozKontrolSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaGozKontrolSonuc> getAllTestKaplamaGozKontrolSonuc(
			Integer pipeId, Integer globalId) {

		List<TestKaplamaGozKontrolSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestKaplamaGozKontrolSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
