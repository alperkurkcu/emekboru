/**
 * 
 */
package com.emekboru.jpaman.coatingmaterials;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.coatingmaterials.KaplamaMalzemeKullanmaPe;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KaplamaMalzemeKullanmaPeManager extends
		CommonQueries<KaplamaMalzemeKullanmaPe> {

	@SuppressWarnings("unchecked")
	public static List<KaplamaMalzemeKullanmaPe> getAllKaplamaMalzemeKullanmaPe() {

		List<KaplamaMalzemeKullanmaPe> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("KaplamaMalzemeKullanmaPe.findAll")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<KaplamaMalzemeKullanmaPe> findByDateVardiya(String prmTarih1,
			Integer prmVardiya) {

		List<KaplamaMalzemeKullanmaPe> result = null;

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		try {
			String query = new String();
			query = "SELECT r FROM KaplamaMalzemeKullanmaPe r where 1=1 AND r.harcanmaTarihi BETWEEN '"
					+ prmTarih1
					+ " 00:00' AND '"
					+ prmTarih1
					+ " 23:59' AND r.vardiya ="
					+ prmVardiya
					+ " AND r.coatRawMaterial.coatMaterialType.coatMaterialTypeId=1000 order by r.eklemeZamani DESC";

			System.out.println("final query: " + query);

			Query findQuery = manager.createQuery(query);
			result = findQuery.getResultList();
			manager.getTransaction().commit();
			manager.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (result.size() == 0)
			return null;
		return result;
	}

}
