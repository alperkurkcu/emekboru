/**
 * 
 */
package com.emekboru.jpaman.istakipformu;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.istakipformu.IsTakipFormuKumlamaDevamIslemler;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class IsTakipFormuKumlamaDevamIslemlerManager extends
		CommonQueries<IsTakipFormuKumlamaDevamIslemler> {

	@SuppressWarnings("unchecked")
	public static List<IsTakipFormuKumlamaDevamIslemler> getAllIsTakipFormuKumlamaDevamIslemler(
			Integer itemId, Integer icDis) {

		List<IsTakipFormuKumlamaDevamIslemler> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"IsTakipFormuKumlamaDevamIslemler.findAll")
					.setParameter("prmItemId", itemId)
					.setParameter("prmIcDis", icDis).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
