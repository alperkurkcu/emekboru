package com.emekboru.jpaman;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class Factory {

	private static EntityManagerFactory factory;
	private static EntityManager manager = null;
	static {
		try {
			factory = Persistence.createEntityManagerFactory("emekboru");
		} catch (Throwable ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static void closeFactory() {
		factory.close();
	}

	public static EntityManagerFactory getInstance() {
		return factory;
	}

	public static synchronized EntityManager getEntityManager() {
		if (manager == null) {
			if (factory == null)
				factory = Persistence.createEntityManagerFactory("emekboru");
			manager = factory.createEntityManager();
		}
		return manager;
	}

	public static EntityTransaction getTransaction() {
		if (manager != null)
			return manager.getTransaction();
		else {
			return getEntityManager().getTransaction();
		}
	}

	public static void beginTransaction() {
		EntityTransaction transaction = getTransaction();
		if (transaction != null)
			transaction.begin();
	}

	public static void commitTransaction() {
		EntityTransaction transaction = getTransaction();
		if (transaction != null)
			transaction.commit();

	}
}
