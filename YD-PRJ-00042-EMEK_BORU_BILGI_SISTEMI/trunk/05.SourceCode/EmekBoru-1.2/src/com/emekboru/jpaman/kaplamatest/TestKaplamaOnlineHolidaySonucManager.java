/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaOnlineHolidaySonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaOnlineHolidaySonucManager extends
		CommonQueries<TestKaplamaOnlineHolidaySonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaOnlineHolidaySonuc> getAllTestKaplamaOnlineHolidaySonucTest(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaOnlineHolidaySonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("TestKaplamaOnlineHolidaySonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
