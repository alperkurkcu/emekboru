/**
 * 
 */
package com.emekboru.jpaman.machines;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.machines.MachineSalesPipeNo;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class MachineSalesPipeNoManager extends
		CommonQueries<MachineSalesPipeNo> {

	@SuppressWarnings("unchecked")
	public static List<MachineSalesPipeNo> findByMachineSalesItem(
			Integer prmMachineId, Integer prmItemId) {

		List<MachineSalesPipeNo> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"MachineSalesPipeNo.findByMachineSalesItem")
					.setParameter("prmMachineId", prmMachineId)
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<MachineSalesPipeNo> findBySalesItem(
			Integer prmMachineId, Integer prmItemId) {

		List<MachineSalesPipeNo> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("MachineSalesPipeNo.findBySalesItem")
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
