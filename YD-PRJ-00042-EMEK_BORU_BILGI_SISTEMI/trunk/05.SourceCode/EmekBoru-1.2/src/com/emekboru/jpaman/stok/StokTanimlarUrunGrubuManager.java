/**
 * 
 */
package com.emekboru.jpaman.stok;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.stok.StokTanimlarUrunGrubu;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class StokTanimlarUrunGrubuManager extends
		CommonQueries<StokTanimlarUrunGrubu> {

	@SuppressWarnings("unchecked")
	public List<StokTanimlarUrunGrubu> getAllStokTanimlarUrunGrubu() {

		List<StokTanimlarUrunGrubu> result = null;
		try {
			EntityManager em = Factory.getEntityManager();
			result = em.createNamedQuery("StokTanimlarUrunGrubu.findAll")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
