/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;
import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaBetonOranBelirlenmesiSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author YD-PC
 *
 */
public class TestKaplamaBetonOranBelirlenmesiSonucManager extends
CommonQueries<TestKaplamaBetonOranBelirlenmesiSonuc> {
	
	@SuppressWarnings("unchecked")
	public static List<TestKaplamaBetonOranBelirlenmesiSonuc> getAllTestKaplamaBetonOranBelirlenmesiSonuc(Integer globalId,
			Integer pipeId) {

		List<TestKaplamaBetonOranBelirlenmesiSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestKaplamaBetonOranBelirlenmesiSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
