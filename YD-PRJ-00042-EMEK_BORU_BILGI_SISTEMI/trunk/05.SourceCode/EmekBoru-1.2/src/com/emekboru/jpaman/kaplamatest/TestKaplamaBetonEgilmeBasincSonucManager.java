package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaBetonEgilmeBasincSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class TestKaplamaBetonEgilmeBasincSonucManager extends
		CommonQueries<TestKaplamaBetonEgilmeBasincSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaBetonEgilmeBasincSonuc> getAllTestKaplamaBetonEgilmeBasincSonuc(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaBetonEgilmeBasincSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestKaplamaBetonEgilmeBasincSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
