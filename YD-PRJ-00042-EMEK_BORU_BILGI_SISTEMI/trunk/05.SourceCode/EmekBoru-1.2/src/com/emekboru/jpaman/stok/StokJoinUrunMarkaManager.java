/**
 * 
 */
package com.emekboru.jpaman.stok;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.stok.StokJoinUrunMarka;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class StokJoinUrunMarkaManager extends CommonQueries<StokJoinUrunMarka> {

	@SuppressWarnings("unchecked")
	public List<StokJoinUrunMarka> getAllStokJoinUrunMarka(Integer urunId) {

		List<StokJoinUrunMarka> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("StokJoinUrunMarka.findAllByUrunId")
					.setParameter("prmUrunId", urunId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<StokJoinUrunMarka> getAllStokJoinUrunMarka(Integer urunId,
			Integer markaId) {

		List<StokJoinUrunMarka> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"StokJoinUrunMarka.findAllByUrunIdMarkaId")
					.setParameter("prmUrunId", urunId)
					.setParameter("prmMarkaId", markaId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
