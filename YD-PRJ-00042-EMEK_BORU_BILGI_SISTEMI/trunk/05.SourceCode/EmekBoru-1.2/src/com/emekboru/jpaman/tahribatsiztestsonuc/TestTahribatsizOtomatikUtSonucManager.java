/**
 * 
 */
package com.emekboru.jpaman.tahribatsiztestsonuc;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizOtomatikUtSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
public class TestTahribatsizOtomatikUtSonucManager extends
		CommonQueries<TestTahribatsizOtomatikUtSonuc> {

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizOtomatikUtSonuc> getAllTestTahribatsizOtomatikUtSonuc(
			Integer pipeId) {

		List<TestTahribatsizOtomatikUtSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("TestTahribatsizOtomatikUtSonuc.findAll")
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<Integer> findRuloIdByPipeId(Integer pipeId) {

		List<Integer> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			Query query = em
					.createNativeQuery("SELECT rulo_id FROM RULO_PIPE_LINK where pipe_id=?");
			query.setParameter(1, pipeId);
			result = query.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizOtomatikUtSonuc> dunUretilmisBorularIcin(
			Integer prmMachineId) {

		List<TestTahribatsizOtomatikUtSonuc> result = null;
		EntityManager em = Factory.getEntityManager();

		String tarihIlk = UtilInsCore.getYesterday().toString()
				.substring(0, 10)
				+ " 08:00:00";
		String tarihSon = UtilInsCore.getTarihZaman().toString()
				.substring(0, 10)
				+ " 07:59:59";

		Timestamp tsIlk = Timestamp.valueOf(tarihIlk);
		Timestamp tsSon = Timestamp.valueOf(tarihSon);

		try {
			String query = "SELECT o FROM TestTahribatsizOtomatikUtSonuc mel WHERE mel.pipe.pipeId in (select o.pipe.pipeId from MachinePipeLink o where o.pipe.eklemeZamani BETWEEN :prmDate1 AND :prmDate2 and o.machine.machineId=:prmMachineId) order by mel.pipe.pipeId";
			Query index = em.createQuery(query);
			index.setParameter("prmDate1", tsIlk)
					.setParameter("prmDate2", tsSon)
					.setParameter("prmMachineId", prmMachineId);
			result = index.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizOtomatikUtSonuc> tumUretilmisBorularIcin(
			Integer prmSalesItemId, Integer prmMachineId) {

		List<TestTahribatsizOtomatikUtSonuc> result = null;
		EntityManager em = Factory.getEntityManager();

		try {
			String query = "SELECT mel FROM TestTahribatsizOtomatikUtSonuc mel WHERE mel.pipe.pipeId in (select o.pipe.pipeId from MachinePipeLink o where o.machine.machineId=:prmMachineId and o.pipe.salesItem.itemId=:prmSalesItemId) order by mel.pipe.pipeId";
			Query index = em.createQuery(query);
			index.setParameter("prmSalesItemId", prmSalesItemId).setParameter(
					"prmMachineId", prmMachineId);
			result = index.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<TestTahribatsizOtomatikUtSonuc> findBetweenTwoParameters(
			String prmFirstBarcode, String prmSecondBarcode) {

		List<TestTahribatsizOtomatikUtSonuc> result = null;
		EntityManager em = Factory.getEntityManager();

		try {
			try {
				String query = "SELECT r FROM TestTahribatsizOtomatikUtSonuc r WHERE r.pipe.pipeId in (select p.pipeId from Pipe p where p.pipeBarkodNo BETWEEN :prmFirstBarcode AND :prmSecondBarcode ) order by r.pipe.pipeId ASC";
				result = em.createQuery(query)
						.setParameter("prmFirstBarcode", prmFirstBarcode)
						.setParameter("prmSecondBarcode", prmSecondBarcode)
						.getResultList();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			return result;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
