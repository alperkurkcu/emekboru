package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaProcessBozulmaSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class TestKaplamaProcessBozulmaSonucManager extends
		CommonQueries<TestKaplamaProcessBozulmaSonuc> {
	@SuppressWarnings("unchecked")
	public static List<TestKaplamaProcessBozulmaSonuc> getAllTestKaplamaProcessBozulmaSonuc(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaProcessBozulmaSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("TestKaplamaProcessBozulmaSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
