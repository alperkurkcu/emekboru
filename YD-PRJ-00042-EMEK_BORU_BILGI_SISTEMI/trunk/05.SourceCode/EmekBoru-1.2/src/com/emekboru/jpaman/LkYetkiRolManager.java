package com.emekboru.jpaman;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.LkYetkiRol;

public class LkYetkiRolManager extends CommonQueries<LkYetkiRol> {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List<LkYetkiRol> getAllYetkiRolList() {
		List result = null;
		try {
			result = Factory.getEntityManager()
					.createNamedQuery("LkYetkiRol.findAll").getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	public static void addOrUpdateLkYetkiRol(LkYetkiRol yetkiRol) {
		EntityManager em = Factory.getEntityManager();

		em.getTransaction().begin();
		if (yetkiRol.getId() != null) {
			em.merge(yetkiRol);
		} else {
			em.persist(yetkiRol);
		}
		em.getTransaction().commit();
	}

	public static LkYetkiRol getLkYetkiRolById(Integer id) {
		EntityManager em = Factory.getEntityManager();
		return em.find(LkYetkiRol.class, id);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List<LkYetkiRol> getLkYetkiRol(String ad, String aciklama) {
		EntityManager em = Factory.getEntityManager();
		List result = null;
		try {
			result = em
					.createNamedQuery("LkYetkiRol.findByAdAciklamaAndMenuId")
					.setParameter("prmAd", ad)
					.setParameter("prmAciklama", aciklama).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List<LkYetkiRol> getRoleByMenuId(Integer id) {

		EntityManager em = Factory.getEntityManager();
		List result = null;
		try {
			result = em.createNamedQuery("LkYetkiRol.findByMenuId")
					.setParameter("prmMenuId", id).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List<LkYetkiRol> findBySubMenu(boolean isSubMenu) {

		EntityManager em = Factory.getEntityManager();
		List result = null;
		try {
			result = em.createNamedQuery("LkYetkiRol.findBySubMenu")
					.setParameter("prmIsSubMenu", isSubMenu).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
