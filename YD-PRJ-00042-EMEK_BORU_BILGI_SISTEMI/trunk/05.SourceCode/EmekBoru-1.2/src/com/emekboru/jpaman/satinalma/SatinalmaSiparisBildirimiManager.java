/**
 * 
 */
package com.emekboru.jpaman.satinalma;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.satinalma.SatinalmaSiparisBildirimi;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class SatinalmaSiparisBildirimiManager extends
		CommonQueries<SatinalmaSiparisBildirimi> {

	@SuppressWarnings("unchecked")
	public List<SatinalmaSiparisBildirimi> getAllSatinalmaSiparisBildirimi() {

		List<SatinalmaSiparisBildirimi> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("SatinalmaSiparisBildirimi.findAll")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
