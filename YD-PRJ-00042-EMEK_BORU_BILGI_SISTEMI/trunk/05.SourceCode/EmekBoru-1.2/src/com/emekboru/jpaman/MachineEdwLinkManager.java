/**
 * 
 */
package com.emekboru.jpaman;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.MachineEdwLink;

/**
 * @author Alper K
 * 
 */
public class MachineEdwLinkManager extends CommonQueries<MachineEdwLink> {

	@SuppressWarnings("unchecked")
	public static List<MachineEdwLink> getMachineEdwLinkDustsInMachine(
			Integer machineId) {

		List<MachineEdwLink> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("MachineEdwLink.findDustByMachineId")
					.setParameter("prmMachineId", machineId).getResultList();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<MachineEdwLink> getMachineEdwLinkWiresInMachine(
			Integer machineId) {

		List<MachineEdwLink> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("MachineEdwLink.findWireByMachineId")
					.setParameter("prmMachineId", machineId).getResultList();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<MachineEdwLink> findAllWireByMachineId(
			Integer prmMachineId) {

		List<MachineEdwLink> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("MachineEdwLink.findAllWireByMachineId")
					.setParameter("prmMachineId", prmMachineId).getResultList();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
