/**
 * 
 */
package com.emekboru.jpaman.kalibrasyon;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kalibrasyon.KalibrasyonOfflineBoruUcu;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KalibrasyonOfflineBoruUcuManager extends
		CommonQueries<KalibrasyonOfflineBoruUcu> {
	@SuppressWarnings("unchecked")
	public static List<KalibrasyonOfflineBoruUcu> getAllKalibrasyon() {

		List<KalibrasyonOfflineBoruUcu> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("KalibrasyonOfflineBoruUcu.findAll")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
