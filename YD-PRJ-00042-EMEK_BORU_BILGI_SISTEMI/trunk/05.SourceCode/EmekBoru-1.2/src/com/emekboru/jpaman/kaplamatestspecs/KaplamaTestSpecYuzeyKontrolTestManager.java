package com.emekboru.jpaman.kaplamatestspecs;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.isolation.IsolationTestType;
import com.emekboru.jpa.kaplamatestspecs.KaplamaTestSpecYuzeyKontrolTest;
import com.emekboru.jpaman.Factory;

public class KaplamaTestSpecYuzeyKontrolTestManager {

	@SuppressWarnings("unchecked")
	public List<KaplamaTestSpecYuzeyKontrolTest> seciliYuzeyKontrolTestTanim(
			IsolationTestType testType, Integer orderId) {

		List<KaplamaTestSpecYuzeyKontrolTest> result = new ArrayList<KaplamaTestSpecYuzeyKontrolTest>();

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query findQuerie = manager
				.createNamedQuery("KTSYKT.seciliYuzeyKontrolTestTanim");
		findQuerie.setParameter("prmTestType", testType);
		findQuerie.setParameter("prmOrderId", orderId);
		result = findQuerie.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return result;
	}
}
