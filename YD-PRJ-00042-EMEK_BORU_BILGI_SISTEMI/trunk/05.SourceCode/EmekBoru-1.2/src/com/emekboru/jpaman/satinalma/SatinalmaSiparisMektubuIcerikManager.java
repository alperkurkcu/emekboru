/**
 * 
 */
package com.emekboru.jpaman.satinalma;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.satinalma.SatinalmaSiparisMektubuIcerik;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class SatinalmaSiparisMektubuIcerikManager extends
		CommonQueries<SatinalmaSiparisMektubuIcerik> {

	@SuppressWarnings("unchecked")
	public List<SatinalmaSiparisMektubuIcerik> findByKararId(Integer prmKararId) {

		List<SatinalmaSiparisMektubuIcerik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaSiparisMektubuIcerik.findByKararId")
					.setParameter("prmDosyaId", prmKararId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaSiparisMektubuIcerik> findByKararIdMusteriId(
			Integer prmKararId, Integer prmMusteriId) {

		List<SatinalmaSiparisMektubuIcerik> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaSiparisMektubuIcerik.findByKararIdMusteriId")
					.setParameter("prmKararId", prmKararId)
					.setParameter("prmMusteriId", prmMusteriId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
