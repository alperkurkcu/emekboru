/**
 * 
 */
package com.emekboru.jpaman.kaplamamakinesi;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiAsitYikama;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KaplamaMakinesiAsitYikamaManager extends
		CommonQueries<KaplamaMakinesiAsitYikama> {

	@SuppressWarnings("unchecked")
	public static List<KaplamaMakinesiAsitYikama> getAllKaplamaMakinesiAsitYikama(
			Integer prmPipeId) {

		List<KaplamaMakinesiAsitYikama> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"KaplamaMakinesiAsitYikama.findAllByPipeId")
					.setParameter("prmPipeId", prmPipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
