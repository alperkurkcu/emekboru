package com.emekboru.jpaman.isolation;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;
import com.emekboru.utils.IsolationType;

public class IsolationTestDefinitionManager extends
		CommonQueries<IsolationTestDefinition> {

	@SuppressWarnings("unchecked")
	public List<IsolationTestDefinition> isolationFindById(Integer testId) {

		List<IsolationTestDefinition> result = new ArrayList<IsolationTestDefinition>();

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query findQuerie = manager
				.createNamedQuery("IsolationTestDefinition.findById");
		findQuerie.setParameter(1, testId);
		result = findQuerie.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return result;

	}

	@SuppressWarnings("unchecked")
	public static List<IsolationTestDefinition> findIsolationByItemIdAndIsolationType(
			Integer kaplama, SalesItem salesItem) {

		List<IsolationTestDefinition> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			if (kaplama == 0) {
				result = em
						.createNamedQuery(
								"IsolationTestDefinition.findIsolationByItemIdAndIsolationType")
						.setParameter(1, IsolationType.DIS_KAPLAMA)
						.setParameter(2, salesItem).getResultList();
			} else if (kaplama == 1) {
				result = em
						.createNamedQuery(
								"IsolationTestDefinition.findIsolationByItemIdAndIsolationType")
						.setParameter(1, IsolationType.IC_KAPLAMA)
						.setParameter(2, salesItem).getResultList();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
