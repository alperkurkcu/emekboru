/**
 * 
 */
package com.emekboru.jpaman.satinalma;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.satinalma.SatinalmaSatinalmaKarari;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class SatinalmaSatinalmaKarariManager extends
		CommonQueries<SatinalmaSatinalmaKarari> {

	@SuppressWarnings("unchecked")
	public List<SatinalmaSatinalmaKarari> getAllSatinalmaSatinalmaKarari() {

		List<SatinalmaSatinalmaKarari> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("SatinalmaSatinalmaKarari.findAll")
					.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<SatinalmaSatinalmaKarari> getPersonalSatinalmaSatinalmaKarari(
			Integer prmUserId) {

		List<SatinalmaSatinalmaKarari> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"SatinalmaSatinalmaKarari.findAllByUserId")
					.setParameter("prmUserId", prmUserId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	// @SuppressWarnings({ "unused", "unchecked" })
	// public static String getOtomatikSayi() {
	//
	// List<SatinalmaSatinalmaKarari> result = null;
	// Integer sayi = null;
	// String sonuc = null;
	//
	// DateFormat dateFormat = new SimpleDateFormat("yyyy");
	// Date date = new Date();
	//
	// EntityManager manager = Factory.getInstance().createEntityManager();
	// manager.getTransaction().begin();
	// try {
	// String query = new String();
	// query =
	// "SELECT c FROM SatinalmaSatinalmaKarari c WHERE c.eklemeZamani BETWEEN '"
	// + dateFormat.format(date)
	// + ".01.01' AND '"
	// + dateFormat.format(date) + ".12.31'";
	//
	// Query findQuery = manager.createQuery(query);
	// result = findQuery.getResultList();
	// manager.getTransaction().commit();
	// manager.close();
	// } catch (Exception ex) {
	// ex.printStackTrace();
	// }
	// if (result.size() == 0)
	// return sonuc = "ATF-" + dateFormat.format(date) + "/"
	// + (new DecimalFormat("0000000").format(1));
	// sayi = result.size() + 1;
	// return sonuc = "ATF-" + dateFormat.format(date) + "/"
	// + (new DecimalFormat("0000000").format(sayi));
	//
	// }

	@SuppressWarnings("unchecked")
	public Integer getOtomatikSayi() {

		List<SatinalmaSatinalmaKarari> result = null;
		Integer sayi = 0;
		// String sonuc = null;

		DateFormat dateFormat = new SimpleDateFormat("yyyy");
		Date date = new Date();

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		try {
			String query = new String();
			query = "SELECT c FROM SatinalmaSatinalmaKarari c WHERE c.eklemeZamani BETWEEN '"
					+ dateFormat.format(date)
					+ ".01.01' AND '"
					+ dateFormat.format(date) + ".12.31'";

			System.out.println("final query: " + query);

			Query findQuery = manager.createQuery(query);
			result = findQuery.getResultList();
			manager.getTransaction().commit();
			manager.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (result.size() == 0)
			return sayi;
		sayi = result.size() + 1;
		return sayi;

	}

}
