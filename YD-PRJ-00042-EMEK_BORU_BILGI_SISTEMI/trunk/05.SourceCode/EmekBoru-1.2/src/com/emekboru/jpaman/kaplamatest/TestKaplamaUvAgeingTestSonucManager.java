/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaUvAgeingTestSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaUvAgeingTestSonucManager extends
		CommonQueries<TestKaplamaUvAgeingTestSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaUvAgeingTestSonuc> getAllUvAgeingTestSonuc(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaUvAgeingTestSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("TestKaplamaUvAgeingTestSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
