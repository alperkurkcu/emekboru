/**
 * 
 */
package com.emekboru.jpaman.isemirleri;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.isemri.IsEmriPolietilenKaplamaKontrolParametreleri;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class IsEmriPolietilenKaplamaKontrolParametreleriManager extends
		CommonQueries<IsEmriPolietilenKaplamaKontrolParametreleri> {

	@SuppressWarnings("unchecked")
	public static List<IsEmriPolietilenKaplamaKontrolParametreleri> getAllIsEmriPolietilenKaplamaKontrolParametreleri(
			Integer prmIsEmriId) {

		List<IsEmriPolietilenKaplamaKontrolParametreleri> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"IsEmriPolietilenKaplamaKontrolParametreleri.findAllByIsEmriId")
					.setParameter("prmIsEmriId", prmIsEmriId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
