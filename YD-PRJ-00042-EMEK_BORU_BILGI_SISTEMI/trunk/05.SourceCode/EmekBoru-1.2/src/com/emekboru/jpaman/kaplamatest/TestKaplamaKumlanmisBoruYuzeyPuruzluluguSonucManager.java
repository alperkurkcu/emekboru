/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonucManager extends
		CommonQueries<TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc> getAllTestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
