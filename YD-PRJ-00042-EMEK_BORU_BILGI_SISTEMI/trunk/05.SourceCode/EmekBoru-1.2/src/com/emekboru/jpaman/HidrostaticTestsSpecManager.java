package com.emekboru.jpaman;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.HidrostaticTestsSpec;

public class HidrostaticTestsSpecManager extends
		CommonQueries<HidrostaticTestsSpec> {

	@SuppressWarnings("unchecked")
	public Integer kontrol(Integer globalId, Integer itemId) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query findQuerie = manager
				.createNamedQuery("HidrostaticTestsSpec.kontrol");
		findQuerie.setParameter("prmGlobalId", globalId);
		findQuerie.setParameter("prmItemId", itemId);
		List<HidrostaticTestsSpec> resultList = findQuerie.getResultList();
		Integer count = resultList.size();
		if (count.equals(null)) {
			count = 0;
		}
		manager.getTransaction().commit();
		manager.close();
		return count;
	}

	@SuppressWarnings("unchecked")
	public static List<HidrostaticTestsSpec> findByItemId(Integer prmItemId) {

		List<HidrostaticTestsSpec> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("HidrostaticTestsSpec.findByItemId")
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
