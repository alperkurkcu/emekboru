/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaStorageConditionTestSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaStorageConditionTestSonucManager extends
		CommonQueries<TestKaplamaStorageConditionTestSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaStorageConditionTestSonuc> getAllStorageConditionTestSonuc(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaStorageConditionTestSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestKaplamaStorageConditionTestSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
