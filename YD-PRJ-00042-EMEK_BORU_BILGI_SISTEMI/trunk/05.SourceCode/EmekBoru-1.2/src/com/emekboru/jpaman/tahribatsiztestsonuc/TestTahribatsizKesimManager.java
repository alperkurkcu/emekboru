/**
 * 
 */
package com.emekboru.jpaman.tahribatsiztestsonuc;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizKesim;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestTahribatsizKesimManager extends
		CommonQueries<TestTahribatsizKesim> {

	@SuppressWarnings("unchecked")
	public static List<TestTahribatsizKesim> getAllTestTahribatsizKesim(
			Integer pipeId) {

		List<TestTahribatsizKesim> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestTahribatsizKesim.findAll")
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}