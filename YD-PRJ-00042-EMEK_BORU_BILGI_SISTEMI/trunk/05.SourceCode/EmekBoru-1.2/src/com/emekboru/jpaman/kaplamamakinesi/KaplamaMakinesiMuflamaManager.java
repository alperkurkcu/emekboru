/**
 * 
 */
package com.emekboru.jpaman.kaplamamakinesi;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiMuflama;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KaplamaMakinesiMuflamaManager extends
		CommonQueries<KaplamaMakinesiMuflama> {

	@SuppressWarnings("unchecked")
	public static List<KaplamaMakinesiMuflama> getAllKaplamaMakinesiMuflama(
			Integer prmPipeId) {

		List<KaplamaMakinesiMuflama> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("KaplamaMakinesiMuflama.findAllByPipeId")
					.setParameter("prmPipeId", prmPipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
