/**
 * 
 */
package com.emekboru.jpaman.istakipformu;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.istakipformu.IsTakipFormuEpoksiKaplamaDevamIslemler;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class IsTakipFormuEpoksiKaplamaDevamIslemlerManager extends
		CommonQueries<IsTakipFormuEpoksiKaplamaDevamIslemler> {

	@SuppressWarnings("unchecked")
	public static List<IsTakipFormuEpoksiKaplamaDevamIslemler> getAllIsTakipFormuEpoksiKaplamaDevamIslemler(
			Integer itemId) {

		List<IsTakipFormuEpoksiKaplamaDevamIslemler> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"IsTakipFormuEpoksiKaplamaDevamIslemler.findAll")
					.setParameter("prmItemId", itemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
