/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaHolidayPinholeSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaHolidayPinholeSonucManager extends
		CommonQueries<TestKaplamaHolidayPinholeSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaHolidayPinholeSonuc> getAllTestKaplamaHolidayPinholeSonucTest(
			Integer globalId, Integer pipeId) {

		List<TestKaplamaHolidayPinholeSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("TestKaplamaHolidayPinholeSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
