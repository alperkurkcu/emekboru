package com.emekboru.jpaman;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.TestAgirlikDusurmeSonuc;
import com.emekboru.jpa.sales.order.SalesItem;

public class TestAgirlikDusurmeSonucManager extends
		CommonQueries<TestAgirlikDusurmeSonuc> {

	@SuppressWarnings({ "unchecked" })
	public static List<TestAgirlikDusurmeSonuc> getAllTestAgirlikDusmeSonuc(
			Integer globalId, Integer pipeId) {

		List<TestAgirlikDusurmeSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestAgirlikDusurmeSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<TestAgirlikDusurmeSonuc> getBySalesItemGlobalId(
			SalesItem prmSalesItem, int prmGlobalId) {

		List<TestAgirlikDusurmeSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestAgirlikDusurmeSonuc.getBySalesItemGlobalId")
					.setParameter("prmSalesItem", prmSalesItem.getItemId())
					.setParameter("prmGlobalId", prmGlobalId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public static List<TestAgirlikDusurmeSonuc> getByDokumNo(String prmDokumNo) {

		List<TestAgirlikDusurmeSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery("TestAgirlikDusurmeSonuc.getByDokumNo")
					.setParameter("prmDokumNo", prmDokumNo).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
