package com.emekboru.jpaman;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.CekmeTestsSpec;

public class CekmeTestSpecManager extends CommonQueries<CekmeTestsSpec> {

	// itemId sipari� kalemidir.
	public Integer kontrol(Integer globalId, Integer itemId) {
		Integer count = 0;

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query findQuerie = manager.createNamedQuery("CTS.kontrol");
		findQuerie.setParameter("prmGlobalId", globalId);
		findQuerie.setParameter("prmItemId", itemId);
		// testId yi count a esitleyip hem testId cekm�s oluyorum hemde
		// i�erisinde item var m� yok mu kontrol edyorum
		if (findQuerie.getResultList().isEmpty()) {
			return count;
		} else {
			Number result = (Number) findQuerie.getSingleResult();
			if (result.equals(null)) {

			} else {
				count = result.intValue();
				if (count.equals(null)) {
					count = 0;
				}
			}
		}
		manager.getTransaction().commit();
		manager.close();
		return count;
	}

	@SuppressWarnings("unchecked")
	public List<CekmeTestsSpec> seciliCekmeTestTanim(Integer prmGlobalId,
			Integer prmItemId) {

		List<CekmeTestsSpec> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("CTS.seciliCekmeTestTanim")
					.setParameter("prmGlobalId", prmGlobalId)
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;

	}

	@SuppressWarnings("unchecked")
	public List<CekmeTestsSpec> findByItemId(Integer prmItemId) {

		List<CekmeTestsSpec> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("CTS.findByItemId")
					.setParameter("prmItemId", prmItemId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;

	}
}
