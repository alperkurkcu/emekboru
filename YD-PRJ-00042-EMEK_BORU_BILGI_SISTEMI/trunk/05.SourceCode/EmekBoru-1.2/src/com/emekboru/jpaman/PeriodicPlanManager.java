package com.emekboru.jpaman;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.PeriodicPlan;

public class PeriodicPlanManager extends CommonQueries<PeriodicPlan> {

	@SuppressWarnings("unchecked")
	public List<PeriodicPlan> findAllOrderedForSchedule() {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();

		String query = "SELECT c FROM PeriodicPlan c ORDER BY c.year DESC, " + 
		"c.machine.name ASC, c.machinePart.partName ASC, c.planPeriod ASC";

		Logger.getAnonymousLogger().info(this.getClass().getName() + ":" + query);
		
		Query findQuery = manager.createQuery(query);
		List<PeriodicPlan> returnedList = findQuery.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return returnedList;
	}
}
