package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaDarbeSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

public class TestKaplamaDarbeSonucManager extends
		CommonQueries<TestKaplamaDarbeSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaDarbeSonuc> getAllKaplamaDarbeTestSonuc(
			int globalId, Integer pipeId) {

		List<TestKaplamaDarbeSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em.createNamedQuery("TestKaplamaDarbeSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
