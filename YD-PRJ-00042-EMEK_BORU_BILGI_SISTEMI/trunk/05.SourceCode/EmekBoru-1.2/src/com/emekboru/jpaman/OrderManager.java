package com.emekboru.jpaman;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.primefaces.model.SortOrder;

import com.emekboru.config.Config;
import com.emekboru.config.Status;
import com.emekboru.jpa.Customer;
import com.emekboru.jpa.Order;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class OrderManager extends CommonQueries<Order> {

	public final static String MAX_NUMBER_QUERY = "SELECT o.orderNumber FROM Order o WHERE o.orderYear=:orderYear "
			+ "ORDER BY o.orderNumber DESC";

	public int getLastOrderNumber(int orderYear) {

		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query findQuery = manager.createQuery(MAX_NUMBER_QUERY);
		findQuery.setParameter("orderYear", orderYear);
		List<Integer> results = findQuery.setMaxResults(1).getResultList();
		manager.getTransaction().commit();
		manager.close();

		if (results.size() == 0)
			return 0;

		return results.get(0);
	}

	// search the database for orders that are not completed yet
	// the Status.isEnd = false
	public List<Order> findNotEnded(int customerId, List<Status> statuses) {

		List<Order> result = new ArrayList<Order>();
		ArrayList<WhereClauseArgs> conditionList = new ArrayList<WhereClauseArgs>(
				0);
		WhereClauseArgs custArg = new WhereClauseArgs.Builder()
				.setFieldName("customer.customerId").setValue(customerId)
				.build();
		conditionList.add(custArg);
		List<Order> list = selectFromWhereQuerieComplexName(Order.class,
				conditionList);
		for (Order o : list)
			if (isStatusOk(o, statuses))
				result.add(o);

		return result;
	}

	// load all orders that are not finished yet from the database
	// from all customers
	public List<Order> findAllNotEnded(Config config) {

		WhereClauseArgs arg = null;
		ArrayList<WhereClauseArgs> condList = new ArrayList<WhereClauseArgs>();
		int index = 0;
		List<Status> statuses = config.getOrder().findUnendedStatuses();

		for (int i = 0; i < statuses.size(); i++) {

			if (!statuses.get(i).isEnd()) {
				index++;
				if (i == statuses.size() - 1)
					arg = new WhereClauseArgs.Builder()
							.setFieldName("statusId")
							.setOrderOfOccurance(index)
							.setValue(statuses.get(i).getId()).build();
				else
					arg = new WhereClauseArgs.Builder()
							.setFieldName("statusId").setNextOrClause(true)
							.setOrderOfOccurance(index)
							.setValue(statuses.get(i).getId()).build();
				condList.add(arg);
			}
		}

		List<Order> orders = selectFromWhereQuerieComplexName(Order.class,
				condList);
		return orders;
	}

	protected boolean isStatusOk(Order order, List<Status> statuses) {

		for (Status s : statuses)
			if (s.getId().equals(order.getStatusId()))
				return true;

		return false;
	}

	public List<Order> findCustomersNotEnded(Customer customer, Config config) {

		List<Order> result = new ArrayList<Order>(0);
		WhereClauseArgs arg1 = new WhereClauseArgs.Builder()
				.setFieldName("customer.customerId")
				.setValue(customer.getCustomerId()).build();
		ArrayList<WhereClauseArgs> list = new ArrayList<WhereClauseArgs>();
		list.add(arg1);

		List<Order> allList = selectFromWhereQuerieComplexName(Order.class,
				list);
		List<Status> statuses = config.getOrder().findUnendedStatuses();

		for (Order o : allList)
			if (isStatusOk(o, statuses))
				result.add(o);

		return result;
	}

	public List<Order> findRangeFilter(int start, int pagesize,
			ArrayList<String> alanlar, ArrayList<String> veriler,
			String sortField, SortOrder sortOrder) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		String hsql = "select r from Order r ";
		if (alanlar != null && alanlar.size() > 0) {
			hsql += "where ";
			for (int i = 0; i < alanlar.size(); i++) {
				if (alanlar.get(i) != null && !alanlar.get(i).equals("")
						&& !alanlar.get(i).equals("orderYearAndOrderNumber"))
					hsql += "upper(r." + alanlar.get(i) + ") like '%"
							+ veriler.get(i).toUpperCase() + "%' ";
				else if (alanlar.get(i) != null && !alanlar.get(i).equals("")
						&& alanlar.get(i).equals("orderYearAndOrderNumber")) {
					hsql += " CONCAT(r.orderYear,CONCAT('/',r.orderNumber )) like '%"
							+ veriler.get(i) + "%' ";
				}
			}
		}
		String siralama;
		if (sortField != null && !sortField.equals("")
				&& !sortField.equals("orderYearAndOrderNumber")) {
			if (sortOrder.ordinal() == 1) {
				siralama = " desc";
			} else {
				siralama = " asc";
			}
			hsql += "order by r." + sortField + siralama;
		} else if (sortField != null && !sortField.equals("")
				&& sortField.equals("orderYearAndOrderNumber")) {
			if (sortOrder.ordinal() == 1) {
				siralama = " desc";
			} else {
				siralama = " asc";
			}
			hsql += "order by r.orderYear " + siralama + " , r.orderNumber"
					+ siralama;
		} else
			hsql += "order by r.orderId desc";

		Query query = manager.createQuery(hsql);
		query.setMaxResults(pagesize);
		query.setFirstResult(start);
		List<Order> orders = query.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return orders;
	}

	public int countTotalRecord() {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		Query query = manager.createNamedQuery("totalOrder");
		Number result = (Number) query.getSingleResult();
		int rr = result.intValue();
		manager.getTransaction().commit();
		manager.close();
		return rr;
	}

	public List<Order> getOrderByOrderYearAndCustomer(String orderYear,
			int customerId) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		String hsql = "select r from Order r where r.customer.customerId = "
				+ customerId + " and r.orderYear = " + orderYear;
		Query query = manager.createQuery(hsql);
		List<Order> orders = query.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return orders;
	}

	public List<Order> getOrderByOrderYear(String orderYear) {
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getEntityManagerFactory().getCache().evictAll();
		manager.getTransaction().begin();
		String hsql = "select r from Order r where r.orderYear = " + orderYear
				+ " order by r.orderNumber desc";
		Query query = manager.createQuery(hsql);
		List<Order> orders = query.getResultList();
		manager.getTransaction().commit();
		manager.close();
		return orders;
	}

	// public List<Order> loadOrders() {
	// EntityManager manager = Factory.getInstance().createEntityManager();
	// manager.getEntityManagerFactory().getCache().evictAll();
	// manager.getTransaction().begin();
	// Query findQuerie = manager.createNamedQuery("Order.findAll");
	// findQuerie.setParameter("prmEnded", false);
	// List<Order> resultList = findQuerie.getResultList();
	// manager.getTransaction().commit();
	// manager.close();
	// return resultList;
	// }

}
