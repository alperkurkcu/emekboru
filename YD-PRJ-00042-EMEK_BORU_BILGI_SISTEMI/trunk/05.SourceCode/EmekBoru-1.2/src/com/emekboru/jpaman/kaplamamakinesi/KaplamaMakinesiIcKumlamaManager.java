/**
 * 
 */
package com.emekboru.jpaman.kaplamamakinesi;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiIcKumlama;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KaplamaMakinesiIcKumlamaManager extends
		CommonQueries<KaplamaMakinesiIcKumlama> {

	@SuppressWarnings("unchecked")
	public static List<KaplamaMakinesiIcKumlama> getAllKaplamaMakinesiIcKumlama(
			Integer prmPipeId) {

		List<KaplamaMakinesiIcKumlama> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"KaplamaMakinesiIcKumlama.findAllByPipeId")
					.setParameter("prmPipeId", prmPipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
