package com.emekboru.jpaman;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.emekboru.jpa.CoatRawMaterial;


public class CoatRawMaterialManager extends CommonQueries<CoatRawMaterial>{

	@SuppressWarnings("unchecked")
	public List<CoatRawMaterial> findUnfinishedCoatMaterialByType(Integer coatMaterialTypeId){
		
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query querie = manager.createNamedQuery("CoatRawMaterial.findUnfinishedCoatMaterialByType");
		querie.setParameter("coatMaterialTypeId", coatMaterialTypeId);
		List<CoatRawMaterial> resultList = querie.getResultList();
		manager.close();
		return resultList;
	}
	
	//find the earliest date (the entrance date)
	@SuppressWarnings("unchecked")
	public List<Date> findMinimumDateByType(Integer coatMaterialTypeId){
		
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query findQuerie = manager.createNamedQuery("CoatRawMaterial.findMinimumDateByType");
		findQuerie.setParameter("coatMaterialTypeId", coatMaterialTypeId);		
		List<Date> list = findQuerie.setMaxResults(1).getResultList();
		return list;
	}

	//find the latest date (the entrance date)
	@SuppressWarnings("unchecked")
	public List<Date> findMaximumDateByType(Integer coatMaterialTypeId){
		
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		Query findQuerie = manager.createNamedQuery("CoatRawMaterial.findMaximumDateByType");
		findQuerie.setParameter("coatMaterialTypeId", coatMaterialTypeId);		
		List<Date> list = findQuerie.setMaxResults(1).getResultList();
		return list;
	}


}
