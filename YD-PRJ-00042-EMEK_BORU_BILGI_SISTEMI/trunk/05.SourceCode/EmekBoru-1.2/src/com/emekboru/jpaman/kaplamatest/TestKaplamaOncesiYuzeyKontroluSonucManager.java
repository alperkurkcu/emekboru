/**
 * 
 */
package com.emekboru.jpaman.kaplamatest;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamatest.TestKaplamaOncesiYuzeyKontroluSonuc;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class TestKaplamaOncesiYuzeyKontroluSonucManager extends
		CommonQueries<TestKaplamaOncesiYuzeyKontroluSonuc> {

	@SuppressWarnings("unchecked")
	public static List<TestKaplamaOncesiYuzeyKontroluSonuc> getAllTestKaplamaOncesiYuzeyKontroluSonuc(
			int globalId, Integer pipeId) {

		List<TestKaplamaOncesiYuzeyKontroluSonuc> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"TestKaplamaOncesiYuzeyKontroluSonuc.findAll")
					.setParameter("prmGlobalId", globalId)
					.setParameter("prmPipeId", pipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}
}
