/**
 * 
 */
package com.emekboru.jpaman.kaplamamakinesi;

import java.util.List;

import javax.persistence.EntityManager;

import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiFircalama;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
public class KaplamaMakinesiFircalamaManager extends
		CommonQueries<KaplamaMakinesiFircalama> {

	@SuppressWarnings("unchecked")
	public static List<KaplamaMakinesiFircalama> getAllKaplamaMakinesiFircalama(
			Integer prmPipeId) {

		List<KaplamaMakinesiFircalama> result = null;
		EntityManager em = Factory.getEntityManager();
		try {
			result = em
					.createNamedQuery(
							"KaplamaMakinesiFircalama.findAllByPipeId")
					.setParameter("prmPipeId", prmPipeId).getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
