package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaBetonGozKontrolSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaBetonGozKontrolSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaplamaBetonGozKontrolSonucBean")
@ViewScoped
public class TestKaplamaBetonGozKontrolSonucBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private TestKaplamaBetonGozKontrolSonuc testKaplamaBetonGozKontrolSonucForm = new TestKaplamaBetonGozKontrolSonuc();
	private List<TestKaplamaBetonGozKontrolSonuc> allTestKaplamaBetonGozKontrolSonucList = new ArrayList<TestKaplamaBetonGozKontrolSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateTestKaplamaBetonGozKontrolSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaBetonGozKontrolSonucManager manager = new TestKaplamaBetonGozKontrolSonucManager();

		if (testKaplamaBetonGozKontrolSonucForm.getId() == null) {

			testKaplamaBetonGozKontrolSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaBetonGozKontrolSonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaBetonGozKontrolSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaBetonGozKontrolSonucForm.setBagliTestId(bagliTest);
			testKaplamaBetonGozKontrolSonucForm.setBagliGlobalId(bagliTest);

			manager.enterNew(testKaplamaBetonGozKontrolSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {
			testKaplamaBetonGozKontrolSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaBetonGozKontrolSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			manager.updateEntity(testKaplamaBetonGozKontrolSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addTestKaplamaBetonGozKontrolSonuc() {

		testKaplamaBetonGozKontrolSonucForm = new TestKaplamaBetonGozKontrolSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaBetonGozKontrolSonucListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaBetonGozKontrolSonucList = TestKaplamaBetonGozKontrolSonucManager
				.getAllTestKaplamaBetonGozKontrolSonuc(globalId, pipeId);
		testKaplamaBetonGozKontrolSonucForm = new TestKaplamaBetonGozKontrolSonuc();
	}

	public void deleteTestKaplamaBetonGozKontrolSonuc() {

		bagliTestId = testKaplamaBetonGozKontrolSonucForm.getBagliGlobalId()
				.getId();

		if (testKaplamaBetonGozKontrolSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaBetonGozKontrolSonucManager manager = new TestKaplamaBetonGozKontrolSonucManager();

		manager.delete(testKaplamaBetonGozKontrolSonucForm);
		testKaplamaBetonGozKontrolSonucForm = new TestKaplamaBetonGozKontrolSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters
	public TestKaplamaBetonGozKontrolSonuc getTestKaplamaBetonGozKontrolSonucForm() {
		return testKaplamaBetonGozKontrolSonucForm;
	}

	public void setTestKaplamaBetonGozKontrolSonucForm(
			TestKaplamaBetonGozKontrolSonuc testKaplamaBetonGozKontrolSonucForm) {
		this.testKaplamaBetonGozKontrolSonucForm = testKaplamaBetonGozKontrolSonucForm;
	}

	public List<TestKaplamaBetonGozKontrolSonuc> getAllTestKaplamaBetonGozKontrolSonucList() {
		return allTestKaplamaBetonGozKontrolSonucList;
	}

	public void setAllTestKaplamaBetonGozKontrolSonucList(
			List<TestKaplamaBetonGozKontrolSonuc> allTestKaplamaBetonGozKontrolSonucList) {
		this.allTestKaplamaBetonGozKontrolSonucList = allTestKaplamaBetonGozKontrolSonucList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
