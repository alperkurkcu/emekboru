/**
 * 
 */
package com.emekboru.beans.kalibrasyon;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.kalibrasyon.KalibrasyonManuelUt;
import com.emekboru.jpaman.kalibrasyon.KalibrasyonManuelUtManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "kalibrasyonManuelUtBean")
@ViewScoped
public class KalibrasyonManuelUtBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<KalibrasyonManuelUt> allKalibrasyonList = new ArrayList<KalibrasyonManuelUt>();
	private KalibrasyonManuelUt kalibrasyonForm = new KalibrasyonManuelUt();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender = false;

	public KalibrasyonManuelUtBean() {
		// fillTestList();
	}

	public void kalibrasyonListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void addKalibrasyon() {
		updateButtonRender = false;
	}

	public void addOrUpdateKalibrasyon(ActionEvent e) {

		KalibrasyonManuelUtManager manager = new KalibrasyonManuelUtManager();
		if (kalibrasyonForm.getId() == null) {
			kalibrasyonForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			kalibrasyonForm.setEkleyenKullanici(userBean.getUser().getId());

			manager.enterNew(kalibrasyonForm);
			FacesContextUtils.addInfoMessage("SubmitMessage");
		} else {
			if (updateButtonRender) {
				kalibrasyonForm.setGuncellemeZamani(new java.sql.Timestamp(
						new java.util.Date().getTime()));
				kalibrasyonForm.setGuncelleyenKullanici(userBean.getUser()
						.getId());
				manager.updateEntity(kalibrasyonForm);
				FacesContextUtils.addInfoMessage("UpdateItemMessage");
			} else {
				clonner();
			}
		}
		fillTestList();
	}

	@SuppressWarnings("static-access")
	public void fillTestList() {
		KalibrasyonManuelUtManager manager = new KalibrasyonManuelUtManager();
		allKalibrasyonList = manager.getAllKalibrasyon();
		manager.refreshCollection(allKalibrasyonList);
		kalibrasyonForm = new KalibrasyonManuelUt();
	}

	public void deleteKalibrasyon() {

		KalibrasyonManuelUtManager manager = new KalibrasyonManuelUtManager();
		if (kalibrasyonForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		manager.delete(kalibrasyonForm);
		kalibrasyonForm = new KalibrasyonManuelUt();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		fillTestList();
	}

	public void clonner() {

		KalibrasyonManuelUt clone = new KalibrasyonManuelUt();
		KalibrasyonManuelUtManager manager = new KalibrasyonManuelUtManager();
		try {
			clone = (KalibrasyonManuelUt) this.kalibrasyonForm.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			FacesContextUtils.addPlainErrorMessage(e.getMessage());
		}

		clone.setId(null);
		clone.setEklemeZamani(new java.sql.Timestamp(new java.util.Date()
				.getTime()));
		clone.setEkleyenKullanici(userBean.getUser().getId());

		manager.enterNew(clone);
		FacesContextUtils.addInfoMessage("SubmitMessage");
		fillTestList();
	}

	// setters getters

	/**
	 * @return the allKalibrasyonList
	 */
	public List<KalibrasyonManuelUt> getAllKalibrasyonList() {
		return allKalibrasyonList;
	}

	/**
	 * @param allKalibrasyonList
	 *            the allKalibrasyonList to set
	 */
	public void setAllKalibrasyonList(
			List<KalibrasyonManuelUt> allKalibrasyonList) {
		this.allKalibrasyonList = allKalibrasyonList;
	}

	/**
	 * @return the kalibrasyonForm
	 */
	public KalibrasyonManuelUt getKalibrasyonForm() {
		return kalibrasyonForm;
	}

	/**
	 * @param kalibrasyonForm
	 *            the kalibrasyonForm to set
	 */
	public void setKalibrasyonForm(KalibrasyonManuelUt kalibrasyonForm) {
		this.kalibrasyonForm = kalibrasyonForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
