/**
 * 
 */
package com.emekboru.beans.stok;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.stok.StokTanimlarUrunGrubu;
import com.emekboru.jpaman.stok.StokTanimlarUrunGrubuManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "stokTanimlarUrunGrubuBean")
@ViewScoped
public class StokTanimlarUrunGrubuBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<StokTanimlarUrunGrubu> allStokTanimlarUrunGrubuList = new ArrayList<StokTanimlarUrunGrubu>();

	private StokTanimlarUrunGrubu stokTanimlarUrunGrubuForm = new StokTanimlarUrunGrubu();

	StokTanimlarUrunGrubuManager formManager = new StokTanimlarUrunGrubuManager();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	public StokTanimlarUrunGrubuBean() {

		fillTestList();
	}

	public void addForm() {

		stokTanimlarUrunGrubuForm = new StokTanimlarUrunGrubu();
		updateButtonRender = false;
	}

	public void addOrUpdateForm() {

		if (stokTanimlarUrunGrubuForm.getId() == null) {

			stokTanimlarUrunGrubuForm.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			stokTanimlarUrunGrubuForm.setEkleyenKullanici(userBean.getUser()
					.getId());

			formManager.enterNew(stokTanimlarUrunGrubuForm);

			this.stokTanimlarUrunGrubuForm = new StokTanimlarUrunGrubu();
			FacesContextUtils.addInfoMessage("FormSubmitMessage");

		} else {

			stokTanimlarUrunGrubuForm.setGuncellemeZamani(UtilInsCore
					.getTarihZaman());
			stokTanimlarUrunGrubuForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			formManager.updateEntity(stokTanimlarUrunGrubuForm);

			FacesContextUtils.addInfoMessage("FormUpdateMessage");
		}

		fillTestList();
	}

	public void deleteForm() {

		if (stokTanimlarUrunGrubuForm.getId() == null) {

			FacesContextUtils.addWarnMessage("NoSelectMessage");
			return;
		}
		formManager.delete(stokTanimlarUrunGrubuForm);
		fillTestList();
		FacesContextUtils.addWarnMessage("TestDeleteMessage");
	}

	public void fillTestList() {

		StokTanimlarUrunGrubuManager manager = new StokTanimlarUrunGrubuManager();
		allStokTanimlarUrunGrubuList = manager.getAllStokTanimlarUrunGrubu();
	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	// setters getters

	public List<StokTanimlarUrunGrubu> getAllStokTanimlarUrunGrubuList() {
		return allStokTanimlarUrunGrubuList;
	}

	public void setAllStokTanimlarUrunGrubuList(
			List<StokTanimlarUrunGrubu> allStokTanimlarUrunGrubuList) {
		this.allStokTanimlarUrunGrubuList = allStokTanimlarUrunGrubuList;
	}

	public StokTanimlarUrunGrubu getStokTanimlarUrunGrubuForm() {
		return stokTanimlarUrunGrubuForm;
	}

	public void setStokTanimlarUrunGrubuForm(
			StokTanimlarUrunGrubu stokTanimlarUrunGrubuForm) {
		this.stokTanimlarUrunGrubuForm = stokTanimlarUrunGrubuForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
