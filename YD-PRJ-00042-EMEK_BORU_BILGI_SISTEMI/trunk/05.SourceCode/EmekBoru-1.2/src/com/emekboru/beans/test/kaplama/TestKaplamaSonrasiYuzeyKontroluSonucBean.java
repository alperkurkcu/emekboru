/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaSonrasiYuzeyKontroluSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaSonrasiYuzeyKontroluSonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testKaplamaSonrasiYuzeyKontroluSonucBean")
@ViewScoped
public class TestKaplamaSonrasiYuzeyKontroluSonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<TestKaplamaSonrasiYuzeyKontroluSonuc> allKaplamaSonrasiYuzeyKontroluSonucList = new ArrayList<TestKaplamaSonrasiYuzeyKontroluSonuc>();
	private TestKaplamaSonrasiYuzeyKontroluSonuc testKaplamaSonrasiYuzeyKontroluSonucForm = new TestKaplamaSonrasiYuzeyKontroluSonuc();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;
	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addKaplamaSonrasiYuzeyKontroluSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaSonrasiYuzeyKontroluSonucManager tkdsManager = new TestKaplamaSonrasiYuzeyKontroluSonucManager();

		if (testKaplamaSonrasiYuzeyKontroluSonucForm.getId() == null) {

			testKaplamaSonrasiYuzeyKontroluSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaSonrasiYuzeyKontroluSonucForm
					.setEkleyenKullanici(userBean.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaSonrasiYuzeyKontroluSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaSonrasiYuzeyKontroluSonucForm.setBagliTestId(bagliTest);
			testKaplamaSonrasiYuzeyKontroluSonucForm
					.setBagliGlobalId(bagliTest);

			tkdsManager.enterNew(testKaplamaSonrasiYuzeyKontroluSonucForm);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaSonrasiYuzeyKontroluSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaSonrasiYuzeyKontroluSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			tkdsManager.updateEntity(testKaplamaSonrasiYuzeyKontroluSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");

		}
		fillTestList(prmGlobalId, prmPipeId);
	}

	public void addKaplamaSonrasiYuzeyKontrolu() {
		testKaplamaSonrasiYuzeyKontroluSonucForm = new TestKaplamaSonrasiYuzeyKontroluSonuc();
		updateButtonRender = false;
	}

	public void kaplamaDarbeListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(int globalId, Integer pipeId2) {
		allKaplamaSonrasiYuzeyKontroluSonucList = TestKaplamaSonrasiYuzeyKontroluSonucManager
				.getAllTestKaplamaSonrasiYuzeyKontroluSonuc(globalId, pipeId2);
		testKaplamaSonrasiYuzeyKontroluSonucForm = new TestKaplamaSonrasiYuzeyKontroluSonuc();
	}

	public void deleteTestKaplamaSonrasiYuzeyKontroluSonuc() {

		bagliTestId = testKaplamaSonrasiYuzeyKontroluSonucForm.getBagliTestId()
				.getId();

		if (testKaplamaSonrasiYuzeyKontroluSonucForm == null
				|| testKaplamaSonrasiYuzeyKontroluSonucForm.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaSonrasiYuzeyKontroluSonucManager tkdsManager = new TestKaplamaSonrasiYuzeyKontroluSonucManager();
		tkdsManager.delete(testKaplamaSonrasiYuzeyKontroluSonucForm);
		testKaplamaSonrasiYuzeyKontroluSonucForm = new TestKaplamaSonrasiYuzeyKontroluSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setters getters

	/**
	 * @return the allKaplamaSonrasiYuzeyKontroluSonucList
	 */
	public List<TestKaplamaSonrasiYuzeyKontroluSonuc> getAllKaplamaSonrasiYuzeyKontroluSonucList() {
		return allKaplamaSonrasiYuzeyKontroluSonucList;
	}

	/**
	 * @param allKaplamaSonrasiYuzeyKontroluSonucList
	 *            the allKaplamaSonrasiYuzeyKontroluSonucList to set
	 */
	public void setAllKaplamaSonrasiYuzeyKontroluSonucList(
			List<TestKaplamaSonrasiYuzeyKontroluSonuc> allKaplamaSonrasiYuzeyKontroluSonucList) {
		this.allKaplamaSonrasiYuzeyKontroluSonucList = allKaplamaSonrasiYuzeyKontroluSonucList;
	}

	/**
	 * @return the testKaplamaSonrasiYuzeyKontroluSonucForm
	 */
	public TestKaplamaSonrasiYuzeyKontroluSonuc getTestKaplamaSonrasiYuzeyKontroluSonucForm() {
		return testKaplamaSonrasiYuzeyKontroluSonucForm;
	}

	/**
	 * @param testKaplamaSonrasiYuzeyKontroluSonucForm
	 *            the testKaplamaSonrasiYuzeyKontroluSonucForm to set
	 */
	public void setTestKaplamaSonrasiYuzeyKontroluSonucForm(
			TestKaplamaSonrasiYuzeyKontroluSonuc testKaplamaSonrasiYuzeyKontroluSonucForm) {
		this.testKaplamaSonrasiYuzeyKontroluSonucForm = testKaplamaSonrasiYuzeyKontroluSonucForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
