package com.emekboru.beans.sales.order;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.emekboru.beans.sales.SalesMenuBean;
import com.emekboru.jpa.sales.order.SalesInvoice;
import com.emekboru.jpaman.sales.order.SalesInvoiceManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "salesInvoiceBean")
@ViewScoped
public class SalesInvoiceBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6623493466399110153L;

	@ManagedProperty(value = "#{salesMenuBean}")
	private SalesMenuBean salesMenuBean;

	private SalesInvoice newInvoice = new SalesInvoice();
	private SalesInvoice selectedInvoice;

	private List<SalesInvoice> invoiceList;
	private List<SalesInvoice> filteredList;

	public SalesInvoiceBean() {
		newInvoice = new SalesInvoice();
		selectedInvoice = new SalesInvoice();
	}

	@PostConstruct
	public void load() {
		System.out.println("SalesInvoiceBean.load()");

		try {
			SalesInvoiceManager manager = new SalesInvoiceManager();
			invoiceList = manager.findAll(SalesInvoice.class);

			selectedInvoice = invoiceList.get(0);
		} catch (Exception e) {
			System.out.println("SalesInvoiceBean:" + e.toString());
		}
	}

	public void add() {
		System.out.println("SalesInvoiceBean.add()");

		try {
			SalesInvoiceManager manager = new SalesInvoiceManager();

			invoiceList.add(newInvoice);
			manager.enterNew(newInvoice);

			newInvoice = new SalesInvoice();
			FacesContextUtils.addInfoMessage("SubmitMessage");
		} catch (Exception e) {
			System.out.println("SalesInvoiceBean:" + e.toString());
		}
	}

	public void update() {
		System.out.println("SalesInvoiceBean.update()");

		try {
			SalesInvoiceManager manager = new SalesInvoiceManager();
			manager.updateEntity(selectedInvoice);

			FacesContextUtils.addInfoMessage("UpdateItemMessage");
		} catch (Exception e) {
			System.out.println("SalesInvoiceBean:" + e.toString());
		}
	}

	public void delete() {
		System.out.println("SalesInvoiceBean.delete()");

		try {
			SalesInvoiceManager manager = new SalesInvoiceManager();
			manager.delete(selectedInvoice);

			invoiceList.remove(selectedInvoice);
			selectedInvoice = invoiceList.get(0);
			FacesContextUtils.addWarnMessage("DeletedMessage");
		} catch (Exception e) {
			System.out.println("SalesInvoiceBean:" + e.toString());
		}
	}

	public void calculateRemainderSelected() {
		System.out.println("SalesInvoiceBean.calculateRemainder()");

		try {
			if (selectedInvoice.getTotalAmount() > 0
					&& selectedInvoice.getTotalAmount() >= selectedInvoice
							.getPaid()) {
				selectedInvoice.setRest(selectedInvoice.getTotalAmount()
						- selectedInvoice.getPaid());
			} else {
				selectedInvoice.setRest(0);
			}
			System.out.println("rest = " + selectedInvoice.getRest());
		} catch (Exception ex) {
			System.out.println("SalesInvoiceBean.calculateRemainderSelected:"
					+ ex.toString());
		}
	}

	public void calculateRemainderNew() {
		System.out.println("SalesInvoiceBean.calculateRemainder()");

		try {
			if (newInvoice.getTotalAmount() > 0
					&& newInvoice.getTotalAmount() >= newInvoice.getPaid()) {
				newInvoice.setRest(newInvoice.getTotalAmount()
						- newInvoice.getPaid());
			} else {
				newInvoice.setRest(0);
			}
			System.out.println("rest = " + newInvoice.getRest());
		} catch (Exception ex) {
			System.out.println("SalesInvoiceBean.calculateRemainderNew"
					+ ex.toString());
		}
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public SalesInvoice getNewInvoice() {
		return newInvoice;
	}

	public void setNewInvoice(SalesInvoice newInvoice) {
		this.newInvoice = newInvoice;
	}

	public SalesInvoice getSelectedInvoice() {
		try {
			if (selectedInvoice == null)
				selectedInvoice = new SalesInvoice();
		} catch (Exception ex) {
			selectedInvoice = new SalesInvoice();
		}
		return selectedInvoice;
	}

	public void setSelectedInvoice(SalesInvoice selectedInvoice) {
		this.selectedInvoice = selectedInvoice;
	}

	public List<SalesInvoice> getInvoiceList() {
		return invoiceList;
	}

	public void setInvoiceList(List<SalesInvoice> invoiceList) {
		this.invoiceList = invoiceList;
	}

	public List<SalesInvoice> getFilteredList() {
		try {
			selectedInvoice = filteredList.get(0);
			salesMenuBean.showNewType1Menu();
		} catch (Exception ex) {
			System.out.println("getFilteredList: " + ex);
		}
		return filteredList;
	}

	public void setFilteredList(List<SalesInvoice> filteredList) {
		this.filteredList = filteredList;
	}

	public SalesMenuBean getSalesMenuBean() {
		return salesMenuBean;
	}

	public void setSalesMenuBean(SalesMenuBean salesMenuBean) {
		this.salesMenuBean = salesMenuBean;
	}
}
