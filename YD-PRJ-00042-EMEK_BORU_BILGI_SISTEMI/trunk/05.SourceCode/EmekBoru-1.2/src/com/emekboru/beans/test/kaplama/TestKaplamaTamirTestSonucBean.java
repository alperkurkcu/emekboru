package com.emekboru.beans.test.kaplama;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaTamirTestSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaTamirTestSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaplamaTamirTestSonucBean")
@ViewScoped
public class TestKaplamaTamirTestSonucBean {

	private TestKaplamaTamirTestSonuc testKaplamaTamirTestSonucForm = new TestKaplamaTamirTestSonuc();
	private List<TestKaplamaTamirTestSonuc> allTestKaplamaTamirTestSonucList = new ArrayList<TestKaplamaTamirTestSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateTestKaplamaTamirTestSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaTamirTestSonucManager manager = new TestKaplamaTamirTestSonucManager();

		if (testKaplamaTamirTestSonucForm.getId() == null) {

			testKaplamaTamirTestSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaTamirTestSonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaTamirTestSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaTamirTestSonucForm.setBagliTestId(bagliTest);
			testKaplamaTamirTestSonucForm.setBagliGlobalId(bagliTest);

			manager.enterNew(testKaplamaTamirTestSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaTamirTestSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaTamirTestSonucForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			manager.updateEntity(testKaplamaTamirTestSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addTestKaplamaTamirTestSonuc() {

		testKaplamaTamirTestSonucForm = new TestKaplamaTamirTestSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaTamirTestSonucListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaTamirTestSonucList = TestKaplamaTamirTestSonucManager
				.getAllTestKaplamaTamirTestSonuc(globalId, pipeId);
		testKaplamaTamirTestSonucForm = new TestKaplamaTamirTestSonuc();
	}

	public void deleteTestKaplamaTamirTestSonuc() {

		bagliTestId = testKaplamaTamirTestSonucForm.getBagliGlobalId().getId();

		if (testKaplamaTamirTestSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaTamirTestSonucManager manager = new TestKaplamaTamirTestSonucManager();
		manager.delete(testKaplamaTamirTestSonucForm);
		testKaplamaTamirTestSonucForm = new TestKaplamaTamirTestSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters
	public TestKaplamaTamirTestSonuc getTestKaplamaTamirTestSonucForm() {
		return testKaplamaTamirTestSonucForm;
	}

	public void setTestKaplamaTamirTestSonucForm(
			TestKaplamaTamirTestSonuc testKaplamaTamirTestSonucForm) {
		this.testKaplamaTamirTestSonucForm = testKaplamaTamirTestSonucForm;
	}

	public List<TestKaplamaTamirTestSonuc> getAllTestKaplamaTamirTestSonucList() {
		return allTestKaplamaTamirTestSonucList;
	}

	public void setAllTestKaplamaTamirTestSonucList(
			List<TestKaplamaTamirTestSonuc> allTestKaplamaTamirTestSonucList) {
		this.allTestKaplamaTamirTestSonucList = allTestKaplamaTamirTestSonucList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
