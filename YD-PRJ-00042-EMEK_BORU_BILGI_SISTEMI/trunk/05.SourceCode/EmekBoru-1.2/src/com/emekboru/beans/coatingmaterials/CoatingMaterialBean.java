package com.emekboru.beans.coatingmaterials;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.CoatMaterialType;
import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.PipeCoatLayer;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiAmg1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiAmg1Spec;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiBkm1KatiMadde;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiBkm1KuruMadde;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiBkm1Ph;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiEpoksiTozEea1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiEpoksiTozEjs1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiEpoksiTozEkz1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiEpoksiTozEni1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiEpoksiTozEta1Sonuc;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiEpoksiTozEty1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiMfr1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiMfr1Spec;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiOit1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiPdd1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiPdd1Spec;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiSertlik;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeGirdiSertlikSpec;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeUreticiKst1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeUreticiMfr1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeUreticiOit1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeUreticiPdd1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeUreticiPkd1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeUreticiPks1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeUreticiPku1;
import com.emekboru.jpa.kaplamagirditest.TestHammaddeUreticiYst1;
import com.emekboru.jpaman.CoatMaterialTypeManager;
import com.emekboru.jpaman.CoatRawMaterialManager;
import com.emekboru.jpaman.PipeCoatLayerManager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeGirdiAmg1Manager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeGirdiAmg1SpecManager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeGirdiBkm1KatiMaddeManager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeGirdiBkm1KuruMaddeManager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeGirdiBkm1PhManager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeGirdiEpoksiTozEea1Manager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeGirdiEpoksiTozEjs1Manager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeGirdiEpoksiTozEkz1Manager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeGirdiEpoksiTozEni1Manager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeGirdiEpoksiTozEta1SonucManager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeGirdiEpoksiTozEty1Manager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeGirdiMfr1Manager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeGirdiMfr1SpecManager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeGirdiOit1Manager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeGirdiPdd1Manager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeGirdiPdd1SpecManager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeGirdiSertlikManager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeGirdiSertlikSpecManager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeUreticiKst1Manager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeUreticiMfr1Manager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeUreticiOit1Manager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeUreticiPdd1Manager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeUreticiPkd1Manager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeUreticiPks1Manager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeUreticiPku1Manager;
import com.emekboru.jpaman.kaplamagirditest.TestHammaddeUreticiYst1Manager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.TimeSegment;
import com.emekboru.utils.UtilInsCore;

@ManagedBean(name = "coatingMaterialBean")
@ViewScoped
public class CoatingMaterialBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;
	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private List<CoatMaterialType> coatMaterialTypeList;
	private CoatMaterialType currentType;
	private CoatMaterialType newType;
	private List<CoatRawMaterial> coatRawMaterialList;
	private CoatRawMaterial currentCoatMaterial;
	private CoatRawMaterial historyCoatMaterial;
	private CoatRawMaterial newCoatMaterial;
	private List<CoatRawMaterial> availableCoatRawMaterial;

	private TestHammaddeGirdiEpoksiTozEta1Sonuc selectedEta1Sonuc = new TestHammaddeGirdiEpoksiTozEta1Sonuc();
	private TestHammaddeGirdiEpoksiTozEta1Sonuc eta1 = new TestHammaddeGirdiEpoksiTozEta1Sonuc();
	private TestHammaddeGirdiEpoksiTozEni1 selectedEni1Sonuc = new TestHammaddeGirdiEpoksiTozEni1();
	private TestHammaddeGirdiEpoksiTozEni1 eni1 = new TestHammaddeGirdiEpoksiTozEni1();
	private TestHammaddeGirdiEpoksiTozEjs1 selectedEjs1Sonuc = new TestHammaddeGirdiEpoksiTozEjs1();
	private TestHammaddeGirdiEpoksiTozEjs1 ejs1 = new TestHammaddeGirdiEpoksiTozEjs1();
	private TestHammaddeGirdiEpoksiTozEea1 selectedEea1Sonuc = new TestHammaddeGirdiEpoksiTozEea1();
	private TestHammaddeGirdiEpoksiTozEea1 eea1 = new TestHammaddeGirdiEpoksiTozEea1();
	private TestHammaddeGirdiEpoksiTozEkz1 selectedEkz1Sonuc = new TestHammaddeGirdiEpoksiTozEkz1();
	private TestHammaddeGirdiEpoksiTozEkz1 ekz1 = new TestHammaddeGirdiEpoksiTozEkz1();
	private TestHammaddeGirdiEpoksiTozEty1 selectedEty1Sonuc = new TestHammaddeGirdiEpoksiTozEty1();
	private TestHammaddeGirdiEpoksiTozEty1 ety1 = new TestHammaddeGirdiEpoksiTozEty1();

	private TestHammaddeUreticiMfr1 selectedUreticiMfr1Sonuc = new TestHammaddeUreticiMfr1();
	private TestHammaddeUreticiMfr1 mfr1Uretici = new TestHammaddeUreticiMfr1();
	private TestHammaddeUreticiPdd1 selectedUreticiPdd1Sonuc = new TestHammaddeUreticiPdd1();
	private TestHammaddeUreticiPdd1 pdd1Uretici = new TestHammaddeUreticiPdd1();
	private TestHammaddeUreticiOit1 selectedUreticiOit1Sonuc = new TestHammaddeUreticiOit1();
	private TestHammaddeUreticiOit1 oit1Uretici = new TestHammaddeUreticiOit1();

	private TestHammaddeUreticiYst1 selectedUreticiYst1Sonuc = new TestHammaddeUreticiYst1();
	private TestHammaddeUreticiYst1 yst1Uretici = new TestHammaddeUreticiYst1();
	private TestHammaddeUreticiKst1 selectedUreticiKst1Sonuc = new TestHammaddeUreticiKst1();
	private TestHammaddeUreticiKst1 kst1Uretici = new TestHammaddeUreticiKst1();
	private TestHammaddeUreticiPkd1 selectedUreticiPkd1Sonuc = new TestHammaddeUreticiPkd1();
	private TestHammaddeUreticiPkd1 pkd1Uretici = new TestHammaddeUreticiPkd1();
	private TestHammaddeUreticiPks1 selectedUreticiPks1Sonuc = new TestHammaddeUreticiPks1();
	private TestHammaddeUreticiPks1 pks1Uretici = new TestHammaddeUreticiPks1();
	private TestHammaddeUreticiPku1 selectedUreticiPku1Sonuc = new TestHammaddeUreticiPku1();
	private TestHammaddeUreticiPku1 pku1Uretici = new TestHammaddeUreticiPku1();

	private TestHammaddeGirdiMfr1 selectedMfr1Sonuc = new TestHammaddeGirdiMfr1();
	private TestHammaddeGirdiMfr1 mfr1 = new TestHammaddeGirdiMfr1();
	private TestHammaddeGirdiPdd1 selectedPdd1Sonuc = new TestHammaddeGirdiPdd1();
	private TestHammaddeGirdiPdd1 pdd1 = new TestHammaddeGirdiPdd1();
	private TestHammaddeGirdiOit1 selectedOit1Sonuc = new TestHammaddeGirdiOit1();
	private TestHammaddeGirdiOit1 oit1 = new TestHammaddeGirdiOit1();

	private TestHammaddeGirdiMfr1Spec selectedMfr1SpecSonuc = new TestHammaddeGirdiMfr1Spec();
	private TestHammaddeGirdiMfr1Spec mfr1Spec = new TestHammaddeGirdiMfr1Spec();
	private TestHammaddeGirdiPdd1Spec selectedPdd1SpecSonuc = new TestHammaddeGirdiPdd1Spec();
	private TestHammaddeGirdiPdd1Spec pdd1Spec = new TestHammaddeGirdiPdd1Spec();
	private TestHammaddeGirdiSertlikSpec selectedSertlikSpecSonuc = new TestHammaddeGirdiSertlikSpec();
	private TestHammaddeGirdiSertlikSpec sertlikSpec = new TestHammaddeGirdiSertlikSpec();

	private TestHammaddeGirdiBkm1Ph selectedBkm1PhSonuc = new TestHammaddeGirdiBkm1Ph();
	private TestHammaddeGirdiBkm1Ph bkm1Ph = new TestHammaddeGirdiBkm1Ph();
	private TestHammaddeGirdiBkm1KatiMadde selectedBkm1KatiMaddeSonuc = new TestHammaddeGirdiBkm1KatiMadde();
	private TestHammaddeGirdiBkm1KatiMadde bkm1katiMadde = new TestHammaddeGirdiBkm1KatiMadde();
	private TestHammaddeGirdiBkm1KuruMadde selectedBkm1KuruMaddeSonuc = new TestHammaddeGirdiBkm1KuruMadde();
	private TestHammaddeGirdiBkm1KuruMadde bkm1KuruMadde = new TestHammaddeGirdiBkm1KuruMadde();

	private TestHammaddeGirdiSertlik selectedGirdiSertlikSonuc = new TestHammaddeGirdiSertlik();
	private TestHammaddeGirdiSertlik sertlik = new TestHammaddeGirdiSertlik();

	private TestHammaddeGirdiAmg1 selectedAmg1Sonuc = new TestHammaddeGirdiAmg1();
	private TestHammaddeGirdiAmg1 amg1 = new TestHammaddeGirdiAmg1();
	private TestHammaddeGirdiAmg1Spec selectedAmg1SpecSonuc = new TestHammaddeGirdiAmg1Spec();
	private TestHammaddeGirdiAmg1Spec amg1Spec = new TestHammaddeGirdiAmg1Spec();

	public CoatingMaterialBean() {
		historyCoatMaterial = new CoatRawMaterial();
		newCoatMaterial = new CoatRawMaterial();
		newType = new CoatMaterialType();
		currentType = new CoatMaterialType();
		availableCoatRawMaterial = new ArrayList<CoatRawMaterial>();
		currentCoatMaterial = new CoatRawMaterial();
		coatRawMaterialList = new ArrayList<CoatRawMaterial>();
		CoatMaterialTypeManager materialTypeManager = new CoatMaterialTypeManager();
		coatMaterialTypeList = materialTypeManager
				.findAll(CoatMaterialType.class);
	}

	public void goToCoatingMaterialPage() {
		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect(config.getPageUrl().COATING_MATERIALS_PAGE);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void loadAvailableCoatMaterials(CoatMaterialType coatMaterialType) {
		currentCoatMaterial = new CoatRawMaterial();
		currentType = coatMaterialType;
		CoatRawMaterialManager rawMaterialManager = new CoatRawMaterialManager();
		availableCoatRawMaterial = (List<CoatRawMaterial>) rawMaterialManager
				.findUnfinishedCoatMaterialByType(coatMaterialType
						.getCoatMaterialTypeId());
	}

	public void updateCoatMaterialType() {
		System.out.println("CoatingMaterialBean.updateCoatMaterialType()");
		CoatMaterialTypeManager typeManager = new CoatMaterialTypeManager();
		typeManager.updateEntity(currentType);
		CoatMaterialTypeManager materialTypeManager = new CoatMaterialTypeManager();
		coatMaterialTypeList = materialTypeManager
				.findAll(CoatMaterialType.class);
		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}

	public void deleteCoatMaterialType() {
		System.out.println("CoatingMaterialBean.deleteCoatMaterialType()");
		CoatMaterialTypeManager typeManager = new CoatMaterialTypeManager();
		List<CoatMaterialType> typeList = (List<CoatMaterialType>) typeManager
				.findByField(CoatMaterialType.class, "coatMaterialTypeId",
						currentType.getCoatMaterialTypeId());
		if (typeList.size() == 0) {
			FacesContextUtils.addErrorMessage("NoExistingItemMessage");
			return;
		}
		CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
		List<CoatRawMaterial> materialList = (List<CoatRawMaterial>) materialManager
				.findByField(CoatRawMaterial.class, "coatMaterialTypeId",
						currentType.getCoatMaterialTypeId());
		if (materialList.size() > 0) {
			FacesContextUtils.addErrorMessage("NoExistingItemMessage");
			return;
		}
		typeManager.delete(currentType);
		CoatMaterialTypeManager materialTypeManager = new CoatMaterialTypeManager();
		coatMaterialTypeList = materialTypeManager
				.findAll(CoatMaterialType.class);
		currentType = new CoatMaterialType();
		availableCoatRawMaterial = new ArrayList<CoatRawMaterial>();
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	public void addCoatMaterialType() {
		System.out.println("CoatingMaterialBean.addCoatMaterialType()");
		CoatMaterialTypeManager typeManager = new CoatMaterialTypeManager();
		typeManager.enterNew(newType);
		FacesContextUtils.addInfoMessage("SubmitMessage");
		coatMaterialTypeList.add(newType);
		newType = new CoatMaterialType();
	}

	public void addTypeItem() {
		System.out.println("CoatingMaterialBean.addTypeItem()");
		if (newCoatMaterial.getMiktari() <= 0) {
			FacesContextUtils.addErrorMessage("NegativeAmountMessage");
			return;
		}
		CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
		newCoatMaterial.setCoatMaterialType(currentType);
		newCoatMaterial.setRemainingAmount(newCoatMaterial.getMiktari());
		materialManager.enterNew(newCoatMaterial);
		availableCoatRawMaterial.add(newCoatMaterial);
		FacesContextUtils.addInfoMessage("SubmitMessage");
		newCoatMaterial = new CoatRawMaterial();
	}

	public void editTypeItem() {
		System.out.println("CoatingMaterialBean.editTypeItem()");
		if (currentCoatMaterial.getMiktari() <= 0) {
			FacesContextUtils.addErrorMessage("NegativeAmountMessage");
			return;
		}
		PipeCoatLayerManager layerManager = new PipeCoatLayerManager();
		List<PipeCoatLayer> layerList = (List<PipeCoatLayer>) layerManager
				.findByField(PipeCoatLayer.class, "coatMaterialId",
						currentCoatMaterial.getCoatMaterialId());
		if (layerList.size() > 0) {
			FacesContextUtils.addErrorMessage("CoatPipeRelatedMessage");
			return;
		}
		CoatRawMaterialManager coatMaterialManager = new CoatRawMaterialManager();
		coatMaterialManager.updateEntity(currentCoatMaterial);
		availableCoatRawMaterial = (List<CoatRawMaterial>) coatMaterialManager
				.findUnfinishedCoatMaterialByType(currentType
						.getCoatMaterialTypeId());
		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}

	public void deleteTypeItem() {
		System.out.println("CoatingMaterialBean.deleteTypeItem()");
		if (currentCoatMaterial.getCoatMaterialId() == null) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		PipeCoatLayerManager layerManager = new PipeCoatLayerManager();
		List<PipeCoatLayer> layerList = (List<PipeCoatLayer>) layerManager
				.findByField(PipeCoatLayer.class, "coatMaterialId",
						currentCoatMaterial.getCoatMaterialId());
		if (layerList.size() > 0) {
			FacesContextUtils.addErrorMessage("PipeCoatRelatedMessage");
			return;
		}
		CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
		materialManager.delete(currentCoatMaterial);
		availableCoatRawMaterial.remove(currentCoatMaterial);
		currentCoatMaterial = new CoatRawMaterial();
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	@SuppressWarnings("unchecked")
	public void loadCurrentYearItems(CoatMaterialType coatMaterialType) {
		System.out.println("CoatingMaterialBean.loadSelectedYearItems()");
		currentType = coatMaterialType;
		CoatingMaterialListBean.generateYearList(coatMaterialType);
		CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
		TimeSegment timeSegment = new TimeSegment(new Date());
		coatRawMaterialList = (List<CoatRawMaterial>) materialManager
				.findTypeBetweenTwoValues(CoatRawMaterial.class,
						"entranceDate", timeSegment.getStartDate(),
						timeSegment.getEndDate(),
						coatMaterialType.getCoatMaterialTypeId());
	}

	@SuppressWarnings("unchecked")
	public void loadSelectedYearItems(AjaxBehaviorEvent event) {
		System.out.println("CoatingMaterialBean.loadSelectedYearItems()");
		HtmlSelectOneMenu one = (HtmlSelectOneMenu) event.getSource();
		Integer year = Integer.valueOf(one.getValue().toString());
		TimeSegment timeSegment = new TimeSegment(year);
		CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
		coatRawMaterialList = (List<CoatRawMaterial>) materialManager
				.findTypeBetweenTwoValues(CoatRawMaterial.class,
						"entranceDate", timeSegment.getStartDate(),
						timeSegment.getEndDate(),
						currentType.getCoatMaterialTypeId());
	}

	public void loadSelectedEpoksiToz() {
		reset();
		loadSelectedEpoksiTozBoyaTermalAnalizTesti();
		loadSelectedEpoksiTozBoyaNemIcerigiTesti();
		loadSelectedEpoksiTozBoyaJelSuresiTesti();
		loadSelectedEpoksiTozBoyaKurZamaniBelirlemeTesti();
		loadSelectedEpoksiTozBoyaElekAnalizTesti();
		loadSelectedEpoksiTozBoyaYogunlukTesti();
	}

	public void loadSelectedEpoksiYas() {
		reset();
	}

	public void loadSelectedYapistirici() {
		reset();
		loadSelectedHammaddeMfrTesti();
		loadSelectedPlastiklerinYogunlugununBelirlenmesiTesti();
		loadSelectedYumusamaSicakligiTesti();
		loadSelectedKirilganlikTesti();
		loadSelectedPlastiklerinYogunlugununBelirlenmesiTesti();
	}

	public void loadSelectedPolietilen() {
		reset();
		loadSelectedHammaddeMfrTesti();
		loadSelectedPlastiklerinYogunlugununBelirlenmesiTesti();
		loadSelectedOksidasyonInduksiyonZamaniTesti();
		loadSelectedYumusamaSicakligiTesti();
		loadSelectedKirilganlikTesti();
		loadSelectedCevreselBaskiKirilganlikTesti();
		loadSelectedKarbonSiyahiIcerigiTesti();
		loadSelectedCekmeGerilmeKopmaUzamaTesti();
		loadSelectedSertlikTesti();
	}

	public void loadSelectedBeton() {
		loadSelectedPhTesti();
		loadSelectedKuruMaddeTesti();
		loadSelectedKatiMaddeTesti();
		reset();
	}

	public void loadSelectedKumlama() {
		loadSelectedAsindiriciMalzemeGirisTesti();
		reset();
	}

	public void loadSelectedAsidik() {
		reset();
	}

	public void reset() {

		selectedEta1Sonuc = new TestHammaddeGirdiEpoksiTozEta1Sonuc();
		selectedEjs1Sonuc = new TestHammaddeGirdiEpoksiTozEjs1();
		selectedEni1Sonuc = new TestHammaddeGirdiEpoksiTozEni1();
		selectedEea1Sonuc = new TestHammaddeGirdiEpoksiTozEea1();
		selectedEkz1Sonuc = new TestHammaddeGirdiEpoksiTozEkz1();
		selectedEty1Sonuc = new TestHammaddeGirdiEpoksiTozEty1();
		selectedAmg1Sonuc = new TestHammaddeGirdiAmg1();

		selectedMfr1Sonuc = new TestHammaddeGirdiMfr1();
		selectedPdd1Sonuc = new TestHammaddeGirdiPdd1();
		selectedOit1Sonuc = new TestHammaddeGirdiOit1();

		selectedUreticiMfr1Sonuc = new TestHammaddeUreticiMfr1();
		selectedUreticiPdd1Sonuc = new TestHammaddeUreticiPdd1();
		selectedUreticiOit1Sonuc = new TestHammaddeUreticiOit1();
		selectedUreticiYst1Sonuc = new TestHammaddeUreticiYst1();
		selectedUreticiKst1Sonuc = new TestHammaddeUreticiKst1();
		selectedUreticiPkd1Sonuc = new TestHammaddeUreticiPkd1();
		selectedUreticiPks1Sonuc = new TestHammaddeUreticiPks1();
		selectedUreticiPku1Sonuc = new TestHammaddeUreticiPku1();

		selectedMfr1SpecSonuc = new TestHammaddeGirdiMfr1Spec();
		selectedPdd1SpecSonuc = new TestHammaddeGirdiPdd1Spec();
		selectedAmg1SpecSonuc = new TestHammaddeGirdiAmg1Spec();

		selectedBkm1PhSonuc = new TestHammaddeGirdiBkm1Ph();
		selectedBkm1KatiMaddeSonuc = new TestHammaddeGirdiBkm1KatiMadde();
		selectedBkm1KuruMaddeSonuc = new TestHammaddeGirdiBkm1KuruMadde();

		selectedGirdiSertlikSonuc = new TestHammaddeGirdiSertlik();
	}

	public void loadReplySelectedMaterial() {
		CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
		currentCoatMaterial = materialManager.loadObject(CoatRawMaterial.class,
				currentCoatMaterial.getCoatMaterialId());
	}

	private void loadSelectedEpoksiTozBoyaTermalAnalizTesti() {
		loadReplySelectedMaterial();
		List<TestHammaddeGirdiEpoksiTozEta1Sonuc> testHammaddeGirdiEpoksiTozEta1Sonucs = currentCoatMaterial
				.getTestHammaddeGirdiEpoksiTozEta1Sonucs();
		if (testHammaddeGirdiEpoksiTozEta1Sonucs != null
				&& testHammaddeGirdiEpoksiTozEta1Sonucs.size() > 0) {
			eta1 = currentCoatMaterial
					.getTestHammaddeGirdiEpoksiTozEta1Sonucs().get(0);
		} else {
			eta1 = new TestHammaddeGirdiEpoksiTozEta1Sonuc();
		}
	}

	private void loadSelectedEpoksiTozBoyaNemIcerigiTesti() {
		loadReplySelectedMaterial();
		List<TestHammaddeGirdiEpoksiTozEni1> testHammaddeGirdiEpoksiTozEni1s = currentCoatMaterial
				.getTestHammaddeGirdiEpoksiTozEni1s();
		if (testHammaddeGirdiEpoksiTozEni1s != null
				&& testHammaddeGirdiEpoksiTozEni1s.size() > 0) {
			eni1 = currentCoatMaterial.getTestHammaddeGirdiEpoksiTozEni1s()
					.get(0);
		} else {
			eni1 = new TestHammaddeGirdiEpoksiTozEni1();
		}
	}

	private void loadSelectedEpoksiTozBoyaJelSuresiTesti() {
		loadReplySelectedMaterial();
		List<TestHammaddeGirdiEpoksiTozEjs1> testHammaddeGirdiEpoksiTozEjs1s = currentCoatMaterial
				.getTestHammaddeGirdiEpoksiTozEjs1s();
		if (testHammaddeGirdiEpoksiTozEjs1s != null
				&& testHammaddeGirdiEpoksiTozEjs1s.size() > 0) {
			ejs1 = currentCoatMaterial.getTestHammaddeGirdiEpoksiTozEjs1s()
					.get(0);
		} else {
			ejs1 = new TestHammaddeGirdiEpoksiTozEjs1();
		}
	}

	private void loadSelectedEpoksiTozBoyaElekAnalizTesti() {
		loadReplySelectedMaterial();
		List<TestHammaddeGirdiEpoksiTozEea1> testHammaddeGirdiEpoksiTozEea1s = currentCoatMaterial
				.getTestHammaddeGirdiEpoksiTozEea1s();
		if (testHammaddeGirdiEpoksiTozEea1s != null
				&& testHammaddeGirdiEpoksiTozEea1s.size() > 0) {
			eea1 = currentCoatMaterial.getTestHammaddeGirdiEpoksiTozEea1s()
					.get(0);
		} else {
			eea1 = new TestHammaddeGirdiEpoksiTozEea1();
		}
	}

	private void loadSelectedEpoksiTozBoyaKurZamaniBelirlemeTesti() {
		loadReplySelectedMaterial();
		List<TestHammaddeGirdiEpoksiTozEkz1> testHammaddeGirdiEpoksiTozEkz1s = currentCoatMaterial
				.getTestHammaddeGirdiEpoksiTozEkz1s();
		if (testHammaddeGirdiEpoksiTozEkz1s != null
				&& testHammaddeGirdiEpoksiTozEkz1s.size() > 0) {
			ekz1 = currentCoatMaterial.getTestHammaddeGirdiEpoksiTozEkz1s()
					.get(0);
		} else {
			ekz1 = new TestHammaddeGirdiEpoksiTozEkz1();
		}
	}

	private void loadSelectedEpoksiTozBoyaYogunlukTesti() {
		loadReplySelectedMaterial();
		List<TestHammaddeGirdiEpoksiTozEty1> testHammaddeGirdiEpoksiTozEty1s = currentCoatMaterial
				.getTestHammaddeGirdiEpoksiTozEty1s();
		if (testHammaddeGirdiEpoksiTozEty1s != null
				&& testHammaddeGirdiEpoksiTozEty1s.size() > 0) {
			ety1 = currentCoatMaterial.getTestHammaddeGirdiEpoksiTozEty1s()
					.get(0);
		} else {
			ety1 = new TestHammaddeGirdiEpoksiTozEty1();
		}
	}

	private void loadSelectedHammaddeMfrTesti() {
		loadReplySelectedMaterial();
		List<TestHammaddeGirdiMfr1> testHammaddeGirdiMfr1Sonucs = currentCoatMaterial
				.getTestHammaddeGirdiMfr1s();
		if (testHammaddeGirdiMfr1Sonucs != null
				&& testHammaddeGirdiMfr1Sonucs.size() > 0) {
			mfr1 = currentCoatMaterial.getTestHammaddeGirdiMfr1s().get(0);
		} else {
			mfr1 = new TestHammaddeGirdiMfr1();
		}

		List<TestHammaddeUreticiMfr1> testHammaddeGirdiMfr1UreticiSonucs = currentCoatMaterial
				.getTestHammaddeUreticiMfr1s();
		if (testHammaddeGirdiMfr1UreticiSonucs != null
				&& testHammaddeGirdiMfr1UreticiSonucs.size() > 0) {
			mfr1Uretici = currentCoatMaterial.getTestHammaddeUreticiMfr1s()
					.get(0);
		} else {
			mfr1Uretici = new TestHammaddeUreticiMfr1();
		}

		List<TestHammaddeGirdiMfr1Spec> testHammaddeGirdiMfr1Specs = currentCoatMaterial
				.getTestHammaddeGirdiMfr1Specs();
		if (testHammaddeGirdiMfr1Specs != null
				&& testHammaddeGirdiMfr1Specs.size() > 0) {
			mfr1Spec = currentCoatMaterial.getTestHammaddeGirdiMfr1Specs().get(
					0);
		} else {
			mfr1Spec = new TestHammaddeGirdiMfr1Spec();
		}
	}

	private void loadSelectedOksidasyonInduksiyonZamaniTesti() {
		loadReplySelectedMaterial();
		List<TestHammaddeGirdiOit1> testHammaddeGirdiOit1Sonucs = currentCoatMaterial
				.getTestHammaddeGirdiOit1s();
		if (testHammaddeGirdiOit1Sonucs != null
				&& testHammaddeGirdiOit1Sonucs.size() > 0) {
			oit1 = currentCoatMaterial.getTestHammaddeGirdiOit1s().get(0);
		} else {
			oit1 = new TestHammaddeGirdiOit1();
		}

		List<TestHammaddeUreticiOit1> testHammaddeGirdiOit1UreticiSonucs = currentCoatMaterial
				.getTestHammaddeUreticiOit1s();
		if (testHammaddeGirdiOit1UreticiSonucs != null
				&& testHammaddeGirdiOit1UreticiSonucs.size() > 0) {
			oit1Uretici = currentCoatMaterial.getTestHammaddeUreticiOit1s()
					.get(0);
		} else {
			oit1Uretici = new TestHammaddeUreticiOit1();
		}
	}

	private void loadSelectedPlastiklerinYogunlugununBelirlenmesiTesti() {
		loadReplySelectedMaterial();
		List<TestHammaddeGirdiPdd1> testHammaddeGirdiPdd1Sonucs = currentCoatMaterial
				.getTestHammaddeGirdiPdd1s();
		if (testHammaddeGirdiPdd1Sonucs != null
				&& testHammaddeGirdiPdd1Sonucs.size() > 0) {
			pdd1 = currentCoatMaterial.getTestHammaddeGirdiPdd1s().get(0);
		} else {
			pdd1 = new TestHammaddeGirdiPdd1();
		}

		List<TestHammaddeUreticiPdd1> testHammaddeGirdiPdd1UreticiSonucs = currentCoatMaterial
				.getTestHammaddeUreticiPdd1s();
		if (testHammaddeGirdiPdd1UreticiSonucs != null
				&& testHammaddeGirdiPdd1UreticiSonucs.size() > 0) {
			pdd1Uretici = currentCoatMaterial.getTestHammaddeUreticiPdd1s()
					.get(0);
		} else {
			pdd1Uretici = new TestHammaddeUreticiPdd1();
		}

		List<TestHammaddeGirdiPdd1Spec> testHammaddeGirdiPdd1Specs = currentCoatMaterial
				.getTestHammaddeGirdiPdd1Specs();
		if (testHammaddeGirdiPdd1Specs != null
				&& testHammaddeGirdiPdd1Specs.size() > 0) {
			pdd1Spec = currentCoatMaterial.getTestHammaddeGirdiPdd1Specs().get(
					0);
		} else {
			pdd1Spec = new TestHammaddeGirdiPdd1Spec();
		}
	}

	private void loadSelectedKirilganlikTesti() {
		loadReplySelectedMaterial();
		List<TestHammaddeUreticiKst1> testHammaddeUreticiKst1Sonucs = currentCoatMaterial
				.getTestHammaddeUreticiKst1s();
		if (testHammaddeUreticiKst1Sonucs != null
				&& testHammaddeUreticiKst1Sonucs.size() > 0) {
			kst1Uretici = currentCoatMaterial.getTestHammaddeUreticiKst1s()
					.get(0);
		} else {
			kst1Uretici = new TestHammaddeUreticiKst1();
		}
	}

	private void loadSelectedYumusamaSicakligiTesti() {
		loadReplySelectedMaterial();
		List<TestHammaddeUreticiYst1> testHammaddeUreticiYst1Sonucs = currentCoatMaterial
				.getTestHammaddeUreticiYst1s();
		if (testHammaddeUreticiYst1Sonucs != null
				&& testHammaddeUreticiYst1Sonucs.size() > 0) {
			yst1Uretici = currentCoatMaterial.getTestHammaddeUreticiYst1s()
					.get(0);
		} else {
			yst1Uretici = new TestHammaddeUreticiYst1();
		}
	}

	private void loadSelectedCekmeGerilmeKopmaUzamaTesti() {
		loadReplySelectedMaterial();
		List<TestHammaddeUreticiPkd1> testHammaddeUreticiPkd1Sonucs = currentCoatMaterial
				.getTestHammaddeUreticiPkd1s();
		if (testHammaddeUreticiPkd1Sonucs != null
				&& testHammaddeUreticiPkd1Sonucs.size() > 0) {
			pkd1Uretici = currentCoatMaterial.getTestHammaddeUreticiPkd1s()
					.get(0);
		} else {
			pkd1Uretici = new TestHammaddeUreticiPkd1();
		}
	}

	private void loadSelectedKarbonSiyahiIcerigiTesti() {
		loadReplySelectedMaterial();
		List<TestHammaddeUreticiPks1> testHammaddeUreticiPks1Sonucs = currentCoatMaterial
				.getTestHammaddeUreticiPks1s();
		if (testHammaddeUreticiPks1Sonucs != null
				&& testHammaddeUreticiPks1Sonucs.size() > 0) {
			pks1Uretici = currentCoatMaterial.getTestHammaddeUreticiPks1s()
					.get(0);
		} else {
			pks1Uretici = new TestHammaddeUreticiPks1();
		}
	}

	private void loadSelectedCevreselBaskiKirilganlikTesti() {
		loadReplySelectedMaterial();
		List<TestHammaddeUreticiPku1> testHammaddeUreticiPku1Sonucs = currentCoatMaterial
				.getTestHammaddeUreticiPku1s();
		if (testHammaddeUreticiPku1Sonucs != null
				&& testHammaddeUreticiPku1Sonucs.size() > 0) {
			pku1Uretici = currentCoatMaterial.getTestHammaddeUreticiPku1s()
					.get(0);
		} else {
			pku1Uretici = new TestHammaddeUreticiPku1();
		}
	}

	private void loadSelectedPhTesti() {
		loadReplySelectedMaterial();
		List<TestHammaddeGirdiBkm1Ph> testHammaddeUreticiBkm1PhSonucs = currentCoatMaterial
				.getTestHammaddeGirdiBkm1Phs();
		if (testHammaddeUreticiBkm1PhSonucs != null
				&& testHammaddeUreticiBkm1PhSonucs.size() > 0) {
			bkm1Ph = currentCoatMaterial.getTestHammaddeGirdiBkm1Phs().get(0);
		} else {
			bkm1Ph = new TestHammaddeGirdiBkm1Ph();
		}
	}

	private void loadSelectedKatiMaddeTesti() {
		loadReplySelectedMaterial();
		List<TestHammaddeGirdiBkm1KatiMadde> testHammaddeUreticiBkm1KatiMaddeSonucs = currentCoatMaterial
				.getTestHammaddeGirdiBkm1KatiMaddes();
		if (testHammaddeUreticiBkm1KatiMaddeSonucs != null
				&& testHammaddeUreticiBkm1KatiMaddeSonucs.size() > 0) {
			bkm1katiMadde = currentCoatMaterial
					.getTestHammaddeGirdiBkm1KatiMaddes().get(0);
		} else {
			bkm1katiMadde = new TestHammaddeGirdiBkm1KatiMadde();
		}
	}

	private void loadSelectedKuruMaddeTesti() {
		loadReplySelectedMaterial();
		List<TestHammaddeGirdiBkm1KuruMadde> testHammaddeUreticiBkm1KuruMaddeSonucs = currentCoatMaterial
				.getTestHammaddeGirdiBkm1KuruMaddes();
		if (testHammaddeUreticiBkm1KuruMaddeSonucs != null
				&& testHammaddeUreticiBkm1KuruMaddeSonucs.size() > 0) {
			bkm1KuruMadde = currentCoatMaterial
					.getTestHammaddeGirdiBkm1KuruMaddes().get(0);
		} else {
			bkm1KuruMadde = new TestHammaddeGirdiBkm1KuruMadde();
		}
	}

	private void loadSelectedSertlikTesti() {
		loadReplySelectedMaterial();
		List<TestHammaddeGirdiSertlik> testHammaddeGirdiSertliks = currentCoatMaterial
				.getTestHammaddeGirdiSertliks();
		if (testHammaddeGirdiSertliks != null
				&& testHammaddeGirdiSertliks.size() > 0) {
			sertlik = currentCoatMaterial.getTestHammaddeGirdiSertliks().get(0);
		} else {
			sertlik = new TestHammaddeGirdiSertlik();
		}

		List<TestHammaddeGirdiSertlikSpec> testHammaddeGirdiSertlikSpecs = currentCoatMaterial
				.getTestHammaddeGirdiSertlikSpecs();
		if (testHammaddeGirdiSertlikSpecs != null
				&& testHammaddeGirdiSertlikSpecs.size() > 0) {
			sertlikSpec = currentCoatMaterial
					.getTestHammaddeGirdiSertlikSpecs().get(0);
		} else {
			sertlikSpec = new TestHammaddeGirdiSertlikSpec();
		}
	}

	private void loadSelectedAsindiriciMalzemeGirisTesti() {
		loadReplySelectedMaterial();
		List<TestHammaddeGirdiAmg1> testHammaddeGirdiAmg1Sonucs = currentCoatMaterial
				.getTestHammaddeGirdiAmg1s();
		if (testHammaddeGirdiAmg1Sonucs != null
				&& testHammaddeGirdiAmg1Sonucs.size() > 0) {
			amg1 = currentCoatMaterial.getTestHammaddeGirdiAmg1s().get(0);
		} else {
			amg1 = new TestHammaddeGirdiAmg1();
		}

		List<TestHammaddeGirdiMfr1Spec> testHammaddeGirdiMfr1Specs = currentCoatMaterial
				.getTestHammaddeGirdiMfr1Specs();
		if (testHammaddeGirdiMfr1Specs != null
				&& testHammaddeGirdiMfr1Specs.size() > 0) {
			amg1Spec = currentCoatMaterial.getTestHammaddeGirdiAmg1Specs().get(
					0);
		} else {
			amg1Spec = new TestHammaddeGirdiAmg1Spec();
		}
	}

	public void saveSelectedEta1Sonuc() {

		TestHammaddeGirdiEpoksiTozEta1SonucManager eta1Manager = new TestHammaddeGirdiEpoksiTozEta1SonucManager();

		if (selectedEta1Sonuc.getId() == null) {

			selectedEta1Sonuc.setEklemeZamani(UtilInsCore.getTarihZaman());
			selectedEta1Sonuc.setEkleyenKullanici(userBean.getUser().getId());
			selectedEta1Sonuc.setCoatRawMaterial(currentCoatMaterial);

			eta1Manager.enterNew(selectedEta1Sonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedEta1Sonuc.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			selectedEta1Sonuc.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			eta1Manager.updateEntity(selectedEta1Sonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedEpoksiTozBoyaTermalAnalizTesti();
		reset();
	}

	public void deleteSelectedEta1Sonuc() {

		TestHammaddeGirdiEpoksiTozEta1SonucManager eta1Manager = new TestHammaddeGirdiEpoksiTozEta1SonucManager();

		if (selectedEta1Sonuc == null || selectedEta1Sonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		eta1Manager.delete(selectedEta1Sonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedEni1Sonuc() {

		TestHammaddeGirdiEpoksiTozEni1Manager eni1Manager = new TestHammaddeGirdiEpoksiTozEni1Manager();

		if (selectedEni1Sonuc.getId() == null) {

			selectedEni1Sonuc.setEklemeZamani(UtilInsCore.getTarihZaman());
			selectedEni1Sonuc.setEkleyenKullanici(userBean.getUser().getId());
			selectedEni1Sonuc.setCoatRawMaterial(currentCoatMaterial);

			eni1Manager.enterNew(selectedEni1Sonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedEni1Sonuc.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			selectedEni1Sonuc.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			eni1Manager.updateEntity(selectedEni1Sonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedEpoksiTozBoyaNemIcerigiTesti();
		reset();
	}

	public void deleteSelectedEni1Sonuc() {

		TestHammaddeGirdiEpoksiTozEni1Manager eni1Manager = new TestHammaddeGirdiEpoksiTozEni1Manager();

		if (selectedEni1Sonuc == null || selectedEni1Sonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		eni1Manager.delete(selectedEni1Sonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedEjs1Sonuc() {

		TestHammaddeGirdiEpoksiTozEjs1Manager ejs1Manager = new TestHammaddeGirdiEpoksiTozEjs1Manager();

		if (selectedEjs1Sonuc.getId() == null) {

			selectedEjs1Sonuc.setEklemeZamani(UtilInsCore.getTarihZaman());
			selectedEjs1Sonuc.setEkleyenKullanici(userBean.getUser().getId());
			selectedEjs1Sonuc.setCoatRawMaterial(currentCoatMaterial);

			ejs1Manager.enterNew(selectedEjs1Sonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedEjs1Sonuc.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			selectedEjs1Sonuc.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			ejs1Manager.updateEntity(selectedEjs1Sonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedEpoksiTozBoyaNemIcerigiTesti();
		reset();
	}

	public void deleteSelectedEjs1Sonuc() {

		TestHammaddeGirdiEpoksiTozEjs1Manager ejs1Manager = new TestHammaddeGirdiEpoksiTozEjs1Manager();

		if (selectedEjs1Sonuc == null || selectedEjs1Sonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		ejs1Manager.delete(selectedEjs1Sonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedEea1Sonuc() {

		TestHammaddeGirdiEpoksiTozEea1Manager eea1Manager = new TestHammaddeGirdiEpoksiTozEea1Manager();

		if (selectedEea1Sonuc.getId() == null) {

			selectedEea1Sonuc.setEklemeZamani(UtilInsCore.getTarihZaman());
			selectedEea1Sonuc.setEkleyenKullanici(userBean.getUser().getId());
			selectedEea1Sonuc.setCoatRawMaterial(currentCoatMaterial);

			eea1Manager.enterNew(selectedEea1Sonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedEea1Sonuc.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			selectedEea1Sonuc.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			eea1Manager.updateEntity(selectedEea1Sonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedEpoksiTozBoyaElekAnalizTesti();
		reset();
	}

	public void deleteSelectedEea1Sonuc() {

		TestHammaddeGirdiEpoksiTozEea1Manager eea1Manager = new TestHammaddeGirdiEpoksiTozEea1Manager();

		if (selectedEea1Sonuc == null || selectedEea1Sonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		eea1Manager.delete(selectedEea1Sonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedEkz1Sonuc() {

		TestHammaddeGirdiEpoksiTozEkz1Manager ekz1Manager = new TestHammaddeGirdiEpoksiTozEkz1Manager();

		if (selectedEkz1Sonuc.getId() == null) {

			selectedEkz1Sonuc.setEklemeZamani(UtilInsCore.getTarihZaman());
			selectedEkz1Sonuc.setEkleyenKullanici(userBean.getUser().getId());
			selectedEkz1Sonuc.setCoatRawMaterial(currentCoatMaterial);

			ekz1Manager.enterNew(selectedEkz1Sonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedEkz1Sonuc.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			selectedEkz1Sonuc.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			ekz1Manager.updateEntity(selectedEkz1Sonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedEpoksiTozBoyaKurZamaniBelirlemeTesti();
		reset();
	}

	public void deleteSelectedEkz1Sonuc() {

		TestHammaddeGirdiEpoksiTozEkz1Manager ekz1Manager = new TestHammaddeGirdiEpoksiTozEkz1Manager();

		if (selectedEkz1Sonuc == null || selectedEkz1Sonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		ekz1Manager.delete(selectedEkz1Sonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedMfr1Sonuc() {

		TestHammaddeGirdiMfr1Manager mfr1Manager = new TestHammaddeGirdiMfr1Manager();

		if (selectedMfr1Sonuc.getId() == null) {

			selectedMfr1Sonuc.setEklemeZamani(UtilInsCore.getTarihZaman());
			selectedMfr1Sonuc.setEkleyenKullanici(userBean.getUser().getId());
			selectedMfr1Sonuc.setCoatRawMaterial(currentCoatMaterial);

			mfr1Manager.enterNew(selectedMfr1Sonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedMfr1Sonuc.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			selectedMfr1Sonuc.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			mfr1Manager.updateEntity(selectedMfr1Sonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedHammaddeMfrTesti();
		reset();
	}

	public void deleteSelectedMfr1Sonuc() {

		TestHammaddeGirdiMfr1Manager mfr1Manager = new TestHammaddeGirdiMfr1Manager();

		if (selectedMfr1Sonuc == null || selectedMfr1Sonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		mfr1Manager.delete(selectedMfr1Sonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedMfr1Spec() {

		TestHammaddeGirdiMfr1SpecManager mfr1SpecManager = new TestHammaddeGirdiMfr1SpecManager();

		if (selectedMfr1SpecSonuc.getId() == null) {

			selectedMfr1SpecSonuc.setEklemeZamani(UtilInsCore.getTarihZaman());
			selectedMfr1SpecSonuc.setEkleyenKullanici(userBean.getUser()
					.getId());
			selectedMfr1SpecSonuc.setCoatRawMaterial(currentCoatMaterial);

			mfr1SpecManager.enterNew(selectedMfr1SpecSonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedMfr1SpecSonuc.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			selectedMfr1SpecSonuc.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			mfr1SpecManager.updateEntity(selectedMfr1SpecSonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedHammaddeMfrTesti();
		reset();
	}

	public void deleteSelectedMfr1Spec() {

		TestHammaddeGirdiMfr1SpecManager mfr1SpecManager = new TestHammaddeGirdiMfr1SpecManager();

		if (selectedMfr1SpecSonuc == null || selectedMfr1SpecSonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		mfr1SpecManager.delete(selectedMfr1SpecSonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedUreticiMfr1Sonuc() {

		TestHammaddeUreticiMfr1Manager mfr1UreticiManager = new TestHammaddeUreticiMfr1Manager();

		if (selectedUreticiMfr1Sonuc.getId() == null) {

			selectedUreticiMfr1Sonuc.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			selectedUreticiMfr1Sonuc.setEkleyenKullanici(userBean.getUser()
					.getId());
			selectedUreticiMfr1Sonuc.setCoatRawMaterial(currentCoatMaterial);

			mfr1UreticiManager.enterNew(selectedUreticiMfr1Sonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedUreticiMfr1Sonuc
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			selectedUreticiMfr1Sonuc.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			mfr1UreticiManager.updateEntity(selectedUreticiMfr1Sonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedHammaddeMfrTesti();
		reset();
	}

	public void deleteSelectedUreticiMfr1Sonuc() {

		TestHammaddeUreticiMfr1Manager mfr1UreticiManager = new TestHammaddeUreticiMfr1Manager();

		if (selectedUreticiMfr1Sonuc == null
				|| selectedUreticiMfr1Sonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		mfr1UreticiManager.delete(selectedUreticiMfr1Sonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedOit1Sonuc() {

		TestHammaddeGirdiOit1Manager oit1Manager = new TestHammaddeGirdiOit1Manager();

		if (selectedOit1Sonuc.getId() == null) {

			selectedOit1Sonuc.setEklemeZamani(UtilInsCore.getTarihZaman());
			selectedOit1Sonuc.setEkleyenKullanici(userBean.getUser().getId());
			selectedOit1Sonuc.setCoatRawMaterial(currentCoatMaterial);

			oit1Manager.enterNew(selectedOit1Sonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedOit1Sonuc.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			selectedOit1Sonuc.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			oit1Manager.updateEntity(selectedOit1Sonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedOksidasyonInduksiyonZamaniTesti();
		reset();
	}

	public void deleteSelectedOit1Sonuc() {

		TestHammaddeGirdiOit1Manager oit1Manager = new TestHammaddeGirdiOit1Manager();

		if (selectedOit1Sonuc == null || selectedOit1Sonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		oit1Manager.delete(selectedOit1Sonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedUreticiOit1Sonuc() {

		TestHammaddeUreticiOit1Manager oit1UreticiManager = new TestHammaddeUreticiOit1Manager();

		if (selectedUreticiOit1Sonuc.getId() == null) {

			selectedUreticiOit1Sonuc.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			selectedUreticiOit1Sonuc.setEkleyenKullanici(userBean.getUser()
					.getId());
			selectedUreticiOit1Sonuc.setCoatRawMaterial(currentCoatMaterial);

			oit1UreticiManager.enterNew(selectedUreticiOit1Sonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedUreticiOit1Sonuc
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			selectedUreticiOit1Sonuc.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			oit1UreticiManager.updateEntity(selectedUreticiOit1Sonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedOksidasyonInduksiyonZamaniTesti();
		reset();
	}

	public void deleteSelectedUreticiOit1Sonuc() {

		TestHammaddeUreticiOit1Manager oit1UreticiManager = new TestHammaddeUreticiOit1Manager();

		if (selectedUreticiOit1Sonuc == null
				|| selectedUreticiOit1Sonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		oit1UreticiManager.delete(selectedUreticiOit1Sonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedPdd1Sonuc() {

		TestHammaddeGirdiPdd1Manager pdd1Manager = new TestHammaddeGirdiPdd1Manager();

		if (selectedPdd1Sonuc.getId() == null) {

			selectedPdd1Sonuc.setEklemeZamani(UtilInsCore.getTarihZaman());
			selectedPdd1Sonuc.setEkleyenKullanici(userBean.getUser().getId());
			selectedPdd1Sonuc.setCoatRawMaterial(currentCoatMaterial);

			pdd1Manager.enterNew(selectedPdd1Sonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedPdd1Sonuc.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			selectedPdd1Sonuc.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			pdd1Manager.updateEntity(selectedPdd1Sonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedPlastiklerinYogunlugununBelirlenmesiTesti();
		reset();
	}

	public void deleteSelectedPdd1Sonuc() {

		TestHammaddeGirdiPdd1Manager pdd1Manager = new TestHammaddeGirdiPdd1Manager();

		if (selectedPdd1Sonuc == null || selectedPdd1Sonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		pdd1Manager.delete(selectedPdd1Sonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedPdd1Spec() {

		TestHammaddeGirdiPdd1SpecManager pdd1SpecManager = new TestHammaddeGirdiPdd1SpecManager();

		if (selectedPdd1SpecSonuc.getId() == null) {

			selectedPdd1SpecSonuc.setEklemeZamani(UtilInsCore.getTarihZaman());
			selectedPdd1SpecSonuc.setEkleyenKullanici(userBean.getUser()
					.getId());
			selectedPdd1SpecSonuc.setCoatRawMaterial(currentCoatMaterial);

			pdd1SpecManager.enterNew(selectedPdd1SpecSonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedPdd1SpecSonuc.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			selectedPdd1SpecSonuc.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			pdd1SpecManager.updateEntity(selectedPdd1SpecSonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedPlastiklerinYogunlugununBelirlenmesiTesti();
		reset();
	}

	public void deleteSelectedPdd1Spec() {

		TestHammaddeGirdiPdd1SpecManager pdd1SpecManager = new TestHammaddeGirdiPdd1SpecManager();

		if (selectedPdd1SpecSonuc == null || selectedPdd1SpecSonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		pdd1SpecManager.delete(selectedPdd1SpecSonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedUreticiPdd1Sonuc() {

		TestHammaddeUreticiPdd1Manager pdd1UreticiManager = new TestHammaddeUreticiPdd1Manager();

		if (selectedPdd1Sonuc.getId() == null) {

			selectedUreticiPdd1Sonuc.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			selectedUreticiPdd1Sonuc.setEkleyenKullanici(userBean.getUser()
					.getId());
			selectedUreticiPdd1Sonuc.setCoatRawMaterial(currentCoatMaterial);

			pdd1UreticiManager.enterNew(selectedUreticiPdd1Sonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedUreticiPdd1Sonuc
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			selectedUreticiPdd1Sonuc.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			pdd1UreticiManager.updateEntity(selectedUreticiPdd1Sonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedPlastiklerinYogunlugununBelirlenmesiTesti();
		reset();
	}

	public void deleteSelectedUreticiPdd1Sonuc() {

		TestHammaddeUreticiPdd1Manager pdd1UreticiManager = new TestHammaddeUreticiPdd1Manager();

		if (selectedUreticiPdd1Sonuc == null
				|| selectedUreticiPdd1Sonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		pdd1UreticiManager.delete(selectedUreticiPdd1Sonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedEty1Sonuc() {

		TestHammaddeGirdiEpoksiTozEty1Manager ety1Manager = new TestHammaddeGirdiEpoksiTozEty1Manager();

		if (selectedEty1Sonuc.getId() == null) {

			selectedEty1Sonuc.setEklemeZamani(UtilInsCore.getTarihZaman());
			selectedEty1Sonuc.setEkleyenKullanici(userBean.getUser().getId());
			selectedEty1Sonuc.setCoatRawMaterial(currentCoatMaterial);

			ety1Manager.enterNew(selectedEty1Sonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedEty1Sonuc.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			selectedEty1Sonuc.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			ety1Manager.updateEntity(selectedEty1Sonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedPlastiklerinYogunlugununBelirlenmesiTesti();
		reset();
	}

	public void deleteSelectedEty1Sonuc() {

		TestHammaddeGirdiEpoksiTozEty1Manager ety1Manager = new TestHammaddeGirdiEpoksiTozEty1Manager();

		if (selectedEty1Sonuc == null || selectedEty1Sonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		ety1Manager.delete(selectedEty1Sonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedYst1Sonuc() {

		TestHammaddeUreticiYst1Manager yst1Manager = new TestHammaddeUreticiYst1Manager();

		if (selectedUreticiYst1Sonuc.getId() == null) {

			selectedUreticiYst1Sonuc.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			selectedUreticiYst1Sonuc.setEkleyenKullanici(userBean.getUser()
					.getId());
			selectedUreticiYst1Sonuc.setCoatRawMaterial(currentCoatMaterial);

			yst1Manager.enterNew(selectedUreticiYst1Sonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedUreticiYst1Sonuc
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			selectedUreticiYst1Sonuc.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			yst1Manager.updateEntity(selectedUreticiYst1Sonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedPlastiklerinYogunlugununBelirlenmesiTesti();
		reset();
	}

	public void deleteSelectedYst1Sonuc() {

		TestHammaddeUreticiYst1Manager yst1Manager = new TestHammaddeUreticiYst1Manager();

		if (selectedUreticiYst1Sonuc == null
				|| selectedUreticiYst1Sonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		yst1Manager.delete(selectedUreticiYst1Sonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedKst1Sonuc() {

		TestHammaddeUreticiKst1Manager kst1Manager = new TestHammaddeUreticiKst1Manager();

		if (selectedUreticiKst1Sonuc.getId() == null) {

			selectedUreticiKst1Sonuc.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			selectedUreticiKst1Sonuc.setEkleyenKullanici(userBean.getUser()
					.getId());
			selectedUreticiKst1Sonuc.setCoatRawMaterial(currentCoatMaterial);

			kst1Manager.enterNew(selectedUreticiKst1Sonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedUreticiKst1Sonuc
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			selectedUreticiKst1Sonuc.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			kst1Manager.updateEntity(selectedUreticiKst1Sonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedPlastiklerinYogunlugununBelirlenmesiTesti();
		reset();
	}

	public void deleteSelectedKst1Sonuc() {

		TestHammaddeUreticiKst1Manager kst1Manager = new TestHammaddeUreticiKst1Manager();

		if (selectedUreticiKst1Sonuc == null
				|| selectedUreticiKst1Sonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		kst1Manager.delete(selectedUreticiKst1Sonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedPkd1Sonuc() {

		TestHammaddeUreticiPkd1Manager pkd1Manager = new TestHammaddeUreticiPkd1Manager();

		if (selectedUreticiPkd1Sonuc.getId() == null) {

			selectedUreticiPkd1Sonuc.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			selectedUreticiPkd1Sonuc.setEkleyenKullanici(userBean.getUser()
					.getId());
			selectedUreticiPkd1Sonuc.setCoatRawMaterial(currentCoatMaterial);

			pkd1Manager.enterNew(selectedUreticiPkd1Sonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedUreticiPkd1Sonuc
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			selectedUreticiPkd1Sonuc.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			pkd1Manager.updateEntity(selectedUreticiPkd1Sonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedPlastiklerinYogunlugununBelirlenmesiTesti();
		reset();
	}

	public void deleteSelectedPkd1Sonuc() {

		TestHammaddeUreticiPkd1Manager pkd1Manager = new TestHammaddeUreticiPkd1Manager();

		if (selectedUreticiPkd1Sonuc == null
				|| selectedUreticiPkd1Sonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		pkd1Manager.delete(selectedUreticiPkd1Sonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedPks1Sonuc() {

		TestHammaddeUreticiPks1Manager pks1Manager = new TestHammaddeUreticiPks1Manager();

		if (selectedUreticiPks1Sonuc.getId() == null) {

			selectedUreticiPks1Sonuc.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			selectedUreticiPks1Sonuc.setEkleyenKullanici(userBean.getUser()
					.getId());
			selectedUreticiPks1Sonuc.setCoatRawMaterial(currentCoatMaterial);

			pks1Manager.enterNew(selectedUreticiPks1Sonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedUreticiPks1Sonuc
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			selectedUreticiPks1Sonuc.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			pks1Manager.updateEntity(selectedUreticiPks1Sonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedPlastiklerinYogunlugununBelirlenmesiTesti();
		reset();
	}

	public void deleteSelectedPks1Sonuc() {

		TestHammaddeUreticiPks1Manager pks1Manager = new TestHammaddeUreticiPks1Manager();

		if (selectedUreticiPks1Sonuc == null
				|| selectedUreticiPks1Sonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		pks1Manager.delete(selectedUreticiPks1Sonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedPku1Sonuc() {

		TestHammaddeUreticiPku1Manager pku1Manager = new TestHammaddeUreticiPku1Manager();

		if (selectedUreticiPku1Sonuc.getId() == null) {

			selectedUreticiPku1Sonuc.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			selectedUreticiPku1Sonuc.setEkleyenKullanici(userBean.getUser()
					.getId());
			selectedUreticiPku1Sonuc.setCoatRawMaterial(currentCoatMaterial);

			pku1Manager.enterNew(selectedUreticiPku1Sonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedUreticiPku1Sonuc
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			selectedUreticiPku1Sonuc.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			pku1Manager.updateEntity(selectedUreticiPku1Sonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedPlastiklerinYogunlugununBelirlenmesiTesti();
		reset();
	}

	public void deleteSelectedPku1Sonuc() {

		TestHammaddeUreticiPku1Manager pku1Manager = new TestHammaddeUreticiPku1Manager();

		if (selectedUreticiPku1Sonuc == null
				|| selectedUreticiPku1Sonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		pku1Manager.delete(selectedUreticiPku1Sonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedBkm1PhSonuc() {

		TestHammaddeGirdiBkm1PhManager pku1Manager = new TestHammaddeGirdiBkm1PhManager();

		if (selectedBkm1PhSonuc.getId() == null) {

			selectedBkm1PhSonuc.setEklemeZamani(UtilInsCore.getTarihZaman());
			selectedBkm1PhSonuc.setEkleyenKullanici(userBean.getUser().getId());
			selectedBkm1PhSonuc.setCoatRawMaterial(currentCoatMaterial);

			pku1Manager.enterNew(selectedBkm1PhSonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedBkm1PhSonuc.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			selectedBkm1PhSonuc.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			pku1Manager.updateEntity(selectedBkm1PhSonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedPlastiklerinYogunlugununBelirlenmesiTesti();
		reset();
	}

	public void deleteSelectedBkm1PhSonuc() {

		TestHammaddeGirdiBkm1PhManager pku1Manager = new TestHammaddeGirdiBkm1PhManager();

		if (selectedBkm1PhSonuc == null || selectedBkm1PhSonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		pku1Manager.delete(selectedBkm1PhSonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedBkm1KatiMaddeSonuc() {

		TestHammaddeGirdiBkm1KatiMaddeManager pku1Manager = new TestHammaddeGirdiBkm1KatiMaddeManager();

		if (selectedBkm1KatiMaddeSonuc.getId() == null) {

			selectedBkm1KatiMaddeSonuc.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			selectedBkm1KatiMaddeSonuc.setEkleyenKullanici(userBean.getUser()
					.getId());
			selectedBkm1KatiMaddeSonuc.setCoatRawMaterial(currentCoatMaterial);

			pku1Manager.enterNew(selectedBkm1KatiMaddeSonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedBkm1KatiMaddeSonuc
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			selectedBkm1KatiMaddeSonuc.setGuncelleyenKullanici(userBean
					.getUser().getId());
			pku1Manager.updateEntity(selectedBkm1KatiMaddeSonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedPlastiklerinYogunlugununBelirlenmesiTesti();
		reset();
	}

	public void deleteSelectedBkm1KatiMaddeSonuc() {

		TestHammaddeGirdiBkm1KatiMaddeManager pku1Manager = new TestHammaddeGirdiBkm1KatiMaddeManager();

		if (selectedBkm1KatiMaddeSonuc == null
				|| selectedBkm1KatiMaddeSonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		pku1Manager.delete(selectedBkm1KatiMaddeSonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedBkm1KuruMaddeSonuc() {

		TestHammaddeGirdiBkm1KuruMaddeManager pku1Manager = new TestHammaddeGirdiBkm1KuruMaddeManager();

		if (selectedBkm1KuruMaddeSonuc.getId() == null) {

			selectedBkm1KuruMaddeSonuc.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			selectedBkm1KuruMaddeSonuc.setEkleyenKullanici(userBean.getUser()
					.getId());
			selectedBkm1KuruMaddeSonuc.setCoatRawMaterial(currentCoatMaterial);

			pku1Manager.enterNew(selectedBkm1KuruMaddeSonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedBkm1KuruMaddeSonuc
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			selectedBkm1KuruMaddeSonuc.setGuncelleyenKullanici(userBean
					.getUser().getId());
			pku1Manager.updateEntity(selectedBkm1KuruMaddeSonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedPlastiklerinYogunlugununBelirlenmesiTesti();
		reset();
	}

	public void deleteSelectedBkm1KuruMaddeSonuc() {

		TestHammaddeGirdiBkm1KuruMaddeManager pku1Manager = new TestHammaddeGirdiBkm1KuruMaddeManager();

		if (selectedBkm1KuruMaddeSonuc == null
				|| selectedBkm1KuruMaddeSonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		pku1Manager.delete(selectedBkm1KuruMaddeSonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedSertlikSonuc() {

		TestHammaddeGirdiSertlikManager sertlikManager = new TestHammaddeGirdiSertlikManager();

		if (selectedGirdiSertlikSonuc.getId() == null) {

			selectedGirdiSertlikSonuc.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			selectedGirdiSertlikSonuc.setEkleyenKullanici(userBean.getUser()
					.getId());
			selectedGirdiSertlikSonuc.setCoatRawMaterial(currentCoatMaterial);

			sertlikManager.enterNew(selectedGirdiSertlikSonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedGirdiSertlikSonuc
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			selectedGirdiSertlikSonuc.setGuncelleyenKullanici(userBean
					.getUser().getId());
			sertlikManager.updateEntity(selectedGirdiSertlikSonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedSertlikTesti();
		reset();
	}

	public void deleteSelectedSertlikSonuc() {

		TestHammaddeGirdiSertlikManager sertlikManager = new TestHammaddeGirdiSertlikManager();

		if (selectedGirdiSertlikSonuc == null
				|| selectedGirdiSertlikSonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		sertlikManager.delete(selectedGirdiSertlikSonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedSertlikSpec() {

		TestHammaddeGirdiSertlikSpecManager specManager = new TestHammaddeGirdiSertlikSpecManager();

		if (selectedSertlikSpecSonuc.getId() == null) {

			selectedSertlikSpecSonuc.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			selectedSertlikSpecSonuc.setEkleyenKullanici(userBean.getUser()
					.getId());
			selectedSertlikSpecSonuc.setCoatRawMaterial(currentCoatMaterial);

			specManager.enterNew(selectedSertlikSpecSonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedSertlikSpecSonuc
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			selectedSertlikSpecSonuc.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			specManager.updateEntity(selectedSertlikSpecSonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedPlastiklerinYogunlugununBelirlenmesiTesti();
		reset();
	}

	public void deleteSelectedSertlikSpec() {

		TestHammaddeGirdiSertlikSpecManager specManager = new TestHammaddeGirdiSertlikSpecManager();

		if (selectedSertlikSpecSonuc == null
				|| selectedSertlikSpecSonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		specManager.delete(selectedSertlikSpecSonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedAmg1Sonuc() {

		TestHammaddeGirdiAmg1Manager asindiriciManager = new TestHammaddeGirdiAmg1Manager();

		if (selectedAmg1Sonuc.getId() == null) {

			selectedAmg1Sonuc.setEklemeZamani(UtilInsCore.getTarihZaman());
			selectedAmg1Sonuc.setEkleyenKullanici(userBean.getUser().getId());
			selectedAmg1Sonuc.setCoatRawMaterial(currentCoatMaterial);

			asindiriciManager.enterNew(selectedAmg1Sonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedAmg1Sonuc.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			selectedAmg1Sonuc.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			asindiriciManager.updateEntity(selectedAmg1Sonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedAsindiriciMalzemeGirisTesti();
		reset();
	}

	public void deleteSelectedAmg1Sonuc() {

		TestHammaddeGirdiAmg1Manager asindiriciManager = new TestHammaddeGirdiAmg1Manager();

		if (selectedAmg1Sonuc == null || selectedAmg1Sonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		asindiriciManager.delete(selectedAmg1Sonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	public void saveSelectedAmg1Spec() {

		TestHammaddeGirdiAmg1SpecManager specManager = new TestHammaddeGirdiAmg1SpecManager();

		if (selectedAmg1SpecSonuc.getId() == null) {

			selectedAmg1SpecSonuc.setEklemeZamani(UtilInsCore.getTarihZaman());
			selectedAmg1SpecSonuc.setEkleyenKullanici(userBean.getUser()
					.getId());
			selectedAmg1SpecSonuc.setCoatRawMaterial(currentCoatMaterial);

			specManager.enterNew(selectedAmg1SpecSonuc);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			selectedAmg1SpecSonuc.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			selectedAmg1SpecSonuc.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			specManager.updateEntity(selectedAmg1SpecSonuc);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		loadSelectedAsindiriciMalzemeGirisTesti();
		reset();
	}

	public void deleteSelectedAmg1Spec() {

		TestHammaddeGirdiAmg1SpecManager specManager = new TestHammaddeGirdiAmg1SpecManager();

		if (selectedAmg1SpecSonuc == null || selectedAmg1SpecSonuc.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		specManager.delete(selectedAmg1SpecSonuc);

		FacesContextUtils.addWarnMessage("DeletedMessage");
		reset();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public List<CoatMaterialType> getCoatMaterialTypeList() {
		return coatMaterialTypeList;
	}

	public void setCoatMaterialTypeList(
			List<CoatMaterialType> coatMaterialTypeList) {
		this.coatMaterialTypeList = coatMaterialTypeList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<CoatRawMaterial> getCoatRawMaterialList() {
		return coatRawMaterialList;
	}

	public void setCoatRawMaterialList(List<CoatRawMaterial> coatRawMaterialList) {
		this.coatRawMaterialList = coatRawMaterialList;
	}

	public CoatRawMaterial getCurrentCoatMaterial() {
		return currentCoatMaterial;
	}

	public void setCurrentCoatMaterial(CoatRawMaterial currentCoatMaterial) {
		this.currentCoatMaterial = currentCoatMaterial;
	}

	public List<CoatRawMaterial> getAvailableCoatRawMaterial() {
		return availableCoatRawMaterial;
	}

	public void setAvailableCoatRawMaterial(
			List<CoatRawMaterial> availableCoatRawMaterial) {
		this.availableCoatRawMaterial = availableCoatRawMaterial;
	}

	public CoatMaterialType getCurrentType() {
		return currentType;
	}

	public void setCurrentType(CoatMaterialType currentType) {
		this.currentType = currentType;
	}

	public CoatMaterialType getNewType() {
		return newType;
	}

	public void setNewType(CoatMaterialType newType) {
		this.newType = newType;
	}

	public CoatRawMaterial getNewCoatMaterial() {
		return newCoatMaterial;
	}

	public void setNewCoatMaterial(CoatRawMaterial newCoatMaterial) {
		this.newCoatMaterial = newCoatMaterial;
	}

	public CoatRawMaterial getHistoryCoatMaterial() {
		return historyCoatMaterial;
	}

	public void setHistoryCoatMaterial(CoatRawMaterial historyCoatMaterial) {
		this.historyCoatMaterial = historyCoatMaterial;
	}

	/**
	 * @return the selectedEta1Sonuc
	 */
	public TestHammaddeGirdiEpoksiTozEta1Sonuc getSelectedEta1Sonuc() {
		return selectedEta1Sonuc;
	}

	/**
	 * @param selectedEta1Sonuc
	 *            the selectedEta1Sonuc to set
	 */
	public void setSelectedEta1Sonuc(
			TestHammaddeGirdiEpoksiTozEta1Sonuc selectedEta1Sonuc) {
		this.selectedEta1Sonuc = selectedEta1Sonuc;
	}

	/**
	 * @return the config
	 */
	public ConfigBean getConfig() {
		return config;
	}

	/**
	 * @param config
	 *            the config to set
	 */
	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the eta1
	 */
	public TestHammaddeGirdiEpoksiTozEta1Sonuc getEta1() {
		return eta1;
	}

	/**
	 * @param eta1
	 *            the eta1 to set
	 */
	public void setEta1(TestHammaddeGirdiEpoksiTozEta1Sonuc eta1) {
		this.eta1 = eta1;
	}

	/**
	 * @return the selectedEni1Sonuc
	 */
	public TestHammaddeGirdiEpoksiTozEni1 getSelectedEni1Sonuc() {
		return selectedEni1Sonuc;
	}

	/**
	 * @param selectedEni1Sonuc
	 *            the selectedEni1Sonuc to set
	 */
	public void setSelectedEni1Sonuc(
			TestHammaddeGirdiEpoksiTozEni1 selectedEni1Sonuc) {
		this.selectedEni1Sonuc = selectedEni1Sonuc;
	}

	/**
	 * @return the eni1
	 */
	public TestHammaddeGirdiEpoksiTozEni1 getEni1() {
		return eni1;
	}

	/**
	 * @param eni1
	 *            the eni1 to set
	 */
	public void setEni1(TestHammaddeGirdiEpoksiTozEni1 eni1) {
		this.eni1 = eni1;
	}

	/**
	 * @return the selectedEjs1Sonuc
	 */
	public TestHammaddeGirdiEpoksiTozEjs1 getSelectedEjs1Sonuc() {
		return selectedEjs1Sonuc;
	}

	/**
	 * @param selectedEjs1Sonuc
	 *            the selectedEjs1Sonuc to set
	 */
	public void setSelectedEjs1Sonuc(
			TestHammaddeGirdiEpoksiTozEjs1 selectedEjs1Sonuc) {
		this.selectedEjs1Sonuc = selectedEjs1Sonuc;
	}

	/**
	 * @return the ejs1
	 */
	public TestHammaddeGirdiEpoksiTozEjs1 getEjs1() {
		return ejs1;
	}

	/**
	 * @param ejs1
	 *            the ejs1 to set
	 */
	public void setEjs1(TestHammaddeGirdiEpoksiTozEjs1 ejs1) {
		this.ejs1 = ejs1;
	}

	/**
	 * @return the selectedEea1Sonuc
	 */
	public TestHammaddeGirdiEpoksiTozEea1 getSelectedEea1Sonuc() {
		return selectedEea1Sonuc;
	}

	/**
	 * @param selectedEea1Sonuc
	 *            the selectedEea1Sonuc to set
	 */
	public void setSelectedEea1Sonuc(
			TestHammaddeGirdiEpoksiTozEea1 selectedEea1Sonuc) {
		this.selectedEea1Sonuc = selectedEea1Sonuc;
	}

	/**
	 * @return the eea1
	 */
	public TestHammaddeGirdiEpoksiTozEea1 getEea1() {
		return eea1;
	}

	/**
	 * @param eea1
	 *            the eea1 to set
	 */
	public void setEea1(TestHammaddeGirdiEpoksiTozEea1 eea1) {
		this.eea1 = eea1;
	}

	/**
	 * @return the selectedEkz1Sonuc
	 */
	public TestHammaddeGirdiEpoksiTozEkz1 getSelectedEkz1Sonuc() {
		return selectedEkz1Sonuc;
	}

	/**
	 * @param selectedEkz1Sonuc
	 *            the selectedEkz1Sonuc to set
	 */
	public void setSelectedEkz1Sonuc(
			TestHammaddeGirdiEpoksiTozEkz1 selectedEkz1Sonuc) {
		this.selectedEkz1Sonuc = selectedEkz1Sonuc;
	}

	/**
	 * @return the ekz1
	 */
	public TestHammaddeGirdiEpoksiTozEkz1 getEkz1() {
		return ekz1;
	}

	/**
	 * @param ekz1
	 *            the ekz1 to set
	 */
	public void setEkz1(TestHammaddeGirdiEpoksiTozEkz1 ekz1) {
		this.ekz1 = ekz1;
	}

	/**
	 * @return the selectedMfr1Sonuc
	 */
	public TestHammaddeGirdiMfr1 getSelectedMfr1Sonuc() {
		return selectedMfr1Sonuc;
	}

	/**
	 * @param selectedMfr1Sonuc
	 *            the selectedMfr1Sonuc to set
	 */
	public void setSelectedMfr1Sonuc(TestHammaddeGirdiMfr1 selectedMfr1Sonuc) {
		this.selectedMfr1Sonuc = selectedMfr1Sonuc;
	}

	/**
	 * @return the mfr1
	 */
	public TestHammaddeGirdiMfr1 getMfr1() {
		return mfr1;
	}

	/**
	 * @param mfr1
	 *            the mfr1 to set
	 */
	public void setMfr1(TestHammaddeGirdiMfr1 mfr1) {
		this.mfr1 = mfr1;
	}

	/**
	 * @return the selectedPdd1Sonuc
	 */
	public TestHammaddeGirdiPdd1 getSelectedPdd1Sonuc() {
		return selectedPdd1Sonuc;
	}

	/**
	 * @param selectedPdd1Sonuc
	 *            the selectedPdd1Sonuc to set
	 */
	public void setSelectedPdd1Sonuc(TestHammaddeGirdiPdd1 selectedPdd1Sonuc) {
		this.selectedPdd1Sonuc = selectedPdd1Sonuc;
	}

	/**
	 * @return the pdd1
	 */
	public TestHammaddeGirdiPdd1 getPdd1() {
		return pdd1;
	}

	/**
	 * @param pdd1
	 *            the pdd1 to set
	 */
	public void setPdd1(TestHammaddeGirdiPdd1 pdd1) {
		this.pdd1 = pdd1;
	}

	/**
	 * @return the selectedOit1Sonuc
	 */
	public TestHammaddeGirdiOit1 getSelectedOit1Sonuc() {
		return selectedOit1Sonuc;
	}

	/**
	 * @param selectedOit1Sonuc
	 *            the selectedOit1Sonuc to set
	 */
	public void setSelectedOit1Sonuc(TestHammaddeGirdiOit1 selectedOit1Sonuc) {
		this.selectedOit1Sonuc = selectedOit1Sonuc;
	}

	/**
	 * @return the oit1
	 */
	public TestHammaddeGirdiOit1 getOit1() {
		return oit1;
	}

	/**
	 * @param oit1
	 *            the oit1 to set
	 */
	public void setOit1(TestHammaddeGirdiOit1 oit1) {
		this.oit1 = oit1;
	}

	/**
	 * @return the selectedEty1Sonuc
	 */
	public TestHammaddeGirdiEpoksiTozEty1 getSelectedEty1Sonuc() {
		return selectedEty1Sonuc;
	}

	/**
	 * @param selectedEty1Sonuc
	 *            the selectedEty1Sonuc to set
	 */
	public void setSelectedEty1Sonuc(
			TestHammaddeGirdiEpoksiTozEty1 selectedEty1Sonuc) {
		this.selectedEty1Sonuc = selectedEty1Sonuc;
	}

	/**
	 * @return the ety1
	 */
	public TestHammaddeGirdiEpoksiTozEty1 getEty1() {
		return ety1;
	}

	/**
	 * @param ety1
	 *            the ety1 to set
	 */
	public void setEty1(TestHammaddeGirdiEpoksiTozEty1 ety1) {
		this.ety1 = ety1;
	}

	/**
	 * @return the selectedUreticiMfr1Sonuc
	 */
	public TestHammaddeUreticiMfr1 getSelectedUreticiMfr1Sonuc() {
		return selectedUreticiMfr1Sonuc;
	}

	/**
	 * @param selectedUreticiMfr1Sonuc
	 *            the selectedUreticiMfr1Sonuc to set
	 */
	public void setSelectedUreticiMfr1Sonuc(
			TestHammaddeUreticiMfr1 selectedUreticiMfr1Sonuc) {
		this.selectedUreticiMfr1Sonuc = selectedUreticiMfr1Sonuc;
	}

	/**
	 * @return the mfr1Uretici
	 */
	public TestHammaddeUreticiMfr1 getMfr1Uretici() {
		return mfr1Uretici;
	}

	/**
	 * @param mfr1Uretici
	 *            the mfr1Uretici to set
	 */
	public void setMfr1Uretici(TestHammaddeUreticiMfr1 mfr1Uretici) {
		this.mfr1Uretici = mfr1Uretici;
	}

	/**
	 * @return the selectedUreticiPdd1Sonuc
	 */
	public TestHammaddeUreticiPdd1 getSelectedUreticiPdd1Sonuc() {
		return selectedUreticiPdd1Sonuc;
	}

	/**
	 * @param selectedUreticiPdd1Sonuc
	 *            the selectedUreticiPdd1Sonuc to set
	 */
	public void setSelectedUreticiPdd1Sonuc(
			TestHammaddeUreticiPdd1 selectedUreticiPdd1Sonuc) {
		this.selectedUreticiPdd1Sonuc = selectedUreticiPdd1Sonuc;
	}

	/**
	 * @return the pdd1Uretici
	 */
	public TestHammaddeUreticiPdd1 getPdd1Uretici() {
		return pdd1Uretici;
	}

	/**
	 * @param pdd1Uretici
	 *            the pdd1Uretici to set
	 */
	public void setPdd1Uretici(TestHammaddeUreticiPdd1 pdd1Uretici) {
		this.pdd1Uretici = pdd1Uretici;
	}

	/**
	 * @return the selectedUreticiOit1Sonuc
	 */
	public TestHammaddeUreticiOit1 getSelectedUreticiOit1Sonuc() {
		return selectedUreticiOit1Sonuc;
	}

	/**
	 * @param selectedUreticiOit1Sonuc
	 *            the selectedUreticiOit1Sonuc to set
	 */
	public void setSelectedUreticiOit1Sonuc(
			TestHammaddeUreticiOit1 selectedUreticiOit1Sonuc) {
		this.selectedUreticiOit1Sonuc = selectedUreticiOit1Sonuc;
	}

	/**
	 * @return the oit1Uretici
	 */
	public TestHammaddeUreticiOit1 getOit1Uretici() {
		return oit1Uretici;
	}

	/**
	 * @param oit1Uretici
	 *            the oit1Uretici to set
	 */
	public void setOit1Uretici(TestHammaddeUreticiOit1 oit1Uretici) {
		this.oit1Uretici = oit1Uretici;
	}

	/**
	 * @return the selectedMfr1SpecSonuc
	 */
	public TestHammaddeGirdiMfr1Spec getSelectedMfr1SpecSonuc() {
		return selectedMfr1SpecSonuc;
	}

	/**
	 * @param selectedMfr1SpecSonuc
	 *            the selectedMfr1SpecSonuc to set
	 */
	public void setSelectedMfr1SpecSonuc(
			TestHammaddeGirdiMfr1Spec selectedMfr1SpecSonuc) {
		this.selectedMfr1SpecSonuc = selectedMfr1SpecSonuc;
	}

	/**
	 * @return the mfr1Spec
	 */
	public TestHammaddeGirdiMfr1Spec getMfr1Spec() {
		return mfr1Spec;
	}

	/**
	 * @param mfr1Spec
	 *            the mfr1Spec to set
	 */
	public void setMfr1Spec(TestHammaddeGirdiMfr1Spec mfr1Spec) {
		this.mfr1Spec = mfr1Spec;
	}

	/**
	 * @return the selectedPdd1SpecSonuc
	 */
	public TestHammaddeGirdiPdd1Spec getSelectedPdd1SpecSonuc() {
		return selectedPdd1SpecSonuc;
	}

	/**
	 * @param selectedPdd1SpecSonuc
	 *            the selectedPdd1SpecSonuc to set
	 */
	public void setSelectedPdd1SpecSonuc(
			TestHammaddeGirdiPdd1Spec selectedPdd1SpecSonuc) {
		this.selectedPdd1SpecSonuc = selectedPdd1SpecSonuc;
	}

	/**
	 * @return the pdd1Spec
	 */
	public TestHammaddeGirdiPdd1Spec getPdd1Spec() {
		return pdd1Spec;
	}

	/**
	 * @param pdd1Spec
	 *            the pdd1Spec to set
	 */
	public void setPdd1Spec(TestHammaddeGirdiPdd1Spec pdd1Spec) {
		this.pdd1Spec = pdd1Spec;
	}

	/**
	 * @return the selectedUreticiYst1Sonuc
	 */
	public TestHammaddeUreticiYst1 getSelectedUreticiYst1Sonuc() {
		return selectedUreticiYst1Sonuc;
	}

	/**
	 * @param selectedUreticiYst1Sonuc
	 *            the selectedUreticiYst1Sonuc to set
	 */
	public void setSelectedUreticiYst1Sonuc(
			TestHammaddeUreticiYst1 selectedUreticiYst1Sonuc) {
		this.selectedUreticiYst1Sonuc = selectedUreticiYst1Sonuc;
	}

	/**
	 * @return the yst1Uretici
	 */
	public TestHammaddeUreticiYst1 getYst1Uretici() {
		return yst1Uretici;
	}

	/**
	 * @param yst1Uretici
	 *            the yst1Uretici to set
	 */
	public void setYst1Uretici(TestHammaddeUreticiYst1 yst1Uretici) {
		this.yst1Uretici = yst1Uretici;
	}

	/**
	 * @return the selectedUreticiKst1Sonuc
	 */
	public TestHammaddeUreticiKst1 getSelectedUreticiKst1Sonuc() {
		return selectedUreticiKst1Sonuc;
	}

	/**
	 * @param selectedUreticiKst1Sonuc
	 *            the selectedUreticiKst1Sonuc to set
	 */
	public void setSelectedUreticiKst1Sonuc(
			TestHammaddeUreticiKst1 selectedUreticiKst1Sonuc) {
		this.selectedUreticiKst1Sonuc = selectedUreticiKst1Sonuc;
	}

	/**
	 * @return the kst1Uretici
	 */
	public TestHammaddeUreticiKst1 getKst1Uretici() {
		return kst1Uretici;
	}

	/**
	 * @param kst1Uretici
	 *            the kst1Uretici to set
	 */
	public void setKst1Uretici(TestHammaddeUreticiKst1 kst1Uretici) {
		this.kst1Uretici = kst1Uretici;
	}

	/**
	 * @return the selectedUreticiPkd1Sonuc
	 */
	public TestHammaddeUreticiPkd1 getSelectedUreticiPkd1Sonuc() {
		return selectedUreticiPkd1Sonuc;
	}

	/**
	 * @param selectedUreticiPkd1Sonuc
	 *            the selectedUreticiPkd1Sonuc to set
	 */
	public void setSelectedUreticiPkd1Sonuc(
			TestHammaddeUreticiPkd1 selectedUreticiPkd1Sonuc) {
		this.selectedUreticiPkd1Sonuc = selectedUreticiPkd1Sonuc;
	}

	/**
	 * @return the pkd1Uretici
	 */
	public TestHammaddeUreticiPkd1 getPkd1Uretici() {
		return pkd1Uretici;
	}

	/**
	 * @param pkd1Uretici
	 *            the pkd1Uretici to set
	 */
	public void setPkd1Uretici(TestHammaddeUreticiPkd1 pkd1Uretici) {
		this.pkd1Uretici = pkd1Uretici;
	}

	/**
	 * @return the selectedUreticiPks1Sonuc
	 */
	public TestHammaddeUreticiPks1 getSelectedUreticiPks1Sonuc() {
		return selectedUreticiPks1Sonuc;
	}

	/**
	 * @param selectedUreticiPks1Sonuc
	 *            the selectedUreticiPks1Sonuc to set
	 */
	public void setSelectedUreticiPks1Sonuc(
			TestHammaddeUreticiPks1 selectedUreticiPks1Sonuc) {
		this.selectedUreticiPks1Sonuc = selectedUreticiPks1Sonuc;
	}

	/**
	 * @return the pks1Uretici
	 */
	public TestHammaddeUreticiPks1 getPks1Uretici() {
		return pks1Uretici;
	}

	/**
	 * @param pks1Uretici
	 *            the pks1Uretici to set
	 */
	public void setPks1Uretici(TestHammaddeUreticiPks1 pks1Uretici) {
		this.pks1Uretici = pks1Uretici;
	}

	/**
	 * @return the selectedUreticiPku1Sonuc
	 */
	public TestHammaddeUreticiPku1 getSelectedUreticiPku1Sonuc() {
		return selectedUreticiPku1Sonuc;
	}

	/**
	 * @param selectedUreticiPku1Sonuc
	 *            the selectedUreticiPku1Sonuc to set
	 */
	public void setSelectedUreticiPku1Sonuc(
			TestHammaddeUreticiPku1 selectedUreticiPku1Sonuc) {
		this.selectedUreticiPku1Sonuc = selectedUreticiPku1Sonuc;
	}

	/**
	 * @return the pku1Uretici
	 */
	public TestHammaddeUreticiPku1 getPku1Uretici() {
		return pku1Uretici;
	}

	/**
	 * @param pku1Uretici
	 *            the pku1Uretici to set
	 */
	public void setPku1Uretici(TestHammaddeUreticiPku1 pku1Uretici) {
		this.pku1Uretici = pku1Uretici;
	}

	/**
	 * @return the selectedBkm1PhSonuc
	 */
	public TestHammaddeGirdiBkm1Ph getSelectedBkm1PhSonuc() {
		return selectedBkm1PhSonuc;
	}

	/**
	 * @param selectedBkm1PhSonuc
	 *            the selectedBkm1PhSonuc to set
	 */
	public void setSelectedBkm1PhSonuc(
			TestHammaddeGirdiBkm1Ph selectedBkm1PhSonuc) {
		this.selectedBkm1PhSonuc = selectedBkm1PhSonuc;
	}

	/**
	 * @return the bkm1Ph
	 */
	public TestHammaddeGirdiBkm1Ph getBkm1Ph() {
		return bkm1Ph;
	}

	/**
	 * @param bkm1Ph
	 *            the bkm1Ph to set
	 */
	public void setBkm1Ph(TestHammaddeGirdiBkm1Ph bkm1Ph) {
		this.bkm1Ph = bkm1Ph;
	}

	/**
	 * @return the selectedBkm1KatiMaddeSonuc
	 */
	public TestHammaddeGirdiBkm1KatiMadde getSelectedBkm1KatiMaddeSonuc() {
		return selectedBkm1KatiMaddeSonuc;
	}

	/**
	 * @param selectedBkm1KatiMaddeSonuc
	 *            the selectedBkm1KatiMaddeSonuc to set
	 */
	public void setSelectedBkm1KatiMaddeSonuc(
			TestHammaddeGirdiBkm1KatiMadde selectedBkm1KatiMaddeSonuc) {
		this.selectedBkm1KatiMaddeSonuc = selectedBkm1KatiMaddeSonuc;
	}

	/**
	 * @return the bkm1katiMadde
	 */
	public TestHammaddeGirdiBkm1KatiMadde getBkm1katiMadde() {
		return bkm1katiMadde;
	}

	/**
	 * @param bkm1katiMadde
	 *            the bkm1katiMadde to set
	 */
	public void setBkm1katiMadde(TestHammaddeGirdiBkm1KatiMadde bkm1katiMadde) {
		this.bkm1katiMadde = bkm1katiMadde;
	}

	/**
	 * @return the selectedBkm1KuruMaddeSonuc
	 */
	public TestHammaddeGirdiBkm1KuruMadde getSelectedBkm1KuruMaddeSonuc() {
		return selectedBkm1KuruMaddeSonuc;
	}

	/**
	 * @param selectedBkm1KuruMaddeSonuc
	 *            the selectedBkm1KuruMaddeSonuc to set
	 */
	public void setSelectedBkm1KuruMaddeSonuc(
			TestHammaddeGirdiBkm1KuruMadde selectedBkm1KuruMaddeSonuc) {
		this.selectedBkm1KuruMaddeSonuc = selectedBkm1KuruMaddeSonuc;
	}

	/**
	 * @return the bkm1KuruMadde
	 */
	public TestHammaddeGirdiBkm1KuruMadde getBkm1KuruMadde() {
		return bkm1KuruMadde;
	}

	/**
	 * @param bkm1KuruMadde
	 *            the bkm1KuruMadde to set
	 */
	public void setBkm1KuruMadde(TestHammaddeGirdiBkm1KuruMadde bkm1KuruMadde) {
		this.bkm1KuruMadde = bkm1KuruMadde;
	}

	/**
	 * @return the selectedSertlikSpecSonuc
	 */
	public TestHammaddeGirdiSertlikSpec getSelectedSertlikSpecSonuc() {
		return selectedSertlikSpecSonuc;
	}

	/**
	 * @param selectedSertlikSpecSonuc
	 *            the selectedSertlikSpecSonuc to set
	 */
	public void setSelectedSertlikSpecSonuc(
			TestHammaddeGirdiSertlikSpec selectedSertlikSpecSonuc) {
		this.selectedSertlikSpecSonuc = selectedSertlikSpecSonuc;
	}

	/**
	 * @return the sertlikSpec
	 */
	public TestHammaddeGirdiSertlikSpec getSertlikSpec() {
		return sertlikSpec;
	}

	/**
	 * @param sertlikSpec
	 *            the sertlikSpec to set
	 */
	public void setSertlikSpec(TestHammaddeGirdiSertlikSpec sertlikSpec) {
		this.sertlikSpec = sertlikSpec;
	}

	/**
	 * @return the sertlik
	 */
	public TestHammaddeGirdiSertlik getSertlik() {
		return sertlik;
	}

	/**
	 * @param sertlik
	 *            the sertlik to set
	 */
	public void setSertlik(TestHammaddeGirdiSertlik sertlik) {
		this.sertlik = sertlik;
	}

	/**
	 * @return the selectedGirdiSertlikSonuc
	 */
	public TestHammaddeGirdiSertlik getSelectedGirdiSertlikSonuc() {
		return selectedGirdiSertlikSonuc;
	}

	/**
	 * @param selectedGirdiSertlikSonuc
	 *            the selectedGirdiSertlikSonuc to set
	 */
	public void setSelectedGirdiSertlikSonuc(
			TestHammaddeGirdiSertlik selectedGirdiSertlikSonuc) {
		this.selectedGirdiSertlikSonuc = selectedGirdiSertlikSonuc;
	}

	/**
	 * @return the selectedAmg1Sonuc
	 */
	public TestHammaddeGirdiAmg1 getSelectedAmg1Sonuc() {
		return selectedAmg1Sonuc;
	}

	/**
	 * @param selectedAmg1Sonuc
	 *            the selectedAmg1Sonuc to set
	 */
	public void setSelectedAmg1Sonuc(TestHammaddeGirdiAmg1 selectedAmg1Sonuc) {
		this.selectedAmg1Sonuc = selectedAmg1Sonuc;
	}

	/**
	 * @return the amg1
	 */
	public TestHammaddeGirdiAmg1 getAmg1() {
		return amg1;
	}

	/**
	 * @param amg1
	 *            the amg1 to set
	 */
	public void setAmg1(TestHammaddeGirdiAmg1 amg1) {
		this.amg1 = amg1;
	}

	/**
	 * @return the selectedAmg1SpecSonuc
	 */
	public TestHammaddeGirdiAmg1Spec getSelectedAmg1SpecSonuc() {
		return selectedAmg1SpecSonuc;
	}

	/**
	 * @param selectedAmg1SpecSonuc
	 *            the selectedAmg1SpecSonuc to set
	 */
	public void setSelectedAmg1SpecSonuc(
			TestHammaddeGirdiAmg1Spec selectedAmg1SpecSonuc) {
		this.selectedAmg1SpecSonuc = selectedAmg1SpecSonuc;
	}

	/**
	 * @return the amg1Spec
	 */
	public TestHammaddeGirdiAmg1Spec getAmg1Spec() {
		return amg1Spec;
	}

	/**
	 * @param amg1Spec
	 *            the amg1Spec to set
	 */
	public void setAmg1Spec(TestHammaddeGirdiAmg1Spec amg1Spec) {
		this.amg1Spec = amg1Spec;
	}

}
