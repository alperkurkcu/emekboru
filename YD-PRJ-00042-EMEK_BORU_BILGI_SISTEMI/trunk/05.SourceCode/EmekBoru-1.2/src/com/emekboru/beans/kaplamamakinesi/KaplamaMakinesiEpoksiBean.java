/**
 * 
 */
package com.emekboru.beans.kaplamamakinesi;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isemri.IsEmriEpoksiKaplama;
import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiEpoksi;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isemirleri.IsEmriEpoksiKaplamaManager;
import com.emekboru.jpaman.kaplamamakinesi.KaplamaMakinesiEpoksiManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "kaplamaMakinesiEpoksiBean")
@ViewScoped
public class KaplamaMakinesiEpoksiBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<KaplamaMakinesiEpoksi> allKaplamaMakinesiEpoksiList = new ArrayList<KaplamaMakinesiEpoksi>();
	private KaplamaMakinesiEpoksi kaplamaMakinesiEpoksiForm = new KaplamaMakinesiEpoksi();

	private KaplamaMakinesiEpoksi newEpoksi = new KaplamaMakinesiEpoksi();
	private List<KaplamaMakinesiEpoksi> epoksiList;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	private Pipe selectedPipe = new Pipe();

	private boolean kontrol = false;

	private String barkodNo = null;

	private IsEmriEpoksiKaplama selectedIsEmriKaplama = new IsEmriEpoksiKaplama();

	public KaplamaMakinesiEpoksiBean() {

	}

	public void addEpoksi(ActionEvent ae) {

		UICommand cmd = (UICommand) ae.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId != null) {
			PipeManager pipeMan = new PipeManager();
			selectedPipe = pipeMan.loadObject(Pipe.class, prmPipeId);
		}

		if (newEpoksi.getKaplamaYuzeyi() == -1) {

			System.out.println(newEpoksi.getKaplamaYuzeyi());
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_FATAL,
							"EPOKSİ KAPLAMA YAPILACAK YÜZEY SEÇİLMEDİ, LÜTFEN SEÇİP TEKRAR DENEYİNİZ!",
							null));
			System.out.println("KaplamaMakinesiEpoksiBean.addEpoksi-HATA");
			newEpoksi = new KaplamaMakinesiEpoksi();
			return;
		}

		try {
			KaplamaMakinesiEpoksiManager manager = new KaplamaMakinesiEpoksiManager();

			newEpoksi.setEklemeZamani(UtilInsCore.getTarihZaman());
			newEpoksi.setEkleyenKullanici(userBean.getUser().getId());
			newEpoksi.setPipe(selectedPipe);
			manager.enterNew(newEpoksi);
			newEpoksi = new KaplamaMakinesiEpoksi();

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"EPOKSİ KAPLAMA İŞLEMİ BAŞARIYLA EKLENMİŞTİR!"));

		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL,
					"EPOKSİ KAPLAMA İŞLEMİ EKLENEMEDİ!", null));
			System.out.println("KaplamaMakinesiEpoksiBean.addEpoksi-HATA");
		}

		fillTestList(prmPipeId);
	}

	public void addEpoksiSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		Boolean prmKontrol = kaplamaMakinesiEpoksiForm.getDurum();
		KaplamaMakinesiEpoksiManager epoksiManager = new KaplamaMakinesiEpoksiManager();

		if (prmKontrol == null) {

			try {

				kaplamaMakinesiEpoksiForm.setDurum(false);
				kaplamaMakinesiEpoksiForm.setKaplamaBaslamaZamani(UtilInsCore
						.getTarihZaman());
				kaplamaMakinesiEpoksiForm.setKaplamaBaslamaKullanici(userBean
						.getUser().getId());
				epoksiManager.updateEntity(kaplamaMakinesiEpoksiForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"EPOKSİ KAPLAMA İŞLEMİ BAŞARIYLA BAŞLANMIŞTIR!"));
			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"EPOKSİ KAPLAMA İŞLEMİ BAŞLANAMADI!", null));
				System.out
						.println("KaplamaMakinesiEpoksiBean.addEpoksiSonuc-HATA-BAŞLAMA");
			}
		} else if (prmKontrol == false) {

			try {

				kaplamaMakinesiEpoksiForm.setDurum(true);
				kaplamaMakinesiEpoksiForm.setKaplamaBitisZamani(UtilInsCore
						.getTarihZaman());
				kaplamaMakinesiEpoksiForm.setKaplamaBitisKullanici(userBean
						.getUser().getId());
				epoksiManager.updateEntity(kaplamaMakinesiEpoksiForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"EPOKSİ KAPLAMA İŞLEMİ BAŞARIYLA BİTİRİLMİŞTİR!"));

			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"EPOKSİ KAPLAMA İŞLEMİ BİTİRİLEMEDİ!", null));
				System.out
						.println("KaplamaMakinesiEpoksiBean.addEpoksiSonuc-HATA-BİTİŞ"
								+ e.toString());
			}

		}

		fillTestList(prmPipeId);
	}

	@SuppressWarnings("static-access")
	public void fillTestList(Integer prmPipeId) {

		KaplamaMakinesiEpoksiManager epoksiManager = new KaplamaMakinesiEpoksiManager();
		allKaplamaMakinesiEpoksiList = epoksiManager
				.getAllKaplamaMakinesiEpoksi(prmPipeId);
		epoksiManager.refreshCollection(allKaplamaMakinesiEpoksiList);
	}

	public void kaplamaListener(SelectEvent event) {
		updateButtonRender = true;
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!", null));
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			KaplamaMakinesiEpoksiManager epoksiManager = new KaplamaMakinesiEpoksiManager();
			allKaplamaMakinesiEpoksiList = epoksiManager
					.getAllKaplamaMakinesiEpoksi(selectedPipe.getPipeId());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
		}
	}

	public void deleteEpoksiSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		KaplamaMakinesiEpoksiManager epoksiManager = new KaplamaMakinesiEpoksiManager();

		if (kaplamaMakinesiEpoksiForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		epoksiManager.delete(kaplamaMakinesiEpoksiForm);
		kaplamaMakinesiEpoksiForm = new KaplamaMakinesiEpoksi();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"EPOKSİ KAPLAMA BAŞARIYLA SİLİNMİŞTİR!"));

		fillTestList(prmPipeId);
	}

	@SuppressWarnings("static-access")
	public void isEmriGoster() {

		IsEmriEpoksiKaplamaManager isEmriManager = new IsEmriEpoksiKaplamaManager();
		if (isEmriManager.getAllIsEmriEpoksiKaplama(
				selectedPipe.getSalesItem().getItemId()).size() > 0) {
			selectedIsEmriKaplama = isEmriManager.getAllIsEmriEpoksiKaplama(
					selectedPipe.getSalesItem().getItemId()).get(0);
		} else {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					" EPOKSİ KAPLAMA İŞ EMRİ BULUNAMADI, LÜTFEN EKLETİNİZ!",
					null));
			return;
		}
	}

	// setters getters

	public List<KaplamaMakinesiEpoksi> getAllKaplamaMakinesiEpoksiList() {
		return allKaplamaMakinesiEpoksiList;
	}

	public void setAllKaplamaMakinesiEpoksiList(
			List<KaplamaMakinesiEpoksi> allKaplamaMakinesiEpoksiList) {
		this.allKaplamaMakinesiEpoksiList = allKaplamaMakinesiEpoksiList;
	}

	public KaplamaMakinesiEpoksi getKaplamaMakinesiEpoksiForm() {
		return kaplamaMakinesiEpoksiForm;
	}

	public void setKaplamaMakinesiEpoksiForm(
			KaplamaMakinesiEpoksi kaplamaMakinesiEpoksiForm) {
		this.kaplamaMakinesiEpoksiForm = kaplamaMakinesiEpoksiForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	public boolean isKontrol() {
		return kontrol;
	}

	public void setKontrol(boolean kontrol) {
		this.kontrol = kontrol;
	}

	public String getBarkodNo() {
		return barkodNo;
	}

	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public KaplamaMakinesiEpoksi getNewEpoksi() {
		return newEpoksi;
	}

	public void setNewEpoksi(KaplamaMakinesiEpoksi newEpoksi) {
		this.newEpoksi = newEpoksi;
	}

	public List<KaplamaMakinesiEpoksi> getEpoksiList() {
		return epoksiList;
	}

	public void setEpoksiList(List<KaplamaMakinesiEpoksi> epoksiList) {
		this.epoksiList = epoksiList;
	}

	public IsEmriEpoksiKaplama getSelectedIsEmriKaplama() {
		return selectedIsEmriKaplama;
	}

	public void setSelectedIsEmriKaplama(
			IsEmriEpoksiKaplama selectedIsEmriKaplama) {
		this.selectedIsEmriKaplama = selectedIsEmriKaplama;
	}
}
