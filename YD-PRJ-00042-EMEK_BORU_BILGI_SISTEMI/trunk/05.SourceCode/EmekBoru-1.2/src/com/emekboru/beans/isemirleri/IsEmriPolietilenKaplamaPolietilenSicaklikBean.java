/**
 * 
 */
package com.emekboru.beans.isemirleri;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.isemri.IsEmriPolietilenKaplama;
import com.emekboru.jpa.isemri.IsEmriPolietilenKaplamaPolietilenSicaklik;
import com.emekboru.jpaman.isemirleri.IsEmriPolietilenKaplamaManager;
import com.emekboru.jpaman.isemirleri.IsEmriPolietilenKaplamaPolietilenSicaklikManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isEmriPolietilenKaplamaPolietilenSicaklikBean")
@ViewScoped
public class IsEmriPolietilenKaplamaPolietilenSicaklikBean implements
		Serializable {

	private static final long serialVersionUID = 1L;

	private IsEmriPolietilenKaplamaPolietilenSicaklik isEmriPolietilenKaplamaPolietilenSicaklikForm = new IsEmriPolietilenKaplamaPolietilenSicaklik();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	public IsEmriPolietilenKaplamaPolietilenSicaklikBean() {

	}

	public void addOrUpdateIsEmriPolietilenKaplamaPolietilenSicaklik(
			ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmIsEmriId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");

		IsEmriPolietilenKaplama isEmriPolietilenKaplama = new IsEmriPolietilenKaplamaManager()
				.loadObject(IsEmriPolietilenKaplama.class, prmIsEmriId);

		IsEmriPolietilenKaplamaPolietilenSicaklikManager isMan = new IsEmriPolietilenKaplamaPolietilenSicaklikManager();
		IsEmriPolietilenKaplamaManager kaplamaMan = new IsEmriPolietilenKaplamaManager();

		if (isEmriPolietilenKaplamaPolietilenSicaklikForm.getId() == null) {

			isEmriPolietilenKaplamaPolietilenSicaklikForm
					.setIsEmriPolietilenKaplama(isEmriPolietilenKaplama);
			isEmriPolietilenKaplamaPolietilenSicaklikForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			isEmriPolietilenKaplamaPolietilenSicaklikForm
					.setEkleyenEmployee(userBean.getUser());
			isEmriPolietilenKaplamaPolietilenSicaklikForm
					.setIsEmriPolietilenKaplama(isEmriPolietilenKaplama);

			isMan.enterNew(isEmriPolietilenKaplamaPolietilenSicaklikForm);

			isEmriPolietilenKaplama
					.setIsEmriPolietilenKaplamaPolietilenSicaklik(isEmriPolietilenKaplamaPolietilenSicaklikForm);
			kaplamaMan.updateEntity(isEmriPolietilenKaplama);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							"POLİETİLEN EXTRUDER 1 - 2 BÖLGELERİ SICAKLIKLARI BAŞARIYLA EKLENMİŞTİR!"));
		} else {

			isEmriPolietilenKaplamaPolietilenSicaklikForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			isEmriPolietilenKaplamaPolietilenSicaklikForm
					.setGuncelleyenEmployee(userBean.getUser());
			isMan.updateEntity(isEmriPolietilenKaplamaPolietilenSicaklikForm);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_WARN,
							"POLİETİLEN EXTRUDER 1 - 2 BÖLGELERİ SICAKLIKLARI BAŞARIYLA GÜNCELLENMİŞTİR!",
							null));
		}
	}

	public void deleteIsEmriPolietilenKaplamaPolietilenSicaklik() {

		IsEmriPolietilenKaplamaPolietilenSicaklikManager isMan = new IsEmriPolietilenKaplamaPolietilenSicaklikManager();

		if (isEmriPolietilenKaplamaPolietilenSicaklikForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		isMan.delete(isEmriPolietilenKaplamaPolietilenSicaklikForm);
		isEmriPolietilenKaplamaPolietilenSicaklikForm = new IsEmriPolietilenKaplamaPolietilenSicaklik();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"POLİETİLEN EXTRUDER 1 - 2 BÖLGELERİ SICAKLIKLARI BAŞARIYLA SİLİNMİŞTİR!",
						null));
	}

	@SuppressWarnings("static-access")
	public void loadPolietilenSicakliklar(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmIsEmriId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");

		IsEmriPolietilenKaplamaPolietilenSicaklikManager paramMan = new IsEmriPolietilenKaplamaPolietilenSicaklikManager();
		if (paramMan.getAllIsEmriPolietilenKaplamaPolietilenSicaklik(
				prmIsEmriId).size() > 0) {
			isEmriPolietilenKaplamaPolietilenSicaklikForm = paramMan
					.getAllIsEmriPolietilenKaplamaPolietilenSicaklik(
							prmIsEmriId).get(0);
		}
	}

	// setters getters

	public IsEmriPolietilenKaplamaPolietilenSicaklik getIsEmriPolietilenKaplamaPolietilenSicaklikForm() {
		return isEmriPolietilenKaplamaPolietilenSicaklikForm;
	}

	public void setIsEmriPolietilenKaplamaPolietilenSicaklikForm(
			IsEmriPolietilenKaplamaPolietilenSicaklik isEmriPolietilenKaplamaPolietilenSicaklikForm) {
		this.isEmriPolietilenKaplamaPolietilenSicaklikForm = isEmriPolietilenKaplamaPolietilenSicaklikForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
