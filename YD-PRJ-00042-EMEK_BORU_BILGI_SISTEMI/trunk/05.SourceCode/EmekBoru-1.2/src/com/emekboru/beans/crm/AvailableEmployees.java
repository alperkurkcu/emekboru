package com.emekboru.beans.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.Employee;
import com.emekboru.jpaman.EmployeeManager;
import com.emekboru.utils.FacesContextUtils;


@ManagedBean
@ViewScoped
public class AvailableEmployees implements Serializable{

	private static final long serialVersionUID = 6616016624248085282L;

	private List<Employee> employees;
	private Employee[] selectedEmployees;
	
	public AvailableEmployees(){
		employees = new ArrayList<Employee>();
	}
	
	public void load(){
		EmployeeManager manager = new EmployeeManager();
		Events eventsBean = 
				(Events) FacesContextUtils.getViewBean("events", Events.class);
		employees = manager.notHandled(eventsBean.getSelectedEvent().getEventId());
	}

	/**
	 * 		GETTERS AND SETTERS
	 */
	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Employee[] getSelectedEmployees() {
		return selectedEmployees;
	}

	public void setSelectedEmployees(Employee[] selectedEmployees) {
		this.selectedEmployees = selectedEmployees;
	}
}
