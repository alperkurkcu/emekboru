/**
 * 
 */
package com.emekboru.beans.employee;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Department;
import com.emekboru.jpa.Employee;
import com.emekboru.jpaman.DepartmentManager;
import com.emekboru.jpaman.EmployeeManager;

/**
 * @author kursat
 * 
 */

@ManagedBean(name = "employeeDepartmentSupervisorBean")
@ViewScoped
public class EmployeeDepartmentSupervisorBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private List<Employee> allEmployees = new ArrayList<Employee>();
	private List<Department> allDepartments = new ArrayList<Department>();
	private Employee[] selectedEmployees;

	private Department selectedDepartment = new Department();
	private Employee selectedSupervisor = new Employee();

	/**
	 * 
	 */
	public EmployeeDepartmentSupervisorBean() {

		EmployeeManager employeeManager = new EmployeeManager();
		// allEmployees = employeeManager.findAll(Employee.class);
		allEmployees = employeeManager.findByActivePassive(true);

		DepartmentManager departmentManager = new DepartmentManager();
		allDepartments = departmentManager.findAll(Department.class);

	}

	public void assignDepartmentToEmployees() {
		EmployeeManager employeeManager = new EmployeeManager();

		for (Employee employee : selectedEmployees) {
			employee.setDepartment(selectedDepartment);
			employeeManager.updateEntity(employee);
		}

		FacesContext.getCurrentInstance().addMessage(
				null,
				new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Seçilen çalışanlara departman atandı.", null));

	}

	public void assignSupervisorToEmployees() {
		EmployeeManager employeeManager = new EmployeeManager();

		for (Employee employee : selectedEmployees) {
			employee.setSupervisor(selectedSupervisor);
			employeeManager.updateEntity(employee);
		}

		FacesContext.getCurrentInstance().addMessage(
				null,
				new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Seçilen çalışanlara amir atandı.", null));

	}

	// ///////////////////////////////////////////////////
	// ///////// G E T T E R S - S E T T E R S ///////////
	// ///////////////////////////////////////////////////

	public List<Employee> getAllEmployees() {
		return allEmployees;
	}

	public List<Department> getAllDepartments() {
		return allDepartments;
	}

	public void setAllDepartments(List<Department> allDepartments) {
		this.allDepartments = allDepartments;
	}

	public Employee[] getSelectedEmployees() {
		return selectedEmployees;
	}

	public void setSelectedEmployees(Employee[] selectedEmployees) {
		this.selectedEmployees = selectedEmployees;
	}

	public Department getSelectedDepartment() {
		return selectedDepartment;
	}

	public void setSelectedDepartment(Department selectedDepartment) {
		this.selectedDepartment = selectedDepartment;
	}

	public Employee getSelectedSupervisor() {
		return selectedSupervisor;
	}

	public void setSelectedSupervisor(Employee selectedSupervisor) {
		this.selectedSupervisor = selectedSupervisor;
	}

	public void setAllEmployees(List<Employee> allEmployees) {
		this.allEmployees = allEmployees;
	}

	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

}
