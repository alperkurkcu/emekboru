/**
 * 
 */
package com.emekboru.beans.stok;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.stok.StokTanimlarBirim;
import com.emekboru.jpaman.stok.StokTanimlarBirimManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "stokTanimlarBirimBean")
@ViewScoped
public class StokTanimlarBirimBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<StokTanimlarBirim> allStokTanimlarBirimList = new ArrayList<StokTanimlarBirim>();

	private StokTanimlarBirim stokTanimlarBirimForm = new StokTanimlarBirim();

	StokTanimlarBirimManager formManager = new StokTanimlarBirimManager();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	public StokTanimlarBirimBean() {

		fillTestList();
	}

	public void addForm() {

		stokTanimlarBirimForm = new StokTanimlarBirim();
		updateButtonRender = false;
	}

	public void addOrUpdateForm() {

		if (stokTanimlarBirimForm.getId() == null) {

			stokTanimlarBirimForm.setEklemeZamani(UtilInsCore.getTarihZaman());
			stokTanimlarBirimForm.setEkleyenKullanici(userBean.getUser()
					.getId());

			formManager.enterNew(stokTanimlarBirimForm);

			this.stokTanimlarBirimForm = new StokTanimlarBirim();
			FacesContextUtils.addInfoMessage("FormSubmitMessage");

		} else {

			stokTanimlarBirimForm.setGuncellemeZamani(UtilInsCore
					.getTarihZaman());
			stokTanimlarBirimForm.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			formManager.updateEntity(stokTanimlarBirimForm);

			FacesContextUtils.addInfoMessage("FormUpdateMessage");
		}

		fillTestList();
	}

	public void deleteForm() {

		if (stokTanimlarBirimForm.getId() == null) {

			FacesContextUtils.addWarnMessage("NoSelectMessage");
			return;
		}
		formManager.delete(stokTanimlarBirimForm);
		fillTestList();
		FacesContextUtils.addWarnMessage("TestDeleteMessage");
	}

	public void fillTestList() {

		StokTanimlarBirimManager manager = new StokTanimlarBirimManager();
		allStokTanimlarBirimList = manager.getAllStokTanimlarBirim();
	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	// setters getters

	public List<StokTanimlarBirim> getAllStokTanimlarBirimList() {
		return allStokTanimlarBirimList;
	}

	public void setAllStokTanimlarBirimList(
			List<StokTanimlarBirim> allStokTanimlarBirimList) {
		this.allStokTanimlarBirimList = allStokTanimlarBirimList;
	}

	public StokTanimlarBirim getStokTanimlarBirimForm() {
		return stokTanimlarBirimForm;
	}

	public void setStokTanimlarBirimForm(StokTanimlarBirim stokTanimlarBirimForm) {
		this.stokTanimlarBirimForm = stokTanimlarBirimForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
