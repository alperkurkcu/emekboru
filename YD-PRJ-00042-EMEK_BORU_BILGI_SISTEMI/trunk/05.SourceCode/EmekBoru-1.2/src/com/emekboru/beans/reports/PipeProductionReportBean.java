package com.emekboru.beans.reports;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.math.RandomUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.emekboru.config.AVkey;
import com.emekboru.config.Attribute;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Machine;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpaman.MachineManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.utils.DateTrans;

@ManagedBean(name = "pipeProductionReportBean")
@ViewScoped
public class PipeProductionReportBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Integer TOP_ROW_OFFSET = 9;
	private static final Integer BOTTOM_ROW_OFFSET = 10;

	@EJB
	private ConfigBean config;

	private Machine selectedMachine;
	private List<Pipe> pipeList;
	private String language;
	private String sortingOrder;
	private Date startDate;
	private Date endDate;

	public PipeProductionReportBean() {
		selectedMachine = new Machine();
		pipeList = new ArrayList<Pipe>();
		language = new String();
		sortingOrder = new String();
	}

	public void genPipeProductionReport() {

		Attribute attr = null;
		POIFSFileSystem fs = null;

		try {

			if (language.equals("en")) {

				attr = config.findAttr(AVkey.MAN_REPORT_EN);
				fs = new POIFSFileSystem(new FileInputStream(new File(
						attr.getValue())));
			}

			if (language.equals("tr")) {

				attr = config.findAttr(AVkey.MAN_REPORT_TR);
				fs = new POIFSFileSystem(new FileInputStream(new File(
						"D://manform_tr.xls")));
			}

			HSSFWorkbook workbook = new HSSFWorkbook(fs);
			HSSFSheet sheet = workbook.getSheetAt(0);
			HSSFRow rowTop = sheet.getRow(9);
			HSSFCell leftTopCell = rowTop.getCell(0);
			HSSFCell centerTopCell = rowTop.getCell(1);
			HSSFCell rightTopCell = rowTop.getCell(4);

			HSSFRow bottomRow = sheet.getRow(10);
			HSSFCell leftBottomCell = bottomRow.getCell(0);
			HSSFCell centerBottomCell = bottomRow.getCell(1);
			HSSFCell rightBottomCell = bottomRow.getCell(4);
			HSSFRow resultRow = sheet.getRow(7);
			HSSFCell resultCell = resultRow.getCell(4);
			findPipes();
			findSelectedMachine();

			Double lengthSum = new Double(0);
			Double weightSum = new Double(0);

			for (int i = 0; i < pipeList.size(); i++) {

				Pipe pipe = pipeList.get(i);
				HSSFRow row = sheet.createRow(i + TOP_ROW_OFFSET);
				HSSFCell cell1 = row.createCell(0);
				HSSFCell cell2 = row.createCell(1);
				HSSFCell cell3 = row.createCell(2);
				HSSFCell cell4 = row.createCell(3);
				HSSFCell cell5 = row.createCell(4);
				cell1.setCellStyle(leftTopCell.getCellStyle());
				cell2.setCellStyle(centerTopCell.getCellStyle());
				cell3.setCellStyle(centerTopCell.getCellStyle());
				cell4.setCellStyle(centerTopCell.getCellStyle());
				cell5.setCellStyle(rightTopCell.getCellStyle());

				cell1.setCellValue(pipe.getSalesItem().getProposal()
						.getSalesOrder().getOrderNo()
						+ "/"
						+ pipe.getSalesItem().getItemNo()
						+ "-"
						+ pipe.getPipeIndex());
				cell2.setCellValue(String.valueOf(pipe.getEtKalinligi())
						+ " x " + String.valueOf(pipe.getDisCap()));
				cell3.setCellValue(String.valueOf(pipe.getBoy()));
				cell4.setCellValue(String.valueOf(pipe.getAgirligi()));
				cell5.setCellValue(" ");

				lengthSum += pipe.getBoy();
				weightSum += pipe.getAgirligi();
			}
			// if its the last row, add one empty row and calculate results
			HSSFRow row1 = sheet.createRow(pipeList.size() + TOP_ROW_OFFSET);
			HSSFCell cell11 = row1.createCell(0);
			HSSFCell cell21 = row1.createCell(1);
			HSSFCell cell31 = row1.createCell(2);
			HSSFCell cell41 = row1.createCell(3);
			HSSFCell cell51 = row1.createCell(4);
			cell11.setCellStyle(leftBottomCell.getCellStyle());
			cell21.setCellStyle(centerBottomCell.getCellStyle());
			cell31.setCellStyle(centerBottomCell.getCellStyle());
			cell41.setCellStyle(centerBottomCell.getCellStyle());
			cell51.setCellStyle(rightBottomCell.getCellStyle());

			HSSFRow lastRow = sheet.createRow(pipeList.size()
					+ BOTTOM_ROW_OFFSET);
			HSSFCell lengthSumCell = lastRow.createCell(2);
			HSSFCell weightSumCell = lastRow.createCell(3);
			lengthSumCell.setCellStyle(resultCell.getCellStyle());
			weightSumCell.setCellStyle(resultCell.getCellStyle());
			lengthSumCell.setCellValue(lengthSum.toString());
			weightSumCell.setCellValue(weightSum.toString());

			// set the dates and the machine name:
			HSSFRow machineNameRow = sheet.getRow(4);
			HSSFCell machineNameCell = machineNameRow.getCell(0);
			machineNameCell.setCellValue(selectedMachine.getName());

			HSSFRow dateRow = sheet.getRow(6);
			HSSFCell startDateCell = dateRow.getCell(1);
			HSSFCell endDateCell = dateRow.getCell(3);
			startDateCell
					.setCellValue(DateTrans.getSimpleFormatDate(startDate));
			endDateCell.setCellValue(DateTrans.getSimpleFormatDate(endDate));

			attr = config.findAttr(AVkey.TEST_REPORT_EN);
			FileOutputStream output = new FileOutputStream(new File(
					"D://manform_tr" + RandomUtils.nextInt() + ".xls"));
			workbook.write(output);
			output.close();
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect(config.getPageUrl().PIPE_PRODUCTION_REPORT);
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
	}

	public void findPipes() {

		PipeManager pipeManager = new PipeManager();
		// pipeList = pipeManager.findPipeProducedBetweenDatesFromMachine(
		// selectedMachine.getMachineId(), startDate, endDate);
		pipeList = pipeManager.allPipesInMachine(selectedMachine, startDate,
				endDate);
	}

	public void findSelectedMachine() {

		MachineManager machineManager = new MachineManager();
		selectedMachine = (Machine) machineManager.findByField(Machine.class,
				"machineId", selectedMachine.getMachineId()).get(0);
	}

	public void sendReportToStream(HSSFWorkbook workbook) {
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			HttpServletRequest request = (HttpServletRequest) context
					.getExternalContext().getRequest();
			context.getExternalContext().redirect(
					config.getPageUrl().TEST__REPORT_PAGE);

		} catch (IOException e) {
		}
	}

	/**
	 * GETTERS AND SETTERS
	 */

	public Machine getSelectedMachine() {
		return selectedMachine;
	}

	public void setSelectedMachine(Machine selectedMachine) {
		this.selectedMachine = selectedMachine;
	}

	public List<Pipe> getPipeList() {
		return pipeList;
	}

	public void setPipeList(List<Pipe> pipeList) {
		this.pipeList = pipeList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getSortingOrder() {
		return sortingOrder;
	}

	public void setSortingOrder(String sortingOrder) {
		this.sortingOrder = sortingOrder;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
