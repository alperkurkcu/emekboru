/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaMpqtKaplamaKalinligliSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaMpqtKaplamaKalinligliSonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testKaplamaMpqtKaplamaKalinligliSonucBean")
@ViewScoped
public class TestKaplamaMpqtKaplamaKalinligliSonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private TestKaplamaMpqtKaplamaKalinligliSonuc testKaplamaMpqtKaplamaKalinligliSonucForm = new TestKaplamaMpqtKaplamaKalinligliSonuc();
	private List<TestKaplamaMpqtKaplamaKalinligliSonuc> allTestKaplamaMpqtKaplamaKalinligliSonucList = new ArrayList<TestKaplamaMpqtKaplamaKalinligliSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	float ortalamaEpoksi = 0F;
	float ortalamaYapistirici = 0F;
	float ortalamaPEPP = 0F;

	public void addOrUpdateTestKaplamaMpqtKaplamaKalinligliSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaMpqtKaplamaKalinligliSonucManager manager = new TestKaplamaMpqtKaplamaKalinligliSonucManager();

		if (testKaplamaMpqtKaplamaKalinligliSonucForm.getId() == null) {

			testKaplamaMpqtKaplamaKalinligliSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaMpqtKaplamaKalinligliSonucForm
					.setEkleyenKullanici(userBean.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaMpqtKaplamaKalinligliSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaMpqtKaplamaKalinligliSonucForm.setBagliTestId(bagliTest);
			testKaplamaMpqtKaplamaKalinligliSonucForm
					.setBagliGlobalId(bagliTest);

			ortalamaBul();

			manager.enterNew(testKaplamaMpqtKaplamaKalinligliSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaMpqtKaplamaKalinligliSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaMpqtKaplamaKalinligliSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());

			ortalamaBul();

			manager.updateEntity(testKaplamaMpqtKaplamaKalinligliSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	private void ortalamaBul() {

		ortalamaEpoksi = (testKaplamaMpqtKaplamaKalinligliSonucForm
				.getEpoksiMin().floatValue() + testKaplamaMpqtKaplamaKalinligliSonucForm
				.getEpoksiMax().floatValue()) / 2;
		testKaplamaMpqtKaplamaKalinligliSonucForm.setEpoksiOrt(BigDecimal
				.valueOf(ortalamaEpoksi));

		ortalamaYapistirici = (testKaplamaMpqtKaplamaKalinligliSonucForm
				.getYapistiriciMin().floatValue() + testKaplamaMpqtKaplamaKalinligliSonucForm
				.getYapistiriciMax().floatValue()) / 2;
		testKaplamaMpqtKaplamaKalinligliSonucForm.setYapistiriciOrt(BigDecimal
				.valueOf(ortalamaYapistirici));

		ortalamaPEPP = (testKaplamaMpqtKaplamaKalinligliSonucForm
				.getPolietilenPolipropilenMin().floatValue() + testKaplamaMpqtKaplamaKalinligliSonucForm
				.getPolietilenPolipropilenMax().floatValue()) / 2;
		testKaplamaMpqtKaplamaKalinligliSonucForm
				.setPolietilenPolipropilenOrt(BigDecimal.valueOf(ortalamaPEPP));
	}

	public void addTestKaplamaMpqtKaplamaKalinligliSonuc() {

		testKaplamaMpqtKaplamaKalinligliSonucForm = new TestKaplamaMpqtKaplamaKalinligliSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaMpqtKaplamaKalinligliSonucListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaMpqtKaplamaKalinligliSonucList = TestKaplamaMpqtKaplamaKalinligliSonucManager
				.getAllTestKaplamaMpqtKaplamaKalinligliSonuc(globalId, pipeId);
		testKaplamaMpqtKaplamaKalinligliSonucForm = new TestKaplamaMpqtKaplamaKalinligliSonuc();
	}

	public void deleteTestKaplamaMpqtKaplamaKalinligliSonuc() {

		bagliTestId = testKaplamaMpqtKaplamaKalinligliSonucForm
				.getBagliGlobalId().getId();

		if (testKaplamaMpqtKaplamaKalinligliSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaMpqtKaplamaKalinligliSonucManager manager = new TestKaplamaMpqtKaplamaKalinligliSonucManager();
		manager.delete(testKaplamaMpqtKaplamaKalinligliSonucForm);
		testKaplamaMpqtKaplamaKalinligliSonucForm = new TestKaplamaMpqtKaplamaKalinligliSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setters getters

	/**
	 * @return the testKaplamaMpqtKaplamaKalinligliSonucForm
	 */
	public TestKaplamaMpqtKaplamaKalinligliSonuc getTestKaplamaMpqtKaplamaKalinligliSonucForm() {
		return testKaplamaMpqtKaplamaKalinligliSonucForm;
	}

	/**
	 * @param testKaplamaMpqtKaplamaKalinligliSonucForm
	 *            the testKaplamaMpqtKaplamaKalinligliSonucForm to set
	 */
	public void setTestKaplamaMpqtKaplamaKalinligliSonucForm(
			TestKaplamaMpqtKaplamaKalinligliSonuc testKaplamaMpqtKaplamaKalinligliSonucForm) {
		this.testKaplamaMpqtKaplamaKalinligliSonucForm = testKaplamaMpqtKaplamaKalinligliSonucForm;
	}

	/**
	 * @return the allTestKaplamaMpqtKaplamaKalinligliSonucList
	 */
	public List<TestKaplamaMpqtKaplamaKalinligliSonuc> getAllTestKaplamaMpqtKaplamaKalinligliSonucList() {
		return allTestKaplamaMpqtKaplamaKalinligliSonucList;
	}

	/**
	 * @param allTestKaplamaMpqtKaplamaKalinligliSonucList
	 *            the allTestKaplamaMpqtKaplamaKalinligliSonucList to set
	 */
	public void setAllTestKaplamaMpqtKaplamaKalinligliSonucList(
			List<TestKaplamaMpqtKaplamaKalinligliSonuc> allTestKaplamaMpqtKaplamaKalinligliSonucList) {
		this.allTestKaplamaMpqtKaplamaKalinligliSonucList = allTestKaplamaMpqtKaplamaKalinligliSonucList;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
