package com.emekboru.beans.utils;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.Ulkeler;
import com.emekboru.jpaman.UlkelerManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "ulkelerBean")
@SessionScoped
public class UlkelerBean {

	private Ulkeler ulkelerForm = new Ulkeler();
	private List<Ulkeler> allUlkelerList = new ArrayList<Ulkeler>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender = false;

	public UlkelerBean() {

		allUlkelerList = UlkelerManager.getAllUlkeler();
		// ulkelerForm = new Ulkeler();
	}

	public void addOrUpdateUlkeler(ActionEvent e) {

		if (ulkelerForm.getId() == null) {

			Ulkeler ulkeler = new Ulkeler();
			ulkeler.setUlkeKodu(ulkelerForm.getUlkeKodu());
			ulkeler.setUlkeIsmi(ulkelerForm.getUlkeIsmi());

			ulkeler.setEklemeZamani(new java.sql.Timestamp(new java.util.Date()
					.getTime()));
			ulkeler.setEkleyenKullanici(userBean.getUser().getId());

			UlkelerManager.addOrUpdateUlkeler(ulkeler);
			FacesContextUtils.addInfoMessage("SubmitMessage");
		} else {

			ulkelerForm.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			ulkelerForm.setGuncelleyenKullanici(userBean.getUser().getId());
			UlkelerManager.addOrUpdateUlkeler(ulkelerForm);
			FacesContextUtils.addInfoMessage("UpdateItemMessage");
		}
		fillTestList();
	}

	public void fillTestList() {
		allUlkelerList = UlkelerManager.getAllUlkeler();
	}

	public void deleteUlkeler() {

		if (ulkelerForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		UlkelerManager.deleteUlkeler(ulkelerForm);
		ulkelerForm = new Ulkeler();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		fillTestList();
	}

	public void ulkeListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void addUlkeler() {

		ulkelerForm = new Ulkeler();
		updateButtonRender = false;
	}

	// setters getters

	public Ulkeler getUlkelerForm() {
		return ulkelerForm;
	}

	public void setUlkelerForm(Ulkeler ulkelerForm) {
		this.ulkelerForm = ulkelerForm;
	}

	public List<Ulkeler> getAllUlkelerList() {
		return allUlkelerList;
	}

	public void setAllUlkelerList(List<Ulkeler> allUlkelerList) {
		this.allUlkelerList = allUlkelerList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
