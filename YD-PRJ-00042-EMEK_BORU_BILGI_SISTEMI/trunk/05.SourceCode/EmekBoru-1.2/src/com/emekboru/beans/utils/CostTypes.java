package com.emekboru.beans.utils;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;

import com.emekboru.jpa.CostType;
import com.emekboru.jpaman.CostTypeManager;
import com.emekboru.jpaman.Factory;
import com.emekboru.utils.FacesContextUtils;


@ManagedBean(name = "costTypes")
@SessionScoped
public class CostTypes implements Serializable{

	private static final long serialVersionUID = -8546008562480907380L;
	
	private List<CostType> 	costTypes;
	private CostType 		selected;
	private CostType		newType;
	
	public CostTypes(){
		CostTypeManager manager = new CostTypeManager();
		costTypes = manager.findAll(CostType.class);
		selected  = new CostType();
		newType	  = new CostType();
	}
	
	public void load(){
		
		CostTypeManager manager = new CostTypeManager();
		costTypes = manager.findAll(CostType.class);
		System.out.println("how many cost types: "+costTypes.size());
	}

	public void add(){
		
		CostTypeManager manager = new CostTypeManager();
		manager.enterNew(newType);
		costTypes.add(newType);
		newType = new CostType();
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}
	
	public void update(){
		
		CostTypeManager manager = new CostTypeManager();
		manager.updateEntity(selected);
		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}
	
	public void delete(){
	
		if(selected.getCostTypeId()==null){
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		EntityManager manager = Factory.getInstance().createEntityManager();
		selected = manager.find(CostType.class, selected.getCostTypeId());
		manager.getTransaction().begin();
		if(selected.getEventCostBreakdowns().size()>0){
			FacesContextUtils.addErrorMessage("DeleteFailMessage");
			return;
		}
		manager.remove(selected);
		manager.getTransaction().commit();
		manager.close();
		costTypes.remove(selected);
		selected = new CostType();
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}
	
	public CostType find(int id){

		for (CostType type : costTypes) {
			if(type.getCostTypeId() == id)
				return type;
		}
		return null;
	}
	
	/**
	 * 		GETTERS AND SETTERS
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<CostType> getCostTypes() {
		return costTypes;
	}

	public void setCostTypes(List<CostType> costTypes) {
		this.costTypes = costTypes;
	}

	public CostType getSelected() {
		return selected;
	}

	public void setSelected(CostType selected) {
		this.selected = selected;
	}

	public CostType getNewType() {
		return newType;
	}

	public void setNewType(CostType newType) {
		this.newType = newType;
	}
}
