/**
 * 
 */
package com.emekboru.beans.coatingmaterials;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.coatingmaterials.KaplamaMalzemeKullanmaPe;
import com.emekboru.jpaman.CoatRawMaterialManager;
import com.emekboru.jpaman.coatingmaterials.KaplamaMalzemeKullanmaPeManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "kaplamaMalzemeKullanmaPeBean")
@ViewScoped
public class KaplamaMalzemeKullanmaPeBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<KaplamaMalzemeKullanmaPe> allKaplamaMalzemeKullanmaPeList = new ArrayList<KaplamaMalzemeKullanmaPe>();
	private KaplamaMalzemeKullanmaPe kaplamaMalzemeKullanmaPeForm = new KaplamaMalzemeKullanmaPe();

	private CoatRawMaterial selectedMaterial = new CoatRawMaterial();
	private List<CoatRawMaterial> coatRawMaterialList;
	private List<CoatRawMaterial> polietilen;
	private List<CoatRawMaterial> yapistirici;
	private List<CoatRawMaterial> epoksiToz;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	private BigDecimal kullanilanMiktar;

	private boolean durum = false;

	public KaplamaMalzemeKullanmaPeBean() {

		coatRawMaterialList = new ArrayList<CoatRawMaterial>();
		CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
		polietilen = materialManager.findUnfinishedCoatMaterialByType(1000);
		yapistirici = materialManager.findUnfinishedCoatMaterialByType(1002);
		epoksiToz = materialManager.findUnfinishedCoatMaterialByType(300001);

		coatRawMaterialList.addAll(polietilen);
		coatRawMaterialList.addAll(yapistirici);
		coatRawMaterialList.addAll(epoksiToz);
	}

	public void addNew() {
		kaplamaMalzemeKullanmaPeForm = new KaplamaMalzemeKullanmaPe();
		updateButtonRender = false;
	}

	public void addMalzemeKullanmaSonuc(ActionEvent e) {
		
		if (kaplamaMalzemeKullanmaPeForm.getHarcananMiktar() == null) {
			kaplamaMalzemeKullanmaPeForm.setHarcananMiktar(BigDecimal.ZERO);
		}

		if (!malzemeKontrol()) {
			return;
		}

		KaplamaMalzemeKullanmaPeManager malzemeKullanmaManager = new KaplamaMalzemeKullanmaPeManager();

		if (kaplamaMalzemeKullanmaPeForm.getId() == null) {

			try {
				kaplamaMalzemeKullanmaPeForm.setEklemeZamani(UtilInsCore
						.getTarihZaman());
				kaplamaMalzemeKullanmaPeForm.setEkleyenKullanici(userBean
						.getUser().getId());
				kaplamaMalzemeKullanmaPeForm
						.setCoatRawMaterial(selectedMaterial);
				malzemeKullanmaManager.enterNew(kaplamaMalzemeKullanmaPeForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"PE MALZEMESİ İŞLEMİ BAŞARIYLA EKLENMİŞTİR!"));

				kullanilanMiktar = kaplamaMalzemeKullanmaPeForm
						.getHarcananMiktar();
				durum = true;
				malzemeGuncelle();

			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"PE MALZEMESİ İŞLEMİ EKLENEMEDİ!", null));
				System.out
						.println("KaplamaMalzemeKullanmaPeBean.addMalzemeKullanmaSonuc-HATA-EKLEME");
			}
		} else {

			try {

				kaplamaMalzemeKullanmaPeForm.setGuncellemeZamani(UtilInsCore
						.getTarihZaman());
				kaplamaMalzemeKullanmaPeForm.setGuncelleyenKullanici(userBean
						.getUser().getId());
				malzemeKullanmaManager
						.updateEntity(kaplamaMalzemeKullanmaPeForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"PE MALZEMESİ İŞLEMİ BAŞARIYLA GÜNCELLENMİŞTİR!"));

				kullanilanMiktar = kaplamaMalzemeKullanmaPeForm
						.getHarcananMiktar();
				durum = true;
				malzemeGuncelle();

			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"PE MALZEMESİ KULLANMA İŞLEMİ GÜNCELLENEMEDİ!", null));
				System.out
						.println("KaplamaMalzemeKullanmaPeBean.addMalzemeKullanmaSonuc-HATA-GÜNCELLEME"
								+ e.toString());
			}

		}

		fillTestList();
	}

	@SuppressWarnings("static-access")
	public void fillTestList() {

		KaplamaMalzemeKullanmaPeManager malzemeKullanmaManager = new KaplamaMalzemeKullanmaPeManager();
		allKaplamaMalzemeKullanmaPeList = malzemeKullanmaManager
				.getAllKaplamaMalzemeKullanmaPe();
	}

	public void kaplamaListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void deleteMalzemeKullanmaSonuc(ActionEvent e) {

		KaplamaMalzemeKullanmaPeManager malzemeKullanmaManager = new KaplamaMalzemeKullanmaPeManager();

		if (kaplamaMalzemeKullanmaPeForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		kullanilanMiktar = kaplamaMalzemeKullanmaPeForm.getHarcananMiktar();

		durum = false;
		malzemeGuncelle();

		malzemeKullanmaManager.delete(kaplamaMalzemeKullanmaPeForm);
		kaplamaMalzemeKullanmaPeForm = new KaplamaMalzemeKullanmaPe();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"PE MALZEMESİ KULLANMA İŞLEMİ BAŞARIYLA SİLİNMİŞTİR!"));

		fillTestList();
	}

	public void malzemeGuncelle() {

		CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
		if (durum) {// enternew
			selectedMaterial.setRemainingAmount(selectedMaterial
					.getRemainingAmount() - kullanilanMiktar.doubleValue());
			materialManager.updateEntity(selectedMaterial);
		} else if (!durum) {// delete
			kaplamaMalzemeKullanmaPeForm.getCoatRawMaterial()
					.setRemainingAmount(
							kaplamaMalzemeKullanmaPeForm.getCoatRawMaterial()
									.getRemainingAmount()
									+ kullanilanMiktar.doubleValue());
			materialManager.updateEntity(kaplamaMalzemeKullanmaPeForm
					.getCoatRawMaterial());
		}
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"PE MALZEMESİ BAŞARIYLA GÜNCELLENMİŞTİR!"));
	}

	public boolean malzemeKontrol() {

		FacesContext context = FacesContext.getCurrentInstance();
		if (kaplamaMalzemeKullanmaPeForm.getHarcananMiktar().floatValue() > selectedMaterial
				.getRemainingAmount()) {
			context.addMessage(
					null,
					new FacesMessage(
							"KULLANILACAK MİKTAR KALAN MALZEME MİKTARINDAN BÜYÜKTÜR! LÜTFEN KONTROL EDİNİZ!"));
			return false;
		} else {
			return true;
		}
	}

	// getters setters

	/**
	 * @return the allKaplamaMalzemeKullanmaPeList
	 */
	public List<KaplamaMalzemeKullanmaPe> getAllKaplamaMalzemeKullanmaPeList() {
		return allKaplamaMalzemeKullanmaPeList;
	}

	/**
	 * @param allKaplamaMalzemeKullanmaPeList
	 *            the allKaplamaMalzemeKullanmaPeList to set
	 */
	public void setAllKaplamaMalzemeKullanmaPeList(
			List<KaplamaMalzemeKullanmaPe> allKaplamaMalzemeKullanmaPeList) {
		this.allKaplamaMalzemeKullanmaPeList = allKaplamaMalzemeKullanmaPeList;
	}

	/**
	 * @return the kaplamaMalzemeKullanmaPeForm
	 */
	public KaplamaMalzemeKullanmaPe getKaplamaMalzemeKullanmaPeForm() {
		return kaplamaMalzemeKullanmaPeForm;
	}

	/**
	 * @param kaplamaMalzemeKullanmaPeForm
	 *            the kaplamaMalzemeKullanmaPeForm to set
	 */
	public void setKaplamaMalzemeKullanmaPeForm(
			KaplamaMalzemeKullanmaPe kaplamaMalzemeKullanmaPeForm) {
		this.kaplamaMalzemeKullanmaPeForm = kaplamaMalzemeKullanmaPeForm;
	}

	/**
	 * @return the selectedMaterial
	 */
	public CoatRawMaterial getSelectedMaterial() {
		return selectedMaterial;
	}

	/**
	 * @param selectedMaterial
	 *            the selectedMaterial to set
	 */
	public void setSelectedMaterial(CoatRawMaterial selectedMaterial) {
		this.selectedMaterial = selectedMaterial;
	}

	/**
	 * @return the coatRawMaterialList
	 */
	public List<CoatRawMaterial> getCoatRawMaterialList() {
		return coatRawMaterialList;
	}

	/**
	 * @param coatRawMaterialList
	 *            the coatRawMaterialList to set
	 */
	public void setCoatRawMaterialList(List<CoatRawMaterial> coatRawMaterialList) {
		this.coatRawMaterialList = coatRawMaterialList;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the kullanilanMiktar
	 */
	public BigDecimal getKullanilanMiktar() {
		return kullanilanMiktar;
	}

	/**
	 * @param kullanilanMiktar
	 *            the kullanilanMiktar to set
	 */
	public void setKullanilanMiktar(BigDecimal kullanilanMiktar) {
		this.kullanilanMiktar = kullanilanMiktar;
	}

	/**
	 * @return the durum
	 */
	public boolean isDurum() {
		return durum;
	}

	/**
	 * @param durum
	 *            the durum to set
	 */
	public void setDurum(boolean durum) {
		this.durum = durum;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
