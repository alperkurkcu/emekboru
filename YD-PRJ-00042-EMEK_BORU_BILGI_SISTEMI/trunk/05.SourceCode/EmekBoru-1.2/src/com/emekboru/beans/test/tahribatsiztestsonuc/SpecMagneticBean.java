/**
 * 
 */
package com.emekboru.beans.test.tahribatsiztestsonuc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.tahribatsiztestsonuc.SpecMagnetic;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.SpecMagneticManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "specMagneticBean")
@ViewScoped
public class SpecMagneticBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<SpecMagnetic> allSpecList = new ArrayList<SpecMagnetic>();
	private SpecMagnetic specForm = new SpecMagnetic();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender = false;

	public SpecMagneticBean() {
	}

	public void specListener(SelectEvent event) {
		updateButtonRender = true;
	}

	@SuppressWarnings("static-access")
	public void addSpec(Integer prmPipeId) {

		SpecMagneticManager specManager = new SpecMagneticManager();
		if (specManager.getAllSpecMagnetic(prmPipeId).size() == 0) {
			specForm = new SpecMagnetic();
		} else {
			specForm = specManager.getAllSpecMagnetic(prmPipeId).get(0);
		}
		updateButtonRender = false;
	}

	public void addOrUpdateSpec(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);

		SpecMagneticManager manager = new SpecMagneticManager();
		if (specForm.getId() == null) {
			specForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			specForm.setEkleyenKullanici(userBean.getUser().getId());
			specForm.setPipe(pipe);

			manager.enterNew(specForm);
			FacesContextUtils.addInfoMessage("SubmitMessage");
		} else {
			specForm.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			specForm.setGuncelleyenKullanici(userBean.getUser().getId());
			manager.updateEntity(specForm);
			FacesContextUtils.addInfoMessage("UpdateItemMessage");
		}
		fillTestList(pipe.getPipeId());
	}

	@SuppressWarnings("static-access")
	public void fillTestList(Integer prmPipeId) {
		SpecMagneticManager manager = new SpecMagneticManager();
		allSpecList = manager.getAllSpecMagnetic(prmPipeId);
		manager.refreshCollection(allSpecList);
		specForm = new SpecMagnetic();
	}

	public void deleteSpec() {

		SpecMagneticManager manager = new SpecMagneticManager();
		if (specForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		manager.delete(specForm);
		specForm = new SpecMagnetic();
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	// setters getters

	/**
	 * @return the allSpecList
	 */
	public List<SpecMagnetic> getAllSpecList() {
		return allSpecList;
	}

	/**
	 * @param allSpecList
	 *            the allSpecList to set
	 */
	public void setAllSpecList(List<SpecMagnetic> allSpecList) {
		this.allSpecList = allSpecList;
	}

	/**
	 * @return the specForm
	 */
	public SpecMagnetic getSpecForm() {
		return specForm;
	}

	/**
	 * @param specForm
	 *            the specForm to set
	 */
	public void setSpecForm(SpecMagnetic specForm) {
		this.specForm = specForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
