package com.emekboru.beans.test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.sales.order.SalesOrderBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.DestructiveTestsSpec;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.TestBukmeSonuc;
import com.emekboru.jpaman.DestructiveTestsSpecManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.TestBukmeSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testBukmeSonucBean")
@ViewScoped
public class TestBukmeSonucBean {

	private List<TestBukmeSonuc> allBukmeTestSonucList = new ArrayList<TestBukmeSonuc>();
	private TestBukmeSonuc testBukmeSonucForm = new TestBukmeSonuc();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;
	private boolean updateButtonRender;

	@EJB
	private ConfigBean config;

	private String path;
	private String privatePath;

	private File theFile = null;
	OutputStream output = null;
	InputStream input = null;

	@SuppressWarnings("unused")
	private File[] fileArray;

	private StreamedContent downloadedFile;
	private String downloadedFileName;

	SalesOrderBean sob = new SalesOrderBean();

	Integer prmGlobalId = null;
	Integer prmTestId = null;
	Integer prmPipeId = null;

	private Pipe pipe = null;
	private DestructiveTestsSpec bagliTest = null;
	private String sampleNo = null;

	public void sampleNoOlustur() {
		Integer count = 0;
		TestBukmeSonucManager manager = new TestBukmeSonucManager();
		count = manager.getAllBukmeTestSonuc(prmGlobalId,
				prmPipeId).size() + 1;
		sampleNo = bagliTest.getCode() + pipe.getPipeIndex() + "-" + count;
	}

	public TestBukmeSonucBean() {

		privatePath = File.separatorChar + "testDocs" + File.separatorChar
				+ "uploadedDocuments" + File.separatorChar + "bukmeSonuc"
				+ File.separatorChar;
	}

	@PostConstruct
	public void load() {
		System.out.println("testBukmeSonucBean.load()");

		try {
			// this.setPath(config.getConfig().getFolder().getAbsolutePath()
			// + privatePath);

			if (System.getProperty("os.name").startsWith("Windows")) {

				this.setPath("C:/emekfiles" + privatePath);
			} else {

				this.setPath("/home/emekboru/Desktop/emekfiles" + privatePath);
			}

			System.out.println("Path for Upload File (BukmeSonuc):			" + path);
		} catch (Exception ex) {
			System.out.println("Path for Upload File (BukmeSonuc):			"
					+ FacesContext.getCurrentInstance().getExternalContext()
							.getRealPath(privatePath));

			System.out.print(path);
			System.out.println("CAUSE OF " + ex.toString());
		}

		theFile = new File(path);
		if (!theFile.isDirectory()) {
			theFile.mkdirs();
		}

		fileArray = theFile.listFiles();

	}

	public void addBukmeTestSonuc(ActionEvent e) {
		UICommand cmd = (UICommand) e.getComponent();
		prmGlobalId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");
		prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestBukmeSonucManager tbsManager = new TestBukmeSonucManager();

		if (testBukmeSonucForm.getId() == null) {

			testBukmeSonucForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			testBukmeSonucForm.setEkleyenKullanici(userBean.getUser().getId());

			pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testBukmeSonucForm.setPipe(pipe);

			bagliTest = new DestructiveTestsSpecManager().loadObject(
					DestructiveTestsSpec.class, prmTestId);
			testBukmeSonucForm.setBagliTestId(bagliTest);
			testBukmeSonucForm.setBagliGlobalId(bagliTest);

			sampleNoOlustur();
			testBukmeSonucForm.setSampleNo(sampleNo);

			tbsManager.enterNew(testBukmeSonucForm);

			// testlerin hangi borudan yapılacagı kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 2, true);
			DestructiveTestsSpecManager desMan = new DestructiveTestsSpecManager();
			desMan.testSonucKontrol(prmTestId, true);

			fillTestList(prmGlobalId, prmPipeId);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			testBukmeSonucForm.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			testBukmeSonucForm.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			tbsManager.updateEntity(testBukmeSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");

		}
		TestBukmeSonucManager manager = new TestBukmeSonucManager();
		allBukmeTestSonucList = manager.getAllBukmeTestSonuc(
				prmGlobalId, prmPipeId);
	}

	public void addBukmeTest() {
		testBukmeSonucForm = new TestBukmeSonuc();
	}

	public void bukmeListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(int prmGlobalId, Integer prmPipeId) {
		TestBukmeSonucManager manager = new TestBukmeSonucManager();
		allBukmeTestSonucList = manager.getAllBukmeTestSonuc(
				prmGlobalId, prmPipeId);
	}

	public void deleteTestBukmeSonuc() {

		TestBukmeSonucManager tbsManager = new TestBukmeSonucManager();

		if (testBukmeSonucForm == null || testBukmeSonucForm.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		tbsManager.delete(testBukmeSonucForm);

		// sonuc var mı false
		DestructiveTestsSpecManager desMan = new DestructiveTestsSpecManager();
		desMan.testSonucKontrol(
				testBukmeSonucForm.getBagliTestId().getTestId(), false);

		testBukmeSonucForm = new TestBukmeSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");

	}

	public void addUploadedFile(FileUploadEvent event)
			throws AbortProcessingException, IOException {
		System.out.println("testBukmeSonucBean.addUploadedFile()");

		try {
			String name = sob.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				testBukmeSonucForm.setDocuments(testBukmeSonucForm
						.getDocuments() + "//" + name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				TestBukmeSonucManager manager = new TestBukmeSonucManager();
				manager.updateEntity(testBukmeSonucForm);

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				FacesContextUtils.addWarnMessage("salesFileUploadedMessage");
			} else {
				System.out.println("File already exists.");

				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("testBukmeSonucBean: " + e.toString());
		}
	}

	@SuppressWarnings("resource")
	public StreamedContent getDownloadedFile() {
		System.out.println("TestBukmeSonucBean.getDownloadedFile()");

		try {
			if (downloadedFileName != null) {
				RandomAccessFile accessFile = new RandomAccessFile(path
						+ downloadedFileName, "r");
				byte[] downloadByte = new byte[(int) accessFile.length()];
				accessFile.read(downloadByte);
				InputStream stream = new ByteArrayInputStream(downloadByte);

				String suffix = FilenameUtils.getExtension(downloadedFileName);
				String contentType = "text/plain";
				if (suffix.contentEquals("jpg") || suffix.contentEquals("png")
						|| suffix.contentEquals("gif")) {
					contentType = "image/" + contentType;
				} else if (suffix.contentEquals("doc")
						|| suffix.contentEquals("docx")) {
					contentType = "application/msword";
				} else if (suffix.contentEquals("xls")
						|| suffix.contentEquals("xlsx")) {
					contentType = "application/vnd.ms-excel";
				} else if (suffix.contentEquals("pdf")) {
					contentType = "application/pdf";
				}
				downloadedFile = new DefaultStreamedContent(stream,
						"contentType", downloadedFileName);

				stream.close();
			}
		} catch (Exception e) {
			System.out.println("TestBukmeSonucBean.getDownloadedFile():"
					+ e.toString());
		}
		return downloadedFile;
	}

	public String getDownloadedFileName() {
		return downloadedFileName;
	}

	public void setDownloadedFileName(String downloadedFileName) {
		try {
			this.downloadedFileName = downloadedFileName;
		} catch (Exception e) {
			System.out.println("TestBukmeSonucBean.setDownloadedFileName():"
					+ e.toString());
		}
	}

	public void setPath(String path) {
		this.path = path;
	}

	// Getters & Setters

	public List<TestBukmeSonuc> getAllBukmeTestSonucList() {
		return allBukmeTestSonucList;
	}

	public void setAllBukmeTestSonucList(
			List<TestBukmeSonuc> allBukmeTestSonucList) {
		this.allBukmeTestSonucList = allBukmeTestSonucList;
	}

	public TestBukmeSonuc getTestBukmeSonucForm() {
		return testBukmeSonucForm;
	}

	public void setTestBukmeSonucForm(TestBukmeSonuc testBukmeSonucForm) {
		this.testBukmeSonucForm = testBukmeSonucForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}
}
