package com.emekboru.beans.sales.customer;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.sales.customer.SalesCustomerVisitingFrequency;
import com.emekboru.jpaman.sales.customer.SalesCustomerVisitingFrequencyManager;

@ManagedBean(name = "salesCustomerVisitingFrequencyBean")
@ViewScoped
public class SalesCustomerVisitingFrequencyBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2283918471282453591L;
	private List<SalesCustomerVisitingFrequency> visitingFrequencyList;

	public SalesCustomerVisitingFrequencyBean() {

		SalesCustomerVisitingFrequencyManager manager = new SalesCustomerVisitingFrequencyManager();
		visitingFrequencyList = manager
				.findAll(SalesCustomerVisitingFrequency.class);
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public List<SalesCustomerVisitingFrequency> getVisitingFrequencyList() {
		return visitingFrequencyList;
	}

	public void setVisitingFrequencyList(
			List<SalesCustomerVisitingFrequency> visitingFrequencyList) {
		this.visitingFrequencyList = visitingFrequencyList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}