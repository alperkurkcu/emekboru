/**
 * 
 */
package com.emekboru.beans.satinalma;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.satinalma.SatinalmaSiparisBildirimi;
import com.emekboru.jpa.satinalma.SatinalmaSiparisBildirimiIcerik;
import com.emekboru.jpaman.satinalma.SatinalmaSiparisBildirimiIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaSiparisBildirimiManager;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "satinalmaSiparisBildirimiIcerikBean")
@ViewScoped
public class SatinalmaSiparisBildirimiIcerikBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	private SatinalmaSiparisBildirimiIcerik satinalmaSiparisBildirimiIcerikForm = new SatinalmaSiparisBildirimiIcerik();
	private List<SatinalmaSiparisBildirimiIcerik> allSatinalmaSiparisBildirimiIcerikList = new ArrayList<SatinalmaSiparisBildirimiIcerik>();

	private SatinalmaSiparisBildirimi satinalmaSiparisBildirimiForm = new SatinalmaSiparisBildirimi();

	@PostConstruct
	public void load() {
		updateButtonRender = false;
	}

	public void addIcerik() {
		satinalmaSiparisBildirimiIcerikForm = new SatinalmaSiparisBildirimiIcerik();
	}

	public void fillTestList(Integer prmFormId) {

		SatinalmaSiparisBildirimiManager bildirimManager = new SatinalmaSiparisBildirimiManager();
		satinalmaSiparisBildirimiForm = bildirimManager.loadObject(
				SatinalmaSiparisBildirimi.class, prmFormId);
		SatinalmaSiparisBildirimiIcerikManager manager = new SatinalmaSiparisBildirimiIcerikManager();
		allSatinalmaSiparisBildirimiIcerikList = manager
				.findByBildirimId(satinalmaSiparisBildirimiForm.getId());
		satinalmaSiparisBildirimiIcerikForm = new SatinalmaSiparisBildirimiIcerik();
		updateButtonRender = false;
	}

	public void addOrUpdateIcerik() {

		SatinalmaSiparisBildirimiIcerikManager icerikManager = new SatinalmaSiparisBildirimiIcerikManager();

		if (satinalmaSiparisBildirimiIcerikForm.getId() == null) {

			satinalmaSiparisBildirimiIcerikForm.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			satinalmaSiparisBildirimiIcerikForm.setEkleyenKullanici(userBean
					.getUser().getId());
			satinalmaSiparisBildirimiIcerikForm
					.setSatinalmaSiparisBildirimi(satinalmaSiparisBildirimiForm);

			try {
				icerikManager.enterNew(satinalmaSiparisBildirimiIcerikForm);
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"SİPARİŞ BAŞARIYLA EKLENDİ!", null));
			} catch (Exception e) {
				System.out
						.println("satinalmaSiparisBildirimiIcerikBean.addOrUpdateIcerik: "
								+ e.toString());
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"SİPARİŞ EKLENEMEDİ, HATA!", null));
			}

		} else {

			satinalmaSiparisBildirimiIcerikForm.setGuncellemeZamani(UtilInsCore
					.getTarihZaman());
			satinalmaSiparisBildirimiIcerikForm
					.setGuncelleyenKullanici(userBean.getUser().getId());

			try {
				icerikManager.updateEntity(satinalmaSiparisBildirimiIcerikForm);
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"SİPARİŞ BAŞARIYLA GÜNCELLENDİ!", null));
			} catch (Exception e) {
				System.out
						.println("satinalmaSiparisBildirimiIcerikBean.addOrUpdateIcerik: "
								+ e.toString());
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"SİPARİŞ GÜNCELLENEMEDİ, HATA!", null));
			}
		}

		fillTestList(satinalmaSiparisBildirimiForm.getId());

	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	// setters getters

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the satinalmaSiparisBildirimiIcerikForm
	 */
	public SatinalmaSiparisBildirimiIcerik getSatinalmaSiparisBildirimiIcerikForm() {
		return satinalmaSiparisBildirimiIcerikForm;
	}

	/**
	 * @param satinalmaSiparisBildirimiIcerikForm
	 *            the satinalmaSiparisBildirimiIcerikForm to set
	 */
	public void setSatinalmaSiparisBildirimiIcerikForm(
			SatinalmaSiparisBildirimiIcerik satinalmaSiparisBildirimiIcerikForm) {
		this.satinalmaSiparisBildirimiIcerikForm = satinalmaSiparisBildirimiIcerikForm;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the allSatinalmaSiparisBildirimiIcerikList
	 */
	public List<SatinalmaSiparisBildirimiIcerik> getAllSatinalmaSiparisBildirimiIcerikList() {
		return allSatinalmaSiparisBildirimiIcerikList;
	}

	/**
	 * @param allSatinalmaSiparisBildirimiIcerikList
	 *            the allSatinalmaSiparisBildirimiIcerikList to set
	 */
	public void setAllSatinalmaSiparisBildirimiIcerikList(
			List<SatinalmaSiparisBildirimiIcerik> allSatinalmaSiparisBildirimiIcerikList) {
		this.allSatinalmaSiparisBildirimiIcerikList = allSatinalmaSiparisBildirimiIcerikList;
	}
}
