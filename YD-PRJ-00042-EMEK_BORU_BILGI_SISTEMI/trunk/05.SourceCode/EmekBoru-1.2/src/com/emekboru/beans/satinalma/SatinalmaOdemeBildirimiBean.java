/**
 * 
 */
package com.emekboru.beans.satinalma;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.satinalma.SatinalmaOdemeBildirimi;
import com.emekboru.jpa.satinalma.SatinalmaSiparisBildirimi;
import com.emekboru.jpaman.satinalma.SatinalmaOdemeBildirimiManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "satinalmaOdemeBildirimiBean")
@ViewScoped
public class SatinalmaOdemeBildirimiBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	private SatinalmaOdemeBildirimi satinalmaOdemeBildirimiForm = new SatinalmaOdemeBildirimi();
	private SatinalmaSiparisBildirimi satinalmaSiparisBildirimiForm = new SatinalmaSiparisBildirimi();

	private List<SatinalmaOdemeBildirimi> allSatinalmaOdemeBildirimiList = new ArrayList<SatinalmaOdemeBildirimi>();

	private String downloadedFileName;
	private StreamedContent downloadedFile;

	private File[] fileArray;

	private File theFile = null;
	OutputStream output = null;
	InputStream input = null;

	private String path;
	private String privatePath;
	private String toBeDeleted;

	private boolean isThisFirstTime;

	@PostConstruct
	public void load() {
		updateButtonRender = false;
		fillTestList();
		System.out.println("satinalmaOdemeBildirimiBean.load()");

		if (isThisFirstTime) {
			try {
				this.setPath(config.getConfig().getFolder().getAbsolutePath()
						+ privatePath);

				System.out
						.println("Path for Upload File (satinalmaOdemeBildirimi):			"
								+ path);
			} catch (Exception ex) {
				System.out
						.println("Path for Upload File (satinalmaOdemeBildirimi):			"
								+ FacesContext.getCurrentInstance()
										.getExternalContext()
										.getRealPath(privatePath));

				System.out.print(path);
				System.out.println("CAUSE OF " + ex.toString());
			}
			theFile = new File(path);
			if (!theFile.isDirectory()) {
				theFile.mkdirs();
			}
			fileArray = theFile.listFiles();
			isThisFirstTime = false;
		}
	}

	public SatinalmaOdemeBildirimiBean() {

		// fillTestList();
		privatePath = File.separatorChar + "satinalma" + File.separatorChar
				+ "uploadedDocuments" + File.separatorChar + "odemeBildirimi"
				+ File.separatorChar;

		isThisFirstTime = true;
	}

	public void fillTestList() {

		SatinalmaOdemeBildirimiManager manager = new SatinalmaOdemeBildirimiManager();
		allSatinalmaOdemeBildirimiList = manager
				.getAllSatinalmaOdemeBildirimi();
		satinalmaOdemeBildirimiForm = new SatinalmaOdemeBildirimi();
		updateButtonRender = false;
	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void addFromSiparisBildirimi(
			SatinalmaSiparisBildirimi satinalmaSiparisBildirimi) {

		SatinalmaOdemeBildirimiManager manager = new SatinalmaOdemeBildirimiManager();

		this.satinalmaSiparisBildirimiForm = satinalmaSiparisBildirimi;

		satinalmaOdemeBildirimiForm
				.setSatinalmaSiparisBildirimi(satinalmaSiparisBildirimiForm);
		satinalmaOdemeBildirimiForm
				.setEklemeZamani(UtilInsCore.getTarihZaman());
		satinalmaOdemeBildirimiForm.setEkleyenKullanici(userBean.getUser()
				.getId());
		try {
			manager.enterNew(satinalmaOdemeBildirimiForm);
		} catch (Exception e) {
			System.out
					.println("satinalmaOdemeBildirimiBean.addFromSiparisBildirimi: "
							+ e.toString());
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖDEME BİLDİRİMİ OLUŞTURULAMADI, HATA!", null));
		}

		satinalmaOdemeBildirimiForm = new SatinalmaOdemeBildirimi();
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				"ÖDEME BİLDİRİMİ BAŞARIYLA OLUŞTURULDU!", null));
	}

	public void addOrUpdateForm() {

		SatinalmaOdemeBildirimiManager manager = new SatinalmaOdemeBildirimiManager();

		if (satinalmaOdemeBildirimiForm.getId() == null) {

			satinalmaOdemeBildirimiForm.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			satinalmaOdemeBildirimiForm.setEkleyenKullanici(userBean.getUser()
					.getId());
			try {
				manager.enterNew(satinalmaOdemeBildirimiForm);
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"ÖDEME BİLDİRİMİ BAŞARIYLA EKLENDİ!", null));
			} catch (Exception e) {
				System.out
						.println("satinalmaOdemeBildirimiBean.addOrUpdateForm: "
								+ e.toString());
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"ÖDEME BİLDİRİMİ OLUŞTURULAMADI, HATA!", null));
			}
		} else {

			satinalmaOdemeBildirimiForm.setGuncellemeZamani(UtilInsCore
					.getTarihZaman());
			satinalmaOdemeBildirimiForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			try {
				manager.updateEntity(satinalmaOdemeBildirimiForm);
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"ÖDEME BİLDİRİMİ BAŞARIYLA GÜNCELLENDİ!", null));
			} catch (Exception e) {
				System.out.println("satinalmaOdemeBildirimiBean.addFromKarar: "
						+ e.toString());
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"ÖDEME BİLDİRİMİ GÜNCELLENEMEDİ, HATA!", null));
			}
		}
	}

	public void addUploadedFile(FileUploadEvent event)
			throws AbortProcessingException, IOException {
		System.out.println("satinalmaOdemeBildirimiBean.addUploadedFile()");

		try {
			String name = this.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				satinalmaOdemeBildirimiForm
						.setDocuments(satinalmaOdemeBildirimiForm
								.getDocuments() + "//" + name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				SatinalmaOdemeBildirimiManager manager = new SatinalmaOdemeBildirimiManager();
				manager.updateEntity(satinalmaOdemeBildirimiForm);

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				FacesContextUtils.addWarnMessage("salesFileUploadedMessage");
			} else {
				System.out.println("File already exists.");

				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("satinalmaOdemeBildirimiBean: " + e.toString());
		}
	}

	public void deleteTheSelectedFile() {
		System.out
				.println("satinalmaOdemeBildirimiBean.deleteTheSelectedFile()");
		try {
			// Delete the uploaded file
			theFile = new File(path);
			fileArray = theFile.listFiles();

			System.out.println("document that will be deleted: " + privatePath
					+ toBeDeleted);
			for (int j = 0; j < fileArray.length; j++) {
				System.out.println("fileArray[j].toString(): "
						+ fileArray[j].toString());
				if (fileArray[j].toString().contains(privatePath + toBeDeleted)) {
					fileArray[j].delete();

					if (satinalmaOdemeBildirimiForm.getDocuments()
							.contentEquals("//" + toBeDeleted)) {
						satinalmaOdemeBildirimiForm
								.setDocuments(satinalmaOdemeBildirimiForm
										.getDocuments().replace(
												"//" + toBeDeleted, "null"));
					} else {
						satinalmaOdemeBildirimiForm
								.setDocuments(satinalmaOdemeBildirimiForm
										.getDocuments().replace(
												"//" + toBeDeleted, ""));
					}
					FacesContextUtils
							.addWarnMessage("salesTheFileIsDeletedMessage");
				}
			}
		} catch (Exception e) {
			System.out.println("satinalmaOdemeBildirimiBean: " + e.toString());
		}
	}

	public void deleteAndUpload(FileUploadEvent event)
			throws AbortProcessingException, IOException {

		System.out.println("satinalmaOdemeBildirimiBean.deleteAndUpload()");

		try {
			// Delete the uploaded file
			theFile = new File(path);
			fileArray = theFile.listFiles();

			for (int i = 0; i < satinalmaOdemeBildirimiForm.getFileNames()
					.size(); i++) {
				for (int j = 0; j < fileArray.length; j++) {
					System.out.println("fileArray[j].toString(): "
							+ fileArray[j].toString());
					if (fileArray[j].toString().contains(
							privatePath
									+ satinalmaOdemeBildirimiForm
											.getFileNames().get(i))) {
						fileArray[j].delete();
					}
				}
			}

			String name = this.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				satinalmaOdemeBildirimiForm.setDocuments("//" + name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				SatinalmaOdemeBildirimiManager manager = new SatinalmaOdemeBildirimiManager();
				manager.updateEntity(satinalmaOdemeBildirimiForm);

				FacesContextUtils.addInfoMessage("UpdateItemMessage");
			} else {
				System.out.println("File already exists.");

				satinalmaOdemeBildirimiForm.setDocuments(null);
				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("satinalmaOdemeBildirimiBean: " + e.toString());
		}
	}

	public String checkFileName(String fileName) {

		fileName = fileName.replace(" ", "_");
		fileName = fileName.replace("ç", "c");
		fileName = fileName.replace("Ç", "C");
		fileName = fileName.replace("ğ", "g");
		fileName = fileName.replace("Ğ", "G");
		fileName = fileName.replace("İ", "i");
		fileName = fileName.replace("ı", "i");
		fileName = fileName.replace("ö", "o");
		fileName = fileName.replace("Ö", "O");
		fileName = fileName.replace("ş", "s");
		fileName = fileName.replace("Ş", "S");
		fileName = fileName.replace("ü", "u");
		fileName = fileName.replace("Ü", "U");

		return fileName;
	}

	// gettesr setters

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the satinalmaOdemeBildirimiForm
	 */
	public SatinalmaOdemeBildirimi getSatinalmaOdemeBildirimiForm() {
		return satinalmaOdemeBildirimiForm;
	}

	/**
	 * @param satinalmaOdemeBildirimiForm
	 *            the satinalmaOdemeBildirimiForm to set
	 */
	public void setSatinalmaOdemeBildirimiForm(
			SatinalmaOdemeBildirimi satinalmaOdemeBildirimiForm) {
		this.satinalmaOdemeBildirimiForm = satinalmaOdemeBildirimiForm;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the satinalmaSiparisBildirimiForm
	 */
	public SatinalmaSiparisBildirimi getSatinalmaSiparisBildirimiForm() {
		return satinalmaSiparisBildirimiForm;
	}

	/**
	 * @param satinalmaSiparisBildirimiForm
	 *            the satinalmaSiparisBildirimiForm to set
	 */
	public void setSatinalmaSiparisBildirimiForm(
			SatinalmaSiparisBildirimi satinalmaSiparisBildirimiForm) {
		this.satinalmaSiparisBildirimiForm = satinalmaSiparisBildirimiForm;
	}

	/**
	 * @return the allSatinalmaOdemeBildirimiList
	 */
	public List<SatinalmaOdemeBildirimi> getAllSatinalmaOdemeBildirimiList() {
		return allSatinalmaOdemeBildirimiList;
	}

	/**
	 * @param allSatinalmaOdemeBildirimiList
	 *            the allSatinalmaOdemeBildirimiList to set
	 */
	public void setAllSatinalmaOdemeBildirimiList(
			List<SatinalmaOdemeBildirimi> allSatinalmaOdemeBildirimiList) {
		this.allSatinalmaOdemeBildirimiList = allSatinalmaOdemeBildirimiList;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path
	 *            the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the downloadedFileName
	 */
	public String getDownloadedFileName() {
		return downloadedFileName;
	}

	/**
	 * @param downloadedFileName
	 *            the downloadedFileName to set
	 */
	public void setDownloadedFileName(String downloadedFileName) {
		this.downloadedFileName = downloadedFileName;
	}

	@SuppressWarnings("resource")
	public StreamedContent getDownloadedFile() {
		System.out.println("satinalmaOdemeBildirimiBean.getDownloadedFile()");

		try {
			if (downloadedFileName != null) {
				RandomAccessFile accessFile = new RandomAccessFile(path
						+ downloadedFileName, "r");
				byte[] downloadByte = new byte[(int) accessFile.length()];
				accessFile.read(downloadByte);
				InputStream stream = new ByteArrayInputStream(downloadByte);

				String suffix = FilenameUtils.getExtension(downloadedFileName);
				String contentType = "text/plain";
				if (suffix.contentEquals("jpg") || suffix.contentEquals("png")
						|| suffix.contentEquals("gif")) {
					contentType = "image/" + contentType;
				} else if (suffix.contentEquals("doc")
						|| suffix.contentEquals("docx")) {
					contentType = "application/msword";
				} else if (suffix.contentEquals("xls")
						|| suffix.contentEquals("xlsx")) {
					contentType = "application/vnd.ms-excel";
				} else if (suffix.contentEquals("pdf")) {
					contentType = "application/pdf";
				}
				downloadedFile = new DefaultStreamedContent(stream,
						"contentType", downloadedFileName);

				stream.close();
			}
		} catch (Exception e) {
			System.out
					.println("satinalmaSatinalmaKarariBean.getDownloadedFile():"
							+ e.toString());
		}
		return downloadedFile;
	}

	/**
	 * @param downloadedFile
	 *            the downloadedFile to set
	 */
	public void setDownloadedFile(StreamedContent downloadedFile) {
		this.downloadedFile = downloadedFile;
	}

	/**
	 * @return the toBeDeleted
	 */
	public String getToBeDeleted() {
		return toBeDeleted;
	}

	/**
	 * @param toBeDeleted
	 *            the toBeDeleted to set
	 */
	public void setToBeDeleted(String toBeDeleted) {
		this.toBeDeleted = toBeDeleted;
	}
}
