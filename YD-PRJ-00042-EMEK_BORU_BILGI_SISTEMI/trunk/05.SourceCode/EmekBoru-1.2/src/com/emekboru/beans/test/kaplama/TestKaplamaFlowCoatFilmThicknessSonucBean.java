/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaFlowCoatFilmThicknessSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaFlowCoatFilmThicknessSonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testKaplamaFlowCoatFilmThicknessSonucBean")
@ViewScoped
public class TestKaplamaFlowCoatFilmThicknessSonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<TestKaplamaFlowCoatFilmThicknessSonuc> allKaplamaFlowCoatFilmThicknessSonucList = new ArrayList<TestKaplamaFlowCoatFilmThicknessSonuc>();
	private TestKaplamaFlowCoatFilmThicknessSonuc testKaplamaFlowCoatFilmThicknessSonucForm = new TestKaplamaFlowCoatFilmThicknessSonuc();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;
	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addKaplamaFlowCoatFilmThicknessSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaFlowCoatFilmThicknessSonucManager tkdsManager = new TestKaplamaFlowCoatFilmThicknessSonucManager();

		if (testKaplamaFlowCoatFilmThicknessSonucForm.getId() == null) {

			testKaplamaFlowCoatFilmThicknessSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaFlowCoatFilmThicknessSonucForm
					.setEkleyenKullanici(userBean.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaFlowCoatFilmThicknessSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaFlowCoatFilmThicknessSonucForm.setBagliTestId(bagliTest);
			testKaplamaFlowCoatFilmThicknessSonucForm
					.setBagliGlobalId(bagliTest);

			tkdsManager.enterNew(testKaplamaFlowCoatFilmThicknessSonucForm);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
		} else {

			testKaplamaFlowCoatFilmThicknessSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaFlowCoatFilmThicknessSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			tkdsManager.updateEntity(testKaplamaFlowCoatFilmThicknessSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");

		}
		fillTestList(prmGlobalId, prmPipeId);
	}

	public void addKaplamaFlowCoatFilmThickness() {
		testKaplamaFlowCoatFilmThicknessSonucForm = new TestKaplamaFlowCoatFilmThicknessSonuc();
		updateButtonRender = false;
	}

	public void kaplamaFlowCoatFilmThicknessListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(int globalId, Integer pipeId2) {
		allKaplamaFlowCoatFilmThicknessSonucList = TestKaplamaFlowCoatFilmThicknessSonucManager
				.getAllTestKaplamaFlowCoatFilmThicknessSonuc(globalId, pipeId2);
		testKaplamaFlowCoatFilmThicknessSonucForm = new TestKaplamaFlowCoatFilmThicknessSonuc();
	}

	public void deleteTestKaplamaFlowCoatFilmThicknessSonuc() {

		bagliTestId = testKaplamaFlowCoatFilmThicknessSonucForm
				.getBagliTestId().getId();

		if (testKaplamaFlowCoatFilmThicknessSonucForm == null
				|| testKaplamaFlowCoatFilmThicknessSonucForm.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaFlowCoatFilmThicknessSonucManager tkdsManager = new TestKaplamaFlowCoatFilmThicknessSonucManager();
		tkdsManager.delete(testKaplamaFlowCoatFilmThicknessSonucForm);
		testKaplamaFlowCoatFilmThicknessSonucForm = new TestKaplamaFlowCoatFilmThicknessSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setters getters

	/**
	 * @return the allKaplamaFlowCoatFilmThicknessSonucList
	 */
	public List<TestKaplamaFlowCoatFilmThicknessSonuc> getAllKaplamaFlowCoatFilmThicknessSonucList() {
		return allKaplamaFlowCoatFilmThicknessSonucList;
	}

	/**
	 * @param allKaplamaFlowCoatFilmThicknessSonucList
	 *            the allKaplamaFlowCoatFilmThicknessSonucList to set
	 */
	public void setAllKaplamaFlowCoatFilmThicknessSonucList(
			List<TestKaplamaFlowCoatFilmThicknessSonuc> allKaplamaFlowCoatFilmThicknessSonucList) {
		this.allKaplamaFlowCoatFilmThicknessSonucList = allKaplamaFlowCoatFilmThicknessSonucList;
	}

	/**
	 * @return the testKaplamaFlowCoatFilmThicknessSonucForm
	 */
	public TestKaplamaFlowCoatFilmThicknessSonuc getTestKaplamaFlowCoatFilmThicknessSonucForm() {
		return testKaplamaFlowCoatFilmThicknessSonucForm;
	}

	/**
	 * @param testKaplamaFlowCoatFilmThicknessSonucForm
	 *            the testKaplamaFlowCoatFilmThicknessSonucForm to set
	 */
	public void setTestKaplamaFlowCoatFilmThicknessSonucForm(
			TestKaplamaFlowCoatFilmThicknessSonuc testKaplamaFlowCoatFilmThicknessSonucForm) {
		this.testKaplamaFlowCoatFilmThicknessSonucForm = testKaplamaFlowCoatFilmThicknessSonucForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
