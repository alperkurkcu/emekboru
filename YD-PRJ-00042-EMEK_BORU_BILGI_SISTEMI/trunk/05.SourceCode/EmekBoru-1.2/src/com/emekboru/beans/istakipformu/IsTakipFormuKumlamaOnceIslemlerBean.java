/**
 * 
 */
package com.emekboru.beans.istakipformu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.istakipformu.IsTakipFormuKumlamaOnceIslemler;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isTakipFormuKumlamaOnceIslemlerBean")
@ViewScoped
public class IsTakipFormuKumlamaOnceIslemlerBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<IsTakipFormuKumlamaOnceIslemler> allIsTakipFormuKumlamaOnceIslemlerList = new ArrayList<IsTakipFormuKumlamaOnceIslemler>();
	private IsTakipFormuKumlamaOnceIslemler isTakipFormuKumlamaOnceIslemlerForm = new IsTakipFormuKumlamaOnceIslemler();

	private boolean updateButtonRender;
	private boolean visibleButtonRender;

	public IsTakipFormuKumlamaOnceIslemlerBean() {

		updateButtonRender = false;
		visibleButtonRender = false;
	}

	// setters getters

	public List<IsTakipFormuKumlamaOnceIslemler> getAllIsTakipFormuKumlamaOnceIslemlerList() {
		return allIsTakipFormuKumlamaOnceIslemlerList;
	}

	public void setAllIsTakipFormuKumlamaOnceIslemlerList(
			List<IsTakipFormuKumlamaOnceIslemler> allIsTakipFormuKumlamaOnceIslemlerList) {
		this.allIsTakipFormuKumlamaOnceIslemlerList = allIsTakipFormuKumlamaOnceIslemlerList;
	}

	public IsTakipFormuKumlamaOnceIslemler getIsTakipFormuKumlamaOnceIslemlerForm() {
		return isTakipFormuKumlamaOnceIslemlerForm;
	}

	public void setIsTakipFormuKumlamaOnceIslemlerForm(
			IsTakipFormuKumlamaOnceIslemler isTakipFormuKumlamaOnceIslemlerForm) {
		this.isTakipFormuKumlamaOnceIslemlerForm = isTakipFormuKumlamaOnceIslemlerForm;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public boolean isVisibleButtonRender() {
		return visibleButtonRender;
	}

	public void setVisibleButtonRender(boolean visibleButtonRender) {
		this.visibleButtonRender = visibleButtonRender;
	}

}
