/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaElektrikselDirencTestSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaElektrikselDirencTestSonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testKaplamaElektrikselDirencTestSonucBean")
@ViewScoped
public class TestKaplamaElektrikselDirencTestSonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private TestKaplamaElektrikselDirencTestSonuc testKaplamaElektrikselDirencTestSonucForm = new TestKaplamaElektrikselDirencTestSonuc();
	private List<TestKaplamaElektrikselDirencTestSonuc> allTestKaplamaElektrikselDirencTestSonucList = new ArrayList<TestKaplamaElektrikselDirencTestSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateTestKaplamaElektrikselDirencTestSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaElektrikselDirencTestSonucManager tkbebManager = new TestKaplamaElektrikselDirencTestSonucManager();

		if (testKaplamaElektrikselDirencTestSonucForm.getId() == null) {

			testKaplamaElektrikselDirencTestSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaElektrikselDirencTestSonucForm
					.setEkleyenKullanici(userBean.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaElektrikselDirencTestSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaElektrikselDirencTestSonucForm.setBagliTestId(bagliTest);
			testKaplamaElektrikselDirencTestSonucForm
					.setBagliGlobalId(bagliTest);

			tkbebManager.enterNew(testKaplamaElektrikselDirencTestSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			testKaplamaElektrikselDirencTestSonucForm = new TestKaplamaElektrikselDirencTestSonuc();
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaElektrikselDirencTestSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaElektrikselDirencTestSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			tkbebManager
					.updateEntity(testKaplamaElektrikselDirencTestSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addTestKaplamaElektrikselDirencTestSonuc() {

		testKaplamaElektrikselDirencTestSonucForm = new TestKaplamaElektrikselDirencTestSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaElektrikselDirencTestSonucListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaElektrikselDirencTestSonucList = TestKaplamaElektrikselDirencTestSonucManager
				.getAllElektrikselDirencTestSonuc(globalId, pipeId);
		testKaplamaElektrikselDirencTestSonucForm = new TestKaplamaElektrikselDirencTestSonuc();
	}

	public void deleteTestKaplamaElektrikselDirencTestSonuc() {

		bagliTestId = testKaplamaElektrikselDirencTestSonucForm
				.getBagliGlobalId().getId();

		if (testKaplamaElektrikselDirencTestSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		TestKaplamaElektrikselDirencTestSonucManager tkbebManager = new TestKaplamaElektrikselDirencTestSonucManager();

		tkbebManager.delete(testKaplamaElektrikselDirencTestSonucForm);
		testKaplamaElektrikselDirencTestSonucForm = new TestKaplamaElektrikselDirencTestSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters

	/**
	 * @return the testKaplamaElektrikselDirencTestSonucForm
	 */
	public TestKaplamaElektrikselDirencTestSonuc getTestKaplamaElektrikselDirencTestSonucForm() {
		return testKaplamaElektrikselDirencTestSonucForm;
	}

	/**
	 * @param testKaplamaElektrikselDirencTestSonucForm
	 *            the testKaplamaElektrikselDirencTestSonucForm to set
	 */
	public void setTestKaplamaElektrikselDirencTestSonucForm(
			TestKaplamaElektrikselDirencTestSonuc testKaplamaElektrikselDirencTestSonucForm) {
		this.testKaplamaElektrikselDirencTestSonucForm = testKaplamaElektrikselDirencTestSonucForm;
	}

	/**
	 * @return the allTestKaplamaElektrikselDirencTestSonucList
	 */
	public List<TestKaplamaElektrikselDirencTestSonuc> getAllTestKaplamaElektrikselDirencTestSonucList() {
		return allTestKaplamaElektrikselDirencTestSonucList;
	}

	/**
	 * @param allTestKaplamaElektrikselDirencTestSonucList
	 *            the allTestKaplamaElektrikselDirencTestSonucList to set
	 */
	public void setAllTestKaplamaElektrikselDirencTestSonucList(
			List<TestKaplamaElektrikselDirencTestSonuc> allTestKaplamaElektrikselDirencTestSonucList) {
		this.allTestKaplamaElektrikselDirencTestSonucList = allTestKaplamaElektrikselDirencTestSonucList;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
