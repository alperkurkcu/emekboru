package com.emekboru.beans.machine;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import com.emekboru.jpa.MachinePart;
import com.emekboru.jpaman.MachinePartManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "machinePartBean")
@ViewScoped
public class MachinePartBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private MachinePart selectedPart;
	private MachinePart newPart;

	public MachinePartBean() {
		selectedPart = new MachinePart();
		newPart = new MachinePart();
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void cancelMachinePartSubmittion(ActionEvent event) {
		newPart = new MachinePart();
	}

	public void submitNewMachinePart(ActionEvent event) {
		System.out.println("MachinePartBean.submitNewMachinePart()");

		MachinePartManager partManager = new MachinePartManager();
		if (newPart != null && newPart.getMachinePartId() != null) {
			partManager.updateEntity(newPart);
		} else {
			partManager.enterNew(newPart);
		}
		FacesContextUtils.addInfoMessage("SubmitAddMessage");

		newPart.getMachine().getMachineParts().add(newPart);

		// load new activity to datatable
		MachineUtilsListBean.loadMachineParts();
		newPart = new MachinePart();
	}

	public void updateMachinePart(ActionEvent event) {
		MachinePartManager partManager = new MachinePartManager();
		if (selectedPart != null && selectedPart.getMachinePartId() != null) {
			partManager.updateEntity(selectedPart);
		}
		FacesContextUtils.addInfoMessage("UpdateItemMessage");

		MachineUtilsListBean.loadMachineParts();
		selectedPart = new MachinePart();
	}

	public void deleteMachinePart(ActionEvent event) {

		MachinePartManager partManager = new MachinePartManager();
		if (selectedPart != null && selectedPart.getMachinePartId() != null) {
			partManager.deleteByField(MachinePart.class, "machinePartId",
					selectedPart.getMachinePartId());
		}
		FacesContextUtils.addWarnMessage("DeletedMessage");

		MachineUtilsListBean.loadMachineParts();
		selectedPart = new MachinePart();
	}

	public List<MachinePart> getMachineParts() {

		List<MachinePart> result = null;
		return result;
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public MachinePart getSelectedPart() {
		return selectedPart;
	}

	public void setSelectedPart(MachinePart selectedPart) {
		this.selectedPart = selectedPart;
	}

	public MachinePart getNewPart() {
		return newPart;
	}

	public void setNewPart(MachinePart newPart) {
		this.newPart = newPart;
	}

}
