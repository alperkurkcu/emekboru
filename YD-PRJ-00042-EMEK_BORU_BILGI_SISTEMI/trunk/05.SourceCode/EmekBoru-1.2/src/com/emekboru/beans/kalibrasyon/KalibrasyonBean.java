/**
 * 
 */
package com.emekboru.beans.kalibrasyon;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "kalibrasyonBean")
@ViewScoped
public class KalibrasyonBean {

	@EJB
	private ConfigBean configBean;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	// Manuel Kalibrasyon acılış sayfası
	public void goToKalibrasyonManuel() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().KALIBRASYON_MANUEL_PAGE);
	}

	// Online Kalibrasyon acılış sayfası
	public void goToKalibrasyonOnline() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().KALIBRASYON_ONLINE_PAGE);
	}

	// Online Laminasyon Kalibrasyon acılış sayfası
	public void goToKalibrasyonOnlineLaminasyon() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().KALIBRASYON_ONLINE_LAMINASYON_PAGE);
	}

	// Offline Kalibrasyon acılış sayfası
	public void goToKalibrasyonOffline() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().KALIBRASYON_OFFLINE_PAGE);
	}

	// Offline Boru Ucu Kalibrasyon acılış sayfası
	public void goToKalibrasyonOfflineBoruUcu() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().KALIBRASYON_OFFLINE_BORU_UCU_PAGE);
	}

	// Offline Laminasyon Kalibrasyon acılış sayfası
	public void goToKalibrasyonOfflineLaminasyon() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().KALIBRASYON_OFFLINE_LAMINASYON_PAGE);
	}

	// FL Kalibrasyon acılış sayfası
	public void goToKalibrasyonFloroskopik() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().KALIBRASYON_FLOROSKOPIK_PAGE);
	}

	// Radyografik Kalibrasyon acılış sayfası
	public void goToKalibrasyonRadyografik() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().KALIBRASYON_RADYOGRAFIK_PAGE);
	}

	// Penetrasyon Kalibrasyon acılış sayfası
	public void goToKalibrasyonPenetrant() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().KALIBRASYON_PENETRANT_PAGE);
	}

	// Manyetik Kalibrasyon acılış sayfası
	public void goToKalibrasyonManyetik() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().KALIBRASYON_MANYETIK_PAGE);
	}

	// Manyetik Kalibrasyon acılış sayfası
	public void goToKalibrasyonManyetikBoruucu() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().KALIBRASYON_MANYETIK_BORU_UCU_PAGE);
	}

	// setters getters

	/**
	 * @return the configBean
	 */
	public ConfigBean getConfigBean() {
		return configBean;
	}

	/**
	 * @param configBean
	 *            the configBean to set
	 */
	public void setConfigBean(ConfigBean configBean) {
		this.configBean = configBean;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}
}
