package com.emekboru.beans.test.kaplama;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaYapismaSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaYapismaSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaplamaYapismaSonucBean")
@ViewScoped
public class TestKaplamaYapismaSonucBean {

	private TestKaplamaYapismaSonuc testKaplamaYapismaSonucForm = new TestKaplamaYapismaSonuc();
	private List<TestKaplamaYapismaSonuc> allKaplamaYapismaSonucList = new ArrayList<TestKaplamaYapismaSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateKaplamaYapismaTestSonuc(ActionEvent e) {
		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaYapismaSonucManager manager = new TestKaplamaYapismaSonucManager();

		if (testKaplamaYapismaSonucForm.getId() == null) {

			testKaplamaYapismaSonucForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			testKaplamaYapismaSonucForm.setEkleyenKullanici(userBean.getUser()
					.getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaYapismaSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaYapismaSonucForm.setBagliTestId(bagliTest);
			testKaplamaYapismaSonucForm.setBagliGlobalId(bagliTest);

			manager.enterNew(testKaplamaYapismaSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaYapismaSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaYapismaSonucForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			manager.updateEntity(testKaplamaYapismaSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addKaplamaYapismaTest() {

		testKaplamaYapismaSonucForm = new TestKaplamaYapismaSonuc();
		updateButtonRender = false;
	}

	public void kaplamaYapismaListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allKaplamaYapismaSonucList = TestKaplamaYapismaSonucManager
				.getAllKaplamaYapismaSonucTest(globalId, pipeId);
		testKaplamaYapismaSonucForm = new TestKaplamaYapismaSonuc();
	}

	public void deleteTestKaplamaYapismaSonuc() {

		bagliTestId = testKaplamaYapismaSonucForm.getBagliTestId().getId();
		if (testKaplamaYapismaSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaYapismaSonucManager manager = new TestKaplamaYapismaSonucManager();
		manager.delete(testKaplamaYapismaSonucForm);
		testKaplamaYapismaSonucForm = new TestKaplamaYapismaSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setters getters
	public TestKaplamaYapismaSonuc getTestKaplamaYapismaSonucForm() {
		return testKaplamaYapismaSonucForm;
	}

	public void setTestKaplamaYapismaSonucForm(
			TestKaplamaYapismaSonuc testKaplamaYapismaSonucForm) {
		this.testKaplamaYapismaSonucForm = testKaplamaYapismaSonucForm;
	}

	public List<TestKaplamaYapismaSonuc> getAllKaplamaYapismaSonucList() {
		return allKaplamaYapismaSonucList;
	}

	public void setAllKaplamaYapismaSonucList(
			List<TestKaplamaYapismaSonuc> allKaplamaYapismaSonucList) {
		this.allKaplamaYapismaSonucList = allKaplamaYapismaSonucList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
