package com.emekboru.beans.machine;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import com.emekboru.jpa.MaintenancePlanType;
import com.emekboru.jpaman.MaintenancePlanTypeManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "maintenancePlanTypeBean")
@ViewScoped
public class MaintenancePlanTypeBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private MaintenancePlanType selectedPlanType;
	private MaintenancePlanType newPlanType;

	public MaintenancePlanTypeBean() {
		selectedPlanType = new MaintenancePlanType();
		newPlanType = new MaintenancePlanType();
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void cancelMaintenancePlanTypeSubmittion(ActionEvent event) {
		newPlanType = new MaintenancePlanType();
	}

	public void submitNewMaintenancePlanType(ActionEvent event) {
		System.out
				.println("maintenancePlanTypeBean.submitNewMaintenancePlanType()");

		MaintenancePlanTypeManager planTypeManager = new MaintenancePlanTypeManager();
		if (newPlanType != null
				&& newPlanType.getMaintenancePlanTypeId() != null) {
			System.out.println("maintenancePlanTypeBean.updateEntity");
			planTypeManager.updateEntity(newPlanType);
		} else {
			System.out.println("maintenancePlanTypeBean.enterNew");
			planTypeManager.enterNew(newPlanType);
		}
		FacesContextUtils.addInfoMessage("SubmitMessage");

		MachineUtilsListBean.loadMaintenancePlanTypeList();
		newPlanType = new MaintenancePlanType();
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void updateMaintenancePlanType(ActionEvent event) {
		System.out.println("MachineTypeBean.updateEntity");
		MaintenancePlanTypeManager planTypeManager = new MaintenancePlanTypeManager();
		if (selectedPlanType != null
				&& selectedPlanType.getMaintenancePlanTypeId() != null) {
			planTypeManager.updateEntity(selectedPlanType);
		}

		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}

	public void deleteMaintenancePlanType(ActionEvent event) {

		MaintenancePlanTypeManager planTypeManager = new MaintenancePlanTypeManager();
		if (selectedPlanType != null
				&& selectedPlanType.getMaintenancePlanTypeId() != null) {
			planTypeManager.deleteByField(MaintenancePlanType.class,
					"maintenancePlanTypeId",
					selectedPlanType.getMaintenancePlanTypeId());
		}

		FacesContextUtils.addWarnMessage("DeletedMessage");
		selectedPlanType = new MaintenancePlanType();
		MachineUtilsListBean.setAllMaintenancePlanTypes();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public MaintenancePlanType getSelectedPlanType() {
		return selectedPlanType;
	}

	public void setSelectedPlanType(MaintenancePlanType selectedPlanType) {
		this.selectedPlanType = selectedPlanType;
	}

	public MaintenancePlanType getNewPlanType() {
		return newPlanType;
	}

	public void setNewPlanType(MaintenancePlanType newPlanType) {
		this.newPlanType = newPlanType;
	}

}
