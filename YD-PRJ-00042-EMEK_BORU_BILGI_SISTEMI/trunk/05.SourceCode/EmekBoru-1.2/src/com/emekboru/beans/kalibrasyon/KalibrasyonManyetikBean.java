/**
 * 
 */
package com.emekboru.beans.kalibrasyon;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.kalibrasyon.KalibrasyonManyetik;
import com.emekboru.jpaman.kalibrasyon.KalibrasyonManyetikManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "kalibrasyonManyetikBean")
@ViewScoped
public class KalibrasyonManyetikBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<KalibrasyonManyetik> allKalibrasyonList = new ArrayList<KalibrasyonManyetik>();
	private KalibrasyonManyetik kalibrasyonForm = new KalibrasyonManyetik();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender = false;

	public KalibrasyonManyetikBean() {
		// fillTestList();
	}

	public void kalibrasyonListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void addKalibrasyon() {
		updateButtonRender = false;
	}

	public void addOrUpdateKalibrasyon(ActionEvent e) {

		KalibrasyonManyetikManager manager = new KalibrasyonManyetikManager();
		if (kalibrasyonForm.getId() == null) {
			kalibrasyonForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			kalibrasyonForm.setEkleyenKullanici(userBean.getUser().getId());

			manager.enterNew(kalibrasyonForm);
			FacesContextUtils.addInfoMessage("SubmitMessage");
		} else {
			if (updateButtonRender) {
				kalibrasyonForm.setGuncellemeZamani(new java.sql.Timestamp(
						new java.util.Date().getTime()));
				kalibrasyonForm.setGuncelleyenKullanici(userBean.getUser()
						.getId());
				manager.updateEntity(kalibrasyonForm);
				FacesContextUtils.addInfoMessage("UpdateItemMessage");
			} else {
				clonner();
			}
		}
		fillTestList();
	}

	@SuppressWarnings("static-access")
	public void fillTestList() {
		KalibrasyonManyetikManager manager = new KalibrasyonManyetikManager();
		allKalibrasyonList = manager.getAllKalibrasyon();
		manager.refreshCollection(allKalibrasyonList);
		kalibrasyonForm = new KalibrasyonManyetik();
	}

	public void deleteKalibrasyon() {

		KalibrasyonManyetikManager manager = new KalibrasyonManyetikManager();
		if (kalibrasyonForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		manager.delete(kalibrasyonForm);
		kalibrasyonForm = new KalibrasyonManyetik();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		fillTestList();
	}

	public void clonner() {

		KalibrasyonManyetik clone = new KalibrasyonManyetik();
		KalibrasyonManyetikManager manager = new KalibrasyonManyetikManager();
		try {
			clone = (KalibrasyonManyetik) this.kalibrasyonForm.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			FacesContextUtils.addPlainErrorMessage(e.getMessage());
		}

		clone.setId(null);
		clone.setEklemeZamani(new java.sql.Timestamp(new java.util.Date()
				.getTime()));
		clone.setEkleyenKullanici(userBean.getUser().getId());

		manager.enterNew(clone);
		FacesContextUtils.addInfoMessage("SubmitMessage");
		fillTestList();
	}

	// getters setters

	/**
	 * @return the allKalibrasyonList
	 */
	public List<KalibrasyonManyetik> getAllKalibrasyonList() {
		return allKalibrasyonList;
	}

	/**
	 * @param allKalibrasyonList
	 *            the allKalibrasyonList to set
	 */
	public void setAllKalibrasyonList(
			List<KalibrasyonManyetik> allKalibrasyonList) {
		this.allKalibrasyonList = allKalibrasyonList;
	}

	/**
	 * @return the kalibrasyonForm
	 */
	public KalibrasyonManyetik getKalibrasyonForm() {
		return kalibrasyonForm;
	}

	/**
	 * @param kalibrasyonForm
	 *            the kalibrasyonForm to set
	 */
	public void setKalibrasyonForm(KalibrasyonManyetik kalibrasyonForm) {
		this.kalibrasyonForm = kalibrasyonForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
