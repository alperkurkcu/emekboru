package com.emekboru.beans.reports;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.CoatMaterialType;
import com.emekboru.jpaman.CoatMaterialTypeManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "coatTypeListBean")
@ViewScoped
public class CoatTypeListBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	private List<CoatMaterialType> typeList;

	public CoatTypeListBean() {

		CoatMaterialTypeManager typeManager = new CoatMaterialTypeManager();
		typeList = typeManager.findAll(CoatMaterialType.class);
	}

	public void goToStockReportPage() {

		FacesContextUtils.redirect(config.getPageUrl().STOCK__REPORT_PAGE);
	}

	/**
	 * GETTERS AND SETTERS
	 */

	public List<CoatMaterialType> getTypeList() {
		return typeList;
	}

	public void setTypeList(List<CoatMaterialType> typeList) {
		this.typeList = typeList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
