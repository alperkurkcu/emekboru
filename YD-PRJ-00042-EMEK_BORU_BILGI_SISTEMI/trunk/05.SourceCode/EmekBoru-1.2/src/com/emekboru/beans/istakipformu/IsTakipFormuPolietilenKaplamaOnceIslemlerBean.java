/**
 * 
 */
package com.emekboru.beans.istakipformu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.istakipformu.IsTakipFormuPolietilenKaplamaOnceIslemler;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isTakipFormuPolietilenKaplamaOnceIslemlerBean")
@ViewScoped
public class IsTakipFormuPolietilenKaplamaOnceIslemlerBean implements
		Serializable {

	private static final long serialVersionUID = 1L;

	private List<IsTakipFormuPolietilenKaplamaOnceIslemler> allIsTakipFormuPolietilenKaplamaOnceIslemlerList = new ArrayList<IsTakipFormuPolietilenKaplamaOnceIslemler>();
	private IsTakipFormuPolietilenKaplamaOnceIslemler isTakipFormuPolietilenKaplamaOnceIslemlerForm = new IsTakipFormuPolietilenKaplamaOnceIslemler();

	// setters getters
	public List<IsTakipFormuPolietilenKaplamaOnceIslemler> getAllIsTakipFormuPolietilenKaplamaOnceIslemlerList() {
		return allIsTakipFormuPolietilenKaplamaOnceIslemlerList;
	}

	public void setAllIsTakipFormuPolietilenKaplamaOnceIslemlerList(
			List<IsTakipFormuPolietilenKaplamaOnceIslemler> allIsTakipFormuPolietilenKaplamaOnceIslemlerList) {
		this.allIsTakipFormuPolietilenKaplamaOnceIslemlerList = allIsTakipFormuPolietilenKaplamaOnceIslemlerList;
	}

	public IsTakipFormuPolietilenKaplamaOnceIslemler getIsTakipFormuPolietilenKaplamaOnceIslemlerForm() {
		return isTakipFormuPolietilenKaplamaOnceIslemlerForm;
	}

	public void setIsTakipFormuPolietilenKaplamaOnceIslemlerForm(
			IsTakipFormuPolietilenKaplamaOnceIslemler isTakipFormuPolietilenKaplamaOnceIslemlerForm) {
		this.isTakipFormuPolietilenKaplamaOnceIslemlerForm = isTakipFormuPolietilenKaplamaOnceIslemlerForm;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
