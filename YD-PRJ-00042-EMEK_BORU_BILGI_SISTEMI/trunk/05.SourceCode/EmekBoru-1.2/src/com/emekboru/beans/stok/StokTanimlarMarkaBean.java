/**
 * 
 */
package com.emekboru.beans.stok;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.stok.StokTanimlarMarka;
import com.emekboru.jpaman.stok.StokTanimlarMarkaManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "stokTanimlarMarkaBean")
@ViewScoped
public class StokTanimlarMarkaBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<StokTanimlarMarka> allStokTanimlarMarkaList = new ArrayList<StokTanimlarMarka>();

	private StokTanimlarMarka stokTanimlarMarkaForm = new StokTanimlarMarka();

	StokTanimlarMarkaManager formManager = new StokTanimlarMarkaManager();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	public StokTanimlarMarkaBean() {

		fillTestList();
	}

	public void addForm() {

		stokTanimlarMarkaForm = new StokTanimlarMarka();
		updateButtonRender = false;
	}

	public void addOrUpdateForm() {

		if (stokTanimlarMarkaForm.getId() == null) {

			stokTanimlarMarkaForm.setEklemeZamani(UtilInsCore.getTarihZaman());
			stokTanimlarMarkaForm.setEkleyenKullanici(userBean.getUser().getId());

			formManager.enterNew(stokTanimlarMarkaForm);

			this.stokTanimlarMarkaForm = new StokTanimlarMarka();
			FacesContextUtils.addInfoMessage("FormSubmitMessage");

		} else {

			stokTanimlarMarkaForm.setGuncellemeZamani(UtilInsCore.getTarihZaman());
			stokTanimlarMarkaForm.setGuncelleyenKullanici(userBean.getUser().getId());
			formManager.updateEntity(stokTanimlarMarkaForm);

			FacesContextUtils.addInfoMessage("FormUpdateMessage");
		}

		fillTestList();
	}

	public void deleteForm() {

		if (stokTanimlarMarkaForm.getId() == null) {

			FacesContextUtils.addWarnMessage("NoSelectMessage");
			return;
		}
		formManager.delete(stokTanimlarMarkaForm);
		fillTestList();
		FacesContextUtils.addWarnMessage("TestDeleteMessage");
	}

	public void fillTestList() {

		Thread thread = new Thread() {
			@Override
			public void run() {
				try {
					StokTanimlarMarkaManager manager = new StokTanimlarMarkaManager();
					allStokTanimlarMarkaList = manager.getAllStokTanimlarMarka();
				} catch (Exception e) {
					e.printStackTrace();
					allStokTanimlarMarkaList.clear();
				}
			}

		};

		thread.run();

	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	// setters getters

	public List<StokTanimlarMarka> getAllStokTanimlarMarkaList() {
		return allStokTanimlarMarkaList;
	}

	public void setAllStokTanimlarMarkaList(List<StokTanimlarMarka> allStokTanimlarMarkaList) {
		this.allStokTanimlarMarkaList = allStokTanimlarMarkaList;
	}

	public StokTanimlarMarka getStokTanimlarMarkaForm() {
		return stokTanimlarMarkaForm;
	}

	public void setStokTanimlarMarkaForm(StokTanimlarMarka stokTanimlarMarkaForm) {
		this.stokTanimlarMarkaForm = stokTanimlarMarkaForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
