/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaFlowCoatStrippingSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaFlowCoatStrippingSonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testKaplamaFlowCoatStrippingSonucBean")
@ViewScoped
public class TestKaplamaFlowCoatStrippingSonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<TestKaplamaFlowCoatStrippingSonuc> allKaplamaFlowCoatStrippingSonucList = new ArrayList<TestKaplamaFlowCoatStrippingSonuc>();
	private TestKaplamaFlowCoatStrippingSonuc testKaplamaFlowCoatStrippingSonucForm = new TestKaplamaFlowCoatStrippingSonuc();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;
	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addKaplamaFlowCoatStrippingSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaFlowCoatStrippingSonucManager tkdsManager = new TestKaplamaFlowCoatStrippingSonucManager();

		if (testKaplamaFlowCoatStrippingSonucForm.getId() == null) {

			testKaplamaFlowCoatStrippingSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaFlowCoatStrippingSonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaFlowCoatStrippingSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaFlowCoatStrippingSonucForm.setBagliTestId(bagliTest);
			testKaplamaFlowCoatStrippingSonucForm.setBagliGlobalId(bagliTest);

			tkdsManager.enterNew(testKaplamaFlowCoatStrippingSonucForm);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaFlowCoatStrippingSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaFlowCoatStrippingSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			tkdsManager.updateEntity(testKaplamaFlowCoatStrippingSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");

		}
		fillTestList(prmGlobalId, prmPipeId);
	}

	public void addKaplamaFlowCoatStripping() {
		testKaplamaFlowCoatStrippingSonucForm = new TestKaplamaFlowCoatStrippingSonuc();
		updateButtonRender = false;
	}

	public void kaplamaFlowCoatStrippingListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(int globalId, Integer pipeId2) {
		allKaplamaFlowCoatStrippingSonucList = TestKaplamaFlowCoatStrippingSonucManager
				.getAllTestKaplamaFlowCoatStrippingSonuc(globalId, pipeId2);
		testKaplamaFlowCoatStrippingSonucForm = new TestKaplamaFlowCoatStrippingSonuc();
	}

	public void deleteTestKaplamaFlowCoatStrippingSonuc() {

		bagliTestId = testKaplamaFlowCoatStrippingSonucForm.getBagliTestId()
				.getId();

		if (testKaplamaFlowCoatStrippingSonucForm == null
				|| testKaplamaFlowCoatStrippingSonucForm.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaFlowCoatStrippingSonucManager tkdsManager = new TestKaplamaFlowCoatStrippingSonucManager();
		tkdsManager.delete(testKaplamaFlowCoatStrippingSonucForm);
		testKaplamaFlowCoatStrippingSonucForm = new TestKaplamaFlowCoatStrippingSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setters getters

	/**
	 * @return the allKaplamaFlowCoatStrippingSonucList
	 */
	public List<TestKaplamaFlowCoatStrippingSonuc> getAllKaplamaFlowCoatStrippingSonucList() {
		return allKaplamaFlowCoatStrippingSonucList;
	}

	/**
	 * @param allKaplamaFlowCoatStrippingSonucList
	 *            the allKaplamaFlowCoatStrippingSonucList to set
	 */
	public void setAllKaplamaFlowCoatStrippingSonucList(
			List<TestKaplamaFlowCoatStrippingSonuc> allKaplamaFlowCoatStrippingSonucList) {
		this.allKaplamaFlowCoatStrippingSonucList = allKaplamaFlowCoatStrippingSonucList;
	}

	/**
	 * @return the testKaplamaFlowCoatStrippingSonucForm
	 */
	public TestKaplamaFlowCoatStrippingSonuc getTestKaplamaFlowCoatStrippingSonucForm() {
		return testKaplamaFlowCoatStrippingSonucForm;
	}

	/**
	 * @param testKaplamaFlowCoatStrippingSonucForm
	 *            the testKaplamaFlowCoatStrippingSonucForm to set
	 */
	public void setTestKaplamaFlowCoatStrippingSonucForm(
			TestKaplamaFlowCoatStrippingSonuc testKaplamaFlowCoatStrippingSonucForm) {
		this.testKaplamaFlowCoatStrippingSonucForm = testKaplamaFlowCoatStrippingSonucForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
