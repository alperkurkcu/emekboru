package com.emekboru.beans.utils;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;

import com.emekboru.jpa.ContactType;
import com.emekboru.jpaman.ContactTypeManager;
import com.emekboru.jpaman.Factory;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean
@SessionScoped
public class ContactTypes implements Serializable {

	private static final long serialVersionUID = 1297703205244694211L;

	private List<ContactType> contactTypes;
	private ContactType selected;
	private ContactType newType;

	public ContactTypes() {
		ContactTypeManager manager = new ContactTypeManager();
		contactTypes = manager.findAll(ContactType.class);
		selected = new ContactType();
		newType = new ContactType();
	}

	@PostConstruct
	public void load() {
		ContactTypeManager manager = new ContactTypeManager();
		contactTypes = manager.findAll(ContactType.class);
		System.out.println("how many contact types: " + contactTypes.size());
	}

	public void add() {

		ContactTypeManager manager = new ContactTypeManager();
		manager.enterNew(newType);
		contactTypes.add(newType);
		newType = new ContactType();
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void update() {

		ContactTypeManager manager = new ContactTypeManager();
		manager.updateEntity(selected);
		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}

	public void delete() {

		if (selected.getContactTypeId() == null) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		EntityManager manager = Factory.getInstance().createEntityManager();
		selected = manager.find(ContactType.class, selected.getContactTypeId());
		manager.getTransaction().begin();
		manager.remove(selected);
		manager.getTransaction().commit();
		manager.close();
		contactTypes.remove(selected);
		selected = new ContactType();
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	/**
	 * GETTERS AND SETTERS
	 */

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ContactType getSelected() {
		return selected;
	}

	public void setSelected(ContactType selected) {
		this.selected = selected;
	}

	public ContactType getNewType() {
		return newType;
	}

	public void setNewType(ContactType newType) {
		this.newType = newType;
	}

	public List<ContactType> getContactTypes() {
		return contactTypes;
	}

	public void setContactTypes(List<ContactType> contactTypes) {
		this.contactTypes = contactTypes;
	}
}
