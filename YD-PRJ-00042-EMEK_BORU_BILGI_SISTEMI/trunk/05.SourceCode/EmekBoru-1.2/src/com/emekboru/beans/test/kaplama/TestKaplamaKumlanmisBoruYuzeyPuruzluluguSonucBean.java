/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucBean")
@ViewScoped
public class TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonucBean implements
		Serializable {

	private static final long serialVersionUID = 1L;

	private TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm = new TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc();
	private List<TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc> allKaplamaKumlanmisBoruYuzeyPuruzluluguSonucList = new ArrayList<TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	float ortalamaA = 0F;
	float ortalamaB = 0F;
	float ortalamaOrta = 0F;

	public void addOrUpdateKaplamaKumlanmisBoruYuzeyPuruzluluguTestSonuc(
			ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonucManager tkbsManager = new TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonucManager();

		if (testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm.getId() == null) {

			testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm
					.setEkleyenKullanici(userBean.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm
					.setBagliTestId(bagliTest);
			testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm
					.setBagliGlobalId(bagliTest);

			ortalamaBul();

			tkbsManager
					.enterNew(testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			ortalamaBul();
			tkbsManager
					.updateEntity(testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	private void ortalamaBul() {

		ortalamaA = (testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm
				.getaUcuMin().floatValue() + testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm
				.getaUcuMax().floatValue()) / 2;
		testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm.setaUcuOrt(BigDecimal
				.valueOf(ortalamaA));

		ortalamaB = (testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm
				.getbUcuMin().floatValue() + testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm
				.getbUcuMax().floatValue()) / 2;
		testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm.setbUcuOrt(BigDecimal
				.valueOf(ortalamaB));

		ortalamaOrta = (testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm
				.getOrtaUcuMin().floatValue() + testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm
				.getOrtaUcuMax().floatValue()) / 2;
		testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm
				.setOrtaUcuOrt(BigDecimal.valueOf(ortalamaOrta));
	}

	public void addKaplamaKumlanmisBoruYuzeyPuruzluluguTest() {

		testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm = new TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc();
		updateButtonRender = false;
	}

	public void kaplamaKumlanmisBoruYuzeyPuruzluluguListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allKaplamaKumlanmisBoruYuzeyPuruzluluguSonucList = TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonucManager
				.getAllTestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc(globalId,
						pipeId);
		addKaplamaKumlanmisBoruYuzeyPuruzluluguTest();
	}

	public void deleteTestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc() {
		bagliTestId = testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm
				.getBagliTestId().getId();
		if (testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonucManager tkbsManager = new TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonucManager();

		tkbsManager.delete(testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm);
		testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm = new TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getter

	/**
	 * @return the testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm
	 */
	public TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc getTestKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm() {
		return testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm;
	}

	/**
	 * @param testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm
	 *            the testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm to set
	 */
	public void setTestKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm(
			TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm) {
		this.testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm = testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucForm;
	}

	/**
	 * @return the allKaplamaKumlanmisBoruYuzeyPuruzluluguSonucList
	 */
	public List<TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc> getAllKaplamaKumlanmisBoruYuzeyPuruzluluguSonucList() {
		return allKaplamaKumlanmisBoruYuzeyPuruzluluguSonucList;
	}

	/**
	 * @param allKaplamaKumlanmisBoruYuzeyPuruzluluguSonucList
	 *            the allKaplamaKumlanmisBoruYuzeyPuruzluluguSonucList to set
	 */
	public void setAllKaplamaKumlanmisBoruYuzeyPuruzluluguSonucList(
			List<TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc> allKaplamaKumlanmisBoruYuzeyPuruzluluguSonucList) {
		this.allKaplamaKumlanmisBoruYuzeyPuruzluluguSonucList = allKaplamaKumlanmisBoruYuzeyPuruzluluguSonucList;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
