/**
 * 
 */
package com.emekboru.beans.stok;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.stok.StokJoinMarkaUreticiKodu;
import com.emekboru.jpaman.stok.StokJoinMarkaUreticiKoduManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "stokJoinMarkaUreticiKoduBean")
@ViewScoped
public class StokJoinMarkaUreticiKoduBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<StokJoinMarkaUreticiKodu> allStokJoinMarkaUreticiKoduList = new ArrayList<StokJoinMarkaUreticiKodu>();

	private StokJoinMarkaUreticiKodu stokJoinMarkaUreticiKoduForm = new StokJoinMarkaUreticiKodu();

	private StokJoinMarkaUreticiKodu[] selectedMarkalar;

	StokJoinMarkaUreticiKoduManager formManager = new StokJoinMarkaUreticiKoduManager();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	Integer prmUreticiKoduMarkaId = 0;
	Integer prmUreticiKoduUrunId = 0;

	public StokJoinMarkaUreticiKoduBean() {

		allStokJoinMarkaUreticiKoduList = null;
		updateButtonRender = false;
	}

	public void addForm(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		prmUreticiKoduMarkaId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		prmUreticiKoduUrunId = (Integer) cmd.getChildren().get(1)
				.getAttributes().get("value");

		stokJoinMarkaUreticiKoduForm = new StokJoinMarkaUreticiKodu();
		updateButtonRender = false;
	}

	public void addOrUpdateForm() {

		if (stokJoinMarkaUreticiKoduForm.getJoinId() == null) {

			stokJoinMarkaUreticiKoduForm.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			stokJoinMarkaUreticiKoduForm.setEkleyenKullanici(userBean.getUser()
					.getId());
			stokJoinMarkaUreticiKoduForm.getStokTanimlarMarka().setId(
					prmUreticiKoduMarkaId);
			stokJoinMarkaUreticiKoduForm.getStokUrunKartlari().setId(
					prmUreticiKoduUrunId);

			formManager.enterNew(stokJoinMarkaUreticiKoduForm);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"Üretici Kodu Başarıyla Eklenmiştir!"));

		} else {

			stokJoinMarkaUreticiKoduForm.setGuncellemeZamani(UtilInsCore
					.getTarihZaman());
			stokJoinMarkaUreticiKoduForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			formManager.updateEntity(stokJoinMarkaUreticiKoduForm);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"Üretici Kodu Başarıyla Güncellenmiştir!"));
		}

		fillTestList(prmUreticiKoduUrunId, prmUreticiKoduMarkaId);
	}

	public void deleteForm() {

		if (stokJoinMarkaUreticiKoduForm.getJoinId() == null) {

			FacesContextUtils.addWarnMessage("NoSelectMessage");
			return;
		}

		formManager.delete(stokJoinMarkaUreticiKoduForm);
		fillTestList(prmUreticiKoduUrunId, prmUreticiKoduMarkaId);

		FacesContextUtils.addWarnMessage("TestDeleteMessage");
	}

	public void fillTestList(Integer urunId, Integer markaId) {

		StokJoinMarkaUreticiKoduManager manager = new StokJoinMarkaUreticiKoduManager();
		allStokJoinMarkaUreticiKoduList = manager
				.getAllStokJoinMarkaUreticiKodu(urunId, markaId);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"ÜRÜN ÜRETİCİ KODLARI BAŞARI İLE LİSTELENMİŞTİR"));
	}

	public void fillTestList(ActionEvent e) {// sayfa ilk açılırken prmFormId
												// alıyor

		UICommand cmd = (UICommand) e.getComponent();
		prmUreticiKoduMarkaId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		prmUreticiKoduUrunId = (Integer) cmd.getChildren().get(1)
				.getAttributes().get("value");

		StokJoinMarkaUreticiKoduManager manager = new StokJoinMarkaUreticiKoduManager();
		allStokJoinMarkaUreticiKoduList = manager
				.getAllStokJoinMarkaUreticiKodu(prmUreticiKoduUrunId,
						prmUreticiKoduMarkaId);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"Ürünün tüm Üretici Kodları Listelenmiştir!"));
	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	// setters getters

	public List<StokJoinMarkaUreticiKodu> getAllStokJoinMarkaUreticiKoduList() {
		return allStokJoinMarkaUreticiKoduList;
	}

	public void setAllStokJoinMarkaUreticiKoduList(
			List<StokJoinMarkaUreticiKodu> allStokJoinMarkaUreticiKoduList) {
		this.allStokJoinMarkaUreticiKoduList = allStokJoinMarkaUreticiKoduList;
	}

	public StokJoinMarkaUreticiKodu getStokJoinMarkaUreticiKoduForm() {
		return stokJoinMarkaUreticiKoduForm;
	}

	public void setStokJoinMarkaUreticiKoduForm(
			StokJoinMarkaUreticiKodu stokJoinMarkaUreticiKoduForm) {
		this.stokJoinMarkaUreticiKoduForm = stokJoinMarkaUreticiKoduForm;
	}

	public StokJoinMarkaUreticiKodu[] getSelectedMarkalar() {
		return selectedMarkalar;
	}

	public void setSelectedMarkalar(StokJoinMarkaUreticiKodu[] selectedMarkalar) {
		this.selectedMarkalar = selectedMarkalar;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
