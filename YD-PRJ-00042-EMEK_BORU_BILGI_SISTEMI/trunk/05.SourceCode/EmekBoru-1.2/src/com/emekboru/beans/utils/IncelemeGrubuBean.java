package com.emekboru.beans.utils;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.IncelemeGrubu;
import com.emekboru.jpaman.IncelemeGrubuManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "incelemeGrubuBean")
@ViewScoped
public class IncelemeGrubuBean {

	private IncelemeGrubu incelemeGrubuForm = new IncelemeGrubu();
	private List<IncelemeGrubu> allIncelemeGrubuList = new ArrayList<IncelemeGrubu>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender = false;

	public IncelemeGrubuBean() {

		allIncelemeGrubuList = IncelemeGrubuManager.getAllIncelemeGrubu();
	}

	public void addOrUpdateIncelemeGrubu(ActionEvent e) {

		IncelemeGrubuManager manager = new IncelemeGrubuManager();
		if (incelemeGrubuForm.getId() == null) {
			incelemeGrubuForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			incelemeGrubuForm.setEkleyenKullanici(userBean.getUser().getId());

			manager.enterNew(incelemeGrubuForm);
			FacesContextUtils.addInfoMessage("SubmitMessage");
		} else {
			incelemeGrubuForm.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			incelemeGrubuForm.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			manager.updateEntity(incelemeGrubuForm);
			FacesContextUtils.addInfoMessage("UpdateItemMessage");
		}
		fillTestList();
	}

	public void fillTestList() {
		allIncelemeGrubuList = IncelemeGrubuManager.getAllIncelemeGrubu();
	}

	public void deleteIncelemeGrubu() {

		IncelemeGrubuManager manager = new IncelemeGrubuManager();
		if (incelemeGrubuForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		manager.delete(incelemeGrubuForm);
		incelemeGrubuForm = new IncelemeGrubu();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		fillTestList();
	}

	public void incelemeGrubuListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void addIncelemeGrubu() {

		incelemeGrubuForm = new IncelemeGrubu();
		updateButtonRender = false;
	}

	// setters getters
	public IncelemeGrubu getIncelemeGrubuForm() {
		return incelemeGrubuForm;
	}

	public void setIncelemeGrubuForm(IncelemeGrubu incelemeGrubuForm) {
		this.incelemeGrubuForm = incelemeGrubuForm;
	}

	public List<IncelemeGrubu> getAllIncelemeGrubuList() {
		return allIncelemeGrubuList;
	}

	public void setAllIncelemeGrubuList(List<IncelemeGrubu> allIncelemeGrubuList) {
		this.allIncelemeGrubuList = allIncelemeGrubuList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
