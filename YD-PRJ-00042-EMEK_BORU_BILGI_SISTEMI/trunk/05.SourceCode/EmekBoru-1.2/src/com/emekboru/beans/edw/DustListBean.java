package com.emekboru.beans.edw;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AjaxBehaviorEvent;

import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.ElectrodeDustWire;
import com.emekboru.jpaman.ElectrodeDustWireManager;
import com.emekboru.messages.ElectrodeDustWireType;
import com.emekboru.utils.DateTrans;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "dustListBean")
@ViewScoped
public class DustListBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	private List<ElectrodeDustWire> recentDusts;
	private List<ElectrodeDustWire> dusts;
	private List<Integer> dustYearList;
	private int currentYear;

	public DustListBean() {

		dusts = new ArrayList<ElectrodeDustWire>();
		recentDusts = new ArrayList<ElectrodeDustWire>();
		dustYearList = new ArrayList<Integer>();
		currentYear = DateTrans.getYearOfDate(new Date());
	}

	@PostConstruct
	public void init() {

		loadDustYears();
		loadRecentDusts();
		loadYearDusts(currentYear);
	}

	public void loadRecentDusts() {

		ElectrodeDustWireManager man = new ElectrodeDustWireManager();
		recentDusts = man.findRecent(100, ElectrodeDustWireType.DUST);
	}

	public void loadYearDusts(int year) {

		DateTrans.calendar.set(year, Calendar.JANUARY, 1, 0, 1);
		Date startDate = DateTrans.calendar.getTime();
		DateTrans.calendar.set(year, Calendar.DECEMBER, 31, 23, 59);
		Date endDate = DateTrans.calendar.getTime();
		ElectrodeDustWireManager dustManager = new ElectrodeDustWireManager();
		dusts = dustManager.findTypeBetweenDates(ElectrodeDustWireType.DUST,
				startDate, endDate);
	}

	public void loadSelectedYearDusts(AjaxBehaviorEvent event) {

		HtmlSelectOneMenu one = (HtmlSelectOneMenu) event.getSource();
		currentYear = Integer.valueOf(one.getValue().toString());
		loadYearDusts(currentYear);
	}

	/******************************************************************************
	 ******************* HELPER METHODS SECTION ******************** /
	 ******************************************************************************/
	protected void loadDustYears() {

		dustYearList = new ArrayList<Integer>();
		ElectrodeDustWireManager dustManager = new ElectrodeDustWireManager();
		List<Date> minList = dustManager
				.findMinYearForType(ElectrodeDustWireType.DUST);
		Integer min, max;
		if (minList.size() == 1 && minList.get(0) == null) {

			min = 0;
			max = 0;
			return;
		}
		min = DateTrans.getYearOfDate(minList.get(0));
		max = DateTrans.getYearOfDate(new Date());
		for (int i = min; i <= max; i++)
			dustYearList.add(i);
	}

	public void goToDustPage() {

		FacesContextUtils.redirect(config.getPageUrl().DUST_PAGE);
	}

	/******************************************************************************
	 ******************* GETTERS AND SETTERS ******************** /
	 ******************************************************************************/
	public List<Integer> getDustYearList() {
		return dustYearList;
	}

	public void setDustYearList(List<Integer> dustYearList) {
		this.dustYearList = dustYearList;
	}

	public Integer getCurrentYear() {
		return currentYear;
	}

	public void setCurrentYear(Integer currentYear) {
		this.currentYear = currentYear;
	}

	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	public List<ElectrodeDustWire> getRecentDusts() {
		return recentDusts;
	}

	public void setRecentDusts(List<ElectrodeDustWire> recentDusts) {
		this.recentDusts = recentDusts;
	}

	public List<ElectrodeDustWire> getDusts() {
		return dusts;
	}

	public void setDusts(List<ElectrodeDustWire> dusts) {
		this.dusts = dusts;
	}
}
