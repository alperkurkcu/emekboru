/**
 * 
 */
package com.emekboru.beans.reports.test;

/**
 * @author kursat
 *
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.DestructiveTestsSpec;
import com.emekboru.jpa.HidrostaticTestsSpec;
import com.emekboru.jpa.MachinePipeLink;
import com.emekboru.jpa.NondestructiveTestsSpec;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.machines.MachineDuruslarAriza;
import com.emekboru.jpa.rulo.Rulo;
import com.emekboru.jpa.rulo.RuloOzellikBoyutsal;
import com.emekboru.jpa.rulo.RuloPipeLink;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.DestructiveTestsSpecManager;
import com.emekboru.jpaman.HidrostaticTestsSpecManager;
import com.emekboru.jpaman.MachinePipeLinkManager;
import com.emekboru.jpaman.NondestructiveTestsSpecManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.machines.MachineDuruslarArizaManager;
import com.emekboru.jpaman.rulo.RuloManager;
import com.emekboru.jpaman.rulo.RuloPipeLinkManager;
import com.emekboru.jpaman.sales.order.SalesItemManager;
import com.emekboru.utils.FileOperations;
import com.emekboru.utils.ReportUtil;

import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;

@ManagedBean(name = "testReportBean")
@ViewScoped
public class TestReportBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private Pipe pipe = new Pipe();

	public String ruloYear = null;

	@PostConstruct
	public void init() {
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void imalatRaporuXlsx() {

		Double sp2DayShiftPipesOrtalama = 0D;
		Double sp3DayShiftPipesOrtalama = 0D;
		Double sp5DayShiftPipesOrtalama = 0D;

		Double sp2NightShiftPipesOrtalama = 0D;
		Double sp3NightShiftPipesOrtalama = 0D;
		Double sp5NightShiftPipesOrtalama = 0D;

		// gündüz gece vardiya parametresi
		String sp2DayShift = null;
		String sp3DayShift = null;
		String sp5DayShift = null;

		String sp2NightShift = null;
		String sp3NightShift = null;
		String sp5NightShift = null;

		// vardiyelerde üretilen borular
		List<MachinePipeLink> sp2DayShiftPipes = new ArrayList<MachinePipeLink>();
		List<MachinePipeLink> sp3DayShiftPipes = new ArrayList<MachinePipeLink>();
		List<MachinePipeLink> sp5DayShiftPipes = new ArrayList<MachinePipeLink>();

		List<MachinePipeLink> sp2NightShiftPipes = new ArrayList<MachinePipeLink>();
		List<MachinePipeLink> sp3NightShiftPipes = new ArrayList<MachinePipeLink>();
		List<MachinePipeLink> sp5NightShiftPipes = new ArrayList<MachinePipeLink>();

		// vardiyelerde üretilen boruların rulosu
		List<RuloPipeLink> sp2DayShiftRulos = new ArrayList<RuloPipeLink>();
		List<RuloPipeLink> sp3DayShiftRulos = new ArrayList<RuloPipeLink>();
		List<RuloPipeLink> sp5DayShiftRulos = new ArrayList<RuloPipeLink>();

		List<RuloPipeLink> sp2NightShiftRulos = new ArrayList<RuloPipeLink>();
		List<RuloPipeLink> sp3NightShiftRulos = new ArrayList<RuloPipeLink>();
		List<RuloPipeLink> sp5NightShiftRulos = new ArrayList<RuloPipeLink>();

		// vardiyelerde üretilen boruların rulo bilgileri
		RuloOzellikBoyutsal sp2DayShiftRulosSpec = new RuloOzellikBoyutsal();
		RuloOzellikBoyutsal sp3DayShiftRulosSpec = new RuloOzellikBoyutsal();
		RuloOzellikBoyutsal sp5DayShiftRulosSpec = new RuloOzellikBoyutsal();

		RuloOzellikBoyutsal sp2NightShiftRulosSpec = new RuloOzellikBoyutsal();
		RuloOzellikBoyutsal sp3NightShiftRulosSpec = new RuloOzellikBoyutsal();
		RuloOzellikBoyutsal sp5NightShiftRulosSpec = new RuloOzellikBoyutsal();

		// vardiyelerde üretilen boruların sipariş bilgileri
		List<SalesItem> sp2DayShiftSales = new ArrayList<SalesItem>();
		List<SalesItem> sp3DayShiftSales = new ArrayList<SalesItem>();
		List<SalesItem> sp5DayShiftSales = new ArrayList<SalesItem>();

		List<SalesItem> sp2NightShiftSales = new ArrayList<SalesItem>();
		List<SalesItem> sp3NightShiftSales = new ArrayList<SalesItem>();
		List<SalesItem> sp5NightShiftSales = new ArrayList<SalesItem>();

		// vardiye duruş bilgileri
		List<MachineDuruslarAriza> sp2DayAriza = new ArrayList<MachineDuruslarAriza>();
		List<MachineDuruslarAriza> sp3DayAriza = new ArrayList<MachineDuruslarAriza>();
		List<MachineDuruslarAriza> sp5DayAriza = new ArrayList<MachineDuruslarAriza>();

		List<MachineDuruslarAriza> sp2NightAriza = new ArrayList<MachineDuruslarAriza>();
		List<MachineDuruslarAriza> sp3NightAriza = new ArrayList<MachineDuruslarAriza>();
		List<MachineDuruslarAriza> sp5NightAriza = new ArrayList<MachineDuruslarAriza>();

		MachinePipeLinkManager machinePipeLinkManager = new MachinePipeLinkManager();
		RuloPipeLinkManager ruloPipeLinkManager = new RuloPipeLinkManager();
		SalesItemManager salesItemManager = new SalesItemManager();
		MachineDuruslarArizaManager arizaManager = new MachineDuruslarArizaManager();

		// spirallerdeki dünkü vardiya arizalari
		sp2DayAriza = arizaManager.findYesterdaysDayShiftAriza(53);
		sp3DayAriza = arizaManager.findYesterdaysDayShiftAriza(54);
		sp5DayAriza = arizaManager.findYesterdaysDayShiftAriza(55);

		sp2NightAriza = arizaManager.findYesterdaysNightShiftAriza(53);
		sp3NightAriza = arizaManager.findYesterdaysNightShiftAriza(54);
		sp5NightAriza = arizaManager.findYesterdaysNightShiftAriza(55);

		// spirallerdeki dünkü vardiyalarda üretilmiş borular
		try {
			sp2DayShiftPipes = machinePipeLinkManager
					.findYesterdaysDayShiftProduction(53);
			sp3DayShiftPipes = machinePipeLinkManager
					.findYesterdaysDayShiftProduction(54);
			sp5DayShiftPipes = machinePipeLinkManager
					.findYesterdaysDayShiftProduction(55);

			for (int i = 0; i < sp2DayShiftPipes.size(); i++) {
				sp2DayShiftPipesOrtalama += sp2DayShiftPipes.get(i).getPipe()
						.getBoy();
			}
			sp2DayShiftPipesOrtalama = sp2DayShiftPipesOrtalama
					/ sp2DayShiftPipes.size();
			for (int i = 0; i < sp3DayShiftPipes.size(); i++) {
				sp3DayShiftPipesOrtalama += sp3DayShiftPipes.get(i).getPipe()
						.getBoy();
			}
			sp3DayShiftPipesOrtalama = sp3DayShiftPipesOrtalama
					/ sp3DayShiftPipes.size();
			for (int i = 0; i < sp5DayShiftPipes.size(); i++) {
				sp5DayShiftPipesOrtalama += sp5DayShiftPipes.get(i).getPipe()
						.getBoy();
			}
			sp5DayShiftPipesOrtalama = sp5DayShiftPipesOrtalama
					/ sp5DayShiftPipes.size();

			for (int i = 0; i < sp2NightShiftPipes.size(); i++) {
				sp2NightShiftPipesOrtalama += sp2NightShiftPipes.get(i)
						.getPipe().getBoy();
			}
			sp2NightShiftPipesOrtalama = sp2NightShiftPipesOrtalama
					/ sp2NightShiftPipes.size();
			for (int i = 0; i < sp3NightShiftPipes.size(); i++) {
				sp3NightShiftPipesOrtalama += sp3NightShiftPipes.get(i)
						.getPipe().getBoy();
			}
			sp3NightShiftPipesOrtalama = sp3NightShiftPipesOrtalama
					/ sp3NightShiftPipes.size();
			for (int i = 0; i < sp5NightShiftPipes.size(); i++) {
				sp5NightShiftPipesOrtalama += sp5NightShiftPipes.get(i)
						.getPipe().getBoy();
			}
			sp5NightShiftPipesOrtalama = sp5NightShiftPipesOrtalama
					/ sp5NightShiftPipes.size();

		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"GÜNDÜZ ÜRETİLMİŞ BORULAR BULUNAMADI!", null));
			return;
		}

		try {
			sp2NightShiftPipes = machinePipeLinkManager
					.findYesterdaysNightShiftProduction(53);
			sp3NightShiftPipes = machinePipeLinkManager
					.findYesterdaysNightShiftProduction(54);
			sp5NightShiftPipes = machinePipeLinkManager
					.findYesterdaysNightShiftProduction(55);
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"GECE ÜRETİLMİŞ BORULAR BULUNAMADI!", null));
			return;
		}

		// spirallerdeki dünkü vardiyalarda üretilmiş boruların ruloları
		if (sp2DayShiftPipes.size() > 0) {
			sp2DayShiftRulos = ruloPipeLinkManager
					.findByPipeId(sp2DayShiftPipes.get(0).getPipe().getPipeId());
			sp2DayShiftRulosSpec = sp2DayShiftRulos.get(0).getRulo()
					.getRuloOzellikBoyutsal();
		} else if (sp3DayShiftPipes.size() > 0) {
			sp3DayShiftRulos = ruloPipeLinkManager
					.findByPipeId(sp3DayShiftPipes.get(0).getPipe().getPipeId());
			sp3DayShiftRulosSpec = sp3DayShiftRulos.get(0).getRulo()
					.getRuloOzellikBoyutsal();
		} else if (sp5DayShiftPipes.size() > 0) {
			sp5DayShiftRulos = ruloPipeLinkManager
					.findByPipeId(sp5DayShiftPipes.get(0).getPipe().getPipeId());
			sp5DayShiftRulosSpec = sp5DayShiftRulos.get(0).getRulo()
					.getRuloOzellikBoyutsal();
		}

		if (sp2NightShiftPipes.size() > 0) {
			sp2NightShiftRulos = ruloPipeLinkManager
					.findByPipeId(sp2NightShiftPipes.get(0).getPipe()
							.getPipeId());
			sp2NightShiftRulosSpec = sp2NightShiftRulos.get(0).getRulo()
					.getRuloOzellikBoyutsal();
		} else if (sp3NightShiftPipes.size() > 0) {
			sp3NightShiftRulos = ruloPipeLinkManager
					.findByPipeId(sp3NightShiftPipes.get(0).getPipe()
							.getPipeId());
			sp3NightShiftRulosSpec = sp3NightShiftRulos.get(0).getRulo()
					.getRuloOzellikBoyutsal();
		} else if (sp5NightShiftPipes.size() > 0) {
			sp5NightShiftRulos = ruloPipeLinkManager
					.findByPipeId(sp5NightShiftPipes.get(0).getPipe()
							.getPipeId());
			sp5NightShiftRulosSpec = sp5NightShiftRulos.get(0).getRulo()
					.getRuloOzellikBoyutsal();
		}

		Double sp2DayShiftPipesTotalLength = 0D;
		Double sp3DayShiftPipesTotalLength = 0D;
		Double sp5DayShiftPipesTotalLength = 0D;

		Double sp2NightShiftPipesTotalLength = 0D;
		Double sp3NightShiftPipesTotalLength = 0D;
		Double sp5NightShiftPipesTotalLength = 0D;

		Double sp2DayShiftPipesTotalWeigth = 0D;
		Double sp3DayShiftPipesTotalWeigth = 0D;
		Double sp5DayShiftPipesTotalWeigth = 0D;

		Double sp2NightShiftPipesTotalWeigth = 0D;
		Double sp3NightShiftPipesTotalWeigth = 0D;
		Double sp5NightShiftPipesTotalWeigth = 0D;

		if (sp2DayShiftPipes.size() > 0) {
			sp2DayShift = "08:00-20:00";
			sp2DayShiftSales = salesItemManager.findByField(SalesItem.class,
					"itemId", sp2DayShiftPipes.get(0).getPipe().getSalesItem()
							.getItemId());
			for (int i = 0; i < sp2DayShiftPipes.size(); i++) {
				sp2DayShiftPipesTotalLength = sp2DayShiftPipesTotalLength
						+ sp2DayShiftPipes.get(i).getPipe().getBoy();
				sp2DayShiftPipesTotalWeigth = sp2DayShiftPipesTotalWeigth
						+ sp2DayShiftPipes.get(i).getPipe().getAgirligi();
			}
		}
		if (sp3DayShiftPipes.size() > 0) {
			sp3DayShift = "08:00-20:00";
			sp3DayShiftSales = salesItemManager.findByField(SalesItem.class,
					"itemId", sp3DayShiftPipes.get(0).getPipe().getSalesItem()
							.getItemId());
			for (int i = 0; i < sp3DayShiftPipes.size(); i++) {
				sp3DayShiftPipesTotalLength = sp3DayShiftPipesTotalLength
						+ sp3DayShiftPipes.get(i).getPipe().getBoy();
				sp3DayShiftPipesTotalWeigth = sp3DayShiftPipesTotalWeigth
						+ sp3DayShiftPipes.get(i).getPipe().getAgirligi();
			}
		}

		if (sp5DayShiftPipes.size() > 0) {
			sp5DayShift = "08:00-20:00";
			sp5DayShiftSales = salesItemManager.findByField(SalesItem.class,
					"itemId", sp5DayShiftPipes.get(0).getPipe().getSalesItem()
							.getItemId());
			for (int i = 0; i < sp5DayShiftPipes.size(); i++) {
				sp5DayShiftPipesTotalLength = sp5DayShiftPipesTotalLength
						+ sp5DayShiftPipes.get(i).getPipe().getBoy();
				sp5DayShiftPipesTotalWeigth = sp5DayShiftPipesTotalWeigth
						+ sp5DayShiftPipes.get(i).getPipe().getAgirligi();
			}
		}

		if (sp2NightShiftPipes.size() > 0) {
			sp2NightShift = "20:00-08:00";
			sp2NightShiftSales = salesItemManager.findByField(SalesItem.class,
					"itemId", sp2NightShiftPipes.get(0).getPipe()
							.getSalesItem().getItemId());
			for (int i = 0; i < sp2NightShiftPipes.size(); i++) {
				sp2NightShiftPipesTotalLength = sp2NightShiftPipesTotalLength
						+ sp2NightShiftPipes.get(i).getPipe().getBoy();
				sp2NightShiftPipesTotalWeigth = sp2NightShiftPipesTotalWeigth
						+ sp2NightShiftPipes.get(i).getPipe().getAgirligi();
			}
		}
		if (sp3NightShiftPipes.size() > 0) {
			sp3NightShift = "20:00-08:00";
			sp3NightShiftSales = salesItemManager.findByField(SalesItem.class,
					"itemId", sp3NightShiftPipes.get(0).getPipe()
							.getSalesItem().getItemId());
			for (int i = 0; i < sp3NightShiftPipes.size(); i++) {
				sp3NightShiftPipesTotalLength = sp3NightShiftPipesTotalLength
						+ sp3NightShiftPipes.get(i).getPipe().getBoy();
				sp3NightShiftPipesTotalWeigth = sp3NightShiftPipesTotalWeigth
						+ sp3NightShiftPipes.get(i).getPipe().getAgirligi();
			}
		}

		if (sp5NightShiftPipes.size() > 0) {
			sp5NightShift = "20:00-08:00";
			sp5NightShiftSales = salesItemManager.findByField(SalesItem.class,
					"itemId", sp5NightShiftPipes.get(0).getPipe()
							.getSalesItem().getItemId());
			for (int i = 0; i < sp5NightShiftPipes.size(); i++) {
				sp5NightShiftPipesTotalLength = sp5NightShiftPipesTotalLength
						+ sp5NightShiftPipes.get(i).getPipe().getBoy();
				sp5NightShiftPipesTotalWeigth = sp5NightShiftPipesTotalWeigth
						+ sp5NightShiftPipes.get(i).getPipe().getAgirligi();
			}
		}

		List<String> date = new ArrayList<String>();
		date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

		Map dataMap = new HashMap();
		// siparişler
		dataMap.put("sp2DayShiftSales", sp2DayShiftSales);
		dataMap.put("sp3DayShiftSales", sp3DayShiftSales);
		dataMap.put("sp5DayShiftSales", sp5DayShiftSales);

		dataMap.put("sp2NightShiftSales", sp2NightShiftSales);
		dataMap.put("sp3NightShiftSales", sp3NightShiftSales);
		dataMap.put("sp5NightShiftSales", sp5NightShiftSales);

		// borular
		dataMap.put("sp2DayShiftPipes", sp2DayShiftPipes);
		dataMap.put("sp3DayShiftPipes", sp3DayShiftPipes);
		dataMap.put("sp5DayShiftPipes", sp5DayShiftPipes);

		dataMap.put("sp2NightShiftPipes", sp2NightShiftPipes);
		dataMap.put("sp3NightShiftPipes", sp3NightShiftPipes);
		dataMap.put("sp5NightShiftPipes", sp5NightShiftPipes);

		dataMap.put("sp2DayShiftPipesOrtalama", sp2DayShiftPipesOrtalama);
		dataMap.put("sp3DayShiftPipesOrtalama", sp3DayShiftPipesOrtalama);
		dataMap.put("sp5DayShiftPipesOrtalama", sp5DayShiftPipesOrtalama);

		dataMap.put("sp2NightShiftPipesOrtalama", sp2NightShiftPipesOrtalama);
		dataMap.put("sp3NightShiftPipesOrtalama", sp3NightShiftPipesOrtalama);
		dataMap.put("sp5NightShiftPipesOrtalama", sp5NightShiftPipesOrtalama);

		// rulolar
		dataMap.put("sp2DayShiftRulos", sp2DayShiftRulosSpec);
		dataMap.put("sp3DayShiftRulos", sp3DayShiftRulosSpec);
		dataMap.put("sp5DayShiftRulos", sp5DayShiftRulosSpec);

		dataMap.put("sp2NightShiftRulos", sp2NightShiftRulosSpec);
		dataMap.put("sp3NightShiftRulos", sp3NightShiftRulosSpec);
		dataMap.put("sp5NightShiftRulos", sp5NightShiftRulosSpec);

		// veriler
		dataMap.put("sp2DayShiftPipesTotal", sp2DayShiftPipes.size());
		dataMap.put("sp3DayShiftPipesTotal", sp3DayShiftPipes.size());
		dataMap.put("sp5DayShiftPipesTotal", sp5DayShiftPipes.size());

		dataMap.put("sp2NightShiftPipesTotal", sp2NightShiftPipes.size());
		dataMap.put("sp3NightShiftPipesTotal", sp3NightShiftPipes.size());
		dataMap.put("sp5NightShiftPipesTotal", sp5NightShiftPipes.size());

		dataMap.put("sp2DayShiftPipesTotalLength",
				round(sp2DayShiftPipesTotalLength / 1000, 2));
		dataMap.put("sp3DayShiftPipesTotalLength",
				round(sp3DayShiftPipesTotalLength / 1000, 2));
		dataMap.put("sp5DayShiftPipesTotalLength",
				round(sp5DayShiftPipesTotalLength / 1000, 2));

		dataMap.put("sp2NightShiftPipesTotalLength",
				round(sp2NightShiftPipesTotalLength / 1000, 2));
		dataMap.put("sp3NightShiftPipesTotalLength",
				round(sp3NightShiftPipesTotalLength / 1000, 2));
		dataMap.put("sp5NightShiftPipesTotalLength",
				round(sp5NightShiftPipesTotalLength / 1000, 2));

		dataMap.put("sp2DayShiftPipesTotalWeigth",
				round(sp2DayShiftPipesTotalWeigth, 0));
		dataMap.put("sp3DayShiftPipesTotalWeigth",
				round(sp3DayShiftPipesTotalWeigth, 0));
		dataMap.put("sp5DayShiftPipesTotalWeigth",
				round(sp5DayShiftPipesTotalWeigth, 0));

		dataMap.put("sp2NightShiftPipesTotalWeigth",
				round(sp2NightShiftPipesTotalWeigth, 0));
		dataMap.put("sp3NightShiftPipesTotalWeigth",
				round(sp3NightShiftPipesTotalWeigth, 0));
		dataMap.put("sp5NightShiftPipesTotalWeigth",
				round(sp5NightShiftPipesTotalWeigth, 0));

		dataMap.put("date", date);
		dataMap.put("sp2DayShift", sp2DayShift);
		dataMap.put("sp3DayShift", sp3DayShift);
		dataMap.put("sp5DayShift", sp5DayShift);

		dataMap.put("sp2NightShift", sp2NightShift);
		dataMap.put("sp3NightShift", sp3NightShift);
		dataMap.put("sp5NightShift", sp5NightShift);

		// arizalar
		dataMap.put("sp2DayAriza", sp2DayAriza);
		dataMap.put("sp3DayAriza", sp3DayAriza);
		dataMap.put("sp5DayAriza", sp5DayAriza);

		dataMap.put("sp2NightAriza", sp2NightAriza);
		dataMap.put("sp3NightAriza", sp3NightAriza);
		dataMap.put("sp5NightAriza", sp5NightAriza);

		try {
			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/imalatRaporu.xls", date.get(0));

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	@SuppressWarnings("static-access")
	public void kalitePlaniOlustur(SalesItem salesItem) {
		// public void kalitePlaniOlustur() {

		List<SalesItem> salesItems = new ArrayList<SalesItem>();
		SalesItemManager salesItemManager = new SalesItemManager();

		List<NondestructiveTestsSpec> nonDests = new ArrayList<NondestructiveTestsSpec>();
		NondestructiveTestsSpecManager nonDesMan = new NondestructiveTestsSpecManager();

		List<DestructiveTestsSpec> dests = new ArrayList<DestructiveTestsSpec>();
		DestructiveTestsSpecManager desMan = new DestructiveTestsSpecManager();

		List<IsolationTestDefinition> intIso = new ArrayList<IsolationTestDefinition>();
		List<IsolationTestDefinition> extIso = new ArrayList<IsolationTestDefinition>();
		IsolationTestDefinitionManager isoMan = new IsolationTestDefinitionManager();

		salesItems = salesItemManager.findByField(SalesItem.class, "itemId",
				salesItem.getItemId());
		nonDests = nonDesMan.findByField(NondestructiveTestsSpec.class,
				"salesItem.itemId", salesItem.getItemId());
		dests = desMan.findByField(DestructiveTestsSpec.class,
				"salesItem.itemId", salesItem.getItemId());
		intIso = isoMan.findIsolationByItemIdAndIsolationType(1,
				salesItems.get(0));
		extIso = isoMan.findIsolationByItemIdAndIsolationType(0,
				salesItems.get(0));

		String realFilePath = getRealFilePath("/reportformats/test/kalitePlaniReport.docx");
		String realOutFilePath = getRealFilePath("/reportformats/test/kalitePlaniReport.docx"
				.replace(".", "OUT" + "."));

		try {

			// 1) Load Docx file by filling Velocity template engine and cache
			// it to the registry
			// InputStream in = new FileInputStream(new File(
			// "D:\\kalitePlaniReport.docx"));
			InputStream in = new FileInputStream(new File(realFilePath));
			IXDocReport report = XDocReportRegistry.getRegistry().loadReport(
					in, TemplateEngineKind.Velocity);

			// 2) Create context Java model
			List<String> date = new ArrayList<String>();

			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			IContext context = report.createContext();
			context.put("date", date.get(0));
			context.put("item", salesItems.get(0));
			context.put("nonDestructive", nonDests);
			context.put("destructive", dests);
			context.put("internal", intIso);
			context.put("external", extIso);

			// 3) Generate report by merging Java model with the Docx
			OutputStream out = new FileOutputStream(new File(realOutFilePath));
			report.process(context, out);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (XDocReportException e) {
			e.printStackTrace();
		}

		try {
			FileOperations.getInstance().streamFile(realOutFilePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String getRealFilePath(String filePath) {

		String realFilePath = FacesContext.getCurrentInstance()
				.getExternalContext().getRealPath(filePath);

		return realFilePath;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void boruTakipFormu(SalesItem salesItem) {

		try {

			List<String> date = new ArrayList<String>();
			List<Pipe> pipes = new ArrayList<Pipe>();

			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			PipeManager pipeManager = new PipeManager();
			pipes = pipeManager.findByItemIdASC(salesItem.getItemId());

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItem);
			dataMap.put("date", date);
			dataMap.put("pipes", pipes);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/boruTakipFormu.xls", salesItem
							.getProposal().getCustomer().getShortName()
							+ "-" + salesItem.getItemNo());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	// @SuppressWarnings("static-access")
	// public void aspose() {
	//
	// List<SalesItem> salesItems = new ArrayList<SalesItem>();
	// SalesItemManager salesItemManager = new SalesItemManager();
	//
	// salesItems = salesItemManager.findByField(SalesItem.class, "itemId",
	// 300338);
	//
	// List<String> date = new ArrayList<String>();
	//
	// date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
	//
	// try {
	// // String realFilePath =
	// // getRealFilePath("/reportformats/test/kalitePlaniReport.docx");
	// String dataDir = "D:\\";
	// // Open the document.
	// Document doc = new Document(dataDir + "kalitePlaniReport.docx");
	//
	// doc.getRange().replace(
	// "_Customer_",
	// salesItems.get(0).getProposal().getCustomer()
	// .getCommercialName(), false, false);
	// doc.getRange().replace(
	// "_OrderNo_",
	// salesItems.get(0).getProposal().getSalesOrder()
	// .getOrderNo(), false, false);
	// doc.getRange().replace("_KalemNo_", salesItems.get(0).getItemNo(),
	// false, false);
	// doc.getRange().replace("_Cap_",
	// toString().valueOf(salesItems.get(0).getDiameter()), false,
	// false);
	// doc.getRange().replace("_EtKalinligi_",
	// toString().valueOf(salesItems.get(0).getThickness()),
	// false, false);
	// doc.getRange().replace("_Quality_", salesItems.get(0).getQuality(),
	// false, false);
	// doc.getRange().replace("_DisKaplama_",
	// salesItems.get(0).getExternalCoveringType(), false, false);
	// doc.getRange().replace("_IcKaplama_",
	// salesItems.get(0).getInternalCoveringType(), false, false);
	// doc.getRange().replace("_Miktar_",
	// toString().valueOf(salesItems.get(0).getPipeLength()),
	// false, false);
	// doc.getRange().replace("_Tarih_", date.get(0), false, false);
	//
	// doc.save(dataDir + "kalitePlaniReportOut.docx");
	// } catch (Exception e) {
	// // TODO: handle exception
	// }
	// }

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void salesReport(List<SalesItem> salesItems) {

		try {
			List<String> date = new ArrayList<String>();

			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			Map dataMap = new HashMap();
			dataMap.put("date", date);
			dataMap.put("siparisler", salesItems);
			dataMap.put("siparis", salesItems.get(0));
			dataMap.put("kullanici", userBean.getUserFullName());

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/teklifFormu.xls", salesItems.get(0)
							.getProposal().getCustomer().getShortName());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void ruloReportXlsx() {

		try {

			if (ruloYear == null) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"LÜTFEN TARİH SEÇİNİZ!, HATA!", null));
				return;
			}

			RuloManager ruloManager = new RuloManager();
			List<Rulo> rulos = new ArrayList<Rulo>();

			// rulos = ruloManager.findAllOrderBy(Rulo.class,
			// "ruloYil, c.ruloKimlikId");
			rulos = (List<Rulo>) ruloManager.findByFieldOrderBy(Rulo.class,
					"ruloYil", ruloYear, "ruloYil, c.ruloKimlikId");

			List<String> date = new ArrayList<String>();

			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			Map dataMap = new HashMap();
			dataMap.put("rulos", rulos);
			dataMap.put("date", date);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/ruloReport.xls", date.toString());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	private class OtomatikObject {
		String konum = null;
		String aciklama = null;

		/**
		 * @return the konum
		 */
		public String getKonum() {
			return konum;
		}

		/**
		 * @param konum
		 *            the konum to set
		 */
		public void setKonum(String konum) {
			this.konum = konum;
		}

		/**
		 * @return the aciklama
		 */
		public String getAciklama() {
			return aciklama;
		}

		/**
		 * @param aciklama
		 *            the aciklama to set
		 */
		public void setAciklama(String aciklama) {
			this.aciklama = aciklama;
		}
	}

	private class ManuelObject {
		String sonucBir = null;
		String tarihBir = null;
		String sonucIki = null;
		String tarihIki = null;
		String sonucUc = null;
		String tarihUc = null;

		/**
		 * @return the sonucBir
		 */
		public String getSonucBir() {
			return sonucBir;
		}

		/**
		 * @param sonucBir
		 *            the sonucBir to set
		 */
		public void setSonucBir(String sonucBir) {
			this.sonucBir = sonucBir;
		}

		/**
		 * @return the tarihBir
		 */
		public String getTarihBir() {
			return tarihBir;
		}

		/**
		 * @param tarihBir
		 *            the tarihBir to set
		 */
		public void setTarihBir(String tarihBir) {
			this.tarihBir = tarihBir;
		}

		/**
		 * @return the sonucIki
		 */
		public String getSonucIki() {
			return sonucIki;
		}

		/**
		 * @param sonucIki
		 *            the sonucIki to set
		 */
		public void setSonucIki(String sonucIki) {
			this.sonucIki = sonucIki;
		}

		/**
		 * @return the tarihIki
		 */
		public String getTarihIki() {
			return tarihIki;
		}

		/**
		 * @param tarihIki
		 *            the tarihIki to set
		 */
		public void setTarihIki(String tarihIki) {
			this.tarihIki = tarihIki;
		}

		/**
		 * @return the sonucUc
		 */
		public String getSonucUc() {
			return sonucUc;
		}

		/**
		 * @param sonucUc
		 *            the sonucUc to set
		 */
		public void setSonucUc(String sonucUc) {
			this.sonucUc = sonucUc;
		}

		/**
		 * @return the tarihUc
		 */
		public String getTarihUc() {
			return tarihUc;
		}

		/**
		 * @param tarihUc
		 *            the tarihUc to set
		 */
		public void setTarihUc(String tarihUc) {
			this.tarihUc = tarihUc;
		}
	}

	private class TamiratObject {
		String konum = null;
		String tarih = null;

		/**
		 * @return the konum
		 */
		public String getKonum() {
			return konum;
		}

		/**
		 * @param konum
		 *            the konum to set
		 */
		public void setKonum(String konum) {
			this.konum = konum;
		}

		/**
		 * @return the tarih
		 */
		public String getTarih() {
			return tarih;
		}

		/**
		 * @param tarih
		 *            the tarih to set
		 */
		public void setTarih(String tarih) {
			this.tarih = tarih;
		}
	}

	private class FloroskopiObject {
		String konum = null;
		String hataTipi = null;
		String tarih = null;
		String sonuc = null;
		String tamirSonrasi = null;
		String tamirSonrasiTarih = null;

		/**
		 * @return the konum
		 */
		public String getKonum() {
			return konum;
		}

		/**
		 * @param konum
		 *            the konum to set
		 */
		public void setKonum(String konum) {
			this.konum = konum;
		}

		/**
		 * @return the hataTipi
		 */
		public String getHataTipi() {
			return hataTipi;
		}

		/**
		 * @param hataTipi
		 *            the hataTipi to set
		 */
		public void setHataTipi(String hataTipi) {
			this.hataTipi = hataTipi;
		}

		/**
		 * @return the tarih
		 */
		public String getTarih() {
			return tarih;
		}

		/**
		 * @param tarih
		 *            the tarih to set
		 */
		public void setTarih(String tarih) {
			this.tarih = tarih;
		}

		/**
		 * @return the sonuc
		 */
		public String getSonuc() {
			return sonuc;
		}

		/**
		 * @param sonuc
		 *            the sonuc to set
		 */
		public void setSonuc(String sonuc) {
			this.sonuc = sonuc;
		}

		/**
		 * @return the tamirSonrasi
		 */
		public String getTamirSonrasi() {
			return tamirSonrasi;
		}

		/**
		 * @param tamirSonrasi
		 *            the tamirSonrasi to set
		 */
		public void setTamirSonrasi(String tamirSonrasi) {
			this.tamirSonrasi = tamirSonrasi;
		}

		/**
		 * @return the tamirSonrasiTarih
		 */
		public String getTamirSonrasiTarih() {
			return tamirSonrasiTarih;
		}

		/**
		 * @param tamirSonrasiTarih
		 *            the tamirSonrasiTarih to set
		 */
		public void setTamirSonrasiTarih(String tamirSonrasiTarih) {
			this.tamirSonrasiTarih = tamirSonrasiTarih;
		}
	}

	private class HidrostatikObject {
		String basinc = null;
		String tarih = null;

		/**
		 * @return the basinc
		 */
		public String getBasinc() {
			return basinc;
		}

		/**
		 * @param basinc
		 *            the basinc to set
		 */
		public void setBasinc(String basinc) {
			this.basinc = basinc;
		}

		/**
		 * @return the tarih
		 */
		public String getTarih() {
			return tarih;
		}

		/**
		 * @param tarih
		 *            the tarih to set
		 */
		public void setTarih(String tarih) {
			this.tarih = tarih;
		}
	}

	private class GozOlcuObject {
		String aUcuDisCap = null;
		String bUcuDisCap = null;
		String sonucDisCap = null;
		String floroskopi = null;
		String ultarson = null;
		String malzemeHatasi = null;
		String imalatHatasi = null;
		String dogrusallik = null;
		String tamiratBoyu = null;
		String tamiratSayisi = null;
		String boruBoyu = null;
		String sonuc = null;

		/**
		 * @return the aUcuDisCap
		 */
		public String getaUcuDisCap() {
			return aUcuDisCap;
		}

		/**
		 * @param aUcuDisCap
		 *            the aUcuDisCap to set
		 */
		public void setaUcuDisCap(String aUcuDisCap) {
			this.aUcuDisCap = aUcuDisCap;
		}

		/**
		 * @return the bUcuDisCap
		 */
		public String getbUcuDisCap() {
			return bUcuDisCap;
		}

		/**
		 * @param bUcuDisCap
		 *            the bUcuDisCap to set
		 */
		public void setbUcuDisCap(String bUcuDisCap) {
			this.bUcuDisCap = bUcuDisCap;
		}

		/**
		 * @return the sonucDisCap
		 */
		public String getSonucDisCap() {
			return sonucDisCap;
		}

		/**
		 * @param sonucDisCap
		 *            the sonucDisCap to set
		 */
		public void setSonucDisCap(String sonucDisCap) {
			this.sonucDisCap = sonucDisCap;
		}

		/**
		 * @return the floroskopi
		 */
		public String getFloroskopi() {
			return floroskopi;
		}

		/**
		 * @param floroskopi
		 *            the floroskopi to set
		 */
		public void setFloroskopi(String floroskopi) {
			this.floroskopi = floroskopi;
		}

		/**
		 * @return the ultarson
		 */
		public String getUltarson() {
			return ultarson;
		}

		/**
		 * @param ultarson
		 *            the ultarson to set
		 */
		public void setUltarson(String ultarson) {
			this.ultarson = ultarson;
		}

		/**
		 * @return the malzemeHatasi
		 */
		public String getMalzemeHatasi() {
			return malzemeHatasi;
		}

		/**
		 * @param malzemeHatasi
		 *            the malzemeHatasi to set
		 */
		public void setMalzemeHatasi(String malzemeHatasi) {
			this.malzemeHatasi = malzemeHatasi;
		}

		/**
		 * @return the imalatHatasi
		 */
		public String getImalatHatasi() {
			return imalatHatasi;
		}

		/**
		 * @param imalatHatasi
		 *            the imalatHatasi to set
		 */
		public void setImalatHatasi(String imalatHatasi) {
			this.imalatHatasi = imalatHatasi;
		}

		/**
		 * @return the dogrusallik
		 */
		public String getDogrusallik() {
			return dogrusallik;
		}

		/**
		 * @param dogrusallik
		 *            the dogrusallik to set
		 */
		public void setDogrusallik(String dogrusallik) {
			this.dogrusallik = dogrusallik;
		}

		/**
		 * @return the tamiratBoyu
		 */
		public String getTamiratBoyu() {
			return tamiratBoyu;
		}

		/**
		 * @param tamiratBoyu
		 *            the tamiratBoyu to set
		 */
		public void setTamiratBoyu(String tamiratBoyu) {
			this.tamiratBoyu = tamiratBoyu;
		}

		/**
		 * @return the tamiratSayisi
		 */
		public String getTamiratSayisi() {
			return tamiratSayisi;
		}

		/**
		 * @param tamiratSayisi
		 *            the tamiratSayisi to set
		 */
		public void setTamiratSayisi(String tamiratSayisi) {
			this.tamiratSayisi = tamiratSayisi;
		}

		/**
		 * @return the boruBoyu
		 */
		public String getBoruBoyu() {
			return boruBoyu;
		}

		/**
		 * @param boruBoyu
		 *            the boruBoyu to set
		 */
		public void setBoruBoyu(String boruBoyu) {
			this.boruBoyu = boruBoyu;
		}

		/**
		 * @return the sonuc
		 */
		public String getSonuc() {
			return sonuc;
		}

		/**
		 * @param sonuc
		 *            the sonuc to set
		 */
		public void setSonuc(String sonuc) {
			this.sonuc = sonuc;
		}
	}

	private class RadyografikObject {
		String aUcu = null;
		String bUcu = null;
		String tamiratFilmi = null;
		String sonuc = null;

		/**
		 * @return the aUcu
		 */
		public String getaUcu() {
			return aUcu;
		}

		/**
		 * @param aUcu
		 *            the aUcu to set
		 */
		public void setaUcu(String aUcu) {
			this.aUcu = aUcu;
		}

		/**
		 * @return the bUcu
		 */
		public String getbUcu() {
			return bUcu;
		}

		/**
		 * @param bUcu
		 *            the bUcu to set
		 */
		public void setbUcu(String bUcu) {
			this.bUcu = bUcu;
		}

		/**
		 * @return the tamiratFilmi
		 */
		public String getTamiratFilmi() {
			return tamiratFilmi;
		}

		/**
		 * @param tamiratFilmi
		 *            the tamiratFilmi to set
		 */
		public void setTamiratFilmi(String tamiratFilmi) {
			this.tamiratFilmi = tamiratFilmi;
		}

		/**
		 * @return the sonuc
		 */
		public String getSonuc() {
			return sonuc;
		}

		/**
		 * @param sonuc
		 *            the sonuc to set
		 */
		public void setSonuc(String sonuc) {
			this.sonuc = sonuc;
		}
	}

	private class KaplamaMuayeneObject {
		String kaplamaKalinligi = null;
		String kaplamaTestleri = null;
		String tamirat = null;
		String holidayTesti = null;
		String soyulmaTam = null;
		String soyulmaYarimA = null;
		String soyulmaYarimB = null;
		String sonucBir = null;
		String marka = null;
		String boruAgzi = null;
		String sonucIki = null;

		/**
		 * @return the kaplamaKalinligi
		 */
		public String getKaplamaKalinligi() {
			return kaplamaKalinligi;
		}

		/**
		 * @param kaplamaKalinligi
		 *            the kaplamaKalinligi to set
		 */
		public void setKaplamaKalinligi(String kaplamaKalinligi) {
			this.kaplamaKalinligi = kaplamaKalinligi;
		}

		/**
		 * @return the kaplamaTestleri
		 */
		public String getKaplamaTestleri() {
			return kaplamaTestleri;
		}

		/**
		 * @param kaplamaTestleri
		 *            the kaplamaTestleri to set
		 */
		public void setKaplamaTestleri(String kaplamaTestleri) {
			this.kaplamaTestleri = kaplamaTestleri;
		}

		/**
		 * @return the tamirat
		 */
		public String getTamirat() {
			return tamirat;
		}

		/**
		 * @param tamirat
		 *            the tamirat to set
		 */
		public void setTamirat(String tamirat) {
			this.tamirat = tamirat;
		}

		/**
		 * @return the holidayTesti
		 */
		public String getHolidayTesti() {
			return holidayTesti;
		}

		/**
		 * @param holidayTesti
		 *            the holidayTesti to set
		 */
		public void setHolidayTesti(String holidayTesti) {
			this.holidayTesti = holidayTesti;
		}

		/**
		 * @return the soyulmaTam
		 */
		public String getSoyulmaTam() {
			return soyulmaTam;
		}

		/**
		 * @param soyulmaTam
		 *            the soyulmaTam to set
		 */
		public void setSoyulmaTam(String soyulmaTam) {
			this.soyulmaTam = soyulmaTam;
		}

		/**
		 * @return the soyulmaYarimA
		 */
		public String getSoyulmaYarimA() {
			return soyulmaYarimA;
		}

		/**
		 * @param soyulmaYarimA
		 *            the soyulmaYarimA to set
		 */
		public void setSoyulmaYarimA(String soyulmaYarimA) {
			this.soyulmaYarimA = soyulmaYarimA;
		}

		/**
		 * @return the soyulmaYarimB
		 */
		public String getSoyulmaYarimB() {
			return soyulmaYarimB;
		}

		/**
		 * @param soyulmaYarimB
		 *            the soyulmaYarimB to set
		 */
		public void setSoyulmaYarimB(String soyulmaYarimB) {
			this.soyulmaYarimB = soyulmaYarimB;
		}

		/**
		 * @return the sonucBir
		 */
		public String getSonucBir() {
			return sonucBir;
		}

		/**
		 * @param sonucBir
		 *            the sonucBir to set
		 */
		public void setSonucBir(String sonucBir) {
			this.sonucBir = sonucBir;
		}

		/**
		 * @return the marka
		 */
		public String getMarka() {
			return marka;
		}

		/**
		 * @param marka
		 *            the marka to set
		 */
		public void setMarka(String marka) {
			this.marka = marka;
		}

		/**
		 * @return the boruAgzi
		 */
		public String getBoruAgzi() {
			return boruAgzi;
		}

		/**
		 * @param boruAgzi
		 *            the boruAgzi to set
		 */
		public void setBoruAgzi(String boruAgzi) {
			this.boruAgzi = boruAgzi;
		}

		/**
		 * @return the sonucIki
		 */
		public String getSonucIki() {
			return sonucIki;
		}

		/**
		 * @param sonucIki
		 *            the sonucIki to set
		 */
		public void setSonucIki(String sonucIki) {
			this.sonucIki = sonucIki;
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr199ReportXlsx(Integer prmPipeId) {

		if (prmPipeId == 0 || prmPipeId == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"BORU SEÇİLMEDİ, LÜTFEN OK İLE BORU SEÇİNİZ SONRA TEKRAR RAPOR OLUŞTURUNUZ, HATA!",
							null));
			return;
		}

		// otomatik-manuel-tamirat-fl-hidro-gö-radyografik-kaplama muayene

		ManuelObject manuelObject = new ManuelObject();
		TamiratObject tamiratObject = new TamiratObject();
		FloroskopiObject floroskopiObject = new FloroskopiObject();
		HidrostatikObject hidrostatikObject = new HidrostatikObject();
		GozOlcuObject gozOlcuObject = new GozOlcuObject();
		RadyografikObject radyografikObject = new RadyografikObject();
		KaplamaMuayeneObject kaplamaMuayeneObject = new KaplamaMuayeneObject();

		List<OtomatikObject> otomatikObjects = new ArrayList<OtomatikObject>();

		try {
			PipeManager pipeManager = new PipeManager();
			Pipe pipe = new Pipe();
			pipe = pipeManager.loadObject(Pipe.class, prmPipeId);

			for (int i = 0; i < pipe.getTestTahribatsizOtomatikUtSonuclar()
					.size(); i++) {
				OtomatikObject otomatikObject = new OtomatikObject();
				otomatikObject.setKonum(pipe
						.getTestTahribatsizOtomatikUtSonuclar().get(i)
						.getKoordinatQ()
						+ "x"
						+ pipe.getTestTahribatsizOtomatikUtSonuclar().get(i)
								.getKoordinatL());
				otomatikObject.setAciklama(pipe
						.getTestTahribatsizOtomatikUtSonuclar().get(i)
						.getAciklama());
				otomatikObjects.add(otomatikObject);
			}

			HidrostaticTestsSpecManager specMan = new HidrostaticTestsSpecManager();
			HidrostaticTestsSpec hidroSpec = new HidrostaticTestsSpec();
			hidroSpec = specMan.findByItemId(pipe.getSalesItem().getItemId())
					.get(0);

			hidrostatikObject.setBasinc(hidroSpec.getPressure().toString());
			hidrostatikObject.setTarih(new SimpleDateFormat("dd.MM.yyyy")
					.format(pipe.getTestTahribatsizHidrostatik()
							.getMuayeneBitisZamani()));

			List<String> date = new ArrayList<String>();
			List<String> productionDate = new ArrayList<String>();

			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			productionDate.add(new SimpleDateFormat("dd.MM.yyyy").format(pipe
					.getProductionDate()));

			Map dataMap = new HashMap();
			dataMap.put("siparis", pipe.getSalesItem());
			dataMap.put("pipe", pipe);
			dataMap.put("date", date);
			dataMap.put("productionDate", productionDate);
			dataMap.put("otomatikObjects", otomatikObjects);
			dataMap.put("manuelObject", manuelObject);
			dataMap.put("tamiratObject", tamiratObject);
			dataMap.put("floroskopiObject", floroskopiObject);
			dataMap.put("hidrostatikObject", hidrostatikObject);
			dataMap.put("gozOlcuObject", gozOlcuObject);
			dataMap.put("radyografikObject", radyografikObject);
			dataMap.put("kaplamaMuayeneObject", kaplamaMuayeneObject);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/imalat/FR-199.xls", date.toString());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	// getters setters

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the ruloYear
	 */
	public String getRuloYear() {
		return ruloYear;
	}

	/**
	 * @param ruloYear
	 *            the ruloYear to set
	 */
	public void setRuloYear(String ruloYear) {
		this.ruloYear = ruloYear;
	}

	// /**
	// * JR Example
	// */
	// // public void manuelUtInspectionResultReportXlsx() throws JRException,
	// // IOException {
	// //
	// // SalesItemManager salesItemManager = new SalesItemManager();
	// // List<SalesItem> salesItems = new ArrayList<SalesItem>();
	// //
	// // salesItems = salesItemManager.findByField(SalesItem.class, "itemId",
	// // 100);
	// //
	// // ReportUtil reportUtil = new ReportUtil();
	// // reportUtil.jrExportAsXlsx(salesItems,
	// // "/reportformats/test/manuelUtInspectionResultReport.jasper");
	// //
	// // }

	// public void imageTest() throws IOException {
	//
	// ExchangeRateUtil exchangeRateUtil = new ExchangeRateUtil();
	//
	// ReportUtil reportUtil = new ReportUtil();
	//
	// Barcode128 code128 = new Barcode128();
	// code128.setCode("F220H502054");
	// code128.setChecksumText(true);
	// code128.setGenerateChecksum(true);
	// code128.setStartStopText(true);
	//
	// java.awt.Image image = code128.createAwtImage(Color.BLACK, Color.WHITE);
	//
	// HSSFWorkbook hssfWorkbook = reportUtil
	// .getWorkbookFromFilePath("/reportformats/test/test.xls");
	//
	// reportUtil.insertImageToWorkbook(hssfWorkbook, image, "testImage",
	// image.getWidth(null), image.getHeight(null));
	// reportUtil.insertImageToWorkbook(hssfWorkbook, image, "testImage2",
	// image.getWidth(null), image.getHeight(null));
	// reportUtil.writeWorkbookToFile(hssfWorkbook,
	// "/reportformats/test/testOut.xls");
	//
	// }

}
