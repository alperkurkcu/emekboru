package com.emekboru.beans.sales.order;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.emekboru.beans.sales.SalesMenuBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Employee;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpa.sales.order.SalesJoinProposalEmployee;
import com.emekboru.jpa.sales.order.SalesProposal;
import com.emekboru.jpa.sales.order.SalesProposalStatus;
import com.emekboru.jpaman.sales.order.SalesItemManager;
import com.emekboru.jpaman.sales.order.SalesJoinProposalEmployeeManager;
import com.emekboru.jpaman.sales.order.SalesProposalManager;
import com.emekboru.jpaman.sales.order.SalesProposalStatusManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "salesProposalBean")
@ViewScoped
public class SalesProposalBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3908317061314131223L;

	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{salesOrderBean}")
	private SalesOrderBean orderBean;

	@ManagedProperty(value = "#{salesMenuBean}")
	private SalesMenuBean salesMenuBean;

	private SalesProposal newProposal;
	private SalesProposal selectedProposal;
	private SalesProposal reminderProposal;

	private List<SalesProposal> proposalList;
	private List<SalesProposal> filteredList;
	private List<SalesProposal> reminderList;

	private int queueNumber;
	private boolean isThisFirstTime;

	private StreamedContent downloadedFile;
	private String downloadedFileName;

	private File[] fileArray;

	private File theFile = null;
	OutputStream output = null;
	InputStream input = null;

	private String path;
	private String privatePath;
	private String toBeDeleted;

	int length;

	public SalesProposalBean() {
		queueNumber = 1;
		isThisFirstTime = true;

		newProposal = new SalesProposal();

		selectedProposal = new SalesProposal();

		privatePath = File.separatorChar + "sales" + File.separatorChar
				+ "uploadedDocuments" + File.separatorChar + "proposal"
				+ File.separatorChar;
	}

	@PostConstruct
	public void load() {
		System.out.println("SalesProposalBean.load()");

		try {
			this.setPath(config.getConfig().getFolder().getAbsolutePath()
					+ privatePath);

			System.out.println("Path for Upload File (Sales):			" + path);
		} catch (Exception ex) {
			System.out.println("Path for Upload File (Sales):			"
					+ FacesContext.getCurrentInstance().getExternalContext()
							.getRealPath(privatePath));

			System.out.print(path);
			System.out.println("CAUSE OF " + ex.toString());
		}

		theFile = new File(path);
		if (!theFile.isDirectory()) {
			theFile.mkdirs();
		}

		fileArray = theFile.listFiles();

		try {
			SalesProposalManager manager = new SalesProposalManager();
			proposalList = manager.findAllOrderBy(SalesProposal.class,
					"proposalId");

			// manager.findAllOrderBy(SalesProposal.class, "proposalId");
			// proposalList = manager.findAllOrderBy(SalesProposal.class,
			// "salesOrder.orderId");
			if (isThisFirstTime) {
				newProposal = new SalesProposal();
				this.completeProposalNumber();
				isThisFirstTime = false;
			}

			selectedProposal = proposalList.get(0);

			SalesProposalManager salesProposalManager = new SalesProposalManager();
			selectedProposal = salesProposalManager.findByField(
					SalesProposal.class, "proposalId",
					proposalList.get(0).getProposalId()).get(0);

			SalesItemManager salesItemManager = new SalesItemManager();
			selectedProposal.setItemList(salesItemManager.findByField(
					SalesItem.class, "proposal", proposalList.get(0)));

			/* JOIN FUNCTIONS */
			// for (SalesProposal proposal : proposalList) {
			// this.loadJoinProposalEmployee(proposal);
			// }
			/* end of JOIN FUNCTIONS */
		} catch (Exception e) {
			System.out.println("SalesProposalBean: " + e.toString());
		}
	}

	public void add() {
		System.out.println("SalesProposalBean.add()");

		boolean isAdded = false;
		try {
			SalesProposalManager manager = new SalesProposalManager();
			proposalList.add(0, newProposal);
			manager.enterNew(newProposal);
			/* JOIN FUNCTIONS */
			this.addJoinProposalEmployee(newProposal);
			/* end of JOIN FUNCTIONS */

			isAdded = true;
		} catch (Exception e) {
			System.out.println("SalesProposalBean: " + e.toString());
		}

		try {
			if (newProposal.getStatus().getName().toLowerCase()
					.contains("sipariş")
					|| newProposal.getStatus().getName().toLowerCase()
							.contains("order")) {
				if (!orderBean.isProposalTransformedToOrder(newProposal
						.getProposalId())) {
					orderBean.add(newProposal);
				}
			}
		} catch (Exception e) {
			System.out.println("SalesProposalBean: " + e.toString());
		}

		try {
			if (isAdded) {
				newProposal = new SalesProposal();
				this.completeProposalNumber();
				queueNumber++;

				FacesContextUtils.addInfoMessage("SubmitMessage");
			}
		} catch (Exception e) {
			System.out.println("SalesProposalBean: " + e.toString());
		}
	}

	public void update() {
		System.out.println("SalesProposalBean.update()");

		try {
			SalesProposalManager manager = new SalesProposalManager();
			/* JOIN FUNCTIONS */
			this.deleteJoinProposalEmployee(selectedProposal);
			this.addJoinProposalEmployee(selectedProposal);
			/* end of JOIN FUNCTIONS */
			manager.updateEntity(selectedProposal);
			FacesContextUtils.addInfoMessage("UpdateItemMessage");
		} catch (Exception e) {
			System.out.println("SalesProposalBean: " + e.toString());
		}

		try {
			if (selectedProposal.getStatus().getName().toLowerCase()
					.contains("sipariş")
					|| selectedProposal.getStatus().getName().toLowerCase()
							.contains("order")) {
				if (!orderBean.isProposalTransformedToOrder(selectedProposal
						.getProposalId())) {
					orderBean.add(selectedProposal);
				}
			}
		} catch (Exception e) {
			System.out.println("SalesProposalBean: " + e.toString());
		}

		try {
			if (selectedProposal.getStatus().getStatusId() == 10) {
				delete();
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void delete() {
		System.out.println("SalesProposalBean.delete()");

		if (selectedProposal.getStatus().getStatusId() == 3) {
			FacesContextUtils
					.addPlainErrorMessage("Üretimi başlamış siparişi silemezsiniz!");
			return;
		}

		try {
			// if (selectedProposal.getFileNumber() > 0) {
			// isDeleted = false;
			//
			// // Delete the uploaded file
			// theFile = new File(path);
			// fileArray = theFile.listFiles();
			//
			// for (int i = 0; i < selectedProposal.getFileNames().size(); i++)
			// {
			// System.out.println("document that will be deleted: "
			// + privatePath
			// + selectedProposal.getFileNames().get(i));
			// for (int j = 0; j < fileArray.length; j++) {
			// System.out.println("fileArray[j].toString(): "
			// + fileArray[j].toString());
			// if (fileArray[j].toString().contains(
			// privatePath
			// + selectedProposal.getFileNames()
			// .get(i))) {
			// isDeleted = fileArray[j].delete();
			// }
			// }
			// }
			// }
			/* JOIN FUNCTIONS */
			this.deleteJoinProposalEmployee(selectedProposal);
			/* end of JOIN FUNCTIONS */
			SalesProposalManager manager = new SalesProposalManager();
			manager.delete(selectedProposal);

			proposalList.remove(selectedProposal);
			selectedProposal = proposalList.get(0);
			FacesContextUtils.addWarnMessage("DeletedMessage");

		} catch (Exception e) {
			FacesContextUtils.addErrorMessage("salesCannotDelete");
			System.out.println("SalesProposalBean: " + e.toString());
		}
	}

	/* JOIN FUNCTIONS */
	private void addJoinProposalEmployee(SalesProposal proposal) {
		try {
			SalesJoinProposalEmployeeManager joinManager = new SalesJoinProposalEmployeeManager();
			SalesJoinProposalEmployee newJoin;

			for (int i = 0; i < proposal.getEmployeeList().size(); i++) {
				newJoin = new SalesJoinProposalEmployee();
				newJoin.setEmployee(proposal.getEmployeeList().get(i));
				newJoin.setProposal(proposal);
				joinManager.enterNew(newJoin);
			}
		} catch (Exception e) {
			System.out.println("SalesProposalBean: " + e.toString());
		}
	}

	@SuppressWarnings("unused")
	private void loadJoinProposalEmployee(SalesProposal proposal) {
		try {
			SalesJoinProposalEmployeeManager joinManager = new SalesJoinProposalEmployeeManager();
			List<SalesJoinProposalEmployee> joinList = joinManager
					.findAll(SalesJoinProposalEmployee.class);

			proposal.setEmployeeList(new ArrayList<Employee>());

			for (int i = 0; i < joinList.size(); i++) {
				if (joinList.get(i).getProposal().equals(proposal)) {
					proposal.getEmployeeList().add(
							joinList.get(i).getEmployee());
				}
			}
		} catch (Exception e) {
			System.out.println("SalesProposalBean.loadJoinProposalEmployee : "
					+ e.toString());
		}
	}

	private void deleteJoinProposalEmployee(SalesProposal proposal) {
		try {
			SalesJoinProposalEmployeeManager joinManager = new SalesJoinProposalEmployeeManager();
			List<SalesJoinProposalEmployee> joinList = joinManager
					.findAll(SalesJoinProposalEmployee.class);

			for (int i = 0; i < joinList.size(); i++) {
				if (joinList.get(i).getProposal().equals(proposal)) {
					joinManager.delete(joinList.get(i));
				}
			}
		} catch (Exception e) {
			System.out.println("SalesProposalBean: " + e.toString());
		}
	}

	/* end of JOIN FUNCTIONS */
	public void completeProposalNumber() {
		System.out.println("SalesProposalBean.completeProposalNumber()");

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		this.findNextNumber(sdf.format(cal.getTime()).substring(6));

		newProposal.setProposalNumber(sdf.format(cal.getTime()).substring(6)
				+ "/" + String.valueOf(queueNumber));
	}

	public void findNextNumber(String thisYear) {
		boolean isThisYearExist = false;

		try {
			for (int i = 0; i < proposalList.size(); i++) {
				if (proposalList.get(i).getProposalNumber().substring(0, 4)
						.contentEquals(thisYear)) {

					isThisYearExist = true;

					if (queueNumber <= Integer.parseInt(proposalList.get(i)
							.getProposalNumber().substring(5))) {

						queueNumber = Integer.parseInt(proposalList.get(i)
								.getProposalNumber().substring(5)) + 1;
					}
				}
			}
			if (!isThisYearExist) {
				queueNumber = 1;
			}
		} catch (Exception e) {
			System.out.println("SalesProposalBean: " + e.toString());
		}
	}

	public void selectNothing() {
		reminderProposal = null;
	}

	public String checkFileName(String fileName) {

		fileName = fileName.replace(" ", "_");
		fileName = fileName.replace("ç", "c");
		fileName = fileName.replace("Ç", "C");
		fileName = fileName.replace("ğ", "g");
		fileName = fileName.replace("Ğ", "G");
		fileName = fileName.replace("İ", "i");
		fileName = fileName.replace("ı", "i");
		fileName = fileName.replace("ö", "o");
		fileName = fileName.replace("Ö", "O");
		fileName = fileName.replace("ş", "s");
		fileName = fileName.replace("Ş", "S");
		fileName = fileName.replace("ü", "u");
		fileName = fileName.replace("Ü", "U");

		return fileName;
	}

	public void upload(FileUploadEvent event) throws AbortProcessingException,
			IOException {
		System.out.println("SalesProposalBean.upload()");

		try {
			String name = this.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				newProposal.setDocuments("//" + name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				SalesProposalManager manager = new SalesProposalManager();
				manager.updateEntity(selectedProposal);

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				FacesContextUtils.addWarnMessage("salesFileUploadedMessage");
			} else {
				System.out.println("File already exists.");

				newProposal.setDocuments("//");
				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("SalesProposalBean: " + e.toString());
		}
	}

	public void addUploadedFile(FileUploadEvent event)
			throws AbortProcessingException, IOException {
		System.out.println("SalesProposalBean.addUploadedFile()");

		try {
			String name = this.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				selectedProposal.setDocuments(selectedProposal.getDocuments()
						+ "//" + name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				SalesProposalManager manager = new SalesProposalManager();
				manager.updateEntity(selectedProposal);

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				FacesContextUtils.addWarnMessage("salesFileUploadedMessage");
			} else {
				System.out.println("File already exists.");

				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("SalesProposalBean: " + e.toString());
		}
	}

	public void deleteTheSelectedFile() {
		System.out.println("SalesCRMVisitBean.deleteTheSelectedFile()");
		try {
			// Delete the uploaded file
			theFile = new File(path);
			fileArray = theFile.listFiles();

			System.out.println("document that will be deleted: " + privatePath
					+ toBeDeleted);
			for (int j = 0; j < fileArray.length; j++) {
				System.out.println("fileArray[j].toString(): "
						+ fileArray[j].toString());
				if (fileArray[j].toString().contains(privatePath + toBeDeleted)) {
					fileArray[j].delete();

					if (selectedProposal.getDocuments().contentEquals(
							"//" + toBeDeleted)) {
						selectedProposal.setDocuments(selectedProposal
								.getDocuments().replace("//" + toBeDeleted,
										"null"));
					} else {
						selectedProposal
								.setDocuments(selectedProposal.getDocuments()
										.replace("//" + toBeDeleted, ""));
					}
					FacesContextUtils
							.addWarnMessage("salesTheFileIsDeletedMessage");
				}
			}
		} catch (Exception e) {
			System.out.println("SalesProposalBean: " + e.toString());
		}
	}

	public void deleteAndUpload(FileUploadEvent event)
			throws AbortProcessingException, IOException {

		System.out.println("SalesProposalBean.deleteAndUpload()");

		try {
			// Delete the uploaded file
			theFile = new File(path);
			fileArray = theFile.listFiles();

			for (int i = 0; i < selectedProposal.getFileNames().size(); i++) {
				for (int j = 0; j < fileArray.length; j++) {
					System.out.println("fileArray[j].toString(): "
							+ fileArray[j].toString());
					if (fileArray[j].toString().contains(
							privatePath
									+ selectedProposal.getFileNames().get(i))) {
						fileArray[j].delete();
					}
				}
			}

			String name = this.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				selectedProposal.setDocuments("//" + name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				SalesProposalManager manager = new SalesProposalManager();
				manager.updateEntity(selectedProposal);

				FacesContextUtils.addInfoMessage("UpdateItemMessage");
			} else {
				System.out.println("File already exists.");

				selectedProposal.setDocuments(null);
				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("SalesProposalBean: " + e.toString());
		}
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public SalesProposal getNewProposal() {
		return newProposal;
	}

	public void setNewProposal(SalesProposal newProposal) {
		this.newProposal = newProposal;
	}

	public SalesProposal getSelectedProposal() {
		try {
			if (selectedProposal == null)
				selectedProposal = new SalesProposal();
		} catch (Exception ex) {
			selectedProposal = new SalesProposal();
		}
		return selectedProposal;
	}

	public void setSelectedProposal(SalesProposal selectedProposal) {
		this.selectedProposal = selectedProposal;
	}

	public SalesProposal getReminderProposal() {
		return reminderProposal;
	}

	public void setReminderProposal(SalesProposal reminderProposal) {
		this.reminderProposal = reminderProposal;
	}

	public List<SalesProposal> getProposalList() {
		return proposalList;
	}

	public void setProposalList(List<SalesProposal> proposalList) {
		this.proposalList = proposalList;
	}

	public List<SalesProposal> getFilteredList() {
		try {
			selectedProposal = filteredList.get(0);
			salesMenuBean.showNewType1Menu();
		} catch (Exception ex) {
			System.out.println("getFilteredList: " + ex);
		}
		return filteredList;
	}

	public void setFilteredList(List<SalesProposal> filteredList) {
		this.filteredList = filteredList;
	}

	public SalesMenuBean getSalesMenuBean() {
		return salesMenuBean;
	}

	public void setSalesMenuBean(SalesMenuBean salesMenuBean) {
		this.salesMenuBean = salesMenuBean;
	}

	@SuppressWarnings("deprecation")
	public List<SalesProposal> getReminderList() {
		try {
			Calendar currentDate = Calendar.getInstance();
			currentDate.add(Calendar.DAY_OF_MONTH, -15);
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
			String dateNow = formatter.format(currentDate.getTime());
			Date startDate = new Date(dateNow);

			reminderList = new ArrayList<SalesProposal>();

			for (int i = 0; i < proposalList.size(); i++) {
				if (proposalList.get(i).getPostDate().after(startDate)) {
					reminderList.add(proposalList.get(i));
				}
			}

		} catch (Exception e) {
			System.out.println("SalesProposalBean: " + e.toString());
		}

		return reminderList;
	}

	public void setReminderList(List<SalesProposal> reminderList) {
		this.reminderList = reminderList;
	}

	public SalesOrderBean getOrderBean() {
		return orderBean;
	}

	public void setOrderBean(SalesOrderBean orderBean) {
		this.orderBean = orderBean;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getDownloadedFileName() {
		return downloadedFileName;
	}

	public void setDownloadedFileName(String downloadedFileName) {
		this.downloadedFileName = downloadedFileName;
	}

	@SuppressWarnings("resource")
	public StreamedContent getDownloadedFile() {
		System.out.println("SalesProposalBean.getDownloadedFile()");

		try {
			if (downloadedFileName != null) {
				RandomAccessFile accessFile = new RandomAccessFile(path
						+ downloadedFileName, "r");
				byte[] downloadByte = new byte[(int) accessFile.length()];
				accessFile.read(downloadByte);
				InputStream stream = new ByteArrayInputStream(downloadByte);

				String suffix = FilenameUtils.getExtension(downloadedFileName);
				String contentType = "text/plain";
				if (suffix.contentEquals("jpg") || suffix.contentEquals("png")
						|| suffix.contentEquals("gif")) {
					contentType = "image/" + contentType;
				} else if (suffix.contentEquals("doc")
						|| suffix.contentEquals("docx")) {
					contentType = "application/msword";
				} else if (suffix.contentEquals("xls")
						|| suffix.contentEquals("xlsx")) {
					contentType = "application/vnd.ms-excel";
				} else if (suffix.contentEquals("pdf")) {
					contentType = "application/pdf";
				}
				downloadedFile = new DefaultStreamedContent(stream,
						"contentType", downloadedFileName);

				stream.close();
			}
		} catch (Exception e) {
			System.out.println("SalesProposalBean.getDownloadedFile():"
					+ e.toString());
		}
		return downloadedFile;
	}

	public void createRevision() {

		SalesProposalManager salesProposalManager = new SalesProposalManager();
		SalesItemManager salesItemManager = new SalesItemManager();

		SalesProposal salesProposalRevision = new SalesProposal();
		SalesItem salesItemRevision;

		List<SalesItem> salesItems = salesItemManager.findByField(
				SalesItem.class, "proposal", selectedProposal);

		try {
			salesProposalRevision = (SalesProposal) this.selectedProposal
					.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			FacesContextUtils.addPlainErrorMessage(e.getMessage());
		}

		salesProposalRevision.setProposalId(new Integer(0));
		salesProposalRevision.setProposalNumber(null);
		try {
			salesProposalRevision
					.setReferenceNumber(getRevisionReferenceNumber(salesProposalRevision
							.getReferenceNumber()));
		} catch (Exception e) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_FATAL,
							"REFERANS NO BOŞ OLAMAZ, LÜTFEN REFERANS NO GİRİNİZ!",
							null));
			return;
		}
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		this.findNextNumber(sdf.format(cal.getTime()).substring(6));

		salesProposalRevision.setProposalNumber(sdf.format(cal.getTime())
				.substring(6) + "/" + String.valueOf(queueNumber));

		SalesProposalStatusManager salesProposalStatusManager = new SalesProposalStatusManager();

		selectedProposal.setStatus(salesProposalStatusManager.findByField(
				SalesProposalStatus.class, "statusId", 5).get(0));
		salesProposalManager.updateEntity(selectedProposal);

		salesProposalRevision.setStatus(salesProposalStatusManager.findByField(
				SalesProposalStatus.class, "statusId", 1).get(0));
		salesProposalManager.enterNew(salesProposalRevision);

		for (SalesItem item : salesItems) {
			try {
				salesItemRevision = (SalesItem) item.clone();
				salesItemRevision.setItemId(new Integer(0));
				salesItemRevision.setReservedRulos(null);
				salesItemRevision.setProposal(salesProposalRevision);

				salesItemManager.enterNew(salesItemRevision);
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
				FacesContextUtils.addPlainErrorMessage(e.getMessage());
			}
		}
		load();
		salesMenuBean.goToSalesProposal();
		FacesContextUtils
				.addPlainInfoMessage("Revizyon başarıyla oluşturulmuştur!");
	}

	public String getToBeDeleted() {
		return toBeDeleted;
	}

	public void setToBeDeleted(String toBeDeleted) {
		this.toBeDeleted = toBeDeleted;
	}

	private String getRevisionReferenceNumber(String referenceNumber) {

		String revisionProposalNumber = new String();
		String baseReferenceNumber = referenceNumber.split("/REV")[0];

		SalesProposalManager salesProposalManager = new SalesProposalManager();
		List<SalesProposal> salesProposals = salesProposalManager
				.findAllLikeOrderBy(SalesProposal.class, "referenceNumber",
						baseReferenceNumber, "DESC");

		SalesProposal lastRevision = salesProposals.get(0);
		String[] proposalNumberParts = lastRevision.getReferenceNumber().split(
				"/REV");

		if (proposalNumberParts.length == 1) {
			revisionProposalNumber = proposalNumberParts[0] + "/REV01";
		} else {
			Integer revisionIndex = Integer.parseInt(proposalNumberParts[1]);
			revisionIndex += 1;

			revisionProposalNumber = proposalNumberParts[0] + "/REV"
					+ String.format("%02d", revisionIndex).trim();
		}

		return revisionProposalNumber;
	}

}
