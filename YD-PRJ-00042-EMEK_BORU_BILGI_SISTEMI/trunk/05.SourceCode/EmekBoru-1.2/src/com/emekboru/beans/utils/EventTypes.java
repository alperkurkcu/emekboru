package com.emekboru.beans.utils;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;

import com.emekboru.jpa.EventTypeEb;
import com.emekboru.jpaman.EventTypeManager;
import com.emekboru.jpaman.Factory;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "eventTypes")
@SessionScoped
public class EventTypes implements Serializable {

	private static final long serialVersionUID = 6485977784407579620L;

	private List<EventTypeEb> eventTypes;
	private EventTypeEb selected;
	private EventTypeEb newType;

	public EventTypes() {

		EventTypeManager manager = new EventTypeManager();
		eventTypes = manager.findAll(EventTypeEb.class);
		System.out.println("how many event types:" + eventTypes.size());
		selected = new EventTypeEb();
		newType = new EventTypeEb();
	}

	@PostConstruct
	public void load() {

		EventTypeManager manager = new EventTypeManager();
		eventTypes = manager.findAll(EventTypeEb.class);
		System.out.println("how many event types:" + eventTypes.size());
	}

	public void add() {

		EventTypeManager manager = new EventTypeManager();
		manager.enterNew(newType);
		eventTypes.add(newType);
		newType = new EventTypeEb();
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void update() {

		EventTypeManager manager = new EventTypeManager();
		manager.updateEntity(selected);
		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}

	public void delete() {

		if (selected.getEventTypeId() == null) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		EntityManager manager = Factory.getInstance().createEntityManager();
		selected = manager.find(EventTypeEb.class, selected.getEventTypeId());
		manager.getTransaction().begin();
		if (selected.getEvents().size() > 0) {
			FacesContextUtils.addErrorMessage("DeleteFailMessage");
			return;
		}
		manager.remove(selected);
		manager.getTransaction().commit();
		manager.close();
		eventTypes.remove(selected);
		selected = new EventTypeEb();
		System.out.println("how many event types left :" + eventTypes.size());
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	/**
	 * GETTERS AND SETTERS
	 */
	public List<EventTypeEb> getEventTypes() {
		return eventTypes;
	}

	public void setEventTypes(List<EventTypeEb> eventTypes) {
		this.eventTypes = eventTypes;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public EventTypeEb getSelected() {
		return selected;
	}

	public void setSelected(EventTypeEb selected) {
		this.selected = selected;
	}

	public EventTypeEb getNewType() {
		return newType;
	}

	public void setNewType(EventTypeEb newType) {
		this.newType = newType;
	}
}
