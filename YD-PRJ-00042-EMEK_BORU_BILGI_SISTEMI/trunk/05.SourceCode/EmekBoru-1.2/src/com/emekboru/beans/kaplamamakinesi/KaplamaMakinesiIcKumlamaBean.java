/**
 * 
 */
package com.emekboru.beans.kaplamamakinesi;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isemri.IsEmriIcKumlama;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiIcKumlama;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isemirleri.IsEmriIcKumlamaManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamamakinesi.KaplamaMakinesiIcKumlamaManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "kaplamaMakinesiIcKumlamaBean")
@ViewScoped
public class KaplamaMakinesiIcKumlamaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<KaplamaMakinesiIcKumlama> allKaplamaMakinesiIcKumlamaList = new ArrayList<KaplamaMakinesiIcKumlama>();
	private KaplamaMakinesiIcKumlama kaplamaMakinesiIcKumlamaForm = new KaplamaMakinesiIcKumlama();

	private KaplamaMakinesiIcKumlama newKumlama = new KaplamaMakinesiIcKumlama();
	private List<KaplamaMakinesiIcKumlama> kumlamaList;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	private Pipe selectedPipe = new Pipe();

	private boolean kontrol = false;

	private String barkodNo = null;

	private IsEmriIcKumlama selectedIsEmriKaplama = new IsEmriIcKumlama();

	// kaplama kalite
	private List<IsolationTestDefinition> internalIsolationTestDefinitions = new ArrayList<IsolationTestDefinition>();
	private String kumlamaKalitesi = null;

	public KaplamaMakinesiIcKumlamaBean() {

	}

	public void addKumlama(ActionEvent ae) {

		UICommand cmd = (UICommand) ae.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId != null) {
			PipeManager pipeMan = new PipeManager();
			selectedPipe = pipeMan.loadObject(Pipe.class, prmPipeId);
		}

		try {
			KaplamaMakinesiIcKumlamaManager manager = new KaplamaMakinesiIcKumlamaManager();

			newKumlama.setEklemeZamani(UtilInsCore.getTarihZaman());
			newKumlama.setEkleyenKullanici(userBean.getUser().getId());
			newKumlama.setPipe(selectedPipe);
			manager.enterNew(newKumlama);
			newKumlama = new KaplamaMakinesiIcKumlama();

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"İÇ KUMLAMA İŞLEMİ BAŞARIYLA EKLENMİŞTİR!"));

		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL,
					"İÇ KUMLAMA İŞLEMİ EKLENEMEDİ!", null));
			System.out.println("KaplamaMakinesiIcKumlamaBean.addKumlama-HATA");
		}

		fillTestList(prmPipeId);
	}

	public void addKumlamaSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		Boolean prmKontrol = kaplamaMakinesiIcKumlamaForm.getDurum();
		KaplamaMakinesiIcKumlamaManager kumlamaManager = new KaplamaMakinesiIcKumlamaManager();

		if (prmKontrol == null) {

			try {

				kaplamaMakinesiIcKumlamaForm.setDurum(false);
				kaplamaMakinesiIcKumlamaForm
						.setKaplamaBaslamaZamani(UtilInsCore.getTarihZaman());
				kaplamaMakinesiIcKumlamaForm.setKaplamaBaslamaUser(userBean
						.getUser());
				kumlamaManager.updateEntity(kaplamaMakinesiIcKumlamaForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"İÇ KUMLAMA İŞLEMİ BAŞARIYLA BAŞLANMIŞTIR!"));
			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"İÇ KUMLAMA İŞLEMİ BAŞLANAMADI!", null));
				System.out
						.println("KaplamaMakinesiIcKumlamaBean.addKumlamaSonuc-HATA-BAŞLAMA");
			}
		} else if (prmKontrol == false) {

			try {

				kaplamaMakinesiIcKumlamaForm.setDurum(true);
				kaplamaMakinesiIcKumlamaForm.setKaplamaBitisZamani(UtilInsCore
						.getTarihZaman());
				kaplamaMakinesiIcKumlamaForm.setKaplamaBitisUser(userBean
						.getUser());
				kumlamaManager.updateEntity(kaplamaMakinesiIcKumlamaForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"İÇ KUMLAMA İŞLEMİ BAŞARIYLA BİTİRİLMİŞTİR!"));

			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"İÇ KUMLAMA İŞLEMİ BİTİRİLEMEDİ!", null));
				System.out
						.println("KaplamaMakinesiIcKumlamaBean.addKumlamaSonuc-HATA-BİTİŞ"
								+ e.toString());
			}

		}

		fillTestList(prmPipeId);
	}

	@SuppressWarnings("static-access")
	public void fillTestList(Integer prmPipeId) {

		KaplamaMakinesiIcKumlamaManager kumlamaManager = new KaplamaMakinesiIcKumlamaManager();
		allKaplamaMakinesiIcKumlamaList = kumlamaManager
				.getAllKaplamaMakinesiIcKumlama(prmPipeId);
	}

	public void kumlamaListener(SelectEvent event) {
		updateButtonRender = true;
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!", null));
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			KaplamaMakinesiIcKumlamaManager kumlamaManager = new KaplamaMakinesiIcKumlamaManager();
			allKaplamaMakinesiIcKumlamaList = kumlamaManager
					.getAllKaplamaMakinesiIcKumlama(selectedPipe.getPipeId());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
		}
	}

	public void deleteKumlamaSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		KaplamaMakinesiIcKumlamaManager kumlamaManager = new KaplamaMakinesiIcKumlamaManager();

		if (kaplamaMakinesiIcKumlamaForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		kumlamaManager.delete(kaplamaMakinesiIcKumlamaForm);
		kaplamaMakinesiIcKumlamaForm = new KaplamaMakinesiIcKumlama();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"İÇ KUMLAMA BAŞARIYLA SİLİNMİŞTİR!"));

		fillTestList(prmPipeId);
	}

	@SuppressWarnings("static-access")
	public void isEmriGoster() {

		IsolationTestDefinitionManager kaliteMan = new IsolationTestDefinitionManager();
		IsEmriIcKumlamaManager isEmriManager = new IsEmriIcKumlamaManager();

		if (isEmriManager.getAllIsEmriIcKumlama(
				selectedPipe.getSalesItem().getItemId()).size() > 0) {
			selectedIsEmriKaplama = isEmriManager.getAllIsEmriIcKumlama(
					selectedPipe.getSalesItem().getItemId()).get(0);
		} else {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					" İÇ KUMLAMA İŞ EMRİ BULUNAMADI, LÜTFEN EKLETİNİZ!", null));
			return;
		}

		internalIsolationTestDefinitions = kaliteMan
				.findIsolationByItemIdAndIsolationType(1,
						selectedIsEmriKaplama.getSalesItem());
		for (int i = 0; i < internalIsolationTestDefinitions.size(); i++) {
			if (internalIsolationTestDefinitions.get(i).getGlobalId() == 2123) {
				kumlamaKalitesi = internalIsolationTestDefinitions.get(i)
						.getAciklama();
			}
			if (internalIsolationTestDefinitions.get(i).getGlobalId() == 2124) {
				kumlamaKalitesi = kumlamaKalitesi + " "
						+ internalIsolationTestDefinitions.get(i).getAciklama();
			}
		}
	}

	// setters getters

	public List<KaplamaMakinesiIcKumlama> getAllKaplamaMakinesiIcKumlamaList() {
		return allKaplamaMakinesiIcKumlamaList;
	}

	public void setAllKaplamaMakinesiIcKumlamaList(
			List<KaplamaMakinesiIcKumlama> allKaplamaMakinesiIcKumlamaList) {
		this.allKaplamaMakinesiIcKumlamaList = allKaplamaMakinesiIcKumlamaList;
	}

	public KaplamaMakinesiIcKumlama getKaplamaMakinesiIcKumlamaForm() {
		return kaplamaMakinesiIcKumlamaForm;
	}

	public void setKaplamaMakinesiIcKumlamaForm(
			KaplamaMakinesiIcKumlama kaplamaMakinesiIcKumlamaForm) {
		this.kaplamaMakinesiIcKumlamaForm = kaplamaMakinesiIcKumlamaForm;
	}

	public KaplamaMakinesiIcKumlama getNewKumlama() {
		return newKumlama;
	}

	public void setNewKumlama(KaplamaMakinesiIcKumlama newKumlama) {
		this.newKumlama = newKumlama;
	}

	public List<KaplamaMakinesiIcKumlama> getKumlamaList() {
		return kumlamaList;
	}

	public void setKumlamaList(List<KaplamaMakinesiIcKumlama> kumlamaList) {
		this.kumlamaList = kumlamaList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	public boolean isKontrol() {
		return kontrol;
	}

	public void setKontrol(boolean kontrol) {
		this.kontrol = kontrol;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getBarkodNo() {
		return barkodNo;
	}

	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	public IsEmriIcKumlama getSelectedIsEmriKaplama() {
		return selectedIsEmriKaplama;
	}

	public void setSelectedIsEmriKaplama(IsEmriIcKumlama selectedIsEmriKaplama) {
		this.selectedIsEmriKaplama = selectedIsEmriKaplama;
	}

	/**
	 * @return the kumlamaKalitesi
	 */
	public String getKumlamaKalitesi() {
		return kumlamaKalitesi;
	}

	/**
	 * @param kumlamaKalitesi
	 *            the kumlamaKalitesi to set
	 */
	public void setKumlamaKalitesi(String kumlamaKalitesi) {
		this.kumlamaKalitesi = kumlamaKalitesi;
	}
}
