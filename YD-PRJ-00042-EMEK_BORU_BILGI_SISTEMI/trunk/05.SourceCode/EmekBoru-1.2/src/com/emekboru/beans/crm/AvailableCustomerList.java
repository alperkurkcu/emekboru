package com.emekboru.beans.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.Customer;
import com.emekboru.jpaman.CustomerManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "availableCustomerList")
@ViewScoped
public class AvailableCustomerList implements Serializable {

	private static final long serialVersionUID = -1669982598878674805L;

	private List<Customer> customers;

	private Customer[] selectedCustomers;

	@ManagedProperty(value = "#{activeEmployeeList}")
	private ActiveEmployeeList employeesBean;

	public AvailableCustomerList() {
		customers = new ArrayList<Customer>();
	}

	/**
	 * Load all customers that are not handled yet by an Employee this is a
	 * customer that does not have a CustomerRelation handled by the
	 * selectedEmployee (the end date is not null)
	 */
	public void load() {

		ActiveEmployeeList selectedEmpBean = (ActiveEmployeeList) FacesContextUtils
				.getViewBean("activeEmployeeList", ActiveEmployeeList.class);
		System.out
				.println("this is the employee Selected : " + selectedEmpBean);
		CustomerManager manager = new CustomerManager();
		customers = manager.notHandled(selectedEmpBean.getSelectedEmployee()
				.getEmployeeId());
	}

	/**
	 * GETTERS AND SETTERS
	 */
	public List<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ActiveEmployeeList getEmployeesBean() {
		return employeesBean;
	}

	public void setEmployeesBean(ActiveEmployeeList employeesBean) {
		this.employeesBean = employeesBean;
	}

	public Customer[] getSelectedCustomers() {
		return selectedCustomers;
	}

	public void setSelectedCustomers(Customer[] selectedCustomers) {
		this.selectedCustomers = selectedCustomers;
	}

}
