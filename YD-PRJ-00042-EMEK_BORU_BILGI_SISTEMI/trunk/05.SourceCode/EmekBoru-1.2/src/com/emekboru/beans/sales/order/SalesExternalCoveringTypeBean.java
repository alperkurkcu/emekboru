package com.emekboru.beans.sales.order;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.sales.order.SalesExternalCoveringType;
import com.emekboru.jpaman.sales.order.SalesExternalCoveringTypeManager;

@ManagedBean(name = "salesExternalCoveringTypeBean")
@ViewScoped
public class SalesExternalCoveringTypeBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1678390071578087486L;
	private List<SalesExternalCoveringType> typeList;

	public SalesExternalCoveringTypeBean() {

		SalesExternalCoveringTypeManager manager = new SalesExternalCoveringTypeManager();
		typeList = manager.findAll(SalesExternalCoveringType.class);
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public List<SalesExternalCoveringType> getTypeList() {
		return typeList;
	}

	public void setTypeList(List<SalesExternalCoveringType> typeList) {
		this.typeList = typeList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}