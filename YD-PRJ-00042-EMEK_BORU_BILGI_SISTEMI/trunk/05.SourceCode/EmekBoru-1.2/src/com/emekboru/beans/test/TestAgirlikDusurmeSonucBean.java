package com.emekboru.beans.test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.sales.order.SalesOrderBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.DestructiveTestsSpec;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.TestAgirlikDusurmeSonuc;
import com.emekboru.jpaman.DestructiveTestsSpecManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.TestAgirlikDusurmeSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testAgirlikDusurmeSonucBean")
@ViewScoped
public class TestAgirlikDusurmeSonucBean {

	private List<TestAgirlikDusurmeSonuc> allAgirlikDusurmeSonucList = new ArrayList<TestAgirlikDusurmeSonuc>();
	private TestAgirlikDusurmeSonuc testAgirlikDusurmeSonucForm = new TestAgirlikDusurmeSonuc();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	Integer prmGlobalId = null;
	Integer prmTestId = null;
	Integer prmPipeId = null;

	private Pipe pipe = null;
	private DestructiveTestsSpec bagliTest = null;
	private String sampleNo = null;

	public void sampleNoOlustur() {
		Integer count = 0;
		TestAgirlikDusurmeSonucManager manager = new TestAgirlikDusurmeSonucManager();
		count = manager.getAllTestAgirlikDusmeSonuc(prmGlobalId, prmPipeId)
				.size() + 1;
		sampleNo = bagliTest.getCode() + pipe.getPipeIndex() + "-" + count;
	}

	// methods

	@SuppressWarnings("unused")
	public void addAgirlikDusurmeTestSonuc(ActionEvent e) {
		UICommand cmd = (UICommand) e.getComponent();
		prmGlobalId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");
		prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");
		Boolean testD01Render = (Boolean) cmd.getChildren().get(3)
				.getAttributes().get("value");

		TestAgirlikDusurmeSonucManager tadsManager = new TestAgirlikDusurmeSonucManager();

		if (testAgirlikDusurmeSonucForm.getId() == null) {

			testAgirlikDusurmeSonucForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			testAgirlikDusurmeSonucForm.setEkleyenKullanici(userBean.getUser()
					.getId());

			pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testAgirlikDusurmeSonucForm.setPipe(pipe);

			bagliTest = new DestructiveTestsSpecManager().loadObject(
					DestructiveTestsSpec.class, prmTestId);
			testAgirlikDusurmeSonucForm.setBagliTestId(bagliTest);
			testAgirlikDusurmeSonucForm.setBagliGlobalId(bagliTest);

			sampleNoOlustur();
			testAgirlikDusurmeSonucForm.setSampleNo(sampleNo);

			tadsManager.enterNew(testAgirlikDusurmeSonucForm);

			// testlerin hangi borudan yapılacagı kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 2, true);
			DestructiveTestsSpecManager desMan = new DestructiveTestsSpecManager();
			desMan.testSonucKontrol(prmTestId, true);

			fillTestList(prmGlobalId, prmPipeId);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testAgirlikDusurmeSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testAgirlikDusurmeSonucForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			tadsManager.updateEntity(testAgirlikDusurmeSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");

		}
		TestAgirlikDusurmeSonucManager manager = new TestAgirlikDusurmeSonucManager();
		allAgirlikDusurmeSonucList = manager.getAllTestAgirlikDusmeSonuc(
				prmGlobalId, prmPipeId);

	}

	public void addAgirlikDusurmeTest() {
		testAgirlikDusurmeSonucForm = new TestAgirlikDusurmeSonuc();
	}

	public void agirlikDusurmeListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		TestAgirlikDusurmeSonucManager manager = new TestAgirlikDusurmeSonucManager();
		allAgirlikDusurmeSonucList = manager.getAllTestAgirlikDusmeSonuc(
				globalId, pipeId);

	}

	public void deleteTestAgirlikDusurmeSonuc() {

		TestAgirlikDusurmeSonucManager tadManager = new TestAgirlikDusurmeSonucManager();

		if (testAgirlikDusurmeSonucForm == null
				|| testAgirlikDusurmeSonucForm.getId() <= 0) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		tadManager.delete(testAgirlikDusurmeSonucForm);

		// sonuc var mı false
		DestructiveTestsSpecManager desMan = new DestructiveTestsSpecManager();
		desMan.testSonucKontrol(testAgirlikDusurmeSonucForm.getBagliTestId()
				.getTestId(), false);

		testAgirlikDusurmeSonucForm = new TestAgirlikDusurmeSonuc();
		FacesContextUtils.addWarnMessage("TestDeleteMessage");

	}

	// emodocument upload

	@EJB
	private ConfigBean config;

	private String path;
	private String privatePath;

	private File theFile = null;
	OutputStream output = null;
	InputStream input = null;

	@SuppressWarnings("unused")
	private File[] fileArray;

	private StreamedContent downloadedFile;
	private String downloadedFileName;

	SalesOrderBean sob = new SalesOrderBean();

	public TestAgirlikDusurmeSonucBean() {

		privatePath = File.separatorChar + "testDocs" + File.separatorChar
				+ "uploadedDocuments" + File.separatorChar
				+ "agirlikDusurmeSonuc" + File.separatorChar;
	}

	@PostConstruct
	public void load() {
		System.out.println("TestAgirlikDusurmeSonucBean.load()");

		try {
			// this.setPath(config.getConfig().getFolder().getAbsolutePath()
			// + privatePath);

			if (System.getProperty("os.name").startsWith("Windows")) {

				this.setPath("C:/emekfiles" + privatePath);
			} else {

				this.setPath("/home/emekboru/Desktop/emekfiles" + privatePath);
			}

			System.out.println("Path for Upload File (AgirlikDusurmeSonuc):			"
					+ path);
		} catch (Exception ex) {
			System.out.println("Path for Upload File (AgirlikDusurmeSonuc):			"
					+ FacesContext.getCurrentInstance().getExternalContext()
							.getRealPath(privatePath));

			System.out.print(path);
			System.out.println("CAUSE OF " + ex.toString());
		}

		theFile = new File(path);
		if (!theFile.isDirectory()) {
			theFile.mkdirs();
		}

		fileArray = theFile.listFiles();

	}

	public void addUploadedFile(FileUploadEvent event)
			throws AbortProcessingException, IOException {
		System.out.println("TestAgirlikDusurmeSonucBean.addUploadedFile()");

		try {
			String name = sob.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				testAgirlikDusurmeSonucForm
						.setDocuments(testAgirlikDusurmeSonucForm
								.getDocuments() + "//" + name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				TestAgirlikDusurmeSonucManager manager = new TestAgirlikDusurmeSonucManager();
				manager.updateEntity(testAgirlikDusurmeSonucForm);

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				FacesContextUtils.addWarnMessage("salesFileUploadedMessage");
			} else {
				System.out.println("File already exists.");

				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("TestAgirlikDusurmeSonucBean: " + e.toString());
		}
	}

	@SuppressWarnings("resource")
	public StreamedContent getDownloadedFile() {
		System.out.println("TestAgirlikDusurmeSonucBean.getDownloadedFile()");

		try {
			if (downloadedFileName != null) {
				RandomAccessFile accessFile = new RandomAccessFile(path
						+ downloadedFileName, "r");
				byte[] downloadByte = new byte[(int) accessFile.length()];
				accessFile.read(downloadByte);
				InputStream stream = new ByteArrayInputStream(downloadByte);

				String suffix = FilenameUtils.getExtension(downloadedFileName);
				String contentType = "text/plain";
				if (suffix.contentEquals("jpg") || suffix.contentEquals("png")
						|| suffix.contentEquals("gif")) {
					contentType = "image/" + contentType;
				} else if (suffix.contentEquals("doc")
						|| suffix.contentEquals("docx")) {
					contentType = "application/msword";
				} else if (suffix.contentEquals("xls")
						|| suffix.contentEquals("xlsx")) {
					contentType = "application/vnd.ms-excel";
				} else if (suffix.contentEquals("pdf")) {
					contentType = "application/pdf";
				}
				downloadedFile = new DefaultStreamedContent(stream,
						"contentType", downloadedFileName);

				stream.close();
			}
		} catch (Exception e) {
			System.out
					.println("TestAgirlikDusurmeSonucBean.getDownloadedFile():"
							+ e.toString());
		}
		return downloadedFile;
	}

	public String getDownloadedFileName() {
		return downloadedFileName;
	}

	public void setDownloadedFileName(String downloadedFileName) {
		try {
			this.downloadedFileName = downloadedFileName;
		} catch (Exception e) {
			System.out
					.println("TestAgirlikDusurmeSonucBean.setDownloadedFileName():"
							+ e.toString());
		}
	}

	public void setPath(String path) {
		this.path = path;
	}

	// document upload

	// setters getters

	public List<TestAgirlikDusurmeSonuc> getAllAgirlikDusurmeSonucList() {
		return allAgirlikDusurmeSonucList;
	}

	public void setAllAgirlikDusurmeSonucList(
			List<TestAgirlikDusurmeSonuc> allAgirlikDusurmeSonucList) {
		this.allAgirlikDusurmeSonucList = allAgirlikDusurmeSonucList;
	}

	public TestAgirlikDusurmeSonuc getTestAgirlikDusurmeSonucForm() {
		return testAgirlikDusurmeSonucForm;
	}

	public void setTestAgirlikDusurmeSonucForm(
			TestAgirlikDusurmeSonuc testAgirlikDusurmeSonucForm) {
		this.testAgirlikDusurmeSonucForm = testAgirlikDusurmeSonucForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
