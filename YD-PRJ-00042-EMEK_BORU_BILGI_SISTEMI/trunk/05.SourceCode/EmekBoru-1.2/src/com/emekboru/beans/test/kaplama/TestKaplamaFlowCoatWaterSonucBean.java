/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaFlowCoatWaterSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaFlowCoatWaterSonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testKaplamaFlowCoatWaterSonucBean")
@ViewScoped
public class TestKaplamaFlowCoatWaterSonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<TestKaplamaFlowCoatWaterSonuc> allKaplamaFlowCoatWaterSonucList = new ArrayList<TestKaplamaFlowCoatWaterSonuc>();
	private TestKaplamaFlowCoatWaterSonuc testKaplamaFlowCoatWaterSonucForm = new TestKaplamaFlowCoatWaterSonuc();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;
	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addKaplamaFlowCoatWaterSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaFlowCoatWaterSonucManager tkdsManager = new TestKaplamaFlowCoatWaterSonucManager();

		if (testKaplamaFlowCoatWaterSonucForm.getId() == null) {

			testKaplamaFlowCoatWaterSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaFlowCoatWaterSonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaFlowCoatWaterSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaFlowCoatWaterSonucForm.setBagliTestId(bagliTest);
			testKaplamaFlowCoatWaterSonucForm.setBagliGlobalId(bagliTest);

			tkdsManager.enterNew(testKaplamaFlowCoatWaterSonucForm);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaFlowCoatWaterSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaFlowCoatWaterSonucForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			tkdsManager.updateEntity(testKaplamaFlowCoatWaterSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");

		}
		fillTestList(prmGlobalId, prmPipeId);
	}

	public void addKaplamaFlowCoatWater() {
		testKaplamaFlowCoatWaterSonucForm = new TestKaplamaFlowCoatWaterSonuc();
		updateButtonRender = false;
	}

	public void kaplamaFlowCoatWaterListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(int globalId, Integer pipeId2) {
		allKaplamaFlowCoatWaterSonucList = TestKaplamaFlowCoatWaterSonucManager
				.getAllTestKaplamaFlowCoatWaterSonuc(globalId, pipeId2);
		testKaplamaFlowCoatWaterSonucForm = new TestKaplamaFlowCoatWaterSonuc();
	}

	public void deleteTestKaplamaFlowCoatWaterSonuc() {

		bagliTestId = testKaplamaFlowCoatWaterSonucForm.getBagliTestId()
				.getId();

		if (testKaplamaFlowCoatWaterSonucForm == null
				|| testKaplamaFlowCoatWaterSonucForm.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaFlowCoatWaterSonucManager tkdsManager = new TestKaplamaFlowCoatWaterSonucManager();
		tkdsManager.delete(testKaplamaFlowCoatWaterSonucForm);
		testKaplamaFlowCoatWaterSonucForm = new TestKaplamaFlowCoatWaterSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setters getters

	/**
	 * @return the allKaplamaFlowCoatWaterSonucList
	 */
	public List<TestKaplamaFlowCoatWaterSonuc> getAllKaplamaFlowCoatWaterSonucList() {
		return allKaplamaFlowCoatWaterSonucList;
	}

	/**
	 * @param allKaplamaFlowCoatWaterSonucList
	 *            the allKaplamaFlowCoatWaterSonucList to set
	 */
	public void setAllKaplamaFlowCoatWaterSonucList(
			List<TestKaplamaFlowCoatWaterSonuc> allKaplamaFlowCoatWaterSonucList) {
		this.allKaplamaFlowCoatWaterSonucList = allKaplamaFlowCoatWaterSonucList;
	}

	/**
	 * @return the testKaplamaFlowCoatWaterSonucForm
	 */
	public TestKaplamaFlowCoatWaterSonuc getTestKaplamaFlowCoatWaterSonucForm() {
		return testKaplamaFlowCoatWaterSonucForm;
	}

	/**
	 * @param testKaplamaFlowCoatWaterSonucForm
	 *            the testKaplamaFlowCoatWaterSonucForm to set
	 */
	public void setTestKaplamaFlowCoatWaterSonucForm(
			TestKaplamaFlowCoatWaterSonuc testKaplamaFlowCoatWaterSonucForm) {
		this.testKaplamaFlowCoatWaterSonucForm = testKaplamaFlowCoatWaterSonucForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
