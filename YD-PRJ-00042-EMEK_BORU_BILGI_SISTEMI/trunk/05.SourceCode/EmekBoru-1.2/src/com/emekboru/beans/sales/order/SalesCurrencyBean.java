package com.emekboru.beans.sales.order;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.sales.order.SalesCurrency;
import com.emekboru.jpaman.sales.order.SalesCurrencyManager;

@ManagedBean(name = "salesCurrencyBean")
@ViewScoped
public class SalesCurrencyBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2218433706289716014L;
	private List<SalesCurrency> currencyList;

	public SalesCurrencyBean() {

		SalesCurrencyManager manager = new SalesCurrencyManager();
		currencyList = manager.findAll(SalesCurrency.class);
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public List<SalesCurrency> getCurrencyList() {
		return currencyList;
	}

	public void setCurrencyList(List<SalesCurrency> currencyList) {
		this.currencyList = currencyList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}