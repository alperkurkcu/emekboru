package com.emekboru.beans.employee;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Employee;
import com.emekboru.jpaman.EmployeeManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "employeeListBean")
@ViewScoped
public class EmployeeListBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	private static List<Employee> employeeList;
	private static Employee selectedEmployee;

	public Integer departmentId;

	boolean activePassive = true;

	public EmployeeListBean() {

		employeeList = new ArrayList<Employee>();
		selectedEmployee = new Employee();
	}

	@PostConstruct
	public void init() {

		loadEmployeeList();
	}

	public static void removeEmployeeFromList(Employee employee) {

		employeeList.remove(employee);
	}

	public static void addEmployeeToList(Employee employee) {

		employeeList.add(employee);
	}

	public static void updateOneEmployeeFromList(Employee employee) {

		for (int i = 0; i < employeeList.size(); i++) {
			if (employeeList.get(i).getEmployeeId() == employee.getEmployeeId()) {

				employeeList.remove(i);
				employeeList.add(employee);
			}
		}
	}

	public static void loadEmployeeList() {

		EmployeeManager employeeManager = new EmployeeManager();
		// employeeList = employeeManager.findAll(Employee.class);
		employeeList = employeeManager.findAllOrderByASC(Employee.class,
				"firstname");

	}

	public void loadEmployeeListByDepartment() {
		System.out.println("employeeListBean.loadEmployeeListByDepartment()");

		try {
			EmployeeManager employeeManager = new EmployeeManager();
			employeeList = employeeManager.findByField(Employee.class,
					"department.departmentId", departmentId);
		} catch (Exception e) {
			System.out
					.println("employeeListBean.loadEmployeeListByDepartment():"
							+ e.toString());
		}
	}

	public void goToNewEmployeePage(ActionEvent event) {

		loadEmployeeList();
		FacesContextUtils.redirect(config.getPageUrl().NEW_EMPLOYEE_PAGE);
	}

	public void goToNewEmployeePage() {

		loadEmployeeList();
		FacesContextUtils.redirect(config.getPageUrl().NEW_EMPLOYEE_PAGE);
	}

	public void goToEmployeeManagePage(ActionEvent event) {

		loadEmployeeList();
		FacesContextUtils.redirect(config.getPageUrl().MANAGE_EMPLOYEE_PAGE);
	}

	public void goToEmployeeManagePage() {

		loadEmployeeList();
		FacesContextUtils.redirect(config.getPageUrl().MANAGE_EMPLOYEE_PAGE);
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	@SuppressWarnings("static-access")
	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}

	public Employee getSelectedEmployee() {
		return selectedEmployee;
	}

	@SuppressWarnings("static-access")
	public void setSelectedEmployee(Employee selectedEmployee) {
		this.selectedEmployee = selectedEmployee;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the departmentId
	 */
	public Integer getDepartmentId() {
		return departmentId;
	}

	/**
	 * @param departmentId
	 *            the departmentId to set
	 */
	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * @return the activePassive
	 */
	public boolean isActivePassive() {
		return activePassive;
	}

	/**
	 * @param activePassive
	 *            the activePassive to set
	 */
	public void setActivePassive(boolean activePassive) {
		this.activePassive = activePassive;
	}
}
