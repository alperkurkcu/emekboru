/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaUvAgeingTestSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaUvAgeingTestSonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testKaplamaUvAgeingTestSonucBean")
@ViewScoped
public class TestKaplamaUvAgeingTestSonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private TestKaplamaUvAgeingTestSonuc testKaplamaUvAgeingTestSonucForm = new TestKaplamaUvAgeingTestSonuc();
	private List<TestKaplamaUvAgeingTestSonuc> allTestKaplamaUvAgeingTestSonucList = new ArrayList<TestKaplamaUvAgeingTestSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateTestKaplamaUvAgeingTestSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaUvAgeingTestSonucManager tkbebManager = new TestKaplamaUvAgeingTestSonucManager();

		if (testKaplamaUvAgeingTestSonucForm.getId() == null) {

			testKaplamaUvAgeingTestSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaUvAgeingTestSonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaUvAgeingTestSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaUvAgeingTestSonucForm.setBagliTestId(bagliTest);
			testKaplamaUvAgeingTestSonucForm.setBagliGlobalId(bagliTest);

			tkbebManager.enterNew(testKaplamaUvAgeingTestSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			testKaplamaUvAgeingTestSonucForm = new TestKaplamaUvAgeingTestSonuc();
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaUvAgeingTestSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaUvAgeingTestSonucForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			tkbebManager.updateEntity(testKaplamaUvAgeingTestSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addTestKaplamaUvAgeingTestSonuc() {

		testKaplamaUvAgeingTestSonucForm = new TestKaplamaUvAgeingTestSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaUvAgeingTestSonucListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaUvAgeingTestSonucList = TestKaplamaUvAgeingTestSonucManager
				.getAllUvAgeingTestSonuc(globalId, pipeId);
		testKaplamaUvAgeingTestSonucForm = new TestKaplamaUvAgeingTestSonuc();
	}

	public void deleteTestKaplamaUvAgeingTestSonuc() {

		bagliTestId = testKaplamaUvAgeingTestSonucForm.getBagliGlobalId()
				.getId();

		if (testKaplamaUvAgeingTestSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		TestKaplamaUvAgeingTestSonucManager tkbebManager = new TestKaplamaUvAgeingTestSonucManager();

		tkbebManager.delete(testKaplamaUvAgeingTestSonucForm);
		testKaplamaUvAgeingTestSonucForm = new TestKaplamaUvAgeingTestSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters

	/**
	 * @return the testKaplamaUvAgeingTestSonucForm
	 */
	public TestKaplamaUvAgeingTestSonuc getTestKaplamaUvAgeingTestSonucForm() {
		return testKaplamaUvAgeingTestSonucForm;
	}

	/**
	 * @param testKaplamaUvAgeingTestSonucForm
	 *            the testKaplamaUvAgeingTestSonucForm to set
	 */
	public void setTestKaplamaUvAgeingTestSonucForm(
			TestKaplamaUvAgeingTestSonuc testKaplamaUvAgeingTestSonucForm) {
		this.testKaplamaUvAgeingTestSonucForm = testKaplamaUvAgeingTestSonucForm;
	}

	/**
	 * @return the allTestKaplamaUvAgeingTestSonucList
	 */
	public List<TestKaplamaUvAgeingTestSonuc> getAllTestKaplamaUvAgeingTestSonucList() {
		return allTestKaplamaUvAgeingTestSonucList;
	}

	/**
	 * @param allTestKaplamaUvAgeingTestSonucList
	 *            the allTestKaplamaUvAgeingTestSonucList to set
	 */
	public void setAllTestKaplamaUvAgeingTestSonucList(
			List<TestKaplamaUvAgeingTestSonuc> allTestKaplamaUvAgeingTestSonucList) {
		this.allTestKaplamaUvAgeingTestSonucList = allTestKaplamaUvAgeingTestSonucList;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
