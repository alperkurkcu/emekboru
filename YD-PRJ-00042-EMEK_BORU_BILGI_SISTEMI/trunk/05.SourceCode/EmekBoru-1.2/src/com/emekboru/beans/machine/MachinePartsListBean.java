package com.emekboru.beans.machine;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.Machine;
import com.emekboru.jpa.MachinePartCategory;
import com.emekboru.jpaman.MachineManager;
import com.emekboru.jpaman.MachinePartCategoryManager;

@ManagedBean(name = "machinePartsListBean")
@ViewScoped
public class MachinePartsListBean implements Serializable {

	private static final long serialVersionUID = 5568613889109874414L;

	private List<Machine> allMachineList;
	private List<MachinePartCategory> allMachinePartCategoryList;

	public MachinePartsListBean() {
		loadMachines();
		loadMachinePartCategories();
	}

	public void loadMachines() {

		MachineManager machineManager = new MachineManager();
		allMachineList = machineManager.findAll(Machine.class);
	}

	public void loadMachinePartCategories() {

		MachinePartCategoryManager categoryManager = new MachinePartCategoryManager();
		allMachinePartCategoryList = categoryManager
				.findAll(MachinePartCategory.class);
	}

	// ********************************************************** //
	// *** GETTERS AND SETTERS *** //
	// ********************************************************** //

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<Machine> getAllMachineList() {
		return allMachineList;
	}

	public void setAllMachineList(List<Machine> allMachineList) {
		this.allMachineList = allMachineList;
	}

	public List<MachinePartCategory> getAllMachinePartCategoryList() {
		return allMachinePartCategoryList;
	}

	public void setAllMachinePartCategoryList(
			List<MachinePartCategory> allMachinePartCategoryList) {
		this.allMachinePartCategoryList = allMachinePartCategoryList;
	}
}
