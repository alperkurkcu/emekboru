/**
 * 
 */
package com.emekboru.beans.istakipformu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.istakipformu.IsTakipFormuEpoksiKaplamaSonraIslemler;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isTakipFormuEpoksiKaplamaSonraIslemlerBean")
@ViewScoped
public class IsTakipFormuEpoksiKaplamaSonraIslemlerBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<IsTakipFormuEpoksiKaplamaSonraIslemler> allIsTakipFormuEpoksiKaplamaSonraIslemlerList = new ArrayList<IsTakipFormuEpoksiKaplamaSonraIslemler>();
	private IsTakipFormuEpoksiKaplamaSonraIslemler isTakipFormuEpoksiKaplamaSonraIslemlerForm = new IsTakipFormuEpoksiKaplamaSonraIslemler();

	// setters getters

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<IsTakipFormuEpoksiKaplamaSonraIslemler> getAllIsTakipFormuEpoksiKaplamaSonraIslemlerList() {
		return allIsTakipFormuEpoksiKaplamaSonraIslemlerList;
	}

	public void setAllIsTakipFormuEpoksiKaplamaSonraIslemlerList(
			List<IsTakipFormuEpoksiKaplamaSonraIslemler> allIsTakipFormuEpoksiKaplamaSonraIslemlerList) {
		this.allIsTakipFormuEpoksiKaplamaSonraIslemlerList = allIsTakipFormuEpoksiKaplamaSonraIslemlerList;
	}

	public IsTakipFormuEpoksiKaplamaSonraIslemler getIsTakipFormuEpoksiKaplamaSonraIslemlerForm() {
		return isTakipFormuEpoksiKaplamaSonraIslemlerForm;
	}

	public void setIsTakipFormuEpoksiKaplamaSonraIslemlerForm(
			IsTakipFormuEpoksiKaplamaSonraIslemler isTakipFormuEpoksiKaplamaSonraIslemlerForm) {
		this.isTakipFormuEpoksiKaplamaSonraIslemlerForm = isTakipFormuEpoksiKaplamaSonraIslemlerForm;
	}

}
