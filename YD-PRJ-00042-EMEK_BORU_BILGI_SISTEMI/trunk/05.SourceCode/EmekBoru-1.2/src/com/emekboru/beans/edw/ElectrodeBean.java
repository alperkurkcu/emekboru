package com.emekboru.beans.edw;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;

import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.EdwPipeLink;
import com.emekboru.jpa.ElectrodeDustWire;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.sales.customer.SalesCustomer;
import com.emekboru.jpaman.ElectrodeDustWireManager;
import com.emekboru.jpaman.Factory;
import com.emekboru.messages.ElectrodeDustWireType;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "electrodeBean")
@ViewScoped
public class ElectrodeBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
	private ConfigBean config;

	private ElectrodeDustWire selectedElectrode;

	// Attributes necessary to add a new dust
	private ElectrodeDustWire newElectrode;
	// private Customer selectedCustomer;
	private SalesCustomer selectedSalesCustomer;

	// to view the list of pipes produced from the Selected Dust
	private List<Pipe> pipeList;

	public ElectrodeBean() {

		selectedElectrode = new ElectrodeDustWire();
		newElectrode = new ElectrodeDustWire();
		// selectedCustomer = new Customer();
		selectedSalesCustomer = new SalesCustomer();
		pipeList = new ArrayList<Pipe>();
	}

	/******************************************************************************
	 ************** DUST MAIN METHODS SECTION ******************* /
	 ******************************************************************************/
	public void deleteElectrode() {

		if (!isElectrodeOk())
			return;

		EntityManager man = Factory.getInstance().createEntityManager();
		man.getTransaction().begin();
		selectedElectrode = man.find(ElectrodeDustWire.class,
				selectedElectrode.getElectrodeDustWireId());
		man.remove(selectedElectrode);
		man.getTransaction().commit();
		// remove it from the DustLists as well
		ElectrodeListBean bean = (ElectrodeListBean) FacesContextUtils
				.getViewBean("electrodeListBean", ElectrodeListBean.class);
		bean.getElectrodes().remove(selectedElectrode);
		bean.getRecentElectrodes().remove(selectedElectrode);

		selectedElectrode = new ElectrodeDustWire();
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	public void addElectrode() {

		EntityManager man = Factory.getInstance().createEntityManager();
		man.getTransaction().begin();

		// newElectrode.setCustomer(selectedCustomer);
		newElectrode.setSalesCustomer(selectedSalesCustomer);
		newElectrode.setRemainingAmount(newElectrode.getMiktari());
		newElectrode.setType(ElectrodeDustWireType.ELECTRODE);
		newElectrode.setStatus(config.getConfig().getMaterials()
				.getDefaultStatus());
		// newElectrode.setOrderId(0);
		newElectrode.setSalesItemId(0);
		man.persist(newElectrode);
		man.getTransaction().commit();
		man.close();
		// add the bean to the BeanList
		ElectrodeListBean bean = (ElectrodeListBean) FacesContextUtils
				.getViewBean("electrodeListBean", ElectrodeListBean.class);
		bean.getRecentElectrodes().add(newElectrode);
		newElectrode = new ElectrodeDustWire();
		// selectedCustomer = new Customer();
		selectedSalesCustomer = new SalesCustomer();
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void loadHistory() {

		EntityManager man = Factory.getInstance().createEntityManager();
		man.getTransaction().begin();
		selectedElectrode = man.find(ElectrodeDustWire.class,
				selectedElectrode.getElectrodeDustWireId());
		for (EdwPipeLink l : selectedElectrode.getEdwPipeLinks()) {
			pipeList.add(l.getPipe());
		}
		man.getTransaction().commit();
		man.close();
	}

	public void updateElectrode() {

		if (!isElectrodeOk())
			return;

		ElectrodeDustWireManager man = new ElectrodeDustWireManager();
		man.updateEntity(selectedElectrode);
		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}

	/******************************************************************************
	 *************** HELPER PROTECTED METHODS ******************* /
	 ******************************************************************************/
	protected boolean isElectrodeOk() {

		if (selectedElectrode.getStatus(config.getConfig()).isDepleted()
				|| selectedElectrode.getStatus(config.getConfig()).isSold()) {

			System.out.println("Trying to update sold Electrode!!");
			FacesContextUtils.addWarnMessage("NotAvailableMessage");
			return false;
		}
		return true;
	}

	/******************************************************************************
	 ******************** GETTERS AND SETTERS ******************* /
	 ******************************************************************************/
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	public ElectrodeDustWire getSelectedElectrode() {
		return selectedElectrode;
	}

	public void setSelectedElectrode(ElectrodeDustWire selectedElectrode) {
		this.selectedElectrode = selectedElectrode;
	}

	public ElectrodeDustWire getNewElectrode() {
		return newElectrode;
	}

	public void setNewElectrode(ElectrodeDustWire newElectrode) {
		this.newElectrode = newElectrode;
	}

	public List<Pipe> getPipeList() {
		return pipeList;
	}

	public void setPipeList(List<Pipe> pipeList) {
		this.pipeList = pipeList;
	}

	public SalesCustomer getSelectedSalesCustomer() {
		return selectedSalesCustomer;
	}

	public void setSelectedSalesCustomer(SalesCustomer selectedSalesCustomer) {
		this.selectedSalesCustomer = selectedSalesCustomer;
	}

}
