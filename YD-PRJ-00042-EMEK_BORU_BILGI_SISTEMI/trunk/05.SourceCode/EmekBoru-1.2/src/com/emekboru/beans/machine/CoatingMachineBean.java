package com.emekboru.beans.machine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.Error;
import com.emekboru.jpa.Machine;
import com.emekboru.jpa.MachinePipeLink;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.sales.customer.SalesCustomer;
import com.emekboru.jpa.sales.order.SalesItem;

@ManagedBean(name = "coatingMachineBean")
@ViewScoped
public class CoatingMachineBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Machine currentMachine;
	// private Customer currentCompany;
	private SalesCustomer currentSalesCompany;
	// private Order currentOrder;
	// private Order currentOrderItem;
	private SalesItem currentItem;
	private Pipe currentPipe;
	private List<Pipe> inMachinePipeList;
	private List<Pipe> freePipeList;
	private Pipe inMachineSelectedPipe;
	private List<Error> errorList;
	private Error currentError;
	// private List<Order> activeOrderList;
	private List<SalesItem> activeItemList;
	private MachinePipeLink machinePipeLink;

	public CoatingMachineBean() {
		machinePipeLink = new MachinePipeLink();
		currentMachine = new Machine();
		// currentCompany = new Customer();
		currentSalesCompany = new SalesCustomer();
		// currentOrder = new Order();
		// currentOrderItem = new Order();
		currentItem = new SalesItem();
		currentPipe = new Pipe();
		inMachinePipeList = new ArrayList<Pipe>();
		freePipeList = new ArrayList<Pipe>();
		inMachineSelectedPipe = new Pipe();
		errorList = new ArrayList<Error>();
		currentError = new Error();
		// activeOrderList = new ArrayList<Order>();
		activeItemList = new ArrayList<SalesItem>();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public Machine getCurrentMachine() {
		return currentMachine;
	}

	public void setCurrentMachine(Machine currentMachine) {
		this.currentMachine = currentMachine;
	}

	public Pipe getCurrentPipe() {
		return currentPipe;
	}

	public void setCurrentPipe(Pipe currentPipe) {
		this.currentPipe = currentPipe;
	}

	public List<Pipe> getInMachinePipeList() {
		return inMachinePipeList;
	}

	public void setInMachinePipeList(List<Pipe> inMachinePipeList) {
		this.inMachinePipeList = inMachinePipeList;
	}

	public List<Pipe> getFreePipeList() {
		return freePipeList;
	}

	public void setFreePipeList(List<Pipe> freePipeList) {
		this.freePipeList = freePipeList;
	}

	public Pipe getInMachineSelectedPipe() {
		return inMachineSelectedPipe;
	}

	public void setInMachineSelectedPipe(Pipe inMachineSelectedPipe) {
		this.inMachineSelectedPipe = inMachineSelectedPipe;
	}

	public List<Error> getErrorList() {
		return errorList;
	}

	public void setErrorList(List<Error> errorList) {
		this.errorList = errorList;
	}

	public Error getCurrentError() {
		return currentError;
	}

	public void setCurrentError(Error currentError) {
		this.currentError = currentError;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public MachinePipeLink getMachinePipeLink() {
		return machinePipeLink;
	}

	public void setMachinePipeLink(MachinePipeLink machinePipeLink) {
		this.machinePipeLink = machinePipeLink;
	}

	public SalesCustomer getCurrentSalesCompany() {
		return currentSalesCompany;
	}

	public void setCurrentSalesCompany(SalesCustomer currentSalesCompany) {
		this.currentSalesCompany = currentSalesCompany;
	}

	public SalesItem getCurrentItem() {
		return currentItem;
	}

	public void setCurrentItem(SalesItem currentItem) {
		this.currentItem = currentItem;
	}

	public List<SalesItem> getActiveItemList() {
		return activeItemList;
	}

	public void setActiveItemList(List<SalesItem> activeItemList) {
		this.activeItemList = activeItemList;
	}

}
