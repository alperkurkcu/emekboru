package com.emekboru.beans.test.kaplama;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaBetonYuzeyHazirligiSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaBetonYuzeyHazirligiSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaplamaBetonYuzeyHazirligiSonucBean")
@ViewScoped
public class TestKaplamaBetonYuzeyHazirligiSonucBean {

	private TestKaplamaBetonYuzeyHazirligiSonuc testKaplamaBetonYuzeyHazirligiSonucForm = new TestKaplamaBetonYuzeyHazirligiSonuc();
	private List<TestKaplamaBetonYuzeyHazirligiSonuc> allTestKaplamaBetonYuzeyHazirligiSonucList = new ArrayList<TestKaplamaBetonYuzeyHazirligiSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateTestKaplamaBetonYuzeyHazirligiSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaBetonYuzeyHazirligiSonucManager tkbyhManager = new TestKaplamaBetonYuzeyHazirligiSonucManager();

		if (testKaplamaBetonYuzeyHazirligiSonucForm.getId() == null) {

			testKaplamaBetonYuzeyHazirligiSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaBetonYuzeyHazirligiSonucForm
					.setEkleyenKullanici(userBean.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaBetonYuzeyHazirligiSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaBetonYuzeyHazirligiSonucForm.setBagliTestId(bagliTest);
			testKaplamaBetonYuzeyHazirligiSonucForm.setBagliGlobalId(bagliTest);

			tkbyhManager.enterNew(testKaplamaBetonYuzeyHazirligiSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaBetonYuzeyHazirligiSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaBetonYuzeyHazirligiSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			tkbyhManager.updateEntity(testKaplamaBetonYuzeyHazirligiSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addTestKaplamaBetonYuzeyHazirligiSonuc() {

		testKaplamaBetonYuzeyHazirligiSonucForm = new TestKaplamaBetonYuzeyHazirligiSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaBetonYuzeyHazirligiSonucListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaBetonYuzeyHazirligiSonucList = TestKaplamaBetonYuzeyHazirligiSonucManager
				.getAllTestKaplamaBetonYuzeyHazirligiSonuc(globalId, pipeId);
		testKaplamaBetonYuzeyHazirligiSonucForm = new TestKaplamaBetonYuzeyHazirligiSonuc();
	}

	public void deleteTestKaplamaBetonYuzeyHazirligiSonuc() {

		bagliTestId = testKaplamaBetonYuzeyHazirligiSonucForm
				.getBagliGlobalId().getId();

		if (testKaplamaBetonYuzeyHazirligiSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaBetonYuzeyHazirligiSonucManager tkbyhManager = new TestKaplamaBetonYuzeyHazirligiSonucManager();
		tkbyhManager.delete(testKaplamaBetonYuzeyHazirligiSonucForm);
		testKaplamaBetonYuzeyHazirligiSonucForm = new TestKaplamaBetonYuzeyHazirligiSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters
	public TestKaplamaBetonYuzeyHazirligiSonuc getTestKaplamaBetonYuzeyHazirligiSonucForm() {
		return testKaplamaBetonYuzeyHazirligiSonucForm;
	}

	public void setTestKaplamaBetonYuzeyHazirligiSonucForm(
			TestKaplamaBetonYuzeyHazirligiSonuc testKaplamaBetonYuzeyHazirligiSonucForm) {
		this.testKaplamaBetonYuzeyHazirligiSonucForm = testKaplamaBetonYuzeyHazirligiSonucForm;
	}

	public List<TestKaplamaBetonYuzeyHazirligiSonuc> getAllTestKaplamaBetonYuzeyHazirligiSonucList() {
		return allTestKaplamaBetonYuzeyHazirligiSonucList;
	}

	public void setAllTestKaplamaBetonYuzeyHazirligiSonucList(
			List<TestKaplamaBetonYuzeyHazirligiSonuc> allTestKaplamaBetonYuzeyHazirligiSonucList) {
		this.allTestKaplamaBetonYuzeyHazirligiSonucList = allTestKaplamaBetonYuzeyHazirligiSonucList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
