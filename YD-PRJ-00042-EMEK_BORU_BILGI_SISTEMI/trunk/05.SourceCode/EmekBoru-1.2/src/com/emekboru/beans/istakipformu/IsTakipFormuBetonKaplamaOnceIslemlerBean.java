/**
 * 
 */
package com.emekboru.beans.istakipformu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.istakipformu.IsTakipFormuBetonKaplamaOnceIslemler;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isTakipFormuBetonKaplamaOnceIslemlerBean")
@ViewScoped
public class IsTakipFormuBetonKaplamaOnceIslemlerBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<IsTakipFormuBetonKaplamaOnceIslemler> allIsTakipFormuBetonKaplamaOnceIslemlerList = new ArrayList<IsTakipFormuBetonKaplamaOnceIslemler>();
	private IsTakipFormuBetonKaplamaOnceIslemler isTakipFormuBetonKaplamaOnceIslemlerForm = new IsTakipFormuBetonKaplamaOnceIslemler();

	// setters getters
	public List<IsTakipFormuBetonKaplamaOnceIslemler> getAllIsTakipFormuBetonKaplamaOnceIslemlerList() {
		return allIsTakipFormuBetonKaplamaOnceIslemlerList;
	}

	public void setAllIsTakipFormuBetonKaplamaOnceIslemlerList(
			List<IsTakipFormuBetonKaplamaOnceIslemler> allIsTakipFormuBetonKaplamaOnceIslemlerList) {
		this.allIsTakipFormuBetonKaplamaOnceIslemlerList = allIsTakipFormuBetonKaplamaOnceIslemlerList;
	}

	public IsTakipFormuBetonKaplamaOnceIslemler getIsTakipFormuBetonKaplamaOnceIslemlerForm() {
		return isTakipFormuBetonKaplamaOnceIslemlerForm;
	}

	public void setIsTakipFormuBetonKaplamaOnceIslemlerForm(
			IsTakipFormuBetonKaplamaOnceIslemler isTakipFormuBetonKaplamaOnceIslemlerForm) {
		this.isTakipFormuBetonKaplamaOnceIslemlerForm = isTakipFormuBetonKaplamaOnceIslemlerForm;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
