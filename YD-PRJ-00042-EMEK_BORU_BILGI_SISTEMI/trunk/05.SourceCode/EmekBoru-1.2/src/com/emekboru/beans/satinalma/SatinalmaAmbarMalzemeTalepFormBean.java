/**
 * 
 */
package com.emekboru.beans.satinalma;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.converters.StokUrunKartlariConverter;
import com.emekboru.jpa.employee.EmployeeDayoff;
import com.emekboru.jpa.satinalma.SatinalmaAmbarMalzemeTalepForm;
import com.emekboru.jpa.satinalma.SatinalmaDurum;
import com.emekboru.jpa.stok.StokUrunKartlari;
import com.emekboru.jpaman.satinalma.SatinalmaAmbarMalzemeTalepFormManager;
import com.emekboru.jpaman.satinalma.SatinalmaDurumManager;
import com.emekboru.utils.DateTrans;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;
import com.emekboru.utils.lazymodels.LazyAmbarTalepDataModel;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "satinalmaAmbarMalzemeTalepFormBean")
@ViewScoped
public class SatinalmaAmbarMalzemeTalepFormBean implements Serializable {

	private static final long serialVersionUID = 1L;

	// private List<SatinalmaAmbarMalzemeTalepForm>
	// allSatinalmaAmbarMalzemeTalepFormList = new
	// ArrayList<SatinalmaAmbarMalzemeTalepForm>();
	private LazyDataModel<SatinalmaAmbarMalzemeTalepForm> allSatinalmaAmbarMalzemeTalepFormLazyList;
	// private List<SatinalmaAmbarMalzemeTalepForm>
	// allSatinalmaAmbarMalzemeTalepFormPersonalList = new
	// ArrayList<SatinalmaAmbarMalzemeTalepForm>();
	private LazyDataModel<SatinalmaAmbarMalzemeTalepForm> allSatinalmaAmbarMalzemeTalepFormPersonalLazyList;
	private List<SatinalmaAmbarMalzemeTalepForm> allSatinalmaAmbarMalzemeTalepFormOnayBekleyenlerList = new ArrayList<SatinalmaAmbarMalzemeTalepForm>();
	private LazyDataModel<SatinalmaAmbarMalzemeTalepForm> allSatinalmaAmbarMalzemeTalepFormOnayGecmisLazyList;
	// private List<SatinalmaAmbarMalzemeTalepForm>
	// allSatinalmaAmbarMalzemeTalepFormOnayGecmisList = new
	// ArrayList<SatinalmaAmbarMalzemeTalepForm>();
	private List<SatinalmaAmbarMalzemeTalepForm> fixedSatinalmaAmbarMalzemeTalepFormList = new ArrayList<SatinalmaAmbarMalzemeTalepForm>();
	private List<SatinalmaAmbarMalzemeTalepForm> openedFinishedSatinalmaAmbarMalzemeTalepFormList = new ArrayList<SatinalmaAmbarMalzemeTalepForm>();

	private List<Integer> empityList = new ArrayList<Integer>();

	private SatinalmaAmbarMalzemeTalepForm satinalmaAmbarMalzemeTalepFormEkran = new SatinalmaAmbarMalzemeTalepForm();
	private SatinalmaDurum satinalmaDurum = new SatinalmaDurum();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	public static List<StokUrunKartlari> urunItems;

	@ManagedProperty(value = "#{satinalmaMalzemeHizmetTalepFormuBean}")
	private SatinalmaMalzemeHizmetTalepFormuBean satinalmaMalzemeHizmetTalepFormuBean;

	public Integer onayBekleyenlerSayisi = null;

	@PostConstruct
	public void load() {
		urunItems = StokUrunKartlariConverter.urunlerItemDB;
		onayBekleyenlerSayisi = null;
	}

	@SuppressWarnings("deprecation")
	public SatinalmaAmbarMalzemeTalepFormBean() {

		Date date = UtilInsCore.getTarihZaman();

		Calendar dokuz = Calendar.getInstance();
		dokuz.set(Calendar.HOUR_OF_DAY, 9);
		dokuz.set(Calendar.MINUTE, 0);
		dokuz.set(Calendar.SECOND, 0);
		dokuz.set(Calendar.MILLISECOND, 0);
		Date dateDokuz = dokuz.getTime();

		Calendar dokuzOn = Calendar.getInstance();
		dokuzOn.set(Calendar.HOUR_OF_DAY, 9);
		dokuzOn.set(Calendar.MINUTE, 10);
		dokuzOn.set(Calendar.SECOND, 0);
		dokuzOn.set(Calendar.MILLISECOND, 0);
		Date dateDokuzOn = dokuzOn.getTime();

		if (date.getDate() == 1 || date.getDate() == 15) {
			if (date.after(dateDokuz) && date.before(dateDokuzOn)) {
				findEmpity();
				talepDurumKontrolleri();
			}
		}
	}

	public void addForm() {

		satinalmaAmbarMalzemeTalepFormEkran = new SatinalmaAmbarMalzemeTalepForm();
		// today set ediliyor
		java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(
				DateTrans.calendar.getTime().getTime());
		satinalmaAmbarMalzemeTalepFormEkran.setTalepTarihi(currentTimestamp);
		updateButtonRender = false;
	}

	Integer count = 0;

	public void findEmpity() {

		Runnable r = new Runnable() {

			@Override
			public void run() {
				count = 0;

				synchronized (this) {
					SatinalmaAmbarMalzemeTalepFormManager formManager = new SatinalmaAmbarMalzemeTalepFormManager();
					empityList = formManager.findEmpity();
					for (int i = 3; i < empityList.size(); i++) {
						SatinalmaAmbarMalzemeTalepFormManager deleteManager = new SatinalmaAmbarMalzemeTalepFormManager();
						deleteManager.deleteEmpity(empityList.get(i));
						count++;
					}
				}
			}
		};

		Thread thread = new Thread(r);
		thread.start();

		// FacesContext context = FacesContext.getCurrentInstance();
		// if (count > 0) {
		// context.addMessage(null, new FacesMessage(
		// FacesMessage.SEVERITY_INFO, count
		// + " KADAR BOŞ TALEP BAŞARIYLA SİLİNMİŞTİR!", null));
		// } else {
		// context.addMessage(null, new FacesMessage(
		// FacesMessage.SEVERITY_INFO, "SİLİNECEK BOŞ TALEP YOKTUR!",
		// null));
		// }

		FacesContext context = FacesContext.getCurrentInstance();

		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				" BULUNAN BOŞ TALEPLER BAŞARIYLA SİLİNMİŞTİR!", null));
	}

	public void addOrUpdateForm() {

		SatinalmaDurumManager manager = new SatinalmaDurumManager();
		satinalmaDurum = manager.loadObject(SatinalmaDurum.class, 1);

		if (satinalmaAmbarMalzemeTalepFormEkran.getTalepSahibi() == null
				|| satinalmaAmbarMalzemeTalepFormEkran.getSupervisor() == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_FATAL,
							"TALEBİ YAPAN ve ONAY - BÖLÜM MÜDÜRÜ BOŞ BIRAKILAMAZ, TEKRAR DENEYİNİZ!",
							null));
			return;
		}

		SatinalmaAmbarMalzemeTalepFormManager formManager = new SatinalmaAmbarMalzemeTalepFormManager();
		try {

			if (satinalmaAmbarMalzemeTalepFormEkran.getId() == null) {

				satinalmaAmbarMalzemeTalepFormEkran.setEklemeZamani(UtilInsCore
						.getTarihZaman());
				satinalmaAmbarMalzemeTalepFormEkran
						.setEkleyenKullanici(userBean.getUser().getId());
				satinalmaAmbarMalzemeTalepFormEkran
						.setSupervisorApproval(SatinalmaAmbarMalzemeTalepForm.PENDING);
				satinalmaAmbarMalzemeTalepFormEkran.setTamamlanma(false);
				satinalmaAmbarMalzemeTalepFormEkran
						.setSatinalmaDurum(satinalmaDurum);
				sayiBul();

				if (satinalmaAmbarMalzemeTalepFormEkran.getSupervisor() == null) {
					satinalmaAmbarMalzemeTalepFormEkran.setSupervisor(userBean
							.getUser().getEmployee().getSupervisor());
				}

				formManager.enterNew(satinalmaAmbarMalzemeTalepFormEkran);
				this.satinalmaAmbarMalzemeTalepFormEkran = new SatinalmaAmbarMalzemeTalepForm();
				FacesContextUtils.addInfoMessage("FormSubmitMessage");

			} else {

				if (satinalmaAmbarMalzemeTalepFormEkran.isTamamlanma()) {

					FacesContext context = FacesContext.getCurrentInstance();
					context.addMessage(
							null,
							new FacesMessage(
									FacesMessage.SEVERITY_ERROR,
									"ONAYA GÖNDERİLEN İSTEK DÜZENLENEMEZ, LÜTFEN YENİ İSTEK AÇINIZ!",
									null));
					satinalmaAmbarMalzemeTalepFormEkran = new SatinalmaAmbarMalzemeTalepForm();
					return;
				}

				satinalmaAmbarMalzemeTalepFormEkran
						.setGuncellemeZamani(UtilInsCore.getTarihZaman());
				satinalmaAmbarMalzemeTalepFormEkran
						.setGuncelleyenKullanici(userBean.getUser().getId());
				formManager.updateEntity(satinalmaAmbarMalzemeTalepFormEkran);

				FacesContextUtils.addInfoMessage("FormUpdateMessage");

				// Malzeme İstek Formuna dönüştürme.

				try {
					if (satinalmaAmbarMalzemeTalepFormEkran.getSatinalmaDurum()
							.getTitle().toLowerCase().contains("malzeme")) {
						if (!satinalmaMalzemeHizmetTalepFormuBean
								.isAmbarTransformedToMalzeme(satinalmaAmbarMalzemeTalepFormEkran
										.getId())) {
							satinalmaMalzemeHizmetTalepFormuBean
									.addFromAmbar(satinalmaAmbarMalzemeTalepFormEkran);
						}
					}
				} catch (Exception e) {
					System.out
							.println("-HATA-satinalmaAmbarMalzemeTalepFormBean.addOrUpdateForm-Malzeme İstek Formuna dönüştürme"
									+ e.toString());
				}
			}
		} catch (Exception e) {
			System.out
					.println("-HATA-satinalmaAmbarMalzemeTalepFormBean.addOrUpdateForm: "
							+ e.toString());
		}

		fillTestList();
	}

	public void deleteForm() {

		SatinalmaAmbarMalzemeTalepFormManager formManager = new SatinalmaAmbarMalzemeTalepFormManager();
		try {

			if (satinalmaAmbarMalzemeTalepFormEkran.getId() == null) {

				FacesContextUtils.addWarnMessage("NoSelectMessage");
				return;
			}
			formManager.delete(satinalmaAmbarMalzemeTalepFormEkran);
			fillTestList();
			FacesContextUtils.addWarnMessage("TestDeleteMessage");
		} catch (Exception e) {
			System.out
					.println("SatinalmaAmbarMalzemeTalepFormBean - deleteForm(): "
							+ e.toString());
		}
	}

	// public void son100Getir() {
	//
	// Thread thread = new Thread() {
	//
	// @Override
	// public void run() {
	// allSatinalmaAmbarMalzemeTalepFormList = new
	// ArrayList<SatinalmaAmbarMalzemeTalepForm>();
	// SatinalmaAmbarMalzemeTalepFormManager manager = new
	// SatinalmaAmbarMalzemeTalepFormManager();
	// allSatinalmaAmbarMalzemeTalepFormList = manager.findLimited();
	// }
	//
	// };
	// thread.run();
	//
	// }

	// public void fillTestList() {
	//
	// Thread thread = new Thread() {
	//
	// @Override
	// public void run() {
	// System.out
	// .println("getAllSatinalmaAmbarMalzemeTalepForm çalıştıran kullanıcı : "
	// + userBean.getUsername());
	// SatinalmaAmbarMalzemeTalepFormManager manager = new
	// SatinalmaAmbarMalzemeTalepFormManager();
	// allSatinalmaAmbarMalzemeTalepFormList = manager
	// .getAllSatinalmaAmbarMalzemeTalepForm();
	//
	// SatinalmaAmbarMalzemeTalepFormManager manager1 = new
	// SatinalmaAmbarMalzemeTalepFormManager();
	// allSatinalmaAmbarMalzemeTalepFormPersonalList = manager1
	// .getUserSatinalmaAmbarMalzemeTalepForm(userBean
	// .getUser().getId());
	//
	// // SatinalmaAmbarMalzemeTalepFormManager manager2 = new
	// // SatinalmaAmbarMalzemeTalepFormManager();
	// // allSatinalmaAmbarMalzemeTalepFormOnayBekleyenlerList =
	// // manager2
	// // .getOnayBekleyenlerSatinalmaAmbarMalzemeTalepForm(userBean
	// // .getUser().getEmployee().getEmployeeId());
	// // onayBekleyenlerSayisi =
	// // allSatinalmaAmbarMalzemeTalepFormOnayBekleyenlerList
	// // .size();
	//
	// SatinalmaAmbarMalzemeTalepFormManager manager3 = new
	// SatinalmaAmbarMalzemeTalepFormManager();
	// allSatinalmaAmbarMalzemeTalepFormOnayGecmisList = manager3
	// .getOnayGecmisSatinalmaAmbarMalzemeTalepForm(userBean
	// .getUser().getEmployee().getEmployeeId());
	// }
	//
	// };
	// thread.run();
	//
	// satinalmaAmbarMalzemeTalepFormEkran = new
	// SatinalmaAmbarMalzemeTalepForm();
	// updateButtonRender = false;
	// }

	public void fillTestList() {

		allSatinalmaAmbarMalzemeTalepFormLazyList = new LazyAmbarTalepDataModel(
				0, false);
		allSatinalmaAmbarMalzemeTalepFormPersonalLazyList = new LazyAmbarTalepDataModel(
				userBean.getUser().getId(), false);

		// SatinalmaAmbarMalzemeTalepFormManager manager2 = new
		// SatinalmaAmbarMalzemeTalepFormManager();
		// allSatinalmaAmbarMalzemeTalepFormOnayBekleyenlerList =
		// manager2
		// .getOnayBekleyenlerSatinalmaAmbarMalzemeTalepForm(userBean
		// .getUser().getEmployee().getEmployeeId());
		// onayBekleyenlerSayisi =
		// allSatinalmaAmbarMalzemeTalepFormOnayBekleyenlerList
		// .size();

		allSatinalmaAmbarMalzemeTalepFormOnayGecmisLazyList = new LazyAmbarTalepDataModel(
				userBean.getUser().getId(), true);

		satinalmaAmbarMalzemeTalepFormEkran = new SatinalmaAmbarMalzemeTalepForm();
		updateButtonRender = false;
	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	// autocompete için metodlar
	public List<StokUrunKartlari> completeStokUrunKartlari(String query) {
		List<StokUrunKartlari> suggestions = new ArrayList<StokUrunKartlari>();

		for (StokUrunKartlari i : urunItems) {
			if (i.getUrunAdi().startsWith(query))
				suggestions.add(i);
		}

		return suggestions;
	}

	public void sayiBul() {

		SatinalmaAmbarMalzemeTalepFormManager manager = new SatinalmaAmbarMalzemeTalepFormManager();
		satinalmaAmbarMalzemeTalepFormEkran.setFormSayisi(manager
				.getOtomatikSayi().toString());
	}

	public void onayaGonder() {

		// 27 - Onay Bekleniyor
		SatinalmaDurumManager manager = new SatinalmaDurumManager();
		satinalmaDurum = manager.loadObject(SatinalmaDurum.class, 27);

		if (satinalmaAmbarMalzemeTalepFormEkran == null) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ONAYA GÖNDERİLECEK TALEBİ SEÇİNİZ!", null));
			return;
		}

		if (satinalmaAmbarMalzemeTalepFormEkran.isTamamlanma()) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCEDEN ONAYA GÖNDERİLMİŞ İSTEK!", null));
			return;
		}

		satinalmaAmbarMalzemeTalepFormEkran.setTamamlanma(true);
		satinalmaAmbarMalzemeTalepFormEkran.setSatinalmaDurum(satinalmaDurum);
		SatinalmaAmbarMalzemeTalepFormManager talepManager = new SatinalmaAmbarMalzemeTalepFormManager();

		talepManager.updateEntity(satinalmaAmbarMalzemeTalepFormEkran);

		FacesContextUtils.addPlainInfoMessage("TALEP ONAYA GÖNDERİLDİ!");

		this.load();

	}

	public void declineRequest() {

		// 2 - Olumsuz Sonuclandı.
		SatinalmaDurumManager manager = new SatinalmaDurumManager();
		satinalmaDurum = manager.loadObject(SatinalmaDurum.class, 2);

		if (satinalmaAmbarMalzemeTalepFormEkran == null) {

			FacesContextUtils
					.addPlainWarnMessage("LÜTFEN REDDETMEK İSTEDİĞİNİZ TALEBİ SEÇİNİZ!");
			return;
		}

		if (satinalmaAmbarMalzemeTalepFormEkran.getSupervisorApproval() != EmployeeDayoff.PENDING) {

			FacesContextUtils
					.addPlainErrorMessage("ONAYLADIĞINIZ/REDDETTİĞİNİZ BİR TALEBİ DEĞİŞTİREMEZSİNİZ!");
			return;
		}

		satinalmaAmbarMalzemeTalepFormEkran
				.setSupervisorApproval(EmployeeDayoff.DECLINED);
		satinalmaAmbarMalzemeTalepFormEkran.setOnayTarihi(UtilInsCore
				.getTarihZaman());
		satinalmaAmbarMalzemeTalepFormEkran.setSatinalmaDurum(satinalmaDurum);
		SatinalmaAmbarMalzemeTalepFormManager talepManager = new SatinalmaAmbarMalzemeTalepFormManager();

		talepManager.updateEntity(satinalmaAmbarMalzemeTalepFormEkran);

		FacesContextUtils.addPlainInfoMessage("TALEBİ REDDETTİNİZ!");

		this.load();

	}

	public void approveRequest() {

		// 28 - onay verildi.
		SatinalmaDurumManager manager = new SatinalmaDurumManager();
		satinalmaDurum = manager.loadObject(SatinalmaDurum.class, 28);

		if (satinalmaAmbarMalzemeTalepFormEkran == null) {

			FacesContextUtils
					.addPlainWarnMessage("LÜTFEN ONAYLAMAK İSTEDİĞİNİZ TALEBİ SEÇİNİZ!");
			return;
		}

		if (satinalmaAmbarMalzemeTalepFormEkran.getSupervisorApproval() != EmployeeDayoff.PENDING) {

			FacesContextUtils
					.addPlainErrorMessage("ONAYLADIĞINIZ/REDDETTİĞİNİZ BİR TALEBİ DEĞİŞTİREMEZSİNİZ!");
			return;
		}

		satinalmaAmbarMalzemeTalepFormEkran
				.setSupervisorApproval(EmployeeDayoff.APPROVED);
		satinalmaAmbarMalzemeTalepFormEkran.setOnayTarihi(UtilInsCore
				.getTarihZaman());
		satinalmaAmbarMalzemeTalepFormEkran.setSatinalmaDurum(satinalmaDurum);
		SatinalmaAmbarMalzemeTalepFormManager talepManager = new SatinalmaAmbarMalzemeTalepFormManager();

		talepManager.updateEntity(satinalmaAmbarMalzemeTalepFormEkran);

		FacesContextUtils.addPlainInfoMessage("TALEBİ ONAYLADINIZ!");

		this.load();

	}

	public void revizyonaGonder() {

		// 29 - revizyon istendi.
		SatinalmaDurumManager manager = new SatinalmaDurumManager();
		satinalmaDurum = manager.loadObject(SatinalmaDurum.class, 29);

		if (satinalmaAmbarMalzemeTalepFormEkran == null) {

			FacesContextUtils
					.addPlainWarnMessage("LÜTFEN REVİZYON İSTEDİĞİNİZ TALEBİ SEÇİNİZ!");
			return;
		}

		satinalmaAmbarMalzemeTalepFormEkran.setTamamlanma(false);
		satinalmaAmbarMalzemeTalepFormEkran.setSatinalmaDurum(satinalmaDurum);
		SatinalmaAmbarMalzemeTalepFormManager talepManager = new SatinalmaAmbarMalzemeTalepFormManager();

		talepManager.updateEntity(satinalmaAmbarMalzemeTalepFormEkran);

		FacesContextUtils.addPlainWarnMessage("TALEBİ REVİZYONA GÖNDERDİNİZ!");

		this.load();

	}

	public void malzemeHizmetFormunaCevir() {

		if (satinalmaAmbarMalzemeTalepFormEkran == null) {

			FacesContextUtils.addPlainWarnMessage("LÜTFEN TALEBİ SEÇİNİZ!");
			return;
		}

		// Malzeme İstek Formuna dönüştürme.

		try {

			if (!satinalmaMalzemeHizmetTalepFormuBean
					.isAmbarTransformedToMalzeme(satinalmaAmbarMalzemeTalepFormEkran
							.getId())) {
				satinalmaMalzemeHizmetTalepFormuBean
						.addFromAmbar(satinalmaAmbarMalzemeTalepFormEkran);
			}
		} catch (Exception e) {
			System.out.println("SalesProposalBean: " + e.toString());
		}

		FacesContextUtils
				.addPlainInfoMessage("TALEP, MALZEME/HİZMET TALEBİNE ÇEVRİLDİ!");

		this.load();

	}

	public void talepDurumKontrolleri() {

		SatinalmaAmbarMalzemeTalepFormManager manager = new SatinalmaAmbarMalzemeTalepFormManager();
		openedFinishedSatinalmaAmbarMalzemeTalepFormList = manager
				.getOpenedFinishedSatinalmaAmbarMalzemeTalepForm();

		for (int i = 0; i < openedFinishedSatinalmaAmbarMalzemeTalepFormList
				.size(); i++) {
			openedFinishedSatinalmaAmbarMalzemeTalepFormList.get(i)
					.getSatinalmaDurum().setId(14);
			manager.updateEntity(openedFinishedSatinalmaAmbarMalzemeTalepFormList
					.get(i));
		}

	}

	// setters getters
	// public List<SatinalmaAmbarMalzemeTalepForm>
	// getAllSatinalmaAmbarMalzemeTalepFormList() {
	// return allSatinalmaAmbarMalzemeTalepFormList;
	// }
	//
	// public void setAllSatinalmaAmbarMalzemeTalepFormList(
	// List<SatinalmaAmbarMalzemeTalepForm>
	// allSatinalmaAmbarMalzemeTalepFormList) {
	// this.allSatinalmaAmbarMalzemeTalepFormList =
	// allSatinalmaAmbarMalzemeTalepFormList;
	// }

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public SatinalmaAmbarMalzemeTalepForm getSatinalmaAmbarMalzemeTalepFormEkran() {
		return satinalmaAmbarMalzemeTalepFormEkran;
	}

	public void setSatinalmaAmbarMalzemeTalepFormEkran(
			SatinalmaAmbarMalzemeTalepForm satinalmaAmbarMalzemeTalepFormEkran) {
		this.satinalmaAmbarMalzemeTalepFormEkran = satinalmaAmbarMalzemeTalepFormEkran;
	}

	public SatinalmaMalzemeHizmetTalepFormuBean getSatinalmaMalzemeHizmetTalepFormuBean() {
		return satinalmaMalzemeHizmetTalepFormuBean;
	}

	public void setSatinalmaMalzemeHizmetTalepFormuBean(
			SatinalmaMalzemeHizmetTalepFormuBean satinalmaMalzemeHizmetTalepFormuBean) {
		this.satinalmaMalzemeHizmetTalepFormuBean = satinalmaMalzemeHizmetTalepFormuBean;
	}

	// public List<SatinalmaAmbarMalzemeTalepForm>
	// getAllSatinalmaAmbarMalzemeTalepFormPersonalList() {
	// return allSatinalmaAmbarMalzemeTalepFormPersonalList;
	// }
	//
	// public void setAllSatinalmaAmbarMalzemeTalepFormPersonalList(
	// List<SatinalmaAmbarMalzemeTalepForm>
	// allSatinalmaAmbarMalzemeTalepFormPersonalList) {
	// this.allSatinalmaAmbarMalzemeTalepFormPersonalList =
	// allSatinalmaAmbarMalzemeTalepFormPersonalList;
	// }

	public List<SatinalmaAmbarMalzemeTalepForm> getAllSatinalmaAmbarMalzemeTalepFormOnayBekleyenlerList() {
		return allSatinalmaAmbarMalzemeTalepFormOnayBekleyenlerList;
	}

	public void setAllSatinalmaAmbarMalzemeTalepFormOnayBekleyenlerList(
			List<SatinalmaAmbarMalzemeTalepForm> allSatinalmaAmbarMalzemeTalepFormOnayBekleyenlerList) {
		this.allSatinalmaAmbarMalzemeTalepFormOnayBekleyenlerList = allSatinalmaAmbarMalzemeTalepFormOnayBekleyenlerList;
	}

	// public List<SatinalmaAmbarMalzemeTalepForm>
	// getAllSatinalmaAmbarMalzemeTalepFormOnayGecmisList() {
	// return allSatinalmaAmbarMalzemeTalepFormOnayGecmisList;
	// }
	//
	// public void setAllSatinalmaAmbarMalzemeTalepFormOnayGecmisList(
	// List<SatinalmaAmbarMalzemeTalepForm>
	// allSatinalmaAmbarMalzemeTalepFormOnayGecmisList) {
	// this.allSatinalmaAmbarMalzemeTalepFormOnayGecmisList =
	// allSatinalmaAmbarMalzemeTalepFormOnayGecmisList;
	// }

	/**
	 * @return the onayBekleyenlerSayisi
	 */
	public Integer getOnayBekleyenlerSayisi() {
		return onayBekleyenlerSayisi;
	}

	/**
	 * @return the allSatinalmaAmbarMalzemeTalepFormLazyList
	 */
	public LazyDataModel<SatinalmaAmbarMalzemeTalepForm> getAllSatinalmaAmbarMalzemeTalepFormLazyList() {
		return allSatinalmaAmbarMalzemeTalepFormLazyList;
	}

	/**
	 * @param allSatinalmaAmbarMalzemeTalepFormLazyList
	 *            the allSatinalmaAmbarMalzemeTalepFormLazyList to set
	 */
	public void setAllSatinalmaAmbarMalzemeTalepFormLazyList(
			LazyDataModel<SatinalmaAmbarMalzemeTalepForm> allSatinalmaAmbarMalzemeTalepFormLazyList) {
		this.allSatinalmaAmbarMalzemeTalepFormLazyList = allSatinalmaAmbarMalzemeTalepFormLazyList;
	}

	/**
	 * @return the allSatinalmaAmbarMalzemeTalepFormPersonalLazyList
	 */
	public LazyDataModel<SatinalmaAmbarMalzemeTalepForm> getAllSatinalmaAmbarMalzemeTalepFormPersonalLazyList() {
		return allSatinalmaAmbarMalzemeTalepFormPersonalLazyList;
	}

	/**
	 * @param allSatinalmaAmbarMalzemeTalepFormPersonalLazyList
	 *            the allSatinalmaAmbarMalzemeTalepFormPersonalLazyList to set
	 */
	public void setAllSatinalmaAmbarMalzemeTalepFormPersonalLazyList(
			LazyDataModel<SatinalmaAmbarMalzemeTalepForm> allSatinalmaAmbarMalzemeTalepFormPersonalLazyList) {
		this.allSatinalmaAmbarMalzemeTalepFormPersonalLazyList = allSatinalmaAmbarMalzemeTalepFormPersonalLazyList;
	}

	/**
	 * @return the allSatinalmaAmbarMalzemeTalepFormOnayGecmisLazyList
	 */
	public LazyDataModel<SatinalmaAmbarMalzemeTalepForm> getAllSatinalmaAmbarMalzemeTalepFormOnayGecmisLazyList() {
		return allSatinalmaAmbarMalzemeTalepFormOnayGecmisLazyList;
	}

	/**
	 * @param allSatinalmaAmbarMalzemeTalepFormOnayGecmisLazyList
	 *            the allSatinalmaAmbarMalzemeTalepFormOnayGecmisLazyList to set
	 */
	public void setAllSatinalmaAmbarMalzemeTalepFormOnayGecmisLazyList(
			LazyDataModel<SatinalmaAmbarMalzemeTalepForm> allSatinalmaAmbarMalzemeTalepFormOnayGecmisLazyList) {
		this.allSatinalmaAmbarMalzemeTalepFormOnayGecmisLazyList = allSatinalmaAmbarMalzemeTalepFormOnayGecmisLazyList;
	}

}
