package com.emekboru.beans.order;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;

import com.emekboru.beans.edw.AvailDustBean;
import com.emekboru.beans.edw.AvailElectrodeBean;
import com.emekboru.beans.edw.AvailWireBean;
import com.emekboru.beans.rulo.AvailRuloBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Customer;
import com.emekboru.jpa.ElectrodeDustWire;
import com.emekboru.jpa.Order;
import com.emekboru.jpa.rulo.Rulo;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.ElectrodeDustWireManager;
import com.emekboru.jpaman.Factory;
import com.emekboru.jpaman.OrderManager;
import com.emekboru.jpaman.rulo.RuloManager;
import com.emekboru.messages.ElectrodeDustWireType;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "manageOrderBean")
@ViewScoped
public class ManageOrderBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	private Order selectedOrder;
	private Customer selectedCustomer;
	private SalesItem selectedSalesItem;
	private List<Order> orders;

	// allocated materials for the selectedOrder
	private AllocatedMaterials allocatedMaterials;

	public ManageOrderBean() {

		selectedCustomer = new Customer();
		selectedOrder = new Order();
		selectedSalesItem = new SalesItem();
		orders = new ArrayList<Order>(0);

		allocatedMaterials = new AllocatedMaterials();
	}

	/******************************************************************************
	 *************************** BASIC ORDER METHODS **********************
	 ******************************************************************************/
	public void loadOrders() {

		EntityManager man = Factory.getInstance().createEntityManager();
		selectedCustomer = man.find(Customer.class,
				selectedCustomer.getCustomerId());
		man.refresh(selectedCustomer);
		man.getTransaction().begin();
		orders = selectedCustomer.getOrders();
		man.getTransaction().commit();
		man.close();
	}

	public void deleteOrder() {

		EntityManager man = Factory.getInstance().createEntityManager();
		selectedOrder = man.find(Order.class, selectedOrder.getOrderId());
		man.getTransaction().begin();
		man.remove(selectedOrder);
		man.getTransaction().commit();
		orders.remove(selectedOrder);
		man.close();
		selectedOrder = new Order();
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	public void loadAllocatedMaterials() {

		allocatedMaterials.load(selectedSalesItem);
	}

	// this method changes the status without checking the current status of the
	// selected order. Its intent is to serve rarely and only to managers
	public void changeStatus() {

		OrderManager man = new OrderManager();
		man.updateEntity(selectedOrder);
		FacesContextUtils.addWarnMessage("UpdateItemMessage");
	}

	/******************************************************************************
	 ******************* ALLOCATE MATERIALS METHODS ********************** /
	 ******************************************************************************/
	/*
	 * public void allocateCoil(){
	 * 
	 * System.out.println("allocating coil");
	 * if(allocatedMaterials.getSelectedCoil() == null) return;
	 * 
	 * CoilManager man = new CoilManager();
	 * allocatedMaterials.getSelectedCoil().setOrderId(selectedOrder
	 * .getOrderId());
	 * allocatedMaterials.getSelectedCoil().setStatuses(config.getConfig().
	 * getMaterials().findStatusByName(Coil.STATUS_ALLOCATED));
	 * man.updateEntity(allocatedMaterials.getSelectedCoil());
	 * 
	 * allocatedMaterials.getCoils().add(allocatedMaterials .getSelectedCoil());
	 * AvailCoilBean bean = (AvailCoilBean) FacesContextUtils.getViewBean
	 * ("availCoilBean", AvailCoilBean.class);
	 * bean.getAvailableCoils().remove(allocatedMaterials.getSelectedCoil());
	 * FacesContextUtils.addInfoMessage("MaterialAllocationMessage"); }
	 */

	public void allocateRulo() {

		System.out.println("allocating rulo");
		if (allocatedMaterials.getSelectedRulo() == null)
			return;

		RuloManager man = new RuloManager();
		allocatedMaterials.getSelectedRulo().setSalesItemId(
				selectedOrder.getOrderId());
		allocatedMaterials.getSelectedRulo().setStatuses(
				config.getConfig().getMaterials()
						.findStatusByName(Rulo.STATUS_ALLOCATED));
		man.updateEntity(allocatedMaterials.getSelectedRulo());

		allocatedMaterials.getRulos().add(allocatedMaterials.getSelectedRulo());
		AvailRuloBean bean = (AvailRuloBean) FacesContextUtils.getViewBean(
				"availRuloBean", AvailRuloBean.class);
		bean.getAvailableRulos().remove(allocatedMaterials.getSelectedRulo());
		FacesContextUtils.addInfoMessage("MaterialAllocationMessage");
	}

	public void allocateEdw() {

		if (allocatedMaterials.getSelectedEdw() == null)
			return;

		ElectrodeDustWireManager man = new ElectrodeDustWireManager();
		// allocatedMaterials.getSelectedEdw().setOrderId(
		// selectedOrder.getOrderId());
		allocatedMaterials.getSelectedEdw().setSalesItemId(
				selectedSalesItem.getItemId());
		allocatedMaterials.getSelectedEdw().setStatus(
				config.getConfig().getMaterials()
						.findStatusByName(Rulo.STATUS_ALLOCATED));
		man.updateEntity(allocatedMaterials.getSelectedEdw());

		if (allocatedMaterials.getSelectedEdw().getType()
				.equals(ElectrodeDustWireType.ELECTRODE)) {

			AvailElectrodeBean bean = (AvailElectrodeBean) FacesContextUtils
					.getViewBean("availElectrodeBean", AvailElectrodeBean.class);
			bean.getAvailableElectrodes().remove(
					allocatedMaterials.getSelectedEdw());
			allocatedMaterials.getElectrodes().add(
					allocatedMaterials.getSelectedEdw());
		}

		if (allocatedMaterials.getSelectedEdw().getType()
				.equals(ElectrodeDustWireType.DUST)) {

			AvailDustBean bean = (AvailDustBean) FacesContextUtils.getViewBean(
					"availDustBean", AvailDustBean.class);
			bean.getAvailableDusts()
					.remove(allocatedMaterials.getSelectedEdw());
			allocatedMaterials.getDusts().add(
					allocatedMaterials.getSelectedEdw());
		}

		if (allocatedMaterials.getSelectedEdw().getType()
				.equals(ElectrodeDustWireType.WIRE)) {

			AvailWireBean bean = (AvailWireBean) FacesContextUtils.getViewBean(
					"availWireBean", AvailWireBean.class);
			bean.getAvailableWires()
					.remove(allocatedMaterials.getSelectedEdw());
			allocatedMaterials.getWires().add(
					allocatedMaterials.getSelectedEdw());
		}
		FacesContextUtils.addInfoMessage("MaterialAllocationMessage");
	}

	/*
	 * public void removeAllocatedCoil(Coil coil){
	 * 
	 * CoilManager man = new CoilManager();
	 * coil.setStatuses(config.getConfig().getMaterials().
	 * findStatusByName(Coil.STATUS_FREE)); coil.setOrderId(0);
	 * man.updateEntity(coil);
	 * 
	 * allocatedMaterials.getCoils().remove(coil); AvailCoilBean bean =
	 * (AvailCoilBean) FacesContextUtils.getViewBean ("availCoilBean",
	 * AvailCoilBean.class); bean.getAvailableCoils().add(coil); }
	 */
	public void removeAllocatedRulo(Rulo rulo) {

		RuloManager man = new RuloManager();
		rulo.setStatuses(config.getConfig().getMaterials()
				.findStatusByName(Rulo.STATUS_FREE));
		rulo.setSalesItemId(0);
		man.updateEntity(rulo);

		allocatedMaterials.getRulos().remove(rulo);
		AvailRuloBean bean = (AvailRuloBean) FacesContextUtils.getViewBean(
				"availRuloBean", AvailRuloBean.class);
		bean.getAvailableRulos().add(rulo);
	}

	public void removeAllocatedEdw(ElectrodeDustWire edw) {

		ElectrodeDustWireManager man = new ElectrodeDustWireManager();
		edw.setStatus(config.getConfig().getMaterials()
				.findStatusByName(Rulo.STATUS_FREE));
		// edw.setOrderId(0);
		edw.setSalesItemId(0);
		man.updateEntity(edw);

		if (edw.getType().equals(ElectrodeDustWireType.ELECTRODE)) {

			allocatedMaterials.getElectrodes().remove(edw);
			AvailElectrodeBean bean = (AvailElectrodeBean) FacesContextUtils
					.getViewBean("availElectrodeBean", AvailElectrodeBean.class);
			bean.getAvailableElectrodes().add(edw);
		}

		if (edw.getType().equals(ElectrodeDustWireType.DUST)) {

			allocatedMaterials.getDusts().remove(edw);
			AvailDustBean bean = (AvailDustBean) FacesContextUtils.getViewBean(
					"availDustBean", AvailDustBean.class);
			bean.getAvailableDusts().add(edw);
		}

		if (edw.getType().equals(ElectrodeDustWireType.WIRE)) {

			allocatedMaterials.getWires().remove(edw);
			AvailWireBean bean = (AvailWireBean) FacesContextUtils.getViewBean(
					"availWireBean", AvailWireBean.class);
			bean.getAvailableWires().add(edw);
		}
	}

	/******************************************************************************
	 ******************* PROTECTED HELPER METHODS ********************** /
	 ******************************************************************************/

	/******************************************************************************
	 ********************* GETTERS AND SETTERS ********************** /
	 ******************************************************************************/
	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	public Order getSelectedOrder() {
		return selectedOrder;
	}

	public void setSelectedOrder(Order selectedOrder) {
		this.selectedOrder = selectedOrder;
	}

	public Customer getSelectedCustomer() {
		return selectedCustomer;
	}

	public void setSelectedCustomer(Customer selectedCustomer) {
		this.selectedCustomer = selectedCustomer;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	public AllocatedMaterials getAllocatedMaterials() {
		return allocatedMaterials;
	}

	public void setAllocatedMaterials(AllocatedMaterials allocatedMaterials) {
		this.allocatedMaterials = allocatedMaterials;
	}
}
