/**
 * 
 */
package com.emekboru.beans.istakipformu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.istakipformu.IsTakipFormuPolietilenKaplama;
import com.emekboru.jpa.istakipformu.IsTakipFormuPolietilenKaplamaDevamIslemler;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.istakipformu.IsTakipFormuPolietilenKaplamaDevamIslemlerManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isTakipFormuPolietilenKaplamaDevamIslemlerBean")
@ViewScoped
public class IsTakipFormuPolietilenKaplamaDevamIslemlerBean implements
		Serializable {

	private static final long serialVersionUID = 1L;

	private List<IsTakipFormuPolietilenKaplamaDevamIslemler> allIsTakipFormuPolietilenKaplamaDevamIslemlerList = new ArrayList<IsTakipFormuPolietilenKaplamaDevamIslemler>();
	private IsTakipFormuPolietilenKaplamaDevamIslemler isTakipFormuPolietilenKaplamaDevamIslemlerForm = new IsTakipFormuPolietilenKaplamaDevamIslemler();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private boolean buttonRender;

	private Pipe selectedPipe = new Pipe();

	private String barkodNo = null;

	private SalesItem selectedSalesItem = new SalesItem();
	private IsTakipFormuPolietilenKaplama selectedIsTakipFormuPolietilenKaplama = new IsTakipFormuPolietilenKaplama();

	private Integer salesItemId = null;

	public IsTakipFormuPolietilenKaplamaDevamIslemlerBean() {

	}

	public void reset() {

		isTakipFormuPolietilenKaplamaDevamIslemlerForm = new IsTakipFormuPolietilenKaplamaDevamIslemler();
		updateButtonRender = false;
		buttonRender = false;
	};

	public void addOrUpdateFormSonuc() {

		IsTakipFormuPolietilenKaplamaDevamIslemlerManager devamManager = new IsTakipFormuPolietilenKaplamaDevamIslemlerManager();

		if (isTakipFormuPolietilenKaplamaDevamIslemlerForm.getId() == null) {

			try {

				isTakipFormuPolietilenKaplamaDevamIslemlerForm
						.setEklemeZamani(UtilInsCore.getTarihZaman());
				isTakipFormuPolietilenKaplamaDevamIslemlerForm
						.setEkleyenEmployee(userBean.getUser());
				isTakipFormuPolietilenKaplamaDevamIslemlerForm
						.setPipe(selectedPipe);
				isTakipFormuPolietilenKaplamaDevamIslemlerForm
						.setSalesItem(selectedPipe.getSalesItem());

				devamManager
						.enterNew(isTakipFormuPolietilenKaplamaDevamIslemlerForm);

				FacesContext context = FacesContext.getCurrentInstance();

				context.addMessage(
						null,
						new FacesMessage(
								"POLİETİLEN KAPLAMA İŞ TAKİP FORMU BAŞARIYLA EKLENMİŞTİR!"));

			} catch (Exception ex) {

				System.out
						.println("isTakipFormuPolietilenKaplamaDevamIslemlerBean.addOrUpdateFormSonuc-HATA");
			}

		} else {

			try {

				isTakipFormuPolietilenKaplamaDevamIslemlerForm
						.setGuncellemeZamani(UtilInsCore.getTarihZaman());
				isTakipFormuPolietilenKaplamaDevamIslemlerForm
						.setGuncelleyenEmployee(userBean.getUser());
				devamManager
						.updateEntity(isTakipFormuPolietilenKaplamaDevamIslemlerForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								"POLİETİLEN KAPLAMA İŞ TAKİP FORMU BAŞARIYLA GÜNCELLENMİŞTİR!"));
			} catch (Exception ex) {

				System.out
						.println("isTakipFormuPolietilenKaplamaDevamIslemlerBean.addOrUpdateFormSonuc-HATA"
								+ ex.toString());
			}
		}
		fillTestList(selectedPipe.getSalesItem().getItemId());
	}

	@SuppressWarnings("static-access")
	public void fillTestList(Integer prmItemId) {

		IsTakipFormuPolietilenKaplamaDevamIslemlerManager formManager = new IsTakipFormuPolietilenKaplamaDevamIslemlerManager();
		allIsTakipFormuPolietilenKaplamaDevamIslemlerList = formManager
				.getAllIsTakipFormuPolietilenKaplamaDevamIslemler(prmItemId);
		buttonRender = true;
		salesItemId = prmItemId;
	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_WARN, prmBarkod
							+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!", null));
			return;
		} else {

			if (salesItemId != pipeMan.findByBarkodNo(prmBarkod).get(0)
					.getSalesItem().getItemId()) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_WARN,
								prmBarkod
										+ " BARKOD NUMARALI BORU, YANLIŞ SİPARİŞ BORUSUDUR, LÜTFEN KONTROL EDİNİZ!",
								null));
				return;
			}

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			IsTakipFormuPolietilenKaplamaDevamIslemlerManager formManager = new IsTakipFormuPolietilenKaplamaDevamIslemlerManager();
			allIsTakipFormuPolietilenKaplamaDevamIslemlerList = formManager
					.getAllIsTakipFormuPolietilenKaplamaDevamIslemler(selectedPipe
							.getSalesItem().getItemId());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
		}
	}

	public void deleteFormSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		IsTakipFormuPolietilenKaplamaDevamIslemlerManager formManager = new IsTakipFormuPolietilenKaplamaDevamIslemlerManager();

		if (isTakipFormuPolietilenKaplamaDevamIslemlerForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		formManager.delete(isTakipFormuPolietilenKaplamaDevamIslemlerForm);
		isTakipFormuPolietilenKaplamaDevamIslemlerForm = new IsTakipFormuPolietilenKaplamaDevamIslemler();
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"POLİETİLEN KAPLAMA İŞ TAKİP FORMU BAŞARIYLA SİLİNMİŞTİR!"));

		fillTestList(selectedPipe.getSalesItem().getItemId());
	}

	// setters getters

	public List<IsTakipFormuPolietilenKaplamaDevamIslemler> getAllIsTakipFormuPolietilenKaplamaDevamIslemlerList() {
		return allIsTakipFormuPolietilenKaplamaDevamIslemlerList;
	}

	public void setAllIsTakipFormuPolietilenKaplamaDevamIslemlerList(
			List<IsTakipFormuPolietilenKaplamaDevamIslemler> allIsTakipFormuPolietilenKaplamaDevamIslemlerList) {
		this.allIsTakipFormuPolietilenKaplamaDevamIslemlerList = allIsTakipFormuPolietilenKaplamaDevamIslemlerList;
	}

	public IsTakipFormuPolietilenKaplamaDevamIslemler getIsTakipFormuPolietilenKaplamaDevamIslemlerForm() {
		return isTakipFormuPolietilenKaplamaDevamIslemlerForm;
	}

	public void setIsTakipFormuPolietilenKaplamaDevamIslemlerForm(
			IsTakipFormuPolietilenKaplamaDevamIslemler isTakipFormuPolietilenKaplamaDevamIslemlerForm) {
		this.isTakipFormuPolietilenKaplamaDevamIslemlerForm = isTakipFormuPolietilenKaplamaDevamIslemlerForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public boolean isButtonRender() {
		return buttonRender;
	}

	public void setButtonRender(boolean buttonRender) {
		this.buttonRender = buttonRender;
	}

	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	public String getBarkodNo() {
		return barkodNo;
	}

	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	public SalesItem getSelectedSalesItem() {
		return selectedSalesItem;
	}

	public void setSelectedSalesItem(SalesItem selectedSalesItem) {
		this.selectedSalesItem = selectedSalesItem;
	}

	public IsTakipFormuPolietilenKaplama getSelectedIsTakipFormuPolietilenKaplama() {
		return selectedIsTakipFormuPolietilenKaplama;
	}

	public void setSelectedIsTakipFormuPolietilenKaplama(
			IsTakipFormuPolietilenKaplama selectedIsTakipFormuPolietilenKaplama) {
		this.selectedIsTakipFormuPolietilenKaplama = selectedIsTakipFormuPolietilenKaplama;
	}

	public Integer getSalesItemId() {
		return salesItemId;
	}

	public void setSalesItemId(Integer salesItemId) {
		this.salesItemId = salesItemId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
