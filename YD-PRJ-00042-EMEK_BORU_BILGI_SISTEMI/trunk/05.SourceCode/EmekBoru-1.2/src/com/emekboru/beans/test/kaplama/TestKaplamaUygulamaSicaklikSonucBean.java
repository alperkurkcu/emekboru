package com.emekboru.beans.test.kaplama;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaUygulamaSicaklikSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaUygulamaSicaklikSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaplamaUygulamaSicaklikSonucBean")
@ViewScoped
public class TestKaplamaUygulamaSicaklikSonucBean {

	private TestKaplamaUygulamaSicaklikSonuc testKaplamaUygulamaSicaklikSonucForm = new TestKaplamaUygulamaSicaklikSonuc();
	private List<TestKaplamaUygulamaSicaklikSonuc> allTestKaplamaUygulamaSicaklikSonucList = new ArrayList<TestKaplamaUygulamaSicaklikSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateTestKaplamaUygulamaSicaklikSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaUygulamaSicaklikSonucManager tkussManager = new TestKaplamaUygulamaSicaklikSonucManager();

		if (testKaplamaUygulamaSicaklikSonucForm.getId() == null) {

			testKaplamaUygulamaSicaklikSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaUygulamaSicaklikSonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaUygulamaSicaklikSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaUygulamaSicaklikSonucForm.setBagliTestId(bagliTest);
			testKaplamaUygulamaSicaklikSonucForm.setBagliGlobalId(bagliTest);

			tkussManager.enterNew(testKaplamaUygulamaSicaklikSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaUygulamaSicaklikSonucForm
					.setGuncellemeZamani(new java.util.Date());
			testKaplamaUygulamaSicaklikSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			tkussManager.updateEntity(testKaplamaUygulamaSicaklikSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addTestKaplamaUygulamaSicaklikSonuc() {

		testKaplamaUygulamaSicaklikSonucForm = new TestKaplamaUygulamaSicaklikSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaUygulamaSicaklikSonucListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaUygulamaSicaklikSonucList = TestKaplamaUygulamaSicaklikSonucManager
				.getAllTestKaplamaUygulamaSicaklikSonuc(globalId, pipeId);
		testKaplamaUygulamaSicaklikSonucForm = new TestKaplamaUygulamaSicaklikSonuc();
	}

	public void deleteTestKaplamaUygulamaSicaklikSonuc() {

		bagliTestId = testKaplamaUygulamaSicaklikSonucForm.getBagliGlobalId()
				.getId();

		if (testKaplamaUygulamaSicaklikSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaUygulamaSicaklikSonucManager tkussManager = new TestKaplamaUygulamaSicaklikSonucManager();
		tkussManager.delete(testKaplamaUygulamaSicaklikSonucForm);
		testKaplamaUygulamaSicaklikSonucForm = new TestKaplamaUygulamaSicaklikSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters
	public TestKaplamaUygulamaSicaklikSonuc getTestKaplamaUygulamaSicaklikSonucForm() {
		return testKaplamaUygulamaSicaklikSonucForm;
	}

	public void setTestKaplamaUygulamaSicaklikSonucForm(
			TestKaplamaUygulamaSicaklikSonuc testKaplamaUygulamaSicaklikSonucForm) {
		this.testKaplamaUygulamaSicaklikSonucForm = testKaplamaUygulamaSicaklikSonucForm;
	}

	public List<TestKaplamaUygulamaSicaklikSonuc> getAllTestKaplamaUygulamaSicaklikSonucList() {
		return allTestKaplamaUygulamaSicaklikSonucList;
	}

	public void setAllTestKaplamaUygulamaSicaklikSonucList(
			List<TestKaplamaUygulamaSicaklikSonuc> allTestKaplamaUygulamaSicaklikSonucList) {
		this.allTestKaplamaUygulamaSicaklikSonucList = allTestKaplamaUygulamaSicaklikSonucList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
