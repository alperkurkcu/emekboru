/**
 * 
 */
package com.emekboru.beans.istakipformu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.istakipformu.IsTakipFormuEpoksiKaplamaOnceIslemler;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isTakipFormuEpoksiKaplamaOnceIslemlerBean")
@ViewScoped
public class IsTakipFormuEpoksiKaplamaOnceIslemlerBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<IsTakipFormuEpoksiKaplamaOnceIslemler> allIsTakipFormuEpoksiKaplamaOnceIslemlerList = new ArrayList<IsTakipFormuEpoksiKaplamaOnceIslemler>();
	private IsTakipFormuEpoksiKaplamaOnceIslemler isTakipFormuEpoksiKaplamaOnceIslemlerForm = new IsTakipFormuEpoksiKaplamaOnceIslemler();

	// setters getters
	public List<IsTakipFormuEpoksiKaplamaOnceIslemler> getAllIsTakipFormuEpoksiKaplamaOnceIslemlerList() {
		return allIsTakipFormuEpoksiKaplamaOnceIslemlerList;
	}

	public void setAllIsTakipFormuEpoksiKaplamaOnceIslemlerList(
			List<IsTakipFormuEpoksiKaplamaOnceIslemler> allIsTakipFormuEpoksiKaplamaOnceIslemlerList) {
		this.allIsTakipFormuEpoksiKaplamaOnceIslemlerList = allIsTakipFormuEpoksiKaplamaOnceIslemlerList;
	}

	public IsTakipFormuEpoksiKaplamaOnceIslemler getIsTakipFormuEpoksiKaplamaOnceIslemlerForm() {
		return isTakipFormuEpoksiKaplamaOnceIslemlerForm;
	}

	public void setIsTakipFormuEpoksiKaplamaOnceIslemlerForm(
			IsTakipFormuEpoksiKaplamaOnceIslemler isTakipFormuEpoksiKaplamaOnceIslemlerForm) {
		this.isTakipFormuEpoksiKaplamaOnceIslemlerForm = isTakipFormuEpoksiKaplamaOnceIslemlerForm;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
