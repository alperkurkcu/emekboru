/**
 * 
 */
package com.emekboru.beans.istakipformu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.istakipformu.IsTakipFormuImalat;
import com.emekboru.jpa.istakipformu.IsTakipFormuImalatBitinceIslemler;
import com.emekboru.jpa.istakipformu.IsTakipFormuImalatOnceIslemler;
import com.emekboru.jpa.istakipformu.IsTakipFormuImalatSonraIslemler;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.istakipformu.IsTakipFormuImalatBitinceIslemlerManager;
import com.emekboru.jpaman.istakipformu.IsTakipFormuImalatManager;
import com.emekboru.jpaman.istakipformu.IsTakipFormuImalatOnceIslemlerManager;
import com.emekboru.jpaman.istakipformu.IsTakipFormuImalatSonraIslemlerManager;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isTakipFormuImalatBean")
@ViewScoped
public class IsTakipFormuImalatBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private SalesItem selectedItem = new SalesItem();

	private List<IsTakipFormuImalat> allIsTakipFormuImalatList = new ArrayList<IsTakipFormuImalat>();
	private IsTakipFormuImalat isTakipFormuImalatForm = new IsTakipFormuImalat();

	private IsTakipFormuImalat newIsTakip = new IsTakipFormuImalat();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private IsTakipFormuImalatOnceIslemler selectedIsTakipFormuImalatOnceIslemler = new IsTakipFormuImalatOnceIslemler();
	private IsTakipFormuImalatSonraIslemler selectedIsTakipFormuImalatSonraIslemler = new IsTakipFormuImalatSonraIslemler();
	private IsTakipFormuImalatBitinceIslemler selectedIsTakipFormuImalatBitinceIslemler = new IsTakipFormuImalatBitinceIslemler();
	private IsTakipFormuImalatOnceIslemler newIsTakipFormuImalatOnceIslemler = new IsTakipFormuImalatOnceIslemler();
	private IsTakipFormuImalatSonraIslemler newIsTakipFormuImalatSonraIslemler = new IsTakipFormuImalatSonraIslemler();
	private IsTakipFormuImalatBitinceIslemler newIsTakipFormuImalatBitinceIslemler = new IsTakipFormuImalatBitinceIslemler();
	private IsTakipFormuImalat selectedIsTakipFormuImalat = new IsTakipFormuImalat();

	public void addFromIsEmri(SalesItem salesItem) {

		System.out.println("isTakipFormuImalatBean.addFromIsEmri()");

		try {
			IsTakipFormuImalatManager manager = new IsTakipFormuImalatManager();

			newIsTakip.setSalesItem(salesItem);
			newIsTakip.setEklemeZamani(UtilInsCore.getTarihZaman());
			newIsTakip.setEkleyenEmployee(userBean.getUser());

			newIsTakip.getIsTakipFormuImalatOnceIslemler().setEklemeZamani(
					UtilInsCore.getTarihZaman());
			newIsTakip.getIsTakipFormuImalatOnceIslemler().setEkleyenEmployee(
					userBean.getUser());

			newIsTakip.getIsTakipFormuImalatSonraIslemler().setEklemeZamani(
					UtilInsCore.getTarihZaman());
			newIsTakip.getIsTakipFormuImalatSonraIslemler().setEkleyenEmployee(
					userBean.getUser());

			newIsTakip.getIsTakipFormuImalatBitinceIslemler().setEklemeZamani(
					UtilInsCore.getTarihZaman());
			newIsTakip.getIsTakipFormuImalatBitinceIslemler()
					.setEkleyenEmployee(userBean.getUser());

			manager.enterNew(newIsTakip);
			newIsTakip = new IsTakipFormuImalat();

			FacesContext context = FacesContext.getCurrentInstance();

			context.addMessage(null, new FacesMessage(
					"İMALAT İŞ TAKİP FORMU EKLENMİŞTİR!"));

		} catch (Exception e) {
			System.out.println("isTakipFormuImalatBean.addFromIsEmri-HATA");
		}
	}

	public void loadSelectedTakipForm(Integer icDis) {

		reset();
		loadSelectedIsTakipFormuImalatOnceIslemler();
		loadSelectedIsTakipFormuImalatSonraIslemler();
		loadSelectedIsTakipFormuImalatBitinceIslemler();
	}

	@SuppressWarnings("static-access")
	public void loadReplySelectedTakipForm() {
		IsTakipFormuImalatManager takipManager = new IsTakipFormuImalatManager();
		if (takipManager.getAllIsTakipFormuImalat(selectedItem.getItemId())
				.size() > 0) {
			selectedIsTakipFormuImalat = takipManager.getAllIsTakipFormuImalat(
					selectedItem.getItemId()).get(0);
		} else {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"İMALAT İŞ EMRİ EKLENMEMİŞ, LÜTFEN ÖNCE İŞ EMİRLERİNİ EKLEYİNİZ!",
							null));
			return;
		}
	}

	public void reset() {

		selectedIsTakipFormuImalatOnceIslemler = new IsTakipFormuImalatOnceIslemler();
		selectedIsTakipFormuImalatSonraIslemler = new IsTakipFormuImalatSonraIslemler();
		selectedIsTakipFormuImalatBitinceIslemler = new IsTakipFormuImalatBitinceIslemler();
	}

	public void loadSelectedIsTakipFormuImalatOnceIslemler() {
		loadReplySelectedTakipForm();
		selectedIsTakipFormuImalatOnceIslemler = selectedIsTakipFormuImalat
				.getIsTakipFormuImalatOnceIslemler();
	}

	public void loadSelectedIsTakipFormuImalatSonraIslemler() {
		loadReplySelectedTakipForm();
		selectedIsTakipFormuImalatSonraIslemler = selectedIsTakipFormuImalat
				.getIsTakipFormuImalatSonraIslemler();
	}

	public void loadSelectedIsTakipFormuImalatBitinceIslemler() {
		loadReplySelectedTakipForm();
		selectedIsTakipFormuImalatBitinceIslemler = selectedIsTakipFormuImalat
				.getIsTakipFormuImalatBitinceIslemler();
	}

	public void updateSelectedIsTakipFormuImalatOnceIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuImalatOnceIslemlerManager onceManager = new IsTakipFormuImalatOnceIslemlerManager();

		selectedIsTakipFormuImalatOnceIslemler.setGuncellemeZamani(UtilInsCore
				.getTarihZaman());
		selectedIsTakipFormuImalatOnceIslemler.setGuncelleyenEmployee(userBean
				.getUser());
		selectedIsTakipFormuImalatOnceIslemler
				.setIsTakipFormuImalat(selectedIsTakipFormuImalat);

		onceManager.updateEntity(selectedIsTakipFormuImalatOnceIslemler);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"İMALAT İŞE BAŞLAMADAN YAPILACAK İŞLEMLER BAŞARIYLA TANIMLANMIŞTIR!"));

	}

	public void updateSelectedIsTakipFormuImalatSonraIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuImalatSonraIslemlerManager sonraManager = new IsTakipFormuImalatSonraIslemlerManager();

		selectedIsTakipFormuImalatSonraIslemler.setGuncellemeZamani(UtilInsCore
				.getTarihZaman());
		selectedIsTakipFormuImalatSonraIslemler.setGuncelleyenEmployee(userBean
				.getUser());

		sonraManager.updateEntity(selectedIsTakipFormuImalatSonraIslemler);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"İMALAT AYAR BİTTİKTEN SONRA YAPILACAK İŞLEMLER BAŞARIYLA TANIMLANMIŞTIR!"));

	}

	public void updateSelectedIsTakipFormuImalatBitinceIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuImalatBitinceIslemlerManager bitinceManager = new IsTakipFormuImalatBitinceIslemlerManager();

		selectedIsTakipFormuImalatBitinceIslemler
				.setGuncellemeZamani(UtilInsCore.getTarihZaman());
		selectedIsTakipFormuImalatBitinceIslemler
				.setGuncelleyenEmployee(userBean.getUser());

		bitinceManager.updateEntity(selectedIsTakipFormuImalatBitinceIslemler);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"İMALAT ÜRETİM BİTTİKTEN SONRA YAPILACAK İŞLEMLER BAŞARIYLA TANIMLANMIŞTIR!"));

	}

	public void deleteSelectedIsTakipFormuImalatOnceIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuImalatOnceIslemlerManager onceManager = new IsTakipFormuImalatOnceIslemlerManager();

		newIsTakipFormuImalatOnceIslemler
				.setId(selectedIsTakipFormuImalatOnceIslemler.getId());
		newIsTakipFormuImalatOnceIslemler
				.setEklemeZamani(selectedIsTakipFormuImalatOnceIslemler
						.getEklemeZamani());
		newIsTakipFormuImalatOnceIslemler
				.setEkleyenEmployee(selectedIsTakipFormuImalatOnceIslemler
						.getEkleyenEmployee());

		selectedIsTakipFormuImalatOnceIslemler = new IsTakipFormuImalatOnceIslemler();

		selectedIsTakipFormuImalatOnceIslemler = newIsTakipFormuImalatOnceIslemler;

		onceManager.updateEntity(selectedIsTakipFormuImalatOnceIslemler);

		newIsTakipFormuImalatOnceIslemler = new IsTakipFormuImalatOnceIslemler();
		selectedIsTakipFormuImalatOnceIslemler = new IsTakipFormuImalatOnceIslemler();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"İMALAT İŞE BAŞLAMADAN YAPILACAK İŞLEMLER BAŞARIYLA SİLİNMİŞTİR!"));

	}

	public void deleteSelectedIsTakipFormuImalatSonraIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuImalatSonraIslemlerManager sonraManager = new IsTakipFormuImalatSonraIslemlerManager();

		newIsTakipFormuImalatSonraIslemler
				.setId(selectedIsTakipFormuImalatSonraIslemler.getId());
		newIsTakipFormuImalatSonraIslemler
				.setEklemeZamani(selectedIsTakipFormuImalatSonraIslemler
						.getEklemeZamani());
		newIsTakipFormuImalatSonraIslemler
				.setEkleyenEmployee(selectedIsTakipFormuImalatSonraIslemler
						.getEkleyenEmployee());

		selectedIsTakipFormuImalatSonraIslemler = new IsTakipFormuImalatSonraIslemler();

		selectedIsTakipFormuImalatSonraIslemler = newIsTakipFormuImalatSonraIslemler;

		sonraManager.updateEntity(selectedIsTakipFormuImalatSonraIslemler);

		newIsTakipFormuImalatSonraIslemler = new IsTakipFormuImalatSonraIslemler();
		selectedIsTakipFormuImalatSonraIslemler = new IsTakipFormuImalatSonraIslemler();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"İMALAT AYAR BİTTİKTEN SONRA YAPILACAK İŞLEMLER BAŞARIYLA SİLİNMİŞTİR!"));

	}

	public void deleteSelectedIsTakipFormuImalatBitinceIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuImalatBitinceIslemlerManager bitinceManager = new IsTakipFormuImalatBitinceIslemlerManager();

		newIsTakipFormuImalatBitinceIslemler
				.setId(selectedIsTakipFormuImalatBitinceIslemler.getId());
		newIsTakipFormuImalatBitinceIslemler
				.setEklemeZamani(selectedIsTakipFormuImalatBitinceIslemler
						.getEklemeZamani());
		newIsTakipFormuImalatBitinceIslemler
				.setEkleyenEmployee(selectedIsTakipFormuImalatBitinceIslemler
						.getEkleyenEmployee());

		selectedIsTakipFormuImalatBitinceIslemler = new IsTakipFormuImalatBitinceIslemler();

		selectedIsTakipFormuImalatBitinceIslemler = newIsTakipFormuImalatBitinceIslemler;

		bitinceManager.updateEntity(selectedIsTakipFormuImalatBitinceIslemler);

		newIsTakipFormuImalatBitinceIslemler = new IsTakipFormuImalatBitinceIslemler();
		selectedIsTakipFormuImalatBitinceIslemler = new IsTakipFormuImalatBitinceIslemler();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"İMALAT ÜRETİM BİTİNCE YAPILACAK İŞLEMLER BAŞARIYLA SİLİNMİŞTİR!"));

	}

	// setters getters

	/**
	 * @return the selectedItem
	 */
	public SalesItem getSelectedItem() {
		return selectedItem;
	}

	/**
	 * @param selectedItem
	 *            the selectedItem to set
	 */
	public void setSelectedItem(SalesItem selectedItem) {
		this.selectedItem = selectedItem;
	}

	/**
	 * @return the allIsTakipFormuImalatList
	 */
	public List<IsTakipFormuImalat> getAllIsTakipFormuImalatList() {
		return allIsTakipFormuImalatList;
	}

	/**
	 * @param allIsTakipFormuImalatList
	 *            the allIsTakipFormuImalatList to set
	 */
	public void setAllIsTakipFormuImalatList(
			List<IsTakipFormuImalat> allIsTakipFormuImalatList) {
		this.allIsTakipFormuImalatList = allIsTakipFormuImalatList;
	}

	/**
	 * @return the isTakipFormuImalatForm
	 */
	public IsTakipFormuImalat getIsTakipFormuImalatForm() {
		return isTakipFormuImalatForm;
	}

	/**
	 * @param isTakipFormuImalatForm
	 *            the isTakipFormuImalatForm to set
	 */
	public void setIsTakipFormuImalatForm(
			IsTakipFormuImalat isTakipFormuImalatForm) {
		this.isTakipFormuImalatForm = isTakipFormuImalatForm;
	}

	/**
	 * @return the newIsTakip
	 */
	public IsTakipFormuImalat getNewIsTakip() {
		return newIsTakip;
	}

	/**
	 * @param newIsTakip
	 *            the newIsTakip to set
	 */
	public void setNewIsTakip(IsTakipFormuImalat newIsTakip) {
		this.newIsTakip = newIsTakip;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the selectedIsTakipFormuImalatOnceIslemler
	 */
	public IsTakipFormuImalatOnceIslemler getSelectedIsTakipFormuImalatOnceIslemler() {
		return selectedIsTakipFormuImalatOnceIslemler;
	}

	/**
	 * @param selectedIsTakipFormuImalatOnceIslemler
	 *            the selectedIsTakipFormuImalatOnceIslemler to set
	 */
	public void setSelectedIsTakipFormuImalatOnceIslemler(
			IsTakipFormuImalatOnceIslemler selectedIsTakipFormuImalatOnceIslemler) {
		this.selectedIsTakipFormuImalatOnceIslemler = selectedIsTakipFormuImalatOnceIslemler;
	}

	/**
	 * @return the selectedIsTakipFormuImalatSonraIslemler
	 */
	public IsTakipFormuImalatSonraIslemler getSelectedIsTakipFormuImalatSonraIslemler() {
		return selectedIsTakipFormuImalatSonraIslemler;
	}

	/**
	 * @param selectedIsTakipFormuImalatSonraIslemler
	 *            the selectedIsTakipFormuImalatSonraIslemler to set
	 */
	public void setSelectedIsTakipFormuImalatSonraIslemler(
			IsTakipFormuImalatSonraIslemler selectedIsTakipFormuImalatSonraIslemler) {
		this.selectedIsTakipFormuImalatSonraIslemler = selectedIsTakipFormuImalatSonraIslemler;
	}

	/**
	 * @return the selectedIsTakipFormuImalatBitinceIslemler
	 */
	public IsTakipFormuImalatBitinceIslemler getSelectedIsTakipFormuImalatBitinceIslemler() {
		return selectedIsTakipFormuImalatBitinceIslemler;
	}

	/**
	 * @param selectedIsTakipFormuImalatBitinceIslemler
	 *            the selectedIsTakipFormuImalatBitinceIslemler to set
	 */
	public void setSelectedIsTakipFormuImalatBitinceIslemler(
			IsTakipFormuImalatBitinceIslemler selectedIsTakipFormuImalatBitinceIslemler) {
		this.selectedIsTakipFormuImalatBitinceIslemler = selectedIsTakipFormuImalatBitinceIslemler;
	}

	/**
	 * @return the newIsTakipFormuImalatOnceIslemler
	 */
	public IsTakipFormuImalatOnceIslemler getNewIsTakipFormuImalatOnceIslemler() {
		return newIsTakipFormuImalatOnceIslemler;
	}

	/**
	 * @param newIsTakipFormuImalatOnceIslemler
	 *            the newIsTakipFormuImalatOnceIslemler to set
	 */
	public void setNewIsTakipFormuImalatOnceIslemler(
			IsTakipFormuImalatOnceIslemler newIsTakipFormuImalatOnceIslemler) {
		this.newIsTakipFormuImalatOnceIslemler = newIsTakipFormuImalatOnceIslemler;
	}

	/**
	 * @return the newIsTakipFormuImalatSonraIslemler
	 */
	public IsTakipFormuImalatSonraIslemler getNewIsTakipFormuImalatSonraIslemler() {
		return newIsTakipFormuImalatSonraIslemler;
	}

	/**
	 * @param newIsTakipFormuImalatSonraIslemler
	 *            the newIsTakipFormuImalatSonraIslemler to set
	 */
	public void setNewIsTakipFormuImalatSonraIslemler(
			IsTakipFormuImalatSonraIslemler newIsTakipFormuImalatSonraIslemler) {
		this.newIsTakipFormuImalatSonraIslemler = newIsTakipFormuImalatSonraIslemler;
	}

	/**
	 * @return the newIsTakipFormuImalatBitinceIslemler
	 */
	public IsTakipFormuImalatBitinceIslemler getNewIsTakipFormuImalatBitinceIslemler() {
		return newIsTakipFormuImalatBitinceIslemler;
	}

	/**
	 * @param newIsTakipFormuImalatBitinceIslemler
	 *            the newIsTakipFormuImalatBitinceIslemler to set
	 */
	public void setNewIsTakipFormuImalatBitinceIslemler(
			IsTakipFormuImalatBitinceIslemler newIsTakipFormuImalatBitinceIslemler) {
		this.newIsTakipFormuImalatBitinceIslemler = newIsTakipFormuImalatBitinceIslemler;
	}

	/**
	 * @return the selectedIsTakipFormuImalat
	 */
	public IsTakipFormuImalat getSelectedIsTakipFormuImalat() {
		return selectedIsTakipFormuImalat;
	}

	/**
	 * @param selectedIsTakipFormuImalat
	 *            the selectedIsTakipFormuImalat to set
	 */
	public void setSelectedIsTakipFormuImalat(
			IsTakipFormuImalat selectedIsTakipFormuImalat) {
		this.selectedIsTakipFormuImalat = selectedIsTakipFormuImalat;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
