package com.emekboru.beans.test.kaplama;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaUygulamaSartlariSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaUygulamaSartlariSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaplamaUygulamaSartlariSonucBean")
@ViewScoped
public class TestKaplamaUygulamaSartlariSonucBean {

	private TestKaplamaUygulamaSartlariSonuc testKaplamaUygulamaSartlariSonucForm = new TestKaplamaUygulamaSartlariSonuc();
	private List<TestKaplamaUygulamaSartlariSonuc> allTestKaplamaUygulamaSartlariSonucList = new ArrayList<TestKaplamaUygulamaSartlariSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateTestKaplamaUygulamaSartlariSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaUygulamaSartlariSonucManager manager = new TestKaplamaUygulamaSartlariSonucManager();

		if (testKaplamaUygulamaSartlariSonucForm.getId() == null) {

			testKaplamaUygulamaSartlariSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaUygulamaSartlariSonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaUygulamaSartlariSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaUygulamaSartlariSonucForm.setBagliTestId(bagliTest);
			testKaplamaUygulamaSartlariSonucForm.setBagliGlobalId(bagliTest);

			manager.enterNew(testKaplamaUygulamaSartlariSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaUygulamaSartlariSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaUygulamaSartlariSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			manager.updateEntity(testKaplamaUygulamaSartlariSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addTestKaplamaUygulamaSartlariSonuc() {

		testKaplamaUygulamaSartlariSonucForm = new TestKaplamaUygulamaSartlariSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaUygulamaSartlariSonucListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaUygulamaSartlariSonucList = TestKaplamaUygulamaSartlariSonucManager
				.getAllTestKaplamaUygulamaSartlariSonuc(globalId, pipeId);
		testKaplamaUygulamaSartlariSonucForm = new TestKaplamaUygulamaSartlariSonuc();
	}

	public void deleteTestKaplamaUygulamaSartlariSonuc() {

		bagliTestId = testKaplamaUygulamaSartlariSonucForm.getBagliGlobalId()
				.getId();

		if (testKaplamaUygulamaSartlariSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaUygulamaSartlariSonucManager manager = new TestKaplamaUygulamaSartlariSonucManager();
		manager.delete(testKaplamaUygulamaSartlariSonucForm);
		testKaplamaUygulamaSartlariSonucForm = new TestKaplamaUygulamaSartlariSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters
	public TestKaplamaUygulamaSartlariSonuc getTestKaplamaUygulamaSartlariSonucForm() {
		return testKaplamaUygulamaSartlariSonucForm;
	}

	public void setTestKaplamaUygulamaSartlariSonucForm(
			TestKaplamaUygulamaSartlariSonuc testKaplamaUygulamaSartlariSonucForm) {
		this.testKaplamaUygulamaSartlariSonucForm = testKaplamaUygulamaSartlariSonucForm;
	}

	public List<TestKaplamaUygulamaSartlariSonuc> getAllTestKaplamaUygulamaSartlariSonucList() {
		return allTestKaplamaUygulamaSartlariSonucList;
	}

	public void setAllTestKaplamaUygulamaSartlariSonucList(
			List<TestKaplamaUygulamaSartlariSonuc> allTestKaplamaUygulamaSartlariSonucList) {
		this.allTestKaplamaUygulamaSartlariSonucList = allTestKaplamaUygulamaSartlariSonucList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
