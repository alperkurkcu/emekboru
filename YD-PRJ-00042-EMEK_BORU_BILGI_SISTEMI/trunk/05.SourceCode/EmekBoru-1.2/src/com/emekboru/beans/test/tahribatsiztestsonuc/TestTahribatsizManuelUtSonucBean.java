/**
 * 
 */
package com.emekboru.beans.test.tahribatsiztestsonuc;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.test.tahribatsiz.TahribatsizTestOrderListBean;
import com.emekboru.jpa.NondestructiveTestsSpec;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.kalibrasyon.KalibrasyonManuelUt;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizFloroskopikSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizGozOlcuSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizHidrostatik;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizManuelUtSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizTamir;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizTorna;
import com.emekboru.jpaman.NondestructiveTestsSpecManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.kalibrasyon.KalibrasyonManuelUtManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizFloroskopikSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizGozOlcuSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizHidrostatikManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizKoordinatManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizManuelUtSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizTamirManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizTornaManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */

@ManagedBean(name = "testTahribatsizManuelUtSonucBean")
@ViewScoped
public class TestTahribatsizManuelUtSonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<TestTahribatsizManuelUtSonuc> allTestTahribatsizManuelUtSonucList = new ArrayList<TestTahribatsizManuelUtSonuc>();
	private List<TestTahribatsizManuelUtSonuc> tamireGonderilecekKoordinatlarList = new ArrayList<TestTahribatsizManuelUtSonuc>();
	private TestTahribatsizManuelUtSonuc testTahribatsizManuelUtSonucForm = new TestTahribatsizManuelUtSonuc();

	private TestTahribatsizFloroskopikSonuc testTahribatsizFloroskopikSonucForm = new TestTahribatsizFloroskopikSonuc();

	private List<TestTahribatsizTamir> testTahribatsizTamir = new ArrayList<TestTahribatsizTamir>();

	@ManagedProperty(value = "#{testTahribatsizTamirBean}")
	private TestTahribatsizTamirBean tamirBean;

	@ManagedProperty(value = "#{testTahribatsizTornaBean}")
	private TestTahribatsizTornaBean tornaBean;

	private TestTahribatsizTorna testTahribatsizTorna = new TestTahribatsizTorna();

	@ManagedProperty(value = "#{testTahribatsizHidrostatikBean}")
	private TestTahribatsizHidrostatikBean hidroBean;

	private TestTahribatsizHidrostatik testTahribatsizHidrostatik = new TestTahribatsizHidrostatik();

	@ManagedProperty(value = "#{testTahribatsizGozOlcuSonucBean}")
	private TestTahribatsizGozOlcuSonucBean gozOlcuBean;

	private TestTahribatsizGozOlcuSonuc testTahribatsizGozOlcuSonuc = new TestTahribatsizGozOlcuSonuc();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private boolean visibleButtonRender;

	private Pipe selectedPipe = new Pipe();

	private String barkodNo = null;

	private NondestructiveTestsSpec selectedNonTest = new NondestructiveTestsSpec();

	private Boolean tamirKabulKontrol = false;

	private List<KalibrasyonManuelUt> kalibrasyonlar = new ArrayList<KalibrasyonManuelUt>();
	private KalibrasyonManuelUt kalibrasyon = new KalibrasyonManuelUt();

	Timestamp demin = UtilInsCore.getTarihZaman();

	public static final Integer ManuelStation = 1;
	public static final Integer FloroscopicStation = 2;

	public TestTahribatsizManuelUtSonucBean() {

		tamirBean = new TestTahribatsizTamirBean();
		tornaBean = new TestTahribatsizTornaBean();
		hidroBean = new TestTahribatsizHidrostatikBean();
		gozOlcuBean = new TestTahribatsizGozOlcuSonucBean();
		visibleButtonRender = false;
	}

	@SuppressWarnings("static-access")
	public void addOrUpdateManuelUtSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();

		Integer prmPipeId = null;
		Integer prmItemId = null;
		try {
			prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
					.get("value");

			prmItemId = (Integer) cmd.getChildren().get(1).getAttributes()
					.get("value");
		} catch (Exception e1) {
			System.out
					.println(" testTahribatsizManuelUtSonucBean.addOrUpdateManuelUtSonuc-HATA prmPipeId"
							+ userBean.getUsername());
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL,
					"EKLEME HATASI, TEKRAR DENEYİNİZ!", null));
			return;
		}

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}
		if (prmItemId == null) {
			prmItemId = selectedPipe.getSalesItem().getItemId();
		}

		Integer testId = 0;

		TestTahribatsizManuelUtSonucManager manuelUtSonucManager = new TestTahribatsizManuelUtSonucManager();
		TestTahribatsizFloroskopikSonucManager floroskopiSonucManager = new TestTahribatsizFloroskopikSonucManager();
		NondestructiveTestsSpecManager nonManager = new NondestructiveTestsSpecManager();
		TestTahribatsizKoordinatManager koordinatManager = new TestTahribatsizKoordinatManager();

		try {
			if (testTahribatsizManuelUtSonucForm.getKaynakLaminasyon() == 1) {

				testId = nonManager.findByItemIdGlobalId(prmItemId, 1004)
						.get(0).getTestId();
			} else {

				testId = nonManager.findByItemIdGlobalId(prmItemId, 1004)
						.get(0).getTestId();
			}
		} catch (Exception e1) {
			System.out
					.println(" testTahribatsizManuelUtSonucBean.addOrUpdateManuelUtSonuc-HATA testId"
							+ userBean.getUsername());
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL,
					"EKLEME HATASI, TEKRAR DENEYİNİZ!", null));
			return;
		}

		Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);

		if (testTahribatsizManuelUtSonucForm.getId() == null) {

			// aynı koordinatı eklememe işlemi
			for (int i = 0; i < allTestTahribatsizManuelUtSonucList.size(); i++) {
				if ((testTahribatsizManuelUtSonucForm.getKoordinatQ()
						.equals(allTestTahribatsizManuelUtSonucList.get(i)
								.getKoordinatQ()))
						&& (testTahribatsizManuelUtSonucForm.getKoordinatL()
								.equals(allTestTahribatsizManuelUtSonucList
										.get(i).getKoordinatL()))) {

					FacesContext context = FacesContext.getCurrentInstance();
					context.addMessage(
							null,
							new FacesMessage(
									FacesMessage.SEVERITY_FATAL,
									"AYNI KOORDİNAT TEKRARDAN EKLENEMEZ, KAYDETME HATASI!",
									null));
					return;
				}
			}

			try {

				// manuel UT

				testTahribatsizManuelUtSonucForm.setTestId(testId);
				testTahribatsizManuelUtSonucForm.setPipe(pipe);
				testTahribatsizManuelUtSonucForm.setEklemeZamani(UtilInsCore
						.getTarihZaman());
				testTahribatsizManuelUtSonucForm.setEkleyenEmployee(userBean
						.getUser());

				sonKalibrasyonuGoster();
				testTahribatsizManuelUtSonucForm.setKalibrasyon(kalibrasyon);

				// manuelUt ekleme
				manuelUtSonucManager.enterNew(testTahribatsizManuelUtSonucForm);

				// tüm diğer muayenelere koordinatı ekleme
				koordinatManager.tumTablolaraKoordinatEkleme(
						testTahribatsizManuelUtSonucForm.getKoordinatQ(),
						testTahribatsizManuelUtSonucForm.getKoordinatL(), pipe,
						"m", userBean, testTahribatsizManuelUtSonucForm
								.getKaynakIslemi(),
						testTahribatsizManuelUtSonucForm.getKaynakLaminasyon()
								.toString(), testTahribatsizManuelUtSonucForm
								.getDegerlendirme());

				this.testTahribatsizManuelUtSonucForm = new TestTahribatsizManuelUtSonuc();

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"MANUEL UT VERİSİ BAŞARIYLA EKLENMİŞTİR!"));

			} catch (Exception e2) {

				System.out
						.println(" testTahribatsizManuelUtSonucBean.addOrUpdateManuelUtSonuc-HATA EnterNew"
								+ userBean.getUsername());
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"MANUEL UT VERİSİ EKLENEMEDİ, TEKRAR DENEYİNİZ!"));
				return;
			}

		} else {

			try {

				// tamiri kabul eden kullanıcı verileri
				// (kabul ve seçiniz) ya da (red ve seçiniz)
				if (testTahribatsizManuelUtSonucForm.getTamirSonrasiDurum() != null) {
					if ((testTahribatsizManuelUtSonucForm
							.getTamirSonrasiDurum() == 1 && tamirKabulKontrol)
							|| (testTahribatsizManuelUtSonucForm
									.getTamirSonrasiDurum() == 0 && tamirKabulKontrol)) {
						testTahribatsizManuelUtSonucForm
								.setTamirKabulKullanici(userBean.getUser()
										.getId());
						testTahribatsizManuelUtSonucForm
								.setTamirKabulZamani(UtilInsCore
										.getTarihZaman());

						// // değerlendirme tamir ise
						// if
						// (testTahribatsizManuelUtSonucForm.getDegerlendirme()
						// == 2)
						// {
						//
						// // eger tamir işlemi varsa ve bitmemişse tamir
						// sonrası
						// durum
						// // değiştirilemiyor
						// try {
						// if (testTahribatsizManuelUtSonucForm
						// .getTestTahribatsizTamir().getDurum() != true) {
						//
						// FacesContext context = FacesContext
						// .getCurrentInstance();
						// context.addMessage(
						// null,
						// new FacesMessage(
						// FacesMessage.SEVERITY_FATAL,
						// "TAMİR TAMAMLANMADIĞI İÇİN DEĞİŞİKLİK YAPILAMADI, ÖNCE TAMİRİN BİTİRİLMESİ GEREKMEKTEDİR!",
						// null));
						//
						// testTahribatsizManuelUtSonucForm = new
						// TestTahribatsizManuelUtSonuc();
						//
						// return;
						// }
						// } catch (Exception e1) {
						// // TODO Auto-generated catch block
						// e1.printStackTrace();
						// FacesContext context = FacesContext
						// .getCurrentInstance();
						// context.addMessage(
						// null,
						// new FacesMessage(
						// FacesMessage.SEVERITY_FATAL,
						// "TAMİR TAMAMLANMADIĞI İÇİN DEĞİŞİKLİK YAPILAMADI, ÖNCE TAMİRİN BİTİRİLMESİ GEREKMEKTEDİR!",
						// null));
						//
						// testTahribatsizManuelUtSonucForm = new
						// TestTahribatsizManuelUtSonuc();
						//
						// return;
						// }
						// }
					}
				}

				// manuel ut
				testTahribatsizManuelUtSonucForm
						.setGuncellemeZamani(UtilInsCore.getTarihZaman());
				testTahribatsizManuelUtSonucForm.setEkleyenEmployee(userBean
						.getUser());
				testTahribatsizManuelUtSonucForm.setTestId(testId);

				manuelUtSonucManager
						.updateEntity(testTahribatsizManuelUtSonucForm);

				// eger tamirse flye de koordinat ekliyor
				if (testTahribatsizManuelUtSonucForm.getDegerlendirme() == 2) {

					if (!koordinatManager.coordinateCheck(
							testTahribatsizManuelUtSonucForm.getKoordinatQ(),
							testTahribatsizManuelUtSonucForm.getKoordinatL(),
							2, pipe.getPipeId())) {
						koordinatManager.tumTablolaraKoordinatEkleme(
								testTahribatsizManuelUtSonucForm
										.getKoordinatQ(),
								testTahribatsizManuelUtSonucForm
										.getKoordinatL(), pipe, "m", userBean,
								testTahribatsizManuelUtSonucForm
										.getKaynakIslemi(),
								testTahribatsizManuelUtSonucForm
										.getKaynakLaminasyon().toString(),
								testTahribatsizManuelUtSonucForm
										.getDegerlendirme());
					} else {
						koordinatManager
								.tablolardaDurumGuncelleme(
										testTahribatsizManuelUtSonucForm
												.getKoordinatQ(),
										testTahribatsizManuelUtSonucForm
												.getKoordinatL(), pipe
												.getPipeId(), userBean,
										testTahribatsizManuelUtSonucForm
												.getDegerlendirme(),
										2);
					}

				}

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"MANUEL UT VERİSİ BAŞARIYLA GÜNCELLENMİŞTİR!"));

			} catch (Exception e2) {

				System.err
						.println(" testTahribatsizManuelUtSonucBean.addOrUpdateManuelUtSonuc-HATA Tamir Tamamlanma Kısmı");
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								"MANUEL UT VERİSİ GÜNCELLENEMEMİŞTİR, TEKRAR DENEYİNİZ!"));
				return;
			}

			try {
				// floroskopik verilerini guncelleme
				testTahribatsizFloroskopikSonucForm = floroskopiSonucManager
						.findByKoordinatsPipeId(
								testTahribatsizManuelUtSonucForm
										.getKoordinatQ(),
								testTahribatsizManuelUtSonucForm
										.getKoordinatL(),
								testTahribatsizManuelUtSonucForm.getPipe()
										.getPipeId()).get(0);
				testTahribatsizFloroskopikSonucForm
						.setKaynakIslemi(testTahribatsizManuelUtSonucForm
								.getKaynakIslemi());
				floroskopiSonucManager
						.updateEntity(testTahribatsizFloroskopikSonucForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"FLOROSKOPİK KOORDİNATI GÜNCELLENDİ!"));
			} catch (Exception e2) {

				// testTahribatsizFloroskopikSonucForm = new
				// TestTahribatsizFloroskopikSonuc();
				// testTahribatsizFloroskopikSonucForm
				// .setKoordinatQ(testTahribatsizManuelUtSonucForm
				// .getKoordinatQ());
				// testTahribatsizFloroskopikSonucForm
				// .setKoordinatL(testTahribatsizManuelUtSonucForm
				// .getKoordinatL());
				// testTahribatsizFloroskopikSonucForm
				// .setPipe(testTahribatsizManuelUtSonucForm.getPipe());
				// testTahribatsizFloroskopikSonucForm
				// .setKaynakIslemi(testTahribatsizManuelUtSonucForm
				// .getKaynakIslemi());
				// testTahribatsizFloroskopikSonucForm.setEklemeZamani(UtilInsCore
				// .getTarihZaman());
				// testTahribatsizFloroskopikSonucForm.setUser(userBean.getUser());
				// floroskopiSonucManager
				// .enterNew(testTahribatsizFloroskopikSonucForm);
				//
				// System.out
				// .println(" testTahribatsizManuelUtSonucBean.addOrUpdateManuelUtSonuc-HATA Floroskopik Yazma kısımı "
				// + userBean.getUsername());
				//
				// FacesContext context = FacesContext.getCurrentInstance();
				// context.addMessage(
				// null,
				// new FacesMessage(
				// FacesMessage.SEVERITY_ERROR,
				// "FLOROSKOPİK KOORDİNATI BULUNAMADI, YENİ KOORDİNAT EKLENDİ!",
				// null));
				// return;
			}

		}

		fillTestList(prmPipeId);

		// sayfada barkoddan okutunca order item pipe seçimi olmadıgı için
		// sıkıntı cıkıyordu
		TahribatsizTestOrderListBean ttolb = new TahribatsizTestOrderListBean();
		ttolb.loadOrderDetails(selectedPipe.getSalesItem().getItemId());
	}

	public void fillTestList(Integer pipeId) {

		try {
			TestTahribatsizManuelUtSonucManager manuelUtSonucManager = new TestTahribatsizManuelUtSonucManager();
			TestTahribatsizTamirManager tamirMan = new TestTahribatsizTamirManager();

			allTestTahribatsizManuelUtSonucList = manuelUtSonucManager
					.getAllTestTahribatsizManuelUtSonuc(pipeId);

			if (allTestTahribatsizManuelUtSonucList.size() > 0) {
				for (int i = 0; i < allTestTahribatsizManuelUtSonucList.size(); i++) {

					testTahribatsizTamir = tamirMan.findByField(
							TestTahribatsizTamir.class,
							"testTahribatsizManuelUtSonuc.id",
							allTestTahribatsizManuelUtSonucList.get(i).getId());

					if (testTahribatsizTamir.size() > 0) {
						allTestTahribatsizManuelUtSonucList.get(i)
								.setTestTahribatsizTamir(
										testTahribatsizTamir.get(0));
					}
				}
			}

			manuelUtSonucManager
					.refreshCollection(allTestTahribatsizManuelUtSonucList);

			// yapılan tamirlerin yenilenmesi için
			for (int x = 0; x < allTestTahribatsizManuelUtSonucList.size(); x++) {
				allTestTahribatsizManuelUtSonucList.get(x)
						.getTestTahribatsizTamir();
			}
		} catch (Exception e) {
			System.out
					.println(" testTahribatsizManuelUtSonucBean.addOrUpdateManuelUtSonuc-HATA fillTestList"
							+ userBean.getUsername());
			return;
		}
	}

	public void addTestTahribatsizManuelUtSonuc() {
		testTahribatsizManuelUtSonucForm = new TestTahribatsizManuelUtSonuc();
		updateButtonRender = false;
	}

	public void manuelUtListener(SelectEvent event) {
		updateButtonRender = true;
	}

	@SuppressWarnings("static-access")
	public void deleteManuelUtSonuc(ActionEvent e) {

		if (testTahribatsizManuelUtSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		TestTahribatsizManuelUtSonucManager manager = new TestTahribatsizManuelUtSonucManager();
		Integer manuelId = manager
				.findByKoordinatsPipeId(
						testTahribatsizManuelUtSonucForm.getKoordinatQ(),
						testTahribatsizManuelUtSonucForm.getKoordinatL(),
						prmPipeId).get(0).getId();

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		TestTahribatsizManuelUtSonucManager manuelUtSonucManager = new TestTahribatsizManuelUtSonucManager();
		TestTahribatsizFloroskopikSonucManager floroskopiSonucManager = new TestTahribatsizFloroskopikSonucManager();
		TestTahribatsizTamirManager testTahribatsizTamirManager = new TestTahribatsizTamirManager();

		// tamir kontrol
		if (testTahribatsizTamirManager.tamirKontrol(prmPipeId, manuelId)) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"KOORDİNATA BAĞLI TAMİR VERİLERİ VARDIR, SİLME İŞLEMİ BAŞARISIZ!",
							null));
			return;
		}

		manuelUtSonucManager.delete(testTahribatsizManuelUtSonucForm);

		if (floroskopiSonucManager.findByKoordinatsPipeId(
				testTahribatsizManuelUtSonucForm.getKoordinatQ(),
				testTahribatsizManuelUtSonucForm.getKoordinatL(),
				testTahribatsizManuelUtSonucForm.getPipe().getPipeId()).size() > 0) {
			testTahribatsizFloroskopikSonucForm = floroskopiSonucManager
					.findByKoordinatsPipeId(
							testTahribatsizManuelUtSonucForm.getKoordinatQ(),
							testTahribatsizManuelUtSonucForm.getKoordinatL(),
							testTahribatsizManuelUtSonucForm.getPipe()
									.getPipeId()).get(0);
			floroskopiSonucManager.delete(testTahribatsizFloroskopikSonucForm);
		}

		testTahribatsizManuelUtSonucForm = new TestTahribatsizManuelUtSonuc();
		testTahribatsizFloroskopikSonucForm = new TestTahribatsizFloroskopikSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");

		fillTestList(prmPipeId);
	}

	public void detayGoster() {

		try {
			testTahribatsizFloroskopikSonucForm = new TestTahribatsizFloroskopikSonuc();
			TestTahribatsizFloroskopikSonucManager fm = new TestTahribatsizFloroskopikSonucManager();
			testTahribatsizFloroskopikSonucForm = fm.findByKoordinatsPipeId(
					testTahribatsizManuelUtSonucForm.getKoordinatQ(),
					testTahribatsizManuelUtSonucForm.getKoordinatL(),
					testTahribatsizManuelUtSonucForm.getPipe().getPipeId())
					.get(0);
		} catch (Exception e) {

			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"SEÇİLİ KORDİNAT FLOROSKOPİK TESTLERDE BULUNAMAMIŞTIR, TEKRAR DENEYİNİZ!",
							null));
		}
	}

	// public void tamir() {
	//
	// int index = 0;
	// Boolean kontrol = false;
	// // tamirBean = new TestTahribatsizTamirBean();
	// while (allTestTahribatsizManuelUtSonucList.size() > index) {
	// kontrol = TestTahribatsizTamirManager.tamirKontrol(
	// allTestTahribatsizManuelUtSonucList.get(index).getPipe()
	// .getPipeId(), allTestTahribatsizManuelUtSonucList
	// .get(index).getId());
	// if (kontrol) {
	// break;
	// }
	// index++;
	// }
	// if (!kontrol) {
	// tamirBean.addFromManuel(allTestTahribatsizManuelUtSonucList,
	// userBean);
	// fillTestList(allTestTahribatsizManuelUtSonucList.get(0).getPipe()
	// .getPipeId());
	// } else {
	//
	// FacesContext context = FacesContext.getCurrentInstance();
	// context.addMessage(null, new FacesMessage(
	// FacesMessage.SEVERITY_ERROR,
	// "GÖNDERİLECEK TAMİRLER ÖNCEDEN EKLENMİŞ!", null));
	// }
	// }

	public void tamir() {

		int index = 0;
		Boolean kontrol = false;
		tamireGonderilecekKoordinatlarList = new ArrayList<TestTahribatsizManuelUtSonuc>();

		// tamirBean = new TestTahribatsizTamirBean();
		while (allTestTahribatsizManuelUtSonucList.size() > index) {
			TestTahribatsizTamirManager testTahribatsizTamirManager = new TestTahribatsizTamirManager();
			kontrol = testTahribatsizTamirManager.tamirKontrol(
					allTestTahribatsizManuelUtSonucList.get(index).getPipe()
							.getPipeId(), allTestTahribatsizManuelUtSonucList
							.get(index).getId());
			if (!kontrol) {// koordinat tamirde değilse listeye ekliyor
				tamireGonderilecekKoordinatlarList
						.add(allTestTahribatsizManuelUtSonucList.get(index));
			}
			index++;
		}
		if (tamireGonderilecekKoordinatlarList.size() > 0) {
			tamirBean.addFromManuel(tamireGonderilecekKoordinatlarList,
					userBean);
			fillTestList(allTestTahribatsizManuelUtSonucList.get(0).getPipe()
					.getPipeId());
		} else {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"GÖNDERİLECEK TAMİRLER ÖNCEDEN EKLENMİŞ!", null));
		}
	}

	public void torna() {

		int index = 0;
		Boolean kontrol = false;
		// tamirBean = new TestTahribatsizTamirBean();
		while (allTestTahribatsizManuelUtSonucList.size() > index) {
			TestTahribatsizTornaManager manager = new TestTahribatsizTornaManager();
			if (manager.getAllTestTahribatsizTorna(
					allTestTahribatsizManuelUtSonucList.get(index).getPipe()
							.getPipeId()).size() > 0) {

				kontrol = true;
			}
			if (kontrol) {
				break;
			}
			index++;
		}
		if (!kontrol) {
			tornaBean.addFromManuel(allTestTahribatsizManuelUtSonucList,
					userBean);
			fillTestList(allTestTahribatsizManuelUtSonucList.get(0).getPipe()
					.getPipeId());
		} else {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"GÖNDERİLECEK TORNA ÖNCEDEN EKLENMİŞ!", null));
		}
	}

	public void hidrostatik() {

		int index = 0;
		Boolean kontrol = false;
		TestTahribatsizHidrostatikManager manager = new TestTahribatsizHidrostatikManager();
		// tamirBean = new TestTahribatsizTamirBean();
		while (allTestTahribatsizManuelUtSonucList.size() > index) {
			if (manager.getAllTestTahribatsizHidrostatik(
					allTestTahribatsizManuelUtSonucList.get(index).getPipe()
							.getPipeId()).size() > 0) {
				kontrol = true;
			}
			if (kontrol) {
				break;
			}
			index++;
		}
		if (!kontrol) {
			hidroBean.addFromManuel(allTestTahribatsizManuelUtSonucList,
					userBean);
			fillTestList(allTestTahribatsizManuelUtSonucList.get(0).getPipe()
					.getPipeId());
		} else {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"GÖNDERİLECEK HİDROSTATİK ÖNCEDEN EKLENMİŞ!", null));
		}
	}

	public void gozOlcu() {

		int index = 0;
		Boolean kontrol = false;
		// tamirBean = new TestTahribatsizTamirBean();
		TestTahribatsizGozOlcuSonucManager manager = new TestTahribatsizGozOlcuSonucManager();
		while (allTestTahribatsizManuelUtSonucList.size() > index) {
			if (manager.getAllTestTahribatsizGozOlcuSonuc(
					allTestTahribatsizManuelUtSonucList.get(index).getPipe()
							.getPipeId()).size() > 0) {

				kontrol = true;
			}

			if (kontrol) {
				break;
			}
			index++;
		}
		if (!kontrol) {
			gozOlcuBean.addFromManuel(allTestTahribatsizManuelUtSonucList,
					userBean);
			fillTestList(allTestTahribatsizManuelUtSonucList.get(0).getPipe()
					.getPipeId());
		} else {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"GÖNDERİLECEK GÖZ ÖLÇÜ ÖNCEDEN EKLENMİŞ!", null));
		}
	}

	@SuppressWarnings("static-access")
	public void kaliteGoster() {

		NondestructiveTestsSpecManager ndtsm = new NondestructiveTestsSpecManager();
		if (ndtsm.findByItemIdGlobalId(selectedPipe.getSalesItem().getItemId(),
				1001).size() > 0) {
			selectedNonTest = ndtsm.findByItemIdGlobalId(
					selectedPipe.getSalesItem().getItemId(), 1001).get(0);
		} else {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"İŞ EMRİ EKLENMEMİŞ, LÜTFEN EKLETİNİZ!", null));
			return;
		}
	}

	public void durumGoster() {

		TestTahribatsizTornaManager tm = new TestTahribatsizTornaManager();
		TestTahribatsizHidrostatikManager hm = new TestTahribatsizHidrostatikManager();
		TestTahribatsizGozOlcuSonucManager gom = new TestTahribatsizGozOlcuSonucManager();

		if (tm.getAllTestTahribatsizTorna(selectedPipe.getPipeId()).size() == 0) {
			testTahribatsizTorna.setDurum(null);
		} else {

			testTahribatsizTorna = tm.getAllTestTahribatsizTorna(
					selectedPipe.getPipeId()).get(0);
		}

		if (hm.getAllTestTahribatsizHidrostatik(selectedPipe.getPipeId())
				.size() == 0) {
			testTahribatsizHidrostatik.setDurum(null);
		} else {

			testTahribatsizHidrostatik = hm.getAllTestTahribatsizHidrostatik(
					selectedPipe.getPipeId()).get(0);
		}

		if (gom.getAllTestTahribatsizGozOlcuSonuc(selectedPipe.getPipeId())
				.size() == 0) {
			testTahribatsizGozOlcuSonuc.setDurum(null);
		} else {

			testTahribatsizGozOlcuSonuc = gom
					.getAllTestTahribatsizGozOlcuSonuc(selectedPipe.getPipeId())
					.get(0);
		}
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		long diff = Math.abs(System.currentTimeMillis() - demin.getTime());

		if (diff < 3000) {// son arama ile şimdiki arama arasında
							// 3 saniyeden az zaman var
			System.out.println(diff);
			return;
		}

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		TestTahribatsizManuelUtSonucManager manuelManager = new TestTahribatsizManuelUtSonucManager();
		TestTahribatsizTamirManager tamirMan = new TestTahribatsizTamirManager();

		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!", null));
			visibleButtonRender = false;
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);

			allTestTahribatsizManuelUtSonucList = manuelManager
					.getAllTestTahribatsizManuelUtSonuc(selectedPipe
							.getPipeId());

			if (allTestTahribatsizManuelUtSonucList.size() > 0) {
				for (int i = 0; i < allTestTahribatsizManuelUtSonucList.size(); i++) {

					testTahribatsizTamir = tamirMan.findByField(
							TestTahribatsizTamir.class,
							"testTahribatsizManuelUtSonuc.id",
							allTestTahribatsizManuelUtSonucList.get(i).getId());

					if (testTahribatsizTamir.size() > 0) {
						allTestTahribatsizManuelUtSonucList.get(i)
								.setTestTahribatsizTamir(
										testTahribatsizTamir.get(0));
					}
				}
			}
			manuelManager
					.refreshCollection(allTestTahribatsizManuelUtSonucList);

			// yapılan tamirlerin yenilenmesi için
			for (int x = 0; x < allTestTahribatsizManuelUtSonucList.size(); x++) {
				allTestTahribatsizManuelUtSonucList.get(x)
						.getTestTahribatsizTamir();
			}

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
			visibleButtonRender = true;
		}

		KalibrasyonManuelUtManager kalibrasyonManager = new KalibrasyonManuelUtManager();
		kalibrasyonlar = kalibrasyonManager.getAllKalibrasyon();
		demin = UtilInsCore.getTarihZaman();
	}

	@SuppressWarnings("static-access")
	public void sonKalibrasyonuGoster() {
		kalibrasyon = new KalibrasyonManuelUt();
		KalibrasyonManuelUtManager kalibrasyonManager = new KalibrasyonManuelUtManager();
		kalibrasyon = kalibrasyonManager.getAllKalibrasyon().get(0);
	}

	public void kalibrasyonGoster() {
		kalibrasyon = new KalibrasyonManuelUt();
		kalibrasyon = testTahribatsizManuelUtSonucForm.getKalibrasyon();
	}

	public void kalibrasyonAta() {
		TestTahribatsizManuelUtSonucManager manuelManager = new TestTahribatsizManuelUtSonucManager();
		try {
			for (int i = 0; i < allTestTahribatsizManuelUtSonucList.size(); i++) {
				allTestTahribatsizManuelUtSonucList.get(i).setKalibrasyon(
						kalibrasyon);
				manuelManager.updateEntity(allTestTahribatsizManuelUtSonucList
						.get(i));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"KALİBRASYON ATAMA BAŞARISIZ, TEKRAR DENEYİNİZ!", null));
			return;
		}
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				"KALİBRASYON ATAMA BAŞARILI!", null));
	}

	// setters getters

	public List<TestTahribatsizManuelUtSonuc> getAllTestTahribatsizManuelUtSonucList() {
		return allTestTahribatsizManuelUtSonucList;
	}

	public void setAllTestTahribatsizManuelUtSonucList(
			List<TestTahribatsizManuelUtSonuc> allTestTahribatsizManuelUtSonucList) {
		this.allTestTahribatsizManuelUtSonucList = allTestTahribatsizManuelUtSonucList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public TestTahribatsizManuelUtSonuc getTestTahribatsizManuelUtSonucForm() {
		return testTahribatsizManuelUtSonucForm;
	}

	public void setTestTahribatsizManuelUtSonucForm(
			TestTahribatsizManuelUtSonuc testTahribatsizManuelUtSonucForm) {
		this.testTahribatsizManuelUtSonucForm = testTahribatsizManuelUtSonucForm;

		// tamirKontrol
		if (testTahribatsizManuelUtSonucForm.getTamirSonrasiDurum() != null
				&& testTahribatsizManuelUtSonucForm.getTamirSonrasiDurum() == -1) {
			tamirKabulKontrol = true;
		} else {
			tamirKabulKontrol = false;
		}

	}

	public TestTahribatsizFloroskopikSonuc getTestTahribatsizFloroskopikSonucForm() {
		return testTahribatsizFloroskopikSonucForm;
	}

	public void setTestTahribatsizFloroskopikSonucForm(
			TestTahribatsizFloroskopikSonuc testTahribatsizFloroskopikSonucForm) {
		this.testTahribatsizFloroskopikSonucForm = testTahribatsizFloroskopikSonucForm;
	}

	public TestTahribatsizTamirBean getTamirBean() {
		return tamirBean;
	}

	public void setTamirBean(TestTahribatsizTamirBean tamirBean) {
		this.tamirBean = tamirBean;
	}

	public TestTahribatsizTornaBean getTornaBean() {
		return tornaBean;
	}

	public void setTornaBean(TestTahribatsizTornaBean tornaBean) {
		this.tornaBean = tornaBean;
	}

	public TestTahribatsizHidrostatikBean getHidroBean() {
		return hidroBean;
	}

	public void setHidroBean(TestTahribatsizHidrostatikBean hidroBean) {
		this.hidroBean = hidroBean;
	}

	public TestTahribatsizGozOlcuSonucBean getGozOlcuBean() {
		return gozOlcuBean;
	}

	public void setGozOlcuBean(TestTahribatsizGozOlcuSonucBean gozOlcuBean) {
		this.gozOlcuBean = gozOlcuBean;
	}

	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	public String getBarkodNo() {
		return barkodNo;
	}

	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	public NondestructiveTestsSpec getSelectedNonTest() {
		return selectedNonTest;
	}

	public void setSelectedNonTest(NondestructiveTestsSpec selectedNonTest) {
		this.selectedNonTest = selectedNonTest;
	}

	public TestTahribatsizHidrostatik getTestTahribatsizHidrostatik() {
		return testTahribatsizHidrostatik;
	}

	public void setTestTahribatsizHidrostatik(
			TestTahribatsizHidrostatik testTahribatsizHidrostatik) {
		this.testTahribatsizHidrostatik = testTahribatsizHidrostatik;
	}

	public TestTahribatsizGozOlcuSonuc getTestTahribatsizGozOlcuSonuc() {
		return testTahribatsizGozOlcuSonuc;
	}

	public void setTestTahribatsizGozOlcuSonuc(
			TestTahribatsizGozOlcuSonuc testTahribatsizGozOlcuSonuc) {
		this.testTahribatsizGozOlcuSonuc = testTahribatsizGozOlcuSonuc;
	}

	public TestTahribatsizTorna getTestTahribatsizTorna() {
		return testTahribatsizTorna;
	}

	public void setTestTahribatsizTorna(
			TestTahribatsizTorna testTahribatsizTorna) {
		this.testTahribatsizTorna = testTahribatsizTorna;
	}

	public boolean isVisibleButtonRender() {
		return visibleButtonRender;
	}

	/**
	 * @return the kalibrasyonlar
	 */
	public List<KalibrasyonManuelUt> getKalibrasyonlar() {
		return kalibrasyonlar;
	}

	/**
	 * @param kalibrasyonlar
	 *            the kalibrasyonlar to set
	 */
	public void setKalibrasyonlar(List<KalibrasyonManuelUt> kalibrasyonlar) {
		this.kalibrasyonlar = kalibrasyonlar;
	}

	/**
	 * @return the kalibrasyon
	 */
	public KalibrasyonManuelUt getKalibrasyon() {
		return kalibrasyon;
	}

	/**
	 * @param kalibrasyon
	 *            the kalibrasyon to set
	 */
	public void setKalibrasyon(KalibrasyonManuelUt kalibrasyon) {
		this.kalibrasyon = kalibrasyon;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
