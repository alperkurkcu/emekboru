/**
 * 
 */
package com.emekboru.beans.test.tahribatsiztestsonuc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizFloroskopikSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizManuelUtSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizTorna;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizGozOlcuSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizHidrostatikManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizTornaManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */

@ManagedBean(name = "testTahribatsizTornaBean")
@ViewScoped
public class TestTahribatsizTornaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<TestTahribatsizTorna> allTestTahribatsizTornaList = new ArrayList<TestTahribatsizTorna>();
	private TestTahribatsizTorna testTahribatsizTornaForm = new TestTahribatsizTorna();

	private TestTahribatsizTorna newTorna = new TestTahribatsizTorna();
	private List<TestTahribatsizTorna> tornaList;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	int index = 0;

	private boolean updateButtonRender;

	private Pipe selectedPipe = new Pipe();

	private String barkodNo = null;

	@ManagedProperty(value = "#{testTahribatsizGozOlcuSonucBean}")
	private TestTahribatsizGozOlcuSonucBean gozOlcuBean;

	private boolean kontrol = false;

	public TestTahribatsizTornaBean() {

		gozOlcuBean = new TestTahribatsizGozOlcuSonucBean();
	}

	// tornaya gonderme kısmı
	public void addFromManuel(
			List<TestTahribatsizManuelUtSonuc> allTestTahribatsizManuelUtSonucList,
			UserCredentialsBean userBean) {
		System.out.println("TestTahribatsizTornaBean.add()");

		newTorna.setTestTahribatsizManuelUtSonuc(new TestTahribatsizManuelUtSonuc());

		try {
			TestTahribatsizTornaManager manager = new TestTahribatsizTornaManager();

			while (allTestTahribatsizManuelUtSonucList.size() > index) {

				if (allTestTahribatsizManuelUtSonucList.get(index)
						.getTestTahribatsizTorna() == null
						&& allTestTahribatsizManuelUtSonucList.get(index)
								.getDegerlendirme() != null
						&& (allTestTahribatsizManuelUtSonucList.get(index)
								.getDegerlendirme() == 1 // 1=kabul
						|| allTestTahribatsizManuelUtSonucList.get(index)
								.getDegerlendirme() == 2 // 2=tamir
						)
						&& allTestTahribatsizManuelUtSonucList.get(index)
								.getTamirSonrasiDurum() == 1) {// 3=torna
																// 1=kabul

					kontrol = true;
				} else {

					FacesContext context = FacesContext.getCurrentInstance();
					context.addMessage(null, new FacesMessage(
							FacesMessage.SEVERITY_ERROR, "TORNA EKLENEMEDİ!",
							null));
					kontrol = false;
					break;
				}
				index++;
			}

			if (kontrol) {
				// newTorna.setDurum(false);
				newTorna.setEklemeZamani(UtilInsCore.getTarihZaman());
				newTorna.setUser(userBean.getUser());
				newTorna.setTestTahribatsizManuelUtSonuc(allTestTahribatsizManuelUtSonucList
						.get(0));
				newTorna.setPipe(allTestTahribatsizManuelUtSonucList.get(0)
						.getPipe());
				manager.enterNew(newTorna);
				newTorna = new TestTahribatsizTorna();

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"MANUET UT TARAFINDAN TORNA BAŞARIYLA EKLENMİŞTİR!"));

				return;
			} else {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"BORU TORNA'YA GÖNDERİLEMEDİ. BORUYLA İLGİLİ 'KABUL' EDİLMEMİŞ VERİLER VARDIR, LÜTFEN KONTROL EDİNİZ!",
								null));
			}

		} catch (Exception e) {
			System.out.println("TestTahribatsizTornaBean.addFromManuel-HATA");
		}
	}

	public void addFromFl(
			List<TestTahribatsizFloroskopikSonuc> allTestTahribatsizFloroskopikSonucList,
			UserCredentialsBean userBean) {
		System.out.println("TestTahribatsizTornaBean.add()");

		newTorna.setTestTahribatsizFloroskopikSonuc(new TestTahribatsizFloroskopikSonuc());

		try {
			TestTahribatsizTornaManager manager = new TestTahribatsizTornaManager();

			while (allTestTahribatsizFloroskopikSonucList.size() > index) {
				if (allTestTahribatsizFloroskopikSonucList.get(index)
						.getTestTahribatsizTorna() == null
						&& allTestTahribatsizFloroskopikSonucList.get(index)
								.getResult() != null
						&& allTestTahribatsizFloroskopikSonucList.get(index)
								.getResult() == 3) {// 3=torna
					// newTorna.setDurum(false);
					newTorna.setEklemeZamani(UtilInsCore.getTarihZaman());
					newTorna.setUser(userBean.getUser());
					newTorna.setTestTahribatsizFloroskopikSonuc(allTestTahribatsizFloroskopikSonucList
							.get(index));
					newTorna.setPipe(allTestTahribatsizFloroskopikSonucList
							.get(index).getPipe());
					manager.enterNew(newTorna);
					newTorna = new TestTahribatsizTorna();

					FacesContext context = FacesContext.getCurrentInstance();
					context.addMessage(
							null,
							new FacesMessage(
									"FLOROSKOPİ TARAFINDAN TORNA BAŞARIYLE EKLENMİŞTİR!"));

					return;
				}
				index++;
			}

		} catch (Exception e) {
			System.out.println("TestTahribatsizTornaBean.addFromFl-HATA");
		}
	}

	public void addTorna(ActionEvent ae) {

		UICommand cmd = (UICommand) ae.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId != null) {
			PipeManager pipeMan = new PipeManager();
			selectedPipe = pipeMan.loadObject(Pipe.class, prmPipeId);
		}

		try {
			TestTahribatsizTornaManager manager = new TestTahribatsizTornaManager();

			newTorna.setEklemeZamani(UtilInsCore.getTarihZaman());
			newTorna.setUser(userBean.getUser());
			newTorna.setPipe(selectedPipe);
			manager.enterNew(newTorna);
			newTorna = new TestTahribatsizTorna();

			fillTestList(prmPipeId);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"TORNA İŞLEMİ BAŞARIYLA EKLENMİŞTİR!"));
		} catch (Exception e) {
			if (selectedPipe.getPipeId() == null) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"TORNA YAPILAMADI, LÜTFEN BORU BARKODU OKUTUNUZ!",
								null));
			}
			System.out.println("TestTahribatsizTornaBean.addTorna-HATA");
		}
	}

	public void addTornaSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		Boolean prmKontrol = testTahribatsizTornaForm.getDurum();

		TestTahribatsizTornaManager tornaManager = new TestTahribatsizTornaManager();
		TestTahribatsizHidrostatikManager hidroManager = new TestTahribatsizHidrostatikManager();
		TestTahribatsizGozOlcuSonucManager gozManager = new TestTahribatsizGozOlcuSonucManager();

		Boolean hidroKontrol = false;
		Boolean gozOlcuKontrol = false;

		if (prmKontrol == null) {

			try {

				testTahribatsizTornaForm.setDurum(false);
				testTahribatsizTornaForm.setMuayeneBaslamaZamani(UtilInsCore
						.getTarihZaman());
				testTahribatsizTornaForm.setMuayeneBaslamaUser(userBean
						.getUser());
				tornaManager.updateEntity(testTahribatsizTornaForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"TORNA İŞLEMİ BAŞARIYLA BAŞLANMIŞTIR!"));
			} catch (Exception ex) {
				System.out
						.println("TestTahribatsizTornaBean.addTornaSonuc-HATA-BAŞLAMA");

			}
		} else if (prmKontrol == false) {

			try {

				testTahribatsizTornaForm.setDurum(true);
				testTahribatsizTornaForm.setMuayeneBitisZamani(UtilInsCore
						.getTarihZaman());
				testTahribatsizTornaForm
						.setMuayeneBitisUser(userBean.getUser());
				tornaManager.updateEntity(testTahribatsizTornaForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"TORNA İŞLEMİ BAŞARIYLA BİTİRİLMİŞTİR!"));

				if (hidroManager.getAllTestTahribatsizHidrostatik(
						testTahribatsizTornaForm.getPipe().getPipeId()).size() != 0) {
					hidroKontrol = true;
				}

				if (gozManager.getAllTestTahribatsizGozOlcuSonuc(
						testTahribatsizTornaForm.getPipe().getPipeId()).size() != 0) {
					gozOlcuKontrol = true;
				}

				// Boolean hidroKontrol = hidroManager
				// .getAllTestTahribatsizHidrostatik(
				// testTahribatsizTornaForm.getPipe().getPipeId())
				// .get(0).getDurum();

				// Boolean gozOlcuKontrol = gozManager
				// .getAllTestTahribatsizGozOlcuSonuc(
				// testTahribatsizTornaForm.getPipe().getPipeId())
				// .get(0).getDurum();

				if (hidroKontrol && !gozOlcuKontrol) {// eger seçili boru için
														// hidro tamamsa ve göz
					// ölçü yoksa, göz olcu eklenir.

					gozOlcuEkle();
				} else if (testTahribatsizTornaForm.getPipe().getSalesItem()
						.getPipeType().equals("Kazık Borusu")
						&& !gozOlcuKontrol) {// eğer kazık
					// borusuysa ve goz
					// olcu yoksa, goz
					// olcu eklenir
					gozOlcuEkle();
				} else {
					context.addMessage(
							null,
							new FacesMessage(
									FacesMessage.SEVERITY_ERROR,
									"HİDROSTATİK TEST EKSİK OLDUĞUNDAN YA DA GÖZ ÖLÇÜ VERİSİ EKSİK OLDUĞUNDAN, GÖZ ÖLÇÜ EKLENEMEDİ!",
									null));
				}

			} catch (Exception ex) {
				System.out
						.println("TestTahribatsizTornaBean.addTornaSonuc-HATA-BİTİRME"
								+ e.toString());
			}

		} else {

			try {

				testTahribatsizTornaForm.setGuncellemeZamani(UtilInsCore
						.getTarihZaman());
				testTahribatsizTornaForm.setGuncelleyenUser(userBean.getUser());
				tornaManager.updateEntity(testTahribatsizTornaForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"TORNA İŞLEMİ BAŞARIYLA GÜNCELLENMİŞTİR!"));
			} catch (Exception ex) {
				System.out
						.println("TestTahribatsizTornaBean.addTornaSonuc-HATA-GÜNCELLEME"
								+ e.toString());
			}
		}
		fillTestList(prmPipeId);
	}

	@SuppressWarnings("static-access")
	public void fillTestList(Integer prmPipeId) {

		TestTahribatsizTornaManager tornaManager = new TestTahribatsizTornaManager();
		allTestTahribatsizTornaList = tornaManager
				.getAllTestTahribatsizTorna(prmPipeId);
	}

	public void tornaListener(SelectEvent event) {
		updateButtonRender = true;
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_WARN, prmBarkod
							+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!", null));
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			TestTahribatsizTornaManager tornaManager = new TestTahribatsizTornaManager();
			allTestTahribatsizTornaList = tornaManager
					.getAllTestTahribatsizTorna(selectedPipe.getPipeId());
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
		}
	}

	public void deleteTornaSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		TestTahribatsizTornaManager tornaSonucManager = new TestTahribatsizTornaManager();

		if (testTahribatsizTornaForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		tornaSonucManager.delete(testTahribatsizTornaForm);
		testTahribatsizTornaForm = new TestTahribatsizTorna();
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"TORNA BAŞARIYLA SİLİNMİŞTİR!"));

		fillTestList(prmPipeId);
	}

	public void gozOlcuEkle() {

		gozOlcuBean.addFromTorna(testTahribatsizTornaForm, userBean);
		fillTestList(allTestTahribatsizTornaList.get(0).getPipe().getPipeId());
	}

	// setters getters
	public TestTahribatsizTorna getNewTorna() {
		return newTorna;
	}

	public void setNewTorna(TestTahribatsizTorna newTorna) {
		this.newTorna = newTorna;
	}

	public List<TestTahribatsizTorna> getTornaList() {
		return tornaList;
	}

	public void setTornaList(List<TestTahribatsizTorna> tornaList) {
		this.tornaList = tornaList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public List<TestTahribatsizTorna> getAllTestTahribatsizTornaList() {
		return allTestTahribatsizTornaList;
	}

	public void setAllTestTahribatsizTornaList(
			List<TestTahribatsizTorna> allTestTahribatsizTornaList) {
		this.allTestTahribatsizTornaList = allTestTahribatsizTornaList;
	}

	public TestTahribatsizTorna getTestTahribatsizTornaForm() {
		return testTahribatsizTornaForm;
	}

	public void setTestTahribatsizTornaForm(
			TestTahribatsizTorna testTahribatsizTornaForm) {
		this.testTahribatsizTornaForm = testTahribatsizTornaForm;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	public String getBarkodNo() {
		return barkodNo;
	}

	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	public TestTahribatsizGozOlcuSonucBean getGozOlcuBean() {
		return gozOlcuBean;
	}

	public void setGozOlcuBean(TestTahribatsizGozOlcuSonucBean gozOlcuBean) {
		this.gozOlcuBean = gozOlcuBean;
	}

}
