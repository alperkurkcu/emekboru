/**
 * 
 */
package com.emekboru.beans.coatingmaterials;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.coatingmaterials.KaplamaMalzemeKullanmaBeton;
import com.emekboru.jpaman.CoatRawMaterialManager;
import com.emekboru.jpaman.coatingmaterials.KaplamaMalzemeKullanmaBetonManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "kaplamaMalzemeKullanmaBetonBean")
@ViewScoped
public class KaplamaMalzemeKullanmaBetonBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<KaplamaMalzemeKullanmaBeton> allKaplamaMalzemeKullanmaBetonList = new ArrayList<KaplamaMalzemeKullanmaBeton>();
	private KaplamaMalzemeKullanmaBeton kaplamaMalzemeKullanmaBetonForm = new KaplamaMalzemeKullanmaBeton();

	private CoatRawMaterial selectedMaterial = new CoatRawMaterial();
	private List<CoatRawMaterial> coatRawMaterialList;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	private BigDecimal kullanilanMiktar;

	private boolean durum = false;

	public KaplamaMalzemeKullanmaBetonBean() {

		coatRawMaterialList = new ArrayList<CoatRawMaterial>();
		CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
		coatRawMaterialList = materialManager
				.findUnfinishedCoatMaterialByType(7);// beton type
	}

	public void addNew() {
		kaplamaMalzemeKullanmaBetonForm = new KaplamaMalzemeKullanmaBeton();
		updateButtonRender = false;
	}

	public void addMalzemeKullanmaSonuc(ActionEvent e) {

		if (kaplamaMalzemeKullanmaBetonForm.getHarcananMiktar() == null) {
			kaplamaMalzemeKullanmaBetonForm.setHarcananMiktar(BigDecimal.ZERO);
		}

		if (!malzemeKontrol()) {
			return;
		}

		KaplamaMalzemeKullanmaBetonManager malzemeKullanmaManager = new KaplamaMalzemeKullanmaBetonManager();

		if (kaplamaMalzemeKullanmaBetonForm.getId() == null) {

			try {
				kaplamaMalzemeKullanmaBetonForm.setEklemeZamani(UtilInsCore
						.getTarihZaman());
				kaplamaMalzemeKullanmaBetonForm.setEkleyenKullanici(userBean
						.getUser().getId());
				kaplamaMalzemeKullanmaBetonForm
						.setCoatRawMaterial(selectedMaterial);
				malzemeKullanmaManager
						.enterNew(kaplamaMalzemeKullanmaBetonForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"BETON MALZEMESİ İŞLEMİ BAŞARIYLA EKLENMİŞTİR!"));

				kullanilanMiktar = kaplamaMalzemeKullanmaBetonForm
						.getHarcananMiktar();
				durum = true;
				malzemeGuncelle();

			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"BETON MALZEMESİ İŞLEMİ EKLENEMEDİ!", null));
				System.out
						.println("KaplamaMalzemeKullanmaBetonBean.addMalzemeKullanmaSonuc-HATA-EKLEME");
			}
		} else {

			try {

				kaplamaMalzemeKullanmaBetonForm.setGuncellemeZamani(UtilInsCore
						.getTarihZaman());
				kaplamaMalzemeKullanmaBetonForm
						.setGuncelleyenKullanici(userBean.getUser().getId());
				malzemeKullanmaManager
						.updateEntity(kaplamaMalzemeKullanmaBetonForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"BETON MALZEMESİ İŞLEMİ BAŞARIYLA GÜNCELLENMİŞTİR!"));

				kullanilanMiktar = kaplamaMalzemeKullanmaBetonForm
						.getHarcananMiktar();
				durum = true;
				malzemeGuncelle();

			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"BETON MALZEMESİ KULLANMA İŞLEMİ GÜNCELLENEMEDİ!",
								null));
				System.out
						.println("KaplamaMalzemeKullanmaBetonBean.addMalzemeKullanmaSonuc-HATA-GÜNCELLEME"
								+ e.toString());
			}

		}

		fillTestList();
	}

	@SuppressWarnings("static-access")
	public void fillTestList() {

		KaplamaMalzemeKullanmaBetonManager malzemeKullanmaManager = new KaplamaMalzemeKullanmaBetonManager();
		allKaplamaMalzemeKullanmaBetonList = malzemeKullanmaManager
				.getAllKaplamaMalzemeKullanmaBeton();
	}

	public void kaplamaListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void deleteMalzemeKullanmaSonuc(ActionEvent e) {

		KaplamaMalzemeKullanmaBetonManager malzemeKullanmaManager = new KaplamaMalzemeKullanmaBetonManager();

		if (kaplamaMalzemeKullanmaBetonForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		kullanilanMiktar = kaplamaMalzemeKullanmaBetonForm.getHarcananMiktar();

		durum = false;
		malzemeGuncelle();

		malzemeKullanmaManager.delete(kaplamaMalzemeKullanmaBetonForm);
		kaplamaMalzemeKullanmaBetonForm = new KaplamaMalzemeKullanmaBeton();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"BETON MALZEMESİ KULLANMA İŞLEMİ BAŞARIYLA SİLİNMİŞTİR!"));

		fillTestList();
	}

	public void malzemeGuncelle() {

		CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
		if (durum) {// enternew
			selectedMaterial.setRemainingAmount(selectedMaterial
					.getRemainingAmount() - kullanilanMiktar.doubleValue());
			materialManager.updateEntity(selectedMaterial);
		} else if (!durum) {// delete
			kaplamaMalzemeKullanmaBetonForm.getCoatRawMaterial()
					.setRemainingAmount(
							kaplamaMalzemeKullanmaBetonForm
									.getCoatRawMaterial().getRemainingAmount()
									+ kullanilanMiktar.doubleValue());
			materialManager.updateEntity(kaplamaMalzemeKullanmaBetonForm
					.getCoatRawMaterial());
		}
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"BETON MALZEMESİ BAŞARIYLA GÜNCELLENMİŞTİR!"));
	}

	public boolean malzemeKontrol() {

		FacesContext context = FacesContext.getCurrentInstance();
		if (kaplamaMalzemeKullanmaBetonForm.getHarcananMiktar().floatValue() > selectedMaterial
				.getRemainingAmount()) {
			context.addMessage(
					null,
					new FacesMessage(
							"KULLANILACAK MİKTAR KALAN MALZEME MİKTARINDAN BÜYÜKTÜR! LÜTFEN KONTROL EDİNİZ!"));
			return false;
		} else {
			return true;
		}
	}

	// getters setters

	/**
	 * @return the allKaplamaMalzemeKullanmaBetonList
	 */
	public List<KaplamaMalzemeKullanmaBeton> getAllKaplamaMalzemeKullanmaBetonList() {
		return allKaplamaMalzemeKullanmaBetonList;
	}

	/**
	 * @param allKaplamaMalzemeKullanmaBetonList
	 *            the allKaplamaMalzemeKullanmaBetonList to set
	 */
	public void setAllKaplamaMalzemeKullanmaBetonList(
			List<KaplamaMalzemeKullanmaBeton> allKaplamaMalzemeKullanmaBetonList) {
		this.allKaplamaMalzemeKullanmaBetonList = allKaplamaMalzemeKullanmaBetonList;
	}

	/**
	 * @return the kaplamaMalzemeKullanmaBetonForm
	 */
	public KaplamaMalzemeKullanmaBeton getKaplamaMalzemeKullanmaBetonForm() {
		return kaplamaMalzemeKullanmaBetonForm;
	}

	/**
	 * @param kaplamaMalzemeKullanmaBetonForm
	 *            the kaplamaMalzemeKullanmaBetonForm to set
	 */
	public void setKaplamaMalzemeKullanmaBetonForm(
			KaplamaMalzemeKullanmaBeton kaplamaMalzemeKullanmaBetonForm) {
		this.kaplamaMalzemeKullanmaBetonForm = kaplamaMalzemeKullanmaBetonForm;
	}

	/**
	 * @return the selectedMaterial
	 */
	public CoatRawMaterial getSelectedMaterial() {
		return selectedMaterial;
	}

	/**
	 * @param selectedMaterial
	 *            the selectedMaterial to set
	 */
	public void setSelectedMaterial(CoatRawMaterial selectedMaterial) {
		this.selectedMaterial = selectedMaterial;
	}

	/**
	 * @return the coatRawMaterialList
	 */
	public List<CoatRawMaterial> getCoatRawMaterialList() {
		return coatRawMaterialList;
	}

	/**
	 * @param coatRawMaterialList
	 *            the coatRawMaterialList to set
	 */
	public void setCoatRawMaterialList(List<CoatRawMaterial> coatRawMaterialList) {
		this.coatRawMaterialList = coatRawMaterialList;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the kullanilanMiktar
	 */
	public BigDecimal getKullanilanMiktar() {
		return kullanilanMiktar;
	}

	/**
	 * @param kullanilanMiktar
	 *            the kullanilanMiktar to set
	 */
	public void setKullanilanMiktar(BigDecimal kullanilanMiktar) {
		this.kullanilanMiktar = kullanilanMiktar;
	}

	/**
	 * @return the durum
	 */
	public boolean isDurum() {
		return durum;
	}

	/**
	 * @param durum
	 *            the durum to set
	 */
	public void setDurum(boolean durum) {
		this.durum = durum;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
