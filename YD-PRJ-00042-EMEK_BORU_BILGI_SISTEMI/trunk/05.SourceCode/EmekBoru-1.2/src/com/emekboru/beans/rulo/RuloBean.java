package com.emekboru.beans.rulo;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.DestructiveTestsSpec;
import com.emekboru.jpa.Employee;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.rulo.Rulo;
import com.emekboru.jpa.rulo.RuloOzellikBoyutsal;
import com.emekboru.jpa.rulo.RuloOzellikFizikselGirdi;
import com.emekboru.jpa.rulo.RuloOzellikFizikselUretici;
import com.emekboru.jpa.rulo.RuloPipeLink;
import com.emekboru.jpa.rulo.RuloTestCekmeBoyuna;
import com.emekboru.jpa.rulo.RuloTestCekmeEnine;
import com.emekboru.jpa.rulo.RuloTestCentikDarbeBoyuna;
import com.emekboru.jpa.rulo.RuloTestCentikDarbeEnine;
import com.emekboru.jpa.rulo.RuloTestCentikDarbeUretici;
import com.emekboru.jpa.rulo.RuloTestDwttGirdi;
import com.emekboru.jpa.rulo.RuloTestDwttUretici;
import com.emekboru.jpa.rulo.RuloTestKimyasalGirdi;
import com.emekboru.jpa.rulo.RuloTestKimyasalUretici;
import com.emekboru.jpa.sales.customer.SalesCustomer;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.DestructiveTestsSpecManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.rulo.RuloManager;
import com.emekboru.jpaman.rulo.RuloOzellikBoyutsalManager;
import com.emekboru.jpaman.rulo.RuloOzellikFizikselGirdiManager;
import com.emekboru.jpaman.rulo.RuloOzellikFizikselUreticiManager;
import com.emekboru.jpaman.rulo.RuloPipeLinkManager;
import com.emekboru.jpaman.rulo.RuloTestCekmeBoyunaManager;
import com.emekboru.jpaman.rulo.RuloTestCekmeEnineManager;
import com.emekboru.jpaman.rulo.RuloTestCentikDarbeBoyunaManager;
import com.emekboru.jpaman.rulo.RuloTestCentikDarbeEnineManager;
import com.emekboru.jpaman.rulo.RuloTestCentikDarbeUreticiManager;
import com.emekboru.jpaman.rulo.RuloTestDwttGirdiManager;
import com.emekboru.jpaman.rulo.RuloTestDwttUreticiManager;
import com.emekboru.jpaman.rulo.RuloTestKimyasalGirdiManager;
import com.emekboru.jpaman.rulo.RuloTestKimyasalUreticiManager;
import com.emekboru.jpaman.sales.customer.SalesCustomerManager;
import com.emekboru.jpaman.sales.order.SalesItemManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.ReportUtil;
import com.emekboru.utils.RuloUtil;
import com.emekboru.utils.lazymodels.LazyRuloDataModel;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.Barcode128;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;

@ManagedBean(name = "ruloBean")
@ViewScoped
public class RuloBean {

	@EJB
	private ConfigBean config;
	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private Rulo selectedRulo = new Rulo();
	private LazyRuloDataModel rulos;
	private Rulo newRulo = new Rulo();

	private RuloOzellikBoyutsal selectedRuloOzellikBoyutsal = new RuloOzellikBoyutsal();
	private RuloOzellikFizikselGirdi selectedRuloOzellikFizikselGirdi = new RuloOzellikFizikselGirdi();
	private RuloOzellikFizikselUretici selectedRuloOzellikFizikselUretici = new RuloOzellikFizikselUretici();
	private RuloTestCekmeBoyuna selectedRuloTestCekmeBoyuna = new RuloTestCekmeBoyuna();
	private RuloTestCekmeEnine selectedRuloTestCekmeEnine = new RuloTestCekmeEnine();
	private RuloTestCentikDarbeBoyuna selectedRuloTestCentikDarbeBoyuna = new RuloTestCentikDarbeBoyuna();
	private RuloTestCentikDarbeEnine selectedRuloTestCentikDarbeEnine = new RuloTestCentikDarbeEnine();
	private RuloTestCentikDarbeUretici selectedRuloTestCentikDarbeUretici = new RuloTestCentikDarbeUretici();
	private RuloTestKimyasalGirdi selectedRuloTestKimyasalGirdi = new RuloTestKimyasalGirdi();
	private RuloTestKimyasalUretici selectedRuloTestKimyasalUretici = new RuloTestKimyasalUretici();
	private RuloTestDwttUretici selectedRuloTestDwttUretici = new RuloTestDwttUretici();
	private RuloTestDwttGirdi selectedRuloTestDwttGirdi = new RuloTestDwttGirdi();

	private String path = null;

	private List<Rulo> ayniDokumAyniKalinlikRuloList = null;

	public LazyRuloDataModel getRulos() {
		return rulos;
	}

	public void setRulos(LazyRuloDataModel rulos) {
		this.rulos = rulos;
	}

	@PostConstruct
	public void init() {
		downloadedFile = null;
		rulos = new LazyRuloDataModel();
	}

	public void reset() {

		selectedRuloOzellikBoyutsal = new RuloOzellikBoyutsal();
		selectedRuloOzellikFizikselGirdi = new RuloOzellikFizikselGirdi();
		selectedRuloOzellikFizikselUretici = new RuloOzellikFizikselUretici();
		selectedRuloTestCekmeBoyuna = new RuloTestCekmeBoyuna();
		selectedRuloTestCekmeEnine = new RuloTestCekmeEnine();
		selectedRuloTestCentikDarbeBoyuna = new RuloTestCentikDarbeBoyuna();
		selectedRuloTestCentikDarbeEnine = new RuloTestCentikDarbeEnine();
		selectedRuloTestCentikDarbeUretici = new RuloTestCentikDarbeUretici();
		selectedRuloTestKimyasalGirdi = new RuloTestKimyasalGirdi();
		selectedRuloTestKimyasalUretici = new RuloTestKimyasalUretici();
		selectedRuloTestDwttUretici = new RuloTestDwttUretici();
		selectedRuloTestDwttGirdi = new RuloTestDwttGirdi();
	}

	private File theFile = null;
	private String privatePath;
	OutputStream output = null;
	private String downloadedFileName;
	private StreamedContent downloadedFile;

	public RuloBean() {

		privatePath = File.separatorChar + "barkodlar" + File.separatorChar
				+ "ruloBarkod" + File.separatorChar;

		try {
			// this.setPath("/home/emekboru/Desktop/emekfiles" + privatePath);
			this.setPath("C:/emekfiles" + privatePath);

			System.out.println("Path for Upload File (ruloBean):			" + path);
		} catch (Exception ex) {
			System.out.println("Path for Upload File (ruloBean):			"
					+ FacesContext.getCurrentInstance().getExternalContext()
							.getRealPath(privatePath));

			System.out.print(path);
			System.out.println("CAUSE OF " + ex.toString());
		}

		theFile = new File(path);
		if (!theFile.isDirectory()) {
			theFile.mkdirs();
		}

		downloadedFile = null;
		downloadedFileName = null;
	}

	public void addNewRulo() {

		RuloManager ruloManager = new RuloManager();
		newRulo = RuloUtil.getRuloKimlikId(ruloManager, newRulo);
		Employee emp = userBean.getUser().getEmployee();
		newRulo.setIslemYapanId(emp.getEmployeeId());
		newRulo.setIslemYapanIsim(emp.getFirstname().concat(" ")
				.concat(emp.getLastname()));
		newRulo.setRuloDurum(2);
		ruloManager.enterNew(newRulo);
		this.newRulo = new Rulo();
		FacesContextUtils.redirect(config.getPageUrl().COIL_PAGE);
	}

	public void loadReplySelectedOrder() {
		RuloManager ruloManager = new RuloManager();
		selectedRulo = ruloManager.loadObject(Rulo.class,
				selectedRulo.getRuloId());
	}

	// Aynı döküm ve aynı et kalınlığı olan rulolar
	public void ayniRulolariBul(String dokumNo, Float etKalinligi) {

		try {
			if (dokumNo.equals(0)) {
				return;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		RuloManager rm = new RuloManager();
		ayniDokumAyniKalinlikRuloList = rm.getByDokumEtKalinligi(dokumNo,
				etKalinligi);
	}

	// Aynı döküm ve aynı et kalınlığı olan rulolar

	/*
	 * Delete Property
	 */
	public void deleteSelectedRulo() {
		loadReplySelectedOrder();
		RuloManager ruloManager = new RuloManager();
		Employee emp = userBean.getUser().getEmployee();
		selectedRulo.setIslemYapanId(emp.getEmployeeId());
		selectedRulo.setIslemYapanIsim(emp.getFirstname().concat(" ")
				.concat(emp.getLastname()));
		ruloManager.delete(selectedRulo);
		selectedRulo = new Rulo();
	}

	public void deleteSelectedRuloTestKimyasalGirdi() {
		loadReplySelectedOrder();
		List<RuloTestKimyasalGirdi> ruloTestKimyasalGirdis = selectedRulo
				.getRuloTestKimyasalGirdi();
		RuloTestKimyasalGirdiManager ruloTestKimyasalGirdiManager = new RuloTestKimyasalGirdiManager();
		Employee emp = userBean.getUser().getEmployee();
		selectedRuloTestKimyasalGirdi.setIslemYapanId(emp.getEmployeeId());
		selectedRuloTestKimyasalGirdi.setIslemYapanIsim(emp.getFirstname()
				.concat(" ").concat(emp.getLastname()));
		if (ruloTestKimyasalGirdis != null && ruloTestKimyasalGirdis.size() > 0) {
			RuloTestKimyasalGirdi newUpdateRulo = new RuloTestKimyasalGirdi();
			newUpdateRulo.setDurum(Rulo.RULO_TEST_DURUM_GIRILMEMIS);
			newUpdateRulo
					.setRuloTestKimyasalGirdiId(selectedRuloTestKimyasalGirdi
							.getRuloTestKimyasalGirdiId());
			ruloTestKimyasalGirdiManager.update(newUpdateRulo);

		}
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"İşlem başarılı bir şekilde tamamlanmıştır."));
	}

	public void deleteSelectedRuloTestCekmeBoyuna() {
		loadReplySelectedOrder();
		List<RuloTestCekmeBoyuna> ruloTestCekmeBoyunas = selectedRulo
				.getRuloTestCekmeBoyuna();
		RuloTestCekmeBoyunaManager ruloTestCekmeBoyunaManager = new RuloTestCekmeBoyunaManager();
		Employee emp = userBean.getUser().getEmployee();
		selectedRuloTestCekmeBoyuna.setIslemYapanId(emp.getEmployeeId());
		selectedRuloTestCekmeBoyuna.setIslemYapanIsim(emp.getFirstname()
				.concat(" ").concat(emp.getLastname()));
		if (ruloTestCekmeBoyunas != null && ruloTestCekmeBoyunas.size() > 0) {
			RuloTestCekmeBoyuna newUpdateRulo = new RuloTestCekmeBoyuna();
			newUpdateRulo.setDurum(Rulo.RULO_TEST_DURUM_GIRILMEMIS);
			newUpdateRulo.setRuloTestCekmeBoyunaId(selectedRuloTestCekmeBoyuna
					.getRuloTestCekmeBoyunaId());
			ruloTestCekmeBoyunaManager.updateEntity(newUpdateRulo);
		}
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"İşlem başarılı bir şekilde tamamlanmıştır."));

	}

	public void deleteSelectedRuloTestCekmeEnine() {
		loadReplySelectedOrder();
		List<RuloTestCekmeEnine> ruloTestCekmeEnines = selectedRulo
				.getRuloTestCekmeEnine();
		RuloTestCekmeEnineManager ruloTestCekmeEnineManager = new RuloTestCekmeEnineManager();
		Employee emp = userBean.getUser().getEmployee();
		selectedRuloTestCekmeEnine.setIslemYapanId(emp.getEmployeeId());
		selectedRuloTestCekmeEnine.setIslemYapanIsim(emp.getFirstname()
				.concat(" ").concat(emp.getLastname()));
		if (ruloTestCekmeEnines != null && ruloTestCekmeEnines.size() > 0) {
			selectedRuloTestCekmeEnine
					.setDurum(Rulo.RULO_TEST_DURUM_GIRILMEMIS);
			ruloTestCekmeEnineManager.updateEntity(selectedRuloTestCekmeEnine);

		}
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"İşlem başarılı bir şekilde tamamlanmıştır."));

	}

	public void deleteSelectedRuloTestCentikDarbeBoyuna() {
		loadReplySelectedOrder();
		List<RuloTestCentikDarbeBoyuna> ruloTestCentikDarbeBoyunas = selectedRulo
				.getRuloTestCentikDarbeBoyuna();
		RuloTestCentikDarbeBoyunaManager ruloTestCentikDarbeBoyunaManager = new RuloTestCentikDarbeBoyunaManager();
		Employee emp = userBean.getUser().getEmployee();
		selectedRuloTestCentikDarbeBoyuna.setIslemYapanId(emp.getEmployeeId());
		selectedRuloTestCentikDarbeBoyuna.setIslemYapanIsim(emp.getFirstname()
				.concat(" ").concat(emp.getLastname()));
		if (ruloTestCentikDarbeBoyunas != null
				&& ruloTestCentikDarbeBoyunas.size() > 0) {
			RuloTestCentikDarbeBoyuna newUpdateRulo = new RuloTestCentikDarbeBoyuna();
			newUpdateRulo.setDurum(Rulo.RULO_TEST_DURUM_GIRILMEMIS);
			newUpdateRulo
					.setRuloTestCentikDarbeId(selectedRuloTestCentikDarbeBoyuna
							.getRuloTestCentikDarbeId());
			ruloTestCentikDarbeBoyunaManager.updateEntity(newUpdateRulo);
		}
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"İşlem başarılı bir şekilde tamamlanmıştır."));

	}

	public void deleteSelectedRuloTestCentikDarbeEnine() {
		loadReplySelectedOrder();
		List<RuloTestCentikDarbeEnine> ruloTestCentikDarbeEnines = selectedRulo
				.getRuloTestCentikDarbeEnine();
		RuloTestCentikDarbeEnineManager ruloTestCentikDarbeEnineManager = new RuloTestCentikDarbeEnineManager();
		Employee emp = userBean.getUser().getEmployee();
		selectedRuloTestCentikDarbeEnine.setIslemYapanId(emp.getEmployeeId());
		selectedRuloTestCentikDarbeEnine.setIslemYapanIsim(emp.getFirstname()
				.concat(" ").concat(emp.getLastname()));
		if (ruloTestCentikDarbeEnines != null
				&& ruloTestCentikDarbeEnines.size() > 0) {
			RuloTestCentikDarbeEnine newUpdateRulo = new RuloTestCentikDarbeEnine();
			newUpdateRulo.setDurum(Rulo.RULO_TEST_DURUM_GIRILMEMIS);
			newUpdateRulo
					.setRuloTestCentikDarbeId(selectedRuloTestCentikDarbeEnine
							.getRuloTestCentikDarbeId());
			ruloTestCentikDarbeEnineManager.updateEntity(newUpdateRulo);
		}
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"İşlem başarılı bir şekilde tamamlanmıştır."));

	}

	public void deleteSelectedRuloTestCentikDarbeUretici() {
		loadReplySelectedOrder();
		List<RuloTestCentikDarbeUretici> ruloTestCentikDarbeUreticis = selectedRulo
				.getRuloTestCentikDarbeUretici();
		RuloTestCentikDarbeUreticiManager ruloTestCentikDarbeUreticiManager = new RuloTestCentikDarbeUreticiManager();
		Employee emp = userBean.getUser().getEmployee();
		selectedRuloTestCentikDarbeUretici.setIslemYapanId(emp.getEmployeeId());
		selectedRuloTestCentikDarbeUretici.setIslemYapanIsim(emp.getFirstname()
				.concat(" ").concat(emp.getLastname()));
		if (ruloTestCentikDarbeUreticis != null
				&& ruloTestCentikDarbeUreticis.size() > 0) {
			RuloTestCentikDarbeUretici newUpdateRulo = new RuloTestCentikDarbeUretici();
			newUpdateRulo.setDurum(Rulo.RULO_TEST_DURUM_GIRILMEMIS);
			newUpdateRulo
					.setRuloTestCentikDarbeId(selectedRuloTestCentikDarbeUretici
							.getRuloTestCentikDarbeId());
			ruloTestCentikDarbeUreticiManager.updateEntity(newUpdateRulo);
		}
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"İşlem başarılı bir şekilde tamamlanmıştır."));

	}

	public void deleteSelectedRuloTestKimyasalUretici() {
		loadReplySelectedOrder();
		List<RuloTestKimyasalUretici> ruloTestKimyasalUreticis = selectedRulo
				.getRuloTestKimyasalUretici();
		RuloTestKimyasalUreticiManager ruloTestKimyasalUreticiManager = new RuloTestKimyasalUreticiManager();
		Employee emp = userBean.getUser().getEmployee();
		selectedRuloTestKimyasalUretici.setIslemYapanId(emp.getEmployeeId());
		selectedRuloTestKimyasalUretici.setIslemYapanIsim(emp.getFirstname()
				.concat(" ").concat(emp.getLastname()));
		if (ruloTestKimyasalUreticis != null
				&& ruloTestKimyasalUreticis.size() > 0) {
			RuloTestKimyasalUretici newUpdateRulo = new RuloTestKimyasalUretici();
			newUpdateRulo.setDurum(Rulo.RULO_TEST_DURUM_GIRILMEMIS);
			newUpdateRulo
					.setRuloTestKimyasalUreticiId(selectedRuloTestKimyasalUretici
							.getRuloTestKimyasalUreticiId());
			ruloTestKimyasalUreticiManager.updateEntity(newUpdateRulo);
		}
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"İşlem başarılı bir şekilde tamamlanmıştır."));

	}

	public void deleteSelectedRuloOzellikBoyutsal() {
		RuloOzellikBoyutsalManager ruloOzellikBoyutsalManager = new RuloOzellikBoyutsalManager();
		selectedRuloOzellikBoyutsal.setDurum(Rulo.RULO_TEST_DURUM_GIRILMEMIS);
		Employee emp = userBean.getUser().getEmployee();
		selectedRuloOzellikBoyutsal.setIslemYapanId(emp.getEmployeeId());
		selectedRuloOzellikBoyutsal.setIslemYapanIsim(emp.getFirstname()
				.concat(" ").concat(emp.getLastname()));
		ruloOzellikBoyutsalManager.updateEntity(selectedRuloOzellikBoyutsal);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"İşlem başarılı bir şekilde tamamlanmıştır."));

	}

	public void deleteSelectedRuloOzellikFizikselGirdi() {
		loadReplySelectedOrder();
		RuloOzellikFizikselGirdiManager ruloOzellikFizikselGirdiManager = new RuloOzellikFizikselGirdiManager();
		selectedRuloOzellikFizikselGirdi
				.setDurum(Rulo.RULO_TEST_DURUM_GIRILMEMIS);
		Employee emp = userBean.getUser().getEmployee();
		selectedRuloOzellikFizikselGirdi.setIslemYapanId(emp.getEmployeeId());
		selectedRuloOzellikFizikselGirdi.setIslemYapanIsim(emp.getFirstname()
				.concat(" ").concat(emp.getLastname()));
		ruloOzellikFizikselGirdiManager
				.updateEntity(selectedRuloOzellikFizikselGirdi);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"İşlem başarılı bir şekilde tamamlanmıştır."));

	}

	public void deleteSelectedRuloOzellikFizikselUretici() {
		loadReplySelectedOrder();
		RuloOzellikFizikselUreticiManager ruloOzellikFizikselUreticiManager = new RuloOzellikFizikselUreticiManager();
		selectedRuloOzellikFizikselUretici
				.setDurum(Rulo.RULO_TEST_DURUM_GIRILMEMIS);
		Employee emp = userBean.getUser().getEmployee();
		selectedRuloOzellikFizikselUretici.setIslemYapanId(emp.getEmployeeId());
		selectedRuloOzellikFizikselUretici.setIslemYapanIsim(emp.getFirstname()
				.concat(" ").concat(emp.getLastname()));
		ruloOzellikFizikselUreticiManager
				.updateEntity(selectedRuloOzellikFizikselUretici);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"İşlem başarılı bir şekilde tamamlanmıştır."));
	}

	public void deleteSelectedRuloTestDwttUretici() {
		loadReplySelectedOrder();
		RuloTestDwttUreticiManager ruloTestDwttUreticiManager = new RuloTestDwttUreticiManager();
		selectedRuloTestDwttUretici.setDurum(Rulo.RULO_TEST_DURUM_GIRILMEMIS);
		Employee emp = userBean.getUser().getEmployee();
		selectedRuloTestDwttUretici.setIslemYapanId(emp.getEmployeeId());
		selectedRuloTestDwttUretici.setIslemYapanIsim(emp.getFirstname()
				.concat(" ").concat(emp.getLastname()));
		ruloTestDwttUreticiManager.updateEntity(selectedRuloTestDwttUretici);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"İşlem başarılı bir şekilde tamamlanmıştır."));
	}

	public void deleteSelectedRuloTestDwttGirdi() {
		loadReplySelectedOrder();
		RuloTestDwttGirdiManager ruloTestDwttGirdiManager = new RuloTestDwttGirdiManager();
		selectedRuloTestDwttGirdi.setDurum(Rulo.RULO_TEST_DURUM_GIRILMEMIS);
		Employee emp = userBean.getUser().getEmployee();
		selectedRuloTestDwttGirdi.setIslemYapanId(emp.getEmployeeId());
		selectedRuloTestDwttGirdi.setIslemYapanIsim(emp.getFirstname()
				.concat(" ").concat(emp.getLastname()));
		ruloTestDwttGirdiManager.updateEntity(selectedRuloTestDwttGirdi);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"İşlem başarılı bir şekilde tamamlanmıştır."));
	}

	/*
	 * Update rulo Property
	 */

	public void updateSelectedRulo() {
		RuloManager ruloManager = new RuloManager();
		Employee emp = userBean.getUser().getEmployee();
		selectedRulo.setIslemYapanId(emp.getEmployeeId());
		selectedRulo.setIslemYapanIsim(emp.getFirstname().concat(" ")
				.concat(emp.getLastname()));
		SalesCustomerManager customerManager = new SalesCustomerManager();
		selectedRulo.setSalesCustomer(((SalesCustomer) customerManager
				.loadObject(SalesCustomer.class,
						selectedRulo.getSalesCustomerId())));
		ruloManager.updateEntity(selectedRulo);
		selectedRulo = new Rulo();
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"İşlem başarılı bir şekilde güncellenmiştir."));
	}

	// public void updateSelectedRuloTestKimyasalGirdi() {
	// loadReplySelectedOrder();
	// List<RuloTestKimyasalGirdi> ruloTestKimyasalGirdis = selectedRulo
	// .getRuloTestKimyasalGirdi();
	// RuloTestKimyasalGirdiManager ruloTestKimyasalGirdiManager = new
	// RuloTestKimyasalGirdiManager();
	// Employee emp = userBean.getUser().getEmployee();
	// selectedRuloTestKimyasalGirdi.setIslemYapanId(emp.getEmployeeId());
	// selectedRuloTestKimyasalGirdi.setIslemYapanIsim(emp.getFirstname()
	// .concat(" ").concat(emp.getLastname()));
	// if (ruloTestKimyasalGirdis != null && ruloTestKimyasalGirdis.size() > 0)
	// {
	// ruloTestKimyasalGirdiManager.update(selectedRuloTestKimyasalGirdi);
	//
	// } else {
	// selectedRuloTestKimyasalGirdi.setRulo(selectedRulo);
	// ruloTestKimyasalGirdiManager
	// .enterNew(selectedRuloTestKimyasalGirdi);
	//
	// }
	// FacesContext context = FacesContext.getCurrentInstance();
	// context.addMessage(null, new FacesMessage(
	// "İşlem başarılı bir şekilde tamamlanmıştır."));
	// }

	public void updateSelectedRuloTestKimyasalGirdi() {
		loadReplySelectedOrder();

		ayniRulolariBul(selectedRulo.getRuloDokumNo(), selectedRulo
				.getRuloOzellikBoyutsal().getRuloOzellikBoyutsalEtKalinligi());

		for (int i = 0; i < ayniDokumAyniKalinlikRuloList.size(); i++) {

			if (i > 0) {// id nullanıyor yeni giriş için
				selectedRuloTestKimyasalGirdi.setRuloTestKimyasalGirdiId(null);
			}

			List<RuloTestKimyasalGirdi> ruloTestKimyasalGirdis = ayniDokumAyniKalinlikRuloList
					.get(i).getRuloTestKimyasalGirdi();
			RuloTestKimyasalGirdiManager ruloTestKimyasalGirdiManager = new RuloTestKimyasalGirdiManager();
			Employee emp = userBean.getUser().getEmployee();
			selectedRuloTestKimyasalGirdi.setIslemYapanId(emp.getEmployeeId());
			selectedRuloTestKimyasalGirdi.setIslemYapanIsim(emp.getFirstname()
					.concat(" ").concat(emp.getLastname()));
			if (ruloTestKimyasalGirdis != null
					&& ruloTestKimyasalGirdis.size() > 0) {
				ruloTestKimyasalGirdiManager
						.update(selectedRuloTestKimyasalGirdi);

			} else {
				selectedRuloTestKimyasalGirdi
						.setRulo(ayniDokumAyniKalinlikRuloList.get(i));
				ruloTestKimyasalGirdiManager
						.enterNew(selectedRuloTestKimyasalGirdi);

			}
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							"Rulo Kimyasal Analiz Testi (Girdi) "
									+ ayniDokumAyniKalinlikRuloList.get(i)
											.getRuloYilAndRuloKimlikId()
									+ " numaralı rulo için başarılı bir şekilde tamamlanmıştır."));

		}

	}

	// public void updateSelectedRuloTestCekmeBoyuna() {
	// loadReplySelectedOrder();
	// List<RuloTestCekmeBoyuna> ruloTestCekmeBoyunas = selectedRulo
	// .getRuloTestCekmeBoyuna();
	// RuloTestCekmeBoyunaManager ruloTestCekmeBoyunaManager = new
	// RuloTestCekmeBoyunaManager();
	// Employee emp = userBean.getUser().getEmployee();
	// selectedRuloTestCekmeBoyuna.setIslemYapanId(emp.getEmployeeId());
	// selectedRuloTestCekmeBoyuna.setIslemYapanIsim(emp.getFirstname()
	// .concat(" ").concat(emp.getLastname()));
	// if (ruloTestCekmeBoyunas != null && ruloTestCekmeBoyunas.size() > 0) {
	// ruloTestCekmeBoyunaManager
	// .updateEntity(selectedRuloTestCekmeBoyuna);
	//
	// } else {
	// selectedRuloTestCekmeBoyuna.setRulo(selectedRulo);
	// ruloTestCekmeBoyunaManager.enterNew(selectedRuloTestCekmeBoyuna);
	//
	// }
	// FacesContext context = FacesContext.getCurrentInstance();
	// context.addMessage(null, new FacesMessage(
	// "İşlem başarılı bir şekilde tamamlanmıştır."));
	// }

	public void updateSelectedRuloTestCekmeBoyuna() {

		loadReplySelectedOrder();

		ayniRulolariBul(selectedRulo.getRuloDokumNo(), selectedRulo
				.getRuloOzellikBoyutsal().getRuloOzellikBoyutsalEtKalinligi());

		for (int i = 0; i < ayniDokumAyniKalinlikRuloList.size(); i++) {

			if (i > 0) {// id nullanıyor yeni giriş için
				selectedRuloTestCekmeBoyuna.setRuloTestCekmeBoyunaId(null);
			}

			List<RuloTestCekmeBoyuna> ruloTestCekmeBoyunas = ayniDokumAyniKalinlikRuloList
					.get(i).getRuloTestCekmeBoyuna();
			RuloTestCekmeBoyunaManager ruloTestCekmeBoyunaManager = new RuloTestCekmeBoyunaManager();
			Employee emp = userBean.getUser().getEmployee();
			selectedRuloTestCekmeBoyuna.setIslemYapanId(emp.getEmployeeId());
			selectedRuloTestCekmeBoyuna.setIslemYapanIsim(emp.getFirstname()
					.concat(" ").concat(emp.getLastname()));
			if (ruloTestCekmeBoyunas != null && ruloTestCekmeBoyunas.size() > 0) {
				ruloTestCekmeBoyunaManager
						.updateEntity(selectedRuloTestCekmeBoyuna);

			} else {
				selectedRuloTestCekmeBoyuna
						.setRulo(ayniDokumAyniKalinlikRuloList.get(i));
				ruloTestCekmeBoyunaManager
						.enterNew(selectedRuloTestCekmeBoyuna);

			}
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							"Rulo Çekme Testi Boyuna "
									+ ayniDokumAyniKalinlikRuloList.get(i)
											.getRuloYilAndRuloKimlikId()
									+ " numaralı rulo için başarılı bir şekilde tamamlanmıştır."));
		}
	}

	// public void updateSelectedRuloTestCekmeEnine() {
	// loadReplySelectedOrder();
	// List<RuloTestCekmeEnine> ruloTestCekmeEnines = selectedRulo
	// .getRuloTestCekmeEnine();
	// RuloTestCekmeEnineManager ruloTestCekmeEnineManager = new
	// RuloTestCekmeEnineManager();
	// Employee emp = userBean.getUser().getEmployee();
	// selectedRuloTestCekmeEnine.setIslemYapanId(emp.getEmployeeId());
	// selectedRuloTestCekmeEnine.setIslemYapanIsim(emp.getFirstname()
	// .concat(" ").concat(emp.getLastname()));
	// if (ruloTestCekmeEnines != null && ruloTestCekmeEnines.size() > 0) {
	// ruloTestCekmeEnineManager.updateEntity(selectedRuloTestCekmeEnine);
	//
	// } else {
	// selectedRuloTestCekmeEnine.setRulo(selectedRulo);
	// ruloTestCekmeEnineManager.enterNew(selectedRuloTestCekmeEnine);
	// }
	// FacesContext context = FacesContext.getCurrentInstance();
	// context.addMessage(null, new FacesMessage(
	// "İşlem başarılı bir şekilde tamamlanmıştır."));
	// }

	public void updateSelectedRuloTestCekmeEnine() {

		loadReplySelectedOrder();

		ayniRulolariBul(selectedRulo.getRuloDokumNo(), selectedRulo
				.getRuloOzellikBoyutsal().getRuloOzellikBoyutsalEtKalinligi());

		for (int i = 0; i < ayniDokumAyniKalinlikRuloList.size(); i++) {

			if (i > 0) {// id nullanıyor yeni giriş için
				selectedRuloTestCekmeEnine.setRuloTestCekmeEnineId(null);
			}

			List<RuloTestCekmeEnine> ruloTestCekmeEnines = ayniDokumAyniKalinlikRuloList
					.get(i).getRuloTestCekmeEnine();
			RuloTestCekmeEnineManager ruloTestCekmeEnineManager = new RuloTestCekmeEnineManager();
			Employee emp = userBean.getUser().getEmployee();
			selectedRuloTestCekmeEnine.setIslemYapanId(emp.getEmployeeId());
			selectedRuloTestCekmeEnine.setIslemYapanIsim(emp.getFirstname()
					.concat(" ").concat(emp.getLastname()));
			if (ruloTestCekmeEnines != null && ruloTestCekmeEnines.size() > 0) {
				ruloTestCekmeEnineManager
						.updateEntity(selectedRuloTestCekmeEnine);

			} else {
				selectedRuloTestCekmeEnine
						.setRulo(ayniDokumAyniKalinlikRuloList.get(i));
				ruloTestCekmeEnineManager.enterNew(selectedRuloTestCekmeEnine);
			}
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							"Rulo Çekme Testi Enine "
									+ ayniDokumAyniKalinlikRuloList.get(i)
											.getRuloYilAndRuloKimlikId()
									+ " numaralı rulo için başarılı bir şekilde tamamlanmıştır."));
		}
	}

	// public void updateSelectedRuloTestCentikDarbeBoyuna() {
	// loadReplySelectedOrder();
	// List<RuloTestCentikDarbeBoyuna> ruloTestCentikDarbeBoyunas = selectedRulo
	// .getRuloTestCentikDarbeBoyuna();
	// RuloTestCentikDarbeBoyunaManager ruloTestCentikDarbeBoyunaManager = new
	// RuloTestCentikDarbeBoyunaManager();
	// Employee emp = userBean.getUser().getEmployee();
	// selectedRuloTestCentikDarbeBoyuna.setIslemYapanId(emp.getEmployeeId());
	// selectedRuloTestCentikDarbeBoyuna.setIslemYapanIsim(emp.getFirstname()
	// .concat(" ").concat(emp.getLastname()));
	// if (ruloTestCentikDarbeBoyunas != null
	// && ruloTestCentikDarbeBoyunas.size() > 0) {
	// ruloTestCentikDarbeBoyunaManager
	// .updateEntity(selectedRuloTestCentikDarbeBoyuna);
	//
	// } else {
	// selectedRuloTestCentikDarbeBoyuna.setRulo(selectedRulo);
	// ruloTestCentikDarbeBoyunaManager
	// .enterNew(selectedRuloTestCentikDarbeBoyuna);
	//
	// }
	// FacesContext context = FacesContext.getCurrentInstance();
	// context.addMessage(null, new FacesMessage(
	// "İşlem başarılı bir şekilde tamamlanmıştır."));
	// }

	public void updateSelectedRuloTestCentikDarbeBoyuna() {

		loadReplySelectedOrder();

		ayniRulolariBul(selectedRulo.getRuloDokumNo(), selectedRulo
				.getRuloOzellikBoyutsal().getRuloOzellikBoyutsalEtKalinligi());

		for (int i = 0; i < ayniDokumAyniKalinlikRuloList.size(); i++) {

			if (i > 0) {// id nullanıyor yeni giriş için
				selectedRuloTestCentikDarbeBoyuna
						.setRuloTestCentikDarbeId(null);
			}

			List<RuloTestCentikDarbeBoyuna> ruloTestCentikDarbeBoyunas = ayniDokumAyniKalinlikRuloList
					.get(i).getRuloTestCentikDarbeBoyuna();
			RuloTestCentikDarbeBoyunaManager ruloTestCentikDarbeBoyunaManager = new RuloTestCentikDarbeBoyunaManager();
			Employee emp = userBean.getUser().getEmployee();
			selectedRuloTestCentikDarbeBoyuna.setIslemYapanId(emp
					.getEmployeeId());
			selectedRuloTestCentikDarbeBoyuna.setIslemYapanIsim(emp
					.getFirstname().concat(" ").concat(emp.getLastname()));
			if (ruloTestCentikDarbeBoyunas != null
					&& ruloTestCentikDarbeBoyunas.size() > 0) {
				ruloTestCentikDarbeBoyunaManager
						.updateEntity(selectedRuloTestCentikDarbeBoyuna);

			} else {
				selectedRuloTestCentikDarbeBoyuna
						.setRulo(ayniDokumAyniKalinlikRuloList.get(i));
				ruloTestCentikDarbeBoyunaManager
						.enterNew(selectedRuloTestCentikDarbeBoyuna);

			}
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							"Rulo Boyuna Çentik Darbe Testi "
									+ ayniDokumAyniKalinlikRuloList.get(i)
											.getRuloYilAndRuloKimlikId()
									+ " numaralı rulo için başarılı bir şekilde tamamlanmıştır."));
		}
	}

	// public void updateSelectedRuloTestCentikDarbeEnine() {
	// loadReplySelectedOrder();
	// List<RuloTestCentikDarbeEnine> ruloTestCentikDarbeEnines = selectedRulo
	// .getRuloTestCentikDarbeEnine();
	// RuloTestCentikDarbeEnineManager ruloTestCentikDarbeEnineManager = new
	// RuloTestCentikDarbeEnineManager();
	// Employee emp = userBean.getUser().getEmployee();
	// selectedRuloTestCentikDarbeEnine.setIslemYapanId(emp.getEmployeeId());
	// selectedRuloTestCentikDarbeEnine.setIslemYapanIsim(emp.getFirstname()
	// .concat(" ").concat(emp.getLastname()));
	// if (ruloTestCentikDarbeEnines != null
	// && ruloTestCentikDarbeEnines.size() > 0) {
	// ruloTestCentikDarbeEnineManager
	// .updateEntity(selectedRuloTestCentikDarbeEnine);
	//
	// } else {
	// selectedRuloTestCentikDarbeEnine.setRulo(selectedRulo);
	// ruloTestCentikDarbeEnineManager
	// .enterNew(selectedRuloTestCentikDarbeEnine);
	//
	// }
	// FacesContext context = FacesContext.getCurrentInstance();
	// context.addMessage(null, new FacesMessage(
	// "İşlem başarılı bir şekilde tamamlanmıştır."));
	// }

	public void updateSelectedRuloTestCentikDarbeEnine() {

		loadReplySelectedOrder();

		ayniRulolariBul(selectedRulo.getRuloDokumNo(), selectedRulo
				.getRuloOzellikBoyutsal().getRuloOzellikBoyutsalEtKalinligi());

		for (int i = 0; i < ayniDokumAyniKalinlikRuloList.size(); i++) {

			if (i > 0) {// id nullanıyor yeni giriş için
				selectedRuloTestCentikDarbeEnine.setRuloTestCentikDarbeId(null);
			}

			List<RuloTestCentikDarbeEnine> ruloTestCentikDarbeEnines = ayniDokumAyniKalinlikRuloList
					.get(i).getRuloTestCentikDarbeEnine();
			RuloTestCentikDarbeEnineManager ruloTestCentikDarbeEnineManager = new RuloTestCentikDarbeEnineManager();
			Employee emp = userBean.getUser().getEmployee();
			selectedRuloTestCentikDarbeEnine.setIslemYapanId(emp
					.getEmployeeId());
			selectedRuloTestCentikDarbeEnine.setIslemYapanIsim(emp
					.getFirstname().concat(" ").concat(emp.getLastname()));
			if (ruloTestCentikDarbeEnines != null
					&& ruloTestCentikDarbeEnines.size() > 0) {
				ruloTestCentikDarbeEnineManager
						.updateEntity(selectedRuloTestCentikDarbeEnine);

			} else {
				selectedRuloTestCentikDarbeEnine
						.setRulo(ayniDokumAyniKalinlikRuloList.get(i));
				ruloTestCentikDarbeEnineManager
						.enterNew(selectedRuloTestCentikDarbeEnine);

			}
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							"Rulo Enine Çentik Darbe Testi "
									+ ayniDokumAyniKalinlikRuloList.get(i)
											.getRuloYilAndRuloKimlikId()
									+ " numaralı rulo için başarılı bir şekilde tamamlanmıştır."));
		}
	}

	// public void updateSelectedRuloTestCentikDarbeUretici() {
	// loadReplySelectedOrder();
	// List<RuloTestCentikDarbeUretici> ruloTestCentikDarbeUreticis =
	// selectedRulo
	// .getRuloTestCentikDarbeUretici();
	// RuloTestCentikDarbeUreticiManager ruloTestCentikDarbeUreticiManager = new
	// RuloTestCentikDarbeUreticiManager();
	// Employee emp = userBean.getUser().getEmployee();
	// selectedRuloTestCentikDarbeUretici.setIslemYapanId(emp.getEmployeeId());
	// selectedRuloTestCentikDarbeUretici.setIslemYapanIsim(emp.getFirstname()
	// .concat(" ").concat(emp.getLastname()));
	// if (ruloTestCentikDarbeUreticis != null
	// && ruloTestCentikDarbeUreticis.size() > 0) {
	// ruloTestCentikDarbeUreticiManager
	// .updateEntity(selectedRuloTestCentikDarbeUretici);
	//
	// } else {
	// selectedRuloTestCentikDarbeUretici.setRulo(selectedRulo);
	// ruloTestCentikDarbeUreticiManager
	// .enterNew(selectedRuloTestCentikDarbeUretici);
	// }
	// FacesContext context = FacesContext.getCurrentInstance();
	// context.addMessage(null, new FacesMessage(
	// "İşlem başarılı bir şekilde tamamlanmıştır."));
	// }

	public void updateSelectedRuloTestCentikDarbeUretici() {

		loadReplySelectedOrder();

		ayniRulolariBul(selectedRulo.getRuloDokumNo(), selectedRulo
				.getRuloOzellikBoyutsal().getRuloOzellikBoyutsalEtKalinligi());

		for (int i = 0; i < ayniDokumAyniKalinlikRuloList.size(); i++) {

			if (i > 0) {// id nullanıyor yeni giriş için
				selectedRuloTestCentikDarbeUretici
						.setRuloTestCentikDarbeId(null);
			}

			List<RuloTestCentikDarbeUretici> ruloTestCentikDarbeUreticis = ayniDokumAyniKalinlikRuloList
					.get(i).getRuloTestCentikDarbeUretici();
			RuloTestCentikDarbeUreticiManager ruloTestCentikDarbeUreticiManager = new RuloTestCentikDarbeUreticiManager();
			Employee emp = userBean.getUser().getEmployee();
			selectedRuloTestCentikDarbeUretici.setIslemYapanId(emp
					.getEmployeeId());
			selectedRuloTestCentikDarbeUretici.setIslemYapanIsim(emp
					.getFirstname().concat(" ").concat(emp.getLastname()));
			if (ruloTestCentikDarbeUreticis != null
					&& ruloTestCentikDarbeUreticis.size() > 0) {
				ruloTestCentikDarbeUreticiManager
						.updateEntity(selectedRuloTestCentikDarbeUretici);

			} else {
				selectedRuloTestCentikDarbeUretici
						.setRulo(ayniDokumAyniKalinlikRuloList.get(i));
				ruloTestCentikDarbeUreticiManager
						.enterNew(selectedRuloTestCentikDarbeUretici);
			}
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							"Rulo Üretici Çentik Darbe Testi "
									+ ayniDokumAyniKalinlikRuloList.get(i)
											.getRuloYilAndRuloKimlikId()
									+ " numaralı rulo için başarılı bir şekilde tamamlanmıştır."));
		}
	}

	// public void updateSelectedRuloTestKimyasalUretici() {
	// loadReplySelectedOrder();
	// List<RuloTestKimyasalUretici> ruloTestKimyasalUreticis = selectedRulo
	// .getRuloTestKimyasalUretici();
	// RuloTestKimyasalUreticiManager ruloTestKimyasalUreticiManager = new
	// RuloTestKimyasalUreticiManager();
	// Employee emp = userBean.getUser().getEmployee();
	// selectedRuloTestKimyasalUretici.setIslemYapanId(emp.getEmployeeId());
	// selectedRuloTestKimyasalUretici.setIslemYapanIsim(emp.getFirstname()
	// .concat(" ").concat(emp.getLastname()));
	// if (ruloTestKimyasalUreticis != null
	// && ruloTestKimyasalUreticis.size() > 0) {
	// ruloTestKimyasalUreticiManager
	// .updateEntity(selectedRuloTestKimyasalUretici);
	//
	// } else {
	// selectedRuloTestKimyasalUretici.setRulo(selectedRulo);
	// ruloTestKimyasalUreticiManager
	// .enterNew(selectedRuloTestKimyasalUretici);
	//
	// }
	// FacesContext context = FacesContext.getCurrentInstance();
	// context.addMessage(null, new FacesMessage(
	// "İşlem başarılı bir şekilde tamamlanmıştır."));
	// }

	public void updateSelectedRuloTestKimyasalUretici() {

		loadReplySelectedOrder();

		ayniRulolariBul(selectedRulo.getRuloDokumNo(), selectedRulo
				.getRuloOzellikBoyutsal().getRuloOzellikBoyutsalEtKalinligi());

		for (int i = 0; i < ayniDokumAyniKalinlikRuloList.size(); i++) {

			if (i > 0) {// id nullanıyor yeni giriş için
				selectedRuloTestKimyasalUretici
						.setRuloTestKimyasalUreticiId(null);
			}

			List<RuloTestKimyasalUretici> ruloTestKimyasalUreticis = ayniDokumAyniKalinlikRuloList
					.get(i).getRuloTestKimyasalUretici();
			RuloTestKimyasalUreticiManager ruloTestKimyasalUreticiManager = new RuloTestKimyasalUreticiManager();
			Employee emp = userBean.getUser().getEmployee();
			selectedRuloTestKimyasalUretici
					.setIslemYapanId(emp.getEmployeeId());
			selectedRuloTestKimyasalUretici.setIslemYapanIsim(emp
					.getFirstname().concat(" ").concat(emp.getLastname()));
			if (ruloTestKimyasalUreticis != null
					&& ruloTestKimyasalUreticis.size() > 0) {
				ruloTestKimyasalUreticiManager
						.updateEntity(selectedRuloTestKimyasalUretici);

			} else {
				selectedRuloTestKimyasalUretici
						.setRulo(ayniDokumAyniKalinlikRuloList.get(i));
				ruloTestKimyasalUreticiManager
						.enterNew(selectedRuloTestKimyasalUretici);

			}
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							"Rulo Kimyasal Analiz Testi (Üretici) "
									+ ayniDokumAyniKalinlikRuloList.get(i)
											.getRuloYilAndRuloKimlikId()
									+ " numaralı rulo için başarılı bir şekilde tamamlanmıştır."));
		}
	}

	public void updateSelectedRuloOzellikBoyutsal() {
		loadReplySelectedOrder();
		RuloOzellikBoyutsalManager ruloOzellikBoyutsalManager = new RuloOzellikBoyutsalManager();
		selectedRuloOzellikBoyutsal.setDurum(Rulo.RULO_TEST_DURUM_KABUL);
		Employee emp = userBean.getUser().getEmployee();
		selectedRuloOzellikBoyutsal.setIslemYapanId(emp.getEmployeeId());
		selectedRuloOzellikBoyutsal.setIslemYapanIsim(emp.getFirstname()
				.concat(" ").concat(emp.getLastname()));
		selectedRuloOzellikBoyutsal
				.setRuloOzellikBoyutsalKalanMiktar(selectedRuloOzellikBoyutsal
						.getRuloOzellikBoyutsalMiktar());
		ruloOzellikBoyutsalManager.updateEntity(selectedRuloOzellikBoyutsal);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Boyutsal Özellikler "
				+ selectedRulo.getRuloYilAndRuloKimlikId()
				+ " numaralı rulo için başarılı bir şekilde tamamlanmıştır."));

	}

	public void updateSelectedRuloOzellikFizikselGirdi() {
		loadReplySelectedOrder();
		RuloOzellikFizikselGirdiManager ruloOzellikFizikselGirdiManager = new RuloOzellikFizikselGirdiManager();
		Employee emp = userBean.getUser().getEmployee();
		selectedRuloOzellikFizikselGirdi.setIslemYapanId(emp.getEmployeeId());
		selectedRuloOzellikFizikselGirdi.setIslemYapanIsim(emp.getFirstname()
				.concat(" ").concat(emp.getLastname()));
		ruloOzellikFizikselGirdiManager
				.updateEntity(selectedRuloOzellikFizikselGirdi);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"Rulo Fiziksel Özellikler (Girdi) "
								+ selectedRulo.getRuloYilAndRuloKimlikId()
								+ " numaralı rulo için başarılı bir şekilde tamamlanmıştır."));
	}

	public void updateSelectedRuloOzellikFizikselUretici() {
		loadReplySelectedOrder();
		RuloOzellikFizikselUreticiManager ruloOzellikFizikselUreticiManager = new RuloOzellikFizikselUreticiManager();
		Employee emp = userBean.getUser().getEmployee();
		selectedRuloOzellikFizikselUretici.setIslemYapanId(emp.getEmployeeId());
		selectedRuloOzellikFizikselUretici.setIslemYapanIsim(emp.getFirstname()
				.concat(" ").concat(emp.getLastname()));
		ruloOzellikFizikselUreticiManager
				.updateEntity(selectedRuloOzellikFizikselUretici);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"Rulo Fiziksel Özellikler (Üretici) "
								+ selectedRulo.getRuloYilAndRuloKimlikId()
								+ " numaralı rulo için başarılı bir şekilde tamamlanmıştır."));
	}

	public void updateSelectedRuloTestDwttUretici() {

		ayniRulolariBul(selectedRulo.getRuloDokumNo(), selectedRulo
				.getRuloOzellikBoyutsal().getRuloOzellikBoyutsalEtKalinligi());

		for (int i = 0; i < ayniDokumAyniKalinlikRuloList.size(); i++) {

			if (i > 0) {// id nullanıyor yeni giriş için
				selectedRuloTestDwttUretici.setRuloDwttUreticiId(null);
			}

			List<RuloTestDwttUretici> ruloTestDwttUreticis = ayniDokumAyniKalinlikRuloList
					.get(i).getRuloTestDwttUretici();
			RuloTestDwttUreticiManager ruloTestDwttUreticiManager = new RuloTestDwttUreticiManager();
			Employee emp = userBean.getUser().getEmployee();
			selectedRuloTestDwttUretici.setIslemYapanId(emp.getEmployeeId());
			selectedRuloTestDwttUretici.setIslemYapanIsim(emp.getFirstname()
					.concat(" ").concat(emp.getLastname()));
			if (ruloTestDwttUreticis != null && ruloTestDwttUreticis.size() > 0) {
				ruloTestDwttUreticiManager
						.updateEntity(selectedRuloTestDwttUretici);

			} else {
				selectedRuloTestDwttUretici
						.setRulo(ayniDokumAyniKalinlikRuloList.get(i));
				ruloTestDwttUreticiManager
						.enterNew(selectedRuloTestDwttUretici);

			}
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							"Rulo DWTT (Üretici) "
									+ ayniDokumAyniKalinlikRuloList.get(i)
											.getRuloYilAndRuloKimlikId()
									+ " numaralı rulo için başarılı bir şekilde tamamlanmıştır."));

		}
		//
		// loadReplySelectedOrder();
		// RuloTestDwttUreticiManager ruloTestDwttUreticiManager = new
		// RuloTestDwttUreticiManager();
		// Employee emp = userBean.getUser().getEmployee();
		// selectedRuloTestDwttUretici.setIslemYapanId(emp.getEmployeeId());
		// selectedRuloTestDwttUretici.setIslemYapanIsim(emp.getFirstname()
		// .concat(" ").concat(emp.getLastname()));
		// selectedRuloTestDwttUretici.setRulo(selectedRulo);
		// ruloTestDwttUreticiManager.updateEntity(selectedRuloTestDwttUretici);
		// FacesContext context = FacesContext.getCurrentInstance();
		// context.addMessage(null, new FacesMessage("Rulo DWTT (Üretici) "
		// + selectedRulo.getRuloYilAndRuloKimlikId()
		// + " numaralı rulo için başarılı bir şekilde tamamlanmıştır."));
	}

	public void updateSelectedRuloTestDwttGirdi() {

		ayniRulolariBul(selectedRulo.getRuloDokumNo(), selectedRulo
				.getRuloOzellikBoyutsal().getRuloOzellikBoyutsalEtKalinligi());

		for (int i = 0; i < ayniDokumAyniKalinlikRuloList.size(); i++) {

			if (i > 0) {// id nullanıyor yeni giriş için
				selectedRuloTestDwttGirdi.setRuloDwttGirdiId(null);
			}

			List<RuloTestDwttGirdi> ruloTestDwttGirdis = ayniDokumAyniKalinlikRuloList
					.get(i).getRuloTestDwttGirdi();
			RuloTestDwttGirdiManager ruloTestDwttGirdiManager = new RuloTestDwttGirdiManager();
			Employee emp = userBean.getUser().getEmployee();
			selectedRuloTestDwttGirdi.setIslemYapanId(emp.getEmployeeId());
			selectedRuloTestDwttGirdi.setIslemYapanIsim(emp.getFirstname()
					.concat(" ").concat(emp.getLastname()));
			if (ruloTestDwttGirdis != null && ruloTestDwttGirdis.size() > 0) {
				ruloTestDwttGirdiManager
						.updateEntity(selectedRuloTestDwttGirdi);

			} else {
				selectedRuloTestDwttGirdi.setRulo(ayniDokumAyniKalinlikRuloList
						.get(i));
				ruloTestDwttGirdiManager.enterNew(selectedRuloTestDwttGirdi);

			}
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							"Rulo DWTT (Girdi) "
									+ ayniDokumAyniKalinlikRuloList.get(i)
											.getRuloYilAndRuloKimlikId()
									+ " numaralı rulo için başarılı bir şekilde tamamlanmıştır."));

		}
		//
		// loadReplySelectedOrder();
		// RuloTestDwttUreticiManager ruloTestDwttUreticiManager = new
		// RuloTestDwttUreticiManager();
		// Employee emp = userBean.getUser().getEmployee();
		// selectedRuloTestDwttUretici.setIslemYapanId(emp.getEmployeeId());
		// selectedRuloTestDwttUretici.setIslemYapanIsim(emp.getFirstname()
		// .concat(" ").concat(emp.getLastname()));
		// selectedRuloTestDwttUretici.setRulo(selectedRulo);
		// ruloTestDwttUreticiManager.updateEntity(selectedRuloTestDwttUretici);
		// FacesContext context = FacesContext.getCurrentInstance();
		// context.addMessage(null, new FacesMessage("Rulo DWTT (Üretici) "
		// + selectedRulo.getRuloYilAndRuloKimlikId()
		// + " numaralı rulo için başarılı bir şekilde tamamlanmıştır."));
	}

	/*
	 * Load Rulo Property
	 */

	public void loadSelectedRulo() {
		reset();
		loadSelectedRuloOzellikBoyutsal();
		loadSelectedRuloOzellikFizikselGirdi();
		loadSelectedRuloOzellikFizikselUretici();
		loadSelectedRuloTestCekmeBoyuna();
		loadSelectedRuloTestCekmeEnine();
		loadSelectedRuloTestCentikDarbeUretici();
		loadSelectedRuloTestCentikDarbeEnine();
		loadSelectedRuloTestCentikDarbeBoyuna();
		loadSelectedRuloTestKimyasalGirdi();
		loadSelectedRuloTestKimyasalUretici();
		loadSelectedRuloTestDwttUretici();
		loadSelectedRuloTestDwttGirdi();
	}

	public void loadSelectedRuloTestKimyasalGirdi() {
		loadReplySelectedOrder();
		List<RuloTestKimyasalGirdi> ruloTestKimyasalGirdis = selectedRulo
				.getRuloTestKimyasalGirdi();
		if (ruloTestKimyasalGirdis != null && ruloTestKimyasalGirdis.size() > 0) {
			selectedRuloTestKimyasalGirdi = selectedRulo
					.getRuloTestKimyasalGirdi().get(0);
		} else {
			selectedRuloTestKimyasalGirdi = new RuloTestKimyasalGirdi();
		}
	}

	public void loadSelectedRuloTestCekmeBoyuna() {
		loadReplySelectedOrder();
		List<RuloTestCekmeBoyuna> ruloTestCekmeBoyunas = selectedRulo
				.getRuloTestCekmeBoyuna();
		if (ruloTestCekmeBoyunas != null && ruloTestCekmeBoyunas.size() > 0) {
			selectedRuloTestCekmeBoyuna = selectedRulo.getRuloTestCekmeBoyuna()
					.get(0);
		} else {
			selectedRuloTestCekmeBoyuna = new RuloTestCekmeBoyuna();
		}
	}

	public void loadSelectedRuloTestKimyasalUretici() {
		loadReplySelectedOrder();
		List<RuloTestKimyasalUretici> ruloTestKimyasalUreticis = selectedRulo
				.getRuloTestKimyasalUretici();
		if (ruloTestKimyasalUreticis != null
				&& ruloTestKimyasalUreticis.size() > 0) {
			selectedRuloTestKimyasalUretici = selectedRulo
					.getRuloTestKimyasalUretici().get(0);
		} else {
			selectedRuloTestKimyasalUretici = new RuloTestKimyasalUretici();
		}
	}

	public void loadRuloForCustomer() {
		if (selectedRulo.getSalesCustomer() == null) {
			selectedRulo.setSalesCustomer(new SalesCustomer());
		} else {
			selectedRulo.setSalesCustomerId(selectedRulo.getSalesCustomer()
					.getSalesCustomerId());
		}
	}

	public void loadSelectedRuloTestCekmeEnine() {
		loadReplySelectedOrder();
		List<RuloTestCekmeEnine> ruloTestCekmeEnines = selectedRulo
				.getRuloTestCekmeEnine();
		if (ruloTestCekmeEnines != null && ruloTestCekmeEnines.size() > 0) {
			selectedRuloTestCekmeEnine = selectedRulo.getRuloTestCekmeEnine()
					.get(0);
		} else {
			selectedRuloTestCekmeEnine = new RuloTestCekmeEnine();
		}
	}

	public void loadSelectedRuloTestCentikDarbeBoyuna() {
		loadReplySelectedOrder();
		List<RuloTestCentikDarbeBoyuna> centikDarbeBoyunas = selectedRulo
				.getRuloTestCentikDarbeBoyuna();
		if (centikDarbeBoyunas != null && centikDarbeBoyunas.size() > 0) {
			selectedRuloTestCentikDarbeBoyuna = selectedRulo
					.getRuloTestCentikDarbeBoyuna().get(0);
		} else {
			selectedRuloTestCentikDarbeBoyuna = new RuloTestCentikDarbeBoyuna();
		}
	}

	public void loadSelectedRuloTestCentikDarbeEnine() {
		loadReplySelectedOrder();
		List<RuloTestCentikDarbeEnine> ruloTestCentikDarbeEnines = selectedRulo
				.getRuloTestCentikDarbeEnine();
		if (ruloTestCentikDarbeEnines != null
				&& ruloTestCentikDarbeEnines.size() > 0) {
			selectedRuloTestCentikDarbeEnine = selectedRulo
					.getRuloTestCentikDarbeEnine().get(0);
		} else {
			selectedRuloTestCentikDarbeEnine = new RuloTestCentikDarbeEnine();
		}
	}

	public void loadSelectedRuloTestCentikDarbeUretici() {
		loadReplySelectedOrder();
		List<RuloTestCentikDarbeUretici> ruloTestCentikDarbeUreticis = selectedRulo
				.getRuloTestCentikDarbeUretici();
		if (ruloTestCentikDarbeUreticis != null
				&& ruloTestCentikDarbeUreticis.size() > 0) {
			selectedRuloTestCentikDarbeUretici = selectedRulo
					.getRuloTestCentikDarbeUretici().get(0);
		} else {
			selectedRuloTestCentikDarbeUretici = new RuloTestCentikDarbeUretici();
		}
	}

	public void loadSelectedRuloOzellikBoyutsal() {
		loadReplySelectedOrder();
		selectedRuloOzellikBoyutsal = selectedRulo.getRuloOzellikBoyutsal();
	}

	public void loadSelectedRuloOzellikFizikselGirdi() {
		loadReplySelectedOrder();
		selectedRuloOzellikFizikselGirdi = selectedRulo
				.getRuloOzellikFizikselGirdi();
	}

	public void loadSelectedRuloOzellikFizikselUretici() {
		loadReplySelectedOrder();
		selectedRuloOzellikFizikselUretici = selectedRulo
				.getRuloOzellikFizikselUretici();
	}

	public void loadSelectedRuloTestDwttUretici() {
		loadReplySelectedOrder();
		List<RuloTestDwttUretici> ruloTestDwttUreticis = selectedRulo
				.getRuloTestDwttUretici();
		if (ruloTestDwttUreticis != null && ruloTestDwttUreticis.size() > 0) {
			selectedRuloTestDwttUretici = selectedRulo.getRuloTestDwttUretici()
					.get(0);
		} else {
			selectedRuloTestDwttUretici = new RuloTestDwttUretici();
		}
	}

	public void loadSelectedRuloTestDwttGirdi() {
		loadReplySelectedOrder();
		List<RuloTestDwttGirdi> ruloTestDwttGirdis = selectedRulo
				.getRuloTestDwttGirdi();
		if (ruloTestDwttGirdis != null && ruloTestDwttGirdis.size() > 0) {
			selectedRuloTestDwttGirdi = selectedRulo.getRuloTestDwttGirdi()
					.get(0);
		} else {
			selectedRuloTestDwttGirdi = new RuloTestDwttGirdi();
		}
	}

	public void barcodeBas() throws IOException, DocumentException {

		// step 1
		// A4
		// selectPath();

		theFile = new File(path + "rulo_" + selectedRulo.getRuloYil() + "_"
				+ selectedRulo.getRuloKimlikId() + ".pdf");

		Document document = new Document(new Rectangle(210, 297));
		// A5
		// Document document = new Document(new Rectangle(148, 210));
		// A7 barkod buyuklugu
		// Document document = new Document(new Rectangle(74, 105));
		// step 2

		PdfWriter writer = PdfWriter.getInstance(document,
				new FileOutputStream(theFile));

		// step 3
		document.open();
		// step 4
		PdfContentByte cb = writer.getDirectContent();

		Barcode128 code128 = new Barcode128();
		code128.setCode(selectedRulo.getRuloYil() + "/"
				+ selectedRulo.getRuloKimlikId());
		code128.setChecksumText(true);
		code128.setGenerateChecksum(true);
		code128.setStartStopText(true);

		Image image1 = code128.createImageWithBarcode(cb, null, null);
		image1.setAbsolutePosition(2, 233);
		// image1.scaleAbsolute(74, 105);
		document.add(image1);
		Image image2 = code128.createImageWithBarcode(cb, null, null);
		image2.setAbsolutePosition(110, 233);
		// image2.scaleAbsolute(74, 105);
		document.add(image2);
		Image image3 = code128.createImageWithBarcode(cb, null, null);
		image3.setAbsolutePosition(2, 159);
		// image3.scaleAbsolute(72, 84);
		document.add(image3);
		Image image4 = code128.createImageWithBarcode(cb, null, null);
		image4.setAbsolutePosition(110, 159);
		// image4.scaleAbsolute(105, 84);
		document.add(image4);

		document.close();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(selectedRulo.getRuloYil()
				+ "/" + selectedRulo.getRuloKimlikId()
				+ " Numaralı Rulo Barkodu Üretildi!"));

		// ruloya a barkodNo yazılması
		String barkodNo = selectedRulo.getRuloYil() + "/"
				+ selectedRulo.getRuloKimlikId();
		RuloManager ruloMan = new RuloManager();
		selectedRulo.setRuloBarkodNo(barkodNo);
		ruloMan.updateEntity(selectedRulo);

		downloadedFileName = "rulo_" + selectedRulo.getRuloYil() + "_"
				+ selectedRulo.getRuloKimlikId() + ".pdf";
	}

	@SuppressWarnings("resource")
	public StreamedContent getDownloadedFile() {

		try {
			if (downloadedFileName != null) {
				RandomAccessFile accessFile = new RandomAccessFile(path
						+ downloadedFileName, "r");
				byte[] downloadByte = new byte[(int) accessFile.length()];
				accessFile.read(downloadByte);
				InputStream stream = new ByteArrayInputStream(downloadByte);

				String suffix = FilenameUtils.getExtension(downloadedFileName);
				String contentType = "text/plain";
				if (suffix.contentEquals("jpg") || suffix.contentEquals("png")
						|| suffix.contentEquals("gif")) {
					contentType = "image/" + contentType;
				} else if (suffix.contentEquals("doc")
						|| suffix.contentEquals("docx")) {
					contentType = "application/msword";
				} else if (suffix.contentEquals("xls")
						|| suffix.contentEquals("xlsx")) {
					contentType = "application/vnd.ms-excel";
				} else if (suffix.contentEquals("pdf")) {
					contentType = "application/pdf";
				}
				downloadedFile = new DefaultStreamedContent(stream,
						"contentType", downloadedFileName);

				stream.close();
			} else {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"Lütfen Barkod Oluşturunuz, Barkod Bulunamadı!"));
			}
		} catch (Exception e) {
			System.out.println("RuloBean.getDownloadedFile():" + e.toString());
		}
		return downloadedFile;
	}

	// rulo aciliş sayfası

	public void goToCoilPage() {

		FacesContextUtils.redirect(config.getPageUrl().COIL_PAGE);
	}

	// rulo pipe link değişim aciliş sayfası

	public void goToRuloPipeLinkEditPage() {

		FacesContextUtils
				.redirect(config.getPageUrl().RULO_PIPE_LINK_EDIT_PAGE);
	}

	private InputStream excelFile;

	public void readFullExcelFile(FileUploadEvent event) {
		// public void readExcelFile() {

		RuloManager ruloManager = new RuloManager();
		RuloOzellikBoyutsalManager boyutsalManager = new RuloOzellikBoyutsalManager();
		RuloTestCekmeEnineManager cekmeEnineManager = new RuloTestCekmeEnineManager();
		RuloTestKimyasalGirdiManager kimyasalGirdiManager = new RuloTestKimyasalGirdiManager();
		SalesCustomerManager customerManager = new SalesCustomerManager();

		NumberFormat nf = NumberFormat.getInstance(Locale.US);

		newRulo = new Rulo();

		try {

			excelFile = event.getFile().getInputstream();
			// excelFile = new FileInputStream(new
			// File("D:/ruloExceldenAlma.xls"));
		} catch (IOException e1) {

			e1.printStackTrace();
		}

		try {
			HSSFWorkbook wb = new HSSFWorkbook(excelFile);
			HSSFSheet sheet = wb.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			rowIterator.next();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through each columns
				Iterator<Cell> cellIterator = row.cellIterator();
				int i = 1;
				Double veriDouble = new Double(0);
				String veriString = "";
				Integer data = 0;

				newRulo = new Rulo();
				selectedRuloTestCekmeEnine = new RuloTestCekmeEnine();
				selectedRuloTestKimyasalGirdi = new RuloTestKimyasalGirdi();

				while (cellIterator.hasNext()) {

					Cell cell = cellIterator.next();
					if (i == 1) {
						try {
							veriString = cell.getStringCellValue();
							newRulo.setRuloYil(20 + veriString.substring(0, 2));
							newRulo.setRuloKimlikId(veriString.substring(3));
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA kimlik (i==1)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMLİK NUMARASINDA HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 2) {

						try {
							data = customerManager
									.findAllLike(SalesCustomer.class,
											"shortName",
											cell.getStringCellValue()).get(0)
									.getSalesCustomerId();
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA customer (i==2)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"FİRMA İSMİNDE HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
						newRulo.getSalesCustomer().setSalesCustomerId(data);
						newRulo.setSalesCustomerId(data);
					} else if (i == 3) {
						try {
							if (cell.getCellType() == 1) {
								newRulo.setRuloEtiketNo(cell
										.getStringCellValue());
							} else {
								veriDouble = cell.getNumericCellValue();
								data = veriDouble.intValue();
								newRulo.setRuloEtiketNo(data.toString());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA etiket (i==3)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"ETİKET NUMARASINDA HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 4) {

						try {
							if (cell.getCellType() == 1) {
								newRulo.setRuloBobinNo(cell
										.getStringCellValue());
							} else {
								veriDouble = cell.getNumericCellValue();
								data = veriDouble.intValue();
								newRulo.setRuloBobinNo(data.toString());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA bobin (i==4)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"BOBİN NUMARASINDA HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 5) {

						try {
							if (cell.getCellType() == 1) {
								newRulo.setRuloDokumNo(cell
										.getStringCellValue());
							} else {
								veriDouble = cell.getNumericCellValue();
								data = veriDouble.intValue();
								newRulo.setRuloDokumNo(data.toString());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA döküm (i==5)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"DÖKÜM NUMARASINDA HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 6) {

						try {
							if (cell.getCellType() == 1) {
								newRulo.setRuloKalite(cell.getStringCellValue());
							} else {
								veriDouble = cell.getNumericCellValue();
								data = veriDouble.intValue();
								newRulo.setRuloKalite(data.toString());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA kalite (i==6)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KALİTESİNDE HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 7) {

						try {
							veriDouble = cell.getNumericCellValue();
							newRulo.getRuloOzellikBoyutsal()
									.setRuloOzellikBoyutsalEtKalinligi(
											veriDouble.floatValue());
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA etkalinliği (i==7)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"ET KALINLIĞINDA HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 8) {
						try {
							veriDouble = cell.getNumericCellValue();
							newRulo.getRuloOzellikBoyutsal()
									.setRuloOzellikBoyutsalGenislik(
											veriDouble.floatValue());
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA etkalinliği (i==8)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"GENİŞLİKTE HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 9) {
						try {
							veriDouble = cell.getNumericCellValue();
							newRulo.getRuloOzellikBoyutsal()
									.setRuloOzellikBoyutsalMiktar(
											veriDouble.floatValue());
							newRulo.getRuloOzellikBoyutsal()
									.setRuloOzellikBoyutsalKalanMiktar(
											veriDouble.floatValue());
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA ağirlik (i==9)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"AĞIRLIKTA HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 10) {

						try {
							newRulo.setRuloTarih(cell.getDateCellValue());
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA tarih (i==10)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"GELİŞ TARİHİNDE HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getDateCellValue()));
							return;
						}
					} else if (i == 11) {

						try {
							veriDouble = cell.getNumericCellValue();
							selectedRuloTestCekmeEnine
									.setRuloTestCekmeEnineCekmeDayanci(veriDouble
											.floatValue());
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA çekmeMPa (i==11)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"ÇEKME MPa HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}

					} else if (i == 12) {

						try {
							selectedRuloTestCekmeEnine
									.setRuloTestCekmeEnineAkmaDayanci(veriDouble
											.floatValue());
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA akmaMPa (i==12)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"AKMA MPa HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}

					} else if (i == 13) {
						try {
							veriDouble = cell.getNumericCellValue();
							selectedRuloTestCekmeEnine
									.setRuloTestCekmeEnineUzama(veriDouble
											.floatValue());
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA uzama (i==13)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"UZAMADA HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 14) {

						try {
							veriDouble = cell.getNumericCellValue();
							selectedRuloTestCekmeEnine
									.setRuloTestCekmeEnineAkmaCekme(veriDouble
											.floatValue());
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA akma/cekme (i==14)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"AKMA/ÇEKME HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 15) {

						try {
							selectedRuloTestCekmeEnine
									.setRuloTestCekmeEnineTarih(cell
											.getDateCellValue());
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA tarih (i==15)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"ÇEKME TEST TARİHİNDE HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getDateCellValue()));
							return;
						}
					} else if (i == 16) {

						try {

							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi.setcOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiC(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi.setcOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiC(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiC(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiC(veriDouble
												.floatValue());
							}

						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA c (i==16)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'C' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 17) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setSiOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiSi(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setSiOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiSi(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiSi(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiSi(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA Sİ (i==17)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Si' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 18) {

						try {
							veriDouble = cell.getNumericCellValue();
							selectedRuloTestKimyasalGirdi
									.setRuloTestKimyasalGirdiMn(veriDouble
											.floatValue());
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA Mn (i==18)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Mn' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 19) {

						try {
							veriDouble = cell.getNumericCellValue();
							selectedRuloTestKimyasalGirdi
									.setRuloTestKimyasalGirdiP(veriDouble
											.floatValue());
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA P (i==19)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'P' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 20) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi.setsOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiS(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi.setsOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiS(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiS(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiS(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA S (i==20)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'S' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 21) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setCrOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiCr(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setCrOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiCr(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiCr(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiCr(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA cr (i==21)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Cr' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 22) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setNiOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiNi(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setNiOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiNi(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiNi(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiNi(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA Ni (i==22)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Ni' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 23) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setMoOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiMo(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setMoOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiMo(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiMo(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiMo(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA Mo (i==23)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Mo' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 24) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setCuOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiCu(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setCuOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiCu(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiCu(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiCu(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA cu (i==24)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Cu' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 25) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setAlOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiAl(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setAlOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiAl(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiAl(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiAl(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA Al (i==25)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Al' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 26) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setTiOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiTi(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setTiOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiTi(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiTi(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiTi(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA ti (i==26)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Ti' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 27) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi.setvOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiV(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi.setvOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiV(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiV(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiV(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA V (i==27)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'V' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 28) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setNbOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiNb(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setNbOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiNb(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiNb(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiNb(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA Nb (i==28)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Nb' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 29) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi.setwOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiW(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi.setwOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiW(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiW(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiW(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA w (i==29)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'W' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 30) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi.setbOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiB(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi.setbOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiB(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 10000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiB(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiB(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA B (i==30)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'B' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 31) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setAsOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiAs(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setAsOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiAs(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiAs(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiAs(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA As (i==31)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'As' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 32) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setSbOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiSb(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setSbOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiSb(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiSb(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiSb(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA Sb (i==32)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Sb' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 33) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setSnOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiSn(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setSnOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiSn(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiSn(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiSn(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA Sn (i==33)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Sn' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 34) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setPbOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiPb(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setPbOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiPb(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiPb(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiPb(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA Pb (i==34)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Pb' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 35) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setCaOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiCa(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setCaOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiCa(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 10000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiCa(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiCa(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA ca (i==35)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Ca' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 36) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi.setnOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiN(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi.setnOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiN(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 10000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiN(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiN(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA N (i==36)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'N' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 37) {

						try {
							veriDouble = cell.getNumericCellValue();
							selectedRuloTestKimyasalGirdi
									.setRuloTestKimyasalGirdiCe1(veriDouble
											.floatValue());
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA ce1 (i==37)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'CE1' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 38) {

						try {
							veriDouble = cell.getNumericCellValue();
							selectedRuloTestKimyasalGirdi
									.setRuloTestKimyasalGirdiCe2(veriDouble
											.floatValue());
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA ce2 (i==38)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'CE2' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 39) {

						try {
							selectedRuloTestKimyasalGirdi
									.setRuloTestKimyasalGirdiTarih(cell
											.getDateCellValue());
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA tarih (i==39)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST TARİHİNDE HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getDateCellValue()));
							return;
						}
					}
					i++;
				}

				newRulo.setRuloDurum(2);
				newRulo.setRuloNot("DENEME");
				newRulo.setIslemYapanIsim(userBean.getUser().getUsername());
				newRulo.setIslemYapanId(userBean.getUser().getId());

				newRulo.getRuloOzellikBoyutsal().setDurum(1);
				newRulo.getRuloOzellikBoyutsal().setIslemYapanIsim(
						userBean.getUser().getUsername());
				newRulo.getRuloOzellikBoyutsal().setIslemYapanId(
						userBean.getUser().getId());

				selectedRuloTestCekmeEnine.setDurum(1);
				selectedRuloTestCekmeEnine.setIslemYapanIsim(userBean.getUser()
						.getUsername());
				selectedRuloTestCekmeEnine.setIslemYapanId(userBean.getUser()
						.getId());
				selectedRuloTestCekmeEnine
						.setRuloTestCekmeEnineEtKalinligi(newRulo
								.getRuloOzellikBoyutsal()
								.getRuloOzellikBoyutsalEtKalinligi());
				selectedRuloTestCekmeEnine
						.setRuloTestCekmeEnineGenislik(newRulo
								.getRuloOzellikBoyutsal()
								.getRuloOzellikBoyutsalGenislik());

				ruloManager.enterNew(newRulo);
				boyutsalManager.updateEntity(newRulo.getRuloOzellikBoyutsal());

				selectedRuloTestCekmeEnine.setRulo(newRulo);
				cekmeEnineManager.enterNew(selectedRuloTestCekmeEnine);

				if (selectedRuloTestKimyasalGirdi.getRuloTestKimyasalGirdiC() > 0) {
					selectedRuloTestKimyasalGirdi.setRulo(newRulo);
					selectedRuloTestKimyasalGirdi.setDurum(1);
					kimyasalGirdiManager
							.enterNew(selectedRuloTestKimyasalGirdi);
				}
			}
			excelFile.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"EXCEL KISMI OLARAK YÜKLENDİ, LÜTFEN EKLENENLERİ KONTROL EDİP TEKRAR DENEYİNİZ!",
							null));
		} catch (IOException e) {
			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"EXCEL DOSYA HATASI, LÜTFEN EKLENENLERİ KONTROL EDİP TEKRAR DENEYİNİZ!",
							null));
		}

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"EXCEL VERİLERİ BAŞARIYLA EKLENMİŞTİR!"));

	}

	public void readPartialExcelFile(FileUploadEvent event) {
		// public void readExcelFile() {

		RuloManager ruloManager = new RuloManager();
		RuloTestCekmeEnineManager cekmeEnineManager = new RuloTestCekmeEnineManager();
		RuloTestKimyasalGirdiManager kimyasalGirdiManager = new RuloTestKimyasalGirdiManager();

		NumberFormat nf = NumberFormat.getInstance(Locale.US);

		selectedRulo = new Rulo();

		try {

			excelFile = event.getFile().getInputstream();
			// excelFile = new FileInputStream(new
			// File("D:/ruloExceldenAlma.xls"));
		} catch (IOException e1) {

			e1.printStackTrace();
		}

		try {
			HSSFWorkbook wb = new HSSFWorkbook(excelFile);
			HSSFSheet sheet = wb.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			rowIterator.next();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through each columns
				Iterator<Cell> cellIterator = row.cellIterator();
				int i = 1;
				Double veriDouble = new Double(0);
				String veriString = "";

				selectedRulo = new Rulo();
				selectedRuloTestCekmeEnine = new RuloTestCekmeEnine();
				selectedRuloTestKimyasalGirdi = new RuloTestKimyasalGirdi();

				while (cellIterator.hasNext()) {

					Cell cell = cellIterator.next();
					if (i == 1) {
						try {
							veriString = cell.getStringCellValue();
							selectedRulo = ruloManager.findByYilKimlik(
									20 + veriString.substring(0, 2),
									veriString.substring(3));
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA kimlik (i==1)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMLİK NUMARASINDA HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 2) {

						try {
							veriDouble = cell.getNumericCellValue();
							selectedRuloTestCekmeEnine
									.setRuloTestCekmeEnineCekmeDayanci(veriDouble
											.floatValue());
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA çekmeMPa (i==11)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"ÇEKME MPa HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}

					} else if (i == 3) {

						try {
							selectedRuloTestCekmeEnine
									.setRuloTestCekmeEnineAkmaDayanci(veriDouble
											.floatValue());
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA akmaMPa (i==12)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"AKMA MPa HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}

					} else if (i == 4) {
						try {
							veriDouble = cell.getNumericCellValue();
							selectedRuloTestCekmeEnine
									.setRuloTestCekmeEnineUzama(veriDouble
											.floatValue());
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA uzama (i==13)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"UZAMADA HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 5) {

						try {
							veriDouble = cell.getNumericCellValue();
							selectedRuloTestCekmeEnine
									.setRuloTestCekmeEnineAkmaCekme(veriDouble
											.floatValue());
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA akma/cekme (i==14)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"AKMA/ÇEKME HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 6) {

						try {
							selectedRuloTestCekmeEnine
									.setRuloTestCekmeEnineTarih(cell
											.getDateCellValue());
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA tarih (i==15)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"ÇEKME TEST TARİHİNDE HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getDateCellValue()));
							return;
						}
					} else if (i == 7) {

						try {

							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi.setcOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiC(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi.setcOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiC(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiC(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiC(veriDouble
												.floatValue());
							}

						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA c (i==16)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'C' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 8) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setSiOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiSi(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setSiOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiSi(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiSi(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiSi(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA Sİ (i==17)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Si' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 9) {

						try {
							veriDouble = cell.getNumericCellValue();
							selectedRuloTestKimyasalGirdi
									.setRuloTestKimyasalGirdiMn(veriDouble
											.floatValue());
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA Mn (i==18)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Mn' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 10) {

						try {
							veriDouble = cell.getNumericCellValue();
							selectedRuloTestKimyasalGirdi
									.setRuloTestKimyasalGirdiP(veriDouble
											.floatValue());
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA P (i==19)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'P' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 11) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi.setsOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiS(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi.setsOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiS(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiS(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiS(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA S (i==20)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'S' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 12) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setCrOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiCr(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setCrOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiCr(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiCr(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiCr(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA cr (i==21)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Cr' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 13) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setNiOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiNi(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setNiOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiNi(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiNi(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiNi(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA Ni (i==22)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Ni' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 14) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setMoOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiMo(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setMoOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiMo(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiMo(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiMo(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA Mo (i==23)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Mo' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 15) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setCuOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiCu(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setCuOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiCu(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiCu(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiCu(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA cu (i==24)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Cu' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 16) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setAlOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiAl(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setAlOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiAl(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiAl(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiAl(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA Al (i==25)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Al' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 17) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setTiOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiTi(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setTiOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiTi(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiTi(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiTi(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA ti (i==26)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Ti' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 18) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi.setvOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiV(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi.setvOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiV(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiV(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiV(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA V (i==27)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'V' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 19) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setNbOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiNb(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setNbOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiNb(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiNb(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiNb(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA Nb (i==28)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Nb' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 20) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi.setwOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiW(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi.setwOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiW(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiW(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiW(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA w (i==29)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'W' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 21) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi.setbOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiB(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi.setbOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiB(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 10000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiB(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiB(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA B (i==30)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'B' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 22) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setAsOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiAs(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setAsOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiAs(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiAs(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiAs(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA As (i==31)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'As' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 23) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setSbOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiSb(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setSbOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiSb(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiSb(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiSb(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA Sb (i==32)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Sb' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 24) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setSnOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiSn(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setSnOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiSn(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiSn(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiSn(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA Sn (i==33)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Sn' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 25) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setPbOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiPb(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setPbOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiPb(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiPb(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiPb(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA Pb (i==34)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Pb' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 26) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi
											.setCaOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiCa(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi
											.setCaOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiCa(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 10000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiCa(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiCa(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA ca (i==35)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'Ca' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 27) {

						try {
							if (cell.getCellType() == 1) {
								veriString = cell.getStringCellValue();
								if (veriString.substring(0, 1).equals(">")) {
									selectedRuloTestKimyasalGirdi.setnOnEk(">");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiN(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 1000);

								} else if (veriString.substring(0, 1).equals(
										"<")) {
									selectedRuloTestKimyasalGirdi.setnOnEk("<");
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiN(nf
													.parse(veriString
															.substring(2))
													.floatValue() / 10000);

								} else {
									selectedRuloTestKimyasalGirdi
											.setRuloTestKimyasalGirdiN(Float
													.parseFloat(veriString));
								}
							} else {
								veriDouble = cell.getNumericCellValue();
								selectedRuloTestKimyasalGirdi
										.setRuloTestKimyasalGirdiN(veriDouble
												.floatValue());
							}
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA N (i==36)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'N' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 28) {

						try {
							veriDouble = cell.getNumericCellValue();
							selectedRuloTestKimyasalGirdi
									.setRuloTestKimyasalGirdiCe1(veriDouble
											.floatValue());
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA ce1 (i==37)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'CE1' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 29) {

						try {
							veriDouble = cell.getNumericCellValue();
							selectedRuloTestKimyasalGirdi
									.setRuloTestKimyasalGirdiCe2(veriDouble
											.floatValue());
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA ce2 (i==38)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST 'CE2' HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getStringCellValue()));
							return;
						}
					} else if (i == 30) {

						try {
							selectedRuloTestKimyasalGirdi
									.setRuloTestKimyasalGirdiTarih(cell
											.getDateCellValue());
						} catch (Exception e2) {

							System.out
									.println(" ruloBean.readExcelFile-HATA tarih (i==39)");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(null, new FacesMessage(
									"KİMYASAL TEST TARİHİNDE HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"
											+ cell.getDateCellValue()));
							return;
						}
					}
					i++;
				}

				selectedRuloTestCekmeEnine.setDurum(1);
				selectedRuloTestCekmeEnine.setIslemYapanIsim(userBean.getUser()
						.getUsername());
				selectedRuloTestCekmeEnine.setIslemYapanId(userBean.getUser()
						.getId());
				selectedRuloTestCekmeEnine
						.setRuloTestCekmeEnineEtKalinligi(selectedRulo
								.getRuloOzellikBoyutsal()
								.getRuloOzellikBoyutsalEtKalinligi());
				selectedRuloTestCekmeEnine
						.setRuloTestCekmeEnineGenislik(selectedRulo
								.getRuloOzellikBoyutsal()
								.getRuloOzellikBoyutsalGenislik());

				selectedRuloTestCekmeEnine.setRulo(selectedRulo);
				cekmeEnineManager.enterNew(selectedRuloTestCekmeEnine);

				if (selectedRuloTestKimyasalGirdi.getRuloTestKimyasalGirdiC() > 0) {
					selectedRuloTestKimyasalGirdi.setRulo(selectedRulo);
					selectedRuloTestKimyasalGirdi.setDurum(1);
					kimyasalGirdiManager
							.enterNew(selectedRuloTestKimyasalGirdi);
				}
			}
			excelFile.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"EXCEL KISMI OLARAK YÜKLENDİ, LÜTFEN EKLENENLERİ KONTROL EDİP TEKRAR DENEYİNİZ!",
							null));
		} catch (IOException e) {
			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"EXCEL DOSYA HATASI, LÜTFEN EKLENENLERİ KONTROL EDİP TEKRAR DENEYİNİZ!",
							null));
		}

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"EXCEL VERİLERİ BAŞARIYLA EKLENMİŞTİR!"));

	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void ruloGozOlcuInspectionResultReportXlsx(Integer pipeId,
			Integer testId) {

		if (pipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();

			pipes = pipeManager.findByField(Pipe.class, "pipeId", pipeId);

			RuloTestKimyasalGirdiManager ruloTestKimyasalGirdiManager = new RuloTestKimyasalGirdiManager();

			SalesItemManager salesItemManager = new SalesItemManager();

			List<SalesItem> salesItems = new ArrayList<SalesItem>();

			List<RuloTestKimyasalGirdi> ruloTestKimyasalGirdi = new ArrayList<RuloTestKimyasalGirdi>();

			List<DestructiveTestsSpec> destructiveTestsSpecs = new ArrayList<DestructiveTestsSpec>();

			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			ruloTestKimyasalGirdi = ruloTestKimyasalGirdiManager.findByField(
					RuloTestKimyasalGirdi.class, "pipe.pipeId", pipeId);

			DestructiveTestsSpecManager dtsManager = new DestructiveTestsSpecManager();
			destructiveTestsSpecs.add(dtsManager.loadObject(
					DestructiveTestsSpec.class, testId));

			// rulo ozellikleri getirme
			RuloPipeLink ruloPipeLink = new RuloPipeLink();
			try {
				RuloPipeLinkManager rpm = new RuloPipeLinkManager();
				ruloPipeLink = rpm.findByPipeId(pipes.get(0).getPipeId())
						.get(0);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								"RULO BULMA HATASI!", null));
			}

			List<String> date = new ArrayList<String>();

			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("siparisRuloTestOzellik", destructiveTestsSpecs);
			dataMap.put("rulo", ruloPipeLink.getRulo());
			dataMap.put("ruloTestSonuc", ruloTestKimyasalGirdi);
			dataMap.put("date", date);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/rulo/FR-133.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"FR-133 RAPORU BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"FR-133 RAPORU OLUŞTURULAMADI, HATA!", null));
			e.printStackTrace();
		}
	}

	/*
	 * Setter Getter Method
	 */

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public Rulo getSelectedRulo() {
		return selectedRulo;
	}

	public void setSelectedRulo(Rulo selectedRulo) {
		this.selectedRulo = selectedRulo;
	}

	public Rulo getNewRulo() {
		return newRulo;
	}

	public void setNewRulo(Rulo newRulo) {
		this.newRulo = newRulo;
	}

	public RuloOzellikBoyutsal getSelectedRuloOzellikBoyutsal() {
		return selectedRuloOzellikBoyutsal;
	}

	public void setSelectedRuloOzellikBoyutsal(
			RuloOzellikBoyutsal selectedRuloOzellikBoyutsal) {
		this.selectedRuloOzellikBoyutsal = selectedRuloOzellikBoyutsal;
	}

	public RuloOzellikFizikselGirdi getSelectedRuloOzellikFizikselGirdi() {
		return selectedRuloOzellikFizikselGirdi;
	}

	public void setSelectedRuloOzellikFizikselGirdi(
			RuloOzellikFizikselGirdi selectedRuloOzellikFizikselGirdi) {
		this.selectedRuloOzellikFizikselGirdi = selectedRuloOzellikFizikselGirdi;
	}

	public RuloOzellikFizikselUretici getSelectedRuloOzellikFizikselUretici() {
		return selectedRuloOzellikFizikselUretici;
	}

	public void setSelectedRuloOzellikFizikselUretici(
			RuloOzellikFizikselUretici selectedRuloOzellikFizikselUretici) {
		this.selectedRuloOzellikFizikselUretici = selectedRuloOzellikFizikselUretici;
	}

	public RuloTestCekmeBoyuna getSelectedRuloTestCekmeBoyuna() {
		return selectedRuloTestCekmeBoyuna;
	}

	public void setSelectedRuloTestCekmeBoyuna(
			RuloTestCekmeBoyuna selectedRuloTestCekmeBoyuna) {
		this.selectedRuloTestCekmeBoyuna = selectedRuloTestCekmeBoyuna;
	}

	public RuloTestCekmeEnine getSelectedRuloTestCekmeEnine() {
		return selectedRuloTestCekmeEnine;
	}

	public void setSelectedRuloTestCekmeEnine(
			RuloTestCekmeEnine selectedRuloTestCekmeEnine) {
		this.selectedRuloTestCekmeEnine = selectedRuloTestCekmeEnine;
	}

	public RuloTestCentikDarbeBoyuna getSelectedRuloTestCentikDarbeBoyuna() {
		return selectedRuloTestCentikDarbeBoyuna;
	}

	public void setSelectedRuloTestCentikDarbeBoyuna(
			RuloTestCentikDarbeBoyuna selectedRuloTestCentikDarbeBoyuna) {
		this.selectedRuloTestCentikDarbeBoyuna = selectedRuloTestCentikDarbeBoyuna;
	}

	public RuloTestCentikDarbeEnine getSelectedRuloTestCentikDarbeEnine() {
		return selectedRuloTestCentikDarbeEnine;
	}

	public void setSelectedRuloTestCentikDarbeEnine(
			RuloTestCentikDarbeEnine selectedRuloTestCentikDarbeEnine) {
		this.selectedRuloTestCentikDarbeEnine = selectedRuloTestCentikDarbeEnine;
	}

	public RuloTestCentikDarbeUretici getSelectedRuloTestCentikDarbeUretici() {
		return selectedRuloTestCentikDarbeUretici;
	}

	public void setSelectedRuloTestCentikDarbeUretici(
			RuloTestCentikDarbeUretici selectedRuloTestCentikDarbeUretici) {
		this.selectedRuloTestCentikDarbeUretici = selectedRuloTestCentikDarbeUretici;
	}

	public RuloTestKimyasalUretici getSelectedRuloTestKimyasalUretici() {
		return selectedRuloTestKimyasalUretici;
	}

	public void setSelectedRuloTestKimyasalUretici(
			RuloTestKimyasalUretici selectedRuloTestKimyasalUretici) {
		this.selectedRuloTestKimyasalUretici = selectedRuloTestKimyasalUretici;
	}

	public RuloTestKimyasalGirdi getSelectedRuloTestKimyasalGirdi() {
		return selectedRuloTestKimyasalGirdi;
	}

	public void setSelectedRuloTestKimyasalGirdi(
			RuloTestKimyasalGirdi selectedRuloTestKimyasalGirdi) {
		this.selectedRuloTestKimyasalGirdi = selectedRuloTestKimyasalGirdi;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public File getTheFile() {
		return theFile;
	}

	public String getDownloadedFileName() {
		return downloadedFileName;
	}

	public void setDownloadedFileName(String downloadedFileName) {
		this.downloadedFileName = downloadedFileName;
	}

	public void setDownloadedFile(StreamedContent downloadedFile) {
		this.downloadedFile = downloadedFile;
	}

	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	/**
	 * @return the selectedRuloTestDwttUretici
	 */
	public RuloTestDwttUretici getSelectedRuloTestDwttUretici() {
		return selectedRuloTestDwttUretici;
	}

	/**
	 * @param selectedRuloTestDwttUretici
	 *            the selectedRuloTestDwttUretici to set
	 */
	public void setSelectedRuloTestDwttUretici(
			RuloTestDwttUretici selectedRuloTestDwttUretici) {
		this.selectedRuloTestDwttUretici = selectedRuloTestDwttUretici;
	}

	/**
	 * @return the selectedRuloTestDwttGirdi
	 */
	public RuloTestDwttGirdi getSelectedRuloTestDwttGirdi() {
		return selectedRuloTestDwttGirdi;
	}

	/**
	 * @param selectedRuloTestDwttGirdi
	 *            the selectedRuloTestDwttGirdi to set
	 */
	public void setSelectedRuloTestDwttGirdi(
			RuloTestDwttGirdi selectedRuloTestDwttGirdi) {
		this.selectedRuloTestDwttGirdi = selectedRuloTestDwttGirdi;
	}

}
