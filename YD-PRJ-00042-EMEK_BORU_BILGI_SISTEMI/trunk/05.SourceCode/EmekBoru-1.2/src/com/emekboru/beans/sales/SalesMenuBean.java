package com.emekboru.beans.sales;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.emekboru.config.ConfigBean;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "salesMenuBean")
@SessionScoped
public class SalesMenuBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8999156000048375808L;

	@EJB
	private ConfigBean config;

	private boolean newType1Menu;
	private boolean newType2Menu;
	private boolean newType3Menu;
	private boolean manageType1Menu;
	private boolean manageType2Menu;
	private boolean manageType3Menu;

	public SalesMenuBean() {
		newType1Menu = false;
		newType2Menu = false;
		newType3Menu = false;
		manageType1Menu = false;
		manageType2Menu = false;
		manageType3Menu = false;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public boolean getNewType1Menu() {
		return newType1Menu;
	}

	public void setNewType1Menu(boolean newType1Menu) {
		this.newType1Menu = newType1Menu;
	}

	public boolean getNewType2Menu() {
		return newType2Menu;
	}

	public void setNewType2Menu(boolean newType2Menu) {
		this.newType2Menu = newType2Menu;
	}

	public boolean getNewType3Menu() {
		return newType3Menu;
	}

	public void setNewType3Menu(boolean newType3Menu) {
		this.newType3Menu = newType3Menu;
	}

	public boolean getManageType1Menu() {
		return manageType1Menu;
	}

	public void setManageType1Menu(boolean manageType1Menu) {
		this.manageType1Menu = manageType1Menu;
	}

	public boolean getManageType2Menu() {
		return manageType2Menu;
	}

	public void setManageType2Menu(boolean manageType2Menu) {
		this.manageType2Menu = manageType2Menu;
	}

	public boolean getManageType3Menu() {
		return manageType3Menu;
	}

	public void setManageType3Menu(boolean manageType3Menu) {
		this.manageType3Menu = manageType3Menu;
	}

	public void showNewMenu() {
		newType1Menu = true;
		newType2Menu = false;
		newType3Menu = false;
		manageType1Menu = false;
		manageType2Menu = false;
		manageType3Menu = false;
	}

	public void showNewType1Menu() {
		newType1Menu = true;
		newType2Menu = false;
		newType3Menu = false;
		manageType1Menu = false;
		manageType2Menu = false;
		manageType3Menu = false;
	}

	public void showManageType1Menu() {
		newType1Menu = false;
		newType2Menu = false;
		newType3Menu = false;
		manageType1Menu = true;
		manageType2Menu = false;
		manageType3Menu = false;
	}

	public void showNewType2Menu() {
		newType1Menu = false;
		newType2Menu = true;
		newType3Menu = false;
		manageType1Menu = false;
		manageType2Menu = false;
		manageType3Menu = false;
	}

	public void showManageType2Menu() {
		newType1Menu = false;
		newType2Menu = false;
		newType3Menu = false;
		manageType1Menu = false;
		manageType2Menu = true;
		manageType3Menu = false;
	}

	public void showNewType3Menu() {
		newType1Menu = false;
		newType2Menu = false;
		newType3Menu = true;
		manageType1Menu = false;
		manageType2Menu = false;
		manageType3Menu = false;
	}

	public void showManageType3Menu() {
		newType1Menu = false;
		newType2Menu = false;
		newType3Menu = false;
		manageType1Menu = false;
		manageType2Menu = false;
		manageType3Menu = true;
	}

	// public void goToSalesCustomer(ActionEvent event) {
	// this.showNewType1Menu();
	// FacesContextUtils.redirect(config.getPageUrl().SALES_CUSTOMER_PAGE);
	// }

	// public void goToSalesCRM(ActionEvent event) {
	// this.showNewType1Menu();
	// FacesContextUtils.redirect(config.getPageUrl().SALES_CRM_PAGE);
	// }

	// public void goToSalesEvents(ActionEvent event) {
	// this.showNewType1Menu();
	// FacesContextUtils.redirect(config.getPageUrl().SALES_EVENTS_PAGE);
	// }
	//
	// public void goToSalesDocuments(ActionEvent event) {
	// this.showNewType1Menu();
	// FacesContextUtils.redirect(config.getPageUrl().SALES_DOCUMENTS_PAGE);
	// }
	//
	// public void goToSalesReminder(ActionEvent event) {
	// this.showNewType1Menu();
	// FacesContextUtils.redirect(config.getPageUrl().SALES_REMINDER_PAGE);
	// }
	//
	// public void goToSalesProposal(ActionEvent event) {
	// this.showNewType1Menu();
	// FacesContextUtils.redirect(config.getPageUrl().SALES_PROPOSAL_PAGE);
	// }
	//
	// public void goToSalesOrder(ActionEvent event) {
	// this.showNewType1Menu();
	// FacesContextUtils.redirect(config.getPageUrl().SALES_ORDER_PAGE);
	// }
	//
	// public void goToSalesInvoice(ActionEvent event) {
	// this.showNewType1Menu();
	// FacesContextUtils.redirect(config.getPageUrl().SALES_INVOICE_PAGE);
	// }

	// deneme
	public void goToSalesCustomer() {
		this.showNewType1Menu();
		FacesContextUtils.redirect(config.getPageUrl().SALES_CUSTOMER_PAGE);
	}

	public void goToSalesCRM() {
		this.showNewType1Menu();
		FacesContextUtils.redirect(config.getPageUrl().SALES_CRM_PAGE);
	}

	public void goToSalesEvents() {
		this.showNewType1Menu();
		FacesContextUtils.redirect(config.getPageUrl().SALES_EVENTS_PAGE);
	}

	public void goToSalesDocuments() {
		this.showNewType1Menu();
		FacesContextUtils.redirect(config.getPageUrl().SALES_DOCUMENTS_PAGE);
	}

	public void goToSalesReminder() {
		this.showNewType1Menu();
		FacesContextUtils.redirect(config.getPageUrl().SALES_REMINDER_PAGE);
	}

	public void goToSalesProposal() {
		this.showNewType1Menu();
		FacesContextUtils.redirect(config.getPageUrl().SALES_PROPOSAL_PAGE);
	}

	public void goToSalesOrder() {
		this.showNewType1Menu();
		FacesContextUtils.redirect(config.getPageUrl().SALES_ORDER_PAGE);
	}

	public void goToSalesInvoice() {
		this.showNewType1Menu();
		FacesContextUtils.redirect(config.getPageUrl().SALES_INVOICE_PAGE);
	}

	public void goToSalesList() {
		FacesContextUtils.redirect(config.getPageUrl().SALES_LIST_PAGE);
	}
	// deneme
}
