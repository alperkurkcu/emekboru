/**
 * 
 */
package com.emekboru.beans.utils;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.rulo.Rulo;
import com.emekboru.jpa.rulo.RuloPipeLink;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.rulo.RuloPipeLinkManager;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "boruRuloDuzenleBean")
@SessionScoped
public class BoruRuloDuzenleBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Pipe selectedPipe = null;
	private Rulo selectedRulo = null;
	private RuloPipeLink selectedlink = null;
	private String barkodNo = null;

	public BoruRuloDuzenleBean() {

		selectedPipe = new Pipe();
		selectedRulo = new Rulo();
		selectedlink = new RuloPipeLink();
		barkodNo = null;
	}

	public void boruBul(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();

		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!", null));
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));

			RuloPipeLinkManager linkManager = new RuloPipeLinkManager();

			if (selectedRulo == null || selectedRulo.getRuloId() == null) {

				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR, " LÜTFEN RULO SEÇİNİZ!",
						null));
				return;
			} else {
				selectedlink = selectedPipe.getRuloPipeLink();
				selectedlink.setRuloYil(selectedRulo.getRuloYil());
				selectedlink.setRulo(selectedRulo);
				linkManager.updateEntity(selectedlink);

				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						" BORUNUN RULOSU BAŞARIYLA DEĞİŞMİŞTİR!", null));
			}

			selectedlink = new RuloPipeLink();
			selectedPipe = new Pipe();
			selectedRulo = new Rulo();
		}

	}

	// setters getters

	/**
	 * @return the selectedPipe
	 */
	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	/**
	 * @param selectedPipe
	 *            the selectedPipe to set
	 */
	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	/**
	 * @return the selectedRulo
	 */
	public Rulo getSelectedRulo() {
		return selectedRulo;
	}

	/**
	 * @param selectedRulo
	 *            the selectedRulo to set
	 */
	public void setSelectedRulo(Rulo selectedRulo) {
		this.selectedRulo = selectedRulo;
	}

	/**
	 * @return the barkodNo
	 */
	public String getBarkodNo() {
		return barkodNo;
	}

	/**
	 * @param barkodNo
	 *            the barkodNo to set
	 */
	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

}
