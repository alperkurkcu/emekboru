/**
 * 
 */
package com.emekboru.beans.kalibrasyon;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.kalibrasyon.KalibrasyonOtomatikUltrasonik;
import com.emekboru.jpaman.kalibrasyon.KalibrasyonOtomatikUltrasonikManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "kalibrasyonOtomatikUltrasonikBean")
@ViewScoped
public class KalibrasyonOtomatikUltrasonikBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<KalibrasyonOtomatikUltrasonik> allKalibrasyonList = new ArrayList<KalibrasyonOtomatikUltrasonik>();
	private KalibrasyonOtomatikUltrasonik kalibrasyonForm = new KalibrasyonOtomatikUltrasonik();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender = false;

	private Integer prmOnlineOffline = null;

	public KalibrasyonOtomatikUltrasonikBean() {
	}

	public void kalibrasyonListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void addKalibrasyon(Integer prmOnlineOffline) {
		this.prmOnlineOffline = prmOnlineOffline;
		updateButtonRender = false;
	}

	public void addOrUpdateKalibrasyon(ActionEvent e) {

		KalibrasyonOtomatikUltrasonikManager manager = new KalibrasyonOtomatikUltrasonikManager();
		if (kalibrasyonForm.getId() == null) {
			kalibrasyonForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			kalibrasyonForm.setEkleyenKullanici(userBean.getUser().getId());
			kalibrasyonForm.setOnlineOffline(prmOnlineOffline);
			manager.enterNew(kalibrasyonForm);
			FacesContextUtils.addInfoMessage("SubmitMessage");
		} else {
			if (updateButtonRender) {
				kalibrasyonForm.setGuncellemeZamani(new java.sql.Timestamp(
						new java.util.Date().getTime()));
				kalibrasyonForm.setGuncelleyenKullanici(userBean.getUser()
						.getId());
				manager.updateEntity(kalibrasyonForm);
				FacesContextUtils.addInfoMessage("UpdateItemMessage");
			} else {
				clonner();
			}
		}
		fillTestList(kalibrasyonForm.getOnlineOffline());
	}

	@SuppressWarnings("static-access")
	public void fillTestList(Integer prmOnlineOffline) {
		KalibrasyonOtomatikUltrasonikManager manager = new KalibrasyonOtomatikUltrasonikManager();
		allKalibrasyonList = manager.getAllKalibrasyon(prmOnlineOffline);
		kalibrasyonForm = new KalibrasyonOtomatikUltrasonik();
	}

	public void deleteKalibrasyon() {

		KalibrasyonOtomatikUltrasonikManager manager = new KalibrasyonOtomatikUltrasonikManager();
		if (kalibrasyonForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		prmOnlineOffline = kalibrasyonForm.getOnlineOffline();
		manager.delete(kalibrasyonForm);
		kalibrasyonForm = new KalibrasyonOtomatikUltrasonik();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		fillTestList(prmOnlineOffline);
	}

	public void clonner() {

		KalibrasyonOtomatikUltrasonik clone = new KalibrasyonOtomatikUltrasonik();
		KalibrasyonOtomatikUltrasonikManager manager = new KalibrasyonOtomatikUltrasonikManager();
		prmOnlineOffline = kalibrasyonForm.getOnlineOffline();
		try {
			clone = (KalibrasyonOtomatikUltrasonik) this.kalibrasyonForm
					.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			FacesContextUtils.addPlainErrorMessage(e.getMessage());
		}

		clone.setId(null);
		clone.setEklemeZamani(new java.sql.Timestamp(new java.util.Date()
				.getTime()));
		clone.setEkleyenKullanici(userBean.getUser().getId());

		manager.enterNew(clone);
		FacesContextUtils.addInfoMessage("SubmitMessage");
		fillTestList(prmOnlineOffline);
	}

	// setters getters

	/**
	 * @return the allKalibrasyonList
	 */
	public List<KalibrasyonOtomatikUltrasonik> getAllKalibrasyonList() {
		return allKalibrasyonList;
	}

	/**
	 * @param allKalibrasyonList
	 *            the allKalibrasyonList to set
	 */
	public void setAllKalibrasyonList(
			List<KalibrasyonOtomatikUltrasonik> allKalibrasyonList) {
		this.allKalibrasyonList = allKalibrasyonList;
	}

	/**
	 * @return the kalibrasyonForm
	 */
	public KalibrasyonOtomatikUltrasonik getKalibrasyonForm() {
		return kalibrasyonForm;
	}

	/**
	 * @param kalibrasyonForm
	 *            the kalibrasyonForm to set
	 */
	public void setKalibrasyonForm(KalibrasyonOtomatikUltrasonik kalibrasyonForm) {
		this.kalibrasyonForm = kalibrasyonForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the prmOnlineOffline
	 */
	public Integer getPrmOnlineOffline() {
		return prmOnlineOffline;
	}

	/**
	 * @param prmOnlineOffline
	 *            the prmOnlineOffline to set
	 */
	public void setPrmOnlineOffline(Integer prmOnlineOffline) {
		this.prmOnlineOffline = prmOnlineOffline;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
