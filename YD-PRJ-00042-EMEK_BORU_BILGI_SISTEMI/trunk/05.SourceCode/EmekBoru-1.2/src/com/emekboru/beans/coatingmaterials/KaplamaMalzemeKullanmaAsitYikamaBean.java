package com.emekboru.beans.coatingmaterials;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.coatingmaterials.KaplamaMalzemeKullanmaAsitYikama;
import com.emekboru.jpaman.CoatRawMaterialManager;
import com.emekboru.jpaman.coatingmaterials.KaplamaMalzemeKullanmaAsitYikamaManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * 
 */

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "kaplamaMalzemeKullanmaAsitYikamaBean")
@ViewScoped
public class KaplamaMalzemeKullanmaAsitYikamaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<KaplamaMalzemeKullanmaAsitYikama> allKaplamaMalzemeKullanmaAsitYikamaList = new ArrayList<KaplamaMalzemeKullanmaAsitYikama>();
	private KaplamaMalzemeKullanmaAsitYikama kaplamaMalzemeKullanmaAsitYikamaForm = new KaplamaMalzemeKullanmaAsitYikama();

	private CoatRawMaterial selectedMaterial = new CoatRawMaterial();
	private List<CoatRawMaterial> coatRawMaterialList;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	private BigDecimal kullanilanMiktar;

	private boolean durum = false;

	public KaplamaMalzemeKullanmaAsitYikamaBean() {

	}

	public void addNew() {
		kaplamaMalzemeKullanmaAsitYikamaForm = new KaplamaMalzemeKullanmaAsitYikama();
		updateButtonRender = false;
	}

	public void addMalzemeKullanmaSonuc(ActionEvent e) {

		if (kaplamaMalzemeKullanmaAsitYikamaForm.getHarcananMiktar() == null) {
			kaplamaMalzemeKullanmaAsitYikamaForm
					.setHarcananMiktar(BigDecimal.ZERO);
		}

		if (!malzemeKontrol()) {
			return;
		}

		KaplamaMalzemeKullanmaAsitYikamaManager malzemeKullanmaManager = new KaplamaMalzemeKullanmaAsitYikamaManager();

		if (kaplamaMalzemeKullanmaAsitYikamaForm.getId() == null) {

			try {
				kaplamaMalzemeKullanmaAsitYikamaForm
						.setEklemeZamani(UtilInsCore.getTarihZaman());
				kaplamaMalzemeKullanmaAsitYikamaForm
						.setEkleyenKullanici(userBean.getUser().getId());
				malzemeKullanmaManager
						.updateEntity(kaplamaMalzemeKullanmaAsitYikamaForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"ASİT YIKAMA MALZEMESİ İŞLEMİ BAŞARIYLA EKLENMİŞTİR!"));

				kullanilanMiktar = kaplamaMalzemeKullanmaAsitYikamaForm
						.getHarcananMiktar();
				durum = true;
				malzemeGuncelle();
			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"ASİT YIKAMA MALZEMESİ İŞLEMİ EKLENEMEDİ!", null));
				System.out
						.println("KaplamaMalzemeKullanmaAsitYikamaBean.addMalzemeKullanmaSonuc-HATA-EKLEME");
			}
		} else {

			try {

				kaplamaMalzemeKullanmaAsitYikamaForm
						.setGuncellemeZamani(UtilInsCore.getTarihZaman());
				kaplamaMalzemeKullanmaAsitYikamaForm
						.setGuncelleyenKullanici(userBean.getUser().getId());
				kaplamaMalzemeKullanmaAsitYikamaForm
						.setCoatRawMaterial(selectedMaterial);
				malzemeKullanmaManager
						.updateEntity(kaplamaMalzemeKullanmaAsitYikamaForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								"ASİT YIKAMA MALZEMESİ İŞLEMİ BAŞARIYLA GÜNCELLENMİŞTİR!"));

				kullanilanMiktar = kaplamaMalzemeKullanmaAsitYikamaForm
						.getHarcananMiktar();
				durum = true;
				malzemeGuncelle();

			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"ASİT YIKAMA MALZEMESİ KULLANMA İŞLEMİ GÜNCELLENEMEDİ!",
								null));
				System.out
						.println("KaplamaMalzemeKullanmaAsitYikamaBean.addMalzemeKullanmaSonuc-HATA-GÜNCELLEME"
								+ e.toString());
			}

		}

		fillTestList();
	}

	@SuppressWarnings("static-access")
	public void fillTestList() {

		KaplamaMalzemeKullanmaAsitYikamaManager malzemeKullanmaManager = new KaplamaMalzemeKullanmaAsitYikamaManager();
		allKaplamaMalzemeKullanmaAsitYikamaList = malzemeKullanmaManager
				.getAllKaplamaMalzemeKullanmaAsitYikama();
	}

	public void kaplamaListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void deleteMalzemeKullanmaSonuc(ActionEvent e) {

		KaplamaMalzemeKullanmaAsitYikamaManager malzemeKullanmaManager = new KaplamaMalzemeKullanmaAsitYikamaManager();

		if (kaplamaMalzemeKullanmaAsitYikamaForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		malzemeKullanmaManager.delete(kaplamaMalzemeKullanmaAsitYikamaForm);
		kaplamaMalzemeKullanmaAsitYikamaForm = new KaplamaMalzemeKullanmaAsitYikama();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"ASİT YIKAMA MALZEMESİ KULLANMA İŞLEMİ BAŞARIYLA SİLİNMİŞTİR!"));

		kullanilanMiktar = kaplamaMalzemeKullanmaAsitYikamaForm
				.getHarcananMiktar();
		durum = false;
		malzemeGuncelle();

		fillTestList();
	}

	public boolean malzemeKontrol() {

		FacesContext context = FacesContext.getCurrentInstance();
		if (kaplamaMalzemeKullanmaAsitYikamaForm.getHarcananMiktar()
				.floatValue() > selectedMaterial.getRemainingAmount()) {
			context.addMessage(
					null,
					new FacesMessage(
							"KULLANILACAK MİKTAR KALAN MALZEME MİKTARINDAN BÜYÜKTÜR! LÜTFEN KONTROL EDİNİZ!"));
			return false;
		} else {
			return true;
		}
	}

	public void malzemeGuncelle() {

		CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
		if (durum) {// enternew
			selectedMaterial.setRemainingAmount(selectedMaterial
					.getRemainingAmount() - kullanilanMiktar.doubleValue());
			materialManager.updateEntity(selectedMaterial);
		} else if (!durum) {// delete
			kaplamaMalzemeKullanmaAsitYikamaForm.getCoatRawMaterial()
					.setRemainingAmount(
							kaplamaMalzemeKullanmaAsitYikamaForm
									.getCoatRawMaterial().getRemainingAmount()
									+ kullanilanMiktar.doubleValue());
			materialManager.updateEntity(kaplamaMalzemeKullanmaAsitYikamaForm
					.getCoatRawMaterial());
		}
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"ASİT YIKAMA MALZEMESİ BAŞARIYLA GÜNCELLENMİŞTİR!"));
	}

	// getters setters

	/**
	 * @return the allKaplamaMalzemeKullanmaAsitYikamaList
	 */
	public List<KaplamaMalzemeKullanmaAsitYikama> getAllKaplamaMalzemeKullanmaAsitYikamaList() {
		return allKaplamaMalzemeKullanmaAsitYikamaList;
	}

	/**
	 * @param allKaplamaMalzemeKullanmaAsitYikamaList
	 *            the allKaplamaMalzemeKullanmaAsitYikamaList to set
	 */
	public void setAllKaplamaMalzemeKullanmaAsitYikamaList(
			List<KaplamaMalzemeKullanmaAsitYikama> allKaplamaMalzemeKullanmaAsitYikamaList) {
		this.allKaplamaMalzemeKullanmaAsitYikamaList = allKaplamaMalzemeKullanmaAsitYikamaList;
	}

	/**
	 * @return the kaplamaMalzemeKullanmaAsitYikamaForm
	 */
	public KaplamaMalzemeKullanmaAsitYikama getKaplamaMalzemeKullanmaAsitYikamaForm() {
		return kaplamaMalzemeKullanmaAsitYikamaForm;
	}

	/**
	 * @param kaplamaMalzemeKullanmaAsitYikamaForm
	 *            the kaplamaMalzemeKullanmaAsitYikamaForm to set
	 */
	public void setKaplamaMalzemeKullanmaAsitYikamaForm(
			KaplamaMalzemeKullanmaAsitYikama kaplamaMalzemeKullanmaAsitYikamaForm) {
		this.kaplamaMalzemeKullanmaAsitYikamaForm = kaplamaMalzemeKullanmaAsitYikamaForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the selectedMaterial
	 */
	public CoatRawMaterial getSelectedMaterial() {
		return selectedMaterial;
	}

	/**
	 * @param selectedMaterial
	 *            the selectedMaterial to set
	 */
	public void setSelectedMaterial(CoatRawMaterial selectedMaterial) {
		this.selectedMaterial = selectedMaterial;
	}

	/**
	 * @return the coatRawMaterialList
	 */
	public List<CoatRawMaterial> getCoatRawMaterialList() {
		return coatRawMaterialList;
	}

	/**
	 * @param coatRawMaterialList
	 *            the coatRawMaterialList to set
	 */
	public void setCoatRawMaterialList(List<CoatRawMaterial> coatRawMaterialList) {
		this.coatRawMaterialList = coatRawMaterialList;
	}

	/**
	 * @return the kullanilanMiktar
	 */
	public BigDecimal getKullanilanMiktar() {
		return kullanilanMiktar;
	}

	/**
	 * @param kullanilanMiktar
	 *            the kullanilanMiktar to set
	 */
	public void setKullanilanMiktar(BigDecimal kullanilanMiktar) {
		this.kullanilanMiktar = kullanilanMiktar;
	}

	/**
	 * @return the durum
	 */
	public boolean isDurum() {
		return durum;
	}

	/**
	 * @param durum
	 *            the durum to set
	 */
	public void setDurum(boolean durum) {
		this.durum = durum;
	}

}
