package com.emekboru.beans.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.primefaces.component.inputtext.InputText;

@FacesValidator("emptyFieldValidator")
public class EmptyFieldValidator implements Validator{

	private final static String emptyFieldMessage = "The given field cannot be an empty!";

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		
		if (((InputText) component).getSubmittedValue() == "") 
		{
			FacesMessage msg = new FacesMessage(emptyFieldMessage,emptyFieldMessage);
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
		
	}

}
