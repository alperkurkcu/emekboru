/**
 * 
 */
package com.emekboru.beans.coatingmaterials;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.coatingmaterials.KaplamaMalzemeKullanmaEpoksi;
import com.emekboru.jpaman.CoatRawMaterialManager;
import com.emekboru.jpaman.coatingmaterials.KaplamaMalzemeKullanmaEpoksiManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "kaplamaMalzemeKullanmaEpoksiBean")
@ViewScoped
public class KaplamaMalzemeKullanmaEpoksiBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<KaplamaMalzemeKullanmaEpoksi> allKaplamaMalzemeKullanmaEpoksiList = new ArrayList<KaplamaMalzemeKullanmaEpoksi>();
	private KaplamaMalzemeKullanmaEpoksi kaplamaMalzemeKullanmaEpoksiForm = new KaplamaMalzemeKullanmaEpoksi();

	private CoatRawMaterial selectedMaterial = new CoatRawMaterial();
	private List<CoatRawMaterial> coatRawMaterialList;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	private BigDecimal kullanilanMiktar;

	private boolean durum = false;

	public KaplamaMalzemeKullanmaEpoksiBean() {

		coatRawMaterialList = new ArrayList<CoatRawMaterial>();
		CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
		coatRawMaterialList = materialManager
				.findUnfinishedCoatMaterialByType(8);// epoksi toz type
	}

	public void addNew() {
		kaplamaMalzemeKullanmaEpoksiForm = new KaplamaMalzemeKullanmaEpoksi();
		updateButtonRender = false;
	}

	public void addMalzemeKullanmaSonuc(ActionEvent e) {
		
		if (kaplamaMalzemeKullanmaEpoksiForm.getHarcananMiktar() == null) {
			kaplamaMalzemeKullanmaEpoksiForm
					.setHarcananMiktar(BigDecimal.ZERO);
		}

		if (!malzemeKontrol()) {
			return;
		}

		KaplamaMalzemeKullanmaEpoksiManager malzemeKullanmaManager = new KaplamaMalzemeKullanmaEpoksiManager();

		if (kaplamaMalzemeKullanmaEpoksiForm.getId() == null) {

			try {
				kaplamaMalzemeKullanmaEpoksiForm.setEklemeZamani(UtilInsCore
						.getTarihZaman());
				kaplamaMalzemeKullanmaEpoksiForm.setEkleyenKullanici(userBean
						.getUser().getId());
				kaplamaMalzemeKullanmaEpoksiForm
						.setCoatRawMaterial(selectedMaterial);
				malzemeKullanmaManager
						.enterNew(kaplamaMalzemeKullanmaEpoksiForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"EPOKSİ MALZEMESİ İŞLEMİ BAŞARIYLA EKLENMİŞTİR!", null));

				kullanilanMiktar = kaplamaMalzemeKullanmaEpoksiForm
						.getHarcananMiktar();
				durum = true;
				malzemeGuncelle();

			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"EPOKSİ MALZEMESİ İŞLEMİ EKLENEMEDİ!", null));
				System.out
						.println("KaplamaMalzemeKullanmaEpoksiBean.addMalzemeKullanmaSonuc-HATA-EKLEME");
			}
		} else {

			try {

				kaplamaMalzemeKullanmaEpoksiForm
						.setGuncellemeZamani(UtilInsCore.getTarihZaman());
				kaplamaMalzemeKullanmaEpoksiForm
						.setGuncelleyenKullanici(userBean.getUser().getId());
				malzemeKullanmaManager
						.updateEntity(kaplamaMalzemeKullanmaEpoksiForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"EPOKSİ MALZEMESİ İŞLEMİ BAŞARIYLA GÜNCELLENMİŞTİR!"));

				kullanilanMiktar = kaplamaMalzemeKullanmaEpoksiForm
						.getHarcananMiktar();
				durum = true;
				malzemeGuncelle();

			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"EPOKSİ MALZEMESİ KULLANMA İŞLEMİ GÜNCELLENEMEDİ!",
						null));
				System.out
						.println("KaplamaMalzemeKullanmaEpoksiBean.addMalzemeKullanmaSonuc-HATA-GÜNCELLEME"
								+ e.toString());
			}

		}

		fillTestList();
	}

	@SuppressWarnings("static-access")
	public void fillTestList() {

		KaplamaMalzemeKullanmaEpoksiManager malzemeKullanmaManager = new KaplamaMalzemeKullanmaEpoksiManager();
		allKaplamaMalzemeKullanmaEpoksiList = malzemeKullanmaManager
				.getAllKaplamaMalzemeKullanmaEpoksi();
	}

	public void kaplamaListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void deleteMalzemeKullanmaSonuc(ActionEvent e) {

		KaplamaMalzemeKullanmaEpoksiManager malzemeKullanmaManager = new KaplamaMalzemeKullanmaEpoksiManager();

		if (kaplamaMalzemeKullanmaEpoksiForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		kullanilanMiktar = kaplamaMalzemeKullanmaEpoksiForm.getHarcananMiktar();

		durum = false;
		malzemeGuncelle();

		malzemeKullanmaManager.delete(kaplamaMalzemeKullanmaEpoksiForm);
		kaplamaMalzemeKullanmaEpoksiForm = new KaplamaMalzemeKullanmaEpoksi();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"EPOKSİ MALZEMESİ KULLANMA İŞLEMİ BAŞARIYLA SİLİNMİŞTİR!"));

		fillTestList();
	}

	public void malzemeGuncelle() {

		CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
		if (durum) {// enternew
			selectedMaterial.setRemainingAmount(selectedMaterial
					.getRemainingAmount() - kullanilanMiktar.doubleValue());
			materialManager.updateEntity(selectedMaterial);
		} else if (!durum) {// delete
			kaplamaMalzemeKullanmaEpoksiForm.getCoatRawMaterial()
					.setRemainingAmount(
							kaplamaMalzemeKullanmaEpoksiForm
									.getCoatRawMaterial().getRemainingAmount()
									+ kullanilanMiktar.doubleValue());
			materialManager.updateEntity(kaplamaMalzemeKullanmaEpoksiForm
					.getCoatRawMaterial());
		}
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"EPOKSİ MALZEMESİ BAŞARIYLA GÜNCELLENMİŞTİR!"));
	}

	public boolean malzemeKontrol() {

		FacesContext context = FacesContext.getCurrentInstance();
		if (kaplamaMalzemeKullanmaEpoksiForm.getHarcananMiktar().floatValue() > selectedMaterial
				.getRemainingAmount()) {
			context.addMessage(
					null,
					new FacesMessage(
							"KULLANILACAK MİKTAR KALAN MALZEME MİKTARINDAN BÜYÜKTÜR! LÜTFEN KONTROL EDİNİZ!"));
			return false;
		} else {
			return true;
		}
	}

	// getters setters
	/**
	 * @return the allKaplamaMalzemeKullanmaEpoksiList
	 */
	public List<KaplamaMalzemeKullanmaEpoksi> getAllKaplamaMalzemeKullanmaEpoksiList() {
		return allKaplamaMalzemeKullanmaEpoksiList;
	}

	/**
	 * @param allKaplamaMalzemeKullanmaEpoksiList
	 *            the allKaplamaMalzemeKullanmaEpoksiList to set
	 */
	public void setAllKaplamaMalzemeKullanmaEpoksiList(
			List<KaplamaMalzemeKullanmaEpoksi> allKaplamaMalzemeKullanmaEpoksiList) {
		this.allKaplamaMalzemeKullanmaEpoksiList = allKaplamaMalzemeKullanmaEpoksiList;
	}

	/**
	 * @return the kaplamaMalzemeKullanmaEpoksiForm
	 */
	public KaplamaMalzemeKullanmaEpoksi getKaplamaMalzemeKullanmaEpoksiForm() {
		return kaplamaMalzemeKullanmaEpoksiForm;
	}

	/**
	 * @param kaplamaMalzemeKullanmaEpoksiForm
	 *            the kaplamaMalzemeKullanmaEpoksiForm to set
	 */
	public void setKaplamaMalzemeKullanmaEpoksiForm(
			KaplamaMalzemeKullanmaEpoksi kaplamaMalzemeKullanmaEpoksiForm) {
		this.kaplamaMalzemeKullanmaEpoksiForm = kaplamaMalzemeKullanmaEpoksiForm;
	}

	/**
	 * @return the selectedMaterial
	 */
	public CoatRawMaterial getSelectedMaterial() {
		return selectedMaterial;
	}

	/**
	 * @param selectedMaterial
	 *            the selectedMaterial to set
	 */
	public void setSelectedMaterial(CoatRawMaterial selectedMaterial) {
		this.selectedMaterial = selectedMaterial;
	}

	/**
	 * @return the coatRawMaterialList
	 */
	public List<CoatRawMaterial> getCoatRawMaterialList() {
		return coatRawMaterialList;
	}

	/**
	 * @param coatRawMaterialList
	 *            the coatRawMaterialList to set
	 */
	public void setCoatRawMaterialList(List<CoatRawMaterial> coatRawMaterialList) {
		this.coatRawMaterialList = coatRawMaterialList;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the kullanilanMiktar
	 */
	public BigDecimal getKullanilanMiktar() {
		return kullanilanMiktar;
	}

	/**
	 * @param kullanilanMiktar
	 *            the kullanilanMiktar to set
	 */
	public void setKullanilanMiktar(BigDecimal kullanilanMiktar) {
		this.kullanilanMiktar = kullanilanMiktar;
	}

	/**
	 * @return the durum
	 */
	public boolean isDurum() {
		return durum;
	}

	/**
	 * @param durum
	 *            the durum to set
	 */
	public void setDurum(boolean durum) {
		this.durum = durum;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
