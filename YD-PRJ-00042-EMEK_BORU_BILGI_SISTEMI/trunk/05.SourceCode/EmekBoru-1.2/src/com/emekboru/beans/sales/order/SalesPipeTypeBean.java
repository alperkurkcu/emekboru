package com.emekboru.beans.sales.order;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.sales.order.SalesPipeType;
import com.emekboru.jpaman.sales.order.SalesPipeTypeManager;

@ManagedBean(name = "salesPipeTypeBean")
@ViewScoped
public class SalesPipeTypeBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6142178385464982068L;
	private List<SalesPipeType> typeList;

	public SalesPipeTypeBean() {

		SalesPipeTypeManager manager = new SalesPipeTypeManager();
		typeList = manager.findAll(SalesPipeType.class);
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public List<SalesPipeType> getTypeList() {
		return typeList;
	}

	public void setTypeList(List<SalesPipeType> typeList) {
		this.typeList = typeList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}