package com.emekboru.beans.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.primefaces.component.password.Password;

@FacesValidator("passwordFieldValidator")
public class PasswordFieldValidator implements Validator{

	private final static String passwordEmptyMessage = "The password cannot be an empty field!";

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		if (((Password) component).getSubmittedValue() == "") 
		{
			FacesMessage msg = new FacesMessage(passwordEmptyMessage,passwordEmptyMessage);
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}

	}

}
