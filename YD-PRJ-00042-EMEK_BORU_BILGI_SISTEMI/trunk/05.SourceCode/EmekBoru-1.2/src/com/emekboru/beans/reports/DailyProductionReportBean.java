/**
 * 
 */
package com.emekboru.beans.reports;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.BoruBoyutsalKontrolTolerans;
import com.emekboru.jpa.EdwPipeLink;
import com.emekboru.jpa.HidrostaticTestsSpec;
import com.emekboru.jpa.MachinePipeLink;
import com.emekboru.jpa.NondestructiveTestsSpec;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.rulo.Rulo;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizFloroskopikSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizGozOlcuSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizHidrostatik;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizManuelUtSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizManyetikParcacikSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizOfflineOtomatikUtSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizOtomatikUtSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizTamir;
import com.emekboru.jpaman.BoruBoyutsalKontrolToleransManager;
import com.emekboru.jpaman.EdwPipeLinkManager;
import com.emekboru.jpaman.HidrostaticTestsSpecManager;
import com.emekboru.jpaman.MachinePipeLinkManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizFloroskopikSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizGozOlcuSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizHidrostatikManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizManuelUtSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizManyetikParcacikSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizOfflineOtomatikUtSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizOtomatikUtSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizTamirManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.ReportUtil;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "dailyProductionReportBean")
@ViewScoped
public class DailyProductionReportBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	Integer raporNo = 0;
	String muayeneyiYapan = null;
	String hazirlayan = null;

	Integer raporVardiya;
	Date raporTarihi;
	String raporTarihiAString;
	String raporTarihiBString;

	Boolean kontrol = false;

	private Pipe selectedPipe = new Pipe();
	private List<MachinePipeLink> yesterdayPipes = new ArrayList<MachinePipeLink>();

	private List<Point> points = new ArrayList<Point>();
	private List<UltrasonicObject> ultrasonicObjects = new ArrayList<UltrasonicObject>();
	private List<OfflineObject> offlineObjects = new ArrayList<OfflineObject>();

	private Integer selectedMachineId = null;

	private String barkodNo = null;
	private Integer prmType = 0;

	private List<Rulo> pipeRulos = new ArrayList<Rulo>();

	@PostConstruct
	public void init() {
	}

	public class Point {
		public String boruBarkod = null;
		public String onlineTarih = null;
		public Integer onlineVardiya = null;
		public String ilkGozDokum = null;
		public String ilkGozLot = null;
		public String ilkGozTarih = null;
		public String ilkGozBoy = null;
		public String hidroTarihBir = null;
		public String hidroTarihIki = null;
		public String hidroSonuc = null;
		public String manualTarih = null;
		public String manualSonuc = null;
		public String radyoskopiTarihBir = null;
		public String radyoskopiTamirBir = null;
		public String radyoskopiTarihIki = null;
		public String radyoskopiTamirIki = null;
		public String radyoskopiSonuc = null;
		public String offlineTarih = null;
		public String offlineTamir = null;
		public String offlineSonuc = null;
		public String offlineManualTarih = null;
		public String offlineManualTamir = null;
		public String offlineManualsonuc = null;
		public String sonGozTarih = null;
		public String sonGozBoy = null;
		public String sonGozSonuc = null;
		public String sonGozKes;
		public String sonGozKesMetre;
		public String sonGozBobinYokeTarih;
		public String sonGozBobinYokeSonuc;

		/**
		 * @return the boruBarkod
		 */
		public String getBoruBarkod() {
			return boruBarkod;
		}

		/**
		 * @param boruBarkod
		 *            the boruBarkod to set
		 */
		public void setBoruBarkod(String boruBarkod) {
			this.boruBarkod = boruBarkod;
		}

		/**
		 * @return the onlineTarih
		 */
		public String getOnlineTarih() {
			return onlineTarih;
		}

		/**
		 * @param onlineTarih
		 *            the onlineTarih to set
		 */
		public void setOnlineTarih(String onlineTarih) {
			this.onlineTarih = onlineTarih;
		}

		/**
		 * @return the onlineVardiya
		 */
		public Integer getOnlineVardiya() {
			return onlineVardiya;
		}

		/**
		 * @param onlineVardiya
		 *            the onlineVardiya to set
		 */
		public void setOnlineVardiya(Integer onlineVardiya) {
			this.onlineVardiya = onlineVardiya;
		}

		/**
		 * @return the ilkGozDokum
		 */
		public String getIlkGozDokum() {
			return ilkGozDokum;
		}

		/**
		 * @param ilkGozDokum
		 *            the ilkGozDokum to set
		 */
		public void setIlkGozDokum(String ilkGozDokum) {
			this.ilkGozDokum = ilkGozDokum;
		}

		/**
		 * @return the ilkGozLot
		 */
		public String getIlkGozLot() {
			return ilkGozLot;
		}

		/**
		 * @param ilkGozLot
		 *            the ilkGozLot to set
		 */
		public void setIlkGozLot(String ilkGozLot) {
			this.ilkGozLot = ilkGozLot;
		}

		/**
		 * @return the ilkGozTarih
		 */
		public String getIlkGozTarih() {
			return ilkGozTarih;
		}

		/**
		 * @param ilkGozTarih
		 *            the ilkGozTarih to set
		 */
		public void setIlkGozTarih(String ilkGozTarih) {
			this.ilkGozTarih = ilkGozTarih;
		}

		/**
		 * @return the ilkGozBoy
		 */
		public String getIlkGozBoy() {
			return ilkGozBoy;
		}

		/**
		 * @param ilkGozBoy
		 *            the ilkGozBoy to set
		 */
		public void setIlkGozBoy(String ilkGozBoy) {
			this.ilkGozBoy = ilkGozBoy;
		}

		/**
		 * @return the hidroTarihBir
		 */
		public String getHidroTarihBir() {
			return hidroTarihBir;
		}

		/**
		 * @param hidroTarihBir
		 *            the hidroTarihBir to set
		 */
		public void setHidroTarihBir(String hidroTarihBir) {
			this.hidroTarihBir = hidroTarihBir;
		}

		/**
		 * @return the hidroTarihIki
		 */
		public String getHidroTarihIki() {
			return hidroTarihIki;
		}

		/**
		 * @param hidroTarihIki
		 *            the hidroTarihIki to set
		 */
		public void setHidroTarihIki(String hidroTarihIki) {
			this.hidroTarihIki = hidroTarihIki;
		}

		/**
		 * @return the hidroSonuc
		 */
		public String getHidroSonuc() {
			return hidroSonuc;
		}

		/**
		 * @param hidroSonuc
		 *            the hidroSonuc to set
		 */
		public void setHidroSonuc(String hidroSonuc) {
			this.hidroSonuc = hidroSonuc;
		}

		/**
		 * @return the manualTarih
		 */
		public String getManualTarih() {
			return manualTarih;
		}

		/**
		 * @param manualTarih
		 *            the manualTarih to set
		 */
		public void setManualTarih(String manualTarih) {
			this.manualTarih = manualTarih;
		}

		/**
		 * @return the manualSonuc
		 */
		public String getManualSonuc() {
			return manualSonuc;
		}

		/**
		 * @param manualSonuc
		 *            the manualSonuc to set
		 */
		public void setManualSonuc(String manualSonuc) {
			this.manualSonuc = manualSonuc;
		}

		/**
		 * @return the radyoskopiTarihBir
		 */
		public String getRadyoskopiTarihBir() {
			return radyoskopiTarihBir;
		}

		/**
		 * @param radyoskopiTarihBir
		 *            the radyoskopiTarihBir to set
		 */
		public void setRadyoskopiTarihBir(String radyoskopiTarihBir) {
			this.radyoskopiTarihBir = radyoskopiTarihBir;
		}

		/**
		 * @return the radyoskopiTamirBir
		 */
		public String getRadyoskopiTamirBir() {
			return radyoskopiTamirBir;
		}

		/**
		 * @param radyoskopiTamirBir
		 *            the radyoskopiTamirBir to set
		 */
		public void setRadyoskopiTamirBir(String radyoskopiTamirBir) {
			this.radyoskopiTamirBir = radyoskopiTamirBir;
		}

		/**
		 * @return the radyoskopiTarihIki
		 */
		public String getRadyoskopiTarihIki() {
			return radyoskopiTarihIki;
		}

		/**
		 * @param radyoskopiTarihIki
		 *            the radyoskopiTarihIki to set
		 */
		public void setRadyoskopiTarihIki(String radyoskopiTarihIki) {
			this.radyoskopiTarihIki = radyoskopiTarihIki;
		}

		/**
		 * @return the radyoskopiTamirIki
		 */
		public String getRadyoskopiTamirIki() {
			return radyoskopiTamirIki;
		}

		/**
		 * @param radyoskopiTamirIki
		 *            the radyoskopiTamirIki to set
		 */
		public void setRadyoskopiTamirIki(String radyoskopiTamirIki) {
			this.radyoskopiTamirIki = radyoskopiTamirIki;
		}

		/**
		 * @return the radyoskopiSonuc
		 */
		public String getRadyoskopiSonuc() {
			return radyoskopiSonuc;
		}

		/**
		 * @param radyoskopiSonuc
		 *            the radyoskopiSonuc to set
		 */
		public void setRadyoskopiSonuc(String radyoskopiSonuc) {
			this.radyoskopiSonuc = radyoskopiSonuc;
		}

		/**
		 * @return the offlineTarih
		 */
		public String getOfflineTarih() {
			return offlineTarih;
		}

		/**
		 * @param offlineTarih
		 *            the offlineTarih to set
		 */
		public void setOfflineTarih(String offlineTarih) {
			this.offlineTarih = offlineTarih;
		}

		/**
		 * @return the offlineTamir
		 */
		public String getOfflineTamir() {
			return offlineTamir;
		}

		/**
		 * @param offlineTamir
		 *            the offlineTamir to set
		 */
		public void setOfflineTamir(String offlineTamir) {
			this.offlineTamir = offlineTamir;
		}

		/**
		 * @return the offlineSonuc
		 */
		public String getOfflineSonuc() {
			return offlineSonuc;
		}

		/**
		 * @param offlineSonuc
		 *            the offlineSonuc to set
		 */
		public void setOfflineSonuc(String offlineSonuc) {
			this.offlineSonuc = offlineSonuc;
		}

		/**
		 * @return the offlineManualTarih
		 */
		public String getOfflineManualTarih() {
			return offlineManualTarih;
		}

		/**
		 * @param offlineManualTarih
		 *            the offlineManualTarih to set
		 */
		public void setOfflineManualTarih(String offlineManualTarih) {
			this.offlineManualTarih = offlineManualTarih;
		}

		/**
		 * @return the offlineManualTamir
		 */
		public String getOfflineManualTamir() {
			return offlineManualTamir;
		}

		/**
		 * @param offlineManualTamir
		 *            the offlineManualTamir to set
		 */
		public void setOfflineManualTamir(String offlineManualTamir) {
			this.offlineManualTamir = offlineManualTamir;
		}

		/**
		 * @return the offlineManualsonuc
		 */
		public String getOfflineManualsonuc() {
			return offlineManualsonuc;
		}

		/**
		 * @param offlineManualsonuc
		 *            the offlineManualsonuc to set
		 */
		public void setOfflineManualsonuc(String offlineManualsonuc) {
			this.offlineManualsonuc = offlineManualsonuc;
		}

		/**
		 * @return the sonGozTarih
		 */
		public String getSonGozTarih() {
			return sonGozTarih;
		}

		/**
		 * @param sonGozTarih
		 *            the sonGozTarih to set
		 */
		public void setSonGozTarih(String sonGozTarih) {
			this.sonGozTarih = sonGozTarih;
		}

		/**
		 * @return the sonGozBoy
		 */
		public String getSonGozBoy() {
			return sonGozBoy;
		}

		/**
		 * @param sonGozBoy
		 *            the sonGozBoy to set
		 */
		public void setSonGozBoy(String sonGozBoy) {
			this.sonGozBoy = sonGozBoy;
		}

		/**
		 * @return the sonGozSonuc
		 */
		public String getSonGozSonuc() {
			return sonGozSonuc;
		}

		/**
		 * @param sonGozSonuc
		 *            the sonGozSonuc to set
		 */
		public void setSonGozSonuc(String sonGozSonuc) {
			this.sonGozSonuc = sonGozSonuc;
		}

		/**
		 * @return the sonGozKes
		 */
		public String getSonGozKes() {
			return sonGozKes;
		}

		/**
		 * @param sonGozKes
		 *            the sonGozKes to set
		 */
		public void setSonGozKes(String sonGozKes) {
			this.sonGozKes = sonGozKes;
		}

		/**
		 * @return the sonGozKesMetre
		 */
		public String getSonGozKesMetre() {
			return sonGozKesMetre;
		}

		/**
		 * @param sonGozKesMetre
		 *            the sonGozKesMetre to set
		 */
		public void setSonGozKesMetre(String sonGozKesMetre) {
			this.sonGozKesMetre = sonGozKesMetre;
		}

		/**
		 * @return the sonGozBobinYokeTarih
		 */
		public String getSonGozBobinYokeTarih() {
			return sonGozBobinYokeTarih;
		}

		/**
		 * @param sonGozBobinYokeTarih
		 *            the sonGozBobinYokeTarih to set
		 */
		public void setSonGozBobinYokeTarih(String sonGozBobinYokeTarih) {
			this.sonGozBobinYokeTarih = sonGozBobinYokeTarih;
		}

		/**
		 * @return the sonGozBobinYokeSonuc
		 */
		public String getSonGozBobinYokeSonuc() {
			return sonGozBobinYokeSonuc;
		}

		/**
		 * @param sonGozBobinYokeSonuc
		 *            the sonGozBobinYokeSonuc to set
		 */
		public void setSonGozBobinYokeSonuc(String sonGozBobinYokeSonuc) {
			this.sonGozBobinYokeSonuc = sonGozBobinYokeSonuc;
		}

	}

	public class UltrasonicObject {
		public Integer siraNo = null;
		public String boruBarkod = null;
		public String tarih = null;
		public Integer vardiya = null;
		public String operator = null;
		public Integer laminasyon = null;
		public Integer kaynak = null;
		public Integer haz = null;

		/**
		 * @return the siraNo
		 */
		public Integer getSiraNo() {
			return siraNo;
		}

		/**
		 * @param siraNo
		 *            the siraNo to set
		 */
		public void setSiraNo(Integer siraNo) {
			this.siraNo = siraNo;
		}

		/**
		 * @return the boruBarkod
		 */
		public String getBoruBarkod() {
			return boruBarkod;
		}

		/**
		 * @param boruBarkod
		 *            the boruBarkod to set
		 */
		public void setBoruBarkod(String boruBarkod) {
			this.boruBarkod = boruBarkod;
		}

		/**
		 * @return the tarih
		 */
		public String getTarih() {
			return tarih;
		}

		/**
		 * @param tarih
		 *            the tarih to set
		 */
		public void setTarih(String tarih) {
			this.tarih = tarih;
		}

		/**
		 * @return the vardiya
		 */
		public Integer getVardiya() {
			return vardiya;
		}

		/**
		 * @param vardiya
		 *            the vardiya to set
		 */
		public void setVardiya(Integer vardiya) {
			this.vardiya = vardiya;
		}

		/**
		 * @return the operator
		 */
		public String getOperator() {
			return operator;
		}

		/**
		 * @param operator
		 *            the operator to set
		 */
		public void setOperator(String operator) {
			this.operator = operator;
		}

		/**
		 * @return the laminasyon
		 */
		public Integer getLaminasyon() {
			return laminasyon;
		}

		/**
		 * @param laminasyon
		 *            the laminasyon to set
		 */
		public void setLaminasyon(Integer laminasyon) {
			this.laminasyon = laminasyon;
		}

		/**
		 * @return the kaynak
		 */
		public Integer getKaynak() {
			return kaynak;
		}

		/**
		 * @param kaynak
		 *            the kaynak to set
		 */
		public void setKaynak(Integer kaynak) {
			this.kaynak = kaynak;
		}

		/**
		 * @return the haz
		 */
		public Integer getHaz() {
			return haz;
		}

		/**
		 * @param haz
		 *            the haz to set
		 */
		public void setHaz(Integer haz) {
			this.haz = haz;
		}

	}

	public class OfflineObject {

		public String boruBarkod = null;
		public String tarih = null;
		public Integer vardiya = null;
		public String operator = null;
		public Integer malzemeA = null;
		public Integer malzemeB = null;
		public Integer kaynakA = null;
		public Integer kaynakB = null;
		public Integer kaynak = null;
		public Integer laminasyon = null;
		public Integer haz = null;
		public String kalibrasyonSonuc = null;

		/**
		 * @return the boruBarkod
		 */
		public String getBoruBarkod() {
			return boruBarkod;
		}

		/**
		 * @param boruBarkod
		 *            the boruBarkod to set
		 */
		public void setBoruBarkod(String boruBarkod) {
			this.boruBarkod = boruBarkod;
		}

		/**
		 * @return the tarih
		 */
		public String getTarih() {
			return tarih;
		}

		/**
		 * @param tarih
		 *            the tarih to set
		 */
		public void setTarih(String tarih) {
			this.tarih = tarih;
		}

		/**
		 * @return the vardiya
		 */
		public Integer getVardiya() {
			return vardiya;
		}

		/**
		 * @param vardiya
		 *            the vardiya to set
		 */
		public void setVardiya(Integer vardiya) {
			this.vardiya = vardiya;
		}

		/**
		 * @return the operator
		 */
		public String getOperator() {
			return operator;
		}

		/**
		 * @param operator
		 *            the operator to set
		 */
		public void setOperator(String operator) {
			this.operator = operator;
		}

		/**
		 * @return the kaynak
		 */
		public Integer getKaynak() {
			return kaynak;
		}

		/**
		 * @param kaynak
		 *            the kaynak to set
		 */
		public void setKaynak(Integer kaynak) {
			this.kaynak = kaynak;
		}

		/**
		 * @return the kalibrasyonSonuc
		 */
		public String getKalibrasyonSonuc() {
			return kalibrasyonSonuc;
		}

		/**
		 * @param kalibrasyonSonuc
		 *            the kalibrasyonSonuc to set
		 */
		public void setKalibrasyonSonuc(String kalibrasyonSonuc) {
			this.kalibrasyonSonuc = kalibrasyonSonuc;
		}

		/**
		 * @return the malzemeA
		 */
		public Integer getMalzemeA() {
			return malzemeA;
		}

		/**
		 * @param malzemeA
		 *            the malzemeA to set
		 */
		public void setMalzemeA(Integer malzemeA) {
			this.malzemeA = malzemeA;
		}

		/**
		 * @return the malzemeB
		 */
		public Integer getMalzemeB() {
			return malzemeB;
		}

		/**
		 * @param malzemeB
		 *            the malzemeB to set
		 */
		public void setMalzemeB(Integer malzemeB) {
			this.malzemeB = malzemeB;
		}

		/**
		 * @return the kaynakA
		 */
		public Integer getKaynakA() {
			return kaynakA;
		}

		/**
		 * @param kaynakA
		 *            the kaynakA to set
		 */
		public void setKaynakA(Integer kaynakA) {
			this.kaynakA = kaynakA;
		}

		/**
		 * @return the kaynakB
		 */
		public Integer getKaynakB() {
			return kaynakB;
		}

		/**
		 * @param kaynakB
		 *            the kaynakB to set
		 */
		public void setKaynakB(Integer kaynakB) {
			this.kaynakB = kaynakB;
		}

		/**
		 * @return the laminasyon
		 */
		public Integer getLaminasyon() {
			return laminasyon;
		}

		/**
		 * @param laminasyon
		 *            the laminasyon to set
		 */
		public void setLaminasyon(Integer laminasyon) {
			this.laminasyon = laminasyon;
		}

		/**
		 * @return the haz
		 */
		public Integer getHaz() {
			return haz;
		}

		/**
		 * @param haz
		 *            the haz to set
		 */
		public void setHaz(Integer haz) {
			this.haz = haz;
		}

	}

	public class ManyetikObject {

		public String boruBarkod = null;
		public String tarih = null;
		public Integer vardiya = null;
		public String operator = null;
		public Integer boruAgzi = null;
		public Integer taslanmis = null;
		public Integer tamirat = null;
		public Integer hataAdedi = null;
		public Integer turu = null;
		public Integer boyutlari = null;
		public String kalibrasyon = null;
		public String sonuc = null;
		public String aciklama = null;

		/**
		 * @return the boruBarkod
		 */
		public String getBoruBarkod() {
			return boruBarkod;
		}

		/**
		 * @param boruBarkod
		 *            the boruBarkod to set
		 */
		public void setBoruBarkod(String boruBarkod) {
			this.boruBarkod = boruBarkod;
		}

		/**
		 * @return the tarih
		 */
		public String getTarih() {
			return tarih;
		}

		/**
		 * @param tarih
		 *            the tarih to set
		 */
		public void setTarih(String tarih) {
			this.tarih = tarih;
		}

		/**
		 * @return the vardiya
		 */
		public Integer getVardiya() {
			return vardiya;
		}

		/**
		 * @param vardiya
		 *            the vardiya to set
		 */
		public void setVardiya(Integer vardiya) {
			this.vardiya = vardiya;
		}

		/**
		 * @return the operator
		 */
		public String getOperator() {
			return operator;
		}

		/**
		 * @param operator
		 *            the operator to set
		 */
		public void setOperator(String operator) {
			this.operator = operator;
		}

		/**
		 * @return the boruAgzi
		 */
		public Integer getBoruAgzi() {
			return boruAgzi;
		}

		/**
		 * @param boruAgzi
		 *            the boruAgzi to set
		 */
		public void setBoruAgzi(Integer boruAgzi) {
			this.boruAgzi = boruAgzi;
		}

		/**
		 * @return the taslanmis
		 */
		public Integer getTaslanmis() {
			return taslanmis;
		}

		/**
		 * @param taslanmis
		 *            the taslanmis to set
		 */
		public void setTaslanmis(Integer taslanmis) {
			this.taslanmis = taslanmis;
		}

		/**
		 * @return the tamirat
		 */
		public Integer getTamirat() {
			return tamirat;
		}

		/**
		 * @param tamirat
		 *            the tamirat to set
		 */
		public void setTamirat(Integer tamirat) {
			this.tamirat = tamirat;
		}

		/**
		 * @return the hataAdedi
		 */
		public Integer getHataAdedi() {
			return hataAdedi;
		}

		/**
		 * @param hataAdedi
		 *            the hataAdedi to set
		 */
		public void setHataAdedi(Integer hataAdedi) {
			this.hataAdedi = hataAdedi;
		}

		/**
		 * @return the turu
		 */
		public Integer getTuru() {
			return turu;
		}

		/**
		 * @param turu
		 *            the turu to set
		 */
		public void setTuru(Integer turu) {
			this.turu = turu;
		}

		/**
		 * @return the boyutlari
		 */
		public Integer getBoyutlari() {
			return boyutlari;
		}

		/**
		 * @param boyutlari
		 *            the boyutlari to set
		 */
		public void setBoyutlari(Integer boyutlari) {
			this.boyutlari = boyutlari;
		}

		/**
		 * @return the kalibrasyon
		 */
		public String getKalibrasyon() {
			return kalibrasyon;
		}

		/**
		 * @param kalibrasyon
		 *            the kalibrasyon to set
		 */
		public void setKalibrasyon(String kalibrasyon) {
			this.kalibrasyon = kalibrasyon;
		}

		/**
		 * @return the sonuc
		 */
		public String getSonuc() {
			return sonuc;
		}

		/**
		 * @param sonuc
		 *            the sonuc to set
		 */
		public void setSonuc(String sonuc) {
			this.sonuc = sonuc;
		}

		/**
		 * @return the aciklama
		 */
		public String getAciklama() {
			return aciklama;
		}

		/**
		 * @param aciklama
		 *            the aciklama to set
		 */
		public void setAciklama(String aciklama) {
			this.aciklama = aciklama;
		}

	}

	public void goToGunlukRaporlar() {

		FacesContextUtils.redirect(config.getPageUrl().GUNLUK_RAPORLAR_PAGE);
	}

	public void fillTestListFromBarkod(String prmBarkod, Integer prmType) {

		// barkodNo ya göre pipe bulma
		this.prmType = prmType;
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " BARKOD NUMARALI BORU SİSTEMDE YOKTUR!", null));
			return;
		} else {
			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);

			MachinePipeLinkManager machinePipeLinkManager = new MachinePipeLinkManager();
			if (prmType == -1) {
				yesterdayPipes = machinePipeLinkManager
						.findYesterdaysProduction(selectedMachineId);
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(selectedPipe
						.getSalesItem().getProposal().getCustomer()
						.getShortName()
						+ "   "
						+ selectedPipe.getSalesItem().getProposal()
								.getSalesOrder().getOrderNo()
						+ "-"
						+ selectedPipe.getSalesItem().getItemNo()
						+ " NUMARALI SİPARİŞTEN DÜN ÜRETİLMİŞ "
						+ yesterdayPipes.size() + " BORU SEÇİLMİŞTİR!"));
			} else if (prmType == 1) {
				yesterdayPipes = machinePipeLinkManager.findByItemId(
						selectedPipe.getSalesItem().getItemId(),
						selectedMachineId);
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(selectedPipe
						.getSalesItem().getProposal().getCustomer()
						.getShortName()
						+ "   "
						+ selectedPipe.getSalesItem().getProposal()
								.getSalesOrder().getOrderNo()
						+ "-"
						+ selectedPipe.getSalesItem().getItemNo()
						+ " NUMARALI SİPARİŞTEN TOPLAM ÜRETİLMİŞ "
						+ yesterdayPipes.size() + " BORU SEÇİLMİŞTİR!"));
			}
		}
	}

	public void ruloBul(List<MachinePipeLink> prmPipeList) {

		List<Rulo> rulos = new ArrayList<Rulo>();
		pipeRulos.clear();
		// int j = 0;
		boolean check = true;
		rulos.add(prmPipeList.get(0).getPipe().getRuloPipeLink().getRulo());
		pipeRulos.add(prmPipeList.get(0).getPipe().getRuloPipeLink().getRulo());
		for (int i = 0; i < prmPipeList.size(); i++) {
			for (int j = 0; j < rulos.size(); j++) {
				if (rulos.get(j).equals(
						prmPipeList.get(i).getPipe().getRuloPipeLink()
								.getRulo())) {
					check = true;
					break;
				} else {
					check = false;
				}
			}
			if (!check) {
				rulos.add(prmPipeList.get(i).getPipe().getRuloPipeLink()
						.getRulo());
				pipeRulos.add(prmPipeList.get(i).getPipe().getRuloPipeLink()
						.getRulo());
				check = true;
			}
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void reportPackageXls() {

		if (selectedPipe.getPipeId() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {

			for (int i = 0; i < yesterdayPipes.size(); i++) {
				Point point = new Point();
				point.setBoruBarkod(yesterdayPipes.get(i).getPipe()
						.getPipeBarkodNo());

				try {
					if (yesterdayPipes.get(i).getPipe()
							.getTestTahribatsizOtomatikUtSonuclar() != null
							&& yesterdayPipes.get(i).getPipe()
									.getTestTahribatsizOtomatikUtSonuclar()
									.size() > 0) {
						point.setOnlineTarih(yesterdayPipes.get(i).getPipe()
								.getTestTahribatsizOtomatikUtSonuclar().get(0)
								.getEklemeZamani().toString());
						point.setOnlineVardiya(UtilInsCore
								.findShift(yesterdayPipes.get(i).getPipe()
										.getTestTahribatsizOtomatikUtSonuclar()
										.get(0).getEklemeZamani().getHours()));
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					point.setOnlineTarih(null);
					point.setOnlineVardiya(null);
				}

				try {
					if (yesterdayPipes.get(i).getPipe()
							.getTestTahribatsizHidrostatikler() != null
							&& yesterdayPipes.get(i).getPipe()
									.getTestTahribatsizHidrostatikler().size() > 1) {
						point.setHidroTarihBir(yesterdayPipes.get(i).getPipe()
								.getTestTahribatsizHidrostatikler().get(0)
								.getEklemeZamani().toString());
						point.setHidroTarihIki(yesterdayPipes.get(i).getPipe()
								.getTestTahribatsizHidrostatikler().get(1)
								.getEklemeZamani().toString());
						point.setHidroSonuc("OK");
					} else if (yesterdayPipes.get(i).getPipe()
							.getTestTahribatsizHidrostatikler() != null
							&& yesterdayPipes.get(i).getPipe()
									.getTestTahribatsizHidrostatikler().size() == 1) {
						point.setHidroTarihBir(yesterdayPipes.get(i).getPipe()
								.getTestTahribatsizHidrostatikler().get(0)
								.getEklemeZamani().toString());
						point.setHidroSonuc("OK");
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					point.setHidroTarihBir(null);
					point.setHidroTarihIki(null);
					point.setHidroSonuc("");
				}

				try {
					if (yesterdayPipes.get(i).getPipe()
							.getTestTahribatsizManuelUtSonuclar() != null
							&& yesterdayPipes.get(i).getPipe()
									.getTestTahribatsizManuelUtSonuclar()
									.size() > 0) {
						point.setManualTarih(yesterdayPipes.get(i).getPipe()
								.getTestTahribatsizManuelUtSonuclar().get(0)
								.getEklemeZamani().toString());
						point.setManualSonuc("-");
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					point.setManualTarih(null);
					point.setManualSonuc("-");
				}

				try {
					if (yesterdayPipes.get(i).getPipe()
							.getTestTahribatsizFloroskopikSonuclar() != null
							&& yesterdayPipes.get(i).getPipe()
									.getTestTahribatsizFloroskopikSonuclar()
									.size() > 0) {
						point.setRadyoskopiTarihBir(yesterdayPipes.get(i)
								.getPipe()
								.getTestTahribatsizFloroskopikSonuclar().get(0)
								.getEklemeZamani().toString());
						point.setRadyoskopiTamirBir(yesterdayPipes.get(i)
								.getPipe()
								.getTestTahribatsizFloroskopikSonuclar().get(0)
								.getKoordinatL().toString());
						point.setRadyoskopiTarihIki("-");
						point.setRadyoskopiTamirIki("-");
						point.setRadyoskopiSonuc("-");
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					point.setRadyoskopiTarihBir(null);
					point.setRadyoskopiTamirBir(null);
					point.setRadyoskopiTarihIki("-");
					point.setRadyoskopiTamirIki("-");
					point.setRadyoskopiSonuc("-");
				}

				try {
					if (yesterdayPipes.get(i).getPipe()
							.getTestTahribatsizOfflineOtomatikUtSonuclar() != null
							&& yesterdayPipes
									.get(i)
									.getPipe()
									.getTestTahribatsizOfflineOtomatikUtSonuclar()
									.size() > 0) {
						point.setOfflineTarih(yesterdayPipes.get(i).getPipe()
								.getTestTahribatsizOfflineOtomatikUtSonuclar()
								.get(0).getEklemeZamani().toString());
						point.setOfflineTamir(yesterdayPipes.get(i).getPipe()
								.getTestTahribatsizOfflineOtomatikUtSonuclar()
								.get(0).getKoordinatL().toString());
						point.setOfflineSonuc("-");
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					point.setOfflineTarih(null);
					point.setOfflineTamir(null);
					point.setOfflineSonuc("-");
				}

				point.setOfflineManualTarih("-");
				point.setOfflineManualTamir("-");
				point.setOfflineManualsonuc("-");

				try {
					if (yesterdayPipes.get(i).getPipe()
							.getTestTahribatsizGozOlcuSonuclar() != null
							&& yesterdayPipes.get(i).getPipe()
									.getTestTahribatsizGozOlcuSonuclar().size() > 0) {
						if (yesterdayPipes.get(i).getPipe()
								.getTestTahribatsizGozOlcuSonuclar().get(0)
								.getEklemeZamani() != null) {
							point.setSonGozTarih(yesterdayPipes.get(i)
									.getPipe()
									.getTestTahribatsizGozOlcuSonuclar().get(0)
									.getEklemeZamani().toString());
						}
						if (yesterdayPipes.get(i).getPipe()
								.getTestTahribatsizGozOlcuSonuclar().get(0)
								.getBoruUzunluk() != null) {
							point.setSonGozBoy(yesterdayPipes.get(i).getPipe()
									.getTestTahribatsizGozOlcuSonuclar().get(0)
									.getBoruUzunluk().toString());
						}
						if (yesterdayPipes.get(i).getPipe()
								.getTestTahribatsizGozOlcuSonuclar().get(0)
								.getResult() != null) {
							if (yesterdayPipes.get(i).getPipe()
									.getTestTahribatsizGozOlcuSonuclar().get(0)
									.getResult() == -1) {
								point.setSonGozSonuc("NOT SELECTED");
							} else if (yesterdayPipes.get(i).getPipe()
									.getTestTahribatsizGozOlcuSonuclar().get(0)
									.getResult() == 1) {
								point.setSonGozSonuc("ACCEPTED");
							} else if (yesterdayPipes.get(i).getPipe()
									.getTestTahribatsizGozOlcuSonuclar().get(0)
									.getResult() == 2) {
								point.setSonGozSonuc("DENIED");
							} else if (yesterdayPipes.get(i).getPipe()
									.getTestTahribatsizGozOlcuSonuclar().get(0)
									.getResult() == 3) {
								point.setSonGozSonuc("REPAIR");
							} else if (yesterdayPipes.get(i).getPipe()
									.getTestTahribatsizGozOlcuSonuclar().get(0)
									.getResult() == 4) {
								point.setSonGozSonuc("TORNA");
							} else if (yesterdayPipes.get(i).getPipe()
									.getTestTahribatsizGozOlcuSonuclar().get(0)
									.getResult() == 5) {
								point.setSonGozSonuc("CUT");
							} else if (yesterdayPipes.get(i).getPipe()
									.getTestTahribatsizGozOlcuSonuclar().get(0)
									.getResult() == 6) {
								point.setSonGozSonuc("HYDRO");
								point.setSonGozKes("CUT");
								point.setSonGozKesMetre("-");
							}
						}
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					point.setSonGozTarih(null);
					point.setSonGozBoy(null);
					point.setSonGozSonuc("-");
				}

				try {
					if (yesterdayPipes.get(i).getPipe()
							.getTestTahribatsizManyetikParcacikSonuclar() != null
							&& yesterdayPipes
									.get(i)
									.getPipe()
									.getTestTahribatsizManyetikParcacikSonuclar()
									.size() > 0) {
						point.setSonGozBobinYokeTarih(yesterdayPipes.get(i)
								.getPipe()
								.getTestTahribatsizManyetikParcacikSonuclar()
								.get(0).getEklemeZamani().toString());
						if (yesterdayPipes.get(i).getPipe()
								.getTestTahribatsizManyetikParcacikSonuclar()
								.get(0).getSonuc() == -1) {
							point.setSonGozBobinYokeSonuc("NOT SELECTED");
						} else if (yesterdayPipes.get(i).getPipe()
								.getTestTahribatsizManyetikParcacikSonuclar()
								.get(0).getSonuc() == 1) {
							point.setSonGozBobinYokeSonuc("ACCEPTED");
						} else if (yesterdayPipes.get(i).getPipe()
								.getTestTahribatsizManyetikParcacikSonuclar()
								.get(0).getSonuc() == 0) {
							point.setSonGozBobinYokeSonuc("DENIED");
						} else if (yesterdayPipes.get(i).getPipe()
								.getTestTahribatsizManyetikParcacikSonuclar()
								.get(0).getSonuc() == 2) {
							point.setSonGozBobinYokeSonuc("REPAIR");
						}
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					point.setSonGozBobinYokeTarih(null);
					point.setSonGozBobinYokeSonuc("-");
				}

				points.add(point);
			}

			Map dataMap = new HashMap();
			dataMap.put("sonuc", points);
			dataMap.put("siparis", selectedPipe.getSalesItem());

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/gunlukraporlar/reportPackage.xls",
					selectedPipe.getSalesItem().getProposal().getCustomer()
							.getShortName());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void coilReportXls() {

		if (selectedPipe.getPipeId() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {

			ruloBul(yesterdayPipes);

			Map dataMap = new HashMap();
			dataMap.put("rulos", pipeRulos);
			dataMap.put("siparis", selectedPipe.getSalesItem());

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/gunlukraporlar/coil.xls", selectedPipe
							.getSalesItem().getProposal().getCustomer()
							.getShortName());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void consumablesReportXls(Integer prmDurum) {

		if (selectedPipe.getPipeId() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {

			EdwPipeLinkManager linkManager = new EdwPipeLinkManager();
			List<EdwPipeLink> links = new ArrayList<EdwPipeLink>();
			if (prmType == 1) {
				links = linkManager.tumUretilmisBorularIcin(selectedPipe
						.getSalesItem().getItemId(), selectedMachineId);
			} else if (prmType == -1) {
				links = linkManager.dunUretilmisBorularIcin(selectedMachineId);
			}

			Map dataMap = new HashMap();
			dataMap.put("links", links);
			dataMap.put("siparis", selectedPipe.getSalesItem());

			ReportUtil reportUtil = new ReportUtil();
			if (prmDurum == 2) {
				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/gunlukraporlar/consumables.xls",
						selectedPipe.getSalesItem().getProposal().getCustomer()
								.getShortName());
			} else if (prmDurum == 1) {
				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/gunlukraporlar/sawInsideWelding.xls",
						selectedPipe.getSalesItem().getProposal().getCustomer()
								.getShortName());
			} else if (prmDurum == 0) {
				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/gunlukraporlar/sawOutsideWelding.xls",
						selectedPipe.getSalesItem().getProposal().getCustomer()
								.getShortName());
			}

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void hidroTestReportXls() {

		if (selectedPipe.getPipeId() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {

			TestTahribatsizHidrostatikManager manager = new TestTahribatsizHidrostatikManager();
			List<TestTahribatsizHidrostatik> sonucs = new ArrayList<TestTahribatsizHidrostatik>();

			if (prmType == 1) {
				sonucs = manager.tumUretilmisBorularIcin(selectedPipe
						.getSalesItem().getItemId(), selectedMachineId);
			} else if (prmType == -1) {
				sonucs = manager.dunUretilmisBorularIcin(selectedMachineId);
			}

			List<HidrostaticTestsSpec> hidroSpecs = new ArrayList<HidrostaticTestsSpec>();
			HidrostaticTestsSpecManager specManager = new HidrostaticTestsSpecManager();
			hidroSpecs = specManager.findByItemId(selectedPipe.getSalesItem()
					.getItemId());

			Map dataMap = new HashMap();
			dataMap.put("siparis", selectedPipe.getSalesItem());
			dataMap.put("sonucs", sonucs);
			dataMap.put("specs", hidroSpecs);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/gunlukraporlar/hydrostatic.xls",
					selectedPipe.getSalesItem().getProposal().getCustomer()
							.getShortName());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void ultrasonicTestReportXls() {

		if (selectedPipe.getPipeId() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {

			TestTahribatsizOtomatikUtSonucManager manager = new TestTahribatsizOtomatikUtSonucManager();
			List<TestTahribatsizOtomatikUtSonuc> sonucs = new ArrayList<TestTahribatsizOtomatikUtSonuc>();
			List<TestTahribatsizOtomatikUtSonuc> boruBazli = new ArrayList<TestTahribatsizOtomatikUtSonuc>();
			if (prmType == 1) {
				sonucs = manager.tumUretilmisBorularIcin(selectedPipe
						.getSalesItem().getItemId(), selectedMachineId);
			} else if (prmType == -1) {
				sonucs = manager.dunUretilmisBorularIcin(selectedMachineId);
			}

			UltrasonicObject ultrasonicObject = new UltrasonicObject();
			String prmPipeId = sonucs.get(0).getPipe().getPipeBarkodNo();
			Integer countKaynak = 0;
			Integer countLaminasyon = 0;
			Integer countHaz = 0;
			for (int i = 0; i < sonucs.size(); i++) {

				if (sonucs.get(i).getPipe().getPipeBarkodNo().equals(prmPipeId)) {
					boruBazli.add(sonucs.get(i));
				} else {

					for (int j = 0; j < boruBazli.size(); j++) {
						if (boruBazli.get(j).getKaynakLaminasyon() == 1) {
							countKaynak++;
						} else if (boruBazli.get(j).getKaynakLaminasyon() == 2) {
							countLaminasyon++;
						} else if (boruBazli.get(j).getKaynakLaminasyon() == 3) {
							countHaz++;
						}
					}

					ultrasonicObject.setBoruBarkod(prmPipeId);
					ultrasonicObject.setTarih(sonucs.get(i).getEklemeZamani()
							.toString());
					ultrasonicObject.setOperator(sonucs.get(i)
							.getEkleyenEmployee().getEmployee().getFirstname()
							+ " "
							+ sonucs.get(i).getEkleyenEmployee().getEmployee()
									.getLastname());
					ultrasonicObject.setKaynak(countKaynak);
					ultrasonicObject.setLaminasyon(countLaminasyon);
					ultrasonicObject.setHaz(countHaz);

					ultrasonicObjects.add(ultrasonicObject);
					ultrasonicObject = new UltrasonicObject();
					countKaynak = 0;
					countLaminasyon = 0;
					countHaz = 0;
					boruBazli.clear();
					boruBazli.add(sonucs.get(i));
					prmPipeId = sonucs.get(i).getPipe().getPipeBarkodNo();
				}

			}

			Map dataMap = new HashMap();
			dataMap.put("siparis", selectedPipe.getSalesItem());
			dataMap.put("sonucs", ultrasonicObjects);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/gunlukraporlar/online.xls", selectedPipe
							.getSalesItem().getProposal().getCustomer()
							.getShortName());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void repairWeldReportXls() {

		if (selectedPipe.getPipeId() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {

			TestTahribatsizTamirManager manager = new TestTahribatsizTamirManager();
			List<TestTahribatsizTamir> sonucs = new ArrayList<TestTahribatsizTamir>();
			if (prmType == 1) {
				sonucs = manager.tumUretilmisBorularIcin(selectedPipe
						.getSalesItem().getItemId(), selectedMachineId);
			} else if (prmType == -1) {
				sonucs = manager.dunUretilmisBorularIcin(selectedMachineId);
			}

			Map dataMap = new HashMap();
			dataMap.put("siparis", selectedPipe.getSalesItem());
			dataMap.put("sonucs", sonucs);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/gunlukraporlar/welding.xls", selectedPipe
							.getSalesItem().getProposal().getCustomer()
							.getShortName());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void manuelTestReportXls() {

		if (selectedPipe.getPipeId() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {

			TestTahribatsizManuelUtSonucManager manager = new TestTahribatsizManuelUtSonucManager();
			List<TestTahribatsizManuelUtSonuc> sonucs = new ArrayList<TestTahribatsizManuelUtSonuc>();
			if (prmType == 1) {
				sonucs = manager.tumUretilmisBorularIcin(selectedPipe
						.getSalesItem().getItemId(), selectedMachineId);
			} else if (prmType == -1) {
				sonucs = manager.dunUretilmisBorularIcin(selectedMachineId);
			}

			List<NondestructiveTestsSpec> nondestructiveTestsSpecs = new ArrayList<NondestructiveTestsSpec>();
			for (NondestructiveTestsSpec nondestructiveTestsSpec : selectedPipe
					.getSalesItem().getNondestructiveTestsSpecs()) {
				if (nondestructiveTestsSpec.getGlobalId() == 1004)
					nondestructiveTestsSpecs.add(nondestructiveTestsSpec);
			}
			if (nondestructiveTestsSpecs.size() < 1) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"KALİTE TANIMLARI EKSİKTİR. RAPOR OLUŞTURULAMADI, HATA!",
								null));
				return;
			}

			muayeneyiYapan = sonucs.get(0).getEkleyenEmployee().getEmployee()
					.getFirstname()
					+ " "
					+ sonucs.get(0).getEkleyenEmployee().getEmployee()
							.getLastname();
			hazirlayan = userBean.getUserFullName();

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(sonucs.get(0)
					.getEklemeZamani()));

			Map dataMap = new HashMap();
			dataMap.put("siparis", selectedPipe.getSalesItem());
			dataMap.put("siparisManuelOzellik", nondestructiveTestsSpecs);
			dataMap.put("manuelSonuc", sonucs);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("date", date);
			dataMap.put("kalibrasyon", sonucs.get(0).getKalibrasyon());

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/gunlukraporlar/manuel.xls", selectedPipe
							.getSalesItem().getProposal().getCustomer()
							.getShortName());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void floroskopicTestReportXls() {

		if (selectedPipe.getPipeId() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {

			List<NondestructiveTestsSpec> nondestructiveTestsSpecs = new ArrayList<NondestructiveTestsSpec>();
			for (NondestructiveTestsSpec nondestructiveTestsSpec : selectedPipe
					.getSalesItem().getNondestructiveTestsSpecs()) {
				if (nondestructiveTestsSpec.getGlobalId() == 1006)
					nondestructiveTestsSpecs.add(nondestructiveTestsSpec);
			}
			if (nondestructiveTestsSpecs.size() < 1) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"KALİTE TANIMLARI EKSİKTİR. RAPOR OLUŞTURULAMADI, HATA!",
								null));
				return;
			}

			TestTahribatsizFloroskopikSonucManager testTahribatsizFloroskopikSonucManager = new TestTahribatsizFloroskopikSonucManager();
			List<TestTahribatsizFloroskopikSonuc> testTahribatsizFloroskopikSonucs = new ArrayList<TestTahribatsizFloroskopikSonuc>();

			if (prmType == 1) {
				testTahribatsizFloroskopikSonucs = testTahribatsizFloroskopikSonucManager
						.tumUretilmisBorularIcin(selectedPipe.getSalesItem()
								.getItemId(), selectedMachineId);
			} else if (prmType == -1) {
				testTahribatsizFloroskopikSonucs = testTahribatsizFloroskopikSonucManager
						.dunUretilmisBorularIcin(selectedMachineId);
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy")
					.format(testTahribatsizFloroskopikSonucs.get(0)
							.getEklemeZamani()));
			muayeneyiYapan = testTahribatsizFloroskopikSonucs.get(0).getUser()
					.getEmployee().getFirstname()
					+ " "
					+ testTahribatsizFloroskopikSonucs.get(0).getUser()
							.getEmployee().getLastname();
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", selectedPipe.getSalesItem());
			dataMap.put("siparisFloroskopikOzellik", nondestructiveTestsSpecs);
			dataMap.put("floroskopikSonuc", testTahribatsizFloroskopikSonucs);
			dataMap.put("date", date);
			dataMap.put("boruUcu", "A");
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("kalibrasyon", testTahribatsizFloroskopikSonucs.get(0)
					.getKalibrasyon());

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/gunlukraporlar/floroskopic.xls",
					selectedPipe.getSalesItem().getProposal().getCustomer()
							.getShortName());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void offlineTestReportXls() {

		if (selectedPipe.getPipeId() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {

			TestTahribatsizOfflineOtomatikUtSonucManager manager = new TestTahribatsizOfflineOtomatikUtSonucManager();
			List<TestTahribatsizOfflineOtomatikUtSonuc> sonucs = new ArrayList<TestTahribatsizOfflineOtomatikUtSonuc>();
			List<TestTahribatsizOfflineOtomatikUtSonuc> boruBazli = new ArrayList<TestTahribatsizOfflineOtomatikUtSonuc>();
			if (prmType == 1) {
				sonucs = manager.tumUretilmisBorularIcin(selectedPipe
						.getSalesItem().getItemId(), selectedMachineId);
			} else if (prmType == -1) {
				sonucs = manager.dunUretilmisBorularIcin(selectedMachineId);
			}

			OfflineObject offlineObject = new OfflineObject();
			String prmPipeId = sonucs.get(0).getPipe().getPipeBarkodNo();
			Integer countKaynak = 0;
			Integer countLaminasyon = 0;
			Integer countHaz = 0;
			Integer countMalzemeA = 0;
			Integer countMalzemeB = 0;
			Integer countKaynakA = 0;
			Integer countKaynakB = 0;
			for (int i = 0; i < sonucs.size(); i++) {

				if (sonucs.get(i).getPipe().getPipeBarkodNo().equals(prmPipeId)) {
					boruBazli.add(sonucs.get(i));
				} else {

					for (int j = 0; j < boruBazli.size(); j++) {
						if (boruBazli.get(j).getKaynakLaminasyon() == 1) {
							countKaynak++;
						} else if (boruBazli.get(j).getKaynakLaminasyon() == 2) {
							countLaminasyon++;
						} else if (boruBazli.get(j).getKaynakLaminasyon() == 3) {
							countHaz++;
						} else if (boruBazli.get(j).getKaynakLaminasyon() == 4) {
							countMalzemeA++;
						} else if (boruBazli.get(j).getKaynakLaminasyon() == 5) {
							countKaynakA++;
						} else if (boruBazli.get(j).getKaynakLaminasyon() == 6) {
							countMalzemeB++;
						} else if (boruBazli.get(j).getKaynakLaminasyon() == 7) {
							countKaynakB++;
						}
					}

					offlineObject.setBoruBarkod(prmPipeId);
					offlineObject.setTarih(sonucs.get(i).getEklemeZamani()
							.toString());
					offlineObject.setOperator(sonucs.get(i)
							.getEkleyenEmployee().getEmployee().getFirstname()
							+ " "
							+ sonucs.get(i).getEkleyenEmployee().getEmployee()
									.getLastname());
					offlineObject.setKaynak(countKaynak);
					offlineObject.setLaminasyon(countLaminasyon);
					offlineObject.setHaz(countHaz);
					offlineObject.setMalzemeA(countMalzemeA);
					offlineObject.setMalzemeB(countMalzemeB);
					offlineObject.setKaynakA(countKaynakA);
					offlineObject.setKaynakB(countKaynakB);

					offlineObjects.add(offlineObject);
					offlineObject = new OfflineObject();
					countKaynak = 0;
					countLaminasyon = 0;
					countHaz = 0;
					countMalzemeA = 0;
					countMalzemeB = 0;
					countKaynakA = 0;
					countKaynakB = 0;
					boruBazli.clear();
					boruBazli.add(sonucs.get(i));
					prmPipeId = sonucs.get(i).getPipe().getPipeBarkodNo();
				}

			}

			Map dataMap = new HashMap();
			dataMap.put("siparis", selectedPipe.getSalesItem());
			dataMap.put("sonucs", offlineObjects);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/gunlukraporlar/offline.xls", selectedPipe
							.getSalesItem().getProposal().getCustomer()
							.getShortName());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void finalVisualReportXls() {

		if (selectedPipe.getPipeId() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {

			BoruBoyutsalKontrolToleransManager toleransManager = new BoruBoyutsalKontrolToleransManager();
			List<BoruBoyutsalKontrolTolerans> boruBoyutsalKontrolTolerans = new ArrayList<BoruBoyutsalKontrolTolerans>();
			boruBoyutsalKontrolTolerans = toleransManager
					.seciliBoyutsalKontrol(selectedPipe.getSalesItem()
							.getItemId());
			if (boruBoyutsalKontrolTolerans.size() < 1) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"BORU BOYUTSAL KONTROL ve TOLERANSLARI GİRİLMEMİŞTİR, KALİTE VERİLERİ BOŞTUR. RAPOR OLUŞTURULAMADI, HATA!",
								null));
				return;
			}

			TestTahribatsizGozOlcuSonucManager testTahribatsizGozOlcuSonucManager = new TestTahribatsizGozOlcuSonucManager();
			List<TestTahribatsizGozOlcuSonuc> testTahribatsizGozOlcuSonucs = new ArrayList<TestTahribatsizGozOlcuSonuc>();
			testTahribatsizGozOlcuSonucs = testTahribatsizGozOlcuSonucManager
					.tumUretilmisBorularIcin(selectedPipe.getSalesItem()
							.getItemId(), selectedMachineId);

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy")
					.format(testTahribatsizGozOlcuSonucs.get(0)
							.getEklemeZamani()));

			Map dataMap = new HashMap();
			dataMap.put("siparis", selectedPipe.getSalesItem());
			dataMap.put("gozOlcuSonuc", testTahribatsizGozOlcuSonucs);
			dataMap.put("tolerans", boruBoyutsalKontrolTolerans.get(0));
			dataMap.put("date", date);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/gunlukraporlar/finalvisual.xls",
					selectedPipe.getSalesItem().getProposal().getCustomer()
							.getShortName());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void manyetikParcacikTestReportXls(Integer prmTest) {

		if (selectedPipe.getPipeId() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {

			TestTahribatsizManyetikParcacikSonucManager manager = new TestTahribatsizManyetikParcacikSonucManager();
			List<TestTahribatsizManyetikParcacikSonuc> sonucs = new ArrayList<TestTahribatsizManyetikParcacikSonuc>();
			if (prmType == 1) {
				sonucs = manager.tumUretilmisBorularIcin(selectedPipe
						.getSalesItem().getItemId(), selectedMachineId);
			} else if (prmType == -1) {
				sonucs = manager.dunUretilmisBorularIcin(selectedMachineId);
			}

			List<NondestructiveTestsSpec> nondestructiveTestsSpecs = new ArrayList<NondestructiveTestsSpec>();
			for (NondestructiveTestsSpec nondestructiveTestsSpec : selectedPipe
					.getSalesItem().getNondestructiveTestsSpecs()) {
				if (nondestructiveTestsSpec.getGlobalId() == 1006)
					nondestructiveTestsSpecs.add(nondestructiveTestsSpec);
			}
			if (nondestructiveTestsSpecs.size() < 1) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"KALİTE TANIMLARI EKSİKTİR. RAPOR OLUŞTURULAMADI, HATA!",
								null));
				return;
			}

			Map dataMap = new HashMap();
			dataMap.put("siparis", selectedPipe.getSalesItem());
			dataMap.put("sonucs", sonucs);
			dataMap.put("specs", nondestructiveTestsSpecs);

			ReportUtil reportUtil = new ReportUtil();
			if (prmTest == 1) {
				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/gunlukraporlar/manyetikparcacik.xls",
						selectedPipe.getSalesItem().getProposal().getCustomer()
								.getShortName());
			} else if (prmTest == 2) {
				reportUtil
						.jxlsExportAsXls(
								dataMap,
								"/reportformats/gunlukraporlar/finalvisualgrinding.xls",
								selectedPipe.getSalesItem().getProposal()
										.getCustomer().getShortName());
			}
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	// setters getters

	/**
	 * @return the config
	 */
	public ConfigBean getConfig() {
		return config;
	}

	/**
	 * @param config
	 *            the config to set
	 */
	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the barkodNo
	 */
	public String getBarkodNo() {
		return barkodNo;
	}

	/**
	 * @param barkodNo
	 *            the barkodNo to set
	 */
	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the selectedMachineId
	 */
	public Integer getSelectedMachineId() {
		return selectedMachineId;
	}

	/**
	 * @param selectedMachineId
	 *            the selectedMachineId to set
	 */
	public void setSelectedMachineId(Integer selectedMachineId) {
		this.selectedMachineId = selectedMachineId;
	}

	/**
	 * @return the selectedPipe
	 */
	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	/**
	 * @param selectedPipe
	 *            the selectedPipe to set
	 */
	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	/**
	 * @return the raporVardiya
	 */
	public Integer getRaporVardiya() {
		return raporVardiya;
	}

	/**
	 * @param raporVardiya
	 *            the raporVardiya to set
	 */
	public void setRaporVardiya(Integer raporVardiya) {
		this.raporVardiya = raporVardiya;
	}

}
