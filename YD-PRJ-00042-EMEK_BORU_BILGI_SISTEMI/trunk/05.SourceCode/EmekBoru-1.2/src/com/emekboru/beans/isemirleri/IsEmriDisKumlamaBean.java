/**
 * 
 */
package com.emekboru.beans.isemirleri;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.emekboru.beans.istakipformu.IsTakipFormuKumlamaBean;
import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.isemri.IsEmriDisKumlama;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.CoatRawMaterialManager;
import com.emekboru.jpaman.isemirleri.IsEmriDisKumlamaManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.sales.order.SalesItemManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isEmriDisKumlamaBean")
@ViewScoped
public class IsEmriDisKumlamaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private IsEmriDisKumlama isEmriDisKumlamaForm = new IsEmriDisKumlama();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	@ManagedProperty(value = "#{isTakipFormuKumlamaBean}")
	private IsTakipFormuKumlamaBean isTakipFormuKumlamaBean;

	private List<CoatRawMaterial> allDisKumlamaMalzemeList = new ArrayList<CoatRawMaterial>();

	// kaplama kalite
	private List<IsolationTestDefinition> internalIsolationTestDefinitions = new ArrayList<IsolationTestDefinition>();
	private String kumlamaKalitesi = null;

	public IsEmriDisKumlamaBean() {

	}

	public void addOrUpdateIsEmriDisKumlama(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmItemId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		SalesItem salesItem = new SalesItemManager().loadObject(
				SalesItem.class, prmItemId);

		IsEmriDisKumlamaManager isMan = new IsEmriDisKumlamaManager();

		if (isEmriDisKumlamaForm.getId() == null) {

			isEmriDisKumlamaForm.setSalesItem(salesItem);
			isEmriDisKumlamaForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			isEmriDisKumlamaForm.setEkleyenEmployee(userBean.getUser());

			isMan.enterNew(isEmriDisKumlamaForm);

			// iç kumlama iş takip formu ekleme
			isTakipFormuKumlamaBean.addFromIsEmri(0, salesItem);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"DIŞ KAPLAMA İŞ EMRİ BAŞARIYLA EKLENMİŞTİR!"));
		} else {

			isEmriDisKumlamaForm.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			isEmriDisKumlamaForm.setGuncelleyenEmployee(userBean.getUser());
			isMan.updateEntity(isEmriDisKumlamaForm);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_WARN,
					"DIŞ KAPLAMA İŞ EMRİ BAŞARIYLA GÜNCELLENMİŞTİR!", null));
		}
	}

	public void deleteIsEmriDisKumlama() {

		IsEmriDisKumlamaManager isMan = new IsEmriDisKumlamaManager();

		if (isEmriDisKumlamaForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		isMan.delete(isEmriDisKumlamaForm);
		isEmriDisKumlamaForm = new IsEmriDisKumlama();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
				"İÇ KAPLAMA İŞ EMRİ BAŞARIYLA SİLİNMİŞTİR!", null));
	}

	@SuppressWarnings("static-access")
	public void loadIsEmri(Integer itemId) {

		IsEmriDisKumlamaManager isMan = new IsEmriDisKumlamaManager();
		IsolationTestDefinitionManager kaliteMan = new IsolationTestDefinitionManager();

		if (isMan.getAllIsEmriDisKumlama(itemId).size() > 0) {
			isEmriDisKumlamaForm = new IsEmriDisKumlamaManager()
					.getAllIsEmriDisKumlama(itemId).get(0);
		} else {
			isEmriDisKumlamaForm = new IsEmriDisKumlama();
		}

		internalIsolationTestDefinitions = kaliteMan
				.findIsolationByItemIdAndIsolationType(0,
						isEmriDisKumlamaForm.getSalesItem());
		for (int i = 0; i < internalIsolationTestDefinitions.size(); i++) {
			if (internalIsolationTestDefinitions.get(i).getGlobalId() == 2005) {
				kumlamaKalitesi = internalIsolationTestDefinitions.get(i)
						.getAciklama();
			}
			if (internalIsolationTestDefinitions.get(i).getGlobalId() == 2006) {
				kumlamaKalitesi = kumlamaKalitesi + " "
						+ internalIsolationTestDefinitions.get(i).getAciklama();
			}
		}
	}

	public void isEmriMalzemeEkle() {

		IsEmriDisKumlamaManager isMan = new IsEmriDisKumlamaManager();

		isMan.updateEntity(isEmriDisKumlamaForm);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
				"İÇ KAPLAMA MALZEMESİ BAŞARIYLA EKLENMİŞTİR!", null));
	}

	public void loadKumlamaMalzemeleri() {

		CoatRawMaterialManager materialMan = new CoatRawMaterialManager();
		// 1001 kumlama turudur - coat_material_type tablosundan
		allDisKumlamaMalzemeList = materialMan
				.findUnfinishedCoatMaterialByType(1001);
	}

	// setters getters

	public IsEmriDisKumlama getIsEmriDisKumlamaForm() {
		return isEmriDisKumlamaForm;
	}

	public void setIsEmriDisKumlamaForm(IsEmriDisKumlama isEmriDisKumlamaForm) {
		this.isEmriDisKumlamaForm = isEmriDisKumlamaForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<CoatRawMaterial> getAllDisKumlamaMalzemeList() {
		return allDisKumlamaMalzemeList;
	}

	public void setAllDisKumlamaMalzemeList(
			List<CoatRawMaterial> allDisKumlamaMalzemeList) {
		this.allDisKumlamaMalzemeList = allDisKumlamaMalzemeList;
	}

	public IsTakipFormuKumlamaBean getIsTakipFormuKumlamaBean() {
		return isTakipFormuKumlamaBean;
	}

	public void setIsTakipFormuKumlamaBean(
			IsTakipFormuKumlamaBean isTakipFormuKumlamaBean) {
		this.isTakipFormuKumlamaBean = isTakipFormuKumlamaBean;
	}

	/**
	 * @return the kumlamaKalitesi
	 */
	public String getKumlamaKalitesi() {
		return kumlamaKalitesi;
	}

	/**
	 * @param kumlamaKalitesi
	 *            the kumlamaKalitesi to set
	 */
	public void setKumlamaKalitesi(String kumlamaKalitesi) {
		this.kumlamaKalitesi = kumlamaKalitesi;
	}

}
