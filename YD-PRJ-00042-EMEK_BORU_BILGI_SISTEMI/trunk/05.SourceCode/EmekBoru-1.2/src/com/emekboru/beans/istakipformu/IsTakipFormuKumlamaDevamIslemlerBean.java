/**
 * 
 */
package com.emekboru.beans.istakipformu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.istakipformu.IsTakipFormuKumlama;
import com.emekboru.jpa.istakipformu.IsTakipFormuKumlamaDevamIslemler;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.istakipformu.IsTakipFormuKumlamaDevamIslemlerManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isTakipFormuKumlamaDevamIslemlerBean")
@ViewScoped
public class IsTakipFormuKumlamaDevamIslemlerBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<IsTakipFormuKumlamaDevamIslemler> allIsTakipFormuKumlamaDevamIslemlerList = new ArrayList<IsTakipFormuKumlamaDevamIslemler>();
	private IsTakipFormuKumlamaDevamIslemler isTakipFormuKumlamaDevamIslemlerForm = new IsTakipFormuKumlamaDevamIslemler();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private boolean buttonRender;

	private Pipe selectedPipe = new Pipe();

	private String barkodNo = null;

	private SalesItem selectedSalesItem = new SalesItem();
	private IsTakipFormuKumlama selectedIsTakipFormuKumlama = new IsTakipFormuKumlama();

	private Integer salesItemId = null;
	private Integer icDisForm;

	public IsTakipFormuKumlamaDevamIslemlerBean() {

	}

	public void reset() {

		isTakipFormuKumlamaDevamIslemlerForm = new IsTakipFormuKumlamaDevamIslemler();
		updateButtonRender = false;
		buttonRender = false;
	};

	public void addOrUpdateFormSonuc() {

		IsTakipFormuKumlamaDevamIslemlerManager devamManager = new IsTakipFormuKumlamaDevamIslemlerManager();

		if (isTakipFormuKumlamaDevamIslemlerForm.getId() == null) {

			try {

				isTakipFormuKumlamaDevamIslemlerForm
						.setEklemeZamani(UtilInsCore.getTarihZaman());
				isTakipFormuKumlamaDevamIslemlerForm
						.setEkleyenEmployee(userBean.getUser());
				isTakipFormuKumlamaDevamIslemlerForm.setIcDis(icDisForm);
				isTakipFormuKumlamaDevamIslemlerForm.setPipe(selectedPipe);
				isTakipFormuKumlamaDevamIslemlerForm.setSalesItem(selectedPipe
						.getSalesItem());

				devamManager.enterNew(isTakipFormuKumlamaDevamIslemlerForm);

				FacesContext context = FacesContext.getCurrentInstance();
				if (icDisForm == 1) {
					context.addMessage(null, new FacesMessage(
							"İÇ KUMLAMA İŞ TAKİP FORMU BAŞARIYLA EKLENMİŞTİR!"));
				} else if (icDisForm == 0) {
					context.addMessage(
							null,
							new FacesMessage(
									"DIŞ KUMLAMA İŞ TAKİP FORMU BAŞARIYLA EKLENMİŞTİR!"));
				}
			} catch (Exception ex) {

				System.out
						.println("isTakipFormuKumlamaDevamIslemlerBean.addOrUpdateFormSonuc-HATA");
			}

		} else {

			try {

				isTakipFormuKumlamaDevamIslemlerForm
						.setGuncellemeZamani(UtilInsCore.getTarihZaman());
				isTakipFormuKumlamaDevamIslemlerForm
						.setGuncelleyenEmployee(userBean.getUser());
				devamManager.updateEntity(isTakipFormuKumlamaDevamIslemlerForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"İÇ KUMLAMA İŞ TAKİP FORMU BAŞARIYLA GÜNCELLENMİŞTİR!"));
			} catch (Exception ex) {

				System.out
						.println("isTakipFormuKumlamaDevamIslemlerBean.addOrUpdateFormSonuc-HATA"
								+ ex.toString());
			}
		}
		fillTestList(selectedPipe.getSalesItem().getItemId(), icDisForm);
	}

	@SuppressWarnings("static-access")
	public void fillTestList(Integer prmItemId, Integer icDis) {

		IsTakipFormuKumlamaDevamIslemlerManager formManager = new IsTakipFormuKumlamaDevamIslemlerManager();
		allIsTakipFormuKumlamaDevamIslemlerList = formManager
				.getAllIsTakipFormuKumlamaDevamIslemler(prmItemId, icDis);
		buttonRender = true;
		salesItemId = prmItemId;
		icDisForm = icDis;
	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_WARN, prmBarkod
							+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!", null));
			return;
		} else {

			if (salesItemId != pipeMan.findByBarkodNo(prmBarkod).get(0)
					.getSalesItem().getItemId()) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_WARN,
								prmBarkod
										+ " BARKOD NUMARALI BORU, YANLIŞ SİPARİŞ BORUSUDUR, LÜTFEN KONTROL EDİNİZ!",
								null));
				return;
			}

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			IsTakipFormuKumlamaDevamIslemlerManager formManager = new IsTakipFormuKumlamaDevamIslemlerManager();
			allIsTakipFormuKumlamaDevamIslemlerList = formManager
					.getAllIsTakipFormuKumlamaDevamIslemler(selectedPipe
							.getSalesItem().getItemId(), icDisForm);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
		}
	}

	public void deleteFormSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		IsTakipFormuKumlamaDevamIslemlerManager formManager = new IsTakipFormuKumlamaDevamIslemlerManager();

		if (isTakipFormuKumlamaDevamIslemlerForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		formManager.delete(isTakipFormuKumlamaDevamIslemlerForm);
		isTakipFormuKumlamaDevamIslemlerForm = new IsTakipFormuKumlamaDevamIslemler();
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"İÇ KUMLAMA İŞ TAKİP FORMU BAŞARIYLA SİLİNMİŞTİR!"));

		fillTestList(selectedPipe.getSalesItem().getItemId(), icDisForm);
	}

	// setters getters

	public List<IsTakipFormuKumlamaDevamIslemler> getAllIsTakipFormuKumlamaDevamIslemlerList() {
		return allIsTakipFormuKumlamaDevamIslemlerList;
	}

	public void setAllIsTakipFormuKumlamaDevamIslemlerList(
			List<IsTakipFormuKumlamaDevamIslemler> allIsTakipFormuKumlamaDevamIslemlerList) {
		this.allIsTakipFormuKumlamaDevamIslemlerList = allIsTakipFormuKumlamaDevamIslemlerList;
	}

	public IsTakipFormuKumlamaDevamIslemler getIsTakipFormuKumlamaDevamIslemlerForm() {
		return isTakipFormuKumlamaDevamIslemlerForm;
	}

	public void setIsTakipFormuKumlamaDevamIslemlerForm(
			IsTakipFormuKumlamaDevamIslemler isTakipFormuKumlamaDevamIslemlerForm) {
		this.isTakipFormuKumlamaDevamIslemlerForm = isTakipFormuKumlamaDevamIslemlerForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	public String getBarkodNo() {
		return barkodNo;
	}

	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	public IsTakipFormuKumlama getSelectedIsTakipFormuKumlama() {
		return selectedIsTakipFormuKumlama;
	}

	public void setSelectedIsTakipFormuKumlama(
			IsTakipFormuKumlama selectedIsTakipFormuKumlama) {
		this.selectedIsTakipFormuKumlama = selectedIsTakipFormuKumlama;
	}

	public SalesItem getSelectedSalesItem() {
		return selectedSalesItem;
	}

	public void setSelectedSalesItem(SalesItem selectedSalesItem) {
		this.selectedSalesItem = selectedSalesItem;
	}

	public boolean isButtonRender() {
		return buttonRender;
	}

	public void setButtonRender(boolean buttonRender) {
		this.buttonRender = buttonRender;
	}

}
