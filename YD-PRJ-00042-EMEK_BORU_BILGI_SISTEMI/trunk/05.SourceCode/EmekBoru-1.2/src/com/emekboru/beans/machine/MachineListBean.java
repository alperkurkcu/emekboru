package com.emekboru.beans.machine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Machine;
import com.emekboru.jpa.MachinePart;
import com.emekboru.jpa.MachinePartCategory;
import com.emekboru.jpaman.MachineManager;
import com.emekboru.jpaman.MachinePartCategoryManager;
import com.emekboru.jpaman.MachinePartManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "machineListBean")
@SessionScoped
public class MachineListBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	private List<Machine> allMachineList;
	private List<MachinePart> allMachinePartList;
	private List<MachinePartCategory> allMachinePartCategoryList;
	private List<Machine> spiralMachineList;
	private List<Machine> productionMachineList;
	private List<Machine> coatMachineList;

	public MachineListBean() {

		allMachineList = new ArrayList<Machine>(0);
		allMachinePartList = new ArrayList<MachinePart>(0);
		allMachinePartCategoryList = new ArrayList<MachinePartCategory>(0);
		spiralMachineList = new ArrayList<Machine>(0);
		productionMachineList = new ArrayList<Machine>(0);
		coatMachineList = new ArrayList<Machine>(0);

		// deneme
		// manreport için
		loadSpiralMachines();
		// deneme
	}

	// ********************************************************************* //
	// ********* METHODS RESPONSIBLE FOR LOADING MACHINE LISTS ************* //
	// ********************************************************************* //

	public void loadAllMachines() {

		MachineManager machineManager = new MachineManager();
		allMachineList = machineManager.findAllOrdered(Machine.class, "name");
	}

	public void loadAllMachineParts() {

		MachinePartManager partManager = new MachinePartManager();
		allMachinePartList = partManager.findAll(MachinePart.class);
	}

	public void loadAllMachinePartCategories() {

		MachinePartCategoryManager categoryManager = new MachinePartCategoryManager();
		allMachinePartCategoryList = categoryManager
				.findAll(MachinePartCategory.class);
	}

	// find the MACHINES that are related with the production
	// process(except SPIRAL machines)

	// public void loadSpiralMachines() {
	//
	// MachineManager man = new MachineManager();
	// spiralMachineList = man.findSpiralMachines();
	// }

	public void loadSpiralMachines() {

		MachineManager man = new MachineManager();
		spiralMachineList = man.findByFieldOrderBy(Machine.class,
				"machineType.isSpiral", true, "name");
	}

	// public void loadManufacturingMachines() {
	//
	// productionMachineList.clear();
	// MachineManager man = new MachineManager();
	// List<Machine> machines = man.findManufacturingMachines();
	// for (Machine m : machines) {
	// if (!m.getMachineType().isSpiral())
	// productionMachineList.add(m);
	// }
	// }

	public void loadManufacturingMachines() {

		productionMachineList.clear();
		MachineManager man = new MachineManager();
		List<Machine> machines = man.findByFieldOrderBy(Machine.class,
				"machineType.isManufacturing", true, "name");
		for (Machine m : machines) {
			if (!m.getMachineType().isSpiral())
				productionMachineList.add(m);
		}
	}

	// public void loadCoatMachines() {
	//
	// MachineManager man = new MachineManager();
	// coatMachineList = man.findCoatingMachines();
	// }

	public void loadCoatMachines() {

		MachineManager man = new MachineManager();
		coatMachineList = man.findByFieldOrderBy(Machine.class,
				"machineType.isCoating", true, "name");
	}

	// ********************************************************************* //
	// **************** METHODS FOR REDIRECTING TO PAGES ******************* //
	// ********************************************************************* //

	public void goToNewMachinePage() {

		loadAllMachines();
		FacesContextUtils.redirect(config.getPageUrl().NEW_MACHINE_PAGE);
	}

	public void goToNewMachinePartPage() {

		loadAllMachineParts();
		loadAllMachines();
		loadAllMachinePartCategories();
		FacesContextUtils.redirect(config.getPageUrl().NEW_MACHINE_PART_PAGE);
	}

	public void goToSpiralMachinePage() {

		loadSpiralMachines();
		FacesContextUtils.redirect(config.getPageUrl().SPIRAL_MACHINE_PAGE);
	}

	public void goToProductionMachinePage() {

		loadManufacturingMachines();
		FacesContextUtils.redirect(config.getPageUrl().PRODUCTION_MACHINE_PAGE);
	}

	public void goToCoatMachinePage() {

		loadCoatMachines();
		FacesContextUtils.redirect(config.getPageUrl().COAT_MACHINE_PAGE);
	}

	public void goToProductionReport() {

		loadSpiralMachines();
		FacesContextUtils.redirect(config.getPageUrl().PIPE_PRODUCTION_REPORT);
	}

	public void goToMachineMaintencePage() {

		loadAllMachines();
		FacesContextUtils
				.redirect(config.getPageUrl().MACHINE_MAINTENANCE_PAGE);
	}

	public void goToMachineKaplamaArizalar() {

		loadAllMachines();
		FacesContextUtils
				.redirect(config.getPageUrl().MACHINE_KAPLAMA_ARIZA_PAGE);
	}

	public void goToMachineUretimArizalar() {

		loadAllMachines();
		FacesContextUtils
				.redirect(config.getPageUrl().MACHINE_URETIM_ARIZA_PAGE);
	}
	
	public void goToPlazmaSarfMalzemeleriKullanim() {

		FacesContextUtils
				.redirect(config.getPageUrl().PLAZMA_SARF_MALZEMELERI_KULLANIM_PAGE);
	}

	// ********************************************************************* //
	// *********************** GETTERS AND SETTERS ************************* //
	// ********************************************************************* //
	public List<Machine> getSpiralMachineList() {
		return spiralMachineList;
	}

	public List<Machine> getCoatMachineList() {
		return coatMachineList;
	}

	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	public List<Machine> getAllMachineList() {
		return allMachineList;
	}

	public void setAllMachineList(List<Machine> allMachineList) {
		this.allMachineList = allMachineList;
	}

	public List<MachinePart> getAllMachinePartList() {
		return allMachinePartList;
	}

	public void setAllMachinePartList(List<MachinePart> allMachinePartList) {
		this.allMachinePartList = allMachinePartList;
	}

	public List<MachinePartCategory> getAllMachinePartCategoryList() {
		return allMachinePartCategoryList;
	}

	public void setAllMachinePartCategoryList(
			List<MachinePartCategory> allMachinePartCategoryList) {
		this.allMachinePartCategoryList = allMachinePartCategoryList;
	}

	public List<Machine> getProductionMachineList() {
		return productionMachineList;
	}

	public void setProductionMachineList(List<Machine> productionMachineList) {
		this.productionMachineList = productionMachineList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setSpiralMachineList(List<Machine> spiralMachineList) {
		this.spiralMachineList = spiralMachineList;
	}

	public void setCoatMachineList(List<Machine> coatMachineList) {
		this.coatMachineList = coatMachineList;
	}

}
