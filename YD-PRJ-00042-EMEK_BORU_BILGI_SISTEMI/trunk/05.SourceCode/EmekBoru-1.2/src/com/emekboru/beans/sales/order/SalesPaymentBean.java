package com.emekboru.beans.sales.order;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.sales.order.SalesPayment;
import com.emekboru.jpaman.sales.order.SalesPaymentManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "salesPaymentBean")
@ViewScoped
public class SalesPaymentBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5312644659146241632L;

	@ManagedProperty(value = "#{salesOrderBean}")
	private SalesOrderBean orderBean;

	private SalesPayment newPayment = new SalesPayment();
	private SalesPayment selectedPayment;

	private List<SalesPayment> paymentList;
	private List<SalesPayment> paymentsOfSelectedOrder;

	public SalesPaymentBean() {
		newPayment = new SalesPayment();
		selectedPayment = new SalesPayment();
	}

	@PostConstruct
	public void load() {
		System.out.println("SalesPaymentBean.load()");

		try {
			SalesPaymentManager manager = new SalesPaymentManager();
			paymentList = manager.findAll(SalesPayment.class);

			selectedPayment = paymentList.get(0);
		} catch (Exception e) {
			System.out.println("SalesPaymentBean:"+e.toString());
		}
	}

	public void add() {
		System.out.println("SalesPaymentBean.add()");

		try {
			SalesPaymentManager manager = new SalesPaymentManager();
			newPayment.setOrder(orderBean.getSelectedOrder());
			paymentList.add(newPayment);
			manager.enterNew(newPayment);

			newPayment = new SalesPayment();
			FacesContextUtils.addInfoMessage("SubmitMessage");
		} catch (Exception e) {
			System.out.println("SalesPaymentBean:"+e.toString());
		}
	}

	public void update() {
		System.out.println("SalesPaymentBean.update()");

		try {
			SalesPaymentManager manager = new SalesPaymentManager();
			manager.updateEntity(selectedPayment);

			FacesContextUtils.addInfoMessage("UpdateItemMessage");
		} catch (Exception e) {
			System.out.println("SalesPaymentBean:"+e.toString());
		}
	}

	public void delete() {
		System.out.println("SalesPaymentBean.delete()");

		try {
			SalesPaymentManager manager = new SalesPaymentManager();
			manager.delete(selectedPayment);

			paymentList.remove(selectedPayment);
			selectedPayment = paymentList.get(0);
			FacesContextUtils.addWarnMessage("DeletedMessage");
		} catch (Exception e) {
			System.out.println("SalesPaymentBean:"+e.toString());
		}
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public SalesPayment getNewPayment() {
		return newPayment;
	}

	public void setNewPayment(SalesPayment newPayment) {
		this.newPayment = newPayment;
	}

	public SalesPayment getSelectedPayment() {
		try {
			if (selectedPayment == null)
				selectedPayment = new SalesPayment();
		} catch (Exception ex) {
			selectedPayment = new SalesPayment();
		}
		return selectedPayment;
	}

	public void setSelectedPayment(SalesPayment selectedPayment) {
		this.selectedPayment = selectedPayment;
	}

	public List<SalesPayment> getPaymentList() {
		return paymentList;
	}

	public void setPaymentList(List<SalesPayment> paymentList) {
		this.paymentList = paymentList;
	}

	public SalesOrderBean getOrderBean() {
		return orderBean;
	}

	public void setOrderBean(SalesOrderBean orderBean) {
		this.orderBean = orderBean;
	}

	public List<SalesPayment> getPaymentsOfSelectedOrder() {

		paymentsOfSelectedOrder = new ArrayList<SalesPayment>();

		try {
			for (int i = 0; i < paymentList.size(); i++) {

				if (paymentList.get(i).getOrder()
						.equals(orderBean.getSelectedOrder())) {
					paymentsOfSelectedOrder.add(paymentList.get(i));
				}
			}
		} catch (Exception e) {
			System.out.println("SalesPaymentBean:"+e.toString());
		}

		return paymentsOfSelectedOrder;
	}

	public void setPaymentsOfSelectedOrder(
			List<SalesPayment> paymentsOfSelectedOrder) {
		this.paymentsOfSelectedOrder = paymentsOfSelectedOrder;
	}
}
