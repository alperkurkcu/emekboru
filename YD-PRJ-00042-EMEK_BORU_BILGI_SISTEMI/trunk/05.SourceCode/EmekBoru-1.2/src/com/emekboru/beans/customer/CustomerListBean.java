package com.emekboru.beans.customer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.sales.customer.SalesCustomer;
import com.emekboru.jpaman.sales.customer.SalesCustomerManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "customerListBean")
@ViewScoped
public class CustomerListBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	private List<SalesCustomer> salesCustomers;

	public CustomerListBean() {

	}

	@PostConstruct
	public void loadCustomers() {

		salesCustomers = new ArrayList<SalesCustomer>();
		SalesCustomerManager customerManager = new SalesCustomerManager();
		salesCustomers = customerManager.findAllOrderByASC(SalesCustomer.class,
				"commercialName, c.shortName");
	}

	public void goToNewCustomerPage() {

		FacesContextUtils.redirect(config.getPageUrl().NEW_CUSTOMER_PAGE);
	}

	public void goToManageCustomerPage() {

		FacesContextUtils.redirect(config.getPageUrl().MANAGE_CUSTOMER_PAGE);
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<SalesCustomer> getSalesCustomers() {
		return salesCustomers;
	}

	public void setSalesCustomers(List<SalesCustomer> salesCustomers) {
		this.salesCustomers = salesCustomers;
	}

}
