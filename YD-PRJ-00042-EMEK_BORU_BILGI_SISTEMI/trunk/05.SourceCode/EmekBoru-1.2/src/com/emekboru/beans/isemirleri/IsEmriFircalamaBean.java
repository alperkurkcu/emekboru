/**
 * 
 */
package com.emekboru.beans.isemirleri;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.isemri.IsEmriFircalama;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.isemirleri.IsEmriFircalamaManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.sales.order.SalesItemManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isEmriFircalamaBean")
@ViewScoped
public class IsEmriFircalamaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private IsEmriFircalama isEmriFircalamaForm = new IsEmriFircalama();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	// @ManagedProperty(value = "#{isTakipFormuKumlamaBean}")
	// private IsTakipFormuKumlamaBean isTakipFormuKumlamaBean;

	// private List<CoatRawMaterial> allIcKumlamaMalzemeList = new
	// ArrayList<CoatRawMaterial>();

	// // kaplama kalite
	// private List<IsolationTestDefinition> internalIsolationTestDefinitions =
	// new ArrayList<IsolationTestDefinition>();
	// private String kumlamaKalitesi = null;

	public IsEmriFircalamaBean() {

	}

	public void addOrUpdateIsEmriFircalama(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmItemId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		SalesItem salesItem = new SalesItemManager().loadObject(
				SalesItem.class, prmItemId);

		IsEmriFircalamaManager isEmriManager = new IsEmriFircalamaManager();

		if (isEmriFircalamaForm.getId() == null) {

			isEmriFircalamaForm.setSalesItem(salesItem);
			isEmriFircalamaForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			isEmriFircalamaForm.setEkleyenEmployee(userBean.getUser());

			isEmriManager.enterNew(isEmriFircalamaForm);

			// // iç kumlama iş takip formu ekleme
			// isTakipFormuKumlamaBean.addFromIsEmri(1, salesItem);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"FIRÇALAMA İŞ EMRİ BAŞARIYLA EKLENMİŞTİR!"));
		} else {

			isEmriFircalamaForm.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			isEmriFircalamaForm.setGuncelleyenEmployee(userBean.getUser());
			isEmriManager.updateEntity(isEmriFircalamaForm);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_WARN,
					"FIRÇALAMA İŞ EMRİ BAŞARIYLA GÜNCELLENMİŞTİR!", null));
		}
	}

	public void deleteIsEmriFircalama() {

		IsEmriFircalamaManager isEmriManager = new IsEmriFircalamaManager();

		if (isEmriFircalamaForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		isEmriManager.delete(isEmriFircalamaForm);
		isEmriFircalamaForm = new IsEmriFircalama();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
				"FIRÇALAMA İŞ EMRİ BAŞARIYLA SİLİNMİŞTİR!", null));
	}

	@SuppressWarnings("static-access")
	public void loadIsEmri(Integer itemId) {

		IsolationTestDefinitionManager kaliteMan = new IsolationTestDefinitionManager();
		IsEmriFircalamaManager isEmriManager = new IsEmriFircalamaManager();

		if (isEmriManager.getAllIsEmriFircalama(itemId).size() > 0) {
			isEmriFircalamaForm = new IsEmriFircalamaManager()
					.getAllIsEmriFircalama(itemId).get(0);
		} else {
			isEmriFircalamaForm = new IsEmriFircalama();
		}

		// internalIsolationTestDefinitions = kaliteMan
		// .findIsolationByItemIdAndIsolationType(1,
		// isEmriFircalamaForm.getSalesItem());
		// for (int i = 0; i < internalIsolationTestDefinitions.size(); i++) {
		// if (internalIsolationTestDefinitions.get(i).getGlobalId() == 2123) {
		// kumlamaKalitesi = internalIsolationTestDefinitions.get(i)
		// .getAciklama();
		// }
		// if (internalIsolationTestDefinitions.get(i).getGlobalId() == 2124) {
		// kumlamaKalitesi = kumlamaKalitesi + " "
		// + internalIsolationTestDefinitions.get(i).getAciklama();
		// }
		// }
	}
	// setters getters

	/**
	 * @return the isEmriFircalamaForm
	 */
	public IsEmriFircalama getIsEmriFircalamaForm() {
		return isEmriFircalamaForm;
	}

	/**
	 * @param isEmriFircalamaForm the isEmriFircalamaForm to set
	 */
	public void setIsEmriFircalamaForm(IsEmriFircalama isEmriFircalamaForm) {
		this.isEmriFircalamaForm = isEmriFircalamaForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
