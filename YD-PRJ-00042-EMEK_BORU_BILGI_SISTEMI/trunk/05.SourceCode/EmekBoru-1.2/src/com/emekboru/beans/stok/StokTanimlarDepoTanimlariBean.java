/**
 * 
 */
package com.emekboru.beans.stok;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.stok.StokTanimlarDepoTanimlari;
import com.emekboru.jpaman.stok.StokTanimlarDepoTanimlariManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "stokTanimlarDepoTanimlariBean")
@ViewScoped
public class StokTanimlarDepoTanimlariBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<StokTanimlarDepoTanimlari> allStokTanimlarDepoTanimlariList = new ArrayList<StokTanimlarDepoTanimlari>();

	private StokTanimlarDepoTanimlari stokTanimlarDepoTanimlariForm = new StokTanimlarDepoTanimlari();

	StokTanimlarDepoTanimlariManager formManager = new StokTanimlarDepoTanimlariManager();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	public StokTanimlarDepoTanimlariBean() {

		fillTestList();
	}

	public void addForm() {

		stokTanimlarDepoTanimlariForm = new StokTanimlarDepoTanimlari();
		updateButtonRender = false;
	}

	public void addOrUpdateForm() {

		if (stokTanimlarDepoTanimlariForm.getId() == null) {

			stokTanimlarDepoTanimlariForm.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			stokTanimlarDepoTanimlariForm.setEkleyenKullanici(userBean
					.getUser().getId());

			formManager.enterNew(stokTanimlarDepoTanimlariForm);

			this.stokTanimlarDepoTanimlariForm = new StokTanimlarDepoTanimlari();
			FacesContextUtils.addInfoMessage("FormSubmitMessage");

		} else {

			stokTanimlarDepoTanimlariForm.setGuncellemeZamani(UtilInsCore
					.getTarihZaman());
			stokTanimlarDepoTanimlariForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			formManager.updateEntity(stokTanimlarDepoTanimlariForm);

			FacesContextUtils.addInfoMessage("FormUpdateMessage");
		}

		fillTestList();
	}

	public void deleteForm() {

		if (stokTanimlarDepoTanimlariForm.getId() == null) {

			FacesContextUtils.addWarnMessage("NoSelectMessage");
			return;
		}
		formManager.delete(stokTanimlarDepoTanimlariForm);
		fillTestList();
		FacesContextUtils.addWarnMessage("TestDeleteMessage");
	}

	public void fillTestList() {
		StokTanimlarDepoTanimlariManager manager = new StokTanimlarDepoTanimlariManager();
		allStokTanimlarDepoTanimlariList = manager
				.getAllStokTanimlarDepoTanimlari();
	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	// setters getters

	public List<StokTanimlarDepoTanimlari> getAllStokTanimlarDepoTanimlariList() {
		return allStokTanimlarDepoTanimlariList;
	}

	public void setAllStokTanimlarDepoTanimlariList(
			List<StokTanimlarDepoTanimlari> allStokTanimlarDepoTanimlariList) {
		this.allStokTanimlarDepoTanimlariList = allStokTanimlarDepoTanimlariList;
	}

	public StokTanimlarDepoTanimlari getStokTanimlarDepoTanimlariForm() {
		return stokTanimlarDepoTanimlariForm;
	}

	public void setStokTanimlarDepoTanimlariForm(
			StokTanimlarDepoTanimlari stokTanimlarDepoTanimlariForm) {
		this.stokTanimlarDepoTanimlariForm = stokTanimlarDepoTanimlariForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}
}
