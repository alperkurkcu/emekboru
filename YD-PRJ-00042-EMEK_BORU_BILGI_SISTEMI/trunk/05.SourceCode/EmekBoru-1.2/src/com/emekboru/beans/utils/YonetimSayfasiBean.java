/**
 * 
 */
package com.emekboru.beans.utils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.component.rowexpansion.RowExpansion;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.ToggleEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Machine;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizGozOlcuSonuc;
import com.emekboru.jpaman.MachineManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.sales.order.SalesItemManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizGozOlcuSonucManager;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "yonetimSayfasiBean")
@ViewScoped
public class YonetimSayfasiBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private List<Pipe> tab1ProducedPipes;
	private List<Pipe> tab2ProducedPipes;
	private List<Pipe> tab2Sp2ProducedPipes;
	private List<Pipe> tab2Sp3ProducedPipes;
	private List<Pipe> tab2Sp5ProducedPipes;

	private Pipe pipe;
	private Machine machine;

	Integer prmMachineId = 53;

	private List<SalesItem> itemList;
	private List<Pipe> pipeList;
	private List<TestTahribatsizGozOlcuSonuc> gozOlcuList;

	Long yapilanMetraj = 0L;
	Long kabulMetraj = 0L;
	Long kalanMetraj = 0L;

	public YonetimSayfasiBean() {
		tab1ProducedPipes = new ArrayList<Pipe>();
		tab1ProducedPipes = new ArrayList<Pipe>();
		tab2Sp2ProducedPipes = new ArrayList<Pipe>();
		tab2Sp3ProducedPipes = new ArrayList<Pipe>();
		tab2Sp5ProducedPipes = new ArrayList<Pipe>();
	}

	@PostConstruct
	private void load() {

		MachineManager machineMan = new MachineManager();
		machine = machineMan.loadObject(Machine.class, prmMachineId);
		producedPipes();
		lastProducedPipe();

		SalesItemManager manager = new SalesItemManager();

		itemList = manager.loadSalesItemsOrdered();
	}

	public void onTabChange1(TabChangeEvent event) {

		if (event.getTab().getId().equals("sp2Tab1Id")) {
			prmMachineId = 53;
		} else if ((event.getTab().getId().equals("sp3Tab1Id"))) {
			prmMachineId = 54;
		} else if ((event.getTab().getId().equals("sp5Tab1Id"))) {
			prmMachineId = 55;
		}

		MachineManager machineMan = new MachineManager();

		machine = machineMan.loadObject(Machine.class, prmMachineId);
		producedPipes();
	}

	public void onTabChange2(TabChangeEvent event) {
		MachineManager machineMan = new MachineManager();
		if (event.getTab().getId().equals("sp2Tab2Id")) {
			prmMachineId = 53;
			machine = machineMan.loadObject(Machine.class, prmMachineId);
			lastProducedPipe();
		} else if ((event.getTab().getId().equals("sp3Tab2Id"))) {
			prmMachineId = 54;
			machine = machineMan.loadObject(Machine.class, prmMachineId);
			lastProducedPipe();
		} else if ((event.getTab().getId().equals("sp5Tab2Id"))) {
			prmMachineId = 55;
			machine = machineMan.loadObject(Machine.class, prmMachineId);
			lastProducedPipe();
		}

	}

	public void onRowExpansion(RowExpansion event) {

	}

	public void onRowToggle(ToggleEvent event) {
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Row State " + event.getVisibility(), "Model:"
						+ ((SalesItem) event.getSource()).getItemId());

		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	@SuppressWarnings("static-access")
	public void itemDurum(SalesItem prmItem) {

		yapilanMetraj = 0L;
		kabulMetraj = 0L;
		kalanMetraj = 0L;

		PipeManager pipeMan = new PipeManager();
		TestTahribatsizGozOlcuSonucManager gozOlcuMan = new TestTahribatsizGozOlcuSonucManager();
		pipeList = pipeMan.findByItemId(prmItem.getItemId());
		gozOlcuList = gozOlcuMan.findByItemIdSonucNoReleaseDate(prmItem
				.getItemId());

		for (int i = 0; i < pipeList.size(); i++) {
			yapilanMetraj += (long) pipeList.get(i).getBoy();
		}

		for (int i = 0; i < gozOlcuList.size(); i++) {
			kabulMetraj += gozOlcuList.get(i).getBoruUzunluk().longValue();
		}

		yapilanMetraj = yapilanMetraj / 1000;
		kabulMetraj = kabulMetraj / 1000;
		kalanMetraj = kalanMetraj / 1000;
		kalanMetraj = (long) prmItem.getTotalLengthM() - kabulMetraj;

	}

	public void producedPipes() {
		// load the pipes that were produced so far for the selected order
		PipeManager pipeMan = new PipeManager();

		tab1ProducedPipes = pipeMan.allPipesInMachine(machine, 2);
		return;
	}

	public void lastProducedPipe() {
		// load the pipes that were produced so far for the selected order
		PipeManager pipeMan = new PipeManager();

		// tab2ProducedPipes = pipeMan.lastPipeInMachine(machine);
		tab2ProducedPipes = pipeMan.allPipesInMachine(machine, 1);

		if (tab2ProducedPipes.size() > 0 && prmMachineId.equals(53)) {
			int y = 0;
			tab2Sp2ProducedPipes.add(tab2ProducedPipes.get(0));
			for (int i = 0; i < tab2Sp2ProducedPipes.size(); i++) {
				if (tab2Sp2ProducedPipes.get(y).getSalesItem()
						.equals(tab2ProducedPipes.get(i).getSalesItem())) {

				} else {
					tab2Sp2ProducedPipes.add(tab2ProducedPipes.get(i));
					y++;
				}
			}
		}
		if (tab2ProducedPipes.size() > 0 && prmMachineId.equals(54)) {
			int y = 0;
			tab2Sp3ProducedPipes.add(tab2ProducedPipes.get(0));
			for (int i = 0; i < tab2ProducedPipes.size(); i++) {
				if (tab2Sp3ProducedPipes.get(y).getSalesItem()
						.equals(tab2ProducedPipes.get(i).getSalesItem())) {

				} else {
					tab2Sp3ProducedPipes.add(tab2ProducedPipes.get(i));
					y++;
				}
			}
		}
		if (tab2ProducedPipes.size() > 0 && prmMachineId.equals(55)) {
			int y = 0;
			tab2Sp5ProducedPipes.add(tab2ProducedPipes.get(0));
			for (int i = 0; i < tab2ProducedPipes.size(); i++) {
				if (tab2Sp5ProducedPipes.get(y).getSalesItem()
						.equals(tab2ProducedPipes.get(i).getSalesItem())) {

				} else {
					tab2Sp5ProducedPipes.add(tab2ProducedPipes.get(i));
					y++;
				}
			}
		}
		return;
	}

	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	// setters getters

	/**
	 * @return the config
	 */
	public ConfigBean getConfig() {
		return config;
	}

	/**
	 * @param config
	 *            the config to set
	 */
	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the machine
	 */
	public Machine getMachine() {
		return machine;
	}

	/**
	 * @param machine
	 *            the machine to set
	 */
	public void setMachine(Machine machine) {
		this.machine = machine;
	}

	/**
	 * @return the itemList
	 */
	public List<SalesItem> getItemList() {
		return itemList;
	}

	/**
	 * @param itemList
	 *            the itemList to set
	 */
	public void setItemList(List<SalesItem> itemList) {
		this.itemList = itemList;
	}

	/**
	 * @return the pipeList
	 */
	public List<Pipe> getPipeList() {
		return pipeList;
	}

	/**
	 * @param pipeList
	 *            the pipeList to set
	 */
	public void setPipeList(List<Pipe> pipeList) {
		this.pipeList = pipeList;
	}

	/**
	 * @return the yapilanMetraj
	 */
	public Long getYapilanMetraj() {
		return yapilanMetraj;
	}

	/**
	 * @param yapilanMetraj
	 *            the yapilanMetraj to set
	 */
	public void setYapilanMetraj(Long yapilanMetraj) {
		this.yapilanMetraj = yapilanMetraj;
	}

	/**
	 * @return the gozOlcuList
	 */
	public List<TestTahribatsizGozOlcuSonuc> getGozOlcuList() {
		return gozOlcuList;
	}

	/**
	 * @param gozOlcuList
	 *            the gozOlcuList to set
	 */
	public void setGozOlcuList(List<TestTahribatsizGozOlcuSonuc> gozOlcuList) {
		this.gozOlcuList = gozOlcuList;
	}

	/**
	 * @return the kalanMetraj
	 */
	public Long getKalanMetraj() {
		return kalanMetraj;
	}

	/**
	 * @param kalanMetraj
	 *            the kalanMetraj to set
	 */
	public void setKalanMetraj(Long kalanMetraj) {
		this.kalanMetraj = kalanMetraj;
	}

	/**
	 * @return the kabulMetraj
	 */
	public Long getKabulMetraj() {
		return kabulMetraj;
	}

	/**
	 * @param kabulMetraj
	 *            the kabulMetraj to set
	 */
	public void setKabulMetraj(Long kabulMetraj) {
		this.kabulMetraj = kabulMetraj;
	}

	/**
	 * @return the tab1ProducedPipes
	 */
	public List<Pipe> getTab1ProducedPipes() {
		return tab1ProducedPipes;
	}

	/**
	 * @param tab1ProducedPipes
	 *            the tab1ProducedPipes to set
	 */
	public void setTab1ProducedPipes(List<Pipe> tab1ProducedPipes) {
		this.tab1ProducedPipes = tab1ProducedPipes;
	}

	/**
	 * @return the tab2ProducedPipes
	 */
	public List<Pipe> getTab2ProducedPipes() {
		return tab2ProducedPipes;
	}

	/**
	 * @param tab2ProducedPipes
	 *            the tab2ProducedPipes to set
	 */
	public void setTab2ProducedPipes(List<Pipe> tab2ProducedPipes) {
		this.tab2ProducedPipes = tab2ProducedPipes;
	}

	/**
	 * @return the tab2Sp2ProducedPipes
	 */
	public List<Pipe> getTab2Sp2ProducedPipes() {
		return tab2Sp2ProducedPipes;
	}

	/**
	 * @param tab2Sp2ProducedPipes
	 *            the tab2Sp2ProducedPipes to set
	 */
	public void setTab2Sp2ProducedPipes(List<Pipe> tab2Sp2ProducedPipes) {
		this.tab2Sp2ProducedPipes = tab2Sp2ProducedPipes;
	}

	/**
	 * @return the tab2Sp3ProducedPipes
	 */
	public List<Pipe> getTab2Sp3ProducedPipes() {
		return tab2Sp3ProducedPipes;
	}

	/**
	 * @param tab2Sp3ProducedPipes
	 *            the tab2Sp3ProducedPipes to set
	 */
	public void setTab2Sp3ProducedPipes(List<Pipe> tab2Sp3ProducedPipes) {
		this.tab2Sp3ProducedPipes = tab2Sp3ProducedPipes;
	}

	/**
	 * @return the tab2Sp5ProducedPipes
	 */
	public List<Pipe> getTab2Sp5ProducedPipes() {
		return tab2Sp5ProducedPipes;
	}

	/**
	 * @param tab2Sp5ProducedPipes
	 *            the tab2Sp5ProducedPipes to set
	 */
	public void setTab2Sp5ProducedPipes(List<Pipe> tab2Sp5ProducedPipes) {
		this.tab2Sp5ProducedPipes = tab2Sp5ProducedPipes;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

}
