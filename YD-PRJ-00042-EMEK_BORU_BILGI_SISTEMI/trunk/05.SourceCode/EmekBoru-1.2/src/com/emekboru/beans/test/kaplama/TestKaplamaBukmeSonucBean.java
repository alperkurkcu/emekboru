package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaBukmeSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaBukmeSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaplamaBukmeSonucBean")
@ViewScoped
public class TestKaplamaBukmeSonucBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private TestKaplamaBukmeSonuc testKaplamaBukmeSonucForm = new TestKaplamaBukmeSonuc();
	private List<TestKaplamaBukmeSonuc> allKaplamaBukmeSonucList = new ArrayList<TestKaplamaBukmeSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateKaplamaBukmeTestSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaBukmeSonucManager tkbsManager = new TestKaplamaBukmeSonucManager();

		if (testKaplamaBukmeSonucForm.getId() == null) {

			testKaplamaBukmeSonucForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			testKaplamaBukmeSonucForm.setEkleyenKullanici(userBean.getUser()
					.getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaBukmeSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaBukmeSonucForm.setBagliTestId(bagliTest);
			testKaplamaBukmeSonucForm.setBagliGlobalId(bagliTest);

			tkbsManager.enterNew(testKaplamaBukmeSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaBukmeSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaBukmeSonucForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			tkbsManager.updateEntity(testKaplamaBukmeSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addKaplamaBukmeTest() {

		testKaplamaBukmeSonucForm = new TestKaplamaBukmeSonuc();
		updateButtonRender = false;
	}

	public void kaplamaBukmeListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allKaplamaBukmeSonucList = TestKaplamaBukmeSonucManager
				.getAllKaplamaBukmeSonucTest(globalId, pipeId);
		testKaplamaBukmeSonucForm = new TestKaplamaBukmeSonuc();
	}

	public void deleteTestKaplamaBukmeSonuc() {
		bagliTestId = testKaplamaBukmeSonucForm.getBagliTestId().getId();
		if (testKaplamaBukmeSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaBukmeSonucManager tkbsManager = new TestKaplamaBukmeSonucManager();

		tkbsManager.delete(testKaplamaBukmeSonucForm);
		testKaplamaBukmeSonucForm = new TestKaplamaBukmeSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setters getters
	public TestKaplamaBukmeSonuc getTestKaplamaBukmeSonucForm() {
		return testKaplamaBukmeSonucForm;
	}

	public void setTestKaplamaBukmeSonucForm(
			TestKaplamaBukmeSonuc testKaplamaBukmeSonucForm) {
		this.testKaplamaBukmeSonucForm = testKaplamaBukmeSonucForm;
	}

	public List<TestKaplamaBukmeSonuc> getAllKaplamaBukmeSonucList() {
		return allKaplamaBukmeSonucList;
	}

	public void setAllKaplamaBukmeSonucList(
			List<TestKaplamaBukmeSonuc> allKaplamaBukmeSonucList) {
		this.allKaplamaBukmeSonucList = allKaplamaBukmeSonucList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
