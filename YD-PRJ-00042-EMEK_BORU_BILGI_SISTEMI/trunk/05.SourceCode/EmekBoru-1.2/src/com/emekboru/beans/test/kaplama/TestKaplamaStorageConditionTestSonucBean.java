/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaStorageConditionTestSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaStorageConditionTestSonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testKaplamaStorageConditionTestSonucBean")
@ViewScoped
public class TestKaplamaStorageConditionTestSonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private TestKaplamaStorageConditionTestSonuc testKaplamaStorageConditionTestSonucForm = new TestKaplamaStorageConditionTestSonuc();
	private List<TestKaplamaStorageConditionTestSonuc> allTestKaplamaStorageConditionTestSonucList = new ArrayList<TestKaplamaStorageConditionTestSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateTestKaplamaStorageConditionTestSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaStorageConditionTestSonucManager tkbebManager = new TestKaplamaStorageConditionTestSonucManager();

		if (testKaplamaStorageConditionTestSonucForm.getId() == null) {

			testKaplamaStorageConditionTestSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaStorageConditionTestSonucForm
					.setEkleyenKullanici(userBean.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaStorageConditionTestSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaStorageConditionTestSonucForm.setBagliTestId(bagliTest);
			testKaplamaStorageConditionTestSonucForm
					.setBagliGlobalId(bagliTest);

			tkbebManager.enterNew(testKaplamaStorageConditionTestSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			testKaplamaStorageConditionTestSonucForm = new TestKaplamaStorageConditionTestSonuc();
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaStorageConditionTestSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaStorageConditionTestSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			tkbebManager.updateEntity(testKaplamaStorageConditionTestSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addTestKaplamaStorageConditionTestSonuc() {

		testKaplamaStorageConditionTestSonucForm = new TestKaplamaStorageConditionTestSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaStorageConditionTestSonucListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaStorageConditionTestSonucList = TestKaplamaStorageConditionTestSonucManager
				.getAllStorageConditionTestSonuc(globalId, pipeId);
		testKaplamaStorageConditionTestSonucForm = new TestKaplamaStorageConditionTestSonuc();
	}

	public void deleteTestKaplamaStorageConditionTestSonuc() {

		bagliTestId = testKaplamaStorageConditionTestSonucForm
				.getBagliGlobalId().getId();

		if (testKaplamaStorageConditionTestSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		TestKaplamaStorageConditionTestSonucManager tkbebManager = new TestKaplamaStorageConditionTestSonucManager();

		tkbebManager.delete(testKaplamaStorageConditionTestSonucForm);
		testKaplamaStorageConditionTestSonucForm = new TestKaplamaStorageConditionTestSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters

	/**
	 * @return the testKaplamaStorageConditionTestSonucForm
	 */
	public TestKaplamaStorageConditionTestSonuc getTestKaplamaStorageConditionTestSonucForm() {
		return testKaplamaStorageConditionTestSonucForm;
	}

	/**
	 * @param testKaplamaStorageConditionTestSonucForm
	 *            the testKaplamaStorageConditionTestSonucForm to set
	 */
	public void setTestKaplamaStorageConditionTestSonucForm(
			TestKaplamaStorageConditionTestSonuc testKaplamaStorageConditionTestSonucForm) {
		this.testKaplamaStorageConditionTestSonucForm = testKaplamaStorageConditionTestSonucForm;
	}

	/**
	 * @return the allTestKaplamaStorageConditionTestSonucList
	 */
	public List<TestKaplamaStorageConditionTestSonuc> getAllTestKaplamaStorageConditionTestSonucList() {
		return allTestKaplamaStorageConditionTestSonucList;
	}

	/**
	 * @param allTestKaplamaStorageConditionTestSonucList
	 *            the allTestKaplamaStorageConditionTestSonucList to set
	 */
	public void setAllTestKaplamaStorageConditionTestSonucList(
			List<TestKaplamaStorageConditionTestSonuc> allTestKaplamaStorageConditionTestSonucList) {
		this.allTestKaplamaStorageConditionTestSonucList = allTestKaplamaStorageConditionTestSonucList;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
