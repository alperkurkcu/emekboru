/**
 * 
 */
package com.emekboru.beans.isemirleri;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.isemri.IsEmriPolietilenKaplama;
import com.emekboru.jpa.isemri.IsEmriPolietilenKaplamaYapistiriciSicaklik;
import com.emekboru.jpaman.isemirleri.IsEmriPolietilenKaplamaManager;
import com.emekboru.jpaman.isemirleri.IsEmriPolietilenKaplamaYapistiriciSicaklikManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isEmriPolietilenKaplamaYapistiriciSicaklikBean")
@ViewScoped
public class IsEmriPolietilenKaplamaYapistiriciSicaklikBean implements
		Serializable {

	private static final long serialVersionUID = 1L;

	private IsEmriPolietilenKaplamaYapistiriciSicaklik isEmriPolietilenKaplamaYapistiriciSicaklikForm = new IsEmriPolietilenKaplamaYapistiriciSicaklik();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	public IsEmriPolietilenKaplamaYapistiriciSicaklikBean() {

	}

	public void addOrUpdateIsEmriPolietilenKaplamaPolietilenSicaklik(
			ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmIsEmriId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");

		IsEmriPolietilenKaplama isEmriPolietilenKaplama = new IsEmriPolietilenKaplamaManager()
				.loadObject(IsEmriPolietilenKaplama.class, prmIsEmriId);

		IsEmriPolietilenKaplamaYapistiriciSicaklikManager isMan = new IsEmriPolietilenKaplamaYapistiriciSicaklikManager();
		IsEmriPolietilenKaplamaManager kaplamaMan = new IsEmriPolietilenKaplamaManager();

		if (isEmriPolietilenKaplamaYapistiriciSicaklikForm.getId() == null) {

			isEmriPolietilenKaplamaYapistiriciSicaklikForm
					.setIsEmriPolietilenKaplama(isEmriPolietilenKaplama);
			isEmriPolietilenKaplamaYapistiriciSicaklikForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			isEmriPolietilenKaplamaYapistiriciSicaklikForm
					.setEkleyenEmployee(userBean.getUser());
			isEmriPolietilenKaplamaYapistiriciSicaklikForm
					.setIsEmriPolietilenKaplama(isEmriPolietilenKaplama);

			isMan.enterNew(isEmriPolietilenKaplamaYapistiriciSicaklikForm);

			isEmriPolietilenKaplama
					.setIsEmriPolietilenKaplamaYapistiriciSicaklik(isEmriPolietilenKaplamaYapistiriciSicaklikForm);
			kaplamaMan.updateEntity(isEmriPolietilenKaplama);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							"YAPIŞTIRICI EXTRUDER BÖLGELERİ SICAKLIKLARI BAŞARIYLA EKLENMİŞTİR!"));
		} else {

			isEmriPolietilenKaplamaYapistiriciSicaklikForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			isEmriPolietilenKaplamaYapistiriciSicaklikForm
					.setGuncelleyenEmployee(userBean.getUser());
			isMan.updateEntity(isEmriPolietilenKaplamaYapistiriciSicaklikForm);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_WARN,
							"YAPIŞTIRICI EXTRUDER BÖLGELERİ SICAKLIKLARI BAŞARIYLA GÜNCELLENMİŞTİR!",
							null));
		}
	}

	public void deleteIsEmriPolietilenKaplamaPolietilenSicaklik() {

		IsEmriPolietilenKaplamaYapistiriciSicaklikManager isMan = new IsEmriPolietilenKaplamaYapistiriciSicaklikManager();

		if (isEmriPolietilenKaplamaYapistiriciSicaklikForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		isMan.delete(isEmriPolietilenKaplamaYapistiriciSicaklikForm);
		isEmriPolietilenKaplamaYapistiriciSicaklikForm = new IsEmriPolietilenKaplamaYapistiriciSicaklik();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"YAPIŞTIRICI EXTRUDER BÖLGELERİ SICAKLIKLARI BAŞARIYLA SİLİNMİŞTİR!",
						null));
	}

	@SuppressWarnings("static-access")
	public void loadPolietilenSicakliklar(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmIsEmriId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");

		IsEmriPolietilenKaplamaYapistiriciSicaklikManager paramMan = new IsEmriPolietilenKaplamaYapistiriciSicaklikManager();
		if (paramMan.getAllIsEmriPolietilenKaplamaYapistiriciSicaklik(
				prmIsEmriId).size() > 0) {
			isEmriPolietilenKaplamaYapistiriciSicaklikForm = paramMan
					.getAllIsEmriPolietilenKaplamaYapistiriciSicaklik(
							prmIsEmriId).get(0);
		}
	}

	// setters getters

	public IsEmriPolietilenKaplamaYapistiriciSicaklik getIsEmriPolietilenKaplamaYapistiriciSicaklikForm() {
		return isEmriPolietilenKaplamaYapistiriciSicaklikForm;
	}

	public void setIsEmriPolietilenKaplamaYapistiriciSicaklikForm(
			IsEmriPolietilenKaplamaYapistiriciSicaklik isEmriPolietilenKaplamaYapistiriciSicaklikForm) {
		this.isEmriPolietilenKaplamaYapistiriciSicaklikForm = isEmriPolietilenKaplamaYapistiriciSicaklikForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
