package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaKumlanmisBoruYuzeyTemizligiSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaplamaKumlanmisBoruYuzeyTemizligiSonucBean")
@ViewScoped
public class TestKaplamaKumlanmisBoruYuzeyTemizligiSonucBean implements
		Serializable {

	private static final long serialVersionUID = 1L;

	private TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc testKaplamaKumlanmisBoruYuzeyTemizligiSonucForm = new TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc();
	private List<TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc> allTestKaplamaKumlanmisBoruYuzeyTemizligiSonucList = new ArrayList<TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateTestKaplamaKumlanmisBoruYuzeyTemizligiSonuc(
			ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaKumlanmisBoruYuzeyTemizligiSonucManager manager = new TestKaplamaKumlanmisBoruYuzeyTemizligiSonucManager();

		if (testKaplamaKumlanmisBoruYuzeyTemizligiSonucForm.getId() == null) {

			testKaplamaKumlanmisBoruYuzeyTemizligiSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaKumlanmisBoruYuzeyTemizligiSonucForm
					.setEkleyenKullanici(userBean.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaKumlanmisBoruYuzeyTemizligiSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaKumlanmisBoruYuzeyTemizligiSonucForm
					.setBagliTestId(bagliTest);
			testKaplamaKumlanmisBoruYuzeyTemizligiSonucForm
					.setBagliGlobalId(bagliTest);

			manager.enterNew(testKaplamaKumlanmisBoruYuzeyTemizligiSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
			testKaplamaKumlanmisBoruYuzeyTemizligiSonucForm = new TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc();
		} else {

			testKaplamaKumlanmisBoruYuzeyTemizligiSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaKumlanmisBoruYuzeyTemizligiSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			manager.updateEntity(testKaplamaKumlanmisBoruYuzeyTemizligiSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
			testKaplamaKumlanmisBoruYuzeyTemizligiSonucForm = new TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc();
			updateButtonRender = false;
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addTestKaplamaKumlanmisBoruYuzeyTemizligiSonuc() {

		testKaplamaKumlanmisBoruYuzeyTemizligiSonucForm = new TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaKumlanmisBoruYuzeyTemizligiSonucListener(
			SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaKumlanmisBoruYuzeyTemizligiSonucList = TestKaplamaKumlanmisBoruYuzeyTemizligiSonucManager
				.getAllTestKaplamaKumlanmisBoruYuzeyTemizligiSonuc(globalId,
						pipeId);
		testKaplamaKumlanmisBoruYuzeyTemizligiSonucForm = new TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc();
	}

	public void deleteTestKaplamaKumlanmisBoruYuzeyTemizligiSonuc() {

		bagliTestId = testKaplamaKumlanmisBoruYuzeyTemizligiSonucForm
				.getBagliGlobalId().getId();

		if (testKaplamaKumlanmisBoruYuzeyTemizligiSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaKumlanmisBoruYuzeyTemizligiSonucManager manager = new TestKaplamaKumlanmisBoruYuzeyTemizligiSonucManager();
		manager.delete(testKaplamaKumlanmisBoruYuzeyTemizligiSonucForm);
		testKaplamaKumlanmisBoruYuzeyTemizligiSonucForm = new TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters
	public TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc getTestKaplamaKumlanmisBoruYuzeyTemizligiSonucForm() {
		return testKaplamaKumlanmisBoruYuzeyTemizligiSonucForm;
	}

	public void setTestKaplamaKumlanmisBoruYuzeyTemizligiSonucForm(
			TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc testKaplamaKumlanmisBoruYuzeyTemizligiSonucForm) {
		this.testKaplamaKumlanmisBoruYuzeyTemizligiSonucForm = testKaplamaKumlanmisBoruYuzeyTemizligiSonucForm;
	}

	public List<TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc> getAllTestKaplamaKumlanmisBoruYuzeyTemizligiSonucList() {
		return allTestKaplamaKumlanmisBoruYuzeyTemizligiSonucList;
	}

	public void setAllTestKaplamaKumlanmisBoruYuzeyTemizligiSonucList(
			List<TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc> allTestKaplamaKumlanmisBoruYuzeyTemizligiSonucList) {
		this.allTestKaplamaKumlanmisBoruYuzeyTemizligiSonucList = allTestKaplamaKumlanmisBoruYuzeyTemizligiSonucList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
