package com.emekboru.beans.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.emekboru.jpa.Currency;
import com.emekboru.jpaman.CurrencyManager;


@ManagedBean
@SessionScoped
public class Currencies implements Serializable{

	private static final long serialVersionUID = -1898465985729518325L;

	public List<Currency> 	currencies;
	public Currency 		defaultCurrency;
	
	public Currencies(){
		currencies 		= new ArrayList<Currency>();
		defaultCurrency = new Currency();
	}
	
	@PostConstruct
	public void load(){
		CurrencyManager manager = new CurrencyManager();
		currencies = manager.findAll(Currency.class);
		for (Currency currency : currencies) {
			if(currency.getIsDefault()){
				defaultCurrency = currency;
				break;
			}
		}//end of for loop...
		
		System.out.println("how many currencies : "+ currencies.size());
	}

	/**
	 * 		GETTERS AND SETTERS
	 */
	public List<Currency> getCurrencies() {
		return currencies;
	}

	public void setCurrencies(List<Currency> currencies) {
		this.currencies = currencies;
	}

	public Currency getDefaultCurrency() {
		return defaultCurrency;
	}

	public void setDefaultCurrency(Currency defaultCurrency) {
		this.defaultCurrency = defaultCurrency;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
