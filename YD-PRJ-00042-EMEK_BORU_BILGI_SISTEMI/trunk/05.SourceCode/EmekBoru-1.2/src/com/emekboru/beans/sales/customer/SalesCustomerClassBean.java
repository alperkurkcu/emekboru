package com.emekboru.beans.sales.customer;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.sales.customer.SalesCustomerClass;
import com.emekboru.jpaman.sales.customer.SalesCustomerClassManager;

@ManagedBean(name = "salesCustomerClassBean")
@ViewScoped
public class SalesCustomerClassBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7788333123115605770L;
	private List<SalesCustomerClass> classList;

	public SalesCustomerClassBean() {

		SalesCustomerClassManager manager = new SalesCustomerClassManager();
		classList = manager.findAll(SalesCustomerClass.class);
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public List<SalesCustomerClass> getClassList() {
		return classList;
	}

	public void setClassList(List<SalesCustomerClass> classList) {
		this.classList = classList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}