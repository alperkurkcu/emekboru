/**
 * 
 */
package com.emekboru.beans.quality;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.kaplamatestspecs.KaplamaKaliteKaynakAgziKoruma;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "kaplamaKaliteKaynakAgziKorumaBean")
@ViewScoped
public class KaplamaKaliteKaynakAgziKorumaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private KaplamaKaliteKaynakAgziKoruma kaplamaKaliteKaynakAgziKorumaForm = new KaplamaKaliteKaynakAgziKoruma();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private List<KaplamaKaliteKaynakAgziKoruma> allKaplamaKaliteKaynakAgziKorumaList = new ArrayList<KaplamaKaliteKaynakAgziKoruma>();

	// setters getters
	/**
	 * @return the kaplamaKaliteKaynakAgziKorumaForm
	 */
	public KaplamaKaliteKaynakAgziKoruma getKaplamaKaliteKaynakAgziKorumaForm() {
		return kaplamaKaliteKaynakAgziKorumaForm;
	}

	/**
	 * @param kaplamaKaliteKaynakAgziKorumaForm
	 *            the kaplamaKaliteKaynakAgziKorumaForm to set
	 */
	public void setKaplamaKaliteKaynakAgziKorumaForm(
			KaplamaKaliteKaynakAgziKoruma kaplamaKaliteKaynakAgziKorumaForm) {
		this.kaplamaKaliteKaynakAgziKorumaForm = kaplamaKaliteKaynakAgziKorumaForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the allKaplamaKaliteKaynakAgziKorumaList
	 */
	public List<KaplamaKaliteKaynakAgziKoruma> getAllKaplamaKaliteKaynakAgziKorumaList() {
		return allKaplamaKaliteKaynakAgziKorumaList;
	}

	/**
	 * @param allKaplamaKaliteKaynakAgziKorumaList
	 *            the allKaplamaKaliteKaynakAgziKorumaList to set
	 */
	public void setAllKaplamaKaliteKaynakAgziKorumaList(
			List<KaplamaKaliteKaynakAgziKoruma> allKaplamaKaliteKaynakAgziKorumaList) {
		this.allKaplamaKaliteKaynakAgziKorumaList = allKaplamaKaliteKaynakAgziKorumaList;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
