package com.emekboru.beans.sales.customer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.sales.SalesMenuBean;
import com.emekboru.jpa.sales.customer.SalesCustomer;
import com.emekboru.jpa.sales.customer.SalesCustomerFunctionType;
import com.emekboru.jpa.sales.customer.SalesJoinCustomerFunctionType;
import com.emekboru.jpaman.sales.customer.SalesCustomerManager;
import com.emekboru.jpaman.sales.customer.SalesJoinCustomerFunctionTypeManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "salesCustomerBean")
@ViewScoped
public class SalesCustomerBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4553054039598747730L;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	@ManagedProperty(value = "#{salesMenuBean}")
	private SalesMenuBean salesMenuBean;

	private SalesCustomer newCustomer = new SalesCustomer();
	private SalesCustomer selectedCustomer;

	private List<SalesCustomer> customerList;
	private List<SalesCustomer> filteredCustomerList;

	public SalesCustomerBean() {
		newCustomer = new SalesCustomer();
		selectedCustomer = new SalesCustomer();
	}

	@PostConstruct
	public void load() {
		System.out.println("SalesCustomerBean.load()");

		try {
			SalesCustomerManager manager = new SalesCustomerManager();
			// customerList = manager.findAll(SalesCustomer.class);
			customerList = manager.findAllOrderByASC(SalesCustomer.class,
					"commercialName, c.shortName");

			/* JOIN FUNCTIONS */
			// for (SalesCustomer customer : customerList) {
			// this.loadJoinCustomerFunctionType(customer);
			// }
			/* end of JOIN FUNCTIONS */
			selectedCustomer = customerList.get(0);
		} catch (Exception e) {
			System.out.println("SalesCustomerBean:" + e.toString());
		}
		System.out.println("SalesCustomerBean.load()- BİTTİ");
	}

	public void add() {
		System.out.println("SalesCustomerBean.add()");

		try {
			SalesCustomerManager manager = new SalesCustomerManager();
			newCustomer.setUserLastUpdate(userBean.getUser().getEmployee()
					.getFirstname()
					+ " " + userBean.getUser().getEmployee().getLastname());

			// customerList.add(newCustomer);
			/* JOIN FUNCTIONS */
			this.addJoinCustomerFunctionType(newCustomer);
			/* end of JOIN FUNCTIONS */

			manager.enterNew(newCustomer);

			customerList = manager.findAllOrderByASC(SalesCustomer.class,
					"commercialName, c.shortName");

			newCustomer = new SalesCustomer();
			FacesContextUtils.addInfoMessage("SubmitMessage");
		} catch (Exception e) {
			System.out.println("SalesCustomerBean:" + e.toString());
		}
	}

	public void update() {
		System.out.println("SalesCustomerBean.update()");

		try {
			SalesCustomerManager manager = new SalesCustomerManager();
			selectedCustomer.setUserLastUpdate(userBean.getUser().getEmployee()
					.getFirstname()
					+ " " + userBean.getUser().getEmployee().getLastname());

			/* JOIN FUNCTIONS */
			this.deleteJoinCustomerFunctionType(selectedCustomer);
			this.addJoinCustomerFunctionType(selectedCustomer);
			/* end of JOIN FUNCTIONS */
			manager.updateEntity(selectedCustomer);

			FacesContextUtils.addInfoMessage("UpdateItemMessage");
		} catch (Exception e) {
			System.out.println("SalesCustomerBean:" + e.toString());
		}
	}

	public void delete() {
		System.out.println("SalesCustomerBean.delete()");

		try {
			SalesCustomerManager manager = new SalesCustomerManager();
			/* JOIN FUNCTIONS */
			this.deleteJoinCustomerFunctionType(selectedCustomer);
			/* end of JOIN FUNCTIONS */
			manager.delete(selectedCustomer);
			customerList.remove(selectedCustomer);

			selectedCustomer = customerList.get(0);
			FacesContextUtils.addWarnMessage("DeletedMessage");
		} catch (Exception e) {
			System.out.println("SalesCustomerBean.delete(): " + e.toString());
			FacesContextUtils.addErrorMessage("salesCannotDelete");
		}
	}

	/* JOIN FUNCTIONS */
	private void addJoinCustomerFunctionType(SalesCustomer e) {
		SalesJoinCustomerFunctionTypeManager joinManager = new SalesJoinCustomerFunctionTypeManager();
		SalesJoinCustomerFunctionType newJoin;

		// deneme - Emek Boru hatasi

		if (e.getFunctionTypeList() != null) {

			for (int i = 0; i < e.getFunctionTypeList().size(); i++) {
				newJoin = new SalesJoinCustomerFunctionType();
				newJoin.setFunctionType(e.getFunctionTypeList().get(i));
				newJoin.setCustomer(e);
				joinManager.enterNew(newJoin);
			}
		}
	}

	private void loadJoinCustomerFunctionType(SalesCustomer customer) {
		SalesJoinCustomerFunctionTypeManager joinManager = new SalesJoinCustomerFunctionTypeManager();
		List<SalesJoinCustomerFunctionType> joinList = joinManager
				.findAll(SalesJoinCustomerFunctionType.class);

		customer.setFunctionTypeList(new ArrayList<SalesCustomerFunctionType>());

		for (int i = 0; i < joinList.size(); i++) {
			if (joinList.get(i).getCustomer().equals(customer)) {
				customer.getFunctionTypeList().add(
						joinList.get(i).getFunctionType());
			}
		}
	}

	private void deleteJoinCustomerFunctionType(SalesCustomer customer) {
		SalesJoinCustomerFunctionTypeManager joinManager = new SalesJoinCustomerFunctionTypeManager();
		List<SalesJoinCustomerFunctionType> joinList = joinManager
				.findAll(SalesJoinCustomerFunctionType.class);

		for (int i = 0; i < joinList.size(); i++) {
			if (joinList.get(i).getCustomer().equals(customer)) {
				joinManager.delete(joinList.get(i));
			}
		}
	}

	/* end of JOIN FUNCTIONS */
	/*
	 * GETTERS AND SETTERS
	 */

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public SalesCustomer getNewCustomer() {
		return newCustomer;
	}

	public void setNewCustomer(SalesCustomer newCustomer) {
		this.newCustomer = newCustomer;
	}

	public SalesCustomer getSelectedCustomer() {
		try {
			if (selectedCustomer == null)
				selectedCustomer = new SalesCustomer();
		} catch (Exception ex) {
			selectedCustomer = new SalesCustomer();
		}
		return selectedCustomer;
	}

	public void setSelectedCustomer(SalesCustomer selectedCustomer) {
		this.selectedCustomer = selectedCustomer;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public List<SalesCustomer> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(List<SalesCustomer> customerList) {
		this.customerList = customerList;
	}

	public List<SalesCustomer> getFilteredCustomerList() {
		try {
			selectedCustomer = filteredCustomerList.get(0);
			salesMenuBean.showNewType1Menu();
		} catch (Exception ex) {
			System.out.println("getFilteredList: " + ex);
		}
		return filteredCustomerList;
	}

	public void setFilteredCustomerList(List<SalesCustomer> filteredCustomerList) {
		this.filteredCustomerList = filteredCustomerList;
	}

	public SalesMenuBean getSalesMenuBean() {
		return salesMenuBean;
	}

	public void setSalesMenuBean(SalesMenuBean salesMenuBean) {
		this.salesMenuBean = salesMenuBean;
	}
}
