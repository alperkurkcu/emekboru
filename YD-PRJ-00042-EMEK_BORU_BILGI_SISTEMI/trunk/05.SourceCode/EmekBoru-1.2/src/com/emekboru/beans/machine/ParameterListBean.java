package com.emekboru.beans.machine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.emekboru.jpa.CementMachineParameter;
import com.emekboru.jpa.EpoxyMachineParameter;
import com.emekboru.jpa.Machine;
import com.emekboru.jpa.PipeCoat;
import com.emekboru.jpa.PolietilenMachineParameter;

@ManagedBean(name = "parameterListBean")
@SessionScoped
public class ParameterListBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<EpoxyMachineParameter> epoxyMachineParameters;
	private List<PolietilenMachineParameter> polietilenMachineParameters;
	private List<CementMachineParameter> cementMachineParameters;
	private PipeCoat selectedCoat;
	private String initializer;

	public ParameterListBean() {

		initializer = "initialer";
		epoxyMachineParameters = new ArrayList<EpoxyMachineParameter>();
		polietilenMachineParameters = new ArrayList<PolietilenMachineParameter>();
		cementMachineParameters = new ArrayList<CementMachineParameter>();
		selectedCoat = new PipeCoat();
	}

	public void viewMachineParameters(Machine machine) {

		// if(machine.getType().equals(MachineType.CEMENT_MACHINE)){
		//
		// CementMachineParameterManager cementParamManager = new
		// CementMachineParameterManager();
		// cementMachineParameters = (List<CementMachineParameter>)
		// cementParamManager.
		// findByField(CementMachineParameter.class, "machineId",
		// machine.getMachineId());
		// System.out.println("cement parameters : "+cementMachineParameters.size());
		// return;
		// }
		// if(machine.getType().equals(MachineType.POLIETILEN_MACHINE)){
		//
		// PolietilenMachineParameterManager polietilenParamManager = new
		// PolietilenMachineParameterManager();
		// polietilenMachineParameters = (List<PolietilenMachineParameter>)
		// polietilenParamManager.findByField( PolietilenMachineParameter.class,
		// "machineId", machine.getMachineId());
		// return;
		// }
		// if(machine.getType().equals(MachineType.EPOXYBOYA_MACHINE)){
		//
		// EpoxyMachineParameterManager epoxyParamManager = new
		// EpoxyMachineParameterManager();
		// epoxyMachineParameters = (List<EpoxyMachineParameter>)
		// epoxyParamManager
		// .findByField(EpoxyMachineParameter.class, "machineId"
		// , machine.getMachineId());
		// System.out.println("epoxy parameters : "+epoxyMachineParameters.size());
		// return;
		// }
		// FacesContextUtils.addWarnMessage("NoParamsMessage");
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public List<EpoxyMachineParameter> getEpoxyMachineParameters() {
		return epoxyMachineParameters;
	}

	public PipeCoat getSelectedCoat() {
		return selectedCoat;
	}

	public void setSelectedCoat(PipeCoat selectedCoat) {
		this.selectedCoat = selectedCoat;
	}

	public void setEpoxyMachineParameters(
			List<EpoxyMachineParameter> epoxyMachineParameters) {
		this.epoxyMachineParameters = epoxyMachineParameters;
	}

	public List<PolietilenMachineParameter> getPolietilenMachineParameters() {
		return polietilenMachineParameters;
	}

	public void setPolietilenMachineParameters(
			List<PolietilenMachineParameter> polietilenMachineParameters) {
		this.polietilenMachineParameters = polietilenMachineParameters;
	}

	public List<CementMachineParameter> getCementMachineParameters() {
		return cementMachineParameters;
	}

	public void setCementMachineParameters(
			List<CementMachineParameter> cementMachineParameters) {
		this.cementMachineParameters = cementMachineParameters;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getInitializer() {
		return initializer;
	}

	public void setInitializer(String initializer) {
		this.initializer = initializer;
	}
}
