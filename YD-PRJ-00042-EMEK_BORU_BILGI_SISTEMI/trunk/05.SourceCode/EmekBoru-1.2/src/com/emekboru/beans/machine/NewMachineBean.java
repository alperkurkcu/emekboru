package com.emekboru.beans.machine;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Machine;
import com.emekboru.jpa.MachinePart;
import com.emekboru.jpaman.MachineManager;
import com.emekboru.jpaman.MachinePartManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "newMachineBean")
@ViewScoped
public class NewMachineBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	private Machine newMachine;
	private Machine selectedMachine;
	private List<MachinePart> machineParts;

	public NewMachineBean() {

		newMachine = new Machine();
		selectedMachine = new Machine();
	}

	@PostConstruct
	public void init() {

	}

	public void submitNewMachine() {

		MachineManager machineManager = new MachineManager();
		List<Machine> machineList = (List<Machine>) machineManager.findByField(
				Machine.class, "name", newMachine.getName());
		// check if there is a company with the same name
		if (machineList.size() > 0) {

			newMachine.setName("");
			FacesContextUtils.addErrorMessage("ItemExistMessage");
			return;
		}

		if (newMachine != null && newMachine.getMachineId() > 0)
			machineManager.updateEntity(newMachine);
		else
			machineManager.enterNew(newMachine);

		FacesContextUtils.addInfoMessage("SubmitMessage");

		// add the new machine to the list
		MachineListBean bean = (MachineListBean) FacesContextUtils.getViewBean(
				"machineListBean", MachineListBean.class);
		bean.loadAllMachines();
		bean.getAllMachineList().add(newMachine);
		newMachine = new Machine();
	}

	public void deleteMachine() {

		MachineManager machineManager = new MachineManager();
		machineManager.deleteByField(Machine.class, "machineId",
				selectedMachine.getMachineId());
		FacesContextUtils.addWarnMessage("DeletedMessage");

		// remove this machine from the list
		MachineListBean bean = (MachineListBean) FacesContextUtils.getViewBean(
				"machineListBean", MachineListBean.class);
		bean.loadAllMachines();
		bean.getAllMachineList().remove(newMachine);
		newMachine = new Machine();
	}

	public void updateMachine() {

		if (selectedMachine.getMachineId() <= 0) {

			return;
		}

		MachineManager man = new MachineManager();
		man.updateEntity(selectedMachine);
		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}

	public void displayMachineParts() {

		if (newMachine != null) {
			machineParts = MachinePartManager.findByMachineId(newMachine
					.getMachineId());
		}
	}

	public void beforeUpdate(Machine machine) {

		selectedMachine = machine;
	}

	public void beforeDelete(Machine machine) {

		selectedMachine = machine;
	}

	// ********************************************************** //
	// *** GETTERS AND SETTERS *** //
	// ********************************************************** //
	public Machine getNewMachine() {
		return newMachine;
	}

	public void setNewMachine(Machine newMachine) {
		this.newMachine = newMachine;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Machine getSelectedMachine() {
		return selectedMachine;
	}

	public void setSelectedMachine(Machine selectedMachine) {
		this.selectedMachine = selectedMachine;
	}

	public List<MachinePart> getMachineParts() {
		return machineParts;
	}

	public void setMachineParts(List<MachinePart> machineParts) {
		this.machineParts = machineParts;
	}

}
