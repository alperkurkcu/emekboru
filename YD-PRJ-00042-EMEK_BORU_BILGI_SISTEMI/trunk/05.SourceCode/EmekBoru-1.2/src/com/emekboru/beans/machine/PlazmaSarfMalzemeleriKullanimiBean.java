/**
 * 
 */
package com.emekboru.beans.machine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.Machine;
import com.emekboru.jpa.machines.PlazmaSarfMalzemeleriKullanimi;
import com.emekboru.jpaman.MachineManager;
import com.emekboru.jpaman.machines.PlazmaSarfMalzemeleriKullanimiManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "plazmaSarfMalzemeleriKullanimiBean")
@ViewScoped
public class PlazmaSarfMalzemeleriKullanimiBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<PlazmaSarfMalzemeleriKullanimi> allSarfMalzemeleri = new ArrayList<PlazmaSarfMalzemeleriKullanimi>();
	private PlazmaSarfMalzemeleriKullanimi sarfMalzemeleriForm = new PlazmaSarfMalzemeleriKullanimi();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private List<Machine> allUretimMachineList;

	private boolean updateButtonRender = false;

	public PlazmaSarfMalzemeleriKullanimiBean() {
		allUretimMachineList = new ArrayList<Machine>(0);
		loadAllMachines();
		fillTestList();
	}

	public void formListener(SelectEvent event) {

		updateButtonRender = true;
	}

	public void addSarfMalzemeleri() {
		sarfMalzemeleriForm = new PlazmaSarfMalzemeleriKullanimi();
		updateButtonRender = false;
	}

	public void loadAllMachines() {

		MachineManager machineManager = new MachineManager();
		allUretimMachineList = machineManager.findByField(Machine.class,
				"machineType.machineTypeId", 2);
	}

	public void addOrUpdateSarfMalzemeleri() {

		PlazmaSarfMalzemeleriKullanimiManager manager = new PlazmaSarfMalzemeleriKullanimiManager();
		if (sarfMalzemeleriForm.getId() == null) {
			sarfMalzemeleriForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			sarfMalzemeleriForm.setEkleyenEmployee(userBean.getUser());

			manager.enterNew(sarfMalzemeleriForm);
			FacesContextUtils.addInfoMessage("SubmitMessage");
		} else if (sarfMalzemeleriForm.getId() != null) {
			sarfMalzemeleriForm.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			sarfMalzemeleriForm.setGuncelleyenEmployee(userBean.getUser());
			manager.updateEntity(sarfMalzemeleriForm);
			FacesContextUtils.addInfoMessage("UpdateItemMessage");
		}
		fillTestList();
	}

	@SuppressWarnings("static-access")
	public void fillTestList() {
		PlazmaSarfMalzemeleriKullanimiManager manager = new PlazmaSarfMalzemeleriKullanimiManager();
		allSarfMalzemeleri = manager.getAllSarfMalzemeleri();
		manager.refreshCollection(allSarfMalzemeleri);
		sarfMalzemeleriForm = new PlazmaSarfMalzemeleriKullanimi();
	}

	public void deleteSarfMalzemeleri() {

		PlazmaSarfMalzemeleriKullanimiManager manager = new PlazmaSarfMalzemeleriKullanimiManager();
		if (sarfMalzemeleriForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		manager.delete(sarfMalzemeleriForm);
		sarfMalzemeleriForm = new PlazmaSarfMalzemeleriKullanimi();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		fillTestList();
	}

	// setters getters
	/**
	 * @return the allSarfMalzemeleri
	 */
	public List<PlazmaSarfMalzemeleriKullanimi> getAllSarfMalzemeleri() {
		return allSarfMalzemeleri;
	}

	/**
	 * @param allSarfMalzemeleri
	 *            the allSarfMalzemeleri to set
	 */
	public void setAllSarfMalzemeleri(
			List<PlazmaSarfMalzemeleriKullanimi> allSarfMalzemeleri) {
		this.allSarfMalzemeleri = allSarfMalzemeleri;
	}

	/**
	 * @return the sarfMalzemeleriForm
	 */
	public PlazmaSarfMalzemeleriKullanimi getSarfMalzemeleriForm() {
		return sarfMalzemeleriForm;
	}

	/**
	 * @param sarfMalzemeleriForm
	 *            the sarfMalzemeleriForm to set
	 */
	public void setSarfMalzemeleriForm(
			PlazmaSarfMalzemeleriKullanimi sarfMalzemeleriForm) {
		this.sarfMalzemeleriForm = sarfMalzemeleriForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the allUretimMachineList
	 */
	public List<Machine> getAllUretimMachineList() {
		return allUretimMachineList;
	}

	/**
	 * @param allUretimMachineList
	 *            the allUretimMachineList to set
	 */
	public void setAllUretimMachineList(List<Machine> allUretimMachineList) {
		this.allUretimMachineList = allUretimMachineList;
	}

}
