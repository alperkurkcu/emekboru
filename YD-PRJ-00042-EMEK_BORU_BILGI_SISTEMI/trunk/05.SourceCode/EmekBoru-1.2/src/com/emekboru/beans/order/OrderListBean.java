package com.emekboru.beans.order;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;

import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Order;
import com.emekboru.jpaman.OrderManager;
import com.emekboru.jpaman.WhereClauseArgs;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "orderListBean")
@SessionScoped
public class OrderListBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	private List<Order> unfinishedOrderItemList;

	public OrderListBean() {

		unfinishedOrderItemList = new ArrayList<Order>();
	}

	@SuppressWarnings("rawtypes")
	public void loadUnfinishedOrderItemList() {

		OrderManager orderManager = new OrderManager();

		WhereClauseArgs<Date> arg1 = new WhereClauseArgs.Builder<Date>()
				.setFieldName("endDate").setValue(null).build();

		ArrayList<WhereClauseArgs> conditionList = new ArrayList<WhereClauseArgs>();
		conditionList.add(arg1);
		unfinishedOrderItemList = orderManager.selectFromWhereQuerie(
				Order.class, conditionList);
	}

	public void goToQualitySpecPage(ActionEvent event) {

		FacesContextUtils.redirect(config.getPageUrl().QUALITY_SPEC_PAGE);
	}

	public void goToQualitySpecPage() {

		FacesContextUtils.redirect(config.getPageUrl().QUALITY_SPEC_PAGE);
	}

	// *****************************************************************************//
	// *************************** GETTERS AND SETTERS
	// ****************************//
	// *****************************************************************************//
	public List<Order> getUnfinishedOrderItemList() {

		return unfinishedOrderItemList;
	}

	public void setUnfinishedOrderItemList(List<Order> unfinishedOrderItemList) {

		this.unfinishedOrderItemList = unfinishedOrderItemList;
	}

	public static long getSerialversionuid() {

		return serialVersionUID;
	}

}
