package com.emekboru.beans.order;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.sales.order.SalesOrderBean;
import com.emekboru.beans.test.TestAgirlikDusurmeSonucBean;
import com.emekboru.beans.test.TestBukmeSonucBean;
import com.emekboru.beans.test.TestCekmeSonucBean;
import com.emekboru.beans.test.TestCentikDarbeSonucBean;
import com.emekboru.beans.test.TestKaynakSonucBean;
import com.emekboru.beans.test.TestKimyasalAnalizSonucBean;
import com.emekboru.beans.test.TestRendersBean;
import com.emekboru.beans.test.kaplama.TestKaplamaBetonEgilmeBasincSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaBetonGozKontrolSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaBetonOranBelirlenmesiSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaBetonYuzeyHazirligiSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaBuchholzSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaBukmeSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaDarbeSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaDeliciUcDirencSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaElektrikselDirencTestSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaFlowCoatBendSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaFlowCoatBondSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaFlowCoatBuchholzSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaFlowCoatCureSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaFlowCoatFilmThicknessSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaFlowCoatPinholeSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaFlowCoatStrippingSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaFlowCoatWaterSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaGasBlisteringTestSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaGozKontrolSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaHolidayPinholeSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaHydraulicBlisteringTestSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaKalinligiSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaKaplamasizBolgeSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaKatodikSoyulmaSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaKumlamaMalzemesiTuzMiktariSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaKumlamaOncesiOnIsitmaSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaKumlamaOrtamSartlariSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaKumlanmisBoruTozMiktariSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaKumlanmisBoruTuzMiktariSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaKumlanmisBoruYuzeyTemizligiSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaMixedWetPaintTestSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaMpqtKaplamaKalinligliSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaNihaiKontrolSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaOncesiYuzeyKontroluSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaOnlineHolidaySonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaProcessBozulmaSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaPulloffYapismaSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaSertlikSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaSonrasiYuzeyKontroluSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaStorageConditionTestSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaTamirTestSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaThermalAgeingTestSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaTozBoyaSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaTozEpoksiXcutSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaUvAgeingTestSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaUygulamaSicaklikSonucBean;
import com.emekboru.beans.test.kaplama.TestKaplamaYapismaSonucBean;
import com.emekboru.beans.test.kaplama.TestPolyolefinUzamaSonucBean;
import com.emekboru.beans.test.tahribatsiztestsonuc.TestTahribatsizFloroskopikSonucBean;
import com.emekboru.beans.test.tahribatsiztestsonuc.TestTahribatsizOtomatikUtSonucBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.config.Test;
import com.emekboru.jpa.AgirlikTestsSpec;
import com.emekboru.jpa.BoruBoyutsalKontrolTolerans;
import com.emekboru.jpa.BoruKaliteKaplamaTurleri;
import com.emekboru.jpa.BukmeTestsSpec;
import com.emekboru.jpa.CekmeTestsSpec;
import com.emekboru.jpa.CentikDarbeTestsSpec;
import com.emekboru.jpa.ChemicalRequirement;
import com.emekboru.jpa.DestructiveTestsSpec;
import com.emekboru.jpa.HidrostaticTestsSpec;
import com.emekboru.jpa.KaynakMakroTestSpec;
import com.emekboru.jpa.KaynakSertlikTestsSpec;
import com.emekboru.jpa.Layer;
import com.emekboru.jpa.NondestructiveTestsSpec;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.PlannedIsolation;
import com.emekboru.jpa.QualitySpec;
import com.emekboru.jpa.isolation.AbstractTestsSpec;
import com.emekboru.jpa.isolation.BetonEgmeTestsSpec;
import com.emekboru.jpa.isolation.BukmeIsolationTestsSpec;
import com.emekboru.jpa.isolation.DarbeTestsSpec;
import com.emekboru.jpa.isolation.DeliciUcaKarsiDirencTestsSpec;
import com.emekboru.jpa.isolation.HolidayTestsSpec;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.isolation.IsolationTestType;
import com.emekboru.jpa.isolation.KaplamaKalinlikTestsSpec;
import com.emekboru.jpa.isolation.KatodikSoyulmaTestsSpec;
import com.emekboru.jpa.isolation.PolyolefinUzamaTestsSpec;
import com.emekboru.jpa.isolation.SertlikTestsSpec;
import com.emekboru.jpa.isolation.TozBoyaDSCTestsSpec;
import com.emekboru.jpa.isolation.YapismaTestsSpec;
import com.emekboru.jpa.isolation.YuzeyKontrolTestsSpec;
import com.emekboru.jpa.kaplamatestspecs.KaliteKaplamaMarkalama;
import com.emekboru.jpa.kaplamatestspecs.KaplamaKaliteCutBackKoruma;
import com.emekboru.jpa.kaplamatestspecs.KaplamaKaliteKaynakAgziKoruma;
import com.emekboru.jpa.sales.customer.SalesCustomer;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.AgirlikTestsSpecManager;
import com.emekboru.jpaman.BoruBoyutsalKontrolToleransManager;
import com.emekboru.jpaman.BoruKaliteKaplamaTurleriManager;
import com.emekboru.jpaman.BukmeTestsSpecManager;
import com.emekboru.jpaman.CekmeTestSpecManager;
import com.emekboru.jpaman.CentikDarbeTestsSpecManager;
import com.emekboru.jpaman.ChemicalRequirementManager;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.DestructiveTestsSpecManager;
import com.emekboru.jpaman.Factory;
import com.emekboru.jpaman.HidrostaticTestsSpecManager;
import com.emekboru.jpaman.KaynakMacroTestSpecManager;
import com.emekboru.jpaman.KaynakSertlikTestSpecManager;
import com.emekboru.jpaman.LayerManager;
import com.emekboru.jpaman.NondestructiveTestsSpecManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.PlannedIsolationManager;
import com.emekboru.jpaman.QualitySpecManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.KaliteKaplamaMarkalamaManager;
import com.emekboru.jpaman.kaplamatest.KaplamaKaliteCutBackKorumaManager;
import com.emekboru.jpaman.kaplamatest.KaplamaKaliteKaynakAgziKorumaManager;
import com.emekboru.jpaman.sales.order.SalesItemManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.IsolationType;
import com.emekboru.utils.ReportUtil;
import com.emekboru.utils.UtilInsCore;

@ManagedBean(name = "qualitySpecBean")
@ViewScoped
public class QualitySpecBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	// tahribatlı testler
	@ManagedProperty(value = "#{testKimyasalAnalizSonucBean}")
	private TestKimyasalAnalizSonucBean testKimyasalAnalizSonucBean;

	@ManagedProperty(value = "#{testCekmeSonucBean}")
	private TestCekmeSonucBean testCekmeSonucBean;

	@ManagedProperty(value = "#{testBukmeSonucBean}")
	private TestBukmeSonucBean testBukmeSonucBean;

	@ManagedProperty(value = "#{testCentikDarbeSonucBean}")
	private TestCentikDarbeSonucBean testCentikDarbeSonucBean;

	@ManagedProperty(value = "#{testAgirlikDusurmeSonucBean}")
	private TestAgirlikDusurmeSonucBean testAgirlikDusurmeSonucBean;

	@ManagedProperty(value = "#{testKaynakSonucBean}")
	private TestKaynakSonucBean testKaynakSonucBean;

	// tahribatsız test sonuc
	@ManagedProperty(value = "#{testTahribatsizOtomatikUtSonucBean}")
	private TestTahribatsizOtomatikUtSonucBean testTahribatsizOtomatikUtSonucBean;

	@ManagedProperty(value = "#{testTahribatsizFloroskopikSonucBean}")
	private TestTahribatsizFloroskopikSonucBean testTahribatsizFloroskopikSonucBean;

	// kaplama testleri
	@ManagedProperty(value = "#{testKaplamaBetonEgilmeBasincSonucBean}")
	private TestKaplamaBetonEgilmeBasincSonucBean testKaplamaBetonEgilmeBasincSonucBean;

	@ManagedProperty(value = "#{testKaplamaKumlamaOrtamSartlariSonucBean}")
	private TestKaplamaKumlamaOrtamSartlariSonucBean testKaplamaKumlamaOrtamSartlariSonucBean;

	@ManagedProperty(value = "#{testKaplamaGozKontrolSonucBean}")
	private TestKaplamaGozKontrolSonucBean testKaplamaGozKontrolSonucBean;

	@ManagedProperty(value = "#{testKaplamaBetonOranBelirlenmesiSonucBean}")
	private TestKaplamaBetonOranBelirlenmesiSonucBean testKaplamaBetonOranBelirlenmesiSonucBean;

	@ManagedProperty(value = "#{testKaplamaDarbeSonucBean}")
	private TestKaplamaDarbeSonucBean testKaplamaDarbeSonucBean;

	@ManagedProperty(value = "#{testKaplamaKumlanmisBoruTozMiktariSonucBean}")
	private TestKaplamaKumlanmisBoruTozMiktariSonucBean testKaplamaKumlanmisBoruTozMiktariSonucBean;

	@ManagedProperty(value = "#{testKaplamaBukmeSonucBean}")
	private TestKaplamaBukmeSonucBean testKaplamaBukmeSonucBean;

	@ManagedProperty(value = "#{testKaplamaKumlanmisBoruTuzMiktariSonucBean}")
	private TestKaplamaKumlanmisBoruTuzMiktariSonucBean testKaplamaKumlanmisBoruTuzMiktariSonucBean;

	@ManagedProperty(value = "#{testKaplamaTozBoyaSonucBean}")
	private TestKaplamaTozBoyaSonucBean testKaplamaTozBoyaSonucBean;

	@ManagedProperty(value = "#{testKaplamaTozEpoksiXcutSonucBean}")
	private TestKaplamaTozEpoksiXcutSonucBean testKaplamaTozEpoksiXcutSonucBean;

	@ManagedProperty(value = "#{testKaplamaHolidayPinholeSonucBean}")
	private TestKaplamaHolidayPinholeSonucBean testKaplamaHolidayPinholeSonucBean;

	@ManagedProperty(value = "#{testKaplamaOnlineHolidaySonucBean}")
	private TestKaplamaOnlineHolidaySonucBean testKaplamaOnlineHolidaySonucBean;

	@ManagedProperty(value = "#{testKaplamaYapismaSonucBean}")
	private TestKaplamaYapismaSonucBean testKaplamaYapismaSonucBean;

	@ManagedProperty(value = "#{testKaplamaDeliciUcDirencSonucBean}")
	private TestKaplamaDeliciUcDirencSonucBean testKaplamaDeliciUcDirencSonucBean;

	@ManagedProperty(value = "#{testPolyolefinUzamaSonucBean}")
	private TestPolyolefinUzamaSonucBean testPolyolefinUzamaSonucBean;

	@ManagedProperty(value = "#{testKaplamaKatodikSoyulmaSonucBean}")
	private TestKaplamaKatodikSoyulmaSonucBean testKaplamaKatodikSoyulmaSonucBean;

	@ManagedProperty(value = "#{testKaplamaSertlikSonucBean}")
	private TestKaplamaSertlikSonucBean testKaplamaSertlikSonucBean;

	@ManagedProperty(value = "#{testKaplamaKalinligiSonucBean}")
	private TestKaplamaKalinligiSonucBean testKaplamaKalinligiSonucBean;

	@ManagedProperty(value = "#{testKaplamaUygulamaSicaklikSonucBean}")
	private TestKaplamaUygulamaSicaklikSonucBean testKaplamaUygulamaSicaklikSonucBean;

	@ManagedProperty(value = "#{testKaplamaBetonYuzeyHazirligiSonucBean}")
	private TestKaplamaBetonYuzeyHazirligiSonucBean testKaplamaBetonYuzeyHazirligiSonucBean;

	@ManagedProperty(value = "#{testKaplamaTamirTestSonucBean}")
	private TestKaplamaTamirTestSonucBean testKaplamaTamirTestSonucBean;

	@ManagedProperty(value = "#{testKaplamaBuchholzSonucBean}")
	private TestKaplamaBuchholzSonucBean testKaplamaBuchholzSonucBean;

	@ManagedProperty(value = "#{testKaplamaKumlanmisBoruYuzeyTemizligiSonucBean}")
	private TestKaplamaKumlanmisBoruYuzeyTemizligiSonucBean testKaplamaKumlanmisBoruYuzeyTemizligiSonucBean;

	@ManagedProperty(value = "#{testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucBean}")
	private TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonucBean testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucBean;

	@ManagedProperty(value = "#{testKaplamaKumlamaOncesiOnIsitmaSonucBean}")
	private TestKaplamaKumlamaOncesiOnIsitmaSonucBean testKaplamaKumlamaOncesiOnIsitmaSonucBean;

	@ManagedProperty(value = "#{testKaplamaKaplamasizBolgeSonucBean}")
	private TestKaplamaKaplamasizBolgeSonucBean testKaplamaKaplamasizBolgeSonucBean;

	@ManagedProperty(value = "#{testKaplamaNihaiKontrolSonucBean}")
	private TestKaplamaNihaiKontrolSonucBean testKaplamaNihaiKontrolSonucBean;

	@ManagedProperty(value = "#{testKaplamaKumlamaMalzemesiTuzMiktariSonucBean}")
	private TestKaplamaKumlamaMalzemesiTuzMiktariSonucBean testKaplamaKumlamaMalzemesiTuzMiktariSonucBean;

	@ManagedProperty(value = "#{testKaplamaBetonGozKontrolSonucBean}")
	private TestKaplamaBetonGozKontrolSonucBean testKaplamaBetonGozKontrolSonucBean;

	@ManagedProperty(value = "#{testKaplamaProcessBozulmaSonucBean}")
	private TestKaplamaProcessBozulmaSonucBean testKaplamaProcessBozulmaSonucBean;

	@ManagedProperty(value = "#{testKaplamaFlowCoatBendSonucBean}")
	private TestKaplamaFlowCoatBendSonucBean testKaplamaFlowCoatBendSonucBean;

	@ManagedProperty(value = "#{testKaplamaFlowCoatBondSonucBean}")
	private TestKaplamaFlowCoatBondSonucBean testKaplamaFlowCoatBondSonucBean;

	@ManagedProperty(value = "#{testKaplamaFlowCoatBuchholzSonucBean}")
	private TestKaplamaFlowCoatBuchholzSonucBean testKaplamaFlowCoatBuchholzSonucBean;

	@ManagedProperty(value = "#{testKaplamaFlowCoatCureSonucBean}")
	private TestKaplamaFlowCoatCureSonucBean testKaplamaFlowCoatCureSonucBean;

	@ManagedProperty(value = "#{testKaplamaFlowCoatFilmThicknessSonucBean}")
	private TestKaplamaFlowCoatFilmThicknessSonucBean testKaplamaFlowCoatFilmThicknessSonucBean;

	@ManagedProperty(value = "#{testKaplamaFlowCoatPinholeSonucBean}")
	private TestKaplamaFlowCoatPinholeSonucBean testKaplamaFlowCoatPinholeSonucBean;

	@ManagedProperty(value = "#{testKaplamaFlowCoatStrippingSonucBean}")
	private TestKaplamaFlowCoatStrippingSonucBean testKaplamaFlowCoatStrippingSonucBean;

	@ManagedProperty(value = "#{testKaplamaFlowCoatWaterSonucBean}")
	private TestKaplamaFlowCoatWaterSonucBean testKaplamaFlowCoatWaterSonucBean;

	@ManagedProperty(value = "#{testKaplamaSonrasiYuzeyKontroluSonucBean}")
	private TestKaplamaSonrasiYuzeyKontroluSonucBean testKaplamaSonrasiYuzeyKontroluSonucBean;

	@ManagedProperty(value = "#{testKaplamaOncesiYuzeyKontroluSonucBean}")
	private TestKaplamaOncesiYuzeyKontroluSonucBean testKaplamaOncesiYuzeyKontroluSonucBean;

	@ManagedProperty(value = "#{testKaplamaPulloffYapismaSonucBean}")
	private TestKaplamaPulloffYapismaSonucBean testKaplamaPulloffYapismaSonucBean;

	@ManagedProperty(value = "#{testKaplamaMpqtKaplamaKalinligliSonucBean}")
	private TestKaplamaMpqtKaplamaKalinligliSonucBean testKaplamaMpqtKaplamaKalinligliSonucBean;

	@ManagedProperty(value = "#{testKaplamaElektrikselDirencTestSonucBean}")
	private TestKaplamaElektrikselDirencTestSonucBean testKaplamaElektrikselDirencTestSonucBean;

	@ManagedProperty(value = "#{testKaplamaGasBlisteringTestSonucBean}")
	private TestKaplamaGasBlisteringTestSonucBean testKaplamaGasBlisteringTestSonucBean;

	@ManagedProperty(value = "#{testKaplamaHydraulicBlisteringTestSonucBean}")
	private TestKaplamaHydraulicBlisteringTestSonucBean testKaplamaHydraulicBlisteringTestSonucBean;

	@ManagedProperty(value = "#{testKaplamaMixedWetPaintTestSonucBean}")
	private TestKaplamaMixedWetPaintTestSonucBean testKaplamaMixedWetPaintTestSonucBean;

	@ManagedProperty(value = "#{testKaplamaStorageConditionTestSonucBean}")
	private TestKaplamaStorageConditionTestSonucBean testKaplamaStorageConditionTestSonucBean;

	@ManagedProperty(value = "#{testKaplamaThermalAgeingTestSonucBean}")
	private TestKaplamaThermalAgeingTestSonucBean testKaplamaThermalAgeingTestSonucBean;

	@ManagedProperty(value = "#{testKaplamaUvAgeingTestSonucBean}")
	private TestKaplamaUvAgeingTestSonucBean testKaplamaUvAgeingTestSonucBean;

	@ManagedProperty(value = "#{testRendersBean}")
	private TestRendersBean testRendersBean;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private Integer salesItemId = 0;

	private Pipe selectedPipe = null;
	private Pipe bandEkiPipe = null;
	private SalesCustomer selectedSalesCustomer = null;
	private List<SalesItem> items = null;
	private SalesItem selectedSalesItem = null;

	// we load the coating for the selected order (if it has any)
	private PlannedIsolation inIsolation = null;
	private PlannedIsolation outIsolation = null;
	private Layer layer = null;

	// Tests related with SelectedOrder
	private QualitySpec qualitySpec = null;

	private BoruBoyutsalKontrolTolerans boyutsalKontrol = null;
	private HidrostaticTestsSpec selectedHidro = null;
	private DestructiveTestsSpec selectedTest = new DestructiveTestsSpec();
	private NondestructiveTestsSpec selectedNonTest = new NondestructiveTestsSpec();
	private KaliteKaplamaMarkalama selectedMarkalama = null;
	private KaplamaKaliteKaynakAgziKoruma selectedKaynakAgziKoruma = null;
	private KaplamaKaliteCutBackKoruma selectedCutBack = null;

	// here we keep track of boolean values for tests that we have to perform
	// these booleans control in the JSF page tests to be displayed
	private SelectedTests selectedTests = null;

	// A.K. - file upload
	// private UploadedFile documentFile;
	// private String docFullName = null;
	// private String docOrigName = null;
	private List<Pipe> producedPipes;
	private List<Pipe> tahribatliTestYapilmisBorular;

	// Kaplama Testleri

	private AbstractTestsSpec isolationTestsDetail = null;

	private IsolationTestDefinition isolationTestDefinition = new IsolationTestDefinition();
	private List<IsolationTestDefinition> internalIsolationTestDefinitions = new ArrayList<IsolationTestDefinition>();
	private List<IsolationTestDefinition> externalIsolationTestDefinitions = new ArrayList<IsolationTestDefinition>();
	Class<?> isolationLoadClass = null;
	private IsolationTestDefinition selectedIsolation = new IsolationTestDefinition();

	// EntityManager oMan = Factory.getInstance().createEntityManager();

	public Integer uploadRender = 0;

	private String barkodNo = null;

	// kalite tanimlari kaplama türleri
	private BoruKaliteKaplamaTurleri boruKaliteKaplamaTurleri = new BoruKaliteKaplamaTurleri();

	public QualitySpecBean() {

		inIsolation = new PlannedIsolation();
		outIsolation = new PlannedIsolation();
		layer = new Layer();
		qualitySpec = new QualitySpec();
		boyutsalKontrol = new BoruBoyutsalKontrolTolerans();
		selectedHidro = new HidrostaticTestsSpec();
		selectedMarkalama = new KaliteKaplamaMarkalama();
		selectedKaynakAgziKoruma = new KaplamaKaliteKaynakAgziKoruma();
		selectedCutBack = new KaplamaKaliteCutBackKoruma();
		producedPipes = new ArrayList<Pipe>();
		tahribatliTestYapilmisBorular = new ArrayList<Pipe>();
		selectedSalesCustomer = new SalesCustomer();
		selectedSalesItem = new SalesItem();
		items = new ArrayList<SalesItem>(0);

		privatePath = File.separatorChar + "testDocs" + File.separatorChar
				+ "uploadedDocuments" + File.separatorChar
				+ "boruBoyutsalTanim" + File.separatorChar;

	}

	@PostConstruct
	public void init() {

		selectedTests = new SelectedTests(config.getConfig());

		System.out.println("QualitySpecBean.load()");

		try {
			this.setPath(config.getConfig().getFolder().getAbsolutePath()
					+ privatePath);

			System.out.println("Path for Upload File (QualitySpec):			" + path);
		} catch (Exception ex) {
			System.out.println("Path for Upload File (QualitySpec):			"
					+ FacesContext.getCurrentInstance().getExternalContext()
							.getRealPath(privatePath));

			System.out.print(path);
			System.out.println("CAUSE OF " + ex.toString());
		}

		theFile = new File(path);
		if (!theFile.isDirectory()) {
			theFile.mkdirs();
		}

		fileArray = theFile.listFiles();

	}

	// public void saveIsolationTestDetatil() {
	// EntityManager man = Factory.getInstance().createEntityManager();
	// man.getTransaction().begin();
	// isolationTestsDetail.setOrder(selectedOrder);
	// //
	// isolationTestsDetail.setIsolationTestDefinition(isolationTestDefinition);
	// man.persist(isolationTestsDetail);
	//
	// isolationTestDefinition.setHasDetay(true);
	// man.merge(isolationTestDefinition);
	// man.getTransaction().commit();
	// man.close();
	//
	// FacesContextUtils.addInfoMessage("SubmitMessage");
	// }

	public void saveIsolationTestDetail() {

		if (isolationTestDefinition == null) {

			FacesContextUtils.addInfoMessage("AddDetails");
			return;
		}

		EntityManager man = Factory.getInstance().createEntityManager();
		man.getTransaction().begin();
		isolationTestsDetail.setSalesItem(selectedSalesItem);
		// isolationTestsDetail.setIsolationTestDefinition(isolationTestDefinition);
		man.persist(isolationTestsDetail);

		isolationTestDefinition.setHasDetay(true);
		man.merge(isolationTestDefinition);
		man.getTransaction().commit();
		man.close();

		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	// public void deleteIsolationTestDetatil() {
	// EntityManager man = Factory.getInstance().createEntityManager();
	// man.getTransaction().begin();
	// isolationTestsDetail.setOrder(selectedOrder);
	// //
	// isolationTestsDetail.setIsolationTestDefinition(isolationTestDefinition);
	// man.remove(isolationTestsDetail);
	// man.getTransaction().commit();
	// man.close();
	// FacesContextUtils.addInfoMessage("SubmitMessage");
	// }

	public void deleteIsolationTestDetatil() {

		if (isolationTestDefinition == null) {

			FacesContextUtils.addInfoMessage("DeletionError");
			return;
		}

		EntityManager man = Factory.getInstance().createEntityManager();
		man.getTransaction().begin();
		isolationTestsDetail.setSalesItem(selectedSalesItem);
		// isolationTestsDetail.setIsolationTestDefinition(isolationTestDefinition);
		man.remove(isolationTestsDetail);
		man.getTransaction().commit();
		man.close();
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	/*
	 * Load orders which are not completed yet. These orders' statuses should
	 * indicate isEnd = false.
	 */
	// public void loadActiveOrders() {
	//
	// EntityManager entMan = Factory.getInstance().createEntityManager();
	// selectedCustomer = entMan.find(Customer.class,
	// selectedCustomer.getCustomerId());
	// entMan.refresh(selectedCustomer);
	// OrderManager man = new OrderManager();
	// List<Status> statusList = config.getConfig().getOrder()
	// .findUnendedStatuses();
	// orders = man.findNotEnded(selectedCustomer.getCustomerId(), statusList);
	// }

	public void loadActiveOrders() {

		EntityManager entMan = Factory.getInstance().createEntityManager();
		selectedSalesCustomer = entMan.find(SalesCustomer.class,
				selectedSalesCustomer.getSalesCustomerId());
		entMan.refresh(selectedSalesCustomer);
		SalesItemManager itemManager = new SalesItemManager();
		items = itemManager.findAll(SalesItem.class);
	}

	// public void loadOrderDetails() {
	// EntityManager man = Factory.getInstance().createEntityManager();
	// man.getTransaction().begin();
	// selectedOrder = man.find(Order.class, selectedOrder.getOrderId());
	// loadTestOrders();
	// // selectedCustomer = selectedOrder.getCustomer();
	// selectedOrder.getPlannedIsolation();
	// selectedOrder.getChemicalRequirements();
	// selectedOrder.getQualitySpec();
	// // alper
	// selectedOrder.getBoyutsalKontrol();
	// selectedOrder.getDestructiveTestsSpecs();
	// selectedOrder.getNondestructiveTestsSpecs();
	//
	// Query internalQuery = man
	// .createNamedQuery(
	// "IsolationTestDefinition.findIsolationByOrderIdAndIsolationType")
	// .setParameter(1, IsolationType.IC_KAPLAMA)
	// .setParameter(2, selectedOrder);
	// internalIsolationTestDefinitions = internalQuery.getResultList();
	// Query externalQuery = man
	// .createNamedQuery(
	// "IsolationTestDefinition.findIsolationByOrderIdAndIsolationType")
	// .setParameter(1, IsolationType.DIS_KAPLAMA)
	// .setParameter(2, selectedOrder);
	// externalIsolationTestDefinitions = externalQuery.getResultList();
	//
	// man.getTransaction().commit();
	// man.close();
	// if (selectedOrder.getQualitySpec() != null)
	// qualitySpec = selectedOrder.getQualitySpec();
	//
	// // alper
	// if (selectedOrder.getBoyutsalKontrol() != null)
	// boyutsalKontrol = selectedOrder.getBoyutsalKontrol();
	//
	// for (PlannedIsolation iso : selectedOrder.getPlannedIsolation()) {
	// if (iso.getInternalExternal().equals(
	// IsolationType.IC_KAPLAMA.getType()))
	// inIsolation = iso;
	// if (iso.getInternalExternal().equals(
	// IsolationType.DIS_KAPLAMA.getType()))
	// outIsolation = iso;
	// }
	//
	// // init the Boolean values for MENU part of qualityspecs.jsf page
	// for (NondestructiveTestsSpec t : selectedOrder
	// .getNondestructiveTestsSpecs())
	// selectedTests.activate(t.getGlobalId());
	// for (DestructiveTestsSpec t : selectedOrder.getDestructiveTestsSpecs())
	// selectedTests.activate(t.getGlobalId());
	//
	// orderId = selectedOrder.getOrderId();
	// }

	@SuppressWarnings("unchecked")
	public void loadOrderDetails() {
		EntityManager man = Factory.getInstance().createEntityManager();
		man.getTransaction().begin();
		selectedSalesItem = man.find(SalesItem.class,
				selectedSalesItem.getItemId());
		loadTestOrders();
		selectedSalesCustomer = selectedSalesItem.getProposal().getCustomer();
		selectedSalesItem.getPlannedIsolation();
		selectedSalesItem.getChemicalRequirements();
		selectedSalesItem.getQualitySpec();

		selectedSalesItem.getBoyutsalKontrol();
		selectedSalesItem.getDestructiveTestsSpecs();
		selectedSalesItem.getNondestructiveTestsSpecs();

		Query internalQuery = man
				.createNamedQuery(
						"IsolationTestDefinition.findIsolationByItemIdAndIsolationType")
				.setParameter(1, IsolationType.IC_KAPLAMA)
				.setParameter(2, selectedSalesItem);
		internalIsolationTestDefinitions = internalQuery.getResultList();
		Query externalQuery = man
				.createNamedQuery(
						"IsolationTestDefinition.findIsolationByItemIdAndIsolationType")
				.setParameter(1, IsolationType.DIS_KAPLAMA)
				.setParameter(2, selectedSalesItem);
		externalIsolationTestDefinitions = externalQuery.getResultList();

		man.getTransaction().commit();
		man.close();
		if (selectedSalesItem.getQualitySpec() != null)
			qualitySpec = selectedSalesItem.getQualitySpec();

		if (selectedSalesItem.getBoyutsalKontrol() != null)
			boyutsalKontrol = selectedSalesItem.getBoyutsalKontrol();

		// boru boyutsal toleransları yukluyor
		BoruBoyutsalKontrolToleransManager boyutMan = new BoruBoyutsalKontrolToleransManager();
		if (boyutMan.seciliBoyutsalKontrol(selectedSalesItem.getItemId())
				.size() == 0) {
			boyutsalKontrol = new BoruBoyutsalKontrolTolerans();
		} else {
			boyutsalKontrol = boyutMan.seciliBoyutsalKontrol(
					selectedSalesItem.getItemId()).get(0);
		}

		for (PlannedIsolation iso : selectedSalesItem.getPlannedIsolation()) {
			if (iso.getInternalExternal().equals(
					IsolationType.IC_KAPLAMA.getType()))
				inIsolation = iso;
			if (iso.getInternalExternal().equals(
					IsolationType.DIS_KAPLAMA.getType()))
				outIsolation = iso;
		}

		// init the Boolean values for MENU part of qualityspecs.jsf page
		for (NondestructiveTestsSpec t : selectedSalesItem
				.getNondestructiveTestsSpecs())
			selectedTests.activate(t.getGlobalId());
		for (DestructiveTestsSpec t : selectedSalesItem
				.getDestructiveTestsSpecs())
			selectedTests.activate(t.getGlobalId());

		salesItemId = selectedSalesItem.getItemId();
	}

	@SuppressWarnings("static-access")
	public void clonner() {

		FacesContext context = FacesContext.getCurrentInstance();
		// sonraki itemi bulma
		List<SalesItem> cloneItems = new ArrayList<SalesItem>();
		SalesItem cloneItem = new SalesItem();

		try {
			for (int i = 0; i < items.size(); i++) {
				if (items.get(i).getProposal()
						.equals(selectedSalesItem.getProposal())) {
					cloneItems.add(items.get(i));
				}
			}

			if (cloneItems.size() > 0) {
				Collections.sort(cloneItems, new Comparator<SalesItem>() {
					@Override
					public int compare(final SalesItem object1,
							final SalesItem object2) {
						Integer itemNo1 = Integer.parseInt(object1.getItemNo());
						Integer itemNo2 = Integer.parseInt(object2.getItemNo());
						return itemNo1.compareTo(itemNo2);
					}
				});
			}
		} catch (Exception e) {
			System.out.println("SalesItemBean:" + e.toString());
		}

		for (int i = 0; i < cloneItems.size(); i++) {
			if (Integer.parseInt(cloneItems.get(i).getItemNo()) == Integer
					.parseInt(selectedSalesItem.getItemNo()) + 1) {
				cloneItem = cloneItems.get(i);
				context.addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_INFO,
								"KLONLANACAK, "
										+ cloneItem.getProposal().getCustomer()
												.getShortName() + " "
										+ cloneItem.getItemNo()
										+ ". SİPARİŞİDİR!", null));
			}
		}

		if (cloneItem.getItemId() == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL,
					"BAŞKA KLONLANACAK KALEM YOKTUR!", null));
			return;
		}

		// boyutsal Klonlama
		BoruBoyutsalKontrolTolerans cloneBoyutsal = new BoruBoyutsalKontrolTolerans();
		BoruBoyutsalKontrolToleransManager managerBoyutsal = new BoruBoyutsalKontrolToleransManager();
		if (managerBoyutsal.seciliBoyutsalKontrol(cloneItem.getItemId()).size() > 0) {
			try {
				cloneBoyutsal = (BoruBoyutsalKontrolTolerans) this.selectedSalesItem
						.getBoyutsalKontrol().clone();
				cloneBoyutsal.setId(null);
				cloneBoyutsal.setEklemeZamani(new java.sql.Timestamp(
						new java.util.Date().getTime()));
				cloneBoyutsal.setEkleyenKullanici(userBean.getUser().getId());
				cloneBoyutsal.setSalesItem(cloneItem);

				managerBoyutsal.enterNew(cloneBoyutsal);
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_WARN,
						"BORU BOYUTSAL KONROL TOLERANS KLONLANMIŞTIR!", null));
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
				FacesContextUtils.addPlainErrorMessage(e.getMessage());
			}
		}

		// nondes klonlama
		NondestructiveTestsSpecManager nonManager = new NondestructiveTestsSpecManager();
		List<NondestructiveTestsSpec> cloneNonDes = new ArrayList<NondestructiveTestsSpec>();
		if (nonManager.findByItemId(cloneItem.getItemId()).size() != nonManager
				.findByItemId(selectedSalesItem.getItemId()).size()) {
			try {
				cloneNonDes = nonManager.findByItemId(selectedSalesItem
						.getItemId());
				for (int i = 0; i < cloneNonDes.size(); i++) {

					cloneNonDes.get(i).setTestId(null);
					cloneNonDes.get(i).setSalesItem(cloneItem);
					cloneNonDes.get(i).setEklemeZamani(
							new java.sql.Timestamp(new java.util.Date()
									.getTime()));
					cloneNonDes.get(i).setEkleyenKullanici(
							userBean.getUser().getId());
					nonManager.enterNew(cloneNonDes.get(i));
				}
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_WARN,
						"TAHRİBATSIZ TESTLER KLONLANMIŞTIR!", null));
			} catch (Exception e) {
				e.printStackTrace();
				FacesContextUtils.addPlainErrorMessage(e.getMessage());
			}
		}

		// des klonlama
		DestructiveTestsSpecManager desManager = new DestructiveTestsSpecManager();
		List<DestructiveTestsSpec> cloneDes = new ArrayList<DestructiveTestsSpec>();
		if (desManager.findByItemId(cloneItem.getItemId()).size() != desManager
				.findByItemId(selectedSalesItem.getItemId()).size()) {
			try {
				cloneDes = desManager.findByItemId(selectedSalesItem
						.getItemId());
				for (int i = 0; i < cloneDes.size(); i++) {

					cloneDes.get(i).setTestId(null);
					cloneDes.get(i).setSalesItem(cloneItem);
					cloneDes.get(i).setEklemeZamani(
							new java.sql.Timestamp(new java.util.Date()
									.getTime()));
					cloneDes.get(i).setEkleyenKullanici(
							userBean.getUser().getId());
					cloneDes.get(i).setAgirlikTestsSpecs(null);
					cloneDes.get(i).setBukmeTestsSpecs(null);
					cloneDes.get(i).setCekmeTestsSpecs(null);
					cloneDes.get(i).setCentikDarbeTestsSpecs(null);
					cloneDes.get(i).setHasSonuc(false);
					cloneDes.get(i).setHasSpecs(false);
					cloneDes.get(i).setKaynakMakroTestSpecs(null);
					cloneDes.get(i).setKaynakSertlikTestsSpecs(null);
					cloneDes.get(i).setKimyasalTestsSpecs(null);
					desManager.enterNew(cloneDes.get(i));
				}
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_WARN,
						"TAHRİBATLI TESTLER KLONLANMIŞTIR!", null));
			} catch (Exception e) {
				e.printStackTrace();
				FacesContextUtils.addPlainErrorMessage(e.getMessage());
			}
		}

		// des spec - kimyasal Klonlama

		ChemicalRequirement cloneChem = new ChemicalRequirement();
		ChemicalRequirementManager managerChem = new ChemicalRequirementManager();
		if (managerChem.kontrol(selectedSalesItem.getItemId()) > 0
				&& managerChem.kontrol(cloneItem.getItemId()) == 0) {
			try {
				cloneChem = managerChem.seciliChemicalRequirementTanim(
						selectedSalesItem.getItemId()).get(0);
				cloneChem.setChemicalReqId(null);
				cloneChem.setSalesItem(cloneItem);
				managerChem.enterNew(cloneChem);

				hasSonuc(1012, cloneChem.getSalesItem().getItemId());

			} catch (Exception e) {
				e.printStackTrace();
				FacesContextUtils.addPlainErrorMessage(e.getMessage());
			}

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_WARN,
					"KİMYASAL TOLERANS KLONLANMIŞTIR!", null));
		}

		// des spec - Cekme Klonlama

		List<CekmeTestsSpec> cloneCekmes = new ArrayList<CekmeTestsSpec>();
		CekmeTestSpecManager managerCekme = new CekmeTestSpecManager();
		if (managerCekme.findByItemId(cloneItem.getItemId()).size() != managerCekme
				.findByItemId(selectedSalesItem.getItemId()).size()) {
			try {
				cloneCekmes = managerCekme.findByItemId(selectedSalesItem
						.getItemId());
				for (int i = 0; i < cloneCekmes.size(); i++) {
					cloneCekmes.get(i).setTestId(null);
					cloneCekmes.get(i).setSalesItem(cloneItem);
					managerCekme.enterNew(cloneCekmes.get(i));

					hasSonuc(cloneCekmes.get(i).getGlobalId(),
							cloneCekmes.get(i).getSalesItem().getItemId());
				}

				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_WARN,
						"ÇEKME TANIMLAR KLONLANMIŞTIR!", null));

			} catch (Exception e) {
				e.printStackTrace();
				FacesContextUtils.addPlainErrorMessage(e.getMessage());
			}
		}

		// des spec - Bukme Klonlama

		List<BukmeTestsSpec> cloneBukmes = new ArrayList<BukmeTestsSpec>();
		BukmeTestsSpecManager managerBukme = new BukmeTestsSpecManager();
		if (managerBukme.findByItemId(cloneItem.getItemId()).size() != managerBukme
				.findByItemId(selectedSalesItem.getItemId()).size()) {
			try {
				cloneBukmes = managerBukme.findByItemId(selectedSalesItem
						.getItemId());
				for (int i = 0; i < cloneBukmes.size(); i++) {
					cloneBukmes.get(i).setTestId(null);
					cloneBukmes.get(i).setSalesItem(cloneItem);
					managerBukme.enterNew(cloneBukmes.get(i));

					hasSonuc(cloneBukmes.get(i).getGlobalId(),
							cloneBukmes.get(i).getSalesItem().getItemId());
				}

				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_WARN,
						"BÜKME TANIMLAR KLONLANMIŞTIR!", null));

			} catch (Exception e) {
				e.printStackTrace();
				FacesContextUtils.addPlainErrorMessage(e.getMessage());
			}
		}

		// des spec - Centik Klonlama

		List<CentikDarbeTestsSpec> cloneCentiks = new ArrayList<CentikDarbeTestsSpec>();
		CentikDarbeTestsSpecManager managerCentik = new CentikDarbeTestsSpecManager();
		if (managerCentik.findByItemId(cloneItem.getItemId()).size() != managerCentik
				.findByItemId(selectedSalesItem.getItemId()).size()) {
			try {
				cloneCentiks = managerCentik.findByItemId(selectedSalesItem
						.getItemId());
				for (int i = 0; i < cloneCentiks.size(); i++) {
					cloneCentiks.get(i).setTestId(null);
					cloneCentiks.get(i).setSalesItem(cloneItem);
					managerCentik.enterNew(cloneCentiks.get(i));

					hasSonuc(cloneCentiks.get(i).getGlobalId(), cloneCentiks
							.get(i).getSalesItem().getItemId());
				}

				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_WARN,
						"ÇENTİK TANIMLAR KLONLANMIŞTIR!", null));

			} catch (Exception e) {
				e.printStackTrace();
				FacesContextUtils.addPlainErrorMessage(e.getMessage());
			}
		}

		// des spec - Agirlik Klonlama

		List<AgirlikTestsSpec> cloneAgirliks = new ArrayList<AgirlikTestsSpec>();
		AgirlikTestsSpecManager managerAgirlik = new AgirlikTestsSpecManager();
		if (managerAgirlik.findByItemId(cloneItem.getItemId()).size() != managerAgirlik
				.findByItemId(selectedSalesItem.getItemId()).size()) {
			try {
				cloneAgirliks = managerAgirlik.findByItemId(selectedSalesItem
						.getItemId());
				for (int i = 0; i < cloneAgirliks.size(); i++) {
					cloneAgirliks.get(i).setTestId(null);
					cloneAgirliks.get(i).setSalesItem(cloneItem);
					managerAgirlik.enterNew(cloneAgirliks.get(i));

					hasSonuc(cloneAgirliks.get(i).getGlobalId(), cloneAgirliks
							.get(i).getSalesItem().getItemId());
				}

				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_WARN,
						"AĞIRLIK TANIMLAR KLONLANMIŞTIR!", null));

			} catch (Exception e) {
				e.printStackTrace();
				FacesContextUtils.addPlainErrorMessage(e.getMessage());
			}
		}

		// des spec - Kaynak Makro Klonlama

		List<KaynakMakroTestSpec> cloneMakros = new ArrayList<KaynakMakroTestSpec>();
		KaynakMacroTestSpecManager managerMakro = new KaynakMacroTestSpecManager();
		if (managerMakro.findByItemId(cloneItem.getItemId()).size() != managerMakro
				.findByItemId(selectedSalesItem.getItemId()).size()) {
			try {
				cloneMakros = managerMakro.findByItemId(selectedSalesItem
						.getItemId());
				for (int i = 0; i < cloneMakros.size(); i++) {
					cloneMakros.get(i).setTestId(null);
					cloneMakros.get(i).setSalesItem(cloneItem);
					managerMakro.enterNew(cloneMakros.get(i));

					hasSonuc(cloneMakros.get(i).getGlobalId(),
							cloneMakros.get(i).getSalesItem().getItemId());
				}

				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_WARN,
						"KAYNAK MAKRO TANIMLAR KLONLANMIŞTIR!", null));

			} catch (Exception e) {
				e.printStackTrace();
				FacesContextUtils.addPlainErrorMessage(e.getMessage());
			}
		}

		// des spec - Kaynak Sertlik Klonlama

		List<KaynakSertlikTestsSpec> clonesSertliks = new ArrayList<KaynakSertlikTestsSpec>();
		KaynakSertlikTestSpecManager managerSertlik = new KaynakSertlikTestSpecManager();
		if (managerSertlik.findByItemId(cloneItem.getItemId()).size() != managerSertlik
				.findByItemId(selectedSalesItem.getItemId()).size()) {
			try {
				clonesSertliks = managerSertlik.findByItemId(selectedSalesItem
						.getItemId());
				for (int i = 0; i < clonesSertliks.size(); i++) {
					clonesSertliks.get(i).setTestId(null);
					clonesSertliks.get(i).setSalesItem(cloneItem);
					managerSertlik.enterNew(clonesSertliks.get(i));

					hasSonuc(clonesSertliks.get(i).getGlobalId(),
							clonesSertliks.get(i).getSalesItem().getItemId());
				}

				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_WARN,
						"KAYNAK MAKRO TANIMLAR KLONLANMIŞTIR!", null));

			} catch (Exception e) {
				e.printStackTrace();
				FacesContextUtils.addPlainErrorMessage(e.getMessage());
			}
		}
	}

	@SuppressWarnings("static-access")
	public void hasSonuc(Integer prmGlobalId, Integer prmSalesId) {
		DestructiveTestsSpec destructiveTestsSpec = new DestructiveTestsSpec();
		DestructiveTestsSpecManager desManager = new DestructiveTestsSpecManager();
		if (desManager.findByGlobalIdItemId(prmGlobalId, prmSalesId).size() > 0) {
			// bağlı desTest bulma
			destructiveTestsSpec = desManager.findByGlobalIdItemId(prmGlobalId,
					prmSalesId).get(0);
			destructiveTestsSpec.setHasSpecs(true);
			desManager.updateEntity(destructiveTestsSpec);
		}
	}

	public void loadTestOrders() {
		EntityManager entMan = Factory.getInstance().createEntityManager();
		selectedSalesCustomer = entMan.find(SalesCustomer.class,
				selectedSalesItem.getProposal().getCustomer()
						.getSalesCustomerId());
		entMan.refresh(selectedSalesCustomer);
		SalesItemManager itemMan = new SalesItemManager();
		items = itemMan.findAllOrdered(SalesItem.class, "itemNo");
	}

	// ALPER
	@SuppressWarnings("unchecked")
	public void loadIsolationTestDetail(
			IsolationTestDefinition isolationTestDefinition) {
		IsolationTestType isolationTestType = isolationTestDefinition
				.getTestKodu();
		// Class<?> isolationLoadClass = null;
		// dış kaplama icin
		if (isolationTestType == IsolationTestType.CBT1) {
			isolationLoadClass = BukmeIsolationTestsSpec.class;
		} else if (isolationTestType == IsolationTestType.CSC1) {
			isolationLoadClass = YuzeyKontrolTestsSpec.class;
		} else if (isolationTestType == IsolationTestType.CDS1) {
			isolationLoadClass = TozBoyaDSCTestsSpec.class;
		} else if (isolationTestType == IsolationTestType.CCT1) {
			isolationLoadClass = KaplamaKalinlikTestsSpec.class;
		} else if (isolationTestType == IsolationTestType.OHT1) {
			isolationLoadClass = HolidayTestsSpec.class;
		} else if (isolationTestType == IsolationTestType.CIT1) {
			isolationLoadClass = DarbeTestsSpec.class;
		} else if (isolationTestType == IsolationTestType.CAT1) {
			isolationLoadClass = YapismaTestsSpec.class;
		} else if (isolationTestType == IsolationTestType.CIT2) {
			isolationLoadClass = DeliciUcaKarsiDirencTestsSpec.class;
		} else if (isolationTestType == IsolationTestType.CET1) {
			isolationLoadClass = PolyolefinUzamaTestsSpec.class;
		} else if (isolationTestType == IsolationTestType.CCD1) {
			isolationLoadClass = KatodikSoyulmaTestsSpec.class;
		} else if (isolationTestType == IsolationTestType.CHT2) {
			isolationLoadClass = SertlikTestsSpec.class;
		}
		// ic kaplama icin

		else if (isolationTestType == IsolationTestType.LCT1) {
			isolationLoadClass = KaplamaKalinlikTestsSpec.class;
		} else if (isolationTestType == IsolationTestType.LSC1) {
			isolationLoadClass = YuzeyKontrolTestsSpec.class;
		} else if (isolationTestType == IsolationTestType.LSP1) {
			isolationLoadClass = YuzeyKontrolTestsSpec.class;
		} else if (isolationTestType == IsolationTestType.LBT1) {
			isolationLoadClass = BukmeIsolationTestsSpec.class;
		} else if (isolationTestType == IsolationTestType.LHT1) {
			isolationLoadClass = HolidayTestsSpec.class;
		} else if (isolationTestType == IsolationTestType.LIT1) {
			isolationLoadClass = DarbeTestsSpec.class;
		} else if (isolationTestType == IsolationTestType.LAT1) {
			isolationLoadClass = YapismaTestsSpec.class;
		} else if (isolationTestType == IsolationTestType.LIT2) {
			isolationLoadClass = DeliciUcaKarsiDirencTestsSpec.class;
		} else if (isolationTestType == IsolationTestType.LCD1) {
			isolationLoadClass = KatodikSoyulmaTestsSpec.class;
		} else if (isolationTestType == IsolationTestType.LHT2) {
			isolationLoadClass = SertlikTestsSpec.class;
		} else if (isolationTestType == IsolationTestType.BCB1) {
			isolationLoadClass = BetonEgmeTestsSpec.class;
		}
		EntityManager em = Factory.getInstance().createEntityManager();
		em.getTransaction().begin();

		// String sql = " select a from " + isolationLoadClass.getSimpleName()
		// + " a where a.order_id = " + selectedOrder.getOrderId()
		// + " and a.testtype = "
		// + isolationTestDefinition.getIsolationType().ordinal();

		String sql = " select * from " + isolationLoadClass.getSimpleName()
				+ " a where a.sales_item_id = " + selectedSalesItem.getItemId()
				+ " and a.testtype = "
				+ isolationTestDefinition.getIsolationType().ordinal();

		List<AbstractTestsSpec> tests = em.createNativeQuery(sql)
				.getResultList();

		if (tests.size() > 0) {

			isolationTestsDetail = (AbstractTestsSpec) tests.get(0);
		} else {
			try {
				isolationTestsDetail = (AbstractTestsSpec) isolationLoadClass
						.newInstance();
				isolationTestsDetail
						.setIsolationTestDefinition(isolationTestDefinition);
				System.out.println(isolationTestDefinition.getIsolationType()
						.ordinal());
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		em.getTransaction().commit();
		em.close();
	}

	// A.K. - tahribatl� ya da tahribats�z testlerde se�im yap�lm�s listeyi
	// refresh i�in kullan�l�yor.
	public void orderUnsellection() {
		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);
		testsBean.setSelectedDest(new DestructiveTestsSpec());
		testsBean.setSelectedNondest(new NondestructiveTestsSpec());
		loadRequiredTests();
	}

	public void submitInLayer() {

		layer.setIsolation(inIsolation);
		EntityManager man = Factory.getInstance().createEntityManager();
		man.getTransaction().begin();
		inIsolation = man.find(PlannedIsolation.class,
				inIsolation.getIsolationId());
		inIsolation.getLayers().add(layer);
		man.getTransaction().commit();
		man.close();
		layer = new Layer();
		FacesContextUtils.addInfoMessage("TestSubmitMessage");
	}

	public void submitOutLayer() {

		layer.setIsolation(outIsolation);
		EntityManager man = Factory.getInstance().createEntityManager();
		man.getTransaction().begin();
		outIsolation = man.find(PlannedIsolation.class,
				outIsolation.getIsolationId());
		outIsolation.getLayers().add(layer);
		man.getTransaction().commit();
		man.close();
		layer = new Layer();
		FacesContextUtils.addInfoMessage("TestSubmitMessage");
	}

	public void updateInIsolation() {

		PlannedIsolationManager man = new PlannedIsolationManager();
		man.updateEntity(inIsolation);
		FacesContextUtils.addInfoMessage("TestUpdateMessage");
	}

	public void updateOutIsolation() {

		PlannedIsolationManager man = new PlannedIsolationManager();
		man.updateEntity(outIsolation);
		FacesContextUtils.addInfoMessage("TestUpdateMessage");
	}

	public void deleteLayer(Layer layer) {

		LayerManager man = new LayerManager();
		man.delete(layer);
		EntityManager isoMan = Factory.getInstance().createEntityManager();
		if (inIsolation.getLayers() != null) {

			inIsolation.getLayers().remove(layer);
			isoMan.getTransaction().begin();
			inIsolation = isoMan.find(PlannedIsolation.class,
					inIsolation.getIsolationId());
			isoMan.refresh(inIsolation);
			isoMan.clear();
		}
		if (outIsolation.getLayers() != null) {

			outIsolation.getLayers().remove(layer);
			isoMan.getTransaction().begin();
			outIsolation = isoMan.find(PlannedIsolation.class,
					outIsolation.getIsolationId());
			isoMan.refresh(outIsolation);
			isoMan.clear();
		}
		isoMan.close();
		FacesContextUtils.addWarnMessage("TestDeleteMessage");
	}

	// ********************************************************************* //
	// METHODS RELATED WITH QUALITY SPECIFICATION //
	// ********************************************************************* //
	// update it or submit new if there isnt any in the database

	public void submitQualitySpec() {

		QualitySpecManager man = new QualitySpecManager();
		// if we have on then just update it, otherwise enter a new record
		if (selectedSalesItem.getQualitySpec() != null
				&& selectedSalesItem.getQualitySpec().getQualitySpecId() > 0) {

			qualitySpec.setSalesItem(selectedSalesItem);
			man.updateEntity(qualitySpec);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"KALİTE TANIMLARI BAŞARI İLE GÜNCELLENMİŞTİR!"));
		} else {
			QualitySpecManager qMan = new QualitySpecManager();
			qualitySpec.setSalesItem(selectedSalesItem);
			selectedSalesItem.setQualitySpec(qualitySpec);
			qMan.enterNew(selectedSalesItem.getQualitySpec());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"KALİTE TANIMLARI BAŞARI İLE EKLENMİŞTİR!"));
		}

	}

	// public void submitBoyutsalKontrol() {
	//
	// BoruBoyutsalKontrolToleransManager man = new
	// BoruBoyutsalKontrolToleransManager();
	// // if we have on then just update it, otherwise enter a new record
	// if (selectedOrder.getBoyutsalKontrol() != null
	// && selectedOrder.getBoyutsalKontrol().getId() > 0) {
	//
	// boyutsalKontrol.setOrder(selectedOrder);
	// boyutsalKontrol.setGuncellemeZamani(new java.sql.Timestamp(
	// new java.util.Date().getTime()));
	// boyutsalKontrol.setGuncelleyenKullanici(userBean.getUser().getId());
	// man.updateEntity(boyutsalKontrol);
	// FacesContextUtils.addInfoMessage("TestUpdateMessage");
	// } else {
	// boyutsalKontrol.setOrder(selectedOrder);
	// selectedOrder.setBoyutsalKontrol(boyutsalKontrol);
	// boyutsalKontrol.setEklemeZamani(new java.sql.Timestamp(
	// new java.util.Date().getTime()));
	// boyutsalKontrol.setEkleyenKullanici(userBean.getUser().getId());
	// man.enterNew(selectedOrder.getBoyutsalKontrol());
	// FacesContextUtils.addInfoMessage("TestSubmitMessage");
	// }
	//
	// }

	public void submitBoyutsalKontrol() {

		BoruBoyutsalKontrolToleransManager man = new BoruBoyutsalKontrolToleransManager();
		// if we have on then just update it, otherwise enter a new record
		if (selectedSalesItem.getBoyutsalKontrol() != null
				&& selectedSalesItem.getBoyutsalKontrol().getId() > 0) {

			boyutsalKontrol.setSalesItem(selectedSalesItem);
			boyutsalKontrol.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			boyutsalKontrol.setGuncelleyenKullanici(userBean.getUser().getId());
			man.updateEntity(boyutsalKontrol);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"BORU BOYUTSAL BİLGİLERİ BAŞARI İLE GÜNCELLENMİŞTİR!"));
		} else {

			boyutsalKontrol.setSalesItem(selectedSalesItem);
			selectedSalesItem.setBoyutsalKontrol(boyutsalKontrol);
			boyutsalKontrol.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			boyutsalKontrol.setEkleyenKullanici(userBean.getUser().getId());
			man.enterNew(selectedSalesItem.getBoyutsalKontrol());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"BORU BOYUTSAL BİLGİLERİ BAŞARI İLE KAYDEDİLMİŞTİR!"));
		}

	}

	public void deleteQualitySpec() {

		if (selectedSalesItem.getQualitySpec() == null
				|| selectedSalesItem.getQualitySpec().getQualitySpecId() <= 0) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		QualitySpecManager man = new QualitySpecManager();
		man.delete(selectedSalesItem.getQualitySpec());
		selectedSalesItem.setQualitySpec(new QualitySpec());
		FacesContextUtils.addWarnMessage("TestDeleteMessage");
	}

	public void deleteBoyutsalKontrol() {

		if (selectedSalesItem.getBoyutsalKontrol() == null
				|| selectedSalesItem.getBoyutsalKontrol().getId() <= 0) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		BoruBoyutsalKontrolToleransManager man = new BoruBoyutsalKontrolToleransManager();
		man.delete(selectedSalesItem.getBoyutsalKontrol());
		selectedSalesItem.setBoyutsalKontrol(new BoruBoyutsalKontrolTolerans());
		FacesContextUtils.addWarnMessage("TestDeleteMessage");
	}

	// load Destructive and Nondestructive tests entered for the order
	public void loadRequiredTests() {

		SalesItemManager salesManager = new SalesItemManager();

		// EntityManager man = Factory.getInstance().createEntityManager();
		// man.getTransaction().begin();
		selectedSalesItem = salesManager.loadObject(SalesItem.class,
				salesItemId);
		selectedSalesItem.getDestructiveTestsSpecs();
		selectedSalesItem.getNondestructiveTestsSpecs();

		// boru kaplama türleri basılıyor
		if (selectedSalesItem.getBoruKaliteKaplamaTurleri() == null) {

			boruKaliteKaplamaTurleri = new BoruKaliteKaplamaTurleri();
		} else {
			boruKaliteKaplamaTurleri = selectedSalesItem
					.getBoruKaliteKaplamaTurleri();
		}

		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);
		testsBean.setDestTest(new DestructiveTestsSpec());
		if (testsBean.getSelectedDest() == null) {
			// man.getTransaction().commit();
			// man.close();
		} else {
			testsBean.setDestTest(testsBean.getSelectedDest());
			testsBean.getDestTest();
			// man.getTransaction().commit();
			// man.close();
		}
	}

	// A.K. - Seçilen taribatlı teste tanımlanmıs specs var mı?
	public boolean hasSpecs(Integer globalId) {
		if (globalId >= 1013 && globalId <= 1017) {
			CekmeTestSpecManager cekMan = new CekmeTestSpecManager();
			if (cekMan.kontrol(globalId, selectedSalesItem.getItemId()) > 0) {
				return true;
			}

		} else if (globalId >= 1018 && globalId <= 1023) {
			BukmeTestsSpecManager bukMan = new BukmeTestsSpecManager();
			if (bukMan.kontrol(globalId, selectedSalesItem.getItemId()) > 0) {
				return true;
			}

		} else if ((globalId >= 1024 && globalId <= 1028) || globalId == 1040
				|| (globalId >= 1033 && globalId <= 1037)) {
			CentikDarbeTestsSpecManager cenMan = new CentikDarbeTestsSpecManager();
			if (cenMan.kontrol(globalId, selectedSalesItem.getItemId()) > 0) {
				return true;
			}

		} else if (globalId >= 1029) {
			AgirlikTestsSpecManager agirMan = new AgirlikTestsSpecManager();
			if (agirMan.kontrol(globalId, selectedSalesItem.getItemId()) > 0) {
				return true;
			}

		}
		return false;
	}

	public void addNondestructives() {

		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);
		// A.K. - xhtmlden name ve code alinamadigi icin boyle yapildi
		testsBean.getSelectedNondest().setCompleted(true);
		for (int i = 0; i < testsBean.getNondestructiveTests().size(); i++) {
			if (testsBean.getNondestructiveTests().get(i).getGlobalId()
					.equals(testsBean.getSelectedNondest().getGlobalId())) {
				testsBean.getSelectedNondest().setName(
						testsBean.getNondestructiveTests().get(i).getName());
				testsBean.getSelectedNondest().setCode(
						testsBean.getNondestructiveTests().get(i).getCode());
			}
		}
		EntityManager man = Factory.getInstance().createEntityManager();
		NondestructiveTestsSpecManager nondesMan = new NondestructiveTestsSpecManager();
		man.getTransaction().begin();

		selectedSalesItem = man.find(SalesItem.class,
				selectedSalesItem.getItemId());

		NondestructiveTestsSpec t = testsBean.getSelectedNondest();

		if (t.getCompleted()) {
			t.setSalesItem(selectedSalesItem);
			if (!selectedSalesItem.getNondestructiveTestsSpecs().contains(t)) {
				// we need to update the BOOLEAN values of displaying the
				// test
				// in the menu panel
				selectedTests.activate(t.getGlobalId());
				selectedSalesItem.getNondestructiveTestsSpecs().add(t);
			} else {
				selectedSalesItem.getNondestructiveTestsSpecs().remove(t);
				selectedSalesItem.getNondestructiveTestsSpecs().add(t);
			}
		}

		if (t.getTestId() != null) {

			// A.K. - Guncelleme zamanı ve guncelleyen kullanıcı
			t.setGuncellemeZamani(UtilInsCore.getTarihZaman());
			t.setGuncelleyenKullanici(userBean.getUser().getId());
			nondesMan.updateEntity(t);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		// A.K. - Eklenme zamanı ve ekleyen kullanıcı
		t.setEklemeZamani(UtilInsCore.getTarihZaman());
		t.setEkleyenKullanici(userBean.getUser().getId());
		man.getTransaction().commit();
		FacesContextUtils.addInfoMessage("TestSubmitMessage");
	}

	public void deleteNondestructives() {

		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);

		CommonQueries<NondestructiveTestsSpec> man = new CommonQueries<NondestructiveTestsSpec>();
		if (testsBean.getSelectedNondest() == null
				|| testsBean.getSelectedNondest().getTestId() == 0) {

			FacesContextUtils.addWarnMessage("NoSelectMessage");
			return;
		}
		EntityManager salesMan = Factory.getInstance().createEntityManager();

		selectedSalesItem = salesMan.find(SalesItem.class,
				selectedSalesItem.getItemId());
		salesMan.refresh(selectedSalesItem);

		salesMan.close();
		man.delete(testsBean.getSelectedNondest());
		// set the boolean value for the qualitySpec.jsf page
		selectedTests.deactivate(testsBean.getSelectedNondest().getGlobalId());
		testsBean.setSelectedNondest(new NondestructiveTestsSpec());
		FacesContextUtils.addWarnMessage("TestDeleteMessage");
	}

	public void addDestructives() {

		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);

		// A.K. - xhtmlden name ve code al�namad�g� i�in boyle yap�ld�
		testsBean.getDestTest().setCompleted(true);
		testsBean.getDestTest().setHasSonuc(false);
		for (int i = 0; i < testsBean.getDestructiveTests().size(); i++) {
			if (testsBean.getDestructiveTests().get(i).getGlobalId()
					.equals(testsBean.getDestTest().getGlobalId())) {
				testsBean.getDestTest().setName(
						testsBean.getDestructiveTests().get(i).getName());
				testsBean.getDestTest().setCode(
						testsBean.getDestructiveTests().get(i).getCode());
			}
		}
		DestructiveTestsSpecManager destMan = new DestructiveTestsSpecManager();
		EntityManager man = Factory.getInstance().createEntityManager();
		man.getTransaction().begin();

		selectedSalesItem = man.find(SalesItem.class,
				selectedSalesItem.getItemId());

		DestructiveTestsSpec t = testsBean.getDestTest();

		if (t.getCompleted()) {
			t.setSalesItem(selectedSalesItem);
			if (!selectedSalesItem.getDestructiveTestsSpecs().contains(t)) {

				selectedTests.activate(t.getGlobalId());
				selectedSalesItem.getDestructiveTestsSpecs().add(t);
			} else {
				selectedSalesItem.getDestructiveTestsSpecs().remove(t);
				selectedSalesItem.getDestructiveTestsSpecs().add(t);
			}
		}

		if (t.getTestId() != null) {

			// A.K. - Guncelleme zamanı ve guncelleyen kullanıcı
			t.setGuncellemeZamani(UtilInsCore.getTarihZaman());
			t.setGuncelleyenKullanici(userBean.getUser().getId());

			destMan.updateEntity(t);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		t.setHasSpecs(false);
		// A.K. - Eklenme zamanı ve ekleyen kullanıcı
		t.setEklemeZamani(UtilInsCore.getTarihZaman());
		t.setEkleyenKullanici(userBean.getUser().getId());

		man.getTransaction().commit();
		FacesContextUtils.addInfoMessage("TestSubmitMessage");
	}

	public void deleteDestructives() {

		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);

		CommonQueries<DestructiveTestsSpec> man = new CommonQueries<DestructiveTestsSpec>();
		if (testsBean.getSelectedDest() == null
				|| testsBean.getSelectedDest().getTestId() == 0) {

			FacesContextUtils.addWarnMessage("NoSelectMessage");
			return;
		}

		if (testsBean.getSelectedDest().getGlobalId() == 1013
				|| testsBean.getSelectedDest().getGlobalId() == 1014
				|| testsBean.getSelectedDest().getGlobalId() == 1015
				|| testsBean.getSelectedDest().getGlobalId() == 1016
				|| testsBean.getSelectedDest().getGlobalId() == 1017) {
			CekmeTestSpecManager ctsMan = new CekmeTestSpecManager();
			Integer count = ctsMan.kontrol(testsBean.getSelectedDest()
					.getGlobalId(), testsBean.getSelectedDest().getSalesItem()
					.getItemId());
			if (count > 0) {
				deleteTensile();
			}
		}

		// Bükme testleri silinirken Specs de tanımlama kontrolu 1018 ile 1023
		// arası testler icin
		if (testsBean.getSelectedDest().getGlobalId() == 1018
				|| testsBean.getSelectedDest().getGlobalId() == 1019
				|| testsBean.getSelectedDest().getGlobalId() == 1020
				|| testsBean.getSelectedDest().getGlobalId() == 1021
				|| testsBean.getSelectedDest().getGlobalId() == 1022
				|| testsBean.getSelectedDest().getGlobalId() == 1023) {
			BukmeTestsSpecManager btsMan = new BukmeTestsSpecManager();
			Integer count = btsMan.kontrol(testsBean.getSelectedDest()
					.getGlobalId(), testsBean.getSelectedDest().getSalesItem()
					.getItemId());
			if (count > 0) {
				deleteBukme();
			}
		}

		if (testsBean.getSelectedDest().getGlobalId() == 1024
				|| testsBean.getSelectedDest().getGlobalId() == 1025
				|| testsBean.getSelectedDest().getGlobalId() == 1026
				|| testsBean.getSelectedDest().getGlobalId() == 1027
				|| testsBean.getSelectedDest().getGlobalId() == 1028
				|| testsBean.getSelectedDest().getGlobalId() == 1033
				|| testsBean.getSelectedDest().getGlobalId() == 1034
				|| testsBean.getSelectedDest().getGlobalId() == 1035
				|| testsBean.getSelectedDest().getGlobalId() == 1036
				|| testsBean.getSelectedDest().getGlobalId() == 1037
				|| testsBean.getSelectedDest().getGlobalId() == 1040) {
			CentikDarbeTestsSpecManager cdtsMan = new CentikDarbeTestsSpecManager();
			Integer count = cdtsMan.kontrol(testsBean.getSelectedDest()
					.getGlobalId(), testsBean.getSelectedDest().getSalesItem()
					.getItemId());
			if (count > 0) {
				deleteCentik();
			}
		}

		if (testsBean.getSelectedDest().getGlobalId() == 1029
				|| testsBean.getSelectedDest().getGlobalId() == 1030) {
			AgirlikTestsSpecManager atsMan = new AgirlikTestsSpecManager();
			Integer count = atsMan.kontrol(testsBean.getSelectedDest()
					.getGlobalId(), testsBean.getSelectedDest().getSalesItem()
					.getItemId());
			if (count > 0) {
				deleteAgirlik(testsBean.getSelectedDest().getSalesItem()
						.getItemId());
			}
		}

		if (testsBean.getSelectedDest().getGlobalId() == 1031) {
			KaynakMacroTestSpecManager kmtsMan = new KaynakMacroTestSpecManager();
			Integer count = kmtsMan.silmeKontrolu(testsBean.getSelectedDest()
					.getGlobalId(), testsBean.getSelectedDest().getSalesItem()
					.getItemId());
			if (count > 0) {
				deleteKaynakMacro();
			}
		}

		if (testsBean.getSelectedDest().getGlobalId() == 1032) {
			KaynakSertlikTestSpecManager kstsMan = new KaynakSertlikTestSpecManager();
			Integer count = kstsMan.kontrol(testsBean.getSelectedDest()
					.getGlobalId(), testsBean.getSelectedDest().getSalesItem()
					.getItemId());
			if (count > 0) {
				deleteKaynakSertlik();
			}
		}

		man.delete(testsBean.getSelectedDest());

		EntityManager orderMan = Factory.getInstance().createEntityManager();
		selectedSalesItem = orderMan.find(SalesItem.class,
				selectedSalesItem.getItemId());
		orderMan.refresh(selectedSalesItem);
		orderMan.close();

		selectedTests.deactivate(testsBean.getSelectedDest().getGlobalId());
		testsBean.setSelectedDest(new DestructiveTestsSpec());
		FacesContextUtils.addWarnMessage("TestDeleteMessage");
	}

	// **************** CHEMICAL REQUIREMENTS METHODS
	// *************************//
	public void prepareChemReq(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		String test = (String) cmd.getChildren().get(0).getAttributes()
				.get("value");

		EntityManager man = Factory.getInstance().createEntityManager();
		selectedSalesItem = man.find(SalesItem.class,
				selectedSalesItem.getItemId());
		selectedSalesItem.getChemicalRequirements();
		man.close();
		if (selectedSalesItem.getChemicalRequirements() == null
				|| selectedSalesItem.getChemicalRequirements()
						.getChemicalReqId() == null) {

			selectedSalesItem
					.setChemicalRequirements(new ChemicalRequirement());
			selectedSalesItem.getChemicalRequirements().setSalesItem(
					selectedSalesItem);
		}

		uploadRender(test);
	}

	public void submitChemReq() {

		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);
		CommonQueries<DestructiveTestsSpec> destMan = new CommonQueries<DestructiveTestsSpec>();
		ChemicalRequirementManager man = new ChemicalRequirementManager();
		if (selectedSalesItem.getChemicalRequirements().getChemicalReqId() != null
				&& selectedSalesItem.getChemicalRequirements()
						.getChemicalReqId() > 0) {

			man.updateEntity(selectedSalesItem.getChemicalRequirements());
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		} else {
			selectedSalesItem.getChemicalRequirements().setSalesItem(
					selectedSalesItem);
			man.enterNew(selectedSalesItem.getChemicalRequirements());
			// A.K. - tanım eklenirken destructive testte hasSpecs true
			// yapılıyor
			testsBean.getSelectedDest().setHasSpecs(true);
			destMan.updateEntity(testsBean.getSelectedDest());
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		}
		EntityManager oMan = Factory.getInstance().createEntityManager();
		selectedSalesItem = oMan.find(SalesItem.class,
				selectedSalesItem.getItemId());
		oMan.refresh(selectedSalesItem);
		oMan.close();
	}

	public void deleteChemReq() {

		ChemicalRequirementManager man = new ChemicalRequirementManager();
		man.delete(selectedSalesItem.getChemicalRequirements());
		CommonQueries<DestructiveTestsSpec> destMan = new CommonQueries<DestructiveTestsSpec>();
		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);
		testsBean.getSelectedDest().setHasSpecs(false);
		destMan.updateEntity(testsBean.getSelectedDest());
		selectedSalesItem.setChemicalRequirements(new ChemicalRequirement());
		EntityManager oMan = Factory.getInstance().createEntityManager();
		selectedSalesItem = oMan.find(SalesItem.class,
				selectedSalesItem.getItemId());
		oMan.refresh(selectedSalesItem);
		oMan.close();
		FacesContextUtils.addWarnMessage("TestDeleteMessage");
	}

	// ********************** TENSILE TEST METHODS *************************//
	// The type of tensile Test is the same GlobalId we use in the config XML
	// file. The related ids are: 1013, 1014, 1015, 1016, 1017. This method
	// takes the GlobalId as an argument and sets the respective tensileTest
	// in the TestsBean
	public void prepareTensileTest(int type) {

		EntityManager man = Factory.getInstance().createEntityManager();
		man.getTransaction().begin();
		selectedSalesItem = man.find(SalesItem.class,
				selectedSalesItem.getItemId());
		selectedSalesItem.getCekmeTestsSpecs();
		man.getTransaction().commit();
		man.close();

		// set the appropriate test in the TestsBean
		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);
		testsBean.setCekmeVisibleFields(type);
		for (CekmeTestsSpec t : selectedSalesItem.getCekmeTestsSpecs()) {

			if (t.getGlobalId().equals(type)) {

				testsBean.setCekmeVisibleFields(type);
				testsBean.setCekme(t);
				return;
			}
		}
		Test test = config.getConfig().getPipe().findTestByGlobalId(type);
		testsBean.setCekme(new CekmeTestsSpec(test));
		testsBean.getCekme();
	}

	public void addTensile() {

		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);
		CekmeTestSpecManager man = new CekmeTestSpecManager();
		CommonQueries<DestructiveTestsSpec> destMan = new CommonQueries<DestructiveTestsSpec>();

		if (man.kontrol(testsBean.getSelectedDest().getGlobalId(),
				selectedSalesItem.getItemId()) > 0) {
			testsBean.getCekme().setSalesItem(selectedSalesItem);
			testsBean.getCekme().setTestId(
					man.kontrol(testsBean.getSelectedDest().getGlobalId(),
							selectedSalesItem.getItemId()));
			man.updateEntity(testsBean.getCekme());
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		} else {
			testsBean.getCekme().setSalesItem(selectedSalesItem);
			man.enterNew(testsBean.getCekme());
			// A.K. - tanım eklenirken destructive testte hasSpecs true
			// yapılıyor
			testsBean.getSelectedDest().setHasSpecs(true);
			testsBean.getSelectedDest()
					.setCekmeTestsSpecs(testsBean.getCekme());
			destMan.updateEntity(testsBean.getSelectedDest());
			loadRequiredTests();
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		}
	}

	public void deleteTensile() {

		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);
		CekmeTestSpecManager man = new CekmeTestSpecManager();
		CommonQueries<DestructiveTestsSpec> destMan = new CommonQueries<DestructiveTestsSpec>();

		if (testsBean.getCekme() != null
				|| testsBean.getCekme().getTestId() > 0) {

			man.delete(testsBean.getCekme());
			testsBean.getCekme().reset();
			// A.K. - tan�m eklen�rken destructive testte hasSpecs false
			// yapiliyor
			testsBean.getSelectedDest().setHasSpecs(false);
			destMan.updateEntity(testsBean.getSelectedDest());
			loadRequiredTests();
			FacesContextUtils.addWarnMessage("TestDeleteMessage");
		} else {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
		}
	}

	// ********************** BUKME TEST METHODS *************************//
	public void prepareBukmeTests(Integer globalId) {

		BukmeTestsSpecManager bukMan = new BukmeTestsSpecManager();
		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);
		// veritaban�nda kayitli bukme specs var mi?
		if (bukMan
				.seciliBukmeTestTanim(globalId, selectedSalesItem.getItemId())
				.size() == 0) {
			testsBean.getBukme().reset();
		} else {
			testsBean.setBukme(bukMan.seciliBukmeTestTanim(globalId,
					selectedSalesItem.getItemId()).get(0));
			testsBean.getBukme();
		}
	}

	public void addBukme() {

		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);
		EntityManager oMan = Factory.getInstance().createEntityManager();
		selectedSalesItem = oMan.find(SalesItem.class,
				selectedSalesItem.getItemId());
		CommonQueries<BukmeTestsSpec> man = new CommonQueries<BukmeTestsSpec>();
		CommonQueries<DestructiveTestsSpec> destMan = new CommonQueries<DestructiveTestsSpec>();
		BukmeTestsSpecManager bukMan = new BukmeTestsSpecManager();
		// update if already exists in the database
		// databasede kay�tl� test var m� kontrolu
		testsBean.getBukme().setGlobalId(
				testsBean.getSelectedDest().getGlobalId());
		testsBean.getBukme().setCode(testsBean.getSelectedDest().getCode());
		testsBean.getBukme().setCompleted(true);
		testsBean.getBukme().setName(testsBean.getSelectedDest().getName());

		// yeni istek, acı sabit 180 olacak
		testsBean.getBukme().setBukmeAcisi((float) 180);

		if (bukMan.kontrol(testsBean.getBukme().getGlobalId(),
				selectedSalesItem.getItemId()) > 0) {
			testsBean.getBukme().setTestId(
					bukMan.kontrol(testsBean.getBukme().getGlobalId(),
							selectedSalesItem.getItemId()));
			man.updateEntity(testsBean.getBukme());
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		} else {
			testsBean.getBukme().setSalesItem(selectedSalesItem);
			man.enterNew(testsBean.getBukme());
			// A.K. - tanim eklenirken destructive testte hasSpecs true
			// yapiliyor
			testsBean.getSelectedDest().setHasSpecs(true);
			testsBean.getSelectedDest()
					.setBukmeTestsSpecs(testsBean.getBukme());
			destMan.updateEntity(testsBean.getSelectedDest());
		}

		oMan.refresh(selectedSalesItem);
		oMan.close();
		testsBean.getBukme().reset();
		testsBean.getSelectedDest().reset();
		FacesContextUtils.addInfoMessage("TestSubmitMessage");
	}

	public void deleteBukme() {

		BukmeTestsSpecManager man = new BukmeTestsSpecManager();
		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);
		List<BukmeTestsSpec> deleteMan = man
				.seciliBukmeTestTanim(
						testsBean.getSelectedDest().getGlobalId(),
						selectedSalesItem.getItemId());
		CommonQueries<DestructiveTestsSpec> destMan = new CommonQueries<DestructiveTestsSpec>();
		if (deleteMan.size() == 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
		} else {
			man.delete(deleteMan.get(0));
			deleteMan.get(0).reset();
			testsBean.setBukme(deleteMan.get(0));
			// tanim silinirken destructive testte hasSpecs false yapiliyor
			testsBean.getSelectedDest().setHasSpecs(false);
			destMan.updateEntity(testsBean.getSelectedDest());
			FacesContextUtils.addWarnMessage("TestDeleteMessage");
		}
	}

	public void prepareCentikTests(Integer globalId) {

		CentikDarbeTestsSpecManager centikMan = new CentikDarbeTestsSpecManager();
		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);

		List<CentikDarbeTestsSpec> seciliTest = centikMan
				.seciliCentikTestTanim(globalId, selectedSalesItem.getItemId());

		if (seciliTest.size() == 0) {
			testsBean.setCentik(new CentikDarbeTestsSpec());
			// testsBean.getCentik().reset();
		} else {
			testsBean.setCentik(seciliTest.get(0));
			testsBean.getCentik();
		}
	}

	public void addCentik() {

		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);

		EntityManager oMan = Factory.getInstance().createEntityManager();
		selectedSalesItem = oMan.find(SalesItem.class,
				selectedSalesItem.getItemId());
		testsBean.getCentik().setCompleted(true);
		testsBean.getCentik().setGlobalId(
				testsBean.getSelectedDest().getGlobalId());
		testsBean.getCentik().setCode(testsBean.getSelectedDest().getCode());
		testsBean.getCentik().setCompleted(true);
		testsBean.getCentik().setName(testsBean.getSelectedDest().getName());
		CommonQueries<CentikDarbeTestsSpec> man = new CommonQueries<CentikDarbeTestsSpec>();
		CommonQueries<DestructiveTestsSpec> destMan = new CommonQueries<DestructiveTestsSpec>();
		CentikDarbeTestsSpecManager cekMan = new CentikDarbeTestsSpecManager();
		// update if already exists in the database
		if (cekMan.kontrol(testsBean.getCentik().getGlobalId(),
				selectedSalesItem.getItemId()) > 0) {
			testsBean.getCentik().setSalesItem(selectedSalesItem);
			testsBean.getCentik().setTestId(
					cekMan.kontrol(testsBean.getCentik().getGlobalId(),
							selectedSalesItem.getItemId()));
			man.updateEntity(testsBean.getCentik());
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		} else {
			testsBean.getCentik().setSalesItem(selectedSalesItem);
			man.enterNew(testsBean.getCentik());
			// tanim eklen�rken destructive testte hasSpecs true yapiliyor
			testsBean.getSelectedDest().setHasSpecs(true);
			testsBean.getSelectedDest().setCentikDarbeTestsSpecs(
					testsBean.getCentik());
			destMan.updateEntity(testsBean.getSelectedDest());
		}
		oMan.refresh(selectedSalesItem);
		oMan.close();
		testsBean.setCentik(new CentikDarbeTestsSpec());
		FacesContextUtils.addInfoMessage("TestSubmitMessage");
	}

	public void deleteCentik() {

		CentikDarbeTestsSpecManager man = new CentikDarbeTestsSpecManager();
		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);
		List<CentikDarbeTestsSpec> delete = man
				.seciliCentikTestTanim(testsBean.getSelectedDest()
						.getGlobalId(), selectedSalesItem.getItemId());
		CommonQueries<DestructiveTestsSpec> destMan = new CommonQueries<DestructiveTestsSpec>();
		if (delete.size() == 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
		} else {
			man.delete(delete.get(0));
			testsBean.setCentik(new CentikDarbeTestsSpec());
			// tanim silinirken destructive testde hasSpecs false yapiliyor
			testsBean.getSelectedDest().setHasSpecs(false);
			destMan.updateEntity(testsBean.getSelectedDest());
			FacesContextUtils.addWarnMessage("TestDeleteMessage");
		}
	}

	public void prepareAgirlikTests(Integer globalId) {

		AgirlikTestsSpecManager agirMan = new AgirlikTestsSpecManager();
		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);

		List<AgirlikTestsSpec> seciliTest = agirMan.seciliAgirlikTestTanim(
				globalId, selectedSalesItem.getItemId());

		if (seciliTest.size() == 0) {
			testsBean.getAgirlik().reset();
		} else {
			testsBean.setAgirlik(seciliTest.get(0));
			testsBean.getAgirlik();
		}
	}

	public void addAgirlik() {

		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);

		EntityManager oMan = Factory.getInstance().createEntityManager();
		selectedSalesItem = oMan.find(SalesItem.class,
				selectedSalesItem.getItemId());
		testsBean.getAgirlik().setCompleted(true);
		testsBean.getAgirlik().setGlobalId(
				testsBean.getSelectedDest().getGlobalId());
		testsBean.getAgirlik().setName(testsBean.getSelectedDest().getName());
		CommonQueries<AgirlikTestsSpec> man = new CommonQueries<AgirlikTestsSpec>();
		CommonQueries<DestructiveTestsSpec> destMan = new CommonQueries<DestructiveTestsSpec>();
		// update if already exists in the database
		if (testsBean.getAgirlik().getTestId() != null
				&& testsBean.getAgirlik().getTestId() > 0) {
			man.updateEntity(testsBean.getAgirlik());
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		} else {

			testsBean.getAgirlik().setSalesItem(selectedSalesItem);
			man.enterNew(testsBean.getAgirlik());
			// A.K. - tanim eklenirken destructive testte hasSpecs true
			// yapiliyor
			testsBean.getSelectedDest().setHasSpecs(true);
			testsBean.getSelectedDest().setAgirlikTestsSpecs(
					testsBean.getAgirlik());
			destMan.updateEntity(testsBean.getSelectedDest());
		}
		oMan.refresh(selectedSalesItem);
		oMan.close();
		testsBean.getAgirlik().reset();
		FacesContextUtils.addInfoMessage("TestSubmitMessage");
	}

	public void deleteAgirlik(Integer globalId) {

		AgirlikTestsSpecManager man = new AgirlikTestsSpecManager();
		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);

		globalId = testsBean.getSelectedDest().getGlobalId();

		List<AgirlikTestsSpec> delete = man.seciliAgirlikTestTanim(globalId,
				selectedSalesItem.getItemId());
		CommonQueries<DestructiveTestsSpec> destMan = new CommonQueries<DestructiveTestsSpec>();
		if (delete.size() == 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
		} else {
			man.delete(delete.get(0));
			delete.get(0).reset();
			testsBean.setAgirlikTest(delete.get(0));
			// tanim silinirken destructive testde hasSpecs false yapiliyor
			testsBean.getSelectedDest().setHasSpecs(false);
			destMan.updateEntity(testsBean.getSelectedDest());
			FacesContextUtils.addWarnMessage("TestDeleteMessage");
		}
	}

	public void prepareKaynakMacro() {

		KaynakMacroTestSpecManager kaynakMan = new KaynakMacroTestSpecManager();
		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);

		List<KaynakMakroTestSpec> seciliTest = kaynakMan
				.seciliKaynakMakroTestTanim(1031, selectedSalesItem.getItemId());

		if (seciliTest.size() == 0) {
			testsBean.getKaynakMacroTest().reset();
		} else {
			testsBean.setKaynakMacroTest(seciliTest.get(0));
			testsBean.getKaynakMacroTest();
		}
	}

	public void addKaynakMacro() {

		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);
		testsBean.getKaynakMacroTest().setCode(
				testsBean.getSelectedDest().getCode());
		testsBean.getKaynakMacroTest().setName(
				testsBean.getSelectedDest().getName());
		testsBean.getKaynakMacroTest().setGlobalId(
				testsBean.getSelectedDest().getGlobalId());
		KaynakMacroTestSpecManager man = new KaynakMacroTestSpecManager();
		CommonQueries<DestructiveTestsSpec> destMan = new CommonQueries<DestructiveTestsSpec>();
		if (testsBean.getKaynakMacroTest().getTestId() != null
				&& testsBean.getKaynakMacroTest().getTestId() > 0) {

			man.updateEntity(testsBean.getKaynakMacroTest());
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		} else {

			testsBean.getKaynakMacroTest().setSalesItem(selectedSalesItem);
			man.enterNew(testsBean.getKaynakMacroTest());
			// tanim eklenirken destructive testte hasSpecs true yapiliyor
			testsBean.getSelectedDest().setHasSpecs(true);
			testsBean.getSelectedDest().setKaynakMakroTestSpecs(
					testsBean.getKaynakMacroTest());
			destMan.updateEntity(testsBean.getSelectedDest());
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		}
	}

	public void deleteKaynakMacro() {

		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);
		KaynakMacroTestSpecManager man = new KaynakMacroTestSpecManager();
		List<KaynakMakroTestSpec> delete = man.seciliKaynakMakroTestTanim(
				testsBean.getSelectedDest().getGlobalId(),
				selectedSalesItem.getItemId());
		CommonQueries<DestructiveTestsSpec> destMan = new CommonQueries<DestructiveTestsSpec>();
		if (delete.size() == 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
		} else {
			man.delete(delete.get(0));
			delete.get(0).reset();
			testsBean.setKaynakMacroTest(delete.get(0));
			// tanim silinirken destructive testde hasSpecs false yapiliyor
			testsBean.getSelectedDest().setHasSpecs(false);
			destMan.updateEntity(testsBean.getSelectedDest());
			FacesContextUtils.addWarnMessage("TestDeleteMessage");
		}
	}

	public void prepareKaynakSertlik() {

		KaynakSertlikTestSpecManager kaynakMan = new KaynakSertlikTestSpecManager();
		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);

		List<KaynakSertlikTestsSpec> seciliTest = kaynakMan
				.seciliKaynakSertlikTestTanim(1032,
						selectedSalesItem.getItemId());

		if (seciliTest.size() == 0) {
			testsBean.getKaynakSertlikTest().reset();
		} else {
			testsBean.setKaynakSertlikTest(seciliTest.get(0));
			testsBean.getKaynakSertlikTest();
		}
	}

	public void addKaynakSertlik() {

		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);
		KaynakSertlikTestSpecManager man = new KaynakSertlikTestSpecManager();

		// A.K. completed=false
		testsBean.getKaynakSertlikTest().setCompleted(false);
		testsBean.getKaynakSertlikTest().setCode(
				testsBean.getSelectedDest().getCode());
		testsBean.getKaynakSertlikTest().setName(
				testsBean.getSelectedDest().getName());
		testsBean.getKaynakSertlikTest().setGlobalId(
				testsBean.getSelectedDest().getGlobalId());
		CommonQueries<DestructiveTestsSpec> destMan = new CommonQueries<DestructiveTestsSpec>();
		if (testsBean.getKaynakSertlikTest().getTestId() != null
				&& testsBean.getKaynakSertlikTest().getTestId() > 0) {

			man.updateEntity(testsBean.getKaynakSertlikTest());
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		} else {

			testsBean.getKaynakSertlikTest().setSalesItem(selectedSalesItem);
			man.enterNew(testsBean.getKaynakSertlikTest());
			// tan�m eklen�rken destructive testte hasSpecs true yap�l�yor
			testsBean.getSelectedDest().setHasSpecs(true);
			testsBean.getSelectedDest().setKaynakSertlikTestsSpecs(
					testsBean.getKaynakSertlikTest());
			destMan.updateEntity(testsBean.getSelectedDest());
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		}
	}

	public void deleteKaynakSertlik() {

		TestsBean testsBean = (TestsBean) FacesContextUtils.getViewBean(
				"testsBean", TestsBean.class);
		KaynakSertlikTestSpecManager man = new KaynakSertlikTestSpecManager();
		List<KaynakSertlikTestsSpec> delete = man.seciliKaynakSertlikTestTanim(
				testsBean.getSelectedDest().getGlobalId(),
				selectedSalesItem.getItemId());
		CommonQueries<DestructiveTestsSpec> destMan = new CommonQueries<DestructiveTestsSpec>();
		if (delete.size() == 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
		} else {
			man.delete(delete.get(0));
			delete.get(0).reset();
			testsBean.setKaynakSertlikTest(delete.get(0));
			// tanim silinirken destructive testde hasSpecs false yapiliyor
			testsBean.getSelectedDest().setHasSpecs(false);
			destMan.updateEntity(testsBean.getSelectedDest());
			FacesContextUtils.addWarnMessage("TestDeleteMessage");
		}
	}

	public void saveExternalIsolationTestDefinition() {

		EntityManager man = Factory.getInstance().createEntityManager();
		man.getTransaction().begin();
		isolationTestDefinition.setSalesItem(selectedSalesItem);
		isolationTestDefinition.setIsolationType(IsolationType.DIS_KAPLAMA);
		isolationTestDefinition.setTestAdi(isolationTestDefinition
				.getTestKodu().getLabel());
		isolationTestDefinition.setHasDetay(false);
		isolationTestDefinition.setHasSonuc(false);
		isolationTestDefinition.setGlobalId(isolationTestDefinition
				.getTestKodu().getGlobalId());
		man.persist(isolationTestDefinition);
		man.getTransaction().commit();
		man.close();
		FacesContextUtils.addInfoMessage("SubmitMessage");
		// dis kaplama test listesini tekrar getirme
		externalIsolationTestDefinitions = QualitySpecManager
				.getAllExternalIsolationTestDefinitions(selectedSalesItem);

	}

	public void deleteExternalIsolationTestDefinition() {

		if (isolationTestDefinition == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		CommonQueries<IsolationTestDefinition> man = new CommonQueries<IsolationTestDefinition>();
		man.delete(isolationTestDefinition);
		FacesContextUtils.addInfoMessage("TestDeleteMessage");

		// dis kaplama test listesini tekrar getirme
		externalIsolationTestDefinitions = QualitySpecManager
				.getAllExternalIsolationTestDefinitions(selectedSalesItem);
	}

	public void saveUpdateInternalIsolationTestDefinition() {

		EntityManager man = Factory.getInstance().createEntityManager();
		man.getTransaction().begin();
		isolationTestDefinition.setSalesItem(selectedSalesItem);
		isolationTestDefinition.setIsolationType(IsolationType.IC_KAPLAMA);
		isolationTestDefinition.setTestAdi(isolationTestDefinition
				.getTestKodu().getLabel());
		isolationTestDefinition.setHasDetay(false);
		isolationTestDefinition.setHasSonuc(false);
		isolationTestDefinition.setGlobalId(isolationTestDefinition
				.getTestKodu().getGlobalId());
		if (isolationTestDefinition.getId() == null) {
			man.persist(isolationTestDefinition);
			man.getTransaction().commit();
			man.close();

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"İÇ KAPLAMA BAŞARIYLA EKLENMİŞTİR!"));
		} else {
			man.merge(isolationTestDefinition);
			man.getTransaction().commit();
			man.close();

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"İÇ KAPLAMA BAŞARIYLA GÜNCELLENMİŞTİR!"));
		}

		// ic kaplama test listesini tekrar getirme
		internalIsolationTestDefinitions = QualitySpecManager
				.getAllInternalIsolationTestDefinitions(selectedSalesItem);
	}

	public void saveInternalIsolationTestDefinition() {
		/*
		 * EntityManager man = Factory.getInstance().createEntityManager();
		 * man.getTransaction().begin();
		 * isolationTestDefinition.setOrder(selectedOrder);
		 * isolationTestDefinition.setIsolationType(IsolationType.IC_KAPLAMA);
		 * isolationTestDefinition.setTestAdi(isolationTestDefinition
		 * .getTestKodu().getLabel());
		 * isolationTestDefinition.setHasDetay(false);
		 * isolationTestDefinition.setHasSonuc(false);
		 * isolationTestDefinition.setGlobalId(isolationTestDefinition
		 * .getTestKodu().getGlobalId()); man.persist(isolationTestDefinition);
		 * man.getTransaction().commit(); man.close();
		 * FacesContextUtils.addInfoMessage("SubmitMessage");
		 * 
		 * // i� kaplama test listesini tekrar getirme
		 * internalIsolationTestDefinitions = QualitySpecManager
		 * .getAllInternalIsolationTestDefinitions(selectedOrder);
		 */

		EntityManager man = Factory.getInstance().createEntityManager();
		man.getTransaction().begin();
		isolationTestDefinition.setSalesItem(selectedSalesItem);
		isolationTestDefinition.setIsolationType(IsolationType.IC_KAPLAMA);
		isolationTestDefinition.setTestAdi(isolationTestDefinition
				.getTestKodu().getLabel());
		isolationTestDefinition.setHasDetay(false);
		isolationTestDefinition.setHasSonuc(false);
		isolationTestDefinition.setGlobalId(isolationTestDefinition
				.getTestKodu().getGlobalId());
		man.persist(isolationTestDefinition);
		man.getTransaction().commit();
		man.close();
		FacesContextUtils.addInfoMessage("SubmitMessage");

		// ic kaplama test listesini tekrar getirme
		internalIsolationTestDefinitions = QualitySpecManager
				.getAllInternalIsolationTestDefinitions(selectedSalesItem);
	}

	public void deleteInternalIsolationTestDefinition() {

		/*
		 * if (isolationTestDefinition == null) {
		 * 
		 * FacesContextUtils.addErrorMessage("NoSelectMessage"); return; }
		 * 
		 * CommonQueries<IsolationTestDefinition> man = new
		 * CommonQueries<IsolationTestDefinition>();
		 * man.delete(isolationTestDefinition);
		 * FacesContextUtils.addInfoMessage("TestDeleteMessage");
		 * 
		 * // i� kaplama test listesini tekrar getirme
		 * internalIsolationTestDefinitions = QualitySpecManager
		 * .getAllInternalIsolationTestDefinitions(selectedOrder);
		 */

		// enteg
		if (isolationTestDefinition == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		CommonQueries<IsolationTestDefinition> man = new CommonQueries<IsolationTestDefinition>();
		man.delete(isolationTestDefinition);
		FacesContextUtils.addInfoMessage("TestDeleteMessage");

		// i� kaplama test listesini tekrar getirme
		internalIsolationTestDefinitions = QualitySpecManager
				.getAllInternalIsolationTestDefinitions(selectedSalesItem);
		// enteg
	}

	public void createNewIsolationTestDefinition() {
		isolationTestDefinition = new IsolationTestDefinition();
	}

	public IsolationTestType[] getIsolationTestTypeCode() {
		return IsolationTestType.values();
	}

	public void prepareHydrostatic() {

		/*
		 * EntityManager man = Factory.getInstance().createEntityManager(); //
		 * selectedOrder = man.find(Order.class, selectedOrder.getOrderId());
		 * selectedOrder = man.find(Order.class, orderId);
		 * selectedOrder.getHidrostaticTestsSpecs(); man.close(); if
		 * (selectedOrder.getHidrostaticTestsSpecs() == null ||
		 * selectedOrder.getHidrostaticTestsSpecs().size() == 0) {
		 * 
		 * Test t = config.getConfig().getPipe()
		 * .findTestByGlobalId(HidrostaticTestsSpec.GLOBAL_ID_1); selectedHidro
		 * = new HidrostaticTestsSpec(t); return; selectedHidro =
		 * selectedOrder.getHidrostaticTestsSpecs().get(0);
		 */

		// enteg
		EntityManager man = Factory.getInstance().createEntityManager();
		selectedSalesItem = man.find(SalesItem.class, salesItemId);
		selectedSalesItem.getHidrostaticTestsSpecs();
		man.close();
		if (selectedSalesItem.getHidrostaticTestsSpecs() == null
				|| selectedSalesItem.getHidrostaticTestsSpecs().size() == 0) {

			Test t = config.getConfig().getPipe()
					.findTestByGlobalId(HidrostaticTestsSpec.GLOBAL_ID_1);
			selectedHidro = new HidrostaticTestsSpec(t);
			return;
			// enteg
		}

		selectedHidro = selectedSalesItem.getHidrostaticTestsSpecs().get(0);
	}

	public void addHydrostatic() {

		/*
		 * EntityManager man = Factory.getInstance().createEntityManager();
		 * man.getTransaction().begin(); selectedOrder = man.find(Order.class,
		 * selectedOrder.getOrderId()); HidrostaticTestsSpecManager hidMan = new
		 * HidrostaticTestsSpecManager(); int count = hidMan.kontrol(1011,
		 * selectedOrder.getOrderId());
		 * 
		 * // A.K. - kay�t yoksa ekleme, varsa guncelleme
		 * selectedHidro.setOrder(selectedOrder);
		 * selectedHidro.setOrderId(selectedOrder.getOrderId());
		 * selectedHidro.setCompleted(false);
		 * selectedOrder.getHidrostaticTestsSpecs().add(selectedHidro); if
		 * (count > 0) { updateInHydrostatic(); } man.getTransaction().commit();
		 * man.close(); FacesContextUtils.addInfoMessage("TestSubmitMessage");
		 */

		// enteg
		EntityManager man = Factory.getInstance().createEntityManager();
		man.getTransaction().begin();
		selectedSalesItem = man.find(SalesItem.class,
				selectedSalesItem.getItemId());
		HidrostaticTestsSpecManager hidMan = new HidrostaticTestsSpecManager();
		int count = hidMan.kontrol(1011, selectedSalesItem.getItemId());

		// A.K. - kay�t yoksa ekleme, varsa guncelleme
		selectedHidro.setSalesItem(selectedSalesItem);
		selectedHidro.setSalesItemId(selectedSalesItem.getItemId());
		selectedHidro.setCompleted(false);
		selectedSalesItem.getHidrostaticTestsSpecs().add(selectedHidro);
		if (count > 0) {

			HidrostaticTestsSpecManager hidroMan = new HidrostaticTestsSpecManager();
			hidroMan.updateEntity(selectedHidro);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}
		man.getTransaction().commit();
		man.close();
		FacesContextUtils.addInfoMessage("TestSubmitMessage");
		// enteg
	}

	public void deleteHydrostatic() {

		/*
		 * if (selectedOrder.getHidrostaticTestsSpecs().size() == 0) {
		 * 
		 * FacesContextUtils.addErrorMessage("NoSelectMessage"); return; }
		 * 
		 * CommonQueries<HidrostaticTestsSpec> man = new
		 * CommonQueries<HidrostaticTestsSpec>(); man.delete(selectedHidro);
		 * Test t = config.getConfig().getPipe()
		 * .findTestByGlobalId(HidrostaticTestsSpec.GLOBAL_ID_1); selectedHidro
		 * = new HidrostaticTestsSpec(t);
		 * 
		 * EntityManager m = Factory.getInstance().createEntityManager();
		 * selectedOrder = m.find(Order.class, selectedOrder.getOrderId());
		 * m.refresh(selectedOrder);
		 * FacesContextUtils.addInfoMessage("TestDeleteMessage");
		 */
		if (selectedSalesItem.getHidrostaticTestsSpecs().size() == 0) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		CommonQueries<HidrostaticTestsSpec> man = new CommonQueries<HidrostaticTestsSpec>();
		man.delete(selectedHidro);
		Test t = config.getConfig().getPipe()
				.findTestByGlobalId(HidrostaticTestsSpec.GLOBAL_ID_1);
		selectedHidro = new HidrostaticTestsSpec(t);

		EntityManager m = Factory.getInstance().createEntityManager();
		selectedSalesItem = m.find(SalesItem.class,
				selectedSalesItem.getItemId());
		m.refresh(selectedSalesItem);
		FacesContextUtils.addInfoMessage("TestDeleteMessage");

	}

	// A.K. - Secilen testlerde tan�m eklenme kontrolu
	public boolean kontrol(Integer salesItemId, Integer globalId) {
		if (globalId == 1012) {
			ChemicalRequirementManager chemMan = new ChemicalRequirementManager();
			List<ChemicalRequirement> chemList = chemMan
					.seciliChemicalRequirementTanim(salesItemId);
			if (chemList != null & chemList.size() > 0) {
				return true;
			} else
				return false;
		} else if (globalId == 0) {
			BoruBoyutsalKontrolToleransManager qualMan = new BoruBoyutsalKontrolToleransManager();
			List<BoruBoyutsalKontrolTolerans> qualList = qualMan
					.seciliBoyutsalKontrol(salesItemId);
			if (qualList != null & qualList.size() > 0) {
				return true;
			} else
				return false;
		} else
			return false;
	}

	public void producedPipes() {
		// load the pipes that were produced so far for the selected order
		PipeManager man = new PipeManager();
		if (selectedSalesItem != null) {
			producedPipes = man.findByItemId(selectedSalesItem.getItemId());
		} else {
			producedPipes = man.findAllOrdered(Pipe.class, "pipeId");
		}

		bandEkiBul();
	}

	public void tahribatliTestYapilmisBoruListesiGetir() {
		// load the pipes that were produced so far for the selected order
		PipeManager man = new PipeManager();
		if (selectedSalesItem != null) {
			tahribatliTestYapilmisBorular = man
					.findByItemIdAnDDestructiveTest(selectedSalesItem
							.getItemId());
			// indirect list sorunu yuzunden
			for (Integer i = 0; i < tahribatliTestYapilmisBorular.size(); i++) {
				tahribatliTestYapilmisBorular.get(i).getTestBukmeSonuclar()
						.size();
				tahribatliTestYapilmisBorular.get(i).getTestCekmeSonuclar()
						.size();
				tahribatliTestYapilmisBorular.get(i)
						.getTestKimyasalAnalizSonuclar().size();
				tahribatliTestYapilmisBorular.get(i)
						.getTestAgirlikDusurmeSonuclar().size();
				tahribatliTestYapilmisBorular.get(i).getTestKaynakSonuclar()
						.size();
				tahribatliTestYapilmisBorular.get(i)
						.getTestCentikDarbeSonuclar().size();
			}
		} else {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_FATAL,
							"SİPARİŞ KALEMİ SEÇİLMEMİŞ, LÜTFEN SİPARİŞ KALEMİ SEÇİNİZ!",
							null));
		}
	}

	public class TestObject {
		public String boruNo = null;
		public String testKodu = null;
		public String testAdi = null;
		public String tarih = null;

		/**
		 * @return the boruNo
		 */
		public String getBoruNo() {
			return boruNo;
		}

		/**
		 * @param boruNo
		 *            the boruNo to set
		 */
		public void setBoruNo(String boruNo) {
			this.boruNo = boruNo;
		}

		/**
		 * @return the testKodu
		 */
		public String getTestKodu() {
			return testKodu;
		}

		/**
		 * @param testKodu
		 *            the testKodu to set
		 */
		public void setTestKodu(String testKodu) {
			this.testKodu = testKodu;
		}

		/**
		 * @return the tarih
		 */
		public String getTarih() {
			return tarih;
		}

		/**
		 * @param tarih
		 *            the tarih to set
		 */
		public void setTarih(String tarih) {
			this.tarih = tarih;
		}

		/**
		 * @return the testAdi
		 */
		public String getTestAdi() {
			return testAdi;
		}

		/**
		 * @param testAdi
		 *            the testAdi to set
		 */
		public void setTestAdi(String testAdi) {
			this.testAdi = testAdi;
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void testTarihleriRaporu(SalesItem salesItem) {

		try {

			List<String> date = new ArrayList<String>();
			List<TestObject> testObjects = new ArrayList<TestObject>();

			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			for (Integer i = 0; i < tahribatliTestYapilmisBorular.size(); i++) {

				for (int j = 0; j < tahribatliTestYapilmisBorular.get(i)
						.getTestBukmeSonuclar().size(); j++) {
					try {
						TestObject testObject = new TestObject();
						testObject.setBoruNo(tahribatliTestYapilmisBorular.get(
								i).getPipeBarkodNo());
						testObject.setTestKodu(tahribatliTestYapilmisBorular
								.get(i).getTestBukmeSonuclar().get(j)
								.getBagliTestId().getCode());
						testObject.setTestAdi(tahribatliTestYapilmisBorular
								.get(i).getTestBukmeSonuclar().get(j)
								.getBagliTestId().getName());
						testObject.setTarih(new SimpleDateFormat("dd.MM.yyyy")
								.format(tahribatliTestYapilmisBorular.get(i)
										.getTestBukmeSonuclar().get(j)
										.getTestTarihi()));
						testObjects.add(testObject);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				for (int j = 0; j < tahribatliTestYapilmisBorular.get(i)
						.getTestCekmeSonuclar().size(); j++) {
					try {
						TestObject testObject = new TestObject();
						testObject.setBoruNo(tahribatliTestYapilmisBorular.get(
								i).getPipeBarkodNo());
						testObject.setTestKodu(tahribatliTestYapilmisBorular
								.get(i).getTestCekmeSonuclar().get(j)
								.getBagliTestId().getCode());
						testObject.setTestAdi(tahribatliTestYapilmisBorular
								.get(i).getTestCekmeSonuclar().get(j)
								.getBagliTestId().getName());
						testObject.setTarih(new SimpleDateFormat("dd.MM.yyyy")
								.format(tahribatliTestYapilmisBorular.get(i)
										.getTestCekmeSonuclar().get(j)
										.getTestTarihi()));
						testObjects.add(testObject);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				for (int j = 0; j < tahribatliTestYapilmisBorular.get(i)
						.getTestKimyasalAnalizSonuclar().size(); j++) {
					try {
						TestObject testObject = new TestObject();
						testObject.setBoruNo(tahribatliTestYapilmisBorular.get(
								i).getPipeBarkodNo());
						testObject.setTestKodu(tahribatliTestYapilmisBorular
								.get(i).getTestKimyasalAnalizSonuclar().get(j)
								.getBagliTestId().getCode());
						testObject.setTestAdi(tahribatliTestYapilmisBorular
								.get(i).getTestKimyasalAnalizSonuclar().get(j)
								.getBagliTestId().getName());
						testObject.setTarih(new SimpleDateFormat("dd.MM.yyyy")
								.format(tahribatliTestYapilmisBorular.get(i)
										.getTestKimyasalAnalizSonuclar().get(j)
										.getTestTarihi()));
						testObjects.add(testObject);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				for (int j = 0; j < tahribatliTestYapilmisBorular.get(i)
						.getTestAgirlikDusurmeSonuclar().size(); j++) {
					try {
						TestObject testObject = new TestObject();
						testObject.setBoruNo(tahribatliTestYapilmisBorular.get(
								i).getPipeBarkodNo());
						testObject.setTestKodu(tahribatliTestYapilmisBorular
								.get(i).getTestAgirlikDusurmeSonuclar().get(j)
								.getBagliTestId().getCode());
						testObject.setTestAdi(tahribatliTestYapilmisBorular
								.get(i).getTestAgirlikDusurmeSonuclar().get(j)
								.getBagliTestId().getName());
						testObject.setTarih(new SimpleDateFormat("dd.MM.yyyy")
								.format(tahribatliTestYapilmisBorular.get(i)
										.getTestAgirlikDusurmeSonuclar().get(j)
										.getTestTarihi()));
						testObjects.add(testObject);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				for (int j = 0; j < tahribatliTestYapilmisBorular.get(i)
						.getTestKaynakSonuclar().size(); j++) {
					try {
						TestObject testObject = new TestObject();
						testObject.setBoruNo(tahribatliTestYapilmisBorular.get(
								i).getPipeBarkodNo());
						testObject.setTestKodu(tahribatliTestYapilmisBorular
								.get(i).getTestKaynakSonuclar().get(j)
								.getBagliTestId().getCode());
						testObject.setTestAdi(tahribatliTestYapilmisBorular
								.get(i).getTestKaynakSonuclar().get(j)
								.getBagliTestId().getName());
						testObject.setTarih(new SimpleDateFormat("dd.MM.yyyy")
								.format(tahribatliTestYapilmisBorular.get(i)
										.getTestKaynakSonuclar().get(j)
										.getTestTarihi()));
						testObjects.add(testObject);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				for (int j = 0; j < tahribatliTestYapilmisBorular.get(i)
						.getTestCentikDarbeSonuclar().size(); j++) {
					try {
						TestObject testObject = new TestObject();
						testObject.setBoruNo(tahribatliTestYapilmisBorular.get(
								i).getPipeBarkodNo());
						testObject.setTestKodu(tahribatliTestYapilmisBorular
								.get(i).getTestCentikDarbeSonuclar().get(j)
								.getBagliTestId().getCode());
						testObject.setTestAdi(tahribatliTestYapilmisBorular
								.get(i).getTestCentikDarbeSonuclar().get(j)
								.getBagliTestId().getName());
						testObject.setTarih(new SimpleDateFormat("dd.MM.yyyy")
								.format(tahribatliTestYapilmisBorular.get(i)
										.getTestCentikDarbeSonuclar().get(j)
										.getTestTarihi()));
						testObjects.add(testObject);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

			Map dataMap = new HashMap();
			dataMap.put("sonuc", testObjects);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/testTarihleriRaporu.xls",
					tahribatliTestYapilmisBorular.get(0).getSalesItem()
							.getProposal().getCustomer().getShortName()
							+ "-"
							+ tahribatliTestYapilmisBorular.get(0)
									.getSalesItem().getItemNo());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		falseAllRenders();
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!"));
			return;
		} else {
			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			selectedSalesItem = selectedPipe.getSalesItem();
			// producedPipes =
			// pipeMan.findByItemId(selectedSalesItem.getItemId());
			// producedPipes();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
		}

		bandEkiBul();
	}

	public void bandEkiBul() {

		PipeManager pipeMan = new PipeManager();
		// band eki varsa band eki rulosu bulunuyor
		if (selectedPipe == null) {
			return;
		}

		try {
			if (selectedPipe.getBandEki()) {
				bandEkiPipe = pipeMan.findBandEkli(selectedPipe.getPipeIndex(),
						selectedPipe.getSalesItem().getItemId()).get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL, "BORU BAND EKİ BULUNAMADI!",
					null));
			return;
		}

	}

	public void nonTestResultListener(SelectEvent event) {
		System.out.println(selectedNonTest.getGlobalId());
		// tahribats�z testler
		// otomatik Ut kaynak
		if (selectedNonTest.getGlobalId() == 1001) {
			falseAllRenders();
			testRendersBean.setTestOtomatikUtKaynak(true);
			testTahribatsizOtomatikUtSonucBean.fillTestList(selectedPipe
					.getPipeId());
		}// otomatik Ut laminasyon
		else if (selectedNonTest.getGlobalId() == 1002) {
			falseAllRenders();
			testRendersBean.setTestOtomatikUtKaynak(true);
			testTahribatsizOtomatikUtSonucBean.fillTestList(selectedPipe
					.getPipeId());
		}
	}

	// ALPER
	public void testResultListener(SelectEvent event) {
		if (selectedTest.getGlobalId() != null) {// tahribatl� testler ise
			// kimyasal
			if (selectedTest.getGlobalId() == 1012) {
				falseAllRenders();
				testRendersBean.setTestKimyasalRender(true);
				testKimyasalAnalizSonucBean.fillTestList(1012,
						selectedPipe.getPipeId());
			}
			// cekme
			else if (selectedTest.getGlobalId() == 1013) {
				falseAllRenders();
				testRendersBean.setTestZ01Render(true);
				testRendersBean.setTestCekmeRender(true);
				testCekmeSonucBean.fillTestList(1013, selectedPipe.getPipeId());
			} else if (selectedTest.getGlobalId() == 1014) {
				falseAllRenders();
				testRendersBean.setTestZ02Z03Z07Render(true);
				testRendersBean.setTestZ02Render(true);
				testRendersBean.setTestCekmeRender(true);
				testCekmeSonucBean.fillTestList(1014, selectedPipe.getPipeId());
			} else if (selectedTest.getGlobalId() == 1015) {
				falseAllRenders();
				testRendersBean.setTestCekmeRender(true);
				testRendersBean.setTestZ02Z03Z07Render(true);
				testRendersBean.setTestZ03Render(true);
				testCekmeSonucBean.fillTestList(1015, selectedPipe.getPipeId());
			} else if (selectedTest.getGlobalId() == 1016) {
				falseAllRenders();
				testRendersBean.setTestCekmeRender(true);
				testRendersBean.setTestZ06Render(true);
				testCekmeSonucBean.fillTestList(1016, selectedPipe.getPipeId());
			} else if (selectedTest.getGlobalId() == 1017) {
				falseAllRenders();
				testRendersBean.setTestCekmeRender(true);
				testRendersBean.setTestZ02Z03Z07Render(true);
				testRendersBean.setTestZ07Render(true);
				testCekmeSonucBean.fillTestList(1017, selectedPipe.getPipeId());
			}
			// bukme
			else if (selectedTest.getGlobalId() == 1018) {
				falseAllRenders();
				testRendersBean.setTestBukmeRender(true);
				testRendersBean.setTestF01Render(true);
				testBukmeSonucBean.fillTestList(1018, selectedPipe.getPipeId());
			} else if (selectedTest.getGlobalId() == 1019) {
				falseAllRenders();
				testRendersBean.setTestF02Render(true);
				testRendersBean.setTestBukmeRender(true);
				testBukmeSonucBean.fillTestList(1019, selectedPipe.getPipeId());
			} else if (selectedTest.getGlobalId() == 1020) {
				falseAllRenders();
				testRendersBean.setTestBukmeRender(true);
				testRendersBean.setTestF03Render(true);
				testBukmeSonucBean.fillTestList(1020, selectedPipe.getPipeId());
			} else if (selectedTest.getGlobalId() == 1021) {
				falseAllRenders();
				testRendersBean.setTestBukmeRender(true);
				testRendersBean.setTestF04Render(true);
				testBukmeSonucBean.fillTestList(1021, selectedPipe.getPipeId());
			} else if (selectedTest.getGlobalId() == 1022) {
				falseAllRenders();
				testRendersBean.setTestBukmeRender(true);
				testRendersBean.setTestF05Render(true);
				testBukmeSonucBean.fillTestList(1022, selectedPipe.getPipeId());
			} else if (selectedTest.getGlobalId() == 1023) {
				falseAllRenders();
				testRendersBean.setTestBukmeRender(true);
				testRendersBean.setTestF06Render(true);
				testBukmeSonucBean.fillTestList(1023, selectedPipe.getPipeId());
			}
			// centik
			else if (selectedTest.getGlobalId() == 1024) {
				falseAllRenders();
				testRendersBean.setTestCentikDarbeRender(true);
				testRendersBean.setTestK01Render(true);
				testCentikDarbeSonucBean.fillTestList(1024,
						selectedPipe.getPipeId());
			} else if (selectedTest.getGlobalId() == 1025) {
				falseAllRenders();
				testRendersBean.setTestCentikDarbeRender(true);
				testRendersBean.setTestK02Render(true);
				testCentikDarbeSonucBean.fillTestList(1025,
						selectedPipe.getPipeId());
			} else if (selectedTest.getGlobalId() == 1026) {
				falseAllRenders();
				testRendersBean.setTestCentikDarbeRender(true);
				testRendersBean.setTestK03Render(true);
				testCentikDarbeSonucBean.fillTestList(1026,
						selectedPipe.getPipeId());
			} else if (selectedTest.getGlobalId() == 1027) {
				falseAllRenders();
				testRendersBean.setTestCentikDarbeRender(true);
				testRendersBean.setTestK04Render(true);
				testCentikDarbeSonucBean.fillTestList(1027,
						selectedPipe.getPipeId());
			} else if (selectedTest.getGlobalId() == 1028) {
				falseAllRenders();
				testRendersBean.setTestCentikDarbeRender(true);
				testRendersBean.setTestK05Render(true);
				testCentikDarbeSonucBean.fillTestList(1028,
						selectedPipe.getPipeId());
			} else if (selectedTest.getGlobalId() == 1033) {
				falseAllRenders();
				testRendersBean.setTestCentikDarbeRender(true);
				testRendersBean.setTestU01Render(true);
				testCentikDarbeSonucBean.fillTestList(1033,
						selectedPipe.getPipeId());
			} else if (selectedTest.getGlobalId() == 1034) {
				falseAllRenders();
				testRendersBean.setTestCentikDarbeRender(true);
				testRendersBean.setTestU02Render(true);
				testCentikDarbeSonucBean.fillTestList(1034,
						selectedPipe.getPipeId());
			} else if (selectedTest.getGlobalId() == 1035) {
				falseAllRenders();
				testRendersBean.setTestCentikDarbeRender(true);
				testRendersBean.setTestU03Render(true);
				testCentikDarbeSonucBean.fillTestList(1035,
						selectedPipe.getPipeId());
			} else if (selectedTest.getGlobalId() == 1036) {
				falseAllRenders();
				testRendersBean.setTestCentikDarbeRender(true);
				testRendersBean.setTestU04Render(true);
				testCentikDarbeSonucBean.fillTestList(1036,
						selectedPipe.getPipeId());
			} else if (selectedTest.getGlobalId() == 1037) {
				falseAllRenders();
				testRendersBean.setTestCentikDarbeRender(true);
				testRendersBean.setTestU05Render(true);
				testCentikDarbeSonucBean.fillTestList(1035,
						selectedPipe.getPipeId());
			}
			// agirlik
			else if (selectedTest.getGlobalId() == 1029) {
				falseAllRenders();
				testRendersBean.setTestAgirlikRender(true);
				testRendersBean.setTestD01Render(true);
				testAgirlikDusurmeSonucBean.fillTestList(1029,
						selectedPipe.getPipeId());
			} else if (selectedTest.getGlobalId() == 1030) {
				falseAllRenders();
				testRendersBean.setTestAgirlikRender(true);
				testRendersBean.setTestD02Render(true);
				testAgirlikDusurmeSonucBean.fillTestList(1030,
						selectedPipe.getPipeId());
			}
			// kaynak
			else if (selectedTest.getGlobalId() == 1031) {
				falseAllRenders();
				testRendersBean.setTestKaynakRender(true);
				testRendersBean.setTestM01Render(true);
				testKaynakSonucBean
						.fillTestList(1031, selectedPipe.getPipeId());
			} else if (selectedTest.getGlobalId() == 1032) {
				falseAllRenders();
				testRendersBean.setTestKaynakRender(true);
				testRendersBean.setTestH01Render(true);
				testKaynakSonucBean
						.fillTestList(1032, selectedPipe.getPipeId());
			}
		} else // kaplama testleri ise
		{
			if (selectedIsolation.getTestKodu() == IsolationTestType.CIT1) {
				// kaplama darbe
				falseAllRenders();
				testRendersBean.setTestKaplamaDarbeRender(true);
				testRendersBean.setTestCIT1Render(true);
				testKaplamaDarbeSonucBean.fillTestList(2016,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.LIT1) {
				// kaplama darbe
				falseAllRenders();
				testRendersBean.setTestKaplamaDarbeRender(true);
				testRendersBean.setTestLIT1Render(true);
				testKaplamaDarbeSonucBean.fillTestList(2132,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CSC1) {
				// yuzey kontroleri
				falseAllRenders();
				testRendersBean.setTestYuzeyKontrolleriRender(true);
				testRendersBean.setTestCSC1Render(true);
				testKaplamaKumlanmisBoruYuzeyTemizligiSonucBean.fillTestList(
						2005, selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.LSC1) {
				// yuzey kontroleri
				falseAllRenders();
				testRendersBean.setTestYuzeyKontrolleriRender(true);
				testRendersBean.setTestLSC1Render(true);
				testKaplamaKumlanmisBoruYuzeyTemizligiSonucBean.fillTestList(
						2123, selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.LSP1) {
				// yuzey kontroleri
				falseAllRenders();
				testRendersBean.setTestKaplamaYuzeyPuruzlulukTestiRender(true);
				testRendersBean.setTestLSP1Render(true);
				testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucBean.fillTestList(
						2124, selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CSP1) {
				// yuzey kontroleri
				falseAllRenders();
				testRendersBean.setTestKaplamaYuzeyPuruzlulukTestiRender(true);
				testRendersBean.setTestCSP1Render(true);
				testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucBean.fillTestList(
						2006, selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CBT1) {
				// kaplama bukme testleri
				falseAllRenders();
				testRendersBean.setTestKaplamaBukmeRender(true);
				testRendersBean.setTestCBT1Render(true);
				testKaplamaBukmeSonucBean.fillTestList(2012,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.LBT1) {
				// kaplama bukme testleri
				falseAllRenders();
				testRendersBean.setTestKaplamaBukmeRender(true);
				testRendersBean.setTestLBT1Render(true);
				testKaplamaBukmeSonucBean.fillTestList(2129,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CDS1) {
				// toz boya
				falseAllRenders();
				testRendersBean.setTestTozBoyaRender(true);
				testRendersBean.setTestCDS1Render(true);
				testKaplamaTozBoyaSonucBean.fillTestList(2013,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.EXC1) {
				// toz boya
				falseAllRenders();
				testRendersBean.setTestKaplamaTozEpoksiXcutSonucRender(true);
				testRendersBean.setTestEXC1Render(true);
				testKaplamaTozEpoksiXcutSonucBean.fillTestList(2140,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.OHT1) {
				// holiday test
				falseAllRenders();
				testRendersBean.setTestKaplamaOnlineHolidaySonucRender(true);
				testRendersBean.setTestOHT1Render(true);
				testKaplamaOnlineHolidaySonucBean.fillTestList(2015,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.LHT1) {
				// holiday test
				falseAllRenders();
				testRendersBean.setTestKaplamaHolidayPinholeSonucRender(true);
				testRendersBean.setTestLHT1Render(true);
				testKaplamaHolidayPinholeSonucBean.fillTestList(2131,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CAT1) {
				// yapışma testi
				falseAllRenders();
				testRendersBean.setTestYapismaRender(true);
				testRendersBean.setTestCAT1Render(true);
				testKaplamaYapismaSonucBean.fillTestList(2017,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.LAT1) {
				// yapışma testi
				falseAllRenders();
				testRendersBean.setTestYapismaRender(true);
				testRendersBean.setTestLAT1Render(true);
				testKaplamaYapismaSonucBean.fillTestList(2133,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CIT2) {
				// delici uc direnc testi
				falseAllRenders();
				testRendersBean.setTestDeliciRender(true);
				testRendersBean.setTestCIT2Render(true);
				testKaplamaDeliciUcDirencSonucBean.fillTestList(2018,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.LIT2) {
				// delici uc direnc testi
				falseAllRenders();
				testRendersBean.setTestDeliciRender(true);
				testRendersBean.setTestLIT2Render(true);
				testKaplamaDeliciUcDirencSonucBean.fillTestList(2034,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CET1) {
				falseAllRenders();
				testRendersBean.setTestPolyolefinRender(true);
				testRendersBean.setTestCET1Render(true);
				testPolyolefinUzamaSonucBean.fillTestList(2019,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CCD1) {
				// katodik
				falseAllRenders();
				testRendersBean.setTestKatodikRender(true);
				testRendersBean.setTestCCD1Render(true);
				testKaplamaKatodikSoyulmaSonucBean.fillTestList(2020,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.LCD1) {
				// katodik
				falseAllRenders();
				testRendersBean.setTestKatodikRender(true);
				testRendersBean.setTestLCD1Render(true);
				testKaplamaKatodikSoyulmaSonucBean.fillTestList(2135,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CHT2) {
				// sertlik
				falseAllRenders();
				testRendersBean.setTestSertlikRender(true);
				testRendersBean.setTestCHT2Render(true);
				testKaplamaSertlikSonucBean.fillTestList(2021,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.LHT2) {
				// sertlik
				falseAllRenders();
				testRendersBean.setTestSertlikRender(true);
				testRendersBean.setTestLHT2Render(true);
				testKaplamaSertlikSonucBean.fillTestList(2036,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.BGK1) {
				falseAllRenders();
				testRendersBean.setTestKaplamaBetonGozKontrolSonucRender(true);
				testRendersBean.setTestBGK1Render(true);
				testKaplamaBetonGozKontrolSonucBean.fillTestList(2144,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.BYH1) {
				falseAllRenders();
				testRendersBean
						.settestKaplamaBetonYuzeyHazirligiSonucRender(true);
				testRendersBean.setTestBYH1Render(true);
				testKaplamaBetonYuzeyHazirligiSonucBean.fillTestList(2143,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.LSK1) {
				falseAllRenders();
				testRendersBean.setTestKaplamaOncesiYuzeyKontrolRender(true);
				testRendersBean.setTestLSK1Render(true);
				testKaplamaOncesiYuzeyKontroluSonucBean.fillTestList(2150,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CCT1) {
				// MPQT kaplama kalınlıgı
				falseAllRenders();
				testRendersBean.setTestKaplamaMpqtKaplamaKalinligiRender(true);
				testRendersBean.setTestCCT1Render(true);
				testKaplamaMpqtKaplamaKalinligliSonucBean.fillTestList(2014,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.LCT1) {
				// kaplama kalınlıgı
				falseAllRenders();
				testRendersBean.setTestKaplamaKalinligiRender(true);
				testRendersBean.setTestLCT1Render(true);
				testKaplamaKalinligiSonucBean.fillTestList(2130,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.LFI1) {
				falseAllRenders();
				testRendersBean.setTestKaplamaNihaiKontrolSonucRender(true);
				testRendersBean.setTestLFI1Render(true);
				testKaplamaNihaiKontrolSonucBean.fillTestList(2137,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CFI1) {
				falseAllRenders();
				testRendersBean.setTestKaplamaNihaiKontrolSonucRender(true);
				testRendersBean.setTestCFI1Render(true);
				testKaplamaNihaiKontrolSonucBean.fillTestList(2022,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.LSD1) {
				falseAllRenders();
				testRendersBean
						.setTestKaplamaKumlanmisBoruTozMiktariSonucRender(true);
				testRendersBean.setTestLSD1Render(true);
				testKaplamaKumlanmisBoruTozMiktariSonucBean.fillTestList(2125,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CSD1) {
				falseAllRenders();
				testRendersBean
						.setTestKaplamaKumlanmisBoruTozMiktariSonucRender(true);
				testRendersBean.setTestCSD1Render(true);
				testKaplamaKumlanmisBoruTozMiktariSonucBean.fillTestList(2007,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CKB1) {
				falseAllRenders();
				testRendersBean.setTestKaplamaKaplamasizBolgeSonucRender(true);
				testRendersBean.setTestCKB1Render(true);
				testKaplamaKaplamasizBolgeSonucBean.fillTestList(2033,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.LKB1) {
				falseAllRenders();
				testRendersBean.setTestKaplamaKaplamasizBolgeSonucRender(true);
				testRendersBean.setTestLKB1Render(true);
				testKaplamaKaplamasizBolgeSonucBean.fillTestList(2146,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.KSK1) {
				falseAllRenders();
				testRendersBean.setTestKaplamaUygulamaSicaklikSonucRender(true);
				testRendersBean.setTestKSK1Render(true);
				testKaplamaUygulamaSicaklikSonucBean.fillTestList(2030,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CGK1) {
				falseAllRenders();
				testRendersBean.setTestKaplamaGozKontrolSonucRender(true);
				testRendersBean.setTestCGK1Render(true);
				testKaplamaGozKontrolSonucBean.fillTestList(2035,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CSS1) {
				falseAllRenders();
				testRendersBean
						.setTestKaplamaKumlanmisBoruTuzMiktariSonucRender(true);
				testRendersBean.setTestCSS1Render(true);
				testKaplamaKumlanmisBoruTuzMiktariSonucBean.fillTestList(2008,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.LSS1) {
				falseAllRenders();
				testRendersBean
						.setTestKaplamaKumlanmisBoruTuzMiktariSonucRender(true);
				testRendersBean.setTestLSS1Render(true);
				testKaplamaKumlanmisBoruTuzMiktariSonucBean.fillTestList(2126,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.LBZ1) {
				// bucholz test
				falseAllRenders();
				testRendersBean.setTestKaplamaBuchholzSonucRender(true);
				testRendersBean.setTestLBZ1Render(true);
				testKaplamaBuchholzSonucBean.fillTestList(2139,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CKT1) {
				falseAllRenders();
				testRendersBean.setTestKaplamaTamirTestSonucRender(true);
				testRendersBean.setTestCKT1Render(true);
				testKaplamaTamirTestSonucBean.fillTestList(2011,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CKT2) {
				falseAllRenders();
				testRendersBean.setTestKaplamaTamirTestSonucRender(true);
				testRendersBean.setTestCKT2Render(true);
				testKaplamaTamirTestSonucBean.fillTestList(2034,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.LKT1) {
				// epoksi tamir testi
				falseAllRenders();
				testRendersBean.setTestKaplamaTamirTestSonucRender(true);
				testRendersBean.setTestLKT1Render(true);
				testKaplamaTamirTestSonucBean.fillTestList(2149,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CAS1) {
				// kumlama malzemesindeki tuz miktarı
				falseAllRenders();
				testRendersBean
						.setTestKaplamaKumlamaMalzemesiTuzMiktariSonucRender(true);
				testRendersBean.setTestCAS1Render(true);
				testKaplamaKumlamaMalzemesiTuzMiktariSonucBean.fillTestList(
						2009, selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.LAS1) {
				// kumlama malzemesindeki tuz miktarı
				falseAllRenders();
				testRendersBean
						.setTestKaplamaKumlamaMalzemesiTuzMiktariSonucRender(true);
				testRendersBean.setTestLAS1Render(true);
				testKaplamaKumlamaMalzemesiTuzMiktariSonucBean.fillTestList(
						2127, selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CPP1) {
				falseAllRenders();
				testRendersBean.setTestKaplamaProcessBozulmaSonucRender(true);
				testRendersBean.setTestCPP1Render(true);
				testKaplamaProcessBozulmaSonucBean.fillTestList(2031,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CPP2) {
				falseAllRenders();
				testRendersBean.setTestKaplamaProcessBozulmaSonucRender(true);
				testRendersBean.setTestCPP2Render(true);
				testKaplamaProcessBozulmaSonucBean.fillTestList(2032,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CAC1) {
				falseAllRenders();
				testRendersBean
						.setTestKaplamaKumlamaOrtamSartlariSonucRender(true);
				testRendersBean.setTestCAC1Render(true);
				testKaplamaKumlamaOrtamSartlariSonucBean.fillTestList(2010,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.LAC1) {
				falseAllRenders();
				testRendersBean
						.setTestKaplamaKumlamaOrtamSartlariSonucRender(true);
				testRendersBean.setTestLAC1Render(true);
				testKaplamaKumlamaOrtamSartlariSonucBean.fillTestList(2128,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.BSC1) {
				falseAllRenders();
				testRendersBean
						.setTestKaplamaBetonOranBelirlenmesiSonucRender(true);
				testRendersBean.setTestBSC1Render(true);
				testKaplamaBetonOranBelirlenmesiSonucBean.fillTestList(2141,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.BCB1) {
				falseAllRenders();
				testRendersBean
						.setTestKaplamaBetonEgilmeBasincSonucRender(true);
				testRendersBean.setTestBCB1Render(true);
				testKaplamaBetonEgilmeBasincSonucBean.fillTestList(2138,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.LKK1) {
				// kumlama malzemesi boyut sekil ozellik testi
				falseAllRenders();
				testRendersBean
						.setTestKaplamaKumlamaMalzemesiBoyutSekilOzellikKontroluSonucRender(true);
				testRendersBean.setTestLKK1Render(true);
				testKaplamaUygulamaSicaklikSonucBean.fillTestList(2148,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CKK1) {
				// kumlama malzemesi boyut sekil ozellik testi
				falseAllRenders();
				testRendersBean
						.setTestKaplamaKumlamaMalzemesiBoyutSekilOzellikKontroluSonucRender(true);
				testRendersBean.setTestCKK1Render(true);
				testKaplamaUygulamaSicaklikSonucBean.fillTestList(2025,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.LDT1) {
				// Daldirma Testi
				falseAllRenders();
				testRendersBean.setTestKaplamaDaldirmaTestiSonucRender(true);
				testRendersBean.setTestLDT1Render(true);
				testKaplamaUygulamaSicaklikSonucBean.fillTestList(2151,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.FPT1) {
				// Flow Coat - Pinhole
				falseAllRenders();
				testRendersBean.setTestKaplamaFlowCoatPinholeSonucRender(true);
				testRendersBean.setTestFPT1Render(true);
				testKaplamaFlowCoatPinholeSonucBean.fillTestList(2152,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.FBH1) {
				// Flow Coat - Buchholz
				falseAllRenders();
				testRendersBean.setTestKaplamaFlowCoatBuchholzSonucRender(true);
				testRendersBean.setTestFBH1Render(true);
				testKaplamaFlowCoatBuchholzSonucBean.fillTestList(2153,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.FFT1) {
				// Flow Coat - Film Thickness
				falseAllRenders();
				testRendersBean
						.setTestKaplamaFlowCoatFilmThicknessSonucRender(true);
				testRendersBean.setTestFFT1Render(true);
				testKaplamaFlowCoatFilmThicknessSonucBean.fillTestList(2154,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.FCT1) {
				// Flow Coat - Cure
				falseAllRenders();
				testRendersBean.setTestKaplamaFlowCoatCureSonucRender(true);
				testRendersBean.setTestFCT1Render(true);
				testKaplamaFlowCoatCureSonucBean.fillTestList(2155,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.FBT1) {
				// Flow Coat - Bend
				falseAllRenders();
				testRendersBean.setTestKaplamaFlowCoatBendSonucRender(true);
				testRendersBean.setTestFBT1Render(true);
				testKaplamaFlowCoatBendSonucBean.fillTestList(2156,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.FWT1) {
				// Flow Coat - Water
				falseAllRenders();
				testRendersBean.setTestKaplamaFlowCoatWaterSonucRender(true);
				testRendersBean.setTestFWT1Render(true);
				testKaplamaFlowCoatWaterSonucBean.fillTestList(2157,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.FAT1) {
				// Flow Coat - Adhesion
				falseAllRenders();
				testRendersBean.setTestKaplamaFlowCoatBondSonucRender(true);
				testRendersBean.setTestFAT1Render(true);
				testKaplamaFlowCoatBondSonucBean.fillTestList(2158,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.FST1) {
				// Flow Coat - Stripping
				falseAllRenders();
				testRendersBean
						.setTestKaplamaFlowCoatStrippingSonucRender(true);
				testRendersBean.setTestFST1Render(true);
				testKaplamaFlowCoatStrippingSonucBean.fillTestList(2159,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.BKK1) {
				// kaplama sonrası yüzey kontrolü
				falseAllRenders();
				testRendersBean.setTestKaplamaSonrasiYuzeyKontrolRender(true);
				testRendersBean.setTestBKK1Render(true);
				testKaplamaSonrasiYuzeyKontroluSonucBean.fillTestList(2145,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.POY1) {
				// pulloff yapışma testi
				falseAllRenders();
				testRendersBean.setTestKaplamaPulloffYapismaSonucRender(true);
				testRendersBean.setTestPOY1Render(true);
				testKaplamaPulloffYapismaSonucBean.fillTestList(2142,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CTY) {
				// Termal Ageing testi
				falseAllRenders();
				testRendersBean
						.setTestKaplamaThermalAgeingTestSonucRender(true);
				testRendersBean.setTestCTYRender(true);
				testKaplamaThermalAgeingTestSonucBean.fillTestList(2142,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CUV) {
				// UV Ageing testi
				falseAllRenders();
				testRendersBean.setTestKaplamaUvAgeingTestSonucRender(true);
				testRendersBean.setTestCUVRender(true);
				testKaplamaUvAgeingTestSonucBean.fillTestList(2040,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CED) {
				// Elektriksel Direnç testi
				falseAllRenders();
				testRendersBean
						.setTestKaplamaElektrikselDirencTestSonucRender(true);
				testRendersBean.setTestCEDRender(true);
				testKaplamaElektrikselDirencTestSonucBean.fillTestList(2039,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.CST) {
				// Storage UV Condition testi
				falseAllRenders();
				testRendersBean
						.setTestKaplamaStorageConditionTestSonucRender(true);
				testRendersBean.setTestCSTRender(true);
				testKaplamaStorageConditionTestSonucBean.fillTestList(2041,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.LGB) {
				// Gas Blistering testi
				falseAllRenders();
				testRendersBean.setTestKaplamaGasBlisteringSonucRender(true);
				testRendersBean.setTestLGBRender(true);
				testKaplamaGasBlisteringTestSonucBean.fillTestList(2160,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.LHB) {
				// Hydraulic Blistering testi
				falseAllRenders();
				testRendersBean
						.setTestKaplamaHydraulicBlisteringSonucRender(true);
				testRendersBean.setTestLHBRender(true);
				testKaplamaHydraulicBlisteringTestSonucBean.fillTestList(2161,
						selectedPipe.getPipeId());
			} else if (selectedIsolation.getTestKodu() == IsolationTestType.LMP) {
				// Mixed Wet Paint testi
				falseAllRenders();
				testRendersBean
						.setTestKaplamaMixedWetPaintTestSonucRender(true);
				testRendersBean.setTestLMPRender(true);
				testKaplamaMixedWetPaintTestSonucBean.fillTestList(2162,
						selectedPipe.getPipeId());
			}
		}
	}

	public void falseAllRenders() {

		if (testRendersBean == null) {
			return;
		} else {
			UtilInsCore.setProperties(testRendersBean, false);
		}
	}

	public void sonucKontrol(Integer testId, boolean kontrol) {
		IsolationTestDefinitionManager isoMan = new IsolationTestDefinitionManager();
		IsolationTestDefinition kontrolIsolationList = (IsolationTestDefinition) isoMan
				.loadObject(IsolationTestDefinition.class, testId);
		if (kontrolIsolationList == null
				|| kontrolIsolationList.getId() == null) {
			System.out.println("Error");
			return;
		}
		kontrolIsolationList.setHasSonuc(kontrol);
		isoMan.updateEntity(kontrolIsolationList);

	}

	// document upload

	public void uploadRender(String test) {

		// true ise chemReq false ise Boyutsal
		if (test.equals("1")) {
			uploadRender = 1;
			privatePath = File.separatorChar + "testDocs" + File.separatorChar
					+ "uploadedDocuments" + File.separatorChar
					+ "kimyasalTanim" + File.separatorChar;
			init();
		} else
			uploadRender = 0;

		System.out.println(test);

	}

	private String path;
	private String privatePath;

	private File theFile = null;
	OutputStream output = null;
	InputStream input = null;

	@SuppressWarnings("unused")
	private File[] fileArray;

	private StreamedContent downloadedFile;
	private String downloadedFileName;

	SalesOrderBean sob = new SalesOrderBean();

	public void addUploadedFile(FileUploadEvent event)
			throws AbortProcessingException, IOException {
		System.out.println("QualitySpecBean.addUploadedFile()");

		try {
			String name = sob.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");

				if (uploadRender == 0) {
					selectedSalesItem.getBoyutsalKontrol().setDocuments(
							selectedSalesItem.getBoyutsalKontrol()
									.getDocuments() + "//" + name);
				} else {

					selectedSalesItem.getChemicalRequirements().setDocuments(
							selectedSalesItem.getChemicalRequirements()
									.getDocuments() + "//" + name);
				}

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				if (uploadRender == 0) {

					BoruBoyutsalKontrolToleransManager manager = new BoruBoyutsalKontrolToleransManager();
					manager.updateEntity(selectedSalesItem.getBoyutsalKontrol());
				} else {

					ChemicalRequirementManager chemMen = new ChemicalRequirementManager();
					chemMen.updateEntity(selectedSalesItem
							.getChemicalRequirements());
				}
				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				FacesContextUtils.addWarnMessage("salesFileUploadedMessage");
			} else {
				System.out.println("File already exists.");

				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("QualitySpecBean: " + e.toString());
		}
	}

	@SuppressWarnings("resource")
	public StreamedContent getDownloadedFile() {
		System.out.println("QualitySpecBean.getDownloadedFile()");

		try {
			if (downloadedFileName != null) {
				RandomAccessFile accessFile = new RandomAccessFile(path
						+ downloadedFileName, "r");
				byte[] downloadByte = new byte[(int) accessFile.length()];
				accessFile.read(downloadByte);
				InputStream stream = new ByteArrayInputStream(downloadByte);

				String suffix = FilenameUtils.getExtension(downloadedFileName);
				String contentType = "text/plain";
				if (suffix.contentEquals("jpg") || suffix.contentEquals("png")
						|| suffix.contentEquals("gif")) {
					contentType = "image/" + contentType;
				} else if (suffix.contentEquals("doc")
						|| suffix.contentEquals("docx")) {
					contentType = "application/msword";
				} else if (suffix.contentEquals("xls")
						|| suffix.contentEquals("xlsx")) {
					contentType = "application/vnd.ms-excel";
				} else if (suffix.contentEquals("pdf")) {
					contentType = "application/pdf";
				}
				downloadedFile = new DefaultStreamedContent(stream,
						"contentType", downloadedFileName);

				stream.close();
			}
		} catch (Exception e) {
			System.out.println("QualitySpecBean.getDownloadedFile():"
					+ e.toString());
		}
		return downloadedFile;
	}

	public String getDownloadedFileName() {
		return downloadedFileName;
	}

	public void setDownloadedFileName(String downloadedFileName) {
		try {
			this.downloadedFileName = downloadedFileName;
		} catch (Exception e) {
			System.out.println("QualitySpecBean.setDownloadedFileName():"
					+ e.toString());
		}
	}

	public void setPath(String path) {
		this.path = path;
	}

	// document upload

	public void goToPipeDestructiveTestPage() {
		// Tahribatlı test açılış sayfası
		falseAllRenders();
		FacesContextUtils.redirect(config.getPageUrl().PIPE_DESTRUCTIVE_TEST);
	}

	public void goToPipeCoatingTestPage() {
		// Kaplama Testleri açılış sayfası
		falseAllRenders();
		FacesContextUtils.redirect(config.getPageUrl().PIPE_COATING_TEST);
	}

	public void goToUlkelerPage() {
		// Ulkeler açılış sayfası
		FacesContextUtils.redirect(config.getPageUrl().ULKELER_PAGE);
	}

	public void goToIncelemeGrubuPage() {
		// Inceleme Grubu açılış sayfası
		FacesContextUtils.redirect(config.getPageUrl().INCELEME_GRUBU_PAGE);
	}

	// kaplama türleri ekleme kısmı

	public void addOrUpdateKaplamaTurleri(ActionEvent e) {

		BoruKaliteKaplamaTurleriManager bkktManager = new BoruKaliteKaplamaTurleriManager();

		if (boruKaliteKaplamaTurleri.getId() == null) {

			try {

				boruKaliteKaplamaTurleri.setEklemeZamani(UtilInsCore
						.getTarihZaman());
				boruKaliteKaplamaTurleri.setEkleyenKullanici(userBean.getUser()
						.getId());
				boruKaliteKaplamaTurleri.setSalesItem(selectedSalesItem);

				bkktManager.enterNew(boruKaliteKaplamaTurleri);

				// this.boruKaliteKaplamaTurleri = new
				// BoruKaliteKaplamaTurleri();

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								"SİPARİŞ İÇ - DIŞ KAPLAMA TÜRÜ VERİSİ BAŞARIYLA EKLENMİŞTİR!"));

			} catch (Exception e2) {

				System.out
						.println(" qualitySpecBean.addOrUpdateKaplamaTurleri-HATA EnterNew");
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								"SİPARİŞ İÇ - DIŞ KAPLAMA TÜRÜ VERİSİ EKLENEMEDİ, TEKRAR DENEYİNİZ!"));
				return;
			}

		} else {

			try {

				boruKaliteKaplamaTurleri.setGuncellemeZamani(UtilInsCore
						.getTarihZaman());
				boruKaliteKaplamaTurleri.setGuncelleyenKullanici(userBean
						.getUser().getId());

				bkktManager.updateEntity(boruKaliteKaplamaTurleri);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								"SİPARİŞ İÇ - DIŞ KAPLAMA TÜRÜ VERİSİ BAŞARIYLA GÜNCELLENMİŞTİR!"));

			} catch (Exception e2) {

				System.err
						.println(" qualitySpecBean.addOrUpdateKaplamaTurleri-HATA Guncelleme");
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								"SİPARİŞ İÇ - DIŞ KAPLAMA TÜRÜ VERİSİ GÜNCELLENEMEMİŞTİR, TEKRAR DENEYİNİZ!"));
				return;
			}
		}

	}

	public void deleteKaplamaTurleri(ActionEvent e) {

		BoruKaliteKaplamaTurleriManager bkktManager = new BoruKaliteKaplamaTurleriManager();

		bkktManager.delete(boruKaliteKaplamaTurleri);

		boruKaliteKaplamaTurleri = new BoruKaliteKaplamaTurleri();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"SİPARİŞ İÇ - DIŞ KAPLAMA TÜRÜ VERİSİ SİLİNMİŞTİR!"));

	}

	public void prepareMarkalama() {

		SalesItemManager salesManager = new SalesItemManager();
		selectedSalesItem = salesManager.loadObject(SalesItem.class,
				salesItemId);
		selectedSalesItem.getKaliteKaplamaMarkalama();

		if (selectedSalesItem.getKaliteKaplamaMarkalama() == null) {

			selectedMarkalama = new KaliteKaplamaMarkalama();
			return;
		}
		selectedMarkalama = selectedSalesItem.getKaliteKaplamaMarkalama();
	}

	@SuppressWarnings("static-access")
	public void addOrUpdateMarkalama() {

		SalesItemManager salesManager = new SalesItemManager();
		KaliteKaplamaMarkalamaManager markalamaManager = new KaliteKaplamaMarkalamaManager();
		selectedSalesItem = salesManager.loadObject(SalesItem.class,
				salesItemId);

		int count = markalamaManager.getAllKaliteKaplamaMarkalama(
				selectedSalesItem.getItemId()).size();

		if (selectedMarkalama.getId() == null && count == 0) {

			selectedMarkalama.setSalesItem(selectedSalesItem);
			selectedSalesItem.setKaliteKaplamaMarkalama(selectedMarkalama);

			selectedMarkalama.setEklemeZamani(UtilInsCore.getTarihZaman());
			selectedMarkalama.setEkleyenKullanici(userBean.getUser().getId());
			markalamaManager.enterNew(selectedMarkalama);

			this.selectedMarkalama = new KaliteKaplamaMarkalama();

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"KAPLAMA MARKALAMA BAŞARIYLA EKLENMİŞTİR!"));
		} else if (selectedMarkalama.getId() != null && count != 0) {

			selectedMarkalama.setSalesItem(selectedSalesItem);
			selectedSalesItem.setKaliteKaplamaMarkalama(selectedMarkalama);
			selectedMarkalama.setGuncellemeZamani(UtilInsCore.getTarihZaman());
			selectedMarkalama.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			markalamaManager.updateEntity(selectedMarkalama);

			this.selectedMarkalama = new KaliteKaplamaMarkalama();

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"KAPLAMA MARKALAMA BAŞARIYLA GÜNCELLENMİŞTİR!"));
		}
	}

	public void deleteMarkalama() {

		if (selectedSalesItem.getKaliteKaplamaMarkalama().getId() == 0) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		SalesItemManager salesManager = new SalesItemManager();
		KaliteKaplamaMarkalamaManager markalamaManager = new KaliteKaplamaMarkalamaManager();

		markalamaManager.delete(selectedMarkalama);

		this.selectedMarkalama = new KaliteKaplamaMarkalama();

		selectedSalesItem = salesManager.loadObject(SalesItem.class,
				salesItemId);
		salesManager.refreshEntity(selectedSalesItem);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"KAPLAMA MARKALAMA BAŞARIYLA SİLİNMİŞTİR!"));
	}

	public void prepareKaynakAgzi() {

		SalesItemManager salesManager = new SalesItemManager();
		selectedSalesItem = salesManager.loadObject(SalesItem.class,
				salesItemId);
		selectedSalesItem.getKaplamaKaliteKaynakAgziKoruma();

		if (selectedSalesItem.getKaplamaKaliteKaynakAgziKoruma() == null) {

			selectedKaynakAgziKoruma = new KaplamaKaliteKaynakAgziKoruma();
			return;
		}
		selectedKaynakAgziKoruma = selectedSalesItem
				.getKaplamaKaliteKaynakAgziKoruma();
	}

	@SuppressWarnings("static-access")
	public void addOrUpdateKaynakAgzi() {

		SalesItemManager salesManager = new SalesItemManager();
		KaplamaKaliteKaynakAgziKorumaManager korumaManager = new KaplamaKaliteKaynakAgziKorumaManager();
		selectedSalesItem = salesManager.loadObject(SalesItem.class,
				salesItemId);

		int count = korumaManager.getAllKaplamaKaliteKaynakAgziKoruma(
				selectedSalesItem.getItemId()).size();

		if (selectedKaynakAgziKoruma.getId() == null && count == 0) {

			selectedKaynakAgziKoruma.setSalesItem(selectedSalesItem);
			selectedSalesItem
					.setKaplamaKaliteKaynakAgziKoruma(selectedKaynakAgziKoruma);
			selectedKaynakAgziKoruma.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			selectedKaynakAgziKoruma.setEkleyenKullanici(userBean.getUser()
					.getId());
			korumaManager.enterNew(selectedKaynakAgziKoruma);

			this.selectedKaynakAgziKoruma = new KaplamaKaliteKaynakAgziKoruma();

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"KAYNAK AĞZI KORUMA BAŞARIYLA EKLENMİŞTİR!"));
		} else if (selectedKaynakAgziKoruma.getId() != null && count != 0) {

			selectedKaynakAgziKoruma.setSalesItem(selectedSalesItem);
			selectedSalesItem
					.setKaplamaKaliteKaynakAgziKoruma(selectedKaynakAgziKoruma);
			selectedKaynakAgziKoruma.setGuncellemeZamani(UtilInsCore
					.getTarihZaman());
			selectedKaynakAgziKoruma.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			korumaManager.updateEntity(selectedKaynakAgziKoruma);

			this.selectedKaynakAgziKoruma = new KaplamaKaliteKaynakAgziKoruma();

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"KAPLAMA MARKALAMA BAŞARIYLA GÜNCELLENMİŞTİR!"));
		}
	}

	public void deleteKaynakAgzi() {

		if (selectedSalesItem.getKaliteKaplamaMarkalama().getId() == 0) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		SalesItemManager salesManager = new SalesItemManager();
		KaplamaKaliteKaynakAgziKorumaManager kaplamaManager = new KaplamaKaliteKaynakAgziKorumaManager();

		kaplamaManager.delete(selectedKaynakAgziKoruma);

		this.selectedKaynakAgziKoruma = new KaplamaKaliteKaynakAgziKoruma();

		selectedSalesItem = salesManager.loadObject(SalesItem.class,
				salesItemId);
		salesManager.refreshEntity(selectedSalesItem);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"KAPLAMA MARKALAMA BAŞARIYLA SİLİNMİŞTİR!"));
	}

	public void prepareCutBack() {

		SalesItemManager salesManager = new SalesItemManager();
		selectedSalesItem = salesManager.loadObject(SalesItem.class,
				salesItemId);
		selectedSalesItem.getKaplamaKaliteCutBackKoruma();

		if (selectedSalesItem.getKaplamaKaliteCutBackKoruma() == null) {

			selectedCutBack = new KaplamaKaliteCutBackKoruma();
			return;
		}
		selectedCutBack = selectedSalesItem.getKaplamaKaliteCutBackKoruma();
	}

	@SuppressWarnings("static-access")
	public void addOrUpdateCutBack() {

		SalesItemManager salesManager = new SalesItemManager();
		KaplamaKaliteCutBackKorumaManager cbManager = new KaplamaKaliteCutBackKorumaManager();
		selectedSalesItem = salesManager.loadObject(SalesItem.class,
				salesItemId);

		int count = cbManager.getAllKaplamaKaliteCutBackKoruma(
				selectedSalesItem.getItemId()).size();

		if (selectedCutBack.getId() == null && count == 0) {

			selectedCutBack.setSalesItem(selectedSalesItem);
			selectedSalesItem.setKaplamaKaliteCutBackKoruma(selectedCutBack);

			selectedCutBack.setEklemeZamani(UtilInsCore.getTarihZaman());
			selectedCutBack.setEkleyenKullanici(userBean.getUser().getId());
			cbManager.enterNew(selectedCutBack);

			this.selectedCutBack = new KaplamaKaliteCutBackKoruma();

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"CUT BACK KORUMA BAŞARIYLA EKLENMİŞTİR!"));
		} else if (selectedCutBack.getId() != null && count != 0) {

			selectedCutBack.setSalesItem(selectedSalesItem);
			selectedSalesItem.setKaplamaKaliteCutBackKoruma(selectedCutBack);
			selectedCutBack.setGuncellemeZamani(UtilInsCore.getTarihZaman());
			selectedCutBack.setGuncelleyenKullanici(userBean.getUser().getId());
			cbManager.updateEntity(selectedCutBack);

			this.selectedCutBack = new KaplamaKaliteCutBackKoruma();

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"CUT BACK KORUMA BAŞARIYLA GÜNCELLENMİŞTİR!"));
		}
	}

	public void deleteCutBack() {

		if (selectedSalesItem.getKaplamaKaliteCutBackKoruma().getId() == 0) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		SalesItemManager salesManager = new SalesItemManager();
		KaplamaKaliteCutBackKorumaManager cbManager = new KaplamaKaliteCutBackKorumaManager();

		cbManager.delete(selectedCutBack);

		this.selectedCutBack = new KaplamaKaliteCutBackKoruma();

		selectedSalesItem = salesManager.loadObject(SalesItem.class,
				salesItemId);
		salesManager.refreshEntity(selectedSalesItem);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"CUT BACK KORUMA BAŞARIYLA SİLİNMİŞTİR!"));
	}

	// ***********************************************************************//
	// GETTERS AND SETTERS //
	// ***********************************************************************//

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	public PlannedIsolation getInIsolation() {
		return inIsolation;
	}

	public void setInIsolation(PlannedIsolation inIsolation) {
		this.inIsolation = inIsolation;
	}

	public PlannedIsolation getOutIsolation() {
		return outIsolation;
	}

	public void setOutIsolation(PlannedIsolation outIsolation) {
		this.outIsolation = outIsolation;
	}

	public Layer getLayer() {
		return layer;
	}

	public void setLayer(Layer layer) {
		this.layer = layer;
	}

	public QualitySpec getQualitySpec() {
		return qualitySpec;
	}

	public void setQualitySpec(QualitySpec qualitySpec) {
		this.qualitySpec = qualitySpec;
	}

	public BoruBoyutsalKontrolTolerans getBoyutsalKontrol() {
		return boyutsalKontrol;
	}

	public void setBoyutsalKontrol(BoruBoyutsalKontrolTolerans boyutsalKontrol) {
		this.boyutsalKontrol = boyutsalKontrol;
	}

	public HidrostaticTestsSpec getSelectedHidro() {
		return selectedHidro;
	}

	public void setSelectedHidro(HidrostaticTestsSpec selectedHidro) {
		this.selectedHidro = selectedHidro;
	}

	public SelectedTests getSelectedTests() {
		return selectedTests;
	}

	public void setSelectedTests(SelectedTests selectedTests) {
		this.selectedTests = selectedTests;
	}

	public List<Pipe> getProducedPipes() {
		return producedPipes;
	}

	public void setProducedPipes(List<Pipe> producedPipes) {
		this.producedPipes = producedPipes;
	}

	public DestructiveTestsSpec getSelectedTest() {
		return selectedTest;
	}

	public void setSelectedTest(DestructiveTestsSpec selectedTest) {
		this.selectedTest = selectedTest;
	}

	public TestCekmeSonucBean getTestCekmeSonucBean() {
		return testCekmeSonucBean;
	}

	public void setTestCekmeSonucBean(TestCekmeSonucBean testCekmeSonucBean) {
		this.testCekmeSonucBean = testCekmeSonucBean;
	}

	public TestRendersBean getTestRendersBean() {
		return testRendersBean;
	}

	public void setTestRendersBean(TestRendersBean testRendersBean) {
		this.testRendersBean = testRendersBean;
	}

	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	public TestBukmeSonucBean getTestBukmeSonucBean() {
		return testBukmeSonucBean;
	}

	public void setTestBukmeSonucBean(TestBukmeSonucBean testBukmeSonucBean) {
		this.testBukmeSonucBean = testBukmeSonucBean;
	}

	public TestAgirlikDusurmeSonucBean getTestAgirlikDusurmeSonucBean() {
		return testAgirlikDusurmeSonucBean;
	}

	public void setTestAgirlikDusurmeSonucBean(
			TestAgirlikDusurmeSonucBean testAgirlikDusurmeSonucBean) {
		this.testAgirlikDusurmeSonucBean = testAgirlikDusurmeSonucBean;
	}

	public TestCentikDarbeSonucBean getTestCentikDarbeSonucBean() {
		return testCentikDarbeSonucBean;
	}

	public void setTestCentikDarbeSonucBean(
			TestCentikDarbeSonucBean testCentikDarbeSonucBean) {
		this.testCentikDarbeSonucBean = testCentikDarbeSonucBean;
	}

	public TestKaynakSonucBean getTestKaynakSonucBean() {
		return testKaynakSonucBean;
	}

	public void setTestKaynakSonucBean(TestKaynakSonucBean testKaynakSonucBean) {
		this.testKaynakSonucBean = testKaynakSonucBean;
	}

	public TestKimyasalAnalizSonucBean getTestKimyasalAnalizSonucBean() {
		return testKimyasalAnalizSonucBean;
	}

	public void setTestKimyasalAnalizSonucBean(
			TestKimyasalAnalizSonucBean testKimyasalAnalizSonucBean) {
		this.testKimyasalAnalizSonucBean = testKimyasalAnalizSonucBean;
	}

	public TestKaplamaDarbeSonucBean getTestKaplamaDarbeSonucBean() {
		return testKaplamaDarbeSonucBean;
	}

	public void setTestKaplamaDarbeSonucBean(
			TestKaplamaDarbeSonucBean testKaplamaDarbeSonucBean) {
		this.testKaplamaDarbeSonucBean = testKaplamaDarbeSonucBean;
	}

	public TestKaplamaBukmeSonucBean getTestKaplamaBukmeSonucBean() {
		return testKaplamaBukmeSonucBean;
	}

	public void setTestKaplamaBukmeSonucBean(
			TestKaplamaBukmeSonucBean testKaplamaBukmeSonucBean) {
		this.testKaplamaBukmeSonucBean = testKaplamaBukmeSonucBean;
	}

	public TestKaplamaTozBoyaSonucBean getTestKaplamaTozBoyaSonucBean() {
		return testKaplamaTozBoyaSonucBean;
	}

	public void setTestKaplamaTozBoyaSonucBean(
			TestKaplamaTozBoyaSonucBean testKaplamaTozBoyaSonucBean) {
		this.testKaplamaTozBoyaSonucBean = testKaplamaTozBoyaSonucBean;
	}

	public TestKaplamaYapismaSonucBean getTestKaplamaYapismaSonucBean() {
		return testKaplamaYapismaSonucBean;
	}

	public void setTestKaplamaYapismaSonucBean(
			TestKaplamaYapismaSonucBean testKaplamaYapismaSonucBean) {
		this.testKaplamaYapismaSonucBean = testKaplamaYapismaSonucBean;
	}

	public TestPolyolefinUzamaSonucBean getTestPolyolefinUzamaSonucBean() {
		return testPolyolefinUzamaSonucBean;
	}

	public void setTestPolyolefinUzamaSonucBean(
			TestPolyolefinUzamaSonucBean testPolyolefinUzamaSonucBean) {
		this.testPolyolefinUzamaSonucBean = testPolyolefinUzamaSonucBean;
	}

	public TestKaplamaSertlikSonucBean getTestKaplamaSertlikSonucBean() {
		return testKaplamaSertlikSonucBean;
	}

	public void setTestKaplamaSertlikSonucBean(
			TestKaplamaSertlikSonucBean testKaplamaSertlikSonucBean) {
		this.testKaplamaSertlikSonucBean = testKaplamaSertlikSonucBean;
	}

	public TestKaplamaKalinligiSonucBean getTestKaplamaKalinligiSonucBean() {
		return testKaplamaKalinligiSonucBean;
	}

	public void setTestKaplamaKalinligiSonucBean(
			TestKaplamaKalinligiSonucBean testKaplamaKalinligiSonucBean) {
		this.testKaplamaKalinligiSonucBean = testKaplamaKalinligiSonucBean;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public AbstractTestsSpec getIsolationTestsDetail() {
		return isolationTestsDetail;
	}

	public void setIsolationTestsDetail(AbstractTestsSpec isolationTestsDetail) {
		this.isolationTestsDetail = isolationTestsDetail;
	}

	public IsolationTestDefinition getIsolationTestDefinition() {
		return isolationTestDefinition;
	}

	public void setIsolationTestDefinition(
			IsolationTestDefinition isolationTestDefinition) {
		this.isolationTestDefinition = isolationTestDefinition;
	}

	public List<IsolationTestDefinition> getInternalIsolationTestDefinitions() {
		return internalIsolationTestDefinitions;
	}

	public void setInternalIsolationTestDefinitions(
			List<IsolationTestDefinition> internalIsolationTestDefinitions) {
		this.internalIsolationTestDefinitions = internalIsolationTestDefinitions;
	}

	public List<IsolationTestDefinition> getExternalIsolationTestDefinitions() {
		return externalIsolationTestDefinitions;
	}

	public void setExternalIsolationTestDefinitions(
			List<IsolationTestDefinition> externalIsolationTestDefinitions) {
		this.externalIsolationTestDefinitions = externalIsolationTestDefinitions;
	}

	private IsolationTestType[] externalIsolationTypeCodes = new IsolationTestType[] {
			IsolationTestType.CSC1, IsolationTestType.CSP1,
			IsolationTestType.CSD1, IsolationTestType.CSS1,
			IsolationTestType.CAS1, IsolationTestType.CAC1,
			IsolationTestType.CKT1, IsolationTestType.CBT1,
			IsolationTestType.CDS1, IsolationTestType.CCT1,
			IsolationTestType.OHT1, IsolationTestType.CIT1,
			IsolationTestType.CIT2, IsolationTestType.CET1,
			IsolationTestType.CAT1, IsolationTestType.CCD1,
			IsolationTestType.CHT2, IsolationTestType.CFI1,
			IsolationTestType.DKK1, IsolationTestType.CKK1,
			IsolationTestType.KSK1, IsolationTestType.CPP1,
			IsolationTestType.CPP2, IsolationTestType.CKB1,
			IsolationTestType.CKT2, IsolationTestType.CGK1,
			IsolationTestType.CED, IsolationTestType.CTY,
			IsolationTestType.CST, IsolationTestType.CUV };

	private IsolationTestType[] internalIsolationTypeCodes = new IsolationTestType[] {
			IsolationTestType.LCT1, IsolationTestType.LSC1,
			IsolationTestType.LSP1, IsolationTestType.LSD1,
			IsolationTestType.LSS1, IsolationTestType.LAS1,
			IsolationTestType.LAC1, IsolationTestType.LBT1,
			IsolationTestType.LHT1, IsolationTestType.LIT1,
			IsolationTestType.LAT1, IsolationTestType.LIT2,
			IsolationTestType.LCD1, IsolationTestType.LHT2,
			IsolationTestType.LFI1, IsolationTestType.BCB1,
			IsolationTestType.LBZ1, IsolationTestType.EXC1,
			IsolationTestType.BSC1, IsolationTestType.POY1,
			IsolationTestType.BYH1, IsolationTestType.BGK1,
			IsolationTestType.BKK1, IsolationTestType.LKB1,
			IsolationTestType.LKK1, IsolationTestType.LKT1,
			IsolationTestType.LSK1, IsolationTestType.LDT1,
			IsolationTestType.FPT1, IsolationTestType.FBH1,
			IsolationTestType.FFT1, IsolationTestType.FCT1,
			IsolationTestType.FBT1, IsolationTestType.FWT1,
			IsolationTestType.FAT1, IsolationTestType.FST1,
			IsolationTestType.LGB, IsolationTestType.LHB, IsolationTestType.LMP };

	public IsolationTestType[] getExternalIsolationTypeCodes() {
		return externalIsolationTypeCodes;
	}

	public void setExternalIsolationTypeCodes(
			IsolationTestType[] externalIsolationTypeCodes) {
		this.externalIsolationTypeCodes = externalIsolationTypeCodes;
	}

	public IsolationTestType[] getInternalIsolationTypeCodes() {
		return internalIsolationTypeCodes;
	}

	public void setInternalIsolationTypeCodes(
			IsolationTestType[] internalIsolationTypeCodes) {
		this.internalIsolationTypeCodes = internalIsolationTypeCodes;
	}

	public IsolationTestDefinition getSelectedIsolation() {
		return selectedIsolation;
	}

	public void setSelectedIsolation(IsolationTestDefinition selectedIsolation) {
		this.selectedIsolation = selectedIsolation;
	}

	public NondestructiveTestsSpec getSelectedNonTest() {
		return selectedNonTest;
	}

	public void setSelectedNonTest(NondestructiveTestsSpec selectedNonTest) {
		this.selectedNonTest = selectedNonTest;
	}

	public TestTahribatsizOtomatikUtSonucBean getTestTahribatsizOtomatikUtSonucBean() {
		return testTahribatsizOtomatikUtSonucBean;
	}

	public void setTestTahribatsizOtomatikUtSonucBean(
			TestTahribatsizOtomatikUtSonucBean testTahribatsizOtomatikUtSonucBean) {
		this.testTahribatsizOtomatikUtSonucBean = testTahribatsizOtomatikUtSonucBean;
	}

	public TestTahribatsizFloroskopikSonucBean getTestTahribatsizFloroskopikSonucBean() {
		return testTahribatsizFloroskopikSonucBean;
	}

	/**
	 * @return the testKaplamaBetonYuzeyHazirligiSonucBean
	 */
	public TestKaplamaBetonYuzeyHazirligiSonucBean getTestKaplamaBetonYuzeyHazirligiSonucBean() {
		return testKaplamaBetonYuzeyHazirligiSonucBean;
	}

	/**
	 * @param testKaplamaBetonYuzeyHazirligiSonucBean
	 *            the testKaplamaBetonYuzeyHazirligiSonucBean to set
	 */
	public void setTestKaplamaBetonYuzeyHazirligiSonucBean(
			TestKaplamaBetonYuzeyHazirligiSonucBean testKaplamaBetonYuzeyHazirligiSonucBean) {
		this.testKaplamaBetonYuzeyHazirligiSonucBean = testKaplamaBetonYuzeyHazirligiSonucBean;
	}

	public void setTestTahribatsizFloroskopikSonucBean(
			TestTahribatsizFloroskopikSonucBean testTahribatsizFloroskopikSonucBean) {
		this.testTahribatsizFloroskopikSonucBean = testTahribatsizFloroskopikSonucBean;
	}

	public SalesItem getSelectedSalesItem() {
		return selectedSalesItem;
	}

	public void setSelectedSalesItem(SalesItem selectedSalesItem) {
		this.selectedSalesItem = selectedSalesItem;
	}

	public List<SalesItem> getItems() {
		return items;
	}

	public void setItems(List<SalesItem> items) {
		this.items = items;
	}

	public SalesCustomer getSelectedSalesCustomer() {
		return selectedSalesCustomer;
	}

	public void setSelectedSalesCustomer(SalesCustomer selectedSalesCustomer) {
		this.selectedSalesCustomer = selectedSalesCustomer;
	}

	public String getBarkodNo() {
		return barkodNo;
	}

	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	public List<Pipe> getTahribatliTestYapilmisBorular() {
		return tahribatliTestYapilmisBorular;
	}

	public void setTahribatliTestYapilmisBorular(
			List<Pipe> tahribatliTestYapilmisBorular) {
		this.tahribatliTestYapilmisBorular = tahribatliTestYapilmisBorular;
	}

	public BoruKaliteKaplamaTurleri getBoruKaliteKaplamaTurleri() {
		return boruKaliteKaplamaTurleri;
	}

	public void setBoruKaliteKaplamaTurleri(
			BoruKaliteKaplamaTurleri boruKaliteKaplamaTurleri) {
		this.boruKaliteKaplamaTurleri = boruKaliteKaplamaTurleri;
	}

	/**
	 * @return the selectedMarkalama
	 */
	public KaliteKaplamaMarkalama getSelectedMarkalama() {
		return selectedMarkalama;
	}

	/**
	 * @param selectedMarkalama
	 *            the selectedMarkalama to set
	 */
	public void setSelectedMarkalama(KaliteKaplamaMarkalama selectedMarkalama) {
		this.selectedMarkalama = selectedMarkalama;
	}

	/**
	 * @return the selectedKaynakAgziKoruma
	 */
	public KaplamaKaliteKaynakAgziKoruma getSelectedKaynakAgziKoruma() {
		return selectedKaynakAgziKoruma;
	}

	/**
	 * @param selectedKaynakAgziKoruma
	 *            the selectedKaynakAgziKoruma to set
	 */
	public void setSelectedKaynakAgziKoruma(
			KaplamaKaliteKaynakAgziKoruma selectedKaynakAgziKoruma) {
		this.selectedKaynakAgziKoruma = selectedKaynakAgziKoruma;
	}

	/**
	 * @return the selectedCutBack
	 */
	public KaplamaKaliteCutBackKoruma getSelectedCutBack() {
		return selectedCutBack;
	}

	/**
	 * @param selectedCutBack
	 *            the selectedCutBack to set
	 */
	public void setSelectedCutBack(KaplamaKaliteCutBackKoruma selectedCutBack) {
		this.selectedCutBack = selectedCutBack;
	}

	/**
	 * @return the bandEkiPipe
	 */
	public Pipe getBandEkiPipe() {
		return bandEkiPipe;
	}

	/**
	 * @param bandEkiPipe
	 *            the bandEkiPipe to set
	 */
	public void setBandEkiPipe(Pipe bandEkiPipe) {
		this.bandEkiPipe = bandEkiPipe;
	}

	/**
	 * @return the testKaplamaKumlamaOncesiOnIsitmaSonucBean
	 */
	public TestKaplamaKumlamaOncesiOnIsitmaSonucBean getTestKaplamaKumlamaOncesiOnIsitmaSonucBean() {
		return testKaplamaKumlamaOncesiOnIsitmaSonucBean;
	}

	/**
	 * @param testKaplamaKumlamaOncesiOnIsitmaSonucBean
	 *            the testKaplamaKumlamaOncesiOnIsitmaSonucBean to set
	 */
	public void setTestKaplamaKumlamaOncesiOnIsitmaSonucBean(
			TestKaplamaKumlamaOncesiOnIsitmaSonucBean testKaplamaKumlamaOncesiOnIsitmaSonucBean) {
		this.testKaplamaKumlamaOncesiOnIsitmaSonucBean = testKaplamaKumlamaOncesiOnIsitmaSonucBean;
	}

	/**
	 * @return the testKaplamaUygulamaSicaklikSonucBean
	 */
	public TestKaplamaUygulamaSicaklikSonucBean getTestKaplamaUygulamaSicaklikSonucBean() {
		return testKaplamaUygulamaSicaklikSonucBean;
	}

	/**
	 * @param testKaplamaUygulamaSicaklikSonucBean
	 *            the testKaplamaUygulamaSicaklikSonucBean to set
	 */
	public void setTestKaplamaUygulamaSicaklikSonucBean(
			TestKaplamaUygulamaSicaklikSonucBean testKaplamaUygulamaSicaklikSonucBean) {
		this.testKaplamaUygulamaSicaklikSonucBean = testKaplamaUygulamaSicaklikSonucBean;
	}

	/**
	 * @return the testKaplamaTamirTestSonucBean
	 */
	public TestKaplamaTamirTestSonucBean getTestKaplamaTamirTestSonucBean() {
		return testKaplamaTamirTestSonucBean;
	}

	/**
	 * @param testKaplamaTamirTestSonucBean
	 *            the testKaplamaTamirTestSonucBean to set
	 */
	public void setTestKaplamaTamirTestSonucBean(
			TestKaplamaTamirTestSonucBean testKaplamaTamirTestSonucBean) {
		this.testKaplamaTamirTestSonucBean = testKaplamaTamirTestSonucBean;
	}

	/**
	 * @return the testKaplamaKaplamasizBolgeSonucBean
	 */
	public TestKaplamaKaplamasizBolgeSonucBean getTestKaplamaKaplamasizBolgeSonucBean() {
		return testKaplamaKaplamasizBolgeSonucBean;
	}

	/**
	 * @param testKaplamaKaplamasizBolgeSonucBean
	 *            the testKaplamaKaplamasizBolgeSonucBean to set
	 */
	public void setTestKaplamaKaplamasizBolgeSonucBean(
			TestKaplamaKaplamasizBolgeSonucBean testKaplamaKaplamasizBolgeSonucBean) {
		this.testKaplamaKaplamasizBolgeSonucBean = testKaplamaKaplamasizBolgeSonucBean;
	}

	/**
	 * @return the testKaplamaNihaiKontrolSonucBean
	 */
	public TestKaplamaNihaiKontrolSonucBean getTestKaplamaNihaiKontrolSonucBean() {
		return testKaplamaNihaiKontrolSonucBean;
	}

	/**
	 * @param testKaplamaNihaiKontrolSonucBean
	 *            the testKaplamaNihaiKontrolSonucBean to set
	 */
	public void setTestKaplamaNihaiKontrolSonucBean(
			TestKaplamaNihaiKontrolSonucBean testKaplamaNihaiKontrolSonucBean) {
		this.testKaplamaNihaiKontrolSonucBean = testKaplamaNihaiKontrolSonucBean;
	}

	/**
	 * @return the testKaplamaGozKontrolSonucBean
	 */
	public TestKaplamaGozKontrolSonucBean getTestKaplamaGozKontrolSonucBean() {
		return testKaplamaGozKontrolSonucBean;
	}

	/**
	 * @param testKaplamaGozKontrolSonucBean
	 *            the testKaplamaGozKontrolSonucBean to set
	 */
	public void setTestKaplamaGozKontrolSonucBean(
			TestKaplamaGozKontrolSonucBean testKaplamaGozKontrolSonucBean) {
		this.testKaplamaGozKontrolSonucBean = testKaplamaGozKontrolSonucBean;
	}

	/**
	 * @return the testKaplamaBetonOranBelirlenmesiSonucBean
	 */
	public TestKaplamaBetonOranBelirlenmesiSonucBean getTestKaplamaBetonOranBelirlenmesiSonucBean() {
		return testKaplamaBetonOranBelirlenmesiSonucBean;
	}

	/**
	 * @param testKaplamaBetonOranBelirlenmesiSonucBean
	 *            the testKaplamaBetonOranBelirlenmesiSonucBean to set
	 */
	public void setTestKaplamaBetonOranBelirlenmesiSonucBean(
			TestKaplamaBetonOranBelirlenmesiSonucBean testKaplamaBetonOranBelirlenmesiSonucBean) {
		this.testKaplamaBetonOranBelirlenmesiSonucBean = testKaplamaBetonOranBelirlenmesiSonucBean;
	}

	/**
	 * @return the testKaplamaKumlamaOrtamSartlariSonucBean
	 */
	public TestKaplamaKumlamaOrtamSartlariSonucBean getTestKaplamaKumlamaOrtamSartlariSonucBean() {
		return testKaplamaKumlamaOrtamSartlariSonucBean;
	}

	/**
	 * @param testKaplamaKumlamaOrtamSartlariSonucBean
	 *            the testKaplamaKumlamaOrtamSartlariSonucBean to set
	 */
	public void setTestKaplamaKumlamaOrtamSartlariSonucBean(
			TestKaplamaKumlamaOrtamSartlariSonucBean testKaplamaKumlamaOrtamSartlariSonucBean) {
		this.testKaplamaKumlamaOrtamSartlariSonucBean = testKaplamaKumlamaOrtamSartlariSonucBean;
	}

	/**
	 * @return the testKaplamaBetonEgilmeBasincSonucBean
	 */
	public TestKaplamaBetonEgilmeBasincSonucBean getTestKaplamaBetonEgilmeBasincSonucBean() {
		return testKaplamaBetonEgilmeBasincSonucBean;
	}

	/**
	 * @param testKaplamaBetonEgilmeBasincSonucBean
	 *            the testKaplamaBetonEgilmeBasincSonucBean to set
	 */
	public void setTestKaplamaBetonEgilmeBasincSonucBean(
			TestKaplamaBetonEgilmeBasincSonucBean testKaplamaBetonEgilmeBasincSonucBean) {
		this.testKaplamaBetonEgilmeBasincSonucBean = testKaplamaBetonEgilmeBasincSonucBean;
	}

	/**
	 * @return the testKaplamaKumlanmisBoruTozMiktariSonucBean
	 */
	public TestKaplamaKumlanmisBoruTozMiktariSonucBean getTestKaplamaKumlanmisBoruTozMiktariSonucBean() {
		return testKaplamaKumlanmisBoruTozMiktariSonucBean;
	}

	/**
	 * @param testKaplamaKumlanmisBoruTozMiktariSonucBean
	 *            the testKaplamaKumlanmisBoruTozMiktariSonucBean to set
	 */
	public void setTestKaplamaKumlanmisBoruTozMiktariSonucBean(
			TestKaplamaKumlanmisBoruTozMiktariSonucBean testKaplamaKumlanmisBoruTozMiktariSonucBean) {
		this.testKaplamaKumlanmisBoruTozMiktariSonucBean = testKaplamaKumlanmisBoruTozMiktariSonucBean;
	}

	/**
	 * @return the testKaplamaKumlamaMalzemesiTuzMiktariSonucBean
	 */
	public TestKaplamaKumlamaMalzemesiTuzMiktariSonucBean getTestKaplamaKumlamaMalzemesiTuzMiktariSonucBean() {
		return testKaplamaKumlamaMalzemesiTuzMiktariSonucBean;
	}

	/**
	 * @param testKaplamaKumlamaMalzemesiTuzMiktariSonucBean
	 *            the testKaplamaKumlamaMalzemesiTuzMiktariSonucBean to set
	 */
	public void setTestKaplamaKumlamaMalzemesiTuzMiktariSonucBean(
			TestKaplamaKumlamaMalzemesiTuzMiktariSonucBean testKaplamaKumlamaMalzemesiTuzMiktariSonucBean) {
		this.testKaplamaKumlamaMalzemesiTuzMiktariSonucBean = testKaplamaKumlamaMalzemesiTuzMiktariSonucBean;
	}

	/**
	 * @return the testKaplamaKumlanmisBoruTuzMiktariSonucBean
	 */
	public TestKaplamaKumlanmisBoruTuzMiktariSonucBean getTestKaplamaKumlanmisBoruTuzMiktariSonucBean() {
		return testKaplamaKumlanmisBoruTuzMiktariSonucBean;
	}

	/**
	 * @param testKaplamaKumlanmisBoruTuzMiktariSonucBean
	 *            the testKaplamaKumlanmisBoruTuzMiktariSonucBean to set
	 */
	public void setTestKaplamaKumlanmisBoruTuzMiktariSonucBean(
			TestKaplamaKumlanmisBoruTuzMiktariSonucBean testKaplamaKumlanmisBoruTuzMiktariSonucBean) {
		this.testKaplamaKumlanmisBoruTuzMiktariSonucBean = testKaplamaKumlanmisBoruTuzMiktariSonucBean;
	}

	/**
	 * @return the testKaplamaBuchholzSonucBean
	 */
	public TestKaplamaBuchholzSonucBean getTestKaplamaBuchholzSonucBean() {
		return testKaplamaBuchholzSonucBean;
	}

	/**
	 * @param testKaplamaBuchholzSonucBean
	 *            the testKaplamaBuchholzSonucBean to set
	 */
	public void setTestKaplamaBuchholzSonucBean(
			TestKaplamaBuchholzSonucBean testKaplamaBuchholzSonucBean) {
		this.testKaplamaBuchholzSonucBean = testKaplamaBuchholzSonucBean;
	}

	/**
	 * @return the testKaplamaKumlanmisBoruYuzeyTemizligiSonucBean
	 */
	public TestKaplamaKumlanmisBoruYuzeyTemizligiSonucBean getTestKaplamaKumlanmisBoruYuzeyTemizligiSonucBean() {
		return testKaplamaKumlanmisBoruYuzeyTemizligiSonucBean;
	}

	/**
	 * @param testKaplamaKumlanmisBoruYuzeyTemizligiSonucBean
	 *            the testKaplamaKumlanmisBoruYuzeyTemizligiSonucBean to set
	 */
	public void setTestKaplamaKumlanmisBoruYuzeyTemizligiSonucBean(
			TestKaplamaKumlanmisBoruYuzeyTemizligiSonucBean testKaplamaKumlanmisBoruYuzeyTemizligiSonucBean) {
		this.testKaplamaKumlanmisBoruYuzeyTemizligiSonucBean = testKaplamaKumlanmisBoruYuzeyTemizligiSonucBean;
	}

	/**
	 * @return the testKaplamaProcessBozulmaSonucBean
	 */
	public TestKaplamaProcessBozulmaSonucBean getTestKaplamaProcessBozulmaSonucBean() {
		return testKaplamaProcessBozulmaSonucBean;
	}

	/**
	 * @param testKaplamaProcessBozulmaSonucBean
	 *            the testKaplamaProcessBozulmaSonucBean to set
	 */
	public void setTestKaplamaProcessBozulmaSonucBean(
			TestKaplamaProcessBozulmaSonucBean testKaplamaProcessBozulmaSonucBean) {
		this.testKaplamaProcessBozulmaSonucBean = testKaplamaProcessBozulmaSonucBean;
	}

	/**
	 * @return the testKaplamaBetonGozKontrolSonucBean
	 */
	public TestKaplamaBetonGozKontrolSonucBean getTestKaplamaBetonGozKontrolSonucBean() {
		return testKaplamaBetonGozKontrolSonucBean;
	}

	/**
	 * @param testKaplamaBetonGozKontrolSonucBean
	 *            the testKaplamaBetonGozKontrolSonucBean to set
	 */
	public void setTestKaplamaBetonGozKontrolSonucBean(
			TestKaplamaBetonGozKontrolSonucBean testKaplamaBetonGozKontrolSonucBean) {
		this.testKaplamaBetonGozKontrolSonucBean = testKaplamaBetonGozKontrolSonucBean;
	}

	/**
	 * @return the testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucBean
	 */
	public TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonucBean getTestKaplamaKumlanmisBoruYuzeyPuruzluluguSonucBean() {
		return testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucBean;
	}

	/**
	 * @param testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucBean
	 *            the testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucBean to set
	 */
	public void setTestKaplamaKumlanmisBoruYuzeyPuruzluluguSonucBean(
			TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonucBean testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucBean) {
		this.testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucBean = testKaplamaKumlanmisBoruYuzeyPuruzluluguSonucBean;
	}

	/**
	 * @return the testKaplamaHolidayPinholeSonucBean
	 */
	public TestKaplamaHolidayPinholeSonucBean getTestKaplamaHolidayPinholeSonucBean() {
		return testKaplamaHolidayPinholeSonucBean;
	}

	/**
	 * @param testKaplamaHolidayPinholeSonucBean
	 *            the testKaplamaHolidayPinholeSonucBean to set
	 */
	public void setTestKaplamaHolidayPinholeSonucBean(
			TestKaplamaHolidayPinholeSonucBean testKaplamaHolidayPinholeSonucBean) {
		this.testKaplamaHolidayPinholeSonucBean = testKaplamaHolidayPinholeSonucBean;
	}

	/**
	 * @return the testKaplamaOnlineHolidaySonucBean
	 */
	public TestKaplamaOnlineHolidaySonucBean getTestKaplamaOnlineHolidaySonucBean() {
		return testKaplamaOnlineHolidaySonucBean;
	}

	/**
	 * @param testKaplamaOnlineHolidaySonucBean
	 *            the testKaplamaOnlineHolidaySonucBean to set
	 */
	public void setTestKaplamaOnlineHolidaySonucBean(
			TestKaplamaOnlineHolidaySonucBean testKaplamaOnlineHolidaySonucBean) {
		this.testKaplamaOnlineHolidaySonucBean = testKaplamaOnlineHolidaySonucBean;
	}

	/**
	 * @return the testKaplamaTozEpoksiXcutSonucBean
	 */
	public TestKaplamaTozEpoksiXcutSonucBean getTestKaplamaTozEpoksiXcutSonucBean() {
		return testKaplamaTozEpoksiXcutSonucBean;
	}

	/**
	 * @param testKaplamaTozEpoksiXcutSonucBean
	 *            the testKaplamaTozEpoksiXcutSonucBean to set
	 */
	public void setTestKaplamaTozEpoksiXcutSonucBean(
			TestKaplamaTozEpoksiXcutSonucBean testKaplamaTozEpoksiXcutSonucBean) {
		this.testKaplamaTozEpoksiXcutSonucBean = testKaplamaTozEpoksiXcutSonucBean;
	}

	/**
	 * @return the testKaplamaFlowCoatBendSonucBean
	 */
	public TestKaplamaFlowCoatBendSonucBean getTestKaplamaFlowCoatBendSonucBean() {
		return testKaplamaFlowCoatBendSonucBean;
	}

	/**
	 * @param testKaplamaFlowCoatBendSonucBean
	 *            the testKaplamaFlowCoatBendSonucBean to set
	 */
	public void setTestKaplamaFlowCoatBendSonucBean(
			TestKaplamaFlowCoatBendSonucBean testKaplamaFlowCoatBendSonucBean) {
		this.testKaplamaFlowCoatBendSonucBean = testKaplamaFlowCoatBendSonucBean;
	}

	/**
	 * @return the testKaplamaFlowCoatBondSonucBean
	 */
	public TestKaplamaFlowCoatBondSonucBean getTestKaplamaFlowCoatBondSonucBean() {
		return testKaplamaFlowCoatBondSonucBean;
	}

	/**
	 * @param testKaplamaFlowCoatBondSonucBean
	 *            the testKaplamaFlowCoatBondSonucBean to set
	 */
	public void setTestKaplamaFlowCoatBondSonucBean(
			TestKaplamaFlowCoatBondSonucBean testKaplamaFlowCoatBondSonucBean) {
		this.testKaplamaFlowCoatBondSonucBean = testKaplamaFlowCoatBondSonucBean;
	}

	/**
	 * @return the testKaplamaFlowCoatBuchholzSonucBean
	 */
	public TestKaplamaFlowCoatBuchholzSonucBean getTestKaplamaFlowCoatBuchholzSonucBean() {
		return testKaplamaFlowCoatBuchholzSonucBean;
	}

	/**
	 * @param testKaplamaFlowCoatBuchholzSonucBean
	 *            the testKaplamaFlowCoatBuchholzSonucBean to set
	 */
	public void setTestKaplamaFlowCoatBuchholzSonucBean(
			TestKaplamaFlowCoatBuchholzSonucBean testKaplamaFlowCoatBuchholzSonucBean) {
		this.testKaplamaFlowCoatBuchholzSonucBean = testKaplamaFlowCoatBuchholzSonucBean;
	}

	/**
	 * @return the testKaplamaFlowCoatCureSonucBean
	 */
	public TestKaplamaFlowCoatCureSonucBean getTestKaplamaFlowCoatCureSonucBean() {
		return testKaplamaFlowCoatCureSonucBean;
	}

	/**
	 * @param testKaplamaFlowCoatCureSonucBean
	 *            the testKaplamaFlowCoatCureSonucBean to set
	 */
	public void setTestKaplamaFlowCoatCureSonucBean(
			TestKaplamaFlowCoatCureSonucBean testKaplamaFlowCoatCureSonucBean) {
		this.testKaplamaFlowCoatCureSonucBean = testKaplamaFlowCoatCureSonucBean;
	}

	/**
	 * @return the testKaplamaFlowCoatFilmThicknessSonucBean
	 */
	public TestKaplamaFlowCoatFilmThicknessSonucBean getTestKaplamaFlowCoatFilmThicknessSonucBean() {
		return testKaplamaFlowCoatFilmThicknessSonucBean;
	}

	/**
	 * @param testKaplamaFlowCoatFilmThicknessSonucBean
	 *            the testKaplamaFlowCoatFilmThicknessSonucBean to set
	 */
	public void setTestKaplamaFlowCoatFilmThicknessSonucBean(
			TestKaplamaFlowCoatFilmThicknessSonucBean testKaplamaFlowCoatFilmThicknessSonucBean) {
		this.testKaplamaFlowCoatFilmThicknessSonucBean = testKaplamaFlowCoatFilmThicknessSonucBean;
	}

	/**
	 * @return the testKaplamaFlowCoatPinholeSonucBean
	 */
	public TestKaplamaFlowCoatPinholeSonucBean getTestKaplamaFlowCoatPinholeSonucBean() {
		return testKaplamaFlowCoatPinholeSonucBean;
	}

	/**
	 * @param testKaplamaFlowCoatPinholeSonucBean
	 *            the testKaplamaFlowCoatPinholeSonucBean to set
	 */
	public void setTestKaplamaFlowCoatPinholeSonucBean(
			TestKaplamaFlowCoatPinholeSonucBean testKaplamaFlowCoatPinholeSonucBean) {
		this.testKaplamaFlowCoatPinholeSonucBean = testKaplamaFlowCoatPinholeSonucBean;
	}

	/**
	 * @return the testKaplamaFlowCoatStrippingSonucBean
	 */
	public TestKaplamaFlowCoatStrippingSonucBean getTestKaplamaFlowCoatStrippingSonucBean() {
		return testKaplamaFlowCoatStrippingSonucBean;
	}

	/**
	 * @param testKaplamaFlowCoatStrippingSonucBean
	 *            the testKaplamaFlowCoatStrippingSonucBean to set
	 */
	public void setTestKaplamaFlowCoatStrippingSonucBean(
			TestKaplamaFlowCoatStrippingSonucBean testKaplamaFlowCoatStrippingSonucBean) {
		this.testKaplamaFlowCoatStrippingSonucBean = testKaplamaFlowCoatStrippingSonucBean;
	}

	/**
	 * @return the testKaplamaFlowCoatWaterSonucBean
	 */
	public TestKaplamaFlowCoatWaterSonucBean getTestKaplamaFlowCoatWaterSonucBean() {
		return testKaplamaFlowCoatWaterSonucBean;
	}

	/**
	 * @param testKaplamaFlowCoatWaterSonucBean
	 *            the testKaplamaFlowCoatWaterSonucBean to set
	 */
	public void setTestKaplamaFlowCoatWaterSonucBean(
			TestKaplamaFlowCoatWaterSonucBean testKaplamaFlowCoatWaterSonucBean) {
		this.testKaplamaFlowCoatWaterSonucBean = testKaplamaFlowCoatWaterSonucBean;
	}

	/**
	 * @return the testKaplamaSonrasiYuzeyKontroluSonucBean
	 */
	public TestKaplamaSonrasiYuzeyKontroluSonucBean getTestKaplamaSonrasiYuzeyKontroluSonucBean() {
		return testKaplamaSonrasiYuzeyKontroluSonucBean;
	}

	/**
	 * @param testKaplamaSonrasiYuzeyKontroluSonucBean
	 *            the testKaplamaSonrasiYuzeyKontroluSonucBean to set
	 */
	public void setTestKaplamaSonrasiYuzeyKontroluSonucBean(
			TestKaplamaSonrasiYuzeyKontroluSonucBean testKaplamaSonrasiYuzeyKontroluSonucBean) {
		this.testKaplamaSonrasiYuzeyKontroluSonucBean = testKaplamaSonrasiYuzeyKontroluSonucBean;
	}

	/**
	 * @return the testKaplamaPulloffYapismaSonucBean
	 */
	public TestKaplamaPulloffYapismaSonucBean getTestKaplamaPulloffYapismaSonucBean() {
		return testKaplamaPulloffYapismaSonucBean;
	}

	/**
	 * @param testKaplamaPulloffYapismaSonucBean
	 *            the testKaplamaPulloffYapismaSonucBean to set
	 */
	public void setTestKaplamaPulloffYapismaSonucBean(
			TestKaplamaPulloffYapismaSonucBean testKaplamaPulloffYapismaSonucBean) {
		this.testKaplamaPulloffYapismaSonucBean = testKaplamaPulloffYapismaSonucBean;
	}

	/**
	 * @return the testKaplamaOncesiYuzeyKontroluSonucBean
	 */
	public TestKaplamaOncesiYuzeyKontroluSonucBean getTestKaplamaOncesiYuzeyKontroluSonucBean() {
		return testKaplamaOncesiYuzeyKontroluSonucBean;
	}

	/**
	 * @param testKaplamaOncesiYuzeyKontroluSonucBean
	 *            the testKaplamaOncesiYuzeyKontroluSonucBean to set
	 */
	public void setTestKaplamaOncesiYuzeyKontroluSonucBean(
			TestKaplamaOncesiYuzeyKontroluSonucBean testKaplamaOncesiYuzeyKontroluSonucBean) {
		this.testKaplamaOncesiYuzeyKontroluSonucBean = testKaplamaOncesiYuzeyKontroluSonucBean;
	}

	/**
	 * @return the testKaplamaMpqtKaplamaKalinligliSonucBean
	 */
	public TestKaplamaMpqtKaplamaKalinligliSonucBean getTestKaplamaMpqtKaplamaKalinligliSonucBean() {
		return testKaplamaMpqtKaplamaKalinligliSonucBean;
	}

	/**
	 * @param testKaplamaMpqtKaplamaKalinligliSonucBean
	 *            the testKaplamaMpqtKaplamaKalinligliSonucBean to set
	 */
	public void setTestKaplamaMpqtKaplamaKalinligliSonucBean(
			TestKaplamaMpqtKaplamaKalinligliSonucBean testKaplamaMpqtKaplamaKalinligliSonucBean) {
		this.testKaplamaMpqtKaplamaKalinligliSonucBean = testKaplamaMpqtKaplamaKalinligliSonucBean;
	}

	/**
	 * @return the testKaplamaDeliciUcDirencSonucBean
	 */
	public TestKaplamaDeliciUcDirencSonucBean getTestKaplamaDeliciUcDirencSonucBean() {
		return testKaplamaDeliciUcDirencSonucBean;
	}

	/**
	 * @param testKaplamaDeliciUcDirencSonucBean
	 *            the testKaplamaDeliciUcDirencSonucBean to set
	 */
	public void setTestKaplamaDeliciUcDirencSonucBean(
			TestKaplamaDeliciUcDirencSonucBean testKaplamaDeliciUcDirencSonucBean) {
		this.testKaplamaDeliciUcDirencSonucBean = testKaplamaDeliciUcDirencSonucBean;
	}

	/**
	 * @return the testKaplamaKatodikSoyulmaSonucBean
	 */
	public TestKaplamaKatodikSoyulmaSonucBean getTestKaplamaKatodikSoyulmaSonucBean() {
		return testKaplamaKatodikSoyulmaSonucBean;
	}

	/**
	 * @param testKaplamaKatodikSoyulmaSonucBean
	 *            the testKaplamaKatodikSoyulmaSonucBean to set
	 */
	public void setTestKaplamaKatodikSoyulmaSonucBean(
			TestKaplamaKatodikSoyulmaSonucBean testKaplamaKatodikSoyulmaSonucBean) {
		this.testKaplamaKatodikSoyulmaSonucBean = testKaplamaKatodikSoyulmaSonucBean;
	}

	/**
	 * @return the testKaplamaElektrikselDirencTestSonucBean
	 */
	public TestKaplamaElektrikselDirencTestSonucBean getTestKaplamaElektrikselDirencTestSonucBean() {
		return testKaplamaElektrikselDirencTestSonucBean;
	}

	/**
	 * @param testKaplamaElektrikselDirencTestSonucBean
	 *            the testKaplamaElektrikselDirencTestSonucBean to set
	 */
	public void setTestKaplamaElektrikselDirencTestSonucBean(
			TestKaplamaElektrikselDirencTestSonucBean testKaplamaElektrikselDirencTestSonucBean) {
		this.testKaplamaElektrikselDirencTestSonucBean = testKaplamaElektrikselDirencTestSonucBean;
	}

	/**
	 * @return the testKaplamaGasBlisteringTestSonucBean
	 */
	public TestKaplamaGasBlisteringTestSonucBean getTestKaplamaGasBlisteringTestSonucBean() {
		return testKaplamaGasBlisteringTestSonucBean;
	}

	/**
	 * @param testKaplamaGasBlisteringTestSonucBean
	 *            the testKaplamaGasBlisteringTestSonucBean to set
	 */
	public void setTestKaplamaGasBlisteringTestSonucBean(
			TestKaplamaGasBlisteringTestSonucBean testKaplamaGasBlisteringTestSonucBean) {
		this.testKaplamaGasBlisteringTestSonucBean = testKaplamaGasBlisteringTestSonucBean;
	}

	/**
	 * @return the testKaplamaHydraulicBlisteringTestSonucBean
	 */
	public TestKaplamaHydraulicBlisteringTestSonucBean getTestKaplamaHydraulicBlisteringTestSonucBean() {
		return testKaplamaHydraulicBlisteringTestSonucBean;
	}

	/**
	 * @param testKaplamaHydraulicBlisteringTestSonucBean
	 *            the testKaplamaHydraulicBlisteringTestSonucBean to set
	 */
	public void setTestKaplamaHydraulicBlisteringTestSonucBean(
			TestKaplamaHydraulicBlisteringTestSonucBean testKaplamaHydraulicBlisteringTestSonucBean) {
		this.testKaplamaHydraulicBlisteringTestSonucBean = testKaplamaHydraulicBlisteringTestSonucBean;
	}

	/**
	 * @return the testKaplamaMixedWetPaintTestSonucBean
	 */
	public TestKaplamaMixedWetPaintTestSonucBean getTestKaplamaMixedWetPaintTestSonucBean() {
		return testKaplamaMixedWetPaintTestSonucBean;
	}

	/**
	 * @param testKaplamaMixedWetPaintTestSonucBean
	 *            the testKaplamaMixedWetPaintTestSonucBean to set
	 */
	public void setTestKaplamaMixedWetPaintTestSonucBean(
			TestKaplamaMixedWetPaintTestSonucBean testKaplamaMixedWetPaintTestSonucBean) {
		this.testKaplamaMixedWetPaintTestSonucBean = testKaplamaMixedWetPaintTestSonucBean;
	}

	/**
	 * @return the testKaplamaStorageConditionTestSonucBean
	 */
	public TestKaplamaStorageConditionTestSonucBean getTestKaplamaStorageConditionTestSonucBean() {
		return testKaplamaStorageConditionTestSonucBean;
	}

	/**
	 * @param testKaplamaStorageConditionTestSonucBean
	 *            the testKaplamaStorageConditionTestSonucBean to set
	 */
	public void setTestKaplamaStorageConditionTestSonucBean(
			TestKaplamaStorageConditionTestSonucBean testKaplamaStorageConditionTestSonucBean) {
		this.testKaplamaStorageConditionTestSonucBean = testKaplamaStorageConditionTestSonucBean;
	}

	/**
	 * @return the testKaplamaThermalAgeingTestSonucBean
	 */
	public TestKaplamaThermalAgeingTestSonucBean getTestKaplamaThermalAgeingTestSonucBean() {
		return testKaplamaThermalAgeingTestSonucBean;
	}

	/**
	 * @param testKaplamaThermalAgeingTestSonucBean
	 *            the testKaplamaThermalAgeingTestSonucBean to set
	 */
	public void setTestKaplamaThermalAgeingTestSonucBean(
			TestKaplamaThermalAgeingTestSonucBean testKaplamaThermalAgeingTestSonucBean) {
		this.testKaplamaThermalAgeingTestSonucBean = testKaplamaThermalAgeingTestSonucBean;
	}

	/**
	 * @return the testKaplamaUvAgeingTestSonucBean
	 */
	public TestKaplamaUvAgeingTestSonucBean getTestKaplamaUvAgeingTestSonucBean() {
		return testKaplamaUvAgeingTestSonucBean;
	}

	/**
	 * @param testKaplamaUvAgeingTestSonucBean
	 *            the testKaplamaUvAgeingTestSonucBean to set
	 */
	public void setTestKaplamaUvAgeingTestSonucBean(
			TestKaplamaUvAgeingTestSonucBean testKaplamaUvAgeingTestSonucBean) {
		this.testKaplamaUvAgeingTestSonucBean = testKaplamaUvAgeingTestSonucBean;
	}

}
