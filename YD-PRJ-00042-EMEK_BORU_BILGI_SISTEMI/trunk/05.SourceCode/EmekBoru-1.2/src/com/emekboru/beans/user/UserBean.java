package com.emekboru.beans.user;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.persistence.EntityManager;

import com.emekboru.jpa.Department;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpaman.Factory;
import com.emekboru.jpaman.UserManager;
import com.emekboru.utils.FacesContextUtils;


@ManagedBean(name = "userBean")
@ViewScoped
public class UserBean implements Serializable{

	private static final long serialVersionUID = 1L;
	private SystemUser currentUser;
	private Department department;
	private SystemUser newUser;
	private String repeatedPassword;
	private String newPassword;

	public UserBean(){
		
		currentUser = new SystemUser();
		newUser = new SystemUser();
		repeatedPassword = new String();
		department = new Department();
		newPassword = new String();
	}
	
	public void cancelUserSubmittion(){
		
		System.out.println("UserBean.cancelUserSubmittion()");
		newUser = new SystemUser();
		repeatedPassword = new String();
	}
	
	public void addUser(ActionEvent event){
		
		System.out.println("UserBean.addUser()");
		UserManager userManager = new UserManager();
		List<SystemUser> userList = (List<SystemUser>) userManager
				.findByField(SystemUser.class, "username", newUser.getUsername());

		if(userList.size()>0){
			
			FacesContextUtils.addErrorMessage("ExistUsernameMessage");
			repeatedPassword = new String();
			newUser = new SystemUser();
			return; 
		}
			
		if(!newUser.getPassword().equals(repeatedPassword)){
			
			FacesContextUtils.addErrorMessage("NoPasswordMatch");
			repeatedPassword = new String();
			return;
		}
			
		newUser.setAdmin(false);
		userManager.enterNew(newUser);
		
		EntityManager manager = Factory.getInstance().createEntityManager();
		newUser = manager.find(SystemUser.class, newUser.getUsername());
		manager.refresh(newUser);
		manager.close();
		UserListBean.loadNoUserEmployeeList();
		UserListBean.loadUserList();
		newUser = new SystemUser();
		repeatedPassword = new String();
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void updateUser(ActionEvent event){
		
		System.out.println("UserBean.updateUser()");
		UserManager userManager = new UserManager();
		userManager.updateEntity(currentUser);
		UserListBean.loadUserList();
		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}
	
	public void deleteUser(ActionEvent event){
		
		System.out.println("UserBean.deleteUser()");
		if(currentUser.isAdmin()){
			
			FacesContextUtils.addErrorMessage("DeleteAdminMessage");
			return;			
		}

		UserManager manager = new UserManager();
		manager.delete(currentUser);
		currentUser = new SystemUser();
		department = new Department();
		UserListBean.loadUserList();
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}
	
	public void changeUserPassword(ActionEvent event){
		
		if(!newPassword.equals(repeatedPassword)){
			
			FacesContextUtils.addErrorMessage("NoPasswordMatch");
			return;
		}
		UserManager userManager = new UserManager();
		userManager.updateEntity(currentUser);
		newPassword = new String();
		repeatedPassword = new String();
		currentUser = (SystemUser) userManager.findByField(SystemUser.class, 
				"username" , currentUser.getUsername()).get(0);
		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}
	
	public void loadUserList(){
		
		UserListBean.loadUserList();
	}
	
	public void deactivateUser(){
		
		if(currentUser == null || currentUser.getUsername() ==null){
			
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		
		if(currentUser.isAdmin()){
			
			FacesContextUtils.addErrorMessage("AdminDeactivation");
			return;
		}
		
		currentUser.setIsActive(false);
		UserManager userManager = new UserManager();
		userManager.updateEntity(currentUser);
		FacesContextUtils.addErrorMessage("UserDeactivated");
	}
	
	public void activateUser(){
		
		if(currentUser == null || currentUser.getUsername() ==null){
			
			FacesContextUtils.addInfoMessage("NoSelectMessage");
			return;
		}
		
		currentUser.setIsActive(true);
		System.out.println("UserBean.activateUser()");
		UserManager userManager = new UserManager();
		userManager.updateEntity(currentUser);
		FacesContextUtils.addInfoMessage("UserActivated");
	}
	
	/**
	 * 			GETTERS AND SETTERS
	 */
	
	public SystemUser getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(SystemUser currentUser) {
		this.currentUser = currentUser;
	}

	public SystemUser getNewUser() {
		return newUser;
	}

	public void setNewUser(SystemUser newUser) {
		this.newUser = newUser;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getRepeatedPassword() {
		return repeatedPassword;
	}

	public void setRepeatedPassword(String repeatedPassword) {
		this.repeatedPassword = repeatedPassword;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
}
