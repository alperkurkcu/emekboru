/**
 * 
 */
package com.emekboru.beans.kaplamamakinesi;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiFircalama;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.kaplamamakinesi.KaplamaMakinesiFircalamaManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "kaplamaMakinesiFircalamaBean")
@ViewScoped
public class KaplamaMakinesiFircalamaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<KaplamaMakinesiFircalama> allKaplamaMakinesiFircalamaList = new ArrayList<KaplamaMakinesiFircalama>();
	private KaplamaMakinesiFircalama kaplamaMakinesiFircalamaForm = new KaplamaMakinesiFircalama();

	private KaplamaMakinesiFircalama newFircalama = new KaplamaMakinesiFircalama();
	private List<KaplamaMakinesiFircalama> fircalamaList;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	private Pipe selectedPipe = new Pipe();

	private boolean kontrol = false;

	private String barkodNo = null;

	public KaplamaMakinesiFircalamaBean() {

	}

	public void addFircalama(ActionEvent ae) {

		UICommand cmd = (UICommand) ae.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId != null) {
			PipeManager pipeMan = new PipeManager();
			selectedPipe = pipeMan.loadObject(Pipe.class, prmPipeId);
		}

		try {
			KaplamaMakinesiFircalamaManager manager = new KaplamaMakinesiFircalamaManager();

			newFircalama.setEklemeZamani(UtilInsCore.getTarihZaman());
			newFircalama.setEkleyenKullanici(userBean.getUser().getId());
			newFircalama.setPipe(selectedPipe);
			manager.enterNew(newFircalama);
			newFircalama = new KaplamaMakinesiFircalama();

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"FIRÇALAMA İŞLEMİ BAŞARIYLA EKLENMİŞTİR!"));

		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL,
					"FIRÇALAMA İŞLEMİ EKLENEMEDİ!", null));
			System.out
					.println("KaplamaMakinesiFircalamaBean.addFircalama-HATA");
		}

		fillTestList(prmPipeId);
	}

	public void addFircalamaSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		Boolean prmKontrol = kaplamaMakinesiFircalamaForm.getDurum();
		KaplamaMakinesiFircalamaManager fircalamaManager = new KaplamaMakinesiFircalamaManager();

		if (prmKontrol == null) {

			try {

				kaplamaMakinesiFircalamaForm.setDurum(false);
				kaplamaMakinesiFircalamaForm
						.setKaplamaBaslamaZamani(UtilInsCore.getTarihZaman());
				kaplamaMakinesiFircalamaForm
						.setKaplamaBaslamaKullanici(userBean.getUser().getId());
				fircalamaManager.updateEntity(kaplamaMakinesiFircalamaForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"FIRÇALAMA İŞLEMİ BAŞARIYLA BAŞLANMIŞTIR!"));
			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"FIRÇALAMA İŞLEMİ BAŞLANAMADI!", null));
				System.out
						.println("KaplamaMakinesiFircalamaBean.addFircalamaSonuc-HATA-BAŞLAMA");
			}
		} else if (prmKontrol == false) {

			try {

				kaplamaMakinesiFircalamaForm.setDurum(true);
				kaplamaMakinesiFircalamaForm.setKaplamaBitisZamani(UtilInsCore
						.getTarihZaman());
				kaplamaMakinesiFircalamaForm.setKaplamaBitisKullanici(userBean
						.getUser().getId());
				fircalamaManager.updateEntity(kaplamaMakinesiFircalamaForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"FIRÇALAMA İŞLEMİ BAŞARIYLA BİTİRİLMİŞTİR!"));

			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"FIRÇALAMA İŞLEMİ BİTİRİLEMEDİ!", null));
				System.out
						.println("KaplamaMakinesiFircalamaBean.addFircalamaSonuc-HATA-BİTİŞ"
								+ e.toString());
			}

		}

		fillTestList(prmPipeId);
	}

	@SuppressWarnings("static-access")
	public void fillTestList(Integer prmPipeId) {

		KaplamaMakinesiFircalamaManager fircalamaManager = new KaplamaMakinesiFircalamaManager();
		allKaplamaMakinesiFircalamaList = fircalamaManager
				.getAllKaplamaMakinesiFircalama(prmPipeId);
	}

	public void kaplamaListener(SelectEvent event) {
		updateButtonRender = true;
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!", null));
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			KaplamaMakinesiFircalamaManager fircalamaManager = new KaplamaMakinesiFircalamaManager();
			allKaplamaMakinesiFircalamaList = fircalamaManager
					.getAllKaplamaMakinesiFircalama(selectedPipe.getPipeId());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
		}
	}

	public void deleteFircalamaSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		KaplamaMakinesiFircalamaManager fircalamaManager = new KaplamaMakinesiFircalamaManager();

		if (kaplamaMakinesiFircalamaForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		fircalamaManager.delete(kaplamaMakinesiFircalamaForm);
		kaplamaMakinesiFircalamaForm = new KaplamaMakinesiFircalama();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"FIRÇALAMA BAŞARIYLA SİLİNMİŞTİR!"));

		fillTestList(prmPipeId);
	}

	// setters getters

	public List<KaplamaMakinesiFircalama> getAllKaplamaMakinesiFircalamaList() {
		return allKaplamaMakinesiFircalamaList;
	}

	public void setAllKaplamaMakinesiFircalamaList(
			List<KaplamaMakinesiFircalama> allKaplamaMakinesiFircalamaList) {
		this.allKaplamaMakinesiFircalamaList = allKaplamaMakinesiFircalamaList;
	}

	public KaplamaMakinesiFircalama getKaplamaMakinesiFircalamaForm() {
		return kaplamaMakinesiFircalamaForm;
	}

	public void setKaplamaMakinesiFircalamaForm(
			KaplamaMakinesiFircalama kaplamaMakinesiFircalamaForm) {
		this.kaplamaMakinesiFircalamaForm = kaplamaMakinesiFircalamaForm;
	}

	public KaplamaMakinesiFircalama getNewFircalama() {
		return newFircalama;
	}

	public void setNewFircalama(KaplamaMakinesiFircalama newFircalama) {
		this.newFircalama = newFircalama;
	}

	public List<KaplamaMakinesiFircalama> getFircalamaList() {
		return fircalamaList;
	}

	public void setFircalamaList(List<KaplamaMakinesiFircalama> fircalamaList) {
		this.fircalamaList = fircalamaList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	public boolean isKontrol() {
		return kontrol;
	}

	public void setKontrol(boolean kontrol) {
		this.kontrol = kontrol;
	}

	public String getBarkodNo() {
		return barkodNo;
	}

	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
