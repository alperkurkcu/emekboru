package com.emekboru.beans.test.kaplama;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaProcessBozulmaSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaProcessBozulmaSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaplamaProcessBozulmaSonucBean")
@ViewScoped
public class TestKaplamaProcessBozulmaSonucBean {

	private TestKaplamaProcessBozulmaSonuc testKaplamaProcessBozulmaSonucForm = new TestKaplamaProcessBozulmaSonuc();
	private List<TestKaplamaProcessBozulmaSonuc> allTestKaplamaProcessBozulmaSonucList = new ArrayList<TestKaplamaProcessBozulmaSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateTestKaplamaProcessBozulmaSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaProcessBozulmaSonucManager manager = new TestKaplamaProcessBozulmaSonucManager();

		if (testKaplamaProcessBozulmaSonucForm.getId() == null) {

			testKaplamaProcessBozulmaSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaProcessBozulmaSonucForm.setEkleyenEmployee(userBean
					.getUser());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaProcessBozulmaSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaProcessBozulmaSonucForm.setBagliTestId(bagliTest);
			testKaplamaProcessBozulmaSonucForm.setBagliGlobalId(bagliTest);

			manager.enterNew(testKaplamaProcessBozulmaSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaProcessBozulmaSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaProcessBozulmaSonucForm.setGuncelleyenEmployee(userBean
					.getUser());
			manager.updateEntity(testKaplamaProcessBozulmaSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addTestKaplamaProcessBozulmaSonuc() {

		testKaplamaProcessBozulmaSonucForm = new TestKaplamaProcessBozulmaSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaProcessBozulmaSonucListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaProcessBozulmaSonucList = TestKaplamaProcessBozulmaSonucManager
				.getAllTestKaplamaProcessBozulmaSonuc(globalId, pipeId);
		testKaplamaProcessBozulmaSonucForm = new TestKaplamaProcessBozulmaSonuc();
	}

	public void deleteTestKaplamaProcessBozulmaSonuc() {

		bagliTestId = testKaplamaProcessBozulmaSonucForm.getBagliGlobalId()
				.getId();

		if (testKaplamaProcessBozulmaSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaProcessBozulmaSonucManager manager = new TestKaplamaProcessBozulmaSonucManager();
		manager.delete(testKaplamaProcessBozulmaSonucForm);
		testKaplamaProcessBozulmaSonucForm = new TestKaplamaProcessBozulmaSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters
	public TestKaplamaProcessBozulmaSonuc getTestKaplamaProcessBozulmaSonucForm() {
		return testKaplamaProcessBozulmaSonucForm;
	}

	public void setTestKaplamaProcessBozulmaSonucForm(
			TestKaplamaProcessBozulmaSonuc testKaplamaProcessBozulmaSonucForm) {
		this.testKaplamaProcessBozulmaSonucForm = testKaplamaProcessBozulmaSonucForm;
	}

	public List<TestKaplamaProcessBozulmaSonuc> getAllTestKaplamaProcessBozulmaSonucList() {
		return allTestKaplamaProcessBozulmaSonucList;
	}

	public void setAllTestKaplamaProcessBozulmaSonucList(
			List<TestKaplamaProcessBozulmaSonuc> allTestKaplamaProcessBozulmaSonucList) {
		this.allTestKaplamaProcessBozulmaSonucList = allTestKaplamaProcessBozulmaSonucList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
