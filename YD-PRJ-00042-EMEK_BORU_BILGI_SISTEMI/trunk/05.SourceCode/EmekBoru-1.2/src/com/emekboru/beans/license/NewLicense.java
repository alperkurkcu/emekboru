package com.emekboru.beans.license;

import java.io.Serializable;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.emekboru.beans.crm.ActiveEmployeeList;
import com.emekboru.jpa.License;
import com.emekboru.jpa.LicenseRenewResponsible;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean
@RequestScoped
public class NewLicense implements Serializable{

	private static final long serialVersionUID = -1109417906587639488L;

	private License 				license;
	private LicenseRenewResponsible lrr;
	public NewLicense(){
		license = new License();
		lrr 	= new LicenseRenewResponsible();
	}
	
	/**
	 * Preprocess the data so we can add a LicenseRenewResponsible at the same time 4
	 * with the License object
	 */
	public void preprocess(){
		
		ActiveEmployeeList empBean = (ActiveEmployeeList) 
				FacesContextUtils.getViewBean("activeEmployeeList", ActiveEmployeeList.class);
		lrr.setPreviousExpirationDate(license.getExpirationDate());
		lrr.setLicense(license);
		lrr.setEmployee(empBean.find(lrr.getEmployee().getEmployeeId()));
		license.setLicenseRenewResponsibles(new ArrayList<LicenseRenewResponsible>());
		license.getLicenseRenewResponsibles().add(lrr);
	}

	/**
	 * 		GETTERS AND SETTERS
	 */
	public License getLicense() {
		return license;
	}

	public void setLicense(License license) {
		this.license = license;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public LicenseRenewResponsible getLrr() {
		return lrr;
	}

	public void setLrr(LicenseRenewResponsible lrr) {
		this.lrr = lrr;
	}
}
