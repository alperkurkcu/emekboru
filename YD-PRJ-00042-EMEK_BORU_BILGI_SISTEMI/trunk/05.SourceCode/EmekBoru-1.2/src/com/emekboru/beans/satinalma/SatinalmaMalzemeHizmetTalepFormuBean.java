/**
 * 
 */
package com.emekboru.beans.satinalma;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.component.accordionpanel.AccordionPanel;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.Employee;
import com.emekboru.jpa.employee.EmployeeDayoff;
import com.emekboru.jpa.satinalma.SatinalmaAmbarMalzemeTalepForm;
import com.emekboru.jpa.satinalma.SatinalmaDurum;
import com.emekboru.jpa.satinalma.SatinalmaMalzemeHizmetTalepFormu;
import com.emekboru.jpa.satinalma.SatinalmaMalzemeHizmetTalepFormuIcerik;
import com.emekboru.jpa.satinalma.SatinalmaTakipDosyalarIcerik;
import com.emekboru.jpaman.EmployeeManager;
import com.emekboru.jpaman.satinalma.SatinalmaDurumManager;
import com.emekboru.jpaman.satinalma.SatinalmaMalzemeHizmetTalepFormuIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaMalzemeHizmetTalepFormuManager;
import com.emekboru.jpaman.satinalma.SatinalmaTakipDosyalarIcerikManager;
import com.emekboru.utils.DateTrans;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "satinalmaMalzemeHizmetTalepFormuBean")
@ViewScoped
public class SatinalmaMalzemeHizmetTalepFormuBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<SatinalmaMalzemeHizmetTalepFormu> allSatinalmaMalzemeHizmetTalepFormuList = new ArrayList<SatinalmaMalzemeHizmetTalepFormu>();
	private List<SatinalmaMalzemeHizmetTalepFormu> allSatinalmaMalzemeHizmetTalepFormuPersonalList = new ArrayList<SatinalmaMalzemeHizmetTalepFormu>();
	private List<SatinalmaMalzemeHizmetTalepFormu> allSatinalmaMalzemeHizmetTalepFormuOnayBekleyenlerList = new ArrayList<SatinalmaMalzemeHizmetTalepFormu>();
	private List<SatinalmaMalzemeHizmetTalepFormu> allSatinalmaMalzemeHizmetTalepFormuOnayGecmislerList = new ArrayList<SatinalmaMalzemeHizmetTalepFormu>();
	private List<SatinalmaMalzemeHizmetTalepFormu> allSatinalmaMalzemeHizmetTalepFormuGMOnayBekleyenlerList = new ArrayList<SatinalmaMalzemeHizmetTalepFormu>();
	private List<SatinalmaMalzemeHizmetTalepFormu> allSatinalmaMalzemeHizmetTalepFormuTumSatinalmaList = new ArrayList<SatinalmaMalzemeHizmetTalepFormu>();
	private List<SatinalmaMalzemeHizmetTalepFormu> allSatinalmaMalzemeHizmetTalepFormuSatinalmaByEmployeeIdList = new ArrayList<SatinalmaMalzemeHizmetTalepFormu>();

	private List<SatinalmaTakipDosyalarIcerik> allSatinalmaTakipDosyalarIcerikByEmployeeIdList = new ArrayList<SatinalmaTakipDosyalarIcerik>();

	private SatinalmaMalzemeHizmetTalepFormu satinalmaMalzemeHizmetTalepFormuEkran = new SatinalmaMalzemeHizmetTalepFormu();
	private SatinalmaDurum satinalmaDurum = new SatinalmaDurum();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	@ManagedProperty(value = "#{satinalmaMalzemeHizmetTalepFormuIcerikBean}")
	private SatinalmaMalzemeHizmetTalepFormuIcerikBean satinalmaMalzemeHizmetTalepFormuIcerikBean;

	@ManagedProperty(value = "#{satinalmaTakipDosyalarIcerikBean}")
	private SatinalmaTakipDosyalarIcerikBean satinalmaTakipDosyalarIcerikBean;

	private boolean updateButtonRender;

	// AmbarTalepten gelen veri için
	private SatinalmaMalzemeHizmetTalepFormu newMalzeme = new SatinalmaMalzemeHizmetTalepFormu();

	@ManagedProperty(value = "#{satinalmaTeklifIstemeBean}")
	private SatinalmaTeklifIstemeBean satinalmaTeklifIstemeBean;

	@ManagedProperty(value = "#{satinalmaSatinalmaKarariBean}")
	private SatinalmaSatinalmaKarariBean satinalmaSatinalmaKarariBean;

	private Employee gm = new Employee();

	public Integer amirOnayBekleyenlerSayisi = null;
	public Integer gmOnayBekleyenlerSayisi = null;

	String activeIndex = "0";

	public SatinalmaMalzemeHizmetTalepFormuBean() {

	}

	@PostConstruct
	public void load() {

		EmployeeManager manager = new EmployeeManager();

		fillTestList();
		// 91 - AhmetGuven
		gm = manager.findByField(Employee.class, "employeeId", 91).get(0);
		amirOnayBekleyenlerSayisi = null;
		gmOnayBekleyenlerSayisi = null;

	}

	public void onTabChange(TabChangeEvent event) {
		activeIndex = ((AccordionPanel) event.getComponent()).getActiveIndex();

	}

	public void addForm() {

		satinalmaMalzemeHizmetTalepFormuEkran = new SatinalmaMalzemeHizmetTalepFormu();
		// today set ediliyor
		java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(
				DateTrans.calendar.getTime().getTime());
		satinalmaMalzemeHizmetTalepFormuEkran.setTalepTarihi(currentTimestamp);
		updateButtonRender = false;
	}

	

	@SuppressWarnings("static-access")
	public void addFromAmbar(SatinalmaAmbarMalzemeTalepForm ambarMalzeme) {

		FacesContextUtils
				.addPlainInfoMessage("TALEP, MALZEME/HİZMET TALEBİNE GÖNDERİLDİ!");

		// 7 - revizyon istendi.
		SatinalmaDurumManager manager = new SatinalmaDurumManager();
		satinalmaDurum = manager.loadObject(SatinalmaDurum.class, 7);

		SatinalmaMalzemeHizmetTalepFormuManager malzemeManager = new SatinalmaMalzemeHizmetTalepFormuManager();
		SatinalmaDurumManager durumManager = new SatinalmaDurumManager();

		try {

			newMalzeme.setSatinalmaAmbarMalzemeTalepForm(ambarMalzeme);
			newMalzeme.setEklemeZamani(UtilInsCore.getTarihZaman());
			newMalzeme.setEkleyenKullanici(userBean.getUser().getId());
			newMalzeme.setSatinalmaDurum(durumManager.findByField(
					SatinalmaDurum.class, "id", 7).get(0));

			newMalzeme
					.setSupervisorApproval(satinalmaMalzemeHizmetTalepFormuEkran.PENDING);
			newMalzeme
					.setGmApproval(satinalmaMalzemeHizmetTalepFormuEkran.PENDING);
			newMalzeme
					.setFinansApproval(satinalmaMalzemeHizmetTalepFormuEkran.PENDING);

			if (newMalzeme.getSupervisor() == null) {
				newMalzeme.setSupervisor(userBean.getUser().getEmployee()
						.getSupervisor());
			}

			if (newMalzeme.getGeneralManager() == null) {
				newMalzeme.setGeneralManager(gm);
			}

			newMalzeme.setSatinalmaDurum(satinalmaDurum);
			sayiBul();
			newMalzeme.setFormSayisi(satinalmaMalzemeHizmetTalepFormuEkran
					.getFormSayisi());
			allSatinalmaMalzemeHizmetTalepFormuList.add(newMalzeme);
			malzemeManager.enterNew(newMalzeme);

			FacesContextUtils
					.addPlainInfoMessage("YENİ MALZEME/HİZMET FORMU EKLENDİ!");

		} catch (Exception e) {
			System.out.println("SatinalmaMalzemeHizmetTalepFormuBean: "
					+ e.toString());
		}

		satinalmaMalzemeHizmetTalepFormuIcerikBean
				.addFromAmbarIcerik(ambarMalzeme.getId());

		newMalzeme = new SatinalmaMalzemeHizmetTalepFormu();
	}

	@SuppressWarnings("static-access")
	public void addOrUpdateForm() {

		// 1-Yeni eklendi.
		SatinalmaDurumManager manager = new SatinalmaDurumManager();
		satinalmaDurum = manager.loadObject(SatinalmaDurum.class, 1);

		SatinalmaMalzemeHizmetTalepFormuManager formManager = new SatinalmaMalzemeHizmetTalepFormuManager();

		if (satinalmaMalzemeHizmetTalepFormuEkran.getId() == null) {

			satinalmaMalzemeHizmetTalepFormuEkran.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			satinalmaMalzemeHizmetTalepFormuEkran.setEkleyenKullanici(userBean
					.getUser().getId());

			satinalmaMalzemeHizmetTalepFormuEkran
					.setSupervisorApproval(satinalmaMalzemeHizmetTalepFormuEkran.PENDING);
			satinalmaMalzemeHizmetTalepFormuEkran
					.setGmApproval(satinalmaMalzemeHizmetTalepFormuEkran.PENDING);
			satinalmaMalzemeHizmetTalepFormuEkran
					.setFinansApproval(satinalmaMalzemeHizmetTalepFormuEkran.PENDING);
			satinalmaMalzemeHizmetTalepFormuEkran
					.setSatinalmaDurum(satinalmaDurum);
			sayiBul();

			if (satinalmaMalzemeHizmetTalepFormuEkran.getSupervisor() == null) {
				satinalmaMalzemeHizmetTalepFormuEkran.setSupervisor(userBean
						.getUser().getEmployee().getSupervisor());
			}

			if (satinalmaMalzemeHizmetTalepFormuEkran.getGeneralManager() == null) {
				satinalmaMalzemeHizmetTalepFormuEkran.setGeneralManager(gm);
			}

			formManager.enterNew(satinalmaMalzemeHizmetTalepFormuEkran);

			this.satinalmaMalzemeHizmetTalepFormuEkran = new SatinalmaMalzemeHizmetTalepFormu();
			FacesContextUtils.addInfoMessage("FormSubmitMessage");

		} else {

			satinalmaMalzemeHizmetTalepFormuEkran
					.setGuncellemeZamani(UtilInsCore.getTarihZaman());
			satinalmaMalzemeHizmetTalepFormuEkran
					.setGuncelleyenKullanici(userBean.getUser().getId());

			if (satinalmaMalzemeHizmetTalepFormuEkran.getSupervisor() == null) {
				satinalmaMalzemeHizmetTalepFormuEkran.setSupervisor(userBean
						.getUser().getEmployee().getSupervisor());
			}

			formManager.updateEntity(satinalmaMalzemeHizmetTalepFormuEkran);

			FacesContextUtils.addInfoMessage("FormUpdateMessage");

			// // Teklif İsteme Formuna dönüştürme.
			//
			// try {
			// if (satinalmaMalzemeHizmetTalepFormuEkran.getSatinalmaDurum()
			// .getTitle().toLowerCase().contains("teklif")) {
			// if (!satinalmaTeklifIstemeBean
			// .isMalzemeTransformedToTeklif(satinalmaMalzemeHizmetTalepFormuEkran
			// .getId())) {
			// satinalmaTeklifIstemeBean
			// .addFromMalzeme(satinalmaMalzemeHizmetTalepFormuEkran);
			// }
			// }
			// } catch (Exception e) {
			// System.out.println("SalesProposalBean: " + e.toString());
			// }

			// satinalma kararına dönüştürme

			try {
				if (satinalmaMalzemeHizmetTalepFormuEkran.getSatinalmaDurum()
						.getTitle().toLowerCase().contains("teklif")) {
					if (!satinalmaSatinalmaKarariBean
							.isMalzemeTransformedToKarar(satinalmaMalzemeHizmetTalepFormuEkran
									.getId())) {
						satinalmaSatinalmaKarariBean
								.addFromMalzeme(satinalmaMalzemeHizmetTalepFormuEkran);
					}
				}
			} catch (Exception e) {
				System.out.println("SalesProposalBean: " + e.toString());
			}

		}

		fillTestList();
	}

	public void satinAlmayaGonder() {

		try {

			if (!satinalmaTakipDosyalarIcerikBean
					.isMalzemeTransformedToSatinAlma(satinalmaMalzemeHizmetTalepFormuEkran
							.getId())) {
				satinalmaTakipDosyalarIcerikBean
						.addFromMalzemeIcerik(satinalmaMalzemeHizmetTalepFormuEkran
								.getId());

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"İSTEK SATIN ALMA MODÜLÜNE BAŞARIYLA GÖNDERİLMİŞTİR!",
						null));
			}
		} catch (Exception e) {
			System.out.println("satinalmaMalzemeHizmetTalepFormuBean: "
					+ e.toString());
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"İSTEK SATIN ALMA MODÜLÜNE GÖNDERİLEMEDİ, HATA!", null));
		}

	}

	public void deleteForm() {

		SatinalmaMalzemeHizmetTalepFormuManager formManager = new SatinalmaMalzemeHizmetTalepFormuManager();

		if (satinalmaMalzemeHizmetTalepFormuEkran.getId() == null) {

			FacesContextUtils.addWarnMessage("NoSelectMessage");
			return;
		}

		formManager.delete(satinalmaMalzemeHizmetTalepFormuEkran);
		fillTestList();
		FacesContextUtils.addWarnMessage("TestDeleteMessage");
	}

	public void fillTestList() {

		Thread thread = new Thread() {

			@Override
			public void run() {
				SatinalmaMalzemeHizmetTalepFormuManager manager = new SatinalmaMalzemeHizmetTalepFormuManager();
				allSatinalmaMalzemeHizmetTalepFormuList = manager
						.getAllSatinalmaMalzemeHizmetTalepFormu();
				allSatinalmaMalzemeHizmetTalepFormuPersonalList = manager
						.getUserSatinalmaMalzemeHizmetTalepFormu(userBean
								.getUser().getId());
				allSatinalmaMalzemeHizmetTalepFormuOnayBekleyenlerList = manager
						.getOnayBekleyenlerSatinalmaMalzemeHizmetTalepFormu(userBean
								.getUser().getEmployee().getEmployeeId());
				allSatinalmaMalzemeHizmetTalepFormuOnayGecmislerList = manager
						.getOnayGecmislerSatinalmaMalzemeHizmetTalepFormu(userBean
								.getUser().getEmployee().getEmployeeId());
				allSatinalmaMalzemeHizmetTalepFormuGMOnayBekleyenlerList = manager
						.getGMOnayBekleyenlerSatinalmaMalzemeHizmetTalepFormu(userBean
								.getUser().getEmployee().getEmployeeId());
				allSatinalmaMalzemeHizmetTalepFormuTumSatinalmaList = manager
						.getTumSatinalmayaDusenlerSatinalmaMalzemeHizmetTalepFormu();
				// allSatinalmaMalzemeHizmetTalepFormuSatinalmaByEmployeeIdList
				// =
				// SatinalmaMalzemeHizmetTalepFormuManager
				// .getSatinalmayaDusenlerByEmployeeIdSatinalmaMalzemeHizmetTalepFormu(userBean
				// .getUser().getEmployee().getEmployeeId());

				// deneme
				SatinalmaTakipDosyalarIcerikManager manager1 = new SatinalmaTakipDosyalarIcerikManager();
				allSatinalmaTakipDosyalarIcerikByEmployeeIdList = manager1
						.getAllSatinalmaMalzemeHizmetTalepFormuByGorevliId(userBean
								.getUser().getEmployee().getEmployeeId());
				// allYonlendirilenler = SatinalmaMalzemeHizmetTalepFormuManager
				// .findYonlendirilenler(userBean.getUser().getEmployee()
				// .getEmployeeId());
				// deneme
			}

		};
		thread.run();

		satinalmaMalzemeHizmetTalepFormuEkran = new SatinalmaMalzemeHizmetTalepFormu();
		updateButtonRender = false;
		listeSayisi();
	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void listeSayisi() {

		amirOnayBekleyenlerSayisi = allSatinalmaMalzemeHizmetTalepFormuOnayBekleyenlerList
				.size();
		gmOnayBekleyenlerSayisi = allSatinalmaMalzemeHizmetTalepFormuGMOnayBekleyenlerList
				.size();
	}

	public void sayiBul() {

		SatinalmaMalzemeHizmetTalepFormuManager manager = new SatinalmaMalzemeHizmetTalepFormuManager();
		satinalmaMalzemeHizmetTalepFormuEkran.setFormSayisi(manager
				.getOtomatikSayi().toString());
	}

	// Dönüştürülecek AmbarId var mı?
	public boolean isAmbarTransformedToMalzeme(int theAmbarId) {

		for (int i = 0; i < allSatinalmaMalzemeHizmetTalepFormuList.size(); i++) {
			if (allSatinalmaMalzemeHizmetTalepFormuList.get(i)
					.getSatinalmaAmbarMalzemeTalepForm() != null) {
				if (allSatinalmaMalzemeHizmetTalepFormuList.get(i)
						.getSatinalmaAmbarMalzemeTalepForm().getId() == theAmbarId) {
					return true;
				}
			}
		}

		return false;
	}

	public void onayaGonder() {

		// 27 - onay bekleniyor.
		SatinalmaDurumManager manager = new SatinalmaDurumManager();
		satinalmaDurum = manager.loadObject(SatinalmaDurum.class, 27);

		if (satinalmaMalzemeHizmetTalepFormuEkran == null) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ONAYA GÖNDERİLECEK TALEBİ SEÇİNİZ!", null));
			return;
		}

		if (satinalmaMalzemeHizmetTalepFormuEkran.isTamamlanma()) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCEDEN ONAYA GÖNDERİLMİŞ İSTEK!", null));
			return;
		}

		satinalmaMalzemeHizmetTalepFormuEkran.setTamamlanma(true);
		satinalmaMalzemeHizmetTalepFormuEkran.setOnayTarihi(UtilInsCore
				.getTarihZaman());
		satinalmaMalzemeHizmetTalepFormuEkran.setSatinalmaDurum(satinalmaDurum);
		SatinalmaMalzemeHizmetTalepFormuManager talepManager = new SatinalmaMalzemeHizmetTalepFormuManager();

		talepManager.updateEntity(satinalmaMalzemeHizmetTalepFormuEkran);

		FacesContextUtils.addPlainInfoMessage("TALEP ONAYA GÖNDERİLDİ!");

		this.load();

	}

	public void revizyonaGonder() {

		// 29 - revizyon istendi.
		SatinalmaDurumManager manager = new SatinalmaDurumManager();
		satinalmaDurum = manager.loadObject(SatinalmaDurum.class, 29);

		if (satinalmaMalzemeHizmetTalepFormuEkran == null) {

			FacesContextUtils
					.addPlainWarnMessage("LÜTFEN REVİZYON İSTEDİĞİNİZ TALEBİ SEÇİNİZ!");
			return;
		}

		satinalmaMalzemeHizmetTalepFormuEkran.setTamamlanma(false);
		satinalmaMalzemeHizmetTalepFormuEkran.setOnayTarihi(null);
		satinalmaMalzemeHizmetTalepFormuEkran.setSatinalmaDurum(satinalmaDurum);
		SatinalmaMalzemeHizmetTalepFormuManager talepManager = new SatinalmaMalzemeHizmetTalepFormuManager();

		talepManager.updateEntity(satinalmaMalzemeHizmetTalepFormuEkran);

		FacesContextUtils.addPlainWarnMessage("TALEBİ REVİZYONA GÖNDERDİNİZ!");

		this.load();

	}

	public void gmIslemleri(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		String prmIslem = (String) cmd.getChildren().get(0).getAttributes()
				.get("value");

		SatinalmaDurumManager manager = new SatinalmaDurumManager();

		if (prmIslem.equals("0")) {
			// rev
			satinalmaDurum = manager.loadObject(SatinalmaDurum.class, 29);

			if (satinalmaMalzemeHizmetTalepFormuEkran == null) {

				FacesContextUtils
						.addPlainWarnMessage("LÜTFEN REVİZYON İSTEDİĞİNİZ TALEBİ SEÇİNİZ!");
				return;
			}

			satinalmaMalzemeHizmetTalepFormuEkran.setTamamlanma(false);
			satinalmaMalzemeHizmetTalepFormuEkran.setOnayTarihi(null);
			satinalmaMalzemeHizmetTalepFormuEkran
					.setSatinalmaDurum(satinalmaDurum);
			SatinalmaMalzemeHizmetTalepFormuManager talepManager = new SatinalmaMalzemeHizmetTalepFormuManager();
			talepManager.updateEntity(satinalmaMalzemeHizmetTalepFormuEkran);
			FacesContextUtils
					.addPlainWarnMessage("TALEBİ REVİZYONA GÖNDERDİNİZ!");
			this.load();
		} else if (prmIslem.equals("1")) {
			// onay
			satinalmaDurum = manager.loadObject(SatinalmaDurum.class, 28);

			if (satinalmaMalzemeHizmetTalepFormuEkran == null) {

				FacesContextUtils
						.addPlainWarnMessage("LÜTFEN ONAYLANACAK TALEBİ SEÇİNİZ!");
				return;
			}

			if (satinalmaMalzemeHizmetTalepFormuEkran.getGmApproval() != EmployeeDayoff.PENDING) {

				FacesContextUtils
						.addPlainWarnMessage("ONAYLANAN/REDDEDİLEN TALEP DEĞİŞTİRİLEMEZ!");
				return;
			}

			satinalmaMalzemeHizmetTalepFormuEkran
					.setGmApproval(EmployeeDayoff.APPROVED);
			satinalmaMalzemeHizmetTalepFormuEkran
					.setSatinalmaDurum(satinalmaDurum);
			SatinalmaMalzemeHizmetTalepFormuManager talepManager = new SatinalmaMalzemeHizmetTalepFormuManager();
			talepManager.updateEntity(satinalmaMalzemeHizmetTalepFormuEkran);
			FacesContextUtils.addPlainWarnMessage("TALEP ONAYLANDI!");

			if (satinalmaMalzemeHizmetTalepFormuEkran.getSupervisorApproval() == 1
					&& satinalmaMalzemeHizmetTalepFormuEkran.getGmApproval() == 1) {

				satinAlmayaGonder();
			}

			this.load();

		} else if (prmIslem.equals("2")) {
			// red
			satinalmaDurum = manager.loadObject(SatinalmaDurum.class, 2);

			if (satinalmaMalzemeHizmetTalepFormuEkran == null) {

				FacesContextUtils
						.addPlainWarnMessage("LÜTFEN REDDETMEK İSTEDİĞİNİZ TALEBİ SEÇİNİZ!");
				return;
			}

			if (satinalmaMalzemeHizmetTalepFormuEkran.getGmApproval() != EmployeeDayoff.PENDING) {

				FacesContextUtils
						.addPlainWarnMessage("ONAYLADIĞINIZ/REDDETTİĞİNİZ TALEBİ DEĞİŞTİREMEZSİNİZ!");
				return;
			}

			satinalmaMalzemeHizmetTalepFormuEkran
					.setGmApproval(EmployeeDayoff.DECLINED);
			satinalmaMalzemeHizmetTalepFormuEkran.setOnayTarihi(UtilInsCore
					.getTarihZaman());
			satinalmaMalzemeHizmetTalepFormuEkran
					.setSatinalmaDurum(satinalmaDurum);
			SatinalmaMalzemeHizmetTalepFormuManager talepManager = new SatinalmaMalzemeHizmetTalepFormuManager();

			talepManager.updateEntity(satinalmaMalzemeHizmetTalepFormuEkran);

			FacesContextUtils.addPlainWarnMessage("TALEP REDDEDİLDİ!");

			this.load();
		}
	}

	public void declineRequest() {

		// 2 - Olumsuz Sonuclandı.
		SatinalmaDurumManager manager = new SatinalmaDurumManager();
		satinalmaDurum = manager.loadObject(SatinalmaDurum.class, 2);

		if (satinalmaMalzemeHizmetTalepFormuEkran == null) {

			FacesContextUtils
					.addPlainWarnMessage("LÜTFEN REDDETMEK İSTEDİĞİNİZ TALEBİ SEÇİNİZ!");
			return;
		}

		if (satinalmaMalzemeHizmetTalepFormuEkran.getSupervisorApproval() != EmployeeDayoff.PENDING) {

			FacesContextUtils
					.addPlainWarnMessage("ONAYLADIĞINIZ/REDDETTİĞİNİZ TALEBİ DEĞİŞTİREMEZSİNİZ!");
			return;
		}

		satinalmaMalzemeHizmetTalepFormuEkran
				.setSupervisorApproval(EmployeeDayoff.DECLINED);
		satinalmaMalzemeHizmetTalepFormuEkran.setOnayTarihi(UtilInsCore
				.getTarihZaman());
		satinalmaMalzemeHizmetTalepFormuEkran.setSatinalmaDurum(satinalmaDurum);
		SatinalmaMalzemeHizmetTalepFormuManager talepManager = new SatinalmaMalzemeHizmetTalepFormuManager();

		talepManager.updateEntity(satinalmaMalzemeHizmetTalepFormuEkran);

		FacesContextUtils.addPlainWarnMessage("İzni reddettiniz!");

		this.load();

	}

	public void approveRequest() {

		// 28 - onay verildi.
		SatinalmaDurumManager manager = new SatinalmaDurumManager();
		satinalmaDurum = manager.loadObject(SatinalmaDurum.class, 28);

		if (satinalmaMalzemeHizmetTalepFormuEkran == null) {

			FacesContextUtils
					.addPlainWarnMessage("LÜFTEN ONAYLAMAK ISTEDIĞINIZ TALEBI SEÇINIZ!");
			return;
		}

		if (satinalmaMalzemeHizmetTalepFormuEkran.getSupervisorApproval() != EmployeeDayoff.PENDING) {

			FacesContextUtils
					.addPlainWarnMessage("ONAYLADIĞINIZ YA DA REDDETTIĞINIZ BIR TALEBI DEĞIŞTIREMEZSINIZ!");
			return;
		}

		satinalmaMalzemeHizmetTalepFormuEkran
				.setSupervisorApproval(EmployeeDayoff.APPROVED);
		satinalmaMalzemeHizmetTalepFormuEkran.setSatinalmaDurum(satinalmaDurum);
		SatinalmaMalzemeHizmetTalepFormuManager talepManager = new SatinalmaMalzemeHizmetTalepFormuManager();

		talepManager.updateEntity(satinalmaMalzemeHizmetTalepFormuEkran);

		FacesContextUtils.addPlainWarnMessage("TALEBI ONAYLADINIZ!");

		if (satinalmaMalzemeHizmetTalepFormuEkran.getSupervisorApproval() == 1
				&& satinalmaMalzemeHizmetTalepFormuEkran.getGmApproval() == 1) {

			satinAlmayaGonder();
		}

		this.load();

	}

	// setters getters

	public List<SatinalmaMalzemeHizmetTalepFormu> getAllSatinalmaMalzemeHizmetTalepFormuList() {
		return allSatinalmaMalzemeHizmetTalepFormuList;
	}

	public void setAllSatinalmaMalzemeHizmetTalepFormuList(
			List<SatinalmaMalzemeHizmetTalepFormu> allSatinalmaMalzemeHizmetTalepFormuList) {
		this.allSatinalmaMalzemeHizmetTalepFormuList = allSatinalmaMalzemeHizmetTalepFormuList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public SatinalmaMalzemeHizmetTalepFormu getSatinalmaMalzemeHizmetTalepFormuEkran() {
		return satinalmaMalzemeHizmetTalepFormuEkran;
	}

	public void setSatinalmaMalzemeHizmetTalepFormuEkran(
			SatinalmaMalzemeHizmetTalepFormu satinalmaMalzemeHizmetTalepFormuEkran) {
		this.satinalmaMalzemeHizmetTalepFormuEkran = satinalmaMalzemeHizmetTalepFormuEkran;
	}

	public SatinalmaTeklifIstemeBean getSatinalmaTeklifIstemeBean() {
		return satinalmaTeklifIstemeBean;
	}

	public void setSatinalmaTeklifIstemeBean(
			SatinalmaTeklifIstemeBean satinalmaTeklifIstemeBean) {
		this.satinalmaTeklifIstemeBean = satinalmaTeklifIstemeBean;
	}

	public SatinalmaMalzemeHizmetTalepFormuIcerikBean getSatinalmaMalzemeHizmetTalepFormuIcerikBean() {
		return satinalmaMalzemeHizmetTalepFormuIcerikBean;
	}

	public void setSatinalmaMalzemeHizmetTalepFormuIcerikBean(
			SatinalmaMalzemeHizmetTalepFormuIcerikBean satinalmaMalzemeHizmetTalepFormuIcerikBean) {
		this.satinalmaMalzemeHizmetTalepFormuIcerikBean = satinalmaMalzemeHizmetTalepFormuIcerikBean;
	}

	public SatinalmaSatinalmaKarariBean getSatinalmaSatinalmaKarariBean() {
		return satinalmaSatinalmaKarariBean;
	}

	public void setSatinalmaSatinalmaKarariBean(
			SatinalmaSatinalmaKarariBean satinalmaSatinalmaKarariBean) {
		this.satinalmaSatinalmaKarariBean = satinalmaSatinalmaKarariBean;
	}

	public List<SatinalmaMalzemeHizmetTalepFormu> getAllSatinalmaMalzemeHizmetTalepFormuPersonalList() {
		return allSatinalmaMalzemeHizmetTalepFormuPersonalList;
	}

	public void setAllSatinalmaMalzemeHizmetTalepFormuPersonalList(
			List<SatinalmaMalzemeHizmetTalepFormu> allSatinalmaMalzemeHizmetTalepFormuPersonalList) {
		this.allSatinalmaMalzemeHizmetTalepFormuPersonalList = allSatinalmaMalzemeHizmetTalepFormuPersonalList;
	}

	public List<SatinalmaMalzemeHizmetTalepFormu> getAllSatinalmaMalzemeHizmetTalepFormuOnayBekleyenlerList() {
		return allSatinalmaMalzemeHizmetTalepFormuOnayBekleyenlerList;
	}

	public void setAllSatinalmaMalzemeHizmetTalepFormuOnayBekleyenlerList(
			List<SatinalmaMalzemeHizmetTalepFormu> allSatinalmaMalzemeHizmetTalepFormuOnayBekleyenlerList) {
		this.allSatinalmaMalzemeHizmetTalepFormuOnayBekleyenlerList = allSatinalmaMalzemeHizmetTalepFormuOnayBekleyenlerList;
	}

	public List<SatinalmaMalzemeHizmetTalepFormu> getAllSatinalmaMalzemeHizmetTalepFormuOnayGecmislerList() {
		return allSatinalmaMalzemeHizmetTalepFormuOnayGecmislerList;
	}

	public void setAllSatinalmaMalzemeHizmetTalepFormuOnayGecmislerList(
			List<SatinalmaMalzemeHizmetTalepFormu> allSatinalmaMalzemeHizmetTalepFormuOnayGecmislerList) {
		this.allSatinalmaMalzemeHizmetTalepFormuOnayGecmislerList = allSatinalmaMalzemeHizmetTalepFormuOnayGecmislerList;
	}

	public List<SatinalmaMalzemeHizmetTalepFormu> getAllSatinalmaMalzemeHizmetTalepFormuGMOnayBekleyenlerList() {
		return allSatinalmaMalzemeHizmetTalepFormuGMOnayBekleyenlerList;
	}

	public void setAllSatinalmaMalzemeHizmetTalepFormuGMOnayBekleyenlerList(
			List<SatinalmaMalzemeHizmetTalepFormu> allSatinalmaMalzemeHizmetTalepFormuGMOnayBekleyenlerList) {
		this.allSatinalmaMalzemeHizmetTalepFormuGMOnayBekleyenlerList = allSatinalmaMalzemeHizmetTalepFormuGMOnayBekleyenlerList;
	}

	/**
	 * @return the satinalmaTakipDosyalarIcerikBean
	 */
	public SatinalmaTakipDosyalarIcerikBean getSatinalmaTakipDosyalarIcerikBean() {
		return satinalmaTakipDosyalarIcerikBean;
	}

	/**
	 * @param satinalmaTakipDosyalarIcerikBean
	 *            the satinalmaTakipDosyalarIcerikBean to set
	 */
	public void setSatinalmaTakipDosyalarIcerikBean(
			SatinalmaTakipDosyalarIcerikBean satinalmaTakipDosyalarIcerikBean) {
		this.satinalmaTakipDosyalarIcerikBean = satinalmaTakipDosyalarIcerikBean;
	}

	/**
	 * @return the allSatinalmaMalzemeHizmetTalepFormuTumSatinalmaList
	 */
	public List<SatinalmaMalzemeHizmetTalepFormu> getAllSatinalmaMalzemeHizmetTalepFormuTumSatinalmaList() {
		return allSatinalmaMalzemeHizmetTalepFormuTumSatinalmaList;
	}

	/**
	 * @param allSatinalmaMalzemeHizmetTalepFormuTumSatinalmaList
	 *            the allSatinalmaMalzemeHizmetTalepFormuTumSatinalmaList to set
	 */
	public void setAllSatinalmaMalzemeHizmetTalepFormuTumSatinalmaList(
			List<SatinalmaMalzemeHizmetTalepFormu> allSatinalmaMalzemeHizmetTalepFormuTumSatinalmaList) {
		this.allSatinalmaMalzemeHizmetTalepFormuTumSatinalmaList = allSatinalmaMalzemeHizmetTalepFormuTumSatinalmaList;
	}

	/**
	 * @return the allSatinalmaMalzemeHizmetTalepFormuSatinalmaByEmployeeIdList
	 */
	public List<SatinalmaMalzemeHizmetTalepFormu> getAllSatinalmaMalzemeHizmetTalepFormuSatinalmaByEmployeeIdList() {
		return allSatinalmaMalzemeHizmetTalepFormuSatinalmaByEmployeeIdList;
	}

	/**
	 * @param allSatinalmaMalzemeHizmetTalepFormuSatinalmaByEmployeeIdList
	 *            the
	 *            allSatinalmaMalzemeHizmetTalepFormuSatinalmaByEmployeeIdList
	 *            to set
	 */
	public void setAllSatinalmaMalzemeHizmetTalepFormuSatinalmaByEmployeeIdList(
			List<SatinalmaMalzemeHizmetTalepFormu> allSatinalmaMalzemeHizmetTalepFormuSatinalmaByEmployeeIdList) {
		this.allSatinalmaMalzemeHizmetTalepFormuSatinalmaByEmployeeIdList = allSatinalmaMalzemeHizmetTalepFormuSatinalmaByEmployeeIdList;
	}

	/**
	 * @return the allSatinalmaTakipDosyalarIcerikByEmployeeIdList
	 */
	public List<SatinalmaTakipDosyalarIcerik> getAllSatinalmaTakipDosyalarIcerikByEmployeeIdList() {
		return allSatinalmaTakipDosyalarIcerikByEmployeeIdList;
	}

	/**
	 * @param allSatinalmaTakipDosyalarIcerikByEmployeeIdList
	 *            the allSatinalmaTakipDosyalarIcerikByEmployeeIdList to set
	 */
	public void setAllSatinalmaTakipDosyalarIcerikByEmployeeIdList(
			List<SatinalmaTakipDosyalarIcerik> allSatinalmaTakipDosyalarIcerikByEmployeeIdList) {
		this.allSatinalmaTakipDosyalarIcerikByEmployeeIdList = allSatinalmaTakipDosyalarIcerikByEmployeeIdList;
	}

	/**
	 * @return the amirOnayBekleyenlerSayisi
	 */
	public Integer getAmirOnayBekleyenlerSayisi() {
		return amirOnayBekleyenlerSayisi;
	}

	/**
	 * @return the gmOnayBekleyenlerSayisi
	 */
	public Integer getGmOnayBekleyenlerSayisi() {
		return gmOnayBekleyenlerSayisi;
	}

	/**
	 * @return the activeIndex
	 */
	public String getActiveIndex() {
		return activeIndex;
	}

	/**
	 * @param activeIndex
	 *            the activeIndex to set
	 */
	public void setActiveIndex(String activeIndex) {
		this.activeIndex = activeIndex;
	}

}
