/**
 * 
 */
package com.emekboru.beans.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.emekboru.jpa.Employee;
import com.emekboru.jpa.employee.EmployeePerformance;
import com.emekboru.jpa.rulo.Rulo;
import com.emekboru.jpa.sales.customer.SalesCustomer;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpa.satinalma.SatinalmaAmbarMalzemeTalepForm;
import com.emekboru.jpa.satinalma.SatinalmaMalzemeHizmetTalepFormu;
import com.emekboru.jpa.satinalma.SatinalmaTakipDosyalar;
import com.emekboru.jpa.shipment.ShipmentDriver;
import com.emekboru.jpa.shipment.ShipmentTrailer;
import com.emekboru.jpa.shipment.ShipmentTruck;
import com.emekboru.jpa.stok.StokUrunKartlari;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.EmployeeManager;
import com.emekboru.jpaman.rulo.RuloManager;
import com.emekboru.jpaman.sales.customer.SalesCustomerManager;
import com.emekboru.jpaman.sales.order.SalesItemManager;
import com.emekboru.jpaman.satinalma.SatinalmaAmbarMalzemeTalepFormManager;
import com.emekboru.jpaman.satinalma.SatinalmaMalzemeHizmetTalepFormuManager;
import com.emekboru.jpaman.satinalma.SatinalmaTakipDosyalarManager;
import com.emekboru.jpaman.shipment.ShipmentDriverManager;
import com.emekboru.jpaman.shipment.ShipmentTrailerManager;
import com.emekboru.jpaman.shipment.ShipmentTruckManager;
import com.emekboru.jpaman.stok.StokUrunKartlariManager;

/**
 * @author kursat
 * 
 */

@ManagedBean(name = "autoCompleteBean")
@ViewScoped
public class AutoCompleteBean implements Serializable {

	private static final long serialVersionUID = 1L;

	List<SatinalmaAmbarMalzemeTalepForm> ambarTaleps;

	public AutoCompleteBean() {

		ambarTaleps = new ArrayList<SatinalmaAmbarMalzemeTalepForm>();
	}

	public List<String> autoCompleteEmployeePerformanceSession(
			String searchString) {

		CommonQueries<EmployeePerformance> employeePerformanceManager = new CommonQueries<EmployeePerformance>();

		List<EmployeePerformance> allEmployeePerformances = employeePerformanceManager
				.findAll(EmployeePerformance.class);
		employeePerformanceManager.refreshCollection(allEmployeePerformances);
		List<String> suggestions = new ArrayList<String>();

		Locale locale = new Locale("tr", "TR");

		for (EmployeePerformance ep : allEmployeePerformances) {

			String stringToBeSearched = ep.getSession().toLowerCase(locale);

			if (stringToBeSearched.contains(searchString)) {
				suggestions.add(ep.getSession());
			}
		}

		return suggestions;
	}

	public List<Employee> autoCompleteEmployee(String searchString) {

		EmployeeManager empManager = new EmployeeManager();

		// List<Employee> allEmployees = new EmployeeManager()
		// .findAll(Employee.class);
		List<Employee> allActiveEmployees = new EmployeeManager()
				.findByAllActive();
		empManager.refreshCollection(allActiveEmployees);
		List<Employee> suggestions = new ArrayList<Employee>();

		Locale locale = new Locale("tr", "TR");

		searchString = searchString.toLowerCase(locale);

		for (Employee e : allActiveEmployees) {

			String stringToBeSearched = e.getEmployeeId().toString()
					.toLowerCase(locale)
					+ " - "
					+ e.getFirstname().toLowerCase(locale)
					+ " "
					+ e.getLastname().toLowerCase(locale);

			if (stringToBeSearched.contains(searchString)) {
				suggestions.add(e);
			}
		}

		return suggestions;
	}

	// proje - sipariş arama
	public List<SalesItem> autoCompleteSalesItem(String searchString) {

		SalesItemManager salesManager = new SalesItemManager();

		List<SalesItem> items = new SalesItemManager().findAll(SalesItem.class);
		salesManager.refreshCollection(items);
		List<SalesItem> suggestions = new ArrayList<SalesItem>();

		Locale locale = new Locale("tr", "TR");

		searchString = searchString.toLowerCase(locale);

		try {

			for (SalesItem i : items) {

				String stringToBeSearched = i.getProposal().getSalesOrder()
						.getOrderNo().toString().toLowerCase(locale)
						+ " - "
						+ i.getItemNo()
						+ " -- "
						+ i.getProposal().getCustomer().getCommercialName()
								.toString().toLowerCase(locale);

				if (stringToBeSearched.contains(searchString)) {
					suggestions.add(i);
				}
			}

		} catch (Exception e) {
			System.out.println("autoCompleteBean() - autoCompleteSalesItem(): "
					+ e.toString());
		}

		return suggestions;
	}

	// ambar talep formu arama
	public synchronized List<SatinalmaAmbarMalzemeTalepForm> autoCompleteAmbarTalepFormu(
			String searchString) {

		SatinalmaAmbarMalzemeTalepFormManager ambarManager = new SatinalmaAmbarMalzemeTalepFormManager();

		// List<SatinalmaAmbarMalzemeTalepForm> ambarTaleps = new
		// SatinalmaAmbarMalzemeTalepFormManager()
		// .findAll(SatinalmaAmbarMalzemeTalepForm.class);
		ambarTaleps = new SatinalmaAmbarMalzemeTalepFormManager().findByYear();
		ambarManager.refreshCollection(ambarTaleps);
		List<SatinalmaAmbarMalzemeTalepForm> suggestions = new ArrayList<SatinalmaAmbarMalzemeTalepForm>();

		Locale locale = new Locale("tr", "TR");

		searchString = searchString.toLowerCase(locale);

		try {

			for (SatinalmaAmbarMalzemeTalepForm i : ambarTaleps) {

				String stringToBeSearched = i.getFormSayisi().toLowerCase(
						locale);

				if (stringToBeSearched.contains(searchString)) {
					suggestions.add(i);
				}
			}

		} catch (Exception e) {
			System.out
					.println("autoCompleteBean() - autoCompleteAmbarTalepFormu(): "
							+ e.toString());
		}

		return suggestions;
	}

	// malzeme talep formu arama
	public List<SatinalmaMalzemeHizmetTalepFormu> autoCompleteMalzemeTalepFormu(
			String searchString) {

		SatinalmaMalzemeHizmetTalepFormuManager malzemeManager = new SatinalmaMalzemeHizmetTalepFormuManager();

		List<SatinalmaMalzemeHizmetTalepFormu> malzemeTaleps = new SatinalmaMalzemeHizmetTalepFormuManager()
				.findAll(SatinalmaMalzemeHizmetTalepFormu.class);
		malzemeManager.refreshCollection(malzemeTaleps);
		List<SatinalmaMalzemeHizmetTalepFormu> suggestions = new ArrayList<SatinalmaMalzemeHizmetTalepFormu>();

		Locale locale = new Locale("tr", "TR");

		searchString = searchString.toLowerCase(locale);

		try {

			for (SatinalmaMalzemeHizmetTalepFormu i : malzemeTaleps) {

				String stringToBeSearched = i.getFormSayisi().toLowerCase(
						locale);

				if (stringToBeSearched.contains(searchString)) {
					suggestions.add(i);
				}
			}

		} catch (Exception e) {
			System.out
					.println("autoCompleteBean() - autoCompleteMalzemeTalepFormu(): "
							+ e.toString());
		}

		return suggestions;
	}

	// depo stok urun kartları arama
	public List<StokUrunKartlari> autoCompleteStokUrunKartlari(
			String searchString) {

		StokUrunKartlariManager stokManager = new StokUrunKartlariManager();

		Locale locale = new Locale("tr", "TR");
		searchString = searchString.toUpperCase(locale);

		// List<StokUrunKartlari> urunKartlariByBarkodKodu = new
		// StokUrunKartlariManager()
		// .findAllLike(StokUrunKartlari.class, "barkodKodu", searchString);
		// List<StokUrunKartlari> urunKartlariByUrunAdi = new
		// StokUrunKartlariManager()
		// .findAllLike(StokUrunKartlari.class, "urunAdi", searchString);

		List<StokUrunKartlari> urunKartlariByBarkodKodu = new StokUrunKartlariManager()
				.findByBarkodAktif(searchString);
		List<StokUrunKartlari> urunKartlariByUrunAdi = new StokUrunKartlariManager()
				.findByUrunAdiAktif(searchString);

		stokManager.refreshCollection(urunKartlariByBarkodKodu);
		stokManager.refreshCollection(urunKartlariByUrunAdi);

		List<StokUrunKartlari> suggestions = new ArrayList<StokUrunKartlari>();

		suggestions.addAll(urunKartlariByUrunAdi);
		suggestions.addAll(urunKartlariByBarkodKodu);

		StokUrunKartlari stokKoduYok = new StokUrunKartlariManager()
				.loadObject(StokUrunKartlari.class, 4932);

		// istenmeyen ürünler listeden çıkarılıyor
		// List<Integer> exceptedBarcodes = Arrays.asList(7818, 7819, 7820,
		// 7821,
		// 7822, 7828, 7829);
		// for (int i = 0; i < exceptedBarcodes.size(); i++) {
		// StokUrunKartlari exceptedUrun = new StokUrunKartlariManager()
		// .loadObject(StokUrunKartlari.class, exceptedBarcodes.get(i));
		// for (int j = 0; j < suggestions.size(); j++) {
		// if (suggestions.get(j).getId().equals(exceptedUrun.getId())) {
		// suggestions.remove(suggestions.get(j));
		// }
		// }
		// }
		// istenmeyen ürünler listeden çıkarılıyor

		if (suggestions.size() == 0) {
			suggestions.add(stokKoduYok);
		}

		return suggestions;
	}

	// emek dosya no
	public List<SatinalmaTakipDosyalar> autoCompleteDosyaNo(String searchString) {

		SatinalmaTakipDosyalarManager dosyaManager = new SatinalmaTakipDosyalarManager();

		List<SatinalmaTakipDosyalar> dosyalar = new SatinalmaTakipDosyalarManager()
				.findAll(SatinalmaTakipDosyalar.class);
		dosyaManager.refreshCollection(dosyalar);
		List<SatinalmaTakipDosyalar> suggestions = new ArrayList<SatinalmaTakipDosyalar>();

		Locale locale = new Locale("tr", "TR");

		searchString = searchString.toLowerCase(locale);

		try {

			for (SatinalmaTakipDosyalar i : dosyalar) {

				String stringToBeSearched = i.getDosyaTakipNo().toString()
						.toLowerCase(locale);

				if (stringToBeSearched.contains(searchString)) {
					suggestions.add(i);
				}
			}

			if (suggestions.size() == 0) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"ARANILAN DOSYA BULUNAMADI!", null));
			}

		} catch (Exception e) {
			System.out
					.println("autoCompleteBean() - autoCompleteSatinalmaTakipDosyalar(): "
							+ e.toString());
		}

		return suggestions;
	}

	public List<ShipmentDriver> autoCompleteShipmentDriver(String searchString) {

		ShipmentDriverManager driverManager = new ShipmentDriverManager();

		List<ShipmentDriver> allShipmentDrivers = new ShipmentDriverManager()
				.findAll(ShipmentDriver.class);
		driverManager.refreshCollection(allShipmentDrivers);
		List<ShipmentDriver> suggestions = new ArrayList<ShipmentDriver>();

		Locale locale = new Locale("tr", "TR");

		searchString = searchString.toLowerCase(locale);

		for (ShipmentDriver sd : allShipmentDrivers) {

			String stringToBeSearched = sd.getId().toString()
					.toLowerCase(locale)
					+ " - " + sd.getName().toLowerCase(locale);

			if (stringToBeSearched.contains(searchString)) {
				suggestions.add(sd);
			}
		}

		return suggestions;
	}

	public List<ShipmentTruck> autoCompleteShipmentTruck(String searchString) {

		ShipmentTruckManager truckManager = new ShipmentTruckManager();

		List<ShipmentTruck> allShipmentTrucks = new ShipmentTruckManager()
				.findAll(ShipmentTruck.class);
		truckManager.refreshCollection(allShipmentTrucks);
		List<ShipmentTruck> suggestions = new ArrayList<ShipmentTruck>();

		Locale locale = new Locale("tr", "TR");

		searchString = searchString.toLowerCase(locale);

		for (ShipmentTruck st : allShipmentTrucks) {

			String stringToBeSearched = st.getId().toString()
					.toLowerCase(locale)
					+ " - " + st.getPlate().toLowerCase(locale);

			if (stringToBeSearched.contains(searchString)) {
				suggestions.add(st);
			}
		}

		return suggestions;
	}

	public List<ShipmentTrailer> autoCompleteShipmentTrailer(String searchString) {

		ShipmentTrailerManager trailerMan = new ShipmentTrailerManager();

		List<ShipmentTrailer> allShipmentTrailers = new ShipmentTrailerManager()
				.findAll(ShipmentTrailer.class);
		trailerMan.refreshCollection(allShipmentTrailers);
		List<ShipmentTrailer> suggestions = new ArrayList<ShipmentTrailer>();

		Locale locale = new Locale("tr", "TR");

		searchString = searchString.toLowerCase(locale);

		for (ShipmentTrailer st : allShipmentTrailers) {

			String stringToBeSearched = st.getId().toString()
					.toLowerCase(locale)
					+ " - " + st.getPlate().toLowerCase(locale);

			if (stringToBeSearched.contains(searchString)) {
				suggestions.add(st);
			}
		}

		return suggestions;
	}

	public List<SalesCustomer> autoCompleteSalesCustomer(String searchString) {

		SalesCustomerManager cusMan = new SalesCustomerManager();

		List<SalesCustomer> allSalesCustomer = new SalesCustomerManager()
				.findAll(SalesCustomer.class);
		cusMan.refreshCollection(allSalesCustomer);
		List<SalesCustomer> suggestions = new ArrayList<SalesCustomer>();

		Locale locale = new Locale("tr", "TR");

		searchString = searchString.toLowerCase(locale);

		for (SalesCustomer sc : allSalesCustomer) {

			String stringToBeSearched = sc.getShortName().toLowerCase();

			if (stringToBeSearched.contains(searchString)) {
				suggestions.add(sc);
			}
		}

		return suggestions;
	}

	public List<String> autoCompleteSalesItemManufacturingStandard(
			String searchString) {

		SalesItemManager salesItemManager = new SalesItemManager();

		List<String> manufacturingStandards = salesItemManager
				.findDistinctValuesOfColumn("manufacturingStandard");

		List<String> suggestions = new ArrayList<String>();

		Locale locale = new Locale("tr", "TR");

		searchString = searchString.toLowerCase(locale);

		for (String manufacturingStandard : manufacturingStandards) {

			if (manufacturingStandard == null)
				continue;

			if (manufacturingStandard.toLowerCase(locale)
					.contains(searchString)) {
				suggestions.add(manufacturingStandard);
			}
		}

		return suggestions;
	}

	public List<String> autoCompleteSalesItemQuality(String searchString) {

		SalesItemManager salesItemManager = new SalesItemManager();

		List<String> qualities = salesItemManager
				.findDistinctValuesOfColumn("quality");

		List<String> suggestions = new ArrayList<String>();

		Locale locale = new Locale("tr", "TR");

		searchString = searchString.toLowerCase(locale);

		for (String quality : qualities) {

			if (quality == null)
				continue;

			if (quality.toLowerCase(locale).contains(searchString)) {
				suggestions.add(quality);
			}
		}

		return suggestions;
	}

	public List<String> autoCompleteSalesItemInternalCoveringType(
			String searchString) {

		SalesItemManager salesItemManager = new SalesItemManager();

		List<String> internalCoveringTypes = salesItemManager
				.findDistinctValuesOfColumn("internalCoveringType");

		List<String> suggestions = new ArrayList<String>();

		Locale locale = new Locale("tr", "TR");

		searchString = searchString.toLowerCase(locale);

		for (String coveringType : internalCoveringTypes) {

			if (coveringType == null)
				continue;

			if (coveringType.toLowerCase(locale).contains(searchString)) {
				suggestions.add(coveringType);
			}
		}

		return suggestions;
	}

	public List<String> autoCompleteSalesItemExternalCoveringType(
			String searchString) {

		SalesItemManager salesItemManager = new SalesItemManager();

		List<String> externalCoveringTypes = salesItemManager
				.findDistinctValuesOfColumn("externalCoveringType");

		List<String> suggestions = new ArrayList<String>();

		Locale locale = new Locale("tr", "TR");

		searchString = searchString.toLowerCase(locale);

		for (String coveringType : externalCoveringTypes) {

			if (coveringType == null)
				continue;

			if (coveringType.toLowerCase(locale).contains(searchString)) {
				suggestions.add(coveringType);
			}
		}

		return suggestions;
	}

	public List<String> autoCompleteSalesItemInternalCoveringStandard(
			String searchString) {

		SalesItemManager salesItemManager = new SalesItemManager();

		List<String> internalCoveringStandards = salesItemManager
				.findDistinctValuesOfColumn("internalCoveringStandard");

		List<String> suggestions = new ArrayList<String>();

		Locale locale = new Locale("tr", "TR");

		searchString = searchString.toLowerCase(locale);

		for (String coveringStandard : internalCoveringStandards) {

			if (coveringStandard == null)
				continue;

			if (coveringStandard.toLowerCase(locale).contains(searchString)) {
				suggestions.add(coveringStandard);
			}
		}

		return suggestions;
	}

	public List<String> autoCompleteSalesItemExternalCoveringStandard(
			String searchString) {

		SalesItemManager salesItemManager = new SalesItemManager();

		List<String> externalCoveringStandards = salesItemManager
				.findDistinctValuesOfColumn("externalCoveringStandard");

		List<String> suggestions = new ArrayList<String>();

		Locale locale = new Locale("tr", "TR");

		searchString = searchString.toLowerCase(locale);

		for (String coveringStandard : externalCoveringStandards) {

			if (coveringStandard == null)
				continue;

			if (coveringStandard.toLowerCase(locale).contains(searchString)) {
				suggestions.add(coveringStandard);
			}
		}

		return suggestions;
	}

	public List<String> autoCompleteNewUrunKodu(String searchString) {

		StokUrunKartlariManager stokUrunKartlariManager = new StokUrunKartlariManager();

		List<String> urunKodlari = stokUrunKartlariManager
				.findFirstOfColumnLike("barkodKodu", searchString, "DESC");

		List<String> suggestions = new ArrayList<String>();

		Locale locale = new Locale("tr", "TR");

		for (String urunKodu : urunKodlari) {

			if (urunKodu == null)
				continue;

			if (urunKodu.toLowerCase(locale).contains(searchString)) {
				Integer lastUrunKodu = Integer.parseInt(urunKodu) + 1;
				suggestions.add(lastUrunKodu.toString());
			}
		}

		return suggestions;
	}

	public List<String> autoCompleteRuloYer(String searchString) {

		RuloManager ruloManager = new RuloManager();

		List<String> ruloYerler = ruloManager.findDistinctValuesOfColumn(
				Rulo.class, "ruloYer");

		List<String> suggestions = new ArrayList<String>();

		Locale locale = new Locale("tr", "TR");

		searchString = searchString.toLowerCase(locale);

		for (String ruloYer : ruloYerler) {

			if (ruloYer == null)
				continue;

			if (ruloYer.toLowerCase(locale).contains(searchString)) {
				suggestions.add(ruloYer);
			}
		}

		return suggestions;
	}

	public List<String> autoCompleteRuloKalite(String searchString) {

		RuloManager ruloManager = new RuloManager();

		List<String> ruloYerler = ruloManager.findDistinctValuesOfColumn(
				Rulo.class, "ruloKalite");

		List<String> suggestions = new ArrayList<String>();

		Locale locale = new Locale("tr", "TR");

		searchString = searchString.toLowerCase(locale);

		for (String ruloYer : ruloYerler) {

			if (ruloYer == null)
				continue;

			if (ruloYer.toLowerCase(locale).contains(searchString)) {
				suggestions.add(ruloYer);
			}
		}

		return suggestions;
	}
}
