package com.emekboru.beans.order;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import com.emekboru.config.Config;
import com.emekboru.config.Test;

public class SelectedTests implements Serializable{

	private static final long serialVersionUID = -1141604776265138175L;

	private HashMap<Integer, Boolean> selectedTest;
	
	public SelectedTests(Config config){
		
		selectedTest = new HashMap<Integer, Boolean>();
		List<Test> tests = config.getPipe().getManufacturing().getTests();

		for(Test t : tests)
			selectedTest.put(t.getGlobalId(), false);
		
	}

	public void activate(int globalId){
		
		selectedTest.put(globalId, true);
	}
	
	public void deactivate(int globalId){
		
		selectedTest.put(globalId, false);
	}
	
	public boolean status(int globalId){
		
		return selectedTest.get(globalId);
	}
	
	// ******************************************************************** //
	//            			GETTERS AND SETTERS 							//
	// ******************************************************************** //

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
