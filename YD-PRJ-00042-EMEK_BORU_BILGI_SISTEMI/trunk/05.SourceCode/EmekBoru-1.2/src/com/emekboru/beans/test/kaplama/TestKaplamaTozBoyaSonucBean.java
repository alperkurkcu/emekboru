package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaTozBoyaSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaTozBoyaSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaplamaTozBoyaSonucBean")
@ViewScoped
public class TestKaplamaTozBoyaSonucBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private TestKaplamaTozBoyaSonuc testKaplamaTozBoyaSonucForm = new TestKaplamaTozBoyaSonuc();
	private List<TestKaplamaTozBoyaSonuc> allTestKaplamaTozBoyaSonucList = new ArrayList<TestKaplamaTozBoyaSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	Integer prmGlobalId = null;
	Integer prmTestId = null;
	Integer prmPipeId = null;

	private Pipe pipe = null;

	public void addOrUpdateKaplamaTozBoyaTestSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		prmGlobalId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");
		prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaTozBoyaSonucManager tktbManager = new TestKaplamaTozBoyaSonucManager();

		if (testKaplamaTozBoyaSonucForm.getId() == null) {

			testKaplamaTozBoyaSonucForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			testKaplamaTozBoyaSonucForm.setEkleyenEmployee(userBean.getUser());

			pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaTozBoyaSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaTozBoyaSonucForm.setBagliTestId(bagliTest);
			testKaplamaTozBoyaSonucForm.setBagliGlobalId(bagliTest);

			tktbManager.enterNew(testKaplamaTozBoyaSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaTozBoyaSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaTozBoyaSonucForm.setGuncelleyenEmployee(userBean
					.getUser());
			tktbManager.updateEntity(testKaplamaTozBoyaSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addKaplamaTozBoyaTest() {

		testKaplamaTozBoyaSonucForm = new TestKaplamaTozBoyaSonuc();
		updateButtonRender = false;
	}

	public void kaplamaTozBoyaListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaTozBoyaSonucList = TestKaplamaTozBoyaSonucManager
				.getAllKaplamaTozBoyaSonucTest(globalId, pipeId);
		testKaplamaTozBoyaSonucForm = new TestKaplamaTozBoyaSonuc();
	}

	public void deleteTestKaplamaBukmeSonuc() {

		bagliTestId = testKaplamaTozBoyaSonucForm.getBagliTestId().getId();

		if (testKaplamaTozBoyaSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		TestKaplamaTozBoyaSonucManager tktbManager = new TestKaplamaTozBoyaSonucManager();
		tktbManager.delete(testKaplamaTozBoyaSonucForm);
		testKaplamaTozBoyaSonucForm = new TestKaplamaTozBoyaSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setters and getters
	public TestKaplamaTozBoyaSonuc getTestKaplamaTozBoyaSonucForm() {
		return testKaplamaTozBoyaSonucForm;
	}

	public void setTestKaplamaTozBoyaSonucForm(
			TestKaplamaTozBoyaSonuc testKaplamaTozBoyaSonucForm) {
		this.testKaplamaTozBoyaSonucForm = testKaplamaTozBoyaSonucForm;
	}

	/**
	 * @return the allTestKaplamaTozBoyaSonucList
	 */
	public List<TestKaplamaTozBoyaSonuc> getAllTestKaplamaTozBoyaSonucList() {
		return allTestKaplamaTozBoyaSonucList;
	}

	/**
	 * @param allTestKaplamaTozBoyaSonucList
	 *            the allTestKaplamaTozBoyaSonucList to set
	 */
	public void setAllTestKaplamaTozBoyaSonucList(
			List<TestKaplamaTozBoyaSonuc> allTestKaplamaTozBoyaSonucList) {
		this.allTestKaplamaTozBoyaSonucList = allTestKaplamaTozBoyaSonucList;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the prmGlobalId
	 */
	public Integer getPrmGlobalId() {
		return prmGlobalId;
	}

	/**
	 * @param prmGlobalId
	 *            the prmGlobalId to set
	 */
	public void setPrmGlobalId(Integer prmGlobalId) {
		this.prmGlobalId = prmGlobalId;
	}

	/**
	 * @return the prmTestId
	 */
	public Integer getPrmTestId() {
		return prmTestId;
	}

	/**
	 * @param prmTestId
	 *            the prmTestId to set
	 */
	public void setPrmTestId(Integer prmTestId) {
		this.prmTestId = prmTestId;
	}

	/**
	 * @return the prmPipeId
	 */
	public Integer getPrmPipeId() {
		return prmPipeId;
	}

	/**
	 * @param prmPipeId
	 *            the prmPipeId to set
	 */
	public void setPrmPipeId(Integer prmPipeId) {
		this.prmPipeId = prmPipeId;
	}

	/**
	 * @return the pipe
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * @param pipe
	 *            the pipe to set
	 */
	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

}
