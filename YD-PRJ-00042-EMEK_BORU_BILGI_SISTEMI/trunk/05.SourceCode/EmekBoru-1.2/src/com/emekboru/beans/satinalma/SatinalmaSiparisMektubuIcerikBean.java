/**
 * 
 */
package com.emekboru.beans.satinalma;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.satinalma.SatinalmaSiparisMektubuIcerik;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "satinalmaSiparisMektubuIcerikBean")
@ViewScoped
public class SatinalmaSiparisMektubuIcerikBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<SatinalmaSiparisMektubuIcerik> allSatinalmaSiparisMektubuIcerikList = new ArrayList<SatinalmaSiparisMektubuIcerik>();

	private SatinalmaSiparisMektubuIcerik SatinalmaSiparisMektubuIcerikForm = new SatinalmaSiparisMektubuIcerik();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
