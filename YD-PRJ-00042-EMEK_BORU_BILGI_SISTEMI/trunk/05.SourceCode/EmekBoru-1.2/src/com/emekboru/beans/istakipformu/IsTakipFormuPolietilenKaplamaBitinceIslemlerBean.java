/**
 * 
 */
package com.emekboru.beans.istakipformu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.istakipformu.IsTakipFormuPolietilenKaplamaBitinceIslemler;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isTakipFormuPolietilenKaplamaBitinceIslemlerBean")
@ViewScoped
public class IsTakipFormuPolietilenKaplamaBitinceIslemlerBean implements
		Serializable {

	private static final long serialVersionUID = 1L;

	private List<IsTakipFormuPolietilenKaplamaBitinceIslemler> allIsTakipFormuPolietilenKaplamaBitinceIslemlerList = new ArrayList<IsTakipFormuPolietilenKaplamaBitinceIslemler>();
	private IsTakipFormuPolietilenKaplamaBitinceIslemler isTakipFormuPolietilenKaplamaBitinceIslemlerForm = new IsTakipFormuPolietilenKaplamaBitinceIslemler();

	// setters getters
	public List<IsTakipFormuPolietilenKaplamaBitinceIslemler> getAllIsTakipFormuPolietilenKaplamaBitinceIslemlerList() {
		return allIsTakipFormuPolietilenKaplamaBitinceIslemlerList;
	}

	public void setAllIsTakipFormuPolietilenKaplamaBitinceIslemlerList(
			List<IsTakipFormuPolietilenKaplamaBitinceIslemler> allIsTakipFormuPolietilenKaplamaBitinceIslemlerList) {
		this.allIsTakipFormuPolietilenKaplamaBitinceIslemlerList = allIsTakipFormuPolietilenKaplamaBitinceIslemlerList;
	}

	public IsTakipFormuPolietilenKaplamaBitinceIslemler getIsTakipFormuPolietilenKaplamaBitinceIslemlerForm() {
		return isTakipFormuPolietilenKaplamaBitinceIslemlerForm;
	}

	public void setIsTakipFormuPolietilenKaplamaBitinceIslemlerForm(
			IsTakipFormuPolietilenKaplamaBitinceIslemler isTakipFormuPolietilenKaplamaBitinceIslemlerForm) {
		this.isTakipFormuPolietilenKaplamaBitinceIslemlerForm = isTakipFormuPolietilenKaplamaBitinceIslemlerForm;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
