/**
 * 
 */
package com.emekboru.beans.shipment;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.shipment.Shipment;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.shipment.ShipmentManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author kursat
 * 
 */
@ManagedBean(name = "shipmentBean")
@ViewScoped
public class ShipmentBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean configBean;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userCredentialsBean;

	private String readBarcode = new String();

	private List<Shipment> allNewShipments = new ArrayList<Shipment>();
	private List<Shipment> allPraparedShipments = new ArrayList<Shipment>();

	private Shipment newShipment = new Shipment();
	private Shipment selectedShipment = new Shipment();

	private ShipmentManager shipmentManager = new ShipmentManager();
	private PipeManager pipeManager = new PipeManager();

	private Boolean userNotifiedAboutPipesExistance = false;

	private Pipe selectedPipe = new Pipe();

	public ShipmentBean() {
		super();
	}

	@PostConstruct
	private void load() {

		allPraparedShipments.clear();
		allNewShipments.clear();

		allNewShipments = shipmentManager.findByField(Shipment.class, "status",
				Shipment.NEWLY_CREATED);
		allPraparedShipments = shipmentManager.findByField(Shipment.class,
				"status", Shipment.PREPARED);
	}

	public void goToNewShipment() {
		FacesContextUtils.redirect(configBean.getPageUrl().NEW_SHIPMENT_PAGE);
	}

	public void goToPreparedShipment() {
		FacesContextUtils
				.redirect(configBean.getPageUrl().PREPARED_SHIPMENT_PAGE);
	}

	public void addNewShipment() {

		int ekleyenKullanici = this.userCredentialsBean.getUser().getId();
		Timestamp eklemeZamani = new java.sql.Timestamp(
				new java.util.Date().getTime());

		newShipment.setEklemeZamani(eklemeZamani);
		newShipment.setEkleyenKullanici(ekleyenKullanici);
		newShipment.setStatus(Shipment.NEWLY_CREATED);
		newShipment.setShipmentNumber(getNewShipmentNumber());

		shipmentManager.enterNew(newShipment);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Yeni sevkiyat eklendi."));

		this.load();
		newShipment = new Shipment();
	}

	public void deleteSelectedShipment() {

		shipmentManager.delete(selectedShipment);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"Seçili sevkiyat silinmiştir."));

		this.load();
		selectedShipment = new Shipment();

	}

	public void prepareShipment() {

		selectedShipment.setStatus(Shipment.PREPARED);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Sevkiyat tamamlandı."));

		shipmentManager.updateEntity(selectedShipment);

		selectedShipment = new Shipment();
		this.load();

	}

	public void updateselectedShipment() {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("Sevkiyat güncellendi."));

		shipmentManager.updateEntity(selectedShipment);

		selectedShipment = new Shipment();
		this.load();
	}

	public void addPipeToShipment() {
		if (pipeManager.findByBarkodNo(this.readBarcode).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, this.readBarcode
							+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!", null));
			return;
		}

		selectedPipe = pipeManager.findByBarkodNo(this.readBarcode).get(0);

		if (selectedPipe.getShipment() != null) {
			if (selectedPipe.getShipment().getId() == selectedShipment.getId()) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"BU BORU ZATEN BU SEVKİYATA EKLİ!", null));
				return;
			}

			else if (userNotifiedAboutPipesExistance == false) {

				userNotifiedAboutPipesExistance = true;

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"BU BORU ZATEN BİR SEVKİYATA EKLİ! "
										+ "YİNEDE BU SEVKİYATA EKLEMEK İSTİYORSANIZ "
										+ "TEKRAR BORU EKLE BUTONUNA TIKLAYINIZ.",
								null));
				return;
			} else {
				userNotifiedAboutPipesExistance = false;

				selectedPipe.getShipment().getPipes().remove(selectedPipe);
				shipmentManager.updateEntity(selectedPipe.getShipment());
				pipeManager.updateEntity(selectedPipe);
			}
		}

		selectedShipment.getPipes().add(0, selectedPipe);
		shipmentManager.updateEntity(selectedShipment);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(this.readBarcode
				+ " BARKOD NUMARALI BORU SEÇİLİ SEVKİYATA EKLENMİŞTİR!"));

		this.load();
	}

	public void removePipeFromShipment() {

		if (selectedPipe.getPipeId() == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "LÜTFEN BORU SEÇİNİZ!", null));
			return;
		}

		selectedShipment.getPipes().remove(selectedPipe);
		selectedPipe.setShipment(null);

		shipmentManager.updateEntity(selectedShipment);

		pipeManager.updateEntity(selectedPipe);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						this.selectedPipe.getPipeBarkodNo()
								+ " BARKOD NUMARALI BORU SEÇİLİ SEVKİYATTAN SİLİNMİŞTİR!"));

		this.load();
	}

	private String getNewShipmentNumber() {
		Integer year = Calendar.getInstance().get(Calendar.YEAR);

		String lastShipmentNumber = new String();
		lastShipmentNumber = shipmentManager.findMaxLike("shipmentNumber",
				"shipmentNumber", year.toString());

		if (lastShipmentNumber == null) {
			return year + "/1";
		}

		int nextNumber = Integer.parseInt(lastShipmentNumber.split("/")[1]);
		return year + "/" + ++nextNumber;
	}

	/*
	 * 
	 * S E T T E R S
	 * 
	 * A N D
	 * 
	 * G E T T E R S
	 */

	public ConfigBean getConfigBean() {
		return configBean;
	}

	public void setConfigBean(ConfigBean configBean) {
		this.configBean = configBean;
	}

	public UserCredentialsBean getUserCredentialsBean() {
		return userCredentialsBean;
	}

	public void setUserCredentialsBean(UserCredentialsBean userCredentialsBean) {
		this.userCredentialsBean = userCredentialsBean;
	}

	public String getReadBarcode() {
		return readBarcode;
	}

	public void setReadBarcode(String readBarcode) {
		this.readBarcode = readBarcode;
	}

	public List<Shipment> getAllNewShipments() {
		return allNewShipments;
	}

	public void setAllNewShipments(List<Shipment> allNewShipments) {
		this.allNewShipments = allNewShipments;
	}

	public List<Shipment> getAllPraparedShipments() {
		return allPraparedShipments;
	}

	public void setAllPraparedShipments(List<Shipment> allPraparedShipments) {
		this.allPraparedShipments = allPraparedShipments;
	}

	public Shipment getNewShipment() {
		return newShipment;
	}

	public void setNewShipment(Shipment newShipment) {
		this.newShipment = newShipment;
	}

	public Shipment getSelectedShipment() {
		return selectedShipment;
	}

	public void setSelectedShipment(Shipment selectedShipment) {
		this.selectedShipment = selectedShipment;
	}

	public ShipmentManager getShipmentManager() {
		return shipmentManager;
	}

	public void setShipmentManager(ShipmentManager shipmentManager) {
		this.shipmentManager = shipmentManager;
	}

	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
