/**
 * 
 */
package com.emekboru.beans.kaplamamakinesi;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isemri.IsEmriBetonKaplama;
import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiBeton;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isemirleri.IsEmriBetonKaplamaManager;
import com.emekboru.jpaman.kaplamamakinesi.KaplamaMakinesiBetonManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "kaplamaMakinesiBetonBean")
@ViewScoped
public class KaplamaMakinesiBetonBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<KaplamaMakinesiBeton> allKaplamaMakinesiBetonList = new ArrayList<KaplamaMakinesiBeton>();
	private KaplamaMakinesiBeton kaplamaMakinesiBetonForm = new KaplamaMakinesiBeton();

	private KaplamaMakinesiBeton newBeton = new KaplamaMakinesiBeton();
	private List<KaplamaMakinesiBeton> betonList;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	private Pipe selectedPipe = new Pipe();

	private boolean kontrol = false;

	private String barkodNo = null;

	private IsEmriBetonKaplama selectedIsEmriKaplama = new IsEmriBetonKaplama();

	public KaplamaMakinesiBetonBean() {

	}

	public void addBeton(ActionEvent ae) {

		UICommand cmd = (UICommand) ae.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId != null) {
			PipeManager pipeMan = new PipeManager();
			selectedPipe = pipeMan.loadObject(Pipe.class, prmPipeId);
		}

		try {
			KaplamaMakinesiBetonManager manager = new KaplamaMakinesiBetonManager();

			newBeton.setEklemeZamani(UtilInsCore.getTarihZaman());
			newBeton.setEkleyenKullanici(userBean.getUser().getId());
			newBeton.setPipe(selectedPipe);
			manager.enterNew(newBeton);
			newBeton = new KaplamaMakinesiBeton();

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"BETON KAPLAMA İŞLEMİ BAŞARIYLA EKLENMİŞTİR!"));

		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL,
					"BETON KAPLAMA İŞLEMİ EKLENEMEDİ!", null));
			System.out.println("KaplamaMakinesiBetonBean.addBeton-HATA");
		}

		fillTestList(prmPipeId);
	}

	public void addBetonSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		Boolean prmKontrol = kaplamaMakinesiBetonForm.getDurum();
		KaplamaMakinesiBetonManager betonManager = new KaplamaMakinesiBetonManager();

		if (prmKontrol == null) {

			try {

				kaplamaMakinesiBetonForm.setDurum(false);
				kaplamaMakinesiBetonForm.setKaplamaBaslamaZamani(UtilInsCore
						.getTarihZaman());
				kaplamaMakinesiBetonForm.setKaplamaBaslamaKullanici(userBean
						.getUser().getId());
				betonManager.updateEntity(kaplamaMakinesiBetonForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"BETON KAPLAMA İŞLEMİ BAŞARIYLA BAŞLANMIŞTIR!"));
			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"BETON KAPLAMA İŞLEMİ BAŞLANAMADI!", null));
				System.out
						.println("KaplamaMakinesiBetonBean.addBetonSonuc-HATA-BAŞLAMA");
			}
		} else if (prmKontrol == false) {

			try {

				kaplamaMakinesiBetonForm.setDurum(true);
				kaplamaMakinesiBetonForm.setKaplamaBitisZamani(UtilInsCore
						.getTarihZaman());
				kaplamaMakinesiBetonForm.setKaplamaBitisKullanici(userBean
						.getUser().getId());
				betonManager.updateEntity(kaplamaMakinesiBetonForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"BETON KAPLAMA İŞLEMİ BAŞARIYLA BİTİRİLMİŞTİR!"));

			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"BETON KAPLAMA İŞLEMİ BİTİRİLEMEDİ!", null));
				System.out
						.println("KaplamaMakinesiBetonBean.addBetonSonuc-HATA-BİTİŞ"
								+ e.toString());
			}

		}

		fillTestList(prmPipeId);
	}

	@SuppressWarnings("static-access")
	public void fillTestList(Integer prmPipeId) {

		KaplamaMakinesiBetonManager betonManager = new KaplamaMakinesiBetonManager();
		allKaplamaMakinesiBetonList = betonManager
				.getAllKaplamaMakinesiBeton(prmPipeId);
	}

	public void kaplamaListener(SelectEvent event) {
		updateButtonRender = true;
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!", null));
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			KaplamaMakinesiBetonManager betonManager = new KaplamaMakinesiBetonManager();
			allKaplamaMakinesiBetonList = betonManager
					.getAllKaplamaMakinesiBeton(selectedPipe.getPipeId());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
		}
	}

	public void deleteBetonSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		KaplamaMakinesiBetonManager betonManager = new KaplamaMakinesiBetonManager();

		if (kaplamaMakinesiBetonForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		betonManager.delete(kaplamaMakinesiBetonForm);
		kaplamaMakinesiBetonForm = new KaplamaMakinesiBeton();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"BETON KAPLAMA BAŞARIYLA SİLİNMİŞTİR!"));

		fillTestList(prmPipeId);
	}

	@SuppressWarnings("static-access")
	public void isEmriGoster() {

		IsEmriBetonKaplamaManager isEmriManager = new IsEmriBetonKaplamaManager();
		if (isEmriManager.getAllIsEmriBetonKaplama(
				selectedPipe.getSalesItem().getItemId()).size() > 0) {
			selectedIsEmriKaplama = isEmriManager.getAllIsEmriBetonKaplama(
					selectedPipe.getSalesItem().getItemId()).get(0);
		} else {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					" BETON KAPLAMA İŞ EMRİ BULUNAMADI, LÜTFEN EKLETİNİZ!",
					null));
			return;
		}
	}

	// setters getters

	public List<KaplamaMakinesiBeton> getAllKaplamaMakinesiBetonList() {
		return allKaplamaMakinesiBetonList;
	}

	public void setAllKaplamaMakinesiBetonList(
			List<KaplamaMakinesiBeton> allKaplamaMakinesiBetonList) {
		this.allKaplamaMakinesiBetonList = allKaplamaMakinesiBetonList;
	}

	public KaplamaMakinesiBeton getKaplamaMakinesiBetonForm() {
		return kaplamaMakinesiBetonForm;
	}

	public void setKaplamaMakinesiBetonForm(
			KaplamaMakinesiBeton kaplamaMakinesiBetonForm) {
		this.kaplamaMakinesiBetonForm = kaplamaMakinesiBetonForm;
	}

	public KaplamaMakinesiBeton getNewBeton() {
		return newBeton;
	}

	public void setNewBeton(KaplamaMakinesiBeton newBeton) {
		this.newBeton = newBeton;
	}

	public List<KaplamaMakinesiBeton> getBetonList() {
		return betonList;
	}

	public void setBetonList(List<KaplamaMakinesiBeton> betonList) {
		this.betonList = betonList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	public boolean isKontrol() {
		return kontrol;
	}

	public void setKontrol(boolean kontrol) {
		this.kontrol = kontrol;
	}

	public String getBarkodNo() {
		return barkodNo;
	}

	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public IsEmriBetonKaplama getSelectedIsEmriKaplama() {
		return selectedIsEmriKaplama;
	}

	public void setSelectedIsEmriKaplama(
			IsEmriBetonKaplama selectedIsEmriKaplama) {
		this.selectedIsEmriKaplama = selectedIsEmriKaplama;
	}

}
