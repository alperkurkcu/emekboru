package com.emekboru.beans.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Employee;
import com.emekboru.jpaman.EmployeeManager;
import com.emekboru.jpaman.JpqlComparativeClauses;
import com.emekboru.jpaman.WhereClauseArgs;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "activeEmployeeList")
@ViewScoped
public class ActiveEmployeeList implements Serializable {

	private static final long serialVersionUID = -2224171291790913926L;

	@EJB
	private ConfigBean config;

	private List<Employee> employees;
	private Employee selectedEmployee;

	/**
	 * The default constructor required by JSF.
	 */
	public ActiveEmployeeList() {
		employees = new ArrayList<Employee>();
		selectedEmployee = new Employee();
	}

	@SuppressWarnings("rawtypes")
	@PostConstruct
	public void load() {

		employees = new ArrayList<Employee>();
		EmployeeManager manager = new EmployeeManager();
		ArrayList<WhereClauseArgs> conditionList = new ArrayList<WhereClauseArgs>();

		WhereClauseArgs<Boolean> arg1 = new WhereClauseArgs.Builder<Boolean>()
				.setFieldName("active")
				.setComparativeClause(JpqlComparativeClauses.NQ_EQUAL)
				.setComplexKey(false).setValue(true).build();

		conditionList.add(arg1);
		employees = manager
				.selectFromWhereQuerie(Employee.class, conditionList);
	}

	public Employee find(int employeeId) {

		for (Employee emp : employees) {
			if (emp.getEmployeeId() == employeeId)
				return emp;
		}
		return null;
	}

	public void goToCmPlanningPage() {

		FacesContextUtils
				.redirect(config.getPageUrl().CUSTOMER_MANAGEMENT_PAGE);
	}

	/**
	 * GETTERS AND SETTERS
	 */
	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Employee getSelectedEmployee() {
		return selectedEmployee;
	}

	public void setSelectedEmployee(Employee selectedEmployee) {
		this.selectedEmployee = selectedEmployee;
	}
}
