/**
 * 
 */
package com.emekboru.beans.istakipformu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.istakipformu.IsTakipFormuBetonKaplamaBitinceIslemler;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isTakipFormuBetonKaplamaBitinceIslemlerBean")
@ViewScoped
public class IsTakipFormuBetonKaplamaBitinceIslemlerBean implements
		Serializable {

	private static final long serialVersionUID = 1L;

	private List<IsTakipFormuBetonKaplamaBitinceIslemler> allIsTakipFormuBetonKaplamaBitinceIslemlerList = new ArrayList<IsTakipFormuBetonKaplamaBitinceIslemler>();
	private IsTakipFormuBetonKaplamaBitinceIslemler isTakipFormuBetonKaplamaBitinceIslemlerForm = new IsTakipFormuBetonKaplamaBitinceIslemler();

	// setters getters
	public List<IsTakipFormuBetonKaplamaBitinceIslemler> getAllIsTakipFormuBetonKaplamaBitinceIslemlerList() {
		return allIsTakipFormuBetonKaplamaBitinceIslemlerList;
	}

	public void setAllIsTakipFormuBetonKaplamaBitinceIslemlerList(
			List<IsTakipFormuBetonKaplamaBitinceIslemler> allIsTakipFormuBetonKaplamaBitinceIslemlerList) {
		this.allIsTakipFormuBetonKaplamaBitinceIslemlerList = allIsTakipFormuBetonKaplamaBitinceIslemlerList;
	}

	public IsTakipFormuBetonKaplamaBitinceIslemler getIsTakipFormuBetonKaplamaBitinceIslemlerForm() {
		return isTakipFormuBetonKaplamaBitinceIslemlerForm;
	}

	public void setIsTakipFormuBetonKaplamaBitinceIslemlerForm(
			IsTakipFormuBetonKaplamaBitinceIslemler isTakipFormuBetonKaplamaBitinceIslemlerForm) {
		this.isTakipFormuBetonKaplamaBitinceIslemlerForm = isTakipFormuBetonKaplamaBitinceIslemlerForm;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
