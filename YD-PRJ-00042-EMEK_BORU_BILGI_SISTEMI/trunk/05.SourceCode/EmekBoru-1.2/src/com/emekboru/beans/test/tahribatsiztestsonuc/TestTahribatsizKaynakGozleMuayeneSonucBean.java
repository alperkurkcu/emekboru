/**
 * 
 */
package com.emekboru.beans.test.tahribatsiztestsonuc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.test.tahribatsiz.TahribatsizTestOrderListBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizKaynakGozleMuayeneSonuc;
import com.emekboru.jpaman.NondestructiveTestsSpecManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizKaynakGozleMuayeneSonucManager;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */

@ManagedBean(name = "testTahribatsizKaynakGozleMuayeneSonucBean")
@ViewScoped
public class TestTahribatsizKaynakGozleMuayeneSonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<TestTahribatsizKaynakGozleMuayeneSonuc> allTestTahribatsizKaynakGozleMuayeneSonucList = new ArrayList<TestTahribatsizKaynakGozleMuayeneSonuc>();
	private TestTahribatsizKaynakGozleMuayeneSonuc testTahribatsizKaynakGozleMuayeneSonucForm = new TestTahribatsizKaynakGozleMuayeneSonuc();

	int index = 0;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private boolean visibleButtonRender;

	private Pipe selectedPipe = new Pipe();

	private String barkodNo = null;

	public TestTahribatsizKaynakGozleMuayeneSonucBean() {
		visibleButtonRender = false;
	}

	public void kaynakGozleListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer pipeId) {

		TestTahribatsizKaynakGozleMuayeneSonucManager testTahribatsizKaynakGozleMuayeneSonucManager = new TestTahribatsizKaynakGozleMuayeneSonucManager();
		allTestTahribatsizKaynakGozleMuayeneSonucList = testTahribatsizKaynakGozleMuayeneSonucManager
				.getAllTestTahribatsizKaynakGozleMuayeneSonuc(pipeId);
	}

	public void addTestTahribatsizKaynakGozleMuayeneSonuc() {
		testTahribatsizKaynakGozleMuayeneSonucForm = new TestTahribatsizKaynakGozleMuayeneSonuc();
		updateButtonRender = false;
	}

	@SuppressWarnings({ "static-access", "unused" })
	public void addOrUpdateKaynakGozleSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();

		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");
		Integer prmItemId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");

		Integer testId = 0;

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}
		if (prmItemId == null) {
			prmItemId = selectedPipe.getSalesItem().getItemId();
		}

		TestTahribatsizKaynakGozleMuayeneSonucManager kaynakGozleManager = new TestTahribatsizKaynakGozleMuayeneSonucManager();
		NondestructiveTestsSpecManager nonManager = new NondestructiveTestsSpecManager();

		try {
			testId = nonManager.findByItemIdGlobalId(prmItemId, 1039).get(0)
					.getTestId();

		} catch (Exception e2) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"KOORDİNAT EKLEME BAŞARISIZ, KALİTE TANIMLARINDA KAYNAK GÖZLE MUAYENE TESTİ EKSİKTİR!",
							null));
			return;
		}

		if (testTahribatsizKaynakGozleMuayeneSonucForm.getId() == null) {

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);

			// testTahribatsizKaynakGozleMuayeneSonucForm.setTestId(testId);
			testTahribatsizKaynakGozleMuayeneSonucForm.setPipe(pipe);

			testTahribatsizKaynakGozleMuayeneSonucForm
					.setEklemeZamani(UtilInsCore.getTarihZaman());
			testTahribatsizKaynakGozleMuayeneSonucForm
					.setEkleyenEmployee(userBean.getUser());
			kaynakGozleManager
					.enterNew(testTahribatsizKaynakGozleMuayeneSonucForm);

			this.testTahribatsizKaynakGozleMuayeneSonucForm = new TestTahribatsizKaynakGozleMuayeneSonuc();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							"KAYNAK GÖZLE MUAYENE KOORDİNATI BAŞARI İLE KAYDEDİLMİŞTİR!"));
		} else {

			testTahribatsizKaynakGozleMuayeneSonucForm
					.setGuncellemeZamani(UtilInsCore.getTarihZaman());
			testTahribatsizKaynakGozleMuayeneSonucForm
					.setGuncelleyenEmployee(userBean.getUser());
			// testTahribatsizKaynakGozleMuayeneSonucForm.setTestId(testId);

			kaynakGozleManager
					.updateEntity(testTahribatsizKaynakGozleMuayeneSonucForm);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"KAYNAK GÖZLE MUAYENE BAŞARI İLE GÜNCELLENMİŞTİR!"));
		}

		fillTestList(prmPipeId);
		testTahribatsizKaynakGozleMuayeneSonucForm = new TestTahribatsizKaynakGozleMuayeneSonuc();
		updateButtonRender = false;
		// sayfada barkoddan okutunca order item pipe seçimi olmadıgı için
		// sıkıntı cıkıyordu
		TahribatsizTestOrderListBean ttolb = new TahribatsizTestOrderListBean();
		ttolb.loadOrderDetails(selectedPipe.getSalesItem().getItemId());
	}

	public void deleteKaynakGozleSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		TestTahribatsizKaynakGozleMuayeneSonucManager kaynakGozleManager = new TestTahribatsizKaynakGozleMuayeneSonucManager();

		if (testTahribatsizKaynakGozleMuayeneSonucForm.getId() == null) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"LİSTEDEN SEÇİLMİŞ BİR DEĞER YOKTUR!", null));
			return;
		}

		kaynakGozleManager.delete(testTahribatsizKaynakGozleMuayeneSonucForm);
		testTahribatsizKaynakGozleMuayeneSonucForm = new TestTahribatsizKaynakGozleMuayeneSonuc();
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
				"KAYNAK GÖZLE MUAYENE KOORDİNATI SİLİNMİŞTİR!", null));

		fillTestList(prmPipeId);
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0
				|| prmBarkod.equals("")) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " BARKOD NUMARALI BORU SİSTEMDE YOKTUR!", null));
			visibleButtonRender = false;
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			TestTahribatsizKaynakGozleMuayeneSonucManager kaynakGozleManager = new TestTahribatsizKaynakGozleMuayeneSonucManager();
			allTestTahribatsizKaynakGozleMuayeneSonucList = kaynakGozleManager
					.getAllTestTahribatsizKaynakGozleMuayeneSonuc(selectedPipe
							.getPipeId());
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
			visibleButtonRender = true;
		}
	}

	// setters getters

	/**
	 * @return the allTestTahribatsizKaynakGozleMuayeneSonucList
	 */
	public List<TestTahribatsizKaynakGozleMuayeneSonuc> getAllTestTahribatsizKaynakGozleMuayeneSonucList() {
		return allTestTahribatsizKaynakGozleMuayeneSonucList;
	}

	/**
	 * @param allTestTahribatsizKaynakGozleMuayeneSonucList
	 *            the allTestTahribatsizKaynakGozleMuayeneSonucList to set
	 */
	public void setAllTestTahribatsizKaynakGozleMuayeneSonucList(
			List<TestTahribatsizKaynakGozleMuayeneSonuc> allTestTahribatsizKaynakGozleMuayeneSonucList) {
		this.allTestTahribatsizKaynakGozleMuayeneSonucList = allTestTahribatsizKaynakGozleMuayeneSonucList;
	}

	/**
	 * @return the testTahribatsizKaynakGozleMuayeneSonucForm
	 */
	public TestTahribatsizKaynakGozleMuayeneSonuc getTestTahribatsizKaynakGozleMuayeneSonucForm() {
		return testTahribatsizKaynakGozleMuayeneSonucForm;
	}

	/**
	 * @param testTahribatsizKaynakGozleMuayeneSonucForm
	 *            the testTahribatsizKaynakGozleMuayeneSonucForm to set
	 */
	public void setTestTahribatsizKaynakGozleMuayeneSonucForm(
			TestTahribatsizKaynakGozleMuayeneSonuc testTahribatsizKaynakGozleMuayeneSonucForm) {
		this.testTahribatsizKaynakGozleMuayeneSonucForm = testTahribatsizKaynakGozleMuayeneSonucForm;
	}

	/**
	 * @return the index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @param index
	 *            the index to set
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the selectedPipe
	 */
	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	/**
	 * @param selectedPipe
	 *            the selectedPipe to set
	 */
	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	/**
	 * @return the barkodNo
	 */
	public String getBarkodNo() {
		return barkodNo;
	}

	/**
	 * @param barkodNo
	 *            the barkodNo to set
	 */
	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the visibleButtonRender
	 */
	public boolean isVisibleButtonRender() {
		return visibleButtonRender;
	}

}
