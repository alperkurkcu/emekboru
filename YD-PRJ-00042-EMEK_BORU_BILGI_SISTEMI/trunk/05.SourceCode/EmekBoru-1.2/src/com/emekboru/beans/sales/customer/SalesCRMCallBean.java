package com.emekboru.beans.sales.customer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.emekboru.beans.sales.SalesMenuBean;
import com.emekboru.jpa.Employee;
import com.emekboru.jpa.sales.customer.SalesCRMCall;
import com.emekboru.jpa.sales.customer.SalesContactPeople;
import com.emekboru.jpa.sales.customer.SalesJoinCRMCallContactPeople;
import com.emekboru.jpa.sales.customer.SalesJoinCRMCallEmployee;
import com.emekboru.jpaman.sales.customer.SalesCRMCallManager;
import com.emekboru.jpaman.sales.customer.SalesContactPeopleManager;
import com.emekboru.jpaman.sales.customer.SalesJoinCRMCallContactPeopleManager;
import com.emekboru.jpaman.sales.customer.SalesJoinCRMCallEmployeeManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "salesCRMCallBean")
@ViewScoped
public class SalesCRMCallBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6397640185565482285L;

	@ManagedProperty(value = "#{salesMenuBean}")
	private SalesMenuBean salesMenuBean;

	private SalesCRMCall newCall = new SalesCRMCall();
	private SalesCRMCall selectedCall;

	private List<SalesCRMCall> callList;
	private List<SalesCRMCall> filteredList;
	private List<SalesCRMCall> callsOfCustomer;

	private List<SalesContactPeople> newContactPeopleList;
	private List<SalesContactPeople> selectedContactPeopleList;

	public List<Employee> employeeList;
	private Employee[] selectedEmployees;
	public Integer departmentId;

	public SalesCRMCallBean() {
		newCall = new SalesCRMCall();
		selectedCall = new SalesCRMCall();
		employeeList = new ArrayList<Employee>();
	}

	@PostConstruct
	public void load() {
		System.out.println("SalesCRMCallBean.load()");

		try {
			SalesCRMCallManager manager = new SalesCRMCallManager();
			callList = manager.findAll(SalesCRMCall.class);
			/* JOIN FUNCTIONS */
			for (SalesCRMCall call : callList) {
				this.loadJoinCallEmployee(call);
				this.loadJoinCallContactPeople(call);
			}
			/* end of JOIN FUNCTIONS */
			selectedCall = callList.get(0);
		} catch (Exception e) {
			System.out.println("SalesCRMCallBean:" + e.toString());
		}
	}

	public void add() {
		System.out.println("SalesCRMCallBean.add()");

		try {

			for (int i = 0; i < selectedEmployees.length; i++) {
				newCall.getParticipantList().add(selectedEmployees[i]);
			}

			SalesCRMCallManager manager = new SalesCRMCallManager();
			manager.enterNew(newCall);
			/* JOIN FUNCTIONS */
			this.addJoinCallEmployee(newCall);
			this.addJoinCallContactPeople(newCall);
			/* end of JOIN FUNCTIONS */
			callList.add(newCall);
			newCall = new SalesCRMCall();
			FacesContextUtils.addInfoMessage("SubmitMessage");
		} catch (Exception e) {
			System.out.println("SalesCRMCallBean:" + e.toString());
		}
	}

	public void update() {
		System.out.println("SalesCRMCallBean.update()");

		try {
			SalesCRMCallManager manager = new SalesCRMCallManager();

			/* JOIN FUNCTIONS */
			this.deleteJoinCallEmployee(selectedCall);
			this.addJoinCallEmployee(selectedCall);
			this.deleteJoinCallContactPeople(selectedCall);
			this.addJoinCallContactPeople(selectedCall);
			/* end of JOIN FUNCTIONS */
			manager.updateEntity(selectedCall);

			FacesContextUtils.addInfoMessage("UpdateItemMessage");
		} catch (Exception e) {
			System.out.println("SalesCRMCallBean:" + e.toString());
		}
	}

	public void delete() {
		System.out.println("SalesCRMCallBean.delete()");

		try {
			/* JOIN FUNCTIONS */
			this.deleteJoinCallEmployee(selectedCall);
			this.deleteJoinCallContactPeople(selectedCall);
			/* end of JOIN FUNCTIONS */
			SalesCRMCallManager manager = new SalesCRMCallManager();
			manager.delete(selectedCall);

			callList.remove(selectedCall);
			selectedCall = callList.get(0);

			FacesContextUtils.addWarnMessage("DeletedMessage");
		} catch (Exception e) {
			System.out.println("SalesCRMCallBean:" + e.toString());
		}
	}

	/* JOIN FUNCTIONS */
	private void addJoinCallEmployee(SalesCRMCall e) {
		SalesJoinCRMCallEmployeeManager joinManager = new SalesJoinCRMCallEmployeeManager();
		SalesJoinCRMCallEmployee newJoin;

		for (int i = 0; i < e.getParticipantList().size(); i++) {
			newJoin = new SalesJoinCRMCallEmployee();
			newJoin.setEmployee(e.getParticipantList().get(i));
			newJoin.setCall(e);
			joinManager.enterNew(newJoin);
		}
	}

	private void loadJoinCallEmployee(SalesCRMCall call) {
		SalesJoinCRMCallEmployeeManager joinManager = new SalesJoinCRMCallEmployeeManager();
		List<SalesJoinCRMCallEmployee> joinList = joinManager
				.findAll(SalesJoinCRMCallEmployee.class);

		call.setParticipantList(new ArrayList<Employee>());

		for (int i = 0; i < joinList.size(); i++) {
			if (joinList.get(i).getCall().equals(call)) {
				call.getParticipantList().add(joinList.get(i).getEmployee());
			}
		}
	}

	private void deleteJoinCallEmployee(SalesCRMCall call) {
		SalesJoinCRMCallEmployeeManager joinManager = new SalesJoinCRMCallEmployeeManager();
		List<SalesJoinCRMCallEmployee> joinList = joinManager
				.findAll(SalesJoinCRMCallEmployee.class);

		for (int i = 0; i < joinList.size(); i++) {
			if (joinList.get(i).getCall().equals(call)) {
				joinManager.delete(joinList.get(i));
			}
		}
	}

	private void addJoinCallContactPeople(SalesCRMCall e) {
		SalesJoinCRMCallContactPeopleManager joinManager = new SalesJoinCRMCallContactPeopleManager();
		SalesJoinCRMCallContactPeople newJoin;

		for (int i = 0; i < e.getContactPeopleList().size(); i++) {
			newJoin = new SalesJoinCRMCallContactPeople();
			newJoin.setContactPeople(e.getContactPeopleList().get(i));
			newJoin.setCall(e);
			joinManager.enterNew(newJoin);
		}
	}

	private void loadJoinCallContactPeople(SalesCRMCall call) {
		SalesJoinCRMCallContactPeopleManager joinManager = new SalesJoinCRMCallContactPeopleManager();
		List<SalesJoinCRMCallContactPeople> joinList = joinManager
				.findAll(SalesJoinCRMCallContactPeople.class);

		call.setContactPeopleList(new ArrayList<SalesContactPeople>());

		for (int i = 0; i < joinList.size(); i++) {
			if (joinList.get(i).getCall().equals(call)) {
				call.getContactPeopleList().add(
						joinList.get(i).getContactPeople());
			}
		}
	}

	private void deleteJoinCallContactPeople(SalesCRMCall call) {
		SalesJoinCRMCallContactPeopleManager joinManager = new SalesJoinCRMCallContactPeopleManager();
		List<SalesJoinCRMCallContactPeople> joinList = joinManager
				.findAll(SalesJoinCRMCallContactPeople.class);

		for (int i = 0; i < joinList.size(); i++) {
			if (joinList.get(i).getCall().equals(call)) {
				joinManager.delete(joinList.get(i));
			}
		}
	}

	public void loadContactListForNewCall() {
		System.out.println("SalesCRMCallBean.loadContactListForSelectedCall()");

		try {
			SalesContactPeopleManager peopleManager = new SalesContactPeopleManager();
			newContactPeopleList = peopleManager.findByField(
					SalesContactPeople.class, "salesCustomer",
					newCall.getSalesCustomer());
		} catch (Exception e) {
			System.out.println("SalesCRMCallBean:" + e.toString());
		}
	}

	public void loadContactListForSelectedCall() {
		System.out.println("SalesCRMCallBean.loadContactListForSelectedCall()");

		try {
			SalesContactPeopleManager peopleManager = new SalesContactPeopleManager();
			selectedContactPeopleList = peopleManager.findByField(
					SalesContactPeople.class, "salesCustomer",
					selectedCall.getSalesCustomer());
		} catch (Exception e) {
			System.out.println("SalesCRMCallBean:" + e.toString());
		}
	}

	/* end of JOIN FUNCTIONS */
	/*
	 * GETTERS AND SETTERS
	 */

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public SalesCRMCall getNewCall() {
		return newCall;
	}

	public void setNewCall(SalesCRMCall newCall) {
		this.newCall = newCall;
	}

	public SalesCRMCall getSelectedCall() {
		try {
			if (selectedCall == null)
				selectedCall = new SalesCRMCall();
		} catch (Exception ex) {
			selectedCall = new SalesCRMCall();
		}
		return selectedCall;
	}

	public void setSelectedCall(SalesCRMCall selectedCall) {
		this.selectedCall = selectedCall;
	}

	public List<SalesCRMCall> getCallList() {
		return callList;
	}

	public void setCallList(List<SalesCRMCall> callList) {
		this.callList = callList;
	}

	public List<SalesCRMCall> getFilteredList() {
		try {
			selectedCall = filteredList.get(0);
			salesMenuBean.showNewType1Menu();
		} catch (Exception ex) {
			System.out.println("getFilteredList: " + ex);
		}
		return filteredList;
	}

	public void setFilteredList(List<SalesCRMCall> filteredList) {
		this.filteredList = filteredList;
	}

	public SalesMenuBean getSalesMenuBean() {
		return salesMenuBean;
	}

	public void setSalesMenuBean(SalesMenuBean salesMenuBean) {
		this.salesMenuBean = salesMenuBean;
	}

	public List<SalesCRMCall> getCallsOfCustomer() {
		try {
			callsOfCustomer = new ArrayList<SalesCRMCall>();

			for (int i = 0; i < callList.size(); i++) {
				callsOfCustomer.add(callList.get(i));
			}
		} catch (Exception e) {
			System.out.println("SalesCRMCallBean:" + e.toString());
		}
		return callsOfCustomer;
	}

	public void setCallsOfCustomer(List<SalesCRMCall> callsOfCustomer) {
		this.callsOfCustomer = callsOfCustomer;
	}

	/**
	 * @return the employeeList
	 */
	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	/**
	 * @param employeeList
	 *            the employeeList to set
	 */
	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}

	/**
	 * @return the departmentId
	 */
	public Integer getDepartmentId() {
		return departmentId;
	}

	/**
	 * @param departmentId
	 *            the departmentId to set
	 */
	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the newContactPeopleList
	 */
	public List<SalesContactPeople> getNewContactPeopleList() {
		return newContactPeopleList;
	}

	/**
	 * @param newContactPeopleList
	 *            the newContactPeopleList to set
	 */
	public void setNewContactPeopleList(
			List<SalesContactPeople> newContactPeopleList) {
		this.newContactPeopleList = newContactPeopleList;
	}

	/**
	 * @return the selectedContactPeopleList
	 */
	public List<SalesContactPeople> getSelectedContactPeopleList() {
		return selectedContactPeopleList;
	}

	/**
	 * @param selectedContactPeopleList
	 *            the selectedContactPeopleList to set
	 */
	public void setSelectedContactPeopleList(
			List<SalesContactPeople> selectedContactPeopleList) {
		this.selectedContactPeopleList = selectedContactPeopleList;
	}

	/**
	 * @return the selectedEmployees
	 */
	public Employee[] getSelectedEmployees() {
		return selectedEmployees;
	}

	/**
	 * @param selectedEmployees
	 *            the selectedEmployees to set
	 */
	public void setSelectedEmployees(Employee[] selectedEmployees) {
		this.selectedEmployees = selectedEmployees;
	}
}
