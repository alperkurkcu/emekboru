/**
 * 
 */
package com.emekboru.beans.stok;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.config.ConfigBean;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */

@ManagedBean(name = "stokSayfalarBean")
@ViewScoped
public class StokSayfalarBean {

	@EJB
	private ConfigBean config;

	// Stok açılış sayfası
	public void goToStok() {

		FacesContextUtils.redirect(config.getPageUrl().STOK_MAIN_PAGE);
	}
	
	public void goToStokGirisCikisListesi() {

		FacesContextUtils.redirect(config.getPageUrl().STOK_GIRIS_CIKIS_LISTESI_PAGE);
	}

}
