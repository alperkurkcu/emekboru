package com.emekboru.beans.machine;

import java.awt.Color;
import java.awt.Toolkit;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.StreamedContent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.EdwPipeLink;
import com.emekboru.jpa.ElectrodeDustWire;
import com.emekboru.jpa.Machine;
import com.emekboru.jpa.MachineEdwLink;
import com.emekboru.jpa.MachinePipeLink;
import com.emekboru.jpa.MachineRuloLink;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.PipeMachineRuloLink;
import com.emekboru.jpa.machines.MachineSalesPipeNo;
import com.emekboru.jpa.rulo.Rulo;
import com.emekboru.jpa.rulo.RuloPipeLink;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.EdwPipeLinkManager;
import com.emekboru.jpaman.ElectrodeDustWireManager;
import com.emekboru.jpaman.Factory;
import com.emekboru.jpaman.MachineEdwLinkManager;
import com.emekboru.jpaman.MachineManager;
import com.emekboru.jpaman.MachinePipeLinkManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.machines.MachineSalesPipeNoManager;
import com.emekboru.jpaman.rulo.RuloManager;
import com.emekboru.jpaman.rulo.RuloPipeLinkManager;
import com.emekboru.messages.PipeStatus;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.FileOperations;
import com.emekboru.utils.ReportUtil;
import com.emekboru.utils.UtilInsCore;
import com.emekboru.utils.lazymodels.LazyRuloDataModel;
import com.emekboru.utils.lazymodels.LazySalesItemDataModel;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.Barcode128;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;

@ManagedBean(name = "spiralMachineBean")
@ViewScoped
public class SpiralMachineBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
	private ConfigBean config;
	// machine we select for operations
	private Machine selectedMachine;
	// selected materials to the machine
	private List<Rulo> selectedRulos;
	private List<ElectrodeDustWire> selectedDusts;
	private List<ElectrodeDustWire> selectedWires;
	private List<ElectrodeDustWire> selectedDustsIc;
	private List<ElectrodeDustWire> selectedWiresIcAc;
	private List<ElectrodeDustWire> selectedDustsDis;
	private List<ElectrodeDustWire> selectedWiresDisAc;
	private List<ElectrodeDustWire> selectedWiresIcDc;
	private List<ElectrodeDustWire> selectedWiresDisDc;
	// allocated materials to certain orders
	private AllocatedMaterials allocated;
	// the date set while starting production
	private Date startDate;
	private Date endDate;
	private boolean hasRulo = false;
	private boolean hasDust = false;
	private boolean hasWire = false;
	// attributes that are set while producing from Raw materials
	private MachineRuloLink ruloMachineLink;
	private MachineEdwLink dustMachineLink;
	private MachineEdwLink wireMachineLink;
	private MachineEdwLink selectedWireMachineLink;
	private List<MachineEdwLink> allWireMachineLink;
	private SalesItem salesItem;
	// selected attributes when we want to finish production
	private Rulo currentRulo;
	private ElectrodeDustWire currentDust;
	private ElectrodeDustWire currentWire;
	// attributes for pipe production
	private boolean canProducePipe;
	private List<Pipe> producedPipes;
	private Pipe pipe;
	private double cuttedRuloWidth;
	private List<Rulo> doubleRuloList;
	private Rulo doubleRulo;

	private Pipe selectedPipe;

	private MachineSalesPipeNo siralamaObjesi;
	private List<MachineSalesPipeNo> siralamaObjeleri;
	// private UretimParametreleri uretimParametreleri;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private String path = null;
	private File theFile = null;
	private String privatePath;
	private String downloadedFileName;
	private StreamedContent downloadedFile;

	// machine_pipe_link
	private MachinePipeLink machinePipeLink = new MachinePipeLink();

	private String ruloEk = null;

	private LazyDataModel<SalesItem> lazySalesItemList;
	private LazyDataModel<Rulo> lazyRuloList;

	public SpiralMachineBean() {

		selectedMachine = new Machine();
		currentRulo = new Rulo();
		dustMachineLink = new MachineEdwLink();
		wireMachineLink = new MachineEdwLink();
		allWireMachineLink = new ArrayList<MachineEdwLink>();
		currentDust = new ElectrodeDustWire();
		currentWire = new ElectrodeDustWire();
		salesItem = new SalesItem();
		pipe = new Pipe();
		doubleRulo = new Rulo();
		doubleRuloList = new ArrayList<Rulo>();
		producedPipes = new ArrayList<Pipe>();

		selectedRulos = new ArrayList<Rulo>();
		selectedDusts = new ArrayList<ElectrodeDustWire>();
		selectedWiresIcAc = new ArrayList<ElectrodeDustWire>();
		selectedDustsIc = new ArrayList<ElectrodeDustWire>();
		selectedWiresDisAc = new ArrayList<ElectrodeDustWire>();
		selectedDustsDis = new ArrayList<ElectrodeDustWire>();
		selectedWiresIcDc = new ArrayList<ElectrodeDustWire>();
		selectedWiresDisDc = new ArrayList<ElectrodeDustWire>();
		selectedWires = new ArrayList<ElectrodeDustWire>();
		allocated = new AllocatedMaterials();
		startDate = new Date();
		endDate = new Date();
		ruloMachineLink = new MachineRuloLink();

		selectedPipe = new Pipe();

		siralamaObjesi = new MachineSalesPipeNo();
		siralamaObjeleri = new ArrayList<MachineSalesPipeNo>();
		// uretimParametreleri = new UretimParametreleri();

		privatePath = File.separatorChar + "barkodlar" + File.separatorChar
				+ "boruBarkod" + File.separatorChar;

		try {
			this.setPath("C:/emekfiles" + privatePath);

			System.out.println("Path for Upload File (spiralMachineBean):			"
					+ path);
		} catch (Exception ex) {
			System.out.println("Path for Upload File (spiralMachineBean):			"
					+ FacesContext.getCurrentInstance().getExternalContext()
							.getRealPath(privatePath));

			System.out.print(path);
			System.out.println("CAUSE OF " + ex.toString());
		}

		theFile = new File(path);
		if (!theFile.isDirectory()) {
			theFile.mkdirs();
		}

		downloadedFileName = null;
	}

	// display the machine information to the MAIN PANEL
	public void viewMachine(Machine machine) {

		selectedMachine = machine;
		setCurrentRulo(machine.getMachineId());

		// sistem burada şişiyor
		setCurrentEDWbyMachineId(machine.getMachineId());

		// at the end check if we have enough materials to start production
		canProducePipe = canProducePipe();
		System.out.println("So can we produce pipe: " + canProducePipe);
	}

	@SuppressWarnings("static-access")
	protected void setCurrentEDWbyMachineId(int machineId) {

		ElectrodeDustWireManager edwManager = new ElectrodeDustWireManager();
		MachineEdwLinkManager medwMan = new MachineEdwLinkManager();

		// sistem burada şişiyor
		selectedDusts = edwManager.findDustsInMachine(selectedMachine
				.getMachineId());
		if (selectedDusts.size() > 0) {
			hasDust = true;
		}
		selectedDustsIc = edwManager.findDustByMachineIdByDurum(
				selectedMachine.getMachineId(), 1);
		if (selectedDustsIc.size() > 0) {
			hasDust = true;
		}
		selectedDustsDis = edwManager.findDustByMachineIdByDurum(
				selectedMachine.getMachineId(), 0);
		if (selectedDustsDis.size() > 0) {
			hasDust = true;
		}

		selectedWires = edwManager.findWiresInMachine(selectedMachine
				.getMachineId());
		if (selectedWires.size() > 0) {
			hasWire = true;
		}
		selectedWiresIcAc = edwManager.findWireByMachineIdByDurum(
				selectedMachine.getMachineId(), 1, 1);
		if (selectedWiresIcAc.size() > 0) {
			hasWire = true;
		}
		selectedWiresDisAc = edwManager.findWireByMachineIdByDurum(
				selectedMachine.getMachineId(), 0, 1);
		if (selectedWiresDisAc.size() > 0) {
			hasWire = true;
		}
		selectedWiresIcDc = edwManager.findWireByMachineIdByDurum(
				selectedMachine.getMachineId(), 1, 2);
		if (selectedWiresIcDc.size() > 0) {
			hasWire = true;
		}
		selectedWiresDisDc = edwManager.findWireByMachineIdByDurum(
				selectedMachine.getMachineId(), 0, 2);
		if (selectedWiresDisDc.size() > 0) {
			hasWire = true;
		}

		// ic diş bağlı tum teller
		allWireMachineLink = medwMan.findAllWireByMachineId(selectedMachine
				.getMachineId());

	}

	protected void setCurrentRulo(int machineId) {

		RuloManager ruloManager = new RuloManager();
		selectedRulos = ruloManager.findInMachine(machineId);
		// if we have at least one coil we need to set the order
		// we are working on
		if (selectedRulos.size() > 0) {
			hasRulo = true;
			// setOrder();
			// setSalesItem();
		} else
			hasRulo = false;
	}

	@SuppressWarnings("static-access")
	protected void setSelectedRuloNos(int prmMachineId, int prmItemId) {

		MachineSalesPipeNoManager mspnManager = new MachineSalesPipeNoManager();

		if (mspnManager.findByMachineSalesItem(selectedMachine.getMachineId(),
				salesItem.getItemId()).size() > 0) {
			siralamaObjesi = mspnManager.findByMachineSalesItem(
					selectedMachine.getMachineId(), salesItem.getItemId()).get(
					0);
		} else {
			siralamaObjesi = new MachineSalesPipeNo();
		}

	}

	// @SuppressWarnings("static-access")
	// protected void setUretimParametreleri(int prmMachineId, int prmItemId) {
	//
	// UretimParametreleriManager upManager = new UretimParametreleriManager();
	//
	// if (upManager.getAllUretimParametreleri(selectedMachine.getMachineId(),
	// salesItem.getItemId()).size() > 0) {
	// uretimParametreleri = upManager.getAllUretimParametreleri(
	// selectedMachine.getMachineId(), salesItem.getItemId()).get(
	// 0);
	// } else {
	// uretimParametreleri = new UretimParametreleri();
	// }
	//
	// }

	protected void setSalesItem() {
		Integer itemId = selectedRulos.get(0).getSalesItemId();
		if (itemId == null) {
			itemId = 0;
		}
		EntityManager man = Factory.getInstance().createEntityManager();
		salesItem = man.find(SalesItem.class, itemId);
	}

	// ***********************************************************************
	// //
	// ******************* SET MATERIALS TO SPIRAL MACHINE ****************** //
	// ***********************************************************************//

	public void sendRulosToMachine() {

		if (allocated.getSelectedRulos().length == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"RULO SEÇİLMEMİŞTİR, LÜTFEN RULO SEÇİNİZ!"));
			return;
		}
		salesItem.getItemId();
		// check if the selected rulos belong to the same order
		for (int i = 0; i < allocated.getSelectedRulos().length; i++) {
			Rulo ruloTo = allocated.getSelectedRulos()[i];
			for (int j = 0; j < allocated.getSelectedRulos().length; j++) {
				Rulo ruloFrom = allocated.getSelectedRulos()[j];
				if (i != j
						&& ruloTo.getSalesItemId() != ruloFrom.getSalesItemId()) {
					FacesContextUtils.addErrorMessage("NoSameOrderMessage");
					return;
				}
			}
		}

		RuloManager ruloManager = new RuloManager();
		EntityManager man = Factory.getInstance().createEntityManager();
		try {
			selectedMachine = man.find(Machine.class,
					selectedMachine.getMachineId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out
					.println("HATA - selectedMachine - spiralMachineBean.sendRulosToMachine - selectedMachine");
			e.printStackTrace();
		}
		man.getTransaction().begin();
		for (int i = 0; i < allocated.getSelectedRulos().length; i++) {
			Rulo rulo = allocated.getSelectedRulos()[i];
			if (selectedRulos.size() <= config.getConfig().getMachines()
					.getManufacturing().getSpiral().getRuloNumber()) {
				MachineRuloLink link = new MachineRuloLink();
				link.setRulo(rulo);
				link.setMachine(selectedMachine);
				link.setEnded(false);
				link.setPurpose(1);
				link.setStartDate(startDate);
				selectedMachine.getMachineRuloLinks().add(link);

				// update the rulo Status to IN_PRODUCTION and remove the rulo
				// from the list
				rulo.setStatuses(config.getConfig().getMaterials()
						.findStatusByName(Rulo.STATUS_IN_PRODUCTION));
				ruloManager.updateEntity(rulo);
				selectedRulos.add(rulo);
				// allocated.getAllocatedRulos().remove(rulo);
			}
		}
		man.getTransaction().commit();
		man.clear();

		// set the selected order
		// try {
		// salesItem = man.find(SalesItem.class, selectedRulos.get(0)
		// .getSalesItemId());
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// System.out
		// .println("HATA - salesItem - spiralMachineBean.sendRulosToMachine - salesItem");
		// e.printStackTrace();
		// }

		salesItem.getProposal().getSalesOrder().getOrderId();
		man.close();

		canProducePipe = canProducePipe();
		viewMachine(selectedMachine);
	}

	public void sendDustsToMachine() {

		ElectrodeDustWireManager edwManager = new ElectrodeDustWireManager();
		EntityManager man = Factory.getInstance().createEntityManager();
		selectedMachine = man.find(Machine.class,
				selectedMachine.getMachineId());
		man.getTransaction().begin();
		for (int i = 0; i < allocated.getSelectedDusts().length; i++) {
			ElectrodeDustWire dust = allocated.getSelectedDusts()[i];
			if (dustMachineLink.getIcDis() == 1
					&& selectedDusts.size() <= config.getConfig().getMachines()
							.getManufacturing().getSpiral()
							.getMaxDustIcNumber()) {
				MachineEdwLink link = new MachineEdwLink();
				link.setEdw(dust);
				link.setMachine(selectedMachine);
				link.setStartDate(startDate);
				link.setElectrodeDustWireId(dust.getElectrodeDustWireId());
				link.setAcDc(dustMachineLink.getAcDc());
				link.setIcDis(dustMachineLink.getIcDis());
				selectedMachine.getMachineEdwLinks().add(link);
				// update the dust Status to IN_PRODUCTION and remove
				// it from the list
				dust.setStatus(config.getConfig().getMaterials()
						.findStatusByName(Rulo.STATUS_IN_PRODUCTION));
				edwManager.updateEntity(dust);
				selectedDusts.add(dust);
				allocated.getAllocatedDusts().remove(dust);
			} else if (dustMachineLink.getIcDis() == 0
					&& selectedDusts.size() <= config.getConfig().getMachines()
							.getManufacturing().getSpiral()
							.getMaxDustDisNumber()) {
				MachineEdwLink link = new MachineEdwLink();
				link.setEdw(dust);
				link.setMachine(selectedMachine);
				link.setStartDate(startDate);
				link.setElectrodeDustWireId(dust.getElectrodeDustWireId());
				link.setAcDc(dustMachineLink.getAcDc());
				link.setIcDis(dustMachineLink.getIcDis());
				selectedMachine.getMachineEdwLinks().add(link);
				// update the dust Status to IN_PRODUCTION and remove
				// it from the list
				dust.setStatus(config.getConfig().getMaterials()
						.findStatusByName(Rulo.STATUS_IN_PRODUCTION));
				edwManager.updateEntity(dust);
				selectedDusts.add(dust);
				allocated.getAllocatedDusts().remove(dust);
			} else {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"DAHA FAZLA TOZ EKLENEMEZ!"));
			}
		}
		man.getTransaction().commit();
		man.close();

		allocated.setSelectedDusts(null);

		canProducePipe = canProducePipe();
		viewMachine(selectedMachine);
	}

	public void sendWiresToMachine() {

		ElectrodeDustWireManager edwManager = new ElectrodeDustWireManager();
		EntityManager man = Factory.getInstance().createEntityManager();
		selectedMachine = man.find(Machine.class,
				selectedMachine.getMachineId());
		man.getTransaction().begin();
		// for (int i = 0; i < allocated.getSelectedWires().length; i++) {
		// ElectrodeDustWire wire = allocated.getSelectedWires()[i];
		ElectrodeDustWire wire = allocated.getSelectedWire();
		if (wireMachineLink.getIcDis() == 1
				&& (selectedWiresIcAc.size() + selectedWiresIcDc.size()) <= config
						.getConfig().getMachines().getManufacturing()
						.getSpiral().getMaxWireIcNumber()) {
			MachineEdwLink link = new MachineEdwLink();
			link.setEdw(wire);
			link.setMachine(selectedMachine);
			link.setStartDate(startDate);
			link.setElectrodeDustWireId(wire.getElectrodeDustWireId());
			link.setAcDc(wireMachineLink.getAcDc());
			link.setIcDis(wireMachineLink.getIcDis());
			selectedMachine.getMachineEdwLinks().add(link);
			// update the dust Status to IN_PRODUCTION and remove it from
			// allocation list
			wire.setStatus(config.getConfig().getMaterials()
					.findStatusByName(Rulo.STATUS_IN_PRODUCTION));
			edwManager.updateEntity(wire);
			selectedWires.add(wire);
			allocated.getAllocatedWires().remove(wire);
		} else if (wireMachineLink.getIcDis() == 0
				&& (selectedWiresDisAc.size() + selectedWiresDisDc.size()) <= config
						.getConfig().getMachines().getManufacturing()
						.getSpiral().getMaxWireDisNumber()) {
			MachineEdwLink link = new MachineEdwLink();
			link.setEdw(wire);
			link.setMachine(selectedMachine);
			link.setStartDate(startDate);
			link.setElectrodeDustWireId(wire.getElectrodeDustWireId());
			link.setAcDc(wireMachineLink.getAcDc());
			link.setIcDis(wireMachineLink.getIcDis());
			selectedMachine.getMachineEdwLinks().add(link);
			// update the dust Status to IN_PRODUCTION and remove it from
			// allocation list
			wire.setStatus(config.getConfig().getMaterials()
					.findStatusByName(Rulo.STATUS_IN_PRODUCTION));
			edwManager.updateEntity(wire);
			selectedWires.add(wire);
			allocated.getAllocatedWires().remove(wire);
		} else {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"DAHA FAZLA TEL EKLENEMEZ!"));
		}
		// }
		man.getTransaction().commit();
		man.close();

		allocated.setSelectedWires(null);
		allocated.setSelectedWire(null);

		canProducePipe = canProducePipe();
		viewMachine(selectedMachine);
	}

	public void setMachineSalesItemPipeNo() {
		FacesContext context = FacesContext.getCurrentInstance();

		if (!baslangicBitisKontrol()) {
			return;
		}

		MachineSalesPipeNoManager manager = new MachineSalesPipeNoManager();
		if (siralamaObjesi.getId() == null) {
			siralamaObjesi.setSalesItem(salesItem);
			siralamaObjesi.setMachine(selectedMachine);
			siralamaObjesi.setEklemeZamani(UtilInsCore.getTarihZaman());
			siralamaObjesi.setEkleyenKullanici(userBean.getUser().getId());
			siralamaObjesi.setDurum(true);
			manager.enterNew(siralamaObjesi);
			context.addMessage(null, new FacesMessage(
					"BAŞLANGIÇ BİTİŞ NUMARASI BAŞARIYLA EKLENMİŞTİR!"));
		} else {
			siralamaObjesi.setSalesItem(salesItem);
			siralamaObjesi.setMachine(selectedMachine);
			siralamaObjesi.setGuncellemeZamani(UtilInsCore.getTarihZaman());
			siralamaObjesi.setGuncelleyenKullanici(userBean.getUser().getId());
			siralamaObjesi.setDurum(true);
			manager.updateEntity(siralamaObjesi);

			context.addMessage(null, new FacesMessage(
					"BAŞLANGIÇ BİTİŞ NUMARASI BAŞARIYLA GÜNCELLENMİŞTİR!"));
		}

		canProducePipe = canProducePipe();
	}

	@SuppressWarnings("static-access")
	public boolean baslangicBitisKontrol() {
		MachineSalesPipeNoManager manager = new MachineSalesPipeNoManager();
		FacesContext context = FacesContext.getCurrentInstance();

		siralamaObjeleri = manager.findBySalesItem(
				selectedMachine.getMachineId(), salesItem.getItemId());

		if (siralamaObjesi.getFirstPipeNo() == 0
				|| siralamaObjesi.getLastPipeNo() == 0) {
			context.addMessage(null, new FacesMessage(
					"'0' BORU NUMARASI VERİLEMEZ!"));
			return false;
		}

		if (siralamaObjesi.getFirstPipeNo() >= siralamaObjesi.getLastPipeNo()) {
			context.addMessage(null, new FacesMessage(
					"BAŞLANGIÇ NUMARASI BİTİŞ NUMARASINDAN KÜÇÜK OLAMAZ!"));
			return false;
		}

		if (siralamaObjeleri.size() == 0) {
			return true;
		} else if (siralamaObjeleri.size() > 0) {
			for (int i = 0; i < siralamaObjeleri.size(); i++) {
				if (siralamaObjesi.getFirstPipeNo() >= siralamaObjeleri.get(i)
						.getFirstPipeNo()
						&& siralamaObjesi.getFirstPipeNo() >= siralamaObjeleri
								.get(i).getLastPipeNo()) {
					return true;
				} else if (siralamaObjesi.getLastPipeNo() <= siralamaObjeleri
						.get(i).getFirstPipeNo()
						&& siralamaObjesi.getLastPipeNo() <= siralamaObjeleri
								.get(i).getLastPipeNo()) {
					return true;
				}
			}
		}
		context.addMessage(
				null,
				new FacesMessage(
						"DİĞER SPİRAL MAKİNELERİNDE SAYI ARALIĞI ÇAKIŞIYOR! BU NUMARALAR VERİLEMEZ! KONTROL EDİNİZ!"));
		return false;
	}

	public void deleteMachineSalesItemPipeNo() {
		FacesContext context = FacesContext.getCurrentInstance();
		MachineSalesPipeNoManager manager = new MachineSalesPipeNoManager();
		if (siralamaObjesi.getId() == null) {
			context.addMessage(null, new FacesMessage(
					"BAŞLANGIÇ BİTİŞ NUMARASI BULUNAMADI, SİLME BAŞARISIZ!"));
		} else {
			siralamaObjesi.setSalesItem(salesItem);
			siralamaObjesi.setMachine(selectedMachine);
			siralamaObjesi.setGuncellemeZamani(UtilInsCore.getTarihZaman());
			siralamaObjesi.setGuncelleyenKullanici(userBean.getUser().getId());
			siralamaObjesi.setDurum(false);
			manager.updateEntity(siralamaObjesi);
			siralamaObjesi = new MachineSalesPipeNo();
			context.addMessage(null, new FacesMessage(
					"BAŞLANGIÇ BİTİŞ NUMARASI BAŞARIYLA SİLİNMİŞTİR!"));
		}

		canProducePipe = canProducePipe();
	}

	// ***********************************************************************
	// //
	// ******************* LOAD ALLOCATED MATERIALS METHODS ******************
	// //
	// ***********************************************************************
	// //

	// public void loadAllocatedRulos() {
	//
	// RuloManager ruloManager = new RuloManager();
	// allocated.setAllocatedRulos((List<Rulo>) ruloManager
	// .getAvailableRulosForSpiralMachine(order));
	// }
	public void loadAllocatedRulos() {
		RuloManager ruloManager = new RuloManager();

		if (salesItem == null || salesItem.getItemId() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"SİPARİŞ SEÇİLMEMİŞTİR, TEKRAR SİPARİŞ SEÇİNİZ!"));
			return;
		}

		// if (salesItem.getItemId() == 0) {
		//
		// allocated.setAllocatedRulos((List<Rulo>) ruloManager
		// .getAvailableRulosForSpiralMachine(allocated
		// .getSelectedSalesItems()));
		// } else {
		//
		// allocated.setAllocatedRulos((List<Rulo>) ruloManager
		// .getAvailableRulosForSpiralMachine(salesItem));
		// }

		if (salesItem.getItemId() == 0) {

			allocated
					.setAllocatedRulosLazy(lazyRuloList = new LazyRuloDataModel(
							allocated.getSelectedSalesItems()));
		} else {

			allocated
					.setAllocatedRulosLazy(lazyRuloList = new LazyRuloDataModel(
							salesItem));
		}
	}

	public void loadAllocatedDusts() {

		ElectrodeDustWireManager edwManager = new ElectrodeDustWireManager();
		allocated.setAllocatedDusts(edwManager.findAllocatedDusts(salesItem));
	}

	public void loadAllocatedWires() {

		ElectrodeDustWireManager edwManager = new ElectrodeDustWireManager();
		allocated.setAllocatedWires(edwManager.findAllocatedWires(salesItem));
	}

	// ***********************************************************************
	// //
	// ******************* COMPLETE PRODUCTION FROM RAW MATERIALS ************
	// //
	// ***********************************************************************
	// //

	public void beforeRuloProduction(Rulo rulo) {

		RuloManager man = new RuloManager();
		currentRulo = rulo;
		ruloMachineLink = man.findCurrentLinkInMachine(selectedMachine, rulo);
	}

	// check if the amount (consumed and scraped) its not greater than
	// the remaining amount. update the MachineRuloLink and change the
	// status of the rulo to available/depleted (based on the remaining amount)
	public void ruloProduction() {

		Float usedAmount = ruloMachineLink.getAmountConsumed()
				+ ruloMachineLink.getAmountScrap();

		if (currentRulo.getRuloOzellikBoyutsal()
				.getRuloOzellikBoyutsalKalanMiktar() == null) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							"RULO ÖZELLİKLERİNDE KALAN MİKTAR BELİRTİLMEMİŞ, RULO KULLANILAMADI!"));
			return;
		}

		if (currentRulo.getRuloOzellikBoyutsal()
				.getRuloOzellikBoyutsalKalanMiktar() < usedAmount) {

			FacesContextUtils.addErrorMessage("WrongAmountMessage");
			return;
		}

		if (currentRulo.getRuloOzellikBoyutsal()
				.getRuloOzellikBoyutsalKalanMiktar() == usedAmount) {

			CommonQueries<MachineRuloLink> cl = new CommonQueries<MachineRuloLink>();
			ruloMachineLink.setEnded(true);
			cl.updateEntity(ruloMachineLink);
		}

		RuloManager ruloMan = new RuloManager();
		currentRulo.getRuloOzellikBoyutsal().setRuloOzellikBoyutsalKalanMiktar(
				currentRulo.getRuloOzellikBoyutsal()
						.getRuloOzellikBoyutsalMiktar() - usedAmount);
		currentRulo.setSalesItemId(0);
		if (currentRulo.getRuloOzellikBoyutsal()
				.getRuloOzellikBoyutsalKalanMiktar() <= 0) {
			currentRulo.setStatuses(config.getConfig().getMaterials()
					.findStatusByName(Rulo.STATUS_DEPLETED));
		} else {
			currentRulo.setStatuses(config.getConfig().getMaterials()
					.findStatusByName(Rulo.STATUS_FREE));
		}
		ruloMan.updateEntity(currentRulo);
		// remove the completed coil from the list
		selectedRulos.remove(currentRulo);
		ruloMan.deleteMachineRuloLink(currentRulo);

		ruloMachineLink = new MachineRuloLink();
		FacesContextUtils.addInfoMessage("SubmitMessage");
		canProducePipe = canProducePipe();
	}

	public void cancelRuloProduction(Rulo rulo) {

		RuloManager man = new RuloManager();
		currentRulo = rulo;
		ruloMachineLink = man.findCurrentLinkInMachine(selectedMachine, rulo);
		// if we found some PipeMachineRuloLinks for the machineRuloLink it
		// means
		// that the production has started and we cannot delete it
		if (ruloMachineLink.getPmrLinks() == null
				|| ruloMachineLink.getPmrLinks().size() > 0) {
			FacesContextUtils.addErrorMessage("CoilPipeRelatedMessage");
			return;
		}
		// if we didn't produce any pipe then just remove the MachineRuloLink
		EntityManager ruloMan = Factory.getInstance().createEntityManager();
		currentRulo = ruloMan.find(Rulo.class, currentRulo.getRuloId());
		ruloMan.getTransaction().begin();
		currentRulo.getMachineRuloLinks().remove(ruloMachineLink);
		// change the coil status back to ALLOCATED
		// currentRulo.setStatuses(config.getConfig().getMaterials().findStatusByName(Rulo.STATUS_ALLOCATED));
		currentRulo.setStatuses(config.getConfig().getMaterials()
				.findStatusByName(Rulo.STATUS_FREE));
		ruloMan.getTransaction().commit();
		ruloMan.close();
		selectedRulos.remove(currentRulo);
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// check the number of resources available after this remove in order
		// to make it impossible the production of pipes with insufficient
		// of resources
		canProducePipe = canProducePipe();
		viewMachine(selectedMachine);
	}

	public void beforeDustProduction(ElectrodeDustWire dust) {

		ElectrodeDustWireManager man = new ElectrodeDustWireManager();
		currentDust = dust;
		dustMachineLink = man.findCurrentDustInMachine(selectedMachine, dust);
		viewMachine(selectedMachine);
	}

	public void dustProduction() {

		if (dustMachineLink.getEndDate().before(dustMachineLink.getStartDate())) {
			FacesContextUtils.addErrorMessage("WrongDateMessage");
			return;
		}
		if (currentDust.getRemainingAmount()
				- dustMachineLink.getAmountConsumed() < 0) {
			FacesContextUtils.addErrorMessage("WrongAmountMessage");
			return;
		}

		ElectrodeDustWireManager dustManager = new ElectrodeDustWireManager();
		currentDust.setRemainingAmount(currentDust.getRemainingAmount()
				- dustMachineLink.getAmountConsumed());
		currentDust.setSalesItemId(0);

		if (currentDust.getRemainingAmount() <= 0) {
			currentDust.setStatus(config.getConfig().getMaterials()
					.findStatusByName(Rulo.STATUS_DEPLETED));
		} else {
			currentDust.setStatus(config.getConfig().getMaterials()
					.findStatusByName(Rulo.STATUS_FREE));
		}

		dustManager.updateEntity(currentDust);
		selectedDusts.remove(currentDust);

		// update the link as well
		CommonQueries<MachineEdwLink> edwL = new CommonQueries<MachineEdwLink>();
		dustMachineLink.setEnded(true);
		edwL.updateEntity(dustMachineLink);
		dustMachineLink = new MachineEdwLink();
		currentDust = new ElectrodeDustWire();
		FacesContextUtils.addInfoMessage("SubmitMessage");
		canProducePipe = canProducePipe();
		viewMachine(selectedMachine);
	}

	public void cancelDustProduction(ElectrodeDustWire dust) {

		ElectrodeDustWireManager man = new ElectrodeDustWireManager();
		currentDust = dust;
		dustMachineLink = man.findCurrentDustInMachine(selectedMachine,
				currentDust);
		// if we found some edw_pipe_link for the machine_edw_link it
		// means
		// that the production has started and we cannot delete it
		if (dustMachineLink.getEdwLinks() == null
				|| dustMachineLink.getEdwLinks().size() > 0) {
			FacesContextUtils.addErrorMessage("DustPipeRelatedMessage");
			return;
		}
		EntityManager dustMan = Factory.getInstance().createEntityManager();
		currentDust = dustMan.find(ElectrodeDustWire.class,
				currentDust.getElectrodeDustWireId());
		dustMan.getTransaction().begin();
		// currentDust.setStatus(config.getConfig().getMaterials().findStatusByName(Rulo.STATUS_ALLOCATED));
		currentDust.getMachineEdwLinks().remove(dustMachineLink);
		dustMan.getTransaction().commit();
		dustMan.close();
		selectedDusts.remove(currentDust);
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// check the number of resources available after this remove in order
		// to make it impossible the production of pipes with insufficient
		// of resources
		canProducePipe = canProducePipe();
		viewMachine(selectedMachine);
	}

	public void beforeWireProduction(ElectrodeDustWire wire) {

		ElectrodeDustWireManager man = new ElectrodeDustWireManager();
		currentWire = wire;
		wireMachineLink = man.findCurrentWireInMachine(selectedMachine, wire);
		viewMachine(selectedMachine);
	}

	public void wireProduction() {

		if (wireMachineLink.getEndDate().before(wireMachineLink.getStartDate())) {
			FacesContextUtils.addErrorMessage("WrongDateMessage");
			return;
		}

		if (currentWire.getRemainingAmount()
				- wireMachineLink.getAmountConsumed() < 0) {
			FacesContextUtils.addErrorMessage("WrongAmountMessage");
			return;
		}

		ElectrodeDustWireManager wireManager = new ElectrodeDustWireManager();
		currentWire.setRemainingAmount(currentWire.getRemainingAmount()
				- wireMachineLink.getAmountConsumed());
		currentWire.setSalesItemId(0);
		if (currentWire.getRemainingAmount() <= 0) {
			currentWire.setStatus(config.getConfig().getMaterials()
					.findStatusByName(Rulo.STATUS_DEPLETED));
		} else {
			currentWire.setStatus(config.getConfig().getMaterials()
					.findStatusByName(Rulo.STATUS_FREE));
		}
		wireManager.updateEntity(currentWire);
		selectedWires.remove(currentWire);
		// update the link as well
		CommonQueries<MachineEdwLink> edwL = new CommonQueries<MachineEdwLink>();
		wireMachineLink.setEnded(true);
		edwL.updateEntity(wireMachineLink);

		wireMachineLink = new MachineEdwLink();
		currentWire = new ElectrodeDustWire();
		FacesContextUtils.addInfoMessage("SubmitMessage");
		canProducePipe = canProducePipe();
		viewMachine(selectedMachine);
	}

	public void cancelWireProduction(ElectrodeDustWire wire) {

		ElectrodeDustWireManager man = new ElectrodeDustWireManager();
		currentWire = wire;
		wireMachineLink = man.findCurrentWireInMachine(selectedMachine,
				currentWire);
		// if we found some edw_pipe_link for the machine_edw_link it
		// means
		// that the production has started and we cannot delete it
		if (wireMachineLink.getEdwLinks() == null
				|| wireMachineLink.getEdwLinks().size() > 0) {
			FacesContextUtils.addErrorMessage("WirePipeRelatedMessage");
			return;
		}
		EntityManager wireMan = Factory.getInstance().createEntityManager();
		currentWire = wireMan.find(ElectrodeDustWire.class,
				currentWire.getElectrodeDustWireId());
		wireMan.getTransaction().begin();
		// currentWire.setStatus(config.getConfig().getMaterials().findStatusByName(Rulo.STATUS_ALLOCATED));
		currentWire.getMachineEdwLinks().remove(wireMachineLink);
		wireMan.getTransaction().commit();
		wireMan.close();
		selectedWires.remove(currentWire);
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// check the number of resources available after this remove in order
		// to make it impossible the production of pipes with insufficient
		// of resources
		canProducePipe = canProducePipe();
		viewMachine(selectedMachine);
	}

	// ***********************************************************************
	// //
	// ******************* PIPE PRODUCTION AND DOUBLE COIL CASE **************
	// //
	// ***********************************************************************
	// //
	// public void beforePipeProduction() {
	//
	// // to start pipe production we need 1coil/4dusts/4wires
	// canProducePipe = canProducePipe();
	// if (!canProducePipe) {
	// return;
	// }
	// // load the pipes that were produced so far for the selected order
	// PipeManager man = new PipeManager();
	// producedPipes = man.findByOrderId(order.getOrderId());
	// }

	public void beforePipeProduction() {

		// to start pipe production we need 1coil/2dusts/2wires
		canProducePipe = canProducePipe();
		if (!canProducePipe) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							"BORU ÜRETİMİ İÇİN GEREKLİ MİNİMUM MALZEMELER EKSİK, LÜTFEN KAYNAK TOZU, KAYNAK TELİ ve RULOYU KONTROL EDİNİZ!"));
			return;
		}

		// orjinal producedPipes();
		producedPipes();
		// seçilen siparişten veriler otomatik gelmesi için
		pipeNew();
	}

	// A.K.
	public void producedPipes() {
		// load the pipes that were produced so far for the selected order
		// PipeManager pipeMan = new PipeManager();
		// producedPipes = pipeMan.findByItemId(salesItem.getItemId());
		// FacesContext context = FacesContext.getCurrentInstance();
		// context.addMessage(null, new
		// FacesMessage("BORULAR LİSTELENMİŞTİR!"));
		// return;
		PipeManager pipeMan = new PipeManager();
		producedPipes = new ArrayList<Pipe>();
		// producedPipes = pipeMan.findByItemIdMachineId(salesItem.getItemId(),
		// selectedMachine.getMachineId());
		producedPipes = pipeMan.findByItemIdMachineIdLast100(
				salesItem.getItemId(), selectedMachine.getMachineId());
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage("BORULAR LİSTELENMİŞTİR!"));
		return;
	}

	// public void pipeProduction() {
	//
	// pipe.setOrder(order);
	//
	// // bandEki var m�?
	//
	// // CoilManager coilMan = new CoilManager();
	// RuloManager ruloMan = new RuloManager();
	// PipeManager pipeMan = new PipeManager();
	// EdwPipeLinkManager edwPipeLinkMan = new EdwPipeLinkManager();
	// pipe.setPipeId(pipeMan.getMaxPipeId().get(0).intValue() + 1);
	// // add the links to pipe (EdwLink & CoilLink)
	// for (Rulo c : selectedRulos) {
	// PipeMachineRuloLink pmrl = new PipeMachineRuloLink();
	// MachineRuloLink cLink = ruloMan.findCurrentLinkInMachine(
	// selectedMachine, c);
	// pmrl.setMachineRuloLink(cLink);
	// pmrl.setPipe(pipe);
	// pipe.getPmrLinks().add(pmrl);
	//
	// System.out.println("how many coils : " + pipe.getPmrLinks().size());
	// }
	// for (ElectrodeDustWire dust : selectedDusts) {
	//
	// EdwPipeLink dustLink = new EdwPipeLink();
	// dustLink.setEdw(dust);
	// dustLink.setPipe(pipe);
	// pipe.getEdwPipeLinks().add(dustLink);
	// System.out.println("how many DUSTS : "
	// + pipe.getEdwPipeLinks().size());
	// }
	//
	// for (ElectrodeDustWire wire : selectedWires) {
	//
	// EdwPipeLink wireLink = new EdwPipeLink();
	// wireLink.setEdw(wire);
	// wireLink.setPipe(pipe);
	// pipe.getEdwPipeLinks().add(wireLink);
	// System.out.println("how many WIRES: "
	// + pipe.getEdwPipeLinks().size());
	// }
	//
	// pipe.setStatus(PipeStatus.FREE);
	// List<Integer> indexList = pipeMan.getOrderMaxPipeIndex(pipe.getOrder()
	// .getOrderId());
	// if (indexList.size() == 0) {
	// pipe.setPipeIndex(1);
	// } else {
	// pipe.setPipeIndex(indexList.get(0) + 1);
	// }
	//
	// // add the pipe to the Pipe table
	// // pipeMan.enterNew(pipe);
	// pipeMan.insertPipe(pipe);
	// // add the pipe to the list of order's PIPES
	// producedPipes.add(pipe);
	// // add used edw and pipe to EdwPipeLink table
	// for (int j = 0; j < pipe.getEdwPipeLinks().size(); j++) {
	// edwPipeLinkMan.insertEdwPipeLink(pipe.getEdwPipeLinks().get(j)
	// .getEdw().getElectrodeDustWireId(), pipe.getPipeId());
	// }
	// // insert to RuloPipeLink table
	// pipeMan.insertRuloPipeLink(pipe);
	// // submitCompletedTest();
	// FacesContextUtils.addInfoMessage("SubmitMessage");
	// }

	@SuppressWarnings("static-access")
	public void pipeProduction() {

		RuloManager ruloMan = new RuloManager();
		PipeManager pipeMan = new PipeManager();
		MachinePipeLinkManager mplManager = new MachinePipeLinkManager();

		Pipe sonBoru = new Pipe();
		// if (pipeMan.findByItemId(salesItem.getItemId()).size() > 0) {
		// sonBoru = pipeMan.findByItemId(salesItem.getItemId()).get(0);
		//
		// long diff = Math.abs(System.currentTimeMillis()
		// - sonBoru.getEklemeZamani().getTime());
		//
		// if (diff < 60000) {// son eklenen boru ile şimdiki boru arasında
		// // 1dakikadan az zaman var
		//
		// FacesContext context = FacesContext.getCurrentInstance();
		// context.addMessage(
		// null,
		// new FacesMessage(
		// FacesMessage.SEVERITY_FATAL,
		// "LÜTFEN 60 SANİYE BEKLEYİNİZ! ÇOK HIZLI BORU ÜRETİMİ VAR, SON BORU SİSTEME EKLENMEDİ!",
		// null));
		// return;
		// }
		if (mplManager.findByItemIdMachineIdLimited(salesItem.getItemId(),
				selectedMachine.getMachineId()).size() > 0) {
			sonBoru = mplManager
					.findByItemIdMachineIdLimited(salesItem.getItemId(),
							selectedMachine.getMachineId()).get(0).getPipe();

			long diff = Math.abs(System.currentTimeMillis()
					- sonBoru.getEklemeZamani().getTime());

			if (diff < 30000) {// son eklenen boru ile şimdiki boru arasında
								// 1dakikadan az zaman var

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"LÜTFEN 30 SANİYE BEKLEYİNİZ! ÇOK HIZLI BORU ÜRETİMİ VAR, SON BORU SİSTEME EKLENMEDİ!",
								null));
				return;
			}
		}

		// orijinal
		// pipe.setStatus(PipeStatus.FREE);
		// List<Integer> indexList = pipeMan.getItemMaxPipeIndex(pipe
		// .getSalesItem().getItemId());
		// if (indexList.size() == 0 || indexList.get(0) == null) {
		// pipe.setPipeIndex(1);
		// } else {
		// pipe.setPipeIndex(indexList.get(0) + 1);
		// }
		// orijinal
		pipe.setStatus(PipeStatus.FREE);
		pipe.setSalesItem(salesItem);

		MachineSalesPipeNoManager mspnManager = new MachineSalesPipeNoManager();

		if (mspnManager.findByMachineSalesItem(selectedMachine.getMachineId(),
				salesItem.getItemId()).size() > 0) {// farklı makinelerde aynı
													// sipariş üretimi için
			siralamaObjesi = mspnManager.findByMachineSalesItem(
					selectedMachine.getMachineId(), salesItem.getItemId()).get(
					0);

			Integer indexList = 0;
			if (mplManager.findByItemIdMachineIdLimited(salesItem.getItemId(),
					selectedMachine.getMachineId()).size() > 0) {
				indexList = mplManager
						.findByItemIdMachineIdLimited(salesItem.getItemId(),
								selectedMachine.getMachineId()).get(0)
						.getPipe().getPipeIndex();
			}

			if (indexList == 0) {
				pipe.setPipeIndex(siralamaObjesi.getFirstPipeNo());// ilk
																	// boruysa
			} else if (indexList + 1 < siralamaObjesi.getLastPipeNo()) {
				if (indexList + 1 <= siralamaObjesi.getFirstPipeNo()) {
					pipe.setPipeIndex(siralamaObjesi.getFirstPipeNo());
				} else {
					pipe.setPipeIndex(indexList + 1);
				}
			} else if (indexList + 1 > siralamaObjesi.getLastPipeNo()) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"EKLENECEK BORU MAKİNEDE ÜRETİLECEK SON BORU NUMARASINI AŞMAKTADIR, LÜTFEN KONTROL EDİNİZ!",
								null));
				return;
			}
		} else {
			List<Integer> indexList = pipeMan.getItemMaxPipeIndex(pipe
					.getSalesItem().getItemId());
			if (indexList.size() == 0 || indexList.get(0) == null) {
				pipe.setPipeIndex(1);
			} else {
				pipe.setPipeIndex(indexList.get(0) + 1);
			}
		}
		// aynı boru numarası verme kontrolu
		if (pipeMan.findByItemIdIndexId(salesItem.getItemId(),
				pipe.getPipeIndex()).size() > 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_FATAL,
							"BORU NUMARALANDIRMASINDA HATA VAR, BU BORU NUMARASI "
									+ pipe.getPipeIndex()
									+ " DAHA ÖNCEDEN VERİLMİŞ. LÜTFEN BİLGİ İŞLEME BİLDİRİNİZ!",
							null));
			return;
		}

		EdwPipeLinkManager edwPipeLinkMan = new EdwPipeLinkManager();
		pipe.setPipeId(pipeMan.getMaxPipeId().get(0).intValue() + 1);
		// add the links to pipe (EdwLink & CoilLink)
		for (Rulo c : selectedRulos) {
			PipeMachineRuloLink pmrl = new PipeMachineRuloLink();
			MachineRuloLink cLink = ruloMan.findCurrentLinkInMachine(
					selectedMachine, c);
			pmrl.setMachineRuloLink(cLink);
			pmrl.setPipe(pipe);
			pipe.getPmrLinks().add(pmrl);

			System.out.println("how many coils : " + pipe.getPmrLinks().size());
		}

		for (ElectrodeDustWire dust : selectedDusts) {

			EdwPipeLink dustLink = new EdwPipeLink();
			dustLink.setEdw(dust);
			dustLink.setPipe(pipe);
			pipe.getEdwPipeLinks().add(dustLink);
			System.out.println("how many DUSTS : "
					+ pipe.getEdwPipeLinks().size());
		}

		// for (ElectrodeDustWire wire : selectedWires) {
		for (MachineEdwLink wire : allWireMachineLink) {
			EdwPipeLink wireLink = new EdwPipeLink();
			wireLink.setEdw(wire.getEdw());
			wireLink.setPipe(pipe);
			wireLink.setAkim(wire.getAkim());
			wireLink.setVolt(wire.getVolt());
			wireLink.setAcDc(wire.getAcDc());
			wireLink.setIcDis(wire.getIcDis());
			pipe.getEdwPipeLinks().add(wireLink);
			System.out.println("how many WIRES: "
					+ pipe.getEdwPipeLinks().size());
		}

		pipe.setEklemeZamani(UtilInsCore.getTarihZaman());
		pipe.setEkleyenKullanici(userBean.getUser().getId());

		// add the pipe to the Pipe table
		// pipeMan.enterNew(pipe);
		pipeMan.insertPipe(pipe);
		// add the pipe to the list of order's PIPES
		producedPipes.add(pipe);
		// add used edw and pipe to EdwPipeLink table
		for (int j = 0; j < pipe.getEdwPipeLinks().size(); j++) {
			edwPipeLinkMan.insertEdwPipeLink(pipe.getEdwPipeLinks().get(j));
		}

		// insert to RuloPipeLink table
		pipeMan.insertRuloPipeLink(pipe, selectedRulos.get(0));
		// submitCompletedTest();
		FacesContextUtils.addInfoMessage("PipeSubmitMessage");

		// machine_pipe_link
		MachinePipeLinkManager mplMan = new MachinePipeLinkManager();
		machinePipeLink.setMachine(selectedMachine);
		machinePipeLink.setPipe(pipe);
		machinePipeLink.setDate(UtilInsCore.getTarihZaman());
		mplMan.enterNew(machinePipeLink);

		// for unselect list
		pipe = new Pipe();
		machinePipeLink = new MachinePipeLink();
		producedPipes();
	}

	public void pipeEdit() {

		PipeManager pipeMan = new PipeManager();

		if (pipe.getPipeId() == null) {

			FacesContextUtils.addInfoMessage("PipeNoSelectMessage");
			return;
		} else {
			pipe.setGuncellemeZamani(UtilInsCore.getTarihZaman());
			pipe.setGuncelleyenKullanici(userBean.getUser().getId());
			pipeMan.updateEntity(pipe);
			FacesContextUtils.addInfoMessage("PipeUpdateMessage");
		}
	}

	public void pipeNew() {

		pipe = new Pipe();
		pipe.setDisCap(salesItem.getDiameter());
		pipe.setEtKalinligi(salesItem.getThickness());
		pipe.setBoy(salesItem.getPipeLength());

		// boru agırlıgı hesaplama
		Double disCap = pipe.getDisCap();
		Float etKalinligi = selectedRulos.get(0).getRuloOzellikBoyutsal()
				.getRuloOzellikBoyutsalEtKalinligi();
		Double boy = pipe.getBoy();
		Double agirlik = (disCap - etKalinligi) * etKalinligi * 0.0246615 * boy
				* 1.03;
		DecimalFormat df = new DecimalFormat("#####");
		String dx = df.format(agirlik);
		agirlik = Double.valueOf(dx);
		pipe.setAgirligi(agirlik);

		// kaynak uzunlugu hesap
		Float genislik = selectedRulos.get(0).getRuloOzellikBoyutsal()
				.getRuloOzellikBoyutsalGenislik();
		Double kaynakUzunlugu = disCap * boy * Math.PI / genislik * 1000;
		dx = df.format(kaynakUzunlugu);
		kaynakUzunlugu = Double.valueOf(dx);
		pipe.setKaynakBoy(kaynakUzunlugu);
	}

	public void cutLastPipe() {

		PipeManager pipeMan = new PipeManager();
		RuloManager ruloMan = new RuloManager();

		if (pipe.getPipeId() == null) {

			FacesContextUtils.addInfoMessage("PipeNoSelectMessage");
			return;
		} else {

			List<Integer> indexList = pipeMan.getItemMaxPipeIndex(pipe
					.getSalesItem().getItemId());
			if (pipe.getPipeIndex() != indexList.get(0)
					|| indexList.get(0) == null) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"SON ÜRETİLMİŞ BORU ÜZERİNDE İŞLEM YAPILIR. LÜTFEN SON ÜRETİLMİŞ BORUYU SEÇİNİZ!",
								null));
				return;
			}

			pipe.setPipeId(null);

			EdwPipeLinkManager edwPipeLinkMan = new EdwPipeLinkManager();
			pipe.setPipeId(pipeMan.getMaxPipeId().get(0).intValue() + 1);
			// add the links to pipe (EdwLink & CoilLink)
			for (Rulo c : selectedRulos) {
				PipeMachineRuloLink pmrl = new PipeMachineRuloLink();
				MachineRuloLink cLink = ruloMan.findCurrentLinkInMachine(
						selectedMachine, c);
				pmrl.setMachineRuloLink(cLink);
				pmrl.setPipe(pipe);
				pipe.getPmrLinks().add(pmrl);

				System.out.println("how many coils : "
						+ pipe.getPmrLinks().size());
			}
			for (ElectrodeDustWire dust : selectedDusts) {

				EdwPipeLink dustLink = new EdwPipeLink();
				dustLink.setEdw(dust);
				dustLink.setPipe(pipe);
				pipe.getEdwPipeLinks().add(dustLink);
				System.out.println("how many DUSTS : "
						+ pipe.getEdwPipeLinks().size());
			}

			// for (ElectrodeDustWire wire : selectedWires) {
			for (MachineEdwLink wire : allWireMachineLink) {

				EdwPipeLink wireLink = new EdwPipeLink();
				wireLink.setEdw(wire.getEdw());
				wireLink.setPipe(pipe);
				pipe.getEdwPipeLinks().add(wireLink);
				System.out.println("how many WIRES: "
						+ pipe.getEdwPipeLinks().size());
			}

			pipe.setStatus(PipeStatus.FREE);

			if (indexList.size() == 0 || indexList.get(0) == null) {
				pipe.setPipeIndex(1);
			} else {
				pipe.setPipeIndex(indexList.get(0) + 1);
			}

			pipe.setEklemeZamani(UtilInsCore.getTarihZaman());
			pipe.setEkleyenKullanici(userBean.getUser().getId());

			// add the pipe to the Pipe table
			// pipeMan.enterNew(pipe);
			pipeMan.insertPipe(pipe);
			// add the pipe to the list of order's PIPES
			producedPipes.add(pipe);
			// add used edw and pipe to EdwPipeLink table
			for (int j = 0; j < pipe.getEdwPipeLinks().size(); j++) {
				edwPipeLinkMan.insertEdwPipeLink(pipe.getEdwPipeLinks().get(j));
			}

			// insert to RuloPipeLink table
			pipeMan.insertRuloPipeLink(pipe, selectedRulos.get(0));
			// submitCompletedTest();
			FacesContextUtils.addInfoMessage("PipeSubmitMessage");
			producedPipes();

			// machine_pipe_link
			MachinePipeLinkManager mplMan = new MachinePipeLinkManager();
			machinePipeLink.setMachine(selectedMachine);
			machinePipeLink.setPipe(pipe);
			machinePipeLink.setDate(UtilInsCore.getTarihZaman());
			mplMan.enterNew(machinePipeLink);

			// for unselect list
			pipe = new Pipe();
			machinePipeLink = new MachinePipeLink();
		}
	}

	// **********************************************************************//
	// //
	// ************************* PROTECTED METHOD ***************************//
	// **********************************************************************//
	// //
	protected boolean canProducePipe() {

		int minDustNr = config.getConfig().getMachines().getManufacturing()
				.getSpiral().getDustNumber();
		int minWireNr = config.getConfig().getMachines().getManufacturing()
				.getSpiral().getWireNumber();
		int minRuloNr = config.getConfig().getMachines().getManufacturing()
				.getSpiral().getRuloNumber();

		if (selectedRulos.size() < minRuloNr
				|| selectedDusts.size() < minDustNr
				|| selectedWires.size() < minWireNr) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							"BORU ÜRETİMİ İÇİN GEREKLİ MİNİMUM MALZEMELER EKSİK, LÜTFEN KAYNAK TOZUNU, KAYNAK TELİNİ ve RULOYU KONTROL EDİNİZ!"));
			return false;
		}
		return true;
	}

	protected void submitCompletedTest() {

		// CompletedProductionTestManager productionTestManager = new
		// CompletedProductionTestManager();
		// CompletedProductionTest productionTest = new
		// CompletedProductionTest();
		// CompletedCoatingTest coatingTest = new CompletedCoatingTest();
		// productionTest.setPipeId(pipe.getPipeId());
		// coatingTest.setPipeId(pipe.getPipeId());
		// productionTestManager.enterNew(productionTest);
	}

	// // A.K. - makinedeki ruloya orderId setleme
	// public void loadOrders() {
	// OrderManager orMan = new OrderManager();
	// allocated.setAllocatedOrders((List<Order>) orMan.loadOrders());
	// }
	// public void loadMachineRulo() {
	// MachineManager mrl = new MachineManager();
	// RuloManager ruloMan = new RuloManager();
	// //
	// List<MachineRuloLink> machineRuloList = mrl
	// .loadMachineRulo(selectedMachine.getMachineId());
	// List<Rulo> selectedRulo = ruloMan.getByRuloId(machineRuloList.get(0)
	// .getRulo().getRuloId());
	// selectedRulo.get(0).setOrderId(
	// allocated.getSelectedOrders().getOrderId());
	// ruloMan.updateEntity(selectedRulo.get(0));
	// }

	// A.K. - makinedeki ruloya orderId setleme
	public void loadOrders() {

		// SalesItemManager salesMan = new SalesItemManager();
		// allocated.setAllocatedSalesItems(((List<SalesItem>) salesMan
		// .loadSalesItems()));

		lazySalesItemList = new LazySalesItemDataModel();
		allocated.setAllocatedSalesItemsLazy(lazySalesItemList);
	}

	public void loadMachineRulo() {
		MachineManager mrl = new MachineManager();
		RuloManager ruloMan = new RuloManager();

		List<MachineRuloLink> machineRuloList = mrl
				.loadMachineRulo(selectedMachine.getMachineId());
		List<Rulo> selectedRulo = ruloMan.getByRuloId(machineRuloList.get(0)
				.getRulo().getRuloId());
		selectedRulo.get(0).setSalesItemId(
				allocated.getSelectedSalesItems().getItemId());
		ruloMan.updateEntity(selectedRulo.get(0));
		FacesContextUtils.addInfoMessage("SiparisBasariylaMakinada");
	}

	public void makineyeSiparisEkle() {

		try {
			salesItem = allocated.getSelectedSalesItems();
			FacesContextUtils.addInfoMessage("SiparisBasariylaMakinada");
			setSelectedRuloNos(selectedMachine.getMachineId(),
					salesItem.getItemId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FacesContextUtils.addErrorMessage("SiparisBasariylaMakinada");
		}
	}

	public void barcodeBas() throws IOException, DocumentException {

		theFile = new File(path + "boru_" + pipe.getSalesItem().getItemNo()
				+ "_" + pipe.getPipeIndex() + ".pdf");

		// step 1
		// A4
		Document document = new Document(new Rectangle(210, 297));

		// step 2
		PdfWriter writer = PdfWriter.getInstance(document,
				new FileOutputStream(theFile));

		// step 3
		document.open();

		// step 4
		PdfContentByte cb = writer.getDirectContent();

		String orderNo = pipe.getSalesItem().getProposal().getSalesOrder()
				.getOrderNo();

		Barcode128 code128 = new Barcode128();
		code128.setCode(orderNo.substring(2) + "/"
				+ pipe.getSalesItem().getItemNo() + "-" + pipe.getPipeIndex());
		code128.setChecksumText(true);
		code128.setGenerateChecksum(true);
		code128.setStartStopText(true);

		Image image1 = code128.createImageWithBarcode(cb, null, null);
		image1.setAbsolutePosition(10, 233);
		image1.scaleAbsolute(85, 55);
		document.add(image1);
		Image image2 = code128.createImageWithBarcode(cb, null, null);
		image2.setAbsolutePosition(115, 233);
		image2.scaleAbsolute(85, 55);
		document.add(image2);
		Image image3 = code128.createImageWithBarcode(cb, null, null);
		image3.setAbsolutePosition(10, 159);
		image3.scaleAbsolute(85, 55);
		document.add(image3);
		Image image4 = code128.createImageWithBarcode(cb, null, null);
		image4.setAbsolutePosition(115, 159);
		image4.scaleAbsolute(85, 55);
		document.add(image4);

		document.close();

		// pipe a barkodNo yazılması
		try {
			String barkodNo = orderNo.substring(2) + "/"
					+ pipe.getSalesItem().getItemNo() + "-"
					+ pipe.getPipeIndex();
			PipeManager pipeMan = new PipeManager();
			pipe.setPipeBarkodNo(barkodNo);
			pipeMan.updateEntity(pipe);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(orderNo.substring(2) + "/"
							+ pipe.getSalesItem().getItemNo() + "-"
							+ pipe.getPipeIndex()
							+ " NUMARALI BORU BARKODU ÜRETİLDİ!"));
		} catch (Exception e) {

			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"BARKOD ÜRETİLEMEDİ, LÜTFEN TEKRAR DENEYİNİZ!", null));

		}

		downloadedFileName = "boru_" + pipe.getSalesItem().getItemNo() + "_"
				+ pipe.getPipeIndex() + ".pdf";

	}

	private String getRealFilePath(String filePath) {

		String realFilePath = FacesContext.getCurrentInstance()
				.getExternalContext().getRealPath(filePath);

		return realFilePath;
	}

	public void tanapBarkoduBas() throws IOException, DocumentException {

		Locale locale = Locale.ENGLISH;

		RuloPipeLinkManager rplm = new RuloPipeLinkManager();
		List<RuloPipeLink> rpl = null;
		rpl = rplm.findByPipeId(pipe.getPipeId());

		if (rpl.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							"BARKOD ÜRETME HATASI, LÜTFEN BORU SEÇİNİZ YA DA BİLGİ İŞLEME BİLDİRİNİZ!"));
			return;
		}

		ruloEk = rpl.get(0).getRulo().getSalesCustomer().getShortName()
				.toLowerCase().toUpperCase(locale).substring(0, 2);

		theFile = new File(path + "boru_uec_" + pipe.getPipeIndex() + ".pdf");

		// step 1
		// A4
		Document document = new Document(new Rectangle(210, 297));

		// step 2
		PdfWriter writer = PdfWriter.getInstance(document,
				new FileOutputStream(theFile));

		// step 3
		document.open();

		// step 4
		PdfContentByte cb = writer.getDirectContent();

		Barcode128 normal = new Barcode128();
		normal.setCode("UECEA" + ruloEk + pipe.getPipeIndex());
		normal.setChecksumText(true);
		normal.setGenerateChecksum(true);
		normal.setStartStopText(true);

		String realFilePath = getRealFilePath("/images/emek_logo.jpg");

		java.awt.Image awtImage = Toolkit.getDefaultToolkit().createImage(
				realFilePath);
		Image img = com.itextpdf.text.Image.getInstance(awtImage, null);

		Image image1 = normal.createImageWithBarcode(cb, null, null);
		img.setAbsolutePosition(30, 275);
		img.scaleAbsolute(50, 15);
		image1.setAbsolutePosition(10, 233);
		image1.scaleAbsolute(85, 40);
		document.add(image1);
		document.add(img);

		Image image2 = normal.createImageWithBarcode(cb, null, null);
		img.setAbsolutePosition(135, 275);
		img.scaleAbsolute(50, 15);
		image2.setAbsolutePosition(115, 233);
		image2.scaleAbsolute(85, 40);
		document.add(image2);
		document.add(img);

		Image image3 = normal.createImageWithBarcode(cb, null, null);
		img.setAbsolutePosition(30, 201);
		img.scaleAbsolute(50, 15);
		image3.setAbsolutePosition(10, 159);
		image3.scaleAbsolute(85, 40);
		document.add(image3);
		document.add(img);

		Image image4 = normal.createImageWithBarcode(cb, null, null);
		img.setAbsolutePosition(135, 201);
		img.scaleAbsolute(50, 15);
		image4.setAbsolutePosition(115, 159);
		image4.scaleAbsolute(85, 40);
		document.add(image4);
		document.add(img);

		Image image5 = normal.createImageWithBarcode(cb, null, null);
		img.setAbsolutePosition(30, 126);
		img.scaleAbsolute(50, 15);
		image5.setAbsolutePosition(10, 84);
		image5.scaleAbsolute(85, 40);
		document.add(image5);
		document.add(img);

		Image image6 = normal.createImageWithBarcode(cb, null, null);
		img.setAbsolutePosition(135, 126);
		img.scaleAbsolute(50, 15);
		image6.setAbsolutePosition(115, 84);
		image6.scaleAbsolute(85, 40);
		document.add(image6);
		document.add(img);

		Image image7 = normal.createImageWithBarcode(cb, null, null);
		img.setAbsolutePosition(30, 51);
		img.scaleAbsolute(50, 15);
		image7.setAbsolutePosition(10, 9);
		image7.scaleAbsolute(85, 40);
		document.add(image7);
		document.add(img);

		Image image8 = normal.createImageWithBarcode(cb, null, null);
		img.setAbsolutePosition(135, 51);
		img.scaleAbsolute(50, 15);
		image8.setAbsolutePosition(115, 9);
		image8.scaleAbsolute(85, 40);
		document.add(image8);
		document.add(img);

		document.close();

		// pipe a barkodNo yazılması
		try {
			String barkodNo = "UECEA" + ruloEk + pipe.getPipeIndex();
			PipeManager pipeMan = new PipeManager();
			pipe.setPipeBarkodNo(barkodNo);
			pipeMan.updateEntity(pipe);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(barkodNo
					+ " NUMARALI BORU BARKODU ÜRETİLDİ!"));
		} catch (Exception e) {

			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"BARKOD ÜRETİLEMEDİ, LÜTFEN TEKRAR DENEYİNİZ!", null));

		}

		downloadedFileName = "boru_uec_" + pipe.getPipeIndex() + ".pdf";

	}

	// deneme 08.11.2014 yeni barkod basımı

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void newBarcode() {

		if (pipe == null || pipe.getPipeId() == null || pipe.getPipeId() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		ReportUtil reportUtil = new ReportUtil();

		Barcode128 code128 = new Barcode128();
		code128.setCode(pipe.getPipeBarkodNo());
		code128.setChecksumText(true);
		code128.setGenerateChecksum(true);
		code128.setStartStopText(true);

		java.awt.Image image = code128.createAwtImage(Color.BLACK, Color.WHITE);

		try {
			HSSFWorkbook hssfWorkbook = reportUtil
					.getWorkbookFromFilePath("/reportformats/barcode/pipeBarcode.xls");

			reportUtil.insertImageToWorkbook(hssfWorkbook, image, "testImage",
					image.getWidth(null) * 2, image.getHeight(null) * 4);

			reportUtil.writeWorkbookToFile(hssfWorkbook,
					"/reportformats/barcode/pipeBarcode-" + pipe.getPipeIndex()
							+ ".xls");
		} catch (IOException e1) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"BARKOD İMAJI OLUŞTURULAMADI, HATA!", null));
			e1.printStackTrace();
		}

		try {

			Map dataMap = new HashMap();
			dataMap.put("barcodeInfo", pipe);

			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/barcode/pipeBarcode-" + pipe.getPipeIndex()
							+ ".xls", "");

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"BARKOD BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"BARKOD EXCELİ OLUŞTURULAMADI, HATA!", null));
			e.printStackTrace();
		}

	}

	public void barcodeYazdir() {

		try {
			FileOperations.getInstance().streamFile(
					FacesContext
							.getCurrentInstance()
							.getExternalContext()
							.getRealPath(
									"/reportformats/barcode/pipeBarcode-"
											+ pipe.getPipeIndex() + ".xls"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// deneme 08.11.2014

	@SuppressWarnings("resource")
	public StreamedContent getDownloadedFile() {

		try {
			if (downloadedFileName != null) {
				RandomAccessFile accessFile = new RandomAccessFile(path
						+ downloadedFileName, "r");
				byte[] downloadByte = new byte[(int) accessFile.length()];
				accessFile.read(downloadByte);
				InputStream stream = new ByteArrayInputStream(downloadByte);

				String suffix = FilenameUtils.getExtension(downloadedFileName);
				String contentType = "text/plain";
				if (suffix.contentEquals("jpg") || suffix.contentEquals("png")
						|| suffix.contentEquals("gif")) {
					contentType = "image/" + contentType;
				} else if (suffix.contentEquals("doc")
						|| suffix.contentEquals("docx")) {
					contentType = "application/msword";
				} else if (suffix.contentEquals("xls")
						|| suffix.contentEquals("xlsx")) {
					contentType = "application/vnd.ms-excel";
				} else if (suffix.contentEquals("pdf")) {
					contentType = "application/pdf";
				}
				downloadedFile = new DefaultStreamedContent(stream,
						"contentType", downloadedFileName);

				stream.close();
			} else {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"LÜTFEN BARKOD OLUŞTURUNUZ, BARKOD BULUNAMADI!"));
			}
		} catch (Exception e) {
			System.out.println("SpiralMachineBean.getDownloadedFile():"
					+ e.toString());
		}
		return downloadedFile;
	}

	public void onRowSelect(SelectEvent event) {

		selectedWireMachineLink = (MachineEdwLink) event.getObject();

	}

	public void onRowEdit(RowEditEvent event) {

		MachineEdwLinkManager manager = new MachineEdwLinkManager();
		manager.updateEntity(((MachineEdwLink) event.getObject()));

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				"KAYNAK PARAMETRESİ DEĞİŞTİRİLDİ!", null));
	}

	public void onRowEditPipe(RowEditEvent event) {

		PipeManager manager = new PipeManager();
		Pipe editPipe = new Pipe();
		editPipe = (Pipe) event.getObject();
		if (manager.findByItemIdIndexId(editPipe.getSalesItem().getItemId(),
				editPipe.getPipeIndex()).size() > 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "AYNI BORU NUMARASI VARDIR!",
					null));
			return;
		}
		editPipe.setPipeIndexOld(oldPipeIndex);
		editPipe.setPipeIndexGuncelleyenKullanici(userBean.getUser().getId());
		manager.updateEntity(editPipe);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				"BORU NO DEĞİŞTİRİLMİŞTİR!", null));
	}

	int oldPipeIndex = 0;

	public void setFirstPipeIndex() {

		oldPipeIndex = pipe.getPipeIndex();
	}

	/**
	 * *************************************************************************
	 * *** GETTERS AND SETTERS *********************
	 * /***************************
	 * **************************************************
	 */

	public Rulo getCurrentRulo() {
		return currentRulo;
	}

	public void setCurrentRulo(Rulo selectedRulo) {
		this.currentRulo = selectedRulo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public boolean isHasRulo() {
		return hasRulo;
	}

	public void setHasRulo(boolean hasRulo) {
		this.hasRulo = hasRulo;
	}

	public boolean isHasDust() {
		return hasDust;
	}

	public void setHasDust(boolean hasDust) {
		this.hasDust = hasDust;
	}

	public boolean isHasWire() {
		return hasWire;
	}

	public void setHasWire(boolean hasWire) {
		this.hasWire = hasWire;
	}

	public Machine getSelectedMachine() {
		return selectedMachine;
	}

	public void setSelectedMachine(Machine selectedMachine) {
		this.selectedMachine = selectedMachine;
	}

	public MachineEdwLink getDustMachineLink() {
		return dustMachineLink;
	}

	public void setDustMachineLink(MachineEdwLink dustMachineLink) {
		this.dustMachineLink = dustMachineLink;
	}

	public MachineEdwLink getWireMachineLink() {
		return wireMachineLink;
	}

	public void setWireMachineLink(MachineEdwLink wireMachineLink) {
		this.wireMachineLink = wireMachineLink;
	}

	public ElectrodeDustWire getCurrentDust() {
		return currentDust;
	}

	public void setCurrentDust(ElectrodeDustWire currentDust) {
		this.currentDust = currentDust;
	}

	public ElectrodeDustWire getCurrentWire() {
		return currentWire;
	}

	public void setCurrentWire(ElectrodeDustWire currentdWire) {
		this.currentWire = currentdWire;
	}

	public Double getCuttedRuloWidth() {
		return cuttedRuloWidth;
	}

	public void setCuttedRuloWidth(Double cuttedRuloWidth) {
		this.cuttedRuloWidth = cuttedRuloWidth;
	}

	public void setCuttedRuloWidth(double cuttedRuloWidth) {
		this.cuttedRuloWidth = cuttedRuloWidth;
	}

	public Pipe getPipe() {
		return pipe;
	}

	public void setPipe(Pipe pipe) {
		this.pipe = pipe;
	}

	public List<Rulo> getDoubleRuloList() {
		return doubleRuloList;
	}

	public void setDoubleRuloList(List<Rulo> doubleRuloList) {
		this.doubleRuloList = doubleRuloList;
	}

	public Rulo getDoubleRulo() {
		return doubleRulo;
	}

	public void setDoubleRulo(Rulo doubleRulo) {
		this.doubleRulo = doubleRulo;
	}

	public List<Rulo> getSelectedRulos() {
		return selectedRulos;
	}

	public void setSelectedRulos(List<Rulo> selectedRulos) {
		this.selectedRulos = selectedRulos;
	}

	public List<ElectrodeDustWire> getSelectedDusts() {
		return selectedDusts;
	}

	public void setSelectedDusts(List<ElectrodeDustWire> selectedDusts) {
		this.selectedDusts = selectedDusts;
	}

	public List<ElectrodeDustWire> getSelectedWires() {
		return selectedWires;
	}

	public void setSelectedWires(List<ElectrodeDustWire> selectedWires) {
		this.selectedWires = selectedWires;
	}

	public AllocatedMaterials getAllocated() {
		return allocated;
	}

	public void setAllocated(AllocatedMaterials allocated) {
		this.allocated = allocated;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public MachineRuloLink getRuloMachineLink() {
		return ruloMachineLink;
	}

	public void setRuloMachineLink(MachineRuloLink ruloMachineLink) {
		this.ruloMachineLink = ruloMachineLink;
	}

	public boolean isCanProducePipe() {
		return canProducePipe;
	}

	public void setCanProducePipe(boolean canProducePipe) {
		this.canProducePipe = canProducePipe;
	}

	public List<Pipe> getProducedPipes() {
		return producedPipes;
	}

	public void setProducedPipes(List<Pipe> producedPipes) {
		this.producedPipes = producedPipes;
	}

	public SalesItem getSalesItem() {
		return salesItem;
	}

	public void setSalesItem(SalesItem salesItem) {
		this.salesItem = salesItem;
	}

	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public File getTheFile() {
		return theFile;
	}

	public void setTheFile(File theFile) {
		this.theFile = theFile;
	}

	public String getPrivatePath() {
		return privatePath;
	}

	public void setPrivatePath(String privatePath) {
		this.privatePath = privatePath;
	}

	public String getDownloadedFileName() {
		return downloadedFileName;
	}

	public void setDownloadedFileName(String downloadedFileName) {
		this.downloadedFileName = downloadedFileName;
	}

	public void setDownloadedFile(StreamedContent downloadedFile) {
		this.downloadedFile = downloadedFile;
	}

	/**
	 * @return the siralamaObjesi
	 */
	public MachineSalesPipeNo getSiralamaObjesi() {
		return siralamaObjesi;
	}

	/**
	 * @param siralamaObjesi
	 *            the siralamaObjesi to set
	 */
	public void setSiralamaObjesi(MachineSalesPipeNo siralamaObjesi) {
		this.siralamaObjesi = siralamaObjesi;
	}

	/**
	 * @return the selectedWiresIcAc
	 */
	public List<ElectrodeDustWire> getSelectedWiresIcAc() {
		return selectedWiresIcAc;
	}

	/**
	 * @param selectedWiresIcAc
	 *            the selectedWiresIcAc to set
	 */
	public void setSelectedWiresIcAc(List<ElectrodeDustWire> selectedWiresIcAc) {
		this.selectedWiresIcAc = selectedWiresIcAc;
	}

	/**
	 * @return the selectedWiresDisAc
	 */
	public List<ElectrodeDustWire> getSelectedWiresDisAc() {
		return selectedWiresDisAc;
	}

	/**
	 * @param selectedWiresDisAc
	 *            the selectedWiresDisAc to set
	 */
	public void setSelectedWiresDisAc(List<ElectrodeDustWire> selectedWiresDisAc) {
		this.selectedWiresDisAc = selectedWiresDisAc;
	}

	/**
	 * @return the selectedWiresIcDc
	 */
	public List<ElectrodeDustWire> getSelectedWiresIcDc() {
		return selectedWiresIcDc;
	}

	/**
	 * @param selectedWiresIcDc
	 *            the selectedWiresIcDc to set
	 */
	public void setSelectedWiresIcDc(List<ElectrodeDustWire> selectedWiresIcDc) {
		this.selectedWiresIcDc = selectedWiresIcDc;
	}

	/**
	 * @return the selectedWiresDisDc
	 */
	public List<ElectrodeDustWire> getSelectedWiresDisDc() {
		return selectedWiresDisDc;
	}

	/**
	 * @param selectedWiresDisDc
	 *            the selectedWiresDisDc to set
	 */
	public void setSelectedWiresDisDc(List<ElectrodeDustWire> selectedWiresDisDc) {
		this.selectedWiresDisDc = selectedWiresDisDc;
	}

	/**
	 * @return the selectedDustsIc
	 */
	public List<ElectrodeDustWire> getSelectedDustsIc() {
		return selectedDustsIc;
	}

	/**
	 * @param selectedDustsIc
	 *            the selectedDustsIc to set
	 */
	public void setSelectedDustsIc(List<ElectrodeDustWire> selectedDustsIc) {
		this.selectedDustsIc = selectedDustsIc;
	}

	/**
	 * @return the selectedDustsDis
	 */
	public List<ElectrodeDustWire> getSelectedDustsDis() {
		return selectedDustsDis;
	}

	/**
	 * @param selectedDustsDis
	 *            the selectedDustsDis to set
	 */
	public void setSelectedDustsDis(List<ElectrodeDustWire> selectedDustsDis) {
		this.selectedDustsDis = selectedDustsDis;
	}

	/**
	 * @return the allWireMachineLink
	 */
	public List<MachineEdwLink> getAllWireMachineLink() {
		return allWireMachineLink;
	}

	/**
	 * @param allWireMachineLink
	 *            the allWireMachineLink to set
	 */
	public void setAllWireMachineLink(List<MachineEdwLink> allWireMachineLink) {
		this.allWireMachineLink = allWireMachineLink;
	}

	/**
	 * @return the selectedWireMachineLink
	 */
	public MachineEdwLink getSelectedWireMachineLink() {
		return selectedWireMachineLink;
	}

	/**
	 * @param selectedWireMachineLink
	 *            the selectedWireMachineLink to set
	 */
	public void setSelectedWireMachineLink(
			MachineEdwLink selectedWireMachineLink) {
		this.selectedWireMachineLink = selectedWireMachineLink;
	}
}
