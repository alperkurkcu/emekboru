/**
 * 
 */
package com.emekboru.beans.isemirleri;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.isemri.IsEmriMarkalama;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.isemirleri.IsEmriMarkalamaManager;
import com.emekboru.jpaman.sales.order.SalesItemManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isEmriMarkalamaBean")
@ViewScoped
public class IsEmriMarkalamaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private IsEmriMarkalama isEmriMarkalamaForm = new IsEmriMarkalama();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private File theFile = null;
	OutputStream output = null;
	InputStream input = null;

	private String path;
	private String privatePath;
	// private String toBeDeleted;

	private String downloadedFileName;
	private StreamedContent downloadedFile;

	private Integer prmItemId = null;
	private SalesItem salesItem = new SalesItem();

	@EJB
	private ConfigBean config;

	public IsEmriMarkalamaBean() {

		privatePath = File.separatorChar + "markalama" + File.separatorChar;

		downloadedFileName = null;
	}

	@PostConstruct
	public void load() {
		System.out.println("isEmriMarkalamaBean.load()");

		try {
			this.setPath(config.getConfig().getFolder().getAbsolutePath()
					+ privatePath);

			System.out.println("Path for Upload File (isEmriMarkalamaBean):			"
					+ path);
		} catch (Exception ex) {
			System.out.println("Path for Upload File (isEmriMarkalamaBean):			"
					+ FacesContext.getCurrentInstance().getExternalContext()
							.getRealPath(privatePath));

			System.out.print(path);
			System.out.println("CAUSE OF " + ex.toString());
		}

		theFile = new File(path);
		if (!theFile.isDirectory()) {
			theFile.mkdirs();
		}
	}

	public void addOrUpdateIsEmriMarkalama(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmItemId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		SalesItem salesItem = new SalesItemManager().loadObject(
				SalesItem.class, prmItemId);

		IsEmriMarkalamaManager isEmriManager = new IsEmriMarkalamaManager();

		if (isEmriMarkalamaForm.getId() == null) {

			isEmriMarkalamaForm.setSalesItem(salesItem);
			isEmriMarkalamaForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			isEmriMarkalamaForm.setEkleyenEmployee(userBean.getUser());

			isEmriManager.enterNew(isEmriMarkalamaForm);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"MARKALAMA İŞ EMRİ BAŞARIYLA EKLENMİŞTİR!"));
		} else {

			isEmriMarkalamaForm.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			isEmriMarkalamaForm.setGuncelleyenEmployee(userBean.getUser());
			isEmriManager.updateEntity(isEmriMarkalamaForm);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_WARN,
					"MARKALAMA İŞ EMRİ BAŞARIYLA GÜNCELLENMİŞTİR!", null));
		}
	}

	public void deleteIsEmriMarkalama() {

		IsEmriMarkalamaManager isEmriManager = new IsEmriMarkalamaManager();

		if (isEmriMarkalamaForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		isEmriManager.delete(isEmriMarkalamaForm);
		isEmriMarkalamaForm = new IsEmriMarkalama();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
				"MARKALAMA İŞ EMRİ BAŞARIYLA SİLİNMİŞTİR!", null));
	}

	@SuppressWarnings("static-access")
	public void loadIsEmri(Integer itemId) {

		prmItemId = itemId;
		salesItem = new SalesItemManager().loadObject(SalesItem.class,
				prmItemId);

		IsEmriMarkalamaManager isMan = new IsEmriMarkalamaManager();

		if (isMan.getAllIsEmriMarkalama(itemId).size() > 0) {
			isEmriMarkalamaForm = isMan.getAllIsEmriMarkalama(itemId).get(0);
		} else {
			isEmriMarkalamaForm = new IsEmriMarkalama();
		}
	}

	public String checkFileName(String fileName) {

		fileName = fileName.replace(" ", "_");
		fileName = fileName.replace("ç", "c");
		fileName = fileName.replace("Ç", "C");
		fileName = fileName.replace("ğ", "g");
		fileName = fileName.replace("Ğ", "G");
		fileName = fileName.replace("İ", "i");
		fileName = fileName.replace("ı", "i");
		fileName = fileName.replace("ö", "o");
		fileName = fileName.replace("Ö", "O");
		fileName = fileName.replace("ş", "s");
		fileName = fileName.replace("Ş", "S");
		fileName = fileName.replace("ü", "u");
		fileName = fileName.replace("Ü", "U");

		return fileName;
	}

	public void upload(FileUploadEvent event) throws AbortProcessingException,
			IOException {
		System.out.println("isEmriMarkalamaBean.upload()");

		if (isEmriMarkalamaForm.getDocuments() != null) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL,
					"ÖNCEDEN EKLENMİŞ DOSYA VAR, ÖNCE SİLİNİZ!", null));
			return;
		}

		try {
			String name = this.checkFileName(event.getFile().getFileName());

			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + "markalama_"
					+ salesItem.getProposal().getCustomer().getShortName()
					+ "_" + salesItem.getItemNo() + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				isEmriMarkalamaForm.setDocuments("markalama_"
						+ salesItem.getProposal().getCustomer().getShortName()
						+ "_" + salesItem.getItemNo() + "." + suffix);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				IsEmriMarkalamaManager manager = new IsEmriMarkalamaManager();
				manager.updateEntity(isEmriMarkalamaForm);

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				FacesContextUtils.addWarnMessage("salesFileUploadedMessage");
			} else {
				System.out.println("File already exists.");

				isEmriMarkalamaForm.setDocuments("");
				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("isEmriMarkalamaBean: " + e.toString());
		}
	}

	// download için
	@SuppressWarnings("resource")
	public StreamedContent getDownloadedFile() {

		downloadedFileName = path + isEmriMarkalamaForm.getDocuments();

		try {
			if (downloadedFileName != null) {
				RandomAccessFile accessFile = new RandomAccessFile(
						downloadedFileName, "r");
				byte[] downloadByte = new byte[(int) accessFile.length()];
				accessFile.read(downloadByte);
				InputStream stream = new ByteArrayInputStream(downloadByte);

				String suffix = FilenameUtils.getExtension(downloadedFileName);
				String contentType = "text/plain";
				if (suffix.contentEquals("jpg") || suffix.contentEquals("png")
						|| suffix.contentEquals("gif")) {
					contentType = "image/" + contentType;
				} else if (suffix.contentEquals("doc")
						|| suffix.contentEquals("docx")) {
					contentType = "application/msword";
				} else if (suffix.contentEquals("xls")
						|| suffix.contentEquals("xlsx")) {
					contentType = "application/vnd.ms-excel";
				} else if (suffix.contentEquals("pdf")) {
					contentType = "application/pdf";
				}
				downloadedFile = new DefaultStreamedContent(stream,
						"contentType", downloadedFileName);

				stream.close();
			} else {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"LÜTFEN MARKALAMA EKLEYİNİZ, MARKALAMA BULUNAMADI!"));
			}
		} catch (Exception e) {
			System.out.println("testRawMaterialReportBean.getDownloadedFile():"
					+ e.toString());
		}
		return downloadedFile;
	}

	// setters getters

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the isEmriMarkalamaForm
	 */
	public IsEmriMarkalama getIsEmriMarkalamaForm() {
		return isEmriMarkalamaForm;
	}

	/**
	 * @param isEmriMarkalamaForm
	 *            the isEmriMarkalamaForm to set
	 */
	public void setIsEmriMarkalamaForm(IsEmriMarkalama isEmriMarkalamaForm) {
		this.isEmriMarkalamaForm = isEmriMarkalamaForm;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path
	 *            the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	public File getTheFile() {
		return theFile;
	}

	public void setTheFile(File theFile) {
		this.theFile = theFile;
	}

	public String getPrivatePath() {
		return privatePath;
	}

	public void setPrivatePath(String privatePath) {
		this.privatePath = privatePath;
	}

	public String getDownloadedFileName() {
		return downloadedFileName;
	}

	public void setDownloadedFileName(String downloadedFileName) {
		this.downloadedFileName = downloadedFileName;
	}

	public void setDownloadedFile(StreamedContent downloadedFile) {
		this.downloadedFile = downloadedFile;
	}

	/**
	 * @return the config
	 */
	public ConfigBean getConfig() {
		return config;
	}

	/**
	 * @param config
	 *            the config to set
	 */
	public void setConfig(ConfigBean config) {
		this.config = config;
	}

}
