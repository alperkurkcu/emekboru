/**
 * 
 */
package com.emekboru.beans.test.tahribatsiztestsonuc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.converters.EmployeeConverter;
import com.emekboru.jpa.Employee;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizFloroskopikSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizManuelUtSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizTamir;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizTamirManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */

@ManagedBean(name = "testTahribatsizTamirBean")
@ViewScoped
public class TestTahribatsizTamirBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<TestTahribatsizTamir> allTestTahribatsizTamirList = new ArrayList<TestTahribatsizTamir>();
	private TestTahribatsizTamir testTahribatsizTamirForm = new TestTahribatsizTamir();

	private TestTahribatsizTamir newTamir = new TestTahribatsizTamir();
	private List<TestTahribatsizTamir> tamirList;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	int index;

	private boolean updateButtonRender;

	public static List<Employee> employees;

	private Employee selectedEmployee;

	private Pipe selectedPipe = new Pipe();

	private String barkodNo = null;

	public TestTahribatsizTamirBean() {

		updateButtonRender = false;
		employees = EmployeeConverter.employeeDB;
	}

	// tamire gönderme kısmı
	public void addFromManuel(
			List<TestTahribatsizManuelUtSonuc> allTestTahribatsizManuelUtSonucList,
			UserCredentialsBean userBean) {
		System.out.println("TestTahribatsizTamirBean.add()");

		newTamir.setTestTahribatsizManuelUtSonuc(new TestTahribatsizManuelUtSonuc());
		TestTahribatsizTamirManager tamirManager = new TestTahribatsizTamirManager();

		index = 0;

		try {

			while (allTestTahribatsizManuelUtSonucList.size() > index) {
				if (allTestTahribatsizManuelUtSonucList.get(index)
						.getTestTahribatsizTamir() == null
						&& allTestTahribatsizManuelUtSonucList.get(index)
								.getDegerlendirme() != null
						&& allTestTahribatsizManuelUtSonucList.get(index)
								.getDegerlendirme() == 2) {// 2=tamir
					// newTamir.setDurum(false);
					newTamir.setEklemeZamani(UtilInsCore.getTarihZaman());
					newTamir.setUser(userBean.getUser());
					newTamir.setTestTahribatsizManuelUtSonuc(allTestTahribatsizManuelUtSonucList
							.get(index));
					newTamir.setPipe(allTestTahribatsizManuelUtSonucList.get(
							index).getPipe());
					tamirManager.enterNew(newTamir);
					newTamir = new TestTahribatsizTamir();

					FacesContext context = FacesContext.getCurrentInstance();
					context.addMessage(
							null,
							new FacesMessage(
									"MANUET UT TARAFINDAN TAMİR BAŞARIYLA EKLENMİŞTİR!"));
				}
				index++;
			}

		} catch (Exception e) {
			System.out.println("TestTahribatsizTamirBean:" + e.toString());
		}
	}

	public void addFromFl(
			List<TestTahribatsizFloroskopikSonuc> allTestTahribatsizFloroskopikSonucList,
			UserCredentialsBean userBean) {
		System.out.println("TestTahribatsizTamirBean.add()");

		newTamir.setTestTahribatsizFloroskopikSonuc(new TestTahribatsizFloroskopikSonuc());
		TestTahribatsizTamirManager tamirManager = new TestTahribatsizTamirManager();

		try {

			while (allTestTahribatsizFloroskopikSonucList.size() > index) {
				if (allTestTahribatsizFloroskopikSonucList.get(index)
						.getTestTahribatsizTamir() == null
						&& allTestTahribatsizFloroskopikSonucList.get(index)
								.getResult() != null
						&& allTestTahribatsizFloroskopikSonucList.get(index)
								.getResult() == 2) {// 2=tamir
					// newTamir.setDurum(false);
					newTamir.setEklemeZamani(UtilInsCore.getTarihZaman());
					newTamir.setUser(userBean.getUser());
					newTamir.setTestTahribatsizFloroskopikSonuc(allTestTahribatsizFloroskopikSonucList
							.get(index));
					newTamir.setPipe(allTestTahribatsizFloroskopikSonucList
							.get(index).getPipe());
					tamirManager.enterNew(newTamir);
					newTamir = new TestTahribatsizTamir();
				}
				index++;
			}

			tamirManager.updateEntity(testTahribatsizTamirForm);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"FLOROSKOPİ TARAFINDAN TAMİR BAŞARIYLA EKLENMİŞTİR!"));

		} catch (Exception e) {
			System.out.println("TestTahribatsizTamirBean:" + e.toString());
		}
	}

	// public void addTamirSonuc(ActionEvent e) {
	//
	// UICommand cmd = (UICommand) e.getComponent();
	// Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
	// .get("value");
	//
	// Boolean prmKontrol = testTahribatsizTamirForm.getDurum();
	// TestTahribatsizTamirManager tamirManager = new
	// TestTahribatsizTamirManager();
	//
	// if (prmKontrol == null) {// baslama
	//
	// try {
	// testTahribatsizTamirForm.setDurum(false);
	// testTahribatsizTamirForm.setMuayeneBaslamaZamani(UtilInsCore
	// .getTarihZaman());
	// // testTahribatsizTamirForm
	// // .setMuayeneBaslamaKullanici(userBean.getUser()
	// // .getId());
	// testTahribatsizTamirForm
	// .setMuayeneBaslamaKullanici(testTahribatsizTamirForm
	// .getMuayeneBaslamaUser().getEmployeeId());
	// tamirManager.updateEntity(testTahribatsizTamirForm);
	// FacesContextUtils.addInfoMessage("SubmitMessage");
	// } catch (Exception ex) {
	// System.out.println("TestTahribatsizTamirBean:" + e.toString());
	// }
	// } else if (prmKontrol == false) {// bitirme
	//
	// try {
	// // yarım iş ise
	// if (testTahribatsizTamirForm.getYarimKaldi() == true
	// && testTahribatsizTamirForm.getYarimTamamlandi() == false) {
	//
	// testTahribatsizTamirForm.setDurum(false);
	// testTahribatsizTamirForm.setMuayeneBitisZamani(UtilInsCore
	// .getTarihZaman());
	// testTahribatsizTamirForm
	// .setMuayeneBitisKullanici(testTahribatsizTamirForm
	// .getMuayeneBitisUser().getEmployeeId());
	// tamirManager.updateEntity(testTahribatsizTamirForm);
	//
	// FacesContext context = FacesContext.getCurrentInstance();
	// context.addMessage(null, new FacesMessage(
	// "YARIM KALAN TAMİR VERİSİ GİRİLMİŞTİR!"));
	// } else if (testTahribatsizTamirForm.getYarimKaldi() == false
	// && testTahribatsizTamirForm.getYarimTamamlandi() == true) {
	//
	// testTahribatsizTamirForm.setDurum(true);
	// testTahribatsizTamirForm
	// .setMuayeneBitisZamaniKalan(UtilInsCore
	// .getTarihZaman());
	// testTahribatsizTamirForm
	// .setMuayeneBitisKullaniciKalan(testTahribatsizTamirForm
	// .getMuayeneBitisUser().getEmployeeId());
	//
	// tamirManager.updateEntity(testTahribatsizTamirForm);
	//
	// FacesContext context = FacesContext.getCurrentInstance();
	// context.addMessage(null, new FacesMessage(
	// "YARIM KALAN TAMİR BAŞARIYLA BİTİRİLMİŞTİR!"));
	// } else if (testTahribatsizTamirForm.getYarimKaldi() == false
	// && testTahribatsizTamirForm.getYarimTamamlandi() == false) {
	//
	// testTahribatsizTamirForm.setDurum(true);
	// testTahribatsizTamirForm.setMuayeneBitisZamani(UtilInsCore
	// .getTarihZaman());
	// testTahribatsizTamirForm
	// .setMuayeneBitisKullanici(testTahribatsizTamirForm
	// .getMuayeneBitisUser().getEmployeeId());
	//
	// tamirManager.updateEntity(testTahribatsizTamirForm);
	//
	// FacesContext context = FacesContext.getCurrentInstance();
	// context.addMessage(null, new FacesMessage(
	// "TAMİR BAŞARIYLA BİTİRİLMİŞTİR!"));
	// }
	//
	// } catch (Exception ex) {
	// System.out.println("TestTahribatsizTamirBean:" + e.toString());
	// }
	//
	// } else {
	//
	// try {
	//
	// testTahribatsizTamirForm.setGuncellemeZamani(UtilInsCore
	// .getTarihZaman());
	// testTahribatsizTamirForm.setGuncelleyenKullanici(userBean
	// .getUser().getId());
	// tamirManager.updateEntity(testTahribatsizTamirForm);
	//
	// FacesContext context = FacesContext.getCurrentInstance();
	// context.addMessage(null, new FacesMessage(
	// "TAMİR VERİSİ BAŞARIYLA GÜNCELLENMİŞTİR!"));
	// } catch (Exception ex) {
	// System.out.println("TestTahribatsizTamirBean:" + e.toString());
	// }
	// }
	// fillTestList(prmPipeId);
	// }

	public void addTamirSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		Boolean prmKontrol = testTahribatsizTamirForm.getDurum();
		TestTahribatsizTamirManager tamirManager = new TestTahribatsizTamirManager();

		if (prmKontrol == null) {// baslama

			try {

				testTahribatsizTamirForm.setDurum(false);
				testTahribatsizTamirForm.setMuayeneBaslamaZamani(UtilInsCore
						.getTarihZaman());
				// testTahribatsizTamirForm
				// .setMuayeneBaslamaKullanici(userBean.getUser()
				// .getId());
				testTahribatsizTamirForm
						.setMuayeneBaslamaKullanici(testTahribatsizTamirForm
								.getMuayeneBaslamaUser().getEmployeeId());
				tamirManager.updateEntity(testTahribatsizTamirForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"TAMİR BAŞARIYLA BAŞLANMIŞTIR!"));

			} catch (Exception ex) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"TAMİRE BAŞLANAMADI, HATA VAR!", null));
				System.out.println("TAMİR BAŞARIYLA BAŞLANMAMIŞTIR, HATA!"
						+ e.toString());
			}
		} else if (prmKontrol == false) {// bitirme

			try {

				testTahribatsizTamirForm.setDurum(true);
				testTahribatsizTamirForm.setMuayeneBitisZamani(UtilInsCore
						.getTarihZaman());
				testTahribatsizTamirForm
						.setMuayeneBitisKullanici(testTahribatsizTamirForm
								.getMuayeneBitisUser().getEmployeeId());

				tamirManager.updateEntity(testTahribatsizTamirForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"TAMİR BAŞARIYLA BİTİRİLMİŞTİR!"));

			} catch (Exception ex) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"TAMİR BİTİRİLEMEDİ, HATA VAR!", null));
				System.out.println("TAMİR BAŞARISIZ BİTİRİLMİŞTİR, HATA!"
						+ e.toString());
			}

		} else {

			try {

				testTahribatsizTamirForm.setGuncellemeZamani(UtilInsCore
						.getTarihZaman());
				testTahribatsizTamirForm.setGuncelleyenKullanici(userBean
						.getUser().getId());
				testTahribatsizTamirForm
						.setMuayeneBaslamaKullanici(testTahribatsizTamirForm
								.getMuayeneBaslamaUser().getEmployeeId());
				testTahribatsizTamirForm
						.setMuayeneBitisKullanici(testTahribatsizTamirForm
								.getMuayeneBitisUser().getEmployeeId());
				tamirManager.updateEntity(testTahribatsizTamirForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"TAMİR VERİSİ BAŞARIYLA GÜNCELLENMİŞTİR!"));
			} catch (Exception ex) {
				System.out
						.println("TAMİR VERİSİ BAŞARISIZ GÜNCELLENMİŞTİR, HATA!"
								+ e.toString());
			}
		}
		fillTestList(prmPipeId);
	}

	@SuppressWarnings("static-access")
	public void fillTestList(Integer prmPipeId) {

		TestTahribatsizTamirManager tamirManager = new TestTahribatsizTamirManager();
		allTestTahribatsizTamirList = tamirManager
				.getAllTestTahribatsizTamir(prmPipeId);
		tamirManager.refreshCollection(allTestTahribatsizTamirList);
	}

	public void tamirListener(SelectEvent event) {
		updateButtonRender = true;
	}

	// autocompete için metodlar
	// public List<String> complete(String query) {
	//
	// List<String> results = new ArrayList<String>();
	// List<Employee> employeeList = new ArrayList<Employee>();
	//
	// EmployeeManager empMan = new EmployeeManager();
	// employeeList = empMan.findAllLike(Employee.class, "firstname", query);
	// for (int i = 0; i < employeeList.size(); i++) {
	//
	// results.add(employeeList.get(i).getEmployeeId().toString() + " - "
	// + employeeList.get(i).getFirstname() + " "
	// + employeeList.get(i).getLastname());
	// }
	//
	// return results;
	// }

	// autocompete için metodlar
	public List<Employee> completeEmployee(String query) {
		List<Employee> suggestions = new ArrayList<Employee>();

		for (Employee e : employees) {
			if (e.getLastname().startsWith(query))
				suggestions.add(e);
		}

		return suggestions;
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!", null));
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			TestTahribatsizTamirManager tamirManager = new TestTahribatsizTamirManager();
			allTestTahribatsizTamirList = tamirManager
					.getAllTestTahribatsizTamir(selectedPipe.getPipeId());
			tamirManager.refreshCollection(allTestTahribatsizTamirList);
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
		}
	}

	public void deleteTamirSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		TestTahribatsizTamirManager tamirSonucManager = new TestTahribatsizTamirManager();

		if (testTahribatsizTamirForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		tamirSonucManager.delete(testTahribatsizTamirForm);
		testTahribatsizTamirForm = new TestTahribatsizTamir();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"TAMİR BAŞARIYLA SİLİNMİŞTİR!"));

		fillTestList(prmPipeId);
	}

	public void reset() {

		testTahribatsizTamirForm = new TestTahribatsizTamir();
		updateButtonRender = false;
	}

	// setters getters
	public TestTahribatsizTamir getNewTamir() {
		return newTamir;
	}

	public void setNewTamir(TestTahribatsizTamir newTamir) {
		this.newTamir = newTamir;
	}

	public List<TestTahribatsizTamir> getTamirList() {
		return tamirList;
	}

	public void setTamirList(List<TestTahribatsizTamir> tamirList) {
		this.tamirList = tamirList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public List<TestTahribatsizTamir> getAllTestTahribatsizTamirList() {
		return allTestTahribatsizTamirList;
	}

	public void setAllTestTahribatsizTamirList(
			List<TestTahribatsizTamir> allTestTahribatsizTamirList) {
		this.allTestTahribatsizTamirList = allTestTahribatsizTamirList;
	}

	public TestTahribatsizTamir getTestTahribatsizTamirForm() {
		return testTahribatsizTamirForm;
	}

	public void setTestTahribatsizTamirForm(
			TestTahribatsizTamir testTahribatsizTamirForm) {
		this.testTahribatsizTamirForm = testTahribatsizTamirForm;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public static List<Employee> getEmployees() {
		return employees;
	}

	public static void setEmployees(List<Employee> employees) {
		TestTahribatsizTamirBean.employees = employees;
	}

	public Employee getSelectedEmployee() {
		return selectedEmployee;
	}

	public void setSelectedEmployee(Employee selectedEmployee) {
		this.selectedEmployee = selectedEmployee;
	}

	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	public String getBarkodNo() {
		return barkodNo;
	}

	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

}
