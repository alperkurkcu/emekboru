/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaOncesiYuzeyKontroluSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaOncesiYuzeyKontroluSonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testKaplamaOncesiYuzeyKontroluSonucBean")
@ViewScoped
public class TestKaplamaOncesiYuzeyKontroluSonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<TestKaplamaOncesiYuzeyKontroluSonuc> allKaplamaOncesiYuzeyKontroluSonucList = new ArrayList<TestKaplamaOncesiYuzeyKontroluSonuc>();
	private TestKaplamaOncesiYuzeyKontroluSonuc testKaplamaOncesiYuzeyKontroluSonucForm = new TestKaplamaOncesiYuzeyKontroluSonuc();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;
	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addKaplamaOncesiYuzeyKontroluSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaOncesiYuzeyKontroluSonucManager tkdsManager = new TestKaplamaOncesiYuzeyKontroluSonucManager();

		if (testKaplamaOncesiYuzeyKontroluSonucForm.getId() == null) {

			testKaplamaOncesiYuzeyKontroluSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaOncesiYuzeyKontroluSonucForm
					.setEkleyenKullanici(userBean.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaOncesiYuzeyKontroluSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaOncesiYuzeyKontroluSonucForm.setBagliTestId(bagliTest);
			testKaplamaOncesiYuzeyKontroluSonucForm.setBagliGlobalId(bagliTest);

			tkdsManager.enterNew(testKaplamaOncesiYuzeyKontroluSonucForm);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaOncesiYuzeyKontroluSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaOncesiYuzeyKontroluSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			tkdsManager.updateEntity(testKaplamaOncesiYuzeyKontroluSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");

		}
		fillTestList(prmGlobalId, prmPipeId);
	}

	public void addKaplamaOncesiYuzeyKontrolu() {
		testKaplamaOncesiYuzeyKontroluSonucForm = new TestKaplamaOncesiYuzeyKontroluSonuc();
		updateButtonRender = false;
	}

	public void kaplamaDarbeListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(int globalId, Integer pipeId2) {
		allKaplamaOncesiYuzeyKontroluSonucList = TestKaplamaOncesiYuzeyKontroluSonucManager
				.getAllTestKaplamaOncesiYuzeyKontroluSonuc(globalId, pipeId2);
		testKaplamaOncesiYuzeyKontroluSonucForm = new TestKaplamaOncesiYuzeyKontroluSonuc();
	}

	public void deleteTestKaplamaOncesiYuzeyKontroluSonuc() {

		bagliTestId = testKaplamaOncesiYuzeyKontroluSonucForm.getBagliTestId()
				.getId();

		if (testKaplamaOncesiYuzeyKontroluSonucForm == null
				|| testKaplamaOncesiYuzeyKontroluSonucForm.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaOncesiYuzeyKontroluSonucManager tkdsManager = new TestKaplamaOncesiYuzeyKontroluSonucManager();
		tkdsManager.delete(testKaplamaOncesiYuzeyKontroluSonucForm);
		testKaplamaOncesiYuzeyKontroluSonucForm = new TestKaplamaOncesiYuzeyKontroluSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setters getters

	/**
	 * @return the allKaplamaOncesiYuzeyKontroluSonucList
	 */
	public List<TestKaplamaOncesiYuzeyKontroluSonuc> getAllKaplamaOncesiYuzeyKontroluSonucList() {
		return allKaplamaOncesiYuzeyKontroluSonucList;
	}

	/**
	 * @param allKaplamaOncesiYuzeyKontroluSonucList
	 *            the allKaplamaOncesiYuzeyKontroluSonucList to set
	 */
	public void setAllKaplamaOncesiYuzeyKontroluSonucList(
			List<TestKaplamaOncesiYuzeyKontroluSonuc> allKaplamaOncesiYuzeyKontroluSonucList) {
		this.allKaplamaOncesiYuzeyKontroluSonucList = allKaplamaOncesiYuzeyKontroluSonucList;
	}

	/**
	 * @return the testKaplamaOncesiYuzeyKontroluSonucForm
	 */
	public TestKaplamaOncesiYuzeyKontroluSonuc getTestKaplamaOncesiYuzeyKontroluSonucForm() {
		return testKaplamaOncesiYuzeyKontroluSonucForm;
	}

	/**
	 * @param testKaplamaOncesiYuzeyKontroluSonucForm
	 *            the testKaplamaOncesiYuzeyKontroluSonucForm to set
	 */
	public void setTestKaplamaOncesiYuzeyKontroluSonucForm(
			TestKaplamaOncesiYuzeyKontroluSonuc testKaplamaOncesiYuzeyKontroluSonucForm) {
		this.testKaplamaOncesiYuzeyKontroluSonucForm = testKaplamaOncesiYuzeyKontroluSonucForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
