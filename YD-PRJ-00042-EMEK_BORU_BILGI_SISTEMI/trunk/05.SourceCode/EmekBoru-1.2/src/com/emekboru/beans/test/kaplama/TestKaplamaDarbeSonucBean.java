package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaDarbeSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaDarbeSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaplamaDarbeSonucBean")
@ViewScoped
public class TestKaplamaDarbeSonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<TestKaplamaDarbeSonuc> allKaplamaDarbeTestSonucList = new ArrayList<TestKaplamaDarbeSonuc>();
	private TestKaplamaDarbeSonuc testKaplamaDarbeSonucForm = new TestKaplamaDarbeSonuc();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;
	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	// methods

	public void addKaplamaDarbeTestSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaDarbeSonucManager tkdsManager = new TestKaplamaDarbeSonucManager();

		if (testKaplamaDarbeSonucForm.getId() == null) {

			testKaplamaDarbeSonucForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			testKaplamaDarbeSonucForm.setEkleyenKullanici(userBean.getUser()
					.getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaDarbeSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaDarbeSonucForm.setBagliTestId(bagliTest);
			testKaplamaDarbeSonucForm.setBagliGlobalId(bagliTest);

			tkdsManager.enterNew(testKaplamaDarbeSonucForm);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaDarbeSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaDarbeSonucForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			tkdsManager.updateEntity(testKaplamaDarbeSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");

		}
		fillTestList(prmGlobalId, prmPipeId);
	}

	public void addKaplamaDarbeTest() {
		testKaplamaDarbeSonucForm = new TestKaplamaDarbeSonuc();
		updateButtonRender = false;
	}

	public void kaplamaDarbeListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(int globalId, Integer pipeId2) {
		allKaplamaDarbeTestSonucList = TestKaplamaDarbeSonucManager
				.getAllKaplamaDarbeTestSonuc(globalId, pipeId2);
		testKaplamaDarbeSonucForm = new TestKaplamaDarbeSonuc();
	}

	public void deleteTestKaplamaDarbeSonuc() {

		bagliTestId = testKaplamaDarbeSonucForm.getBagliTestId().getId();

		if (testKaplamaDarbeSonucForm == null
				|| testKaplamaDarbeSonucForm.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaDarbeSonucManager tkdsManager = new TestKaplamaDarbeSonucManager();
		tkdsManager.delete(testKaplamaDarbeSonucForm);
		testKaplamaDarbeSonucForm = new TestKaplamaDarbeSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setters & getters

	public List<TestKaplamaDarbeSonuc> getAllKaplamaDarbeTestSonucList() {
		return allKaplamaDarbeTestSonucList;
	}

	public void setAllKaplamaDarbeTestSonucList(
			List<TestKaplamaDarbeSonuc> allKaplamaDarbeTestSonucList) {
		this.allKaplamaDarbeTestSonucList = allKaplamaDarbeTestSonucList;
	}

	public TestKaplamaDarbeSonuc getTestKaplamaDarbeSonucForm() {
		return testKaplamaDarbeSonucForm;
	}

	public void setTestKaplamaDarbeSonucForm(
			TestKaplamaDarbeSonuc testKaplamaDarbeSonucForm) {
		this.testKaplamaDarbeSonucForm = testKaplamaDarbeSonucForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
