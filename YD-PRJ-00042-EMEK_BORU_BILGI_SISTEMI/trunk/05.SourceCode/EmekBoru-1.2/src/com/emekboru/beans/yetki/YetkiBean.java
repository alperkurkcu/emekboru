package com.emekboru.beans.yetki;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.LkYetkiRol;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.YetkiMenu;
import com.emekboru.jpa.YetkiMenuKullaniciErisim;
import com.emekboru.jpaman.LkYetkiRolManager;
import com.emekboru.jpaman.UserManager;
import com.emekboru.jpaman.YetkiMenuKullaniciErisimManager;
import com.emekboru.jpaman.YetkiMenuManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.YetkiTreeNode;
import com.emekboru.utils.YetkiTreeNodeType;

@ManagedBean(name = "yetkiBean")
@SessionScoped
public class YetkiBean {

	@EJB
	private ConfigBean config;

	private String ad;
	private String aciklama;
	private String selectedUserName;
	private YetkiMenu selectedPage = new YetkiMenu();
	private Integer selectedPageId;
	private List<YetkiMenu> selectedPages = null;
	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private List<SystemUser> userList = null;
	private List<YetkiMenu> yetkiMenuList = null;
	private List<YetkiMenu> allYetkiMenuList = null;

	private List<LkYetkiRol> yetkiRolList = null;
	private List<LkYetkiRol> menuRolList = null;
	private LkYetkiRol selectedYetkiRol = null;
	private SystemUser selectedUser = null;
	private YetkiMenu selectedMenu = null;
	private Integer selectedRole = null;
	private boolean ekranRolRendered;
	private List<YetkiMenuKullaniciErisim> kullaniciErisimList = new ArrayList<YetkiMenuKullaniciErisim>();

	private YetkiTreeNode root = null;
	private YetkiTreeNode selectedNode = null;
	private Integer selectedMenuId;
	private Integer selectedRoleId;
	private List<LkYetkiRol> selectedPageRoles;
	private boolean addYetkiRendered;

	public void goToNewYetkiRolPage() {
		yetkiRolList = LkYetkiRolManager.getAllYetkiRolList();
		selectedPages = YetkiMenuManager.getAllYetkiMenuList();
		FacesContextUtils.redirect(config.getPageUrl().NEW_YETKI_ROL_PAGE);
	}

	public void goToManageYetkiRolPage() {
		yetkiRolList = LkYetkiRolManager.getAllYetkiRolList();
		FacesContextUtils.redirect(config.getPageUrl().MANAGE_YETKI_ROL_PAGE);
	}

	public void rowSelected(SelectEvent event) {
		selectedPages = new ArrayList<YetkiMenu>();
		selectedPages = YetkiMenuManager.getAllYetkiMenuList();
		selectedPage = selectedYetkiRol.getMenu();
		selectedPageId = selectedPage.getId();
	}

	public void rowSelected(NodeSelectEvent event) {
		selectedPages = new ArrayList<YetkiMenu>();
		selectedPages = YetkiMenuManager.getAllYetkiMenuList();
		if (selectedYetkiRol != null)
			selectedPage = selectedYetkiRol.getMenu();
	}

	public void goToNewYetkiMenuKullaniciErisim() {

		FacesContextUtils
				.redirect(config.getPageUrl().NEW_YETKI_MENU_KULLANICI_ERISIM);
		root = YetkiMenuKullaniciErisimManager
				.getAllYetkiMenuKullaniciErisimTree(true, false, false);
	}

	public void newUserYetkiPage(ActionEvent e) {
		if (selectedNode == null)
			FacesContextUtils.addErrorMessage("SelectUserBeforeAddingRole");
		System.out.println("Object Id: " + selectedNode.getObjectId());
		yetkiMenuList = YetkiMenuManager.getAllPages();
		addYetkiRendered = true;
	}

	public void removeUserYetkiPage(ActionEvent e) {
		if (selectedNode == null)
			FacesContextUtils.addErrorMessage("SelectUserBeforeRemovingRole");

		if (selectedNode.getRoleId() == null) {
			FacesContextUtils.addErrorMessage("SelectRoleBeforeRemoving");
		}

		if (!selectedNode.getObjectType().equalsIgnoreCase(
				YetkiTreeNodeType.PAGE)) {
			FacesContextUtils.addErrorMessage("SelectRoleBeforeRemoving");
		}

		YetkiMenuKullaniciErisimManager.removeUserRole(
				selectedNode.getUserId(), selectedNode.getPageId(),
				selectedNode.getRoleId());
		root = YetkiMenuKullaniciErisimManager
				.getAllYetkiMenuKullaniciErisimTree(true, false, true);

	}

	public void goToManageYetkiMenuKullaniciErisimPage() {
		root = YetkiMenuKullaniciErisimManager
				.getAllYetkiMenuKullaniciErisimTree(true, false, true);
		FacesContextUtils
				.redirect(config.getPageUrl().MANAGE_YETKI_MENU_KULLANICI_ERISIM);

	}

	public void cancelYetkiBeanSubmittion() {
		ad = new String();
		aciklama = new String();
	}

	public void addYetkiRol(ActionEvent event) {

		List<LkYetkiRol> list = LkYetkiRolManager.getLkYetkiRol(ad, aciklama);

		if (list.size() > 0) {
			FacesContextUtils.addErrorMessage("ExistUsernameMessage");
			ad = new String();
			aciklama = new String();
			return;
		}
		LkYetkiRol rol = createEntity();
		selectedPage = YetkiMenuManager.getYetkiMenuById(selectedPageId);
		rol.setMenu(selectedPage);
		LkYetkiRolManager.addOrUpdateLkYetkiRol(rol);
		yetkiRolList = LkYetkiRolManager.getAllYetkiRolList();
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void updateYetkiRol(ActionEvent event) {
		if (selectedYetkiRol != null) {
			// selectedYetkiRol.setMenu(selectedPage);
			LkYetkiRolManager.addOrUpdateLkYetkiRol(selectedYetkiRol);
		}
	}

	public void addToUserYetkiList(ActionEvent e) {
		YetkiMenuKullaniciErisim yetkiMenuKullaniciErisim = new YetkiMenuKullaniciErisim();
		yetkiMenuKullaniciErisim.setLkYetkiRol(LkYetkiRolManager
				.getLkYetkiRolById(selectedRoleId));
		yetkiMenuKullaniciErisim.setSystemUser(new UserManager()
				.findBySystemUserId(selectedNode.getObjectId()));
		yetkiMenuKullaniciErisim.setYetkiMenu(YetkiMenuManager
				.getYetkiMenuById(selectedMenuId));
		yetkiMenuKullaniciErisim
				.setEkleyenKullanici(userBean.getUser().getId());
		yetkiMenuKullaniciErisim.setEklemeZamani(new java.sql.Timestamp(
				new java.util.Date().getTime()));
		kullaniciErisimList.add(yetkiMenuKullaniciErisim);
	}

	private LkYetkiRol createEntity() {
		LkYetkiRol lkYetkiRol = new LkYetkiRol();
		lkYetkiRol.setAd(ad);
		lkYetkiRol.setAciklama(aciklama);
		lkYetkiRol.setEkleyenKullanici(userBean.getUser().getId());
		lkYetkiRol.setEklemeZamani(new java.sql.Timestamp(new java.util.Date()
				.getTime()));
		return lkYetkiRol;
	}

	public void menuChangeListener(AjaxBehaviorEvent e) {
		List<LkYetkiRol> result = null;
		List<YetkiMenuKullaniciErisim> userYetkiMenuRoles = null;
		if (selectedMenuId != null) {
			result = LkYetkiRolManager.getRoleByMenuId(selectedMenuId);
			userYetkiMenuRoles = YetkiMenuKullaniciErisimManager
					.getDefinedUserMenuRolesByUserIdAndMenuId(
							selectedNode.getObjectId(), selectedMenuId);

			for (int i = 0; i < userYetkiMenuRoles.size(); i++) {
				result.remove(userYetkiMenuRoles.get(i).getLkYetkiRol());
			}
		}
		selectedPageRoles = result;
	}

	public void saveUserYetkiList() {
		for (int i = 0; i < kullaniciErisimList.size(); i++) {
			YetkiMenuKullaniciErisimManager
					.addOrUpdateYetkiMenuKullaniciErisim(kullaniciErisimList
							.get(i));
		}
		FacesContextUtils.addInfoMessage("SubmitMessage");
		selectedNode = null;
		root = YetkiMenuKullaniciErisimManager
				.getAllYetkiMenuKullaniciErisimTree(true, false, false);
		selectedMenuId = null;
		selectedRoleId = null;

		// userBean.refreshModel();
		kullaniciErisimList = new ArrayList<YetkiMenuKullaniciErisim>();

		// FacesContextUtils.refreshPage();
		// FacesContextUtils.closeSession();
		// FacesContextUtils.newSession();
	}

	@SuppressWarnings("unused")
	public void addToUserAllYetkiList(ActionEvent e) {

		if (selectedNode != null) {
			List<YetkiMenu> allPages = getAllPages();
			List<YetkiMenuKullaniciErisim> userPageAccessList = YetkiMenuKullaniciErisimManager
					.getYetkiMenuKullaniciErisimByUserId(selectedNode
							.getObjectId());
			List<YetkiMenu> userYetkiMenus = getUserYetkiMenus(userPageAccessList);
			List<YetkiMenu> notExistsUserYetkiMenu = new ArrayList<YetkiMenu>();

			for (int i = 0; i < allPages.size(); i++) {
				YetkiMenu yetkiMenu = allPages.get(i);
				if (!userYetkiMenus.contains(yetkiMenu)) {
					notExistsUserYetkiMenu.add(yetkiMenu);
				}
			}
			removeUserRoles(selectedNode.getObjectId());
			List<LkYetkiRol> userRoleList = null;
			YetkiMenuKullaniciErisim userYetkiMenuErisim = null;
			SystemUser user = null;
			for (int i = 0; i < notExistsUserYetkiMenu.size(); i++) {
				userRoleList = LkYetkiRolManager
						.getRoleByMenuId(notExistsUserYetkiMenu.get(i).getId());
				for (int roleId = 0; roleId < userRoleList.size(); roleId++) {
					userYetkiMenuErisim = new YetkiMenuKullaniciErisim();
					userYetkiMenuErisim.setLkYetkiRol(userRoleList.get(roleId));
					userYetkiMenuErisim.setYetkiMenu(userRoleList.get(roleId)
							.getMenu());

					userYetkiMenuErisim
							.setSystemUser(getSystemUser(selectedNode
									.getObjectId()));
					userYetkiMenuErisim.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
					userYetkiMenuErisim.setEkleyenKullanici(userBean.getUser()
							.getId());
					kullaniciErisimList.add(userYetkiMenuErisim);
				}
			}
		}
	}

	private SystemUser getSystemUser(Integer objectId) {
		UserManager userManager = new UserManager();
		SystemUser user = userManager.findBySystemUserId(objectId);
		return user;
	}

	private void removeUserRoles(Integer userId) {
		for (int i = 0; i < kullaniciErisimList.size(); i++) {
			if (kullaniciErisimList.get(i).getSystemUser().getId() == userId) {
				kullaniciErisimList.remove(i);
			}
		}
	}

	private List<YetkiMenu> getUserYetkiMenus(
			List<YetkiMenuKullaniciErisim> userPageAccessList) {
		List<YetkiMenu> pagesList = new ArrayList<YetkiMenu>();
		for (int i = 0; i < userPageAccessList.size(); i++) {
			if (!pagesList.contains(userPageAccessList.get(i).getYetkiMenu())) {
				pagesList.add(userPageAccessList.get(i).getYetkiMenu());
			}
		}

		return pagesList;
	}

	@SuppressWarnings("unused")
	public List<YetkiMenu> getAllPages() {
		List<YetkiMenu> allPages = YetkiMenuManager.getAllPages();
		List<YetkiMenuKullaniciErisim> allErisimList = new ArrayList<YetkiMenuKullaniciErisim>();
		List<YetkiMenuKullaniciErisim> allUsersErisimList = null;
		if (selectedNode != null)
			allUsersErisimList = YetkiMenuKullaniciErisimManager
					.getYetkiMenuKullaniciErisimByUserId(selectedNode
							.getObjectId());
		YetkiMenuKullaniciErisim yetkiMenuKullaniciErisim = null;

		for (int i = 0; i < allUsersErisimList.size(); i++) {
			allPages.remove(allUsersErisimList.get(i).getYetkiMenu());
		}
		return allPages;
	}

	public void clearYetkiList() {
		kullaniciErisimList = new ArrayList<YetkiMenuKullaniciErisim>();
		selectedMenuId = null;
		selectedRoleId = null;
	}

	public void goToEmployeeDepartmentSupervisorPage() {

		FacesContextUtils
				.redirect(config.getPageUrl().YETKI_EMPLOYEE_DEPARTMENT_SUPERVISOR_PAGE);
	}

	public void goToUserAuthorizationPage() {

		FacesContextUtils.redirect(config.getPageUrl().YETKI_USER_AUTH_PAGE);
	}

	public void loadUserList() {

	}

	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	public String getAd() {
		return ad;
	}

	public void setAd(String ad) {
		this.ad = ad;
	}

	public String getAciklama() {
		return aciklama;
	}

	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}

	public String getSelectedUserName() {
		return selectedUserName;
	}

	public void setSelectedUserName(String selectedUserName) {
		this.selectedUserName = selectedUserName;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public List<SystemUser> getUserList() {
		return userList;
	}

	public void setUserList(List<SystemUser> userList) {
		this.userList = userList;
	}

	public List<YetkiMenu> getYetkiMenuList() {
		return yetkiMenuList;
	}

	public void setYetkiMenuList(List<YetkiMenu> yetkiMenuList) {
		this.yetkiMenuList = yetkiMenuList;
	}

	public List<LkYetkiRol> getMenuRolList() {
		return menuRolList;
	}

	public void setMenuRolList(List<LkYetkiRol> menuRolList) {
		this.menuRolList = menuRolList;
	}

	public LkYetkiRol getSelectedYetkiRol() {
		return selectedYetkiRol;
	}

	public void setSelectedYetkiRol(LkYetkiRol selectedYetkiRol) {
		this.selectedYetkiRol = selectedYetkiRol;
	}

	public SystemUser getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(SystemUser selectedUser) {
		this.selectedUser = selectedUser;
	}

	public YetkiMenu getSelectedMenu() {
		return selectedMenu;
	}

	public void setSelectedMenu(YetkiMenu selectedMenu) {
		this.selectedMenu = selectedMenu;
	}

	public Integer getSelectedRole() {
		return selectedRole;
	}

	public void setSelectedRole(Integer selectedRole) {
		this.selectedRole = selectedRole;
	}

	public boolean isEkranRolRendered() {
		return ekranRolRendered;
	}

	public void setEkranRolRendered(boolean ekranRolRendered) {
		this.ekranRolRendered = ekranRolRendered;
	}

	public List<YetkiMenuKullaniciErisim> getKullaniciErisimList() {
		return kullaniciErisimList;
	}

	public void setKullaniciErisimList(
			List<YetkiMenuKullaniciErisim> kullaniciErisimList) {
		this.kullaniciErisimList = kullaniciErisimList;
	}

	public YetkiTreeNode getRoot() {
		return root;
	}

	public void setRoot(YetkiTreeNode root) {
		this.root = root;
	}

	public YetkiTreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(YetkiTreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public Integer getSelectedMenuId() {
		return selectedMenuId;
	}

	public void setSelectedMenuId(Integer selectedMenuId) {
		this.selectedMenuId = selectedMenuId;
	}

	public Integer getSelectedRoleId() {
		return selectedRoleId;
	}

	public void setSelectedRoleId(Integer selectedRoleId) {
		this.selectedRoleId = selectedRoleId;
	}

	public void setYetkiRolList(List<LkYetkiRol> yetkiRolList) {
		this.yetkiRolList = yetkiRolList;
	}

	public List<LkYetkiRol> getYetkiRolList() {
		return yetkiRolList;
	}

	public void setSelectedPageRoles(List<LkYetkiRol> selectedPageRoles) {
		this.selectedPageRoles = selectedPageRoles;
	}

	public List<LkYetkiRol> getSelectedPageRoles() {
		return selectedPageRoles;
	}

	public boolean isAddYetkiRendered() {
		return addYetkiRendered;
	}

	public void setAddYetkiRendered(boolean addYetkiRendered) {
		this.addYetkiRendered = addYetkiRendered;
	}

	public List<YetkiMenu> getAllYetkiMenuList() {
		return allYetkiMenuList;
	}

	public void setAllYetkiMenuList(List<YetkiMenu> allYetkiMenuList) {
		this.allYetkiMenuList = allYetkiMenuList;
	}

	public YetkiMenu getSelectedPage() {
		return selectedPage;
	}

	public void setSelectedPage(YetkiMenu selectedPage) {
		this.selectedPage = selectedPage;
	}

	public List<YetkiMenu> getSelectedPages() {
		return selectedPages;
	}

	public void setSelectedPages(List<YetkiMenu> selectedPages) {
		this.selectedPages = selectedPages;
	}

	public Integer getSelectedPageId() {
		return selectedPageId;
	}

	public void setSelectedPageId(Integer selectedPageId) {
		this.selectedPageId = selectedPageId;
	}

}
