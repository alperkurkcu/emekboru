package com.emekboru.beans.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AjaxBehaviorEvent;
import javax.persistence.EntityManager;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.Employee;
import com.emekboru.jpa.EmployeeEventParticipation;
import com.emekboru.jpaman.EmployeeEventParticipationManager;
import com.emekboru.jpaman.Factory;
import com.emekboru.utils.FacesContextUtils;


@ManagedBean
@ViewScoped
public class MyEvents implements Serializable{

	private static final long serialVersionUID = 1542626061657533208L;

	private List<EmployeeEventParticipation> 	events;
	private EmployeeEventParticipation			selectedEvent;
	
	public MyEvents(){
		events 			= new ArrayList<EmployeeEventParticipation>();
		selectedEvent 	= new EmployeeEventParticipation();
	}

	@PostConstruct
	public void load(){
		
		UserCredentialsBean bean = (UserCredentialsBean) FacesContextUtils
				                     .getSessionBean("userCredentialsBean");
		EntityManager manager = Factory.getInstance().createEntityManager();
		Employee employee = bean.getUser().getEmployee();
		employee = manager.find(Employee.class, employee.getEmployeeId());
		events = employee.getEmployeeEventParticipations();
		System.out.println("how many events we have: "+events.size());
	}
	
	public void onParticipatedSelect(AjaxBehaviorEvent event){

		HtmlSelectOneMenu one = (HtmlSelectOneMenu) event.getSource();
		Boolean participated = Boolean.parseBoolean(one.getValue().toString());
		EmployeeEventParticipationManager manager 
		                             = new EmployeeEventParticipationManager();
		selectedEvent.setParticipated(participated);
		manager.updateEntity(selectedEvent);
	}
	
	/**
	 * 		GETTERS AND SETTERS	
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<EmployeeEventParticipation> getEvents() {
		return events;
	}

	public void setEvents(List<EmployeeEventParticipation> events) {
		this.events = events;
	}

	public EmployeeEventParticipation getSelectedEvent() {
		return selectedEvent;
	}

	public void setSelectedEvent(EmployeeEventParticipation selectedEvent) {
		this.selectedEvent = selectedEvent;
	}
	
}
