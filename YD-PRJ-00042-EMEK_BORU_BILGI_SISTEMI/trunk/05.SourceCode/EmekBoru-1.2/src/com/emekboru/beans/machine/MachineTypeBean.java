package com.emekboru.beans.machine;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import com.emekboru.jpa.MachineType;
import com.emekboru.jpaman.MachineTypeManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "machineTypeBean")
@ViewScoped
public class MachineTypeBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private MachineType selectedMachineType;
	private MachineType newMachineType;

	public MachineTypeBean() {
		selectedMachineType = new MachineType();
		newMachineType = new MachineType();
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void cancelMachineTypeSubmittion(ActionEvent event) {
		newMachineType = new MachineType();
	}

	public void submitNewMachineType(ActionEvent event) {

		MachineTypeManager machineTypeManager = new MachineTypeManager();
		if (newMachineType != null && newMachineType.getMachineTypeId() != null) {

			machineTypeManager.updateEntity(newMachineType);
		} else {

			machineTypeManager.enterNew(newMachineType);
		}

		MachineUtilsListBean.loadMachineTypeList();
		newMachineType = new MachineType();
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void updateMachineType(ActionEvent event) {

		MachineTypeManager machineTypeManager = new MachineTypeManager();
		if (selectedMachineType != null
				&& selectedMachineType.getMachineTypeId() != null) {
			machineTypeManager.updateEntity(selectedMachineType);
		}

		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}

	public void deleteMachineType(ActionEvent event) {

		MachineTypeManager machineTypeManager = new MachineTypeManager();
		machineTypeManager.deleteByField(MachineType.class, "machineTypeId",
				selectedMachineType.getMachineTypeId());

		FacesContextUtils.addWarnMessage("DeletedMessage");
		selectedMachineType = new MachineType();
		MachineUtilsListBean.setAllMachineTypes();
	}

	// ********************************************************** //
	// *** GETTERS AND SETTERS *** //
	// ********************************************************** //
	public MachineType getNewMachineType() {
		return newMachineType;
	}

	public MachineType getSelectedMachineType() {
		return selectedMachineType;
	}

	public void setSelectedMachineType(MachineType selectedMachineType) {
		this.selectedMachineType = selectedMachineType;
	}

	public void setNewMachineType(MachineType newMachineType) {
		this.newMachineType = newMachineType;
	}

}
