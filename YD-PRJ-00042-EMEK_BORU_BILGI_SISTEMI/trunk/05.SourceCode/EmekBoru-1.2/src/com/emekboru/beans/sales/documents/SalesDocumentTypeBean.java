package com.emekboru.beans.sales.documents;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.sales.documents.SalesDocumentType;
import com.emekboru.jpaman.sales.documents.SalesDocumentTypeManager;

@ManagedBean(name = "salesDocumentTypeBean")
@ViewScoped
public class SalesDocumentTypeBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4633092219683556337L;
	private List<SalesDocumentType> documentTypeList;

	public SalesDocumentTypeBean() {

		SalesDocumentTypeManager manager = new SalesDocumentTypeManager();
		documentTypeList = manager.findAll(SalesDocumentType.class);
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public List<SalesDocumentType> getDocumentTypeList() {
		return documentTypeList;
	}

	public void setDocumentTypeList(List<SalesDocumentType> documentTypeList) {
		this.documentTypeList = documentTypeList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}