package com.emekboru.beans.coatingmaterials;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.CoatMaterialType;
import com.emekboru.jpaman.CoatRawMaterialManager;
import com.emekboru.utils.DateTrans;

@ManagedBean(name = "coatingMaterialListBean")
@ViewScoped
public class CoatingMaterialListBean {

	private static List<Integer> historyYearList;
	private Integer currentYear;

	public CoatingMaterialListBean() {
		currentYear = DateTrans.getYearOfDate(new Date());
		historyYearList = new ArrayList<Integer>();
	}

	public static void generateYearList(CoatMaterialType coatMaterialType) {
		CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
		List<Date> earliestDateList = materialManager
				.findMinimumDateByType(coatMaterialType.getCoatMaterialTypeId());
		// the database is empty
		if (earliestDateList.size() == 0) {
			historyYearList = new ArrayList<Integer>();
			return;
		}
		List<Date> latestDateList = materialManager
				.findMaximumDateByType(coatMaterialType.getCoatMaterialTypeId());
		Integer earliestYear = DateTrans.getYearOfDate(earliestDateList.get(0));
		Integer latestYear = DateTrans.getYearOfDate(latestDateList.get(0));
		for (int i = earliestYear; i <= latestYear; i++)
			historyYearList.add(i);
	}

	public List<Integer> getHistoryYearList() {
		return historyYearList;
	}

	@SuppressWarnings("static-access")
	public void setHistoryYearList(List<Integer> historyYearList) {
		this.historyYearList = historyYearList;
	}

	public Integer getCurrentYear() {
		return currentYear;
	}

	public void setCurrentYear(Integer currentYear) {
		this.currentYear = currentYear;
	}

}
