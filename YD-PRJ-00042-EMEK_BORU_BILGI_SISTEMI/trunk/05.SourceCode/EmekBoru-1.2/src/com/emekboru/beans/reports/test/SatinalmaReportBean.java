/**
 * 
 */
package com.emekboru.beans.reports.test;

/**
 * @author kurkcu
 *
 */

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.satinalma.SatinalmaOdemeBildirimi;
import com.emekboru.jpa.satinalma.SatinalmaOdemeBildirimiIcerik;
import com.emekboru.jpa.satinalma.SatinalmaSatinalmaKarari;
import com.emekboru.jpa.satinalma.SatinalmaSatinalmaKarariIcerik;
import com.emekboru.jpa.satinalma.SatinalmaSiparisBildirimi;
import com.emekboru.jpa.satinalma.SatinalmaSiparisBildirimiIcerik;
import com.emekboru.jpa.satinalma.SatinalmaSiparisMektubu;
import com.emekboru.jpa.satinalma.SatinalmaSiparisMektubuIcerik;
import com.emekboru.jpa.satinalma.SatinalmaTeklifIsteme;
import com.emekboru.jpa.satinalma.SatinalmaTeklifIstemeIcerik;
import com.emekboru.jpaman.satinalma.SatinalmaOdemeBildirimiIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaOdemeBildirimiManager;
import com.emekboru.jpaman.satinalma.SatinalmaSatinalmaKarariIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaSatinalmaKarariManager;
import com.emekboru.jpaman.satinalma.SatinalmaSiparisBildirimiIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaSiparisBildirimiManager;
import com.emekboru.jpaman.satinalma.SatinalmaSiparisMektubuIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaSiparisMektubuManager;
import com.emekboru.jpaman.satinalma.SatinalmaTeklifIstemeIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaTeklifIstemeManager;
import com.emekboru.utils.ReportUtil;

@ManagedBean(name = "satinalmaReportBean")
@SessionScoped
public class SatinalmaReportBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	@PostConstruct
	public void init() {
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void teklifFormuXlsx(Integer prmTeklifId) {

		try {
			SatinalmaTeklifIstemeManager teklifManager = new SatinalmaTeklifIstemeManager();
			SatinalmaTeklifIstemeIcerikManager icerikManager = new SatinalmaTeklifIstemeIcerikManager();

			SatinalmaTeklifIsteme teklif = new SatinalmaTeklifIsteme();
			List<SatinalmaTeklifIstemeIcerik> teklifIceriks = new ArrayList<SatinalmaTeklifIstemeIcerik>();

			teklif = teklifManager.loadObject(SatinalmaTeklifIsteme.class,
					prmTeklifId);

			teklifIceriks = icerikManager
					.getAllSatinalmaTeklifIstemeIcerik(prmTeklifId);

			List<String> date = new ArrayList<String>();

			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			Map dataMap = new HashMap();
			dataMap.put("teklif", teklif);
			dataMap.put("teklifIceriks", teklifIceriks);
			dataMap.put("date", date);
			dataMap.put("satinalmaSorumlusu", teklifIceriks.get(0)
					.getSatinalmaTakipDosyalarIcerik().getGorevliPersonel()
					.getFirstname()
					+ " "
					+ teklifIceriks.get(0).getSatinalmaTakipDosyalarIcerik()
							.getGorevliPersonel().getLastname());
			dataMap.put("satinalmaMuduru", "Zehra KAYA");

			ReportUtil reportUtil = new ReportUtil();
			reportUtil
					.jxlsExportAsXls(dataMap,
							"/reportformats/satinalma/teklifFormu.xls",
							date.toString());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"TEKLİF ÇIKTISI BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"TEKLİF ÇIKTISI OLUŞTURULAMADI, HATA!", null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void satinalmaKarariFormuXlsx(Integer prmKararId, Integer prmRapor) {

		try {

			int firmaCount = 0;
			List<SatinalmaTeklifIsteme> teklifList = new ArrayList<SatinalmaTeklifIsteme>();

			List<SatinalmaSatinalmaKarariIcerik> firma1List = new ArrayList<SatinalmaSatinalmaKarariIcerik>();
			List<SatinalmaSatinalmaKarariIcerik> firma2List = new ArrayList<SatinalmaSatinalmaKarariIcerik>();
			List<SatinalmaSatinalmaKarariIcerik> firma3List = new ArrayList<SatinalmaSatinalmaKarariIcerik>();
			List<SatinalmaSatinalmaKarariIcerik> firma4List = new ArrayList<SatinalmaSatinalmaKarariIcerik>();
			List<SatinalmaSatinalmaKarariIcerik> firma5List = new ArrayList<SatinalmaSatinalmaKarariIcerik>();
			List<SatinalmaSatinalmaKarariIcerik> firma6List = new ArrayList<SatinalmaSatinalmaKarariIcerik>();
			List<SatinalmaSatinalmaKarariIcerik> firma7List = new ArrayList<SatinalmaSatinalmaKarariIcerik>();

			double firma1IlkToplam = 0.0;
			double firma1NihaiToplam = 0.0;
			double firma2IlkToplam = 0.0;
			double firma2NihaiToplam = 0.0;
			double firma3IlkToplam = 0.0;
			double firma3NihaiToplam = 0.0;
			double firma4IlkToplam = 0.0;
			double firma4NihaiToplam = 0.0;
			double firma5IlkToplam = 0.0;
			double firma5NihaiToplam = 0.0;
			double firma6IlkToplam = 0.0;
			double firma6NihaiToplam = 0.0;
			double firma7IlkToplam = 0.0;
			double firma7NihaiToplam = 0.0;

			SatinalmaSatinalmaKarariManager kararManager = new SatinalmaSatinalmaKarariManager();
			SatinalmaSatinalmaKarariIcerikManager icerikManager = new SatinalmaSatinalmaKarariIcerikManager();

			SatinalmaSatinalmaKarari karar = new SatinalmaSatinalmaKarari();
			List<SatinalmaSatinalmaKarariIcerik> kararIceriks = new ArrayList<SatinalmaSatinalmaKarariIcerik>();
			List<SatinalmaSatinalmaKarariIcerik> kabulKararIceriks = new ArrayList<SatinalmaSatinalmaKarariIcerik>();

			karar = kararManager.loadObject(SatinalmaSatinalmaKarari.class,
					prmKararId);

			kararIceriks = icerikManager.satinalmaKarariListDoldur(prmKararId);

			kabulKararIceriks = icerikManager
					.findKabulEdilenByKararId(prmKararId);

			SatinalmaTeklifIstemeManager teklifManager = new SatinalmaTeklifIstemeManager();
			teklifList = teklifManager.findByDosyaId(kararIceriks.get(0)
					.getSatinalmaSatinalmaKarari().getDosyaNo());
			firmaCount = teklifList.size();

			List<String> date = new ArrayList<String>();

			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			Map dataMap = new HashMap();
			dataMap.put("karar", karar);
			// dataMap.put("kararIceriks", kararIceriks);
			dataMap.put("kabulKararIceriks", kabulKararIceriks);

			if (firmaCount == 1) {
				SatinalmaSatinalmaKarariIcerikManager manager = new SatinalmaSatinalmaKarariIcerikManager();
				firma1List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(0).getSatinalmaMusteri()
								.getId());
				firma1IlkToplam = getIlkTeklifTotal(firma1List);
				firma1NihaiToplam = getNihaiTeklifTotal(firma1List);

				dataMap.put("firma1List", firma1List);

				dataMap.put("firma1IlkToplam", firma1IlkToplam);
				dataMap.put("firma1NihaiToplam", firma1NihaiToplam);

			} else if (firmaCount == 2) {

				SatinalmaSatinalmaKarariIcerikManager manager = new SatinalmaSatinalmaKarariIcerikManager();
				firma1List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(0).getSatinalmaMusteri()
								.getId());
				firma2List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(1).getSatinalmaMusteri()
								.getId());
				firma1IlkToplam = getIlkTeklifTotal(firma1List);
				firma1NihaiToplam = getNihaiTeklifTotal(firma1List);
				firma2IlkToplam = getIlkTeklifTotal(firma2List);
				firma2NihaiToplam = getNihaiTeklifTotal(firma2List);

				dataMap.put("firma1List", firma1List);
				dataMap.put("firma2List", firma2List);

				dataMap.put("firma1IlkToplam", firma1IlkToplam);
				dataMap.put("firma1NihaiToplam", firma1NihaiToplam);
				dataMap.put("firma2IlkToplam", firma2IlkToplam);
				dataMap.put("firma2NihaiToplam", firma2NihaiToplam);

			} else if (firmaCount == 3) {

				SatinalmaSatinalmaKarariIcerikManager manager = new SatinalmaSatinalmaKarariIcerikManager();
				firma1List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(0).getSatinalmaMusteri()
								.getId());
				firma2List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(1).getSatinalmaMusteri()
								.getId());
				firma3List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(2).getSatinalmaMusteri()
								.getId());
				firma1IlkToplam = getIlkTeklifTotal(firma1List);
				firma1NihaiToplam = getNihaiTeklifTotal(firma1List);
				firma2IlkToplam = getIlkTeklifTotal(firma2List);
				firma2NihaiToplam = getNihaiTeklifTotal(firma2List);
				firma3IlkToplam = getIlkTeklifTotal(firma3List);
				firma3NihaiToplam = getNihaiTeklifTotal(firma3List);

				dataMap.put("firma1List", firma1List);
				dataMap.put("firma2List", firma2List);
				dataMap.put("firma3List", firma3List);

				dataMap.put("firma1IlkToplam", firma1IlkToplam);
				dataMap.put("firma1NihaiToplam", firma1NihaiToplam);
				dataMap.put("firma2IlkToplam", firma2IlkToplam);
				dataMap.put("firma2NihaiToplam", firma2NihaiToplam);
				dataMap.put("firma3IlkToplam", firma3IlkToplam);
				dataMap.put("firma3NihaiToplam", firma3NihaiToplam);

			} else if (firmaCount == 4) {

				SatinalmaSatinalmaKarariIcerikManager manager = new SatinalmaSatinalmaKarariIcerikManager();
				firma1List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(0).getSatinalmaMusteri()
								.getId());
				firma2List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(1).getSatinalmaMusteri()
								.getId());
				firma3List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(2).getSatinalmaMusteri()
								.getId());
				firma4List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(3).getSatinalmaMusteri()
								.getId());
				firma1IlkToplam = getIlkTeklifTotal(firma1List);
				firma1NihaiToplam = getNihaiTeklifTotal(firma1List);
				firma2IlkToplam = getIlkTeklifTotal(firma2List);
				firma2NihaiToplam = getNihaiTeklifTotal(firma2List);
				firma3IlkToplam = getIlkTeklifTotal(firma3List);
				firma3NihaiToplam = getNihaiTeklifTotal(firma3List);
				firma4IlkToplam = getIlkTeklifTotal(firma4List);
				firma4NihaiToplam = getNihaiTeklifTotal(firma4List);

				dataMap.put("firma1List", firma1List);
				dataMap.put("firma2List", firma2List);
				dataMap.put("firma3List", firma3List);
				dataMap.put("firma4List", firma4List);

				dataMap.put("firma1IlkToplam", firma1IlkToplam);
				dataMap.put("firma1NihaiToplam", firma1NihaiToplam);
				dataMap.put("firma2IlkToplam", firma2IlkToplam);
				dataMap.put("firma2NihaiToplam", firma2NihaiToplam);
				dataMap.put("firma3IlkToplam", firma3IlkToplam);
				dataMap.put("firma3NihaiToplam", firma3NihaiToplam);
				dataMap.put("firma4IlkToplam", firma4IlkToplam);
				dataMap.put("firma4NihaiToplam", firma4NihaiToplam);

			} else if (firmaCount == 5) {

				SatinalmaSatinalmaKarariIcerikManager manager = new SatinalmaSatinalmaKarariIcerikManager();
				firma1List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(0).getSatinalmaMusteri()
								.getId());
				firma2List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(1).getSatinalmaMusteri()
								.getId());
				firma3List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(2).getSatinalmaMusteri()
								.getId());
				firma4List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(3).getSatinalmaMusteri()
								.getId());
				firma5List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(4).getSatinalmaMusteri()
								.getId());
				firma1IlkToplam = getIlkTeklifTotal(firma1List);
				firma1NihaiToplam = getNihaiTeklifTotal(firma1List);
				firma2IlkToplam = getIlkTeklifTotal(firma2List);
				firma2NihaiToplam = getNihaiTeklifTotal(firma2List);
				firma3IlkToplam = getIlkTeklifTotal(firma3List);
				firma3NihaiToplam = getNihaiTeklifTotal(firma3List);
				firma4IlkToplam = getIlkTeklifTotal(firma4List);
				firma4NihaiToplam = getNihaiTeklifTotal(firma4List);
				firma5IlkToplam = getIlkTeklifTotal(firma5List);
				firma5NihaiToplam = getNihaiTeklifTotal(firma5List);

				dataMap.put("firma1List", firma1List);
				dataMap.put("firma2List", firma2List);
				dataMap.put("firma3List", firma3List);
				dataMap.put("firma4List", firma4List);
				dataMap.put("firma5List", firma5List);

				dataMap.put("firma1IlkToplam", firma1IlkToplam);
				dataMap.put("firma1NihaiToplam", firma1NihaiToplam);
				dataMap.put("firma2IlkToplam", firma2IlkToplam);
				dataMap.put("firma2NihaiToplam", firma2NihaiToplam);
				dataMap.put("firma3IlkToplam", firma3IlkToplam);
				dataMap.put("firma3NihaiToplam", firma3NihaiToplam);
				dataMap.put("firma4IlkToplam", firma4IlkToplam);
				dataMap.put("firma4NihaiToplam", firma4NihaiToplam);
				dataMap.put("firma5IlkToplam", firma5IlkToplam);
				dataMap.put("firma5NihaiToplam", firma5NihaiToplam);

			} else if (firmaCount == 6) {

				SatinalmaSatinalmaKarariIcerikManager manager = new SatinalmaSatinalmaKarariIcerikManager();
				firma1List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(0).getSatinalmaMusteri()
								.getId());
				firma2List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(1).getSatinalmaMusteri()
								.getId());
				firma3List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(2).getSatinalmaMusteri()
								.getId());
				firma4List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(3).getSatinalmaMusteri()
								.getId());
				firma5List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(4).getSatinalmaMusteri()
								.getId());
				firma6List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(5).getSatinalmaMusteri()
								.getId());
				firma1IlkToplam = getIlkTeklifTotal(firma1List);
				firma1NihaiToplam = getNihaiTeklifTotal(firma1List);
				firma2IlkToplam = getIlkTeklifTotal(firma2List);
				firma2NihaiToplam = getNihaiTeklifTotal(firma2List);
				firma3IlkToplam = getIlkTeklifTotal(firma3List);
				firma3NihaiToplam = getNihaiTeklifTotal(firma3List);
				firma4IlkToplam = getIlkTeklifTotal(firma4List);
				firma4NihaiToplam = getNihaiTeklifTotal(firma4List);
				firma5IlkToplam = getIlkTeklifTotal(firma5List);
				firma5NihaiToplam = getNihaiTeklifTotal(firma5List);
				firma6IlkToplam = getIlkTeklifTotal(firma6List);
				firma6NihaiToplam = getNihaiTeklifTotal(firma6List);

				dataMap.put("firma1List", firma1List);
				dataMap.put("firma2List", firma2List);
				dataMap.put("firma3List", firma3List);
				dataMap.put("firma4List", firma4List);
				dataMap.put("firma5List", firma5List);
				dataMap.put("firma6List", firma6List);

				dataMap.put("firma1IlkToplam", firma1IlkToplam);
				dataMap.put("firma1NihaiToplam", firma1NihaiToplam);
				dataMap.put("firma2IlkToplam", firma2IlkToplam);
				dataMap.put("firma2NihaiToplam", firma2NihaiToplam);
				dataMap.put("firma3IlkToplam", firma3IlkToplam);
				dataMap.put("firma3NihaiToplam", firma3NihaiToplam);
				dataMap.put("firma4IlkToplam", firma4IlkToplam);
				dataMap.put("firma4NihaiToplam", firma4NihaiToplam);
				dataMap.put("firma5IlkToplam", firma5IlkToplam);
				dataMap.put("firma5NihaiToplam", firma5NihaiToplam);
				dataMap.put("firma6IlkToplam", firma6IlkToplam);
				dataMap.put("firma6NihaiToplam", firma6NihaiToplam);

			} else if (firmaCount == 7) {

				SatinalmaSatinalmaKarariIcerikManager manager = new SatinalmaSatinalmaKarariIcerikManager();
				firma1List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(0).getSatinalmaMusteri()
								.getId());
				firma2List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(1).getSatinalmaMusteri()
								.getId());
				firma3List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(2).getSatinalmaMusteri()
								.getId());
				firma4List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(3).getSatinalmaMusteri()
								.getId());
				firma5List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(4).getSatinalmaMusteri()
								.getId());
				firma6List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(5).getSatinalmaMusteri()
								.getId());
				firma7List = manager.satinalmaKarariListDoldurByFirma(
						prmKararId, teklifList.get(6).getSatinalmaMusteri()
								.getId());
				firma1IlkToplam = getIlkTeklifTotal(firma1List);
				firma1NihaiToplam = getNihaiTeklifTotal(firma1List);
				firma2IlkToplam = getIlkTeklifTotal(firma2List);
				firma2NihaiToplam = getNihaiTeklifTotal(firma2List);
				firma3IlkToplam = getIlkTeklifTotal(firma3List);
				firma3NihaiToplam = getNihaiTeklifTotal(firma3List);
				firma4IlkToplam = getIlkTeklifTotal(firma4List);
				firma4NihaiToplam = getNihaiTeklifTotal(firma4List);
				firma5IlkToplam = getIlkTeklifTotal(firma5List);
				firma5NihaiToplam = getNihaiTeklifTotal(firma5List);
				firma6IlkToplam = getIlkTeklifTotal(firma6List);
				firma6NihaiToplam = getNihaiTeklifTotal(firma6List);
				firma7IlkToplam = getIlkTeklifTotal(firma7List);
				firma7NihaiToplam = getNihaiTeklifTotal(firma7List);

				dataMap.put("firma1List", firma1List);
				dataMap.put("firma2List", firma2List);
				dataMap.put("firma3List", firma3List);
				dataMap.put("firma4List", firma4List);
				dataMap.put("firma5List", firma5List);
				dataMap.put("firma6List", firma6List);
				dataMap.put("firma7List", firma7List);

				dataMap.put("firma1IlkToplam", firma1IlkToplam);
				dataMap.put("firma1NihaiToplam", firma1NihaiToplam);
				dataMap.put("firma2IlkToplam", firma2IlkToplam);
				dataMap.put("firma2NihaiToplam", firma2NihaiToplam);
				dataMap.put("firma3IlkToplam", firma3IlkToplam);
				dataMap.put("firma3NihaiToplam", firma3NihaiToplam);
				dataMap.put("firma4IlkToplam", firma4IlkToplam);
				dataMap.put("firma4NihaiToplam", firma4NihaiToplam);
				dataMap.put("firma5IlkToplam", firma5IlkToplam);
				dataMap.put("firma5NihaiToplam", firma5NihaiToplam);
				dataMap.put("firma6IlkToplam", firma6IlkToplam);
				dataMap.put("firma6NihaiToplam", firma6NihaiToplam);
				dataMap.put("firma7IlkToplam", firma7IlkToplam);
				dataMap.put("firma7NihaiToplam", firma7NihaiToplam);
			}

			dataMap.put("odeme", kabulKararIceriks.get(0).getOdemeAciklama());
			dataMap.put("kur", kabulKararIceriks.get(0).getKur());
			dataMap.put("teslimSuresi", kabulKararIceriks.get(0)
					.getTeslimSuresi());
			dataMap.put("teslimYeri", kabulKararIceriks.get(0)
					.getTeslimYeriNakliyeAciklama());
			dataMap.put("date", date);
			dataMap.put("aciklama", karar.getAciklama());
			dataMap.put("sonuc", karar.getSonucFirma());

			ReportUtil reportUtil = new ReportUtil();
			if (prmRapor == 0) {
				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/satinalma/satinalmaKarariVeriler.xls",
						date.toString());
			} else if (prmRapor == 1) {
				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/satinalma/satinalmaKarari.xls",
						date.toString());
			}

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"SATINALMA KARARI ÇIKTISI BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"SATINALMA KARARI ÇIKTISI OLUŞTURULAMADI, HATA!", null));
			e.printStackTrace();
		}

	}

	public double getIlkTeklifTotal(List<SatinalmaSatinalmaKarariIcerik> prmList) {
		double quantityIlk = 0.0;
		for (SatinalmaSatinalmaKarariIcerik p : prmList) {
			if (String.valueOf(p.getIlkTeklifBirimFiyat()) != null)
				quantityIlk += p.getIlkTeklifBirimFiyat()
						* p.getSatinalmaTeklifIstemeIcerik()
								.getSatinalmaTakipDosyalarIcerik()
								.getSatinalmaMalzemeHizmetTalepFormuIcerik()
								.getMiktar();
		}
		return quantityIlk;
	}

	public double getNihaiTeklifTotal(
			List<SatinalmaSatinalmaKarariIcerik> prmList) {
		double quantityNihai = 0.0;
		for (SatinalmaSatinalmaKarariIcerik p : prmList) {
			if (String.valueOf(p.getNihaiTeklifBirimFiyat()) != null)
				quantityNihai += p.getNihaiTeklifBirimFiyat()
						* p.getSatinalmaTeklifIstemeIcerik()
								.getSatinalmaTakipDosyalarIcerik()
								.getSatinalmaMalzemeHizmetTalepFormuIcerik()
								.getMiktar();
		}
		return quantityNihai;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void siparisMektubuFormuXlsx(Integer prmMektupId) {

		try {
			SatinalmaSiparisMektubuManager mektupManager = new SatinalmaSiparisMektubuManager();
			SatinalmaSiparisMektubuIcerikManager icerikManager = new SatinalmaSiparisMektubuIcerikManager();

			SatinalmaSiparisMektubu mektup = new SatinalmaSiparisMektubu();
			List<SatinalmaSiparisMektubuIcerik> mektupIceriks = new ArrayList<SatinalmaSiparisMektubuIcerik>();

			mektup = mektupManager.findByKararId(prmMektupId).get(0);

			mektupIceriks = icerikManager.findByKararId(prmMektupId);

			List<String> date = new ArrayList<String>();

			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			Map dataMap = new HashMap();
			dataMap.put("mektup", mektup);
			dataMap.put("mektupIceriks", mektupIceriks);
			dataMap.put("date", date);
			dataMap.put("kur", mektupIceriks.get(0)
					.getSatinalmaSatinalmaKarariIcerik().getKur());

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/satinalma/siparisFormu.xls",
					date.toString());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"TEKLİF ÇIKTISI BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"TEKLİF ÇIKTISI OLUŞTURULAMADI, HATA!", null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void siparisBildirimiFormuXlsx(Integer prmTeklifId) {

		try {
			SatinalmaSiparisBildirimiManager bildirimManager = new SatinalmaSiparisBildirimiManager();
			SatinalmaSiparisBildirimiIcerikManager icerikManager = new SatinalmaSiparisBildirimiIcerikManager();

			SatinalmaSiparisBildirimi bildirim = new SatinalmaSiparisBildirimi();
			List<SatinalmaSiparisBildirimiIcerik> bildirimIceriks = new ArrayList<SatinalmaSiparisBildirimiIcerik>();

			bildirim = bildirimManager.loadObject(
					SatinalmaSiparisBildirimi.class, prmTeklifId);

			bildirimIceriks = icerikManager.findByBildirimId(prmTeklifId);

			List<String> date = new ArrayList<String>();

			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			Map dataMap = new HashMap();
			dataMap.put("bildirim", bildirim);
			dataMap.put("bildirimIceriks", bildirimIceriks);
			dataMap.put("date", date);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/satinalma/siparisBildirimi.xls",
					date.toString());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"SİPARİŞ BİLDİRİMİ ÇIKTISI BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"SİPARİŞ BİLDİRİMİ ÇIKTISI OLUŞTURULAMADI, HATA!", null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void odemeBildirimiFormuXlsx(Integer prmTeklifId) {

		try {
			SatinalmaOdemeBildirimiManager bildirimManager = new SatinalmaOdemeBildirimiManager();
			SatinalmaOdemeBildirimiIcerikManager icerikManager = new SatinalmaOdemeBildirimiIcerikManager();

			SatinalmaOdemeBildirimi bildirim = new SatinalmaOdemeBildirimi();
			List<SatinalmaOdemeBildirimiIcerik> bildirimIceriks = new ArrayList<SatinalmaOdemeBildirimiIcerik>();

			bildirim = bildirimManager.loadObject(
					SatinalmaOdemeBildirimi.class, prmTeklifId);

			bildirimIceriks = icerikManager.findByBildirimId(prmTeklifId);

			List<String> date = new ArrayList<String>();

			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			Map dataMap = new HashMap();
			dataMap.put("bildirim", bildirim);
			dataMap.put("bildirimIceriks", bildirimIceriks);
			dataMap.put("date", date);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/satinalma/odemeBildirimi.xls",
					date.toString());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"ÖDEME BİLDİRİMİ ÇIKTISI BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖDEME BİLDİRİMİ ÇIKTISI OLUŞTURULAMADI, HATA!", null));
			e.printStackTrace();
		}

	}

	// getters setters

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	// /**
	// * JR Example
	// */
	// // public void manuelUtInspectionResultReportXlsx() throws JRException,
	// // IOException {
	// //
	// // SalesItemManager salesItemManager = new SalesItemManager();
	// // List<SalesItem> salesItems = new ArrayList<SalesItem>();
	// //
	// // salesItems = salesItemManager.findByField(SalesItem.class, "itemId",
	// // 100);
	// //
	// // ReportUtil reportUtil = new ReportUtil();
	// // reportUtil.jrExportAsXlsx(salesItems,
	// // "/reportformats/test/manuelUtInspectionResultReport.jasper");
	// //
	// // }

	// public void imageTest() throws IOException {
	//
	// ExchangeRateUtil exchangeRateUtil = new ExchangeRateUtil();
	//
	// ReportUtil reportUtil = new ReportUtil();
	//
	// Barcode128 code128 = new Barcode128();
	// code128.setCode("F220H502054");
	// code128.setChecksumText(true);
	// code128.setGenerateChecksum(true);
	// code128.setStartStopText(true);
	//
	// java.awt.Image image = code128.createAwtImage(Color.BLACK, Color.WHITE);
	//
	// HSSFWorkbook hssfWorkbook = reportUtil
	// .getWorkbookFromFilePath("/reportformats/test/test.xls");
	//
	// reportUtil.insertImageToWorkbook(hssfWorkbook, image, "testImage",
	// image.getWidth(null), image.getHeight(null));
	// reportUtil.insertImageToWorkbook(hssfWorkbook, image, "testImage2",
	// image.getWidth(null), image.getHeight(null));
	// reportUtil.writeWorkbookToFile(hssfWorkbook,
	// "/reportformats/test/testOut.xls");
	//
	// }

}
