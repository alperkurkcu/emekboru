package com.emekboru.beans.sales.order;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.sales.order.SalesInternalCoveringType;
import com.emekboru.jpaman.sales.order.SalesInternalCoveringTypeManager;

@ManagedBean(name = "salesInternalCoveringTypeBean")
@ViewScoped
public class SalesInternalCoveringTypeBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 400456992895812564L;
	private List<SalesInternalCoveringType> typeList;

	public SalesInternalCoveringTypeBean() {

		SalesInternalCoveringTypeManager manager = new SalesInternalCoveringTypeManager();
		typeList = manager.findAll(SalesInternalCoveringType.class);
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public List<SalesInternalCoveringType> getTypeList() {
		return typeList;
	}

	public void setTypeList(List<SalesInternalCoveringType> typeList) {
		this.typeList = typeList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}