/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaGasBlisteringTestSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaGasBlisteringTestSonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testKaplamaGasBlisteringTestSonucBean")
@ViewScoped
public class TestKaplamaGasBlisteringTestSonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private TestKaplamaGasBlisteringTestSonuc testKaplamaGasBlisteringTestSonucForm = new TestKaplamaGasBlisteringTestSonuc();
	private List<TestKaplamaGasBlisteringTestSonuc> allTestKaplamaGasBlisteringTestSonucList = new ArrayList<TestKaplamaGasBlisteringTestSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateTestKaplamaGasBlisteringTestSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaGasBlisteringTestSonucManager tkbebManager = new TestKaplamaGasBlisteringTestSonucManager();

		if (testKaplamaGasBlisteringTestSonucForm.getId() == null) {

			testKaplamaGasBlisteringTestSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaGasBlisteringTestSonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaGasBlisteringTestSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaGasBlisteringTestSonucForm.setBagliTestId(bagliTest);
			testKaplamaGasBlisteringTestSonucForm.setBagliGlobalId(bagliTest);

			tkbebManager.enterNew(testKaplamaGasBlisteringTestSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			testKaplamaGasBlisteringTestSonucForm = new TestKaplamaGasBlisteringTestSonuc();
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaGasBlisteringTestSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaGasBlisteringTestSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			tkbebManager.updateEntity(testKaplamaGasBlisteringTestSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addTestKaplamaGasBlisteringTestSonuc() {

		testKaplamaGasBlisteringTestSonucForm = new TestKaplamaGasBlisteringTestSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaGasBlisteringTestSonucListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaGasBlisteringTestSonucList = TestKaplamaGasBlisteringTestSonucManager
				.getAllGasBlisteringTestSonuc(globalId, pipeId);
		testKaplamaGasBlisteringTestSonucForm = new TestKaplamaGasBlisteringTestSonuc();
	}

	public void deleteTestKaplamaGasBlisteringTestSonuc() {

		bagliTestId = testKaplamaGasBlisteringTestSonucForm.getBagliGlobalId()
				.getId();

		if (testKaplamaGasBlisteringTestSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		TestKaplamaGasBlisteringTestSonucManager tkbebManager = new TestKaplamaGasBlisteringTestSonucManager();

		tkbebManager.delete(testKaplamaGasBlisteringTestSonucForm);
		testKaplamaGasBlisteringTestSonucForm = new TestKaplamaGasBlisteringTestSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters

	/**
	 * @return the testKaplamaGasBlisteringTestSonucForm
	 */
	public TestKaplamaGasBlisteringTestSonuc getTestKaplamaGasBlisteringTestSonucForm() {
		return testKaplamaGasBlisteringTestSonucForm;
	}

	/**
	 * @param testKaplamaGasBlisteringTestSonucForm
	 *            the testKaplamaGasBlisteringTestSonucForm to set
	 */
	public void setTestKaplamaGasBlisteringTestSonucForm(
			TestKaplamaGasBlisteringTestSonuc testKaplamaGasBlisteringTestSonucForm) {
		this.testKaplamaGasBlisteringTestSonucForm = testKaplamaGasBlisteringTestSonucForm;
	}

	/**
	 * @return the allTestKaplamaGasBlisteringTestSonucList
	 */
	public List<TestKaplamaGasBlisteringTestSonuc> getAllTestKaplamaGasBlisteringTestSonucList() {
		return allTestKaplamaGasBlisteringTestSonucList;
	}

	/**
	 * @param allTestKaplamaGasBlisteringTestSonucList
	 *            the allTestKaplamaGasBlisteringTestSonucList to set
	 */
	public void setAllTestKaplamaGasBlisteringTestSonucList(
			List<TestKaplamaGasBlisteringTestSonuc> allTestKaplamaGasBlisteringTestSonucList) {
		this.allTestKaplamaGasBlisteringTestSonucList = allTestKaplamaGasBlisteringTestSonucList;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

}
