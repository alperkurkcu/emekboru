package com.emekboru.beans.license;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.LicenseRenewResponsible;
import com.emekboru.jpaman.Factory;
import com.emekboru.jpaman.JpqlComparativeClauses;
import com.emekboru.jpaman.LicenseRenewResponsibleManager;
import com.emekboru.jpaman.WhereClauseArgs;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean
@ViewScoped
public class MyLicenses implements Serializable{

	private static final long serialVersionUID = 1067009741476826780L;

	@EJB
	private ConfigBean                      config;
	// Two lists holding licenses to be renewed
	// and licenses that were renewed previously
	private List<LicenseRenewResponsible> 	followers;
	private List<LicenseRenewResponsible> 	completes;
	private LicenseRenewResponsible 		selected;
	
	// Hold a reference of the new date of expiration
	private Date nextExpDate;
	
	public MyLicenses(){
		followers 	= new ArrayList<LicenseRenewResponsible>(1);
		completes 	= new ArrayList<LicenseRenewResponsible>(1);
		selected 	= new LicenseRenewResponsible();
		nextExpDate = new Date();
	}

	@SuppressWarnings("rawtypes")
	@PostConstruct
	public void init(){
		System.out.println("MyLicenses.load()");
		UserCredentialsBean bean = (UserCredentialsBean) FacesContextUtils
				                    .getSessionBean("userCredentialsBean");
		LicenseRenewResponsibleManager manager 
		                            = new LicenseRenewResponsibleManager();
		ArrayList<WhereClauseArgs> conditionList 
		                               = new ArrayList<WhereClauseArgs>(1);
		
		WhereClauseArgs<Integer> arg1 
		                           = new WhereClauseArgs.Builder<Integer>()
				                       .setFieldName("employee.employeeId")
				     .setComparativeClause(JpqlComparativeClauses.NQ_EQUAL)
				                                      .setComplexKey(false)
				      .setValue(bean.getUser().getEmployee().getEmployeeId())
				                                                     .build();
		
		conditionList.add(arg1);
		List<LicenseRenewResponsible> list = manager
				              .selectFromWhereQuerieComplexName(
	               LicenseRenewResponsible.class, conditionList);
		distribute(list);
	}
	
	/*
	 *  Three steps to renew a license:
	 *  1 - Update the current LicenseRenewResponsible with the given date
	 *  2 - Update the License of the selected
	 *  3 - Enter a new LicenseRenewResponsible for the following renew
	 */
	public void renew(){
		
		if(!isOk()){
			FacesContextUtils.addErrorMessage("WrongDateMessage");
			return;
		}
		
		EntityManager manager = Factory.getInstance().createEntityManager();
		selected = manager.find(LicenseRenewResponsible.class, 
				selected.getLicenseRenewResponsibleId());
		manager.getTransaction().begin();
		selected.getLicense().setExpirationDate(nextExpDate);
		selected.setRenewDate(new Date());
		manager.getTransaction().commit();
		manager.clear();
		
		LicenseRenewResponsible lrr = new LicenseRenewResponsible();
		lrr.setEmployee(selected.getEmployee());
		lrr.setLicense(selected.getLicense());
		lrr.setPreviousExpirationDate(nextExpDate);
		lrr.setRenewed(false);
		manager.getTransaction().begin();
		manager.persist(lrr);
		manager.getTransaction().commit();
		manager.close();
		
		followers.remove(selected);
		followers.add(lrr);
		completes.add(selected);
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}
	
	public boolean isOk(){
		
		if(nextExpDate.before(selected.getLicense().getExpirationDate()))
			return false;
		return true;
	}
	public void distribute(List<LicenseRenewResponsible> list){
		
		for (LicenseRenewResponsible lrr : list) {
			if(lrr.getRenewDate() != null)
				completes.add(lrr);
			else
				followers.add(lrr);
		}
	}
	
	public void goToMyLicensesPage(){
		
		FacesContextUtils.redirect(config.getPageUrl().MY_LICENSES_PAGE);
	}
	
	/**
	 * 		GETTERS AND SETTERS
	 */
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public LicenseRenewResponsible getSelected() {
		return selected;
	}

	public void setSelected(LicenseRenewResponsible selected) {
		this.selected = selected;
	}

	public List<LicenseRenewResponsible> getFollowers() {
		return followers;
	}

	public void setFollowers(List<LicenseRenewResponsible> followers) {
		this.followers = followers;
	}

	public List<LicenseRenewResponsible> getCompletes() {
		return completes;
	}

	public void setCompletes(List<LicenseRenewResponsible> completes) {
		this.completes = completes;
	}

	public Date getNextExpDate() {
		return nextExpDate;
	}

	public void setNextExpDate(Date nextExpDate) {
		this.nextExpDate = nextExpDate;
	}
}
