/**
 * 
 */
package com.emekboru.beans.utils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.AgirlikTestsSpec;
import com.emekboru.jpa.BukmeTestsSpec;
import com.emekboru.jpa.CekmeTestsSpec;
import com.emekboru.jpa.CentikDarbeTestsSpec;
import com.emekboru.jpa.ChemicalRequirement;
import com.emekboru.jpa.DestructiveTestsSpec;
import com.emekboru.jpa.KaynakMakroTestSpec;
import com.emekboru.jpa.KaynakSertlikTestsSpec;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.TestAgirlikDusurmeSonuc;
import com.emekboru.jpa.TestBukmeSonuc;
import com.emekboru.jpa.TestCekmeSonuc;
import com.emekboru.jpa.TestCentikDarbeSonuc;
import com.emekboru.jpa.TestKaynakSonuc;
import com.emekboru.jpa.TestKimyasalAnalizSonuc;
import com.emekboru.jpa.coatingmaterials.KaplamaMalzemeKullanmaPe;
import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiPolietilen;
import com.emekboru.jpa.rulo.Rulo;
import com.emekboru.jpa.rulo.RuloTestKimyasalGirdi;
import com.emekboru.jpa.rulo.RuloTestKimyasalUretici;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizGozOlcuSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizKesim;
import com.emekboru.jpaman.AgirlikTestsSpecManager;
import com.emekboru.jpaman.BukmeTestsSpecManager;
import com.emekboru.jpaman.CekmeTestSpecManager;
import com.emekboru.jpaman.CentikDarbeTestsSpecManager;
import com.emekboru.jpaman.ChemicalRequirementManager;
import com.emekboru.jpaman.KaynakMacroTestSpecManager;
import com.emekboru.jpaman.KaynakSertlikTestSpecManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.TestAgirlikDusurmeSonucManager;
import com.emekboru.jpaman.TestBukmeSonucManager;
import com.emekboru.jpaman.TestCekmeSonucManager;
import com.emekboru.jpaman.TestCentikDarbeSonucManager;
import com.emekboru.jpaman.TestKaynakSonucManager;
import com.emekboru.jpaman.TestKimyasalAnalizSonucManager;
import com.emekboru.jpaman.coatingmaterials.KaplamaMalzemeKullanmaPeManager;
import com.emekboru.jpaman.kaplamamakinesi.KaplamaMakinesiPolietilenManager;
import com.emekboru.jpaman.rulo.RuloTestKimyasalGirdiManager;
import com.emekboru.jpaman.rulo.RuloTestKimyasalUreticiManager;
import com.emekboru.jpaman.sales.order.SalesItemManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizGozOlcuSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizKesimManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.ReportUtil;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "partilendirmeBean")
@ViewScoped
public class PartilendirmeBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	public SalesItem selectedSalesItem;
	public Pipe selectedSalesItemPipe;
	private String barkodNo = null;

	private List<Pipe> pipes = new ArrayList<Pipe>();
	private List<Pipe> selectedSorguPipes = new ArrayList<Pipe>();
	private List<Pipe> selectedUcBirPipes = new ArrayList<Pipe>();

	private BigDecimal binSayisi = new BigDecimal("1000");
	private BigDecimal besSayisi = new BigDecimal("5");
	private BigDecimal onSayisi = new BigDecimal("5");
	private BigDecimal onBesSayisi = new BigDecimal("15");
	private BigDecimal yirmiSayisi = new BigDecimal("20");
	private BigDecimal otuzSayisi = new BigDecimal("30");
	private BigDecimal altmisSayisi = new BigDecimal("60");

	private KaplamaMakinesiPolietilen selectedPe = new KaplamaMakinesiPolietilen();
	private Integer prmVardiya = null;
	private TestCekmeSonuc finalCekme = new TestCekmeSonuc();
	private BigDecimal minCVNBody = null;
	private BigDecimal ortalamaCVNBody = null;
	private BigDecimal minDWTT = null;
	private BigDecimal ortalamaDWTT = null;

	private List<Pipe> releaseNoteTarihleriList = new ArrayList<Pipe>();
	private List<Pipe> ucBirTarihleriList = new ArrayList<Pipe>();

	@Temporal(TemporalType.TIMESTAMP)
	private Date selectedReleaseNoteTarihiIlk;
	@Temporal(TemporalType.TIMESTAMP)
	private Date selectedReleaseNoteTarihiSon;

	@Temporal(TemporalType.TIMESTAMP)
	private Date selectedUcBirTarihiIlk;
	@Temporal(TemporalType.TIMESTAMP)
	private Date selectedUcBirTarihiSon;

	private List<TestTahribatsizGozOlcuSonuc> gozOlcuPipes = new ArrayList<TestTahribatsizGozOlcuSonuc>();
	private TestTahribatsizGozOlcuSonuc[] selectedGozOlcuPipes;

	private int partiSayisi = 0;

	private List<String> dokumler = new ArrayList<String>();
	private List<String> dokumlerC03 = new ArrayList<String>();
	private List<String> dokumlerF01 = new ArrayList<String>();
	private List<String> dokumlerF02 = new ArrayList<String>();
	private List<String> dokumlerF03 = new ArrayList<String>();
	private List<String> dokumlerF04 = new ArrayList<String>();
	private List<String> dokumlerF05 = new ArrayList<String>();
	private List<String> dokumlerF06 = new ArrayList<String>();

	private List<String> dokumlerZ01 = new ArrayList<String>();
	private List<String> dokumlerZ02 = new ArrayList<String>();
	private List<String> dokumlerZ03 = new ArrayList<String>();
	private List<String> dokumlerZ06 = new ArrayList<String>();
	private List<String> dokumlerZ07 = new ArrayList<String>();

	private List<String> dokumlerK01 = new ArrayList<String>();
	private List<String> dokumlerK02 = new ArrayList<String>();
	private List<String> dokumlerK03 = new ArrayList<String>();
	private List<String> dokumlerK04 = new ArrayList<String>();
	private List<String> dokumlerK05 = new ArrayList<String>();
	private List<String> dokumlerK08 = new ArrayList<String>();
	private List<String> dokumlerK09 = new ArrayList<String>();
	private List<String> dokumlerK10 = new ArrayList<String>();
	private List<String> dokumlerK11 = new ArrayList<String>();
	private List<String> dokumlerK12 = new ArrayList<String>();
	private List<String> dokumlerK13 = new ArrayList<String>();

	private List<String> dokumlerU01 = new ArrayList<String>();
	private List<String> dokumlerU02 = new ArrayList<String>();
	private List<String> dokumlerU03 = new ArrayList<String>();
	private List<String> dokumlerU04 = new ArrayList<String>();
	private List<String> dokumlerU05 = new ArrayList<String>();

	private List<String> dokumlerD01 = new ArrayList<String>();
	private List<String> dokumlerD02 = new ArrayList<String>();

	private List<String> dokumlerM01 = new ArrayList<String>();
	private List<String> dokumlerH01 = new ArrayList<String>();

	private List<Rulo> pipeRulos = new ArrayList<Rulo>();
	private dokumlerObject dokumlerObject = new dokumlerObject();

	@Temporal(TemporalType.TIMESTAMP)
	private Date releaseNoteTarihi;
	@Temporal(TemporalType.TIMESTAMP)
	private Date ucBirTarihi;

	private List<TestKimyasalAnalizSonuc> c03SonucList = new ArrayList<TestKimyasalAnalizSonuc>();
	private List<TestKimyasalAnalizSonuc> c03EksikSonucList = new ArrayList<TestKimyasalAnalizSonuc>();
	private String c03Siklik = null;
	private int c03SiklikNumber = 0;
	private int c03PartiSayisi = 0;

	private List<TestBukmeSonuc> f01SonucList = new ArrayList<TestBukmeSonuc>();
	private List<TestBukmeSonuc> f02SonucList = new ArrayList<TestBukmeSonuc>();
	private List<TestBukmeSonuc> f03SonucList = new ArrayList<TestBukmeSonuc>();
	private List<TestBukmeSonuc> f04SonucList = new ArrayList<TestBukmeSonuc>();
	private List<TestBukmeSonuc> f05SonucList = new ArrayList<TestBukmeSonuc>();
	private List<TestBukmeSonuc> f06SonucList = new ArrayList<TestBukmeSonuc>();
	private String f01Siklik = null;
	private String f02Siklik = null;
	private String f03Siklik = null;
	private String f04Siklik = null;
	private String f05Siklik = null;
	private String f06Siklik = null;
	private int f01SiklikNumber = 0;
	private int f02SiklikNumber = 0;
	private int f03SiklikNumber = 0;
	private int f04SiklikNumber = 0;
	private int f05SiklikNumber = 0;
	private int f06SiklikNumber = 0;
	private int f01PartiSayisi = 0;
	private int f02PartiSayisi = 0;
	private int f03PartiSayisi = 0;
	private int f04PartiSayisi = 0;
	private int f05PartiSayisi = 0;
	private int f06PartiSayisi = 0;

	private List<TestCekmeSonuc> z01SonucList = new ArrayList<TestCekmeSonuc>();
	private List<TestCekmeSonuc> z02SonucList = new ArrayList<TestCekmeSonuc>();
	private List<TestCekmeSonuc> z03SonucList = new ArrayList<TestCekmeSonuc>();
	private List<TestCekmeSonuc> z06SonucList = new ArrayList<TestCekmeSonuc>();
	private List<TestCekmeSonuc> z07SonucList = new ArrayList<TestCekmeSonuc>();
	private String z01Siklik = null;
	private String z02Siklik = null;
	private String z03Siklik = null;
	private String z06Siklik = null;
	private String z07Siklik = null;
	private int z01SiklikNumber = 0;
	private int z02SiklikNumber = 0;
	private int z03SiklikNumber = 0;
	private int z06SiklikNumber = 0;
	private int z07SiklikNumber = 0;
	private int z01PartiSayisi = 0;
	private int z02PartiSayisi = 0;
	private int z03PartiSayisi = 0;
	private int z06PartiSayisi = 0;
	private int z07PartiSayisi = 0;

	private List<TestCentikDarbeSonuc> u01SonucList = new ArrayList<TestCentikDarbeSonuc>();
	private List<TestCentikDarbeSonuc> u02SonucList = new ArrayList<TestCentikDarbeSonuc>();
	private List<TestCentikDarbeSonuc> u03SonucList = new ArrayList<TestCentikDarbeSonuc>();
	private List<TestCentikDarbeSonuc> u04SonucList = new ArrayList<TestCentikDarbeSonuc>();
	private List<TestCentikDarbeSonuc> u05SonucList = new ArrayList<TestCentikDarbeSonuc>();
	private List<TestCentikDarbeSonuc> u08SonucList = new ArrayList<TestCentikDarbeSonuc>();
	private String u01Siklik = null;
	private String u02Siklik = null;
	private String u03Siklik = null;
	private String u04Siklik = null;
	private String u05Siklik = null;
	private String u08Siklik = null;
	private int u01SiklikNumber = 0;
	private int u02SiklikNumber = 0;
	private int u03SiklikNumber = 0;
	private int u04SiklikNumber = 0;
	private int u05SiklikNumber = 0;
	private int u08SiklikNumber = 0;
	private int u01PartiSayisi = 0;
	private int u02PartiSayisi = 0;
	private int u03PartiSayisi = 0;
	private int u04PartiSayisi = 0;
	private int u05PartiSayisi = 0;
	private int u08PartiSayisi = 0;

	private List<TestAgirlikDusurmeSonuc> d01SonucList = new ArrayList<TestAgirlikDusurmeSonuc>();
	private String d01Siklik = null;
	private int d01SiklikNumber = 0;
	private int d01PartiSayisi = 0;

	private List<TestAgirlikDusurmeSonuc> d02SonucList = new ArrayList<TestAgirlikDusurmeSonuc>();
	private String d02Siklik = null;
	private int d02SiklikNumber = 0;
	private int d02PartiSayisi = 0;

	private List<TestKaynakSonuc> m01SonucList = new ArrayList<TestKaynakSonuc>();
	private String m01Siklik = null;
	private int m01SiklikNumber = 0;
	private int m01PartiSayisi = 0;

	private List<TestKaynakSonuc> h01SonucList = new ArrayList<TestKaynakSonuc>();
	private String h01Siklik = null;
	private int h01SiklikNumber = 0;
	private int h01PartiSayisi = 0;

	private List<TestCentikDarbeSonuc> k01SonucList = new ArrayList<TestCentikDarbeSonuc>();
	private List<TestCentikDarbeSonuc> k02SonucList = new ArrayList<TestCentikDarbeSonuc>();
	private List<TestCentikDarbeSonuc> k03SonucList = new ArrayList<TestCentikDarbeSonuc>();
	private List<TestCentikDarbeSonuc> k04SonucList = new ArrayList<TestCentikDarbeSonuc>();
	private List<TestCentikDarbeSonuc> k05SonucList = new ArrayList<TestCentikDarbeSonuc>();
	private List<TestCentikDarbeSonuc> k08SonucList = new ArrayList<TestCentikDarbeSonuc>();
	private List<TestCentikDarbeSonuc> k09SonucList = new ArrayList<TestCentikDarbeSonuc>();
	private List<TestCentikDarbeSonuc> k10SonucList = new ArrayList<TestCentikDarbeSonuc>();
	private List<TestCentikDarbeSonuc> k11SonucList = new ArrayList<TestCentikDarbeSonuc>();
	private List<TestCentikDarbeSonuc> k12SonucList = new ArrayList<TestCentikDarbeSonuc>();
	private List<TestCentikDarbeSonuc> k13SonucList = new ArrayList<TestCentikDarbeSonuc>();
	private String k01Siklik = null;
	private String k02Siklik = null;
	private String k03Siklik = null;
	private String k04Siklik = null;
	private String k05Siklik = null;
	private String k08Siklik = null;
	private String k09Siklik = null;
	private String k10Siklik = null;
	private String k11Siklik = null;
	private String k12Siklik = null;
	private String k13Siklik = null;
	private int k01SiklikNumber = 0;
	private int k02SiklikNumber = 0;
	private int k03SiklikNumber = 0;
	private int k04SiklikNumber = 0;
	private int k05SiklikNumber = 0;
	private int k08SiklikNumber = 0;
	private int k09SiklikNumber = 0;
	private int k10SiklikNumber = 0;
	private int k11SiklikNumber = 0;
	private int k12SiklikNumber = 0;
	private int k13SiklikNumber = 0;
	private int k01PartiSayisi = 0;
	private int k02PartiSayisi = 0;
	private int k03PartiSayisi = 0;
	private int k04PartiSayisi = 0;
	private int k05PartiSayisi = 0;
	private int k08PartiSayisi = 0;
	private int k09PartiSayisi = 0;
	private int k10PartiSayisi = 0;
	private int k11PartiSayisi = 0;
	private int k12PartiSayisi = 0;
	private int k13PartiSayisi = 0;

	private List<releaseObject> releaseObjects = new ArrayList<releaseObject>();
	private List<shippingListObject> shippingListObjects = new ArrayList<shippingListObject>();
	private List<chemicalObject> chemicalObjects = new ArrayList<chemicalObject>();

	private String kontrolDokum = "BOŞ";

	public PartilendirmeBean() {

	}

	public class zTestsNominals {
		public String activityNumber = null;
		public String location = null;
		public String direction = null;
		public String testTemp = null;
		public String testNorm = null;
		public String batch = null;
		public String re = null;
		public String rm = null;
		public String rerm = null;
		public String elong = null;

		/**
		 * @return the activityNumber
		 */
		public String getActivityNumber() {
			return activityNumber;
		}

		/**
		 * @param activityNumber
		 *            the activityNumber to set
		 */
		public void setActivityNumber(String activityNumber) {
			this.activityNumber = activityNumber;
		}

		/**
		 * @return the location
		 */
		public String getLocation() {
			return location;
		}

		/**
		 * @param location
		 *            the location to set
		 */
		public void setLocation(String location) {
			this.location = location;
		}

		/**
		 * @return the direction
		 */
		public String getDirection() {
			return direction;
		}

		/**
		 * @param direction
		 *            the direction to set
		 */
		public void setDirection(String direction) {
			this.direction = direction;
		}

		/**
		 * @return the testTemp
		 */
		public String getTestTemp() {
			return testTemp;
		}

		/**
		 * @param testTemp
		 *            the testTemp to set
		 */
		public void setTestTemp(String testTemp) {
			this.testTemp = testTemp;
		}

		/**
		 * @return the testNorm
		 */
		public String getTestNorm() {
			return testNorm;
		}

		/**
		 * @param testNorm
		 *            the testNorm to set
		 */
		public void setTestNorm(String testNorm) {
			this.testNorm = testNorm;
		}

		/**
		 * @return the batch
		 */
		public String getBatch() {
			return batch;
		}

		/**
		 * @param batch
		 *            the batch to set
		 */
		public void setBatch(String batch) {
			this.batch = batch;
		}

		/**
		 * @return the re
		 */
		public String getRe() {
			return re;
		}

		/**
		 * @param re
		 *            the re to set
		 */
		public void setRe(String re) {
			this.re = re;
		}

		/**
		 * @return the rm
		 */
		public String getRm() {
			return rm;
		}

		/**
		 * @param rm
		 *            the rm to set
		 */
		public void setRm(String rm) {
			this.rm = rm;
		}

		/**
		 * @return the rerm
		 */
		public String getRerm() {
			return rerm;
		}

		/**
		 * @param rerm
		 *            the rerm to set
		 */
		public void setRerm(String rerm) {
			this.rerm = rerm;
		}

		/**
		 * @return the elong
		 */
		public String getElong() {
			return elong;
		}

		/**
		 * @param elong
		 *            the elong to set
		 */
		public void setElong(String elong) {
			this.elong = elong;
		}
	}

	public class kTestsNominals {
		public String activityNumber = null;
		public String location = null;
		public String direction = null;
		public String testTemp = null;
		public String testNorm = null;
		public String batch = null;
		public String type = null;
		public String size = null;
		public String minEnergy = null;
		public String maxEnergy = null;

		/**
		 * @return the activityNumber
		 */
		public String getActivityNumber() {
			return activityNumber;
		}

		/**
		 * @param activityNumber
		 *            the activityNumber to set
		 */
		public void setActivityNumber(String activityNumber) {
			this.activityNumber = activityNumber;
		}

		/**
		 * @return the location
		 */
		public String getLocation() {
			return location;
		}

		/**
		 * @param location
		 *            the location to set
		 */
		public void setLocation(String location) {
			this.location = location;
		}

		/**
		 * @return the direction
		 */
		public String getDirection() {
			return direction;
		}

		/**
		 * @param direction
		 *            the direction to set
		 */
		public void setDirection(String direction) {
			this.direction = direction;
		}

		/**
		 * @return the testTemp
		 */
		public String getTestTemp() {
			return testTemp;
		}

		/**
		 * @param testTemp
		 *            the testTemp to set
		 */
		public void setTestTemp(String testTemp) {
			this.testTemp = testTemp;
		}

		/**
		 * @return the testNorm
		 */
		public String getTestNorm() {
			return testNorm;
		}

		/**
		 * @param testNorm
		 *            the testNorm to set
		 */
		public void setTestNorm(String testNorm) {
			this.testNorm = testNorm;
		}

		/**
		 * @return the batch
		 */
		public String getBatch() {
			return batch;
		}

		/**
		 * @param batch
		 *            the batch to set
		 */
		public void setBatch(String batch) {
			this.batch = batch;
		}

		/**
		 * @return the type
		 */
		public String getType() {
			return type;
		}

		/**
		 * @param type
		 *            the type to set
		 */
		public void setType(String type) {
			this.type = type;
		}

		/**
		 * @return the size
		 */
		public String getSize() {
			return size;
		}

		/**
		 * @param size
		 *            the size to set
		 */
		public void setSize(String size) {
			this.size = size;
		}

		/**
		 * @return the minEnergy
		 */
		public String getMinEnergy() {
			return minEnergy;
		}

		/**
		 * @param minEnergy
		 *            the minEnergy to set
		 */
		public void setMinEnergy(String minEnergy) {
			this.minEnergy = minEnergy;
		}

		/**
		 * @return the maxEnergy
		 */
		public String getMaxEnergy() {
			return maxEnergy;
		}

		/**
		 * @param maxEnergy
		 *            the maxEnergy to set
		 */
		public void setMaxEnergy(String maxEnergy) {
			this.maxEnergy = maxEnergy;
		}
	}

	public class fTestsNominals {
		public String activityNumber = null;
		public String location = null;
		public String direction = null;
		public String testNorm = null;
		public String batch = null;
		public String jig = null;
		public String angle = null;

		/**
		 * @return the activityNumber
		 */
		public String getActivityNumber() {
			return activityNumber;
		}

		/**
		 * @param activityNumber
		 *            the activityNumber to set
		 */
		public void setActivityNumber(String activityNumber) {
			this.activityNumber = activityNumber;
		}

		/**
		 * @return the location
		 */
		public String getLocation() {
			return location;
		}

		/**
		 * @param location
		 *            the location to set
		 */
		public void setLocation(String location) {
			this.location = location;
		}

		/**
		 * @return the direction
		 */
		public String getDirection() {
			return direction;
		}

		/**
		 * @param direction
		 *            the direction to set
		 */
		public void setDirection(String direction) {
			this.direction = direction;
		}

		/**
		 * @return the testNorm
		 */
		public String getTestNorm() {
			return testNorm;
		}

		/**
		 * @param testNorm
		 *            the testNorm to set
		 */
		public void setTestNorm(String testNorm) {
			this.testNorm = testNorm;
		}

		/**
		 * @return the batch
		 */
		public String getBatch() {
			return batch;
		}

		/**
		 * @param batch
		 *            the batch to set
		 */
		public void setBatch(String batch) {
			this.batch = batch;
		}

		/**
		 * @return the jig
		 */
		public String getJig() {
			return jig;
		}

		/**
		 * @param jig
		 *            the jig to set
		 */
		public void setJig(String jig) {
			this.jig = jig;
		}

		/**
		 * @return the angle
		 */
		public String getAngle() {
			return angle;
		}

		/**
		 * @param angle
		 *            the angle to set
		 */
		public void setAngle(String angle) {
			this.angle = angle;
		}
	}

	public class mTestsNominals {
		public String activityNumber = null;
		public String location = null;
		public String direction = null;
		public String testNorm = null;
		public String penetrasyon = null;

		/**
		 * @return the activityNumber
		 */
		public String getActivityNumber() {
			return activityNumber;
		}

		/**
		 * @param activityNumber
		 *            the activityNumber to set
		 */
		public void setActivityNumber(String activityNumber) {
			this.activityNumber = activityNumber;
		}

		/**
		 * @return the location
		 */
		public String getLocation() {
			return location;
		}

		/**
		 * @param location
		 *            the location to set
		 */
		public void setLocation(String location) {
			this.location = location;
		}

		/**
		 * @return the direction
		 */
		public String getDirection() {
			return direction;
		}

		/**
		 * @param direction
		 *            the direction to set
		 */
		public void setDirection(String direction) {
			this.direction = direction;
		}

		/**
		 * @return the testNorm
		 */
		public String getTestNorm() {
			return testNorm;
		}

		/**
		 * @param testNorm
		 *            the testNorm to set
		 */
		public void setTestNorm(String testNorm) {
			this.testNorm = testNorm;
		}

		/**
		 * @return the penetrasyon
		 */
		public String getPenetrasyon() {
			return penetrasyon;
		}

		/**
		 * @param penetrasyon
		 *            the penetrasyon to set
		 */
		public void setPenetrasyon(String penetrasyon) {
			this.penetrasyon = penetrasyon;
		}
	}

	public class hTestsNominals {
		public String activityNumber = null;
		public String location = null;
		public String direction = null;
		public String testNorm = null;
		public String minHardness = null;

		/**
		 * @return the activityNumber
		 */
		public String getActivityNumber() {
			return activityNumber;
		}

		/**
		 * @param activityNumber
		 *            the activityNumber to set
		 */
		public void setActivityNumber(String activityNumber) {
			this.activityNumber = activityNumber;
		}

		/**
		 * @return the location
		 */
		public String getLocation() {
			return location;
		}

		/**
		 * @param location
		 *            the location to set
		 */
		public void setLocation(String location) {
			this.location = location;
		}

		/**
		 * @return the direction
		 */
		public String getDirection() {
			return direction;
		}

		/**
		 * @param direction
		 *            the direction to set
		 */
		public void setDirection(String direction) {
			this.direction = direction;
		}

		/**
		 * @return the testNorm
		 */
		public String getTestNorm() {
			return testNorm;
		}

		/**
		 * @param testNorm
		 *            the testNorm to set
		 */
		public void setTestNorm(String testNorm) {
			this.testNorm = testNorm;
		}

		/**
		 * @return the minHardness
		 */
		public String getMinHardness() {
			return minHardness;
		}

		/**
		 * @param minHardness
		 *            the minHardness to set
		 */
		public void setMinHardness(String minHardness) {
			this.minHardness = minHardness;
		}
	}

	public class dTestsNominals {
		public String activityNumber = null;
		public String location = null;
		public String direction = null;
		public String testNorm = null;
		public String testTemp = null;
		public String size = null;
		public String minAverageArea = null;
		public String minIndArea = null;

		/**
		 * @return the activityNumber
		 */
		public String getActivityNumber() {
			return activityNumber;
		}

		/**
		 * @param activityNumber
		 *            the activityNumber to set
		 */
		public void setActivityNumber(String activityNumber) {
			this.activityNumber = activityNumber;
		}

		/**
		 * @return the location
		 */
		public String getLocation() {
			return location;
		}

		/**
		 * @param location
		 *            the location to set
		 */
		public void setLocation(String location) {
			this.location = location;
		}

		/**
		 * @return the direction
		 */
		public String getDirection() {
			return direction;
		}

		/**
		 * @param direction
		 *            the direction to set
		 */
		public void setDirection(String direction) {
			this.direction = direction;
		}

		/**
		 * @return the testNorm
		 */
		public String getTestNorm() {
			return testNorm;
		}

		/**
		 * @param testNorm
		 *            the testNorm to set
		 */
		public void setTestNorm(String testNorm) {
			this.testNorm = testNorm;
		}

		/**
		 * @return the testTemp
		 */
		public String getTestTemp() {
			return testTemp;
		}

		/**
		 * @param testTemp
		 *            the testTemp to set
		 */
		public void setTestTemp(String testTemp) {
			this.testTemp = testTemp;
		}

		/**
		 * @return the size
		 */
		public String getSize() {
			return size;
		}

		/**
		 * @param size
		 *            the size to set
		 */
		public void setSize(String size) {
			this.size = size;
		}

		/**
		 * @return the minAverageArea
		 */
		public String getMinAverageArea() {
			return minAverageArea;
		}

		/**
		 * @param minAverageArea
		 *            the minAverageArea to set
		 */
		public void setMinAverageArea(String minAverageArea) {
			this.minAverageArea = minAverageArea;
		}

		/**
		 * @return the minIndArea
		 */
		public String getMinIndArea() {
			return minIndArea;
		}

		/**
		 * @param minIndArea
		 *            the minIndArea to set
		 */
		public void setMinIndArea(String minIndArea) {
			this.minIndArea = minIndArea;
		}
	}

	public void fillTestListFromBarkodPartilendirme(String prmBarkod) {

		// barkodNo ya göre sipariş bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " SİPARİŞ SİSTEMDE YOKTUR!", null));
			return;
		} else {

			selectedSalesItemPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			selectedSalesItem = selectedSalesItemPipe.getSalesItem();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(selectedSalesItem
					.getProposal().getSalesOrder().getOrderNo()
					+ "/"
					+ selectedSalesItem.getItemNo()
					+ " - "
					+ selectedSalesItem.getProposal().getCustomer()
							.getShortName() + " NUMARALI SİPARİŞ SEÇİLMİŞTİR!"));

			startUp();
		}

	}

	public void fillTestListFromBarkodRelease(String prmBarkod) {

		// barkodNo ya göre sipariş bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " SİPARİŞ SİSTEMDE YOKTUR!", null));
			return;
		} else {

			selectedSalesItemPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			selectedSalesItem = selectedSalesItemPipe.getSalesItem();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(selectedSalesItem
					.getProposal().getSalesOrder().getOrderNo()
					+ "/"
					+ selectedSalesItem.getItemNo()
					+ " - "
					+ selectedSalesItem.getProposal().getCustomer()
							.getShortName() + " NUMARALI SİPARİŞ SEÇİLMİŞTİR!"));

			PipeManager manager = new PipeManager();
			// pipes = manager.findByItemId(selectedSalesItem.getItemId());

			// releasenote sayfasındaki release listesini doldurur
			releaseNoteTarihleriList = manager
					.findReleaseCountByItemId(selectedSalesItem.getItemId());
			// releasenote sayfasındaki 3.1 listesini doldurur
			ucBirTarihleriList = manager
					.findUcBirCountByItemId(selectedSalesItem.getItemId());
		}

	}

	// releasenote sayfasındaki Üretilmiş borular listesini dolurur
	public void gozOlcuBorulari() {

		TestTahribatsizGozOlcuSonucManager gozManager = new TestTahribatsizGozOlcuSonucManager();
		gozOlcuPipes = gozManager.findByItemIdSonuc(selectedSalesItem
				.getItemId());

	}

	public void startUp() {

		clear();

		PipeManager manager = new PipeManager();
		pipes = manager.findByItemId(selectedSalesItem.getItemId());

		// releaseNoteTarihleriList = manager
		// .findReleaseCountByItemId(selectedSalesItem.getItemId());
		// ucBirTarihleriList = manager.findUcBirCountByItemId(selectedSalesItem
		// .getItemId());

		// TestTahribatsizGozOlcuSonucManager gozManager = new
		// TestTahribatsizGozOlcuSonucManager();
		// gozOlcuPipes = gozManager.findByItemIdSonuc(selectedSalesItem
		// .getItemId());

		dokumBul(pipes);

		siklikBul();
		kimyasalTest();
		bukmeTest();
		cekmeTest();
		centikTest();
		agirlikTest();
		kaynakTest();
	}

	public void clear() {

		pipes.clear();
		partiSayisi = 0;

		c03SonucList.clear();
		c03EksikSonucList.clear();
		c03Siklik = null;
		c03SiklikNumber = 0;
		c03PartiSayisi = 0;

		f01SonucList.clear();
		f02SonucList.clear();
		f03SonucList.clear();
		f04SonucList.clear();
		f05SonucList.clear();
		f06SonucList.clear();
		f01Siklik = null;
		f02Siklik = null;
		f03Siklik = null;
		f04Siklik = null;
		f05Siklik = null;
		f06Siklik = null;
		f01SiklikNumber = 0;
		f02SiklikNumber = 0;
		f03SiklikNumber = 0;
		f04SiklikNumber = 0;
		f05SiklikNumber = 0;
		f06SiklikNumber = 0;
		f01PartiSayisi = 0;
		f02PartiSayisi = 0;
		f03PartiSayisi = 0;
		f04PartiSayisi = 0;
		f05PartiSayisi = 0;
		f06PartiSayisi = 0;

		z01SonucList.clear();
		z02SonucList.clear();
		z03SonucList.clear();
		z06SonucList.clear();
		z07SonucList.clear();
		z01Siklik = null;
		z02Siklik = null;
		z03Siklik = null;
		z06Siklik = null;
		z07Siklik = null;
		z01SiklikNumber = 0;
		z02SiklikNumber = 0;
		z03SiklikNumber = 0;
		z06SiklikNumber = 0;
		z07SiklikNumber = 0;
		z01PartiSayisi = 0;
		z02PartiSayisi = 0;
		z03PartiSayisi = 0;
		z06PartiSayisi = 0;
		z07PartiSayisi = 0;

		u01SonucList = new ArrayList<TestCentikDarbeSonuc>();
		u02SonucList = new ArrayList<TestCentikDarbeSonuc>();
		u03SonucList = new ArrayList<TestCentikDarbeSonuc>();
		u04SonucList = new ArrayList<TestCentikDarbeSonuc>();
		u05SonucList = new ArrayList<TestCentikDarbeSonuc>();
		u08SonucList = new ArrayList<TestCentikDarbeSonuc>();
		u01Siklik = null;
		u02Siklik = null;
		u03Siklik = null;
		u04Siklik = null;
		u05Siklik = null;
		u08Siklik = null;
		u01SiklikNumber = 0;
		u02SiklikNumber = 0;
		u03SiklikNumber = 0;
		u04SiklikNumber = 0;
		u05SiklikNumber = 0;
		u08SiklikNumber = 0;
		u01PartiSayisi = 0;
		u02PartiSayisi = 0;
		u03PartiSayisi = 0;
		u04PartiSayisi = 0;
		u05PartiSayisi = 0;
		u08PartiSayisi = 0;

		d01SonucList = new ArrayList<TestAgirlikDusurmeSonuc>();
		d01Siklik = null;
		d01SiklikNumber = 0;
		d01PartiSayisi = 0;

		d02SonucList = new ArrayList<TestAgirlikDusurmeSonuc>();
		d02Siklik = null;
		d02SiklikNumber = 0;
		d02PartiSayisi = 0;

		m01SonucList = new ArrayList<TestKaynakSonuc>();
		m01Siklik = null;
		m01SiklikNumber = 0;
		m01PartiSayisi = 0;

		h01SonucList = new ArrayList<TestKaynakSonuc>();
		h01Siklik = null;
		h01SiklikNumber = 0;
		h01PartiSayisi = 0;

		k01SonucList = new ArrayList<TestCentikDarbeSonuc>();
		k02SonucList = new ArrayList<TestCentikDarbeSonuc>();
		k03SonucList = new ArrayList<TestCentikDarbeSonuc>();
		k04SonucList = new ArrayList<TestCentikDarbeSonuc>();
		k05SonucList = new ArrayList<TestCentikDarbeSonuc>();
		k08SonucList = new ArrayList<TestCentikDarbeSonuc>();
		k09SonucList = new ArrayList<TestCentikDarbeSonuc>();
		k10SonucList = new ArrayList<TestCentikDarbeSonuc>();
		k11SonucList = new ArrayList<TestCentikDarbeSonuc>();
		k12SonucList = new ArrayList<TestCentikDarbeSonuc>();
		k13SonucList = new ArrayList<TestCentikDarbeSonuc>();
		k01Siklik = null;
		k02Siklik = null;
		k03Siklik = null;
		k04Siklik = null;
		k05Siklik = null;
		k08Siklik = null;
		k09Siklik = null;
		k10Siklik = null;
		k11Siklik = null;
		k12Siklik = null;
		k13Siklik = null;
		k01SiklikNumber = 0;
		k02SiklikNumber = 0;
		k03SiklikNumber = 0;
		k04SiklikNumber = 0;
		k05SiklikNumber = 0;
		k08SiklikNumber = 0;
		k09SiklikNumber = 0;
		k10SiklikNumber = 0;
		k11SiklikNumber = 0;
		k12SiklikNumber = 0;
		k13SiklikNumber = 0;
		k01PartiSayisi = 0;
		k02PartiSayisi = 0;
		k03PartiSayisi = 0;
		k04PartiSayisi = 0;
		k05PartiSayisi = 0;
		k08PartiSayisi = 0;
		k09PartiSayisi = 0;
		k10PartiSayisi = 0;
		k11PartiSayisi = 0;
		k12PartiSayisi = 0;
		k13PartiSayisi = 0;
	}

	@SuppressWarnings("static-access")
	public void kimyasalTest() {

		TestKimyasalAnalizSonucManager kimyasalManager = new TestKimyasalAnalizSonucManager();
		c03SonucList = kimyasalManager.getBySalesItem(selectedSalesItem);

		dokumlerC03 = dokumler;
		for (int i = 0; i < c03SonucList.size(); i++) {
			for (int j = 0; j < dokumlerC03.size(); j++) {
				if (c03SonucList.get(i).getPipe().getRuloPipeLink().getRulo()
						.getRuloDokumNo().equals(dokumlerC03.get(j))) {
					dokumlerC03.remove(j);
				}
			}
		}
	}

	@SuppressWarnings("static-access")
	public void bukmeTest() {

		TestBukmeSonucManager bukmeManager = new TestBukmeSonucManager();
		f01SonucList = bukmeManager.getBySalesItem(selectedSalesItem, 1018);
		f02SonucList = bukmeManager.getBySalesItem(selectedSalesItem, 1019);
		f03SonucList = bukmeManager.getBySalesItem(selectedSalesItem, 1020);
		f04SonucList = bukmeManager.getBySalesItem(selectedSalesItem, 1021);
		f05SonucList = bukmeManager.getBySalesItem(selectedSalesItem, 1022);
		f06SonucList = bukmeManager.getBySalesItem(selectedSalesItem, 1023);

		dokumlerF01 = eksikDokumBulFTest(dokumlerF01, f01SonucList);
		dokumlerF02 = eksikDokumBulFTest(dokumlerF02, f02SonucList);
		dokumlerF03 = eksikDokumBulFTest(dokumlerF03, f03SonucList);
		dokumlerF04 = eksikDokumBulFTest(dokumlerF04, f04SonucList);
		dokumlerF05 = eksikDokumBulFTest(dokumlerF05, f05SonucList);
		dokumlerF06 = eksikDokumBulFTest(dokumlerF06, f06SonucList);

	}

	@SuppressWarnings("static-access")
	public void cekmeTest() {

		TestCekmeSonucManager cekmeManager = new TestCekmeSonucManager();
		z01SonucList = cekmeManager.getBySalesItemGlobalId(selectedSalesItem,
				1013);
		z02SonucList = cekmeManager.getBySalesItemGlobalId(selectedSalesItem,
				1014);
		z03SonucList = cekmeManager.getBySalesItemGlobalId(selectedSalesItem,
				1015);
		z06SonucList = cekmeManager.getBySalesItemGlobalId(selectedSalesItem,
				1016);
		z07SonucList = cekmeManager.getBySalesItemGlobalId(selectedSalesItem,
				1017);

		dokumlerZ01 = eksikDokumBulZTest(dokumlerZ01, z01SonucList);
		dokumlerZ02 = eksikDokumBulZTest(dokumlerZ02, z02SonucList);
		dokumlerZ03 = eksikDokumBulZTest(dokumlerZ03, z03SonucList);
		dokumlerZ06 = eksikDokumBulZTest(dokumlerZ06, z06SonucList);
		dokumlerZ07 = eksikDokumBulZTest(dokumlerZ07, z07SonucList);
	}

	@SuppressWarnings("static-access")
	public void centikTest() {

		TestCentikDarbeSonucManager centikManager = new TestCentikDarbeSonucManager();
		k01SonucList = centikManager.getBySalesItemGlobalId(selectedSalesItem,
				1024);
		k02SonucList = centikManager.getBySalesItemGlobalId(selectedSalesItem,
				1025);
		k03SonucList = centikManager.getBySalesItemGlobalId(selectedSalesItem,
				1026);
		k04SonucList = centikManager.getBySalesItemGlobalId(selectedSalesItem,
				1027);
		k05SonucList = centikManager.getBySalesItemGlobalId(selectedSalesItem,
				1028);
		k08SonucList = centikManager.getBySalesItemGlobalId(selectedSalesItem,
				1040);
		k09SonucList = centikManager.getBySalesItemGlobalId(selectedSalesItem,
				0);
		k10SonucList = centikManager.getBySalesItemGlobalId(selectedSalesItem,
				0);
		k11SonucList = centikManager.getBySalesItemGlobalId(selectedSalesItem,
				0);
		k12SonucList = centikManager.getBySalesItemGlobalId(selectedSalesItem,
				0);
		k13SonucList = centikManager.getBySalesItemGlobalId(selectedSalesItem,
				0);

		u01SonucList = centikManager.getBySalesItemGlobalId(selectedSalesItem,
				1033);
		u02SonucList = centikManager.getBySalesItemGlobalId(selectedSalesItem,
				1034);
		u03SonucList = centikManager.getBySalesItemGlobalId(selectedSalesItem,
				1035);
		u04SonucList = centikManager.getBySalesItemGlobalId(selectedSalesItem,
				1036);
		u05SonucList = centikManager.getBySalesItemGlobalId(selectedSalesItem,
				1037);

		dokumlerK01 = eksikDokumBulKTest(dokumlerK01, k01SonucList);
		dokumlerK02 = eksikDokumBulKTest(dokumlerK02, k02SonucList);
		dokumlerK03 = eksikDokumBulKTest(dokumlerK03, k03SonucList);
		dokumlerK04 = eksikDokumBulKTest(dokumlerK04, k04SonucList);
		dokumlerK05 = eksikDokumBulKTest(dokumlerK05, k05SonucList);
		dokumlerK08 = eksikDokumBulKTest(dokumlerK08, k08SonucList);
		dokumlerK09 = eksikDokumBulKTest(dokumlerK09, k09SonucList);
		dokumlerK10 = eksikDokumBulKTest(dokumlerK10, k10SonucList);
		dokumlerK11 = eksikDokumBulKTest(dokumlerK11, k11SonucList);
		dokumlerK12 = eksikDokumBulKTest(dokumlerK12, k12SonucList);
		dokumlerK13 = eksikDokumBulKTest(dokumlerK13, k13SonucList);

		dokumlerU01 = eksikDokumBulKTest(dokumlerU01, u01SonucList);
		dokumlerU02 = eksikDokumBulKTest(dokumlerU02, u02SonucList);
		dokumlerU03 = eksikDokumBulKTest(dokumlerU03, u03SonucList);
		dokumlerU04 = eksikDokumBulKTest(dokumlerU04, u04SonucList);
		dokumlerU05 = eksikDokumBulKTest(dokumlerU05, u05SonucList);

	}

	@SuppressWarnings("static-access")
	public void agirlikTest() {

		TestAgirlikDusurmeSonucManager agirlikManager = new TestAgirlikDusurmeSonucManager();
		d01SonucList = agirlikManager.getBySalesItemGlobalId(selectedSalesItem,
				1029);
		d02SonucList = agirlikManager.getBySalesItemGlobalId(selectedSalesItem,
				1030);

		dokumlerD01 = eksikDokumBulDTest(dokumlerD01, d01SonucList);
		dokumlerD02 = eksikDokumBulDTest(dokumlerD02, d02SonucList);

	}

	@SuppressWarnings("static-access")
	public void kaynakTest() {

		TestKaynakSonucManager kaynakManager = new TestKaynakSonucManager();
		m01SonucList = kaynakManager.getBySalesItem(selectedSalesItem, 1031);
		h01SonucList = kaynakManager.getBySalesItem(selectedSalesItem, 1032);

		dokumlerM01 = eksikDokumBulMTest(dokumlerM01, m01SonucList);
		dokumlerH01 = eksikDokumBulMTest(dokumlerM01, h01SonucList);
	}

	public List<String> eksikDokumBulFTest(List<String> prmDokumler,
			List<TestBukmeSonuc> prmList) {
		prmDokumler = new ArrayList<String>();
		if (prmList.size() > 0) {
			prmDokumler = dokumler;
			for (int i = 0; i < prmList.size(); i++) {
				for (int j = 0; j < prmDokumler.size(); j++) {
					if (prmList.get(i).getPipe().getRuloPipeLink().getRulo()
							.getRuloDokumNo().equals(prmDokumler.get(j))) {
						prmDokumler.remove(j);
					}
				}
			}
		}
		return prmDokumler;
	}

	public List<String> eksikDokumBulZTest(List<String> prmDokumler,
			List<TestCekmeSonuc> prmList) {
		prmDokumler = new ArrayList<String>();
		if (prmList.size() > 0) {
			prmDokumler = dokumler;
			for (int i = 0; i < prmList.size(); i++) {
				for (int j = 0; j < prmDokumler.size(); j++) {
					if (prmList.get(i).getPipe().getRuloPipeLink().getRulo()
							.getRuloDokumNo().equals(prmDokumler.get(j))) {
						prmDokumler.remove(j);
					}
				}
			}
		}
		return prmDokumler;
	}

	public List<String> eksikDokumBulKTest(List<String> prmDokumler,
			List<TestCentikDarbeSonuc> prmList) {
		prmDokumler = new ArrayList<String>();
		if (prmList.size() > 0) {
			prmDokumler = dokumler;
			for (int i = 0; i < prmList.size(); i++) {
				for (int j = 0; j < prmDokumler.size(); j++) {
					if (prmList.get(i).getPipe().getRuloPipeLink().getRulo()
							.getRuloDokumNo().equals(prmDokumler.get(j))) {
						prmDokumler.remove(j);
					}
				}
			}
		}
		return prmDokumler;
	}

	public List<String> eksikDokumBulDTest(List<String> prmDokumler,
			List<TestAgirlikDusurmeSonuc> prmList) {
		prmDokumler = new ArrayList<String>();
		if (prmList.size() > 0) {
			prmDokumler = dokumler;
			for (int i = 0; i < prmList.size(); i++) {
				for (int j = 0; j < prmDokumler.size(); j++) {
					if (prmList.get(i).getPipe().getRuloPipeLink().getRulo()
							.getRuloDokumNo().equals(prmDokumler.get(j))) {
						prmDokumler.remove(j);
					}
				}
			}
		}
		return prmDokumler;
	}

	public List<String> eksikDokumBulMTest(List<String> prmDokumler,
			List<TestKaynakSonuc> prmList) {
		prmDokumler = new ArrayList<String>();
		if (prmList.size() > 0) {
			prmDokumler = dokumler;
			for (int i = 0; i < prmList.size(); i++) {
				for (int j = 0; j < prmDokumler.size(); j++) {
					if (prmList.get(i).getPipe().getRuloPipeLink().getRulo()
							.getRuloDokumNo().equals(prmDokumler.get(j))) {
						prmDokumler.remove(j);
					}
				}
			}
		}
		return prmDokumler;
	}

	@SuppressWarnings("unused")
	public void partiNoHesapla(int prmSiklik) {
		// 2/Döküm
		if (prmSiklik == 18) {
			partiSayisi = dokumler.size() * 2;
		}// 1/Döküm
		else if (prmSiklik == 15) {
			partiSayisi = dokumler.size();
		}// 1/50 Boru/Döküm
		else if (prmSiklik == 16) {
			partiSayisi = dokumler.size();
		}// 2/50 Boru/Döküm
		else if (false) {
			partiSayisi = dokumler.size() * 2;
		}// 3/10 Döküm
		else if (prmSiklik == 1046) {
			partiSayisi = dokumler.size() / 10 * 3;
		}// 2set /50 Boru/Döküm
		else if (false) {
			partiSayisi = dokumler.size() * 2;
		}
	}

	public void dokumdekiBoruSayisi() {
	}

	public class releaseObject {

		public BigDecimal akmaDayanci = null;
		public BigDecimal cekmeDayanci = null;
		public BigDecimal uzama = null;
		public BigDecimal centikEnerjiMin = null;
		public BigDecimal centikEnerjiOrtalama = null;
		public String itemNo = null;
		public String batchNo = null;
		public String diameter = null;
		public String thickness = null;
		public BigDecimal minKesmeAlani = null;
		public BigDecimal ortalamaKesmeAlani = null;
		public String aUcuIcCapCevresel = null;
		public String bUcuIcCapCevresel = null;
		public String boruBoyu = null;
		public String boruAgirligi = null;

		/**
		 * @return the akmaDayanci
		 */
		public BigDecimal getAkmaDayanci() {
			return akmaDayanci;
		}

		/**
		 * @param akmaDayanci
		 *            the akmaDayanci to set
		 */
		public void setAkmaDayanci(BigDecimal akmaDayanci) {
			this.akmaDayanci = akmaDayanci;
		}

		/**
		 * @return the cekmeDayanci
		 */
		public BigDecimal getCekmeDayanci() {
			return cekmeDayanci;
		}

		/**
		 * @param cekmeDayanci
		 *            the cekmeDayanci to set
		 */
		public void setCekmeDayanci(BigDecimal cekmeDayanci) {
			this.cekmeDayanci = cekmeDayanci;
		}

		/**
		 * @return the uzama
		 */
		public BigDecimal getUzama() {
			return uzama;
		}

		/**
		 * @param uzama
		 *            the uzama to set
		 */
		public void setUzama(BigDecimal uzama) {
			this.uzama = uzama;
		}

		/**
		 * @return the centikEnerjiMin
		 */
		public BigDecimal getCentikEnerjiMin() {
			return centikEnerjiMin;
		}

		/**
		 * @param centikEnerjiMin
		 *            the centikEnerjiMin to set
		 */
		public void setCentikEnerjiMin(BigDecimal centikEnerjiMin) {
			this.centikEnerjiMin = centikEnerjiMin;
		}

		/**
		 * @return the centikEnerjiOrtalama
		 */
		public BigDecimal getCentikEnerjiOrtalama() {
			return centikEnerjiOrtalama;
		}

		/**
		 * @param centikEnerjiOrtalama
		 *            the centikEnerjiOrtalama to set
		 */
		public void setCentikEnerjiOrtalama(BigDecimal centikEnerjiOrtalama) {
			this.centikEnerjiOrtalama = centikEnerjiOrtalama;
		}

		/**
		 * @return the itemNo
		 */
		public String getItemNo() {
			return itemNo;
		}

		/**
		 * @param itemNo
		 *            the itemNo to set
		 */
		public void setItemNo(String itemNo) {
			this.itemNo = itemNo;
		}

		/**
		 * @return the batchNo
		 */
		public String getBatchNo() {
			return batchNo;
		}

		/**
		 * @param batchNo
		 *            the batchNo to set
		 */
		public void setBatchNo(String batchNo) {
			this.batchNo = batchNo;
		}

		/**
		 * @return the diameter
		 */
		public String getDiameter() {
			return diameter;
		}

		/**
		 * @param diameter
		 *            the diameter to set
		 */
		public void setDiameter(String diameter) {
			this.diameter = diameter;
		}

		/**
		 * @return the thickness
		 */
		public String getThickness() {
			return thickness;
		}

		/**
		 * @param thickness
		 *            the thickness to set
		 */
		public void setThickness(String thickness) {
			this.thickness = thickness;
		}

		/**
		 * @return the minKesmeAlani
		 */
		public BigDecimal getMinKesmeAlani() {
			return minKesmeAlani;
		}

		/**
		 * @param minKesmeAlani
		 *            the minKesmeAlani to set
		 */
		public void setMinKesmeAlani(BigDecimal minKesmeAlani) {
			this.minKesmeAlani = minKesmeAlani;
		}

		/**
		 * @return the ortalamaKesmeAlani
		 */
		public BigDecimal getOrtalamaKesmeAlani() {
			return ortalamaKesmeAlani;
		}

		/**
		 * @param ortalamaKesmeAlani
		 *            the ortalamaKesmeAlani to set
		 */
		public void setOrtalamaKesmeAlani(BigDecimal ortalamaKesmeAlani) {
			this.ortalamaKesmeAlani = ortalamaKesmeAlani;
		}

		/**
		 * @return the aUcuIcCapCevresel
		 */
		public String getaUcuIcCapCevresel() {
			return aUcuIcCapCevresel;
		}

		/**
		 * @param aUcuIcCapCevresel
		 *            the aUcuIcCapCevresel to set
		 */
		public void setaUcuIcCapCevresel(String aUcuIcCapCevresel) {
			this.aUcuIcCapCevresel = aUcuIcCapCevresel;
		}

		/**
		 * @return the bUcuIcCapCevresel
		 */
		public String getbUcuIcCapCevresel() {
			return bUcuIcCapCevresel;
		}

		/**
		 * @param bUcuIcCapCevresel
		 *            the bUcuIcCapCevresel to set
		 */
		public void setbUcuIcCapCevresel(String bUcuIcCapCevresel) {
			this.bUcuIcCapCevresel = bUcuIcCapCevresel;
		}

		/**
		 * @return the boruBoyu
		 */
		public String getBoruBoyu() {
			return boruBoyu;
		}

		/**
		 * @param boruBoyu
		 *            the boruBoyu to set
		 */
		public void setBoruBoyu(String boruBoyu) {
			this.boruBoyu = boruBoyu;
		}

		/**
		 * @return the boruAgirligi
		 */
		public String getBoruAgirligi() {
			return boruAgirligi;
		}

		/**
		 * @param boruAgirligi
		 *            the boruAgirligi to set
		 */
		public void setBoruAgirligi(String boruAgirligi) {
			this.boruAgirligi = boruAgirligi;
		}

	}

	public class chemicalObject {

		public String heatNo = null;
		public String code = null;
		public String type = null;
		public String c = null;
		public String si = null;
		public String mn = null;
		public String p = null;
		public String s = null;
		public String cr = null;
		public String ni = null;
		public String mo = null;
		public String cu = null;
		public String al = null;
		public String ti = null;
		public String v = null;
		public String nb = null;
		public String w = null;
		public String b = null;
		public String as = null;
		public String sb = null;
		public String sn = null;
		public String pb = null;
		public String ca = null;
		public String n = null;
		public String ae = null;
		public String ce = null;

		/**
		 * @return the heatNo
		 */
		public String getHeatNo() {
			return heatNo;
		}

		/**
		 * @param heatNo
		 *            the heatNo to set
		 */
		public void setHeatNo(String heatNo) {
			this.heatNo = heatNo;
		}

		/**
		 * @return the code
		 */
		public String getCode() {
			return code;
		}

		/**
		 * @param code
		 *            the code to set
		 */
		public void setCode(String code) {
			this.code = code;
		}

		/**
		 * @return the type
		 */
		public String getType() {
			return type;
		}

		/**
		 * @param type
		 *            the type to set
		 */
		public void setType(String type) {
			this.type = type;
		}

		/**
		 * @return the c
		 */
		public String getC() {
			return c;
		}

		/**
		 * @param c
		 *            the c to set
		 */
		public void setC(String c) {
			this.c = c;
		}

		/**
		 * @return the si
		 */
		public String getSi() {
			return si;
		}

		/**
		 * @param si
		 *            the si to set
		 */
		public void setSi(String si) {
			this.si = si;
		}

		/**
		 * @return the mn
		 */
		public String getMn() {
			return mn;
		}

		/**
		 * @param mn
		 *            the mn to set
		 */
		public void setMn(String mn) {
			this.mn = mn;
		}

		/**
		 * @return the p
		 */
		public String getP() {
			return p;
		}

		/**
		 * @param p
		 *            the p to set
		 */
		public void setP(String p) {
			this.p = p;
		}

		/**
		 * @return the s
		 */
		public String getS() {
			return s;
		}

		/**
		 * @param s
		 *            the s to set
		 */
		public void setS(String s) {
			this.s = s;
		}

		/**
		 * @return the cr
		 */
		public String getCr() {
			return cr;
		}

		/**
		 * @param cr
		 *            the cr to set
		 */
		public void setCr(String cr) {
			this.cr = cr;
		}

		/**
		 * @return the ni
		 */
		public String getNi() {
			return ni;
		}

		/**
		 * @param ni
		 *            the ni to set
		 */
		public void setNi(String ni) {
			this.ni = ni;
		}

		/**
		 * @return the mo
		 */
		public String getMo() {
			return mo;
		}

		/**
		 * @param mo
		 *            the mo to set
		 */
		public void setMo(String mo) {
			this.mo = mo;
		}

		/**
		 * @return the cu
		 */
		public String getCu() {
			return cu;
		}

		/**
		 * @param cu
		 *            the cu to set
		 */
		public void setCu(String cu) {
			this.cu = cu;
		}

		/**
		 * @return the al
		 */
		public String getAl() {
			return al;
		}

		/**
		 * @param al
		 *            the al to set
		 */
		public void setAl(String al) {
			this.al = al;
		}

		/**
		 * @return the ti
		 */
		public String getTi() {
			return ti;
		}

		/**
		 * @param ti
		 *            the ti to set
		 */
		public void setTi(String ti) {
			this.ti = ti;
		}

		/**
		 * @return the v
		 */
		public String getV() {
			return v;
		}

		/**
		 * @param v
		 *            the v to set
		 */
		public void setV(String v) {
			this.v = v;
		}

		/**
		 * @return the nb
		 */
		public String getNb() {
			return nb;
		}

		/**
		 * @param nb
		 *            the nb to set
		 */
		public void setNb(String nb) {
			this.nb = nb;
		}

		/**
		 * @return the w
		 */
		public String getW() {
			return w;
		}

		/**
		 * @param w
		 *            the w to set
		 */
		public void setW(String w) {
			this.w = w;
		}

		/**
		 * @return the b
		 */
		public String getB() {
			return b;
		}

		/**
		 * @param b
		 *            the b to set
		 */
		public void setB(String b) {
			this.b = b;
		}

		/**
		 * @return the as
		 */
		public String getAs() {
			return as;
		}

		/**
		 * @param as
		 *            the as to set
		 */
		public void setAs(String as) {
			this.as = as;
		}

		/**
		 * @return the sb
		 */
		public String getSb() {
			return sb;
		}

		/**
		 * @param sb
		 *            the sb to set
		 */
		public void setSb(String sb) {
			this.sb = sb;
		}

		/**
		 * @return the sn
		 */
		public String getSn() {
			return sn;
		}

		/**
		 * @param sn
		 *            the sn to set
		 */
		public void setSn(String sn) {
			this.sn = sn;
		}

		/**
		 * @return the pb
		 */
		public String getPb() {
			return pb;
		}

		/**
		 * @param pb
		 *            the pb to set
		 */
		public void setPb(String pb) {
			this.pb = pb;
		}

		/**
		 * @return the ca
		 */
		public String getCa() {
			return ca;
		}

		/**
		 * @param ca
		 *            the ca to set
		 */
		public void setCa(String ca) {
			this.ca = ca;
		}

		/**
		 * @return the n
		 */
		public String getN() {
			return n;
		}

		/**
		 * @param n
		 *            the n to set
		 */
		public void setN(String n) {
			this.n = n;
		}

		/**
		 * @return the ae
		 */
		public String getAe() {
			return ae;
		}

		/**
		 * @param ae
		 *            the ae to set
		 */
		public void setAe(String ae) {
			this.ae = ae;
		}

		/**
		 * @return the ce
		 */
		public String getCe() {
			return ce;
		}

		/**
		 * @param ce
		 *            the ce to set
		 */
		public void setCe(String ce) {
			this.ce = ce;
		}
	}

	public class dokumlerObject {
		public List<String> dokumNo = new ArrayList<String>();;

		/**
		 * @return the dokumNo
		 */
		public List<String> getDokumNo() {
			return dokumNo;
		}

		/**
		 * @param dokumNo
		 *            the dokumNo to set
		 */
		public void setDokumNo(List<String> dokumNo) {
			this.dokumNo = dokumNo;
		}
	}

	public class shippingListObject {

		public String pipeBarkodNo = null;
		public String ruloHeatNo = null;
		public String ruloCoilNo = null;
		public BigDecimal boruAgirlik = null;
		public BigDecimal boruUzunlukMetre = null;

		/**
		 * @return the pipeBarkodNo
		 */
		public String getPipeBarkodNo() {
			return pipeBarkodNo;
		}

		/**
		 * @param pipeBarkodNo
		 *            the pipeBarkodNo to set
		 */
		public void setPipeBarkodNo(String pipeBarkodNo) {
			this.pipeBarkodNo = pipeBarkodNo;
		}

		/**
		 * @return the ruloHeatNo
		 */
		public String getRuloHeatNo() {
			return ruloHeatNo;
		}

		/**
		 * @param ruloHeatNo
		 *            the ruloHeatNo to set
		 */
		public void setRuloHeatNo(String ruloHeatNo) {
			this.ruloHeatNo = ruloHeatNo;
		}

		/**
		 * @return the ruloCoilNo
		 */
		public String getRuloCoilNo() {
			return ruloCoilNo;
		}

		/**
		 * @param ruloCoilNo
		 *            the ruloCoilNo to set
		 */
		public void setRuloCoilNo(String ruloCoilNo) {
			this.ruloCoilNo = ruloCoilNo;
		}

		/**
		 * @return the boruAgirlik
		 */
		public BigDecimal getBoruAgirlik() {
			return boruAgirlik;
		}

		/**
		 * @param boruAgirlik
		 *            the boruAgirlik to set
		 */
		public void setBoruAgirlik(BigDecimal boruAgirlik) {
			this.boruAgirlik = boruAgirlik;
		}

		/**
		 * @return the boruUzunlukMetre
		 */
		public BigDecimal getBoruUzunlukMetre() {
			return boruUzunlukMetre;
		}

		/**
		 * @param boruUzunlukMetre
		 *            the boruUzunlukMetre to set
		 */
		public void setBoruUzunlukMetre(BigDecimal boruUzunlukMetre) {
			this.boruUzunlukMetre = boruUzunlukMetre;
		}

	}

	public void siklikBul() {

		for (int i = 0; i < selectedSalesItem.getDestructiveTestsSpecs().size(); i++) {
			if (selectedSalesItem.getDestructiveTestsSpecs().get(i).getCode()
					.equals("C03")) {
				c03Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				c03SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(c03SiklikNumber);
				c03PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("F01")) {
				f01Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				f01SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(f01SiklikNumber);
				f01PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("F02")) {
				f02Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				f02SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(f02SiklikNumber);
				f02PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("F03")) {
				f03Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				f03SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(f03SiklikNumber);
				f03PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("F04")) {
				f04Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				f04SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(f04SiklikNumber);
				f04PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("F05")) {
				f05Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				f05SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(f05SiklikNumber);
				f05PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("F06")) {
				f06Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				f06SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(f06SiklikNumber);
				f06PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("Z01")) {
				z01Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				z01SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(z01SiklikNumber);
				z01PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("Z02")) {
				z02Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				z02SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(z02SiklikNumber);
				z02PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("Z03")) {
				z03Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				z03SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(z03SiklikNumber);
				z03PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("Z06")) {
				z06Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				z06SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(z06SiklikNumber);
				z06PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("Z07")) {
				z07Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				z07SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(z07SiklikNumber);
				z07PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("K01")) {
				k01Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				k01SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(k01SiklikNumber);
				k01PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("K02")) {
				k02Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				k02SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(k02SiklikNumber);
				k02PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("K03")) {
				k03Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				k03SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(k03SiklikNumber);
				k03PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("K04")) {
				k04Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				k04SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(k04SiklikNumber);
				k04PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("K05")) {
				k05Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				k05SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(k05SiklikNumber);
				k05PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("K08")) {
				k08Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				k08SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(k08SiklikNumber);
				k08PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("D01")) {
				d01Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				d01SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(d01SiklikNumber);
				d01PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("D02")) {
				d02Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				d02SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(d02SiklikNumber);
				d02PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("M01")) {
				m01Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				m01SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(m01SiklikNumber);
				m01PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("H01")) {
				h01Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				h01SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(h01SiklikNumber);
				h01PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("U01")) {
				u01Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				u01SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(u01SiklikNumber);
				u01PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("U02")) {
				u02Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				u02SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(u02SiklikNumber);
				u02PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("U03")) {
				u03Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				u03SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(u03SiklikNumber);
				u03PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("U04")) {
				u04Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				u04SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(u04SiklikNumber);
				u04PartiSayisi = partiSayisi;
			} else if (selectedSalesItem.getDestructiveTestsSpecs().get(i)
					.getCode().equals("U05")) {
				u05Siklik = selectedSalesItem.getDestructiveTestsSpecs().get(i)
						.getIncelemeGrubu().getAdi();
				u05SiklikNumber = selectedSalesItem.getDestructiveTestsSpecs()
						.get(i).getIncelemeGrubu().getId();
				partiNoHesapla(u05SiklikNumber);
				u05PartiSayisi = partiSayisi;
			}
		}
	}

	public void releaseNoteTarihVer() {
		PipeManager pipeManager = new PipeManager();
		for (int i = 0; i < selectedGozOlcuPipes.length; i++) {
			selectedGozOlcuPipes[i].getPipe().setReleaseNoteTarihi(
					releaseNoteTarihi);
			pipeManager.updateEntity(selectedGozOlcuPipes[i].getPipe());
		}

		PipeManager manager = new PipeManager();
		releaseNoteTarihleriList = manager
				.findReleaseCountByItemId(selectedSalesItem.getItemId());

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				"SEÇİLEN BORU SAYISI = " + selectedGozOlcuPipes.length, null));

		selectedGozOlcuPipes = null;
	}

	public void releaseNoteTarihSil() {
		PipeManager pipeManager = new PipeManager();
		for (int i = 0; i < selectedGozOlcuPipes.length; i++) {
			selectedGozOlcuPipes[i].getPipe().setReleaseNoteTarihi(null);
			pipeManager.updateEntity(selectedGozOlcuPipes[i].getPipe());
		}

		PipeManager manager = new PipeManager();
		releaseNoteTarihleriList = manager
				.findReleaseCountByItemId(selectedSalesItem.getItemId());

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
				"SİLİNEN BORU SAYISI = " + selectedGozOlcuPipes.length, null));
		selectedGozOlcuPipes = null;
	}

	public void releaseNoteListele(int prmSorgu) {
		TestTahribatsizGozOlcuSonucManager gozManager = new TestTahribatsizGozOlcuSonucManager();
		if (prmSorgu == 1) {// hepsi
			gozOlcuPipes = gozManager.findByItemIdSonuc(selectedSalesItem
					.getItemId());
		} else if (prmSorgu == 0) {// olmayanlar
			gozOlcuPipes = gozManager
					.findByItemIdSonucNoReleaseDate(selectedSalesItem
							.getItemId());
		} else if (prmSorgu == -1) {// olanlar
			gozOlcuPipes = gozManager
					.findByItemIdSonucYesReleaseDate(selectedSalesItem
							.getItemId());
		} else {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL, "SORGU HATASI", null));
		}
	}

	public void seciliReleaseBorulari() {

		PipeManager manager = new PipeManager();
		selectedSorguPipes = manager.findReleasePipesByReleaseTarihi(
				selectedReleaseNoteTarihiIlk, selectedReleaseNoteTarihiSon);

	}

	public void ucBirTarihVer() {
		PipeManager pipeManager = new PipeManager();
		for (int i = 0; i < selectedGozOlcuPipes.length; i++) {
			selectedGozOlcuPipes[i].getPipe().setUcBirTarihi(ucBirTarihi);
			pipeManager.updateEntity(selectedGozOlcuPipes[i].getPipe());
		}

		PipeManager manager = new PipeManager();
		ucBirTarihleriList = manager.findUcBirCountByItemId(selectedSalesItem
				.getItemId());

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				"TARİHİ SEÇİLEN BORU SAYISI = " + selectedGozOlcuPipes.length,
				null));

		selectedGozOlcuPipes = null;
	}

	public void ucBirTarihSil() {
		PipeManager pipeManager = new PipeManager();
		for (int i = 0; i < selectedGozOlcuPipes.length; i++) {
			selectedGozOlcuPipes[i].getPipe().setUcBirTarihi(null);
			pipeManager.updateEntity(selectedGozOlcuPipes[i].getPipe());
		}

		PipeManager manager = new PipeManager();
		ucBirTarihleriList = manager.findUcBirCountByItemId(selectedSalesItem
				.getItemId());

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
				"TARİHİ SİLİNEN BORU SAYISI = " + selectedGozOlcuPipes.length,
				null));
		selectedGozOlcuPipes = null;
	}

	public void ucBirListele(int prmSorgu) {
		TestTahribatsizGozOlcuSonucManager gozManager = new TestTahribatsizGozOlcuSonucManager();
		if (prmSorgu == 1) {// hepsi
			gozOlcuPipes = gozManager.findByItemIdSonuc(selectedSalesItem
					.getItemId());
		} else if (prmSorgu == 0) {// olmayanlar
			gozOlcuPipes = gozManager
					.findByItemIdSonucNoUcBirDate(selectedSalesItem.getItemId());
		} else if (prmSorgu == -1) {// olanlar
			gozOlcuPipes = gozManager
					.findByItemIdSonucYesUcBirDate(selectedSalesItem
							.getItemId());
		} else {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL, "SORGU HATASI", null));
		}
	}

	public void seciliUcBirBorulari() {

		PipeManager manager = new PipeManager();
		selectedUcBirPipes = manager.findUcBirPipesByUcBirTarihi(
				selectedUcBirTarihiIlk, selectedUcBirTarihiSon);

	}

	public void dokumBul(List<Pipe> prmPipeList) {

		dokumler.clear();
		pipeRulos.clear();
		// int j = 0;
		boolean check = true;
		dokumler.add(prmPipeList.get(0).getRuloPipeLink().getRulo()
				.getRuloDokumNo());
		pipeRulos.add(prmPipeList.get(0).getRuloPipeLink().getRulo());
		for (int i = 0; i < prmPipeList.size(); i++) {
			for (int j = 0; j < dokumler.size(); j++) {
				if (dokumler.get(j).equals(
						prmPipeList.get(i).getRuloPipeLink().getRulo()
								.getRuloDokumNo())) {
					check = true;
					break;
				} else {
					check = false;
				}
			}
			if (!check) {
				dokumler.add(prmPipeList.get(i).getRuloPipeLink().getRulo()
						.getRuloDokumNo());
				pipeRulos.add(prmPipeList.get(i).getRuloPipeLink().getRulo());
				check = true;
			}
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void plateListMatchingWithPipeNumbersReport() {

		if (selectedSorguPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"RELEASE BORULARI SEÇİLMEMİŞTİR!", null));
			return;
		}

		try {
			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();

			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", selectedSorguPipes.get(0).getSalesItem()
							.getItemId());

			siklikBul();
			dokumBul(selectedSorguPipes);

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("pipeSonuc", selectedSorguPipes);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil
					.jxlsExportAsXls(
							dataMap,
							"/reportformats/releaseNote/plateListMatchingWithPipeNumbers.xls",
							"-" + selectedSorguPipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"RAPOR EKSİKTİR, RAPOR OLUŞTURULAMADI, HATA!", null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void plateHeatAnalysisReport() {

		if (selectedSorguPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"RELEASE BORULARI SEÇİLMEMİŞTİR!", null));
			return;
		}

		try {
			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();

			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", selectedSorguPipes.get(0).getSalesItem()
							.getItemId());

			siklikBul();
			dokumBul(selectedSorguPipes);

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("ruloSonuc", pipeRulos);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/releaseNote/plateHeatAnalysis.xls", "-"
							+ selectedSorguPipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"RAPOR VERİLERİ EKSİKTİR, RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void mechanicalTestResultsofPlates() {

		if (selectedSorguPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"RELEASE BORULARI SEÇİLMEMİŞTİR!", null));
			return;
		}

		try {
			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();

			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", selectedSorguPipes.get(0).getSalesItem()
							.getItemId());

			siklikBul();
			dokumBul(selectedSorguPipes);

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("ruloSonuc", pipeRulos);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/releaseNote/plateHeatAnalysis.xls", "-"
							+ selectedSorguPipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"RAPOR VERİLERİ EKSİKTİR, RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void hydrostaticTestReports() {

		if (selectedSorguPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"RELEASE BORULARI SEÇİLMEMİŞTİR!", null));
			return;
		}

		try {
			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();

			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", selectedSorguPipes.get(0).getSalesItem()
							.getItemId());

			siklikBul();
			dokumBul(selectedSorguPipes);

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("hidroSonuc", selectedSorguPipes);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/releaseNote/hydrostaticTestReports.xls",
					"-" + selectedSorguPipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"HİDROSTATİK TEST RAPORU BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"RAPOR VERİLERİ EKSİKTİR, RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void kimyasalAnalizInspectionResultReportXlsx() {

		if (selectedSorguPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"RELEASE BORULARI SEÇİLMEMİŞTİR!", null));
			return;
		}

		try {
			TestKimyasalAnalizSonucManager testKimyasalAnalizSonucManager = new TestKimyasalAnalizSonucManager();

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			List<TestKimyasalAnalizSonuc> testKimyasalAnalizSonucs = new ArrayList<TestKimyasalAnalizSonuc>();

			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", selectedSorguPipes.get(0).getSalesItem()
							.getItemId());

			siklikBul();
			dokumBul(selectedSorguPipes);

			for (int i = 0; i < dokumler.size(); i++) {
				if (testKimyasalAnalizSonucManager
						.getByDokumNo(dokumler.get(i)).size() > 0) {
					testKimyasalAnalizSonucs.add(testKimyasalAnalizSonucManager
							.getByDokumNo(dokumler.get(i)).get(0));
				}
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy")
					.format(testKimyasalAnalizSonucs.get(0).getEklemeZamani()));

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("kimyasalAnalizSonuc", testKimyasalAnalizSonucs);
			dataMap.put("date", date);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/releaseNote/productAnalysisReport.xls", "-"
							+ selectedSorguPipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"C03 RELEASE RAPORU BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"C03 VERİLERİ EKSİKTİR, RAPOR OLUŞTURULAMADI, HATA!", null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void bukmeInspectionResultReportXlsx() {

		if (selectedSorguPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"RELEASE BORULARI SEÇİLMEMİŞTİR!", null));
			return;
		}

		try {
			TestBukmeSonucManager testBukmeSonucManager = new TestBukmeSonucManager();

			SalesItemManager salesItemManager = new SalesItemManager();

			List<SalesItem> salesItems = new ArrayList<SalesItem>();

			List<TestBukmeSonuc> testBukmeSonucs = new ArrayList<TestBukmeSonuc>();

			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", selectedSorguPipes.get(0).getSalesItem()
							.getItemId());

			siklikBul();
			dokumBul(selectedSorguPipes);

			for (int i = 0; i < dokumler.size(); i++) {
				if (testBukmeSonucManager.getByDokumNo(dokumler.get(i)).size() > 0) {
					testBukmeSonucs.add(testBukmeSonucManager.getByDokumNo(
							dokumler.get(i)).get(0));
				}
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(testBukmeSonucs
					.get(0).getEklemeZamani()));

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("bukmeSonuc", testBukmeSonucs);
			dataMap.put("date", date);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/releaseNote/guidedBendRelease.xls", "-"
							+ selectedSorguPipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"KILAVUZLU BÜKME TESTİ RAPOR BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"BÜKME TEST VERİLERİ EKSİKTİR, RAPOR OLUŞTURULAMADI, HATA!",
							null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void agirlikDusurmeInspectionResultReportXlsx() {

		if (selectedSorguPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"RELEASE BORULARI SEÇİLMEMİŞTİR!", null));
			return;
		}

		try {
			TestAgirlikDusurmeSonucManager testAgirlikDusurmeSonucManager = new TestAgirlikDusurmeSonucManager();
			List<TestAgirlikDusurmeSonuc> testAgirlikDusurmeSonucs = new ArrayList<TestAgirlikDusurmeSonuc>();

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();

			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", selectedSorguPipes.get(0).getSalesItem()
							.getItemId());

			// AgirlikTestsSpecManager atsManager = new
			// AgirlikTestsSpecManager();
			// AgirlikTestsSpec spec = new AgirlikTestsSpec();
			// List<DestructiveTestsSpec> destructiveTestsSpecs = new
			// ArrayList<DestructiveTestsSpec>();

			siklikBul();
			dokumBul(selectedSorguPipes);

			// DestructiveTestsSpecManager dtsManager = new
			// DestructiveTestsSpecManager();
			// destructiveTestsSpecs.add(dtsManager.loadObject(
			// DestructiveTestsSpec.class, testId));
			//
			// // test spec getirme
			// try {
			// spec = atsManager.seciliAgirlikTestTanim(
			// destructiveTestsSpecs.get(0).getGlobalId(),
			// salesItems.get(0).getItemId()).get(0);
			// } catch (Exception e) {
			// e.printStackTrace();
			// FacesContext context = FacesContext.getCurrentInstance();
			// context.addMessage(null,
			// new FacesMessage(FacesMessage.SEVERITY_ERROR,
			// "SPEC BULMA HATASI!", null));
			// }

			for (int i = 0; i < dokumler.size(); i++) {
				if (testAgirlikDusurmeSonucManager
						.getByDokumNo(dokumler.get(i)).size() > 0) {
					testAgirlikDusurmeSonucs.add(testAgirlikDusurmeSonucManager
							.getByDokumNo(dokumler.get(i)).get(0));
				}
			}

			List<String> date = new ArrayList<String>();
			// date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			date.add(new SimpleDateFormat("dd.MM.yyyy")
					.format(testAgirlikDusurmeSonucs.get(0).getEklemeZamani()));

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("agirlikDusurmeSonuc", testAgirlikDusurmeSonucs);
			// dataMap.put("spec", spec);
			dataMap.put("date", date);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/releaseNote/dwttRelease.xls", "-"
							+ selectedSorguPipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"AĞIRLIK DÜŞÜRME RAPORU BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"AĞIRLIK DÜŞÜRME VERİLERİ EKSİKTİR, RAPOR OLUŞTURULAMADI, HATA!",
							null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes", "static-access" })
	public void kaynakInspectionResultReportXlsx(int prmGLobalId) {

		if (selectedSorguPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"RELEASE BORULARI SEÇİLMEMİŞTİR!", null));
			return;
		}

		try {
			TestKaynakSonucManager testKaynakSonucManager = new TestKaynakSonucManager();
			List<TestKaynakSonuc> testKaynakSonucs = new ArrayList<TestKaynakSonuc>();

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();

			// List<DestructiveTestsSpec> destructiveTestsSpecs = new
			// ArrayList<DestructiveTestsSpec>();

			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", selectedSorguPipes.get(0).getSalesItem()
							.getItemId());

			siklikBul();
			dokumBul(selectedSorguPipes);

			for (int i = 0; i < dokumler.size(); i++) {
				if (testKaynakSonucManager.getByDokumNoGlobalId(
						dokumler.get(i), prmGLobalId).size() > 0) {
					testKaynakSonucs.add(testKaynakSonucManager
							.getByDokumNoGlobalId(dokumler.get(i), prmGLobalId)
							.get(0));
				}
			}

			// // testId ye gore testSpec ekleme
			// DestructiveTestsSpecManager dtsManager = new
			// DestructiveTestsSpecManager();
			// destructiveTestsSpecs.add(dtsManager.loadObject(
			// DestructiveTestsSpec.class, testId));
			//
			// // test spec getirme
			// KaynakMakroTestSpec spec = new KaynakMakroTestSpec();
			// try {
			// KaynakMacroTestSpecManager kmsManager = new
			// KaynakMacroTestSpecManager();
			// spec = kmsManager.seciliKaynakMakroTestTanim(
			// destructiveTestsSpecs.get(0).getGlobalId(),
			// salesItems.get(0).getItemId()).get(0);
			// } catch (Exception e) {
			// e.printStackTrace();
			// FacesContext context = FacesContext.getCurrentInstance();
			// context.addMessage(null,
			// new FacesMessage(FacesMessage.SEVERITY_ERROR,
			// "SPEC BULMA HATASI!", null));
			// }

			List<String> date = new ArrayList<String>();
			// date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(testKaynakSonucs
					.get(0).getEklemeZamani()));

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("kaynakSonuc", testKaynakSonucs);// form değerleri
			dataMap.put("date", date);

			ReportUtil reportUtil = new ReportUtil();
			if (prmGLobalId == 1031) {
				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/releaseNote/metallographicRelease.xls",
						"-" + selectedSorguPipes.get(0).getPipeIndex());
			} else if (prmGLobalId == 1032) {
				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/releaseNote/hardnessRelease.xls", "-"
								+ selectedSorguPipes.get(0).getPipeIndex());
			}

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_INFO,
							"MAKROGRAFİK MUAYENE RELEASE RAPORU BAŞARIYLA OLUŞTURULDU!",
							null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"MAKROGRAFİK MUAYENE VERİLERİ EKSİKTİR, RAPORU OLUŞTURULAMADI, HATA!",
							null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void centikDarbeInspectionResultReportXlsx() {

		if (selectedSorguPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"RELEASE BORULARI SEÇİLMEMİŞTİR!", null));
			return;
		}

		try {
			TestCentikDarbeSonucManager testCentikDarbeSonucManager = new TestCentikDarbeSonucManager();
			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			List<TestCentikDarbeSonuc> testCentikDarbeSonucs = new ArrayList<TestCentikDarbeSonuc>();

			List<DestructiveTestsSpec> destructiveTestsSpecs = new ArrayList<DestructiveTestsSpec>();

			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", selectedSorguPipes.get(0).getSalesItem()
							.getItemId());

			siklikBul();
			dokumBul(selectedSorguPipes);

			for (int i = 0; i < dokumler.size(); i++) {
				if (testCentikDarbeSonucManager.getByDokumNo(dokumler.get(i))
						.size() > 0) {
					testCentikDarbeSonucs.add(testCentikDarbeSonucManager
							.getByDokumNo(dokumler.get(i)).get(0));
				}
			}

			// // testId ye gore testSpec ekleme
			// DestructiveTestsSpecManager dtsManager = new
			// DestructiveTestsSpecManager();
			// destructiveTestsSpecs.add(dtsManager.loadObject(
			// DestructiveTestsSpec.class, testId));
			//
			// // test spec getirme
			// CentikDarbeTestsSpec spec = new CentikDarbeTestsSpec();
			// try {
			// CentikDarbeTestsSpecManager ctsManager = new
			// CentikDarbeTestsSpecManager();
			// spec = ctsManager.seciliCentikTestTanim(
			// destructiveTestsSpecs.get(0).getGlobalId(),
			// salesItems.get(0).getItemId()).get(0);
			// } catch (Exception e) {
			// e.printStackTrace();
			// FacesContext context = FacesContext.getCurrentInstance();
			// context.addMessage(null,
			// new FacesMessage(FacesMessage.SEVERITY_ERROR,
			// "SPEC BULMA HATASI!", null));
			// }

			List<String> date = new ArrayList<String>();
			// date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			date.add(new SimpleDateFormat("dd.MM.yyyy")
					.format(testCentikDarbeSonucs.get(0).getEklemeZamani()));

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("siparisCentikDarbeOzellik", destructiveTestsSpecs);
			dataMap.put("date", date);
			dataMap.put("testCentikDarbeSonucs", testCentikDarbeSonucs);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/releaseNote/impactRelease.xls", "-"
							+ selectedSorguPipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"ÇENTİK DARBE RELEASE RAPORU BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"ÇENTİK DARBE VERİLERİ EKSİKTİR, RAPOR OLUŞTURULAMADI, HATA!",
							null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void cekmeInspectionResultReportXlsx() {

		if (selectedSorguPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"RELEASE BORULARI SEÇİLMEMİŞTİR!", null));
			return;
		}

		try {

			TestCekmeSonucManager testCekmeSonucManager = new TestCekmeSonucManager();

			SalesItemManager salesItemManager = new SalesItemManager();

			List<SalesItem> salesItems = new ArrayList<SalesItem>();

			List<TestCekmeSonuc> testCekmeSonucs = new ArrayList<TestCekmeSonuc>();

			// List<DestructiveTestsSpec> destructiveTestsSpecs = new
			// ArrayList<DestructiveTestsSpec>();

			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", selectedSorguPipes.get(0).getSalesItem()
							.getItemId());

			siklikBul();
			dokumBul(selectedSorguPipes);

			for (int i = 0; i < dokumler.size(); i++) {
				if (testCekmeSonucManager.getByDokumNo(dokumler.get(i)).size() > 0) {
					testCekmeSonucs.add(testCekmeSonucManager.getByDokumNo(
							dokumler.get(i)).get(0));
				}
			}

			// DestructiveTestsSpecManager dtsManager = new
			// DestructiveTestsSpecManager();
			// destructiveTestsSpecs.add(dtsManager.loadObject(
			// DestructiveTestsSpec.class, testId));
			//
			// // test spec getirme
			// CekmeTestsSpec spec = new CekmeTestsSpec();
			// try {
			// CekmeTestSpecManager ctsManager = new CekmeTestSpecManager();
			// spec = ctsManager.seciliCekmeTestTanim(
			// destructiveTestsSpecs.get(0).getGlobalId(),
			// salesItems.get(0).getItemId()).get(0);
			// } catch (Exception e) {
			// e.printStackTrace();
			// FacesContext context = FacesContext.getCurrentInstance();
			// context.addMessage(null,
			// new FacesMessage(FacesMessage.SEVERITY_ERROR,
			// "SPEC BULMA HATASI!", null));
			// }

			List<String> date = new ArrayList<String>();
			// date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(testCekmeSonucs
					.get(0).getEklemeZamani()));

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("testCekmeSonucs", testCekmeSonucs);
			dataMap.put("date", date);

			ReportUtil reportUtil = new ReportUtil();

			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/releaseNote/tensileRelease.xls", "-"
							+ selectedSorguPipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"ÇEKME RELEASE RAPORU BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÇEKME VERİLERİ EKSİKTİR, RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void gozOlcuInspectionResultReportAllXlsx() {

		if (selectedSorguPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"RELEASE BORULARI SEÇİLMEMİŞTİR!", null));
			return;
		}

		try {
			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();

			TestTahribatsizGozOlcuSonucManager testTahribatsizGozOlcuSonucManager = new TestTahribatsizGozOlcuSonucManager();
			List<TestTahribatsizGozOlcuSonuc> testTahribatsizGozOlcuSonucs = new ArrayList<TestTahribatsizGozOlcuSonuc>();

			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", selectedSorguPipes.get(0).getSalesItem()
							.getItemId());

			for (int i = 0; i < selectedSorguPipes.size(); i++) {
				testTahribatsizGozOlcuSonucs
						.add(testTahribatsizGozOlcuSonucManager.loadObject(
								TestTahribatsizGozOlcuSonuc.class,
								selectedSorguPipes.get(i).getPipeId()));
			}

			List<String> date = new ArrayList<String>();
			// date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			date.add(new SimpleDateFormat("dd.MM.yyyy")
					.format(testTahribatsizGozOlcuSonucs.get(0)
							.getEklemeZamani()));

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("gozOlcuSonuc", testTahribatsizGozOlcuSonucs);
			dataMap.put("date", date);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil
					.jxlsExportAsXls(
							dataMap,
							"/reportformats/releaseNote/visualAndDimensionalInspectionReport.xls",
							"-" + selectedSorguPipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"GÖZ ÖLÇÜ RELEASE RAPORU BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"GÖZ ÖLÇÜ VERİLERİ EKSİKTİR, RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void cuttingRecordResultReportAllXlsx() {

		if (selectedSorguPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"RELEASE BORULARI SEÇİLMEMİŞTİR!", null));
			return;
		}

		try {
			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();

			TestTahribatsizKesimManager testTahribatsizKesimManager = new TestTahribatsizKesimManager();
			List<TestTahribatsizKesim> testTahribatsizKesimonucs = new ArrayList<TestTahribatsizKesim>();

			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", selectedSorguPipes.get(0).getSalesItem()
							.getItemId());

			for (int i = 0; i < selectedSorguPipes.size(); i++) {
				testTahribatsizKesimonucs.add(testTahribatsizKesimManager
						.loadObject(TestTahribatsizKesim.class,
								selectedSorguPipes.get(i).getPipeId()));
			}

			List<String> date = new ArrayList<String>();
			// date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			date.add(new SimpleDateFormat("dd.MM.yyyy")
					.format(testTahribatsizKesimonucs.get(0).getEklemeZamani()));

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("kesimSonuc", testTahribatsizKesimonucs);
			dataMap.put("date", date);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/releaseNote/cuttingRecordRelease.xls", "-"
							+ selectedSorguPipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"GÖZ ÖLÇÜ RELEASE RAPORU BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"GÖZ ÖLÇÜ VERİLERİ EKSİKTİR, RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void tanapPTS() {

		try {
			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			releaseObjects.clear();

			for (int i = 0; i < selectedSorguPipes.size(); i++) {

				List<TestAgirlikDusurmeSonuc> agirliks = new ArrayList<TestAgirlikDusurmeSonuc>();
				if (selectedSorguPipes.get(i).getTestAgirlikDusurmeSonuclar()
						.size() == 0) {
					TestAgirlikDusurmeSonucManager manager = new TestAgirlikDusurmeSonucManager();
					agirliks = manager.getByDokumNo(selectedSorguPipes.get(i)
							.getRuloPipeLink().getRulo().getRuloDokumNo());
				} else {
					agirliks.add(selectedSorguPipes.get(i)
							.getTestAgirlikDusurmeSonuclar().get(0));
				}

				findMinimumAgirliks(agirliks);

				// List<TestBukmeSonuc> bukmes = new
				// ArrayList<TestBukmeSonuc>();
				// if (selectedSorguPipes.get(i).getTestBukmeSonuclar().size()
				// == 0) {
				// TestBukmeSonucManager manager = new TestBukmeSonucManager();
				// bukmes = manager.getByDokumNo(selectedSorguPipes.get(i)
				// .getRuloPipeLink().getRulo().getRuloDokumNo());
				// } else {
				// bukmes.add(selectedSorguPipes.get(i).getTestBukmeSonuclar()
				// .get(0));
				// }

				List<TestKimyasalAnalizSonuc> kimyasals = new ArrayList<TestKimyasalAnalizSonuc>();
				if (selectedSorguPipes.get(i).getTestKimyasalAnalizSonuclar()
						.size() == 0) {
					TestKimyasalAnalizSonucManager manager = new TestKimyasalAnalizSonucManager();
					kimyasals = manager.getByDokumNo(selectedSorguPipes.get(i)
							.getRuloPipeLink().getRulo().getRuloDokumNo());
				} else {
					kimyasals.add(selectedSorguPipes.get(i)
							.getTestKimyasalAnalizSonuclar().get(0));
				}

				// List<TestCekmeSonuc> cekmes = new
				// ArrayList<TestCekmeSonuc>();
				// List<TestCekmeSonuc> cekmeSorgu = new
				// ArrayList<TestCekmeSonuc>();
				// if (selectedSorguPipes.get(i).getTestCekmeSonuclar().size()
				// == 0) {
				// TestCekmeSonucManager manager = new TestCekmeSonucManager();
				// cekmeSorgu = manager.getByDokumNo(selectedSorguPipes.get(i)
				// .getRuloPipeLink().getRulo().getRuloDokumNo());
				// for (int j = 0; j < cekmeSorgu.size(); j++) {
				//
				// if (cekmeSorgu.get(j).getBagliGlobalId().getGlobalId() ==
				// 1014) {
				// cekmes.add(cekmeSorgu.get(j));
				// }
				// }
				//
				// } else {
				// for (int j = 0; j < selectedSorguPipes.get(i)
				// .getTestCekmeSonuclar().size(); j++) {
				// if (selectedSorguPipes.get(i).getTestCekmeSonuclar()
				// .get(j).getBagliGlobalId().getGlobalId() == 1014) {
				// cekmes.add(selectedSorguPipes.get(i)
				// .getTestCekmeSonuclar().get(j));
				//
				// }
				// }
				// }
				List<TestCekmeSonuc> cekmes = new ArrayList<TestCekmeSonuc>();
				List<TestCekmeSonuc> cekmeSorgu = new ArrayList<TestCekmeSonuc>();
				TestCekmeSonucManager manager = new TestCekmeSonucManager();
				cekmeSorgu = manager.getByDokumNo(selectedSorguPipes.get(i)
						.getRuloPipeLink().getRulo().getRuloDokumNo());
				for (int j = 0; j < cekmeSorgu.size(); j++) {

					if (cekmeSorgu.get(j).getBagliGlobalId().getGlobalId() == 1014) {
						cekmes.add(cekmeSorgu.get(j));
					}
				}

				// aynı dökümse ilk dökümün değerleri kullanılıyor
				if (selectedSorguPipes.get(i).getRuloPipeLink().getRulo()
						.getRuloDokumNo().equals(kontrolDokum)) {

				} else {
					findMinimumCekmes(cekmes);

					// List<TestCentikDarbeSonuc> centiks = new
					// ArrayList<TestCentikDarbeSonuc>();
					// if
					// (selectedSorguPipes.get(i).getTestCentikDarbeSonuclar()
					// .size() == 0) {
					// TestCentikDarbeSonucManager manager = new
					// TestCentikDarbeSonucManager();
					// centiks = manager.getByDokumNo(selectedSorguPipes.get(i)
					// .getRuloPipeLink().getRulo().getRuloDokumNo());
					// } else {
					// centiks.add(selectedSorguPipes.get(i)
					// .getTestCentikDarbeSonuclar().get(0));
					// }

					findMinimumCentiks();

					// // GLOBAL ID YAZILACAK DİKKAT!!!
					// List<TestKaynakSonuc> kaynaks = new
					// ArrayList<TestKaynakSonuc>();
					// if
					// (selectedSorguPipes.get(i).getTestKaynakSonuclar().size()
					// == 0) {
					// TestKaynakSonucManager manager = new
					// TestKaynakSonucManager();
					// kaynaks = manager.getByDokumNoGlobalId(selectedSorguPipes
					// .get(i).getRuloPipeLink().getRulo()
					// .getRuloDokumNo(), 0);
					// } else {
					// kaynaks.add(selectedSorguPipes.get(i)
					// .getTestKaynakSonuclar().get(0));
					// }

				}

				releaseObject releaseObject = new releaseObject();
				releaseObject.setAkmaDayanci(finalCekme.getAkmaDayanci());
				releaseObject.setCekmeDayanci(finalCekme.getCekmeDayanci());
				releaseObject.setUzama(finalCekme.getUzama());
				releaseObject.setMinKesmeAlani(minDWTT);
				releaseObject.setOrtalamaKesmeAlani(ortalamaDWTT);
				releaseObject.setCentikEnerjiMin(minCVNBody);
				releaseObject.setCentikEnerjiOrtalama(ortalamaCVNBody);

				if (selectedSorguPipes.get(0).getSalesItem().getItemNo()
						.equals("1")) {
					releaseObject.setItemNo("1,1");
				} else if (selectedSorguPipes.get(0).getSalesItem().getItemNo()
						.equals("2")) {
					releaseObject.setItemNo("1,4");
				} else if (selectedSorguPipes.get(0).getSalesItem().getItemNo()
						.equals("3")) {
					releaseObject.setItemNo("4,1");
				} else if (selectedSorguPipes.get(0).getSalesItem().getItemNo()
						.equals("4")) {
					releaseObject.setItemNo("4,5");
				}

				if (Double.compare(selectedSorguPipes.get(0).getSalesItem()
						.getDiameter(), 1422.0) == 0) {
					releaseObject.setDiameter("56");
					releaseObject.setThickness("19,45");
				} else if (Double.compare(selectedSorguPipes.get(0)
						.getSalesItem().getDiameter(), 1219.0) == 0) {
					releaseObject.setDiameter("48");
					releaseObject.setThickness("16,67");
				}

				KaplamaMalzemeKullanmaPeManager peManager = new KaplamaMalzemeKullanmaPeManager();
				List<KaplamaMalzemeKullanmaPe> peList = new ArrayList<KaplamaMalzemeKullanmaPe>();

				if (peKontrol(selectedSorguPipes.get(i))) {
					prmVardiya = UtilInsCore.findShift(selectedPe
							.getKaplamaBaslamaZamani().getHours());
					String prmTarih1 = new SimpleDateFormat("yyyy-MM-dd")
							.format(selectedPe.getKaplamaBaslamaZamani());
					peList = peManager.findByDateVardiya(prmTarih1, prmVardiya);
				} else {
					FacesContext context = FacesContext.getCurrentInstance();
					context.addMessage(
							null,
							new FacesMessage(
									FacesMessage.SEVERITY_ERROR,
									"PE KAPLAMASI BULUNAMADI, BARKODU KONTROL EDİNİZ!!",
									null));
				}

				if (peList == null || peList.size() == 0) {
					releaseObject.setBatchNo("");
				} else if (peList.size() > 0) {
					releaseObject.setBatchNo(peList.get(0).getCoatRawMaterial()
							.getPartiNo());
				}

				try {
					releaseObject.setBoruBoyu(new DecimalFormat("#0.00")
							.format(selectedSorguPipes.get(i)
									.getTestTahribatsizGozOlcuSonuc()
									.getBoruUzunluk().divide(binSayisi)));

				} catch (Exception e) {
					e.printStackTrace();
					releaseObject.setBoruBoyu(null);
				}

				// try {
				// String[] tokens = new DecimalFormat("#0.##").format(
				// selectedSorguPipes.get(i)
				// .getTestTahribatsizGozOlcuSonuc()
				// .getBoruAgirlik().divide(binSayisi)).split(
				// "[.]");
				// if (tokens.length > 1)
				// releaseObject.setBoruAgirligi(tokens[0] + ","
				// + tokens[1]);
				// else
				// releaseObject.setBoruAgirligi(tokens[0] + ",0000");
				// } catch (Exception e) {
				// e.printStackTrace();
				// releaseObject.setBoruAgirligi(null);
				// }

				try {

					releaseObject.setBoruAgirligi(new DecimalFormat("#0.0000")
							.format(selectedSorguPipes.get(i)
									.getTestTahribatsizGozOlcuSonuc()
									.getBoruAgirlik().divide(binSayisi)));
				} catch (Exception e) {
					e.printStackTrace();
					releaseObject.setBoruAgirligi(null);
				}

				try {
					releaseObject.setaUcuIcCapCevresel(selectedSorguPipes
							.get(i).getTestTahribatsizGozOlcuSonuc()
							.getaUcuIcCapCevresel().toString());
				} catch (Exception e) {
					e.printStackTrace();
					releaseObject.setaUcuIcCapCevresel(null);
				}

				try {
					releaseObject.setbUcuIcCapCevresel(selectedSorguPipes
							.get(i).getTestTahribatsizGozOlcuSonuc()
							.getbUcuIcCapCevresel().toString());
				} catch (Exception e) {
					e.printStackTrace();
					releaseObject.setbUcuIcCapCevresel(null);
				}

				releaseObjects.add(releaseObject);
				releaseObject = new releaseObject();
				// agirliks.clear();
				// minDWTT = null;
				// ortalamaDWTT = null;
				// minCVNBody = null;
				// ortalamaCVNBody = null;
				// bukmes.clear();
				cekmes.clear();
				// finalCekme = new TestCekmeSonuc();
				// centiks.clear();
				kimyasals.clear();
				// kaynaks.clear();

				kontrolDokum = selectedSorguPipes.get(i).getRuloPipeLink()
						.getRulo().getRuloDokumNo();

			}

			Map dataMap = new HashMap();
			dataMap.put("siparis", selectedSorguPipes.get(0).getSalesItem());
			dataMap.put("pipes", selectedSorguPipes);
			dataMap.put("releaseObjects", releaseObjects);
			dataMap.put("date", date);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/releaseNote/PipeTrackingSystem.xls", "-"
							+ selectedSorguPipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"TANAP PTS RAPORU BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {

			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"TANAP PTS VERİLERİ EKSİKTİR, RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings("static-access")
	public boolean peKontrol(Pipe prmPipe) {

		KaplamaMakinesiPolietilenManager manager = new KaplamaMakinesiPolietilenManager();

		if (manager.getAllKaplamaMakinesiPolietilen(prmPipe.getPipeId()).size() > 0) {
			selectedPe = manager.getAllKaplamaMakinesiPolietilen(
					prmPipe.getPipeId()).get(0);
			return true;
		} else {
			selectedPe = new KaplamaMakinesiPolietilen();
			return false;
		}
	}

	public void findMinimumAgirliks(List<TestAgirlikDusurmeSonuc> prmAgirliks) {

		try {
			BigDecimal kontrol = prmAgirliks.get(0).getKesmeAlaniIlk();
			minDWTT = prmAgirliks.get(0).getKesmeAlaniIlk();

			if (prmAgirliks.get(0).getKesmeAlaniIkinci().compareTo(kontrol) == -1) {
				minDWTT = prmAgirliks.get(0).getKesmeAlaniIkinci();
			}
			ortalamaDWTT = prmAgirliks.get(0).getOrtalamaKesmeAlani();
		} catch (Exception e) {
			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_INFO,
							"TANAP PTS RAPORU AĞIRLIK DÜŞÜRME VERİLERİNDE EKSİK VARDIR, KONTROL EDİNİZ!",
							null));
			minDWTT = BigDecimal.ZERO;
			ortalamaDWTT = BigDecimal.ZERO;
		}

	}

	public void findMinimumCekmes(List<TestCekmeSonuc> prmCekmes) {

		finalCekme = new TestCekmeSonuc();
		try {
			BigDecimal kontrol = prmCekmes.get(0).getAkmaDayanci();
			finalCekme = prmCekmes.get(0);

			for (int i = 0; i < prmCekmes.size(); i++) {
				if (prmCekmes.get(i).getAkmaDayanci().compareTo(kontrol) == -1) {
					kontrol = prmCekmes.get(i).getAkmaDayanci();
					finalCekme = prmCekmes.get(i);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_INFO,
							"TANAP PTS RAPORU ÇEKME TEST VERİLERİNDE EKSİK VARDIR, KONTROL EDİNİZ!",
							null));
			finalCekme = new TestCekmeSonuc();
		}
	}

	@SuppressWarnings("static-access")
	public void findMinimumCentiks() {

		minCVNBody = BigDecimal.ZERO;
		ortalamaCVNBody = BigDecimal.ZERO;

		try {
			TestCentikDarbeSonucManager manager = new TestCentikDarbeSonucManager();
			TestCentikDarbeSonuc prmCentik = manager
					.getAllTestCentikDarbeSonuc(1024,
							finalCekme.getPipe().getPipeId()).get(0);

			BigDecimal kontrol = prmCentik.getEnerjiIlk();
			minCVNBody = prmCentik.getEnerjiIlk();

			if (prmCentik.getEnerjiIkinci().compareTo(kontrol) == -1) {
				kontrol = prmCentik.getEnerjiIkinci();
				minCVNBody = prmCentik.getEnerjiIkinci();
			}

			if (prmCentik.getEnerjiUcuncu().compareTo(kontrol) == -1) {
				kontrol = prmCentik.getEnerjiUcuncu();
				minCVNBody = prmCentik.getEnerjiUcuncu();
			}

			ortalamaCVNBody = prmCentik.getOrtalamaEnerji();
		} catch (Exception e) {
			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_INFO,
							"TANAP PTS RAPORU ÇENTİK TEST VERİLERİNDE EKSİK VARDIR, KONTROL EDİNİZ!",
							null));
			minCVNBody = BigDecimal.ZERO;
			ortalamaCVNBody = BigDecimal.ZERO;
		}

	}

	// 3.1 Raporları

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void ucBirNominals() {

		if (selectedUcBirPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "3.1 BORULARI SEÇİLMEMİŞTİR!",
					null));
			return;
		}

		try {
			CekmeTestSpecManager cekmeManager = new CekmeTestSpecManager();
			List<CekmeTestsSpec> zNominal = new ArrayList<CekmeTestsSpec>();

			CentikDarbeTestsSpecManager centikManager = new CentikDarbeTestsSpecManager();
			List<CentikDarbeTestsSpec> kNominal = new ArrayList<CentikDarbeTestsSpec>();

			BukmeTestsSpecManager bukmeManager = new BukmeTestsSpecManager();
			List<BukmeTestsSpec> fNominal = new ArrayList<BukmeTestsSpec>();

			KaynakSertlikTestSpecManager sertlikManager = new KaynakSertlikTestSpecManager();
			List<KaynakSertlikTestsSpec> hNominal = new ArrayList<KaynakSertlikTestsSpec>();

			KaynakMacroTestSpecManager makroManager = new KaynakMacroTestSpecManager();
			List<KaynakMakroTestSpec> mNominal = new ArrayList<KaynakMakroTestSpec>();

			AgirlikTestsSpecManager agirlikManager = new AgirlikTestsSpecManager();
			List<AgirlikTestsSpec> dNominal = new ArrayList<AgirlikTestsSpec>();

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			zNominal = cekmeManager.findByItemId(selectedUcBirPipes.get(0)
					.getSalesItem().getItemId());

			kNominal = centikManager.findByItemId(selectedUcBirPipes.get(0)
					.getSalesItem().getItemId());

			fNominal = bukmeManager.findByItemId(selectedUcBirPipes.get(0)
					.getSalesItem().getItemId());

			mNominal = makroManager.findByItemId(selectedUcBirPipes.get(0)
					.getSalesItem().getItemId());

			hNominal = sertlikManager.findByItemId(selectedUcBirPipes.get(0)
					.getSalesItem().getItemId());

			dNominal = agirlikManager.seciliAgirlikTestTanim(1029,
					selectedUcBirPipes.get(0).getSalesItem().getItemId());

			Map dataMap = new HashMap();
			dataMap.put("siparis", selectedUcBirPipes.get(0).getSalesItem());
			dataMap.put("zNominal", zNominal);
			dataMap.put("kNominal", kNominal);
			dataMap.put("fNominal", fNominal);
			dataMap.put("mNominal", mNominal);
			dataMap.put("hNominal", hNominal);
			dataMap.put("dNominal", dNominal);
			dataMap.put("date", date);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/releaseNote/ucBir/nominals.xls", "-"
							+ selectedUcBirPipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "OLUŞTURULAMADI, HATA!", null));
			e.printStackTrace();
		}

		try {
			ChemicalRequirementManager manager = new ChemicalRequirementManager();
			List<ChemicalRequirement> kimyasalNominal = new ArrayList<ChemicalRequirement>();

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			kimyasalNominal = manager
					.seciliChemicalRequirementTanim(selectedUcBirPipes.get(0)
							.getSalesItem().getItemId());

			Map dataMap = new HashMap();
			dataMap.put("siparis", selectedUcBirPipes.get(0).getSalesItem());
			dataMap.put("chemNominals", kimyasalNominal);
			dataMap.put("date", date);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/releaseNote/ucBir/nominalsChemical.xls",
					"-" + selectedUcBirPipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"C03 RELEASE RAPORU BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"C03 VERİLERİ EKSİKTİR, RAPOR OLUŞTURULAMADI, HATA!", null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void ucBirChemNominals() {

		if (selectedUcBirPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "3.1 BORULARI SEÇİLMEMİŞTİR!",
					null));
			return;
		}

		try {
			ChemicalRequirementManager manager = new ChemicalRequirementManager();
			List<ChemicalRequirement> kimyasalNominal = new ArrayList<ChemicalRequirement>();

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			kimyasalNominal = manager
					.seciliChemicalRequirementTanim(selectedUcBirPipes.get(0)
							.getSalesItem().getItemId());

			Map dataMap = new HashMap();
			dataMap.put("siparis", selectedUcBirPipes.get(0).getSalesItem());
			dataMap.put("chemNominals", kimyasalNominal);
			dataMap.put("date", date);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/releaseNote/ucBir/nominalsChemical.xls",
					"-" + selectedUcBirPipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "OLUŞTURULAMADI, HATA!", null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void ucBirShippingList() {

		if (selectedUcBirPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "3.1 BORULARI SEÇİLMEMİŞTİR!",
					null));
			return;
		}

		try {

			List<String> date = new ArrayList<String>();

			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			BigDecimal totalLength = BigDecimal.ZERO;
			BigDecimal totalWeight = BigDecimal.ZERO;

			for (int i = 0; i < selectedUcBirPipes.size(); i++) {
				totalLength = totalLength.add(selectedUcBirPipes.get(i)
						.getTestTahribatsizGozOlcuSonuc().getBoruUzunluk()
						.divide(binSayisi, 2, RoundingMode.HALF_UP));
				totalWeight = totalWeight.add(selectedUcBirPipes.get(i)
						.getTestTahribatsizGozOlcuSonuc().getBoruAgirlik());
			}

			shippingListObjects.clear();

			for (int i = 0; i < selectedUcBirPipes.size(); i++) {
				shippingListObject shippingListObject = new shippingListObject();
				shippingListObject.setPipeBarkodNo(selectedUcBirPipes.get(i)
						.getPipeBarkodNo());
				shippingListObject.setRuloHeatNo(selectedUcBirPipes.get(i)
						.getRuloPipeLink().getRulo().getRuloDokumNo());
				shippingListObject.setRuloCoilNo(selectedUcBirPipes.get(i)
						.getRuloPipeLink().getRulo().getRuloBobinNo());
				shippingListObject.setBoruAgirlik(selectedUcBirPipes.get(i)
						.getTestTahribatsizGozOlcuSonuc().getBoruAgirlik());
				shippingListObject.setBoruUzunlukMetre(selectedUcBirPipes
						.get(i).getTestTahribatsizGozOlcuSonuc()
						.getBoruUzunluk()
						.divide(binSayisi, 2, RoundingMode.HALF_UP));

				shippingListObjects.add(shippingListObject);
			}

			Map dataMap = new HashMap();
			dataMap.put("siparis", selectedUcBirPipes.get(0).getSalesItem());
			dataMap.put("date", date);
			dataMap.put("cerificateNumber", "UEC-IRN-BP-00001");
			dataMap.put("count", selectedUcBirPipes.size());
			dataMap.put("pipes", shippingListObjects);
			dataMap.put("totalLength", totalLength);
			dataMap.put("totalWeight", totalWeight);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/releaseNote/ucBir/shippingList.xls",
					selectedUcBirPipes.get(0).getSalesItem().getProposal()
							.getCustomer().getShortName());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void ucBirSurveyOfLots() {

		if (selectedUcBirPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "3.1 BORULARI SEÇİLMEMİŞTİR!",
					null));
			return;
		}

		try {

			List<String> date = new ArrayList<String>();

			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			dokumBul(selectedUcBirPipes);

			Map dataMap = new HashMap();
			dataMap.put("siparis", selectedUcBirPipes.get(0).getSalesItem());
			dataMap.put("date", date);
			dataMap.put("count", selectedUcBirPipes.size());
			dataMap.put("dokumler", dokumler);
			dataMap.put("cerificateNumber", "UEC-IRN-BP-00001");

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/releaseNote/ucBir/surveyLots.xls",
					selectedUcBirPipes.get(0).getSalesItem().getProposal()
							.getCustomer().getShortName());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "static-access", "rawtypes", "unchecked" })
	public void ucBirChemicalAnalysis() {

		if (selectedUcBirPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "3.1 BORULARI SEÇİLMEMİŞTİR!",
					null));
			return;
		}

		try {

			TestKimyasalAnalizSonucManager testKimyasalAnalizSonucManager = new TestKimyasalAnalizSonucManager();
			List<TestKimyasalAnalizSonuc> allC03ByDokum = new ArrayList<TestKimyasalAnalizSonuc>();

			RuloTestKimyasalGirdiManager ruloTestKimyasalGirdiManager = new RuloTestKimyasalGirdiManager();
			List<RuloTestKimyasalGirdi> allC02ByDokum = new ArrayList<RuloTestKimyasalGirdi>();

			RuloTestKimyasalUreticiManager ruloTestKimyasalUreticiManager = new RuloTestKimyasalUreticiManager();
			List<RuloTestKimyasalUretici> allC01ByDokum = new ArrayList<RuloTestKimyasalUretici>();

			dokumBul(selectedUcBirPipes);
			chemicalObjects.clear();
			dokumlerObject.getDokumNo().clear();

			for (int i = 0; i < dokumler.size(); i++) {

				allC01ByDokum.clear();
				allC02ByDokum.clear();
				allC03ByDokum.clear();

				System.out.println(dokumler.get(i));

				if (ruloTestKimyasalUreticiManager
						.getByDokumNo(dokumler.get(i)).size() > 0) {
					allC01ByDokum.add(ruloTestKimyasalUreticiManager
							.getByDokumNo(dokumler.get(i)).get(0));
				}

				try {
					for (int j = 0; j < allC01ByDokum.size(); j++) {
						chemicalObject chemicalObject = new chemicalObject();
						chemicalObject.setHeatNo(allC01ByDokum.get(j).getRulo()
								.getRuloDokumNo());
						chemicalObject.setCode("C01");
						chemicalObject.setType("I.Product / Ürün");
						try {
							if (allC01ByDokum.get(j).getcOnEk() == null) {
								chemicalObject
										.setC(new DecimalFormat("#0.000")
												.format(allC01ByDokum
														.get(j)
														.getRuloTestKimyasalUreticiC()));
							} else {
								chemicalObject
										.setC(allC01ByDokum.get(j).getcOnEk()
												+ ""
												+ new DecimalFormat("#0.000")
														.format(allC01ByDokum
																.get(j)
																.getRuloTestKimyasalUreticiC()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setC("");
						}
						try {
							chemicalObject.setSi(new DecimalFormat("#0.000")
									.format(allC01ByDokum.get(j)
											.getRuloTestKimyasalUreticiSi()));
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setSi("");
						}
						try {
							chemicalObject.setMn(new DecimalFormat("#0.000")
									.format(allC01ByDokum.get(j)
											.getRuloTestKimyasalUreticiMn()));
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setMn("");
						}
						try {
							chemicalObject.setP(new DecimalFormat("#0.000")
									.format(allC01ByDokum.get(j)
											.getRuloTestKimyasalUreticiP()));
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setP("");
						}
						try {
							chemicalObject.setS(new DecimalFormat("#0.000")
									.format(allC01ByDokum.get(j)
											.getRuloTestKimyasalUreticiS()));
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setS("");
						}
						try {
							chemicalObject.setCr(new DecimalFormat("#0.000")
									.format(allC01ByDokum.get(j)
											.getRuloTestKimyasalUreticiCr()));
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setCr("");
						}
						try {
							chemicalObject.setNi(new DecimalFormat("#0.000")
									.format(allC01ByDokum.get(j)
											.getRuloTestKimyasalUreticiNi()));
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setNi("");
						}
						try {
							if (allC01ByDokum.get(j).getMoOnEk() == null) {
								chemicalObject
										.setMo(new DecimalFormat("#0.000")
												.format(allC01ByDokum
														.get(j)
														.getRuloTestKimyasalUreticiMo()));
							} else {
								chemicalObject
										.setMo(allC01ByDokum.get(j).getMoOnEk()
												+ ""
												+ new DecimalFormat("#0.000")
														.format(allC01ByDokum
																.get(j)
																.getRuloTestKimyasalUreticiMo()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setMo("");
						}
						try {
							if (allC01ByDokum.get(j).getCuOnEk() == null) {
								chemicalObject
										.setCu(new DecimalFormat("#0.000")
												.format(allC01ByDokum
														.get(j)
														.getRuloTestKimyasalUreticiCu()));
							} else {
								chemicalObject
										.setCu(allC01ByDokum.get(j).getCuOnEk()
												+ ""
												+ new DecimalFormat("#0.000")
														.format(allC01ByDokum
																.get(j)
																.getRuloTestKimyasalUreticiCu()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setCu("");
						}
						try {
							if (allC01ByDokum.get(j).getAlOnEk() == null) {
								chemicalObject
										.setAl(new DecimalFormat("#0.000")
												.format(allC01ByDokum
														.get(j)
														.getRuloTestKimyasalUreticiAl()));
							} else {
								chemicalObject
										.setAl(allC01ByDokum.get(j).getAlOnEk()
												+ ""
												+ new DecimalFormat("#0.000")
														.format(allC01ByDokum
																.get(j)
																.getRuloTestKimyasalUreticiAl()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setAl("");
						}
						try {
							if (allC01ByDokum.get(j).getTiOnEk() == null) {
								chemicalObject
										.setTi(new DecimalFormat("#0.000")
												.format(allC01ByDokum
														.get(j)
														.getRuloTestKimyasalUreticiTi()));
							} else {
								chemicalObject
										.setTi(allC01ByDokum.get(j).getTiOnEk()
												+ ""
												+ new DecimalFormat("#0.000")
														.format(allC01ByDokum
																.get(j)
																.getRuloTestKimyasalUreticiTi()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setTi("");
						}
						try {
							if (allC01ByDokum.get(j).getvOnEk() == null) {
								chemicalObject
										.setV(new DecimalFormat("#0.000")
												.format(allC01ByDokum
														.get(j)
														.getRuloTestKimyasalUreticiV()));
							} else {
								chemicalObject
										.setV(allC01ByDokum.get(j).getvOnEk()
												+ ""
												+ new DecimalFormat("#0.000")
														.format(allC01ByDokum
																.get(j)
																.getRuloTestKimyasalUreticiV()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setV("");
						}
						try {
							if (allC01ByDokum.get(j).getNbOnEk() == null) {
								chemicalObject
										.setNb(new DecimalFormat("#0.000")
												.format(allC01ByDokum
														.get(j)
														.getRuloTestKimyasalUreticiNb()));
							} else {
								chemicalObject
										.setNb(allC01ByDokum.get(j).getNbOnEk()
												+ ""
												+ new DecimalFormat("#0.000")
														.format(allC01ByDokum
																.get(j)
																.getRuloTestKimyasalUreticiNb()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setNb("");
						}
						try {
							if (allC01ByDokum.get(j).getwOnEk() == null) {
								chemicalObject
										.setW(new DecimalFormat("#0.000")
												.format(allC01ByDokum
														.get(j)
														.getRuloTestKimyasalUreticiW()));
							} else {
								chemicalObject
										.setW(allC01ByDokum.get(j).getwOnEk()
												+ ""
												+ new DecimalFormat("#0.000")
														.format(allC01ByDokum
																.get(j)
																.getRuloTestKimyasalUreticiW()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setW("");
						}
						try {
							if (allC01ByDokum.get(j).getbOnEk() == null) {
								chemicalObject
										.setB(new DecimalFormat("#0.0000")
												.format(allC01ByDokum
														.get(j)
														.getRuloTestKimyasalUreticiB()));
							} else {
								chemicalObject
										.setB(allC01ByDokum.get(j).getbOnEk()
												+ ""
												+ new DecimalFormat("#0.0000")
														.format(allC01ByDokum
																.get(j)
																.getRuloTestKimyasalUreticiB()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setB("");
						}
						try {
							if (allC01ByDokum.get(j).getAsOnEk() == null) {
								chemicalObject.setAs(new DecimalFormat(
										"#0.00000").format(allC01ByDokum.get(j)
										.getRuloTestKimyasalUreticiAs()));
							} else {
								chemicalObject
										.setAs(allC01ByDokum.get(j).getAsOnEk()
												+ ""
												+ new DecimalFormat("#0.00000")
														.format(allC01ByDokum
																.get(j)
																.getRuloTestKimyasalUreticiAs()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setAs("");
						}
						try {
							if (allC01ByDokum.get(j).getSbOnEk() == null) {
								chemicalObject
										.setSb(new DecimalFormat("#0.000")
												.format(allC01ByDokum
														.get(j)
														.getRuloTestKimyasalUreticiSb()));
							} else {
								chemicalObject
										.setSb(allC01ByDokum.get(j).getSbOnEk()
												+ ""
												+ new DecimalFormat("#0.000")
														.format(allC01ByDokum
																.get(j)
																.getRuloTestKimyasalUreticiSb()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setSb("");
						}
						try {
							if (allC01ByDokum.get(j).getSnOnEk() == null) {
								chemicalObject
										.setSn(new DecimalFormat("#0.000")
												.format(allC01ByDokum
														.get(j)
														.getRuloTestKimyasalUreticiSn()));
							} else {
								chemicalObject
										.setSn(allC01ByDokum.get(j).getSnOnEk()
												+ ""
												+ new DecimalFormat("#0.000")
														.format(allC01ByDokum
																.get(j)
																.getRuloTestKimyasalUreticiSn()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setSn("");
						}
						try {
							if (allC01ByDokum.get(j).getPbOnEk() == null) {
								chemicalObject
										.setPb(new DecimalFormat("#0.000")
												.format(allC01ByDokum
														.get(j)
														.getRuloTestKimyasalUreticiPb()));
							} else {
								chemicalObject
										.setPb(allC01ByDokum.get(j).getPbOnEk()
												+ ""
												+ new DecimalFormat("#0.000")
														.format(allC01ByDokum
																.get(j)
																.getRuloTestKimyasalUreticiPb()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setPb("");
						}
						try {
							if (allC01ByDokum.get(j).getCaOnEk() == null) {
								chemicalObject.setCa(new DecimalFormat(
										"#0.00000").format(allC01ByDokum.get(j)
										.getRuloTestKimyasalUreticiCa()));
							} else {
								chemicalObject
										.setCa(allC01ByDokum.get(j).getCaOnEk()
												+ ""
												+ new DecimalFormat("#0.00000")
														.format(allC01ByDokum
																.get(j)
																.getRuloTestKimyasalUreticiCa()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setCa("");
						}
						try {
							if (allC01ByDokum.get(j).getnOnEk() == null) {
								chemicalObject
										.setN(new DecimalFormat("#0.0000")
												.format(allC01ByDokum
														.get(j)
														.getRuloTestKimyasalUreticiN()));
							} else {
								chemicalObject
										.setN(allC01ByDokum.get(j).getnOnEk()
												+ ""
												+ new DecimalFormat("#0.0000")
														.format(allC01ByDokum
																.get(j)
																.getRuloTestKimyasalUreticiN()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setN("");
						}
						try {
							chemicalObject
									.setAe(new DecimalFormat("#0.000")
											.format((allC01ByDokum
													.get(j)
													.getRuloTestKimyasalUreticiTi()
													+ allC01ByDokum
															.get(j)
															.getRuloTestKimyasalUreticiV() + allC01ByDokum
													.get(j)
													.getRuloTestKimyasalUreticiNb())));
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setAe("");
						}
						try {
							chemicalObject
									.setCe(new DecimalFormat("#0.000")
											.format((allC01ByDokum
													.get(j)
													.getRuloTestKimyasalUreticiC()
													+ (allC01ByDokum
															.get(j)
															.getRuloTestKimyasalUreticiSi() / 30)
													+ (allC01ByDokum
															.get(j)
															.getRuloTestKimyasalUreticiMn() / 20)
													+ (allC01ByDokum
															.get(j)
															.getRuloTestKimyasalUreticiNi() / 60)
													+ (allC01ByDokum
															.get(j)
															.getRuloTestKimyasalUreticiNi() / 20)
													+ (allC01ByDokum
															.get(j)
															.getRuloTestKimyasalUreticiMo() / 15)
													+ (allC01ByDokum
															.get(j)
															.getRuloTestKimyasalUreticiV() / 10) + (5 * allC01ByDokum
													.get(j)
													.getRuloTestKimyasalUreticiB()))));
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setCe("");
						}

						chemicalObjects.add(chemicalObject);
					}
				} catch (Exception e) {
					e.printStackTrace();
					chemicalObjects.add(null);
				}

				if (ruloTestKimyasalGirdiManager.getByDokumNo(dokumler.get(i))
						.size() > 0) {
					allC02ByDokum.add(ruloTestKimyasalGirdiManager
							.getByDokumNo(dokumler.get(i)).get(0));
				}

				try {
					for (int j = 0; j < allC02ByDokum.size(); j++) {
						chemicalObject chemicalObject = new chemicalObject();
						chemicalObject.setHeatNo(allC02ByDokum.get(j).getRulo()
								.getRuloDokumNo());
						chemicalObject.setCode("C02");
						chemicalObject.setType("P.Product / Ürün");
						try {
							if (allC02ByDokum.get(j).getcOnEk() == null) {
								chemicalObject.setC(new DecimalFormat("#0.000")
										.format(allC02ByDokum.get(j)
												.getRuloTestKimyasalGirdiC()));
							} else {
								chemicalObject
										.setC(allC02ByDokum.get(j).getcOnEk()
												+ ""
												+ new DecimalFormat("#0.000")
														.format(allC02ByDokum
																.get(j)
																.getRuloTestKimyasalGirdiC()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setC("");
						}
						try {
							chemicalObject.setSi(new DecimalFormat("#0.000")
									.format(allC02ByDokum.get(j)
											.getRuloTestKimyasalGirdiSi()));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setSi("");
						}
						try {
							chemicalObject.setMn(new DecimalFormat("#0.000")
									.format(allC02ByDokum.get(j)
											.getRuloTestKimyasalGirdiMn()));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setMn("");
						}
						try {
							chemicalObject.setP(new DecimalFormat("#0.000")
									.format(allC02ByDokum.get(j)
											.getRuloTestKimyasalGirdiP()));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setP("");
						}
						try {
							chemicalObject.setS(new DecimalFormat("#0.000")
									.format(allC02ByDokum.get(j)
											.getRuloTestKimyasalGirdiS()));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setS("");
						}
						try {
							chemicalObject.setCr(new DecimalFormat("#0.000")
									.format(allC02ByDokum.get(j)
											.getRuloTestKimyasalGirdiCr()));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setCr("");
						}
						try {
							chemicalObject.setNi(new DecimalFormat("#0.000")
									.format(allC02ByDokum.get(j)
											.getRuloTestKimyasalGirdiNi()));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setNi("");
						}
						try {
							if (allC02ByDokum.get(j).getMoOnEk() == null) {
								chemicalObject
										.setMo(new DecimalFormat("#0.000")
												.format(allC02ByDokum
														.get(j)
														.getRuloTestKimyasalGirdiMo()));
							} else {
								chemicalObject
										.setMo(allC02ByDokum.get(j).getMoOnEk()
												+ ""
												+ new DecimalFormat("#0.000")
														.format(allC02ByDokum
																.get(j)
																.getRuloTestKimyasalGirdiMo()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setMo("");
						}
						try {
							if (allC02ByDokum.get(j).getCuOnEk() == null) {
								chemicalObject
										.setCu(new DecimalFormat("#0.000")
												.format(allC02ByDokum
														.get(j)
														.getRuloTestKimyasalGirdiCu()));
							} else {
								chemicalObject
										.setCu(allC02ByDokum.get(j).getCuOnEk()
												+ ""
												+ new DecimalFormat("#0.000")
														.format(allC02ByDokum
																.get(j)
																.getRuloTestKimyasalGirdiCu()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setCu("");
						}
						try {
							if (allC02ByDokum.get(j).getAlOnEk() == null) {
								chemicalObject
										.setAl(new DecimalFormat("#0.000")
												.format(allC02ByDokum
														.get(j)
														.getRuloTestKimyasalGirdiAl()));
							} else {
								chemicalObject
										.setAl(allC02ByDokum.get(j).getAlOnEk()
												+ ""
												+ new DecimalFormat("#0.000")
														.format(allC02ByDokum
																.get(j)
																.getRuloTestKimyasalGirdiAl()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setAl("");
						}
						try {
							if (allC02ByDokum.get(j).getTiOnEk() == null) {
								chemicalObject
										.setTi(new DecimalFormat("#0.000")
												.format(allC02ByDokum
														.get(j)
														.getRuloTestKimyasalGirdiTi()));
							} else {
								chemicalObject
										.setTi(allC02ByDokum.get(j).getTiOnEk()
												+ ""
												+ new DecimalFormat("#0.000")
														.format(allC02ByDokum
																.get(j)
																.getRuloTestKimyasalGirdiTi()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setTi("");
						}
						try {
							if (allC02ByDokum.get(j).getvOnEk() == null) {
								chemicalObject.setV(new DecimalFormat("#0.000")
										.format(allC02ByDokum.get(j)
												.getRuloTestKimyasalGirdiV()));
							} else {
								chemicalObject
										.setV(allC02ByDokum.get(j).getvOnEk()
												+ ""
												+ new DecimalFormat("#0.000")
														.format(allC02ByDokum
																.get(j)
																.getRuloTestKimyasalGirdiV()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setV("");
						}
						try {
							if (allC02ByDokum.get(j).getNbOnEk() == null) {
								chemicalObject
										.setNb(new DecimalFormat("#0.000")
												.format(allC02ByDokum
														.get(j)
														.getRuloTestKimyasalGirdiNb()));
							} else {
								chemicalObject
										.setNb(allC02ByDokum.get(j).getNbOnEk()
												+ ""
												+ new DecimalFormat("#0.000")
														.format(allC02ByDokum
																.get(j)
																.getRuloTestKimyasalGirdiNb()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setNb("");
						}
						try {
							if (allC02ByDokum.get(j).getwOnEk() == null) {
								chemicalObject.setW(new DecimalFormat("#0.000")
										.format(allC02ByDokum.get(j)
												.getRuloTestKimyasalGirdiW()));
							} else {
								chemicalObject
										.setW(allC02ByDokum.get(j).getwOnEk()
												+ ""
												+ new DecimalFormat("#0.000")
														.format(allC02ByDokum
																.get(j)
																.getRuloTestKimyasalGirdiW()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setW("");
						}
						try {
							if (allC02ByDokum.get(j).getbOnEk() == null) {
								chemicalObject
										.setB(new DecimalFormat("#0.0000")
												.format(allC02ByDokum
														.get(j)
														.getRuloTestKimyasalGirdiB()));
							} else {
								chemicalObject
										.setB(allC02ByDokum.get(j).getbOnEk()
												+ ""
												+ new DecimalFormat("#0.0000")
														.format(allC02ByDokum
																.get(j)
																.getRuloTestKimyasalGirdiB()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setB("");
						}
						try {
							if (allC02ByDokum.get(j).getAsOnEk() == null) {
								chemicalObject.setAs(new DecimalFormat(
										"#0.00000").format(allC02ByDokum.get(j)
										.getRuloTestKimyasalGirdiAs()));
							} else {
								chemicalObject
										.setAs(allC02ByDokum.get(j).getAsOnEk()
												+ ""
												+ new DecimalFormat("#0.00000")
														.format(allC02ByDokum
																.get(j)
																.getRuloTestKimyasalGirdiAs()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setAs("");
						}
						try {
							if (allC02ByDokum.get(j).getSbOnEk() == null) {
								chemicalObject
										.setSb(new DecimalFormat("#0.000")
												.format(allC02ByDokum
														.get(j)
														.getRuloTestKimyasalGirdiSb()));
							} else {
								chemicalObject
										.setSb(allC02ByDokum.get(j).getSbOnEk()
												+ ""
												+ new DecimalFormat("#0.000")
														.format(allC02ByDokum
																.get(j)
																.getRuloTestKimyasalGirdiSb()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setSb("");
						}
						try {
							if (allC02ByDokum.get(j).getSnOnEk() == null) {
								chemicalObject
										.setSn(new DecimalFormat("#0.000")
												.format(allC02ByDokum
														.get(j)
														.getRuloTestKimyasalGirdiSn()));
							} else {
								chemicalObject
										.setSn(allC02ByDokum.get(j).getSnOnEk()
												+ ""
												+ new DecimalFormat("#0.000")
														.format(allC02ByDokum
																.get(j)
																.getRuloTestKimyasalGirdiSn()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setSn("");
						}
						try {
							if (allC02ByDokum.get(j).getPbOnEk() == null) {
								chemicalObject
										.setPb(new DecimalFormat("#0.000")
												.format(allC02ByDokum
														.get(j)
														.getRuloTestKimyasalGirdiPb()));
							} else {
								chemicalObject
										.setPb(allC02ByDokum.get(j).getPbOnEk()
												+ ""
												+ new DecimalFormat("#0.000")
														.format(allC02ByDokum
																.get(j)
																.getRuloTestKimyasalGirdiPb()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setPb("");
						}
						try {
							if (allC02ByDokum.get(j).getCaOnEk() == null) {
								chemicalObject.setCa(new DecimalFormat(
										"#0.00000").format(allC02ByDokum.get(j)
										.getRuloTestKimyasalGirdiCa()));
							} else {
								chemicalObject
										.setCa(allC02ByDokum.get(j).getCaOnEk()
												+ ""
												+ new DecimalFormat("#0.00000")
														.format(allC02ByDokum
																.get(j)
																.getRuloTestKimyasalGirdiCa()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setCa("");
						}
						try {
							if (allC02ByDokum.get(j).getnOnEk() == null) {
								chemicalObject
										.setN(new DecimalFormat("#0.0000")
												.format(allC02ByDokum
														.get(j)
														.getRuloTestKimyasalGirdiN()));
							} else {
								chemicalObject
										.setN(allC02ByDokum.get(j).getnOnEk()
												+ ""
												+ new DecimalFormat("#0.0000")
														.format(allC02ByDokum
																.get(j)
																.getRuloTestKimyasalGirdiN()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setN("");
						}
						try {
							chemicalObject
									.setAe(new DecimalFormat("#0.000")
											.format((allC02ByDokum
													.get(j)
													.getRuloTestKimyasalGirdiTi()
													+ allC02ByDokum
															.get(j)
															.getRuloTestKimyasalGirdiV() + allC02ByDokum
													.get(j)
													.getRuloTestKimyasalGirdiNb())));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setAe("");
						}
						try {
							chemicalObject
									.setCe(new DecimalFormat("#0.000")
											.format((allC02ByDokum
													.get(j)
													.getRuloTestKimyasalGirdiC()
													+ (allC02ByDokum
															.get(j)
															.getRuloTestKimyasalGirdiSi() / 30)
													+ (allC02ByDokum
															.get(j)
															.getRuloTestKimyasalGirdiMn() / 20)
													+ (allC02ByDokum
															.get(j)
															.getRuloTestKimyasalGirdiNi() / 60)
													+ (allC02ByDokum
															.get(j)
															.getRuloTestKimyasalGirdiNi() / 20)
													+ (allC02ByDokum
															.get(j)
															.getRuloTestKimyasalGirdiMo() / 15)
													+ (allC02ByDokum
															.get(j)
															.getRuloTestKimyasalGirdiV() / 10) + (5 * allC02ByDokum
													.get(j)
													.getRuloTestKimyasalGirdiB()))));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setCe("");
						}

						chemicalObjects.add(chemicalObject);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					chemicalObjects.add(null);
				}

				if (testKimyasalAnalizSonucManager.getByDokumNoSalesItem(
						dokumler.get(i),
						selectedUcBirPipes.get(1).getSalesItem().getItemId())
						.size() > 0) {
					allC03ByDokum.addAll(testKimyasalAnalizSonucManager
							.getByDokumNoSalesItem(dokumler.get(i),
									selectedUcBirPipes.get(1).getSalesItem()
											.getItemId()));
				}

				try {
					for (int j = 0; j < allC03ByDokum.size(); j++) {
						chemicalObject chemicalObject = new chemicalObject();
						chemicalObject.setHeatNo(allC03ByDokum.get(j)
								.getRuloPipeLink().getRulo().getRuloDokumNo());
						chemicalObject.setCode(allC03ByDokum.get(j)
								.getBagliTestId().getCode());
						chemicalObject.setType("P.Product / Ürün");
						try {
							if (allC03ByDokum.get(j).getcSignum() == null) {
								chemicalObject.setC(new DecimalFormat("#0.000")
										.format(allC03ByDokum.get(j).getC()));
							} else {
								chemicalObject.setC(allC03ByDokum.get(j)
										.getcSignum()
										+ ""
										+ new DecimalFormat("#0.0000")
												.format(allC03ByDokum.get(j)
														.getC()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setC("");
						}
						try {
							chemicalObject.setSi(new DecimalFormat("#0.000")
									.format(allC03ByDokum.get(j).getSi()));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setSi("");
						}
						try {
							chemicalObject.setMn(new DecimalFormat("#0.000")
									.format(allC03ByDokum.get(j).getMn()));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setMn("");
						}
						try {
							chemicalObject.setP(new DecimalFormat("#0.000")
									.format(allC03ByDokum.get(j).getP()));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setP("");
						}
						try {
							chemicalObject.setS(new DecimalFormat("#0.000")
									.format(allC03ByDokum.get(j).getS()));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setS("");
						}
						try {
							chemicalObject.setCr(new DecimalFormat("#0.000")
									.format(allC03ByDokum.get(j).getCr()));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setCr("");
						}
						try {
							chemicalObject.setNi(new DecimalFormat("#0.000")
									.format(allC03ByDokum.get(j).getNi()));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setNi("");
						}
						try {
							if (allC03ByDokum.get(j).getMoSignum() == null) {
								chemicalObject
										.setMo(new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getMo()));
							} else {
								chemicalObject.setMo(allC03ByDokum.get(j)
										.getMoSignum()
										+ ""
										+ new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getMo()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setMo("");
						}
						try {
							if (allC03ByDokum.get(j).getCuSignum() == null) {
								chemicalObject
										.setCu(new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getCu()));
							} else {
								chemicalObject.setCu(allC03ByDokum.get(j)
										.getCuSignum()
										+ ""
										+ new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getCu()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setCu("");
						}
						try {
							if (allC03ByDokum.get(j).getAlSignum() == null) {
								chemicalObject
										.setAl(new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getAl()));
							} else {
								chemicalObject.setAl(allC03ByDokum.get(j)
										.getAlSignum()
										+ ""
										+ new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getAl()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setAl("");
						}
						try {
							if (allC03ByDokum.get(j).getTiSignum() == null) {
								chemicalObject
										.setTi(new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getTi()));
							} else {
								chemicalObject.setTi(allC03ByDokum.get(j)
										.getTiSignum()
										+ ""
										+ new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getTi()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setTi("");
						}
						try {
							if (allC03ByDokum.get(j).getvSignum() == null) {
								chemicalObject.setV(new DecimalFormat("#0.000")
										.format(allC03ByDokum.get(j).getV()));
							} else {
								chemicalObject.setV(allC03ByDokum.get(j)
										.getvSignum()
										+ ""
										+ new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getV()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setV("");
						}
						try {
							if (allC03ByDokum.get(j).getNbSignum() == null) {
								chemicalObject
										.setNb(new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getNb()));
							} else {
								chemicalObject.setNb(allC03ByDokum.get(j)
										.getNbSignum()
										+ ""
										+ new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getNb()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setNb("");
						}
						try {
							if (allC03ByDokum.get(j).getwSignum() == null) {
								chemicalObject.setW(new DecimalFormat("#0.000")
										.format(allC03ByDokum.get(j).getW()));
							} else {
								chemicalObject.setW(allC03ByDokum.get(j)
										.getwSignum()
										+ ""
										+ new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getW()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setW("");
						}
						try {
							if (allC03ByDokum.get(j).getbSignum() == null) {
								chemicalObject
										.setB(new DecimalFormat("#0.0000")
												.format(allC03ByDokum.get(j)
														.getB()));
							} else {
								chemicalObject.setB(allC03ByDokum.get(j)
										.getbSignum()
										+ ""
										+ new DecimalFormat("#0.0000")
												.format(allC03ByDokum.get(j)
														.getB()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setB("");
						}
						try {
							if (allC03ByDokum.get(j).getAsSignum() == null) {
								chemicalObject.setAs(new DecimalFormat(
										"#0.00000").format(allC03ByDokum.get(j)
										.getAs()));
							} else {
								chemicalObject.setAs(allC03ByDokum.get(j)
										.getAsSignum()
										+ ""
										+ new DecimalFormat("#0.00000")
												.format(allC03ByDokum.get(j)
														.getAs()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setAs("");
						}
						try {
							if (allC03ByDokum.get(j).getSbSignum() == null) {
								chemicalObject
										.setSb(new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getSb()));
							} else {
								chemicalObject.setSb(allC03ByDokum.get(j)
										.getSbSignum()
										+ ""
										+ new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getSb()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setSb("");
						}
						try {
							if (allC03ByDokum.get(j).getSnSignum() == null) {
								chemicalObject
										.setSn(new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getSn()));
							} else {
								chemicalObject.setSn(allC03ByDokum.get(j)
										.getSnSignum()
										+ ""
										+ new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getSn()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setSn("");
						}
						try {
							if (allC03ByDokum.get(j).getPbSignum() == null) {
								chemicalObject
										.setPb(new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getPb()));
							} else {
								chemicalObject.setPb(allC03ByDokum.get(j)
										.getPbSignum()
										+ ""
										+ new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getPb()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setPb("");
						}
						try {
							if (allC03ByDokum.get(j).getCaSignum() == null) {
								chemicalObject.setCa(new DecimalFormat(
										"#0.00000").format(allC03ByDokum.get(j)
										.getCa()));
							} else {
								chemicalObject.setCa(allC03ByDokum.get(j)
										.getCaSignum()
										+ ""
										+ new DecimalFormat("#0.00000")
												.format(allC03ByDokum.get(j)
														.getCa()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setCa("");
						}
						try {
							if (allC03ByDokum.get(j).getnSignum() == null) {
								chemicalObject
										.setN(new DecimalFormat("#0.0000")
												.format(allC03ByDokum.get(j)
														.getN()));
							} else {
								chemicalObject.setN(allC03ByDokum.get(j)
										.getnSignum()
										+ ""
										+ new DecimalFormat("#0.0000")
												.format(allC03ByDokum.get(j)
														.getN()));
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setN("");
						}
						try {
							chemicalObject
									.setAe(new DecimalFormat("#0.000")
											.format((allC03ByDokum
													.get(j)
													.getTi()
													.add(allC03ByDokum.get(j)
															.getV())
													.add(allC03ByDokum.get(j)
															.getNb()))));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setAe("");
						}
						try {
							chemicalObject
									.setCe(new DecimalFormat("#0.000").format((allC03ByDokum
											.get(j)
											.getC()
											.add(allC03ByDokum.get(j).getSi()
													.divide(otuzSayisi, 3))
											.add(allC03ByDokum.get(j).getMn()
													.divide(yirmiSayisi, 3))
											.add(allC03ByDokum
													.get(j)
													.getNi()
													.divide(altmisSayisi, 3)
													.add(allC03ByDokum
															.get(j)
															.getNi()
															.divide(yirmiSayisi,
																	3)
															.add(allC03ByDokum
																	.get(j)
																	.getMo()
																	.divide(onBesSayisi,
																			3))
															.add(allC03ByDokum
																	.get(j)
																	.getV()
																	.divide(onSayisi,
																			3)
																	.add(allC03ByDokum
																			.get(j)
																			.getB()
																			.multiply(
																					besSayisi))))))));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							chemicalObject.setCe("");
						}

						chemicalObjects.add(chemicalObject);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					chemicalObjects.add(null);
				}
			}

			if (dokumler.size() > 0) {
				dokumlerObject.getDokumNo().addAll(dokumler);
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			System.out.println(allC01ByDokum.size() + " - "
					+ allC02ByDokum.size() + " - " + allC03ByDokum.size());

			Map dataMap = new HashMap();
			dataMap.put("siparis", selectedUcBirPipes.get(0).getSalesItem());
			dataMap.put("sonucs", chemicalObjects);
			dataMap.put("dokumler", dokumlerObject);
			dataMap.put("date", date);
			dataMap.put("cerificateNumber", "UEC-IRN-BP-00001");

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/releaseNote/ucBir/chemical.xls", "-"
							+ selectedUcBirPipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"KİMYASAL RELEASE RAPORU BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"KİMYASAL VERİLERİ EKSİKTİR, RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void ucBirTensileTests() {

		if (selectedUcBirPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "3.1 BORULARI SEÇİLMEMİŞTİR!",
					null));
			return;
		}

		try {

			TestCekmeSonucManager testCekmeSonucManager = new TestCekmeSonucManager();
			List<TestCekmeSonuc> allCekmesByDokum = new ArrayList<TestCekmeSonuc>();

			dokumBul(selectedUcBirPipes);

			for (int i = 0; i < dokumler.size(); i++) {
				if (testCekmeSonucManager.getByDokumNoSalesItem(
						dokumler.get(i),
						selectedUcBirPipes.get(1).getSalesItem().getItemId())
						.size() > 0) {
					allCekmesByDokum.addAll(testCekmeSonucManager
							.getByDokumNoSalesItem(dokumler.get(i),
									selectedUcBirPipes.get(1).getSalesItem()
											.getItemId()));
				}
			}

			if (dokumler.size() > 0) {
				dokumlerObject.getDokumNo().addAll(dokumler);
			}

			List<String> date = new ArrayList<String>();
			// date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(allCekmesByDokum
					.get(0).getEklemeZamani()));

			Map dataMap = new HashMap();
			dataMap.put("siparis", selectedUcBirPipes.get(0).getSalesItem());
			dataMap.put("sonucs", allCekmesByDokum);
			dataMap.put("dokumler", dokumlerObject);
			dataMap.put("date", date);
			dataMap.put("cerificateNumber", "UEC-IRN-BP-00001");

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/releaseNote/ucBir/tensile.xls", "-"
							+ selectedUcBirPipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"ÇEKME RELEASE RAPORU BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÇEKME VERİLERİ EKSİKTİR, RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void ucBirFructureTests() {

		if (selectedUcBirPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "3.1 BORULARI SEÇİLMEMİŞTİR!",
					null));
			return;
		}

		try {

			TestCentikDarbeSonucManager manager = new TestCentikDarbeSonucManager();
			List<TestCentikDarbeSonuc> allCentiksByDokum = new ArrayList<TestCentikDarbeSonuc>();

			dokumBul(selectedUcBirPipes);

			for (int i = 0; i < dokumler.size(); i++) {
				if (manager.getByDokumNoSalesItem(dokumler.get(i),
						selectedUcBirPipes.get(1).getSalesItem().getItemId())
						.size() > 0) {
					allCentiksByDokum.addAll(manager.getByDokumNoSalesItem(
							dokumler.get(i), selectedUcBirPipes.get(1)
									.getSalesItem().getItemId()));
				}
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			Map dataMap = new HashMap();
			dataMap.put("siparis", selectedUcBirPipes.get(0).getSalesItem());
			dataMap.put("date", date);
			dataMap.put("count", selectedUcBirPipes.size());
			dataMap.put("dokumler", dokumler);
			dataMap.put("sonucs", allCentiksByDokum);
			dataMap.put("cerificateNumber", "UEC-IRN-BP-00001");

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/releaseNote/ucBir/charpy.xls",
					selectedUcBirPipes.get(0).getSalesItem().getProposal()
							.getCustomer().getShortName());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "static-access", "rawtypes", "unchecked" })
	public void ucBirDropWeightTests() {

		if (selectedUcBirPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "3.1 BORULARI SEÇİLMEMİŞTİR!",
					null));
			return;
		}

		try {

			List<String> date = new ArrayList<String>();

			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			dokumBul(selectedUcBirPipes);

			List<TestAgirlikDusurmeSonuc> allAgirliksByDokum = new ArrayList<TestAgirlikDusurmeSonuc>();

			for (int i = 0; i < dokumler.size(); i++) {
				TestAgirlikDusurmeSonucManager manager = new TestAgirlikDusurmeSonucManager();
				allAgirliksByDokum
						.addAll(manager.getByDokumNo(dokumler.get(i)));
			}

			Map dataMap = new HashMap();
			dataMap.put("siparis", selectedUcBirPipes.get(0).getSalesItem());
			dataMap.put("date", date);
			dataMap.put("count", selectedUcBirPipes.size());
			dataMap.put("sonucs", allAgirliksByDokum);
			dataMap.put("cerificateNumber", "UEC-IRN-BP-00001");

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/releaseNote/ucBir/dropWeight.xls",
					selectedUcBirPipes.get(0).getSalesItem().getProposal()
							.getCustomer().getShortName());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void ucBirGuidedBendTests() {

		if (selectedUcBirPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "3.1 BORULARI SEÇİLMEMİŞTİR!",
					null));
			return;
		}

		try {

			TestBukmeSonucManager manager = new TestBukmeSonucManager();
			List<TestBukmeSonuc> allBukmesByDokum = new ArrayList<TestBukmeSonuc>();

			dokumBul(selectedUcBirPipes);

			for (int i = 0; i < dokumler.size(); i++) {
				if (manager.getByDokumNoSalesItem(dokumler.get(i),
						selectedUcBirPipes.get(1).getSalesItem().getItemId())
						.size() > 0) {
					allBukmesByDokum.addAll(manager.getByDokumNoSalesItem(
							dokumler.get(i), selectedUcBirPipes.get(1)
									.getSalesItem().getItemId()));
				}
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			Map dataMap = new HashMap();
			dataMap.put("siparis", selectedUcBirPipes.get(0).getSalesItem());
			dataMap.put("date", date);
			dataMap.put("count", selectedUcBirPipes.size());
			dataMap.put("dokumler", dokumler);
			dataMap.put("sonucs", allBukmesByDokum);
			dataMap.put("cerificateNumber", "UEC-IRN-BP-00001");

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/releaseNote/ucBir/guidedBend.xls",
					selectedUcBirPipes.get(0).getSalesItem().getProposal()
							.getCustomer().getShortName());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "static-access", "rawtypes", "unchecked" })
	public void ucBirHardnessTests() {

		if (selectedUcBirPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "3.1 BORULARI SEÇİLMEMİŞTİR!",
					null));
			return;
		}

		try {

			List<String> date = new ArrayList<String>();

			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			dokumBul(selectedUcBirPipes);

			List<TestKaynakSonuc> allSertlikByDokum = new ArrayList<TestKaynakSonuc>();

			for (int i = 0; i < dokumler.size(); i++) {
				TestKaynakSonucManager manager = new TestKaynakSonucManager();
				allSertlikByDokum.addAll(manager.getByDokumNoGlobalId(
						dokumler.get(i), 1032));
			}

			Map dataMap = new HashMap();
			dataMap.put("siparis", selectedUcBirPipes.get(0).getSalesItem());
			dataMap.put("date", date);
			dataMap.put("count", selectedUcBirPipes.size());
			dataMap.put("sonucs", allSertlikByDokum);
			dataMap.put("cerificateNumber", "UEC-IRN-BP-00001");

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/releaseNote/ucBir/hardness.xls",
					selectedUcBirPipes.get(0).getSalesItem().getProposal()
							.getCustomer().getShortName());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "static-access", "rawtypes", "unchecked" })
	public void ucBirMacrographicTests() {

		if (selectedUcBirPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "3.1 BORULARI SEÇİLMEMİŞTİR!",
					null));
			return;
		}

		try {

			List<String> date = new ArrayList<String>();

			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			dokumBul(selectedUcBirPipes);

			List<TestKaynakSonuc> allMakroByDokum = new ArrayList<TestKaynakSonuc>();

			for (int i = 0; i < dokumler.size(); i++) {
				TestKaynakSonucManager manager = new TestKaynakSonucManager();
				allMakroByDokum.addAll(manager.getByDokumNoGlobalId(
						dokumler.get(i), 1031));
			}

			Map dataMap = new HashMap();
			dataMap.put("siparis", selectedUcBirPipes.get(0).getSalesItem());
			dataMap.put("date", date);
			dataMap.put("count", selectedUcBirPipes.size());
			dataMap.put("sonucs", allMakroByDokum);
			dataMap.put("cerificateNumber", "UEC-IRN-BP-00001");

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/releaseNote/ucBir/makro.xls",
					selectedUcBirPipes.get(0).getSalesItem().getProposal()
							.getCustomer().getShortName());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void ucBirSertifikaOlustur() {

		if (selectedUcBirPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "3.1 BORULARI SEÇİLMEMİŞTİR!",
					null));
			return;
		}

		try {

			List<String> date = new ArrayList<String>();

			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			BigDecimal totalLength = null;
			BigDecimal totalWeight = null;

			for (int i = 0; i < selectedUcBirPipes.size(); i++) {
				totalLength = totalLength.add(selectedUcBirPipes.get(i)
						.getTestTahribatsizGozOlcuSonuc().getBoruUzunluk());
				totalWeight = totalWeight.add(selectedUcBirPipes.get(i)
						.getTestTahribatsizGozOlcuSonuc().getBoruAgirlik());
			}

			Map dataMap = new HashMap();
			dataMap.put("siparis", selectedUcBirPipes.get(0).getSalesItem());
			dataMap.put("date", date);
			dataMap.put("count", selectedUcBirPipes.size());
			dataMap.put("totalLength", totalLength);
			dataMap.put("totalWeight", totalWeight);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/releaseNote/3.2.xls", selectedUcBirPipes
							.get(0).getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-"
							+ selectedUcBirPipes.get(0).getSalesItem()
									.getItemNo());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "static-access", "rawtypes", "unchecked" })
	public void ucBirMTCKimyasal() {

		if (selectedUcBirPipes.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "3.1 BORULARI SEÇİLMEMİŞTİR!",
					null));
			return;
		}

		try {

			TestKimyasalAnalizSonucManager testKimyasalAnalizSonucManager = new TestKimyasalAnalizSonucManager();
			List<TestKimyasalAnalizSonuc> allC03ByDokum = new ArrayList<TestKimyasalAnalizSonuc>();

			// RuloTestKimyasalGirdiManager ruloTestKimyasalGirdiManager = new
			// RuloTestKimyasalGirdiManager();
			// List<RuloTestKimyasalGirdi> allC02ByDokum = new
			// ArrayList<RuloTestKimyasalGirdi>();
			//
			// RuloTestKimyasalUreticiManager ruloTestKimyasalUreticiManager =
			// new RuloTestKimyasalUreticiManager();
			// List<RuloTestKimyasalUretici> allC01ByDokum = new
			// ArrayList<RuloTestKimyasalUretici>();

			dokumBul(selectedUcBirPipes);
			chemicalObjects.clear();
			dokumlerObject.getDokumNo().clear();

			for (int i = 0; i < dokumler.size(); i++) {

				// allC01ByDokum.clear();
				// allC02ByDokum.clear();
				allC03ByDokum.clear();

				// System.out.println(dokumler.get(i));

				// if (ruloTestKimyasalUreticiManager
				// .getByDokumNo(dokumler.get(i)).size() > 0) {
				// allC01ByDokum.add(ruloTestKimyasalUreticiManager
				// .getByDokumNo(dokumler.get(i)).get(0));
				// }

				// try {
				// for (int j = 0; j < allC01ByDokum.size(); j++) {
				// chemicalObject chemicalObject = new chemicalObject();
				// chemicalObject.setHeatNo(allC01ByDokum.get(j).getRulo()
				// .getRuloDokumNo());
				// chemicalObject.setCode("C01");
				// chemicalObject.setType("I.Product / Ürün");
				// try {
				// if (allC01ByDokum.get(j).getcOnEk() == null) {
				// chemicalObject
				// .setC(new DecimalFormat("#0.000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiC()));
				// } else {
				// chemicalObject
				// .setC(allC01ByDokum.get(j).getcOnEk()
				// + ""
				// + new DecimalFormat("#0.000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiC()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setC("");
				// }
				// try {
				// chemicalObject.setSi(new DecimalFormat("#0.000")
				// .format(allC01ByDokum.get(j)
				// .getRuloTestKimyasalUreticiSi()));
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setSi("");
				// }
				// try {
				// chemicalObject.setMn(new DecimalFormat("#0.000")
				// .format(allC01ByDokum.get(j)
				// .getRuloTestKimyasalUreticiMn()));
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setMn("");
				// }
				// try {
				// chemicalObject.setP(new DecimalFormat("#0.000")
				// .format(allC01ByDokum.get(j)
				// .getRuloTestKimyasalUreticiP()));
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setP("");
				// }
				// try {
				// chemicalObject.setS(new DecimalFormat("#0.000")
				// .format(allC01ByDokum.get(j)
				// .getRuloTestKimyasalUreticiS()));
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setS("");
				// }
				// try {
				// chemicalObject.setCr(new DecimalFormat("#0.000")
				// .format(allC01ByDokum.get(j)
				// .getRuloTestKimyasalUreticiCr()));
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setCr("");
				// }
				// try {
				// chemicalObject.setNi(new DecimalFormat("#0.000")
				// .format(allC01ByDokum.get(j)
				// .getRuloTestKimyasalUreticiNi()));
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setNi("");
				// }
				// try {
				// if (allC01ByDokum.get(j).getMoOnEk() == null) {
				// chemicalObject
				// .setMo(new DecimalFormat("#0.000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiMo()));
				// } else {
				// chemicalObject
				// .setMo(allC01ByDokum.get(j).getMoOnEk()
				// + ""
				// + new DecimalFormat("#0.000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiMo()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setMo("");
				// }
				// try {
				// if (allC01ByDokum.get(j).getCuOnEk() == null) {
				// chemicalObject
				// .setCu(new DecimalFormat("#0.000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiCu()));
				// } else {
				// chemicalObject
				// .setCu(allC01ByDokum.get(j).getCuOnEk()
				// + ""
				// + new DecimalFormat("#0.000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiCu()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setCu("");
				// }
				// try {
				// if (allC01ByDokum.get(j).getAlOnEk() == null) {
				// chemicalObject
				// .setAl(new DecimalFormat("#0.000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiAl()));
				// } else {
				// chemicalObject
				// .setAl(allC01ByDokum.get(j).getAlOnEk()
				// + ""
				// + new DecimalFormat("#0.000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiAl()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setAl("");
				// }
				// try {
				// if (allC01ByDokum.get(j).getTiOnEk() == null) {
				// chemicalObject
				// .setTi(new DecimalFormat("#0.000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiTi()));
				// } else {
				// chemicalObject
				// .setTi(allC01ByDokum.get(j).getTiOnEk()
				// + ""
				// + new DecimalFormat("#0.000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiTi()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setTi("");
				// }
				// try {
				// if (allC01ByDokum.get(j).getvOnEk() == null) {
				// chemicalObject
				// .setV(new DecimalFormat("#0.000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiV()));
				// } else {
				// chemicalObject
				// .setV(allC01ByDokum.get(j).getvOnEk()
				// + ""
				// + new DecimalFormat("#0.000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiV()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setV("");
				// }
				// try {
				// if (allC01ByDokum.get(j).getNbOnEk() == null) {
				// chemicalObject
				// .setNb(new DecimalFormat("#0.000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiNb()));
				// } else {
				// chemicalObject
				// .setNb(allC01ByDokum.get(j).getNbOnEk()
				// + ""
				// + new DecimalFormat("#0.000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiNb()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setNb("");
				// }
				// try {
				// if (allC01ByDokum.get(j).getwOnEk() == null) {
				// chemicalObject
				// .setW(new DecimalFormat("#0.000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiW()));
				// } else {
				// chemicalObject
				// .setW(allC01ByDokum.get(j).getwOnEk()
				// + ""
				// + new DecimalFormat("#0.000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiW()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setW("");
				// }
				// try {
				// if (allC01ByDokum.get(j).getbOnEk() == null) {
				// chemicalObject
				// .setB(new DecimalFormat("#0.0000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiB()));
				// } else {
				// chemicalObject
				// .setB(allC01ByDokum.get(j).getbOnEk()
				// + ""
				// + new DecimalFormat("#0.0000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiB()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setB("");
				// }
				// try {
				// if (allC01ByDokum.get(j).getAsOnEk() == null) {
				// chemicalObject.setAs(new DecimalFormat(
				// "#0.00000").format(allC01ByDokum.get(j)
				// .getRuloTestKimyasalUreticiAs()));
				// } else {
				// chemicalObject
				// .setAs(allC01ByDokum.get(j).getAsOnEk()
				// + ""
				// + new DecimalFormat("#0.00000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiAs()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setAs("");
				// }
				// try {
				// if (allC01ByDokum.get(j).getSbOnEk() == null) {
				// chemicalObject
				// .setSb(new DecimalFormat("#0.000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiSb()));
				// } else {
				// chemicalObject
				// .setSb(allC01ByDokum.get(j).getSbOnEk()
				// + ""
				// + new DecimalFormat("#0.000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiSb()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setSb("");
				// }
				// try {
				// if (allC01ByDokum.get(j).getSnOnEk() == null) {
				// chemicalObject
				// .setSn(new DecimalFormat("#0.000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiSn()));
				// } else {
				// chemicalObject
				// .setSn(allC01ByDokum.get(j).getSnOnEk()
				// + ""
				// + new DecimalFormat("#0.000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiSn()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setSn("");
				// }
				// try {
				// if (allC01ByDokum.get(j).getPbOnEk() == null) {
				// chemicalObject
				// .setPb(new DecimalFormat("#0.000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiPb()));
				// } else {
				// chemicalObject
				// .setPb(allC01ByDokum.get(j).getPbOnEk()
				// + ""
				// + new DecimalFormat("#0.000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiPb()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setPb("");
				// }
				// try {
				// if (allC01ByDokum.get(j).getCaOnEk() == null) {
				// chemicalObject.setCa(new DecimalFormat(
				// "#0.00000").format(allC01ByDokum.get(j)
				// .getRuloTestKimyasalUreticiCa()));
				// } else {
				// chemicalObject
				// .setCa(allC01ByDokum.get(j).getCaOnEk()
				// + ""
				// + new DecimalFormat("#0.00000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiCa()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setCa("");
				// }
				// try {
				// if (allC01ByDokum.get(j).getnOnEk() == null) {
				// chemicalObject
				// .setN(new DecimalFormat("#0.0000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiN()));
				// } else {
				// chemicalObject
				// .setN(allC01ByDokum.get(j).getnOnEk()
				// + ""
				// + new DecimalFormat("#0.0000")
				// .format(allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiN()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setN("");
				// }
				// try {
				// chemicalObject
				// .setAe(new DecimalFormat("#0.000")
				// .format((allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiTi()
				// + allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiV() + allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiNb())));
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setAe("");
				// }
				// try {
				// chemicalObject
				// .setCe(new DecimalFormat("#0.000")
				// .format((allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiC()
				// + (allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiSi() / 30)
				// + (allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiMn() / 20)
				// + (allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiNi() / 60)
				// + (allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiNi() / 20)
				// + (allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiMo() / 15)
				// + (allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiV() / 10) + (5 * allC01ByDokum
				// .get(j)
				// .getRuloTestKimyasalUreticiB()))));
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setCe("");
				// }
				//
				// chemicalObjects.add(chemicalObject);
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObjects.add(null);
				// }
				//
				// if
				// (ruloTestKimyasalGirdiManager.getByDokumNo(dokumler.get(i))
				// .size() > 0) {
				// allC02ByDokum.add(ruloTestKimyasalGirdiManager
				// .getByDokumNo(dokumler.get(i)).get(0));
				// }
				//
				// try {
				// for (int j = 0; j < allC02ByDokum.size(); j++) {
				// chemicalObject chemicalObject = new chemicalObject();
				// chemicalObject.setHeatNo(allC02ByDokum.get(j).getRulo()
				// .getRuloDokumNo());
				// chemicalObject.setCode("C02");
				// chemicalObject.setType("P.Product / Ürün");
				// try {
				// if (allC02ByDokum.get(j).getcOnEk() == null) {
				// chemicalObject.setC(new DecimalFormat("#0.000")
				// .format(allC02ByDokum.get(j)
				// .getRuloTestKimyasalGirdiC()));
				// } else {
				// chemicalObject
				// .setC(allC02ByDokum.get(j).getcOnEk()
				// + ""
				// + new DecimalFormat("#0.000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiC()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setC("");
				// }
				// try {
				// chemicalObject.setSi(new DecimalFormat("#0.000")
				// .format(allC02ByDokum.get(j)
				// .getRuloTestKimyasalGirdiSi()));
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setSi("");
				// }
				// try {
				// chemicalObject.setMn(new DecimalFormat("#0.000")
				// .format(allC02ByDokum.get(j)
				// .getRuloTestKimyasalGirdiMn()));
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setMn("");
				// }
				// try {
				// chemicalObject.setP(new DecimalFormat("#0.000")
				// .format(allC02ByDokum.get(j)
				// .getRuloTestKimyasalGirdiP()));
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setP("");
				// }
				// try {
				// chemicalObject.setS(new DecimalFormat("#0.000")
				// .format(allC02ByDokum.get(j)
				// .getRuloTestKimyasalGirdiS()));
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setS("");
				// }
				// try {
				// chemicalObject.setCr(new DecimalFormat("#0.000")
				// .format(allC02ByDokum.get(j)
				// .getRuloTestKimyasalGirdiCr()));
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setCr("");
				// }
				// try {
				// chemicalObject.setNi(new DecimalFormat("#0.000")
				// .format(allC02ByDokum.get(j)
				// .getRuloTestKimyasalGirdiNi()));
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setNi("");
				// }
				// try {
				// if (allC02ByDokum.get(j).getMoOnEk() == null) {
				// chemicalObject
				// .setMo(new DecimalFormat("#0.000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiMo()));
				// } else {
				// chemicalObject
				// .setMo(allC02ByDokum.get(j).getMoOnEk()
				// + ""
				// + new DecimalFormat("#0.000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiMo()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setMo("");
				// }
				// try {
				// if (allC02ByDokum.get(j).getCuOnEk() == null) {
				// chemicalObject
				// .setCu(new DecimalFormat("#0.000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiCu()));
				// } else {
				// chemicalObject
				// .setCu(allC02ByDokum.get(j).getCuOnEk()
				// + ""
				// + new DecimalFormat("#0.000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiCu()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setCu("");
				// }
				// try {
				// if (allC02ByDokum.get(j).getAlOnEk() == null) {
				// chemicalObject
				// .setAl(new DecimalFormat("#0.000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiAl()));
				// } else {
				// chemicalObject
				// .setAl(allC02ByDokum.get(j).getAlOnEk()
				// + ""
				// + new DecimalFormat("#0.000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiAl()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setAl("");
				// }
				// try {
				// if (allC02ByDokum.get(j).getTiOnEk() == null) {
				// chemicalObject
				// .setTi(new DecimalFormat("#0.000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiTi()));
				// } else {
				// chemicalObject
				// .setTi(allC02ByDokum.get(j).getTiOnEk()
				// + ""
				// + new DecimalFormat("#0.000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiTi()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setTi("");
				// }
				// try {
				// if (allC02ByDokum.get(j).getvOnEk() == null) {
				// chemicalObject.setV(new DecimalFormat("#0.000")
				// .format(allC02ByDokum.get(j)
				// .getRuloTestKimyasalGirdiV()));
				// } else {
				// chemicalObject
				// .setV(allC02ByDokum.get(j).getvOnEk()
				// + ""
				// + new DecimalFormat("#0.000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiV()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setV("");
				// }
				// try {
				// if (allC02ByDokum.get(j).getNbOnEk() == null) {
				// chemicalObject
				// .setNb(new DecimalFormat("#0.000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiNb()));
				// } else {
				// chemicalObject
				// .setNb(allC02ByDokum.get(j).getNbOnEk()
				// + ""
				// + new DecimalFormat("#0.000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiNb()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setNb("");
				// }
				// try {
				// if (allC02ByDokum.get(j).getwOnEk() == null) {
				// chemicalObject.setW(new DecimalFormat("#0.000")
				// .format(allC02ByDokum.get(j)
				// .getRuloTestKimyasalGirdiW()));
				// } else {
				// chemicalObject
				// .setW(allC02ByDokum.get(j).getwOnEk()
				// + ""
				// + new DecimalFormat("#0.000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiW()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setW("");
				// }
				// try {
				// if (allC02ByDokum.get(j).getbOnEk() == null) {
				// chemicalObject
				// .setB(new DecimalFormat("#0.0000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiB()));
				// } else {
				// chemicalObject
				// .setB(allC02ByDokum.get(j).getbOnEk()
				// + ""
				// + new DecimalFormat("#0.0000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiB()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setB("");
				// }
				// try {
				// if (allC02ByDokum.get(j).getAsOnEk() == null) {
				// chemicalObject.setAs(new DecimalFormat(
				// "#0.00000").format(allC02ByDokum.get(j)
				// .getRuloTestKimyasalGirdiAs()));
				// } else {
				// chemicalObject
				// .setAs(allC02ByDokum.get(j).getAsOnEk()
				// + ""
				// + new DecimalFormat("#0.00000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiAs()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setAs("");
				// }
				// try {
				// if (allC02ByDokum.get(j).getSbOnEk() == null) {
				// chemicalObject
				// .setSb(new DecimalFormat("#0.000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiSb()));
				// } else {
				// chemicalObject
				// .setSb(allC02ByDokum.get(j).getSbOnEk()
				// + ""
				// + new DecimalFormat("#0.000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiSb()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setSb("");
				// }
				// try {
				// if (allC02ByDokum.get(j).getSnOnEk() == null) {
				// chemicalObject
				// .setSn(new DecimalFormat("#0.000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiSn()));
				// } else {
				// chemicalObject
				// .setSn(allC02ByDokum.get(j).getSnOnEk()
				// + ""
				// + new DecimalFormat("#0.000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiSn()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setSn("");
				// }
				// try {
				// if (allC02ByDokum.get(j).getPbOnEk() == null) {
				// chemicalObject
				// .setPb(new DecimalFormat("#0.000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiPb()));
				// } else {
				// chemicalObject
				// .setPb(allC02ByDokum.get(j).getPbOnEk()
				// + ""
				// + new DecimalFormat("#0.000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiPb()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setPb("");
				// }
				// try {
				// if (allC02ByDokum.get(j).getCaOnEk() == null) {
				// chemicalObject.setCa(new DecimalFormat(
				// "#0.00000").format(allC02ByDokum.get(j)
				// .getRuloTestKimyasalGirdiCa()));
				// } else {
				// chemicalObject
				// .setCa(allC02ByDokum.get(j).getCaOnEk()
				// + ""
				// + new DecimalFormat("#0.00000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiCa()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setCa("");
				// }
				// try {
				// if (allC02ByDokum.get(j).getnOnEk() == null) {
				// chemicalObject
				// .setN(new DecimalFormat("#0.0000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiN()));
				// } else {
				// chemicalObject
				// .setN(allC02ByDokum.get(j).getnOnEk()
				// + ""
				// + new DecimalFormat("#0.0000")
				// .format(allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiN()));
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setN("");
				// }
				// try {
				// chemicalObject
				// .setAe(new DecimalFormat("#0.000")
				// .format((allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiTi()
				// + allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiV() + allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiNb())));
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setAe("");
				// }
				// try {
				// chemicalObject
				// .setCe(new DecimalFormat("#0.000")
				// .format((allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiC()
				// + (allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiSi() / 30)
				// + (allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiMn() / 20)
				// + (allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiNi() / 60)
				// + (allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiNi() / 20)
				// + (allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiMo() / 15)
				// + (allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiV() / 10) + (5 * allC02ByDokum
				// .get(j)
				// .getRuloTestKimyasalGirdiB()))));
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObject.setCe("");
				// }
				//
				// chemicalObjects.add(chemicalObject);
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// chemicalObjects.add(null);
				// }

				if (testKimyasalAnalizSonucManager.getByDokumNoSalesItem(
						dokumler.get(i),
						selectedUcBirPipes.get(1).getSalesItem().getItemId())
						.size() > 0) {
					allC03ByDokum.addAll(testKimyasalAnalizSonucManager
							.getByDokumNoSalesItem(dokumler.get(i),
									selectedUcBirPipes.get(1).getSalesItem()
											.getItemId()));
				}

				try {
					for (int j = 0; j < allC03ByDokum.size(); j++) {
						chemicalObject chemicalObject = new chemicalObject();
						chemicalObject.setHeatNo(allC03ByDokum.get(j)
								.getRuloPipeLink().getRulo().getRuloDokumNo());
						chemicalObject.setCode(allC03ByDokum.get(j)
								.getBagliTestId().getCode());
						chemicalObject.setType("P.Product / Ürün");
						try {
							if (allC03ByDokum.get(j).getcSignum() == null) {
								chemicalObject.setC(new DecimalFormat("#0.000")
										.format(allC03ByDokum.get(j).getC()));
							} else {
								chemicalObject.setC(allC03ByDokum.get(j)
										.getcSignum()
										+ ""
										+ new DecimalFormat("#0.0000")
												.format(allC03ByDokum.get(j)
														.getC()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setC("");
						}
						try {
							chemicalObject.setSi(new DecimalFormat("#0.000")
									.format(allC03ByDokum.get(j).getSi()));
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setSi("");
						}
						try {
							chemicalObject.setMn(new DecimalFormat("#0.000")
									.format(allC03ByDokum.get(j).getMn()));
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setMn("");
						}
						try {
							chemicalObject.setP(new DecimalFormat("#0.000")
									.format(allC03ByDokum.get(j).getP()));
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setP("");
						}
						try {
							chemicalObject.setS(new DecimalFormat("#0.000")
									.format(allC03ByDokum.get(j).getS()));
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setS("");
						}
						try {
							chemicalObject.setCr(new DecimalFormat("#0.000")
									.format(allC03ByDokum.get(j).getCr()));
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setCr("");
						}
						try {
							chemicalObject.setNi(new DecimalFormat("#0.000")
									.format(allC03ByDokum.get(j).getNi()));
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setNi("");
						}
						try {
							if (allC03ByDokum.get(j).getMoSignum() == null) {
								chemicalObject
										.setMo(new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getMo()));
							} else {
								chemicalObject.setMo(allC03ByDokum.get(j)
										.getMoSignum()
										+ ""
										+ new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getMo()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setMo("");
						}
						try {
							if (allC03ByDokum.get(j).getCuSignum() == null) {
								chemicalObject
										.setCu(new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getCu()));
							} else {
								chemicalObject.setCu(allC03ByDokum.get(j)
										.getCuSignum()
										+ ""
										+ new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getCu()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setCu("");
						}
						try {
							if (allC03ByDokum.get(j).getAlSignum() == null) {
								chemicalObject
										.setAl(new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getAl()));
							} else {
								chemicalObject.setAl(allC03ByDokum.get(j)
										.getAlSignum()
										+ ""
										+ new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getAl()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setAl("");
						}
						try {
							if (allC03ByDokum.get(j).getTiSignum() == null) {
								chemicalObject
										.setTi(new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getTi()));
							} else {
								chemicalObject.setTi(allC03ByDokum.get(j)
										.getTiSignum()
										+ ""
										+ new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getTi()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setTi("");
						}
						try {
							if (allC03ByDokum.get(j).getvSignum() == null) {
								chemicalObject.setV(new DecimalFormat("#0.000")
										.format(allC03ByDokum.get(j).getV()));
							} else {
								chemicalObject.setV(allC03ByDokum.get(j)
										.getvSignum()
										+ ""
										+ new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getV()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setV("");
						}
						try {
							if (allC03ByDokum.get(j).getNbSignum() == null) {
								chemicalObject
										.setNb(new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getNb()));
							} else {
								chemicalObject.setNb(allC03ByDokum.get(j)
										.getNbSignum()
										+ ""
										+ new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getNb()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setNb("");
						}
						try {
							if (allC03ByDokum.get(j).getwSignum() == null) {
								chemicalObject.setW(new DecimalFormat("#0.000")
										.format(allC03ByDokum.get(j).getW()));
							} else {
								chemicalObject.setW(allC03ByDokum.get(j)
										.getwSignum()
										+ ""
										+ new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getW()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setW("");
						}
						try {
							if (allC03ByDokum.get(j).getbSignum() == null) {
								chemicalObject
										.setB(new DecimalFormat("#0.0000")
												.format(allC03ByDokum.get(j)
														.getB()));
							} else {
								chemicalObject.setB(allC03ByDokum.get(j)
										.getbSignum()
										+ ""
										+ new DecimalFormat("#0.0000")
												.format(allC03ByDokum.get(j)
														.getB()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setB("");
						}
						try {
							if (allC03ByDokum.get(j).getAsSignum() == null) {
								chemicalObject.setAs(new DecimalFormat(
										"#0.00000").format(allC03ByDokum.get(j)
										.getAs()));
							} else {
								chemicalObject.setAs(allC03ByDokum.get(j)
										.getAsSignum()
										+ ""
										+ new DecimalFormat("#0.00000")
												.format(allC03ByDokum.get(j)
														.getAs()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setAs("");
						}
						try {
							if (allC03ByDokum.get(j).getSbSignum() == null) {
								chemicalObject
										.setSb(new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getSb()));
							} else {
								chemicalObject.setSb(allC03ByDokum.get(j)
										.getSbSignum()
										+ ""
										+ new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getSb()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setSb("");
						}
						try {
							if (allC03ByDokum.get(j).getSnSignum() == null) {
								chemicalObject
										.setSn(new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getSn()));
							} else {
								chemicalObject.setSn(allC03ByDokum.get(j)
										.getSnSignum()
										+ ""
										+ new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getSn()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setSn("");
						}
						try {
							if (allC03ByDokum.get(j).getPbSignum() == null) {
								chemicalObject
										.setPb(new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getPb()));
							} else {
								chemicalObject.setPb(allC03ByDokum.get(j)
										.getPbSignum()
										+ ""
										+ new DecimalFormat("#0.000")
												.format(allC03ByDokum.get(j)
														.getPb()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setPb("");
						}
						try {
							if (allC03ByDokum.get(j).getCaSignum() == null) {
								chemicalObject.setCa(new DecimalFormat(
										"#0.00000").format(allC03ByDokum.get(j)
										.getCa()));
							} else {
								chemicalObject.setCa(allC03ByDokum.get(j)
										.getCaSignum()
										+ ""
										+ new DecimalFormat("#0.00000")
												.format(allC03ByDokum.get(j)
														.getCa()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setCa("");
						}
						try {
							if (allC03ByDokum.get(j).getnSignum() == null) {
								chemicalObject
										.setN(new DecimalFormat("#0.0000")
												.format(allC03ByDokum.get(j)
														.getN()));
							} else {
								chemicalObject.setN(allC03ByDokum.get(j)
										.getnSignum()
										+ ""
										+ new DecimalFormat("#0.0000")
												.format(allC03ByDokum.get(j)
														.getN()));
							}
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setN("");
						}
						try {
							chemicalObject
									.setAe(new DecimalFormat("#0.000")
											.format((allC03ByDokum
													.get(j)
													.getTi()
													.add(allC03ByDokum.get(j)
															.getV())
													.add(allC03ByDokum.get(j)
															.getNb()))));
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setAe("");
						}
						try {
							chemicalObject
									.setCe(new DecimalFormat("#0.000").format((allC03ByDokum
											.get(j)
											.getC()
											.add(allC03ByDokum.get(j).getSi()
													.divide(otuzSayisi, 3))
											.add(allC03ByDokum.get(j).getMn()
													.divide(yirmiSayisi, 3))
											.add(allC03ByDokum
													.get(j)
													.getNi()
													.divide(altmisSayisi, 3)
													.add(allC03ByDokum
															.get(j)
															.getNi()
															.divide(yirmiSayisi,
																	3)
															.add(allC03ByDokum
																	.get(j)
																	.getMo()
																	.divide(onBesSayisi,
																			3))
															.add(allC03ByDokum
																	.get(j)
																	.getV()
																	.divide(onSayisi,
																			3)
																	.add(allC03ByDokum
																			.get(j)
																			.getB()
																			.multiply(
																					besSayisi))))))));
						} catch (Exception e) {
							e.printStackTrace();
							chemicalObject.setCe("");
						}

						chemicalObjects.add(chemicalObject);
					}
				} catch (Exception e) {
					e.printStackTrace();
					chemicalObjects.add(null);
				}
			}

			if (dokumler.size() > 0) {
				dokumlerObject.getDokumNo().addAll(dokumler);
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			// System.out.println(allC01ByDokum.size() + " - "
			// + allC02ByDokum.size() + " - " + allC03ByDokum.size());

			Map dataMap = new HashMap();
			dataMap.put("siparis", selectedUcBirPipes.get(0).getSalesItem());
			dataMap.put("sonucs", chemicalObjects);
			dataMap.put("dokumler", dokumlerObject);
			dataMap.put("date", date);
			dataMap.put("cerificateNumber", "UEC-IRN-BP-00001");

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/releaseNote/ucBir/tanapMTCKimyasal.xls",
					"-" + selectedUcBirPipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"KİMYASAL RELEASE RAPORU BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"KİMYASAL VERİLERİ EKSİKTİR, RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	// Release Note Formu açılış sayfası
	public void goToReleaseNote() {

		FacesContextUtils.redirect(config.getPageUrl().RELEASE_NOTE_FORM_PAGE);
	}

	// Partilendirme Formu açılış sayfası
	public void goToPartilendirme() {

		FacesContextUtils.redirect(config.getPageUrl().PARTILENDIRME_FORM_PAGE);
	}

	// setters getters

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the selectedSalesItem
	 */
	public SalesItem getSelectedSalesItem() {
		return selectedSalesItem;
	}

	/**
	 * @param selectedSalesItem
	 *            the selectedSalesItem to set
	 */
	public void setSelectedSalesItem(SalesItem selectedSalesItem) {
		this.selectedSalesItem = selectedSalesItem;
	}

	/**
	 * @return the c03SonucList
	 */
	public List<TestKimyasalAnalizSonuc> getC03SonucList() {
		return c03SonucList;
	}

	/**
	 * @param c03SonucList
	 *            the c03SonucList to set
	 */
	public void setC03SonucList(List<TestKimyasalAnalizSonuc> c03SonucList) {
		this.c03SonucList = c03SonucList;
	}

	/**
	 * @return the f01SonucList
	 */
	public List<TestBukmeSonuc> getF01SonucList() {
		return f01SonucList;
	}

	/**
	 * @param f01SonucList
	 *            the f01SonucList to set
	 */
	public void setF01SonucList(List<TestBukmeSonuc> f01SonucList) {
		this.f01SonucList = f01SonucList;
	}

	/**
	 * @return the f02SonucList
	 */
	public List<TestBukmeSonuc> getF02SonucList() {
		return f02SonucList;
	}

	/**
	 * @param f02SonucList
	 *            the f02SonucList to set
	 */
	public void setF02SonucList(List<TestBukmeSonuc> f02SonucList) {
		this.f02SonucList = f02SonucList;
	}

	/**
	 * @return the f03SonucList
	 */
	public List<TestBukmeSonuc> getF03SonucList() {
		return f03SonucList;
	}

	/**
	 * @param f03SonucList
	 *            the f03SonucList to set
	 */
	public void setF03SonucList(List<TestBukmeSonuc> f03SonucList) {
		this.f03SonucList = f03SonucList;
	}

	/**
	 * @return the f04SonucList
	 */
	public List<TestBukmeSonuc> getF04SonucList() {
		return f04SonucList;
	}

	/**
	 * @param f04SonucList
	 *            the f04SonucList to set
	 */
	public void setF04SonucList(List<TestBukmeSonuc> f04SonucList) {
		this.f04SonucList = f04SonucList;
	}

	/**
	 * @return the f05SonucList
	 */
	public List<TestBukmeSonuc> getF05SonucList() {
		return f05SonucList;
	}

	/**
	 * @param f05SonucList
	 *            the f05SonucList to set
	 */
	public void setF05SonucList(List<TestBukmeSonuc> f05SonucList) {
		this.f05SonucList = f05SonucList;
	}

	/**
	 * @return the f06SonucList
	 */
	public List<TestBukmeSonuc> getF06SonucList() {
		return f06SonucList;
	}

	/**
	 * @param f06SonucList
	 *            the f06SonucList to set
	 */
	public void setF06SonucList(List<TestBukmeSonuc> f06SonucList) {
		this.f06SonucList = f06SonucList;
	}

	/**
	 * @return the c03Siklik
	 */
	public String getC03Siklik() {
		return c03Siklik;
	}

	/**
	 * @param c03Siklik
	 *            the c03Siklik to set
	 */
	public void setC03Siklik(String c03Siklik) {
		this.c03Siklik = c03Siklik;
	}

	/**
	 * @return the c03SiklikNumber
	 */
	public int getC03SiklikNumber() {
		return c03SiklikNumber;
	}

	/**
	 * @param c03SiklikNumber
	 *            the c03SiklikNumber to set
	 */
	public void setC03SiklikNumber(int c03SiklikNumber) {
		this.c03SiklikNumber = c03SiklikNumber;
	}

	/**
	 * @return the f01Siklik
	 */
	public String getF01Siklik() {
		return f01Siklik;
	}

	/**
	 * @param f01Siklik
	 *            the f01Siklik to set
	 */
	public void setF01Siklik(String f01Siklik) {
		this.f01Siklik = f01Siklik;
	}

	/**
	 * @return the f02Siklik
	 */
	public String getF02Siklik() {
		return f02Siklik;
	}

	/**
	 * @param f02Siklik
	 *            the f02Siklik to set
	 */
	public void setF02Siklik(String f02Siklik) {
		this.f02Siklik = f02Siklik;
	}

	/**
	 * @return the f03Siklik
	 */
	public String getF03Siklik() {
		return f03Siklik;
	}

	/**
	 * @param f03Siklik
	 *            the f03Siklik to set
	 */
	public void setF03Siklik(String f03Siklik) {
		this.f03Siklik = f03Siklik;
	}

	/**
	 * @return the f04Siklik
	 */
	public String getF04Siklik() {
		return f04Siklik;
	}

	/**
	 * @param f04Siklik
	 *            the f04Siklik to set
	 */
	public void setF04Siklik(String f04Siklik) {
		this.f04Siklik = f04Siklik;
	}

	/**
	 * @return the f05Siklik
	 */
	public String getF05Siklik() {
		return f05Siklik;
	}

	/**
	 * @param f05Siklik
	 *            the f05Siklik to set
	 */
	public void setF05Siklik(String f05Siklik) {
		this.f05Siklik = f05Siklik;
	}

	/**
	 * @return the f06Siklik
	 */
	public String getF06Siklik() {
		return f06Siklik;
	}

	/**
	 * @param f06Siklik
	 *            the f06Siklik to set
	 */
	public void setF06Siklik(String f06Siklik) {
		this.f06Siklik = f06Siklik;
	}

	/**
	 * @return the f01SiklikNumber
	 */
	public int getF01SiklikNumber() {
		return f01SiklikNumber;
	}

	/**
	 * @param f01SiklikNumber
	 *            the f01SiklikNumber to set
	 */
	public void setF01SiklikNumber(int f01SiklikNumber) {
		this.f01SiklikNumber = f01SiklikNumber;
	}

	/**
	 * @return the f02SiklikNumber
	 */
	public int getF02SiklikNumber() {
		return f02SiklikNumber;
	}

	/**
	 * @param f02SiklikNumber
	 *            the f02SiklikNumber to set
	 */
	public void setF02SiklikNumber(int f02SiklikNumber) {
		this.f02SiklikNumber = f02SiklikNumber;
	}

	/**
	 * @return the f03SiklikNumber
	 */
	public int getF03SiklikNumber() {
		return f03SiklikNumber;
	}

	/**
	 * @param f03SiklikNumber
	 *            the f03SiklikNumber to set
	 */
	public void setF03SiklikNumber(int f03SiklikNumber) {
		this.f03SiklikNumber = f03SiklikNumber;
	}

	/**
	 * @return the f04SiklikNumber
	 */
	public int getF04SiklikNumber() {
		return f04SiklikNumber;
	}

	/**
	 * @param f04SiklikNumber
	 *            the f04SiklikNumber to set
	 */
	public void setF04SiklikNumber(int f04SiklikNumber) {
		this.f04SiklikNumber = f04SiklikNumber;
	}

	/**
	 * @return the f05SiklikNumber
	 */
	public int getF05SiklikNumber() {
		return f05SiklikNumber;
	}

	/**
	 * @param f05SiklikNumber
	 *            the f05SiklikNumber to set
	 */
	public void setF05SiklikNumber(int f05SiklikNumber) {
		this.f05SiklikNumber = f05SiklikNumber;
	}

	/**
	 * @return the f06SiklikNumber
	 */
	public int getF06SiklikNumber() {
		return f06SiklikNumber;
	}

	/**
	 * @param f06SiklikNumber
	 *            the f06SiklikNumber to set
	 */
	public void setF06SiklikNumber(int f06SiklikNumber) {
		this.f06SiklikNumber = f06SiklikNumber;
	}

	/**
	 * @return the z01Siklik
	 */
	public String getZ01Siklik() {
		return z01Siklik;
	}

	/**
	 * @param z01Siklik
	 *            the z01Siklik to set
	 */
	public void setZ01Siklik(String z01Siklik) {
		this.z01Siklik = z01Siklik;
	}

	/**
	 * @return the z02Siklik
	 */
	public String getZ02Siklik() {
		return z02Siklik;
	}

	/**
	 * @param z02Siklik
	 *            the z02Siklik to set
	 */
	public void setZ02Siklik(String z02Siklik) {
		this.z02Siklik = z02Siklik;
	}

	/**
	 * @return the z03Siklik
	 */
	public String getZ03Siklik() {
		return z03Siklik;
	}

	/**
	 * @param z03Siklik
	 *            the z03Siklik to set
	 */
	public void setZ03Siklik(String z03Siklik) {
		this.z03Siklik = z03Siklik;
	}

	/**
	 * @return the z06Siklik
	 */
	public String getZ06Siklik() {
		return z06Siklik;
	}

	/**
	 * @param z06Siklik
	 *            the z06Siklik to set
	 */
	public void setZ06Siklik(String z06Siklik) {
		this.z06Siklik = z06Siklik;
	}

	/**
	 * @return the z07Siklik
	 */
	public String getZ07Siklik() {
		return z07Siklik;
	}

	/**
	 * @param z07Siklik
	 *            the z07Siklik to set
	 */
	public void setZ07Siklik(String z07Siklik) {
		this.z07Siklik = z07Siklik;
	}

	/**
	 * @return the z01SiklikNumber
	 */
	public int getZ01SiklikNumber() {
		return z01SiklikNumber;
	}

	/**
	 * @param z01SiklikNumber
	 *            the z01SiklikNumber to set
	 */
	public void setZ01SiklikNumber(int z01SiklikNumber) {
		this.z01SiklikNumber = z01SiklikNumber;
	}

	/**
	 * @return the z02SiklikNumber
	 */
	public int getZ02SiklikNumber() {
		return z02SiklikNumber;
	}

	/**
	 * @param z02SiklikNumber
	 *            the z02SiklikNumber to set
	 */
	public void setZ02SiklikNumber(int z02SiklikNumber) {
		this.z02SiklikNumber = z02SiklikNumber;
	}

	/**
	 * @return the z03SiklikNumber
	 */
	public int getZ03SiklikNumber() {
		return z03SiklikNumber;
	}

	/**
	 * @param z03SiklikNumber
	 *            the z03SiklikNumber to set
	 */
	public void setZ03SiklikNumber(int z03SiklikNumber) {
		this.z03SiklikNumber = z03SiklikNumber;
	}

	/**
	 * @return the z06SiklikNumber
	 */
	public int getZ06SiklikNumber() {
		return z06SiklikNumber;
	}

	/**
	 * @param z06SiklikNumber
	 *            the z06SiklikNumber to set
	 */
	public void setZ06SiklikNumber(int z06SiklikNumber) {
		this.z06SiklikNumber = z06SiklikNumber;
	}

	/**
	 * @return the z07SiklikNumber
	 */
	public int getZ07SiklikNumber() {
		return z07SiklikNumber;
	}

	/**
	 * @param z07SiklikNumber
	 *            the z07SiklikNumber to set
	 */
	public void setZ07SiklikNumber(int z07SiklikNumber) {
		this.z07SiklikNumber = z07SiklikNumber;
	}

	/**
	 * @return the z01SonucList
	 */
	public List<TestCekmeSonuc> getZ01SonucList() {
		return z01SonucList;
	}

	/**
	 * @param z01SonucList
	 *            the z01SonucList to set
	 */
	public void setZ01SonucList(List<TestCekmeSonuc> z01SonucList) {
		this.z01SonucList = z01SonucList;
	}

	/**
	 * @return the z02SonucList
	 */
	public List<TestCekmeSonuc> getZ02SonucList() {
		return z02SonucList;
	}

	/**
	 * @param z02SonucList
	 *            the z02SonucList to set
	 */
	public void setZ02SonucList(List<TestCekmeSonuc> z02SonucList) {
		this.z02SonucList = z02SonucList;
	}

	/**
	 * @return the z03SonucList
	 */
	public List<TestCekmeSonuc> getZ03SonucList() {
		return z03SonucList;
	}

	/**
	 * @param z03SonucList
	 *            the z03SonucList to set
	 */
	public void setZ03SonucList(List<TestCekmeSonuc> z03SonucList) {
		this.z03SonucList = z03SonucList;
	}

	/**
	 * @return the z06SonucList
	 */
	public List<TestCekmeSonuc> getZ06SonucList() {
		return z06SonucList;
	}

	/**
	 * @param z06SonucList
	 *            the z06SonucList to set
	 */
	public void setZ06SonucList(List<TestCekmeSonuc> z06SonucList) {
		this.z06SonucList = z06SonucList;
	}

	/**
	 * @return the z07SonucList
	 */
	public List<TestCekmeSonuc> getZ07SonucList() {
		return z07SonucList;
	}

	/**
	 * @param z07SonucList
	 *            the z07SonucList to set
	 */
	public void setZ07SonucList(List<TestCekmeSonuc> z07SonucList) {
		this.z07SonucList = z07SonucList;
	}

	/**
	 * @return the c03EksikSonucList
	 */
	public List<TestKimyasalAnalizSonuc> getC03EksikSonucList() {
		return c03EksikSonucList;
	}

	/**
	 * @param c03EksikSonucList
	 *            the c03EksikSonucList to set
	 */
	public void setC03EksikSonucList(
			List<TestKimyasalAnalizSonuc> c03EksikSonucList) {
		this.c03EksikSonucList = c03EksikSonucList;
	}

	/**
	 * @return the partiSayisi
	 */
	public int getPartiSayisi() {
		return partiSayisi;
	}

	/**
	 * @param partiSayisi
	 *            the partiSayisi to set
	 */
	public void setPartiSayisi(int partiSayisi) {
		this.partiSayisi = partiSayisi;
	}

	/**
	 * @return the c03PartiSayisi
	 */
	public int getC03PartiSayisi() {
		return c03PartiSayisi;
	}

	/**
	 * @param c03PartiSayisi
	 *            the c03PartiSayisi to set
	 */
	public void setC03PartiSayisi(int c03PartiSayisi) {
		this.c03PartiSayisi = c03PartiSayisi;
	}

	/**
	 * @return the f01PartiSayisi
	 */
	public int getF01PartiSayisi() {
		return f01PartiSayisi;
	}

	/**
	 * @param f01PartiSayisi
	 *            the f01PartiSayisi to set
	 */
	public void setF01PartiSayisi(int f01PartiSayisi) {
		this.f01PartiSayisi = f01PartiSayisi;
	}

	/**
	 * @return the f02PartiSayisi
	 */
	public int getF02PartiSayisi() {
		return f02PartiSayisi;
	}

	/**
	 * @param f02PartiSayisi
	 *            the f02PartiSayisi to set
	 */
	public void setF02PartiSayisi(int f02PartiSayisi) {
		this.f02PartiSayisi = f02PartiSayisi;
	}

	/**
	 * @return the f03PartiSayisi
	 */
	public int getF03PartiSayisi() {
		return f03PartiSayisi;
	}

	/**
	 * @param f03PartiSayisi
	 *            the f03PartiSayisi to set
	 */
	public void setF03PartiSayisi(int f03PartiSayisi) {
		this.f03PartiSayisi = f03PartiSayisi;
	}

	/**
	 * @return the f04PartiSayisi
	 */
	public int getF04PartiSayisi() {
		return f04PartiSayisi;
	}

	/**
	 * @param f04PartiSayisi
	 *            the f04PartiSayisi to set
	 */
	public void setF04PartiSayisi(int f04PartiSayisi) {
		this.f04PartiSayisi = f04PartiSayisi;
	}

	/**
	 * @return the f05PartiSayisi
	 */
	public int getF05PartiSayisi() {
		return f05PartiSayisi;
	}

	/**
	 * @param f05PartiSayisi
	 *            the f05PartiSayisi to set
	 */
	public void setF05PartiSayisi(int f05PartiSayisi) {
		this.f05PartiSayisi = f05PartiSayisi;
	}

	/**
	 * @return the f06PartiSayisi
	 */
	public int getF06PartiSayisi() {
		return f06PartiSayisi;
	}

	/**
	 * @param f06PartiSayisi
	 *            the f06PartiSayisi to set
	 */
	public void setF06PartiSayisi(int f06PartiSayisi) {
		this.f06PartiSayisi = f06PartiSayisi;
	}

	/**
	 * @return the z01PartiSayisi
	 */
	public int getZ01PartiSayisi() {
		return z01PartiSayisi;
	}

	/**
	 * @param z01PartiSayisi
	 *            the z01PartiSayisi to set
	 */
	public void setZ01PartiSayisi(int z01PartiSayisi) {
		this.z01PartiSayisi = z01PartiSayisi;
	}

	/**
	 * @return the z02PartiSayisi
	 */
	public int getZ02PartiSayisi() {
		return z02PartiSayisi;
	}

	/**
	 * @param z02PartiSayisi
	 *            the z02PartiSayisi to set
	 */
	public void setZ02PartiSayisi(int z02PartiSayisi) {
		this.z02PartiSayisi = z02PartiSayisi;
	}

	/**
	 * @return the z03PartiSayisi
	 */
	public int getZ03PartiSayisi() {
		return z03PartiSayisi;
	}

	/**
	 * @param z03PartiSayisi
	 *            the z03PartiSayisi to set
	 */
	public void setZ03PartiSayisi(int z03PartiSayisi) {
		this.z03PartiSayisi = z03PartiSayisi;
	}

	/**
	 * @return the z06PartiSayisi
	 */
	public int getZ06PartiSayisi() {
		return z06PartiSayisi;
	}

	/**
	 * @param z06PartiSayisi
	 *            the z06PartiSayisi to set
	 */
	public void setZ06PartiSayisi(int z06PartiSayisi) {
		this.z06PartiSayisi = z06PartiSayisi;
	}

	/**
	 * @return the z07PartiSayisi
	 */
	public int getZ07PartiSayisi() {
		return z07PartiSayisi;
	}

	/**
	 * @param z07PartiSayisi
	 *            the z07PartiSayisi to set
	 */
	public void setZ07PartiSayisi(int z07PartiSayisi) {
		this.z07PartiSayisi = z07PartiSayisi;
	}

	/**
	 * @return the k01SonucList
	 */
	public List<TestCentikDarbeSonuc> getK01SonucList() {
		return k01SonucList;
	}

	/**
	 * @param k01SonucList
	 *            the k01SonucList to set
	 */
	public void setK01SonucList(List<TestCentikDarbeSonuc> k01SonucList) {
		this.k01SonucList = k01SonucList;
	}

	/**
	 * @return the k02SonucList
	 */
	public List<TestCentikDarbeSonuc> getK02SonucList() {
		return k02SonucList;
	}

	/**
	 * @param k02SonucList
	 *            the k02SonucList to set
	 */
	public void setK02SonucList(List<TestCentikDarbeSonuc> k02SonucList) {
		this.k02SonucList = k02SonucList;
	}

	/**
	 * @return the k03SonucList
	 */
	public List<TestCentikDarbeSonuc> getK03SonucList() {
		return k03SonucList;
	}

	/**
	 * @param k03SonucList
	 *            the k03SonucList to set
	 */
	public void setK03SonucList(List<TestCentikDarbeSonuc> k03SonucList) {
		this.k03SonucList = k03SonucList;
	}

	/**
	 * @return the k04SonucList
	 */
	public List<TestCentikDarbeSonuc> getK04SonucList() {
		return k04SonucList;
	}

	/**
	 * @param k04SonucList
	 *            the k04SonucList to set
	 */
	public void setK04SonucList(List<TestCentikDarbeSonuc> k04SonucList) {
		this.k04SonucList = k04SonucList;
	}

	/**
	 * @return the k05SonucList
	 */
	public List<TestCentikDarbeSonuc> getK05SonucList() {
		return k05SonucList;
	}

	/**
	 * @param k05SonucList
	 *            the k05SonucList to set
	 */
	public void setK05SonucList(List<TestCentikDarbeSonuc> k05SonucList) {
		this.k05SonucList = k05SonucList;
	}

	/**
	 * @return the k08SonucList
	 */
	public List<TestCentikDarbeSonuc> getK08SonucList() {
		return k08SonucList;
	}

	/**
	 * @param k08SonucList
	 *            the k08SonucList to set
	 */
	public void setK08SonucList(List<TestCentikDarbeSonuc> k08SonucList) {
		this.k08SonucList = k08SonucList;
	}

	/**
	 * @return the d01SonucList
	 */
	public List<TestAgirlikDusurmeSonuc> getD01SonucList() {
		return d01SonucList;
	}

	/**
	 * @param d01SonucList
	 *            the d01SonucList to set
	 */
	public void setD01SonucList(List<TestAgirlikDusurmeSonuc> d01SonucList) {
		this.d01SonucList = d01SonucList;
	}

	/**
	 * @return the d02SonucList
	 */
	public List<TestAgirlikDusurmeSonuc> getD02SonucList() {
		return d02SonucList;
	}

	/**
	 * @param d02SonucList
	 *            the d02SonucList to set
	 */
	public void setD02SonucList(List<TestAgirlikDusurmeSonuc> d02SonucList) {
		this.d02SonucList = d02SonucList;
	}

	/**
	 * @return the k03Siklik
	 */
	public String getK03Siklik() {
		return k03Siklik;
	}

	/**
	 * @param k03Siklik
	 *            the k03Siklik to set
	 */
	public void setK03Siklik(String k03Siklik) {
		this.k03Siklik = k03Siklik;
	}

	/**
	 * @return the k04Siklik
	 */
	public String getK04Siklik() {
		return k04Siklik;
	}

	/**
	 * @param k04Siklik
	 *            the k04Siklik to set
	 */
	public void setK04Siklik(String k04Siklik) {
		this.k04Siklik = k04Siklik;
	}

	/**
	 * @return the k05Siklik
	 */
	public String getK05Siklik() {
		return k05Siklik;
	}

	/**
	 * @param k05Siklik
	 *            the k05Siklik to set
	 */
	public void setK05Siklik(String k05Siklik) {
		this.k05Siklik = k05Siklik;
	}

	/**
	 * @return the k08Siklik
	 */
	public String getK08Siklik() {
		return k08Siklik;
	}

	/**
	 * @param k08Siklik
	 *            the k08Siklik to set
	 */
	public void setK08Siklik(String k08Siklik) {
		this.k08Siklik = k08Siklik;
	}

	/**
	 * @return the k01SiklikNumber
	 */
	public int getK01SiklikNumber() {
		return k01SiklikNumber;
	}

	/**
	 * @param k01SiklikNumber
	 *            the k01SiklikNumber to set
	 */
	public void setK01SiklikNumber(int k01SiklikNumber) {
		this.k01SiklikNumber = k01SiklikNumber;
	}

	/**
	 * @return the k02SiklikNumber
	 */
	public int getK02SiklikNumber() {
		return k02SiklikNumber;
	}

	/**
	 * @param k02SiklikNumber
	 *            the k02SiklikNumber to set
	 */
	public void setK02SiklikNumber(int k02SiklikNumber) {
		this.k02SiklikNumber = k02SiklikNumber;
	}

	/**
	 * @return the k03SiklikNumber
	 */
	public int getK03SiklikNumber() {
		return k03SiklikNumber;
	}

	/**
	 * @param k03SiklikNumber
	 *            the k03SiklikNumber to set
	 */
	public void setK03SiklikNumber(int k03SiklikNumber) {
		this.k03SiklikNumber = k03SiklikNumber;
	}

	/**
	 * @return the k04SiklikNumber
	 */
	public int getK04SiklikNumber() {
		return k04SiklikNumber;
	}

	/**
	 * @param k04SiklikNumber
	 *            the k04SiklikNumber to set
	 */
	public void setK04SiklikNumber(int k04SiklikNumber) {
		this.k04SiklikNumber = k04SiklikNumber;
	}

	/**
	 * @return the k05SiklikNumber
	 */
	public int getK05SiklikNumber() {
		return k05SiklikNumber;
	}

	/**
	 * @param k05SiklikNumber
	 *            the k05SiklikNumber to set
	 */
	public void setK05SiklikNumber(int k05SiklikNumber) {
		this.k05SiklikNumber = k05SiklikNumber;
	}

	/**
	 * @return the k08SiklikNumber
	 */
	public int getK08SiklikNumber() {
		return k08SiklikNumber;
	}

	/**
	 * @param k08SiklikNumber
	 *            the k08SiklikNumber to set
	 */
	public void setK08SiklikNumber(int k08SiklikNumber) {
		this.k08SiklikNumber = k08SiklikNumber;
	}

	/**
	 * @return the k01PartiSayisi
	 */
	public int getK01PartiSayisi() {
		return k01PartiSayisi;
	}

	/**
	 * @param k01PartiSayisi
	 *            the k01PartiSayisi to set
	 */
	public void setK01PartiSayisi(int k01PartiSayisi) {
		this.k01PartiSayisi = k01PartiSayisi;
	}

	/**
	 * @return the k02PartiSayisi
	 */
	public int getK02PartiSayisi() {
		return k02PartiSayisi;
	}

	/**
	 * @param k02PartiSayisi
	 *            the k02PartiSayisi to set
	 */
	public void setK02PartiSayisi(int k02PartiSayisi) {
		this.k02PartiSayisi = k02PartiSayisi;
	}

	/**
	 * @return the k03PartiSayisi
	 */
	public int getK03PartiSayisi() {
		return k03PartiSayisi;
	}

	/**
	 * @param k03PartiSayisi
	 *            the k03PartiSayisi to set
	 */
	public void setK03PartiSayisi(int k03PartiSayisi) {
		this.k03PartiSayisi = k03PartiSayisi;
	}

	/**
	 * @return the k04PartiSayisi
	 */
	public int getK04PartiSayisi() {
		return k04PartiSayisi;
	}

	/**
	 * @param k04PartiSayisi
	 *            the k04PartiSayisi to set
	 */
	public void setK04PartiSayisi(int k04PartiSayisi) {
		this.k04PartiSayisi = k04PartiSayisi;
	}

	/**
	 * @return the k05PartiSayisi
	 */
	public int getK05PartiSayisi() {
		return k05PartiSayisi;
	}

	/**
	 * @param k05PartiSayisi
	 *            the k05PartiSayisi to set
	 */
	public void setK05PartiSayisi(int k05PartiSayisi) {
		this.k05PartiSayisi = k05PartiSayisi;
	}

	/**
	 * @return the k08PartiSayisi
	 */
	public int getK08PartiSayisi() {
		return k08PartiSayisi;
	}

	/**
	 * @param k08PartiSayisi
	 *            the k08PartiSayisi to set
	 */
	public void setK08PartiSayisi(int k08PartiSayisi) {
		this.k08PartiSayisi = k08PartiSayisi;
	}

	/**
	 * @return the d01Siklik
	 */
	public String getD01Siklik() {
		return d01Siklik;
	}

	/**
	 * @param d01Siklik
	 *            the d01Siklik to set
	 */
	public void setD01Siklik(String d01Siklik) {
		this.d01Siklik = d01Siklik;
	}

	/**
	 * @return the d01SiklikNumber
	 */
	public int getD01SiklikNumber() {
		return d01SiklikNumber;
	}

	/**
	 * @param d01SiklikNumber
	 *            the d01SiklikNumber to set
	 */
	public void setD01SiklikNumber(int d01SiklikNumber) {
		this.d01SiklikNumber = d01SiklikNumber;
	}

	/**
	 * @return the d01PartiSayisi
	 */
	public int getD01PartiSayisi() {
		return d01PartiSayisi;
	}

	/**
	 * @param d01PartiSayisi
	 *            the d01PartiSayisi to set
	 */
	public void setD01PartiSayisi(int d01PartiSayisi) {
		this.d01PartiSayisi = d01PartiSayisi;
	}

	/**
	 * @return the d02Siklik
	 */
	public String getD02Siklik() {
		return d02Siklik;
	}

	/**
	 * @param d02Siklik
	 *            the d02Siklik to set
	 */
	public void setD02Siklik(String d02Siklik) {
		this.d02Siklik = d02Siklik;
	}

	/**
	 * @return the d02SiklikNumber
	 */
	public int getD02SiklikNumber() {
		return d02SiklikNumber;
	}

	/**
	 * @param d02SiklikNumber
	 *            the d02SiklikNumber to set
	 */
	public void setD02SiklikNumber(int d02SiklikNumber) {
		this.d02SiklikNumber = d02SiklikNumber;
	}

	/**
	 * @return the d02PartiSayisi
	 */
	public int getD02PartiSayisi() {
		return d02PartiSayisi;
	}

	/**
	 * @param d02PartiSayisi
	 *            the d02PartiSayisi to set
	 */
	public void setD02PartiSayisi(int d02PartiSayisi) {
		this.d02PartiSayisi = d02PartiSayisi;
	}

	/**
	 * @return the m01Siklik
	 */
	public String getM01Siklik() {
		return m01Siklik;
	}

	/**
	 * @param m01Siklik
	 *            the m01Siklik to set
	 */
	public void setM01Siklik(String m01Siklik) {
		this.m01Siklik = m01Siklik;
	}

	/**
	 * @return the m01SiklikNumber
	 */
	public int getM01SiklikNumber() {
		return m01SiklikNumber;
	}

	/**
	 * @param m01SiklikNumber
	 *            the m01SiklikNumber to set
	 */
	public void setM01SiklikNumber(int m01SiklikNumber) {
		this.m01SiklikNumber = m01SiklikNumber;
	}

	/**
	 * @return the m01PartiSayisi
	 */
	public int getM01PartiSayisi() {
		return m01PartiSayisi;
	}

	/**
	 * @param m01PartiSayisi
	 *            the m01PartiSayisi to set
	 */
	public void setM01PartiSayisi(int m01PartiSayisi) {
		this.m01PartiSayisi = m01PartiSayisi;
	}

	/**
	 * @param u03SonucList
	 *            the u03SonucList to set
	 */
	public void setU03SonucList(List<TestCentikDarbeSonuc> u03SonucList) {
		this.u03SonucList = u03SonucList;
	}

	/**
	 * @return the u04SonucList
	 */
	public List<TestCentikDarbeSonuc> getU04SonucList() {
		return u04SonucList;
	}

	/**
	 * @param u04SonucList
	 *            the u04SonucList to set
	 */
	public void setU04SonucList(List<TestCentikDarbeSonuc> u04SonucList) {
		this.u04SonucList = u04SonucList;
	}

	/**
	 * @return the u05SonucList
	 */
	public List<TestCentikDarbeSonuc> getU05SonucList() {
		return u05SonucList;
	}

	/**
	 * @param u05SonucList
	 *            the u05SonucList to set
	 */
	public void setU05SonucList(List<TestCentikDarbeSonuc> u05SonucList) {
		this.u05SonucList = u05SonucList;
	}

	/**
	 * @return the u08SonucList
	 */
	public List<TestCentikDarbeSonuc> getU08SonucList() {
		return u08SonucList;
	}

	/**
	 * @param u08SonucList
	 *            the u08SonucList to set
	 */
	public void setU08SonucList(List<TestCentikDarbeSonuc> u08SonucList) {
		this.u08SonucList = u08SonucList;
	}

	/**
	 * @return the u01Siklik
	 */
	public String getU01Siklik() {
		return u01Siklik;
	}

	/**
	 * @param u01Siklik
	 *            the u01Siklik to set
	 */
	public void setU01Siklik(String u01Siklik) {
		this.u01Siklik = u01Siklik;
	}

	/**
	 * @return the u02Siklik
	 */
	public String getU02Siklik() {
		return u02Siklik;
	}

	/**
	 * @param u02Siklik
	 *            the u02Siklik to set
	 */
	public void setU02Siklik(String u02Siklik) {
		this.u02Siklik = u02Siklik;
	}

	/**
	 * @return the u03Siklik
	 */
	public String getU03Siklik() {
		return u03Siklik;
	}

	/**
	 * @param u03Siklik
	 *            the u03Siklik to set
	 */
	public void setU03Siklik(String u03Siklik) {
		this.u03Siklik = u03Siklik;
	}

	/**
	 * @return the u04Siklik
	 */
	public String getU04Siklik() {
		return u04Siklik;
	}

	/**
	 * @param u04Siklik
	 *            the u04Siklik to set
	 */
	public void setU04Siklik(String u04Siklik) {
		this.u04Siklik = u04Siklik;
	}

	/**
	 * @return the u05Siklik
	 */
	public String getU05Siklik() {
		return u05Siklik;
	}

	/**
	 * @param u05Siklik
	 *            the u05Siklik to set
	 */
	public void setU05Siklik(String u05Siklik) {
		this.u05Siklik = u05Siklik;
	}

	/**
	 * @return the u08Siklik
	 */
	public String getU08Siklik() {
		return u08Siklik;
	}

	/**
	 * @param u08Siklik
	 *            the u08Siklik to set
	 */
	public void setU08Siklik(String u08Siklik) {
		this.u08Siklik = u08Siklik;
	}

	/**
	 * @return the u01SiklikNumber
	 */
	public int getU01SiklikNumber() {
		return u01SiklikNumber;
	}

	/**
	 * @param u01SiklikNumber
	 *            the u01SiklikNumber to set
	 */
	public void setU01SiklikNumber(int u01SiklikNumber) {
		this.u01SiklikNumber = u01SiklikNumber;
	}

	/**
	 * @return the u02SiklikNumber
	 */
	public int getU02SiklikNumber() {
		return u02SiklikNumber;
	}

	/**
	 * @param u02SiklikNumber
	 *            the u02SiklikNumber to set
	 */
	public void setU02SiklikNumber(int u02SiklikNumber) {
		this.u02SiklikNumber = u02SiklikNumber;
	}

	/**
	 * @return the u03SiklikNumber
	 */
	public int getU03SiklikNumber() {
		return u03SiklikNumber;
	}

	/**
	 * @param u03SiklikNumber
	 *            the u03SiklikNumber to set
	 */
	public void setU03SiklikNumber(int u03SiklikNumber) {
		this.u03SiklikNumber = u03SiklikNumber;
	}

	/**
	 * @return the u04SiklikNumber
	 */
	public int getU04SiklikNumber() {
		return u04SiklikNumber;
	}

	/**
	 * @param u04SiklikNumber
	 *            the u04SiklikNumber to set
	 */
	public void setU04SiklikNumber(int u04SiklikNumber) {
		this.u04SiklikNumber = u04SiklikNumber;
	}

	/**
	 * @return the u05SiklikNumber
	 */
	public int getU05SiklikNumber() {
		return u05SiklikNumber;
	}

	/**
	 * @param u05SiklikNumber
	 *            the u05SiklikNumber to set
	 */
	public void setU05SiklikNumber(int u05SiklikNumber) {
		this.u05SiklikNumber = u05SiklikNumber;
	}

	/**
	 * @return the u08SiklikNumber
	 */
	public int getU08SiklikNumber() {
		return u08SiklikNumber;
	}

	/**
	 * @param u08SiklikNumber
	 *            the u08SiklikNumber to set
	 */
	public void setU08SiklikNumber(int u08SiklikNumber) {
		this.u08SiklikNumber = u08SiklikNumber;
	}

	/**
	 * @return the u01PartiSayisi
	 */
	public int getU01PartiSayisi() {
		return u01PartiSayisi;
	}

	/**
	 * @param u01PartiSayisi
	 *            the u01PartiSayisi to set
	 */
	public void setU01PartiSayisi(int u01PartiSayisi) {
		this.u01PartiSayisi = u01PartiSayisi;
	}

	/**
	 * @return the u02PartiSayisi
	 */
	public int getU02PartiSayisi() {
		return u02PartiSayisi;
	}

	/**
	 * @param u02PartiSayisi
	 *            the u02PartiSayisi to set
	 */
	public void setU02PartiSayisi(int u02PartiSayisi) {
		this.u02PartiSayisi = u02PartiSayisi;
	}

	/**
	 * @return the u03PartiSayisi
	 */
	public int getU03PartiSayisi() {
		return u03PartiSayisi;
	}

	/**
	 * @param u03PartiSayisi
	 *            the u03PartiSayisi to set
	 */
	public void setU03PartiSayisi(int u03PartiSayisi) {
		this.u03PartiSayisi = u03PartiSayisi;
	}

	/**
	 * @return the u04PartiSayisi
	 */
	public int getU04PartiSayisi() {
		return u04PartiSayisi;
	}

	/**
	 * @param u04PartiSayisi
	 *            the u04PartiSayisi to set
	 */
	public void setU04PartiSayisi(int u04PartiSayisi) {
		this.u04PartiSayisi = u04PartiSayisi;
	}

	/**
	 * @return the u05PartiSayisi
	 */
	public int getU05PartiSayisi() {
		return u05PartiSayisi;
	}

	/**
	 * @param u05PartiSayisi
	 *            the u05PartiSayisi to set
	 */
	public void setU05PartiSayisi(int u05PartiSayisi) {
		this.u05PartiSayisi = u05PartiSayisi;
	}

	/**
	 * @return the u08PartiSayisi
	 */
	public int getU08PartiSayisi() {
		return u08PartiSayisi;
	}

	/**
	 * @param u08PartiSayisi
	 *            the u08PartiSayisi to set
	 */
	public void setU08PartiSayisi(int u08PartiSayisi) {
		this.u08PartiSayisi = u08PartiSayisi;
	}

	/**
	 * @return the k09SonucList
	 */
	public List<TestCentikDarbeSonuc> getK09SonucList() {
		return k09SonucList;
	}

	/**
	 * @param k09SonucList
	 *            the k09SonucList to set
	 */
	public void setK09SonucList(List<TestCentikDarbeSonuc> k09SonucList) {
		this.k09SonucList = k09SonucList;
	}

	/**
	 * @return the k10SonucList
	 */
	public List<TestCentikDarbeSonuc> getK10SonucList() {
		return k10SonucList;
	}

	/**
	 * @param k10SonucList
	 *            the k10SonucList to set
	 */
	public void setK10SonucList(List<TestCentikDarbeSonuc> k10SonucList) {
		this.k10SonucList = k10SonucList;
	}

	/**
	 * @return the k11SonucList
	 */
	public List<TestCentikDarbeSonuc> getK11SonucList() {
		return k11SonucList;
	}

	/**
	 * @param k11SonucList
	 *            the k11SonucList to set
	 */
	public void setK11SonucList(List<TestCentikDarbeSonuc> k11SonucList) {
		this.k11SonucList = k11SonucList;
	}

	/**
	 * @return the k12SonucList
	 */
	public List<TestCentikDarbeSonuc> getK12SonucList() {
		return k12SonucList;
	}

	/**
	 * @param k12SonucList
	 *            the k12SonucList to set
	 */
	public void setK12SonucList(List<TestCentikDarbeSonuc> k12SonucList) {
		this.k12SonucList = k12SonucList;
	}

	/**
	 * @return the k13SonucList
	 */
	public List<TestCentikDarbeSonuc> getK13SonucList() {
		return k13SonucList;
	}

	/**
	 * @param k13SonucList
	 *            the k13SonucList to set
	 */
	public void setK13SonucList(List<TestCentikDarbeSonuc> k13SonucList) {
		this.k13SonucList = k13SonucList;
	}

	/**
	 * @return the k09Siklik
	 */
	public String getK09Siklik() {
		return k09Siklik;
	}

	/**
	 * @param k09Siklik
	 *            the k09Siklik to set
	 */
	public void setK09Siklik(String k09Siklik) {
		this.k09Siklik = k09Siklik;
	}

	/**
	 * @return the k10Siklik
	 */
	public String getK10Siklik() {
		return k10Siklik;
	}

	/**
	 * @param k10Siklik
	 *            the k10Siklik to set
	 */
	public void setK10Siklik(String k10Siklik) {
		this.k10Siklik = k10Siklik;
	}

	/**
	 * @return the k11Siklik
	 */
	public String getK11Siklik() {
		return k11Siklik;
	}

	/**
	 * @param k11Siklik
	 *            the k11Siklik to set
	 */
	public void setK11Siklik(String k11Siklik) {
		this.k11Siklik = k11Siklik;
	}

	/**
	 * @return the k12Siklik
	 */
	public String getK12Siklik() {
		return k12Siklik;
	}

	/**
	 * @param k12Siklik
	 *            the k12Siklik to set
	 */
	public void setK12Siklik(String k12Siklik) {
		this.k12Siklik = k12Siklik;
	}

	/**
	 * @return the k13Siklik
	 */
	public String getK13Siklik() {
		return k13Siklik;
	}

	/**
	 * @param k13Siklik
	 *            the k13Siklik to set
	 */
	public void setK13Siklik(String k13Siklik) {
		this.k13Siklik = k13Siklik;
	}

	/**
	 * @return the k09SiklikNumber
	 */
	public int getK09SiklikNumber() {
		return k09SiklikNumber;
	}

	/**
	 * @param k09SiklikNumber
	 *            the k09SiklikNumber to set
	 */
	public void setK09SiklikNumber(int k09SiklikNumber) {
		this.k09SiklikNumber = k09SiklikNumber;
	}

	/**
	 * @return the k10SiklikNumber
	 */
	public int getK10SiklikNumber() {
		return k10SiklikNumber;
	}

	/**
	 * @param k10SiklikNumber
	 *            the k10SiklikNumber to set
	 */
	public void setK10SiklikNumber(int k10SiklikNumber) {
		this.k10SiklikNumber = k10SiklikNumber;
	}

	/**
	 * @return the k11SiklikNumber
	 */
	public int getK11SiklikNumber() {
		return k11SiklikNumber;
	}

	/**
	 * @param k11SiklikNumber
	 *            the k11SiklikNumber to set
	 */
	public void setK11SiklikNumber(int k11SiklikNumber) {
		this.k11SiklikNumber = k11SiklikNumber;
	}

	/**
	 * @return the k12SiklikNumber
	 */
	public int getK12SiklikNumber() {
		return k12SiklikNumber;
	}

	/**
	 * @param k12SiklikNumber
	 *            the k12SiklikNumber to set
	 */
	public void setK12SiklikNumber(int k12SiklikNumber) {
		this.k12SiklikNumber = k12SiklikNumber;
	}

	/**
	 * @return the k13SiklikNumber
	 */
	public int getK13SiklikNumber() {
		return k13SiklikNumber;
	}

	/**
	 * @param k13SiklikNumber
	 *            the k13SiklikNumber to set
	 */
	public void setK13SiklikNumber(int k13SiklikNumber) {
		this.k13SiklikNumber = k13SiklikNumber;
	}

	/**
	 * @return the k09PartiSayisi
	 */
	public int getK09PartiSayisi() {
		return k09PartiSayisi;
	}

	/**
	 * @param k09PartiSayisi
	 *            the k09PartiSayisi to set
	 */
	public void setK09PartiSayisi(int k09PartiSayisi) {
		this.k09PartiSayisi = k09PartiSayisi;
	}

	/**
	 * @return the k10PartiSayisi
	 */
	public int getK10PartiSayisi() {
		return k10PartiSayisi;
	}

	/**
	 * @param k10PartiSayisi
	 *            the k10PartiSayisi to set
	 */
	public void setK10PartiSayisi(int k10PartiSayisi) {
		this.k10PartiSayisi = k10PartiSayisi;
	}

	/**
	 * @return the k11PartiSayisi
	 */
	public int getK11PartiSayisi() {
		return k11PartiSayisi;
	}

	/**
	 * @param k11PartiSayisi
	 *            the k11PartiSayisi to set
	 */
	public void setK11PartiSayisi(int k11PartiSayisi) {
		this.k11PartiSayisi = k11PartiSayisi;
	}

	/**
	 * @return the k12PartiSayisi
	 */
	public int getK12PartiSayisi() {
		return k12PartiSayisi;
	}

	/**
	 * @param k12PartiSayisi
	 *            the k12PartiSayisi to set
	 */
	public void setK12PartiSayisi(int k12PartiSayisi) {
		this.k12PartiSayisi = k12PartiSayisi;
	}

	/**
	 * @return the k13PartiSayisi
	 */
	public int getK13PartiSayisi() {
		return k13PartiSayisi;
	}

	/**
	 * @param k13PartiSayisi
	 *            the k13PartiSayisi to set
	 */
	public void setK13PartiSayisi(int k13PartiSayisi) {
		this.k13PartiSayisi = k13PartiSayisi;
	}

	/**
	 * @return the pipes
	 */
	public List<Pipe> getPipes() {
		return pipes;
	}

	/**
	 * @param pipes
	 *            the pipes to set
	 */
	public void setPipes(List<Pipe> pipes) {
		this.pipes = pipes;
	}

	/**
	 * @return the u01SonucList
	 */
	public List<TestCentikDarbeSonuc> getU01SonucList() {
		return u01SonucList;
	}

	/**
	 * @param u01SonucList
	 *            the u01SonucList to set
	 */
	public void setU01SonucList(List<TestCentikDarbeSonuc> u01SonucList) {
		this.u01SonucList = u01SonucList;
	}

	/**
	 * @return the u02SonucList
	 */
	public List<TestCentikDarbeSonuc> getU02SonucList() {
		return u02SonucList;
	}

	/**
	 * @param u02SonucList
	 *            the u02SonucList to set
	 */
	public void setU02SonucList(List<TestCentikDarbeSonuc> u02SonucList) {
		this.u02SonucList = u02SonucList;
	}

	/**
	 * @return the m01SonucList
	 */
	public List<TestKaynakSonuc> getM01SonucList() {
		return m01SonucList;
	}

	/**
	 * @param m01SonucList
	 *            the m01SonucList to set
	 */
	public void setM01SonucList(List<TestKaynakSonuc> m01SonucList) {
		this.m01SonucList = m01SonucList;
	}

	/**
	 * @return the h01SonucList
	 */
	public List<TestKaynakSonuc> getH01SonucList() {
		return h01SonucList;
	}

	/**
	 * @param h01SonucList
	 *            the h01SonucList to set
	 */
	public void setH01SonucList(List<TestKaynakSonuc> h01SonucList) {
		this.h01SonucList = h01SonucList;
	}

	/**
	 * @return the h01Siklik
	 */
	public String getH01Siklik() {
		return h01Siklik;
	}

	/**
	 * @param h01Siklik
	 *            the h01Siklik to set
	 */
	public void setH01Siklik(String h01Siklik) {
		this.h01Siklik = h01Siklik;
	}

	/**
	 * @return the h01SiklikNumber
	 */
	public int getH01SiklikNumber() {
		return h01SiklikNumber;
	}

	/**
	 * @param h01SiklikNumber
	 *            the h01SiklikNumber to set
	 */
	public void setH01SiklikNumber(int h01SiklikNumber) {
		this.h01SiklikNumber = h01SiklikNumber;
	}

	/**
	 * @return the h01PartiSayisi
	 */
	public int getH01PartiSayisi() {
		return h01PartiSayisi;
	}

	/**
	 * @param h01PartiSayisi
	 *            the h01PartiSayisi to set
	 */
	public void setH01PartiSayisi(int h01PartiSayisi) {
		this.h01PartiSayisi = h01PartiSayisi;
	}

	/**
	 * @return the k01Siklik
	 */
	public String getK01Siklik() {
		return k01Siklik;
	}

	/**
	 * @param k01Siklik
	 *            the k01Siklik to set
	 */
	public void setK01Siklik(String k01Siklik) {
		this.k01Siklik = k01Siklik;
	}

	/**
	 * @return the k02Siklik
	 */
	public String getK02Siklik() {
		return k02Siklik;
	}

	/**
	 * @param k02Siklik
	 *            the k02Siklik to set
	 */
	public void setK02Siklik(String k02Siklik) {
		this.k02Siklik = k02Siklik;
	}

	/**
	 * @return the u03SonucList
	 */
	public List<TestCentikDarbeSonuc> getU03SonucList() {
		return u03SonucList;
	}

	/**
	 * @return the dokumler
	 */
	public List<String> getDokumler() {
		return dokumler;
	}

	/**
	 * @param dokumler
	 *            the dokumler to set
	 */
	public void setDokumler(List<String> dokumler) {
		this.dokumler = dokumler;
	}

	/**
	 * @return the gozOlcuPipes
	 */
	public List<TestTahribatsizGozOlcuSonuc> getGozOlcuPipes() {
		return gozOlcuPipes;
	}

	/**
	 * @param gozOlcuPipes
	 *            the gozOlcuPipes to set
	 */
	public void setGozOlcuPipes(List<TestTahribatsizGozOlcuSonuc> gozOlcuPipes) {
		this.gozOlcuPipes = gozOlcuPipes;
	}

	/**
	 * @return the releaseNoteTarihi
	 */
	public Date getReleaseNoteTarihi() {
		return releaseNoteTarihi;
	}

	/**
	 * @param releaseNoteTarihi
	 *            the releaseNoteTarihi to set
	 */
	public void setReleaseNoteTarihi(Date releaseNoteTarihi) {
		this.releaseNoteTarihi = releaseNoteTarihi;
	}

	/**
	 * @return the selectedSorguPipes
	 */
	public List<Pipe> getSelectedSorguPipes() {
		return selectedSorguPipes;
	}

	/**
	 * @param selectedSorguPipes
	 *            the selectedSorguPipes to set
	 */
	public void setSelectedSorguPipes(List<Pipe> selectedSorguPipes) {
		this.selectedSorguPipes = selectedSorguPipes;
	}

	/**
	 * @return the releaseNoteTarihleriList
	 */
	public List<Pipe> getReleaseNoteTarihleriList() {
		return releaseNoteTarihleriList;
	}

	/**
	 * @return the selectedGozOlcuPipes
	 */
	public TestTahribatsizGozOlcuSonuc[] getSelectedGozOlcuPipes() {
		return selectedGozOlcuPipes;
	}

	/**
	 * @param selectedGozOlcuPipes
	 *            the selectedGozOlcuPipes to set
	 */
	public void setSelectedGozOlcuPipes(
			TestTahribatsizGozOlcuSonuc[] selectedGozOlcuPipes) {
		this.selectedGozOlcuPipes = selectedGozOlcuPipes;
	}

	/**
	 * @param releaseNoteTarihleriList
	 *            the releaseNoteTarihleriList to set
	 */
	public void setReleaseNoteTarihleriList(List<Pipe> releaseNoteTarihleriList) {
		this.releaseNoteTarihleriList = releaseNoteTarihleriList;
	}

	/**
	 * @return the selectedReleaseNoteTarihiIlk
	 */
	public Date getSelectedReleaseNoteTarihiIlk() {
		return selectedReleaseNoteTarihiIlk;
	}

	/**
	 * @param selectedReleaseNoteTarihiIlk
	 *            the selectedReleaseNoteTarihiIlk to set
	 */
	public void setSelectedReleaseNoteTarihiIlk(
			Date selectedReleaseNoteTarihiIlk) {
		this.selectedReleaseNoteTarihiIlk = selectedReleaseNoteTarihiIlk;
	}

	/**
	 * @return the selectedReleaseNoteTarihiSon
	 */
	public Date getSelectedReleaseNoteTarihiSon() {
		return selectedReleaseNoteTarihiSon;
	}

	/**
	 * @param selectedReleaseNoteTarihiSon
	 *            the selectedReleaseNoteTarihiSon to set
	 */
	public void setSelectedReleaseNoteTarihiSon(
			Date selectedReleaseNoteTarihiSon) {
		this.selectedReleaseNoteTarihiSon = selectedReleaseNoteTarihiSon;
	}

	/**
	 * @return the selectedSalesItemPipe
	 */
	public Pipe getSelectedSalesItemPipe() {
		return selectedSalesItemPipe;
	}

	/**
	 * @param selectedSalesItemPipe
	 *            the selectedSalesItemPipe to set
	 */
	public void setSelectedSalesItemPipe(Pipe selectedSalesItemPipe) {
		this.selectedSalesItemPipe = selectedSalesItemPipe;
	}

	/**
	 * @return the barkodNo
	 */
	public String getBarkodNo() {
		return barkodNo;
	}

	/**
	 * @param barkodNo
	 *            the barkodNo to set
	 */
	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	/**
	 * @return the selectedUcBirPipes
	 */
	public List<Pipe> getSelectedUcBirPipes() {
		return selectedUcBirPipes;
	}

	/**
	 * @param selectedUcBirPipes
	 *            the selectedUcBirPipes to set
	 */
	public void setSelectedUcBirPipes(List<Pipe> selectedUcBirPipes) {
		this.selectedUcBirPipes = selectedUcBirPipes;
	}

	/**
	 * @return the ucBirTarihi
	 */
	public Date getUcBirTarihi() {
		return ucBirTarihi;
	}

	/**
	 * @param ucBirTarihi
	 *            the ucBirTarihi to set
	 */
	public void setUcBirTarihi(Date ucBirTarihi) {
		this.ucBirTarihi = ucBirTarihi;
	}

	/**
	 * @return the selectedUcBirTarihiIlk
	 */
	public Date getSelectedUcBirTarihiIlk() {
		return selectedUcBirTarihiIlk;
	}

	/**
	 * @param selectedUcBirTarihiIlk
	 *            the selectedUcBirTarihiIlk to set
	 */
	public void setSelectedUcBirTarihiIlk(Date selectedUcBirTarihiIlk) {
		this.selectedUcBirTarihiIlk = selectedUcBirTarihiIlk;
	}

	/**
	 * @return the selectedUcBirTarihiSon
	 */
	public Date getSelectedUcBirTarihiSon() {
		return selectedUcBirTarihiSon;
	}

	/**
	 * @param selectedUcBirTarihiSon
	 *            the selectedUcBirTarihiSon to set
	 */
	public void setSelectedUcBirTarihiSon(Date selectedUcBirTarihiSon) {
		this.selectedUcBirTarihiSon = selectedUcBirTarihiSon;
	}

	/**
	 * @return the ucBirTarihleriList
	 */
	public List<Pipe> getUcBirTarihleriList() {
		return ucBirTarihleriList;
	}

	/**
	 * @param ucBirTarihleriList
	 *            the ucBirTarihleriList to set
	 */
	public void setUcBirTarihleriList(List<Pipe> ucBirTarihleriList) {
		this.ucBirTarihleriList = ucBirTarihleriList;
	}

	/**
	 * @return the dokumlerC03
	 */
	public List<String> getDokumlerC03() {
		return dokumlerC03;
	}

	/**
	 * @param dokumlerC03
	 *            the dokumlerC03 to set
	 */
	public void setDokumlerC03(List<String> dokumlerC03) {
		this.dokumlerC03 = dokumlerC03;
	}

	/**
	 * @return the dokumlerF01
	 */
	public List<String> getDokumlerF01() {
		return dokumlerF01;
	}

	/**
	 * @param dokumlerF01
	 *            the dokumlerF01 to set
	 */
	public void setDokumlerF01(List<String> dokumlerF01) {
		this.dokumlerF01 = dokumlerF01;
	}

	/**
	 * @return the dokumlerF02
	 */
	public List<String> getDokumlerF02() {
		return dokumlerF02;
	}

	/**
	 * @param dokumlerF02
	 *            the dokumlerF02 to set
	 */
	public void setDokumlerF02(List<String> dokumlerF02) {
		this.dokumlerF02 = dokumlerF02;
	}

	/**
	 * @return the dokumlerF03
	 */
	public List<String> getDokumlerF03() {
		return dokumlerF03;
	}

	/**
	 * @param dokumlerF03
	 *            the dokumlerF03 to set
	 */
	public void setDokumlerF03(List<String> dokumlerF03) {
		this.dokumlerF03 = dokumlerF03;
	}

	/**
	 * @return the dokumlerF04
	 */
	public List<String> getDokumlerF04() {
		return dokumlerF04;
	}

	/**
	 * @param dokumlerF04
	 *            the dokumlerF04 to set
	 */
	public void setDokumlerF04(List<String> dokumlerF04) {
		this.dokumlerF04 = dokumlerF04;
	}

	/**
	 * @return the dokumlerF05
	 */
	public List<String> getDokumlerF05() {
		return dokumlerF05;
	}

	/**
	 * @param dokumlerF05
	 *            the dokumlerF05 to set
	 */
	public void setDokumlerF05(List<String> dokumlerF05) {
		this.dokumlerF05 = dokumlerF05;
	}

	/**
	 * @return the dokumlerF06
	 */
	public List<String> getDokumlerF06() {
		return dokumlerF06;
	}

	/**
	 * @param dokumlerF06
	 *            the dokumlerF06 to set
	 */
	public void setDokumlerF06(List<String> dokumlerF06) {
		this.dokumlerF06 = dokumlerF06;
	}

	/**
	 * @return the dokumlerZ01
	 */
	public List<String> getDokumlerZ01() {
		return dokumlerZ01;
	}

	/**
	 * @param dokumlerZ01
	 *            the dokumlerZ01 to set
	 */
	public void setDokumlerZ01(List<String> dokumlerZ01) {
		this.dokumlerZ01 = dokumlerZ01;
	}

	/**
	 * @return the dokumlerZ02
	 */
	public List<String> getDokumlerZ02() {
		return dokumlerZ02;
	}

	/**
	 * @param dokumlerZ02
	 *            the dokumlerZ02 to set
	 */
	public void setDokumlerZ02(List<String> dokumlerZ02) {
		this.dokumlerZ02 = dokumlerZ02;
	}

	/**
	 * @return the dokumlerZ03
	 */
	public List<String> getDokumlerZ03() {
		return dokumlerZ03;
	}

	/**
	 * @param dokumlerZ03
	 *            the dokumlerZ03 to set
	 */
	public void setDokumlerZ03(List<String> dokumlerZ03) {
		this.dokumlerZ03 = dokumlerZ03;
	}

	/**
	 * @return the dokumlerZ06
	 */
	public List<String> getDokumlerZ06() {
		return dokumlerZ06;
	}

	/**
	 * @param dokumlerZ06
	 *            the dokumlerZ06 to set
	 */
	public void setDokumlerZ06(List<String> dokumlerZ06) {
		this.dokumlerZ06 = dokumlerZ06;
	}

	/**
	 * @return the dokumlerZ07
	 */
	public List<String> getDokumlerZ07() {
		return dokumlerZ07;
	}

	/**
	 * @param dokumlerZ07
	 *            the dokumlerZ07 to set
	 */
	public void setDokumlerZ07(List<String> dokumlerZ07) {
		this.dokumlerZ07 = dokumlerZ07;
	}

	/**
	 * @return the dokumlerK01
	 */
	public List<String> getDokumlerK01() {
		return dokumlerK01;
	}

	/**
	 * @param dokumlerK01
	 *            the dokumlerK01 to set
	 */
	public void setDokumlerK01(List<String> dokumlerK01) {
		this.dokumlerK01 = dokumlerK01;
	}

	/**
	 * @return the dokumlerK02
	 */
	public List<String> getDokumlerK02() {
		return dokumlerK02;
	}

	/**
	 * @param dokumlerK02
	 *            the dokumlerK02 to set
	 */
	public void setDokumlerK02(List<String> dokumlerK02) {
		this.dokumlerK02 = dokumlerK02;
	}

	/**
	 * @return the dokumlerK03
	 */
	public List<String> getDokumlerK03() {
		return dokumlerK03;
	}

	/**
	 * @param dokumlerK03
	 *            the dokumlerK03 to set
	 */
	public void setDokumlerK03(List<String> dokumlerK03) {
		this.dokumlerK03 = dokumlerK03;
	}

	/**
	 * @return the dokumlerK04
	 */
	public List<String> getDokumlerK04() {
		return dokumlerK04;
	}

	/**
	 * @param dokumlerK04
	 *            the dokumlerK04 to set
	 */
	public void setDokumlerK04(List<String> dokumlerK04) {
		this.dokumlerK04 = dokumlerK04;
	}

	/**
	 * @return the dokumlerK05
	 */
	public List<String> getDokumlerK05() {
		return dokumlerK05;
	}

	/**
	 * @param dokumlerK05
	 *            the dokumlerK05 to set
	 */
	public void setDokumlerK05(List<String> dokumlerK05) {
		this.dokumlerK05 = dokumlerK05;
	}

	/**
	 * @return the dokumlerK08
	 */
	public List<String> getDokumlerK08() {
		return dokumlerK08;
	}

	/**
	 * @param dokumlerK08
	 *            the dokumlerK08 to set
	 */
	public void setDokumlerK08(List<String> dokumlerK08) {
		this.dokumlerK08 = dokumlerK08;
	}

	/**
	 * @return the dokumlerK09
	 */
	public List<String> getDokumlerK09() {
		return dokumlerK09;
	}

	/**
	 * @param dokumlerK09
	 *            the dokumlerK09 to set
	 */
	public void setDokumlerK09(List<String> dokumlerK09) {
		this.dokumlerK09 = dokumlerK09;
	}

	/**
	 * @return the dokumlerK10
	 */
	public List<String> getDokumlerK10() {
		return dokumlerK10;
	}

	/**
	 * @param dokumlerK10
	 *            the dokumlerK10 to set
	 */
	public void setDokumlerK10(List<String> dokumlerK10) {
		this.dokumlerK10 = dokumlerK10;
	}

	/**
	 * @return the dokumlerK11
	 */
	public List<String> getDokumlerK11() {
		return dokumlerK11;
	}

	/**
	 * @param dokumlerK11
	 *            the dokumlerK11 to set
	 */
	public void setDokumlerK11(List<String> dokumlerK11) {
		this.dokumlerK11 = dokumlerK11;
	}

	/**
	 * @return the dokumlerK12
	 */
	public List<String> getDokumlerK12() {
		return dokumlerK12;
	}

	/**
	 * @param dokumlerK12
	 *            the dokumlerK12 to set
	 */
	public void setDokumlerK12(List<String> dokumlerK12) {
		this.dokumlerK12 = dokumlerK12;
	}

	/**
	 * @return the dokumlerK13
	 */
	public List<String> getDokumlerK13() {
		return dokumlerK13;
	}

	/**
	 * @param dokumlerK13
	 *            the dokumlerK13 to set
	 */
	public void setDokumlerK13(List<String> dokumlerK13) {
		this.dokumlerK13 = dokumlerK13;
	}

	/**
	 * @return the dokumlerU01
	 */
	public List<String> getDokumlerU01() {
		return dokumlerU01;
	}

	/**
	 * @param dokumlerU01
	 *            the dokumlerU01 to set
	 */
	public void setDokumlerU01(List<String> dokumlerU01) {
		this.dokumlerU01 = dokumlerU01;
	}

	/**
	 * @return the dokumlerU02
	 */
	public List<String> getDokumlerU02() {
		return dokumlerU02;
	}

	/**
	 * @param dokumlerU02
	 *            the dokumlerU02 to set
	 */
	public void setDokumlerU02(List<String> dokumlerU02) {
		this.dokumlerU02 = dokumlerU02;
	}

	/**
	 * @return the dokumlerU03
	 */
	public List<String> getDokumlerU03() {
		return dokumlerU03;
	}

	/**
	 * @param dokumlerU03
	 *            the dokumlerU03 to set
	 */
	public void setDokumlerU03(List<String> dokumlerU03) {
		this.dokumlerU03 = dokumlerU03;
	}

	/**
	 * @return the dokumlerU04
	 */
	public List<String> getDokumlerU04() {
		return dokumlerU04;
	}

	/**
	 * @param dokumlerU04
	 *            the dokumlerU04 to set
	 */
	public void setDokumlerU04(List<String> dokumlerU04) {
		this.dokumlerU04 = dokumlerU04;
	}

	/**
	 * @return the dokumlerU05
	 */
	public List<String> getDokumlerU05() {
		return dokumlerU05;
	}

	/**
	 * @param dokumlerU05
	 *            the dokumlerU05 to set
	 */
	public void setDokumlerU05(List<String> dokumlerU05) {
		this.dokumlerU05 = dokumlerU05;
	}

	/**
	 * @return the dokumlerD01
	 */
	public List<String> getDokumlerD01() {
		return dokumlerD01;
	}

	/**
	 * @param dokumlerD01
	 *            the dokumlerD01 to set
	 */
	public void setDokumlerD01(List<String> dokumlerD01) {
		this.dokumlerD01 = dokumlerD01;
	}

	/**
	 * @return the dokumlerD02
	 */
	public List<String> getDokumlerD02() {
		return dokumlerD02;
	}

	/**
	 * @param dokumlerD02
	 *            the dokumlerD02 to set
	 */
	public void setDokumlerD02(List<String> dokumlerD02) {
		this.dokumlerD02 = dokumlerD02;
	}

	/**
	 * @return the dokumlerM01
	 */
	public List<String> getDokumlerM01() {
		return dokumlerM01;
	}

	/**
	 * @param dokumlerM01
	 *            the dokumlerM01 to set
	 */
	public void setDokumlerM01(List<String> dokumlerM01) {
		this.dokumlerM01 = dokumlerM01;
	}

	/**
	 * @return the dokumlerH01
	 */
	public List<String> getDokumlerH01() {
		return dokumlerH01;
	}

	/**
	 * @param dokumlerH01
	 *            the dokumlerH01 to set
	 */
	public void setDokumlerH01(List<String> dokumlerH01) {
		this.dokumlerH01 = dokumlerH01;
	}

}
