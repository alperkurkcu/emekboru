/**
 * 
 */
package com.emekboru.beans.satinalma;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.satinalma.SatinalmaAmbarMalzemeTalepFormIcerik;
import com.emekboru.jpa.satinalma.SatinalmaMalzemeHizmetTalepFormu;
import com.emekboru.jpa.satinalma.SatinalmaMalzemeHizmetTalepFormuIcerik;
import com.emekboru.jpaman.satinalma.SatinalmaAmbarMalzemeTalepFormIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaMalzemeHizmetTalepFormuIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaMalzemeHizmetTalepFormuManager;
import com.emekboru.utils.DateTrans;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */

@ManagedBean(name = "satinalmaMalzemeHizmetTalepFormuIcerikBean")
@ViewScoped
public class SatinalmaMalzemeHizmetTalepFormuIcerikBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<SatinalmaMalzemeHizmetTalepFormuIcerik> allSatinalmaMalzemeHizmetTalepFormuIcerikList = new ArrayList<SatinalmaMalzemeHizmetTalepFormuIcerik>();

	private SatinalmaMalzemeHizmetTalepFormuIcerik satinalmaMalzemeHizmetTalepFormuIcerikEkran = new SatinalmaMalzemeHizmetTalepFormuIcerik();
	private SatinalmaMalzemeHizmetTalepFormu satinalmaMalzemeHizmetTalepFormu = new SatinalmaMalzemeHizmetTalepFormu();
	private SatinalmaMalzemeHizmetTalepFormuIcerik editForm = new SatinalmaMalzemeHizmetTalepFormuIcerik();

	// private SatinalmaMalzemeHizmetTalepFormuIcerik[]
	// satinalmaMalzemeHizmetTalepFormuIcerikFormList;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	int siraNo = 1;

	private List<SatinalmaAmbarMalzemeTalepFormIcerik> ambarIcerikList = new ArrayList<SatinalmaAmbarMalzemeTalepFormIcerik>();

	@PostConstruct
	private void load() {

		updateButtonRender = false;
		satinalmaMalzemeHizmetTalepFormuIcerikEkran = new SatinalmaMalzemeHizmetTalepFormuIcerik();
		satinalmaMalzemeHizmetTalepFormu = new SatinalmaMalzemeHizmetTalepFormu();
	}

	public void addIcerik() {

		satinalmaMalzemeHizmetTalepFormuIcerikEkran = new SatinalmaMalzemeHizmetTalepFormuIcerik();
		// today set ediliyor
		java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(
				DateTrans.calendar.getTime().getTime());
		satinalmaMalzemeHizmetTalepFormuIcerikEkran
				.setIhtiyacSuresi(currentTimestamp);
		updateButtonRender = false;
	}

	public void onRowEdit(RowEditEvent event) {

		SatinalmaMalzemeHizmetTalepFormuIcerikManager icerikManager = new SatinalmaMalzemeHizmetTalepFormuIcerikManager();
		editForm = (SatinalmaMalzemeHizmetTalepFormuIcerik) event.getObject();
		editForm.setKalan(editForm.getMiktar());
		icerikManager
				.updateEntity(((SatinalmaMalzemeHizmetTalepFormuIcerik) event
						.getObject()));
	}

	public void addOrUpdateIcerik(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmFormId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		SatinalmaMalzemeHizmetTalepFormuManager manager = new SatinalmaMalzemeHizmetTalepFormuManager();
		SatinalmaMalzemeHizmetTalepFormuIcerikManager formManager = new SatinalmaMalzemeHizmetTalepFormuIcerikManager();

		siraNoBul(prmFormId);

		if (satinalmaMalzemeHizmetTalepFormuIcerikEkran.getId() == null) {
			try {
				satinalmaMalzemeHizmetTalepFormuIcerikEkran
						.setSatinalmaMalzemeHizmetTalepFormu(manager
								.findByField(
										SatinalmaMalzemeHizmetTalepFormu.class,
										"id", prmFormId).get(0));

				satinalmaMalzemeHizmetTalepFormuIcerikEkran
						.setEklemeZamani(UtilInsCore.getTarihZaman());
				satinalmaMalzemeHizmetTalepFormuIcerikEkran
						.setEkleyenKullanici(userBean.getUser().getId());

				satinalmaMalzemeHizmetTalepFormuIcerikEkran.setSiraNo(siraNo);
				satinalmaMalzemeHizmetTalepFormuIcerikEkran
						.setKalan(satinalmaMalzemeHizmetTalepFormuIcerikEkran
								.getMiktar());
				satinalmaMalzemeHizmetTalepFormuIcerikEkran
						.setStokTanimlarBirim(satinalmaMalzemeHizmetTalepFormuIcerikEkran
								.getStokUrunKartlari().getStokTanimlarBirim());

				formManager
						.enterNew(satinalmaMalzemeHizmetTalepFormuIcerikEkran);
			} catch (Exception e1) {

				e1.printStackTrace();
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"EKLEME HATASI, LÜTFEN 'Sarf Merkezi' ve 'Departman' SEÇİNİZ!",
								null));
				this.satinalmaMalzemeHizmetTalepFormuIcerikEkran = new SatinalmaMalzemeHizmetTalepFormuIcerik();
				return;
			}

			this.satinalmaMalzemeHizmetTalepFormuIcerikEkran = new SatinalmaMalzemeHizmetTalepFormuIcerik();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"İSTEK KALEMİ BAŞARIYLA EKLENMİŞTİR!", null));

		} else {

			satinalmaMalzemeHizmetTalepFormuIcerikEkran
					.setGuncellemeZamani(UtilInsCore.getTarihZaman());
			satinalmaMalzemeHizmetTalepFormuIcerikEkran
					.setGuncelleyenKullanici(userBean.getUser().getId());
			formManager
					.updateEntity(satinalmaMalzemeHizmetTalepFormuIcerikEkran);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"İSTEK KALEMİ BAŞARIYLA G>ÜNCELLENMİŞTİR!", null));
		}

		fillTestList(prmFormId);
	}

	public void deleteForm() {

		FacesContext context = FacesContext.getCurrentInstance();
		if (satinalmaMalzemeHizmetTalepFormuIcerikEkran.getId() == null) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL,
					"SİLME HATASI, SİLİNECEK MALZEME SEÇİNİZ!!", null));

			return;
		}

		Integer prmFormId = satinalmaMalzemeHizmetTalepFormuIcerikEkran
				.getSatinalmaMalzemeHizmetTalepFormu().getId();

		SatinalmaMalzemeHizmetTalepFormuIcerikManager formManager = new SatinalmaMalzemeHizmetTalepFormuIcerikManager();

		formManager.delete(satinalmaMalzemeHizmetTalepFormuIcerikEkran);
		fillTestList(prmFormId);
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				"SEÇİLEN MALZEME BAŞARIYLA SİLİNMİŞTİR!!", null));
	}

	public void fillTestList(Integer formId) {

		SatinalmaMalzemeHizmetTalepFormuIcerikManager manager = new SatinalmaMalzemeHizmetTalepFormuIcerikManager();
		allSatinalmaMalzemeHizmetTalepFormuIcerikList = manager
				.getAllSatinalmaMalzemeHizmetTalepFormuIcerik(formId);
		satinalmaMalzemeHizmetTalepFormuIcerikEkran = new SatinalmaMalzemeHizmetTalepFormuIcerik();
	}

	public void formListener(SelectEvent event) {

		updateButtonRender = true;
	}

	public void makeFalse() {

		updateButtonRender = false;
	}

	private void siraNoBul(int formId) {

		SatinalmaMalzemeHizmetTalepFormuIcerikManager formManager = new SatinalmaMalzemeHizmetTalepFormuIcerikManager();

		if (formManager.findByField(
				SatinalmaMalzemeHizmetTalepFormuIcerik.class,
				"satinalmaMalzemeHizmetTalepFormu.id", formId) != null) {
			siraNo = formManager.findByField(
					SatinalmaMalzemeHizmetTalepFormuIcerik.class,
					"satinalmaMalzemeHizmetTalepFormu.id", formId).size() + 1;
		}
	}

	public void addFromAmbarIcerik(Integer ambarMalzemeFormId) {

		SatinalmaMalzemeHizmetTalepFormuManager malzemeManager = new SatinalmaMalzemeHizmetTalepFormuManager();
		SatinalmaMalzemeHizmetTalepFormuIcerikManager malzemeIcerikManager = new SatinalmaMalzemeHizmetTalepFormuIcerikManager();
		SatinalmaAmbarMalzemeTalepFormIcerikManager ambarIcerikManager = new SatinalmaAmbarMalzemeTalepFormIcerikManager();

		try {

			ambarIcerikList = ambarIcerikManager
					.getAllSatinalmaAmbarMalzemeTalepFormIcerik(ambarMalzemeFormId);
			satinalmaMalzemeHizmetTalepFormu = malzemeManager
					.getAllSatinalmaMalzemeHizmetTalepFormuByFormId(
							ambarMalzemeFormId).get(0);

		} catch (Exception e) {
			System.out.println("SatinalmaMalzemeHizmetTalepFormuBean: "
					+ e.toString());
		}

		for (int i = 0; i < ambarIcerikList.size(); i++) {

			satinalmaMalzemeHizmetTalepFormuIcerikEkran
					.setSatinalmaMalzemeHizmetTalepFormu(satinalmaMalzemeHizmetTalepFormu);
			satinalmaMalzemeHizmetTalepFormuIcerikEkran
					.setEklemeZamani(UtilInsCore.getTarihZaman());
			satinalmaMalzemeHizmetTalepFormuIcerikEkran
					.setEkleyenKullanici(userBean.getUser().getId());
			satinalmaMalzemeHizmetTalepFormuIcerikEkran
					.setStokUrunKartlari(ambarIcerikList.get(i)
							.getStokUrunKartlari());
			satinalmaMalzemeHizmetTalepFormuIcerikEkran
					.setMiktar(ambarIcerikList.get(i).getMiktar());
			satinalmaMalzemeHizmetTalepFormuIcerikEkran
					.setStokTanimlarBirim(ambarIcerikList.get(i)
							.getStokTanimlarBirim());
			satinalmaMalzemeHizmetTalepFormuIcerikEkran.setSarf(ambarIcerikList
					.get(i).getSarf());
			satinalmaMalzemeHizmetTalepFormuIcerikEkran
					.setUretim(ambarIcerikList.get(i).getUretim());
			satinalmaMalzemeHizmetTalepFormuIcerikEkran
					.setBakim(ambarIcerikList.get(i).getBakim());
			satinalmaMalzemeHizmetTalepFormuIcerikEkran
					.setDemirbas(ambarIcerikList.get(i).getDemirbas());
			satinalmaMalzemeHizmetTalepFormuIcerikEkran
					.setIhtiyacSuresi(ambarIcerikList.get(i).getIhtiyacSuresi());
			satinalmaMalzemeHizmetTalepFormuIcerikEkran
					.setSiparisMiktari(ambarIcerikList.get(i)
							.getStokUrunKartlari().getMinStok());

			siraNoBul(satinalmaMalzemeHizmetTalepFormu.getId());

			malzemeIcerikManager
					.enterNew(satinalmaMalzemeHizmetTalepFormuIcerikEkran);

			satinalmaMalzemeHizmetTalepFormuIcerikEkran = new SatinalmaMalzemeHizmetTalepFormuIcerik();
		}

	}

	// SETTERS GETTERS
	public List<SatinalmaMalzemeHizmetTalepFormuIcerik> getAllSatinalmaMalzemeHizmetTalepFormuIcerikList() {
		return allSatinalmaMalzemeHizmetTalepFormuIcerikList;
	}

	public void setAllSatinalmaMalzemeHizmetTalepFormuIcerikList(
			List<SatinalmaMalzemeHizmetTalepFormuIcerik> allSatinalmaMalzemeHizmetTalepFormuIcerikList) {
		this.allSatinalmaMalzemeHizmetTalepFormuIcerikList = allSatinalmaMalzemeHizmetTalepFormuIcerikList;
	}

	public SatinalmaMalzemeHizmetTalepFormuIcerik getSatinalmaMalzemeHizmetTalepFormuIcerikEkran() {
		return satinalmaMalzemeHizmetTalepFormuIcerikEkran;
	}

	public void setSatinalmaMalzemeHizmetTalepFormuIcerikEkran(
			SatinalmaMalzemeHizmetTalepFormuIcerik satinalmaMalzemeHizmetTalepFormuIcerikEkran) {
		this.satinalmaMalzemeHizmetTalepFormuIcerikEkran = satinalmaMalzemeHizmetTalepFormuIcerikEkran;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
