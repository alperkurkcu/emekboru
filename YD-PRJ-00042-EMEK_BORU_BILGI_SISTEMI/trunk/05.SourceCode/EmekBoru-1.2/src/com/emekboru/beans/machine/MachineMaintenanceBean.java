package com.emekboru.beans.machine;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Machine;
import com.emekboru.jpa.MachinePart;
import com.emekboru.jpa.MaintenancePlanType;
import com.emekboru.jpa.PeriodicPlan;
import com.emekboru.jpaman.MachineManager;
import com.emekboru.jpaman.MachinePartManager;
import com.emekboru.jpaman.MaintenancePlanTypeManager;
import com.emekboru.jpaman.PeriodicPlanManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "machineMaintenanceBean")
@SessionScoped
public class MachineMaintenanceBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	private List<Machine> allMachineList;
	private List<MachinePart> allMachinePartList;
	private List<MachinePart> partsOfMachineList;
	private List<MaintenancePlanType> allMaintenancePlanTypeList;
	private List<PeriodicPlan> allPeriodicPlanList;

	private List<Integer> yearList;

	private Machine selectedMachine;

	/**
	 * @return the machine
	 */
	public Machine getMachine() {
		return selectedMachine;
	}

	/**
	 * @param machine
	 *            the machine to set
	 */
	public void setMachine(Machine machine) {
		this.selectedMachine = machine;
	}

	public MachineMaintenanceBean() {

		allMachineList = new ArrayList<Machine>(0);
		allMachinePartList = new ArrayList<MachinePart>(0);
		allMaintenancePlanTypeList = new ArrayList<MaintenancePlanType>(0);
		allPeriodicPlanList = new ArrayList<PeriodicPlan>(0);
		yearList = new ArrayList<Integer>(0);
	}

	// ********************************************************************* //
	// ********* METHODS RESPONSIBLE FOR LOADING MACHINE LISTS ************* //
	// ********************************************************************* //

	public void loadAllMachines() {

		MachineManager machineManager = new MachineManager();
		// allMachineList = machineManager.findAll(Machine.class);

		allMachineList = machineManager.findAllOrdered(Machine.class, "name");
	}

	public void loadAllMachineParts() {

		MachinePartManager partManager = new MachinePartManager();
		// allMachinePartList = partManager.findAll(MachinePart.class);

		allMachinePartList = partManager.findAllOrdered(MachinePart.class,
				"partName");
	}

	public void loadAllPartsOfMachine(int machineId) {

		partsOfMachineList = MachinePartManager.findByMachineId(machineId);

		System.err.println("machineId: " + machineId + " Size: "
				+ partsOfMachineList.size());

	}

	public void loadAllMaintenancePlanTypes() {

		MaintenancePlanTypeManager planTypeManager = new MaintenancePlanTypeManager();
		allMaintenancePlanTypeList = planTypeManager
				.findAll(MaintenancePlanType.class);
	}

	public void loadAllPeriodicPlans() {

		PeriodicPlanManager planManager = new PeriodicPlanManager();
		allPeriodicPlanList = planManager.findAllOrdered(PeriodicPlan.class,
				"periodicPlanId");
	}

	public void loadYears() {

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		String currentDate = dateFormat.format(date);
		Integer currentYear = Integer.parseInt(currentDate.substring(6));

		yearList.add(currentYear - 1);
		yearList.add(currentYear);
		yearList.add(currentYear + 1);
		yearList.add(currentYear + 2);
	}

	// ********************************************************************* //
	// **************** METHODS FOR REDIRECTING TO PAGES ******************* //
	// ********************************************************************* //

	public void goToPeriodicMaintenancePlanPage() {

		loadAllMachines();
		loadAllMachineParts();
		loadAllMaintenancePlanTypes();
		loadAllPeriodicPlans();
		loadYears();
		FacesContextUtils
				.redirect(config.getPageUrl().PERIODIC_MAINTENANCE_PLAN_PAGE);
	}

	public void goToBakimTakvimiPage() {

		FacesContextUtils
				.redirect(config.getPageUrl().MAINTENANCE_SCHEDULE_PAGE);
	}

	// ********************************************************************* //
	// *********************** GETTERS AND SETTERS ************************* //
	// ********************************************************************* //

	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	public List<Machine> getAllMachineList() {
		return allMachineList;
	}

	public void setAllMachineList(List<Machine> allMachineList) {
		this.allMachineList = allMachineList;
	}

	public List<MachinePart> getAllMachinePartList() {
		return allMachinePartList;
	}

	public void setAllMachinePartList(List<MachinePart> allMachinePartList) {
		this.allMachinePartList = allMachinePartList;
	}

	public List<MaintenancePlanType> getAllMaintenancePlanTypeList() {
		return allMaintenancePlanTypeList;
	}

	public void setAllMaintenancePlanTypeList(
			List<MaintenancePlanType> allMaintenancePlanTypeList) {
		this.allMaintenancePlanTypeList = allMaintenancePlanTypeList;
	}

	public List<PeriodicPlan> getAllPeriodicPlanList() {
		return allPeriodicPlanList;
	}

	public void setAllMaintenancePlanList(List<PeriodicPlan> allPeriodicPlanList) {
		this.allPeriodicPlanList = allPeriodicPlanList;
	}

	public List<Integer> getYearList() {
		return yearList;
	}

	public void setYearList(List<Integer> yearList) {
		this.yearList = yearList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the partsOfMachineList
	 */
	public List<MachinePart> getPartsOfMachineList() {
		return partsOfMachineList;
	}

	/**
	 * @param partsOfMachineList
	 *            the partsOfMachineList to set
	 */
	public void setPartsOfMachineList(List<MachinePart> partsOfMachineList) {
		this.partsOfMachineList = partsOfMachineList;
	}

}
