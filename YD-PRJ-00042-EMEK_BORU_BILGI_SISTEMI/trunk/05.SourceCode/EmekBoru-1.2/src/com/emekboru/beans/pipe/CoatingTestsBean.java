package com.emekboru.beans.pipe;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.PlannedIsolation;
import com.emekboru.jpa.sales.customer.SalesCustomer;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "coatingTestsBean")
@ViewScoped
public class CoatingTestsBean implements Serializable {

	private static final long serialVersionUID = 1L;
	// private List<Order> coatingOrdersList;
	private List<SalesItem> coatingSalesItemsList;
	// private Order currentOrderItem;
	private SalesItem currentSalesItem;
	// private Order currentOrder;
	private SalesItem currentSales;
	// private Customer currentCompany;
	private SalesCustomer currentCompany;
	private List<Pipe> pipeList;
	private Pipe currentPipe;
	private PlannedIsolation internalIsolation;
	private PlannedIsolation externalIsolation;

	public CoatingTestsBean() {

		resetData();
		// currentOrderItem = new Order();
		currentSalesItem = new SalesItem();
		// currentCompany = new Customer();
		currentCompany = new SalesCustomer();
		pipeList = new ArrayList<Pipe>();
		currentPipe = new Pipe();
		// coatingOrdersList = new ArrayList<Order>();
		coatingSalesItemsList = new ArrayList<SalesItem>();
		// currentOrder = new Order();
		currentSales = new SalesItem();

	}

	public void resetData() {

		externalIsolation = new PlannedIsolation();
		internalIsolation = new PlannedIsolation();
	}

	public void viewCoatingOrderPipes() {

		if (currentSalesItem.getItemId() == 0)
			return;
		PipeManager pipeManager = new PipeManager();
		pipeList = (List<Pipe>) pipeManager.findByField(Pipe.class, "orderId",
				currentSalesItem.getItemId());
	}

	public void updatePipeLocation() {

		PipeManager pipeManager = new PipeManager();
		pipeManager.updateEntity(currentPipe);
		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}

	/*
	 * LOAD TEST SECTION
	 */

	// ---------------------------------------------------------------------------//

	/*
	 * GETTERS AND SETTERS
	 */

	// public List<Order> getCoatingOrdersList() {
	// return coatingOrdersList;
	// }
	//
	// public void setCoatingOrdersList(List<Order> coatingOrdersList) {
	// this.coatingOrdersList = coatingOrdersList;
	// }

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the coatingSalesItemsList
	 */
	public List<SalesItem> getCoatingSalesItemsList() {
		return coatingSalesItemsList;
	}

	/**
	 * @param coatingSalesItemsList
	 *            the coatingSalesItemsList to set
	 */
	public void setCoatingSalesItemsList(List<SalesItem> coatingSalesItemsList) {
		this.coatingSalesItemsList = coatingSalesItemsList;
	}

	// public Order getCurrentOrderItem() {
	// return currentOrderItem;
	// }
	//
	// public void setCurrentOrderItem(Order currentOrderItem) {
	// this.currentOrderItem = currentOrderItem;
	// }

	public List<Pipe> getPipeList() {
		return pipeList;
	}

	public void setPipeList(List<Pipe> pipeList) {
		this.pipeList = pipeList;
	}

	public Pipe getCurrentPipe() {
		return currentPipe;
	}

	public void setCurrentPipe(Pipe currentPipe) {
		this.currentPipe = currentPipe;
	}

	public PlannedIsolation getInternalIsolation() {
		return internalIsolation;
	}

	public void setInternalIsolation(PlannedIsolation internalIsolation) {
		this.internalIsolation = internalIsolation;
	}

	public PlannedIsolation getExternalIsolation() {
		return externalIsolation;
	}

	public void setExternalIsolation(PlannedIsolation externalIsolation) {
		this.externalIsolation = externalIsolation;
	}

	// public Order getCurrentOrder() {
	// return currentOrder;
	// }
	//
	// public void setCurrentOrder(Order currentOrder) {
	// this.currentOrder = currentOrder;
	// }

	// public Customer getCurrentCompany() {
	// return currentCompany;
	// }
	//
	// public void setCurrentCompany(Customer currentCompany) {
	// this.currentCompany = currentCompany;
	// }

	/**
	 * @return the currentSalesItem
	 */
	public SalesItem getCurrentSalesItem() {
		return currentSalesItem;
	}

	/**
	 * @param currentSalesItem
	 *            the currentSalesItem to set
	 */
	public void setCurrentSalesItem(SalesItem currentSalesItem) {
		this.currentSalesItem = currentSalesItem;
	}

	/**
	 * @return the currentSales
	 */
	public SalesItem getCurrentSales() {
		return currentSales;
	}

	/**
	 * @param currentSales
	 *            the currentSales to set
	 */
	public void setCurrentSales(SalesItem currentSales) {
		this.currentSales = currentSales;
	}

	/**
	 * @return the currentCompany
	 */
	public SalesCustomer getCurrentCompany() {
		return currentCompany;
	}

	/**
	 * @param currentCompany
	 *            the currentCompany to set
	 */
	public void setCurrentCompany(SalesCustomer currentCompany) {
		this.currentCompany = currentCompany;
	}
}
