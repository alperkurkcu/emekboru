/**
 * 
 */
package com.emekboru.beans.employee;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Employee;
import com.emekboru.jpa.employee.EmployeeAdvanceRequest;
import com.emekboru.jpaman.employee.EmployeeAdvanceManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.ReportUtil;

/**
 * @author kursat
 * 
 */
@ManagedBean(name = "employeeAdvanceRequestBean")
@ViewScoped
public class EmployeeAdvanceRequestBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean configBean;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private List<EmployeeAdvanceRequest> allEmployeeAdvanceRequests = new ArrayList<EmployeeAdvanceRequest>();
	private List<EmployeeAdvanceRequest> allSubEmployeeAdvanceRequests = new ArrayList<EmployeeAdvanceRequest>();
	private List<EmployeeAdvanceRequest> allSelfEmployeeAdvanceRequests = new ArrayList<EmployeeAdvanceRequest>();

	private Employee currentEmployee = new Employee();

	private EmployeeAdvanceRequest selectedEmployeeAdvanceRequest = new EmployeeAdvanceRequest();
	private EmployeeAdvanceRequest HRSelectedEmployeeAdvanceRequest = new EmployeeAdvanceRequest();
	private EmployeeAdvanceRequest selectedSubEmployeeAdvanceRequest = new EmployeeAdvanceRequest();

	private EmployeeAdvanceRequest newEmployeeAdvanceRequest = new EmployeeAdvanceRequest();

	public EmployeeAdvanceRequestBean() {
	}

	@PostConstruct
	public void load() {
		this.currentEmployee = this.userBean.getUser().getEmployee();

		EmployeeAdvanceManager employeeAdvanceManager = new EmployeeAdvanceManager();
		this.allEmployeeAdvanceRequests = employeeAdvanceManager.findAll();

		this.allSubEmployeeAdvanceRequests = employeeAdvanceManager
				.findAdvanceRequestsOfSubworkersOfEmployee(this.currentEmployee);

		this.allSelfEmployeeAdvanceRequests = employeeAdvanceManager
				.findAllAdvanceRequestsOfEmployee(currentEmployee);

	}

	/**
	 * WAITING 0 APPROVED 1 DECLINED 2
	 */

	public void makeAdvanceRequest() {

		int ekleyenKullanici = this.userBean.getUser().getId();
		Timestamp eklemeZamani = new java.sql.Timestamp(
				new java.util.Date().getTime());

		newEmployeeAdvanceRequest.setEmployee(currentEmployee);
		newEmployeeAdvanceRequest
				.setSupervisorApproval(EmployeeAdvanceRequest.PENDING);
		newEmployeeAdvanceRequest.setHrApproval(EmployeeAdvanceRequest.PENDING);
		// newEmployeeAdvanceRequest
		// .setSupervisor(currentEmployee.getSupervisor());
		newEmployeeAdvanceRequest.setRequestDate(eklemeZamani);
		newEmployeeAdvanceRequest.setEklemeZamani(eklemeZamani);
		newEmployeeAdvanceRequest.setEkleyenKullanici(ekleyenKullanici);

		EmployeeAdvanceManager employeeDayoffManager = new EmployeeAdvanceManager();
		employeeDayoffManager.enterNew(newEmployeeAdvanceRequest);

		FacesContextUtils.addPlainWarnMessage("Kayıt başarıyla girildi!");

		newEmployeeAdvanceRequest = new EmployeeAdvanceRequest();

		this.load();
	}

	public void declineSubEmployeeAdvanceRequest() {

		if (selectedSubEmployeeAdvanceRequest == null) {

			FacesContextUtils
					.addPlainWarnMessage("Lüften reddetmek istediğiniz isteği seçiniz!");
			return;
		}

		if (selectedSubEmployeeAdvanceRequest.getSupervisorApproval() != EmployeeAdvanceRequest.PENDING) {

			FacesContextUtils
					.addPlainWarnMessage("Onayladığınız ya da reddettiğiniz bir isteği değiştiremezsiniz!");
			return;
		}

		selectedSubEmployeeAdvanceRequest
				.setSupervisorApproval(EmployeeAdvanceRequest.DECLINED);
		EmployeeAdvanceManager employeeDayoffManager = new EmployeeAdvanceManager();

		employeeDayoffManager.updateEntity(selectedSubEmployeeAdvanceRequest);

		FacesContextUtils.addPlainWarnMessage("İsteği reddettiniz!");

		this.load();

	}

	public void approveSubEmployeeAdvanceRequest() {

		if (selectedSubEmployeeAdvanceRequest == null) {

			FacesContextUtils
					.addPlainWarnMessage("Lüften onaylamak istediğiniz isteği seçiniz!");
			return;
		}

		if (selectedSubEmployeeAdvanceRequest.getSupervisorApproval() != EmployeeAdvanceRequest.PENDING) {

			FacesContextUtils
					.addPlainWarnMessage("Onayladığınız ya da reddettiğiniz bir isteği değiştiremezsiniz!");
			return;
		}

		selectedSubEmployeeAdvanceRequest
				.setSupervisorApproval(EmployeeAdvanceRequest.APPROVED);
		EmployeeAdvanceManager employeeDayoffManager = new EmployeeAdvanceManager();

		employeeDayoffManager.updateEntity(selectedSubEmployeeAdvanceRequest);

		FacesContextUtils.addPlainWarnMessage("İsteği onayladınız!");

		this.load();

	}

	public void declineEmployeeAdvanceRequest() {

		if (HRSelectedEmployeeAdvanceRequest == null) {

			FacesContextUtils
					.addPlainWarnMessage("Lüften reddetmek istediğiniz isteği seçiniz!");
			return;
		}

		if (HRSelectedEmployeeAdvanceRequest.getHrApproval() != EmployeeAdvanceRequest.PENDING) {

			FacesContextUtils
					.addPlainWarnMessage("Onayladığınız ya da reddettiğiniz bir isteği değiştiremezsiniz!");
			return;
		}

		HRSelectedEmployeeAdvanceRequest
				.setHrApproval(EmployeeAdvanceRequest.DECLINED);
		EmployeeAdvanceManager employeeDayoffManager = new EmployeeAdvanceManager();

		employeeDayoffManager.updateEntity(HRSelectedEmployeeAdvanceRequest);

		FacesContextUtils.addPlainWarnMessage("İsteği reddettiniz!");

		this.load();
	}

	public void approveEmployeeAdvanceRequest() {

		if (HRSelectedEmployeeAdvanceRequest == null) {

			FacesContextUtils
					.addPlainWarnMessage("Lüften onaylamak istediğiniz isteği seçiniz!");
			return;
		}

		if (HRSelectedEmployeeAdvanceRequest.getHrApproval() != EmployeeAdvanceRequest.PENDING) {

			FacesContextUtils
					.addPlainWarnMessage("Onayladığınız ya da reddettiğiniz bir isteği değiştiremezsiniz!");
			return;
		}

		HRSelectedEmployeeAdvanceRequest
				.setHrApproval(EmployeeAdvanceRequest.APPROVED);
		EmployeeAdvanceManager employeeDayoffManager = new EmployeeAdvanceManager();

		employeeDayoffManager.updateEntity(HRSelectedEmployeeAdvanceRequest);

		FacesContextUtils.addPlainWarnMessage("İsteği onayladınız!");

		this.load();
	}

	public void goToEmployeeAdvanceRequestPage() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().EMPLOYEE_ADVANCE_REQUEST_PAGE);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void exportToExcel(Integer talepId) {

		if (talepId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE AVANS TALEBİ SEÇİNİZ!",
					null));
			return;
		}

		try {

			List<String> date = new ArrayList<String>();
			// date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			// date.add(new SimpleDateFormat("dd.MM.yyyy")
			// .format(testTahribatsizOtomatikUtSonucs.get(0)
			// .getEklemeZamani()));
			//
			// raporNo = pipes.get(0).getPipeIndex();
			// muayeneyiYapan = testTahribatsizOtomatikUtSonucs.get(0)
			// .getEkleyenEmployee().getEmployee().getFirstname()
			// + " "
			// + testTahribatsizOtomatikUtSonucs.get(0)
			// .getEkleyenEmployee().getEmployee().getLastname();
			// hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("date", date);
			// dataMap.put("raporNo", raporNo);
			// dataMap.put("muayeneyiYapan", muayeneyiYapan);
			// dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();

			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/tahribatsiz/FR-473.xls",
					selectedEmployeeAdvanceRequest.getId().toString());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"AVANS ÇIKTISI BAŞARIYLA OLUŞTURULDU!", null));

		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÇIKTI OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	// ///////////////////////////////////////////////////
	// ///////// G E T T E R S - S E T T E R S ///////////
	// ///////////////////////////////////////////////////

	public ConfigBean getConfigBean() {
		return configBean;
	}

	public void setConfigBean(ConfigBean configBean) {
		this.configBean = configBean;
	}

	public List<EmployeeAdvanceRequest> getAllEmployeeAdvanceRequests() {
		return allEmployeeAdvanceRequests;
	}

	public void setAllEmployeeAdvanceRequests(
			List<EmployeeAdvanceRequest> allEmployeeAdvanceRequests) {
		this.allEmployeeAdvanceRequests = allEmployeeAdvanceRequests;
	}

	public Employee getCurrentEmployee() {
		return currentEmployee;
	}

	public void setCurrentEmployee(Employee currentEmployee) {
		this.currentEmployee = currentEmployee;
	}

	public EmployeeAdvanceRequest getSelectedEmployeeAdvanceRequest() {
		return selectedEmployeeAdvanceRequest;
	}

	public void setSelectedEmployeeAdvanceRequest(
			EmployeeAdvanceRequest selectedEmployeeAdvanceRequest) {
		this.selectedEmployeeAdvanceRequest = selectedEmployeeAdvanceRequest;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public List<EmployeeAdvanceRequest> getAllSubEmployeeAdvanceRequests() {
		return allSubEmployeeAdvanceRequests;
	}

	public void setAllSubEmployeeAdvanceRequests(
			List<EmployeeAdvanceRequest> allSubEmployeeAdvanceRequests) {
		this.allSubEmployeeAdvanceRequests = allSubEmployeeAdvanceRequests;
	}

	public EmployeeAdvanceRequest getSelectedSubEmployeeAdvanceRequest() {
		return selectedSubEmployeeAdvanceRequest;
	}

	public void setSelectedSubEmployeeAdvanceRequest(
			EmployeeAdvanceRequest selectedSubEmployeeAdvanceRequest) {
		this.selectedSubEmployeeAdvanceRequest = selectedSubEmployeeAdvanceRequest;
	}

	public EmployeeAdvanceRequest getHRSelectedEmployeeAdvanceRequest() {
		return HRSelectedEmployeeAdvanceRequest;
	}

	public void setHRSelectedEmployeeAdvanceRequest(
			EmployeeAdvanceRequest hRSelectedEmployeeAdvanceRequest) {
		HRSelectedEmployeeAdvanceRequest = hRSelectedEmployeeAdvanceRequest;
	}

	public List<EmployeeAdvanceRequest> getAllSelfEmployeeAdvanceRequests() {
		return allSelfEmployeeAdvanceRequests;
	}

	public void setAllSelfEmployeeAdvanceRequests(
			List<EmployeeAdvanceRequest> allSelfEmployeeAdvanceRequests) {
		this.allSelfEmployeeAdvanceRequests = allSelfEmployeeAdvanceRequests;
	}

	public EmployeeAdvanceRequest getNewEmployeeAdvanceRequest() {
		return newEmployeeAdvanceRequest;
	}

	public void setNewEmployeeAdvanceRequest(
			EmployeeAdvanceRequest newEmployeeAdvanceRequest) {
		this.newEmployeeAdvanceRequest = newEmployeeAdvanceRequest;
	}

}
