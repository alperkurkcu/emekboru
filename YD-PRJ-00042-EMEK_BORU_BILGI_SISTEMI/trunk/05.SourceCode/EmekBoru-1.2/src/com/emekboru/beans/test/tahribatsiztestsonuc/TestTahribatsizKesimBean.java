/**
 * 
 */
package com.emekboru.beans.test.tahribatsiztestsonuc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.converters.EmployeeConverter;
import com.emekboru.jpa.Employee;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizKesim;
import com.emekboru.jpaman.EmployeeManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizKesimManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testTahribatsizKesimBean")
@ViewScoped
public class TestTahribatsizKesimBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<TestTahribatsizKesim> allTestTahribatsizKesimList = new ArrayList<TestTahribatsizKesim>();
	private TestTahribatsizKesim testTahribatsizKesimForm = new TestTahribatsizKesim();

	private TestTahribatsizKesim newKesim = new TestTahribatsizKesim();
	private List<TestTahribatsizKesim> tamirList;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	int index = 0;

	private boolean updateButtonRender;

	public static List<Employee> employees;

	private Employee selectedEmployee;

	private Pipe selectedPipe = new Pipe();

	private String barkodNo = null;

	public TestTahribatsizKesimBean() {

		employees = EmployeeConverter.employeeDB;
	}

	public void addKesimSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		Boolean prmKontrol = testTahribatsizKesimForm.getDurum();
		TestTahribatsizKesimManager kesimManager = new TestTahribatsizKesimManager();

		if (prmKontrol == null) {

			try {
				testTahribatsizKesimForm.setDurum(false);
				testTahribatsizKesimForm.setMuayeneBaslamaZamani(UtilInsCore
						.getTarihZaman());
				testTahribatsizKesimForm.setMuayeneBaslamaKullanici(userBean
						.getUser().getId());
				kesimManager.updateEntity(testTahribatsizKesimForm);

				FacesContextUtils.addInfoMessage("SubmitMessage");
			} catch (Exception ex) {
				System.out.println("TestTahribatsizKesimBean:" + e.toString());
			}
		} else if (prmKontrol == false) {

			try {
				testTahribatsizKesimForm.setDurum(true);
				testTahribatsizKesimForm.setMuayeneBitisZamani(UtilInsCore
						.getTarihZaman());
				testTahribatsizKesimForm.setMuayeneBitisKullanici(userBean
						.getUser().getId());

				kesimManager.updateEntity(testTahribatsizKesimForm);

				FacesContextUtils.addInfoMessage("SubmitMessage");
			} catch (Exception ex) {
				System.out.println("TestTahribatsizTamirBean:" + e.toString());
			}
		} else {

			try {

				testTahribatsizKesimForm.setGuncellemeZamani(UtilInsCore
						.getTarihZaman());
				testTahribatsizKesimForm.setUser(userBean.getUser());
				kesimManager.updateEntity(testTahribatsizKesimForm);

				FacesContextUtils.addInfoMessage("UpdateMessage");
			} catch (Exception ex) {
				System.out.println("TestTahribatsizKesimBean:" + e.toString());
			}
		}
		fillTestList(prmPipeId);
		testTahribatsizKesimForm = new TestTahribatsizKesim();
	}

	public void addKesim(ActionEvent ae) {
		System.out.println("TestTahribatsizKesimBean.add()");

		UICommand cmd = (UICommand) ae.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId != null) {
			PipeManager pipeMan = new PipeManager();
			selectedPipe = pipeMan.loadObject(Pipe.class, prmPipeId);
		}

		try {
			TestTahribatsizKesimManager manager = new TestTahribatsizKesimManager();

			newKesim.setEklemeZamani(UtilInsCore.getTarihZaman());
			newKesim.setUser(userBean.getUser());
			newKesim.setPipe(selectedPipe);
			manager.enterNew(newKesim);
			newKesim = new TestTahribatsizKesim();

			FacesContextUtils.addInfoMessage("SubmitMessage");

		} catch (Exception e) {
			System.out.println("TestTahribatsizKesimBean:" + e.toString());
		}

		fillTestList(prmPipeId);
		testTahribatsizKesimForm = new TestTahribatsizKesim();
		newKesim = new TestTahribatsizKesim();
	}

	@SuppressWarnings("static-access")
	public void fillTestList(Integer prmPipeId) {

		TestTahribatsizKesimManager kesimManager = new TestTahribatsizKesimManager();
		allTestTahribatsizKesimList = kesimManager
				.getAllTestTahribatsizKesim(prmPipeId);
	}

	public void kesimListener(SelectEvent event) {
		updateButtonRender = true;
	}

	// autocompete için metodlar
	public List<String> complete(String query) {

		List<String> results = new ArrayList<String>();
		List<Employee> employeeList = new ArrayList<Employee>();

		EmployeeManager empMan = new EmployeeManager();
		employeeList = empMan.findAllLike(Employee.class, "firstname", query);
		for (int i = 0; i < employeeList.size(); i++) {

			results.add(employeeList.get(i).getEmployeeId().toString() + " - "
					+ employeeList.get(i).getFirstname() + " "
					+ employeeList.get(i).getLastname());
		}

		return results;
	}

	// autocompete için metodlar
	public List<Employee> completeEmployee(String query) {
		List<Employee> suggestions = new ArrayList<Employee>();

		for (Employee e : employees) {
			if (e.getFirstname().startsWith(query))
				suggestions.add(e);
		}

		return suggestions;
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_WARN, prmBarkod
							+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!", null));
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			TestTahribatsizKesimManager kesimManager = new TestTahribatsizKesimManager();
			allTestTahribatsizKesimList = kesimManager
					.getAllTestTahribatsizKesim(selectedPipe.getPipeId());
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
		}
	}

	public void deleteKesimSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		TestTahribatsizKesimManager kesimSonucManager = new TestTahribatsizKesimManager();

		if (testTahribatsizKesimForm.getId() == null) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_WARN,
					" LİSTEDEN SİLİNECEK KESİM SEÇİNİZ!", null));
			return;
		}

		kesimSonucManager.delete(testTahribatsizKesimForm);
		testTahribatsizKesimForm = new TestTahribatsizKesim();
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"KESİM BAŞARI iLE SiLiNMiŞTiR!"));

		fillTestList(prmPipeId);
	}

	// setters getters

	public List<TestTahribatsizKesim> getAllTestTahribatsizKesimList() {
		return allTestTahribatsizKesimList;
	}

	public void setAllTestTahribatsizKesimList(
			List<TestTahribatsizKesim> allTestTahribatsizKesimList) {
		this.allTestTahribatsizKesimList = allTestTahribatsizKesimList;
	}

	public TestTahribatsizKesim getTestTahribatsizKesimForm() {
		return testTahribatsizKesimForm;
	}

	public void setTestTahribatsizKesimForm(
			TestTahribatsizKesim testTahribatsizKesimForm) {
		this.testTahribatsizKesimForm = testTahribatsizKesimForm;
	}

	public TestTahribatsizKesim getNewKesim() {
		return newKesim;
	}

	public void setNewKesim(TestTahribatsizKesim newKesim) {
		this.newKesim = newKesim;
	}

	public List<TestTahribatsizKesim> getTamirList() {
		return tamirList;
	}

	public void setTamirList(List<TestTahribatsizKesim> tamirList) {
		this.tamirList = tamirList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public static List<Employee> getEmployees() {
		return employees;
	}

	public static void setEmployees(List<Employee> employees) {
		TestTahribatsizKesimBean.employees = employees;
	}

	public Employee getSelectedEmployee() {
		return selectedEmployee;
	}

	public void setSelectedEmployee(Employee selectedEmployee) {
		this.selectedEmployee = selectedEmployee;
	}

	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	public String getBarkodNo() {
		return barkodNo;
	}

	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
