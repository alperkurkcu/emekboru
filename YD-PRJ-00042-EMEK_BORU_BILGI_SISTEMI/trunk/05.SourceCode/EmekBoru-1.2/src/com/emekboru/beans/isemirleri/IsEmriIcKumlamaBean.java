/**
 * 
 */
package com.emekboru.beans.isemirleri;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.emekboru.beans.istakipformu.IsTakipFormuKumlamaBean;
import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.isemri.IsEmriIcKumlama;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.CoatRawMaterialManager;
import com.emekboru.jpaman.isemirleri.IsEmriIcKumlamaManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.sales.order.SalesItemManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isEmriIcKumlamaBean")
@ViewScoped
public class IsEmriIcKumlamaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private IsEmriIcKumlama isEmriIcKumlamaForm = new IsEmriIcKumlama();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	@ManagedProperty(value = "#{isTakipFormuKumlamaBean}")
	private IsTakipFormuKumlamaBean isTakipFormuKumlamaBean;

	private List<CoatRawMaterial> allIcKumlamaMalzemeList = new ArrayList<CoatRawMaterial>();

	// kaplama kalite
	private List<IsolationTestDefinition> internalIsolationTestDefinitions = new ArrayList<IsolationTestDefinition>();
	private String kumlamaKalitesi = null;

	public IsEmriIcKumlamaBean() {

	}

	public void addOrUpdateIsEmriIcKumlama(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmItemId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		SalesItem salesItem = new SalesItemManager().loadObject(
				SalesItem.class, prmItemId);

		IsEmriIcKumlamaManager isEmriManager = new IsEmriIcKumlamaManager();

		if (isEmriIcKumlamaForm.getId() == null) {

			isEmriIcKumlamaForm.setSalesItem(salesItem);
			isEmriIcKumlamaForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			isEmriIcKumlamaForm.setEkleyenEmployee(userBean.getUser());

			isEmriManager.enterNew(isEmriIcKumlamaForm);

			// iç kumlama iş takip formu ekleme
			isTakipFormuKumlamaBean.addFromIsEmri(1, salesItem);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"İÇ KUMLAMA İŞ EMRİ BAŞARIYLA EKLENMİŞTİR!"));
		} else {

			isEmriIcKumlamaForm.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			isEmriIcKumlamaForm.setGuncelleyenEmployee(userBean.getUser());
			isEmriManager.updateEntity(isEmriIcKumlamaForm);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_WARN,
					"İÇ KUMLAMA İŞ EMRİ BAŞARIYLA GÜNCELLENMİŞTİR!", null));
		}
	}

	public void deleteIsEmriIcKumlama() {

		IsEmriIcKumlamaManager isEmriManager = new IsEmriIcKumlamaManager();

		if (isEmriIcKumlamaForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		isEmriManager.delete(isEmriIcKumlamaForm);
		isEmriIcKumlamaForm = new IsEmriIcKumlama();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
				"İÇ KUMLAMA İŞ EMRİ BAŞARIYLA SİLİNMİŞTİR!", null));
	}

	@SuppressWarnings("static-access")
	public void loadIsEmri(Integer itemId) {

		IsolationTestDefinitionManager kaliteMan = new IsolationTestDefinitionManager();
		IsEmriIcKumlamaManager isEmriManager = new IsEmriIcKumlamaManager();

		if (isEmriManager.getAllIsEmriIcKumlama(itemId).size() > 0) {
			isEmriIcKumlamaForm = new IsEmriIcKumlamaManager()
					.getAllIsEmriIcKumlama(itemId).get(0);
		} else {
			isEmriIcKumlamaForm = new IsEmriIcKumlama();
		}

		internalIsolationTestDefinitions = kaliteMan
				.findIsolationByItemIdAndIsolationType(1,isEmriIcKumlamaForm
						.getSalesItem());
		for (int i = 0; i < internalIsolationTestDefinitions.size(); i++) {
			if (internalIsolationTestDefinitions.get(i).getGlobalId() == 2123) {
				kumlamaKalitesi = internalIsolationTestDefinitions.get(i)
						.getAciklama();
			}
			if (internalIsolationTestDefinitions.get(i).getGlobalId() == 2124) {
				kumlamaKalitesi = kumlamaKalitesi + " "
						+ internalIsolationTestDefinitions.get(i).getAciklama();
			}
		}
	}

	public void isEmriMalzemeEkle() {

		IsEmriIcKumlamaManager formManager = new IsEmriIcKumlamaManager();
		formManager.updateEntity(isEmriIcKumlamaForm);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
				"KUMLAMA MALZEMESİ BAŞARIYLA GİRİLMİŞTİR!", null));

	}

	public void loadKumlamaMalzemeleri() {

		CoatRawMaterialManager materialMan = new CoatRawMaterialManager();
		// 1001 kumlama turudur - coat_material_type tablosundan
		allIcKumlamaMalzemeList = materialMan
				.findUnfinishedCoatMaterialByType(1001);
	}

	// setters getters

	public IsEmriIcKumlama getIsEmriIcKumlamaForm() {
		return isEmriIcKumlamaForm;
	}

	public void setIsEmriIcKumlamaForm(IsEmriIcKumlama isEmriIcKumlamaForm) {
		this.isEmriIcKumlamaForm = isEmriIcKumlamaForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<CoatRawMaterial> getAllIcKumlamaMalzemeList() {
		return allIcKumlamaMalzemeList;
	}

	public void setAllIcKumlamaMalzemeList(
			List<CoatRawMaterial> allIcKumlamaMalzemeList) {
		this.allIcKumlamaMalzemeList = allIcKumlamaMalzemeList;
	}

	public IsTakipFormuKumlamaBean getIsTakipFormuKumlamaBean() {
		return isTakipFormuKumlamaBean;
	}

	public void setIsTakipFormuKumlamaBean(
			IsTakipFormuKumlamaBean isTakipFormuKumlamaBean) {
		this.isTakipFormuKumlamaBean = isTakipFormuKumlamaBean;
	}

	/**
	 * @return the kumlamaKalitesi
	 */
	public String getKumlamaKalitesi() {
		return kumlamaKalitesi;
	}

	/**
	 * @param kumlamaKalitesi
	 *            the kumlamaKalitesi to set
	 */
	public void setKumlamaKalitesi(String kumlamaKalitesi) {
		this.kumlamaKalitesi = kumlamaKalitesi;
	}

}
