package com.emekboru.beans.machine;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.MachineType;
import com.emekboru.jpaman.MachineTypeManager;

@ManagedBean(name = "machineTypeListBean")
@ViewScoped
public class MachineTypeListBean implements Serializable {

	private static final long serialVersionUID = 5568613889109874414L;
	private List<MachineType> machineTypes;

	public MachineTypeListBean() {

		MachineTypeManager manager = new MachineTypeManager();
		machineTypes = manager.findAll(MachineType.class);
	}

	// ********************************************************** //
	// *** GETTERS AND SETTERS *** //
	// ********************************************************** //
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<MachineType> getMachineTypes() {
		return machineTypes;
	}

	public void setMachineTypes(List<MachineType> machineTypes) {
		this.machineTypes = machineTypes;
	}

}
