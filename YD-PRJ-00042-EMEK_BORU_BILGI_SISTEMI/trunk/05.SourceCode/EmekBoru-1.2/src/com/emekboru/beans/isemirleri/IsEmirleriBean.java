/**
 * 
 */
package com.emekboru.beans.isemirleri;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isEmirleriBean")
@ViewScoped
public class IsEmirleriBean {

	@EJB
	private ConfigBean configBean;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	// Kaplama Is Emirleri acılış sayfası
	public void goToIsEmirleriKaplama() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().IS_EMIRLERI_KAPLAMA_PAGE);
	}

	// Kaplama Is Emirleri Durum Listesi sayfası
	public void goToIsEmirleriKaplamaList() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().IS_EMIRLERI_KAPLAMA_LIST_PAGE);
	}

	// Üretim Is Emirleri acılış sayfası
	public void goToIsEmirleriImalat() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().IS_EMIRLERI_IMALAT_PAGE);
	}

	// Üretim Is Emirleri Durum Listesi sayfası
	public void goToIsEmirleriImalatList() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().IS_EMIRLERI_IMALAT_LIST_PAGE);
	}

	// setters getters

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public ConfigBean getConfigBean() {
		return configBean;
	}

	public void setConfigBean(ConfigBean configBean) {
		this.configBean = configBean;
	}

}
