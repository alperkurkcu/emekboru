/**
 * 
 */
package com.emekboru.beans.satinalma;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.config.ConfigBean;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */

@ManagedBean(name = "satinAlmaSayfalarBean")
@ViewScoped
public class SatinAlmaSayfalarBean {

	@EJB
	private ConfigBean config;

	// Malzeme Talep Formu açılış sayfası
	public void goToMazlemeTalep() {

		FacesContextUtils
				.redirect(config.getPageUrl().SATINALMA_MALZEME_TALEP_FORM_PAGE);
	}

	// Malzeme Talep Formu açılış sayfası
	public void goToAmbarMazlemeTalep() {

		FacesContextUtils
				.redirect(config.getPageUrl().SATINALMA_AMBAR_MALZEME_TALEP_FORM_PAGE);
	}

	// Teklif İsteme açılış sayfası
	public void goToTeklifIsteme() {

		FacesContextUtils
				.redirect(config.getPageUrl().SATINALMA_TEKLIF_ISTEME_PAGE);
	}

	// Satın Alma Kararı açılış sayfası
	public void goToSatinAlmaKarari() {

		FacesContextUtils
				.redirect(config.getPageUrl().SATINALMA_SATIN_ALMA_KARARI_PAGE);
	}

	// Sipariş Mektubu açılış sayfası
	public void goToSiparisMektubu() {

		FacesContextUtils
				.redirect(config.getPageUrl().SATINALMA_SIPARIS_MEKTUBU_PAGE);
	}

	// Müsteri açılış sayfası
	public void goToMusteri() {

		FacesContextUtils.redirect(config.getPageUrl().SATINALMA_MUSTERI_PAGE);
	}

	// Sarf Merkezi açılış sayfası
	public void goToSarfMerkezi() {

		FacesContextUtils
				.redirect(config.getPageUrl().SATINALMA_SARF_MERKEZI_PAGE);
	}

	// Dosya Takip açılış sayfası
	// public void goToDosyaTakip() {
	//
	// FacesContextUtils
	// .redirect(config.getPageUrl().SATINALMA_DOSYA_TAKIP_PAGE);
	// }

	// Satınalma açılış sayfası
	public void goToSatinalma() {

		FacesContextUtils.redirect(config.getPageUrl().SATINALMA_ACILIS_PAGE);
	}

	// Sipariş Bildirimi açılış sayfası
	public void goToSiparisBildirimi() {

		FacesContextUtils
				.redirect(config.getPageUrl().SATINALMA_SIPARIS_BILDIRIMI_PAGE);
	}

	// Odeme Bildirimi açılış sayfası
	public void goToOdemeBildirimi() {

		FacesContextUtils
				.redirect(config.getPageUrl().SATINALMA_ODEME_BILDIRIMI_PAGE);
	}
}
