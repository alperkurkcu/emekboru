package com.emekboru.beans.crm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AjaxBehaviorEvent;
import javax.persistence.EntityManager;

import com.emekboru.beans.utils.CostTypes;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Currency;
import com.emekboru.jpa.Employee;
import com.emekboru.jpa.EmployeeEventParticipation;
import com.emekboru.jpa.EventCostBreakdown;
import com.emekboru.jpa.EventEb;
import com.emekboru.jpaman.EmployeeEventParticipationManager;
import com.emekboru.jpaman.EventCostBreakdownManager;
import com.emekboru.jpaman.EventManager;
import com.emekboru.jpaman.Factory;
import com.emekboru.utils.FacesContextUtils;


@ManagedBean
@ViewScoped
public class Events implements Serializable {

	private static final long serialVersionUID = 7481707628406191306L;

	@EJB
	private ConfigBean config;
	
	private List<EventEb> events;
	private EventEb selectedEvent;
	private EventEb newEvent;
	private EventCostBreakdown selectedCost;
	private Currency selectedCurrency;

	private boolean renderRate;

	public Events() {
		events = new ArrayList<EventEb>();
		selectedEvent = new EventEb();
		newEvent = new EventEb();
		selectedCost = new EventCostBreakdown();
		selectedCurrency = new Currency();
		selectedCurrency.setIsDefault(true);
		renderRate = false;
	}

	@PostConstruct
	public void load() {
		EventManager manager = new EventManager();
		events = manager.findAll(EventEb.class);
	}

	/**
	 * Add an event
	 */
	public void add() {

		EntityManager man = Factory.getInstance().createEntityManager();
		man.getTransaction().begin();
		man.persist(newEvent);
		man.getTransaction().commit();
		man.refresh(newEvent);
		events.add(newEvent);
		newEvent = new EventEb();
		man.close();
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	/**
	 * Update the selectedEvent
	 */
	public void update(){
		
		EventManager manager = new EventManager();
		manager.updateEntity(selectedEvent);
		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}
	/**
	 * Remove an event from the database
	 */
	public void delete() {
		System.out.println("Events.delete()");
		EventManager manager = new EventManager();
		manager.delete(selectedEvent);
		events.remove(selectedEvent);
		selectedEvent = new EventEb();
		selectedCost = new EventCostBreakdown();
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	public void addCost() {

		CostBreakdown costBean = (CostBreakdown) FacesContextUtils.getRequestBean("costBreakdown");
		CostTypes typesBean = (CostTypes) FacesContextUtils.getSessionBean("costTypes");
		EventCostBreakdown newCost = costBean.getCost();
		
		EntityManager manager = Factory.getInstance().createEntityManager();
		newCost.setEvent(selectedEvent);
		newCost.setCostType(typesBean.find(newCost.getCostType().getCostTypeId()));
		//If we deal with the default currency then the exchange rate is 1
		if(selectedCurrency.getIsDefault())
			newCost.setTlExchangeRate((double)1);
		
		selectedEvent = manager.find(EventEb.class, selectedEvent.getEventId());
		manager.getTransaction().begin();
		selectedEvent.getEventCostBreakdowns().add(newCost);
		manager.getTransaction().commit();
		manager.close();
		//add the cost to the TotalCost
		double totalCost	 = selectedEvent.getTotalCost() 
				+ (newCost.getAmountPaid()*newCost.getTlExchangeRate());
		double estimatedCost = selectedEvent.getEstimatedCost() 
				+ (newCost.getAmountPlanned()*newCost.getTlExchangeRate());
		selectedEvent.setTotalCost(totalCost);
		selectedEvent.setEstimatedCost(estimatedCost);
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void deleteCost() {

		EventCostBreakdownManager costManager = new EventCostBreakdownManager();
		costManager.delete(selectedCost);
		selectedEvent.getEventCostBreakdowns().remove(selectedCost);
		//update the total and estimated cost
		double totalCost = selectedEvent.getTotalCost() 
				- selectedCost.getAmountPaid()*selectedCost.getTlExchangeRate();
		double estimatedCost = selectedEvent.getEstimatedCost() 
				- selectedCost.getAmountPlanned()*selectedCost.getTlExchangeRate();

		selectedEvent.setEstimatedCost(estimatedCost);
		selectedEvent.setTotalCost(totalCost);
		selectedCost = new EventCostBreakdown();
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	public void updateCost() {

		if(selectedCurrency.getIsDefault())
			selectedCost.setTlExchangeRate((double)1);
		//load the original Cost record from the DB
		EntityManager costManager = Factory.getInstance().createEntityManager();
		EventCostBreakdown costCopy = costManager
				.find(EventCostBreakdown.class, selectedCost.getEventCostBreakdownId());
		costManager.close();
		
		EventCostBreakdownManager manager = new EventCostBreakdownManager();
		manager.updateEntity(selectedCost);
		//update total and estimated cost
		double totalCost = selectedEvent.getTotalCost() 
				+ selectedCost.getAmountPaid()*selectedCost.getTlExchangeRate() 
				- costCopy.getAmountPaid()*costCopy.getTlExchangeRate();
		double estimatedCost = selectedEvent.getEstimatedCost() 
				+ selectedCost.getAmountPlanned()*selectedCost.getTlExchangeRate() 
				- costCopy.getAmountPlanned()*costCopy.getTlExchangeRate();
		selectedEvent.setEstimatedCost(estimatedCost);
		selectedEvent.setTotalCost(totalCost);
		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}

	public void onCurrencySelect(AjaxBehaviorEvent event) {

		HtmlSelectOneMenu one = (HtmlSelectOneMenu) event.getSource();
		String currencyCode = one.getValue().toString();
		EntityManager manager = Factory.getInstance().createEntityManager();
		selectedCurrency = manager.find(Currency.class, currencyCode);
		manager.close();
	}

	public void addParticipants(){
		
		EntityManager manager = Factory.getInstance().createEntityManager();
		AvailableEmployees empBean = 
				(AvailableEmployees) FacesContextUtils.getViewBean("availableEmployees", AvailableEmployees.class);
		
		selectedEvent = manager.find(EventEb.class, selectedEvent.getEventId());
		manager.getTransaction().begin();
		for (Employee emp : empBean.getSelectedEmployees()) {
			EmployeeEventParticipation participant = new EmployeeEventParticipation();
			participant.setEmployee(emp);
			participant.setEvent(selectedEvent);
			participant.setParticipated(false);
			selectedEvent.getEmployeeEventParticipations().add(participant);
			empBean.getEmployees().remove(emp);
		}
		manager.getTransaction().commit();
		manager.close();
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}
	
	public void removeParticipant(EmployeeEventParticipation participant){
		System.out.println("Events.removeParticipant()");
		selectedEvent.getEmployeeEventParticipations().remove(participant);
		EmployeeEventParticipationManager manager = new EmployeeEventParticipationManager();
		manager.delete(participant);
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}
	
	public void goToPlanEventsPage() {
		FacesContextUtils.redirect(config.getPageUrl().PLAN_EVENTS_PAGE);
	}

	public void goToMyEventsPage() {
		FacesContextUtils.redirect(config.getPageUrl().MY_EVENTS_PAGE);
	}

	/**
	 * GETTERS AND SETTERS
	 */

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<EventEb> getEvents() {
		return events;
	}

	public void setEvents(List<EventEb> events) {
		this.events = events;
	}

	public EventEb getSelectedEvent() {
		return selectedEvent;
	}

	public void setSelectedEvent(EventEb selectedEvent) {
		this.selectedEvent = selectedEvent;
	}

	public EventEb getNewEvent() {
		return newEvent;
	}

	public void setNewEvent(EventEb newEvent) {
		this.newEvent = newEvent;
	}

	public EventCostBreakdown getSelectedCost() {
		return selectedCost;
	}

	public void setSelectedCost(EventCostBreakdown selectedCost) {
		this.selectedCost = selectedCost;
	}

	public boolean isRenderRate() {
		return renderRate;
	}

	public void setRenderRate(boolean renderRate) {
		this.renderRate = renderRate;
	}

	public Currency getSelectedCurrency() {
		return selectedCurrency;
	}

	public void setSelectedCurrency(Currency selectedCurrency) {
		this.selectedCurrency = selectedCurrency;
	}
}
