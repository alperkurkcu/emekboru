package com.emekboru.beans.edw;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AjaxBehaviorEvent;

import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.ElectrodeDustWire;
import com.emekboru.jpaman.ElectrodeDustWireManager;
import com.emekboru.messages.ElectrodeDustWireType;
import com.emekboru.utils.DateTrans;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "wireListBean")
@ViewScoped
public class WireListBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	private List<ElectrodeDustWire> recentWires;
	private List<ElectrodeDustWire> wires;
	private List<Integer> wireYearList;
	private int currentYear;

	public WireListBean() {

		wires = new ArrayList<ElectrodeDustWire>();
		recentWires = new ArrayList<ElectrodeDustWire>();
		wireYearList = new ArrayList<Integer>();
		currentYear = DateTrans.getYearOfDate(new Date());
	}

	@PostConstruct
	public void init() {

		loadWireYears();
		loadRecentWires();
		loadYearWires(currentYear);
	}

	public void loadRecentWires() {

		ElectrodeDustWireManager man = new ElectrodeDustWireManager();
		recentWires = man.findRecent(100, ElectrodeDustWireType.WIRE);
	}

	public void loadYearWires(int year) {

		DateTrans.calendar.set(year, Calendar.JANUARY, 1, 0, 1);
		Date startDate = DateTrans.calendar.getTime();
		DateTrans.calendar.set(year, Calendar.DECEMBER, 31, 23, 59);
		Date endDate = DateTrans.calendar.getTime();
		ElectrodeDustWireManager dustManager = new ElectrodeDustWireManager();
		wires = dustManager.findTypeBetweenDates(ElectrodeDustWireType.WIRE,
				startDate, endDate);
	}

	public void loadSelectedYearWires(AjaxBehaviorEvent event) {

		HtmlSelectOneMenu one = (HtmlSelectOneMenu) event.getSource();
		currentYear = Integer.valueOf(one.getValue().toString());
		loadYearWires(currentYear);
	}

	/******************************************************************************
	 ******************* HELPER METHODS SECTION ******************** /
	 ******************************************************************************/
	protected void loadWireYears() {

		wireYearList = new ArrayList<Integer>();
		ElectrodeDustWireManager dustManager = new ElectrodeDustWireManager();
		List<Date> minList = dustManager
				.findMinYearForType(ElectrodeDustWireType.WIRE);
		Integer min, max;
		if (minList.size() == 1 && minList.get(0) == null) {

			min = 0;
			max = 0;
			return;
		}
		min = DateTrans.getYearOfDate(minList.get(0));
		max = DateTrans.getYearOfDate(new Date());
		for (int i = min; i <= max; i++)
			wireYearList.add(i);
	}

	public void goToWirePage() {

		FacesContextUtils.redirect(config.getPageUrl().WIRE_PAGE);
	}

	/******************************************************************************
	 ******************* GETTERS AND SETTERS ******************** /
	 ******************************************************************************/
	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	public List<ElectrodeDustWire> getRecentWires() {
		return recentWires;
	}

	public void setRecentWires(List<ElectrodeDustWire> recentWires) {
		this.recentWires = recentWires;
	}

	public List<ElectrodeDustWire> getWires() {
		return wires;
	}

	public void setWires(List<ElectrodeDustWire> wires) {
		this.wires = wires;
	}

	public List<Integer> getWireYearList() {
		return wireYearList;
	}

	public void setWireYearList(List<Integer> wireYearList) {
		this.wireYearList = wireYearList;
	}

	public int getCurrentYear() {
		return currentYear;
	}

	public void setCurrentYear(int currentYear) {
		this.currentYear = currentYear;
	}
}
