/**
 * 
 */
package com.emekboru.beans.personel;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

import org.primefaces.event.RowEditEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Department;
import com.emekboru.jpa.Employee;
import com.emekboru.jpa.personel.PersonelCalismaBilgileri;
import com.emekboru.jpa.personel.PersonelEgitimBilgileri;
import com.emekboru.jpa.personel.PersonelKimlikAdres;
import com.emekboru.jpa.personel.PersonelKimlikEmail;
import com.emekboru.jpa.personel.PersonelKimlikTelefon;
import com.emekboru.jpa.personel.PersonelOrtakGorev;
import com.emekboru.jpa.personel.PersonelOrtakKanGrubu;
import com.emekboru.jpa.personel.PersonelOrtakOgrenimDurumu;
import com.emekboru.jpa.personel.PersonelOrtakOzurOrani;
import com.emekboru.jpa.personel.PersonelOrtakUcretTuru;
import com.emekboru.jpa.personel.PersonelPerformansBilgileri;
import com.emekboru.jpa.personel.PersonelSaglikBilgileri;
import com.emekboru.jpa.personel.PersonelSirketDisiDeneyimBilgileri;
import com.emekboru.jpa.personel.PersonelUcretBilgileri;
import com.emekboru.jpaman.DepartmentManager;
import com.emekboru.jpaman.EmployeeManager;
import com.emekboru.jpaman.Factory;
import com.emekboru.jpaman.personel.PersonelEgitimBilgileriManager;
import com.emekboru.jpaman.personel.PersonelKimlikAdresManager;
import com.emekboru.jpaman.personel.PersonelKimlikEmailManager;
import com.emekboru.jpaman.personel.PersonelKimlikTelefonManager;
import com.emekboru.jpaman.personel.PersonelOrtakGorevManager;
import com.emekboru.jpaman.personel.PersonelOrtakKanGrubuManager;
import com.emekboru.jpaman.personel.PersonelOrtakOgrenimDurumuManager;
import com.emekboru.jpaman.personel.PersonelOrtakOzurOraniManager;
import com.emekboru.jpaman.personel.PersonelOrtakUcretTuruManager;
import com.emekboru.jpaman.personel.PersonelSaglikBilgileriManager;
import com.emekboru.jpaman.personel.PersonelUcretBilgileriManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Rıdvan
 * 
 */
@ManagedBean(name = "personelKimlikBean")
@ViewScoped
public class PersonelKimlikBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Employee selectedEmployee = new Employee();
	private List<Employee> allEmployeeList = new ArrayList<Employee>();

	private PersonelKimlikAdres kimlikAdres = new PersonelKimlikAdres();
	private PersonelKimlikEmail kimlikEmail = new PersonelKimlikEmail();
	private PersonelKimlikTelefon kimlikTelefon = new PersonelKimlikTelefon();
	private PersonelSaglikBilgileri kimlikSaglik = new PersonelSaglikBilgileri();
	private PersonelSirketDisiDeneyimBilgileri kimlikDeneyim = new PersonelSirketDisiDeneyimBilgileri();
	private PersonelUcretBilgileri kimlikUcret = new PersonelUcretBilgileri();
	private PersonelPerformansBilgileri kimlikPerformans = new PersonelPerformansBilgileri();
	private PersonelCalismaBilgileri kimlikCalisma = new PersonelCalismaBilgileri();
	private PersonelEgitimBilgileri kimlikEgitim = new PersonelEgitimBilgileri();
	private List<PersonelOrtakKanGrubu> kanGrubuList = new ArrayList<PersonelOrtakKanGrubu>();
	private List<PersonelOrtakOzurOrani> ozurList = new ArrayList<PersonelOrtakOzurOrani>();
	private List<PersonelOrtakOgrenimDurumu> ogrenimList = new ArrayList<PersonelOrtakOgrenimDurumu>();
	private List<PersonelOrtakUcretTuru> ucretList = new ArrayList<PersonelOrtakUcretTuru>();
	private List<Department> departList = new ArrayList<Department>();
	private List<PersonelOrtakGorev> gorevList = new ArrayList<PersonelOrtakGorev>();
	private ConfigBean config;
	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private EmployeeManager employeeManager = new EmployeeManager();

	public PersonelKimlikBean() {
		setSelectedEmployee(new Employee());
		fillTestList();
	}

	public void fillTestList() {
		PersonelOrtakOzurOraniManager orman = new PersonelOrtakOzurOraniManager();
		PersonelOrtakKanGrubuManager kanman = new PersonelOrtakKanGrubuManager();
		PersonelOrtakOgrenimDurumuManager ogrenMan = new PersonelOrtakOgrenimDurumuManager();
		PersonelOrtakUcretTuruManager ucretOrtMan = new PersonelOrtakUcretTuruManager();
		PersonelOrtakGorevManager gorevMan = new PersonelOrtakGorevManager();
		DepartmentManager depMan = new DepartmentManager();
		setAllEmployeeList(getEmployeeManager().findAllOrderByASC(
				Employee.class, "firstname"));

		kanGrubuList = kanman.findAllOrderBy(PersonelOrtakKanGrubu.class, "id");
		ozurList = orman.findAllOrderBy(PersonelOrtakOzurOrani.class, "id");
		ogrenimList = ogrenMan.findAllOrderBy(PersonelOrtakOgrenimDurumu.class,
				"id");
		ucretList = ucretOrtMan.findAllOrderBy(PersonelOrtakUcretTuru.class,
				"id");
		departList = depMan.findAllOrderBy(Department.class, "name");
		gorevList = gorevMan.findAllOrderBy(PersonelOrtakGorev.class,
				"gorevAdi");

		if (getSelectedEmployee().getEmployeeId() != null)
			setSelectedEmployee(getEmployeeManager().loadObject(Employee.class,
					getSelectedEmployee().getEmployeeId()));

	}

	public void add() {

		int ekleyenKimlikId = userBean.getUser().getId();
		Timestamp eklenmeTimestamp = new java.sql.Timestamp(
				new java.util.Date().getTime());

		PersonelKimlikAdresManager adresMan = new PersonelKimlikAdresManager();
		PersonelKimlikEmailManager mailMan = new PersonelKimlikEmailManager();
		PersonelKimlikTelefonManager telMan = new PersonelKimlikTelefonManager();
		PersonelSaglikBilgileriManager sagMan = new PersonelSaglikBilgileriManager();
		PersonelEgitimBilgileriManager egtimMan = new PersonelEgitimBilgileriManager();
		PersonelUcretBilgileriManager ucretMan = new PersonelUcretBilgileriManager();

		// formObjecj.setEklenmeTarihi(eklenmeTimestamp);
		// formObjecj.setKullaniciIdEkleyen(ekleyenKimlikId);
		// formObjecj.setVarsayilanAdres(kimlikAdres.getAdres());
		// formObjecj.setVarsayilanEmail(kimlikEmail.getEmail());
		// formObjecj.setVarsayilanTel(kimlikTelefon.getNumara());
		EntityManager manager = Factory.getInstance().createEntityManager();
		manager.getTransaction().begin();
		manager.persist(getSelectedEmployee());
		manager.getTransaction().commit();
		manager.close();

		int kimlikId = getSelectedEmployee().getEmployeeId();

		kimlikAdres.setEmployee(selectedEmployee);
		kimlikAdres.setEklenmeTarihi(eklenmeTimestamp);
		kimlikAdres.setKullaniciIdEkleyen(ekleyenKimlikId);
		kimlikAdres.setVarsayilanMi(true);
		kimlikEmail.setEmployee(selectedEmployee);
		kimlikEmail.setEklenmeTarihi(eklenmeTimestamp);
		kimlikEmail.setKullaniciIdEkleyen(ekleyenKimlikId);
		kimlikEmail.setVarsayilanMi(true);
		kimlikTelefon.setEmployee(selectedEmployee);
		kimlikTelefon.setEklenmeTarihi(eklenmeTimestamp);
		kimlikTelefon.setKullaniciIdEkleyen(ekleyenKimlikId);
		kimlikTelefon.setVarsayilanMi(true);
		kimlikSaglik.setKimlikId(kimlikId);
		kimlikEgitim.setKimlikId(kimlikId);
		kimlikUcret.setKimlikId(kimlikId);
		egtimMan.enterNew(kimlikEgitim);
		sagMan.enterNew(kimlikSaglik);
		adresMan.enterNew(kimlikAdres);
		mailMan.enterNew(kimlikEmail);
		telMan.enterNew(kimlikTelefon);
		ucretMan.enterNew(kimlikUcret);
		FacesMessage msg = new FacesMessage("Kimlik Eklendi", "");
		FacesContext.getCurrentInstance().addMessage(null, msg);
		this.setSelectedEmployee(new Employee());
		fillTestList();
	}

	public void addOrUpdate(RowEditEvent event) {

		if (((Employee) event.getObject()).getEmployeeId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		} else {

			getEmployeeManager().updateEntity(((Employee) event.getObject()));
			FacesMessage msg = new FacesMessage("Kimlik Edited", "Success");

			FacesContext.getCurrentInstance().addMessage(null, msg);

		}
		fillTestList();
	}

	public void delete() {

		if (getSelectedEmployee().getEmployeeId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		// perMen.deleteByField(Employee.class, "employeeId",
		// formObjecj.getEmployeeId());
		getSelectedEmployee().setActive(false);
		getEmployeeManager().updateEntity(getSelectedEmployee());
		FacesContextUtils.addWarnMessage("DeletedMessage");

		fillTestList();
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the kimlikAdres
	 */
	public PersonelKimlikAdres getKimlikAdres() {
		return kimlikAdres;
	}

	/**
	 * @param kimlikAdres
	 *            the kimlikAdres to set
	 */
	public void setKimlikAdres(PersonelKimlikAdres kimlikAdres) {
		this.kimlikAdres = kimlikAdres;
	}

	/**
	 * @return the kimlikEmail
	 */
	public PersonelKimlikEmail getKimlikEmail() {
		return kimlikEmail;
	}

	/**
	 * @param kimlikEmail
	 *            the kimlikEmail to set
	 */
	public void setKimlikEmail(PersonelKimlikEmail kimlikEmail) {
		this.kimlikEmail = kimlikEmail;
	}

	/**
	 * @return the kimlikTelefon
	 */
	public PersonelKimlikTelefon getKimlikTelefon() {
		return kimlikTelefon;
	}

	/**
	 * @param kimlikTelefon
	 *            the kimlikTelefon to set
	 */
	public void setKimlikTelefon(PersonelKimlikTelefon kimlikTelefon) {
		this.kimlikTelefon = kimlikTelefon;
	}

	/**
	 * @return the kimlikSaglik
	 */
	public PersonelSaglikBilgileri getKimlikSaglik() {
		return kimlikSaglik;
	}

	/**
	 * @param kimlikSaglik
	 *            the kimlikSaglik to set
	 */
	public void setKimlikSaglik(PersonelSaglikBilgileri kimlikSaglik) {
		this.kimlikSaglik = kimlikSaglik;
	}

	/**
	 * @return the kimlikDeneyim
	 */
	public PersonelSirketDisiDeneyimBilgileri getKimlikDeneyim() {
		return kimlikDeneyim;
	}

	/**
	 * @param kimlikDeneyim
	 *            the kimlikDeneyim to set
	 */
	public void setKimlikDeneyim(
			PersonelSirketDisiDeneyimBilgileri kimlikDeneyim) {
		this.kimlikDeneyim = kimlikDeneyim;
	}

	/**
	 * @return the kimlikUcret
	 */
	public PersonelUcretBilgileri getKimlikUcret() {
		return kimlikUcret;
	}

	/**
	 * @param kimlikUcret
	 *            the kimlikUcret to set
	 */
	public void setKimlikUcret(PersonelUcretBilgileri kimlikUcret) {
		this.kimlikUcret = kimlikUcret;
	}

	/**
	 * @return the kimlikPerformans
	 */
	public PersonelPerformansBilgileri getKimlikPerformans() {
		return kimlikPerformans;
	}

	/**
	 * @param kimlikPerformans
	 *            the kimlikPerformans to set
	 */
	public void setKimlikPerformans(PersonelPerformansBilgileri kimlikPerformans) {
		this.kimlikPerformans = kimlikPerformans;
	}

	/**
	 * @return the kimlikCalisma
	 */
	public PersonelCalismaBilgileri getKimlikCalisma() {
		return kimlikCalisma;
	}

	/**
	 * @param kimlikCalisma
	 *            the kimlikCalisma to set
	 */
	public void setKimlikCalisma(PersonelCalismaBilgileri kimlikCalisma) {
		this.kimlikCalisma = kimlikCalisma;
	}

	/**
	 * @return the kimlikEgitim
	 */
	public PersonelEgitimBilgileri getKimlikEgitim() {
		return kimlikEgitim;
	}

	/**
	 * @param kimlikEgitim
	 *            the kimlikEgitim to set
	 */
	public void setKimlikEgitim(PersonelEgitimBilgileri kimlikEgitim) {
		this.kimlikEgitim = kimlikEgitim;
	}

	/**
	 * @return the kanGrubuList
	 */
	public List<PersonelOrtakKanGrubu> getKanGrubuList() {
		return kanGrubuList;
	}

	/**
	 * @param kanGrubuList
	 *            the kanGrubuList to set
	 */
	public void setKanGrubuList(List<PersonelOrtakKanGrubu> kanGrubuList) {
		this.kanGrubuList = kanGrubuList;
	}

	/**
	 * @return the ozurList
	 */
	public List<PersonelOrtakOzurOrani> getOzurList() {
		return ozurList;
	}

	/**
	 * @param ozurList
	 *            the ozurList to set
	 */
	public void setOzurList(List<PersonelOrtakOzurOrani> ozurList) {
		this.ozurList = ozurList;
	}

	/**
	 * @return the ogrenimList
	 */
	public List<PersonelOrtakOgrenimDurumu> getOgrenimList() {
		return ogrenimList;
	}

	/**
	 * @param ogrenimList
	 *            the ogrenimList to set
	 */
	public void setOgrenimList(List<PersonelOrtakOgrenimDurumu> ogrenimList) {
		this.ogrenimList = ogrenimList;
	}

	/**
	 * @return the ucretList
	 */
	public List<PersonelOrtakUcretTuru> getUcretList() {
		return ucretList;
	}

	/**
	 * @param ucretList
	 *            the ucretList to set
	 */
	public void setUcretList(List<PersonelOrtakUcretTuru> ucretList) {
		this.ucretList = ucretList;
	}

	/**
	 * @return the departList
	 */
	public List<Department> getDepartList() {
		return departList;
	}

	/**
	 * @param departList
	 *            the departList to set
	 */
	public void setDepartList(List<Department> departList) {
		this.departList = departList;
	}

	/**
	 * @return the gorevList
	 */
	public List<PersonelOrtakGorev> getGorevList() {
		return gorevList;
	}

	/**
	 * @param gorevList
	 *            the gorevList to set
	 */
	public void setGorevList(List<PersonelOrtakGorev> gorevList) {
		this.gorevList = gorevList;
	}

	/**
	 * @return the config
	 */
	public ConfigBean getConfig() {
		return config;
	}

	/**
	 * @param config
	 *            the config to set
	 */
	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	/**
	 * @return the employeeManager
	 */
	public EmployeeManager getEmployeeManager() {
		return employeeManager;
	}

	/**
	 * @param employeeManager
	 *            the employeeManager to set
	 */
	public void setEmployeeManager(EmployeeManager employeeManager) {
		this.employeeManager = employeeManager;
	}

	/**
	 * @return the selectedEmployee
	 */
	public Employee getSelectedEmployee() {
		return selectedEmployee;
	}

	/**
	 * @param selectedEmployee
	 *            the selectedEmployee to set
	 */
	public void setSelectedEmployee(Employee selectedEmployee) {
		this.selectedEmployee = selectedEmployee;
	}

	/**
	 * @return the allEmployeeList
	 */
	public List<Employee> getAllEmployeeList() {
		return allEmployeeList;
	}

	/**
	 * @param allEmployeeList
	 *            the allEmployeeList to set
	 */
	public void setAllEmployeeList(List<Employee> allEmployeeList) {
		this.allEmployeeList = allEmployeeList;
	}

	// get set
	
	

}
