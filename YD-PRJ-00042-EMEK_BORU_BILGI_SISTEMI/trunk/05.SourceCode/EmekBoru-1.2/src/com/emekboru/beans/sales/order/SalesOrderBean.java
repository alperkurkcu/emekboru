package com.emekboru.beans.sales.order;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.sales.SalesMenuBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.sales.order.SalesOrder;
import com.emekboru.jpa.sales.order.SalesProposal;
import com.emekboru.jpaman.sales.order.SalesOrderManager;
import com.emekboru.jpaman.sales.order.SalesProposalManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "salesOrderBean")
@ViewScoped
public class SalesOrderBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5739956156127737590L;

	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{salesMenuBean}")
	private SalesMenuBean salesMenuBean;

	private SalesOrder newOrder = new SalesOrder();
	private SalesOrder selectedOrder;

	private List<SalesOrder> orderList;
	private List<SalesOrder> filteredList;

	private int queueNumber;
	private boolean isThisFirstTime;

	private StreamedContent downloadedFile;
	private String downloadedFileName;

	private File[] fileArray;

	private File theFile = null;
	OutputStream output = null;
	InputStream input = null;

	private String path;
	private String privatePath;
	private String toBeDeleted;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	public SalesOrderBean() {
		queueNumber = 1;
		isThisFirstTime = true;

		newOrder = new SalesOrder();
		selectedOrder = new SalesOrder();

		privatePath = File.separatorChar + "sales" + File.separatorChar
				+ "uploadedDocuments" + File.separatorChar + "proposal"
				+ File.separatorChar;
	}

	@PostConstruct
	public void load() {
		System.out.println("SalesOrderBean.load()");

		try {
			this.setPath(config.getConfig().getFolder().getAbsolutePath()
					+ privatePath);

			System.out.println("Path for Upload File (Sales):			" + path);
		} catch (Exception ex) {
			System.out.println("Path for Upload File (Sales):			"
					+ FacesContext.getCurrentInstance().getExternalContext()
							.getRealPath(privatePath));

			System.out.print(path);
			System.out.println("CAUSE OF " + ex.toString());
		}

		theFile = new File(path);
		if (!theFile.isDirectory()) {
			theFile.mkdirs();
		}

		fileArray = theFile.listFiles();

		try {
			SalesOrderManager manager = new SalesOrderManager();
			// orderList = manager.findAll(SalesOrder.class);
			orderList = manager.findAllOrderBy(SalesOrder.class,
					"orderId");

			if (isThisFirstTime) {
				newOrder = new SalesOrder();
				this.completeOrderNumber();
				isThisFirstTime = false;
			}

			selectedOrder = orderList.get(0);
		} catch (Exception e) {
			System.out.println("SalesOrderBean:" + e.toString());
		}
	}

	@SuppressWarnings("unused")
	public void add(SalesProposal proposal) {
		System.out.println("SalesOrderBean.add()");

		boolean isAdded = false;

		try {
			SalesOrderManager manager = new SalesOrderManager();

			newOrder.setProposal(proposal);
			orderList.add(newOrder);
			manager.enterNew(newOrder);
			this.completeOrderNumber();
			queueNumber++;

			newOrder = new SalesOrder();
			FacesContextUtils.addInfoMessage("SubmitMessage");

			isAdded = true;
		} catch (Exception e) {
			System.out.println("SalesOrderBean:" + e.toString());
		}
	}

	public void update() {
		System.out.println("SalesOrderBean.update()");

		try {
			SalesOrderManager manager = new SalesOrderManager();
			manager.updateEntity(selectedOrder);

			SalesProposalManager proposalManager = new SalesProposalManager();
			proposalManager.updateEntity(selectedOrder.getProposal());

			FacesContextUtils.addInfoMessage("UpdateItemMessage");
		} catch (Exception e) {
			System.out.println("SalesOrderBean:" + e.toString());
		}
	}

	public void delete() {
		System.out.println("SalesOrderBean.delete()");

		boolean isDeleted = true;
		try {
			if (selectedOrder.getProposal().getFileNumber() > 0) {
				isDeleted = false;

				// Delete the uploaded file
				theFile = new File(path);
				fileArray = theFile.listFiles();

				for (int i = 0; i < selectedOrder.getProposal().getFileNames()
						.size(); i++) {
					System.out
							.println("document that will be deleted: "
									+ privatePath
									+ selectedOrder.getProposal()
											.getFileNames().get(i));
					for (int j = 0; j < fileArray.length; j++) {
						System.out.println("fileArray[j].toString(): "
								+ fileArray[j].toString());
						if (fileArray[j].toString().contains(
								privatePath
										+ selectedOrder.getProposal()
												.getFileNames().get(i))) {
							isDeleted = fileArray[j].delete();
						}
					}
				}
			}
			if (isDeleted) {
				SalesOrderManager manager = new SalesOrderManager();
				manager.delete(selectedOrder);

				SalesProposalManager proposalManager = new SalesProposalManager();
				proposalManager.delete(selectedOrder.getProposal());

				orderList.remove(selectedOrder);
				selectedOrder = orderList.get(0);
				FacesContextUtils.addWarnMessage("DeletedMessage");
			}
		} catch (Exception e) {
			System.out.println("SalesOrderBean:" + e.toString());
		}
	}

	public boolean isProposalTransformedToOrder(int theProposalId) {

		for (int i = 0; i < orderList.size(); i++) {
			if (orderList.get(i).getProposal().getProposalId() == theProposalId) {
				return true;
			}
		}

		return false;
	}

	public void completeOrderNumber() {
		System.out.println("SalesOrderBean.completeOrderNumber()");

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		this.findNextNumber(sdf.format(cal.getTime()).substring(6));

		newOrder.setOrderNo(sdf.format(cal.getTime()).substring(6) + "/"
				+ String.valueOf(queueNumber));
	}

	public void findNextNumber(String thisYear) {
		boolean isThisYearExist = false;

		try {
			for (int i = 0; i < orderList.size(); i++) {
				if (orderList.get(i).getOrderNo().substring(0, 4)
						.contentEquals(thisYear)) {

					isThisYearExist = true;

					if (queueNumber <= Integer.parseInt(orderList.get(i)
							.getOrderNo().substring(5))) {

						queueNumber = Integer.parseInt(orderList.get(i)
								.getOrderNo().substring(5)) + 1;
					}
				}
			}
			if (!isThisYearExist) {
				queueNumber = 1;
			}
		} catch (Exception e) {
			System.out.println("SalesOrderBean:" + e.toString());
		}
	}

	public String checkFileName(String fileName) {

		fileName = fileName.replace(" ", "_");
		fileName = fileName.replace("ç", "c");
		fileName = fileName.replace("Ç", "C");
		fileName = fileName.replace("ğ", "g");
		fileName = fileName.replace("Ğ", "G");
		fileName = fileName.replace("İ", "i");
		fileName = fileName.replace("ı", "i");
		fileName = fileName.replace("ö", "o");
		fileName = fileName.replace("Ö", "O");
		fileName = fileName.replace("ş", "s");
		fileName = fileName.replace("Ş", "S");
		fileName = fileName.replace("ü", "u");
		fileName = fileName.replace("Ü", "U");

		return fileName;
	}

	public void upload(FileUploadEvent event) throws AbortProcessingException,
			IOException {
		System.out.println("SalesOrderBean.upload()");

		try {
			String name = this.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				newOrder.getProposal().setDocuments("//" + name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				SalesProposalManager manager = new SalesProposalManager();
				manager.updateEntity(selectedOrder.getProposal());

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				FacesContextUtils.addWarnMessage("salesFileUploadedMessage");
			} else {
				System.out.println("File already exists.");

				newOrder.getProposal().setDocuments("//");
				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("SalesOrderBean: " + e.toString());
		}
	}

	public void addUploadedFile(FileUploadEvent event)
			throws AbortProcessingException, IOException {
		System.out.println("SalesOrderBean.addUploadedFile()");

		try {
			String name = this.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				selectedOrder.getProposal().setDocuments(
						selectedOrder.getProposal().getDocuments() + "//"
								+ name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				SalesProposalManager manager = new SalesProposalManager();
				manager.updateEntity(selectedOrder.getProposal());

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				FacesContextUtils.addWarnMessage("salesFileUploadedMessage");
			} else {
				System.out.println("File already exists.");

				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("SalesOrderBean: " + e.toString());
		}
	}

	public void deleteTheSelectedFile() {
		System.out.println("SalesCRMVisitBean.deleteTheSelectedFile()");
		try {
			// Delete the uploaded file
			theFile = new File(path);
			fileArray = theFile.listFiles();

			System.out.println("document that will be deleted: " + privatePath
					+ toBeDeleted);
			for (int j = 0; j < fileArray.length; j++) {
				System.out.println("fileArray[j].toString(): "
						+ fileArray[j].toString());
				if (fileArray[j].toString().contains(privatePath + toBeDeleted)) {
					fileArray[j].delete();

					if (selectedOrder.getProposal().getDocuments()
							.contentEquals("//" + toBeDeleted)) {
						selectedOrder.getProposal().setDocuments(
								selectedOrder.getProposal().getDocuments()
										.replace("//" + toBeDeleted, "null"));
					} else {
						selectedOrder.getProposal().setDocuments(
								selectedOrder.getProposal().getDocuments()
										.replace("//" + toBeDeleted, ""));
					}
					FacesContextUtils
							.addWarnMessage("salesTheFileIsDeletedMessage");
				}
			}
		} catch (Exception e) {
			System.out.println("SalesOrderBean: " + e.toString());
		}
	}

	public void deleteAndUpload(FileUploadEvent event)
			throws AbortProcessingException, IOException {

		System.out.println("SalesOrderBean.deleteAndUpload()");

		try {
			// Delete the uploaded file
			theFile = new File(path);
			fileArray = theFile.listFiles();

			for (int i = 0; i < selectedOrder.getProposal().getFileNames()
					.size(); i++) {
				for (int j = 0; j < fileArray.length; j++) {
					System.out.println("fileArray[j].toString(): "
							+ fileArray[j].toString());
					if (fileArray[j].toString().contains(
							privatePath
									+ selectedOrder.getProposal()
											.getFileNames().get(i))) {
						fileArray[j].delete();
					}
				}
			}

			String name = this.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				selectedOrder.getProposal().setDocuments("//" + name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				SalesProposalManager manager = new SalesProposalManager();
				manager.updateEntity(selectedOrder.getProposal());

				FacesContextUtils.addInfoMessage("UpdateItemMessage");
			} else {
				System.out.println("File already exists.");

				selectedOrder.getProposal().setDocuments(null);
				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("SalesOrderBean: " + e.toString());
		}
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public SalesOrder getNewOrder() {
		return newOrder;
	}

	public void setNewOrder(SalesOrder newOrder) {
		this.newOrder = newOrder;
	}

	public SalesOrder getSelectedOrder() {
		try {
			if (selectedOrder == null)
				selectedOrder = new SalesOrder();
		} catch (Exception ex) {
			selectedOrder = new SalesOrder();
		}
		return selectedOrder;
	}

	public void setSelectedOrder(SalesOrder selectedOrder) {
		this.selectedOrder = selectedOrder;
	}

	public List<SalesOrder> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<SalesOrder> orderList) {
		this.orderList = orderList;
	}

	public List<SalesOrder> getFilteredList() {
		try {
			selectedOrder = filteredList.get(0);
			salesMenuBean.showNewType1Menu();
		} catch (Exception ex) {
			System.out.println("getFilteredList: " + ex);
		}
		return filteredList;
	}

	public void setFilteredList(List<SalesOrder> filteredList) {
		this.filteredList = filteredList;
	}

	public SalesMenuBean getSalesMenuBean() {
		return salesMenuBean;
	}

	public void setSalesMenuBean(SalesMenuBean salesMenuBean) {
		this.salesMenuBean = salesMenuBean;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getDownloadedFileName() {
		return downloadedFileName;
	}

	public void setDownloadedFileName(String downloadedFileName) {
		try {
			this.downloadedFileName = downloadedFileName;
		} catch (Exception e) {
			System.out.println("SalesOrderBean.setDownloadedFileName():"
					+ e.toString());
		}
	}

	@SuppressWarnings("resource")
	public StreamedContent getDownloadedFile() {
		System.out.println("SalesOrderBean.getDownloadedFile()");

		try {
			if (downloadedFileName != null) {
				RandomAccessFile accessFile = new RandomAccessFile(path
						+ downloadedFileName, "r");
				byte[] downloadByte = new byte[(int) accessFile.length()];
				accessFile.read(downloadByte);
				InputStream stream = new ByteArrayInputStream(downloadByte);

				String suffix = FilenameUtils.getExtension(downloadedFileName);
				String contentType = "text/plain";
				if (suffix.contentEquals("jpg") || suffix.contentEquals("png")
						|| suffix.contentEquals("gif")) {
					contentType = "image/" + contentType;
				} else if (suffix.contentEquals("doc")
						|| suffix.contentEquals("docx")) {
					contentType = "application/msword";
				} else if (suffix.contentEquals("xls")
						|| suffix.contentEquals("xlsx")) {
					contentType = "application/vnd.ms-excel";
				} else if (suffix.contentEquals("pdf")) {
					contentType = "application/pdf";
				}
				downloadedFile = new DefaultStreamedContent(stream,
						"contentType", downloadedFileName);

				stream.close();
			}
		} catch (Exception e) {
			System.out.println("SalesOrderBean.getDownloadedFile():"
					+ e.toString());
		}
		return downloadedFile;
	}

	public String getToBeDeleted() {
		return toBeDeleted;
	}

	public void setToBeDeleted(String toBeDeleted) {
		this.toBeDeleted = toBeDeleted;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}
}
