package com.emekboru.beans.order;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Customer;
import com.emekboru.jpa.Order;
import com.emekboru.jpa.PlannedIsolation;
import com.emekboru.jpaman.Factory;
import com.emekboru.jpaman.OrderManager;
import com.emekboru.utils.DateTrans;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.IsolationType;

@ManagedBean(name = "newOrderBean")
@SessionScoped
public class NewOrderBean implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String NONE = "none";
	@EJB
	private ConfigBean config;
	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;
	private Date todaysDate;
	private Customer selectedCustomer;
	// data related to the newOrder
	private Order newOrder;
	private PlannedIsolation internalIsolation;
	private PlannedIsolation externalIsolation;
	private boolean includeIntIso;
	private boolean includeExtIso;
	private Order selectedOrder;
	private List<Order> orderList;

	public NewOrderBean() {

		todaysDate = new Date();
		selectedCustomer = new Customer();
		newOrder = new Order();
		selectedOrder = new Order();
		orderList = new ArrayList<Order>();
		internalIsolation = new PlannedIsolation();
		internalIsolation.setInternalExternal(IsolationType.IC_KAPLAMA
				.getType());
		externalIsolation = new PlannedIsolation();
		externalIsolation.setInternalExternal(IsolationType.DIS_KAPLAMA
				.getType());
	}

	// load the orders of the selected customer
	public void loadOrders() {

		System.out.println("NewOrderBean.loadOrders()");
		EntityManager manager = Factory.getInstance().createEntityManager();
		selectedCustomer = manager.find(Customer.class,
				selectedCustomer.getCustomerId());
		// manager.getTransaction().begin();
		manager.refresh(selectedCustomer);
		selectedCustomer.getOrders();
		// manager.getTransaction().commit();
		manager.close();
	}

	public void deleteOrder() {

		if (selectedOrder == null || selectedOrder.getOrderId() == 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		EntityManager man = Factory.getInstance().createEntityManager();
		selectedOrder = man.find(Order.class, selectedOrder.getOrderId());
		man.getTransaction().begin();

		man.remove(selectedOrder);
		man.getTransaction().commit();
		man.close();
		selectedCustomer.getOrders().remove(selectedOrder);
		selectedOrder = new Order();
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	// add an order item for a selected order
	public void addOrder() {
		EntityManager manager = Factory.getInstance().createEntityManager();
		selectedCustomer = manager.find(Customer.class,
				selectedCustomer.getCustomerId());
		manager.refresh(selectedCustomer);

		System.out.println("NewOrderBean.addOrderItem()");
		List<PlannedIsolation> isolations = new ArrayList<PlannedIsolation>(0);
		if (includeIntIso) {
			internalIsolation.setOrder(newOrder);
			internalIsolation.setInternalExternal(IsolationType.DIS_KAPLAMA
					.getType());
			isolations.add(internalIsolation);
		}

		if (includeExtIso) {
			externalIsolation.setOrder(newOrder);
			externalIsolation.setInternalExternal(IsolationType.IC_KAPLAMA
					.getType());
			isolations.add(externalIsolation);
		}

		// set isolations and the person who submitted it and the RESPONSIBLE
		// the status is set to DEFAULT
		newOrder.setCustomer(selectedCustomer);
		newOrder.setPlannedIsolation(isolations);
		newOrder.setUsername(userBean.getUser().getUsername());
		newOrder.setStatusId(config.getConfig().getOrder().findDefaultStatus()
				.getId());
		setOrderNumber();

		OrderManager orderItemManager = new OrderManager();
		orderItemManager.enterNew(newOrder);
		selectedCustomer.getOrders().add(newOrder);
		// loadOrders();
		newOrder = new Order();
		includeExtIso = false;
		includeIntIso = false;
		internalIsolation = new PlannedIsolation();
		externalIsolation = new PlannedIsolation();
		FacesContextUtils.addInfoMessage("SubmitMessage");
		manager.close();
	}

	protected void setOrderNumber() {

		Date currentDate = new Date();
		int currentYear = DateTrans.getYearOfDate(currentDate);
		System.out.println("current year: " + currentYear);
		OrderManager man = new OrderManager();
		int number = man.getLastOrderNumber(currentYear);
		newOrder.setOrderYear(currentYear);
		newOrder.setOrderNumber(number + 1);
		System.out.println("order number : " + newOrder.getOrderNumber());
	}

	protected int getYearFormat(Date date) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		Integer orderYear = calendar.get(Calendar.YEAR);
		String orderYearSt = orderYear.toString();
		orderYearSt = orderYearSt.substring(2);
		orderYear = Integer.valueOf(orderYearSt);
		return orderYear;
	}

	/*
	 * GETTERS AND SETTERS
	 */
	public Order getNewOrder() {
		return newOrder;
	}

	public void setNewOrder(Order newOrder) {
		this.newOrder = newOrder;
	}

	public Order getSelectedOrder() {
		return selectedOrder;
	}

	public void setSelectedOrder(Order selectedOrder) {
		this.selectedOrder = selectedOrder;
	}

	public Date getTodaysDate() {
		return todaysDate;
	}

	public void setTodaysDate(Date todaysDate) {
		this.todaysDate = todaysDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public PlannedIsolation getInternalIsolation() {
		return internalIsolation;
	}

	public void setInternalIsolation(PlannedIsolation internalIsolation) {
		this.internalIsolation = internalIsolation;
	}

	public PlannedIsolation getExternalIsolation() {
		return externalIsolation;
	}

	public void setExternalIsolation(PlannedIsolation externalIsolation) {
		this.externalIsolation = externalIsolation;
	}

	public Customer getSelectedCustomer() {
		return selectedCustomer;
	}

	public void setSelectedCustomer(Customer selectedCustomer) {
		this.selectedCustomer = selectedCustomer;
	}

	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public List<Order> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<Order> orderList) {
		this.orderList = orderList;
	}

	public boolean isIncludeIntIso() {
		return includeIntIso;
	}

	public void setIncludeIntIso(boolean includeIntIso) {
		this.includeIntIso = includeIntIso;
	}

	public boolean isIncludeExtIso() {
		return includeExtIso;
	}

	public void setIncludeExtIso(boolean includeExtIso) {
		this.includeExtIso = includeExtIso;
	}
}
