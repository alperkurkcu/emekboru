package com.emekboru.beans.test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.sales.order.SalesOrderBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.DestructiveTestsSpec;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.TestKimyasalAnalizSonuc;
import com.emekboru.jpaman.DestructiveTestsSpecManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.TestKimyasalAnalizSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKimyasalAnalizSonucBean")
@ViewScoped
public class TestKimyasalAnalizSonucBean {

	private TestKimyasalAnalizSonuc testKimyasalAnalizSonucForm = null;
	private List<TestKimyasalAnalizSonuc> allKimyasalAnalizSonucList = null;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	Integer prmGlobalId = null;
	Integer prmTestId = null;
	Integer prmPipeId = null;

	private Pipe pipe = null;
	private DestructiveTestsSpec bagliTest = null;
	private String sampleNo = null;

	public void sampleNoOlustur() {
		Integer count = 0;
		TestKimyasalAnalizSonucManager manager = new TestKimyasalAnalizSonucManager();
		count = manager.getAllKimyasalAnalizSonucTest(prmGlobalId, prmPipeId)
				.size() + 1;
		sampleNo = bagliTest.getCode() + pipe.getPipeIndex() + "-" + count;
	}

	public void addOrUpdateKimyasalAnalizTestSonuc(ActionEvent e) {

		try {

			UICommand cmd = (UICommand) e.getComponent();
			prmGlobalId = (Integer) cmd.getChildren().get(0).getAttributes()
					.get("value");
			prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
					.get("value");
			prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
					.get("value");

			TestKimyasalAnalizSonucManager kimyasalManager = new TestKimyasalAnalizSonucManager();

			hesapla();

			if (testKimyasalAnalizSonucForm.getId() == null) {

				testKimyasalAnalizSonucForm
						.setEklemeZamani(new java.sql.Timestamp(
								new java.util.Date().getTime()));
				testKimyasalAnalizSonucForm.setEkleyenKullanici(userBean
						.getUser().getId());

				pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
				testKimyasalAnalizSonucForm.setPipe(pipe);

				bagliTest = new DestructiveTestsSpecManager().loadObject(
						DestructiveTestsSpec.class, prmTestId);
				testKimyasalAnalizSonucForm.setBagliTestId(bagliTest);
				testKimyasalAnalizSonucForm.setBagliGlobalId(bagliTest);

				sampleNoOlustur();
				testKimyasalAnalizSonucForm.setSampleNo(sampleNo);

				kimyasalManager.enterNew(testKimyasalAnalizSonucForm);

				DestructiveTestsSpecManager desMan = new DestructiveTestsSpecManager();
				desMan.testSonucKontrol(prmTestId, true);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"KİMYASAL ANALİZ SONUCU BAŞARI İLE KAYDEDİLMİŞTİR!"));
			} else {

				testKimyasalAnalizSonucForm
						.setGuncellemeZamani(new java.sql.Timestamp(
								new java.util.Date().getTime()));
				testKimyasalAnalizSonucForm.setGuncelleyenKullanici(userBean
						.getUser().getId());

				kimyasalManager.updateEntity(testKimyasalAnalizSonucForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"KİMYASAL ANALİZ SONUCU BAŞARI İLE GÜNCELLENMİŞTİR!"));

			}
			fillTestList(prmGlobalId, prmPipeId);

		} catch (Exception e2) {

			System.out
					.println(" testKimyasalAnalizSonucBean.addOrUpdateKimyasalAnalizTestSonuc-HATA");
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"KİMYASAL SONUÇ VERİSİ EKLENEMEDİ, TEKRAR DENEYİNİZ!"));
			return;
		}
	}

	public void hesapla() {
		try {

			double ce1 = testKimyasalAnalizSonucForm.getC().doubleValue()
					+ (testKimyasalAnalizSonucForm.getSi().doubleValue() / 30)
					+ (testKimyasalAnalizSonucForm.getMn().doubleValue() / 20)
					+ (testKimyasalAnalizSonucForm.getCu().doubleValue() / 20)
					+ (testKimyasalAnalizSonucForm.getNi().doubleValue() / 60)
					+ (testKimyasalAnalizSonucForm.getCr().doubleValue() / 20)
					+ (testKimyasalAnalizSonucForm.getV().doubleValue() / 10)
					+ (testKimyasalAnalizSonucForm.getB().doubleValue() * 5);

			double ce2 = testKimyasalAnalizSonucForm.getC().doubleValue()
					+ (testKimyasalAnalizSonucForm.getMn().doubleValue() / 6)
					+ ((testKimyasalAnalizSonucForm.getCr().doubleValue()
							+ testKimyasalAnalizSonucForm.getMo().doubleValue() + testKimyasalAnalizSonucForm
							.getV().doubleValue()) / 5)
					+ ((testKimyasalAnalizSonucForm.getCu().doubleValue() + testKimyasalAnalizSonucForm
							.getNi().doubleValue()) / 15);

			testKimyasalAnalizSonucForm.setCe1(BigDecimal.valueOf(ce1));
			testKimyasalAnalizSonucForm.setCe2(BigDecimal.valueOf(ce2));
		} catch (Exception e2) {

			System.out.println(" testKimyasalAnalizSonucBean.hesapla-HATA");
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							"HESAPLAMA HATASI, KİMYASAL SONUÇ VERİSİ EKLENEMEDİ, TEKRAR DENEYİNİZ!"));
			return;
		}

	}

	public TestKimyasalAnalizSonucBean() {

		testKimyasalAnalizSonucForm = new TestKimyasalAnalizSonuc();
		allKimyasalAnalizSonucList = new ArrayList<TestKimyasalAnalizSonuc>();
		privatePath = File.separatorChar + "testDocs" + File.separatorChar
				+ "uploadedDocuments" + File.separatorChar
				+ "kimyasalAnalizSonuc" + File.separatorChar;
	}

	public void addKimyasalTest() {
		testKimyasalAnalizSonucForm = new TestKimyasalAnalizSonuc();
	}

	public void kimyasalAnalizListener(SelectEvent event) {

		updateButtonRender = true;
		// FacesContextUtils.refreshPage();
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		TestKimyasalAnalizSonucManager manager = new TestKimyasalAnalizSonucManager();
		allKimyasalAnalizSonucList = manager.getAllKimyasalAnalizSonucTest(
				globalId, pipeId);
	}

	public void deleteTestKimyasalAnalizSonuc() {

		TestKimyasalAnalizSonucManager kimyasalManager = new TestKimyasalAnalizSonucManager();

		if (testKimyasalAnalizSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		kimyasalManager.delete(testKimyasalAnalizSonucForm);

		testKimyasalAnalizSonucForm = new TestKimyasalAnalizSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");

	}

	// document upload

	@EJB
	private ConfigBean config;

	private String path;
	private String privatePath;

	private File theFile = null;
	OutputStream output = null;
	InputStream input = null;

	@SuppressWarnings("unused")
	private File[] fileArray;

	private StreamedContent downloadedFile;
	private String downloadedFileName;

	SalesOrderBean sob = new SalesOrderBean();

	@PostConstruct
	public void load() {
		System.out.println("TestKimyasalAnalizSonucBean.load()");

		try {
			// this.setPath(config.getConfig().getFolder().getAbsolutePath()
			// + privatePath);

			if (System.getProperty("os.name").startsWith("Windows")) {

				this.setPath("C:/emekfiles" + privatePath);
			} else {

				this.setPath("/home/emekboru/Desktop/emekfiles" + privatePath);
			}

			System.out.println("Path for Upload File (TestKimyasalSonuc):			"
					+ path);
		} catch (Exception ex) {
			System.out.println("Path for Upload File (TestKimyasalSonuc):			"
					+ FacesContext.getCurrentInstance().getExternalContext()
							.getRealPath(privatePath));

			System.out.print(path);
			System.out.println("CAUSE OF " + ex.toString());
		}

		theFile = new File(path);
		if (!theFile.isDirectory()) {
			theFile.mkdirs();
		}

		fileArray = theFile.listFiles();

	}

	public void addUploadedFile(FileUploadEvent event)
			throws AbortProcessingException, IOException {
		System.out.println("TestKimyasalAnalizSonucBean.addUploadedFile()");

		try {
			String name = sob.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				testKimyasalAnalizSonucForm
						.setDocuments(testKimyasalAnalizSonucForm
								.getDocuments() + "//" + name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				TestKimyasalAnalizSonucManager manager = new TestKimyasalAnalizSonucManager();
				manager.updateEntity(testKimyasalAnalizSonucForm);

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				FacesContextUtils.addWarnMessage("salesFileUploadedMessage");
			} else {
				System.out.println("File already exists.");

				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("TestKimyasalAnalizSonucBean: " + e.toString());
		}
	}

	@SuppressWarnings("resource")
	public StreamedContent getDownloadedFile() {
		System.out.println("TestKimyasalAnalizSonucBean.getDownloadedFile()");

		try {
			if (downloadedFileName != null) {
				RandomAccessFile accessFile = new RandomAccessFile(path
						+ downloadedFileName, "r");
				byte[] downloadByte = new byte[(int) accessFile.length()];
				accessFile.read(downloadByte);
				InputStream stream = new ByteArrayInputStream(downloadByte);

				String suffix = FilenameUtils.getExtension(downloadedFileName);
				String contentType = "text/plain";
				if (suffix.contentEquals("jpg") || suffix.contentEquals("png")
						|| suffix.contentEquals("gif")) {
					contentType = "image/" + contentType;
				} else if (suffix.contentEquals("doc")
						|| suffix.contentEquals("docx")) {
					contentType = "application/msword";
				} else if (suffix.contentEquals("xls")
						|| suffix.contentEquals("xlsx")) {
					contentType = "application/vnd.ms-excel";
				} else if (suffix.contentEquals("pdf")) {
					contentType = "application/pdf";
				}
				downloadedFile = new DefaultStreamedContent(stream,
						"contentType", downloadedFileName);

				stream.close();
			}
		} catch (Exception e) {
			System.out
					.println("TestKimyasalAnalizSonucBean.getDownloadedFile():"
							+ e.toString());
		}
		return downloadedFile;
	}

	public String getDownloadedFileName() {
		return downloadedFileName;
	}

	public void setDownloadedFileName(String downloadedFileName) {
		try {
			this.downloadedFileName = downloadedFileName;
		} catch (Exception e) {
			System.out
					.println("TestKimyasalAnalizSonucBean.setDownloadedFileName():"
							+ e.toString());
		}
	}

	public void setPath(String path) {
		this.path = path;
	}

	// document upload

	// setters getters
	public TestKimyasalAnalizSonuc getTestKimyasalAnalizSonucForm() {
		return testKimyasalAnalizSonucForm;
	}

	public void setTestKimyasalAnalizSonucForm(
			TestKimyasalAnalizSonuc testKimyasalAnalizSonucForm) {
		this.testKimyasalAnalizSonucForm = testKimyasalAnalizSonucForm;
	}

	public List<TestKimyasalAnalizSonuc> getAllKimyasalAnalizSonucList() {
		return allKimyasalAnalizSonucList;
	}

	public void setAllKimyasalAnalizSonucList(
			List<TestKimyasalAnalizSonuc> allKimyasalAnalizSonucList) {
		this.allKimyasalAnalizSonucList = allKimyasalAnalizSonucList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
