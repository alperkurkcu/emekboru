/**
 * 
 */
package com.emekboru.beans.kalibrasyon;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.kalibrasyon.KalibrasyonUltrasonikLaminasyon;
import com.emekboru.jpaman.kalibrasyon.KalibrasyonUltrasonikLaminasyonManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "kalibrasyonUltrasonikLaminasyonBean")
@ViewScoped
public class KalibrasyonUltrasonikLaminasyonBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<KalibrasyonUltrasonikLaminasyon> allKalibrasyonList = new ArrayList<KalibrasyonUltrasonikLaminasyon>();
	private KalibrasyonUltrasonikLaminasyon kalibrasyonForm = new KalibrasyonUltrasonikLaminasyon();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender = false;

	private Integer prmOnlineOffline = null;

	public KalibrasyonUltrasonikLaminasyonBean() {
	}

	public void kalibrasyonListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void addKalibrasyon(Integer prmOnlineOffline) {
		this.prmOnlineOffline = prmOnlineOffline;
		updateButtonRender = false;
	}

	public void addOrUpdateKalibrasyon(ActionEvent e) {

		KalibrasyonUltrasonikLaminasyonManager manager = new KalibrasyonUltrasonikLaminasyonManager();
		if (kalibrasyonForm.getId() == null) {
			kalibrasyonForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			kalibrasyonForm.setEkleyenKullanici(userBean.getUser().getId());
			kalibrasyonForm.setOnlineOffline(prmOnlineOffline);
			manager.enterNew(kalibrasyonForm);
			FacesContextUtils.addInfoMessage("SubmitMessage");
		} else {
			if (updateButtonRender) {
				kalibrasyonForm.setGuncellemeZamani(new java.sql.Timestamp(
						new java.util.Date().getTime()));
				kalibrasyonForm.setGuncelleyenKullanici(userBean.getUser()
						.getId());
				manager.updateEntity(kalibrasyonForm);
				FacesContextUtils.addInfoMessage("UpdateItemMessage");
			} else {
				clonner();
			}
		}
		fillTestList(kalibrasyonForm.getOnlineOffline());
	}

	@SuppressWarnings("static-access")
	public void fillTestList(Integer prmOnlineOffline) {
		KalibrasyonUltrasonikLaminasyonManager manager = new KalibrasyonUltrasonikLaminasyonManager();
		allKalibrasyonList = manager.getAllKalibrasyon(prmOnlineOffline);
		kalibrasyonForm = new KalibrasyonUltrasonikLaminasyon();
	}

	public void deleteKalibrasyon() {

		KalibrasyonUltrasonikLaminasyonManager manager = new KalibrasyonUltrasonikLaminasyonManager();
		if (kalibrasyonForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		prmOnlineOffline = kalibrasyonForm.getOnlineOffline();
		manager.delete(kalibrasyonForm);
		kalibrasyonForm = new KalibrasyonUltrasonikLaminasyon();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		fillTestList(prmOnlineOffline);
	}

	public void clonner() {

		KalibrasyonUltrasonikLaminasyon clone = new KalibrasyonUltrasonikLaminasyon();
		KalibrasyonUltrasonikLaminasyonManager manager = new KalibrasyonUltrasonikLaminasyonManager();
		prmOnlineOffline = kalibrasyonForm.getOnlineOffline();
		try {
			clone = (KalibrasyonUltrasonikLaminasyon) this.kalibrasyonForm
					.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			FacesContextUtils.addPlainErrorMessage(e.getMessage());
		}

		clone.setId(null);
		clone.setEklemeZamani(new java.sql.Timestamp(new java.util.Date()
				.getTime()));
		clone.setEkleyenKullanici(userBean.getUser().getId());

		manager.enterNew(clone);
		FacesContextUtils.addInfoMessage("SubmitMessage");
		fillTestList(prmOnlineOffline);
	}

	// getters setters
	/**
	 * @return the allKalibrasyonList
	 */
	public List<KalibrasyonUltrasonikLaminasyon> getAllKalibrasyonList() {
		return allKalibrasyonList;
	}

	/**
	 * @param allKalibrasyonList
	 *            the allKalibrasyonList to set
	 */
	public void setAllKalibrasyonList(
			List<KalibrasyonUltrasonikLaminasyon> allKalibrasyonList) {
		this.allKalibrasyonList = allKalibrasyonList;
	}

	/**
	 * @return the kalibrasyonForm
	 */
	public KalibrasyonUltrasonikLaminasyon getKalibrasyonForm() {
		return kalibrasyonForm;
	}

	/**
	 * @param kalibrasyonForm
	 *            the kalibrasyonForm to set
	 */
	public void setKalibrasyonForm(
			KalibrasyonUltrasonikLaminasyon kalibrasyonForm) {
		this.kalibrasyonForm = kalibrasyonForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
