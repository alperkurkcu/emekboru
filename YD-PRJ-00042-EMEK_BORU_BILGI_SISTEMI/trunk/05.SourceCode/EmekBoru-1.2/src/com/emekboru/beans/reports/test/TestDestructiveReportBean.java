/**
 * 
 */
package com.emekboru.beans.reports.test;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.AgirlikTestsSpec;
import com.emekboru.jpa.BukmeTestsSpec;
import com.emekboru.jpa.CekmeTestsSpec;
import com.emekboru.jpa.CentikDarbeTestsSpec;
import com.emekboru.jpa.DestructiveTestsSpec;
import com.emekboru.jpa.EdwPipeLink;
import com.emekboru.jpa.KaynakMakroTestSpec;
import com.emekboru.jpa.NondestructiveTestsSpec;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.TestAgirlikDusurmeSonuc;
import com.emekboru.jpa.TestBukmeSonuc;
import com.emekboru.jpa.TestCekmeSonuc;
import com.emekboru.jpa.TestCentikDarbeSonuc;
import com.emekboru.jpa.TestKaynakSonuc;
import com.emekboru.jpa.TestKimyasalAnalizSonuc;
import com.emekboru.jpaman.AgirlikTestsSpecManager;
import com.emekboru.jpaman.BukmeTestsSpecManager;
import com.emekboru.jpaman.CekmeTestSpecManager;
import com.emekboru.jpaman.CentikDarbeTestsSpecManager;
import com.emekboru.jpaman.DestructiveTestsSpecManager;
import com.emekboru.jpaman.EdwPipeLinkManager;
import com.emekboru.jpaman.KaynakMacroTestSpecManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.TestAgirlikDusurmeSonucManager;
import com.emekboru.jpaman.TestBukmeSonucManager;
import com.emekboru.jpaman.TestCekmeSonucManager;
import com.emekboru.jpaman.TestCentikDarbeSonucManager;
import com.emekboru.jpaman.TestKaynakSonucManager;
import com.emekboru.jpaman.TestKimyasalAnalizSonucManager;
import com.emekboru.utils.ReportUtil;

@ManagedBean(name = "testDestructiveReportBean")
@SessionScoped
public class TestDestructiveReportBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	Integer raporNo = 0;
	String muayeneyiYapan = null;
	String hazirlayan = null;

	@PostConstruct
	public void init() {
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void fr234ResultReportXlsx(Integer pipeId, Integer testId) {
		FacesContext context = FacesContext.getCurrentInstance();
		if (pipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", pipeId);

			// SalesItemManager salesItemManager = new SalesItemManager();
			// List<SalesItem> salesItems = new ArrayList<SalesItem>();
			// salesItems = salesItemManager.findByField(SalesItem.class,
			// "itemId", pipes.get(0).getSalesItem().getItemId());

			AgirlikTestsSpecManager atsManager = new AgirlikTestsSpecManager();
			AgirlikTestsSpec spec = new AgirlikTestsSpec();
			List<DestructiveTestsSpec> destructiveTestsSpecs = new ArrayList<DestructiveTestsSpec>();
			DestructiveTestsSpecManager dtsManager = new DestructiveTestsSpecManager();
			destructiveTestsSpecs.add(dtsManager.loadObject(
					DestructiveTestsSpec.class, testId));

			// test spec getirme
			try {
				spec = atsManager.seciliAgirlikTestTanim(
						destructiveTestsSpecs.get(0).getGlobalId(),
						pipes.get(0).getSalesItem().getItemId()).get(0);
			} catch (Exception e) {
				e.printStackTrace();

				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"KALİTE PLANI BULMA HATASI!", null));
			}

			// TestAgirlikDusurmeSonucManager testAgirlikDusurmeSonucManager =
			// new TestAgirlikDusurmeSonucManager();
			List<TestAgirlikDusurmeSonuc> testAgirlikDusurmeSonucs = new ArrayList<TestAgirlikDusurmeSonuc>();
			// testAgirlikDusurmeSonucs = testAgirlikDusurmeSonucManager
			// .findByField(TestAgirlikDusurmeSonuc.class, "pipe.pipeId",
			// pipeId);
			testAgirlikDusurmeSonucs = TestAgirlikDusurmeSonucManager
					.getBySalesItemGlobalId(pipes.get(0).getSalesItem(), 1029);

			if (testAgirlikDusurmeSonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			// date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			date.add(new SimpleDateFormat("dd.MM.yyyy")
					.format(testAgirlikDusurmeSonucs.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = testAgirlikDusurmeSonucs.get(0)
						.getEkleyenEmployee().getEmployee().getFirstname()
						+ " "
						+ testAgirlikDusurmeSonucs.get(0).getEkleyenEmployee()
								.getEmployee().getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", pipes.get(0).getSalesItem());
			dataMap.put("siparisAgirlikDusurmeOzellik", destructiveTestsSpecs);
			dataMap.put("agirlikDusurmeSonuc", testAgirlikDusurmeSonucs);
			dataMap.put("spec", spec);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/tahribatli/FR-234.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void fr123ResultReportXlsx(Integer pipeId, Integer testId) {
		FacesContext context = FacesContext.getCurrentInstance();
		if (pipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", pipeId);

			// SalesItemManager salesItemManager = new SalesItemManager();
			// List<SalesItem> salesItems = new ArrayList<SalesItem>();
			// salesItems = salesItemManager.findByField(SalesItem.class,
			// "itemId", pipes.get(0).getSalesItem().getItemId());

			List<NondestructiveTestsSpec> nondestructiveTestsSpecs = new ArrayList<NondestructiveTestsSpec>();
			for (NondestructiveTestsSpec nondestructiveTestsSpec : pipes.get(0)
					.getSalesItem().getNondestructiveTestsSpecs()) {
				if (nondestructiveTestsSpec.getGlobalId() == 1012)
					nondestructiveTestsSpecs.add(nondestructiveTestsSpec);
			}

			// TestKimyasalAnalizSonucManager testKimyasalAnalizSonucManager =
			// new TestKimyasalAnalizSonucManager();
			List<TestKimyasalAnalizSonuc> testKimyasalAnalizSonucs = new ArrayList<TestKimyasalAnalizSonuc>();
			// testKimyasalAnalizSonucs = testKimyasalAnalizSonucManager
			// .findByField(TestKimyasalAnalizSonuc.class, "pipe.pipeId",
			// pipeId);
			testKimyasalAnalizSonucs = TestKimyasalAnalizSonucManager
					.getBySalesItem(pipes.get(0).getSalesItem());

			if (testKimyasalAnalizSonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			// date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			date.add(new SimpleDateFormat("dd.MM.yyyy")
					.format(testKimyasalAnalizSonucs.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = testKimyasalAnalizSonucs.get(0)
						.getEkleyenEmployee().getEmployee().getFirstname()
						+ " "
						+ testKimyasalAnalizSonucs.get(0).getEkleyenEmployee()
								.getEmployee().getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", pipes.get(0).getSalesItem());
			dataMap.put("siparisKimyasalAnalizOzellik",
					nondestructiveTestsSpecs);
			dataMap.put("kimyasalAnalizSonuc", testKimyasalAnalizSonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/tahribatli/FR-123.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void cekmeInspectionResultReportXlsx(Integer pipeId, Integer testId) {
		FacesContext context = FacesContext.getCurrentInstance();
		if (pipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", pipeId);

			// SalesItemManager salesItemManager = new SalesItemManager();
			// List<SalesItem> salesItems = new ArrayList<SalesItem>();
			// salesItems = salesItemManager.findByField(SalesItem.class,
			// "itemId", pipes.get(0).getSalesItem().getItemId());

			List<DestructiveTestsSpec> destructiveTestsSpecs = new ArrayList<DestructiveTestsSpec>();
			DestructiveTestsSpecManager dtsManager = new DestructiveTestsSpecManager();
			destructiveTestsSpecs.add(dtsManager.loadObject(
					DestructiveTestsSpec.class, testId));

			// test spec getirme
			CekmeTestsSpec spec = new CekmeTestsSpec();
			try {
				CekmeTestSpecManager ctsManager = new CekmeTestSpecManager();
				spec = ctsManager.seciliCekmeTestTanim(
						destructiveTestsSpecs.get(0).getGlobalId(),
						pipes.get(0).getSalesItem().getItemId()).get(0);
			} catch (Exception e) {
				e.printStackTrace();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"KALİTE PLANI BULMA HATASI!", null));
			}

			// TestCekmeSonucManager testCekmeSonucManager = new
			// TestCekmeSonucManager();
			List<TestCekmeSonuc> testCekmeSonucs = new ArrayList<TestCekmeSonuc>();
			List<TestCekmeSonuc> z01Sonucs = new ArrayList<TestCekmeSonuc>();
			List<TestCekmeSonuc> z02Sonucs = new ArrayList<TestCekmeSonuc>();
			List<TestCekmeSonuc> z03Sonucs = new ArrayList<TestCekmeSonuc>();
			List<TestCekmeSonuc> z06Sonucs = new ArrayList<TestCekmeSonuc>();
			List<TestCekmeSonuc> z07Sonucs = new ArrayList<TestCekmeSonuc>();
			// testCekmeSonucs = testCekmeSonucManager.findByField(
			// TestCekmeSonuc.class, "pipe.pipeId", pipeId);
			testCekmeSonucs = TestCekmeSonucManager.getBySalesItem(pipes.get(0)
					.getSalesItem());

			if (testCekmeSonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			// date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(testCekmeSonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = testCekmeSonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ testCekmeSonucs.get(0).getEkleyenEmployee()
								.getEmployee().getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", pipes.get(0).getSalesItem());
			dataMap.put("siparisCekmeOzellik", destructiveTestsSpecs);
			dataMap.put("spec", spec);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			// test bulma
			for (int i = 0; i < testCekmeSonucs.size(); i++) {
				if (testCekmeSonucs.get(i).getBagliGlobalId().getCode()
						.equals(spec.getCode())) {
					z01Sonucs.add(testCekmeSonucs.get(i));
					dataMap.put("cekmeSonuc", z01Sonucs);
				} else if (testCekmeSonucs.get(i).getBagliGlobalId().getCode()
						.equals(spec.getCode())) {
					z02Sonucs.add(testCekmeSonucs.get(i));
					dataMap.put("cekmeSonuc", z02Sonucs);
				} else if (testCekmeSonucs.get(i).getBagliGlobalId().getCode()
						.equals(spec.getCode())) {
					z03Sonucs.add(testCekmeSonucs.get(i));
					dataMap.put("cekmeSonuc", z03Sonucs);
				} else if (testCekmeSonucs.get(i).getBagliGlobalId().getCode()
						.equals(spec.getCode())) {
					z06Sonucs.add(testCekmeSonucs.get(i));
					dataMap.put("cekmeSonuc", z06Sonucs);
				} else if (testCekmeSonucs.get(i).getBagliGlobalId().getCode()
						.equals(spec.getCode())) {
					z07Sonucs.add(testCekmeSonucs.get(i));
					dataMap.put("cekmeSonuc", z07Sonucs);
				}
			}

			ReportUtil reportUtil = new ReportUtil();
			if (spec.getCode().equals("Z02") || spec.getCode().equals("Z03")) {
				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/test/tahribatli/FR-25.xls", pipes
								.get(0).getSalesItem().getProposal()
								.getCustomer().getShortName()
								+ "-" + pipes.get(0).getPipeIndex());

				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"FR-107 RAPORU BAŞARIYLA OLUŞTURULDU!", null));
			} else if (spec.getCode().equals("Z01")
					|| spec.getCode().equals("Z06")
					|| spec.getCode().equals("Z07")) {
				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/test/tahribatli/FR-107.xls",
						pipes.get(0).getSalesItem().getProposal().getCustomer()
								.getShortName()
								+ "-" + pipes.get(0).getPipeIndex());

				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"FR-25 RAPORU BAŞARIYLA OLUŞTURULDU!", null));
			}
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void fr26ResultReportXlsx(Integer pipeId, Integer testId) {
		FacesContext context = FacesContext.getCurrentInstance();
		if (pipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", pipeId);

			// SalesItemManager salesItemManager = new SalesItemManager();
			// List<SalesItem> salesItems = new ArrayList<SalesItem>();
			// salesItems = salesItemManager.findByField(SalesItem.class,
			// "itemId", pipes.get(0).getSalesItem().getItemId());

			List<DestructiveTestsSpec> destructiveTestsSpecs = new ArrayList<DestructiveTestsSpec>();
			// testId ye gore testSpec ekleme
			DestructiveTestsSpecManager dtsManager = new DestructiveTestsSpecManager();
			destructiveTestsSpecs.add(dtsManager.loadObject(
					DestructiveTestsSpec.class, testId));

			// test spec getirme
			BukmeTestsSpec spec = new BukmeTestsSpec();
			try {
				BukmeTestsSpecManager btsManager = new BukmeTestsSpecManager();
				spec = btsManager.seciliBukmeTestTanim(
						destructiveTestsSpecs.get(0).getGlobalId(),
						pipes.get(0).getSalesItem().getItemId()).get(0);
			} catch (Exception e) {
				e.printStackTrace();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"KALİTE PLANI BULMA HATASI!", null));
			}

			TestBukmeSonucManager testBukmeSonucManager = new TestBukmeSonucManager();
			List<TestBukmeSonuc> testBukmeSonucs = new ArrayList<TestBukmeSonuc>();
			testBukmeSonucs = testBukmeSonucManager.findByField(
					TestBukmeSonuc.class, "pipe.pipeId", pipeId);

			if (testBukmeSonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			// date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(testBukmeSonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = testBukmeSonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ testBukmeSonucs.get(0).getEkleyenEmployee()
								.getEmployee().getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", pipes.get(0).getSalesItem());
			dataMap.put("siparisBukmeOzellik", destructiveTestsSpecs.get(0));
			dataMap.put("bukmeSonuc", testBukmeSonucs);
			dataMap.put("spec", spec);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/tahribatli/FR-26.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void fr21ResultReportXlsx(Integer pipeId, Integer testId) {
		FacesContext context = FacesContext.getCurrentInstance();
		if (pipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", pipeId);

			// SalesItemManager salesItemManager = new SalesItemManager();
			// List<SalesItem> salesItems = new ArrayList<SalesItem>();
			// salesItems = salesItemManager.findByField(SalesItem.class,
			// "itemId", pipes.get(0).getSalesItem().getItemId());

			List<DestructiveTestsSpec> destructiveTestsSpecs = new ArrayList<DestructiveTestsSpec>();
			// testId ye gore testSpec ekleme
			DestructiveTestsSpecManager dtsManager = new DestructiveTestsSpecManager();
			destructiveTestsSpecs.add(dtsManager.loadObject(
					DestructiveTestsSpec.class, testId));
			// test spec getirme
			CentikDarbeTestsSpec spec = new CentikDarbeTestsSpec();
			try {
				CentikDarbeTestsSpecManager ctsManager = new CentikDarbeTestsSpecManager();
				spec = ctsManager.seciliCentikTestTanim(
						destructiveTestsSpecs.get(0).getGlobalId(),
						pipes.get(0).getSalesItem().getItemId()).get(0);
			} catch (Exception e) {
				e.printStackTrace();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"KALİTE PLANI BULMA HATASI!", null));
			}

			// TestCentikDarbeSonucManager testCentikDarbeSonucManager = new
			// TestCentikDarbeSonucManager();
			List<TestCentikDarbeSonuc> testCentikDarbeSonucs = new ArrayList<TestCentikDarbeSonuc>();
			List<TestCentikDarbeSonuc> k01Sonucs = new ArrayList<TestCentikDarbeSonuc>();
			List<TestCentikDarbeSonuc> k02Sonucs = new ArrayList<TestCentikDarbeSonuc>();
			List<TestCentikDarbeSonuc> k03Sonucs = new ArrayList<TestCentikDarbeSonuc>();
			List<TestCentikDarbeSonuc> k04Sonucs = new ArrayList<TestCentikDarbeSonuc>();
			List<TestCentikDarbeSonuc> k05Sonucs = new ArrayList<TestCentikDarbeSonuc>();
			List<TestCentikDarbeSonuc> k08Sonucs = new ArrayList<TestCentikDarbeSonuc>();
			List<TestCentikDarbeSonuc> u01Sonucs = new ArrayList<TestCentikDarbeSonuc>();
			List<TestCentikDarbeSonuc> u02Sonucs = new ArrayList<TestCentikDarbeSonuc>();
			List<TestCentikDarbeSonuc> u03Sonucs = new ArrayList<TestCentikDarbeSonuc>();
			List<TestCentikDarbeSonuc> u04Sonucs = new ArrayList<TestCentikDarbeSonuc>();
			List<TestCentikDarbeSonuc> u05Sonucs = new ArrayList<TestCentikDarbeSonuc>();
			// testCentikDarbeSonucs = testCentikDarbeSonucManager.findByField(
			// TestCentikDarbeSonuc.class, "pipe.pipeId", pipeId);

			testCentikDarbeSonucs = TestCentikDarbeSonucManager
					.getBySalesItem(pipes.get(0).getSalesItem());

			if (testCentikDarbeSonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			// date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			date.add(new SimpleDateFormat("dd.MM.yyyy")
					.format(testCentikDarbeSonucs.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = testCentikDarbeSonucs.get(0)
						.getEkleyenEmployee().getEmployee().getFirstname()
						+ " "
						+ testCentikDarbeSonucs.get(0).getEkleyenEmployee()
								.getEmployee().getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", pipes.get(0).getSalesItem());
			dataMap.put("siparisCentikDarbeOzellik", destructiveTestsSpecs);
			dataMap.put("spec", spec);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			// test bulma
			for (int i = 0; i < testCentikDarbeSonucs.size(); i++) {
				if (testCentikDarbeSonucs.get(i).getBagliGlobalId().getCode()
						.equals(spec.getCode())) {
					k01Sonucs.add(testCentikDarbeSonucs.get(i));
					dataMap.put("centikDarbeSonuc", k01Sonucs);
				} else if (testCentikDarbeSonucs.get(i).getBagliGlobalId()
						.getCode().equals(spec.getCode())) {
					k02Sonucs.add(testCentikDarbeSonucs.get(i));
					dataMap.put("centikDarbeSonuc", k02Sonucs);
				} else if (testCentikDarbeSonucs.get(i).getBagliGlobalId()
						.getCode().equals(spec.getCode())) {
					k03Sonucs.add(testCentikDarbeSonucs.get(i));
					dataMap.put("centikDarbeSonuc", k03Sonucs);
				} else if (testCentikDarbeSonucs.get(i).getBagliGlobalId()
						.getCode().equals(spec.getCode())) {
					k04Sonucs.add(testCentikDarbeSonucs.get(i));
					dataMap.put("centikDarbeSonuc", k04Sonucs);
				} else if (testCentikDarbeSonucs.get(i).getBagliGlobalId()
						.getCode().equals(spec.getCode())) {
					k05Sonucs.add(testCentikDarbeSonucs.get(i));
					dataMap.put("centikDarbeSonuc", k05Sonucs);
				} else if (testCentikDarbeSonucs.get(i).getBagliGlobalId()
						.getCode().equals(spec.getCode())) {
					k08Sonucs.add(testCentikDarbeSonucs.get(i));
					dataMap.put("centikDarbeSonuc", k08Sonucs);
				} else if (testCentikDarbeSonucs.get(i).getBagliGlobalId()
						.getCode().equals(spec.getCode())) {
					u01Sonucs.add(testCentikDarbeSonucs.get(i));
					dataMap.put("centikDarbeSonuc", u01Sonucs);
				} else if (testCentikDarbeSonucs.get(i).getBagliGlobalId()
						.getCode().equals(spec.getCode())) {
					u02Sonucs.add(testCentikDarbeSonucs.get(i));
					dataMap.put("centikDarbeSonuc", u02Sonucs);
				} else if (testCentikDarbeSonucs.get(i).getBagliGlobalId()
						.getCode().equals(spec.getCode())) {
					u03Sonucs.add(testCentikDarbeSonucs.get(i));
					dataMap.put("centikDarbeSonuc", u03Sonucs);
				} else if (testCentikDarbeSonucs.get(i).getBagliGlobalId()
						.getCode().equals(spec.getCode())) {
					u04Sonucs.add(testCentikDarbeSonucs.get(i));
					dataMap.put("centikDarbeSonuc", u04Sonucs);
				} else if (testCentikDarbeSonucs.get(i).getBagliGlobalId()
						.getCode().equals(spec.getCode())) {
					u05Sonucs.add(testCentikDarbeSonucs.get(i));
					dataMap.put("centikDarbeSonuc", u05Sonucs);
				}
			}

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/tahribatli/FR-21.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"FR-21 RAPORU BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"FR-21 RAPORU OLUŞTURULAMADI, HATA!", null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "unused", "static-access" })
	public void fr116ResultReportXlsx(Integer pipeId, Integer testId) {
		FacesContext context = FacesContext.getCurrentInstance();
		if (pipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", pipeId);

			// SalesItemManager salesItemManager = new SalesItemManager();
			// List<SalesItem> salesItems = new ArrayList<SalesItem>();
			// salesItems = salesItemManager.findByField(SalesItem.class,
			// "itemId", pipes.get(0).getSalesItem().getItemId());

			List<DestructiveTestsSpec> destructiveTestsSpecs = new ArrayList<DestructiveTestsSpec>();
			// testId ye gore testSpec ekleme
			DestructiveTestsSpecManager dtsManager = new DestructiveTestsSpecManager();
			destructiveTestsSpecs.add(dtsManager.loadObject(
					DestructiveTestsSpec.class, testId));
			// test spec getirme
			KaynakMakroTestSpec spec = new KaynakMakroTestSpec();
			try {
				KaynakMacroTestSpecManager kmsManager = new KaynakMacroTestSpecManager();
				spec = kmsManager.seciliKaynakMakroTestTanim(
						destructiveTestsSpecs.get(0).getGlobalId(),
						pipes.get(0).getSalesItem().getItemId()).get(0);
			} catch (Exception e) {
				e.printStackTrace();

				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"KALİTE PLANI BULMA HATASI!", null));
			}

			TestKaynakSonucManager testKaynakSonucManager = new TestKaynakSonucManager();
			List<TestKaynakSonuc> testKaynakSonucs = new ArrayList<TestKaynakSonuc>();
			List<TestKaynakSonuc> testKaynakSonucs1 = new ArrayList<TestKaynakSonuc>();
			List<TestKaynakSonuc> testKaynakSonucs2 = new ArrayList<TestKaynakSonuc>();
			testKaynakSonucs = testKaynakSonucManager.findByField(
					TestKaynakSonuc.class, "pipe.pipeId", pipeId);

			if (testKaynakSonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}

			EdwPipeLinkManager edwManager = new EdwPipeLinkManager();
			List<EdwPipeLink> edwLink = new ArrayList<EdwPipeLink>();
			List<EdwPipeLink> edwLinkIc = new ArrayList<EdwPipeLink>();
			List<EdwPipeLink> edwLinkDis = new ArrayList<EdwPipeLink>();

			List<EdwPipeLink> edwLinkIcAc = new ArrayList<EdwPipeLink>();
			List<EdwPipeLink> edwLinkDisAc = new ArrayList<EdwPipeLink>();
			List<EdwPipeLink> edwLinkIcDc = new ArrayList<EdwPipeLink>();
			List<EdwPipeLink> edwLinkDisDc = new ArrayList<EdwPipeLink>();

			String edwLinkIcAcA = null;
			String edwLinkIcDcA = null;
			String edwLinkIcAcV = null;
			String edwLinkIcDcV = null;
			String edwLinkDisAcA = null;
			String edwLinkDisDcA = null;
			String edwLinkDisAcV = null;
			String edwLinkDisDcV = null;
			String edwLinkIcAcTel = null;
			String edwLinkIcDcTel = null;
			String edwLinkDisAcTel = null;
			String edwLinkDisDcTel = null;
			try {
				edwLink = edwManager.findByPrmPipeId(pipeId);
				for (int i = 0; i < edwLink.size(); i++) {
					if (edwLink.get(i).getIcDis() != null
							&& edwLink.get(i).getIcDis() == 1) {
						edwLinkIc.add(edwLink.get(i));
					} else if (edwLink.get(i).getIcDis() != null
							&& edwLink.get(i).getIcDis() == 0) {
						edwLinkDis.add(edwLink.get(i));
					}
				}

				for (int i = 0; i < edwLinkIc.size(); i++) {
					if (edwLink.get(i).getAcDc() != null
							&& edwLink.get(i).getAcDc() == 1) {
						edwLinkIcAc.add(edwLink.get(i));
					} else if (edwLink.get(i).getAcDc() != null
							&& edwLink.get(i).getAcDc() == 2) {
						edwLinkIcDc.add(edwLink.get(i));
					}
				}

				for (int i = 0; i < edwLinkDis.size(); i++) {
					if (edwLink.get(i).getAcDc() != null
							&& edwLink.get(i).getAcDc() == 1) {
						edwLinkDisAc.add(edwLink.get(i));
					} else if (edwLink.get(i).getAcDc() != null
							&& edwLink.get(i).getAcDc() == 2) {
						edwLinkDisDc.add(edwLink.get(i));
					}
				}

			} catch (Exception e1) {
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"ÜRETİM PARAMETRELERİNDE HATA VAR, RAPOR ÜRETİLEMEDİ!",
						null));
				return;
			}

			List<String> date = new ArrayList<String>();
			// date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(testKaynakSonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = testKaynakSonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ testKaynakSonucs.get(0).getEkleyenEmployee()
								.getEmployee().getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", pipes.get(0).getSalesItem());
			dataMap.put("siparisKaynakOzellik", destructiveTestsSpecs);
			for (int i = 0; i < testKaynakSonucs.size(); i++) {
				if (testKaynakSonucs.get(i).getBagliGlobalId().getGlobalId() == 1031) {
					testKaynakSonucs1.add(testKaynakSonucs.get(i));
					dataMap.put("icGenislik", testKaynakSonucs1.get(0)
							.getGenislikIc());
					dataMap.put("disGenislik", testKaynakSonucs1.get(0)
							.getGenislikDis());
					dataMap.put("icYukseklik", testKaynakSonucs1.get(0)
							.getYukseklikIc());
					dataMap.put("disYukseklik", testKaynakSonucs1.get(0)
							.getYukseklikDis());
					dataMap.put("icPenetrasyonMm", testKaynakSonucs1.get(0)
							.getPenetrasyonIc());
					dataMap.put("disPenetrasyonMm", testKaynakSonucs1.get(0)
							.getPenetrasyonDis());
					dataMap.put("icPenetrasyonYuzde", "");
					dataMap.put("disPenetrasyonYuzde", "");
					dataMap.put("kaynakKacikligi", testKaynakSonucs1.get(0)
							.getKaynakKacikligi());
					dataMap.put("penetrasyon", testKaynakSonucs1.get(0)
							.getPenetrasyon());
					dataMap.put("sonuc", testKaynakSonucs1.get(0).getSonuc());

				} else if (testKaynakSonucs.get(i).getBagliGlobalId()
						.getGlobalId() == 1032) {
					testKaynakSonucs2.add(testKaynakSonucs.get(i));
					dataMap.put("kaynakSonuc2", testKaynakSonucs2.get(0));// H01
																			// form
																			// değerleri
				}
			}
			dataMap.put("spec", spec);// min max
			dataMap.put("date", date);
			dataMap.put("partiNo", testKaynakSonucs.get(0).getPartiNo());
			dataMap.put("dokumNo", pipes.get(0).getRuloPipeLink().getRulo()
					.getRuloDokumNo());
			dataMap.put("ruloNo", pipes.get(0).getRuloPipeLink().getRulo()
					.getRuloYilAndRuloKimlikId());
			dataMap.put("pipeNo", pipes.get(0).getPipeBarkodNo());

			if (edwLinkIcAc.size() > 0) {
				dataMap.put("edwLinkIcAcA", edwLinkIcAc.get(0).getAkim());
			}
			if (edwLinkIcDc.size() > 0) {
				dataMap.put("edwLinkIcDcA", edwLinkIcDc.get(0).getAkim());
			}
			if (edwLinkIcAc.size() > 0) {
				dataMap.put("edwLinkIcAcV", edwLinkIcAc.get(0).getVolt());
			}
			if (edwLinkIcAc.size() > 0) {
				dataMap.put("edwLinkIcDcV", edwLinkIcAc.get(0).getVolt());
			}

			if (edwLinkDisAc.size() > 0) {
				dataMap.put("edwLinkDisAcA", edwLinkDisAc.get(0).getAkim());
			}
			if (edwLinkDisDc.size() > 0) {
				dataMap.put("edwLinkDisDcA", edwLinkDisDc.get(0).getAkim());
			}
			if (edwLinkDisAc.size() > 0) {
				dataMap.put("edwLinkDisAcV", edwLinkDisAc.get(0).getVolt());
			}
			if (edwLinkDisDc.size() > 0) {
				dataMap.put("edwLinkDisDcV", edwLinkDisDc.get(0).getVolt());
			}

			if (edwLinkIcAc.size() > 0) {
				dataMap.put("edwLinkIcAcTel", edwLinkIcAc.get(0).getEdw()
						.getCap());
			}
			if (edwLinkIcDc.size() > 0) {
				dataMap.put("edwLinkIcDcTel", edwLinkIcDc.get(0).getEdw()
						.getCap());
			}
			if (edwLinkDisAc.size() > 0) {
				dataMap.put("edwLinkDisAcTel", edwLinkDisAc.get(0).getEdw()
						.getCap());
			}
			if (edwLinkDisDc.size() > 0) {
				dataMap.put("edwLinkDisDcTel", edwLinkDisDc.get(0).getEdw()
						.getCap());
			}

			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/tahribatli/FR-116.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"FR-116 RAPORU BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"FR-116 RAPORU OLUŞTURULAMADI, HATA!", null));
			e.printStackTrace();
		}
	}

	/**
	 * @return the config
	 */
	public ConfigBean getConfig() {
		return config;
	}

	/**
	 * @param config
	 *            the config to set
	 */
	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
