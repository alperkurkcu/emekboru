/**
 * 
 */
package com.emekboru.beans.satinalma;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.satinalma.SatinalmaAmbarMalzemeTalepForm;
import com.emekboru.jpa.satinalma.SatinalmaAmbarMalzemeTalepFormIcerik;
import com.emekboru.jpa.satinalma.SatinalmaDurum;
import com.emekboru.jpaman.satinalma.SatinalmaAmbarMalzemeTalepFormIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaAmbarMalzemeTalepFormManager;
import com.emekboru.jpaman.satinalma.SatinalmaDurumManager;
import com.emekboru.utils.DateTrans;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "satinalmaAmbarMalzemeTalepFormIcerikBean")
@ViewScoped
public class SatinalmaAmbarMalzemeTalepFormIcerikBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<SatinalmaAmbarMalzemeTalepFormIcerik> allSatinalmaAmbarMalzemeTalepFormIcerikList = new ArrayList<SatinalmaAmbarMalzemeTalepFormIcerik>();

	private SatinalmaAmbarMalzemeTalepFormIcerik satinalmaAmbarMalzemeTalepFormIcerikEkran = new SatinalmaAmbarMalzemeTalepFormIcerik();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	int siraNo = 1;

	Integer prmFormId = 0;

	private SatinalmaDurum satinalmaDurum = new SatinalmaDurum();

	private boolean kontrol = false;

	public SatinalmaAmbarMalzemeTalepFormIcerikBean() {

		updateButtonRender = false;
	}

	public void addIcerik() {

		satinalmaAmbarMalzemeTalepFormIcerikEkran = new SatinalmaAmbarMalzemeTalepFormIcerik();
		// today set ediliyor
		java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(
				DateTrans.calendar.getTime().getTime());
		satinalmaAmbarMalzemeTalepFormIcerikEkran
				.setIhtiyacSuresi(currentTimestamp);
		updateButtonRender = false;
	}

	public void addOrUpdateIcerik(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		prmFormId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		SatinalmaAmbarMalzemeTalepFormManager manager = new SatinalmaAmbarMalzemeTalepFormManager();
		SatinalmaAmbarMalzemeTalepFormIcerikManager formManager = new SatinalmaAmbarMalzemeTalepFormIcerikManager();

		siraNoBul(prmFormId);

		if (satinalmaAmbarMalzemeTalepFormIcerikEkran.getId() == null) {

			try {
				satinalmaAmbarMalzemeTalepFormIcerikEkran
						.setSatinalmaAmbarMalzemeTalepForm(manager.findByField(
								SatinalmaAmbarMalzemeTalepForm.class, "id",
								prmFormId).get(0));

				if (satinalmaAmbarMalzemeTalepFormIcerikEkran
						.getSatinalmaAmbarMalzemeTalepForm()
						.getSatinalmaDurum().getId() == 14) {
					FacesContext context = FacesContext.getCurrentInstance();
					context.addMessage(null, new FacesMessage(
							FacesMessage.SEVERITY_INFO,
							":) ARTIK BU ŞEKİLDE EKLENEMİYOR, EKLEME HATASI!",
							null));
					return;
				}

				satinalmaAmbarMalzemeTalepFormIcerikEkran
						.setEklemeZamani(UtilInsCore.getTarihZaman());
				satinalmaAmbarMalzemeTalepFormIcerikEkran
						.setEkleyenKullanici(userBean.getUser().getId());

				satinalmaAmbarMalzemeTalepFormIcerikEkran.setSiraNo(siraNo);
				satinalmaAmbarMalzemeTalepFormIcerikEkran
						.setKalan(satinalmaAmbarMalzemeTalepFormIcerikEkran
								.getMiktar());
				satinalmaAmbarMalzemeTalepFormIcerikEkran
						.setStokTanimlarBirim(satinalmaAmbarMalzemeTalepFormIcerikEkran
								.getStokUrunKartlari().getStokTanimlarBirim());

				formManager.enterNew(satinalmaAmbarMalzemeTalepFormIcerikEkran);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"EKLEME HATASI, LÜTFEN 'Sarf Merkezi' ve 'Departman' SEÇİNİZ!",
								null));
				this.satinalmaAmbarMalzemeTalepFormIcerikEkran = new SatinalmaAmbarMalzemeTalepFormIcerik();
				return;
			}

			this.satinalmaAmbarMalzemeTalepFormIcerikEkran = new SatinalmaAmbarMalzemeTalepFormIcerik();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"İSTEK KALEMİ BAŞARIYLA EKLENMİŞTİR!", null));

		} else {

			if (satinalmaAmbarMalzemeTalepFormIcerikEkran
					.getSatinalmaAmbarMalzemeTalepForm().getSatinalmaDurum()
					.getId() == 14) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								":) ARTIK BU ŞEKİLDE EKLENEMİYOR, EKLEME HATASI!",
								null));
				return;
			}

			satinalmaAmbarMalzemeTalepFormIcerikEkran
					.setGuncellemeZamani(UtilInsCore.getTarihZaman());
			satinalmaAmbarMalzemeTalepFormIcerikEkran
					.setGuncelleyenKullanici(userBean.getUser().getId());
			satinalmaAmbarMalzemeTalepFormIcerikEkran
					.setKalan(satinalmaAmbarMalzemeTalepFormIcerikEkran
							.getMiktar());
			formManager.updateEntity(satinalmaAmbarMalzemeTalepFormIcerikEkran);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"İSTEK KALEMİ BAŞARIYLA G>ÜNCELLENMİŞTİR!", null));
		}

		fillTestList(prmFormId);
	}

	public void deleteForm(ActionEvent e) {

		FacesContext context = FacesContext.getCurrentInstance();

		if (satinalmaAmbarMalzemeTalepFormIcerikEkran.getId() == null) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL,
					"SİLME HATASI, SİLİNECEK MALZEME SEÇİNİZ!!", null));
			return;
		}

		prmFormId = satinalmaAmbarMalzemeTalepFormIcerikEkran
				.getSatinalmaAmbarMalzemeTalepForm().getId();

		SatinalmaAmbarMalzemeTalepFormIcerikManager formManager = new SatinalmaAmbarMalzemeTalepFormIcerikManager();

		if (satinalmaAmbarMalzemeTalepFormIcerikEkran.getKalan().equals(
				satinalmaAmbarMalzemeTalepFormIcerikEkran.getMiktar())) {

			formManager.delete(satinalmaAmbarMalzemeTalepFormIcerikEkran);
			fillTestList(prmFormId);
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"SEÇİLEN MALZEME BAŞARIYLA SİLİNMİŞTİR!!", null));
			talepDurumDuzenle();
		} else {

			fillTestList(prmFormId);
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL,
					"SİLME HATASI, SEÇİLEN MALZEME ÇIKIŞI VARDIR!!", null));
		}
	}

	public void talepDurumDuzenle() {

		SatinalmaDurumManager durumManager = new SatinalmaDurumManager();
		SatinalmaAmbarMalzemeTalepFormManager talepMan = new SatinalmaAmbarMalzemeTalepFormManager();

		Integer kalanRender = 0;
		Integer miktarRender = 0;

		if (allSatinalmaAmbarMalzemeTalepFormIcerikList.size() == 0) {
			satinalmaDurum = durumManager.loadObject(SatinalmaDurum.class, 1);
			satinalmaAmbarMalzemeTalepFormIcerikEkran
					.getSatinalmaAmbarMalzemeTalepForm().setSatinalmaDurum(
							satinalmaDurum);
			talepMan.updateEntity(satinalmaAmbarMalzemeTalepFormIcerikEkran
					.getSatinalmaAmbarMalzemeTalepForm());
			return;
		}

		for (int i = 0; i < allSatinalmaAmbarMalzemeTalepFormIcerikList.size(); i++) {
			if (allSatinalmaAmbarMalzemeTalepFormIcerikList.get(i).getKalan() != 0) {
				miktarRender++;
			} else
				kalanRender++;
		}
		if (kalanRender == allSatinalmaAmbarMalzemeTalepFormIcerikList.size()) {
			// kapandı
			satinalmaDurum = durumManager.loadObject(SatinalmaDurum.class, 14);
			satinalmaAmbarMalzemeTalepFormIcerikEkran
					.getSatinalmaAmbarMalzemeTalepForm().setSatinalmaDurum(
							satinalmaDurum);
			talepMan.updateEntity(satinalmaAmbarMalzemeTalepFormIcerikEkran
					.getSatinalmaAmbarMalzemeTalepForm());
			return;
		} else if (miktarRender == allSatinalmaAmbarMalzemeTalepFormIcerikList
				.size()) {
			// yeni açıldı
			satinalmaDurum = durumManager.loadObject(SatinalmaDurum.class, 1);
			satinalmaAmbarMalzemeTalepFormIcerikEkran
					.getSatinalmaAmbarMalzemeTalepForm().setSatinalmaDurum(
							satinalmaDurum);
			talepMan.updateEntity(satinalmaAmbarMalzemeTalepFormIcerikEkran
					.getSatinalmaAmbarMalzemeTalepForm());
			return;
		} else {
			// kısmı tamamlandı
			satinalmaDurum = durumManager.loadObject(SatinalmaDurum.class, 3);
			satinalmaAmbarMalzemeTalepFormIcerikEkran
					.getSatinalmaAmbarMalzemeTalepForm().setSatinalmaDurum(
							satinalmaDurum);
			talepMan.updateEntity(satinalmaAmbarMalzemeTalepFormIcerikEkran
					.getSatinalmaAmbarMalzemeTalepForm());
			return;
		}
	}

	public void fillTestList(Integer formId) {

		SatinalmaAmbarMalzemeTalepFormIcerikManager manager = new SatinalmaAmbarMalzemeTalepFormIcerikManager();
		allSatinalmaAmbarMalzemeTalepFormIcerikList = manager
				.getAllSatinalmaAmbarMalzemeTalepFormIcerik(formId);
	}

	public void formListener(SelectEvent event) {

		updateButtonRender = true;
	}

	private void siraNoBul(int formId) {

		SatinalmaAmbarMalzemeTalepFormIcerikManager formManager = new SatinalmaAmbarMalzemeTalepFormIcerikManager();

		if (formManager.findByField(SatinalmaAmbarMalzemeTalepFormIcerik.class,
				"satinalmaAmbarMalzemeTalepForm.id", formId) != null) {
			siraNo = formManager.findByField(
					SatinalmaAmbarMalzemeTalepFormIcerik.class,
					"satinalmaAmbarMalzemeTalepForm.id", formId).size() + 1;
		}
	}

	// SETTERS GETTERS
	public List<SatinalmaAmbarMalzemeTalepFormIcerik> getAllSatinalmaAmbarMalzemeTalepFormIcerikList() {
		return allSatinalmaAmbarMalzemeTalepFormIcerikList;
	}

	public void setAllSatinalmaAmbarMalzemeTalepFormIcerikList(
			List<SatinalmaAmbarMalzemeTalepFormIcerik> allSatinalmaAmbarMalzemeTalepFormIcerikList) {
		this.allSatinalmaAmbarMalzemeTalepFormIcerikList = allSatinalmaAmbarMalzemeTalepFormIcerikList;
	}

	public SatinalmaAmbarMalzemeTalepFormIcerik getSatinalmaAmbarMalzemeTalepFormIcerikEkran() {
		return satinalmaAmbarMalzemeTalepFormIcerikEkran;
	}

	public void setSatinalmaAmbarMalzemeTalepFormIcerikEkran(
			SatinalmaAmbarMalzemeTalepFormIcerik satinalmaAmbarMalzemeTalepFormIcerikEkran) {
		this.satinalmaAmbarMalzemeTalepFormIcerikEkran = satinalmaAmbarMalzemeTalepFormIcerikEkran;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
