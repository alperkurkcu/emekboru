/**
 * 
 */
package com.emekboru.beans.satinalma;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.satinalma.SatinalmaSarfMerkezi;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.satinalma.SatinalmaSarfMerkeziManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "sarfMerkeziBean")
@ViewScoped
public class SarfMerkeziBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private List<SatinalmaSarfMerkezi> allSatinalmaSarfMerkezis = new ArrayList<SatinalmaSarfMerkezi>();
	private SatinalmaSarfMerkezi selectedSatinalmaSarfMerkezi = new SatinalmaSarfMerkezi();
	private SatinalmaSarfMerkezi newSatinalmaSarfMerkezi = new SatinalmaSarfMerkezi();

	public SarfMerkeziBean() {
	}

	@PostConstruct
	private void load() {

		SatinalmaSarfMerkeziManager manager = new SatinalmaSarfMerkeziManager();
		try {
			allSatinalmaSarfMerkezis = manager.findAllOrderByASC(
					SatinalmaSarfMerkezi.class, "name");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out
					.println("HATA - allSatinalmaSarfMerkezis - SarfMerkeziBean.load()");
			e.printStackTrace();
		}
	}

	public void deleteSelectedSarfMerkezi() {

		if (selectedSatinalmaSarfMerkezi == null) {

			FacesContextUtils.addPlainErrorMessage("Sarf merkezi seçmediniz!");

			return;
		}

		CommonQueries<SatinalmaSarfMerkezi> commonQueries = new CommonQueries<SatinalmaSarfMerkezi>();
		commonQueries.delete(selectedSatinalmaSarfMerkezi);

		FacesContextUtils
				.addPlainInfoMessage("Sarf merkezi başarıyla silindi.");

		this.load();
	}

	public void editSelectedSarfMerkezi() {

		if (selectedSatinalmaSarfMerkezi == null) {

			FacesContextUtils.addPlainErrorMessage("Sarf merkezi seçmediniz!");

			return;
		}

		Timestamp eklemeZamani = new java.sql.Timestamp(
				new java.util.Date().getTime());
		int ekleyenKullanici = userBean.getUser().getId();

		selectedSatinalmaSarfMerkezi.setEklemeZamani(eklemeZamani);
		selectedSatinalmaSarfMerkezi.setEkleyenKullanici(ekleyenKullanici);

		CommonQueries<SatinalmaSarfMerkezi> commonQueries = new CommonQueries<SatinalmaSarfMerkezi>();
		commonQueries.updateEntity(selectedSatinalmaSarfMerkezi);

		FacesContextUtils
				.addPlainInfoMessage("Sarf merkezi başarıyla düzenlendi.");

		this.load();
	}

	public void createNewSarfMerkezi() {

		Timestamp eklemeZamani = new java.sql.Timestamp(
				new java.util.Date().getTime());
		int ekleyenKullanici = userBean.getUser().getId();

		newSatinalmaSarfMerkezi.setEklemeZamani(eklemeZamani);
		newSatinalmaSarfMerkezi.setEkleyenKullanici(ekleyenKullanici);

		CommonQueries<SatinalmaSarfMerkezi> commonQueries = new CommonQueries<SatinalmaSarfMerkezi>();
		commonQueries.enterNew(newSatinalmaSarfMerkezi);

		newSatinalmaSarfMerkezi = new SatinalmaSarfMerkezi();
		FacesContextUtils
				.addPlainInfoMessage("Yeni sarf merkezi başarıyla oluşturuldu.");

		this.load();
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public List<SatinalmaSarfMerkezi> getAllSatinalmaSarfMerkezis() {
		return allSatinalmaSarfMerkezis;
	}

	public void setAllSatinalmaSarfMerkezis(
			List<SatinalmaSarfMerkezi> allSatinalmaSarfMerkezis) {
		this.allSatinalmaSarfMerkezis = allSatinalmaSarfMerkezis;
	}

	public SatinalmaSarfMerkezi getSelectedSatinalmaSarfMerkezi() {
		return selectedSatinalmaSarfMerkezi;
	}

	public void setSelectedSatinalmaSarfMerkezi(
			SatinalmaSarfMerkezi selectedSatinalmaSarfMerkezi) {
		this.selectedSatinalmaSarfMerkezi = selectedSatinalmaSarfMerkezi;
	}

	public SatinalmaSarfMerkezi getNewSatinalmaSarfMerkezi() {
		return newSatinalmaSarfMerkezi;
	}

	public void setNewSatinalmaSarfMerkezi(
			SatinalmaSarfMerkezi newSatinalmaSarfMerkezi) {
		this.newSatinalmaSarfMerkezi = newSatinalmaSarfMerkezi;
	}

}
