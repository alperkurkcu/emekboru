/**
 * 
 */
package com.emekboru.beans.kaplamamakinesi;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "kaplamaMakinesiOrtakBean")
@ViewScoped
public class KaplamaMakinesiOrtakBean {

	@EJB
	private ConfigBean configBean;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	// Dış Kumlama Makinası
	public void goToKaplamaMakinasiDisKumlama() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().KAPLAMA_MAKINESI_DIS_KUMLAMA_PAGE);
	}

	// İç Kumlama Makinası
	public void goToKaplamaMakinasiIcKumlama() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().KAPLAMA_MAKINESI_IC_KUMLAMA_PAGE);
	}

	// Polietilen Makinası
	public void goToKaplamaMakinasiPolietilen() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().KAPLAMA_MAKINESI_POLIETILEN_PAGE);
	}

	// Epoksi Makinası
	public void goToKaplamaMakinasiEpoksi() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().KAPLAMA_MAKINESI_EPOKSI_PAGE);
	}

	// Beton Makinası
	public void goToKaplamaMakinasiBeton() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().KAPLAMA_MAKINESI_BETON_PAGE);
	}

	// Muflama Makinası
	public void goToKaplamaMakinasiMuflama() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().KAPLAMA_MAKINESI_MUFLAMA_PAGE);
	}

	// Fırçalama Makinası
	public void goToKaplamaMakinasiFircalama() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().KAPLAMA_MAKINESI_FIRCALAMA_PAGE);
	}

	// Asit Yıkma Makinası
	public void goToKaplamaMakinasiAsitYikama() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().KAPLAMA_MAKINESI_ASIT_YIKAMA_PAGE);
	}

	// Kaplama Kalite Takip Formu
	public void goToKaplamaKaliteTakipFormu() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().KAPLAMA_KALITE_TAKIP_FORMU_PAGE);
	}

	// Kaplama Malzemeleri Kullanımı
	public void goToKaplamaMalzemesiKullanma() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().KAPLAMA_MALZEMESI_KULLANMA_PAGE);
	}

	// Markalama Robotu kullanma ekranı
	public void goToMarkalamaMakinesi() {

		FacesContextUtils
				.redirect(configBean.getPageUrl().MARKALAMA_MALZEMESI_KULLANMA_PAGE);
	}

	// setters getters

	public ConfigBean getConfigBean() {
		return configBean;
	}

	public void setConfigBean(ConfigBean configBean) {
		this.configBean = configBean;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

}
