/**
 * 
 */
package com.emekboru.beans.test.tahribatsiztestsonuc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.test.tahribatsiz.TahribatsizTestOrderListBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.kalibrasyon.KalibrasyonPenetrant;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizPenetrantSonuc;
import com.emekboru.jpaman.NondestructiveTestsSpecManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.kalibrasyon.KalibrasyonPenetrantManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizPenetrantSonucManager;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */

@ManagedBean(name = "testTahribatsizPenetrantSonucBean")
@ViewScoped
public class TestTahribatsizPenetrantSonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<TestTahribatsizPenetrantSonuc> allTestTahribatsizPenetrantSonucList = new ArrayList<TestTahribatsizPenetrantSonuc>();
	private TestTahribatsizPenetrantSonuc testTahribatsizPenetrantSonucForm = new TestTahribatsizPenetrantSonuc();

	int index = 0;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private boolean visibleButtonRender;

	private Pipe selectedPipe = new Pipe();

	private String barkodNo = null;

	private List<KalibrasyonPenetrant> kalibrasyonlar = new ArrayList<KalibrasyonPenetrant>();
	private KalibrasyonPenetrant kalibrasyon = new KalibrasyonPenetrant();

	public void penetrantListener(SelectEvent event) {
		updateButtonRender = true;
		visibleButtonRender = false;
	}

	@SuppressWarnings("static-access")
	public void fillTestList(Integer pipeId) {

		TestTahribatsizPenetrantSonucManager testTahribatsizPenetrantSonucManager = new TestTahribatsizPenetrantSonucManager();
		allTestTahribatsizPenetrantSonucList = testTahribatsizPenetrantSonucManager
				.getAllTestTahribatsizPenetrantSonuc(pipeId);
	}

	public void addTestTahribatsizPenetrantSonuc() {
		testTahribatsizPenetrantSonucForm = new TestTahribatsizPenetrantSonuc();
		updateButtonRender = false;
	}

	@SuppressWarnings({ "static-access", "unused" })
	public void addOrUpdatePenetrantSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();

		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");
		Integer prmItemId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");

		Integer testId = 0;

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}
		if (prmItemId == null) {
			prmItemId = selectedPipe.getSalesItem().getItemId();
		}

		TestTahribatsizPenetrantSonucManager penetrantManager = new TestTahribatsizPenetrantSonucManager();
		NondestructiveTestsSpecManager nonManager = new NondestructiveTestsSpecManager();

		try {
			testId = nonManager.findByItemIdGlobalId(prmItemId, 1009).get(0)
					.getTestId();

		} catch (Exception e2) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"KOORDİNAT EKLEME BAŞARISIZ, KALİTE TANIMLARINDA RENETRANT TEST EKSİKTİR!",
							null));
			return;
		}

		if (testTahribatsizPenetrantSonucForm.getId() == null) {

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);

			// testTahribatsizPenetrantSonucForm.setTestId(testId);
			testTahribatsizPenetrantSonucForm.setPipe(pipe);

			testTahribatsizPenetrantSonucForm.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			testTahribatsizPenetrantSonucForm.setEkleyenEmployee(userBean
					.getUser());

			sonKalibrasyonuGoster();
			testTahribatsizPenetrantSonucForm.setKalibrasyon(kalibrasyon);

			penetrantManager.enterNew(testTahribatsizPenetrantSonucForm);

			this.testTahribatsizPenetrantSonucForm = new TestTahribatsizPenetrantSonuc();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"PENETRANT MUAYENE KOORDİNATI BAŞARI İLE KAYDEDİLMİŞTİR!"));
		} else {

			testTahribatsizPenetrantSonucForm.setGuncellemeZamani(UtilInsCore
					.getTarihZaman());
			testTahribatsizPenetrantSonucForm.setGuncelleyenEmployee(userBean
					.getUser());
			// testTahribatsizPenetrantSonucForm.setTestId(testId);

			penetrantManager.updateEntity(testTahribatsizPenetrantSonucForm);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"PENETRANT MUAYENE BAŞARI İLE GÜNCELLENMİŞTİR!"));
		}

		fillTestList(prmPipeId);
		testTahribatsizPenetrantSonucForm = new TestTahribatsizPenetrantSonuc();
		updateButtonRender = false;
		// sayfada barkoddan okutunca order item pipe seçimi olmadıgı için
		// sıkıntı cıkıyordu
		TahribatsizTestOrderListBean ttolb = new TahribatsizTestOrderListBean();
		ttolb.loadOrderDetails(selectedPipe.getSalesItem().getItemId());
	}

	public void deletePenetrantSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		TestTahribatsizPenetrantSonucManager penetrantManager = new TestTahribatsizPenetrantSonucManager();

		if (testTahribatsizPenetrantSonucForm.getId() == null) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"LİSTEDEN SEÇİLMİŞ BİR DEĞER YOKTUR!", null));
			return;
		}

		penetrantManager.delete(testTahribatsizPenetrantSonucForm);
		testTahribatsizPenetrantSonucForm = new TestTahribatsizPenetrantSonuc();
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
				"PENETRANT MUAYENE KOORDİNATI SİLİNMİŞTİR!", null));

		fillTestList(prmPipeId);
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0
				|| prmBarkod.equals("")) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " BARKOD NUMARALI BORU SİSTEMDE YOKTUR!", null));
			visibleButtonRender = false;
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			TestTahribatsizPenetrantSonucManager penetrantManager = new TestTahribatsizPenetrantSonucManager();
			allTestTahribatsizPenetrantSonucList = penetrantManager
					.getAllTestTahribatsizPenetrantSonuc(selectedPipe
							.getPipeId());
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
			visibleButtonRender = true;
		}
		KalibrasyonPenetrantManager kalibrasyonManager = new KalibrasyonPenetrantManager();
		kalibrasyonlar = kalibrasyonManager.getAllKalibrasyon();
	}

	@SuppressWarnings("static-access")
	public void sonKalibrasyonuGoster() {
		kalibrasyon = new KalibrasyonPenetrant();
		KalibrasyonPenetrantManager kalibrasyonManager = new KalibrasyonPenetrantManager();
		kalibrasyon = kalibrasyonManager.getAllKalibrasyon().get(0);
	}

	public void kalibrasyonGoster() {
		kalibrasyon = new KalibrasyonPenetrant();
		kalibrasyon = testTahribatsizPenetrantSonucForm.getKalibrasyon();
	}

	public void kalibrasyonAta() {
		TestTahribatsizPenetrantSonucManager penetrantManager = new TestTahribatsizPenetrantSonucManager();
		try {
			for (int i = 0; i < allTestTahribatsizPenetrantSonucList.size(); i++) {
				allTestTahribatsizPenetrantSonucList.get(i).setKalibrasyon(
						kalibrasyon);
				penetrantManager
						.updateEntity(allTestTahribatsizPenetrantSonucList
								.get(i));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"KALİBRASYON ATAMA BAŞARISIZ, TEKRAR DENEYİNİZ!", null));
			return;
		}
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				"KALİBRASYON ATAMA BAŞARILI!", null));
	}

	// setters getters

	/**
	 * @return the allTestTahribatsizPenetrantSonucList
	 */
	public List<TestTahribatsizPenetrantSonuc> getAllTestTahribatsizPenetrantSonucList() {
		return allTestTahribatsizPenetrantSonucList;
	}

	/**
	 * @param allTestTahribatsizPenetrantSonucList
	 *            the allTestTahribatsizPenetrantSonucList to set
	 */
	public void setAllTestTahribatsizPenetrantSonucList(
			List<TestTahribatsizPenetrantSonuc> allTestTahribatsizPenetrantSonucList) {
		this.allTestTahribatsizPenetrantSonucList = allTestTahribatsizPenetrantSonucList;
	}

	/**
	 * @return the testTahribatsizPenetrantSonucForm
	 */
	public TestTahribatsizPenetrantSonuc getTestTahribatsizPenetrantSonucForm() {
		return testTahribatsizPenetrantSonucForm;
	}

	/**
	 * @param testTahribatsizPenetrantSonucForm
	 *            the testTahribatsizPenetrantSonucForm to set
	 */
	public void setTestTahribatsizPenetrantSonucForm(
			TestTahribatsizPenetrantSonuc testTahribatsizPenetrantSonucForm) {
		this.testTahribatsizPenetrantSonucForm = testTahribatsizPenetrantSonucForm;
	}

	/**
	 * @return the index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @param index
	 *            the index to set
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the selectedPipe
	 */
	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	/**
	 * @param selectedPipe
	 *            the selectedPipe to set
	 */
	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	/**
	 * @return the barkodNo
	 */
	public String getBarkodNo() {
		return barkodNo;
	}

	/**
	 * @param barkodNo
	 *            the barkodNo to set
	 */
	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the kalibrasyonlar
	 */
	public List<KalibrasyonPenetrant> getKalibrasyonlar() {
		return kalibrasyonlar;
	}

	/**
	 * @param kalibrasyonlar
	 *            the kalibrasyonlar to set
	 */
	public void setKalibrasyonlar(List<KalibrasyonPenetrant> kalibrasyonlar) {
		this.kalibrasyonlar = kalibrasyonlar;
	}

	/**
	 * @return the kalibrasyon
	 */
	public KalibrasyonPenetrant getKalibrasyon() {
		return kalibrasyon;
	}

	/**
	 * @param kalibrasyon
	 *            the kalibrasyon to set
	 */
	public void setKalibrasyon(KalibrasyonPenetrant kalibrasyon) {
		this.kalibrasyon = kalibrasyon;
	}

	/**
	 * @return the visibleButtonRender
	 */
	public boolean isVisibleButtonRender() {
		return visibleButtonRender;
	}

}
