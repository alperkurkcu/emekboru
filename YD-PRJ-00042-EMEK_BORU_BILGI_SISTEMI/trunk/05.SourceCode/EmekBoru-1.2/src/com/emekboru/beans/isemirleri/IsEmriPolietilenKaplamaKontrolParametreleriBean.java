/**
 * 
 */
package com.emekboru.beans.isemirleri;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.isemri.IsEmriPolietilenKaplama;
import com.emekboru.jpa.isemri.IsEmriPolietilenKaplamaKontrolParametreleri;
import com.emekboru.jpaman.isemirleri.IsEmriPolietilenKaplamaKontrolParametreleriManager;
import com.emekboru.jpaman.isemirleri.IsEmriPolietilenKaplamaManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isEmriPolietilenKaplamaKontrolParametreleriBean")
@ViewScoped
public class IsEmriPolietilenKaplamaKontrolParametreleriBean implements
		Serializable {

	private static final long serialVersionUID = 1L;

	private IsEmriPolietilenKaplamaKontrolParametreleri isEmriPolietilenKaplamaKontrolParametreleriForm = new IsEmriPolietilenKaplamaKontrolParametreleri();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	public IsEmriPolietilenKaplamaKontrolParametreleriBean() {

	}

	public void addOrUpdateIsEmriPolietilenKaplamaKontrolParametreleri(
			ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmIsEmriId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		// Integer prmParamId = 0;

		IsEmriPolietilenKaplama isEmriPolietilenKaplama = new IsEmriPolietilenKaplamaManager()
				.loadObject(IsEmriPolietilenKaplama.class, prmIsEmriId);

		IsEmriPolietilenKaplamaKontrolParametreleriManager isMan = new IsEmriPolietilenKaplamaKontrolParametreleriManager();
		IsEmriPolietilenKaplamaManager kaplamaMan = new IsEmriPolietilenKaplamaManager();

		if (isEmriPolietilenKaplamaKontrolParametreleriForm.getId() == null) {

			isEmriPolietilenKaplamaKontrolParametreleriForm
					.setIsEmriPolietilenKaplama(isEmriPolietilenKaplama);
			isEmriPolietilenKaplamaKontrolParametreleriForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			isEmriPolietilenKaplamaKontrolParametreleriForm
					.setEkleyenEmployee(userBean.getUser());
			isEmriPolietilenKaplamaKontrolParametreleriForm
					.setIsEmriPolietilenKaplama(isEmriPolietilenKaplama);

			isMan.enterNew(isEmriPolietilenKaplamaKontrolParametreleriForm);

			// IsEmriPolietilenKaplamaKontrolParametreleriManager paramMan = new
			// IsEmriPolietilenKaplamaKontrolParametreleriManager();
			// if (paramMan.getAllIsEmriPolietilenKaplamaKontrolParametreleri(
			// prmIsEmriId).size() > 0) {
			// isEmriPolietilenKaplamaKontrolParametreleriForm = paramMan
			// .getAllIsEmriPolietilenKaplamaKontrolParametreleri(
			// prmIsEmriId).get(0);
			// }
			isEmriPolietilenKaplama
					.setIsEmriPolietilenKaplamaKontrolParametreleri(isEmriPolietilenKaplamaKontrolParametreleriForm);
			kaplamaMan.updateEntity(isEmriPolietilenKaplama);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"KONTROL ÜNİTESİ PARAMETRELERİ BAŞARIYLA EKLENMİŞTİR!"));
		} else {

			isEmriPolietilenKaplamaKontrolParametreleriForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			isEmriPolietilenKaplamaKontrolParametreleriForm
					.setGuncelleyenEmployee(userBean.getUser());
			isMan.updateEntity(isEmriPolietilenKaplamaKontrolParametreleriForm);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"KONTROL ÜNİTESİ PARAMETRELERİ BAŞARIYLA GÜNCELLENMİŞTİR!",
					null));
		}
	}

	public void deleteIsEmriPolietilenKaplamaKontrolParametreleri() {

		IsEmriPolietilenKaplamaKontrolParametreleriManager isMan = new IsEmriPolietilenKaplamaKontrolParametreleriManager();

		if (isEmriPolietilenKaplamaKontrolParametreleriForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		isMan.delete(isEmriPolietilenKaplamaKontrolParametreleriForm);
		isEmriPolietilenKaplamaKontrolParametreleriForm = new IsEmriPolietilenKaplamaKontrolParametreleri();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
				"KONTROL ÜNİTESİ PARAMETRELERİ BAŞARIYLA SİLİNMİŞTİR!", null));
	}

	@SuppressWarnings("static-access")
	public void loadParametreler(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmIsEmriId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");

		IsEmriPolietilenKaplamaKontrolParametreleriManager paramMan = new IsEmriPolietilenKaplamaKontrolParametreleriManager();
		if (paramMan.getAllIsEmriPolietilenKaplamaKontrolParametreleri(
				prmIsEmriId).size() > 0) {
			isEmriPolietilenKaplamaKontrolParametreleriForm = paramMan
					.getAllIsEmriPolietilenKaplamaKontrolParametreleri(
							prmIsEmriId).get(0);
		}
	}

	// setters getters

	public IsEmriPolietilenKaplamaKontrolParametreleri getIsEmriPolietilenKaplamaKontrolParametreleriForm() {
		return isEmriPolietilenKaplamaKontrolParametreleriForm;
	}

	public void setIsEmriPolietilenKaplamaKontrolParametreleriForm(
			IsEmriPolietilenKaplamaKontrolParametreleri isEmriPolietilenKaplamaKontrolParametreleriForm) {
		this.isEmriPolietilenKaplamaKontrolParametreleriForm = isEmriPolietilenKaplamaKontrolParametreleriForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
