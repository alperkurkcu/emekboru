/**
 * 
 */
package com.emekboru.beans.istakipformu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.istakipformu.IsTakipFormuKumlama;
import com.emekboru.jpa.istakipformu.IsTakipFormuKumlamaBitinceIslemler;
import com.emekboru.jpa.istakipformu.IsTakipFormuKumlamaOnceIslemler;
import com.emekboru.jpa.istakipformu.IsTakipFormuKumlamaSonraIslemler;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.istakipformu.IsTakipFormuKumlamaBitinceIslemlerManager;
import com.emekboru.jpaman.istakipformu.IsTakipFormuKumlamaManager;
import com.emekboru.jpaman.istakipformu.IsTakipFormuKumlamaOnceIslemlerManager;
import com.emekboru.jpaman.istakipformu.IsTakipFormuKumlamaSonraIslemlerManager;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isTakipFormuKumlamaBean")
@ViewScoped
public class IsTakipFormuKumlamaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private SalesItem selectedItem = new SalesItem();

	private List<IsTakipFormuKumlama> allIsTakipFormuKumlamaList = new ArrayList<IsTakipFormuKumlama>();
	private IsTakipFormuKumlama isTakipFormuKumlamaForm = new IsTakipFormuKumlama();

	private IsTakipFormuKumlama newIsTakip = new IsTakipFormuKumlama();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private IsTakipFormuKumlamaOnceIslemler selectedIsTakipFormuKumlamaOnceIslemler = new IsTakipFormuKumlamaOnceIslemler();
	private IsTakipFormuKumlamaSonraIslemler selectedIsTakipFormuKumlamaSonraIslemler = new IsTakipFormuKumlamaSonraIslemler();
	private IsTakipFormuKumlamaBitinceIslemler selectedIsTakipFormuKumlamaBitinceIslemler = new IsTakipFormuKumlamaBitinceIslemler();
	private IsTakipFormuKumlamaOnceIslemler newIsTakipFormuKumlamaOnceIslemler = new IsTakipFormuKumlamaOnceIslemler();
	private IsTakipFormuKumlamaSonraIslemler newIsTakipFormuKumlamaSonraIslemler = new IsTakipFormuKumlamaSonraIslemler();
	private IsTakipFormuKumlamaBitinceIslemler newIsTakipFormuKumlamaBitinceIslemler = new IsTakipFormuKumlamaBitinceIslemler();
	private IsTakipFormuKumlama selectedIsTakipFormuKumlama = new IsTakipFormuKumlama();

	private Integer icDisForm;

	public void addFromIsEmri(Integer icDisType, SalesItem salesItem) {

		System.out.println("isTakipFormuKumlamaBean.addFromIsEmri()");

		try {
			IsTakipFormuKumlamaManager manager = new IsTakipFormuKumlamaManager();

			newIsTakip.setIcDis(icDisType);
			newIsTakip.setSalesItem(salesItem);
			newIsTakip.setEklemeZamani(UtilInsCore.getTarihZaman());
			newIsTakip.setEkleyenEmployee(userBean.getUser());

			newIsTakip.getIsTakipFormuKumlamaOnceIslemler().setEklemeZamani(
					UtilInsCore.getTarihZaman());
			newIsTakip.getIsTakipFormuKumlamaOnceIslemler().setEkleyenEmployee(
					userBean.getUser());

			newIsTakip.getIsTakipFormuKumlamaSonraIslemler().setEklemeZamani(
					UtilInsCore.getTarihZaman());
			newIsTakip.getIsTakipFormuKumlamaSonraIslemler()
					.setEkleyenEmployee(userBean.getUser());

			newIsTakip.getIsTakipFormuKumlamaBitinceIslemler().setEklemeZamani(
					UtilInsCore.getTarihZaman());
			newIsTakip.getIsTakipFormuKumlamaBitinceIslemler()
					.setEkleyenEmployee(userBean.getUser());

			manager.enterNew(newIsTakip);
			newIsTakip = new IsTakipFormuKumlama();

			FacesContext context = FacesContext.getCurrentInstance();
			if (icDisType == 1) {
				context.addMessage(null, new FacesMessage(
						"İÇ KUMLAMA İŞ TAKİP FORMU EKLENMİŞTİR!"));
			} else if (icDisType == 0) {
				context.addMessage(null, new FacesMessage(
						"DIŞ KUMLAMA İŞ TAKİP FORMU EKLENMİŞTİR!"));
			}

		} catch (Exception e) {
			System.out.println("isTakipFormuKumlamaBean.addFromIsEmri-HATA");
		}
	}

	public void loadSelectedTakipForm(Integer icDis) {

		reset();
		icDisForm = icDis;
		loadSelectedIsTakipFormuKumlamaOnceIslemler();
		loadSelectedIsTakipFormuKumlamaSonraIslemler();
		loadSelectedIsTakipFormuKumlamaBitinceIslemler();
	}

	@SuppressWarnings("static-access")
	public void loadReplySelectedTakipForm() {
		IsTakipFormuKumlamaManager takipManager = new IsTakipFormuKumlamaManager();
		if (takipManager.getAllIsTakipFormuKumlama(selectedItem.getItemId(),
				icDisForm).size() > 0) {
			selectedIsTakipFormuKumlama = takipManager
					.getAllIsTakipFormuKumlama(selectedItem.getItemId(),
							icDisForm).get(0);
		} else {
			FacesContext context = FacesContext.getCurrentInstance();
			if (icDisForm == 1) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"İÇ KUMLAMA İŞ EMRİ EKLENMEMİŞ, LÜTFEN ÖNCE İŞ EMİRLERİNİ EKLEYİNİZ!",
								null));
			} else if (icDisForm == 0) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"DIŞ KUMLAMA İŞ EMRİ EKLENMEMİŞ, LÜTFEN ÖNCE İŞ EMİRLERİNİ EKLEYİNİZ!",
								null));
			}
			return;
		}
	}

	public void reset() {

		selectedIsTakipFormuKumlamaOnceIslemler = new IsTakipFormuKumlamaOnceIslemler();
		selectedIsTakipFormuKumlamaSonraIslemler = new IsTakipFormuKumlamaSonraIslemler();
		selectedIsTakipFormuKumlamaBitinceIslemler = new IsTakipFormuKumlamaBitinceIslemler();
	}

	public void loadSelectedIsTakipFormuKumlamaOnceIslemler() {
		loadReplySelectedTakipForm();
		selectedIsTakipFormuKumlamaOnceIslemler = selectedIsTakipFormuKumlama
				.getIsTakipFormuKumlamaOnceIslemler();
	}

	public void loadSelectedIsTakipFormuKumlamaSonraIslemler() {
		loadReplySelectedTakipForm();
		selectedIsTakipFormuKumlamaSonraIslemler = selectedIsTakipFormuKumlama
				.getIsTakipFormuKumlamaSonraIslemler();
	}

	public void loadSelectedIsTakipFormuKumlamaBitinceIslemler() {
		loadReplySelectedTakipForm();
		selectedIsTakipFormuKumlamaBitinceIslemler = selectedIsTakipFormuKumlama
				.getIsTakipFormuKumlamaBitinceIslemler();
	}

	public void updateSelectedIsTakipFormuKumlamaOnceIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuKumlamaOnceIslemlerManager onceManager = new IsTakipFormuKumlamaOnceIslemlerManager();

		selectedIsTakipFormuKumlamaOnceIslemler.setGuncellemeZamani(UtilInsCore
				.getTarihZaman());
		selectedIsTakipFormuKumlamaOnceIslemler.setGuncelleyenEmployee(userBean
				.getUser());
		selectedIsTakipFormuKumlamaOnceIslemler
				.setIsTakipFormuKumlama(selectedIsTakipFormuKumlama);

		onceManager.updateEntity(selectedIsTakipFormuKumlamaOnceIslemler);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"İÇ KUMLAMADA İŞE BAŞLAMADAN YAPILACAK İŞLEMLER BAŞARIYLA TANIMLANMIŞTIR!"));

	}

	public void updateSelectedIsTakipFormuKumlamaSonraIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuKumlamaSonraIslemlerManager sonraManager = new IsTakipFormuKumlamaSonraIslemlerManager();

		selectedIsTakipFormuKumlamaSonraIslemler
				.setGuncellemeZamani(UtilInsCore.getTarihZaman());
		selectedIsTakipFormuKumlamaSonraIslemler
				.setGuncelleyenEmployee(userBean.getUser());

		sonraManager.updateEntity(selectedIsTakipFormuKumlamaSonraIslemler);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"İÇ KUMLAMADA AYAR BİTTİKTEN SONRA YAPILACAK İŞLEMLER BAŞARIYLA TANIMLANMIŞTIR!"));

	}

	public void updateSelectedIsTakipFormuKumlamaBitinceIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuKumlamaBitinceIslemlerManager bitinceManager = new IsTakipFormuKumlamaBitinceIslemlerManager();

		selectedIsTakipFormuKumlamaBitinceIslemler
				.setGuncellemeZamani(UtilInsCore.getTarihZaman());
		selectedIsTakipFormuKumlamaBitinceIslemler
				.setGuncelleyenEmployee(userBean.getUser());

		bitinceManager.updateEntity(selectedIsTakipFormuKumlamaBitinceIslemler);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"İÇ KUMLAMADA ÜRETİM BİTTİKTEN SONRA YAPILACAK İŞLEMLER BAŞARIYLA TANIMLANMIŞTIR!"));

	}

	public void deleteSelectedIsTakipFormuKumlamaOnceIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuKumlamaOnceIslemlerManager onceManager = new IsTakipFormuKumlamaOnceIslemlerManager();

		newIsTakipFormuKumlamaOnceIslemler
				.setId(selectedIsTakipFormuKumlamaOnceIslemler.getId());
		newIsTakipFormuKumlamaOnceIslemler
				.setEklemeZamani(selectedIsTakipFormuKumlamaOnceIslemler
						.getEklemeZamani());
		newIsTakipFormuKumlamaOnceIslemler
				.setEkleyenEmployee(selectedIsTakipFormuKumlamaOnceIslemler
						.getEkleyenEmployee());

		selectedIsTakipFormuKumlamaOnceIslemler = new IsTakipFormuKumlamaOnceIslemler();

		selectedIsTakipFormuKumlamaOnceIslemler = newIsTakipFormuKumlamaOnceIslemler;

		onceManager.updateEntity(selectedIsTakipFormuKumlamaOnceIslemler);

		newIsTakipFormuKumlamaOnceIslemler = new IsTakipFormuKumlamaOnceIslemler();
		selectedIsTakipFormuKumlamaOnceIslemler = new IsTakipFormuKumlamaOnceIslemler();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"İÇ KUMLAMADA İŞE BAŞLAMADAN YAPILACAK İŞLEMLER BAŞARIYLA SİLİNMİŞTİR!"));

	}

	public void deleteSelectedIsTakipFormuKumlamaSonraIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuKumlamaSonraIslemlerManager sonraManager = new IsTakipFormuKumlamaSonraIslemlerManager();

		newIsTakipFormuKumlamaSonraIslemler
				.setId(selectedIsTakipFormuKumlamaSonraIslemler.getId());
		newIsTakipFormuKumlamaSonraIslemler
				.setEklemeZamani(selectedIsTakipFormuKumlamaSonraIslemler
						.getEklemeZamani());
		newIsTakipFormuKumlamaSonraIslemler
				.setEkleyenEmployee(selectedIsTakipFormuKumlamaSonraIslemler
						.getEkleyenEmployee());

		selectedIsTakipFormuKumlamaSonraIslemler = new IsTakipFormuKumlamaSonraIslemler();

		selectedIsTakipFormuKumlamaSonraIslemler = newIsTakipFormuKumlamaSonraIslemler;

		sonraManager.updateEntity(selectedIsTakipFormuKumlamaSonraIslemler);

		newIsTakipFormuKumlamaSonraIslemler = new IsTakipFormuKumlamaSonraIslemler();
		selectedIsTakipFormuKumlamaSonraIslemler = new IsTakipFormuKumlamaSonraIslemler();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"İÇ KUMLAMADA AYAR BİTTİKTEN SONRA YAPILACAK İŞLEMLER BAŞARIYLA SİLİNMİŞTİR!"));

	}

	public void deleteSelectedIsTakipFormuKumlamaBitinceIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuKumlamaBitinceIslemlerManager bitinceManager = new IsTakipFormuKumlamaBitinceIslemlerManager();

		newIsTakipFormuKumlamaBitinceIslemler
				.setId(selectedIsTakipFormuKumlamaBitinceIslemler.getId());
		newIsTakipFormuKumlamaBitinceIslemler
				.setEklemeZamani(selectedIsTakipFormuKumlamaBitinceIslemler
						.getEklemeZamani());
		newIsTakipFormuKumlamaBitinceIslemler
				.setEkleyenEmployee(selectedIsTakipFormuKumlamaBitinceIslemler
						.getEkleyenEmployee());

		selectedIsTakipFormuKumlamaBitinceIslemler = new IsTakipFormuKumlamaBitinceIslemler();

		selectedIsTakipFormuKumlamaBitinceIslemler = newIsTakipFormuKumlamaBitinceIslemler;

		bitinceManager.updateEntity(selectedIsTakipFormuKumlamaBitinceIslemler);

		newIsTakipFormuKumlamaBitinceIslemler = new IsTakipFormuKumlamaBitinceIslemler();
		selectedIsTakipFormuKumlamaBitinceIslemler = new IsTakipFormuKumlamaBitinceIslemler();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"İÇ KUMLAMADA ÜRETİM BİTİNCE YAPILACAK İŞLEMLER BAŞARIYLA SİLİNMİŞTİR!"));

	}

	// setters getters

	public SalesItem getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(SalesItem selectedItem) {
		this.selectedItem = selectedItem;
	}

	public List<IsTakipFormuKumlama> getAllIsTakipFormuKumlamaList() {
		return allIsTakipFormuKumlamaList;
	}

	public void setAllIsTakipFormuKumlamaList(
			List<IsTakipFormuKumlama> allIsTakipFormuKumlamaList) {
		this.allIsTakipFormuKumlamaList = allIsTakipFormuKumlamaList;
	}

	public IsTakipFormuKumlama getIsTakipFormuKumlamaForm() {
		return isTakipFormuKumlamaForm;
	}

	public void setIsTakipFormuKumlamaForm(
			IsTakipFormuKumlama isTakipFormuKumlamaForm) {
		this.isTakipFormuKumlamaForm = isTakipFormuKumlamaForm;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public IsTakipFormuKumlama getNewIsTakip() {
		return newIsTakip;
	}

	public void setNewIsTakip(IsTakipFormuKumlama newIsTakip) {
		this.newIsTakip = newIsTakip;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public IsTakipFormuKumlamaOnceIslemler getSelectedIsTakipFormuKumlamaOnceIslemler() {
		return selectedIsTakipFormuKumlamaOnceIslemler;
	}

	public void setSelectedIsTakipFormuKumlamaOnceIslemler(
			IsTakipFormuKumlamaOnceIslemler selectedIsTakipFormuKumlamaOnceIslemler) {
		this.selectedIsTakipFormuKumlamaOnceIslemler = selectedIsTakipFormuKumlamaOnceIslemler;
	}

	public IsTakipFormuKumlamaSonraIslemler getSelectedIsTakipFormuKumlamaSonraIslemler() {
		return selectedIsTakipFormuKumlamaSonraIslemler;
	}

	public void setSelectedIsTakipFormuKumlamaSonraIslemler(
			IsTakipFormuKumlamaSonraIslemler selectedIsTakipFormuKumlamaSonraIslemler) {
		this.selectedIsTakipFormuKumlamaSonraIslemler = selectedIsTakipFormuKumlamaSonraIslemler;
	}

	public IsTakipFormuKumlamaBitinceIslemler getSelectedIsTakipFormuKumlamaBitinceIslemler() {
		return selectedIsTakipFormuKumlamaBitinceIslemler;
	}

	public void setSelectedIsTakipFormuKumlamaBitinceIslemler(
			IsTakipFormuKumlamaBitinceIslemler selectedIsTakipFormuKumlamaBitinceIslemler) {
		this.selectedIsTakipFormuKumlamaBitinceIslemler = selectedIsTakipFormuKumlamaBitinceIslemler;
	}

	public IsTakipFormuKumlama getSelectedIsTakipFormuKumlama() {
		return selectedIsTakipFormuKumlama;
	}

	public void setSelectedIsTakipFormuKumlama(
			IsTakipFormuKumlama selectedIsTakipFormuKumlama) {
		this.selectedIsTakipFormuKumlama = selectedIsTakipFormuKumlama;
	}

}
