package com.emekboru.beans.test.kaplama;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaSertlikSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaSertlikSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaplamaSertlikSonucBean")
@ViewScoped
public class TestKaplamaSertlikSonucBean {

	private TestKaplamaSertlikSonuc testKaplamaSertlikSonucForm = new TestKaplamaSertlikSonuc();
	private List<TestKaplamaSertlikSonuc> allKaplamaSertlikSonucList = new ArrayList<TestKaplamaSertlikSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	float ortalama = 0F;

	public void addOrUpdateKaplamaSertlikTestSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaSertlikSonucManager manager = new TestKaplamaSertlikSonucManager();

		if (testKaplamaSertlikSonucForm.getId() == null) {

			testKaplamaSertlikSonucForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			testKaplamaSertlikSonucForm.setEkleyenEmployee(userBean.getUser());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaSertlikSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaSertlikSonucForm.setBagliTestId(bagliTest);
			testKaplamaSertlikSonucForm.setBagliGlobalId(bagliTest);

			ortalamaBul();

			manager.enterNew(testKaplamaSertlikSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaSertlikSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaSertlikSonucForm.setGuncelleyenEmployee(userBean
					.getUser());
			ortalamaBul();
			manager.updateEntity(testKaplamaSertlikSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);
	}

	private void ortalamaBul() {

		ortalama = (testKaplamaSertlikSonucForm.getSertlikDegeriA()
				.floatValue()
				+ testKaplamaSertlikSonucForm.getSertlikDegeriB().floatValue()
				+ testKaplamaSertlikSonucForm.getSertlikDegeriC().floatValue()
				+ testKaplamaSertlikSonucForm.getSertlikDegeriD().floatValue() + testKaplamaSertlikSonucForm
				.getSertlikDegeriE().floatValue()) / 5;
		testKaplamaSertlikSonucForm.setOrtalamaSertlik(BigDecimal
				.valueOf(ortalama));
	}

	public void addKaplamaSertlikTest() {

		testKaplamaSertlikSonucForm = new TestKaplamaSertlikSonuc();
		updateButtonRender = false;
	}

	public void kaplamaSertlikListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allKaplamaSertlikSonucList = TestKaplamaSertlikSonucManager
				.getAllKaplamaSertlikSonucTest(globalId, pipeId);
		testKaplamaSertlikSonucForm = new TestKaplamaSertlikSonuc();
	}

	public void deleteTestKaplamaSertlikSonuc() {

		bagliTestId = testKaplamaSertlikSonucForm.getBagliTestId().getId();

		if (testKaplamaSertlikSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaSertlikSonucManager manager = new TestKaplamaSertlikSonucManager();
		manager.delete(testKaplamaSertlikSonucForm);
		testKaplamaSertlikSonucForm = new TestKaplamaSertlikSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setters getters
	public TestKaplamaSertlikSonuc getTestKaplamaSertlikSonucForm() {
		return testKaplamaSertlikSonucForm;
	}

	public void setTestKaplamaSertlikSonucForm(
			TestKaplamaSertlikSonuc testKaplamaSertlikSonucForm) {
		this.testKaplamaSertlikSonucForm = testKaplamaSertlikSonucForm;
	}

	public List<TestKaplamaSertlikSonuc> getAllKaplamaSertlikSonucList() {
		return allKaplamaSertlikSonucList;
	}

	public void setAllKaplamaSertlikSonucList(
			List<TestKaplamaSertlikSonuc> allKaplamaSertlikSonucList) {
		this.allKaplamaSertlikSonucList = allKaplamaSertlikSonucList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
