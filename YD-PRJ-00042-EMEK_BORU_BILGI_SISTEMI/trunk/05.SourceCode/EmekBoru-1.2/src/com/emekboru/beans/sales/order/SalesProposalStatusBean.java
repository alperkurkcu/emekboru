package com.emekboru.beans.sales.order;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.sales.order.SalesProposalStatus;
import com.emekboru.jpaman.sales.order.SalesProposalStatusManager;

@ManagedBean(name = "salesProposalStatusBean")
@ViewScoped
public class SalesProposalStatusBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4616107632599940175L;
	private List<SalesProposalStatus> statusList;

	public SalesProposalStatusBean() {

		SalesProposalStatusManager manager = new SalesProposalStatusManager();
		statusList = manager.findAll(SalesProposalStatus.class);
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public List<SalesProposalStatus> getStatusList() {
		return statusList;
	}

	public void setStatusList(List<SalesProposalStatus> statusList) {
		this.statusList = statusList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}