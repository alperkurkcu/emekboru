package com.emekboru.beans.test;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.DestructiveTestsSpec;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.TestCentikDarbeSonuc;
import com.emekboru.jpaman.DestructiveTestsSpecManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.TestCentikDarbeSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testCentikDarbeSonucBean")
@ViewScoped
public class TestCentikDarbeSonucBean {

	private List<TestCentikDarbeSonuc> allCentikDarbeSonucList = new ArrayList<TestCentikDarbeSonuc>();
	private TestCentikDarbeSonuc testCentikDarbeSonucForm = new TestCentikDarbeSonuc();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;
	private boolean updateButtonRender;

	Integer prmGlobalId = null;
	Integer prmTestId = null;
	Integer prmPipeId = null;

	private Pipe pipe = null;
	private DestructiveTestsSpec bagliTest = null;
	private String sampleNo = null;

	public void sampleNoOlustur() {
		Integer count = 0;
		TestCentikDarbeSonucManager manager = new TestCentikDarbeSonucManager();
		count = manager.getAllTestCentikDarbeSonuc(prmGlobalId, prmPipeId)
				.size() + 1;
		sampleNo = bagliTest.getCode() + pipe.getPipeIndex() + "-" + count;
	}

	// methods

	public void addCentikDarbeTestSonuc(ActionEvent e) {
		UICommand cmd = (UICommand) e.getComponent();
		prmGlobalId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");
		prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestCentikDarbeSonucManager manager = new TestCentikDarbeSonucManager();

		if (testCentikDarbeSonucForm.getId() == null) {

			testCentikDarbeSonucForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			testCentikDarbeSonucForm.setEkleyenKullanici(userBean.getUser()
					.getId());

			pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testCentikDarbeSonucForm.setPipe(pipe);

			bagliTest = new DestructiveTestsSpecManager().loadObject(
					DestructiveTestsSpec.class, prmTestId);
			testCentikDarbeSonucForm.setBagliTestId(bagliTest);
			testCentikDarbeSonucForm.setBagliGlobalId(bagliTest);

			sampleNoOlustur();
			testCentikDarbeSonucForm.setSampleNo(sampleNo);

			manager.enterNew(testCentikDarbeSonucForm);

			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 2, true);
			DestructiveTestsSpecManager desMan = new DestructiveTestsSpecManager();
			desMan.testSonucKontrol(prmTestId, true);

			fillTestList(prmGlobalId, prmPipeId);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			testCentikDarbeSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testCentikDarbeSonucForm.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			manager.updateEntity(testCentikDarbeSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");

		}
		fillTestList(prmGlobalId, prmPipeId);
	}

	public void addCentikDarbeTest() {
		testCentikDarbeSonucForm = new TestCentikDarbeSonuc();
	}

	public void centikDarbeListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		TestCentikDarbeSonucManager manager = new TestCentikDarbeSonucManager();
		allCentikDarbeSonucList = manager.getAllTestCentikDarbeSonuc(globalId,
				pipeId);

	}

	public void deleteTestCentikDarbeSonuc() {

		Integer pipeId = testCentikDarbeSonucForm.getPipe().getPipeId();
		Integer globalId = testCentikDarbeSonucForm.getBagliGlobalId()
				.getGlobalId();

		if (testCentikDarbeSonucForm == null
				|| testCentikDarbeSonucForm.getId() <= 0) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestCentikDarbeSonucManager manager = new TestCentikDarbeSonucManager();
		manager.delete(testCentikDarbeSonucForm);

		// sonuc var m� false
		DestructiveTestsSpecManager desMan = new DestructiveTestsSpecManager();
		desMan.testSonucKontrol(testCentikDarbeSonucForm.getBagliTestId()
				.getTestId(), false);

		testCentikDarbeSonucForm = new TestCentikDarbeSonuc();
		fillTestList(globalId, pipeId);
		FacesContextUtils.addWarnMessage("TestDeleteMessage");

	}

	public List<TestCentikDarbeSonuc> getAllCentikDarbeSonucList() {
		return allCentikDarbeSonucList;
	}

	public void setAllCentikDarbeSonucList(
			List<TestCentikDarbeSonuc> allCentikDarbeSonucList) {
		this.allCentikDarbeSonucList = allCentikDarbeSonucList;
	}

	public TestCentikDarbeSonuc getTestCentikDarbeSonucForm() {
		return testCentikDarbeSonucForm;
	}

	public void setTestCentikDarbeSonucForm(
			TestCentikDarbeSonuc testCentikDarbeSonucForm) {
		this.testCentikDarbeSonucForm = testCentikDarbeSonucForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
