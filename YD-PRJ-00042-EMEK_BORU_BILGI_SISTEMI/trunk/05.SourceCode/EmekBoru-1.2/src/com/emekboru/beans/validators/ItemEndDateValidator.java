package com.emekboru.beans.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.primefaces.component.calendar.Calendar;

@FacesValidator("itemEndDateValidator")
public class ItemEndDateValidator implements Validator {

	private static final String dateMatchFailureMessage = "The date format does not match the given dd/mm/yyyy acceptable one!";
	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		
		System.out.println("this is the value of calendar : "+((Calendar)component).getSubmittedValue().toString());
		String dateString = ((Calendar)component).getSubmittedValue().toString();
		Pattern datePattern = Pattern.compile("[0-9][0-9]\\W[0-9][0-9]\\W[0-9][0-9][0-9][0-9]");
		Matcher dateMatcher = datePattern.matcher(dateString);

		if(!dateMatcher.matches())
		{
			System.out.println("date does not match");	
			FacesMessage msg = new FacesMessage(dateMatchFailureMessage,dateMatchFailureMessage);
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
		
	}

}
