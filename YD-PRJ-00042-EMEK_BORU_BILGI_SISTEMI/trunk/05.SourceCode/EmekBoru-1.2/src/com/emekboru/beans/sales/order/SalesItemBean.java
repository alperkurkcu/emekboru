package com.emekboru.beans.sales.order;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.xml.xpath.XPathExpressionException;

import org.primefaces.component.accordionpanel.AccordionPanel;

import com.emekboru.beans.sales.SalesMenuBean;
import com.emekboru.jpa.rulo.Rulo;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpa.sales.order.SalesKamyonTirYuklemeKapasitesi;
import com.emekboru.jpaman.rulo.RuloManager;
import com.emekboru.jpaman.sales.order.SalesItemManager;
import com.emekboru.jpaman.sales.order.SalesKamyonTirYuklemeKapasitesiManager;
import com.emekboru.utils.ExchangeRateUtil;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "salesItemBean")
@ViewScoped
public class SalesItemBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7506162397277463796L;

	@ManagedProperty(value = "#{salesProposalBean}")
	private SalesProposalBean proposalBean;

	@ManagedProperty(value = "#{salesOrderBean}")
	private SalesOrderBean orderBean;

	@ManagedProperty(value = "#{salesMenuBean}")
	private SalesMenuBean salesMenuBean;

	private List<SalesItem> itemsOfSelectedProposal;
	private List<SalesItem> itemsOfSelectedOrder;

	private SalesItem newItem = new SalesItem();
	private SalesItem selectedItem;

	private List<SalesItem> itemList;
	private List<SalesItem> filteredList;

	private double result;

	private DecimalFormat decimalFormat = new DecimalFormat("#.####");
	private List<Rulo> allRulos = new ArrayList<Rulo>();

	private Rulo selectedRulo;
	private Rulo[] selectedRulos;

	private Date reservationDate;

	private List<SalesKamyonTirYuklemeKapasitesi> kapasites;
	private List<SalesKamyonTirYuklemeKapasitesi> selectedKapasites;

	private double maxTonaj = 25000;

	// jsfdeki accordionpanelin ozelliklerini koddan değiştirmek için
	AccordionPanel accordionPanel = new AccordionPanel();

	double boruHacim = 0;

	double tirTonaj = 0;
	double tirAdedi = 0;
	double selectedKapasite = 0;
	double tirUcreti = 0;
	double toplamFCA = 0;
	double birimFCA = 0;
	double tirUcretiDoviz = 0;

	double birimFOB = 0;
	double birimFOBDoviz = 0;

	double birimCFR = 0;
	double birimCFRDoviz = 0;

	public SalesItemBean() {

		newItem = new SalesItem();
		selectedItem = new SalesItem();
		kapasites = new ArrayList<SalesKamyonTirYuklemeKapasitesi>();
		selectedKapasites = new ArrayList<SalesKamyonTirYuklemeKapasitesi>();
	}

	@PostConstruct
	public void load() {
		System.out.println("SalesItemBean.load()");

		ExchangeRateUtil exchangeRateUtil = new ExchangeRateUtil();

		Double parityDollarLira;
		Double parityEuroLira;
		Double parityEuroDollar;

		try {
			parityDollarLira = Double.parseDouble(exchangeRateUtil
					.getExchangeRate("USD", "ForexSelling"));
			parityEuroLira = Double.parseDouble(exchangeRateUtil
					.getExchangeRate("EUR", "ForexSelling"));

			parityEuroDollar = Math.round(parityEuroLira / parityDollarLira
					* 1e4) / 1e4;

		} catch (XPathExpressionException e) {
			parityDollarLira = (double) 0;
			parityEuroLira = (double) 0;
			parityEuroDollar = (double) 0;
			e.printStackTrace();

		}

		newItem.setParityDollar(parityDollarLira);
		newItem.setParityEuro(parityEuroLira);
		newItem.setParityEuroDollar(parityEuroDollar);

		try {
			SalesItemManager manager = new SalesItemManager();

			itemList = manager.findAllOrderByDESCASC(SalesItem.class,
					"proposal.proposalId", "c.itemNo");

			selectedItem = itemList.get(0);

			accordionPanel.setActiveIndex("0");
		} catch (Exception e) {
			System.out.println("SalesItemBean:" + e.toString());
		}

		confirmAllocationOfRulos();
	}

	public void fillAvailableRuloList() {

		RuloManager ruloManager = new RuloManager();
		allRulos = ruloManager.getAvailableRulosForSpiralMachine(selectedItem);
	}

	public void allocateRulos() {

		SalesItemManager salesItemManager = new SalesItemManager();

		if (this.selectedItem.getItemId() == 0) {
			FacesContextUtils
					.addPlainErrorMessage("Lütfen sipariş Kalemi seçiniz!");
			return;
		}

		try {

			for (Rulo rulo : selectedRulos) {

				rulo.setReservedSalesItem(selectedItem);
				rulo.setReservationDate(reservationDate);
				rulo.setRuloDurum(10);
				rulo.setRuloGenelDurum(Rulo.RULO_GENEL_DURUM_BEKLEMEDE);

				selectedItem.getReservedRulos().add(rulo);
			}

			salesItemManager.updateEntity(selectedItem);

			FacesContextUtils.addInfoMessage("SubmitMessage");

		} catch (Exception e) {
			System.out.println("SalesItemBean:" + e.toString());
		}
	}

	public void unallocateRulos() {

		// SalesItemManager salesItemManager = new SalesItemManager();

		RuloManager ruloManager = new RuloManager();

		if (this.selectedItem == null) {
			FacesContextUtils
					.addPlainErrorMessage("Lütfen sipariş Kalemi seçiniz!");
			return;
		}

		try {

			for (Rulo rulo : selectedRulos) {

				rulo.setReservedSalesItem(null);
				rulo.setReservationDate(null);
				rulo.setRuloDurum(6);
				rulo.setRuloGenelDurum(Rulo.RULO_GENEL_DURUM_BEKLEMEDE);

				ruloManager.updateEntity(rulo);
				// selectedItem.getReservedRulos().add(rulo);
			}

			// salesItemManager.updateEntity(selectedItem);

			FacesContextUtils.addInfoMessage("SubmitMessage");

		} catch (Exception e) {
			System.out.println("SalesItemBean:" + e.toString());
		}
	}

	private Date clearTimeInfo(Date date) {
		Calendar cal = Calendar.getInstance(); // locale-specific
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		long time = cal.getTimeInMillis();

		return new Date(time);
	}

	public void confirmAllocationOfRulos() {

		RuloManager ruloManager = new RuloManager();

		for (Rulo rulo : this.getAllRulos()) {

			if (rulo.getReservationDate() == null)
				continue;

			if (rulo.getReservedSalesItem() != null
					&& clearTimeInfo(rulo.getReservationDate()).before(
							clearTimeInfo(new Date()))) {

				rulo.setReservedSalesItem(null);
				rulo.setReservationDate(null);
				rulo.setRuloDurum(6);
				rulo.setRuloGenelDurum(Rulo.RULO_GENEL_DURUM_BEKLEMEDE);

				ruloManager.updateEntity(rulo);
			}
		}
	}

	public void add() {
		System.out.println("SalesItemBean.add()");

		try {
			SalesItemManager manager = new SalesItemManager();
			if (newItem.getDiameter() == 0.0) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"DIŞ ÇAP BOŞ GEÇİLEMEZ, KAYDETME HATASI!", null));
				return;
			}
			if (newItem.getThickness() == 0.0) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"ET KALINLIĞI BOŞ GEÇİLEMEZ, KAYDETME HATASI!", null));
				return;
			}
			if (newItem.getPipeLength() == 0.0) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"BORU BOYU BOŞ GEÇİLEMEZ, KAYDETME HATASI!", null));
				return;
			}
			if (newItem.getProposal() != null) {

				// A.K. itemNo set edilme islemi
				newItem.setItemNo(manager.findItemNo(
						newItem.getProposal().getProposalId()).toString());

				// itemList.add(newItem);
				manager.enterNew(newItem);

				newItem.setItemId(0);

				this.completeItemNumber();

				itemList = manager.findAllOrderByDESCASC(SalesItem.class,
						"proposal.proposalId", "c.itemNo");

				FacesContextUtils.addInfoMessage("SubmitMessage");
			}
		} catch (Exception e) {
			System.out.println("SalesItemBean:" + e.toString());
		}
	}

	public void update() {
		System.out.println("SalesItemBean.update()");

		try {
			SalesItemManager manager = new SalesItemManager();
			manager.updateEntity(selectedItem);

			FacesContextUtils.addInfoMessage("UpdateItemMessage");
		} catch (Exception e) {
			System.out.println("SalesItemBean:" + e.toString());
		}
	}

	public void delete() {
		System.out.println("SalesItemBean.delete()");

		if (selectedItem.getProposal().getStatus().getStatusId() != 3) {

			try {
				SalesItemManager manager = new SalesItemManager();
				manager.delete(selectedItem);

				itemList.remove(selectedItem);
				selectedItem = itemList.get(0);
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"SİPARİŞ KALEMİ BAŞARIYLA SİLİNMİŞTİR!", null));
			} catch (Exception e) {
				System.out.println("SalesItemBean:" + e.toString());
			}
		} else {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL,
					"SİPARİŞE DÖNDÜRÜLMÜŞ KALEM SİLİNEMEZ!", null));
		}
	}

	/*---CALCULATIONS--*/
	public void calculateSelectedItem() {
		this.calculateTheoricUnitWeight(selectedItem);
		// this.calculateTransportationUnitPrice(selectedItem);
		this.calculateUnitPriceM(selectedItem);
		this.calculateUnitPriceTon(selectedItem);
		this.calculateTotalPrice(selectedItem);

		this.calculateTotalTonnage(selectedItem);
		this.calculateFCA(selectedItem);
		this.calculateFOBOpen(selectedItem);
		// this.calculateCFROpen(selectedItem);
	}

	public void calculateNewItem() {
		this.calculateTheoricUnitWeight(newItem);
		// this.calculateTransportationUnitPrice(newItem);
		this.calculateUnitPriceM(newItem);
		this.calculateUnitPriceTon(newItem);
		this.calculateTotalPrice(newItem);

		this.calculateTotalTonnage(newItem);
		this.calculateFCA(selectedItem);
		this.calculateFOBOpen(selectedItem);
		// this.calculateCFROpen(selectedItem);
	}

	/*---PURE CALCULATIONS--*/
	public void calculateTheoricUnitWeight(SalesItem theItem) {
		System.out.println("SalesItemBean.calculateTheoricUnitWeight()");

		try {
			if (theItem.getDiameter() > theItem.getThickness()) {
				result = ((int) (theItem.getDiameter() * 100) - (int) (theItem
						.getThickness() * 100))
						* (int) (theItem.getThickness() * 100) * 0.02466 / 100;
				result = (int) result;
				theItem.setUnitWeight(result / 100);

			} else {
				theItem.setUnitWeight(0);
			}
		} catch (Exception ex) {
			System.out.println("SalesItemBean:" + ex.toString());
			theItem.setTransportationUnitPrice(0);
		}
	}

	public void calculateTransportationUnitPrice(SalesItem theItem) {
		System.out.println("SalesItemBean.calculateTransportationUnitPrice()");

		try {
			result = (int) (theItem.getTransportationType1() * 100)
					+ (int) (theItem.getTransportationType2() * 100)
					+ (int) (theItem.getTransportationType3() * 100)
					+ (int) (theItem.getTransportationType4() * 100)
					+ (int) (theItem.getTransportationType5() * 100)
					+ (int) (theItem.getTransportationType6() * 100)
					+ (int) (theItem.getTransportationType7() * 100)
					+ (int) (theItem.getTransportationType8() * 100);

			theItem.setTransportationUnitPrice(result / 100);
		} catch (Exception ex) {
			System.out.println("SalesItemBean:" + ex.toString());
			theItem.setTransportationUnitPrice(0);
		}
	}

	public void calculateUnitPriceM(SalesItem theItem) {
		System.out.println("SalesItemBean.calculateUnitPriceM()");

		Double unitWeight = theItem.getUnitWeight();
		Double extraPrice = theItem.getExtraPrice();
		Double diameter = theItem.getDiameter();
		Double platePrice = theItem.getPlate();
		Double plateExtPrice = Double.parseDouble("0");
		Double plateTransportation = theItem.getPlateTransportation();
		Double wastagePercentage = theItem.getWastagePercentage();
		Double labor = theItem.getLabor();
		Double covering = theItem.getCovering();

		try {
			result = (platePrice + plateExtPrice + plateTransportation
					+ (platePrice + plateExtPrice + plateTransportation)
					* wastagePercentage / 100.0 + labor)
					* unitWeight
					/ 1000.0
					+ covering
					* (diameter / 1000.0)
					* 3.1416 + extraPrice;

			theItem.setUnitPriceM(round(result, 2));
		} catch (Exception ex) {
			System.out.println("SalesItemBean:" + ex.toString());
			theItem.setUnitPriceM(0);
		}
	}

	public void calculateUnitPriceTon(SalesItem theItem) {
		System.out.println("SalesItemBean.calculateUnitPriceTon()");

		try {
			if (theItem.getUnitWeight() != 0) {
				result = theItem.getUnitPriceM() / theItem.getUnitWeight();

				theItem.setUnitPriceTon(round(result, 2));
			}
		} catch (Exception ex) {
			System.out.println("SalesItemBean:" + ex.toString());
			theItem.setUnitPriceTon(0);
		}
	}

	public void calculateTotalTonnage(SalesItem item) {

		System.out.println("SalesItemBean.calculateTotalTonnage()");

		try {
			result = item.getTotalLengthM() * item.getUnitWeight();

			item.setTotalTonnage(round(result, 2));
		} catch (Exception ex) {
			ex.printStackTrace();
			item.setTotalTonnage(0);
		}
	}

	public void calculateTotalPrice(SalesItem theItem) {
		System.out.println("SalesItemBean.calculateTotalPrice()");

		try {
			result = (theItem.getUnitPriceTon() + theItem.getCommissionPrice() + theItem
					.getInsurancePrice()) * theItem.getTotalLengthM();

			double transportationPrice;

			transportationPrice = (round(theItem.getTotalLengthM(), 2) * round(
					theItem.getTransportationUnitPrice(), 2));

			double totalPrice = result + transportationPrice;

			theItem.setTotalPrice(round(totalPrice, 2));

			try {
				theItem.setTotalPriceTL(round(
						totalPrice * theItem.getParityDollar(), 2));
			} catch (Exception e) {
				System.out.println("SalesItemBean:" + e.toString());
				theItem.setTotalPriceTL(0);
			}
			try {
				if (theItem.getParityEuroDollar() != 0)
					theItem.setTotalPriceEuro(round(
							totalPrice / theItem.getParityEuroDollar(), 2));
				else
					theItem.setTotalPriceEuro(0);
			} catch (Exception e) {
				System.out.println("SalesItemBean:" + e.toString());
				theItem.setTotalPriceEuro(0);
			}

		} catch (Exception ex) {
			System.out.println("SalesItemBean:" + ex.toString());
			theItem.setTotalPrice(0);
		}
	}

	public void calculateFCA(SalesItem theItem) {

		System.out.println("SalesItemBean.calculateFCA()");

		SalesKamyonTirYuklemeKapasitesiManager kapasiteManager = new SalesKamyonTirYuklemeKapasitesiManager();

		try {
			kapasites = kapasiteManager.findAllOrderByASC(
					SalesKamyonTirYuklemeKapasitesi.class, "boruCapi");

			if (theItem.getDiameter() > 1626) {
				selectedKapasites.add(kapasites.get(0));
				selectedKapasites.get(0).setTirKapasite(1);
				selectedKapasites.get(0).setBoruCapi(
						(float) theItem.getDiameter());
				tirTonaj = 1 * theItem.getPipeLength()
						* theItem.getUnitWeight();
				selectedKapasite = 1;
			} else {

				outerloop: for (int i = 0; i < kapasites.size(); i++) {
					if (kapasites.get(i).getBoruCapi() >= theItem.getDiameter()) {
						selectedKapasites.add(kapasites.get(i));
						selectedKapasites.add(kapasites.get(i + 1));
						selectedKapasites.add(kapasites.get(i + 2));
						selectedKapasites.add(kapasites.get(i + 3));
						selectedKapasites.add(kapasites.get(i + 4));
						selectedKapasites.add(kapasites.get(i + 5));
						tirTonaj = selectedKapasites.get(0).getTirKapasite()
								* theItem.getPipeLength()
								* theItem.getUnitWeight();
						selectedKapasite = kapasites.get(i).getTirKapasite();
						break outerloop;
					}
				}
			}

			Integer i = 0;
			while (tirTonaj > maxTonaj) {

				tirTonaj = selectedKapasites.get(i).getTirKapasite()
						* theItem.getPipeLength() * theItem.getUnitWeight();
				selectedKapasite = selectedKapasites.get(i).getTirKapasite();
				i++;
			}

			tirAdedi = round(theItem.getTotalLengthM()
					/ (selectedKapasite * theItem.getPipeLength()), 0);

			try {
				// theItem.setFcaBirimUcret(birimFCA);
				theItem.setFcaTirAdedi(round(tirAdedi, 2));
				theItem.setFcaTirTonaj(round(tirTonaj, 2));
				theItem.setFcaTirUcreti(round(tirUcreti, 2));
				theItem.setFcaTirKapasite(round(selectedKapasite, 2));
			} catch (Exception e) {
				System.out.println("SalesItemBean:" + e.toString());
			}

			// System.out.println("Kapasite:" + selectedKapasites.size());
			// System.out.println("KamyonTonaj:" + tirTonaj);

		} catch (Exception ex) {
			System.out.println("SalesItemBean.calculateFCA:" + ex.toString());
		}
	}

	public void calculateFCAPrice() {

		System.out.println("SalesItemBean.calculateFCAPrice()");

		try {
			if (selectedItem.getCurrencyUnit().getCurrencyId() == 1) {// TL
				tirUcretiDoviz = round(selectedItem.getFcaTirUcreti(), 2);
			} else if (selectedItem.getCurrencyUnit().getCurrencyId() == 2) {// USD
				tirUcretiDoviz = round(selectedItem.getFcaTirUcreti()
						/ (selectedItem.getParityDollar()), 2);
			} else if (selectedItem.getCurrencyUnit().getCurrencyId() == 3) {// STG
				tirUcretiDoviz = round(selectedItem.getFcaTirUcreti(), 2);
			} else if (selectedItem.getCurrencyUnit().getCurrencyId() == 4) {// AVRO
				tirUcretiDoviz = round(selectedItem.getFcaTirUcreti()
						/ (selectedItem.getParityEuro()), 2);
			}
		} catch (Exception ex) {
			System.out.println("SalesItemBean.calculateFCAPrice:"
					+ ex.toString());
		}

		try {
			birimFCA = tirUcretiDoviz * tirAdedi
					/ selectedItem.getTotalLengthM();
		} catch (Exception ex) {
			System.out.println("SalesItemBean.calculateFCAPrice:"
					+ ex.toString());
		}

		try {
			selectedItem.setFcaBirimUcret(round(birimFCA, 2));
		} catch (Exception ex) {
			System.out.println("SalesItemBean.calculateFCAPrice:"
					+ ex.toString());
		}
	}

	public void calculateFOBOpen(SalesItem theItem) {

		System.out.println("SalesItemBean.calculateFOBOpen()");

		try {

			birimFOB = theItem.getFobLimanUcreti() * theItem.getTotalTonnage()
					/ 1000 / theItem.getTotalLengthM();

			theItem.setFobBirimUcret(birimFOB);
		} catch (Exception e) {
			System.out
					.println("SalesItemBean.calculateFOBOpen:" + e.toString());
		}
	}

	public void calculateFOBOpenPrice() {

		System.out.println("SalesItemBean.calculateFOBOpenPrice()");

		try {
			if (selectedItem.getCurrencyUnit().getCurrencyId() == 1) {// TL
				birimFOBDoviz = selectedItem.getFobLimanUcreti();
			} else if (selectedItem.getCurrencyUnit().getCurrencyId() == 2) {// USD
				birimFOBDoviz = selectedItem.getFobLimanUcreti()
						/ (selectedItem.getParityDollar());
			} else if (selectedItem.getCurrencyUnit().getCurrencyId() == 3) {// STG
				birimFOBDoviz = selectedItem.getFobLimanUcreti();
			} else if (selectedItem.getCurrencyUnit().getCurrencyId() == 4) {// AVRO
				birimFOBDoviz = selectedItem.getFobLimanUcreti()
						/ (selectedItem.getParityEuro());
			}
		} catch (Exception ex) {
			System.out.println("SalesItemBean.calculateFOBOpenPrice:"
					+ ex.toString());
		}

		try {
			birimFOB = selectedItem.getFobLimanUcreti()
					* selectedItem.getTotalTonnage() / 1000
					/ selectedItem.getTotalLengthM();

		} catch (Exception ex) {
			System.out.println("SalesItemBean.calculateFOBOpenPrice:"
					+ ex.toString());
		}

		try {
			selectedItem.setFobBirimUcret(round(birimFOB, 2));
			selectedItem.setFobUcreti(round(selectedItem.getUnitPriceM()
					+ birimFOB + selectedItem.getFcaBirimUcret(), 2));
		} catch (Exception ex) {
			System.out.println("SalesItemBean.calculateFOBPrice:"
					+ ex.toString());
		}
	}

	public void calculateCFROpenPrice() {

		System.out.println("SalesItemBean.calculateCFRPrice()");

		boruHacim = selectedItem.getDiameter() * selectedItem.getDiameter()
				* selectedItem.getTotalLengthM() / 1000000;

		// try {
		// if (selectedItem.getProposal().getCurrencyUnit().getCurrencyId() ==
		// 1) {// TL
		// birimCFRDoviz = round(selectedItem.getCfrNavlunUcreti(), 2);
		// } else if (selectedItem.getProposal().getCurrencyUnit()
		// .getCurrencyId() == 2) {// USD
		// birimCFRDoviz = round(selectedItem.getCfrNavlunUcreti()
		// / (selectedItem.getParityDollar()), 2);
		// } else if (selectedItem.getProposal().getCurrencyUnit()
		// .getCurrencyId() == 3) {// STG
		// birimCFRDoviz = round(selectedItem.getCfrNavlunUcreti(), 2);
		// } else if (selectedItem.getProposal().getCurrencyUnit()
		// .getCurrencyId() == 4) {// AVRO
		// birimCFRDoviz = round(selectedItem.getCfrNavlunUcreti()
		// / (selectedItem.getParityEuro()), 2);
		// }
		// } catch (Exception ex) {
		// System.out.println("SalesItemBean.calculateCFRPrice:"
		// + ex.toString());
		// }

		try {
			birimCFR = selectedItem.getCfrNavlunUcreti() * boruHacim
					/ selectedItem.getTotalLengthM();

		} catch (Exception ex) {
			System.out.println("SalesItemBean.calculateCFRPrice:"
					+ ex.toString());
		}

		try {
			selectedItem.setCfrBirimUcret(round(birimCFR, 2));
			selectedItem.setCfrUcreti(selectedItem.getFobUcreti()
					+ selectedItem.getCfrBirimUcret());
		} catch (Exception ex) {
			System.out.println("SalesItemBean.calculateCFRPrice:"
					+ ex.toString());
		}
	}

	public void completeItemNumber() {
		System.out.println("SalesItemBean.completeItemNumber()");

		boolean hasProposalAnotherItem = false;
		int queueNumber = 1;
		try {
			for (int i = 0; i < itemList.size() - 1; i++) {
				if (itemList
						.get(i)
						.getProposal()
						.getProposalNumber()
						.contentEquals(
								proposalBean.getSelectedProposal()
										.getProposalNumber())) {

					hasProposalAnotherItem = true;

					if (queueNumber <= Integer.parseInt(itemList.get(i)
							.getItemNo())) {

						queueNumber = Integer.parseInt(itemList.get(i)
								.getItemNo()) + 1;
					}
				}
			}

		} catch (Exception e) {
			System.out.println("SalesItemBean:" + e.toString());
		}

		try {
			if (!hasProposalAnotherItem) {
				queueNumber = 1;
			}

			itemList.get(itemList.size() - 1).setItemNo(
					String.valueOf(queueNumber));
			SalesItemManager itemManager = new SalesItemManager();
			itemManager.updateEntity(selectedItem);

		} catch (Exception e) {
			System.out.println("SalesItemBean:" + e.toString());
		}

	}

	public void activeIndex(Integer index) {

		accordionPanel.setActiveIndex(index.toString());
	}

	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public SalesItem getNewItem() {
		return newItem;
	}

	public void setNewItem(SalesItem newItem) {
		this.newItem = newItem;
	}

	public SalesItem getSelectedItem() {
		try {
			if (selectedItem == null)
				selectedItem = new SalesItem();
		} catch (Exception ex) {
			selectedItem = new SalesItem();
		}
		return selectedItem;
	}

	public void setSelectedItem(SalesItem selectedItem) {
		this.selectedItem = selectedItem;
	}

	public List<SalesItem> getItemList() {
		return itemList;
	}

	public void setItemList(List<SalesItem> itemList) {
		this.itemList = itemList;
	}

	public List<SalesItem> getFilteredList() {
		try {
			selectedItem = filteredList.get(0);
			salesMenuBean.showNewType1Menu();
		} catch (Exception ex) {
			System.out.println("getFilteredList: " + ex);
		}
		return filteredList;
	}

	public void setFilteredList(List<SalesItem> filteredList) {
		this.filteredList = filteredList;
	}

	public SalesMenuBean getSalesMenuBean() {
		return salesMenuBean;
	}

	public void setSalesMenuBean(SalesMenuBean salesMenuBean) {
		this.salesMenuBean = salesMenuBean;
	}

	public SalesProposalBean getProposalBean() {
		return proposalBean;
	}

	public void setProposalBean(SalesProposalBean proposalBean) {
		this.proposalBean = proposalBean;
	}

	public void setOrderBean(SalesOrderBean orderBean) {
		this.orderBean = orderBean;
	}

	public SalesOrderBean getOrderBean() {
		return orderBean;
	}

	public List<SalesItem> getItemsOfSelectedProposal() {

		itemsOfSelectedProposal = new ArrayList<SalesItem>();

		try {
			for (int i = 0; i < itemList.size(); i++) {

				if (itemList.get(i).getProposal()
						.equals(proposalBean.getSelectedProposal())) {
					itemsOfSelectedProposal.add(itemList.get(i));
				}
			}
			if (itemsOfSelectedProposal.size() > 0) {
				Collections.sort(itemsOfSelectedProposal,
						new Comparator<SalesItem>() {
							@Override
							public int compare(final SalesItem object1,
									final SalesItem object2) {
								Integer itemNo1 = Integer.parseInt(object1
										.getItemNo());
								Integer itemNo2 = Integer.parseInt(object2
										.getItemNo());
								return itemNo1.compareTo(itemNo2);
							}
						});
			}

		} catch (Exception e) {
			System.out.println("SalesItemBean:" + e.toString());
		}

		try {
			// A.K. editleme de selected hep aynı geldiği için kapatıldı
			// selectedItem = itemsOfSelectedProposal.get(0);
		} catch (Exception ex) {
			System.out.println("getItemsOfSelectedProposal: " + ex);
		}

		return itemsOfSelectedProposal;
	}

	public void setItemsOfSelectedProposal(
			List<SalesItem> itemsOfSelectedProposal) {
		this.itemsOfSelectedProposal = itemsOfSelectedProposal;
	}

	public List<SalesItem> getItemsOfSelectedOrder() {

		itemsOfSelectedOrder = new ArrayList<SalesItem>();

		try {
			for (int i = 0; i < itemList.size(); i++) {
				if (itemList.get(i).getProposal()
						.equals(orderBean.getSelectedOrder().getProposal())) {
					itemsOfSelectedOrder.add(itemList.get(i));
				}
			}

			if (itemsOfSelectedProposal.size() > 0) {
				Collections.sort(itemsOfSelectedProposal,
						new Comparator<SalesItem>() {
							@Override
							public int compare(final SalesItem object1,
									final SalesItem object2) {
								Integer itemNo1 = Integer.parseInt(object1
										.getItemNo());
								Integer itemNo2 = Integer.parseInt(object2
										.getItemNo());
								return itemNo1.compareTo(itemNo2);
							}
						});
			}
		} catch (Exception e) {
			System.out.println("SalesItemBean:" + e.toString());
		}

		// try {
		// A.K. editleme de selected hep aynı geldiği için kapatıldı
		// selectedItem = itemsOfSelectedOrder.get(0);
		// } catch (Exception ex) {
		// System.out.println("getItemsOfSelectedOrder: " + ex);
		// }

		return itemsOfSelectedOrder;
	}

	public void setItemsOfSelectedOrder(List<SalesItem> itemsOfSelectedOrder) {
		this.itemsOfSelectedOrder = itemsOfSelectedOrder;
	}

	public Rulo getSelectedRulo() {
		return selectedRulo;
	}

	public void setSelectedRulo(Rulo selectedRulo) {
		this.selectedRulo = selectedRulo;
	}

	public AccordionPanel getAccordionPanel() {
		return accordionPanel;
	}

	public void setAccordionPanel(AccordionPanel accordionPanel) {
		this.accordionPanel = accordionPanel;
	}

	public DecimalFormat getDecimalFormat() {
		return decimalFormat;
	}

	public void setDecimalFormat(DecimalFormat decimalFormat) {
		this.decimalFormat = decimalFormat;
	}

	public List<Rulo> getAllRulos() {
		return allRulos;
	}

	public void setAllRulos(List<Rulo> allRulos) {
		this.allRulos = allRulos;
	}

	public Rulo[] getSelectedRulos() {
		return selectedRulos;
	}

	public void setSelectedRulos(Rulo[] selectedRulos) {
		this.selectedRulos = selectedRulos;
	}

	public Date getReservationDate() {
		return reservationDate;
	}

	public void setReservationDate(Date reservationDate) {
		this.reservationDate = reservationDate;
	}

	/**
	 * @return the kapasites
	 */
	public List<SalesKamyonTirYuklemeKapasitesi> getKapasites() {
		return kapasites;
	}

	/**
	 * @param kapasites
	 *            the kapasites to set
	 */
	public void setKapasites(List<SalesKamyonTirYuklemeKapasitesi> kapasites) {
		this.kapasites = kapasites;
	}

	/**
	 * @return the selectedKapasites
	 */
	public List<SalesKamyonTirYuklemeKapasitesi> getSelectedKapasites() {
		return selectedKapasites;
	}

	/**
	 * @param selectedKapasites
	 *            the selectedKapasites to set
	 */
	public void setSelectedKapasites(
			List<SalesKamyonTirYuklemeKapasitesi> selectedKapasites) {
		this.selectedKapasites = selectedKapasites;
	}

	/**
	 * @return the maxTonaj
	 */
	public double getMaxTonaj() {
		return maxTonaj;
	}

	/**
	 * @param maxTonaj
	 *            the maxTonaj to set
	 */
	public void setMaxTonaj(double maxTonaj) {
		this.maxTonaj = maxTonaj;
	}

	/**
	 * @return the tirAdedi
	 */
	public double getTirAdedi() {
		return tirAdedi;
	}

	/**
	 * @param tirAdedi
	 *            the tirAdedi to set
	 */
	public void setTirAdedi(double tirAdedi) {
		this.tirAdedi = tirAdedi;
	}

	/**
	 * @return the selectedKapasite
	 */
	public double getSelectedKapasite() {
		return selectedKapasite;
	}

	/**
	 * @param selectedKapasite
	 *            the selectedKapasite to set
	 */
	public void setSelectedKapasite(double selectedKapasite) {
		this.selectedKapasite = selectedKapasite;
	}

	/**
	 * @return the tirUcreti
	 */
	public double getTirUcreti() {
		return tirUcreti;
	}

	/**
	 * @param tirUcreti
	 *            the tirUcreti to set
	 */
	public void setTirUcreti(double tirUcreti) {
		this.tirUcreti = tirUcreti;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the toplamFCA
	 */
	public double getToplamFCA() {
		return toplamFCA;
	}

	/**
	 * @param toplamFCA
	 *            the toplamFCA to set
	 */
	public void setToplamFCA(double toplamFCA) {
		this.toplamFCA = toplamFCA;
	}

	/**
	 * @return the birimFCA
	 */
	public double getBirimFCA() {
		return birimFCA;
	}

	/**
	 * @param birimFCA
	 *            the birimFCA to set
	 */
	public void setBirimFCA(double birimFCA) {
		this.birimFCA = birimFCA;
	}

	/**
	 * @return the tirUcretiDoviz
	 */
	public double getTirUcretiDoviz() {
		return tirUcretiDoviz;
	}

	/**
	 * @param tirUcretiDoviz
	 *            the tirUcretiDoviz to set
	 */
	public void setTirUcretiDoviz(double tirUcretiDoviz) {
		this.tirUcretiDoviz = tirUcretiDoviz;
	}

	/**
	 * @return the tirTonaj
	 */
	public double getTirTonaj() {
		return tirTonaj;
	}

	/**
	 * @param tirTonaj
	 *            the tirTonaj to set
	 */
	public void setTirTonaj(double tirTonaj) {
		this.tirTonaj = tirTonaj;
	}

	/**
	 * @return the birimCFRDoviz
	 */
	public double getBirimCFRDoviz() {
		return birimCFRDoviz;
	}

	/**
	 * @param birimCFRDoviz
	 *            the birimCFRDoviz to set
	 */
	public void setBirimCFRDoviz(double birimCFRDoviz) {
		this.birimCFRDoviz = birimCFRDoviz;
	}

	/**
	 * @return the boruHacim
	 */
	public double getBoruHacim() {
		return boruHacim;
	}

	/**
	 * @param boruHacim
	 *            the boruHacim to set
	 */
	public void setBoruHacim(double boruHacim) {
		this.boruHacim = boruHacim;
	}

}