/**
 * 
 */
package com.emekboru.beans.kaplamamakinesi;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiAsitYikama;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.kaplamamakinesi.KaplamaMakinesiAsitYikamaManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "kaplamaMakinesiAsitYikamaBean")
@ViewScoped
public class KaplamaMakinesiAsitYikamaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<KaplamaMakinesiAsitYikama> allKaplamaMakinesiAsitYikamaList = new ArrayList<KaplamaMakinesiAsitYikama>();
	private KaplamaMakinesiAsitYikama kaplamaMakinesiAsitYikamaForm = new KaplamaMakinesiAsitYikama();

	private KaplamaMakinesiAsitYikama newAsitYikama = new KaplamaMakinesiAsitYikama();
	private List<KaplamaMakinesiAsitYikama> asitYikamaList;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	private Pipe selectedPipe = new Pipe();

	private boolean kontrol = false;

	private String barkodNo = null;

	// private IsEmriAsitYikamaKaplama selectedIsEmriKaplama = new
	// IsEmriAsitYikamaKaplama();

	public KaplamaMakinesiAsitYikamaBean() {

	}

	public void addAsitYikama(ActionEvent ae) {

		UICommand cmd = (UICommand) ae.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId != null) {
			PipeManager pipeMan = new PipeManager();
			selectedPipe = pipeMan.loadObject(Pipe.class, prmPipeId);
		}

		try {
			KaplamaMakinesiAsitYikamaManager manager = new KaplamaMakinesiAsitYikamaManager();

			newAsitYikama.setEklemeZamani(UtilInsCore.getTarihZaman());
			newAsitYikama.setEkleyenKullanici(userBean.getUser().getId());
			newAsitYikama.setPipe(selectedPipe);
			manager.enterNew(newAsitYikama);
			newAsitYikama = new KaplamaMakinesiAsitYikama();

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"ASİT YIKAMA İŞLEMİ BAŞARIYLA EKLENMİŞTİR!"));

		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL,
					"ASİT YIKAMA İŞLEMİ EKLENEMEDİ!", null));
			System.out
					.println("KaplamaMakinesiAsitYikamaBean.addAsitYikama-HATA");
		}

		fillTestList(prmPipeId);
	}

	public void addAsitYikamaSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		Boolean prmKontrol = kaplamaMakinesiAsitYikamaForm.getDurum();
		KaplamaMakinesiAsitYikamaManager asitYikamaManager = new KaplamaMakinesiAsitYikamaManager();

		if (prmKontrol == null) {

			try {

				kaplamaMakinesiAsitYikamaForm.setDurum(false);
				kaplamaMakinesiAsitYikamaForm
						.setKaplamaBaslamaZamani(UtilInsCore.getTarihZaman());
				kaplamaMakinesiAsitYikamaForm
						.setKaplamaBaslamaKullanici(userBean.getUser().getId());
				asitYikamaManager.updateEntity(kaplamaMakinesiAsitYikamaForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"ASİT YIKAMA İŞLEMİ BAŞARIYLA BAŞLANMIŞTIR!"));
			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"ASİT YIKAMA İŞLEMİ BAŞLANAMADI!", null));
				System.out
						.println("KaplamaMakinesiAsitYikamaBean.addAsitYikamaSonuc-HATA-BAŞLAMA");
			}
		} else if (prmKontrol == false) {

			try {

				kaplamaMakinesiAsitYikamaForm.setDurum(true);
				kaplamaMakinesiAsitYikamaForm.setKaplamaBitisZamani(UtilInsCore
						.getTarihZaman());
				kaplamaMakinesiAsitYikamaForm.setKaplamaBitisKullanici(userBean
						.getUser().getId());
				asitYikamaManager.updateEntity(kaplamaMakinesiAsitYikamaForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"ASİT YIKAMA İŞLEMİ BAŞARIYLA BİTİRİLMİŞTİR!"));

			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"ASİT YIKAMA İŞLEMİ BİTİRİLEMEDİ!", null));
				System.out
						.println("KaplamaMakinesiAsitYikamaBean.addAsitYikamaSonuc-HATA-BİTİŞ"
								+ e.toString());
			}

		}

		fillTestList(prmPipeId);
	}

	@SuppressWarnings("static-access")
	public void fillTestList(Integer prmPipeId) {

		KaplamaMakinesiAsitYikamaManager asitYikamaManager = new KaplamaMakinesiAsitYikamaManager();
		allKaplamaMakinesiAsitYikamaList = asitYikamaManager
				.getAllKaplamaMakinesiAsitYikama(prmPipeId);
	}

	public void kaplamaListener(SelectEvent event) {
		updateButtonRender = true;
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!", null));
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			KaplamaMakinesiAsitYikamaManager asitYikamaManager = new KaplamaMakinesiAsitYikamaManager();
			allKaplamaMakinesiAsitYikamaList = asitYikamaManager
					.getAllKaplamaMakinesiAsitYikama(selectedPipe.getPipeId());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
		}
	}

	public void deleteAsitYikamaSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		KaplamaMakinesiAsitYikamaManager asitYikamaManager = new KaplamaMakinesiAsitYikamaManager();

		if (kaplamaMakinesiAsitYikamaForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		asitYikamaManager.delete(kaplamaMakinesiAsitYikamaForm);
		kaplamaMakinesiAsitYikamaForm = new KaplamaMakinesiAsitYikama();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"ASİT YIKAMA BAŞARIYLA SİLİNMİŞTİR!"));

		fillTestList(prmPipeId);
	}

	// @SuppressWarnings("static-access")
	// public void isEmriGoster() {
	//
	// IsEmriAsitYikamaKaplamaManager isEmriManager = new
	// IsEmriAsitYikamaKaplamaManager();
	// if (isEmriManager.getAllIsEmriAsitYikamaKaplama(
	// selectedPipe.getSalesItem().getItemId()).size() > 0) {
	// selectedIsEmriKaplama = isEmriManager
	// .getAllIsEmriAsitYikamaKaplama(
	// selectedPipe.getSalesItem().getItemId()).get(0);
	// } else {
	//
	// FacesContext context = FacesContext.getCurrentInstance();
	// context.addMessage(null, new FacesMessage(
	// FacesMessage.SEVERITY_ERROR,
	// " ASİT YIKAMA İŞ EMRİ BULUNAMADI, LÜTFEN EKLETİNİZ!",
	// null));
	// return;
	// }
	// }

	// setters getters

	/**
	 * @return the allKaplamaMakinesiAsitYikamaList
	 */
	public List<KaplamaMakinesiAsitYikama> getAllKaplamaMakinesiAsitYikamaList() {
		return allKaplamaMakinesiAsitYikamaList;
	}

	/**
	 * @param allKaplamaMakinesiAsitYikamaList
	 *            the allKaplamaMakinesiAsitYikamaList to set
	 */
	public void setAllKaplamaMakinesiAsitYikamaList(
			List<KaplamaMakinesiAsitYikama> allKaplamaMakinesiAsitYikamaList) {
		this.allKaplamaMakinesiAsitYikamaList = allKaplamaMakinesiAsitYikamaList;
	}

	/**
	 * @return the kaplamaMakinesiAsitYikamaForm
	 */
	public KaplamaMakinesiAsitYikama getKaplamaMakinesiAsitYikamaForm() {
		return kaplamaMakinesiAsitYikamaForm;
	}

	/**
	 * @param kaplamaMakinesiAsitYikamaForm
	 *            the kaplamaMakinesiAsitYikamaForm to set
	 */
	public void setKaplamaMakinesiAsitYikamaForm(
			KaplamaMakinesiAsitYikama kaplamaMakinesiAsitYikamaForm) {
		this.kaplamaMakinesiAsitYikamaForm = kaplamaMakinesiAsitYikamaForm;
	}

	/**
	 * @return the newAsitYikama
	 */
	public KaplamaMakinesiAsitYikama getNewAsitYikama() {
		return newAsitYikama;
	}

	/**
	 * @param newAsitYikama
	 *            the newAsitYikama to set
	 */
	public void setNewAsitYikama(KaplamaMakinesiAsitYikama newAsitYikama) {
		this.newAsitYikama = newAsitYikama;
	}

	/**
	 * @return the asitYikamaList
	 */
	public List<KaplamaMakinesiAsitYikama> getAsitYikamaList() {
		return asitYikamaList;
	}

	/**
	 * @param asitYikamaList
	 *            the asitYikamaList to set
	 */
	public void setAsitYikamaList(List<KaplamaMakinesiAsitYikama> asitYikamaList) {
		this.asitYikamaList = asitYikamaList;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the selectedPipe
	 */
	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	/**
	 * @param selectedPipe
	 *            the selectedPipe to set
	 */
	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	/**
	 * @return the kontrol
	 */
	public boolean isKontrol() {
		return kontrol;
	}

	/**
	 * @param kontrol
	 *            the kontrol to set
	 */
	public void setKontrol(boolean kontrol) {
		this.kontrol = kontrol;
	}

	/**
	 * @return the barkodNo
	 */
	public String getBarkodNo() {
		return barkodNo;
	}

	/**
	 * @param barkodNo
	 *            the barkodNo to set
	 */
	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
