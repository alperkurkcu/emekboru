/**
 * 
 */
package com.emekboru.beans.satinalma;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.satinalma.SatinalmaOdemeBildirimi;
import com.emekboru.jpa.satinalma.SatinalmaOdemeBildirimiIcerik;
import com.emekboru.jpaman.satinalma.SatinalmaOdemeBildirimiIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaOdemeBildirimiManager;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "satinalmaOdemeBildirimiIcerikBean")
@ViewScoped
public class SatinalmaOdemeBildirimiIcerikBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	private SatinalmaOdemeBildirimiIcerik satinalmaOdemeBildirimiIcerikForm = new SatinalmaOdemeBildirimiIcerik();

	private SatinalmaOdemeBildirimi satinalmaOdemeBildirimiForm = new SatinalmaOdemeBildirimi();

	private List<SatinalmaOdemeBildirimiIcerik> allSatinalmaOdemeBildirimiIcerikList = new ArrayList<SatinalmaOdemeBildirimiIcerik>();

	@PostConstruct
	public void load() {
		updateButtonRender = false;
	}

	public void fillTestList(Integer prmFormId) {

		SatinalmaOdemeBildirimiManager bildirimManager = new SatinalmaOdemeBildirimiManager();
		satinalmaOdemeBildirimiForm = bildirimManager.loadObject(
				SatinalmaOdemeBildirimi.class, prmFormId);

		SatinalmaOdemeBildirimiIcerikManager manager = new SatinalmaOdemeBildirimiIcerikManager();
		allSatinalmaOdemeBildirimiIcerikList = manager
				.findByBildirimId(satinalmaOdemeBildirimiForm.getId());
		satinalmaOdemeBildirimiIcerikForm = new SatinalmaOdemeBildirimiIcerik();
		updateButtonRender = false;
	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void addIcerik() {
		satinalmaOdemeBildirimiIcerikForm = new SatinalmaOdemeBildirimiIcerik();
	}

	public void addOrUpdateIcerik() {

		SatinalmaOdemeBildirimiIcerikManager icerikManager = new SatinalmaOdemeBildirimiIcerikManager();

		if (satinalmaOdemeBildirimiIcerikForm.getId() == null) {

			satinalmaOdemeBildirimiIcerikForm.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			satinalmaOdemeBildirimiIcerikForm.setEkleyenKullanici(userBean
					.getUser().getId());
			satinalmaOdemeBildirimiIcerikForm
					.setSatinalmaOdemeBildirimi(satinalmaOdemeBildirimiForm);

			try {
				icerikManager.enterNew(satinalmaOdemeBildirimiIcerikForm);
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"ÖDEME BİLDİRİMİ BAŞARIYLA EKLENDİ!", null));
			} catch (Exception e) {
				System.out
						.println("satinalmaOdemeBildirimiIcerikBean.addOrUpdateIcerik: "
								+ e.toString());
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"ÖDEME BİLDİRİMİ EKLENEMEDİ, HATA!", null));
			}

		} else {

			satinalmaOdemeBildirimiIcerikForm.setGuncellemeZamani(UtilInsCore
					.getTarihZaman());
			satinalmaOdemeBildirimiIcerikForm.setGuncelleyenKullanici(userBean
					.getUser().getId());

			try {
				icerikManager.updateEntity(satinalmaOdemeBildirimiIcerikForm);
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"ÖDEME BİLDİRİMİ BAŞARIYLA GÜNCELLENDİ!", null));
			} catch (Exception e) {
				System.out
						.println("satinalmaOdemeBildirimiIcerikBean.addOrUpdateIcerik: "
								+ e.toString());
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"ÖDEME BİLDİRİMİ GÜNCELLENEMEDİ, HATA!", null));
			}
		}

		fillTestList(satinalmaOdemeBildirimiForm.getId());

	}

	// setters getters
	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the satinalmaOdemeBildirimiIcerikForm
	 */
	public SatinalmaOdemeBildirimiIcerik getSatinalmaOdemeBildirimiIcerikForm() {
		return satinalmaOdemeBildirimiIcerikForm;
	}

	/**
	 * @param satinalmaOdemeBildirimiIcerikForm
	 *            the satinalmaOdemeBildirimiIcerikForm to set
	 */
	public void setSatinalmaOdemeBildirimiIcerikForm(
			SatinalmaOdemeBildirimiIcerik satinalmaOdemeBildirimiIcerikForm) {
		this.satinalmaOdemeBildirimiIcerikForm = satinalmaOdemeBildirimiIcerikForm;
	}

	/**
	 * @return the allSatinalmaOdemeBildirimiIcerikList
	 */
	public List<SatinalmaOdemeBildirimiIcerik> getAllSatinalmaOdemeBildirimiIcerikList() {
		return allSatinalmaOdemeBildirimiIcerikList;
	}

	/**
	 * @param allSatinalmaOdemeBildirimiIcerikList
	 *            the allSatinalmaOdemeBildirimiIcerikList to set
	 */
	public void setAllSatinalmaOdemeBildirimiIcerikList(
			List<SatinalmaOdemeBildirimiIcerik> allSatinalmaOdemeBildirimiIcerikList) {
		this.allSatinalmaOdemeBildirimiIcerikList = allSatinalmaOdemeBildirimiIcerikList;
	}

	/**
	 * @return the satinalmaOdemeBildirimiForm
	 */
	public SatinalmaOdemeBildirimi getSatinalmaOdemeBildirimiForm() {
		return satinalmaOdemeBildirimiForm;
	}

	/**
	 * @param satinalmaOdemeBildirimiForm
	 *            the satinalmaOdemeBildirimiForm to set
	 */
	public void setSatinalmaOdemeBildirimiForm(
			SatinalmaOdemeBildirimi satinalmaOdemeBildirimiForm) {
		this.satinalmaOdemeBildirimiForm = satinalmaOdemeBildirimiForm;
	}

}
