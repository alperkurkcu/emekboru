package com.emekboru.beans.edw;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;

import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.EdwPipeLink;
import com.emekboru.jpa.ElectrodeDustWire;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.sales.customer.SalesCustomer;
import com.emekboru.jpaman.ElectrodeDustWireManager;
import com.emekboru.jpaman.Factory;
import com.emekboru.messages.ElectrodeDustWireType;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "wireBean")
@ViewScoped
public class WireBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
	private ConfigBean config;

	private ElectrodeDustWire selectedWire;

	// Attributes necessary to add a new dust
	private ElectrodeDustWire newWire;
	// private Customer selectedCustomer;
	private SalesCustomer selectedSalesCustomer;

	// to view the list of pipes produced from the Selected Dust
	private List<Pipe> pipeList;

	public WireBean() {

		selectedWire = new ElectrodeDustWire();
		newWire = new ElectrodeDustWire();
		// selectedCustomer = new Customer();
		selectedSalesCustomer = new SalesCustomer();
		pipeList = new ArrayList<Pipe>();
	}

	/******************************************************************************
	 ************** DUST MAIN METHODS SECTION ******************* /
	 ******************************************************************************/
	public void deleteWire() {

		if (!isWireOk())
			return;

		EntityManager man = Factory.getInstance().createEntityManager();
		man.getTransaction().begin();
		selectedWire = man.find(ElectrodeDustWire.class,
				selectedWire.getElectrodeDustWireId());
		man.remove(selectedWire);
		man.getTransaction().commit();
		// remove it from the DustLists as well
		WireListBean bean = (WireListBean) FacesContextUtils.getViewBean(
				"wireListBean", WireListBean.class);
		bean.getWires().remove(selectedWire);
		bean.getRecentWires().remove(selectedWire);

		selectedWire = new ElectrodeDustWire();
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	public void addWire() {

		EntityManager man = Factory.getInstance().createEntityManager();
		man.getTransaction().begin();

		// newWire.setCustomer(selectedCustomer);
		newWire.setSalesCustomer(selectedSalesCustomer);
		newWire.setRemainingAmount(newWire.getMiktari());
		newWire.setType(ElectrodeDustWireType.WIRE);
		newWire.setStatus(config.getConfig().getMaterials().getDefaultStatus());
		// newWire.setOrderId(0);
		newWire.setSalesItemId(0);

		man.persist(newWire);
		man.getTransaction().commit();
		man.close();
		// add the bean to the BeanList
		WireListBean bean = (WireListBean) FacesContextUtils.getViewBean(
				"wireListBean", WireListBean.class);
		bean.getRecentWires().add(newWire);
		newWire = new ElectrodeDustWire();
		// selectedCustomer = new Customer();
		selectedSalesCustomer = new SalesCustomer();
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void loadHistory() {

		EntityManager man = Factory.getInstance().createEntityManager();
		selectedWire = man.find(ElectrodeDustWire.class,
				selectedWire.getElectrodeDustWireId());
		man.getTransaction().begin();
		for (EdwPipeLink l : selectedWire.getEdwPipeLinks()) {
			pipeList.add(l.getPipe());
		}
		man.getTransaction().commit();
		man.close();
	}

	public void updateWire() {

		if (!isWireOk())
			return;

		ElectrodeDustWireManager man = new ElectrodeDustWireManager();
		man.updateEntity(selectedWire);
		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}

	/******************************************************************************
	 *************** HELPER PROTECTED METHODS ******************* /
	 ******************************************************************************/
	protected boolean isWireOk() {

		if (selectedWire.getStatus(config.getConfig()).isDepleted()
				|| selectedWire.getStatus(config.getConfig()).isSold()) {

			System.out.println("Trying to update sold Wire!!");
			FacesContextUtils.addWarnMessage("NotAvailableMessage");
			return false;
		}
		return true;
	}

	/******************************************************************************
	 ******************** GETTERS AND SETTERS ******************* /
	 ******************************************************************************/
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	public ElectrodeDustWire getSelectedWire() {
		return selectedWire;
	}

	public void setSelectedWire(ElectrodeDustWire selectedWire) {
		this.selectedWire = selectedWire;
	}

	public ElectrodeDustWire getNewWire() {
		return newWire;
	}

	public void setNewWire(ElectrodeDustWire newWire) {
		this.newWire = newWire;
	}

	public List<Pipe> getPipeList() {
		return pipeList;
	}

	public void setPipeList(List<Pipe> pipeList) {
		this.pipeList = pipeList;
	}

	public SalesCustomer getSelectedSalesCustomer() {
		return selectedSalesCustomer;
	}

	public void setSelectedSalesCustomer(SalesCustomer selectedSalesCustomer) {
		this.selectedSalesCustomer = selectedSalesCustomer;
	}

}
