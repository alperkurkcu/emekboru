/**
 * 
 */
package com.emekboru.beans.stok;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Employee;
import com.emekboru.jpa.satinalma.SatinalmaSarfMerkezi;
import com.emekboru.jpa.stok.StokGirisCikis;
import com.emekboru.jpa.stok.StokJoinMarkaUreticiKodu;
import com.emekboru.jpa.stok.StokJoinUreticiKoduBarkod;
import com.emekboru.jpa.stok.StokJoinUrunMarka;
import com.emekboru.jpa.stok.StokTanimlarBirim;
import com.emekboru.jpa.stok.StokTanimlarDepoTanimlari;
import com.emekboru.jpa.stok.StokTanimlarMarka;
import com.emekboru.jpa.stok.StokTanimlarRaf;
import com.emekboru.jpa.stok.StokTanimlarUrunGrubu;
import com.emekboru.jpa.stok.StokUrunKartlari;
import com.emekboru.jpaman.EmployeeManager;
import com.emekboru.jpaman.satinalma.SatinalmaSarfMerkeziManager;
import com.emekboru.jpaman.stok.StokGirisCikisManager;
import com.emekboru.jpaman.stok.StokJoinMarkaUreticiKoduManager;
import com.emekboru.jpaman.stok.StokJoinUreticiKoduBarkodManager;
import com.emekboru.jpaman.stok.StokJoinUrunMarkaManager;
import com.emekboru.jpaman.stok.StokTanimlarBirimManager;
import com.emekboru.jpaman.stok.StokTanimlarDepoTanimlariManager;
import com.emekboru.jpaman.stok.StokTanimlarMarkaManager;
import com.emekboru.jpaman.stok.StokTanimlarRafManager;
import com.emekboru.jpaman.stok.StokTanimlarUrunGrubuManager;
import com.emekboru.jpaman.stok.StokUrunKartlariManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.FileOperations;
import com.emekboru.utils.ReportUtil;
import com.emekboru.utils.UtilInsCore;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.Barcode128;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "stokUrunKartlariBean")
@ViewScoped
public class StokUrunKartlariBean implements Serializable {

	private ConfigBean config = new ConfigBean();

	private static final long serialVersionUID = 1L;

	private List<StokUrunKartlari> allStokUrunKartlariList = new ArrayList<StokUrunKartlari>();
	private List<StokUrunKartlari> allStokUrunKartlariBarkodluList = new ArrayList<StokUrunKartlari>();

	private StokUrunKartlari stokUrunKartlariForm = new StokUrunKartlari();
	private StokJoinUrunMarka stokJoinUrunMarka = new StokJoinUrunMarka();
	private StokJoinMarkaUreticiKodu stokJoinMarkaUreticiKodu = new StokJoinMarkaUreticiKodu();
	private StokJoinUreticiKoduBarkod stokJoinUreticiKoduBarkod = new StokJoinUreticiKoduBarkod();
	private StokGirisCikis stokGirisCikis = new StokGirisCikis();

	private StokTanimlarMarka stokTanimlarMarka = new StokTanimlarMarka();

	private StokGirisCikisManager girisCikisManager = new StokGirisCikisManager();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	Long giris;
	Long cikis;
	Long toplam;

	private String path;
	private String privatePath;
	private String privateUrunImagePath;
	private String urunImageDeployPath;

	private File theFile = null;
	private String downloadedFileName;
	private StreamedContent downloadedFile;

	Timestamp demin = UtilInsCore.getTarihZaman();

	public StokUrunKartlariBean() {

		privatePath = File.separatorChar + "barkodlar" + File.separatorChar
				+ "urunBarkod" + File.separatorChar;

		try {
			this.setPath("C:/emekfiles" + privatePath);

		} catch (Exception ex) {
			System.out
					.println("Path for Upload File (StokUrunKartlariBean):			"
							+ FacesContext.getCurrentInstance()
									.getExternalContext()
									.getRealPath(privatePath));

			System.out.print(path);
			System.out.println("CAUSE OF " + ex.toString());
		}

		theFile = new File(path);
		if (!theFile.isDirectory()) {
			theFile.mkdirs();
		}

		downloadedFileName = null;

		// allStokUrunKartlariList = StokUrunKartlariManager
		// .getAllStokUrunKartlari();
		stokUrunKartlariForm = new StokUrunKartlari();
		updateButtonRender = false;

		// fillTestList();
	}

	public void addForm() {

		stokUrunKartlariForm = new StokUrunKartlari();
		updateButtonRender = false;
	}

	public void addOrUpdateForm() {

		StokUrunKartlariManager formManager = new StokUrunKartlariManager();
		StokTanimlarBirimManager birimMan = new StokTanimlarBirimManager();
		StokTanimlarRafManager rafMan = new StokTanimlarRafManager();

		try {

			if (stokUrunKartlariForm.getId() == null) {

				stokUrunKartlariForm.setEklemeZamani(UtilInsCore
						.getTarihZaman());
				stokUrunKartlariForm.setEkleyenKullanici(userBean.getUser()
						.getId());
				stokUrunKartlariForm.setBarkodKodu(stokUrunKartlariForm
						.getUrunKodu().toString());
				stokUrunKartlariForm.setStokTanimlarBirim(birimMan.loadObject(
						StokTanimlarBirim.class, stokUrunKartlariForm
								.getStokTanimlarBirim().getId()));
				stokUrunKartlariForm.setStokTanimlarRaf(rafMan.loadObject(
						StokTanimlarRaf.class, stokUrunKartlariForm
								.getStokTanimlarRaf().getId()));

				formManager.enterNew(stokUrunKartlariForm);

				allStokUrunKartlariBarkodluList.add(stokUrunKartlariForm);
				allStokUrunKartlariList.add(stokUrunKartlariForm);

				this.stokUrunKartlariForm = new StokUrunKartlari();
				FacesContextUtils.addInfoMessage("FormSubmitMessage");

			} else {

				stokUrunKartlariForm.setGuncellemeZamani(UtilInsCore
						.getTarihZaman());
				stokUrunKartlariForm.setGuncelleyenKullanici(userBean.getUser()
						.getId());
				stokUrunKartlariForm.setStokTanimlarBirim(birimMan.loadObject(
						StokTanimlarBirim.class, stokUrunKartlariForm
								.getStokTanimlarBirim().getId()));
				stokUrunKartlariForm.setStokTanimlarRaf(rafMan.loadObject(
						StokTanimlarRaf.class, stokUrunKartlariForm
								.getStokTanimlarRaf().getId()));
				formManager.updateEntity(stokUrunKartlariForm);

				allStokUrunKartlariBarkodluList.add(stokUrunKartlariForm);
				allStokUrunKartlariList.add(stokUrunKartlariForm);

				FacesContextUtils.addInfoMessage("FormUpdateMessage");
			}
		} catch (Exception e) {
			System.out.println("StokUrunKartlariBean - addOrUpdateForm(): "
					+ e.toString());
		}

		fillTestList();
	}

	public void deleteForm() {

		StokUrunKartlariManager formManager = new StokUrunKartlariManager();
		StokGirisCikisManager manager = new StokGirisCikisManager();

		try {

			if (stokUrunKartlariForm.getId() == null) {

				FacesContextUtils.addWarnMessage("NoSelectMessage");
				return;
			}
			// urunden giriş cıkıs yapılmış mı?
			if (manager.silmeKontrol(stokUrunKartlariForm.getId()).size() > 0) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"SİLİNECEK ÜRÜNDEN GİRİŞ-ÇIKIŞ YAPILMIŞ, SİLME İŞLEMİ BAŞARISIZ!",
								null));
				return;
			} else {
				formManager.delete(stokUrunKartlariForm);
			}
			fillTestList();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"ÜRÜN KARTI BAŞARIYLA SİLİNMİŞTİR!", null));
		} catch (Exception e) {
			System.out.println("StokUrunKartlariBean - deleteForm(): "
					+ e.toString());
		}
	}

	@PostConstruct
	public synchronized void fillTestList() {

		long diff = Math.abs(System.currentTimeMillis() - demin.getTime());

		if (diff < 3000) {// son arama ile şimdiki arama arasında
							// 3 saniyeden az zaman var
			System.out.println(diff);
			return;
		}

		this.setPrivateUrunImagePath(File.separatorChar + "urunler"
				+ File.separatorChar + "urunimages" + File.separatorChar);

		this.setPath(config.getConfig().getFolder().getAbsolutePath());

		StokUrunKartlariManager manager = new StokUrunKartlariManager();
		allStokUrunKartlariList = manager.getAllStokUrunKartlari();
		StokJoinUreticiKoduBarkodManager barkodManager = new StokJoinUreticiKoduBarkodManager();
		allStokUrunKartlariBarkodluList = barkodManager
				.getAllStokUrunKartlariBarkodlu();

		stokUrunKartlariForm = new StokUrunKartlari();
		updateButtonRender = false;
	}

	public void talepFormunaGoreStokGetir(Integer prmUrunId) {
		StokJoinUreticiKoduBarkodManager barkodManager = new StokJoinUreticiKoduBarkodManager();
		this.allStokUrunKartlariBarkodluList = barkodManager
				.getAllStokUrunKartlariBarkodluByUrunId(prmUrunId);
	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void depoDurum() {

		giris = girisCikisManager.getGiris(stokUrunKartlariForm.getId()).get(0);
		cikis = girisCikisManager.getCikis(stokUrunKartlariForm.getId()).get(0);
		toplam = giris - cikis;
	}

	public void urunDepoDurum() {

		giris = girisCikisManager.getUrunGiris(stokUrunKartlariForm.getId());
		cikis = girisCikisManager.getUrunCikis(stokUrunKartlariForm.getId());
		toplam = giris - cikis;
	}

	private InputStream excelFile;

	public void readExcelFile(FileUploadEvent event) {
		// public void readExcelFile() {

		StokUrunKartlariManager formManager = new StokUrunKartlariManager();
		StokTanimlarUrunGrubuManager urunMan = new StokTanimlarUrunGrubuManager();
		StokTanimlarRafManager rafMan = new StokTanimlarRafManager();
		StokTanimlarBirimManager birimMan = new StokTanimlarBirimManager();
		SatinalmaSarfMerkeziManager sarfManager = new SatinalmaSarfMerkeziManager();
		StokTanimlarDepoTanimlariManager depoManager = new StokTanimlarDepoTanimlariManager();
		EmployeeManager empManager = new EmployeeManager();

		StokTanimlarMarkaManager markaManager = new StokTanimlarMarkaManager();
		StokJoinUrunMarkaManager sjumManager = new StokJoinUrunMarkaManager();
		StokJoinMarkaUreticiKoduManager sjmuManager = new StokJoinMarkaUreticiKoduManager();
		StokJoinUreticiKoduBarkodManager sjubManager = new StokJoinUreticiKoduBarkodManager();
		StokGirisCikisManager sgcmanager = new StokGirisCikisManager();

		stokUrunKartlariForm = new StokUrunKartlari();

		Boolean devam = true;

		try {

			excelFile = event.getFile().getInputstream();
			// excelFile = new FileInputStream(new File("D:/upload.xls"));
		} catch (IOException e1) {

			e1.printStackTrace();
		}

		try {
			HSSFWorkbook wb = new HSSFWorkbook(excelFile);
			HSSFSheet sheet = wb.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			rowIterator.next();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through each columns
				Iterator<Cell> cellIterator = row.cellIterator();
				int i = 1;
				Double veri = new Double(0);
				Integer data = 0;

				stokUrunKartlariForm = new StokUrunKartlari();
				stokJoinUrunMarka = new StokJoinUrunMarka();
				stokJoinMarkaUreticiKodu = new StokJoinMarkaUreticiKodu();
				devam = true;

				while (cellIterator.hasNext()) {

					Cell cell = cellIterator.next();
					if (i == 1) {
						if (cell.getCellType() == 1) {// string
							stokUrunKartlariForm.setUrunKodu(cell
									.getStringCellValue());
						} else if (cell.getCellType() == 0) {// numerik
							veri = cell.getNumericCellValue();
							stokUrunKartlariForm.setUrunKodu(veri.toString());
						}
					} else if (i == 2) {

						if (cell.getCellType() == 1) {// string
							stokUrunKartlariForm.setUrunAdi(cell
									.getStringCellValue());
						} else if (cell.getCellType() == 0) {// numerik
							veri = cell.getNumericCellValue();
							stokUrunKartlariForm.setUrunAdi(veri.toString());
						}
					} else if (i == 3) {

						try {
							data = urunMan
									.findByField(StokTanimlarUrunGrubu.class,
											"urunGrubu",
											cell.getStringCellValue()).get(0)
									.getId();
						} catch (Exception e2) {

							System.out
									.println(" stokUrunKartlariBean.readExcelFile-HATA anaGrup");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(
									null,
									new FacesMessage(
											"ANA GRUP İSMİNDE HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"));
							return;
						}
						stokUrunKartlariForm.getStokTanimlarUrunGrubu().setId(
								data);
					} else if (i == 4) {

						veri = cell.getNumericCellValue();
						stokUrunKartlariForm.setBarkodKodu(veri.toString());
					} else if (i == 5) {

						try {
							if (cell.getCellType() == 1) {// string
								stokUrunKartlariForm.setAciklama(cell
										.getStringCellValue());
							} else if (cell.getCellType() == 0) {// numerik
								veri = cell.getNumericCellValue();
								stokUrunKartlariForm.setAciklama(veri
										.toString());
							}
						} catch (Exception e2) {

							System.out
									.println(" stokUrunKartlariBean.readExcelFile-HATA aciklamalar");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(
									null,
									new FacesMessage(
											"AÇIKLAMALARDA HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"));
							return;
						}
					} else if (i == 6) {

						try {
							data = birimMan
									.findByField(StokTanimlarBirim.class,
											"birim", cell.getStringCellValue())
									.get(0).getId();
						} catch (Exception e2) {

							System.out
									.println(" stokUrunKartlariBean.readExcelFile-HATA birim");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(
									null,
									new FacesMessage(
											"BİRİM İSMİNDE HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"));
							return;
						}
						stokUrunKartlariForm.getStokTanimlarBirim().setId(data);
					} else if (i == 7) {

						try {
							data = rafMan
									.findByField(StokTanimlarRaf.class, "raf",
											cell.getStringCellValue()).get(0)
									.getId();
						} catch (Exception e2) {

							System.out
									.println(" stokUrunKartlariBean.readExcelFile-HATA raf");
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(
									null,
									new FacesMessage(
											"RAF İSMİNDE HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"));
							return;
						}
						stokUrunKartlariForm.getStokTanimlarRaf().setId(data);
					} else if (i == 8) {

						stokUrunKartlariForm
								.setGozNo(cell.getStringCellValue());
					} else if (i == 9) {

						veri = cell.getNumericCellValue();
						stokUrunKartlariForm.setAdaNo(veri.intValue());

					} else if (i == 10) {

						veri = cell.getNumericCellValue();
						stokUrunKartlariForm.setMinStok(veri.intValue());
					} else if (i == 11) {// buraya kadar genel ozellikleri ile
											// ekleniyor

						veri = cell.getNumericCellValue();
						stokUrunKartlariForm.setMaxStok(veri.intValue());

						stokUrunKartlariForm.setEklemeZamani(UtilInsCore
								.getTarihZaman());
						stokUrunKartlariForm.setEkleyenKullanici(userBean
								.getUser().getId());
						formManager.enterNew(stokUrunKartlariForm);

					} else if (i == 12) {// marka1

						if (cell.getCellType() != 3) {

							// markası olmayanları MARKASI YOK olarak kaydetme
							if (cell.getCellType() == 3) {

								Cell cellNext = cellIterator.next();
								if (cellNext.getCellType() != 3) {
									cell.setCellValue("MARKA YOK");
								} else {
									devam = false;
								}
							}

							if (devam) {

								stokJoinUrunMarka = new StokJoinUrunMarka();
								stokTanimlarMarka = new StokTanimlarMarka();
								try {
									// markası bulunamazsa ekliyor
									if (markaManager.findByField(
											StokTanimlarMarka.class,
											"markaAdi",
											cell.getStringCellValue()).size() == 0) {

										stokTanimlarMarka
												.setEklemeZamani(UtilInsCore
														.getTarihZaman());
										stokTanimlarMarka
												.setEkleyenKullanici(userBean
														.getUser().getId());
										stokTanimlarMarka.setMarkaAdi(cell
												.getStringCellValue());

										markaManager
												.enterNew(stokTanimlarMarka);
										stokTanimlarMarka = new StokTanimlarMarka();
									}
								} catch (Exception e1) {

									System.out
											.println(" stokUrunKartlariBean.readExcelFile-HATA markaFindAdd");
									FacesContext context = FacesContext
											.getCurrentInstance();
									context.addMessage(
											null,
											new FacesMessage(
													"İLK MARKA İSMİNDE SADECE RAKAMLARDAN OLUŞAMAZ, DÜZELTİP TEKRAR DENEYİNİZ!"
															+ cell.getStringCellValue()));
									return;
								}

								// join tablosuna yazıyor
								try {
									data = sjumManager.getAllStokJoinUrunMarka(
											stokUrunKartlariForm.getId(),
											stokJoinUrunMarka
													.getStokTanimlarMarka()
													.getId()).size();
									if (data == 0) {
										stokJoinUrunMarka
												.setStokTanimlarMarka(markaManager
														.findByField(
																StokTanimlarMarka.class,
																"markaAdi",
																cell.getStringCellValue())
														.get(0));
										stokJoinUrunMarka
												.setEklemeZamani(UtilInsCore
														.getTarihZaman());
										stokJoinUrunMarka
												.setEkleyenKullanici(userBean
														.getUser().getId());
										stokJoinUrunMarka
												.setStokUrunKartlari(stokUrunKartlariForm);

										sjumManager.enterNew(stokJoinUrunMarka);
									} else {
										stokJoinUrunMarka = sjumManager
												.getAllStokJoinUrunMarka(
														stokUrunKartlariForm
																.getId(),
														stokJoinUrunMarka
																.getStokTanimlarMarka()
																.getId())
												.get(0);
									}

								} catch (Exception e2) {

									System.out
											.println(" stokUrunKartlariBean.readExcelFile-HATA marka");
									FacesContext context = FacesContext
											.getCurrentInstance();
									context.addMessage(
											null,
											new FacesMessage(
													"İLK MARKA İSMİNDE HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"));
									return;
								}
							}
						} else {
							devam = false;
						}
					} else if (i == 13 && devam) {// uretici1

						if (cell.getCellType() != 1 && cell.getCellType() != 3) {
							cell.setCellValue(cell.toString());
						}

						if (cell.getCellType() != 3) {

							// joinId buluyor
							try {

								stokJoinMarkaUreticiKodu
										.setStokUrunKartlari(stokUrunKartlariForm);
								stokJoinMarkaUreticiKodu
										.setStokTanimlarMarka(stokJoinUrunMarka
												.getStokTanimlarMarka());

								if (cell.getCellType() == 3) {
									stokJoinMarkaUreticiKodu
											.setUreticiKodu("ÜRETİCİ KODU YOK");
								} else {

									if (cell.getCellType() == 1) {// string
										stokJoinMarkaUreticiKodu
												.setUreticiKodu(cell
														.getStringCellValue());
									} else if (cell.getCellType() == 0) {// numerik
										veri = cell.getNumericCellValue();
										stokJoinMarkaUreticiKodu
												.setUreticiKodu(veri.toString());
									}
								}
								stokJoinMarkaUreticiKodu
										.setEklemeZamani(UtilInsCore
												.getTarihZaman());
								stokJoinMarkaUreticiKodu
										.setEkleyenKullanici(userBean.getUser()
												.getId());
								sjmuManager.enterNew(stokJoinMarkaUreticiKodu);
							} catch (Exception e2) {

								System.out
										.println(" stokUrunKartlariBean.readExcelFile-HATA üreticiKodu");
								FacesContext context = FacesContext
										.getCurrentInstance();
								context.addMessage(
										null,
										new FacesMessage(

										"İLK ÜRETİCİ KODUNDA HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"));
								return;
							}
						}

					} else if (i == 14 && devam) {// üretici barkod 1

						if (cell.getCellType() != 1 && cell.getCellType() != 3) {
							cell.setCellValue(cell.toString());
						}

						if (cell.getCellType() != 3) {
							// joinId buluyor
							try {

								stokJoinUreticiKoduBarkod
										.setStokJoinMarkaUreticiKodu(stokJoinMarkaUreticiKodu);
								stokJoinUreticiKoduBarkod
										.setStokTanimlarMarka(stokJoinUrunMarka
												.getStokTanimlarMarka());

								if (cell.getCellType() == 3) {
									stokJoinUreticiKoduBarkod
											.setBarkod(stokJoinUrunMarka
													.getStokUrunKartlari()
													.getUrunKodu().toString());
								} else {
									stokJoinUreticiKoduBarkod.setBarkod(cell
											.getStringCellValue());
								}
								stokJoinUreticiKoduBarkod
										.setEklemeZamani(UtilInsCore
												.getTarihZaman());
								stokJoinUreticiKoduBarkod
										.setEkleyenKullanici(userBean.getUser()
												.getId());
								sjubManager.enterNew(stokJoinUreticiKoduBarkod);

								// stokJoinUrunMarka = new StokJoinUrunMarka();
								// stokJoinMarkaUreticiKodu = new
								// StokJoinMarkaUreticiKodu();
								// stokJoinUreticiKoduBarkod = new
								// StokJoinUreticiKoduBarkod();
							} catch (Exception e2) {

								System.out
										.println(" stokUrunKartlariBean.readExcelFile-HATA üreticiBarkodu");
								FacesContext context = FacesContext
										.getCurrentInstance();
								context.addMessage(
										null,
										new FacesMessage(

										"İLK ÜRETİCİ BARKODUNDA HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"));
								return;
							}
						}
					} else if (i == 15 && devam) {// Başlangıç miktarı

						if (cell.getCellType() != 3) {
							try {
								if (cell.getCellType() == 3) {
									stokGirisCikis.setMiktar(0);
								} else {
									veri = cell.getNumericCellValue();
									stokGirisCikis.setMiktar(veri.intValue());
								}
								stokGirisCikis.setEklemeZamani(UtilInsCore
										.getTarihZaman());
								stokGirisCikis.setEkleyenKullanici(userBean
										.getUser().getId());
								stokGirisCikis
										.setIslemDurumu(String.valueOf(StokGirisCikis.GIRIS_ISLEMI));
								stokGirisCikis
										.setStokUrunKartlari(stokJoinMarkaUreticiKodu
												.getStokUrunKartlari());
								stokGirisCikis
										.setStokTanimlarBirim(stokJoinMarkaUreticiKodu
												.getStokUrunKartlari()
												.getStokTanimlarBirim());
								stokGirisCikis
										.setSatinalmaSarfMerkezi(sarfManager
												.loadObject(
														SatinalmaSarfMerkezi.class,
														1));
								stokGirisCikis
										.setStokTanimlarDepoTanimlari(depoManager
												.loadObject(
														StokTanimlarDepoTanimlari.class,
														1001));
								stokGirisCikis
										.setTeslimAlanKullanici(empManager
												.loadObject(Employee.class, 120));
								stokGirisCikis
										.setTeslimEdenKullanici(empManager
												.loadObject(Employee.class, 388));
								stokGirisCikis
										.setStokJoinUreticiKoduBarkod(stokJoinUreticiKoduBarkod);
								sgcmanager.enterNew(stokGirisCikis);

								stokJoinUrunMarka = new StokJoinUrunMarka();
								stokJoinMarkaUreticiKodu = new StokJoinMarkaUreticiKodu();
								stokJoinUreticiKoduBarkod = new StokJoinUreticiKoduBarkod();
								stokGirisCikis = new StokGirisCikis();
							} catch (Exception e2) {

								System.out
										.println(" stokUrunKartlariBean.readExcelFile-HATA başlangıc değer");
								FacesContext context = FacesContext
										.getCurrentInstance();
								context.addMessage(
										null,
										new FacesMessage(

										"BAŞLANGIÇ DEĞERİNDE HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"));
								return;
							}
						}
					} else if (i == 16 && devam) {// marka2

						if (cell.getCellType() != 3) {

							// markası olmayanları MARKASI YOK olarak kaydetme
							if (cell.getCellType() == 3) {

								Cell cellNext = cellIterator.next();
								if (cellNext.getCellType() != 3) {
									cell.setCellValue("MARKA YOK");
								} else {
									devam = false;
								}
							}

							if (devam) {

								stokJoinUrunMarka = new StokJoinUrunMarka();
								stokTanimlarMarka = new StokTanimlarMarka();
								try {

									stokJoinUrunMarka = new StokJoinUrunMarka();

									// markası bulunamazsa ekliyor
									if (markaManager.findByField(
											StokTanimlarMarka.class,
											"markaAdi",
											cell.getStringCellValue()).size() == 0) {

										stokTanimlarMarka
												.setEklemeZamani(UtilInsCore
														.getTarihZaman());
										stokTanimlarMarka
												.setEkleyenKullanici(userBean
														.getUser().getId());
										stokTanimlarMarka.setMarkaAdi(cell
												.getStringCellValue());

										markaManager
												.enterNew(stokTanimlarMarka);
									}
								} catch (Exception e1) {

									System.out
											.println(" stokUrunKartlariBean.readExcelFile-HATA markaFindAdd");
									FacesContext context = FacesContext
											.getCurrentInstance();
									context.addMessage(
											null,
											new FacesMessage(
													"İKİNCİ MARKA İSMİNDE SADECE RAKAMLARDAN OLUŞAMAZ, DÜZELTİP TEKRAR DENEYİNİZ!"
															+ cell.getStringCellValue()));
									return;
								}

								// join tablosuna yazıyor
								try {
									data = sjumManager
											.getAllStokJoinUrunMarka(
													stokUrunKartlariForm
															.getId(),
													markaManager
															.findByField(
																	StokTanimlarMarka.class,
																	"markaAdi",
																	cell.getStringCellValue())
															.get(0).getId())
											.size();
									if (data == 0) {
										stokJoinUrunMarka
												.setStokTanimlarMarka(markaManager
														.findByField(
																StokTanimlarMarka.class,
																"markaAdi",
																cell.getStringCellValue())
														.get(0));
										stokJoinUrunMarka
												.setEklemeZamani(UtilInsCore
														.getTarihZaman());
										stokJoinUrunMarka
												.setEkleyenKullanici(userBean
														.getUser().getId());
										stokJoinUrunMarka
												.setStokUrunKartlari(stokUrunKartlariForm);

										sjumManager.enterNew(stokJoinUrunMarka);
									} else {
										stokJoinUrunMarka = sjumManager
												.getAllStokJoinUrunMarka(
														stokUrunKartlariForm
																.getId(),
														markaManager
																.findByField(
																		StokTanimlarMarka.class,
																		"markaAdi",
																		cell.getStringCellValue())
																.get(0).getId())
												.get(0);
									}
								} catch (Exception e2) {

									System.out
											.println(" stokUrunKartlariBean.readExcelFile-HATA marka");
									FacesContext context = FacesContext
											.getCurrentInstance();
									context.addMessage(
											null,
											new FacesMessage(
													"İKİNCİ MARKA İSMİNDE HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"));
									return;
								}
							}
						} else {
							devam = false;
						}
					} else if (i == 17 && devam) {// uretici2

						if (cell.getCellType() != 1 && cell.getCellType() != 3) {
							cell.setCellValue(cell.toString());
						}

						if (cell.getCellType() != 3) {

							try {
								stokJoinMarkaUreticiKodu
										.setStokUrunKartlari(stokUrunKartlariForm);
								stokJoinMarkaUreticiKodu
										.setStokTanimlarMarka(stokJoinUrunMarka
												.getStokTanimlarMarka());

								if (cell.getCellType() == 3) {
									stokJoinMarkaUreticiKodu
											.setUreticiKodu("ÜRETİCİ KODU YOK");
								} else {
									stokJoinMarkaUreticiKodu
											.setUreticiKodu(cell
													.getStringCellValue());
								}

								stokJoinMarkaUreticiKodu
										.setEklemeZamani(UtilInsCore
												.getTarihZaman());
								stokJoinMarkaUreticiKodu
										.setEkleyenKullanici(userBean.getUser()
												.getId());
								sjmuManager.enterNew(stokJoinMarkaUreticiKodu);

								stokJoinUrunMarka = new StokJoinUrunMarka();
								stokJoinMarkaUreticiKodu = new StokJoinMarkaUreticiKodu();
							} catch (Exception e2) {

								System.out
										.println(" stokUrunKartlariBean.readExcelFile-HATA marka");
								FacesContext context = FacesContext
										.getCurrentInstance();
								context.addMessage(
										null,
										new FacesMessage(

										"İKİNCİ JOINDE HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"));
								return;
							}
						}

					} else if (i == 18 && devam) {// üretici barkod 2

						if (cell.getCellType() != 1 && cell.getCellType() != 3) {
							cell.setCellValue(cell.toString());
						}

						if (cell.getCellType() != 3) {

							// joinId buluyor
							try {

								stokJoinUreticiKoduBarkod
										.setStokJoinMarkaUreticiKodu(stokJoinMarkaUreticiKodu);
								stokJoinUreticiKoduBarkod
										.setStokTanimlarMarka(stokJoinUrunMarka
												.getStokTanimlarMarka());

								if (cell.getCellType() == 3) {
									stokJoinUreticiKoduBarkod
											.setBarkod(stokJoinUrunMarka
													.getStokUrunKartlari()
													.getUrunKodu().toString());
								} else {
									stokJoinUreticiKoduBarkod.setBarkod(cell
											.getStringCellValue());
								}
								stokJoinUreticiKoduBarkod
										.setEklemeZamani(UtilInsCore
												.getTarihZaman());
								stokJoinUreticiKoduBarkod
										.setEkleyenKullanici(userBean.getUser()
												.getId());
								sjubManager.enterNew(stokJoinUreticiKoduBarkod);

								stokJoinUrunMarka = new StokJoinUrunMarka();
								stokJoinMarkaUreticiKodu = new StokJoinMarkaUreticiKodu();
								stokJoinUreticiKoduBarkod = new StokJoinUreticiKoduBarkod();
							} catch (Exception e2) {

								System.out
										.println(" stokUrunKartlariBean.readExcelFile-HATA üreticiBarkodu2");
								FacesContext context = FacesContext
										.getCurrentInstance();
								context.addMessage(
										null,
										new FacesMessage(

										"İKİNCİ ÜRETİCİ BARKODUNDA HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"));
								return;
							}
						}
					} else if (i == 19 && devam) {// marka3

						if (cell.getCellType() != 3) {

							// markası olmayanları MARKASI YOK olarak kaydetme
							if (cell.getCellType() == 3) {

								Cell cellNext = cellIterator.next();
								// if (cellNext.toString() != null) {
								if (cellNext.getCellType() != 3) {
									cell.setCellValue("MARKA YOK");
								} else {
									devam = false;
								}
							}

							if (devam) {

								stokJoinUrunMarka = new StokJoinUrunMarka();
								stokTanimlarMarka = new StokTanimlarMarka();
								try {
									// markası bulunamazsa ekliyor
									if (markaManager.findByField(
											StokTanimlarMarka.class,
											"markaAdi",
											cell.getStringCellValue()).size() == 0) {

										stokTanimlarMarka
												.setEklemeZamani(UtilInsCore
														.getTarihZaman());
										stokTanimlarMarka
												.setEkleyenKullanici(userBean
														.getUser().getId());
										stokTanimlarMarka.setMarkaAdi(cell
												.getStringCellValue());

										markaManager
												.enterNew(stokTanimlarMarka);
									}
								} catch (Exception e1) {

									System.out
											.println(" stokUrunKartlariBean.readExcelFile-HATA markaFindAdd");
									FacesContext context = FacesContext
											.getCurrentInstance();
									context.addMessage(
											null,
											new FacesMessage(
													"ÜÇÜNCÜ MARKA İSMİNDE SADECE RAKAMLARDAN OLUŞAMAZ, DÜZELTİP TEKRAR DENEYİNİZ!"
															+ cell.getStringCellValue()));
									return;
								}

								// join tablosuna yazıyor
								try {
									data = sjumManager
											.getAllStokJoinUrunMarka(
													stokUrunKartlariForm
															.getId(),
													markaManager
															.findByField(
																	StokTanimlarMarka.class,
																	"markaAdi",
																	cell.getStringCellValue())
															.get(0).getId())
											.size();
									if (data == 0) {
										stokJoinUrunMarka
												.setStokTanimlarMarka(markaManager
														.findByField(
																StokTanimlarMarka.class,
																"markaAdi",
																cell.getStringCellValue())
														.get(0));
										stokJoinUrunMarka
												.setEklemeZamani(UtilInsCore
														.getTarihZaman());
										stokJoinUrunMarka
												.setEkleyenKullanici(userBean
														.getUser().getId());
										stokJoinUrunMarka
												.setStokUrunKartlari(stokUrunKartlariForm);

										sjumManager.enterNew(stokJoinUrunMarka);
									} else {
										stokJoinUrunMarka = sjumManager
												.getAllStokJoinUrunMarka(
														stokUrunKartlariForm
																.getId(),
														markaManager
																.findByField(
																		StokTanimlarMarka.class,
																		"markaAdi",
																		cell.getStringCellValue())
																.get(0).getId())
												.get(0);
									}

								} catch (Exception e2) {

									System.out
											.println(" stokUrunKartlariBean.readExcelFile-HATA marka");
									FacesContext context = FacesContext
											.getCurrentInstance();
									context.addMessage(
											null,
											new FacesMessage(
													"ÜÇÜNCÜ MARKA İSMİNDE HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"));
									return;
								}
							}
						} else {
							devam = false;
						}
					} else if (i == 20 && devam) {// uretici3

						if (cell.getCellType() != 1 && cell.getCellType() != 3) {
							cell.setCellValue(cell.toString());
							// System.out.println(cell.toString());
						}

						if (cell.getCellType() != 3) {

							try {
								stokJoinMarkaUreticiKodu
										.setStokUrunKartlari(stokUrunKartlariForm);
								stokJoinMarkaUreticiKodu
										.setStokTanimlarMarka(stokJoinUrunMarka
												.getStokTanimlarMarka());

								if (cell.getCellType() == 3) {
									stokJoinMarkaUreticiKodu
											.setUreticiKodu("ÜRETİCİ KODU YOK");
								} else {
									stokJoinMarkaUreticiKodu
											.setUreticiKodu(cell
													.getStringCellValue());
								}

								stokJoinMarkaUreticiKodu
										.setEklemeZamani(UtilInsCore
												.getTarihZaman());
								stokJoinMarkaUreticiKodu
										.setEkleyenKullanici(userBean.getUser()
												.getId());
								sjmuManager.enterNew(stokJoinMarkaUreticiKodu);

								stokTanimlarMarka = new StokTanimlarMarka();
								stokJoinUrunMarka = new StokJoinUrunMarka();
								stokJoinMarkaUreticiKodu = new StokJoinMarkaUreticiKodu();

							} catch (Exception e2) {

								System.out
										.println(" stokUrunKartlariBean.readExcelFile-HATA marka");
								FacesContext context = FacesContext
										.getCurrentInstance();
								context.addMessage(
										null,
										new FacesMessage(

										"ÜÇÜNCÜ JOINDE HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"));
								return;
							}

						}

					} else if (i == 21 && devam) {// üretici barkod 3

						if (cell.getCellType() != 1 && cell.getCellType() != 3) {
							cell.setCellValue(cell.toString());
							// System.out.println(cell.toString());
						}

						if (cell.getCellType() != 3) {

							// joinId buluyor
							try {
								stokJoinUreticiKoduBarkod
										.setStokJoinMarkaUreticiKodu(stokJoinMarkaUreticiKodu);
								stokJoinUreticiKoduBarkod
										.setStokTanimlarMarka(stokJoinUrunMarka
												.getStokTanimlarMarka());

								if (cell.getCellType() == 3) {
									stokJoinUreticiKoduBarkod
											.setBarkod(stokJoinUrunMarka
													.getStokUrunKartlari()
													.getUrunKodu().toString());
								} else {
									stokJoinUreticiKoduBarkod.setBarkod(cell
											.getStringCellValue());
								}
								stokJoinUreticiKoduBarkod
										.setEklemeZamani(UtilInsCore
												.getTarihZaman());
								stokJoinUreticiKoduBarkod
										.setEkleyenKullanici(userBean.getUser()
												.getId());
								sjubManager.enterNew(stokJoinUreticiKoduBarkod);

								stokJoinUrunMarka = new StokJoinUrunMarka();
								stokJoinMarkaUreticiKodu = new StokJoinMarkaUreticiKodu();
								stokJoinUreticiKoduBarkod = new StokJoinUreticiKoduBarkod();
							} catch (Exception e2) {

								System.out
										.println(" stokUrunKartlariBean.readExcelFile-HATA üreticiBarkodu");
								FacesContext context = FacesContext
										.getCurrentInstance();
								context.addMessage(
										null,
										new FacesMessage(

										"ÜÇÜNCÜ ÜRETİCİ BARKODUNDA HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"));
								return;
							}
						}
					} else if (i == 22 && devam) {// marka4

						if (cell.getCellType() != 3) {

							// markası olmayanları MARKASI YOK olarak kaydetme
							if (cell.getCellType() == 3) {

								Cell cellNext = cellIterator.next();
								if (cellNext.getCellType() != 3) {
									cell.setCellValue("MARKA YOK");
								} else {
									devam = false;
								}
							}

							if (devam) {

								stokJoinUrunMarka = new StokJoinUrunMarka();
								stokTanimlarMarka = new StokTanimlarMarka();
								try {
									// markası bulunamazsa ekliyor
									if (markaManager.findByField(
											StokTanimlarMarka.class,
											"markaAdi",
											cell.getStringCellValue()).size() == 0) {

										stokTanimlarMarka
												.setEklemeZamani(UtilInsCore
														.getTarihZaman());
										stokTanimlarMarka
												.setEkleyenKullanici(userBean
														.getUser().getId());
										stokTanimlarMarka.setMarkaAdi(cell
												.getStringCellValue());

										markaManager
												.enterNew(stokTanimlarMarka);
									}
								} catch (Exception e1) {

									System.out
											.println(" stokUrunKartlariBean.readExcelFile-HATA markaFindAdd");
									FacesContext context = FacesContext
											.getCurrentInstance();
									context.addMessage(
											null,
											new FacesMessage(
													"DÖRDÜNCÜ MARKA İSMİNDE SADECE RAKAMLARDAN OLUŞAMAZ, DÜZELTİP TEKRAR DENEYİNİZ!"
															+ cell.getStringCellValue()));
									return;
								}

								// join tablosuna yazıyor
								try {
									data = sjumManager
											.getAllStokJoinUrunMarka(
													stokUrunKartlariForm
															.getId(),
													markaManager
															.findByField(
																	StokTanimlarMarka.class,
																	"markaAdi",
																	cell.getStringCellValue())
															.get(0).getId())
											.size();
									if (data == 0) {
										stokJoinUrunMarka
												.setStokTanimlarMarka(markaManager
														.findByField(
																StokTanimlarMarka.class,
																"markaAdi",
																cell.getStringCellValue())
														.get(0));
										stokJoinUrunMarka
												.setEklemeZamani(UtilInsCore
														.getTarihZaman());
										stokJoinUrunMarka
												.setEkleyenKullanici(userBean
														.getUser().getId());
										stokJoinUrunMarka
												.setStokUrunKartlari(stokUrunKartlariForm);

										sjumManager.enterNew(stokJoinUrunMarka);
									} else {
										stokJoinUrunMarka = sjumManager
												.getAllStokJoinUrunMarka(
														stokUrunKartlariForm
																.getId(),
														markaManager
																.findByField(
																		StokTanimlarMarka.class,
																		"markaAdi",
																		cell.getStringCellValue())
																.get(0).getId())
												.get(0);
									}

								} catch (Exception e2) {

									System.out
											.println(" stokUrunKartlariBean.readExcelFile-HATA marka");
									FacesContext context = FacesContext
											.getCurrentInstance();
									context.addMessage(
											null,
											new FacesMessage(
													"DÖRDÜNCÜ MARKA İSMİNDE HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"));
									return;
								}
							}
						} else {
							devam = false;
						}
					} else if (i == 23 && devam) {// uretici4

						if (cell.getCellType() != 1 && cell.getCellType() != 3) {

							cell.setCellValue(cell.toString());
							// System.out.println(cell.toString());
						}

						if (cell.getCellType() != 3) {

							try {
								stokJoinMarkaUreticiKodu
										.setStokUrunKartlari(stokUrunKartlariForm);
								stokJoinMarkaUreticiKodu
										.setStokTanimlarMarka(stokJoinUrunMarka
												.getStokTanimlarMarka());

								if (cell.getCellType() == 3) {
									stokJoinMarkaUreticiKodu
											.setUreticiKodu("ÜRETİCİ KODU YOK");
								} else {
									stokJoinMarkaUreticiKodu
											.setUreticiKodu(cell
													.getStringCellValue());
								}

								stokJoinMarkaUreticiKodu
										.setEklemeZamani(UtilInsCore
												.getTarihZaman());
								stokJoinMarkaUreticiKodu
										.setEkleyenKullanici(userBean.getUser()
												.getId());
								sjmuManager.enterNew(stokJoinMarkaUreticiKodu);

								stokTanimlarMarka = new StokTanimlarMarka();
								stokJoinUrunMarka = new StokJoinUrunMarka();
								stokJoinMarkaUreticiKodu = new StokJoinMarkaUreticiKodu();
							} catch (Exception e2) {

								System.out
										.println(" stokUrunKartlariBean.readExcelFile-HATA marka");
								FacesContext context = FacesContext
										.getCurrentInstance();
								context.addMessage(
										null,
										new FacesMessage(

										"DÖRDÜNCÜ JOINDE HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"));
								return;
							}
						}
					} else if (i == 24 && devam) {// üretici barkod 4

						if (cell.getCellType() != 1 && cell.getCellType() != 3) {
							cell.setCellValue(cell.toString());
							// System.out.println(cell.toString());
						}

						if (cell.getCellType() != 3) {

							// joinId buluyor
							try {

								stokJoinUreticiKoduBarkod
										.setStokJoinMarkaUreticiKodu(stokJoinMarkaUreticiKodu);
								stokJoinUreticiKoduBarkod
										.setStokTanimlarMarka(stokJoinUrunMarka
												.getStokTanimlarMarka());

								if (cell.getCellType() == 3) {
									stokJoinUreticiKoduBarkod
											.setBarkod(stokJoinUrunMarka
													.getStokUrunKartlari()
													.getUrunKodu().toString());
								} else {
									stokJoinUreticiKoduBarkod.setBarkod(cell
											.getStringCellValue());
								}
								stokJoinUreticiKoduBarkod
										.setEklemeZamani(UtilInsCore
												.getTarihZaman());
								stokJoinUreticiKoduBarkod
										.setEkleyenKullanici(userBean.getUser()
												.getId());
								sjubManager.enterNew(stokJoinUreticiKoduBarkod);

								stokJoinUrunMarka = new StokJoinUrunMarka();
								stokJoinMarkaUreticiKodu = new StokJoinMarkaUreticiKodu();
								stokJoinUreticiKoduBarkod = new StokJoinUreticiKoduBarkod();
							} catch (Exception e2) {

								System.out
										.println(" stokUrunKartlariBean.readExcelFile-HATA üreticiBarkodu");
								FacesContext context = FacesContext
										.getCurrentInstance();
								context.addMessage(
										null,
										new FacesMessage(

										"DÖRDÜNCÜ ÜRETİCİ BARKODUNDA HATA VAR, DÜZELTİP TEKRAR DENEYİNİZ!"));
								return;
							}
						}
					}
					i++;
				}
			}
			// file.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"EXCEL KISMI OLARAK YÜKLENDİ, LÜTFEN EKLENENLERİ KONTROL EDİP TEKRAR DENEYİNİZ!",
							null));
		} catch (IOException e) {
			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"EXCEL DOSYA HATASI, LÜTFEN EKLENENLERİ KONTROL EDİP TEKRAR DENEYİNİZ!",
							null));
		}

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"EXCEL VERİLERİ BAŞARIYLA EKLENMİŞTİR!"));

	}

	public void uploadPhoto(FileUploadEvent event) {

		StokUrunKartlariManager formManager = new StokUrunKartlariManager();

		FileOperations fileOperations = FileOperations.getInstance();
		String filePath = getPath() + getPrivateUrunImagePath();
		String fileName = this.getStokUrunKartlariForm().getUrunAdi() + "_"
				+ this.getStokUrunKartlariForm().getBarkodKodu() + "_"
				+ this.getStokUrunKartlariForm().getId();

		String fullFileName = fileName + "."
				+ FilenameUtils.getExtension(event.getFile().getFileName());

		if (fileOperations.exists(filePath, fullFileName)) {
			fileOperations.deleteFile(filePath, fullFileName);
		}

		String photoPath = fileOperations.uploadFile(filePath, fileName, event);

		this.getStokUrunKartlariForm().setPhotoPath(photoPath);
		formManager.updateEntity(stokUrunKartlariForm);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"ÜRÜN FOTOĞRAFI BAŞARIYLA EKLENMİŞTİR!"));
	}

	@SuppressWarnings("unchecked")
	public void barcodeBas() throws IOException, DocumentException {

		ReportUtil reportUtil = new ReportUtil();

		try {

			StokUrunKartlari barcodeInfo = stokUrunKartlariForm
					.getStokJoinUreticiKoduBarkod()
					.getStokJoinMarkaUreticiKodu().getStokUrunKartlari();

			// barcodeInfo.setAdaNo("Ada NO");

			String barcodeCode = stokUrunKartlariForm
					.getStokJoinUreticiKoduBarkod()
					.getStokJoinMarkaUreticiKodu().getStokUrunKartlari()
					.getUrunKodu().toString();

			String markaAdi = stokUrunKartlariForm
					.getStokJoinUreticiKoduBarkod().getStokTanimlarMarka()
					.getMarkaAdi().toString();

			Barcode128 code128 = new Barcode128();
			code128.setCode(barcodeCode);
			code128.setChecksumText(true);
			code128.setGenerateChecksum(true);
			code128.setStartStopText(true);

			java.awt.Image image = code128.createAwtImage(Color.BLACK,
					Color.WHITE);

			HSSFWorkbook hssfWorkbook = reportUtil
					.getWorkbookFromFilePath("/reportformats/barcode/depoUrunKartiBarcode.xls");

			reportUtil.insertImageToWorkbook(hssfWorkbook, image, "testImage",
					image.getWidth(null) * 3, image.getHeight(null) * 4);
			reportUtil.writeWorkbookToFile(hssfWorkbook,
					"/reportformats/barcode/depoUrunKartiBarcode-"
							+ barcodeCode + "-" + markaAdi + ".xls");

			@SuppressWarnings("rawtypes")
			Map dataMap = new HashMap();
			dataMap.put("barcodeInfo", barcodeInfo);

			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/barcode/depoUrunKartiBarcode-"
							+ barcodeCode + "-" + markaAdi + ".xls", "");

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null,
					new FacesMessage(stokUrunKartlariForm.getUrunKodu()
							+ " NUMARALI ÜRÜN BARKODU ÜRETİLDİ!"));

		} catch (Exception e) {

			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"BARKOD ÜRETİLEMEDİ, LÜTFEN TEKRAR DENEYİNİZ!", null));

		}

	}

	public void barcodeYazdir() {

		String barcodeCode = stokUrunKartlariForm
				.getStokJoinUreticiKoduBarkod().getStokJoinMarkaUreticiKodu()
				.getStokUrunKartlari().getUrunKodu().toString();
		String markaAdi = stokUrunKartlariForm.getStokJoinUreticiKoduBarkod()
				.getStokTanimlarMarka().getMarkaAdi().toString();

		try {
			FileOperations.getInstance().streamFile(
					FacesContext
							.getCurrentInstance()
							.getExternalContext()
							.getRealPath(
									"/reportformats/barcode/depoUrunKartiBarcode-"
											+ barcodeCode + "-" + markaAdi
											+ ".xls"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@SuppressWarnings("resource")
	public StreamedContent getDownloadedFile() {

		try {
			if (downloadedFileName != null) {
				RandomAccessFile accessFile = new RandomAccessFile(path
						+ downloadedFileName, "r");
				byte[] downloadByte = new byte[(int) accessFile.length()];
				accessFile.read(downloadByte);
				InputStream stream = new ByteArrayInputStream(downloadByte);

				String suffix = FilenameUtils.getExtension(downloadedFileName);
				String contentType = "text/plain";
				if (suffix.contentEquals("jpg") || suffix.contentEquals("png")
						|| suffix.contentEquals("gif")) {
					contentType = "image/" + contentType;
				} else if (suffix.contentEquals("doc")
						|| suffix.contentEquals("docx")) {
					contentType = "application/msword";
				} else if (suffix.contentEquals("xls")
						|| suffix.contentEquals("xlsx")) {
					contentType = "application/vnd.ms-excel";
				} else if (suffix.contentEquals("pdf")) {
					contentType = "application/pdf";
				}
				downloadedFile = new DefaultStreamedContent(stream,
						"contentType", downloadedFileName);

				stream.close();
			} else {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"LÜTFEN BARKOD OLUŞTURUNUZ, BARKOD BULUNAMADI!"));
			}
		} catch (Exception e) {
			System.out.println("SpiralMachineBean.getDownloadedFile():"
					+ e.toString());
		}
		return downloadedFile;
	}

	// setters getters

	public List<StokUrunKartlari> getAllStokUrunKartlariList() {
		return allStokUrunKartlariList;
	}

	public void setAllStokUrunKartlariList(
			List<StokUrunKartlari> allStokUrunKartlariList) {
		this.allStokUrunKartlariList = allStokUrunKartlariList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public StokUrunKartlari getStokUrunKartlariForm() {
		return stokUrunKartlariForm;
	}

	public void setStokUrunKartlariForm(StokUrunKartlari stokUrunKartlariForm) {
		this.stokUrunKartlariForm = stokUrunKartlariForm;
	}

	public Long getGiris() {
		return giris;
	}

	public void setGiris(Long giris) {
		this.giris = giris;
	}

	public Long getCikis() {
		return cikis;
	}

	public void setCikis(Long cikis) {
		this.cikis = cikis;
	}

	public Long getToplam() {
		return toplam;
	}

	public void setToplam(Long toplam) {
		this.toplam = toplam;
	}

	public List<StokUrunKartlari> getAllStokUrunKartlariBarkodluList() {
		return allStokUrunKartlariBarkodluList;
	}

	public void setAllStokUrunKartlariBarkodluList(
			List<StokUrunKartlari> allStokUrunKartlariBarkodluList) {
		this.allStokUrunKartlariBarkodluList = allStokUrunKartlariBarkodluList;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getPrivateUrunImagePath() {
		return privateUrunImagePath;
	}

	public void setPrivateUrunImagePath(String privateUrunImagePath) {
		this.privateUrunImagePath = privateUrunImagePath;
	}

	public String getUrunImageDeployPath() {
		return urunImageDeployPath;
	}

	public void setUrunImageDeployPath(String urunImageDeployPath) {
		this.urunImageDeployPath = urunImageDeployPath;
	}

	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	/**
	 * @param downloadedFile
	 *            the downloadedFile to set
	 */
	public void setDownloadedFile(StreamedContent downloadedFile) {
		this.downloadedFile = downloadedFile;
	}

	// public List<StokUrunKartlari> getKritikStokList() {
	// return kritikStokList;
	// }
	//
	// public void setKritikStokList(List<StokUrunKartlari> kritikStokList) {
	// this.kritikStokList = kritikStokList;
	// }

}
