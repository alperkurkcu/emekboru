package com.emekboru.beans.order;

import java.io.Serializable;

public class CekmeVisibleFields implements Serializable{

	private static final long serialVersionUID = -6790868117902958858L;

	private boolean cekmeMin = false;
	private boolean cekmeMax = false;

	private boolean akmaMin = false;
	private boolean akmaMax = false;

	private boolean uzamaMin = false;
	private boolean uzamaMax = false;
	
	private boolean akmaOverCekmeMin = false;
	private boolean akmaOverCekmeMax = false;

	public CekmeVisibleFields(){
		
	}

	protected void reset(){
		
		cekmeMin = false;
		cekmeMax = false;
		uzamaMin = false;
		uzamaMax = false;
		akmaMin  = false;
		akmaMax  = false;
		akmaOverCekmeMax = false;
		akmaOverCekmeMin = false;
	}
	
	protected void setOver(){
		
		akmaOverCekmeMax = (akmaMax && cekmeMax) ? true:false;
		akmaOverCekmeMin = (akmaMin && cekmeMin) ? true:false;
	}

	public void setKaynakli(){

		reset();
		cekmeMin = true;
		cekmeMax = true;
		uzamaMin = true;
		uzamaMax = true;
		setOver();
	}
	
	public void setMenine(){
		
		reset();
		cekmeMin = true;
		cekmeMax = true;
		uzamaMin = true;
		uzamaMax = true;
		akmaMin  = true;
		akmaMax  = true;
		setOver();
	}
	
	public void setMboyuna(){
		
		reset();
		cekmeMin = true;
		cekmeMax = true;
		uzamaMin = true;
		uzamaMax = true;
		akmaMin  = true;
		akmaMax  = true;
		setOver();
	}
	
	public void setBant(){
		
		reset();
		cekmeMin = true;
		cekmeMax = true;
		uzamaMin = true;
		uzamaMax = true;
		setOver();
	}
	
	public void setTamKaynak(){
		
		reset();
		cekmeMin = true;
		cekmeMax = true;
		uzamaMin = true;
		uzamaMax = true;
		akmaMin  = true;
		akmaMax  = true;
		setOver();
	}
	
	// ******************************************************************* //
	// 					        GETTERS AND SETTERS         			   //
	// ******************************************************************* //

	public boolean getCekmeMin() {
		return cekmeMin;
	}

	public void setCekmeMin(boolean cekmeMin) {
		this.cekmeMin = cekmeMin;
	}

	public boolean getCekmeMax() {
		return cekmeMax;
	}

	public void setCekmeMax(boolean cekmeMax) {
		this.cekmeMax = cekmeMax;
	}

	public boolean getAkmaMin() {
		return akmaMin;
	}

	public void setAkmaMin(boolean akmaMin) {
		this.akmaMin = akmaMin;
	}

	public boolean getAkmaMax() {
		return akmaMax;
	}

	public void setAkmaMax(boolean akmaMax) {
		this.akmaMax = akmaMax;
	}

	public boolean getUzamaMin() {
		return uzamaMin;
	}

	public void setUzamaMin(boolean uzamaMin) {
		this.uzamaMin = uzamaMin;
	}

	public boolean getUzamaMax() {
		return uzamaMax;
	}

	public void setUzamaMax(boolean uzamaMax) {
		this.uzamaMax = uzamaMax;
	}

	public boolean getAkmaOverCekmeMin() {
		return akmaOverCekmeMin;
	}

	public void setAkmaOverCekmeMin(boolean akmaOverCekmeMin) {
		this.akmaOverCekmeMin = akmaOverCekmeMin;
	}

	public boolean getAkmaOverCekmeMax() {
		return akmaOverCekmeMax;
	}

	public void setAkmaOverCekmeMax(boolean akmaOverCekmeMax) {
		this.akmaOverCekmeMax = akmaOverCekmeMax;
	}

}
