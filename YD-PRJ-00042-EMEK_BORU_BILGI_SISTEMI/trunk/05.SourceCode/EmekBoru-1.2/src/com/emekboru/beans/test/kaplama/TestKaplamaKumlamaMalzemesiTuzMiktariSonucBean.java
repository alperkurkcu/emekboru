package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaKumlamaMalzemesiTuzMiktariSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaKumlamaMalzemesiTuzMiktariSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaplamaKumlamaMalzemesiTuzMiktariSonucBean")
@ViewScoped
public class TestKaplamaKumlamaMalzemesiTuzMiktariSonucBean implements
		Serializable {
	private static final long serialVersionUID = 1L;

	private TestKaplamaKumlamaMalzemesiTuzMiktariSonuc testKaplamaKumlamaMalzemesiTuzMiktariSonucForm = new TestKaplamaKumlamaMalzemesiTuzMiktariSonuc();
	private List<TestKaplamaKumlamaMalzemesiTuzMiktariSonuc> allTestKaplamaKumlamaMalzemesiTuzMiktariSonucList = new ArrayList<TestKaplamaKumlamaMalzemesiTuzMiktariSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateTestKaplamaKumlamaMalzemesiTuzMiktariSonuc(
			ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaKumlamaMalzemesiTuzMiktariSonucManager manager = new TestKaplamaKumlamaMalzemesiTuzMiktariSonucManager();

		if (testKaplamaKumlamaMalzemesiTuzMiktariSonucForm.getId() == null) {

			testKaplamaKumlamaMalzemesiTuzMiktariSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaKumlamaMalzemesiTuzMiktariSonucForm
					.setEkleyenKullanici(userBean.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaKumlamaMalzemesiTuzMiktariSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaKumlamaMalzemesiTuzMiktariSonucForm
					.setBagliTestId(bagliTest);
			testKaplamaKumlamaMalzemesiTuzMiktariSonucForm
					.setBagliGlobalId(bagliTest);

			manager.enterNew(testKaplamaKumlamaMalzemesiTuzMiktariSonucForm);

			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);

			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaKumlamaMalzemesiTuzMiktariSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaKumlamaMalzemesiTuzMiktariSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			manager.updateEntity(testKaplamaKumlamaMalzemesiTuzMiktariSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addTestKaplamaKumlamaMalzemesiTuzMiktariSonuc() {

		testKaplamaKumlamaMalzemesiTuzMiktariSonucForm = new TestKaplamaKumlamaMalzemesiTuzMiktariSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaKumlamaMalzemesiTuzMiktariSonucListener(
			SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaKumlamaMalzemesiTuzMiktariSonucList = TestKaplamaKumlamaMalzemesiTuzMiktariSonucManager
				.getAllTestKaplamaKumlamaMalzemesiTuzMiktariSonuc(globalId,
						pipeId);
		testKaplamaKumlamaMalzemesiTuzMiktariSonucForm = new TestKaplamaKumlamaMalzemesiTuzMiktariSonuc();
	}

	public void deleteTestKaplamaKumlamaMalzemesiTuzMiktariSonuc() {

		bagliTestId = testKaplamaKumlamaMalzemesiTuzMiktariSonucForm
				.getBagliGlobalId().getId();

		if (testKaplamaKumlamaMalzemesiTuzMiktariSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaKumlamaMalzemesiTuzMiktariSonucManager manager = new TestKaplamaKumlamaMalzemesiTuzMiktariSonucManager();
		manager.delete(testKaplamaKumlamaMalzemesiTuzMiktariSonucForm);
		testKaplamaKumlamaMalzemesiTuzMiktariSonucForm = new TestKaplamaKumlamaMalzemesiTuzMiktariSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters
	public TestKaplamaKumlamaMalzemesiTuzMiktariSonuc getTestKaplamaKumlamaMalzemesiTuzMiktariSonucForm() {
		return testKaplamaKumlamaMalzemesiTuzMiktariSonucForm;
	}

	public void setTestKaplamaKumlamaMalzemesiTuzMiktariSonucForm(
			TestKaplamaKumlamaMalzemesiTuzMiktariSonuc testKaplamaKumlamaMalzemesiTuzMiktariSonucForm) {
		this.testKaplamaKumlamaMalzemesiTuzMiktariSonucForm = testKaplamaKumlamaMalzemesiTuzMiktariSonucForm;
	}

	public List<TestKaplamaKumlamaMalzemesiTuzMiktariSonuc> getAllTestKaplamaKumlamaMalzemesiTuzMiktariSonucList() {
		return allTestKaplamaKumlamaMalzemesiTuzMiktariSonucList;
	}

	public void setAllTestKaplamaKumlamaMalzemesiTuzMiktariSonucList(
			List<TestKaplamaKumlamaMalzemesiTuzMiktariSonuc> allTestKaplamaKumlamaMalzemesiTuzMiktariSonucList) {
		this.allTestKaplamaKumlamaMalzemesiTuzMiktariSonucList = allTestKaplamaKumlamaMalzemesiTuzMiktariSonucList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
