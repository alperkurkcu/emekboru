package com.emekboru.beans.machine;

import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Machine;
import com.emekboru.jpa.MachinePart;
import com.emekboru.jpa.MachinePartCategory;
import com.emekboru.jpa.MachineType;
import com.emekboru.jpa.MaintenanceActivity;
import com.emekboru.jpa.MaintenancePlanType;
import com.emekboru.jpaman.MachineManager;
import com.emekboru.jpaman.MachinePartCategoryManager;
import com.emekboru.jpaman.MachinePartManager;
import com.emekboru.jpaman.MachineTypeManager;
import com.emekboru.jpaman.MaintenanceActivityManager;
import com.emekboru.jpaman.MaintenancePlanTypeManager;
import com.emekboru.utils.FacesContextUtils;


@ManagedBean(name = "machineUtilsListBean")
@SessionScoped
public class MachineUtilsListBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EJB
	private ConfigBean config;
	
	private static List<MachineType> allMachineTypeList;	
	private static List<MaintenancePlanType> allMaintenancePlanTypeList;
	private static List<MaintenanceActivity> allMaintenanceActivities;
	private static List<MachinePartCategory> allMachinePartCategories;
	private static List<MachinePart> allMachineParts;
	private static List<Machine>        allMachineList;
	
	public MachineUtilsListBean(){
		/*allMachineTypeList = new ArrayList<MachineType>();
		allMaintenancePlanTypeList = new ArrayList<MaintenancePlanType>();
		allMaintenanceActivities = new ArrayList<MaintenanceActivity>();
		allMachinePartCategories = new ArrayList<MachinePartCategory>();*/	
	}

	
	public static void loadMachineTypeList(){
		
		MachineTypeManager machineTypeManager = new MachineTypeManager();
		allMachineTypeList = machineTypeManager.findAll(MachineType.class);
	}
	
	public static void loadMaintenancePlanTypeList(){
		
		MaintenancePlanTypeManager planTypeManager = new MaintenancePlanTypeManager();
		allMaintenancePlanTypeList = planTypeManager.findAll(MaintenancePlanType.class);
	}
	
	public static void loadMaintenanceActivitiesList(){
		
		MaintenanceActivityManager activityManager = new MaintenanceActivityManager();
		allMaintenanceActivities = activityManager.findAll(MaintenanceActivity.class);
	}
	
	public static void loadMachinePartCategories(){
		
		MachinePartCategoryManager categoryManager = new MachinePartCategoryManager();
		allMachinePartCategories = categoryManager.findAll(MachinePartCategory.class);
	}
	
	public static void loadMachineParts(){
		
		MachinePartManager partManager = new MachinePartManager();
		allMachineParts = partManager.findAll(MachinePart.class);
	}
	
	public static void loadMachines(){
		
		MachineManager machineManager = new MachineManager();
		allMachineList = machineManager.findAll(Machine.class);
	}
	
	public void goToMachineUtils(){
		
		loadMachineTypeList();
		loadMaintenancePlanTypeList();
		loadMaintenanceActivitiesList();
		loadMachinePartCategories();
		loadMachines();
		loadMachineParts();
		FacesContextUtils.redirect(config.getPageUrl().MACHINE_UTILS_PAGE);
	}
	
	public void goToNewMachineParts(){
		
		loadMachinePartCategories();
		loadMachines();
		loadMachineParts();
		FacesContextUtils.redirect(config.getPageUrl().NEW_MACHINE_PART_PAGE);
	}


	public void goToManageMachineUtils(){	
		loadMachineTypeList();
		loadMaintenancePlanTypeList();
		loadMaintenanceActivitiesList();
		loadMachinePartCategories();
		loadMachines();
		loadMachineParts();
		FacesContextUtils.redirect(config.getPageUrl().MANAGE_MACHINE_UTILS_PAGE);
	}
	
	
	public static void setAllMachineTypes() {
		
		MachineTypeManager machineTypeManager = new MachineTypeManager();
		allMachineTypeList = machineTypeManager.findAll(MachineType.class);		
	}
	
	
	public static void setAllMaintenancePlanTypes() {
		
		MaintenancePlanTypeManager planTypeManager = new MaintenancePlanTypeManager();
		allMaintenancePlanTypeList = planTypeManager.findAll(MaintenancePlanType.class);
	}
	
	public static void setAllMaintenanceActivities() {
		
		MaintenanceActivityManager activityManager = new MaintenanceActivityManager();
		allMaintenanceActivities = activityManager.findAll(MaintenanceActivity.class);
	}
	
	public static void setAllMachinePartCategories() {
		
		MachinePartCategoryManager categoryManager = new MachinePartCategoryManager();
		allMachinePartCategories = categoryManager.findAll(MachinePartCategory.class);
	}
	
	public static void setAllMachineParts() {
		
		MachinePartManager partManager = new MachinePartManager();
		allMachineParts = partManager.findAll(MachinePart.class);
	}
	
	public static void setAllMachines() {
		
		MachineManager machineManager = new MachineManager();
		allMachineList = machineManager.findAll(Machine.class);
	}
	
	
	/*
	 * 
	 * GETTERS AND SETTERS
	 * 
	 */

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public List<MachineType> getAllMachineTypeList() {
		return allMachineTypeList;
	}


	public void setAllMachineTypeList(List<MachineType> allMachineTypeList) {
		MachineUtilsListBean.allMachineTypeList = allMachineTypeList;
	}


	public List<MaintenancePlanType> getAllMaintenancePlanTypeList() {
		return allMaintenancePlanTypeList;
	}


	public void setAllMaintenancePlanTypeList(
			List<MaintenancePlanType> allMaintenancePlanTypeList) {
		MachineUtilsListBean.allMaintenancePlanTypeList = allMaintenancePlanTypeList;
	}
	
	public List<MaintenanceActivity> getAllMaintenanceActivities() {
		return allMaintenanceActivities;
	}


	public void setAllMaintenanceActivities(
			List<MaintenanceActivity> allMaintenanceActivities) {
		MachineUtilsListBean.allMaintenanceActivities = allMaintenanceActivities;
	}
	
	public List<MachinePartCategory> getAllMachinePartCategories() {
		return allMachinePartCategories;
	}


	public void setAllMachinePartCategories(
			List<MachinePartCategory> allMachinePartCategories) {
		MachineUtilsListBean.allMachinePartCategories = allMachinePartCategories;
	}
	
	public List<MachinePart> getAllMachineParts() {
		return allMachineParts;
	}


	public void setAllMachineParts(
			List<MachinePart> allMachineParts) {
		MachineUtilsListBean.allMachineParts = allMachineParts;
	}
	
	
	/**
	 * @return the allMachineList
	 */
	public List<Machine> getAllMachineList() {
		return allMachineList;
	}


	/**
	 * @param allMachineList the allMachineList to set
	 */
	public void setAllMachineList(List<Machine> allMachineList) {
		MachineUtilsListBean.allMachineList = allMachineList;
	}

}
