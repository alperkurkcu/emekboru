package com.emekboru.beans.machine;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.primefaces.context.RequestContext;

import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.PeriodicPlan;
import com.emekboru.jpaman.PeriodicPlanManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "periodicPlanBean")
@ViewScoped
public class PeriodicPlanBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	private PeriodicPlan newPlan;
	private PeriodicPlan selectedPlan;
	private List<PeriodicPlan> allPlanList;

	private TimeZone SystemTimeZone = TimeZone.getDefault();

	public PeriodicPlanBean() {

		newPlan = new PeriodicPlan();
		selectedPlan = new PeriodicPlan();
		PeriodicPlanManager planManager = new PeriodicPlanManager();
		// allPlanList = (List<PeriodicPlan>)
		// planManager.findAll(PeriodicPlan.class);

		allPlanList = planManager.findAllOrderedForSchedule();

		System.err.println(allPlanList.size());
	}

	public void submitNewPlan(ActionEvent event) {

		PeriodicPlanManager planManager = new PeriodicPlanManager();
		List<PeriodicPlan> planList = (List<PeriodicPlan>) planManager
				.findByField(PeriodicPlan.class, "machine.machineId", newPlan
						.getMachine().getMachineId());

		// check if there is a periodic plan with the same machine id

		if (planList.size() > 0) {

			for (PeriodicPlan plan : planList) {

				// if exists then check if these periodic plans have the same
				// machine part id

				if (plan.getMachinePart().getMachinePartId() == newPlan
						.getMachinePart().getMachinePartId()) {

					// if any record has the same machine part id then give the
					// warning and exit

					newPlan.getMachine().setName("");
					newPlan.getMachinePart().setPartName("");
					FacesContextUtils.addErrorMessage("ItemExistMessage");

					return;
				}
			}
		}

		if (newPlan != null && newPlan.getPeriodicPlanId() != null)
			planManager.updateEntity(newPlan);
		else {
			Date startDate = newPlan.getStartDate();
			Date endDate = newPlan.getEndDate();

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(startDate);
			int startMonth = calendar.get(Calendar.MONTH) + 1;

			calendar.clear();
			calendar.setTime(endDate);
			int endMonth = calendar.get(Calendar.MONTH) + 1;

			if (startMonth != endMonth) {
				FacesContextUtils.addErrorMessage("MonthsNotMatchMessage");
				return;
			}

			int incrementBy = Integer.parseInt(newPlan.getPlanPeriod());

			for (int i = startMonth; i <= 12; i += incrementBy) {
				calendar.clear();
				calendar.setTime(startDate);
				calendar.add(Calendar.MONTH, i - startMonth);
				Date planStartDate = calendar.getTime();

				calendar.clear();
				calendar.setTime(endDate);
				calendar.add(Calendar.MONTH, i - startMonth);
				Date planEndDate = calendar.getTime();

				switch (i) {
				case 1:
					newPlan.setJanStartDate(planStartDate);
					newPlan.setJanEndDate(planEndDate);
					break;
				case 2:
					newPlan.setFebStartDate(planStartDate);
					newPlan.setFebEndDate(planEndDate);
					break;
				case 3:
					newPlan.setMarStartDate(planStartDate);
					newPlan.setMarEndDate(planEndDate);
					break;
				case 4:
					newPlan.setAprStartDate(planStartDate);
					newPlan.setAprEndDate(planEndDate);
					break;
				case 5:
					newPlan.setMayStartDate(planStartDate);
					newPlan.setMayEndDate(planEndDate);
					break;
				case 6:
					newPlan.setJunStartDate(planStartDate);
					newPlan.setJunEndDate(planEndDate);
					break;
				case 7:
					newPlan.setJulStartDate(planStartDate);
					newPlan.setJulEndDate(planEndDate);
					break;
				case 8:
					newPlan.setAugStartDate(planStartDate);
					newPlan.setAugEndDate(planEndDate);
					break;
				case 9:
					newPlan.setSepStartDate(planStartDate);
					newPlan.setSepEndDate(planEndDate);
					break;
				case 10:
					newPlan.setOctStartDate(planStartDate);
					newPlan.setOctEndDate(planEndDate);
					break;
				case 11:
					newPlan.setNovStartDate(planStartDate);
					newPlan.setNovEndDate(planEndDate);
					break;
				case 12:
					newPlan.setDecStartDate(planStartDate);
					newPlan.setDecEndDate(planEndDate);
					break;
				}
			}

			planManager.enterNew(newPlan);
		}

		FacesContextUtils.addInfoMessage("SubmitAddMessage");

		MachineMaintenanceBean bean = (MachineMaintenanceBean) FacesContextUtils
				.getSessionBean("machineMaintenanceBean");
		bean.loadAllPeriodicPlans();
		newPlan = new PeriodicPlan();
	}

	public void cancelNewPlan(ActionEvent event) {
		newPlan = new PeriodicPlan();
	}

	public void beforeUpdate(PeriodicPlan plan) {

		selectedPlan = plan;
	}

	public void updatePlan(ActionEvent event) {

		if (selectedPlan.getPeriodicPlanId() == null
				|| selectedPlan.getPeriodicPlanId() <= 0) {

			return;
		}

		Date startDate = selectedPlan.getStartDate();
		Date endDate = selectedPlan.getEndDate();

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(startDate);
		int startMonth = calendar.get(Calendar.MONTH) + 1;

		calendar.clear();
		calendar.setTime(endDate);
		int endMonth = calendar.get(Calendar.MONTH) + 1;

		if (startMonth != endMonth) {
			FacesContextUtils.addErrorMessage("MonthsNotMatchMessage");
			return;
		}

		selectedPlan.setJanStartDate(null);
		selectedPlan.setJanEndDate(null);
		selectedPlan.setFebStartDate(null);
		selectedPlan.setFebEndDate(null);
		selectedPlan.setMarStartDate(null);
		selectedPlan.setMarEndDate(null);
		selectedPlan.setAprStartDate(null);
		selectedPlan.setAprEndDate(null);
		selectedPlan.setMayStartDate(null);
		selectedPlan.setMayEndDate(null);
		selectedPlan.setJunStartDate(null);
		selectedPlan.setJunEndDate(null);
		selectedPlan.setJulStartDate(null);
		selectedPlan.setJulEndDate(null);
		selectedPlan.setAugStartDate(null);
		selectedPlan.setAugEndDate(null);
		selectedPlan.setSepStartDate(null);
		selectedPlan.setSepEndDate(null);
		selectedPlan.setOctStartDate(null);
		selectedPlan.setOctEndDate(null);
		selectedPlan.setNovStartDate(null);
		selectedPlan.setNovEndDate(null);
		selectedPlan.setDecStartDate(null);
		selectedPlan.setDecEndDate(null);

		int incrementBy = Integer.parseInt(selectedPlan.getPlanPeriod());

		for (int i = startMonth; i <= 12; i += incrementBy) {
			calendar.clear();
			calendar.setTime(startDate);
			calendar.add(Calendar.MONTH, i - startMonth);
			Date planStartDate = calendar.getTime();

			calendar.clear();
			calendar.setTime(endDate);
			calendar.add(Calendar.MONTH, i - startMonth);
			Date planEndDate = calendar.getTime();

			switch (i) {
			case 1:
				selectedPlan.setJanStartDate(planStartDate);
				selectedPlan.setJanEndDate(planEndDate);
				break;
			case 2:
				selectedPlan.setFebStartDate(planStartDate);
				selectedPlan.setFebEndDate(planEndDate);
				break;
			case 3:
				selectedPlan.setMarStartDate(planStartDate);
				selectedPlan.setMarEndDate(planEndDate);
				break;
			case 4:
				selectedPlan.setAprStartDate(planStartDate);
				selectedPlan.setAprEndDate(planEndDate);
				break;
			case 5:
				selectedPlan.setMayStartDate(planStartDate);
				selectedPlan.setMayEndDate(planEndDate);
				break;
			case 6:
				selectedPlan.setJunStartDate(planStartDate);
				selectedPlan.setJunEndDate(planEndDate);
				break;
			case 7:
				selectedPlan.setJulStartDate(planStartDate);
				selectedPlan.setJulEndDate(planEndDate);
				break;
			case 8:
				selectedPlan.setAugStartDate(planStartDate);
				selectedPlan.setAugEndDate(planEndDate);
				break;
			case 9:
				selectedPlan.setSepStartDate(planStartDate);
				selectedPlan.setSepEndDate(planEndDate);
				break;
			case 10:
				selectedPlan.setOctStartDate(planStartDate);
				selectedPlan.setOctEndDate(planEndDate);
				break;
			case 11:
				selectedPlan.setNovStartDate(planStartDate);
				selectedPlan.setNovEndDate(planEndDate);
				break;
			case 12:
				selectedPlan.setDecStartDate(planStartDate);
				selectedPlan.setDecEndDate(planEndDate);
				break;
			}
		}

		PeriodicPlanManager planManager = new PeriodicPlanManager();
		planManager.updateEntity(selectedPlan);
		FacesContextUtils.addInfoMessage("UpdateItemMessage");

		RequestContext rc = RequestContext.getCurrentInstance();
		rc.execute("updatePeriodicPlanDialogWidget.hide()");

		MachineMaintenanceBean bean = (MachineMaintenanceBean) FacesContextUtils
				.getSessionBean("machineMaintenanceBean");
		bean.loadAllPeriodicPlans();
		selectedPlan = new PeriodicPlan();
	}

	public void beforeDelete(PeriodicPlan plan) {

		selectedPlan = plan;
	}

	public void deletePlan(ActionEvent event) {

		if (selectedPlan.getPeriodicPlanId() == null
				|| selectedPlan.getPeriodicPlanId() <= 0) {

			return;
		}

		PeriodicPlanManager planManager = new PeriodicPlanManager();
		planManager.deleteByField(PeriodicPlan.class, "periodicPlanId",
				selectedPlan.getPeriodicPlanId());
		FacesContextUtils.addWarnMessage("DeletedMessage");

		RequestContext rc = RequestContext.getCurrentInstance();
		rc.execute("deletePeriodicPlanConfirmDialogWidget.hide()");

		MachineMaintenanceBean bean = (MachineMaintenanceBean) FacesContextUtils
				.getSessionBean("machineMaintenanceBean");
		bean.loadAllPeriodicPlans();
		selectedPlan = new PeriodicPlan();
	}

	public void beforeDisplayPlanDates(PeriodicPlan plan) {

		selectedPlan = plan;
	}

	public void updatePlanDates(ActionEvent event) {

		if (selectedPlan.getPeriodicPlanId() == null
				|| selectedPlan.getPeriodicPlanId() <= 0) {

			return;
		}

		selectedPlan = setModifiedDateAppropriately(1, "start",
				selectedPlan.getJanStartDate(), selectedPlan);
		selectedPlan = setModifiedDateAppropriately(1, "end",
				selectedPlan.getJanEndDate(), selectedPlan);
		selectedPlan = setModifiedDateAppropriately(2, "start",
				selectedPlan.getFebStartDate(), selectedPlan);
		selectedPlan = setModifiedDateAppropriately(2, "end",
				selectedPlan.getFebEndDate(), selectedPlan);
		selectedPlan = setModifiedDateAppropriately(3, "start",
				selectedPlan.getMarStartDate(), selectedPlan);
		selectedPlan = setModifiedDateAppropriately(3, "end",
				selectedPlan.getMarEndDate(), selectedPlan);
		selectedPlan = setModifiedDateAppropriately(4, "start",
				selectedPlan.getAprStartDate(), selectedPlan);
		selectedPlan = setModifiedDateAppropriately(4, "end",
				selectedPlan.getAprEndDate(), selectedPlan);
		selectedPlan = setModifiedDateAppropriately(5, "start",
				selectedPlan.getMayStartDate(), selectedPlan);
		selectedPlan = setModifiedDateAppropriately(5, "end",
				selectedPlan.getMayEndDate(), selectedPlan);
		selectedPlan = setModifiedDateAppropriately(6, "start",
				selectedPlan.getJunStartDate(), selectedPlan);
		selectedPlan = setModifiedDateAppropriately(6, "end",
				selectedPlan.getJunEndDate(), selectedPlan);
		selectedPlan = setModifiedDateAppropriately(7, "start",
				selectedPlan.getJulStartDate(), selectedPlan);
		selectedPlan = setModifiedDateAppropriately(7, "end",
				selectedPlan.getJulEndDate(), selectedPlan);
		selectedPlan = setModifiedDateAppropriately(8, "start",
				selectedPlan.getAugStartDate(), selectedPlan);
		selectedPlan = setModifiedDateAppropriately(8, "end",
				selectedPlan.getAugEndDate(), selectedPlan);
		selectedPlan = setModifiedDateAppropriately(9, "start",
				selectedPlan.getSepStartDate(), selectedPlan);
		selectedPlan = setModifiedDateAppropriately(9, "end",
				selectedPlan.getSepEndDate(), selectedPlan);
		selectedPlan = setModifiedDateAppropriately(10, "start",
				selectedPlan.getOctStartDate(), selectedPlan);
		selectedPlan = setModifiedDateAppropriately(10, "end",
				selectedPlan.getOctEndDate(), selectedPlan);
		selectedPlan = setModifiedDateAppropriately(11, "start",
				selectedPlan.getNovStartDate(), selectedPlan);
		selectedPlan = setModifiedDateAppropriately(11, "end",
				selectedPlan.getNovEndDate(), selectedPlan);
		selectedPlan = setModifiedDateAppropriately(12, "start",
				selectedPlan.getDecStartDate(), selectedPlan);
		selectedPlan = setModifiedDateAppropriately(12, "end",
				selectedPlan.getDecEndDate(), selectedPlan);

		selectedPlan = setStartAndEndDates(selectedPlan);

		PeriodicPlanManager planManager = new PeriodicPlanManager();
		planManager.updateEntity(selectedPlan);
		FacesContextUtils.addInfoMessage("UpdateItemMessage");

		RequestContext rc = RequestContext.getCurrentInstance();
		rc.execute("viewPeriodicPlanDatesDialogWidget.hide()");
		
		allPlanList = planManager.findAllOrdered(PeriodicPlan.class, "periodicPlanId");

		MachineMaintenanceBean bean = (MachineMaintenanceBean) FacesContextUtils
				.getSessionBean("machineMaintenanceBean");
		bean.loadAllPeriodicPlans();
		selectedPlan = new PeriodicPlan();
	}

	private PeriodicPlan setModifiedDateAppropriately(int month,
			String monthTag, Date modifiedDate, PeriodicPlan plan) {

		if (modifiedDate != null) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(modifiedDate);
			int modifiedMonth = calendar.get(Calendar.MONTH) + 1;

			if (modifiedMonth != month) {
				switch (modifiedMonth) {
				case 1:
					if (monthTag.equals("start"))
						plan.setJanStartDate(modifiedDate);
					else if (monthTag.equals("end"))
						plan.setJanEndDate(modifiedDate);

					break;

				case 2:
					if (monthTag.equals("start"))
						plan.setFebStartDate(modifiedDate);
					else if (monthTag.equals("end"))
						plan.setFebEndDate(modifiedDate);

					break;

				case 3:
					if (monthTag.equals("start"))
						plan.setMarStartDate(modifiedDate);
					else if (monthTag.equals("end"))
						plan.setMarEndDate(modifiedDate);

					break;

				case 4:
					if (monthTag.equals("start"))
						plan.setAprStartDate(modifiedDate);
					else if (monthTag.equals("end"))
						plan.setAprEndDate(modifiedDate);

					break;

				case 5:
					if (monthTag.equals("start"))
						plan.setMayStartDate(modifiedDate);
					else if (monthTag.equals("end"))
						plan.setMayEndDate(modifiedDate);

					break;

				case 6:
					if (monthTag.equals("start"))
						plan.setJunStartDate(modifiedDate);
					else if (monthTag.equals("end"))
						plan.setJunEndDate(modifiedDate);

					break;

				case 7:
					if (monthTag.equals("start"))
						plan.setJulStartDate(modifiedDate);
					else if (monthTag.equals("end"))
						plan.setJulEndDate(modifiedDate);

					break;

				case 8:
					if (monthTag.equals("start"))
						plan.setAugStartDate(modifiedDate);
					else if (monthTag.equals("end"))
						plan.setAugEndDate(modifiedDate);

					break;

				case 9:
					if (monthTag.equals("start"))
						plan.setSepStartDate(modifiedDate);
					else if (monthTag.equals("end"))
						plan.setSepEndDate(modifiedDate);

					break;

				case 10:
					if (monthTag.equals("start"))
						plan.setOctStartDate(modifiedDate);
					else if (monthTag.equals("end"))
						plan.setOctEndDate(modifiedDate);

					break;

				case 11:
					if (monthTag.equals("start"))
						plan.setNovStartDate(modifiedDate);
					else if (monthTag.equals("end"))
						plan.setNovEndDate(modifiedDate);

					break;

				case 12:
					if (monthTag.equals("start"))
						plan.setDecStartDate(modifiedDate);
					else if (monthTag.equals("end"))
						plan.setDecEndDate(modifiedDate);

					break;
				}

				switch (month) {
				case 1:
					if (monthTag.equals("start"))
						plan.setJanStartDate(null);
					else if (monthTag.equals("end"))
						plan.setJanEndDate(null);

					break;

				case 2:
					if (monthTag.equals("start"))
						plan.setFebStartDate(null);
					else if (monthTag.equals("end"))
						plan.setFebEndDate(null);

					break;

				case 3:
					if (monthTag.equals("start"))
						plan.setMarStartDate(null);
					else if (monthTag.equals("end"))
						plan.setMarEndDate(null);

					break;

				case 4:
					if (monthTag.equals("start"))
						plan.setAprStartDate(null);
					else if (monthTag.equals("end"))
						plan.setAprEndDate(null);

					break;

				case 5:
					if (monthTag.equals("start"))
						plan.setMayStartDate(null);
					else if (monthTag.equals("end"))
						plan.setMayEndDate(null);

					break;

				case 6:
					if (monthTag.equals("start"))
						plan.setJunStartDate(null);
					else if (monthTag.equals("end"))
						plan.setJunEndDate(null);

					break;

				case 7:
					if (monthTag.equals("start"))
						plan.setJulStartDate(null);
					else if (monthTag.equals("end"))
						plan.setJulEndDate(null);

					break;

				case 8:
					if (monthTag.equals("start"))
						plan.setAugStartDate(null);
					else if (monthTag.equals("end"))
						plan.setAugEndDate(null);

					break;

				case 9:
					if (monthTag.equals("start"))
						plan.setSepStartDate(null);
					else if (monthTag.equals("end"))
						plan.setSepEndDate(null);

					break;

				case 10:
					if (monthTag.equals("start"))
						plan.setOctStartDate(null);
					else if (monthTag.equals("end"))
						plan.setOctEndDate(null);

					break;

				case 11:
					if (monthTag.equals("start"))
						plan.setNovStartDate(null);
					else if (monthTag.equals("end"))
						plan.setNovEndDate(null);

					break;

				case 12:
					if (monthTag.equals("start"))
						plan.setDecStartDate(null);
					else if (monthTag.equals("end"))
						plan.setDecEndDate(null);

					break;
				}
			}
		}

		return plan;
	}

	private PeriodicPlan setStartAndEndDates(PeriodicPlan plan) {

		if (plan.getJanStartDate() != null && plan.getJanEndDate() != null) {
			plan.setStartDate(plan.getJanStartDate());
			plan.setEndDate(plan.getJanEndDate());
		} else if (plan.getFebStartDate() != null
				&& plan.getFebEndDate() != null) {
			plan.setStartDate(plan.getFebStartDate());
			plan.setEndDate(plan.getFebEndDate());
		} else if (plan.getMarStartDate() != null
				&& plan.getMarEndDate() != null) {
			plan.setStartDate(plan.getMarStartDate());
			plan.setEndDate(plan.getMarEndDate());
		} else if (plan.getAprStartDate() != null
				&& plan.getAprEndDate() != null) {
			plan.setStartDate(plan.getAprStartDate());
			plan.setEndDate(plan.getAprEndDate());
		} else if (plan.getMayStartDate() != null
				&& plan.getMayEndDate() != null) {
			plan.setStartDate(plan.getMayStartDate());
			plan.setEndDate(plan.getMayEndDate());
		} else if (plan.getJunStartDate() != null
				&& plan.getJunEndDate() != null) {
			plan.setStartDate(plan.getJunStartDate());
			plan.setEndDate(plan.getJunEndDate());
		} else if (plan.getJulStartDate() != null
				&& plan.getJulEndDate() != null) {
			plan.setStartDate(plan.getJulStartDate());
			plan.setEndDate(plan.getJulEndDate());
		} else if (plan.getAugStartDate() != null
				&& plan.getAugEndDate() != null) {
			plan.setStartDate(plan.getAugStartDate());
			plan.setEndDate(plan.getAugEndDate());
		} else if (plan.getSepStartDate() != null
				&& plan.getSepEndDate() != null) {
			plan.setStartDate(plan.getSepStartDate());
			plan.setEndDate(plan.getSepEndDate());
		} else if (plan.getOctStartDate() != null
				&& plan.getOctEndDate() != null) {
			plan.setStartDate(plan.getOctStartDate());
			plan.setEndDate(plan.getOctEndDate());
		} else if (plan.getNovStartDate() != null
				&& plan.getNovEndDate() != null) {
			plan.setStartDate(plan.getNovStartDate());
			plan.setEndDate(plan.getNovEndDate());
		} else if (plan.getDecStartDate() != null
				&& plan.getDecEndDate() != null) {
			plan.setStartDate(plan.getDecStartDate());
			plan.setEndDate(plan.getDecEndDate());
		}

		return plan;
	}

	// ********************************************************** //
	// *** GETTERS AND SETTERS *** //
	// ********************************************************** //

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public PeriodicPlan getNewPlan() {
		return newPlan;
	}

	public void setNewPlan(PeriodicPlan plan) {
		this.newPlan = plan;
	}

	public PeriodicPlan getSelectedPlan() {
		Logger.getAnonymousLogger().info(
				this.getClass().getName() + ": Get Selected Plan: "
						+ selectedPlan.getPeriodicPlanId());
		return selectedPlan;
	}

	public void setSelectedPlan(PeriodicPlan plan) {
		Logger.getAnonymousLogger().info(
				this.getClass().getName() + ": Set Selected Plan: "
						+ plan.getPeriodicPlanId());
		this.selectedPlan = plan;
	}

	/**
	 * @return the allPlanList
	 */
	public List<PeriodicPlan> getAllPlanList() {
		return allPlanList;
	}

	/**
	 * @param allPlanList
	 *            the allPlanList to set
	 */
	public void setAllPlanList(List<PeriodicPlan> allPlanList) {
		this.allPlanList = allPlanList;
	}

	/**
	 * @return the systemTimeZone
	 */
	public TimeZone getSystemTimeZone() {
		return SystemTimeZone;
	}

	/**
	 * @param systemTimeZone
	 *            the systemTimeZone to set
	 */
	public void setSystemTimeZone(TimeZone systemTimeZone) {
		SystemTimeZone = systemTimeZone;
	}

}
