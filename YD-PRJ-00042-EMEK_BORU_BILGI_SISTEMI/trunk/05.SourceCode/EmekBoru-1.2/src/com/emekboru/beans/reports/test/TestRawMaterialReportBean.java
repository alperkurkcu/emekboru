/**
 * 
 */
package com.emekboru.beans.reports.test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpaman.CoatRawMaterialManager;
import com.emekboru.utils.ReportUtil;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testRawMaterialReportBean")
@ViewScoped
public class TestRawMaterialReportBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	String raporNo = "";
	String muayeneyiYapan = null;
	String hazirlayan = null;

	Integer raporVardiya;
	Date raporTarihi;
	String raporTarihiAString;
	String raporTarihiBString;

	Boolean kontrol = false;

	private String downloadedFileName;
	private StreamedContent downloadedFile;
	private String realFilePath;

	@PostConstruct
	public void init() {
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void fr190ReportXlsx(Integer prmCoatMaterialId) {

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmCoatMaterialId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		try {
			CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
			CoatRawMaterial material = new CoatRawMaterial();
			material = materialManager.loadObject(CoatRawMaterial.class,
					prmCoatMaterialId);

			if (material.getTestHammaddeGirdiSertliks().size() == 0) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"HAMMADDE TESTİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(material
					.getTestHammaddeGirdiSertliks().get(0).getEklemeZamani()));

			List<String> entranceDate = new ArrayList<String>();
			entranceDate.add(new SimpleDateFormat("dd.MM.yyyy").format(material
					.getEntranceDate()));

			List<String> testTarihi = new ArrayList<String>();
			testTarihi.add(new SimpleDateFormat("dd.MM.yyyy").format(material
					.getTestHammaddeGirdiSertliks().get(0).getTestTarihi()));

			raporNo = material.getTestHammaddeGirdiSertliks().get(0)
					.getRaporNo();
			try {
				muayeneyiYapan = material.getTestHammaddeGirdiSertliks().get(0)
						.getEkleyenEmployee().getEmployee().getFirstname()
						+ " "
						+ material.getTestHammaddeGirdiSertliks().get(0)
								.getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("material", material);
			dataMap.put("sonucs", material.getTestHammaddeGirdiSertliks());
			dataMap.put("specs", material.getTestHammaddeGirdiSertlikSpecs());
			dataMap.put("date", date);
			dataMap.put("entranceDate", entranceDate);
			dataMap.put("testTarihi", testTarihi);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil
					.jxlsExportAsXls(
							dataMap,
							"/reportformats/test/rawmaterials/FR-190.xls",
							"-" + material.getMarka() + "-"
									+ material.getPartiNo());

			downloadedFileName = "FR-190-" + material.getMarka() + "-"
					+ material.getPartiNo() + ".xls";

			getRealFilePath("/reportformats/test/rawmaterials/"
					+ downloadedFileName);

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void fr215ReportXlsx(Integer prmCoatMaterialId) {

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmCoatMaterialId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		try {
			CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
			CoatRawMaterial material = new CoatRawMaterial();
			material = materialManager.loadObject(CoatRawMaterial.class,
					prmCoatMaterialId);

			if (material.getTestHammaddeGirdiEpoksiTozEjs1s().size() == 0) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"HAMMADDE TESTİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(material
					.getTestHammaddeGirdiEpoksiTozEjs1s().get(0)
					.getEklemeZamani()));

			List<String> entranceDate = new ArrayList<String>();
			entranceDate.add(new SimpleDateFormat("dd.MM.yyyy").format(material
					.getEntranceDate()));

			List<String> testTarihi = new ArrayList<String>();
			testTarihi.add(new SimpleDateFormat("dd.MM.yyyy").format(material
					.getTestHammaddeGirdiEpoksiTozEjs1s().get(0)
					.getTestTarihi()));

			raporNo = material.getTestHammaddeGirdiEpoksiTozEjs1s().get(0)
					.getRaporNo();
			try {
				muayeneyiYapan = material.getTestHammaddeGirdiEpoksiTozEjs1s()
						.get(0).getEkleyenEmployee().getEmployee()
						.getFirstname()
						+ " "
						+ material.getTestHammaddeGirdiEpoksiTozEjs1s().get(0)
								.getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("material", material);
			if (material.getTestHammaddeGirdiEpoksiTozEjs1s().size() == 3) {
				dataMap.put("sonuc1", material
						.getTestHammaddeGirdiEpoksiTozEjs1s().get(0));
				dataMap.put("sonuc2", material
						.getTestHammaddeGirdiEpoksiTozEjs1s().get(1));
				dataMap.put("sonuc3", material
						.getTestHammaddeGirdiEpoksiTozEjs1s().get(2));
			} else if (material.getTestHammaddeGirdiEpoksiTozEjs1s().size() == 2) {
				dataMap.put("sonuc1", material
						.getTestHammaddeGirdiEpoksiTozEjs1s().get(0));
				dataMap.put("sonuc2", material
						.getTestHammaddeGirdiEpoksiTozEjs1s().get(1));
			} else {
				dataMap.put("sonuc1", material
						.getTestHammaddeGirdiEpoksiTozEjs1s().get(0));
			}
			dataMap.put("date", date);
			dataMap.put("entranceDate", entranceDate);
			dataMap.put("testTarihi", testTarihi);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil
					.jxlsExportAsXls(
							dataMap,
							"/reportformats/test/rawmaterials/FR-215.xls",
							"-" + material.getMarka() + "-"
									+ material.getPartiNo());

			downloadedFileName = "FR-215-" + material.getMarka() + "-"
					+ material.getPartiNo() + ".xls";

			getRealFilePath("/reportformats/test/rawmaterials/"
					+ downloadedFileName);

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void fr228ReportXlsx(Integer prmCoatMaterialId) {

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmCoatMaterialId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		try {
			CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
			CoatRawMaterial material = new CoatRawMaterial();
			material = materialManager.loadObject(CoatRawMaterial.class,
					prmCoatMaterialId);

			if (material.getTestHammaddeGirdiEpoksiTozEta1Sonucs().size() == 0) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"HAMMADDE TESTİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(material
					.getTestHammaddeGirdiEpoksiTozEta1Sonucs().get(0)
					.getEklemeZamani()));

			List<String> entranceDate = new ArrayList<String>();
			entranceDate.add(new SimpleDateFormat("dd.MM.yyyy").format(material
					.getEntranceDate()));

			List<String> testTarihi = new ArrayList<String>();
			testTarihi.add(new SimpleDateFormat("dd.MM.yyyy").format(material
					.getTestHammaddeGirdiEpoksiTozEta1Sonucs().get(0)
					.getTestTarihi()));

			raporNo = material.getTestHammaddeGirdiEpoksiTozEta1Sonucs().get(0)
					.getRaporNo();
			try {
				muayeneyiYapan = material
						.getTestHammaddeGirdiEpoksiTozEta1Sonucs().get(0)
						.getEkleyenEmployee().getEmployee().getFirstname()
						+ " "
						+ material.getTestHammaddeGirdiEpoksiTozEta1Sonucs()
								.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("material", material);
			if (material.getTestHammaddeGirdiEpoksiTozEta1Sonucs().size() == 4) {
				dataMap.put("sonuc1", material
						.getTestHammaddeGirdiEpoksiTozEta1Sonucs().get(0));
				dataMap.put("sonuc2", material
						.getTestHammaddeGirdiEpoksiTozEta1Sonucs().get(1));
				dataMap.put("sonuc3", material
						.getTestHammaddeGirdiEpoksiTozEta1Sonucs().get(2));
				dataMap.put("sonuc4", material
						.getTestHammaddeGirdiEpoksiTozEta1Sonucs().get(3));
			} else if (material.getTestHammaddeGirdiEpoksiTozEta1Sonucs()
					.size() == 3) {
				dataMap.put("sonuc1", material
						.getTestHammaddeGirdiEpoksiTozEta1Sonucs().get(0));
				dataMap.put("sonuc2", material
						.getTestHammaddeGirdiEpoksiTozEta1Sonucs().get(1));
				dataMap.put("sonuc3", material
						.getTestHammaddeGirdiEpoksiTozEta1Sonucs().get(2));
			} else if (material.getTestHammaddeGirdiEpoksiTozEta1Sonucs()
					.size() == 2) {
				dataMap.put("sonuc1", material
						.getTestHammaddeGirdiEpoksiTozEta1Sonucs().get(0));
				dataMap.put("sonuc2", material
						.getTestHammaddeGirdiEpoksiTozEta1Sonucs().get(1));
			} else {
				dataMap.put("sonuc1", material
						.getTestHammaddeGirdiEpoksiTozEta1Sonucs().get(0));
			}
			dataMap.put("date", date);
			dataMap.put("entranceDate", entranceDate);
			dataMap.put("testTarihi", testTarihi);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil
					.jxlsExportAsXls(
							dataMap,
							"/reportformats/test/rawmaterials/FR-228.xls",
							"-" + material.getMarka() + "-"
									+ material.getPartiNo());

			downloadedFileName = "FR-228-" + material.getMarka() + "-"
					+ material.getPartiNo() + ".xls";

			getRealFilePath("/reportformats/test/rawmaterials/"
					+ downloadedFileName);

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void fr474ReportXlsx(Integer prmCoatMaterialId) {

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmCoatMaterialId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		try {
			CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
			CoatRawMaterial material = new CoatRawMaterial();
			material = materialManager.loadObject(CoatRawMaterial.class,
					prmCoatMaterialId);

			if (material.getTestHammaddeGirdiEpoksiTozEea1s().size() == 0) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"HAMMADDE TESTİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(material
					.getTestHammaddeGirdiEpoksiTozEea1s().get(0)
					.getEklemeZamani()));

			List<String> entranceDate = new ArrayList<String>();
			entranceDate.add(new SimpleDateFormat("dd.MM.yyyy").format(material
					.getEntranceDate()));

			List<String> testTarihi = new ArrayList<String>();
			testTarihi.add(new SimpleDateFormat("dd.MM.yyyy").format(material
					.getTestHammaddeGirdiEpoksiTozEea1s().get(0)
					.getTestTarihi()));

			raporNo = material.getTestHammaddeGirdiEpoksiTozEea1s().get(0)
					.getRaporNo();
			try {
				muayeneyiYapan = material.getTestHammaddeGirdiEpoksiTozEea1s()
						.get(0).getEkleyenEmployee().getEmployee()
						.getFirstname()
						+ " "
						+ material.getTestHammaddeGirdiEpoksiTozEea1s().get(0)
								.getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("material", material);
			if (material.getTestHammaddeGirdiEpoksiTozEea1s().size() == 4) {
				dataMap.put("sonuc1", material
						.getTestHammaddeGirdiEpoksiTozEea1s().get(0));
				dataMap.put("sonuc2", material
						.getTestHammaddeGirdiEpoksiTozEea1s().get(1));
				dataMap.put("sonuc3", material
						.getTestHammaddeGirdiEpoksiTozEea1s().get(2));
				dataMap.put("sonuc4", material
						.getTestHammaddeGirdiEpoksiTozEea1s().get(3));
			} else if (material.getTestHammaddeGirdiEpoksiTozEea1s().size() == 3) {
				dataMap.put("sonuc1", material
						.getTestHammaddeGirdiEpoksiTozEea1s().get(0));
				dataMap.put("sonuc2", material
						.getTestHammaddeGirdiEpoksiTozEea1s().get(1));
				dataMap.put("sonuc3", material
						.getTestHammaddeGirdiEpoksiTozEea1s().get(2));
			} else if (material.getTestHammaddeGirdiEpoksiTozEea1s().size() == 2) {
				dataMap.put("sonuc1", material
						.getTestHammaddeGirdiEpoksiTozEea1s().get(0));
				dataMap.put("sonuc2", material
						.getTestHammaddeGirdiEpoksiTozEea1s().get(1));
			} else {
				dataMap.put("sonuc1", material
						.getTestHammaddeGirdiEpoksiTozEea1s().get(0));
			}
			dataMap.put("date", date);
			dataMap.put("entranceDate", entranceDate);
			dataMap.put("testTarihi", testTarihi);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/rawmaterials/FR-474.xls",
					material.getMarka() + "-" + material.getPartiNo());

			downloadedFileName = "FR-474" + material.getMarka() + "-"
					+ material.getPartiNo() + ".xls";

			getRealFilePath("/reportformats/test/rawmaterials/"
					+ downloadedFileName);

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void fr475ReportXlsx(Integer prmCoatMaterialId) {

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmCoatMaterialId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		try {
			CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
			CoatRawMaterial material = new CoatRawMaterial();
			material = materialManager.loadObject(CoatRawMaterial.class,
					prmCoatMaterialId);

			if (material.getTestHammaddeGirdiEpoksiTozEkz1s().size() == 0) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"HAMMADDE TESTİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(material
					.getTestHammaddeGirdiEpoksiTozEkz1s().get(0)
					.getEklemeZamani()));

			List<String> entranceDate = new ArrayList<String>();
			entranceDate.add(new SimpleDateFormat("dd.MM.yyyy").format(material
					.getEntranceDate()));

			List<String> testTarihi = new ArrayList<String>();
			testTarihi.add(new SimpleDateFormat("dd.MM.yyyy").format(material
					.getTestHammaddeGirdiEpoksiTozEkz1s().get(0)
					.getTestTarihi()));

			raporNo = material.getTestHammaddeGirdiEpoksiTozEkz1s().get(0)
					.getRaporNo();
			try {
				muayeneyiYapan = material.getTestHammaddeGirdiEpoksiTozEkz1s()
						.get(0).getEkleyenEmployee().getEmployee()
						.getFirstname()
						+ " "
						+ material.getTestHammaddeGirdiEpoksiTozEkz1s().get(0)
								.getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("material", material);
			dataMap.put("sonucs", material.getTestHammaddeGirdiEpoksiTozEkz1s());
			dataMap.put("date", date);
			dataMap.put("entranceDate", entranceDate);
			dataMap.put("testTarihi", testTarihi);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil
					.jxlsExportAsXls(
							dataMap,
							"/reportformats/test/rawmaterials/FR-475.xls",
							"-" + material.getMarka() + "-"
									+ material.getPartiNo());

			downloadedFileName = "FR-475-" + material.getMarka() + "-"
					+ material.getPartiNo() + ".xls";

			getRealFilePath("/reportformats/test/rawmaterials/"
					+ downloadedFileName);

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void fr476ReportXlsx(Integer prmCoatMaterialId) {

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmCoatMaterialId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		try {
			CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
			CoatRawMaterial material = new CoatRawMaterial();
			material = materialManager.loadObject(CoatRawMaterial.class,
					prmCoatMaterialId);

			if (material.getTestHammaddeGirdiEpoksiTozEni1s().size() == 0) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"HAMMADDE TESTİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(material
					.getTestHammaddeGirdiEpoksiTozEni1s().get(0)
					.getEklemeZamani()));

			List<String> entranceDate = new ArrayList<String>();
			entranceDate.add(new SimpleDateFormat("dd.MM.yyyy").format(material
					.getEntranceDate()));

			List<String> testTarihi = new ArrayList<String>();
			testTarihi.add(new SimpleDateFormat("dd.MM.yyyy").format(material
					.getTestHammaddeGirdiEpoksiTozEni1s().get(0)
					.getTestTarihi()));

			raporNo = material.getTestHammaddeGirdiEpoksiTozEni1s().get(0)
					.getRaporNo();
			try {
				muayeneyiYapan = material.getTestHammaddeGirdiEpoksiTozEni1s()
						.get(0).getEkleyenEmployee().getEmployee()
						.getFirstname()
						+ " "
						+ material.getTestHammaddeGirdiEpoksiTozEni1s().get(0)
								.getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("material", material);
			if (material.getTestHammaddeGirdiEpoksiTozEni1s().size() == 3) {
				dataMap.put("sonuc1", material
						.getTestHammaddeGirdiEpoksiTozEni1s().get(0));
				dataMap.put("sonuc2", material
						.getTestHammaddeGirdiEpoksiTozEni1s().get(1));
				dataMap.put("sonuc3", material
						.getTestHammaddeGirdiEpoksiTozEni1s().get(2));
			} else if (material.getTestHammaddeGirdiEpoksiTozEni1s().size() == 2) {
				dataMap.put("sonuc1", material
						.getTestHammaddeGirdiEpoksiTozEni1s().get(0));
				dataMap.put("sonuc2", material
						.getTestHammaddeGirdiEpoksiTozEni1s().get(1));
			} else {
				dataMap.put("sonuc1", material
						.getTestHammaddeGirdiEpoksiTozEni1s().get(0));
			}
			dataMap.put("date", date);
			dataMap.put("entranceDate", entranceDate);
			dataMap.put("testTarihi", testTarihi);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil
					.jxlsExportAsXls(
							dataMap,
							"/reportformats/test/rawmaterials/FR-476.xls",
							"-" + material.getMarka() + "-"
									+ material.getPartiNo());

			downloadedFileName = "FR-476-" + material.getMarka() + "-"
					+ material.getPartiNo() + ".xls";

			getRealFilePath("/reportformats/test/rawmaterials/"
					+ downloadedFileName);

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void fr477ReportXlsx(Integer prmCoatMaterialId) {

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmCoatMaterialId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		try {
			CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
			CoatRawMaterial material = new CoatRawMaterial();
			material = materialManager.loadObject(CoatRawMaterial.class,
					prmCoatMaterialId);

			if (material.getTestHammaddeGirdiPdd1s().size() == 0) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"HAMMADDE TESTİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(material
					.getTestHammaddeGirdiPdd1s().get(0).getEklemeZamani()));

			List<String> entranceDate = new ArrayList<String>();
			entranceDate.add(new SimpleDateFormat("dd.MM.yyyy").format(material
					.getEntranceDate()));

			List<String> testTarihi = new ArrayList<String>();
			testTarihi.add(new SimpleDateFormat("dd.MM.yyyy").format(material
					.getTestHammaddeGirdiPdd1s().get(0).getTestTarihi()));

			raporNo = material.getTestHammaddeGirdiPdd1s().get(0).getRaporNo();
			try {
				muayeneyiYapan = material.getTestHammaddeGirdiPdd1s().get(0)
						.getEkleyenEmployee().getEmployee().getFirstname()
						+ " "
						+ material.getTestHammaddeGirdiPdd1s().get(0)
								.getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("material", material);
			dataMap.put("sonucs", material.getTestHammaddeGirdiPdd1s());
			dataMap.put("uretici", material.getTestHammaddeUreticiPdd1s());
			dataMap.put("specs", material.getTestHammaddeGirdiPdd1Specs());
			dataMap.put("date", date);
			dataMap.put("entranceDate", entranceDate);
			dataMap.put("testTarihi", testTarihi);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil
					.jxlsExportAsXls(
							dataMap,
							"/reportformats/test/rawmaterials/FR-477.xls",
							"-" + material.getMarka() + "-"
									+ material.getPartiNo());

			downloadedFileName = "FR-477-" + material.getMarka() + "-"
					+ material.getPartiNo() + ".xls";

			getRealFilePath("/reportformats/test/rawmaterials/"
					+ downloadedFileName);

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void fr478ReportXlsx(Integer prmCoatMaterialId) {

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmCoatMaterialId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		try {
			CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
			CoatRawMaterial material = new CoatRawMaterial();
			material = materialManager.loadObject(CoatRawMaterial.class,
					prmCoatMaterialId);

			if (material.getTestHammaddeGirdiOit1s().size() == 0) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"HAMMADDE TESTİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(material
					.getTestHammaddeGirdiOit1s().get(0).getEklemeZamani()));

			List<String> entranceDate = new ArrayList<String>();
			entranceDate.add(new SimpleDateFormat("dd.MM.yyyy").format(material
					.getEntranceDate()));

			List<String> testTarihi = new ArrayList<String>();
			testTarihi.add(new SimpleDateFormat("dd.MM.yyyy").format(material
					.getTestHammaddeGirdiOit1s().get(0).getTestTarihi()));

			raporNo = material.getTestHammaddeGirdiOit1s().get(0).getRaporNo();
			try {
				muayeneyiYapan = material.getTestHammaddeGirdiOit1s().get(0)
						.getEkleyenEmployee().getEmployee().getFirstname()
						+ " "
						+ material.getTestHammaddeGirdiOit1s().get(0)
								.getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("material", material);
			dataMap.put("sonucs", material.getTestHammaddeGirdiOit1s());
			dataMap.put("uretici", material.getTestHammaddeUreticiOit1s());
			dataMap.put("date", date);
			dataMap.put("entranceDate", entranceDate);
			dataMap.put("testTarihi", testTarihi);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil
					.jxlsExportAsXls(
							dataMap,
							"/reportformats/test/rawmaterials/FR-478.xls",
							"-" + material.getMarka() + "-"
									+ material.getPartiNo());

			downloadedFileName = "FR-478-" + material.getMarka() + "-"
					+ material.getPartiNo() + ".xls";

			getRealFilePath("/reportformats/test/rawmaterials/"
					+ downloadedFileName);

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void fr479ReportXlsx(Integer prmCoatMaterialId) {

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmCoatMaterialId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		try {
			CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
			CoatRawMaterial material = new CoatRawMaterial();
			material = materialManager.loadObject(CoatRawMaterial.class,
					prmCoatMaterialId);

			if (material.getTestHammaddeGirdiMfr1s().size() == 0) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"HAMMADDE TESTİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(material
					.getTestHammaddeGirdiMfr1s().get(0).getEklemeZamani()));

			List<String> entranceDate = new ArrayList<String>();
			entranceDate.add(new SimpleDateFormat("dd.MM.yyyy").format(material
					.getEntranceDate()));

			List<String> testTarihi = new ArrayList<String>();
			testTarihi.add(new SimpleDateFormat("dd.MM.yyyy").format(material
					.getTestHammaddeGirdiMfr1s().get(0).getTestTarihi()));

			raporNo = material.getTestHammaddeGirdiMfr1s().get(0).getRaporNo();
			try {
				muayeneyiYapan = material.getTestHammaddeGirdiMfr1s().get(0)
						.getEkleyenEmployee().getEmployee().getFirstname()
						+ " "
						+ material.getTestHammaddeGirdiMfr1s().get(0)
								.getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("material", material);
			dataMap.put("sonucs", material.getTestHammaddeGirdiMfr1s());
			dataMap.put("uretici", material.getTestHammaddeUreticiMfr1s());
			dataMap.put("specs", material.getTestHammaddeGirdiMfr1Specs());
			dataMap.put("date", date);
			dataMap.put("entranceDate", entranceDate);
			dataMap.put("testTarihi", testTarihi);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil
					.jxlsExportAsXls(
							dataMap,
							"/reportformats/test/rawmaterials/FR-479.xls",
							"-" + material.getMarka() + "-"
									+ material.getPartiNo());

			downloadedFileName = "FR-479-" + material.getMarka() + "-"
					+ material.getPartiNo() + ".xls";

			getRealFilePath("/reportformats/test/rawmaterials/"
					+ downloadedFileName);

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void fr482ReportXlsx(Integer prmCoatMaterialId) {

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmCoatMaterialId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		try {
			CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
			CoatRawMaterial material = new CoatRawMaterial();
			material = materialManager.loadObject(CoatRawMaterial.class,
					prmCoatMaterialId);

			if (material.getTestHammaddeGirdiEpoksiTozEty1s().size() == 0) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"HAMMADDE TESTİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(material
					.getTestHammaddeGirdiEpoksiTozEty1s().get(0)
					.getEklemeZamani()));

			List<String> entranceDate = new ArrayList<String>();
			entranceDate.add(new SimpleDateFormat("dd.MM.yyyy").format(material
					.getEntranceDate()));

			List<String> testTarihi = new ArrayList<String>();
			testTarihi.add(new SimpleDateFormat("dd.MM.yyyy").format(material
					.getTestHammaddeGirdiEpoksiTozEty1s().get(0)
					.getTestTarihi()));

			raporNo = material.getTestHammaddeGirdiEpoksiTozEty1s().get(0)
					.getRaporNo();
			try {
				muayeneyiYapan = material.getTestHammaddeGirdiEpoksiTozEty1s()
						.get(0).getEkleyenEmployee().getEmployee()
						.getFirstname()
						+ " "
						+ material.getTestHammaddeGirdiEpoksiTozEty1s().get(0)
								.getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_INFO,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("material", material);
			if (material.getTestHammaddeGirdiEpoksiTozEty1s().size() == 4) {
				dataMap.put("sonuc1", material
						.getTestHammaddeGirdiEpoksiTozEty1s().get(0));
				dataMap.put("sonuc2", material
						.getTestHammaddeGirdiEpoksiTozEty1s().get(1));
				dataMap.put("sonuc3", material
						.getTestHammaddeGirdiEpoksiTozEty1s().get(2));
				dataMap.put("sonuc4", material
						.getTestHammaddeGirdiEpoksiTozEty1s().get(3));
			} else if (material.getTestHammaddeGirdiEpoksiTozEty1s().size() == 3) {
				dataMap.put("sonuc1", material
						.getTestHammaddeGirdiEpoksiTozEty1s().get(0));
				dataMap.put("sonuc2", material
						.getTestHammaddeGirdiEpoksiTozEty1s().get(1));
				dataMap.put("sonuc3", material
						.getTestHammaddeGirdiEpoksiTozEty1s().get(2));
			} else if (material.getTestHammaddeGirdiEpoksiTozEty1s().size() == 2) {
				dataMap.put("sonuc1", material
						.getTestHammaddeGirdiEpoksiTozEty1s().get(0));
				dataMap.put("sonuc2", material
						.getTestHammaddeGirdiEpoksiTozEty1s().get(1));
			} else {
				dataMap.put("sonuc1", material
						.getTestHammaddeGirdiEpoksiTozEty1s().get(0));
			}
			dataMap.put("date", date);
			dataMap.put("entranceDate", entranceDate);
			dataMap.put("testTarihi", testTarihi);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil
					.jxlsExportAsXls(
							dataMap,
							"/reportformats/test/rawmaterials/FR-482.xls",
							"-" + material.getMarka() + "-"
									+ material.getPartiNo());

			downloadedFileName = "FR-482-" + material.getMarka() + "-"
					+ material.getPartiNo() + ".xls";

			getRealFilePath("/reportformats/test/rawmaterials/"
					+ downloadedFileName);

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	// download için
	@SuppressWarnings("resource")
	public StreamedContent getDownloadedFile() {

		try {
			if (downloadedFileName != null) {
				RandomAccessFile accessFile = new RandomAccessFile(
						realFilePath, "r");
				byte[] downloadByte = new byte[(int) accessFile.length()];
				accessFile.read(downloadByte);
				InputStream stream = new ByteArrayInputStream(downloadByte);

				String suffix = FilenameUtils.getExtension(downloadedFileName);
				String contentType = "text/plain";
				if (suffix.contentEquals("jpg") || suffix.contentEquals("png")
						|| suffix.contentEquals("gif")) {
					contentType = "image/" + contentType;
				} else if (suffix.contentEquals("doc")
						|| suffix.contentEquals("docx")) {
					contentType = "application/msword";
				} else if (suffix.contentEquals("xls")
						|| suffix.contentEquals("xlsx")) {
					contentType = "application/vnd.ms-excel";
				} else if (suffix.contentEquals("pdf")) {
					contentType = "application/pdf";
				}
				downloadedFile = new DefaultStreamedContent(stream,
						"contentType", downloadedFileName);

				stream.close();
			} else {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"LÜTFEN RAPOR OLUŞTURUNUZ, RAPOR BULUNAMADI!"));
			}
		} catch (Exception e) {
			System.out.println("testRawMaterialReportBean.getDownloadedFile():"
					+ e.toString());
		}
		return downloadedFile;
	}

	private String getRealFilePath(String filePath) {

		realFilePath = FacesContext.getCurrentInstance().getExternalContext()
				.getRealPath(filePath);

		return realFilePath;
	}

	// setters getters

	/**
	 * @return the config
	 */
	public ConfigBean getConfig() {
		return config;
	}

	/**
	 * @param config
	 *            the config to set
	 */
	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the raporVardiya
	 */
	public Integer getRaporVardiya() {
		return raporVardiya;
	}

	/**
	 * @param raporVardiya
	 *            the raporVardiya to set
	 */
	public void setRaporVardiya(Integer raporVardiya) {
		this.raporVardiya = raporVardiya;
	}

	/**
	 * @return the raporTarihi
	 */
	public Date getRaporTarihi() {
		return raporTarihi;
	}

	/**
	 * @param raporTarihi
	 *            the raporTarihi to set
	 */
	public void setRaporTarihi(Date raporTarihi) {
		this.raporTarihi = raporTarihi;
	}

	/**
	 * @return the downloadedFileName
	 */
	public String getDownloadedFileName() {
		return downloadedFileName;
	}

	/**
	 * @param downloadedFileName
	 *            the downloadedFileName to set
	 */
	public void setDownloadedFileName(String downloadedFileName) {
		this.downloadedFileName = downloadedFileName;
	}

	/**
	 * @param downloadedFile
	 *            the downloadedFile to set
	 */
	public void setDownloadedFile(StreamedContent downloadedFile) {
		this.downloadedFile = downloadedFile;
	}

}
