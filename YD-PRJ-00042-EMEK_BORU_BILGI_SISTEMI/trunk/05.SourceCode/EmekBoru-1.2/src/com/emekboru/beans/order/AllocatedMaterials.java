package com.emekboru.beans.order;

import java.util.ArrayList;
import java.util.List;

import com.emekboru.jpa.ElectrodeDustWire;
import com.emekboru.jpa.rulo.Rulo;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.ElectrodeDustWireManager;
import com.emekboru.jpaman.WhereClauseArgs;
import com.emekboru.jpaman.rulo.RuloManager;
import com.emekboru.messages.ElectrodeDustWireType;

public class AllocatedMaterials {

	// hold all the allocated materials for a specific order
	// from ManagerOrderBean.selectedOrder
	private List<Rulo> rulos;
	private List<ElectrodeDustWire> dusts;
	private List<ElectrodeDustWire> electrodes;
	private List<ElectrodeDustWire> wires;

	// this attributes will be used to hold the selected
	// materials we want to allocate
	private Rulo selectedRulo;
	private ElectrodeDustWire selectedEdw;

	public AllocatedMaterials() {

		rulos = new ArrayList<Rulo>(0);
		dusts = new ArrayList<ElectrodeDustWire>(0);
		electrodes = new ArrayList<ElectrodeDustWire>(0);
		wires = new ArrayList<ElectrodeDustWire>(0);

		selectedRulo = new Rulo();
		selectedEdw = new ElectrodeDustWire();
	}

	// load the allocated materials for the given ORDER
	public void load(SalesItem salesItem) {

		loadRulos(salesItem);
		loadEdw(salesItem);
	}

	protected void loadRulos(SalesItem salesItem) {

		RuloManager man = new RuloManager();
		rulos = man.getLoadRulos(salesItem);
	}

	// @SuppressWarnings({ "rawtypes", "unchecked" })
	// protected void loadEdw(Order order) {
	//
	// ElectrodeDustWireManager man = new ElectrodeDustWireManager();
	// ArrayList<WhereClauseArgs> conditionList = new
	// ArrayList<WhereClauseArgs>();
	// WhereClauseArgs arg1 = new WhereClauseArgs.Builder()
	// .setFieldName("orderId").setValue(order.getOrderId()).build();
	// conditionList.add(arg1);
	// List<ElectrodeDustWire> list = man.selectFromWhereQuerie(
	// ElectrodeDustWire.class, conditionList);
	//
	// for (ElectrodeDustWire edw : list) {
	//
	// if (edw.getType().equals(ElectrodeDustWireType.DUST))
	// dusts.add(edw);
	// if (edw.getType().equals(ElectrodeDustWireType.ELECTRODE))
	// electrodes.add(edw);
	// if (edw.getType().equals(ElectrodeDustWireType.WIRE))
	// wires.add(edw);
	//
	// }
	// }
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected void loadEdw(SalesItem salesItem) {

		ElectrodeDustWireManager man = new ElectrodeDustWireManager();
		ArrayList<WhereClauseArgs> conditionList = new ArrayList<WhereClauseArgs>();
		WhereClauseArgs arg1 = new WhereClauseArgs.Builder()
				.setFieldName("itemId").setValue(salesItem.getItemId()).build();
		conditionList.add(arg1);
		List<ElectrodeDustWire> list = man.selectFromWhereQuerie(
				ElectrodeDustWire.class, conditionList);

		for (ElectrodeDustWire edw : list) {

			if (edw.getType().equals(ElectrodeDustWireType.DUST))
				dusts.add(edw);
			if (edw.getType().equals(ElectrodeDustWireType.ELECTRODE))
				electrodes.add(edw);
			if (edw.getType().equals(ElectrodeDustWireType.WIRE))
				wires.add(edw);

		}
	}

	/******************************************************************************
	 ********************* GETTERS AND SETTERS ********************** /
	 ******************************************************************************/
	/*
	 * public List<Coil> getCoils() { return coils; }
	 * 
	 * public void setCoils(List<Coil> coils) { this.coils = coils; }
	 */

	public List<Rulo> getRulos() {
		return rulos;
	}

	public void setRulos(List<Rulo> rulos) {
		this.rulos = rulos;
	}

	public List<ElectrodeDustWire> getDusts() {
		return dusts;
	}

	public void setDusts(List<ElectrodeDustWire> dusts) {
		this.dusts = dusts;
	}

	public List<ElectrodeDustWire> getElectrodes() {
		return electrodes;
	}

	public void setElectrodes(List<ElectrodeDustWire> electrodes) {
		this.electrodes = electrodes;
	}

	public List<ElectrodeDustWire> getWires() {
		return wires;
	}

	public void setWires(List<ElectrodeDustWire> wires) {
		this.wires = wires;
	}

	/*
	 * public Coil getSelectedCoil() { return selectedCoil; }
	 * 
	 * public void setSelectedCoil(Coil selectedCoil) { this.selectedCoil =
	 * selectedCoil; }
	 */

	public Rulo getSelectedRulo() {
		return selectedRulo;
	}

	public void setSelectedRulo(Rulo selectedRulo) {
		this.selectedRulo = selectedRulo;
	}

	public ElectrodeDustWire getSelectedEdw() {
		return selectedEdw;
	}

	public void setSelectedEdw(ElectrodeDustWire selectedEdw) {
		this.selectedEdw = selectedEdw;
	}
}
