/**
 * 
 */
package com.emekboru.beans.satinalma;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.satinalma.SatinalmaMusteri;
import com.emekboru.jpaman.satinalma.SatinalmaMusteriManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "satinalmaMusteriBean")
@ViewScoped
public class SatinalmaMusteriBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private SatinalmaMusteri satinalmaMusteriForm = new SatinalmaMusteri();
	private List<SatinalmaMusteri> allSatinalmaMusteriList = new ArrayList<SatinalmaMusteri>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender = false;

	public SatinalmaMusteriBean() {

		SatinalmaMusteriManager manager = new SatinalmaMusteriManager();
		allSatinalmaMusteriList = manager.getAllSatinalmaMusteri();
	}

	public void addOrUpdateForm(ActionEvent e) {

		SatinalmaMusteriManager formManager = new SatinalmaMusteriManager();

		if (satinalmaMusteriForm.getId() == null) {

			satinalmaMusteriForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			satinalmaMusteriForm
					.setEkleyenKullanici(userBean.getUser().getId());

			formManager.enterNew(satinalmaMusteriForm);
			FacesContextUtils.addInfoMessage("SubmitMessage");
		} else {

			satinalmaMusteriForm.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			satinalmaMusteriForm.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			formManager.updateEntity(satinalmaMusteriForm);
			FacesContextUtils.addInfoMessage("UpdateItemMessage");
		}
		fillTestList();
	}

	public void fillTestList() {
		SatinalmaMusteriManager manager = new SatinalmaMusteriManager();
		allSatinalmaMusteriList = manager.getAllSatinalmaMusteri();
	}

	public void deleteMusteri() {

		SatinalmaMusteriManager formManager = new SatinalmaMusteriManager();

		if (satinalmaMusteriForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		formManager.delete(satinalmaMusteriForm);
		satinalmaMusteriForm = new SatinalmaMusteri();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		fillTestList();
	}

	public void satinalmaMusteriListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void addMusteri() {

		satinalmaMusteriForm = new SatinalmaMusteri();
		updateButtonRender = false;
	}

	// getters setters

	public SatinalmaMusteri getSatinalmaMusteriForm() {
		return satinalmaMusteriForm;
	}

	public void setSatinalmaMusteriForm(SatinalmaMusteri satinalmaMusteriForm) {
		this.satinalmaMusteriForm = satinalmaMusteriForm;
	}

	public List<SatinalmaMusteri> getAllSatinalmaMusteriList() {
		return allSatinalmaMusteriList;
	}

	public void setAllSatinalmaMusteriList(
			List<SatinalmaMusteri> allSatinalmaMusteriList) {
		this.allSatinalmaMusteriList = allSatinalmaMusteriList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}
}
