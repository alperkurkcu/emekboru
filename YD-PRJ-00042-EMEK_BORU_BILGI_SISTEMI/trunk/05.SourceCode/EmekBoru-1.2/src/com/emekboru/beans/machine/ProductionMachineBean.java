package com.emekboru.beans.machine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;

import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.CementMachineParameter;
import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.EpoxyMachineParameter;
import com.emekboru.jpa.Error;
import com.emekboru.jpa.Machine;
import com.emekboru.jpa.MachinePipeLink;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.PipeCoat;
import com.emekboru.jpa.PipeCoatLayer;
import com.emekboru.jpa.PolietilenMachineParameter;
import com.emekboru.jpa.sales.customer.SalesCustomer;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.CementMachineParameterManager;
import com.emekboru.jpaman.CoatRawMaterialManager;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.EpoxyMachineParameterManager;
import com.emekboru.jpaman.ErrorManager;
import com.emekboru.jpaman.Factory;
import com.emekboru.jpaman.JpqlComparativeClauses;
import com.emekboru.jpaman.MachineManager;
import com.emekboru.jpaman.MachinePipeLinkManager;
import com.emekboru.jpaman.PipeCoatLayerManager;
import com.emekboru.jpaman.PipeCoatManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.PolietilenMachineParameterManager;
import com.emekboru.jpaman.WhereClauseArgs;
import com.emekboru.jpaman.sales.customer.SalesCustomerManager;
import com.emekboru.jpaman.sales.order.SalesItemManager;
import com.emekboru.messages.IsolationType;
import com.emekboru.messages.PipeStatus;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "productionMachineBean")
@ViewScoped
public class ProductionMachineBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
	private ConfigBean config;

	private Machine currentProductionMachine;
	// private List<Order> activeOrderList;
	// private Customer currentCompany;
	// private Order selectedOrder;
	private Machine selectedMachine;

	// entegrasyon
	private List<SalesItem> activeItemList;
	private SalesCustomer currentSalesCompany;
	private SalesItem selectedSalesItem;
	// entegrasyon

	// list of current available pipes and list of the selected pipes
	private List<Pipe> freePipeList;
	private Pipe currentPipe;
	// list of pipes in a certain machine
	private List<Pipe> inMachinePipeList;
	private Pipe inMachineSelectedPipe;
	private MachinePipeLink machinePipeLink;
	private List<Error> errorList;
	private Error selectedError;

	// attributes set when completed a pipe in machine
	private double wastedAmount;
	private Date endDate;

	// coating machines related fields
	private CementMachineParameter selectedCementMachineParameter;
	private PolietilenMachineParameter selectedPolietilenMachineParameter;
	private EpoxyMachineParameter selectedEpoxyMachineParameter;
	private PipeCoat selectedCoat;
	private PipeCoat internalCoat;
	private PipeCoat externalCoat;
	private PipeCoatLayer layer1;
	private PipeCoatLayer layer2;
	private PipeCoatLayer layer3;
	private List<CoatRawMaterial> coatMaterialList;
	private List<PipeCoatLayer> internalLayerList;
	private List<PipeCoatLayer> externalLayerList;
	@ManagedProperty(value = "#{parameterListBean}")
	private ParameterListBean injectedParameterList;

	@SuppressWarnings("rawtypes")
	public ProductionMachineBean() {

		endDate = new Date();
		internalCoat = new PipeCoat();
		externalCoat = new PipeCoat();
		internalLayerList = new ArrayList<PipeCoatLayer>();
		externalLayerList = new ArrayList<PipeCoatLayer>();
		layer1 = new PipeCoatLayer();
		layer2 = new PipeCoatLayer();
		layer3 = new PipeCoatLayer();
		coatMaterialList = new ArrayList<CoatRawMaterial>();
		selectedCoat = new PipeCoat();
		currentProductionMachine = new Machine();
		// activeOrderList = new ArrayList<Order>();
		activeItemList = new ArrayList<SalesItem>();
		// currentCompany = new Customer();
		currentSalesCompany = new SalesCustomer();
		// selectedOrder = new Order();
		selectedSalesItem = new SalesItem();
		selectedMachine = new Machine();
		inMachinePipeList = new ArrayList<Pipe>();
		freePipeList = new ArrayList<Pipe>();
		inMachineSelectedPipe = new Pipe();
		machinePipeLink = new MachinePipeLink();
		errorList = new ArrayList<Error>();
		selectedError = new Error();
		currentPipe = new Pipe();
		selectedCementMachineParameter = new CementMachineParameter();
		selectedEpoxyMachineParameter = new EpoxyMachineParameter();
		selectedPolietilenMachineParameter = new PolietilenMachineParameter();
		CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
		ArrayList<WhereClauseArgs> conditionList = new ArrayList<WhereClauseArgs>();

		WhereClauseArgs<Integer> arg1 = new WhereClauseArgs.Builder<Integer>()
				.setFieldName("remainingAmount")
				.setComparativeClause(JpqlComparativeClauses.NQ_GREATER_THEN)
				.setComplexKey(false).setValue(0).build();
		conditionList.add(arg1);
		coatMaterialList = (List<CoatRawMaterial>) materialManager
				.selectFromWhereQuerie(CoatRawMaterial.class, conditionList);
	}

	// ***********************************************************************
	// //
	// ******** LOAD AVAILABLE ORDERS AND PIPES CURRENTLY IN MACHINE *********
	// //
	// ***********************************************************************
	// //
	// view the status of the machine. the pipes in and the orders where we
	// can select some pipes
	public void viewProductionMachine(Machine machine) {

		selectedMachine = machine;
		inMachineSelectedPipe = new Pipe();
		loadActiveOrders();
		loadCurrentPipesInMachine();
	}

	// load all orders that are not finished yet
	protected void loadActiveOrders() {

		if (selectedMachine == null) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		// selectedOrder = new Order();
		selectedSalesItem = new SalesItem();
		// currentCompany = new Customer();
		currentSalesCompany = new SalesCustomer();
		freePipeList = new ArrayList<Pipe>();

		// OrderManager manager = new OrderManager();
		// activeOrderList = manager.findAllNotEnded(config.getConfig());
		SalesItemManager manager = new SalesItemManager();
		// activeItemList = manager.findAll(SalesItem.class);
		activeItemList = manager.findAllOrderBy(SalesItem.class,
				"proposal.salesOrder.orderNo");
	}

	protected void loadCurrentPipesInMachine() {

		inMachinePipeList.clear();
		PipeManager pipeManager = new PipeManager();
		inMachinePipeList = pipeManager.pipesInMachine(selectedMachine);
	}

	// // return pipes of a certain order which are not in any machine and
	// // are free for processing
	// public void loadFreePipes() {
	//
	// freePipeList.clear();
	// EntityManager man = Factory.getInstance().createEntityManager();
	// selectedOrder = man.find(Order.class, selectedOrder.getOrderId());
	// selectedOrder.getPipes();
	// for (Pipe p : selectedOrder.getPipes()) {
	// if (p.getStatus() == PipeStatus.FREE)
	// freePipeList.add(p);
	// }
	//
	// man.close();
	// System.out.println("number of pipes in order: "
	// + selectedOrder.getPipes().size());
	// }

	// return pipes of a certain order which are not in any machine and
	// are free for processing
	public void loadFreePipes() {

		freePipeList.clear();
		EntityManager man = Factory.getInstance().createEntityManager();
		selectedSalesItem = man.find(SalesItem.class,
				selectedSalesItem.getItemId());
		selectedSalesItem.getPipes();
		for (Pipe p : selectedSalesItem.getPipes()) {
			if (p.getStatus() == PipeStatus.FREE)
				freePipeList.add(p);
		}

		man.close();
		System.out.println("number of pipes in order: "
				+ selectedSalesItem.getPipes().size());
	}

	// // return pipes of a certain order
	// public void loadOrderPipes() {
	//
	// CustomerManager companyManager = new CustomerManager();
	// currentCompany = (Customer) companyManager.findByField(Customer.class,
	// "customerId", selectedOrder.getCustomer().getCustomerId()).get(
	// 0);
	// if (freePipeList.size() > 0)
	// currentPipe = freePipeList.get(0);
	// else
	// currentPipe = new Pipe();
	// }
	// return pipes of a certain order
	public void loadOrderPipes() {

		SalesCustomerManager companyManager = new SalesCustomerManager();
		currentSalesCompany = (SalesCustomer) companyManager.findByField(
				SalesCustomer.class,
				"salesCustomerId",
				selectedSalesItem.getProposal().getCustomer()
						.getSalesCustomerId()).get(0);
		if (freePipeList.size() > 0)
			currentPipe = freePipeList.get(0);
		else
			currentPipe = new Pipe();
	}

	// submit the MachinePipeLink for the currentPipe
	// update the pipe status (to busy)
	public void sendPipesToMachine() {

		if (currentPipe.getPipeId() == 0)
			return;

		PipeManager pipeManager = new PipeManager();
		machinePipeLink.setMachine(selectedMachine);
		selectedMachine.getMachinePipeLinks().add(machinePipeLink);
		pipeManager.updateEntity(currentPipe);

		CommonQueries<MachinePipeLink> linkMan = new CommonQueries<MachinePipeLink>();
		machinePipeLink.setEnded(false);
		machinePipeLink.setMachine(selectedMachine);
		machinePipeLink.setPipe(currentPipe);
		linkMan.enterNew(machinePipeLink);
		freePipeList.remove(currentPipe);
		inMachinePipeList.add(currentPipe);
		currentPipe = new Pipe();
		FacesContextUtils.addInfoMessage("SubmitMessage");
		return;
	}

	// // remove a pipe which is in process in a certain machine.
	// // the link should be REMOVED as well
	// public void removePipeFromMachine() {
	//
	// if (inMachineSelectedPipe == null
	// || inMachineSelectedPipe.getPipeId() == 0) {
	// FacesContextUtils.addErrorMessage("NoSelectMessage");
	// return;
	// }
	// CustomerManager companyManager = new CustomerManager();
	// currentCompany = (Customer) companyManager.findByField(Customer.class,
	// "customerId", selectedOrder.getCustomer().getCustomerId()).get(
	// 0);
	// inMachineSelectedPipe.setStatus(PipeStatus.FREE);
	// PipeManager pipeManager = new PipeManager();
	// pipeManager.updateEntity(inMachineSelectedPipe);
	// loadCurrentPipesInMachine();
	// loadFreePipes();
	// FacesContextUtils.addWarnMessage("DeletedMessage");
	// }
	// remove a pipe which is in process in a certain machine.
	// the link should be REMOVED as well
	public void removePipeFromMachine() {

		if (inMachineSelectedPipe == null
				|| inMachineSelectedPipe.getPipeId() == 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		SalesCustomerManager companyManager = new SalesCustomerManager();
		currentSalesCompany = (SalesCustomer) companyManager.findByField(
				SalesCustomer.class,
				"salesCustomerId",
				selectedSalesItem.getProposal().getCustomer()
						.getSalesCustomerId()).get(0);
		inMachineSelectedPipe.setStatus(PipeStatus.FREE);
		PipeManager pipeManager = new PipeManager();
		pipeManager.updateEntity(inMachineSelectedPipe);
		loadCurrentPipesInMachine();
		loadFreePipes();
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	// when a pipe is completed processing in a machine we record
	// the amount that was wasted (if wasted) and we set the status to FREE
	public void finishPipeProcess() {

		PipeManager pipeManager = new PipeManager();
		if (wastedAmount >= inMachineSelectedPipe.getAgirligi()) {
			FacesContextUtils.addErrorMessage("WrongAmountMessage");
			return;
		}
		inMachineSelectedPipe.setAgirligi(inMachineSelectedPipe.getAgirligi()
				- wastedAmount);
		inMachineSelectedPipe.setStatus(PipeStatus.FREE);
		pipeManager.updateEntity(inMachineSelectedPipe);
		inMachinePipeList.remove(inMachineSelectedPipe);
		freePipeList.add(inMachineSelectedPipe);

		// update the appropriate MachinePipeLink
		MachinePipeLinkManager mplMan = new MachinePipeLinkManager();
		MachinePipeLink mpl = mplMan.findNotEnded(inMachineSelectedPipe,
				selectedMachine);
		mpl.setEndDate(endDate);
		mpl.setAmountWaste(wastedAmount);
		mpl.setEnded(true);
		mplMan.updateEntity(mpl);

		inMachineSelectedPipe = new Pipe();
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void deleteProductionMachine(Machine machine) {

		MachineManager machineManager = new MachineManager();
		ErrorManager errorManager = new ErrorManager();
		errorManager.deleteByField(Error.class, "machineId",
				machine.getMachineId());
		machineManager.deleteByField(Machine.class, "machineId",
				machine.getMachineId());
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// activeOrderList = new ArrayList<Order>();
		activeItemList = new ArrayList<SalesItem>();
		// currentCompany = new Customer();
		currentSalesCompany = new SalesCustomer();
		// selectedOrder = new Order();
		selectedSalesItem = new SalesItem();
		inMachinePipeList = new ArrayList<Pipe>();
		freePipeList = new ArrayList<Pipe>();
		inMachineSelectedPipe = new Pipe();
		machinePipeLink = new MachinePipeLink();
	}

	public void deleteError() {

		if (selectedError == null || selectedError.getErrorId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		ErrorManager errorManager = new ErrorManager();
		errorManager.deleteByField(Error.class, "errorId",
				selectedError.getErrorId());
		FacesContextUtils.addWarnMessage("DeletedMessage");
		selectedError = new Error();
	}

	/*
	 * COATING MACHINE FUNCTIONS
	 */

	public void submitCementMachineParameter() {

		CementMachineParameterManager paramManager = new CementMachineParameterManager();
		if (selectedCementMachineParameter.getCementParametersId() != null) {
			PipeCoatManager coatManager = new PipeCoatManager();
			List<PipeCoat> coatList = (List<PipeCoat>) coatManager.findByField(
					PipeCoat.class, "cementParametersId",
					selectedCementMachineParameter.getCementParametersId());
			if (coatList.size() > 0) {
				FacesContextUtils.addErrorMessage("CoatRelatedMessage");
				return;
			}
			paramManager.updateEntity(selectedCementMachineParameter);
			selectedCementMachineParameter = new CementMachineParameter();
			List<CementMachineParameter> cementList = (List<CementMachineParameter>) paramManager
					.findByField(CementMachineParameter.class, "machineId",
							selectedMachine.getMachineId());
			injectedParameterList.setCementMachineParameters(cementList);
			FacesContextUtils.addInfoMessage("UpdateItemMessage");
			return;
		}
		selectedCementMachineParameter.setMachineId(selectedMachine
				.getMachineId());
		paramManager.enterNew(selectedCementMachineParameter);
		injectedParameterList.getCementMachineParameters().add(
				selectedCementMachineParameter);
		selectedCementMachineParameter = new CementMachineParameter();
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void deleteCementMachineParameter() {

		if (selectedCementMachineParameter == null
				|| selectedCementMachineParameter.getCementParametersId() == null) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		CementMachineParameterManager paramManager = new CementMachineParameterManager();
		paramManager.delete(selectedCementMachineParameter);
		injectedParameterList.getCementMachineParameters().remove(
				selectedCementMachineParameter);
		selectedCementMachineParameter = new CementMachineParameter();
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	public void resetSelectedParam() {
		selectedCementMachineParameter = new CementMachineParameter();
		selectedEpoxyMachineParameter = new EpoxyMachineParameter();
		selectedPolietilenMachineParameter = new PolietilenMachineParameter();
	}

	public void submitPolietilenMachineParameter() {

		PolietilenMachineParameterManager paramManager = new PolietilenMachineParameterManager();
		if (selectedPolietilenMachineParameter.getPolietilenParametersId() != null) {
			PipeCoatManager coatManager = new PipeCoatManager();
			List<PipeCoat> coatList = (List<PipeCoat>) coatManager.findByField(
					PipeCoat.class, "polietilenParametersId",
					selectedPolietilenMachineParameter
							.getPolietilenParametersId());
			if (coatList.size() > 0) {
				FacesContextUtils.addErrorMessage("CoatRelatedMessage");
				return;
			}
			paramManager.updateEntity(selectedPolietilenMachineParameter);
			List<PolietilenMachineParameter> polietilenParams = (List<PolietilenMachineParameter>) paramManager
					.findByField(PolietilenMachineParameter.class, "machineId",
							selectedMachine.getMachineId());
			injectedParameterList
					.setPolietilenMachineParameters(polietilenParams);
			selectedPolietilenMachineParameter = new PolietilenMachineParameter();
			FacesContextUtils.addInfoMessage("UpdateItemMessage");
			return;
		}
		selectedPolietilenMachineParameter.setMachineId(selectedMachine
				.getMachineId());
		paramManager.enterNew(selectedPolietilenMachineParameter);
		injectedParameterList.getPolietilenMachineParameters().add(
				selectedPolietilenMachineParameter);
		selectedPolietilenMachineParameter = new PolietilenMachineParameter();
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void deletePolietilenMachineParameter() {

		if (selectedPolietilenMachineParameter == null
				|| selectedPolietilenMachineParameter
						.getPolietilenParametersId() == null) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		PolietilenMachineParameterManager paramManager = new PolietilenMachineParameterManager();
		paramManager.delete(selectedPolietilenMachineParameter);
		injectedParameterList.getPolietilenMachineParameters().remove(
				selectedPolietilenMachineParameter);
		selectedPolietilenMachineParameter = new PolietilenMachineParameter();
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	public void submitEpoxyMachineParameter() {

		EpoxyMachineParameterManager paramManager = new EpoxyMachineParameterManager();
		if (selectedEpoxyMachineParameter.getParametersId() != null) {
			PipeCoatManager coatManager = new PipeCoatManager();
			List<PipeCoat> coatList = (List<PipeCoat>) coatManager.findByField(
					PipeCoat.class, "parametersId",
					selectedEpoxyMachineParameter.getParametersId());
			if (coatList.size() > 0) {
				FacesContextUtils.addErrorMessage("CoatRelatedMessage");
				return;
			}
			paramManager.updateEntity(selectedEpoxyMachineParameter);
			selectedEpoxyMachineParameter = new EpoxyMachineParameter();
			List<EpoxyMachineParameter> epoxyParams = (List<EpoxyMachineParameter>) paramManager
					.findByField(EpoxyMachineParameter.class, "machineId",
							selectedMachine.getMachineId());
			injectedParameterList.setEpoxyMachineParameters(epoxyParams);
			FacesContextUtils.addInfoMessage("UpdateItemMessage");
			return;
		}
		selectedEpoxyMachineParameter.setMachineId(selectedMachine
				.getMachineId());
		paramManager.enterNew(selectedEpoxyMachineParameter);
		injectedParameterList.getEpoxyMachineParameters().add(
				selectedEpoxyMachineParameter);
		selectedEpoxyMachineParameter = new EpoxyMachineParameter();
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void deleteEpoxyMachineParameter() {

		if (selectedEpoxyMachineParameter == null
				|| selectedEpoxyMachineParameter.getParametersId() == null) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		EpoxyMachineParameterManager paramManager = new EpoxyMachineParameterManager();
		paramManager.delete(selectedEpoxyMachineParameter);
		injectedParameterList.getEpoxyMachineParameters().remove(
				selectedEpoxyMachineParameter);
		selectedEpoxyMachineParameter = new EpoxyMachineParameter();
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	@SuppressWarnings({ "rawtypes", "unused" })
	public void loadPipeCoat() {

		PipeCoatManager coatManager = new PipeCoatManager();
		internalCoat = new PipeCoat();
		internalLayerList = new ArrayList<PipeCoatLayer>();
		externalCoat = new PipeCoat();
		externalLayerList = new ArrayList<PipeCoatLayer>();
		injectedParameterList.setSelectedCoat(new PipeCoat());
		injectedParameterList.viewMachineParameters(selectedMachine);
		CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
		ArrayList<WhereClauseArgs> conditionList = new ArrayList<WhereClauseArgs>();

		WhereClauseArgs<Integer> arg1 = new WhereClauseArgs.Builder<Integer>()
				.setFieldName("remainingAmount")
				.setComparativeClause(JpqlComparativeClauses.NQ_GREATER_THEN)
				.setComplexKey(false).setValue(0).build();

		conditionList.add(arg1);
		coatMaterialList = materialManager.selectFromWhereQuerie(
				CoatRawMaterial.class, conditionList);
		PipeCoatLayerManager layerManager = new PipeCoatLayerManager();
		List<PipeCoat> coatList = (List<PipeCoat>) coatManager.findByField(
				PipeCoat.class, "pipeId", currentPipe.getPipeId());

		for (int i = 0; i < coatList.size(); i++) {
			if (coatList.get(i).getSide() == IsolationType.INTERNAL_COAT) {
				internalCoat = coatList.get(i);
				internalLayerList = (List<PipeCoatLayer>) layerManager
						.findByField(PipeCoatLayer.class, "pipeCoatId",
								internalCoat.getPipeCoatId());
			}
			if (coatList.get(i).getSide() == IsolationType.EXTERNAL_COAT) {
				externalCoat = coatList.get(i);
				externalLayerList = (List<PipeCoatLayer>) layerManager
						.findByField(PipeCoatLayer.class, "pipeCoatId",
								externalCoat.getPipeCoatId());
			}
			List<PipeCoatLayer> layerList = (List<PipeCoatLayer>) layerManager
					.findByField(PipeCoatLayer.class, "pipeCoatId", coatList
							.get(i).getPipeCoatId());
		}// end of for loop...
	}

	public void submitPipeCoat() {

		if ((internalCoat.getSide() != null && selectedCoat.getSide() == internalCoat
				.getSide())
				|| (externalCoat.getSide() != null && selectedCoat.getSide() == externalCoat
						.getSide())) {
			FacesContextUtils.addErrorMessage("CoatExistMessage");
			return;
		}

		if ((layer1.getAmountConsumed() <= 0)
				&& (layer2.getAmountConsumed() <= 0)
				&& (layer3.getAmountConsumed() <= 0)) {
			FacesContextUtils.addErrorMessage("LayerRequiredMessage");
			return;
		}
		// check if the amount entered is smaller than the resources
		PipeCoatManager coatManager = new PipeCoatManager();
		injectedParameterList.getSelectedCoat().setPipeId(
				currentPipe.getPipeId());
		coatManager.enterNew(injectedParameterList.getSelectedCoat());
		PipeCoatLayerManager layerManager = new PipeCoatLayerManager();
		if (layer1.getAmountConsumed() > 0
				&& layer1.getCoatMaterialId() != null) {
			CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
			CoatRawMaterial material = (CoatRawMaterial) materialManager
					.findByField(CoatRawMaterial.class, "coatMaterialId",
							layer1.getCoatMaterialId()).get(0);
			if (material.getRemainingAmount() < layer1.getAmountConsumed())
				FacesContextUtils.addInfoMessage("WrongAmountMessage");
			else {
				material.setRemainingAmount(material.getRemainingAmount()
						- layer1.getAmountConsumed());
				materialManager.updateEntity(material);
				layer1.setPipeCoatId(injectedParameterList.getSelectedCoat()
						.getPipeCoatId());
				layer1.setLayerNumber(1);
				layerManager.enterNew(layer1);
			}
		}
		if (layer2.getAmountConsumed() > 0
				&& layer2.getCoatMaterialId() != null) {
			CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
			CoatRawMaterial material = (CoatRawMaterial) materialManager
					.findByField(CoatRawMaterial.class, "coatMaterialId",
							layer2.getCoatMaterialId()).get(0);
			if (material.getRemainingAmount() < layer1.getAmountConsumed())
				FacesContextUtils.addInfoMessage("WrongAmountMessage");
			else {
				material.setRemainingAmount(material.getRemainingAmount()
						- layer2.getAmountConsumed());
				materialManager.updateEntity(material);
				layer2.setPipeCoatId(injectedParameterList.getSelectedCoat()
						.getPipeCoatId());
				layer2.setLayerNumber(2);
				layerManager.enterNew(layer2);
			}
		}

		if (layer3.getAmountConsumed() > 0
				&& layer3.getCoatMaterialId() != null) {
			CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
			CoatRawMaterial material = (CoatRawMaterial) materialManager
					.findByField(CoatRawMaterial.class, "coatMaterialId",
							layer3.getCoatMaterialId()).get(0);
			if (material.getRemainingAmount() < layer3.getAmountConsumed())
				FacesContextUtils.addInfoMessage("WrongAmountMessage");
			else {
				material.setRemainingAmount(material.getRemainingAmount()
						- layer3.getAmountConsumed());
				materialManager.updateEntity(material);
				layer3.setPipeCoatId(injectedParameterList.getSelectedCoat()
						.getPipeCoatId());
				layer3.setLayerNumber(3);
				layerManager.enterNew(layer3);
			}
		}
		loadPipeCoat();
		FacesContextUtils.addInfoMessage("SubmitMessage");
		injectedParameterList.setSelectedCoat(new PipeCoat());
		layer1 = new PipeCoatLayer();
		layer2 = new PipeCoatLayer();
		layer3 = new PipeCoatLayer();
	}

	public void deleteCoatLayer(PipeCoatLayer layer) {
		// add the amount consumed to the COAT RAW MATERIAL and after that
		// remove the layer
		CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
		CoatRawMaterial material = (CoatRawMaterial) materialManager
				.findByField(CoatRawMaterial.class, "coatMaterialId",
						layer.getCoatMaterialId()).get(0);
		material.setRemainingAmount(material.getRemainingAmount()
				+ layer.getAmountConsumed());
		materialManager.updateEntity(material);
		PipeCoatLayerManager layerManager = new PipeCoatLayerManager();
		layerManager.delete(layer);
		loadPipeCoat();
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	public void addInternalCoatLayer() {

	}

	public void addExternalCoatLayer() {

	}

	/*
	 * GETTERS AND SETTERS
	 */

	public Machine getCurrentProductionMachine() {
		return currentProductionMachine;
	}

	public void setCurrentProductionMachine(Machine currentProductionMachine) {
		this.currentProductionMachine = currentProductionMachine;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	// public List<Order> getActiveOrderList() {
	// return activeOrderList;
	// }
	//
	// public void setActiveOrderList(List<Order> activeOrderList) {
	// this.activeOrderList = activeOrderList;
	// }

	public Machine getSelectedMachine() {
		return selectedMachine;
	}

	public void setSelectedMachine(Machine currentMachine) {
		this.selectedMachine = currentMachine;
	}

	public List<Pipe> getInMachinePipeList() {
		return inMachinePipeList;
	}

	public void setInMachinePipeList(List<Pipe> inMachinePipeList) {
		this.inMachinePipeList = inMachinePipeList;
	}

	public List<Pipe> getFreePipeList() {
		return freePipeList;
	}

	public void setFreePipeList(List<Pipe> freePipeList) {
		this.freePipeList = freePipeList;
	}

	public Pipe getInMachineSelectedPipe() {
		return inMachineSelectedPipe;
	}

	public void setInMachineSelectedPipe(Pipe inMachineSelectedPipe) {
		this.inMachineSelectedPipe = inMachineSelectedPipe;
	}

	public MachinePipeLink getMachinePipeLink() {
		return machinePipeLink;
	}

	public void setMachinePipeLink(MachinePipeLink machinePipeLink) {
		this.machinePipeLink = machinePipeLink;
	}

	public List<Error> getErrorList() {
		return errorList;
	}

	public void setErrorList(List<Error> errorList) {
		this.errorList = errorList;
	}

	public Error getSelectedError() {
		return selectedError;
	}

	public void setSelectedError(Error selectedError) {
		this.selectedError = selectedError;
	}

	public Pipe getCurrentPipe() {
		return currentPipe;
	}

	public void setCurrentPipe(Pipe currentPipe) {
		this.currentPipe = currentPipe;
	}

	public CementMachineParameter getSelectedCementMachineParameter() {
		return selectedCementMachineParameter;
	}

	public void setSelectedCementMachineParameter(
			CementMachineParameter selectedCementMachineParameter) {
		this.selectedCementMachineParameter = selectedCementMachineParameter;
	}

	public PolietilenMachineParameter getSelectedPolietilenMachineParameter() {
		return selectedPolietilenMachineParameter;
	}

	public void setSelectedPolietilenMachineParameter(
			PolietilenMachineParameter selectedPolietilenMachineParameter) {
		this.selectedPolietilenMachineParameter = selectedPolietilenMachineParameter;
	}

	public EpoxyMachineParameter getSelectedEpoxyMachineParameter() {
		return selectedEpoxyMachineParameter;
	}

	public void setSelectedEpoxyMachineParameter(
			EpoxyMachineParameter selectedEpoxyMachineParameter) {
		this.selectedEpoxyMachineParameter = selectedEpoxyMachineParameter;
	}

	public PipeCoat getSelectedCoat() {
		return selectedCoat;
	}

	public void setSelectedCoat(PipeCoat selectedCoat) {
		this.selectedCoat = selectedCoat;
	}

	public PipeCoatLayer getLayer1() {
		return layer1;
	}

	public void setLayer1(PipeCoatLayer layer1) {
		this.layer1 = layer1;
	}

	public PipeCoatLayer getLayer2() {
		return layer2;
	}

	public void setLayer2(PipeCoatLayer layer2) {
		this.layer2 = layer2;
	}

	public PipeCoatLayer getLayer3() {
		return layer3;
	}

	public void setLayer3(PipeCoatLayer layer3) {
		this.layer3 = layer3;
	}

	public List<CoatRawMaterial> getCoatMaterialList() {
		return coatMaterialList;
	}

	public void setCoatMaterialList(List<CoatRawMaterial> coatMaterialList) {
		this.coatMaterialList = coatMaterialList;
	}

	public PipeCoat getInternalCoat() {
		return internalCoat;
	}

	public void setInternalCoat(PipeCoat internalCoat) {
		this.internalCoat = internalCoat;
	}

	public PipeCoat getExternalCoat() {
		return externalCoat;
	}

	public void setExternalCoat(PipeCoat externalCoat) {
		this.externalCoat = externalCoat;
	}

	public List<PipeCoatLayer> getInternalLayerList() {
		return internalLayerList;
	}

	public void setInternalLayerList(List<PipeCoatLayer> internalLayerList) {
		this.internalLayerList = internalLayerList;
	}

	public List<PipeCoatLayer> getExternalLayerList() {
		return externalLayerList;
	}

	public void setExternalLayerList(List<PipeCoatLayer> externalLayerList) {
		this.externalLayerList = externalLayerList;
	}

	public ParameterListBean getInjectedParameterList() {
		return injectedParameterList;
	}

	public void setInjectedParameterList(ParameterListBean injectedParameterList) {
		this.injectedParameterList = injectedParameterList;
	}

	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	public double getWastedAmount() {
		return wastedAmount;
	}

	public void setWastedAmount(double wastedAmount) {
		this.wastedAmount = wastedAmount;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public List<SalesItem> getActiveItemList() {
		return activeItemList;
	}

	public void setActiveItemList(List<SalesItem> activeItemList) {
		this.activeItemList = activeItemList;
	}

	public SalesCustomer getCurrentSalesCompany() {
		return currentSalesCompany;
	}

	public void setCurrentSalesCompany(SalesCustomer currentSalesCompany) {
		this.currentSalesCompany = currentSalesCompany;
	}

	public SalesItem getSelectedSalesItem() {
		return selectedSalesItem;
	}

	public void setSelectedSalesItem(SalesItem selectedSalesItem) {
		this.selectedSalesItem = selectedSalesItem;
	}

}
