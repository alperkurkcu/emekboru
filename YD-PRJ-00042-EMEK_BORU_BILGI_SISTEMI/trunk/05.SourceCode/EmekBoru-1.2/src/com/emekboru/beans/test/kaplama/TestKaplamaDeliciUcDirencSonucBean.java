/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaDeliciUcDirencSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaDeliciUcDirencSonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */

@ManagedBean(name = "testKaplamaDeliciUcDirencSonucBean")
@ViewScoped
public class TestKaplamaDeliciUcDirencSonucBean {

	private TestKaplamaDeliciUcDirencSonuc testKaplamaDeliciUcDirencSonucForm = new TestKaplamaDeliciUcDirencSonuc();
	private List<TestKaplamaDeliciUcDirencSonuc> allDeliciUcDirencSonucList = new ArrayList<TestKaplamaDeliciUcDirencSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	float ortalama = 0F;

	public void addOrUpdateDeliciUcDirencTestSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaDeliciUcDirencSonucManager tdudManager = new TestKaplamaDeliciUcDirencSonucManager();

		if (testKaplamaDeliciUcDirencSonucForm.getId() == null) {

			testKaplamaDeliciUcDirencSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaDeliciUcDirencSonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaDeliciUcDirencSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaDeliciUcDirencSonucForm.setBagliTestId(bagliTest);
			testKaplamaDeliciUcDirencSonucForm.setBagliGlobalId(bagliTest);

			ortalamaBul();

			tdudManager.enterNew(testKaplamaDeliciUcDirencSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaDeliciUcDirencSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaDeliciUcDirencSonucForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			ortalamaBul();
			tdudManager.updateEntity(testKaplamaDeliciUcDirencSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	private void ortalamaBul() {

		ortalama = (testKaplamaDeliciUcDirencSonucForm.getPermotasyonDerA()
				.floatValue()
				+ testKaplamaDeliciUcDirencSonucForm.getPermotasyonDerB()
						.floatValue() + testKaplamaDeliciUcDirencSonucForm
				.getPermotasyonDerC().floatValue()) / 3;
		testKaplamaDeliciUcDirencSonucForm.setArtOrtalama(BigDecimal
				.valueOf(ortalama));
	}

	public void addDeliciUcDirencTest() {

		testKaplamaDeliciUcDirencSonucForm = new TestKaplamaDeliciUcDirencSonuc();
		updateButtonRender = false;
	}

	public void deliciUcDirencListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allDeliciUcDirencSonucList = TestKaplamaDeliciUcDirencSonucManager
				.getAllDeliciUcDirencSonucTest(globalId, pipeId);
	}

	public void deleteDeliciUcDirencSonuc() {

		bagliTestId = testKaplamaDeliciUcDirencSonucForm.getBagliTestId()
				.getId();

		if (testKaplamaDeliciUcDirencSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		TestKaplamaDeliciUcDirencSonucManager tdudManager = new TestKaplamaDeliciUcDirencSonucManager();

		tdudManager.delete(testKaplamaDeliciUcDirencSonucForm);
		testKaplamaDeliciUcDirencSonucForm = new TestKaplamaDeliciUcDirencSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters

	/**
	 * @return the testKaplamaDeliciUcDirencSonucForm
	 */
	public TestKaplamaDeliciUcDirencSonuc getTestKaplamaDeliciUcDirencSonucForm() {
		return testKaplamaDeliciUcDirencSonucForm;
	}

	/**
	 * @param testKaplamaDeliciUcDirencSonucForm
	 *            the testKaplamaDeliciUcDirencSonucForm to set
	 */
	public void setTestKaplamaDeliciUcDirencSonucForm(
			TestKaplamaDeliciUcDirencSonuc testKaplamaDeliciUcDirencSonucForm) {
		this.testKaplamaDeliciUcDirencSonucForm = testKaplamaDeliciUcDirencSonucForm;
	}

	/**
	 * @return the allDeliciUcDirencSonucList
	 */
	public List<TestKaplamaDeliciUcDirencSonuc> getAllDeliciUcDirencSonucList() {
		return allDeliciUcDirencSonucList;
	}

	/**
	 * @param allDeliciUcDirencSonucList
	 *            the allDeliciUcDirencSonucList to set
	 */
	public void setAllDeliciUcDirencSonucList(
			List<TestKaplamaDeliciUcDirencSonuc> allDeliciUcDirencSonucList) {
		this.allDeliciUcDirencSonucList = allDeliciUcDirencSonucList;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}
}
