/**
 * 
 */
package com.emekboru.beans.stok;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.stok.StokJoinUrunMarka;
import com.emekboru.jpaman.stok.StokJoinUrunMarkaManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "stokJoinUrunMarkaBean")
@ViewScoped
public class StokJoinUrunMarkaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<StokJoinUrunMarka> allStokJoinUrunMarkaList = new ArrayList<StokJoinUrunMarka>();

	private StokJoinUrunMarka stokJoinUrunMarkaForm = new StokJoinUrunMarka();

	private StokJoinUrunMarka[] selectedMarkalar;

	StokJoinUrunMarkaManager formManager = new StokJoinUrunMarkaManager();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	Integer prmFormId = 0;

	public StokJoinUrunMarkaBean() {

		updateButtonRender = false;
	}

	public void addForm() {

		stokJoinUrunMarkaForm = new StokJoinUrunMarka();
		updateButtonRender = false;
	}

	public void editForm() {

		if (stokJoinUrunMarkaForm.getJoinId() == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage("ÖNCE MARKA SEÇİNİZ!"));
		}
		updateButtonRender = true;
	}

	public void addOrUpdateForm() {

		if (stokJoinUrunMarkaForm.getJoinId() == null
				&& updateButtonRender == false) {

			stokJoinUrunMarkaForm.setEklemeZamani(UtilInsCore.getTarihZaman());
			stokJoinUrunMarkaForm.setEkleyenKullanici(userBean.getUser()
					.getId());
			stokJoinUrunMarkaForm.getStokUrunKartlari().setId(prmFormId);

			formManager.enterNew(stokJoinUrunMarkaForm);

			this.stokJoinUrunMarkaForm = new StokJoinUrunMarka();
			FacesContextUtils.addInfoMessage("FormSubmitMessage");

		} else if (stokJoinUrunMarkaForm.getJoinId() != null
				&& updateButtonRender == true) {

			stokJoinUrunMarkaForm.setGuncellemeZamani(UtilInsCore
					.getTarihZaman());
			stokJoinUrunMarkaForm.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			formManager.updateEntity(stokJoinUrunMarkaForm);

			FacesContextUtils.addInfoMessage("FormUpdateMessage");
		}

		fillTestList(prmFormId);
	}

	public void deleteForm() {

		if (stokJoinUrunMarkaForm.getJoinId() == null) {

			FacesContextUtils.addWarnMessage("NoSelectMessage");
			return;
		}

		formManager.delete(stokJoinUrunMarkaForm);
		fillTestList(prmFormId);

		FacesContextUtils.addWarnMessage("TestDeleteMessage");
	}

	public void fillTestList(Integer urunId) {
		StokJoinUrunMarkaManager manager = new StokJoinUrunMarkaManager();
		allStokJoinUrunMarkaList = manager.getAllStokJoinUrunMarka(urunId);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"ÜRÜN MARKALARI LİSTELENMİŞTİR!"));
	}

	public void fillTestList(ActionEvent e) {// sayfa ilk açılırken prmFormId
												// alıyor

		UICommand cmd = (UICommand) e.getComponent();
		prmFormId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");
		StokJoinUrunMarkaManager manager = new StokJoinUrunMarkaManager();
		allStokJoinUrunMarkaList = manager.getAllStokJoinUrunMarka(prmFormId);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"ÜRÜN MARKALARI LİSTELENMİŞTİR!"));
	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	// setters getters

	public List<StokJoinUrunMarka> getAllStokJoinUrunMarkaList() {
		return allStokJoinUrunMarkaList;
	}

	public void setAllStokJoinUrunMarkaList(
			List<StokJoinUrunMarka> allStokJoinUrunMarkaList) {
		this.allStokJoinUrunMarkaList = allStokJoinUrunMarkaList;
	}

	public StokJoinUrunMarka getStokJoinUrunMarkaForm() {
		return stokJoinUrunMarkaForm;
	}

	public void setStokJoinUrunMarkaForm(StokJoinUrunMarka stokJoinUrunMarkaForm) {
		this.stokJoinUrunMarkaForm = stokJoinUrunMarkaForm;
	}

	public StokJoinUrunMarkaManager getFormManager() {
		return formManager;
	}

	public void setFormManager(StokJoinUrunMarkaManager formManager) {
		this.formManager = formManager;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public StokJoinUrunMarka[] getSelectedMarkalar() {
		return selectedMarkalar;
	}

	public void setSelectedMarkalar(StokJoinUrunMarka[] selectedMarkalar) {
		this.selectedMarkalar = selectedMarkalar;
	}

}
