package com.emekboru.beans.test.kaplama;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaBetonOranBelirlenmesiSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaBetonOranBelirlenmesiSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaplamaBetonOranBelirlenmesiSonucBean")
@ViewScoped
public class TestKaplamaBetonOranBelirlenmesiSonucBean {

	private TestKaplamaBetonOranBelirlenmesiSonuc testKaplamaBetonOranBelirlenmesiSonucForm = new TestKaplamaBetonOranBelirlenmesiSonuc();
	private List<TestKaplamaBetonOranBelirlenmesiSonuc> allTestKaplamaBetonOranBelirlenmesiSonucList = new ArrayList<TestKaplamaBetonOranBelirlenmesiSonuc>();
	TestKaplamaBetonOranBelirlenmesiSonucManager manager = new TestKaplamaBetonOranBelirlenmesiSonucManager();
	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateTestKaplamaBetonOranBelirlenmesiSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		if (testKaplamaBetonOranBelirlenmesiSonucForm.getId() == null) {

			testKaplamaBetonOranBelirlenmesiSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaBetonOranBelirlenmesiSonucForm
					.setEkleyenKullanici(userBean.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaBetonOranBelirlenmesiSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaBetonOranBelirlenmesiSonucForm.setBagliTestId(bagliTest);
			testKaplamaBetonOranBelirlenmesiSonucForm
					.setBagliGlobalId(bagliTest);

			manager.updateEntity(testKaplamaBetonOranBelirlenmesiSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaBetonOranBelirlenmesiSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaBetonOranBelirlenmesiSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			manager.enterNew(testKaplamaBetonOranBelirlenmesiSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addTestKaplamaBetonOranBelirlenmesiSonuc() {

		testKaplamaBetonOranBelirlenmesiSonucForm = new TestKaplamaBetonOranBelirlenmesiSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaBetonOranBelirlenmesiSonucListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaBetonOranBelirlenmesiSonucList = TestKaplamaBetonOranBelirlenmesiSonucManager
				.getAllTestKaplamaBetonOranBelirlenmesiSonuc(globalId, pipeId);
		testKaplamaBetonOranBelirlenmesiSonucForm = new TestKaplamaBetonOranBelirlenmesiSonuc();
	}

	public void deleteTestKaplamaBetonOranBelirlenmesiSonuc() {

		bagliTestId = testKaplamaBetonOranBelirlenmesiSonucForm
				.getBagliGlobalId().getId();

		if (testKaplamaBetonOranBelirlenmesiSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		manager.delete(testKaplamaBetonOranBelirlenmesiSonucForm);
		testKaplamaBetonOranBelirlenmesiSonucForm = new TestKaplamaBetonOranBelirlenmesiSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters
	public TestKaplamaBetonOranBelirlenmesiSonuc getTestKaplamaBetonOranBelirlenmesiSonucForm() {
		return testKaplamaBetonOranBelirlenmesiSonucForm;
	}

	public void setTestKaplamaBetonOranBelirlenmesiSonucForm(
			TestKaplamaBetonOranBelirlenmesiSonuc testKaplamaBetonOranBelirlenmesiSonucForm) {
		this.testKaplamaBetonOranBelirlenmesiSonucForm = testKaplamaBetonOranBelirlenmesiSonucForm;
	}

	public List<TestKaplamaBetonOranBelirlenmesiSonuc> getAllTestKaplamaBetonOranBelirlenmesiSonucList() {
		return allTestKaplamaBetonOranBelirlenmesiSonucList;
	}

	public void setAllTestKaplamaBetonOranBelirlenmesiSonucList(
			List<TestKaplamaBetonOranBelirlenmesiSonuc> allTestKaplamaBetonOranBelirlenmesiSonucList) {
		this.allTestKaplamaBetonOranBelirlenmesiSonucList = allTestKaplamaBetonOranBelirlenmesiSonucList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
