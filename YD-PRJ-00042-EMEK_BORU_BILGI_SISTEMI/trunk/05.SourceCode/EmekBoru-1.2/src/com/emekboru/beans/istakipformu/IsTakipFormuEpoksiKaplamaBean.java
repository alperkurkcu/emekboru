/**
 * 
 */
package com.emekboru.beans.istakipformu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.istakipformu.IsTakipFormuEpoksiKaplama;
import com.emekboru.jpa.istakipformu.IsTakipFormuEpoksiKaplamaBitinceIslemler;
import com.emekboru.jpa.istakipformu.IsTakipFormuEpoksiKaplamaOnceIslemler;
import com.emekboru.jpa.istakipformu.IsTakipFormuEpoksiKaplamaSonraIslemler;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.istakipformu.IsTakipFormuEpoksiKaplamaBitinceIslemlerManager;
import com.emekboru.jpaman.istakipformu.IsTakipFormuEpoksiKaplamaManager;
import com.emekboru.jpaman.istakipformu.IsTakipFormuEpoksiKaplamaOnceIslemlerManager;
import com.emekboru.jpaman.istakipformu.IsTakipFormuEpoksiKaplamaSonraIslemlerManager;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isTakipFormuEpoksiKaplamaBean")
@ViewScoped
public class IsTakipFormuEpoksiKaplamaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private SalesItem selectedItem = new SalesItem();

	private List<IsTakipFormuEpoksiKaplama> allIsTakipFormuEpoksiKaplamaList = new ArrayList<IsTakipFormuEpoksiKaplama>();
	private IsTakipFormuEpoksiKaplama isTakipFormuEpoksiKaplamaForm = new IsTakipFormuEpoksiKaplama();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private IsTakipFormuEpoksiKaplamaOnceIslemler selectedIsTakipFormuEpoksiKaplamaOnceIslemler = new IsTakipFormuEpoksiKaplamaOnceIslemler();
	private IsTakipFormuEpoksiKaplamaSonraIslemler selectedIsTakipFormuEpoksiKaplamaSonraIslemler = new IsTakipFormuEpoksiKaplamaSonraIslemler();
	private IsTakipFormuEpoksiKaplamaBitinceIslemler selectedIsTakipFormuEpoksiKaplamaBitinceIslemler = new IsTakipFormuEpoksiKaplamaBitinceIslemler();
	private IsTakipFormuEpoksiKaplamaOnceIslemler newIsTakipFormuEpoksiKaplamaOnceIslemler = new IsTakipFormuEpoksiKaplamaOnceIslemler();
	private IsTakipFormuEpoksiKaplamaSonraIslemler newIsTakipFormuEpoksiKaplamaSonraIslemler = new IsTakipFormuEpoksiKaplamaSonraIslemler();
	private IsTakipFormuEpoksiKaplamaBitinceIslemler newIsTakipFormuEpoksiKaplamaBitinceIslemler = new IsTakipFormuEpoksiKaplamaBitinceIslemler();
	private IsTakipFormuEpoksiKaplama selectedIsTakipFormuEpoksiKaplama = new IsTakipFormuEpoksiKaplama();

	private IsTakipFormuEpoksiKaplama newIsTakip = new IsTakipFormuEpoksiKaplama();

	public void addFromIsEmri(SalesItem salesItem) {

		System.out.println("isTakipFormuEpoksiKaplamaBean.addFromIsEmri()");

		try {
			IsTakipFormuEpoksiKaplamaManager manager = new IsTakipFormuEpoksiKaplamaManager();

			newIsTakip.setSalesItem(salesItem);
			newIsTakip.setEklemeZamani(UtilInsCore.getTarihZaman());
			newIsTakip.setEkleyenEmployee(userBean.getUser());

			newIsTakip.getIsTakipFormuEpoksiKaplamaOnceIslemler()
					.setEklemeZamani(UtilInsCore.getTarihZaman());
			newIsTakip.getIsTakipFormuEpoksiKaplamaOnceIslemler()
					.setEkleyenEmployee(userBean.getUser());

			newIsTakip.getIsTakipFormuEpoksiKaplamaSonraIslemler()
					.setEklemeZamani(UtilInsCore.getTarihZaman());
			newIsTakip.getIsTakipFormuEpoksiKaplamaSonraIslemler()
					.setEkleyenEmployee(userBean.getUser());

			newIsTakip.getIsTakipFormuEpoksiKaplamaBitinceIslemler()
					.setEklemeZamani(UtilInsCore.getTarihZaman());
			newIsTakip.getIsTakipFormuEpoksiKaplamaBitinceIslemler()
					.setEkleyenEmployee(userBean.getUser());

			manager.enterNew(newIsTakip);
			newIsTakip = new IsTakipFormuEpoksiKaplama();

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"EPOKSİ KAPLAMA İŞ TAKİP FORMU EKLENMİŞTİR!"));

		} catch (Exception e) {
			System.out
					.println("isTakipFormuEpoksiKaplamaBean.addFromIsEmri-HATA");
		}
	}

	public void loadSelectedTakipForm() {

		reset();
		loadSelectedIsTakipFormuEpoksiKaplamaOnceIslemler();
		loadSelectedIsTakipFormuEpoksiKaplamaSonraIslemler();
		loadSelectedIsTakipFormuEpoksiKaplamaBitinceIslemler();
	}

	@SuppressWarnings("static-access")
	public void loadReplySelectedTakipForm() {
		IsTakipFormuEpoksiKaplamaManager takipManager = new IsTakipFormuEpoksiKaplamaManager();
		if (takipManager.getAllIsTakipFormuEpoksiKaplama(
				selectedItem.getItemId()).size() > 0) {
			selectedIsTakipFormuEpoksiKaplama = takipManager
					.getAllIsTakipFormuEpoksiKaplama(selectedItem.getItemId())
					.get(0);
		} else {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"EPOKSİ KAPLAMA İŞ EMRİ EKLENMEMİŞ, LÜTFEN ÖNCE İŞ EMİRLERİNİ EKLEYİNİZ!",
							null));
			return;
		}
	}

	public void reset() {

		selectedIsTakipFormuEpoksiKaplamaOnceIslemler = new IsTakipFormuEpoksiKaplamaOnceIslemler();
		selectedIsTakipFormuEpoksiKaplamaSonraIslemler = new IsTakipFormuEpoksiKaplamaSonraIslemler();
		selectedIsTakipFormuEpoksiKaplamaBitinceIslemler = new IsTakipFormuEpoksiKaplamaBitinceIslemler();
	}

	public void loadSelectedIsTakipFormuEpoksiKaplamaOnceIslemler() {
		loadReplySelectedTakipForm();
		selectedIsTakipFormuEpoksiKaplamaOnceIslemler = selectedIsTakipFormuEpoksiKaplama
				.getIsTakipFormuEpoksiKaplamaOnceIslemler();
	}

	public void loadSelectedIsTakipFormuEpoksiKaplamaSonraIslemler() {
		loadReplySelectedTakipForm();
		selectedIsTakipFormuEpoksiKaplamaSonraIslemler = selectedIsTakipFormuEpoksiKaplama
				.getIsTakipFormuEpoksiKaplamaSonraIslemler();
	}

	public void loadSelectedIsTakipFormuEpoksiKaplamaBitinceIslemler() {
		loadReplySelectedTakipForm();
		selectedIsTakipFormuEpoksiKaplamaBitinceIslemler = selectedIsTakipFormuEpoksiKaplama
				.getIsTakipFormuEpoksiKaplamaBitinceIslemler();
	}

	public void updateSelectedIsTakipFormuEpoksiKaplamaOnceIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuEpoksiKaplamaOnceIslemlerManager onceManager = new IsTakipFormuEpoksiKaplamaOnceIslemlerManager();

		selectedIsTakipFormuEpoksiKaplamaOnceIslemler
				.setGuncellemeZamani(UtilInsCore.getTarihZaman());
		selectedIsTakipFormuEpoksiKaplamaOnceIslemler
				.setGuncelleyenEmployee(userBean.getUser());
		selectedIsTakipFormuEpoksiKaplamaOnceIslemler
				.setIsTakipFormuEpoksiKaplama(selectedIsTakipFormuEpoksiKaplama);

		onceManager.updateEntity(selectedIsTakipFormuEpoksiKaplamaOnceIslemler);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"EPOKSİ KAPLAMADA İŞE BAŞLAMADAN YAPILACAK İŞLEMLER BAŞARIYLA TANIMLANMIŞTIR!"));

	}

	public void updateSelectedIsTakipFormuEpoksiKaplamaSonraIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuEpoksiKaplamaSonraIslemlerManager sonraManager = new IsTakipFormuEpoksiKaplamaSonraIslemlerManager();

		selectedIsTakipFormuEpoksiKaplamaSonraIslemler
				.setGuncellemeZamani(UtilInsCore.getTarihZaman());
		selectedIsTakipFormuEpoksiKaplamaSonraIslemler
				.setGuncelleyenEmployee(userBean.getUser());

		sonraManager
				.updateEntity(selectedIsTakipFormuEpoksiKaplamaSonraIslemler);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"EPOKSİ KAPLAMADA AYAR BİTTİKTEN SONRA YAPILACAK İŞLEMLER BAŞARIYLA TANIMLANMIŞTIR!"));

	}

	public void updateSelectedIsTakipFormuEpoksiKaplamaBitinceIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuEpoksiKaplamaBitinceIslemlerManager bitinceManager = new IsTakipFormuEpoksiKaplamaBitinceIslemlerManager();

		selectedIsTakipFormuEpoksiKaplamaBitinceIslemler
				.setGuncellemeZamani(UtilInsCore.getTarihZaman());
		selectedIsTakipFormuEpoksiKaplamaBitinceIslemler
				.setGuncelleyenEmployee(userBean.getUser());

		bitinceManager
				.updateEntity(selectedIsTakipFormuEpoksiKaplamaBitinceIslemler);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"EPOKSİ KAPLAMADA ÜRETİM BİTTİKTEN SONRA YAPILACAK İŞLEMLER BAŞARIYLA TANIMLANMIŞTIR!"));

	}

	public void deleteSelectedIsTakipFormuEpoksiKaplamaOnceIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuEpoksiKaplamaOnceIslemlerManager onceManager = new IsTakipFormuEpoksiKaplamaOnceIslemlerManager();

		newIsTakipFormuEpoksiKaplamaOnceIslemler
				.setId(selectedIsTakipFormuEpoksiKaplamaOnceIslemler.getId());
		newIsTakipFormuEpoksiKaplamaOnceIslemler
				.setEklemeZamani(selectedIsTakipFormuEpoksiKaplamaOnceIslemler
						.getEklemeZamani());
		newIsTakipFormuEpoksiKaplamaOnceIslemler
				.setEkleyenEmployee(selectedIsTakipFormuEpoksiKaplamaOnceIslemler
						.getEkleyenEmployee());

		selectedIsTakipFormuEpoksiKaplamaOnceIslemler = new IsTakipFormuEpoksiKaplamaOnceIslemler();

		selectedIsTakipFormuEpoksiKaplamaOnceIslemler = newIsTakipFormuEpoksiKaplamaOnceIslemler;

		onceManager.updateEntity(selectedIsTakipFormuEpoksiKaplamaOnceIslemler);

		newIsTakipFormuEpoksiKaplamaOnceIslemler = new IsTakipFormuEpoksiKaplamaOnceIslemler();
		selectedIsTakipFormuEpoksiKaplamaOnceIslemler = new IsTakipFormuEpoksiKaplamaOnceIslemler();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"EPOKSİ KAPLAMADA İŞE BAŞLAMADAN YAPILACAK İŞLEMLER BAŞARIYLA SİLİNMİŞTİR!"));

	}

	public void deleteSelectedIsTakipFormuEpoksiKaplamaSonraIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuEpoksiKaplamaSonraIslemlerManager sonraManager = new IsTakipFormuEpoksiKaplamaSonraIslemlerManager();

		newIsTakipFormuEpoksiKaplamaSonraIslemler
				.setId(selectedIsTakipFormuEpoksiKaplamaSonraIslemler.getId());
		newIsTakipFormuEpoksiKaplamaSonraIslemler
				.setEklemeZamani(selectedIsTakipFormuEpoksiKaplamaSonraIslemler
						.getEklemeZamani());
		newIsTakipFormuEpoksiKaplamaSonraIslemler
				.setEkleyenEmployee(selectedIsTakipFormuEpoksiKaplamaSonraIslemler
						.getEkleyenEmployee());

		selectedIsTakipFormuEpoksiKaplamaSonraIslemler = new IsTakipFormuEpoksiKaplamaSonraIslemler();

		selectedIsTakipFormuEpoksiKaplamaSonraIslemler = newIsTakipFormuEpoksiKaplamaSonraIslemler;

		sonraManager
				.updateEntity(selectedIsTakipFormuEpoksiKaplamaSonraIslemler);

		newIsTakipFormuEpoksiKaplamaSonraIslemler = new IsTakipFormuEpoksiKaplamaSonraIslemler();
		selectedIsTakipFormuEpoksiKaplamaSonraIslemler = new IsTakipFormuEpoksiKaplamaSonraIslemler();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"EPOKSİ KAPLAMADA AYAR BİTTİKTEN SONRA YAPILACAK İŞLEMLER BAŞARIYLA SİLİNMİŞTİR!"));

	}

	public void deleteSelectedIsTakipFormuEpoksiKaplamaBitinceIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuEpoksiKaplamaBitinceIslemlerManager bitinceManager = new IsTakipFormuEpoksiKaplamaBitinceIslemlerManager();

		newIsTakipFormuEpoksiKaplamaBitinceIslemler
				.setId(selectedIsTakipFormuEpoksiKaplamaBitinceIslemler.getId());
		newIsTakipFormuEpoksiKaplamaBitinceIslemler
				.setEklemeZamani(selectedIsTakipFormuEpoksiKaplamaBitinceIslemler
						.getEklemeZamani());
		newIsTakipFormuEpoksiKaplamaBitinceIslemler
				.setEkleyenEmployee(selectedIsTakipFormuEpoksiKaplamaBitinceIslemler
						.getEkleyenEmployee());

		selectedIsTakipFormuEpoksiKaplamaBitinceIslemler = new IsTakipFormuEpoksiKaplamaBitinceIslemler();

		selectedIsTakipFormuEpoksiKaplamaBitinceIslemler = newIsTakipFormuEpoksiKaplamaBitinceIslemler;

		bitinceManager
				.updateEntity(selectedIsTakipFormuEpoksiKaplamaBitinceIslemler);

		newIsTakipFormuEpoksiKaplamaBitinceIslemler = new IsTakipFormuEpoksiKaplamaBitinceIslemler();
		selectedIsTakipFormuEpoksiKaplamaBitinceIslemler = new IsTakipFormuEpoksiKaplamaBitinceIslemler();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"EPOKSİ KAPLAMADA ÜRETİM BİTİNCE YAPILACAK İŞLEMLER BAŞARIYLA SİLİNMİŞTİR!"));

	}

	// setters getters

	public SalesItem getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(SalesItem selectedItem) {
		this.selectedItem = selectedItem;
	}

	public List<IsTakipFormuEpoksiKaplama> getAllIsTakipFormuEpoksiKaplamaList() {
		return allIsTakipFormuEpoksiKaplamaList;
	}

	public void setAllIsTakipFormuEpoksiKaplamaList(
			List<IsTakipFormuEpoksiKaplama> allIsTakipFormuEpoksiKaplamaList) {
		this.allIsTakipFormuEpoksiKaplamaList = allIsTakipFormuEpoksiKaplamaList;
	}

	public IsTakipFormuEpoksiKaplama getIsTakipFormuEpoksiKaplamaForm() {
		return isTakipFormuEpoksiKaplamaForm;
	}

	public void setIsTakipFormuEpoksiKaplamaForm(
			IsTakipFormuEpoksiKaplama isTakipFormuEpoksiKaplamaForm) {
		this.isTakipFormuEpoksiKaplamaForm = isTakipFormuEpoksiKaplamaForm;
	}

	public IsTakipFormuEpoksiKaplama getNewIsTakip() {
		return newIsTakip;
	}

	public void setNewIsTakip(IsTakipFormuEpoksiKaplama newIsTakip) {
		this.newIsTakip = newIsTakip;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public IsTakipFormuEpoksiKaplamaOnceIslemler getSelectedIsTakipFormuEpoksiKaplamaOnceIslemler() {
		return selectedIsTakipFormuEpoksiKaplamaOnceIslemler;
	}

	public void setSelectedIsTakipFormuEpoksiKaplamaOnceIslemler(
			IsTakipFormuEpoksiKaplamaOnceIslemler selectedIsTakipFormuEpoksiKaplamaOnceIslemler) {
		this.selectedIsTakipFormuEpoksiKaplamaOnceIslemler = selectedIsTakipFormuEpoksiKaplamaOnceIslemler;
	}

	public IsTakipFormuEpoksiKaplamaSonraIslemler getSelectedIsTakipFormuEpoksiKaplamaSonraIslemler() {
		return selectedIsTakipFormuEpoksiKaplamaSonraIslemler;
	}

	public void setSelectedIsTakipFormuEpoksiKaplamaSonraIslemler(
			IsTakipFormuEpoksiKaplamaSonraIslemler selectedIsTakipFormuEpoksiKaplamaSonraIslemler) {
		this.selectedIsTakipFormuEpoksiKaplamaSonraIslemler = selectedIsTakipFormuEpoksiKaplamaSonraIslemler;
	}

	public IsTakipFormuEpoksiKaplamaBitinceIslemler getSelectedIsTakipFormuEpoksiKaplamaBitinceIslemler() {
		return selectedIsTakipFormuEpoksiKaplamaBitinceIslemler;
	}

	public void setSelectedIsTakipFormuEpoksiKaplamaBitinceIslemler(
			IsTakipFormuEpoksiKaplamaBitinceIslemler selectedIsTakipFormuEpoksiKaplamaBitinceIslemler) {
		this.selectedIsTakipFormuEpoksiKaplamaBitinceIslemler = selectedIsTakipFormuEpoksiKaplamaBitinceIslemler;
	}

	public IsTakipFormuEpoksiKaplamaOnceIslemler getNewIsTakipFormuEpoksiKaplamaOnceIslemler() {
		return newIsTakipFormuEpoksiKaplamaOnceIslemler;
	}

	public void setNewIsTakipFormuEpoksiKaplamaOnceIslemler(
			IsTakipFormuEpoksiKaplamaOnceIslemler newIsTakipFormuEpoksiKaplamaOnceIslemler) {
		this.newIsTakipFormuEpoksiKaplamaOnceIslemler = newIsTakipFormuEpoksiKaplamaOnceIslemler;
	}

	public IsTakipFormuEpoksiKaplamaSonraIslemler getNewIsTakipFormuEpoksiKaplamaSonraIslemler() {
		return newIsTakipFormuEpoksiKaplamaSonraIslemler;
	}

	public void setNewIsTakipFormuEpoksiKaplamaSonraIslemler(
			IsTakipFormuEpoksiKaplamaSonraIslemler newIsTakipFormuEpoksiKaplamaSonraIslemler) {
		this.newIsTakipFormuEpoksiKaplamaSonraIslemler = newIsTakipFormuEpoksiKaplamaSonraIslemler;
	}

	public IsTakipFormuEpoksiKaplamaBitinceIslemler getNewIsTakipFormuEpoksiKaplamaBitinceIslemler() {
		return newIsTakipFormuEpoksiKaplamaBitinceIslemler;
	}

	public void setNewIsTakipFormuEpoksiKaplamaBitinceIslemler(
			IsTakipFormuEpoksiKaplamaBitinceIslemler newIsTakipFormuEpoksiKaplamaBitinceIslemler) {
		this.newIsTakipFormuEpoksiKaplamaBitinceIslemler = newIsTakipFormuEpoksiKaplamaBitinceIslemler;
	}

	public IsTakipFormuEpoksiKaplama getSelectedIsTakipFormuEpoksiKaplama() {
		return selectedIsTakipFormuEpoksiKaplama;
	}

	public void setSelectedIsTakipFormuEpoksiKaplama(
			IsTakipFormuEpoksiKaplama selectedIsTakipFormuEpoksiKaplama) {
		this.selectedIsTakipFormuEpoksiKaplama = selectedIsTakipFormuEpoksiKaplama;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
