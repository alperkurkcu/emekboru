/**
 * 
 */
package com.emekboru.beans.test.tahribatsiztestsonuc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.tahribatsiztestsonuc.SpecPenetrant;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.SpecPenetrantManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "specPenetrantBean")
@ViewScoped
public class SpecPenetrantBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<SpecPenetrant> allSpecList = new ArrayList<SpecPenetrant>();
	private SpecPenetrant specForm = new SpecPenetrant();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender = false;

	public SpecPenetrantBean() {
	}

	public void specListener(SelectEvent event) {
		updateButtonRender = true;
	}

	@SuppressWarnings("static-access")
	public void addSpec(Integer prmPipeId) {

		SpecPenetrantManager specManager = new SpecPenetrantManager();
		if (specManager.getAllSpecPenetrant(prmPipeId).size() == 0) {
			specForm = new SpecPenetrant();
		} else {
			specForm = specManager.getAllSpecPenetrant(prmPipeId).get(0);
		}
		updateButtonRender = false;
	}

	public void addOrUpdateSpec(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);

		SpecPenetrantManager manager = new SpecPenetrantManager();
		if (specForm.getId() == null) {
			specForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			specForm.setEkleyenKullanici(userBean.getUser().getId());
			specForm.setPipe(pipe);

			manager.enterNew(specForm);
			FacesContextUtils.addInfoMessage("SubmitMessage");
		} else {
			specForm.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			specForm.setGuncelleyenKullanici(userBean.getUser().getId());
			manager.updateEntity(specForm);
			FacesContextUtils.addInfoMessage("UpdateItemMessage");
		}
		fillTestList(pipe.getPipeId());
	}

	@SuppressWarnings("static-access")
	public void fillTestList(Integer prmPipeId) {
		SpecPenetrantManager manager = new SpecPenetrantManager();
		allSpecList = manager.getAllSpecPenetrant(prmPipeId);
		manager.refreshCollection(allSpecList);
		specForm = new SpecPenetrant();
	}

	public void deleteSpec() {

		SpecPenetrantManager manager = new SpecPenetrantManager();
		if (specForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		manager.delete(specForm);
		specForm = new SpecPenetrant();
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	// setters getters

	/**
	 * @return the allSpecList
	 */
	public List<SpecPenetrant> getAllSpecList() {
		return allSpecList;
	}

	/**
	 * @param allSpecList
	 *            the allSpecList to set
	 */
	public void setAllSpecList(List<SpecPenetrant> allSpecList) {
		this.allSpecList = allSpecList;
	}

	/**
	 * @return the specForm
	 */
	public SpecPenetrant getSpecForm() {
		return specForm;
	}

	/**
	 * @param specForm
	 *            the specForm to set
	 */
	public void setSpecForm(SpecPenetrant specForm) {
		this.specForm = specForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
