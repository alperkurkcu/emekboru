/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaTozEpoksiXcutSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaTozEpoksiXcutSonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testKaplamaTozEpoksiXcutSonucBean")
@ViewScoped
public class TestKaplamaTozEpoksiXcutSonucBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private TestKaplamaTozEpoksiXcutSonuc testKaplamaTozEpoksiXcutSonucForm = new TestKaplamaTozEpoksiXcutSonuc();
	private List<TestKaplamaTozEpoksiXcutSonuc> allTestKaplamaTozEpoksiXcutSonucList = new ArrayList<TestKaplamaTozEpoksiXcutSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateTestKaplamaTozEpoksiXcutSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaTozEpoksiXcutSonucManager manager = new TestKaplamaTozEpoksiXcutSonucManager();

		if (testKaplamaTozEpoksiXcutSonucForm.getId() == null) {

			testKaplamaTozEpoksiXcutSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaTozEpoksiXcutSonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaTozEpoksiXcutSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaTozEpoksiXcutSonucForm.setBagliTestId(bagliTest);
			testKaplamaTozEpoksiXcutSonucForm.setBagliGlobalId(bagliTest);

			manager.enterNew(testKaplamaTozEpoksiXcutSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {
			testKaplamaTozEpoksiXcutSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaTozEpoksiXcutSonucForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			manager.updateEntity(testKaplamaTozEpoksiXcutSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addTestKaplamaTozEpoksiXcutSonuc() {

		testKaplamaTozEpoksiXcutSonucForm = new TestKaplamaTozEpoksiXcutSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaTozEpoksiXcutSonucListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaTozEpoksiXcutSonucList = TestKaplamaTozEpoksiXcutSonucManager
				.getAllTestKaplamaTozEpoksiXcutSonuc(globalId, pipeId);
		testKaplamaTozEpoksiXcutSonucForm = new TestKaplamaTozEpoksiXcutSonuc();
	}

	public void deleteTestKaplamaTozEpoksiXcutSonuc() {

		bagliTestId = testKaplamaTozEpoksiXcutSonucForm.getBagliGlobalId()
				.getId();

		if (testKaplamaTozEpoksiXcutSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaTozEpoksiXcutSonucManager manager = new TestKaplamaTozEpoksiXcutSonucManager();

		manager.delete(testKaplamaTozEpoksiXcutSonucForm);
		testKaplamaTozEpoksiXcutSonucForm = new TestKaplamaTozEpoksiXcutSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters

	/**
	 * @return the testKaplamaTozEpoksiXcutSonucForm
	 */
	public TestKaplamaTozEpoksiXcutSonuc getTestKaplamaTozEpoksiXcutSonucForm() {
		return testKaplamaTozEpoksiXcutSonucForm;
	}

	/**
	 * @param testKaplamaTozEpoksiXcutSonucForm
	 *            the testKaplamaTozEpoksiXcutSonucForm to set
	 */
	public void setTestKaplamaTozEpoksiXcutSonucForm(
			TestKaplamaTozEpoksiXcutSonuc testKaplamaTozEpoksiXcutSonucForm) {
		this.testKaplamaTozEpoksiXcutSonucForm = testKaplamaTozEpoksiXcutSonucForm;
	}

	/**
	 * @return the allTestKaplamaTozEpoksiXcutSonucList
	 */
	public List<TestKaplamaTozEpoksiXcutSonuc> getAllTestKaplamaTozEpoksiXcutSonucList() {
		return allTestKaplamaTozEpoksiXcutSonucList;
	}

	/**
	 * @param allTestKaplamaTozEpoksiXcutSonucList
	 *            the allTestKaplamaTozEpoksiXcutSonucList to set
	 */
	public void setAllTestKaplamaTozEpoksiXcutSonucList(
			List<TestKaplamaTozEpoksiXcutSonuc> allTestKaplamaTozEpoksiXcutSonucList) {
		this.allTestKaplamaTozEpoksiXcutSonucList = allTestKaplamaTozEpoksiXcutSonucList;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
