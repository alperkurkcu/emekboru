/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaFlowCoatPinholeSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaFlowCoatPinholeSonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testKaplamaFlowCoatPinholeSonucBean")
@ViewScoped
public class TestKaplamaFlowCoatPinholeSonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<TestKaplamaFlowCoatPinholeSonuc> allKaplamaFlowCoatPinholeSonucList = new ArrayList<TestKaplamaFlowCoatPinholeSonuc>();
	private TestKaplamaFlowCoatPinholeSonuc testKaplamaFlowCoatPinholeSonucForm = new TestKaplamaFlowCoatPinholeSonuc();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;
	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addKaplamaFlowCoatPinholeSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaFlowCoatPinholeSonucManager tkdsManager = new TestKaplamaFlowCoatPinholeSonucManager();

		if (testKaplamaFlowCoatPinholeSonucForm.getId() == null) {

			testKaplamaFlowCoatPinholeSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaFlowCoatPinholeSonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaFlowCoatPinholeSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaFlowCoatPinholeSonucForm.setBagliTestId(bagliTest);
			testKaplamaFlowCoatPinholeSonucForm.setBagliGlobalId(bagliTest);

			tkdsManager.enterNew(testKaplamaFlowCoatPinholeSonucForm);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaFlowCoatPinholeSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaFlowCoatPinholeSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			tkdsManager.updateEntity(testKaplamaFlowCoatPinholeSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");

		}
		fillTestList(prmGlobalId, prmPipeId);
	}

	public void addKaplamaFlowCoatPinhole() {
		testKaplamaFlowCoatPinholeSonucForm = new TestKaplamaFlowCoatPinholeSonuc();
		updateButtonRender = false;
	}

	public void kaplamaFlowCoatPinholeListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(int globalId, Integer pipeId2) {
		allKaplamaFlowCoatPinholeSonucList = TestKaplamaFlowCoatPinholeSonucManager
				.getAllTestKaplamaFlowCoatPinholeSonuc(globalId, pipeId2);
		testKaplamaFlowCoatPinholeSonucForm = new TestKaplamaFlowCoatPinholeSonuc();
	}

	public void deleteTestKaplamaFlowCoatPinholeSonuc() {

		bagliTestId = testKaplamaFlowCoatPinholeSonucForm.getBagliTestId()
				.getId();

		if (testKaplamaFlowCoatPinholeSonucForm == null
				|| testKaplamaFlowCoatPinholeSonucForm.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaFlowCoatPinholeSonucManager tkdsManager = new TestKaplamaFlowCoatPinholeSonucManager();
		tkdsManager.delete(testKaplamaFlowCoatPinholeSonucForm);
		testKaplamaFlowCoatPinholeSonucForm = new TestKaplamaFlowCoatPinholeSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setters getters

	/**
	 * @return the allKaplamaFlowCoatPinholeSonucList
	 */
	public List<TestKaplamaFlowCoatPinholeSonuc> getAllKaplamaFlowCoatPinholeSonucList() {
		return allKaplamaFlowCoatPinholeSonucList;
	}

	/**
	 * @param allKaplamaFlowCoatPinholeSonucList
	 *            the allKaplamaFlowCoatPinholeSonucList to set
	 */
	public void setAllKaplamaFlowCoatPinholeSonucList(
			List<TestKaplamaFlowCoatPinholeSonuc> allKaplamaFlowCoatPinholeSonucList) {
		this.allKaplamaFlowCoatPinholeSonucList = allKaplamaFlowCoatPinholeSonucList;
	}

	/**
	 * @return the testKaplamaFlowCoatPinholeSonucForm
	 */
	public TestKaplamaFlowCoatPinholeSonuc getTestKaplamaFlowCoatPinholeSonucForm() {
		return testKaplamaFlowCoatPinholeSonucForm;
	}

	/**
	 * @param testKaplamaFlowCoatPinholeSonucForm
	 *            the testKaplamaFlowCoatPinholeSonucForm to set
	 */
	public void setTestKaplamaFlowCoatPinholeSonucForm(
			TestKaplamaFlowCoatPinholeSonuc testKaplamaFlowCoatPinholeSonucForm) {
		this.testKaplamaFlowCoatPinholeSonucForm = testKaplamaFlowCoatPinholeSonucForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
