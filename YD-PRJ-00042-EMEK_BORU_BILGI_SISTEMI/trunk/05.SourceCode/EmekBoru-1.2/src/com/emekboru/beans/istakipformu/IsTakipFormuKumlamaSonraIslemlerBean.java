/**
 * 
 */
package com.emekboru.beans.istakipformu;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isTakipFormuKumlamaSonraIslemlerBean")
@ViewScoped
public class IsTakipFormuKumlamaSonraIslemlerBean implements Serializable {

	private static final long serialVersionUID = 1L;

	// setters getters

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
