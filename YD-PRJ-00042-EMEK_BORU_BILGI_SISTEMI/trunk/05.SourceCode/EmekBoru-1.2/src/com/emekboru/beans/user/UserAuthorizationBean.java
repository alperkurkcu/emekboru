/**
 * 
 */
package com.emekboru.beans.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.LkYetkiRol;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.YetkiMenuKullaniciErisim;
import com.emekboru.jpaman.LkYetkiRolManager;
import com.emekboru.jpaman.UserManager;
import com.emekboru.jpaman.YetkiMenuKullaniciErisimManager;
import com.emekboru.jpaman.YetkiMenuManager;

/**
 * @author kursat
 * 
 */

@ManagedBean(name = "userAuthorizationBean")
@ViewScoped
public class UserAuthorizationBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private SystemUser[] selectedUsers;
	private LkYetkiRol[] selectedLkYetkiRols;

	private List<SystemUser> allSystemUsers = new ArrayList<SystemUser>();
	private List<LkYetkiRol> allLkYetkiRols = new ArrayList<LkYetkiRol>();

	private UserManager userManager = new UserManager();
	private YetkiMenuKullaniciErisimManager yetkiMenuKullaniciErisimManager = new YetkiMenuKullaniciErisimManager();

	/**
	 * 
	 */
	public UserAuthorizationBean() {

		allSystemUsers = userManager.findAllUsersActivePassive(true);
		// allLkYetkiRols = LkYetkiRolManager.getAllYetkiRolList();

		allLkYetkiRols = LkYetkiRolManager.findBySubMenu(false);

	}

	public void deleteAllAuthorizationsFromEmployees() {

		if (selectedUsers.length <= 0) {

			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Çalışan seçmediniz!", null));
			return;
		}

		for (SystemUser systemUser : selectedUsers) {

			List<YetkiMenuKullaniciErisim> selectedUsersYetkiMenuKullaniciErisims = yetkiMenuKullaniciErisimManager
					.findByField(YetkiMenuKullaniciErisim.class, "systemUser",
							systemUser);

			for (YetkiMenuKullaniciErisim yetkiMenuKullaniciErisim : selectedUsersYetkiMenuKullaniciErisims) {
				yetkiMenuKullaniciErisimManager
						.delete(yetkiMenuKullaniciErisim);
			}

		}

		FacesContext.getCurrentInstance().addMessage(
				null,
				new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Kullanıcıdan tüm yetkiler silindi!", null));
	}

	public void addSelectedAuthorizationsToEmployees() {

		if (selectedUsers.length <= 0) {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Çalışan seçmediniz!", null));
			return;
		}
		if (selectedLkYetkiRols.length <= 0) {

			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Yetki seçmediniz!", null));
			return;
		}

		List<YetkiMenuKullaniciErisim> yetkiMenuKullaniciErisimsOfSystemUser = new ArrayList<YetkiMenuKullaniciErisim>();
		List<LkYetkiRol> lkYetkiRols = new ArrayList<LkYetkiRol>();
		YetkiMenuKullaniciErisim newYetkiMenuKullaniciErisim;

		for (SystemUser systemUser : selectedUsers) {
			yetkiMenuKullaniciErisimsOfSystemUser = yetkiMenuKullaniciErisimManager
					.findByField(YetkiMenuKullaniciErisim.class, "systemUser",
							systemUser);

			for (YetkiMenuKullaniciErisim yetkiMenuKullaniciErisim : yetkiMenuKullaniciErisimsOfSystemUser) {
				lkYetkiRols.add(yetkiMenuKullaniciErisim.getLkYetkiRol());
			}

			for (LkYetkiRol lkYetkiRol : selectedLkYetkiRols) {

				if (lkYetkiRols.contains(lkYetkiRol))
					continue;

				newYetkiMenuKullaniciErisim = new YetkiMenuKullaniciErisim();

				newYetkiMenuKullaniciErisim.setLkYetkiRol(lkYetkiRol);
				newYetkiMenuKullaniciErisim.setSystemUser(systemUser);

				newYetkiMenuKullaniciErisim.setYetkiMenu(YetkiMenuManager
						.getYetkiMenuById(lkYetkiRol.getMenu().getId()));
				newYetkiMenuKullaniciErisim
						.setEklemeZamani(new java.sql.Timestamp(
								new java.util.Date().getTime()));
				newYetkiMenuKullaniciErisim.setEkleyenKullanici(userBean
						.getUser().getId());

				yetkiMenuKullaniciErisimManager
						.enterNew(newYetkiMenuKullaniciErisim);
			}
		}

		FacesContext.getCurrentInstance().addMessage(
				null,
				new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Seçilen yetkiler başarıyla atandı!", null));
	}

	public void setEmployeesAuthorizationsAsSelectedAuthorizations() {

		if (selectedUsers.length <= 0) {

			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Çalışan seçmediniz!", null));
			return;
		}
		if (selectedLkYetkiRols.length <= 0) {

			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Yetki seçmediniz!", null));
			return;
		}
		deleteAllAuthorizationsFromEmployees();

		YetkiMenuKullaniciErisim yetkiMenuKullaniciErisim;
		YetkiMenuKullaniciErisimManager yetkiMenuKullaniciErisimManager = new YetkiMenuKullaniciErisimManager();

		for (SystemUser systemUser : selectedUsers) {
			for (LkYetkiRol lkYetkiRol : selectedLkYetkiRols) {

				yetkiMenuKullaniciErisim = new YetkiMenuKullaniciErisim();

				yetkiMenuKullaniciErisim.setLkYetkiRol(lkYetkiRol);
				yetkiMenuKullaniciErisim.setSystemUser(systemUser);

				yetkiMenuKullaniciErisim.setYetkiMenu(YetkiMenuManager
						.getYetkiMenuById(lkYetkiRol.getMenu().getId()));
				yetkiMenuKullaniciErisim
						.setEklemeZamani(new java.sql.Timestamp(
								new java.util.Date().getTime()));
				yetkiMenuKullaniciErisim.setEkleyenKullanici(userBean.getUser()
						.getId());

				yetkiMenuKullaniciErisimManager
						.enterNew(yetkiMenuKullaniciErisim);
			}
		}

		FacesContext.getCurrentInstance().addMessage(
				null,
				new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Seçilen yetkiler başarıyla atandı!", null));
	}

	// ///////////////////////////////////////////////////
	// ///////// G E T T E R S - S E T T E R S ///////////
	// ///////////////////////////////////////////////////

	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public SystemUser[] getSelectedUsers() {
		return selectedUsers;
	}

	public void setSelectedUsers(SystemUser[] selectedUsers) {
		this.selectedUsers = selectedUsers;
	}

	public List<SystemUser> getAllSystemUsers() {
		return allSystemUsers;
	}

	public void setAllSystemUsers(List<SystemUser> allSystemUsers) {
		this.allSystemUsers = allSystemUsers;
	}

	public LkYetkiRol[] getSelectedLkYetkiRols() {
		return selectedLkYetkiRols;
	}

	public void setSelectedLkYetkiRols(LkYetkiRol[] selectedLkYetkiRols) {
		this.selectedLkYetkiRols = selectedLkYetkiRols;
	}

	public List<LkYetkiRol> getAllLkYetkiRols() {
		return allLkYetkiRols;
	}

	public void setAllLkYetkiRols(List<LkYetkiRol> allLkYetkiRols) {
		this.allLkYetkiRols = allLkYetkiRols;
	}

}