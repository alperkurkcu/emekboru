/**
 * 
 */
package com.emekboru.beans.coatingmaterials;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.coatingmaterials.KaplamaMalzemeKullanmaDisKumlama;
import com.emekboru.jpaman.CoatRawMaterialManager;
import com.emekboru.jpaman.coatingmaterials.KaplamaMalzemeKullanmaDisKumlamaManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "kaplamaMalzemeKullanmaDisKumlamaBean")
@ViewScoped
public class KaplamaMalzemeKullanmaDisKumlamaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<KaplamaMalzemeKullanmaDisKumlama> allKaplamaMalzemeKullanmaDisKumlamaList = new ArrayList<KaplamaMalzemeKullanmaDisKumlama>();
	private KaplamaMalzemeKullanmaDisKumlama kaplamaMalzemeKullanmaDisKumlamaForm = new KaplamaMalzemeKullanmaDisKumlama();

	private CoatRawMaterial selectedMaterial = new CoatRawMaterial();
	private List<CoatRawMaterial> coatRawMaterialList;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	private BigDecimal kullanilanMiktar;

	private boolean durum = false;

	public KaplamaMalzemeKullanmaDisKumlamaBean() {

		coatRawMaterialList = new ArrayList<CoatRawMaterial>();
		CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
		coatRawMaterialList = materialManager
				.findUnfinishedCoatMaterialByType(1001);// kumlama type
	}

	public void addNew() {
		kaplamaMalzemeKullanmaDisKumlamaForm = new KaplamaMalzemeKullanmaDisKumlama();
		updateButtonRender = false;
	}

	public void addMalzemeKullanmaSonuc(ActionEvent e) {

		if (kaplamaMalzemeKullanmaDisKumlamaForm.getHarcananMiktar() == null) {
			kaplamaMalzemeKullanmaDisKumlamaForm
					.setHarcananMiktar(BigDecimal.ZERO);
		}

		if (!malzemeKontrol()) {
			return;
		}

		KaplamaMalzemeKullanmaDisKumlamaManager malzemeKullanmaManager = new KaplamaMalzemeKullanmaDisKumlamaManager();

		if (kaplamaMalzemeKullanmaDisKumlamaForm.getId() == null) {

			try {
				kaplamaMalzemeKullanmaDisKumlamaForm
						.setEklemeZamani(UtilInsCore.getTarihZaman());
				kaplamaMalzemeKullanmaDisKumlamaForm
						.setEkleyenKullanici(userBean.getUser().getId());
				kaplamaMalzemeKullanmaDisKumlamaForm
						.setCoatRawMaterial(selectedMaterial);
				malzemeKullanmaManager
						.enterNew(kaplamaMalzemeKullanmaDisKumlamaForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"DIŞ KUMLAMA MALZEMESİ İŞLEMİ BAŞARIYLA EKLENMİŞTİR!"));

				kullanilanMiktar = kaplamaMalzemeKullanmaDisKumlamaForm
						.getHarcananMiktar();
				durum = true;
				malzemeGuncelle();

			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"DIŞ KUMLAMA MALZEMESİ İŞLEMİ EKLENEMEDİ!", null));
				System.out
						.println("KaplamaMalzemeKullanmaDisKumlamaBean.addMalzemeKullanmaSonuc-HATA-EKLEME");
			}
		} else {

			try {

				kaplamaMalzemeKullanmaDisKumlamaForm
						.setGuncellemeZamani(UtilInsCore.getTarihZaman());
				kaplamaMalzemeKullanmaDisKumlamaForm
						.setGuncelleyenKullanici(userBean.getUser().getId());
				malzemeKullanmaManager
						.updateEntity(kaplamaMalzemeKullanmaDisKumlamaForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								"DIŞ KUMLAMA MALZEMESİ İŞLEMİ BAŞARIYLA GÜNCELLENMİŞTİR!"));

				kullanilanMiktar = kaplamaMalzemeKullanmaDisKumlamaForm
						.getHarcananMiktar();
				durum = true;
				malzemeGuncelle();

			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"DIŞ KUMLAMA MALZEMESİ KULLANMA İŞLEMİ GÜNCELLENEMEDİ!",
								null));
				System.out
						.println("KaplamaMalzemeKullanmaDisKumlamaBean.addMalzemeKullanmaSonuc-HATA-GÜNCELLEME"
								+ e.toString());
			}

		}

		fillTestList();
	}

	@SuppressWarnings("static-access")
	public void fillTestList() {

		KaplamaMalzemeKullanmaDisKumlamaManager malzemeKullanmaManager = new KaplamaMalzemeKullanmaDisKumlamaManager();
		allKaplamaMalzemeKullanmaDisKumlamaList = malzemeKullanmaManager
				.getAllKaplamaMalzemeKullanmaDisKumlama();
	}

	public void kaplamaListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void deleteMalzemeKullanmaSonuc(ActionEvent e) {

		KaplamaMalzemeKullanmaDisKumlamaManager malzemeKullanmaManager = new KaplamaMalzemeKullanmaDisKumlamaManager();

		if (kaplamaMalzemeKullanmaDisKumlamaForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		kullanilanMiktar = kaplamaMalzemeKullanmaDisKumlamaForm
				.getHarcananMiktar();

		durum = false;
		malzemeGuncelle();

		malzemeKullanmaManager.delete(kaplamaMalzemeKullanmaDisKumlamaForm);
		kaplamaMalzemeKullanmaDisKumlamaForm = new KaplamaMalzemeKullanmaDisKumlama();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"DIŞ KUMLAMA MALZEMESİ KULLANMA İŞLEMİ BAŞARIYLA SİLİNMİŞTİR!"));

		fillTestList();
	}

	public void malzemeGuncelle() {

		CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
		if (durum) {// enternew
			selectedMaterial.setRemainingAmount(selectedMaterial
					.getRemainingAmount() - kullanilanMiktar.doubleValue());
			materialManager.updateEntity(selectedMaterial);
		} else if (!durum) {// delete
			kaplamaMalzemeKullanmaDisKumlamaForm.getCoatRawMaterial()
					.setRemainingAmount(
							kaplamaMalzemeKullanmaDisKumlamaForm
									.getCoatRawMaterial().getRemainingAmount()
									+ kullanilanMiktar.doubleValue());
			materialManager.updateEntity(kaplamaMalzemeKullanmaDisKumlamaForm
					.getCoatRawMaterial());
		}
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"DIŞ KUMLAMA MALZEMESİ BAŞARIYLA GÜNCELLENMİŞTİR!"));
	}

	public boolean malzemeKontrol() {

		FacesContext context = FacesContext.getCurrentInstance();
		if (kaplamaMalzemeKullanmaDisKumlamaForm.getHarcananMiktar()
				.floatValue() > selectedMaterial.getRemainingAmount()) {
			context.addMessage(
					null,
					new FacesMessage(
							"KULLANILACAK MİKTAR KALAN MALZEME MİKTARINDAN BÜYÜKTÜR! LÜTFEN KONTROL EDİNİZ!"));
			return false;
		} else {
			return true;
		}
	}

	// getters setters

	/**
	 * @return the allKaplamaMalzemeKullanmaDisKumlamaList
	 */
	public List<KaplamaMalzemeKullanmaDisKumlama> getAllKaplamaMalzemeKullanmaDisKumlamaList() {
		return allKaplamaMalzemeKullanmaDisKumlamaList;
	}

	/**
	 * @param allKaplamaMalzemeKullanmaDisKumlamaList
	 *            the allKaplamaMalzemeKullanmaDisKumlamaList to set
	 */
	public void setAllKaplamaMalzemeKullanmaDisKumlamaList(
			List<KaplamaMalzemeKullanmaDisKumlama> allKaplamaMalzemeKullanmaDisKumlamaList) {
		this.allKaplamaMalzemeKullanmaDisKumlamaList = allKaplamaMalzemeKullanmaDisKumlamaList;
	}

	/**
	 * @return the kaplamaMalzemeKullanmaDisKumlamaForm
	 */
	public KaplamaMalzemeKullanmaDisKumlama getKaplamaMalzemeKullanmaDisKumlamaForm() {
		return kaplamaMalzemeKullanmaDisKumlamaForm;
	}

	/**
	 * @param kaplamaMalzemeKullanmaDisKumlamaForm
	 *            the kaplamaMalzemeKullanmaDisKumlamaForm to set
	 */
	public void setKaplamaMalzemeKullanmaDisKumlamaForm(
			KaplamaMalzemeKullanmaDisKumlama kaplamaMalzemeKullanmaDisKumlamaForm) {
		this.kaplamaMalzemeKullanmaDisKumlamaForm = kaplamaMalzemeKullanmaDisKumlamaForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the selectedMaterial
	 */
	public CoatRawMaterial getSelectedMaterial() {
		return selectedMaterial;
	}

	/**
	 * @param selectedMaterial
	 *            the selectedMaterial to set
	 */
	public void setSelectedMaterial(CoatRawMaterial selectedMaterial) {
		this.selectedMaterial = selectedMaterial;
	}

	/**
	 * @return the coatRawMaterialList
	 */
	public List<CoatRawMaterial> getCoatRawMaterialList() {
		return coatRawMaterialList;
	}

	/**
	 * @param coatRawMaterialList
	 *            the coatRawMaterialList to set
	 */
	public void setCoatRawMaterialList(List<CoatRawMaterial> coatRawMaterialList) {
		this.coatRawMaterialList = coatRawMaterialList;
	}

}
