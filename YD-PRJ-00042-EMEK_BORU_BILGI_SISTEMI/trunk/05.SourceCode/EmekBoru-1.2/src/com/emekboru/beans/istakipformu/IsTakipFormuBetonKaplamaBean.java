/**
 * 
 */
package com.emekboru.beans.istakipformu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.istakipformu.IsTakipFormuBetonKaplama;
import com.emekboru.jpa.istakipformu.IsTakipFormuBetonKaplamaBitinceIslemler;
import com.emekboru.jpa.istakipformu.IsTakipFormuBetonKaplamaOnceIslemler;
import com.emekboru.jpa.istakipformu.IsTakipFormuBetonKaplamaSonraIslemler;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.istakipformu.IsTakipFormuBetonKaplamaBitinceIslemlerManager;
import com.emekboru.jpaman.istakipformu.IsTakipFormuBetonKaplamaManager;
import com.emekboru.jpaman.istakipformu.IsTakipFormuBetonKaplamaOnceIslemlerManager;
import com.emekboru.jpaman.istakipformu.IsTakipFormuBetonKaplamaSonraIslemlerManager;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isTakipFormuBetonKaplamaBean")
@ViewScoped
public class IsTakipFormuBetonKaplamaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private SalesItem selectedItem = new SalesItem();

	private List<IsTakipFormuBetonKaplama> allIsTakipFormuBetonKaplamaList = new ArrayList<IsTakipFormuBetonKaplama>();
	private IsTakipFormuBetonKaplama isTakipFormuBetonKaplamaForm = new IsTakipFormuBetonKaplama();

	private IsTakipFormuBetonKaplama newIsTakip = new IsTakipFormuBetonKaplama();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private IsTakipFormuBetonKaplamaOnceIslemler selectedIsTakipFormuBetonKaplamaOnceIslemler = new IsTakipFormuBetonKaplamaOnceIslemler();
	private IsTakipFormuBetonKaplamaSonraIslemler selectedIsTakipFormuBetonKaplamaSonraIslemler = new IsTakipFormuBetonKaplamaSonraIslemler();
	private IsTakipFormuBetonKaplamaBitinceIslemler selectedIsTakipFormuBetonKaplamaBitinceIslemler = new IsTakipFormuBetonKaplamaBitinceIslemler();
	private IsTakipFormuBetonKaplamaOnceIslemler newIsTakipFormuBetonKaplamaOnceIslemler = new IsTakipFormuBetonKaplamaOnceIslemler();
	private IsTakipFormuBetonKaplamaBitinceIslemler newIsTakipFormuBetonKaplamaBitinceIslemler = new IsTakipFormuBetonKaplamaBitinceIslemler();
	private IsTakipFormuBetonKaplamaSonraIslemler newIsTakipFormuBetonKaplamaSonraIslemler = new IsTakipFormuBetonKaplamaSonraIslemler();
	private IsTakipFormuBetonKaplama selectedIsTakipFormuBetonKaplama = new IsTakipFormuBetonKaplama();

	public void addFromIsEmri(SalesItem salesItem) {

		System.out.println("isTakipFormuBetonKaplamaBean.addFromIsEmri()");

		try {
			IsTakipFormuBetonKaplamaManager manager = new IsTakipFormuBetonKaplamaManager();

			newIsTakip.setSalesItem(salesItem);
			newIsTakip.setEklemeZamani(UtilInsCore.getTarihZaman());
			newIsTakip.setEkleyenKullanici(userBean.getUser().getId());

			newIsTakip.getIsTakipFormuBetonKaplamaOnceIslemler()
					.setEklemeZamani(UtilInsCore.getTarihZaman());
			newIsTakip.getIsTakipFormuBetonKaplamaOnceIslemler()
					.setEkleyenKullanici(userBean.getUser().getId());

			newIsTakip.getIsTakipFormuBetonKaplamaSonraIslemler()
					.setEklemeZamani(UtilInsCore.getTarihZaman());
			newIsTakip.getIsTakipFormuBetonKaplamaSonraIslemler()
					.setEkleyenKullanici(userBean.getUser().getId());

			newIsTakip.getIsTakipFormuBetonKaplamaBitinceIslemler()
					.setEklemeZamani(UtilInsCore.getTarihZaman());
			newIsTakip.getIsTakipFormuBetonKaplamaBitinceIslemler()
					.setEkleyenKullanici(userBean.getUser().getId());

			manager.enterNew(newIsTakip);
			newIsTakip = new IsTakipFormuBetonKaplama();

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"BETON KAPLAMA İŞ TAKİP FORMU EKLENMİŞTİR!"));

		} catch (Exception e) {
			System.out
					.println("isTakipFormuBetonKaplamaBean.addFromIsEmri-HATA");
		}
	}

	public void loadSelectedTakipForm() {

		reset();
		loadSelectedIsTakipFormuBetonKaplamaOnceIslemler();
		loadSelectedIsTakipFormuBetonKaplamaSonraIslemler();
		loadSelectedIsTakipFormuBetonKaplamaBitinceIslemler();
	}

	public void reset() {

		selectedIsTakipFormuBetonKaplamaOnceIslemler = new IsTakipFormuBetonKaplamaOnceIslemler();
		selectedIsTakipFormuBetonKaplamaSonraIslemler = new IsTakipFormuBetonKaplamaSonraIslemler();
		selectedIsTakipFormuBetonKaplamaBitinceIslemler = new IsTakipFormuBetonKaplamaBitinceIslemler();
	}

	public void loadSelectedIsTakipFormuBetonKaplamaOnceIslemler() {
		loadReplySelectedTakipForm();
		selectedIsTakipFormuBetonKaplamaOnceIslemler = selectedIsTakipFormuBetonKaplama
				.getIsTakipFormuBetonKaplamaOnceIslemler();
	}

	public void loadSelectedIsTakipFormuBetonKaplamaSonraIslemler() {
		loadReplySelectedTakipForm();
		selectedIsTakipFormuBetonKaplamaSonraIslemler = selectedIsTakipFormuBetonKaplama
				.getIsTakipFormuBetonKaplamaSonraIslemler();
	}

	public void loadSelectedIsTakipFormuBetonKaplamaBitinceIslemler() {
		loadReplySelectedTakipForm();
		selectedIsTakipFormuBetonKaplamaBitinceIslemler = selectedIsTakipFormuBetonKaplama
				.getIsTakipFormuBetonKaplamaBitinceIslemler();
	}

	@SuppressWarnings("static-access")
	public void loadReplySelectedTakipForm() {
		IsTakipFormuBetonKaplamaManager takipManager = new IsTakipFormuBetonKaplamaManager();
		if (takipManager.getAllIsTakipFormuBetonKaplama(
				selectedItem.getItemId()).size() > 0) {
			selectedIsTakipFormuBetonKaplama = takipManager
					.getAllIsTakipFormuBetonKaplama(selectedItem.getItemId())
					.get(0);
		} else {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"BETON KAPLAMA İŞ EMRİ EKLENMEMİŞ, LÜTFEN ÖNCE İŞ EMİRLERİNİ EKLEYİNİZ!",
							null));
		}
		return;
	}

	public void updateSelectedIsTakipFormuBetonKaplamaOnceIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuBetonKaplamaOnceIslemlerManager onceManager = new IsTakipFormuBetonKaplamaOnceIslemlerManager();

		selectedIsTakipFormuBetonKaplamaOnceIslemler
				.setGuncellemeZamani(UtilInsCore.getTarihZaman());
		selectedIsTakipFormuBetonKaplamaOnceIslemler
				.setGuncelleyenKullanici(userBean.getUser().getId());
		selectedIsTakipFormuBetonKaplamaOnceIslemler
				.setIsTakipFormuBetonKaplama(selectedIsTakipFormuBetonKaplama);

		onceManager.updateEntity(selectedIsTakipFormuBetonKaplamaOnceIslemler);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"BETON KAPLAMADA İŞE BAŞLAMADAN YAPILACAK İŞLEMLER BAŞARIYLA TANIMLANMIŞTIR!"));
	}

	public void updateSelectedIsTakipFormuBetonKaplamaSonraIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuBetonKaplamaSonraIslemlerManager sonraManager = new IsTakipFormuBetonKaplamaSonraIslemlerManager();

		selectedIsTakipFormuBetonKaplamaSonraIslemler
				.setGuncellemeZamani(UtilInsCore.getTarihZaman());
		selectedIsTakipFormuBetonKaplamaSonraIslemler
				.setGuncelleyenKullanici(userBean.getUser().getId());

		sonraManager
				.updateEntity(selectedIsTakipFormuBetonKaplamaSonraIslemler);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"BETON KAPLAMADA AYAR BİTTİKTEN SONRA YAPILACAK İŞLEMLER BAŞARIYLA TANIMLANMIŞTIR!"));

	}

	public void updateSelectedIsTakipFormuBetonKaplamaBitinceIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuBetonKaplamaBitinceIslemlerManager bitinceManager = new IsTakipFormuBetonKaplamaBitinceIslemlerManager();

		selectedIsTakipFormuBetonKaplamaBitinceIslemler
				.setGuncellemeZamani(UtilInsCore.getTarihZaman());
		selectedIsTakipFormuBetonKaplamaBitinceIslemler
				.setGuncelleyenKullanici(userBean.getUser().getId());

		bitinceManager
				.updateEntity(selectedIsTakipFormuBetonKaplamaBitinceIslemler);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"BETON KAPLAMADA ÜRETİM BİTTİKTEN SONRA YAPILACAK İŞLEMLER BAŞARIYLA TANIMLANMIŞTIR!"));

	}

	public void deleteSelectedIsTakipFormuBetonKaplamaOnceIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuBetonKaplamaOnceIslemlerManager onceManager = new IsTakipFormuBetonKaplamaOnceIslemlerManager();

		newIsTakipFormuBetonKaplamaOnceIslemler
				.setId(selectedIsTakipFormuBetonKaplamaOnceIslemler.getId());
		newIsTakipFormuBetonKaplamaOnceIslemler
				.setEklemeZamani(selectedIsTakipFormuBetonKaplamaOnceIslemler
						.getEklemeZamani());
		newIsTakipFormuBetonKaplamaOnceIslemler
				.setEkleyenKullanici(selectedIsTakipFormuBetonKaplamaOnceIslemler
						.getEkleyenKullanici());

		selectedIsTakipFormuBetonKaplamaOnceIslemler = new IsTakipFormuBetonKaplamaOnceIslemler();

		selectedIsTakipFormuBetonKaplamaOnceIslemler = newIsTakipFormuBetonKaplamaOnceIslemler;

		onceManager.updateEntity(selectedIsTakipFormuBetonKaplamaOnceIslemler);

		newIsTakipFormuBetonKaplamaOnceIslemler = new IsTakipFormuBetonKaplamaOnceIslemler();
		selectedIsTakipFormuBetonKaplamaOnceIslemler = new IsTakipFormuBetonKaplamaOnceIslemler();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"BETON KAPLAMADA İŞE BAŞLAMADAN YAPILACAK İŞLEMLER BAŞARIYLA SİLİNMİŞTİR!"));

	}

	public void deleteSelectedIsTakipFormuBetonKaplamaSonraIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuBetonKaplamaSonraIslemlerManager sonraManager = new IsTakipFormuBetonKaplamaSonraIslemlerManager();

		newIsTakipFormuBetonKaplamaSonraIslemler
				.setId(selectedIsTakipFormuBetonKaplamaSonraIslemler.getId());
		newIsTakipFormuBetonKaplamaSonraIslemler
				.setEklemeZamani(selectedIsTakipFormuBetonKaplamaSonraIslemler
						.getEklemeZamani());
		newIsTakipFormuBetonKaplamaSonraIslemler
				.setEkleyenKullanici(selectedIsTakipFormuBetonKaplamaSonraIslemler
						.getEkleyenKullanici());

		selectedIsTakipFormuBetonKaplamaSonraIslemler = new IsTakipFormuBetonKaplamaSonraIslemler();

		selectedIsTakipFormuBetonKaplamaSonraIslemler = newIsTakipFormuBetonKaplamaSonraIslemler;

		sonraManager
				.updateEntity(selectedIsTakipFormuBetonKaplamaSonraIslemler);

		newIsTakipFormuBetonKaplamaSonraIslemler = new IsTakipFormuBetonKaplamaSonraIslemler();
		selectedIsTakipFormuBetonKaplamaSonraIslemler = new IsTakipFormuBetonKaplamaSonraIslemler();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"BETON KAPLAMADA AYAR BİTTİKTEN SONRA YAPILACAK İŞLEMLER BAŞARIYLA SİLİNMİŞTİR!"));

	}

	public void deleteSelectedIsTakipFormuBetonKaplamaBitinceIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuBetonKaplamaBitinceIslemlerManager bitinceManager = new IsTakipFormuBetonKaplamaBitinceIslemlerManager();

		newIsTakipFormuBetonKaplamaBitinceIslemler
				.setId(selectedIsTakipFormuBetonKaplamaBitinceIslemler.getId());
		newIsTakipFormuBetonKaplamaBitinceIslemler
				.setEklemeZamani(selectedIsTakipFormuBetonKaplamaBitinceIslemler
						.getEklemeZamani());
		newIsTakipFormuBetonKaplamaBitinceIslemler
				.setEkleyenKullanici(selectedIsTakipFormuBetonKaplamaBitinceIslemler
						.getEkleyenKullanici());

		selectedIsTakipFormuBetonKaplamaBitinceIslemler = new IsTakipFormuBetonKaplamaBitinceIslemler();

		selectedIsTakipFormuBetonKaplamaBitinceIslemler = newIsTakipFormuBetonKaplamaBitinceIslemler;

		bitinceManager
				.updateEntity(selectedIsTakipFormuBetonKaplamaBitinceIslemler);

		newIsTakipFormuBetonKaplamaBitinceIslemler = new IsTakipFormuBetonKaplamaBitinceIslemler();
		selectedIsTakipFormuBetonKaplamaBitinceIslemler = new IsTakipFormuBetonKaplamaBitinceIslemler();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"BETON KAPLAMADA ÜRETİM BİTİNCE YAPILACAK İŞLEMLER BAŞARIYLA SİLİNMİŞTİR!"));

	}

	// setters getters
	public SalesItem getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(SalesItem selectedItem) {
		this.selectedItem = selectedItem;
	}

	public List<IsTakipFormuBetonKaplama> getAllIsTakipFormuBetonKaplamaList() {
		return allIsTakipFormuBetonKaplamaList;
	}

	public void setAllIsTakipFormuBetonKaplamaList(
			List<IsTakipFormuBetonKaplama> allIsTakipFormuBetonKaplamaList) {
		this.allIsTakipFormuBetonKaplamaList = allIsTakipFormuBetonKaplamaList;
	}

	public IsTakipFormuBetonKaplama getIsTakipFormuBetonKaplamaForm() {
		return isTakipFormuBetonKaplamaForm;
	}

	public void setIsTakipFormuBetonKaplamaForm(
			IsTakipFormuBetonKaplama isTakipFormuBetonKaplamaForm) {
		this.isTakipFormuBetonKaplamaForm = isTakipFormuBetonKaplamaForm;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public IsTakipFormuBetonKaplama getNewIsTakip() {
		return newIsTakip;
	}

	public void setNewIsTakip(IsTakipFormuBetonKaplama newIsTakip) {
		this.newIsTakip = newIsTakip;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public IsTakipFormuBetonKaplamaOnceIslemler getSelectedIsTakipFormuBetonKaplamaOnceIslemler() {
		return selectedIsTakipFormuBetonKaplamaOnceIslemler;
	}

	public void setSelectedIsTakipFormuBetonKaplamaOnceIslemler(
			IsTakipFormuBetonKaplamaOnceIslemler selectedIsTakipFormuBetonKaplamaOnceIslemler) {
		this.selectedIsTakipFormuBetonKaplamaOnceIslemler = selectedIsTakipFormuBetonKaplamaOnceIslemler;
	}

	public IsTakipFormuBetonKaplamaSonraIslemler getSelectedIsTakipFormuBetonKaplamaSonraIslemler() {
		return selectedIsTakipFormuBetonKaplamaSonraIslemler;
	}

	public void setSelectedIsTakipFormuBetonKaplamaSonraIslemler(
			IsTakipFormuBetonKaplamaSonraIslemler selectedIsTakipFormuBetonKaplamaSonraIslemler) {
		this.selectedIsTakipFormuBetonKaplamaSonraIslemler = selectedIsTakipFormuBetonKaplamaSonraIslemler;
	}

	public IsTakipFormuBetonKaplamaBitinceIslemler getSelectedIsTakipFormuBetonKaplamaBitinceIslemler() {
		return selectedIsTakipFormuBetonKaplamaBitinceIslemler;
	}

	public void setSelectedIsTakipFormuBetonKaplamaBitinceIslemler(
			IsTakipFormuBetonKaplamaBitinceIslemler selectedIsTakipFormuBetonKaplamaBitinceIslemler) {
		this.selectedIsTakipFormuBetonKaplamaBitinceIslemler = selectedIsTakipFormuBetonKaplamaBitinceIslemler;
	}

	public IsTakipFormuBetonKaplamaOnceIslemler getNewIsTakipFormuBetonKaplamaOnceIslemler() {
		return newIsTakipFormuBetonKaplamaOnceIslemler;
	}

	public void setNewIsTakipFormuBetonKaplamaOnceIslemler(
			IsTakipFormuBetonKaplamaOnceIslemler newIsTakipFormuBetonKaplamaOnceIslemler) {
		this.newIsTakipFormuBetonKaplamaOnceIslemler = newIsTakipFormuBetonKaplamaOnceIslemler;
	}

	public IsTakipFormuBetonKaplamaBitinceIslemler getNewIsTakipFormuBetonKaplamaBitinceIslemler() {
		return newIsTakipFormuBetonKaplamaBitinceIslemler;
	}

	public void setNewIsTakipFormuBetonKaplamaBitinceIslemler(
			IsTakipFormuBetonKaplamaBitinceIslemler newIsTakipFormuBetonKaplamaBitinceIslemler) {
		this.newIsTakipFormuBetonKaplamaBitinceIslemler = newIsTakipFormuBetonKaplamaBitinceIslemler;
	}

	public IsTakipFormuBetonKaplamaSonraIslemler getNewIsTakipFormuBetonKaplamaSonraIslemler() {
		return newIsTakipFormuBetonKaplamaSonraIslemler;
	}

	public void setNewIsTakipFormuBetonKaplamaSonraIslemler(
			IsTakipFormuBetonKaplamaSonraIslemler newIsTakipFormuBetonKaplamaSonraIslemler) {
		this.newIsTakipFormuBetonKaplamaSonraIslemler = newIsTakipFormuBetonKaplamaSonraIslemler;
	}

	public IsTakipFormuBetonKaplama getSelectedIsTakipFormuBetonKaplama() {
		return selectedIsTakipFormuBetonKaplama;
	}

	public void setSelectedIsTakipFormuBetonKaplama(
			IsTakipFormuBetonKaplama selectedIsTakipFormuBetonKaplama) {
		this.selectedIsTakipFormuBetonKaplama = selectedIsTakipFormuBetonKaplama;
	}

}
