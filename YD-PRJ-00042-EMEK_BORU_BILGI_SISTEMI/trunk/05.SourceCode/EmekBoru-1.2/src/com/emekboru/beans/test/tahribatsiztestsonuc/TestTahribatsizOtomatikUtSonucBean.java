/**
 * 
 */
package com.emekboru.beans.test.tahribatsiztestsonuc;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.test.tahribatsiz.TahribatsizTestOrderListBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.kalibrasyon.KalibrasyonOtomatikUltrasonik;
import com.emekboru.jpa.kalibrasyon.KalibrasyonUltrasonikLaminasyon;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizOtomatikUtSonuc;
import com.emekboru.jpaman.NondestructiveTestsSpecManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.kalibrasyon.KalibrasyonOtomatikUltrasonikManager;
import com.emekboru.jpaman.kalibrasyon.KalibrasyonUltrasonikLaminasyonManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizFloroskopikSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizGozOlcuSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizKoordinatManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizManuelUtSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizOtomatikUtSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizTamirManager;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */

@ManagedBean(name = "testTahribatsizOtomatikUtSonucBean")
@ViewScoped
public class TestTahribatsizOtomatikUtSonucBean {

	private List<TestTahribatsizOtomatikUtSonuc> allTestTahribatsizOtomatikUtSonucList = new ArrayList<TestTahribatsizOtomatikUtSonuc>();
	private TestTahribatsizOtomatikUtSonuc testTahribatsizOtomatikUtSonucForm = new TestTahribatsizOtomatikUtSonuc();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private boolean visibleButtonRender;

	private Pipe selectedPipe = new Pipe();

	private String barkodNo = null;

	Integer ilkQ = 0;
	Integer ilkL = 0;

	@ManagedProperty(value = "#{testTahribatsizGozOlcuSonucBean}")
	private TestTahribatsizGozOlcuSonucBean gozOlcuBean;

	private List<KalibrasyonOtomatikUltrasonik> kalibrasyonOnlinelar = new ArrayList<KalibrasyonOtomatikUltrasonik>();
	private KalibrasyonOtomatikUltrasonik kalibrasyonOnline = new KalibrasyonOtomatikUltrasonik();

	private List<KalibrasyonUltrasonikLaminasyon> kalibrasyonLaminasyonlar = new ArrayList<KalibrasyonUltrasonikLaminasyon>();
	private KalibrasyonUltrasonikLaminasyon kalibrasyonLaminasyon = new KalibrasyonUltrasonikLaminasyon();

	@SuppressWarnings("static-access")
	public void addOrUpdateOtomatikUtSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();

		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");
		Integer prmItemId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}
		if (prmItemId == null) {
			prmItemId = selectedPipe.getSalesItem().getItemId();
		}

		Integer testId = 0;

		TestTahribatsizOtomatikUtSonucManager otomatikUtSonucManager = new TestTahribatsizOtomatikUtSonucManager();
		NondestructiveTestsSpecManager nonManager = new NondestructiveTestsSpecManager();
		TestTahribatsizKoordinatManager koordinatManager = new TestTahribatsizKoordinatManager();

		try {
			if (testTahribatsizOtomatikUtSonucForm.getKaynakLaminasyon() == 1) {

				testId = nonManager.findByItemIdGlobalId(prmItemId, 1001)
						.get(0).getTestId();
			} else {

				testId = nonManager.findByItemIdGlobalId(prmItemId, 1002)
						.get(0).getTestId();
			}
		} catch (Exception e2) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"KOORDİNAT EKLEME BAŞARISIZ, KALİTE TANIMLARINDA EKSİK VARDIR!",
							null));
		}
		//
		// if (testTahribatsizOtomatikUtSonucForm.getKaynakLaminasyon() == 1) {
		//
		// testId = nonManager.findByItemIdGlobalId(prmItemId, 1001).get(0)
		// .getTestId();
		// } else {
		//
		// testId = nonManager.findByItemIdGlobalId(prmItemId, 1002).get(0)
		// .getTestId();
		// }

		if (testTahribatsizOtomatikUtSonucForm.getId() == null) {

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);

			// aynı koordinatı eklememe işlemi
			for (int i = 0; i < allTestTahribatsizOtomatikUtSonucList.size(); i++) {
				if ((testTahribatsizOtomatikUtSonucForm.getKoordinatQ()
						.equals(allTestTahribatsizOtomatikUtSonucList.get(i)
								.getKoordinatQ()))
						&& (testTahribatsizOtomatikUtSonucForm.getKoordinatL()
								.equals(allTestTahribatsizOtomatikUtSonucList
										.get(i).getKoordinatL()))) {
					System.out
							.println(" testTahribatsizOtomatikUtSonucBean.addOrUpdateOtomatikUtSonuc-HATA SameCoordinates");
					FacesContext context = FacesContext.getCurrentInstance();
					context.addMessage(
							null,
							new FacesMessage(
									FacesMessage.SEVERITY_FATAL,
									"AYNI KOORDİNAT TEKRARDAN EKLENEMEZ, KAYDETME HATASI!",
									null));
					return;
				}
			}

			// otomatik UT
			testTahribatsizOtomatikUtSonucForm.setTestId(testId);
			testTahribatsizOtomatikUtSonucForm.setPipe(pipe);

			testTahribatsizOtomatikUtSonucForm.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			testTahribatsizOtomatikUtSonucForm.setEkleyenEmployee(userBean
					.getUser());

			sonKalibrasyonuGoster(1);
			testTahribatsizOtomatikUtSonucForm
					.setKalibrasyonOnline(kalibrasyonOnline);

			sonKalibrasyonuGoster(2);
			testTahribatsizOtomatikUtSonucForm
					.setKalibrasyonLaminasyon(kalibrasyonLaminasyon);

			// otomatik muayene ekleme
			otomatikUtSonucManager.enterNew(testTahribatsizOtomatikUtSonucForm);

			// kalite speclerde manuelUt ve floroskopi var mi kontrolu
			Integer i = 0;
			Boolean kontrol = false;
			while (i < testTahribatsizOtomatikUtSonucForm.getPipe()
					.getSalesItem().getNondestructiveTestsSpecs().size()
					&& kontrol == false) {
				// manuelUt ve floroskopi var mi kontrolu, varsa tum diger
				// muayenelere koordinati ekleme
				if (testTahribatsizOtomatikUtSonucForm.getPipe().getSalesItem()
						.getNondestructiveTestsSpecs().get(i).getGlobalId() == 1004
						|| testTahribatsizOtomatikUtSonucForm.getPipe()
								.getSalesItem().getNondestructiveTestsSpecs()
								.get(i).getGlobalId() == 1006) {
					koordinatManager.tumTablolaraKoordinatEkleme(
							testTahribatsizOtomatikUtSonucForm.getKoordinatQ(),
							testTahribatsizOtomatikUtSonucForm.getKoordinatL(),
							pipe, "o", userBean, null,
							testTahribatsizOtomatikUtSonucForm
									.getKaynakLaminasyon().toString(), 0);
					kontrol = true;
				}
				i++;
			}

			// eger uretilen kazik borusu ise ekrandan isaretlenip otomatikman
			// manuel ve flye dusurulmesi saglanir
			if (testTahribatsizOtomatikUtSonucForm.getKazikKontrol() == true
					&& kontrol == false) {

				koordinatManager.tumTablolaraKoordinatEkleme(
						testTahribatsizOtomatikUtSonucForm.getKoordinatQ(),
						testTahribatsizOtomatikUtSonucForm.getKoordinatL(),
						pipe, "o", userBean, null,
						testTahribatsizOtomatikUtSonucForm
								.getKaynakLaminasyon().toString(), 0);
			}

			// kazık borusuysa gö otomatik düşmesi için.
			if (testTahribatsizOtomatikUtSonucForm.getKazikKontrol() == true) {
				TestTahribatsizGozOlcuSonucManager gozManager = new TestTahribatsizGozOlcuSonucManager();
				if (gozManager.getAllTestTahribatsizGozOlcuSonuc(prmPipeId)
						.size() == 0) {
					gozOlcuEkle();
				}
			}

			// testlerin hangi borudan yapilacagi kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 3, true);

			this.testTahribatsizOtomatikUtSonucForm = new TestTahribatsizOtomatikUtSonuc();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"OTOMATİK UT KOORDİNATI BAŞARI İLE KAYDEDİLMİŞTİR!"));
		} else {

			// otomatik ut
			testTahribatsizOtomatikUtSonucForm.setGuncellemeZamani(UtilInsCore
					.getTarihZaman());
			testTahribatsizOtomatikUtSonucForm.setGuncelleyenEmployee(userBean
					.getUser());
			testTahribatsizOtomatikUtSonucForm.setTestId(testId);

			otomatikUtSonucManager
					.updateEntity(testTahribatsizOtomatikUtSonucForm);

			koordinatManager.tumTablolardaKoordinatGuncelleme(ilkQ, ilkL,
					testTahribatsizOtomatikUtSonucForm.getKoordinatQ(),
					testTahribatsizOtomatikUtSonucForm.getKoordinatL(),
					selectedPipe, userBean);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"OTOMATİK UT KOORDİNATI BAŞARI İLE GÜNCELLENMİŞTİR!"));
		}

		fillTestList(prmPipeId);
		testTahribatsizOtomatikUtSonucForm = new TestTahribatsizOtomatikUtSonuc();
		updateButtonRender = false;
		// sayfada barkoddan okutunca order item pipe seçimi olmadıgı için
		// sıkıntı cıkıyordu
		TahribatsizTestOrderListBean ttolb = new TahribatsizTestOrderListBean();
		ttolb.loadOrderDetails(selectedPipe.getSalesItem().getItemId());
	}

	public void gozOlcuEkle() {

		try {
			gozOlcuBean.addFromOtomatik(testTahribatsizOtomatikUtSonucForm,
					userBean);
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"GÖZ ÖLÇÜ BAŞARI İLE KAYDEDİLMİŞTİR!"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"GÖZ ÖLÇÜ KAYDDEDİLEMEDİ, HATA!"));
		}

	}

	public void fillTestList(Integer pipeId) {

		try {
			TestTahribatsizOtomatikUtSonucManager manager = new TestTahribatsizOtomatikUtSonucManager();
			allTestTahribatsizOtomatikUtSonucList = manager
					.getAllTestTahribatsizOtomatikUtSonuc(pipeId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void addTestTahribatsizOtomatikUtSonuc() {
		testTahribatsizOtomatikUtSonucForm = new TestTahribatsizOtomatikUtSonuc();
		updateButtonRender = false;
	}

	public void otomatikUtListener(SelectEvent event) {

		updateButtonRender = true;
		visibleButtonRender = false;
		ilkQ = testTahribatsizOtomatikUtSonucForm.getKoordinatQ();
		ilkL = testTahribatsizOtomatikUtSonucForm.getKoordinatL();
	}

	@SuppressWarnings("static-access")
	public void deleteOtomatikUtSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		TestTahribatsizOtomatikUtSonucManager otomatikUtSonucManager = new TestTahribatsizOtomatikUtSonucManager();
		TestTahribatsizManuelUtSonucManager manuelManager = new TestTahribatsizManuelUtSonucManager();
		TestTahribatsizFloroskopikSonucManager flManager = new TestTahribatsizFloroskopikSonucManager();
		TestTahribatsizTamirManager tamirManager = new TestTahribatsizTamirManager();

		if (testTahribatsizOtomatikUtSonucForm.getId() == null) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"LİSTEDEN SEÇİLMİŞ BİR DEĞER YOKTUR!", null));
			return;
		}

		try {

			Integer manuelId = manuelManager
					.findByKoordinatsPipeId(
							testTahribatsizOtomatikUtSonucForm.getKoordinatQ(),
							testTahribatsizOtomatikUtSonucForm.getKoordinatL(),
							prmPipeId).get(0).getId();

			// tamir kontrol
			// if (tamirManager.tamirKontrol(prmPipeId, manuelId)) {
			//
			// tamirManager.delete(tamirManager.tamirBul(prmPipeId, manuelId)
			// .get(0));
			//
			// FacesContext context = FacesContext.getCurrentInstance();
			// context.addMessage(null, new FacesMessage(
			// FacesMessage.SEVERITY_WARN,
			// "KOORDİNATA BAĞLI TAMİR VERİLERİ SİLİNMİŞTİR!", null));
			// }
			// } catch (Exception e2) {
			// // TODO Auto-generated catch block
			// e2.printStackTrace();
			// FacesContext context = FacesContext.getCurrentInstance();
			// context.addMessage(null, new FacesMessage(
			// FacesMessage.SEVERITY_ERROR,
			// "TAMİR KOORDİNATI BULUNAMAMIŞTIR, SİLİNEMEMİŞTİR!", null));
			// }

			if (tamirManager.tamirKontrol(prmPipeId, manuelId)) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KOORDİNATA BAĞLI TAMİR VERİLERİ OLDUĞUNDAN SİLME İŞLEMİ YAPILAMAZ!",
								null));
				return;
			}
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"TAMİR KOORDİNATI BULUNAMAMIŞTIR!", null));
		}

		try {
			// manuel kontrol
			if (manuelManager.findByKoordinatsPipeId(
					testTahribatsizOtomatikUtSonucForm.getKoordinatQ(),
					testTahribatsizOtomatikUtSonucForm.getKoordinatL(),
					prmPipeId).size() > 0) {

				manuelManager.delete(manuelManager.findByKoordinatsPipeId(
						testTahribatsizOtomatikUtSonucForm.getKoordinatQ(),
						testTahribatsizOtomatikUtSonucForm.getKoordinatL(),
						prmPipeId).get(0));

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_WARN,
						"MANUEL KOORDİNATI SİLİNMİŞTİR!", null));
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_WARN,
					"MANUEL KOORDİNATI BULUNAMAMIŞTIR, SİLİNEMEMİŞTİR!", null));
		}

		try {
			// fl kontrol
			if (flManager.findByKoordinatsPipeId(
					testTahribatsizOtomatikUtSonucForm.getKoordinatQ(),
					testTahribatsizOtomatikUtSonucForm.getKoordinatL(),
					prmPipeId).size() > 0) {

				flManager.delete(flManager.findByKoordinatsPipeId(
						testTahribatsizOtomatikUtSonucForm.getKoordinatQ(),
						testTahribatsizOtomatikUtSonucForm.getKoordinatL(),
						prmPipeId).get(0));

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_WARN,
						"FLOROSKOPİK KOORDİNATI SİLİNMİŞTİR!", null));
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"FLOROSKOPİ KOORDİNATI BULUNAMAMIŞTIR, SİLİNEMEMİŞTİR!",
					null));
		}

		otomatikUtSonucManager.delete(testTahribatsizOtomatikUtSonucForm);
		testTahribatsizOtomatikUtSonucForm = new TestTahribatsizOtomatikUtSonuc();
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
				"OTOMATİK KOORDİNATI SİLİNMİŞTİR!", null));

		fillTestList(prmPipeId);
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " BARKOD NUMARALI BORU SİSTEMDE YOKTUR!", null));
			visibleButtonRender = false;
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			TestTahribatsizOtomatikUtSonucManager otomatikUtSonucManager = new TestTahribatsizOtomatikUtSonucManager();
			allTestTahribatsizOtomatikUtSonucList = otomatikUtSonucManager
					.getAllTestTahribatsizOtomatikUtSonuc(selectedPipe
							.getPipeId());
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
			visibleButtonRender = true;
		}

		KalibrasyonOtomatikUltrasonikManager koManager = new KalibrasyonOtomatikUltrasonikManager();
		kalibrasyonOnlinelar = koManager.getAllKalibrasyon(1);

		KalibrasyonUltrasonikLaminasyonManager klManager = new KalibrasyonUltrasonikLaminasyonManager();
		kalibrasyonLaminasyonlar = klManager.getAllKalibrasyon(1);
	}

	@SuppressWarnings("static-access")
	public void sonKalibrasyonuGoster(Integer prmType) {
		if (prmType == 1) {
			kalibrasyonOnline = new KalibrasyonOtomatikUltrasonik();
			KalibrasyonOtomatikUltrasonikManager kalibrasyonManager = new KalibrasyonOtomatikUltrasonikManager();
			kalibrasyonOnline = kalibrasyonManager.getAllKalibrasyon(1).get(0);
		} else if (prmType == 2) {
			kalibrasyonLaminasyon = new KalibrasyonUltrasonikLaminasyon();
			KalibrasyonUltrasonikLaminasyonManager kalibrasyonManager = new KalibrasyonUltrasonikLaminasyonManager();
			kalibrasyonLaminasyon = kalibrasyonManager.getAllKalibrasyon(1)
					.get(0);
		}
	}

	public void kalibrasyonGoster(Integer prmType) {
		if (prmType == 1) {
			kalibrasyonLaminasyon = new KalibrasyonUltrasonikLaminasyon();
			kalibrasyonOnline = new KalibrasyonOtomatikUltrasonik();
			kalibrasyonOnline = testTahribatsizOtomatikUtSonucForm
					.getKalibrasyonOnline();
		} else if (prmType == 2) {
			kalibrasyonOnline = new KalibrasyonOtomatikUltrasonik();
			kalibrasyonLaminasyon = new KalibrasyonUltrasonikLaminasyon();
			kalibrasyonLaminasyon = testTahribatsizOtomatikUtSonucForm
					.getKalibrasyonLaminasyon();
		}

	}

	public void kalibrasyonAta(Integer prmType) {
		TestTahribatsizOtomatikUtSonucManager otomatikManager = new TestTahribatsizOtomatikUtSonucManager();
		if (prmType == 1) {
			try {
				for (int i = 0; i < allTestTahribatsizOtomatikUtSonucList
						.size(); i++) {
					allTestTahribatsizOtomatikUtSonucList.get(i)
							.setKalibrasyonOnline(kalibrasyonOnline);
					otomatikManager
							.updateEntity(allTestTahribatsizOtomatikUtSonucList
									.get(i));
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"KALİBRASYON ATAMA BAŞARISIZ, TEKRAR DENEYİNİZ!", null));
				return;
			}
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "KALİBRASYON ATAMA BAŞARILI!",
					null));
		} else if (prmType == 2) {
			try {
				for (int i = 0; i < allTestTahribatsizOtomatikUtSonucList
						.size(); i++) {
					allTestTahribatsizOtomatikUtSonucList.get(i)
							.setKalibrasyonLaminasyon(kalibrasyonLaminasyon);
					otomatikManager
							.updateEntity(allTestTahribatsizOtomatikUtSonucList
									.get(i));
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"KALİBRASYON ATAMA BAŞARISIZ, TEKRAR DENEYİNİZ!", null));
				return;
			}
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "KALİBRASYON ATAMA BAŞARILI!",
					null));
		}
	}

	// SETTERS GETTERS

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public List<TestTahribatsizOtomatikUtSonuc> getAllTestTahribatsizOtomatikUtSonucList() {
		return allTestTahribatsizOtomatikUtSonucList;
	}

	public void setAllTestTahribatsizOtomatikUtSonucList(
			List<TestTahribatsizOtomatikUtSonuc> allTestTahribatsizOtomatikUtSonucList) {
		this.allTestTahribatsizOtomatikUtSonucList = allTestTahribatsizOtomatikUtSonucList;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public TestTahribatsizOtomatikUtSonuc getTestTahribatsizOtomatikUtSonucForm() {
		return testTahribatsizOtomatikUtSonucForm;
	}

	public void setTestTahribatsizOtomatikUtSonucForm(
			TestTahribatsizOtomatikUtSonuc testTahribatsizOtomatikUtSonucForm) {
		this.testTahribatsizOtomatikUtSonucForm = testTahribatsizOtomatikUtSonucForm;
	}

	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	public String getBarkodNo() {
		return barkodNo;
	}

	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	/**
	 * @return the gozOlcuBean
	 */
	public TestTahribatsizGozOlcuSonucBean getGozOlcuBean() {
		return gozOlcuBean;
	}

	/**
	 * @param gozOlcuBean
	 *            the gozOlcuBean to set
	 */
	public void setGozOlcuBean(TestTahribatsizGozOlcuSonucBean gozOlcuBean) {
		this.gozOlcuBean = gozOlcuBean;
	}

	/**
	 * @return the kalibrasyonOnlinelar
	 */
	public List<KalibrasyonOtomatikUltrasonik> getKalibrasyonOnlinelar() {
		return kalibrasyonOnlinelar;
	}

	/**
	 * @param kalibrasyonOnlinelar
	 *            the kalibrasyonOnlinelar to set
	 */
	public void setKalibrasyonOnlinelar(
			List<KalibrasyonOtomatikUltrasonik> kalibrasyonOnlinelar) {
		this.kalibrasyonOnlinelar = kalibrasyonOnlinelar;
	}

	/**
	 * @return the kalibrasyonOnline
	 */
	public KalibrasyonOtomatikUltrasonik getKalibrasyonOnline() {
		return kalibrasyonOnline;
	}

	/**
	 * @param kalibrasyonOnline
	 *            the kalibrasyonOnline to set
	 */
	public void setKalibrasyonOnline(
			KalibrasyonOtomatikUltrasonik kalibrasyonOnline) {
		this.kalibrasyonOnline = kalibrasyonOnline;
	}

	/**
	 * @return the kalibrasyonLaminasyonlar
	 */
	public List<KalibrasyonUltrasonikLaminasyon> getKalibrasyonLaminasyonlar() {
		return kalibrasyonLaminasyonlar;
	}

	/**
	 * @param kalibrasyonLaminasyonlar
	 *            the kalibrasyonLaminasyonlar to set
	 */
	public void setKalibrasyonLaminasyonlar(
			List<KalibrasyonUltrasonikLaminasyon> kalibrasyonLaminasyonlar) {
		this.kalibrasyonLaminasyonlar = kalibrasyonLaminasyonlar;
	}

	/**
	 * @return the kalibrasyonLaminasyon
	 */
	public KalibrasyonUltrasonikLaminasyon getKalibrasyonLaminasyon() {
		return kalibrasyonLaminasyon;
	}

	/**
	 * @param kalibrasyonLaminasyon
	 *            the kalibrasyonLaminasyon to set
	 */
	public void setKalibrasyonLaminasyon(
			KalibrasyonUltrasonikLaminasyon kalibrasyonLaminasyon) {
		this.kalibrasyonLaminasyon = kalibrasyonLaminasyon;
	}

	/**
	 * @return the visibleButtonRender
	 */
	public boolean isVisibleButtonRender() {
		return visibleButtonRender;
	}

}
