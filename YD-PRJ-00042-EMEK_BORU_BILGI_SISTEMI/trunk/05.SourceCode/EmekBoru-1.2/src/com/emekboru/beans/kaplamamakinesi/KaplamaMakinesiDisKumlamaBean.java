/**
 * 
 */
package com.emekboru.beans.kaplamamakinesi;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isemri.IsEmriDisKumlama;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiDisKumlama;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isemirleri.IsEmriDisKumlamaManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamamakinesi.KaplamaMakinesiDisKumlamaManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "kaplamaMakinesiDisKumlamaBean")
@ViewScoped
public class KaplamaMakinesiDisKumlamaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<KaplamaMakinesiDisKumlama> allKaplamaMakinesiDisKumlamaList = new ArrayList<KaplamaMakinesiDisKumlama>();
	private KaplamaMakinesiDisKumlama kaplamaMakinesiDisKumlamaForm = new KaplamaMakinesiDisKumlama();

	private KaplamaMakinesiDisKumlama newKumlama = new KaplamaMakinesiDisKumlama();
	private List<KaplamaMakinesiDisKumlama> kumlamaList;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	private Pipe selectedPipe = new Pipe();

	private boolean kontrol = false;

	private String barkodNo = null;

	private IsEmriDisKumlama selectedIsEmriKaplama = new IsEmriDisKumlama();

	// kaplama kalite
	private List<IsolationTestDefinition> internalIsolationTestDefinitions = new ArrayList<IsolationTestDefinition>();
	private String kumlamaKalitesi = null;

	public KaplamaMakinesiDisKumlamaBean() {

	}

	public void addKumlama(ActionEvent ae) {

		UICommand cmd = (UICommand) ae.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId != null) {
			PipeManager pipeMan = new PipeManager();
			selectedPipe = pipeMan.loadObject(Pipe.class, prmPipeId);
		}

		try {
			KaplamaMakinesiDisKumlamaManager manager = new KaplamaMakinesiDisKumlamaManager();

			newKumlama.setEklemeZamani(UtilInsCore.getTarihZaman());
			newKumlama.setEkleyenKullanici(userBean.getUser().getId());
			newKumlama.setPipe(selectedPipe);
			manager.enterNew(newKumlama);
			newKumlama = new KaplamaMakinesiDisKumlama();

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"DIŞ KUMLAMA İŞLEMİ BAŞARIYLA EKLENMİŞTİR!"));

		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL,
					"DIŞ KUMLAMA İŞLEMİ EKLENEMEDİ!", null));
			System.out.println("KaplamaMakinesiDisKumlamaBean.addKumlama-HATA");
		}

		fillTestList(prmPipeId);
		updateButtonRender = false;
	}

	public void addKumlamaSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		Boolean prmKontrol = kaplamaMakinesiDisKumlamaForm.getDurum();
		KaplamaMakinesiDisKumlamaManager kumlamaManager = new KaplamaMakinesiDisKumlamaManager();

		if (prmKontrol == null) {

			try {

				kaplamaMakinesiDisKumlamaForm.setDurum(false);
				kaplamaMakinesiDisKumlamaForm
						.setKaplamaBaslamaZamani(UtilInsCore.getTarihZaman());
				kaplamaMakinesiDisKumlamaForm
						.setKaplamaBaslamaKullanici(userBean.getUser().getId());
				kumlamaManager.updateEntity(kaplamaMakinesiDisKumlamaForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"DIŞ KUMLAMA İŞLEMİ BAŞARIYLA BAŞLANMIŞTIR!"));
			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"DIŞ KUMLAMA İŞLEMİ BAŞLANAMADI!", null));
				System.out
						.println("KaplamaMakinesiDisKumlamaBean.addKumlamaSonuc-HATA-BAŞLAMA");
			}
		} else if (prmKontrol == false) {

			try {

				kaplamaMakinesiDisKumlamaForm.setDurum(true);
				kaplamaMakinesiDisKumlamaForm.setKaplamaBitisZamani(UtilInsCore
						.getTarihZaman());
				kaplamaMakinesiDisKumlamaForm.setKaplamaBitisKullanici(userBean
						.getUser().getId());
				kumlamaManager.updateEntity(kaplamaMakinesiDisKumlamaForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"DIŞ KUMLAMA İŞLEMİ BAŞARIYLA BİTİRİLMİŞTİR!"));

			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"DIŞ KUMLAMA İŞLEMİ BİTİRİLEMEDİ!", null));
				System.out
						.println("KaplamaMakinesiDisKumlamaBean.addKumlamaSonuc-HATA-BİTİŞ"
								+ e.toString());
			}

		}

		fillTestList(prmPipeId);
	}

	@SuppressWarnings("static-access")
	public void fillTestList(Integer prmPipeId) {

		KaplamaMakinesiDisKumlamaManager kumlamaManager = new KaplamaMakinesiDisKumlamaManager();
		allKaplamaMakinesiDisKumlamaList = kumlamaManager
				.getAllKaplamaMakinesiDisKumlama(prmPipeId);
	}

	public void kumlamaListener(SelectEvent event) {
		updateButtonRender = true;
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!", null));
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			KaplamaMakinesiDisKumlamaManager kumlamaManager = new KaplamaMakinesiDisKumlamaManager();
			allKaplamaMakinesiDisKumlamaList = kumlamaManager
					.getAllKaplamaMakinesiDisKumlama(selectedPipe.getPipeId());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
		}
	}

	public void deleteKumlamaSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		KaplamaMakinesiDisKumlamaManager kumlamaManager = new KaplamaMakinesiDisKumlamaManager();

		if (kaplamaMakinesiDisKumlamaForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		kumlamaManager.delete(kaplamaMakinesiDisKumlamaForm);
		kaplamaMakinesiDisKumlamaForm = new KaplamaMakinesiDisKumlama();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"DIŞ KUMLAMA BAŞARIYLA SİLİNMİŞTİR!"));

		fillTestList(prmPipeId);
	}

	@SuppressWarnings("static-access")
	public void isEmriGoster() {

		IsolationTestDefinitionManager kaliteMan = new IsolationTestDefinitionManager();
		IsEmriDisKumlamaManager isEmriManager = new IsEmriDisKumlamaManager();
		if (isEmriManager.getAllIsEmriDisKumlama(
				selectedPipe.getSalesItem().getItemId()).size() > 0) {
			selectedIsEmriKaplama = isEmriManager.getAllIsEmriDisKumlama(
					selectedPipe.getSalesItem().getItemId()).get(0);
		} else {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					" DIŞ KUMLAMA İŞ EMRİ BULUNAMADI, LÜTFEN EKLETİNİZ!", null));
			return;
		}

		internalIsolationTestDefinitions = kaliteMan
				.findIsolationByItemIdAndIsolationType(0,
						selectedIsEmriKaplama.getSalesItem());
		for (int i = 0; i < internalIsolationTestDefinitions.size(); i++) {
			if (internalIsolationTestDefinitions.get(i).getGlobalId() == 2005) {
				kumlamaKalitesi = internalIsolationTestDefinitions.get(i)
						.getAciklama();
			}
			if (internalIsolationTestDefinitions.get(i).getGlobalId() == 2006) {
				kumlamaKalitesi = kumlamaKalitesi + " "
						+ internalIsolationTestDefinitions.get(i).getAciklama();
			}
		}
	}

	// setters getters

	public List<KaplamaMakinesiDisKumlama> getAllKaplamaMakinesiDisKumlamaList() {
		return allKaplamaMakinesiDisKumlamaList;
	}

	public void setAllKaplamaMakinesiDisKumlamaList(
			List<KaplamaMakinesiDisKumlama> allKaplamaMakinesiDisKumlamaList) {
		this.allKaplamaMakinesiDisKumlamaList = allKaplamaMakinesiDisKumlamaList;
	}

	public KaplamaMakinesiDisKumlama getKaplamaMakinesiDisKumlamaForm() {
		return kaplamaMakinesiDisKumlamaForm;
	}

	public void setKaplamaMakinesiDisKumlamaForm(
			KaplamaMakinesiDisKumlama kaplamaMakinesiDisKumlamaForm) {
		this.kaplamaMakinesiDisKumlamaForm = kaplamaMakinesiDisKumlamaForm;
	}

	public KaplamaMakinesiDisKumlama getNewKumlama() {
		return newKumlama;
	}

	public void setNewKumlama(KaplamaMakinesiDisKumlama newKumlama) {
		this.newKumlama = newKumlama;
	}

	public List<KaplamaMakinesiDisKumlama> getKumlamaList() {
		return kumlamaList;
	}

	public void setKumlamaList(List<KaplamaMakinesiDisKumlama> kumlamaList) {
		this.kumlamaList = kumlamaList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	public boolean isKontrol() {
		return kontrol;
	}

	public void setKontrol(boolean kontrol) {
		this.kontrol = kontrol;
	}

	public String getBarkodNo() {
		return barkodNo;
	}

	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public IsEmriDisKumlama getSelectedIsEmriKaplama() {
		return selectedIsEmriKaplama;
	}

	public void setSelectedIsEmriKaplama(IsEmriDisKumlama selectedIsEmriKaplama) {
		this.selectedIsEmriKaplama = selectedIsEmriKaplama;
	}

	/**
	 * @return the kumlamaKalitesi
	 */
	public String getKumlamaKalitesi() {
		return kumlamaKalitesi;
	}

	/**
	 * @param kumlamaKalitesi
	 *            the kumlamaKalitesi to set
	 */
	public void setKumlamaKalitesi(String kumlamaKalitesi) {
		this.kumlamaKalitesi = kumlamaKalitesi;
	}

}
