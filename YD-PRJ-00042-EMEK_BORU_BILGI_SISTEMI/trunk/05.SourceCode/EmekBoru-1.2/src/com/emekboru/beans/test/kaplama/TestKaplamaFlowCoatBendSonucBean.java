/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaFlowCoatBendSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaFlowCoatBendSonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testKaplamaFlowCoatBendSonucBean")
@ViewScoped
public class TestKaplamaFlowCoatBendSonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<TestKaplamaFlowCoatBendSonuc> allKaplamaFlowCoatBendSonucList = new ArrayList<TestKaplamaFlowCoatBendSonuc>();
	private TestKaplamaFlowCoatBendSonuc testKaplamaFlowCoatBendSonucForm = new TestKaplamaFlowCoatBendSonuc();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;
	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addKaplamaFlowCoatBendSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaFlowCoatBendSonucManager tkdsManager = new TestKaplamaFlowCoatBendSonucManager();

		if (testKaplamaFlowCoatBendSonucForm.getId() == null) {

			testKaplamaFlowCoatBendSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaFlowCoatBendSonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaFlowCoatBendSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaFlowCoatBendSonucForm.setBagliTestId(bagliTest);
			testKaplamaFlowCoatBendSonucForm.setBagliGlobalId(bagliTest);

			tkdsManager.enterNew(testKaplamaFlowCoatBendSonucForm);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaFlowCoatBendSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaFlowCoatBendSonucForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			tkdsManager.updateEntity(testKaplamaFlowCoatBendSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");

		}
		fillTestList(prmGlobalId, prmPipeId);
	}

	public void addKaplamaFlowCoatBend() {
		testKaplamaFlowCoatBendSonucForm = new TestKaplamaFlowCoatBendSonuc();
		updateButtonRender = false;
	}

	public void kaplamaFlowCoatBendListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(int globalId, Integer pipeId2) {
		allKaplamaFlowCoatBendSonucList = TestKaplamaFlowCoatBendSonucManager
				.getAllTestKaplamaFlowCoatBendSonuc(globalId, pipeId2);
		testKaplamaFlowCoatBendSonucForm = new TestKaplamaFlowCoatBendSonuc();
	}

	public void deleteTestKaplamaFlowCoatBendSonuc() {

		bagliTestId = testKaplamaFlowCoatBendSonucForm.getBagliTestId().getId();

		if (testKaplamaFlowCoatBendSonucForm == null
				|| testKaplamaFlowCoatBendSonucForm.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaFlowCoatBendSonucManager tkdsManager = new TestKaplamaFlowCoatBendSonucManager();
		tkdsManager.delete(testKaplamaFlowCoatBendSonucForm);
		testKaplamaFlowCoatBendSonucForm = new TestKaplamaFlowCoatBendSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setters getters
	/**
	 * @return the allKaplamaFlowCoatBendSonucList
	 */
	public List<TestKaplamaFlowCoatBendSonuc> getAllKaplamaFlowCoatBendSonucList() {
		return allKaplamaFlowCoatBendSonucList;
	}

	/**
	 * @param allKaplamaFlowCoatBendSonucList
	 *            the allKaplamaFlowCoatBendSonucList to set
	 */
	public void setAllKaplamaFlowCoatBendSonucList(
			List<TestKaplamaFlowCoatBendSonuc> allKaplamaFlowCoatBendSonucList) {
		this.allKaplamaFlowCoatBendSonucList = allKaplamaFlowCoatBendSonucList;
	}

	/**
	 * @return the testKaplamaFlowCoatBendSonucForm
	 */
	public TestKaplamaFlowCoatBendSonuc getTestKaplamaFlowCoatBendSonucForm() {
		return testKaplamaFlowCoatBendSonucForm;
	}

	/**
	 * @param testKaplamaFlowCoatBendSonucForm
	 *            the testKaplamaFlowCoatBendSonucForm to set
	 */
	public void setTestKaplamaFlowCoatBendSonucForm(
			TestKaplamaFlowCoatBendSonuc testKaplamaFlowCoatBendSonucForm) {
		this.testKaplamaFlowCoatBendSonucForm = testKaplamaFlowCoatBendSonucForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
