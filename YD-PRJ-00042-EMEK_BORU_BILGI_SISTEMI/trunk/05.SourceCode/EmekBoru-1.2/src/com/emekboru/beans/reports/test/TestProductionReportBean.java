/**
 * 
 */
package com.emekboru.beans.reports.test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizFloroskopikSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizGozOlcuSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizHidrostatik;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizManuelUtSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizOtomatikUtSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizRadyografikSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizTamir;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizTorna;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.sales.order.SalesItemManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizFloroskopikSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizGozOlcuSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizHidrostatikManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizManuelUtSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizOtomatikUtSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizRadyografikSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizTamirManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizTornaManager;
import com.emekboru.utils.ReportUtil;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testProductionReportBean")
@ViewScoped
public class TestProductionReportBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	Integer raporNo = 0;
	String muayeneyiYapan = null;
	String hazirlayan = null;

	Integer raporVardiya;
	Date raporTarihi;
	String raporTarihiAString;
	String raporTarihiBString;

	Boolean kontrol = false;

	private String downloadedFileName;
	private StreamedContent downloadedFile;
	private String realFilePath;

	@PostConstruct
	public void init() {
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void tornaResultReportXlsx(Integer prmPipeId) {

		if (raporVardiya == -1) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "VARDİYA SEÇİNİZ!", null));
			return;
		}
		if (prmPipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}
		if (raporTarihi == null || raporVardiya == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		tarihBul(raporTarihi);

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();

			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			TestTahribatsizTornaManager testTahribatsizTornaManager = new TestTahribatsizTornaManager();
			SalesItemManager salesItemManager = new SalesItemManager();

			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			List<TestTahribatsizTorna> testTahribatsizTornas = new ArrayList<TestTahribatsizTorna>();

			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			if (raporVardiya == 1) {
				testTahribatsizTornas = testTahribatsizTornaManager
						.getByPipeDateVardiya(pipes.get(0).getPipeId(),
								raporTarihiAString, raporTarihiAString,
								raporVardiya);
			} else if (raporVardiya == 0) {
				testTahribatsizTornas = testTahribatsizTornaManager
						.getByPipeDateVardiya(pipes.get(0).getPipeId(),
								raporTarihiAString, raporTarihiBString,
								raporVardiya);
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy")
					.format(testTahribatsizTornas.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			muayeneyiYapan = testTahribatsizTornas.get(0).getUser()
					.getEmployee().getFirstname()
					+ " "
					+ testTahribatsizTornas.get(0).getUser().getEmployee()
							.getLastname();
			hazirlayan = userBean.getUserFullName();

			ReportUtil reportUtil = new ReportUtil();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("tornaSonuc", testTahribatsizTornas);

			if (raporVardiya == 1) {
				dataMap.put("vardiya", "GÜNDÜZ");
			} else if (raporVardiya == 0) {
				dataMap.put("vardiya", "GECE");
			}

			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/imalat/FR-143.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			downloadedFileName = "FR-143"
					+ pipes.get(0).getSalesItem().getProposal().getCustomer()
							.getShortName() + "-" + pipes.get(0).getPipeIndex()
					+ ".xls";

			getRealFilePath("/reportformats/imalat/" + downloadedFileName);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"FR-143 BAŞARIYLA OLUŞTURULDU!", null));

		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void imalatVardiyaReportXlsx() {

		if (raporVardiya == -1) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "VARDİYA SEÇİNİZ!", null));
			return;
		}
		if (raporTarihi == null || raporVardiya == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		tarihBul(raporTarihi);

		try {

			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();

			if (raporVardiya == 1) {
				pipes = pipeManager.getByDateVardiya(raporTarihiAString,
						raporTarihiAString, raporVardiya);
			} else if (raporVardiya == 0) {
				pipes = pipeManager.getByDateVardiya(raporTarihiAString,
						raporTarihiBString, raporVardiya);
			}

			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<String> date = new ArrayList<String>();

			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("pipeSonuc", pipes);

			if (raporVardiya == 1) {
				dataMap.put("vardiya", "GÜNDÜZ");
			} else if (raporVardiya == 0) {
				dataMap.put("vardiya", "GECE");
			}

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/imalat/FR-08.xls", date.toString());

			downloadedFileName = "FR-08" + date.toString() + ".xls";

			getRealFilePath("/reportformats/imalat/" + downloadedFileName);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void plazmaKesimReportXlsx() {

		if (raporVardiya == -1) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "VARDİYA SEÇİNİZ!", null));
			return;
		}
		if (raporTarihi == null || raporVardiya == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		tarihBul(raporTarihi);

		try {

			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();

			if (raporVardiya == 1) {
				pipes = pipeManager.getByDateVardiya(raporTarihiAString,
						raporTarihiAString, raporVardiya);
			} else if (raporVardiya == 0) {
				pipes = pipeManager.getByDateVardiya(raporTarihiAString,
						raporTarihiBString, raporVardiya);
			}

			List<String> date = new ArrayList<String>();

			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			Map dataMap = new HashMap();
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("pipeSonuc", pipes);

			if (raporVardiya == 1) {
				dataMap.put("vardiya", "GÜNDÜZ");
			} else if (raporVardiya == 0) {
				dataMap.put("vardiya", "GECE");
			}

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/imalat/FR-262.xls", date.toString());

			downloadedFileName = "FR-262" + date.toString() + ".xls";

			getRealFilePath("/reportformats/imalat/" + downloadedFileName);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr244ReportXlsx() {

		if (raporVardiya == -1) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "VARDİYA SEÇİNİZ!", null));
			return;
		}
		if (raporTarihi == null || raporVardiya == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		tarihBul(raporTarihi);

		try {

			TestTahribatsizTamirManager tamirManager = new TestTahribatsizTamirManager();
			List<TestTahribatsizTamir> tamirs = new ArrayList<TestTahribatsizTamir>();

			if (raporVardiya == 1) {
				tamirs = tamirManager.getByDateVardiya(raporTarihiAString,
						raporTarihiAString, raporVardiya);
			} else if (raporVardiya == 0) {
				tamirs = tamirManager.getByDateVardiya(raporTarihiAString,
						raporTarihiBString, raporVardiya);
			}

			List<String> date = new ArrayList<String>();

			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			Map dataMap = new HashMap();
			dataMap.put("date", raporTarihiAString);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("tamirSonuc", tamirs);

			if (raporVardiya == 1) {
				dataMap.put("vardiya", "GÜNDÜZ");
			} else if (raporVardiya == 0) {
				dataMap.put("vardiya", "GECE");
			}

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/imalat/FR-244.xls", date.toString());

			downloadedFileName = "FR-244" + date.toString() + ".xls";

			getRealFilePath("/reportformats/imalat/" + downloadedFileName);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr174ReportXlsx() {

		if (raporTarihi == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		tarihBulAy(raporTarihi);

		try {

			TestTahribatsizTamirManager tamirManager = new TestTahribatsizTamirManager();
			List<TestTahribatsizTamir> tamirs = new ArrayList<TestTahribatsizTamir>();

			tamirs = tamirManager.getByDate(raporTarihiAString,
					raporTarihiBString);

			List<String> date = new ArrayList<String>();

			date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

			Map dataMap = new HashMap();
			dataMap.put("dateA", raporTarihiAString);
			dataMap.put("dateB", raporTarihiBString);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("tamirSonuc", tamirs);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/imalat/FR-174.xls", date.toString());

			downloadedFileName = "FR-174" + date.toString() + ".xls";

			getRealFilePath("/reportformats/imalat/" + downloadedFileName);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr199ReportXlsx(Integer prmPipeId) {

		if (prmPipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		tarihBul(raporTarihi);

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			TestTahribatsizOtomatikUtSonucManager otoManager = new TestTahribatsizOtomatikUtSonucManager();
			List<TestTahribatsizOtomatikUtSonuc> testTahribatsizOtomatikUtSonucs = new ArrayList<TestTahribatsizOtomatikUtSonuc>();

			TestTahribatsizManuelUtSonucManager manuelManager = new TestTahribatsizManuelUtSonucManager();
			List<TestTahribatsizManuelUtSonuc> testTahribatsizManuelUtSonucs = new ArrayList<TestTahribatsizManuelUtSonuc>();

			TestTahribatsizTamirManager tamirManager = new TestTahribatsizTamirManager();
			List<TestTahribatsizTamir> testTahribatsizTamirs = new ArrayList<TestTahribatsizTamir>();

			TestTahribatsizFloroskopikSonucManager floroManager = new TestTahribatsizFloroskopikSonucManager();
			List<TestTahribatsizFloroskopikSonuc> testTahribatsizFloroskopikSonucs = new ArrayList<TestTahribatsizFloroskopikSonuc>();

			TestTahribatsizHidrostatikManager hidroManager = new TestTahribatsizHidrostatikManager();
			List<TestTahribatsizHidrostatik> testTahribatsizHidrostatiks = new ArrayList<TestTahribatsizHidrostatik>();

			TestTahribatsizGozOlcuSonucManager gozManager = new TestTahribatsizGozOlcuSonucManager();
			List<TestTahribatsizGozOlcuSonuc> testTahribatsizGozOlcuSonucs = new ArrayList<TestTahribatsizGozOlcuSonuc>();

			TestTahribatsizRadyografikSonucManager radyoManager = new TestTahribatsizRadyografikSonucManager();
			List<TestTahribatsizRadyografikSonuc> testTahribatsizRadyografikSonucs = new ArrayList<TestTahribatsizRadyografikSonuc>();

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			TestTahribatsizTornaManager testTahribatsizTornaManager = new TestTahribatsizTornaManager();
			List<TestTahribatsizTorna> testTahribatsizTornas = new ArrayList<TestTahribatsizTorna>();

			if (raporVardiya == 1) {
				testTahribatsizTornas = testTahribatsizTornaManager
						.getByPipeDateVardiya(pipes.get(0).getPipeId(),
								raporTarihiAString, raporTarihiAString,
								raporVardiya);
			} else if (raporVardiya == 0) {
				testTahribatsizTornas = testTahribatsizTornaManager
						.getByPipeDateVardiya(pipes.get(0).getPipeId(),
								raporTarihiAString, raporTarihiBString,
								raporVardiya);
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy")
					.format(testTahribatsizTornas.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			muayeneyiYapan = testTahribatsizTornas.get(0).getUser()
					.getEmployee().getFirstname()
					+ " "
					+ testTahribatsizTornas.get(0).getUser().getEmployee()
							.getLastname();
			hazirlayan = userBean.getUserFullName();

			ReportUtil reportUtil = new ReportUtil();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("tornaSonuc", testTahribatsizTornas);

			if (raporVardiya == 1) {
				dataMap.put("vardiya", "GÜNDÜZ");
			} else if (raporVardiya == 0) {
				dataMap.put("vardiya", "GECE");
			}

			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/imalat/FR-199.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			downloadedFileName = "FR-199"
					+ pipes.get(0).getSalesItem().getProposal().getCustomer()
							.getShortName() + "-" + pipes.get(0).getPipeIndex()
					+ ".xls";

			getRealFilePath("/reportformats/imalat/" + downloadedFileName);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"FR-199 BAŞARIYLA OLUŞTURULDU!", null));

		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	private void tarihBulAy(Date prmTestDate) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(prmTestDate);

		calendar.add(Calendar.MONTH, 1);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.add(Calendar.DATE, -1);

		Date lastDayOfMonth = calendar.getTime();

		DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		System.out.println("Today            : " + sdf.format(prmTestDate));
		System.out.println("Last Day of Month: " + sdf.format(lastDayOfMonth));

		raporTarihiAString = Integer.toString(raporTarihi.getYear() + 1900)
				+ "/" + Integer.toString(raporTarihi.getMonth() + 1) + "/1";

		raporTarihiBString = sdf.format(lastDayOfMonth);
	}

	@SuppressWarnings("deprecation")
	public void tarihBul(Date prmTestDate) {

		raporTarihiAString = Integer.toString(raporTarihi.getYear() + 1900)
				+ "/" + Integer.toString(raporTarihi.getMonth() + 1) + "/"
				+ Integer.toString(raporTarihi.getDate());
		raporTarihiBString = Integer.toString(raporTarihi.getYear() + 1900)
				+ "/" + Integer.toString(raporTarihi.getMonth() + 1) + "/"
				+ Integer.toString(raporTarihi.getDate() + 1);
	}

	// download için
	@SuppressWarnings("resource")
	public StreamedContent getDownloadedFile() {

		try {
			if (downloadedFileName != null) {
				RandomAccessFile accessFile = new RandomAccessFile(
						realFilePath, "r");
				byte[] downloadByte = new byte[(int) accessFile.length()];
				accessFile.read(downloadByte);
				InputStream stream = new ByteArrayInputStream(downloadByte);

				String suffix = FilenameUtils.getExtension(downloadedFileName);
				String contentType = "text/plain";
				if (suffix.contentEquals("jpg") || suffix.contentEquals("png")
						|| suffix.contentEquals("gif")) {
					contentType = "image/" + contentType;
				} else if (suffix.contentEquals("doc")
						|| suffix.contentEquals("docx")) {
					contentType = "application/msword";
				} else if (suffix.contentEquals("xls")
						|| suffix.contentEquals("xlsx")) {
					contentType = "application/vnd.ms-excel";
				} else if (suffix.contentEquals("pdf")) {
					contentType = "application/pdf";
				}
				downloadedFile = new DefaultStreamedContent(stream,
						"contentType", downloadedFileName);

				stream.close();
			} else {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"LÜTFEN RAPOR OLUŞTURUNUZ, RAPOR BULUNAMADI!"));
			}
		} catch (Exception e) {
			System.out
					.println("testNondestructiveReportBean.getDownloadedFile():"
							+ e.toString());
		}
		return downloadedFile;
	}

	private String getRealFilePath(String filePath) {

		realFilePath = FacesContext.getCurrentInstance().getExternalContext()
				.getRealPath(filePath);

		return realFilePath;
	}

	// setters getters

	/**
	 * @return the config
	 */
	public ConfigBean getConfig() {
		return config;
	}

	/**
	 * @param config
	 *            the config to set
	 */
	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the raporVardiya
	 */
	public Integer getRaporVardiya() {
		return raporVardiya;
	}

	/**
	 * @param raporVardiya
	 *            the raporVardiya to set
	 */
	public void setRaporVardiya(Integer raporVardiya) {
		this.raporVardiya = raporVardiya;
	}

	/**
	 * @return the raporTarihi
	 */
	public Date getRaporTarihi() {
		return raporTarihi;
	}

	/**
	 * @param raporTarihi
	 *            the raporTarihi to set
	 */
	public void setRaporTarihi(Date raporTarihi) {
		this.raporTarihi = raporTarihi;
	}

	/**
	 * @return the downloadedFileName
	 */
	public String getDownloadedFileName() {
		return downloadedFileName;
	}

	/**
	 * @param downloadedFileName
	 *            the downloadedFileName to set
	 */
	public void setDownloadedFileName(String downloadedFileName) {
		this.downloadedFileName = downloadedFileName;
	}

	/**
	 * @param downloadedFile
	 *            the downloadedFile to set
	 */
	public void setDownloadedFile(StreamedContent downloadedFile) {
		this.downloadedFile = downloadedFile;
	}

}