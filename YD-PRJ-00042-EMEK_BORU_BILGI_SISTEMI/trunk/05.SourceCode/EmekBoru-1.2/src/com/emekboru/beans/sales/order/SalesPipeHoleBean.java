package com.emekboru.beans.sales.order;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.sales.order.SalesPipeHole;
import com.emekboru.jpaman.sales.order.SalesPipeHoleManager;

@ManagedBean(name = "salesPipeHoleBean")
@ViewScoped
public class SalesPipeHoleBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 761051139525336304L;
	private List<SalesPipeHole> pipeList;

	public SalesPipeHoleBean() {

		SalesPipeHoleManager manager = new SalesPipeHoleManager();
		pipeList = manager.findAll(SalesPipeHole.class);
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public List<SalesPipeHole> getPipeList() {
		return pipeList;
	}

	public void setPipeList(List<SalesPipeHole> pipeList) {
		this.pipeList = pipeList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}