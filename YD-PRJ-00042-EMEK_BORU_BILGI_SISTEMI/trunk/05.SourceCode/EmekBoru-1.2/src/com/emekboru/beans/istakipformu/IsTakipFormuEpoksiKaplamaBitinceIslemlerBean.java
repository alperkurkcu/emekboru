/**
 * 
 */
package com.emekboru.beans.istakipformu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.istakipformu.IsTakipFormuEpoksiKaplamaBitinceIslemler;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isTakipFormuEpoksiKaplamaBitinceIslemlerBean")
@ViewScoped
public class IsTakipFormuEpoksiKaplamaBitinceIslemlerBean implements
		Serializable {

	private static final long serialVersionUID = 1L;

	private List<IsTakipFormuEpoksiKaplamaBitinceIslemler> allIsTakipFormuEpoksiKaplamaBitinceIslemlerList = new ArrayList<IsTakipFormuEpoksiKaplamaBitinceIslemler>();
	private IsTakipFormuEpoksiKaplamaBitinceIslemler isTakipFormuEpoksiKaplamaBitinceIslemlerForm = new IsTakipFormuEpoksiKaplamaBitinceIslemler();

	// setters getters
	public List<IsTakipFormuEpoksiKaplamaBitinceIslemler> getAllIsTakipFormuEpoksiKaplamaBitinceIslemlerList() {
		return allIsTakipFormuEpoksiKaplamaBitinceIslemlerList;
	}

	public void setAllIsTakipFormuEpoksiKaplamaBitinceIslemlerList(
			List<IsTakipFormuEpoksiKaplamaBitinceIslemler> allIsTakipFormuEpoksiKaplamaBitinceIslemlerList) {
		this.allIsTakipFormuEpoksiKaplamaBitinceIslemlerList = allIsTakipFormuEpoksiKaplamaBitinceIslemlerList;
	}

	public IsTakipFormuEpoksiKaplamaBitinceIslemler getIsTakipFormuEpoksiKaplamaBitinceIslemlerForm() {
		return isTakipFormuEpoksiKaplamaBitinceIslemlerForm;
	}

	public void setIsTakipFormuEpoksiKaplamaBitinceIslemlerForm(
			IsTakipFormuEpoksiKaplamaBitinceIslemler isTakipFormuEpoksiKaplamaBitinceIslemlerForm) {
		this.isTakipFormuEpoksiKaplamaBitinceIslemlerForm = isTakipFormuEpoksiKaplamaBitinceIslemlerForm;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
