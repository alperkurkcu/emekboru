package com.emekboru.beans.tender;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Customer;
import com.emekboru.jpaman.CustomerManager;
import com.emekboru.jpaman.WhereClauseArgs;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean
@ViewScoped
public class TenderCustomers implements Serializable{

	private static final long serialVersionUID = 9009185176901506810L;

	@EJB
	private ConfigBean config;
	
	private List<Customer> governments;
	private List<Customer> contractors;
	
	private Customer selectedGov;
	private Customer selectedCon;
	
	
	public TenderCustomers(){
		governments = new ArrayList<Customer>(1);
		contractors = new ArrayList<Customer>(1);
		selectedCon = new Customer();
		selectedGov = new Customer();
	}
	
	@SuppressWarnings("rawtypes")
	@PostConstruct
	public void load(){
		
		CustomerManager manager = new CustomerManager();
		ArrayList<WhereClauseArgs> conditionList = new ArrayList<WhereClauseArgs>();
		WhereClauseArgs<Boolean> arg1 = new WhereClauseArgs.Builder<Boolean>()
				.setFieldName("isGovernment").setValue(true).build();
		conditionList.add(arg1);
		governments = manager.selectFromWhereQuerie(Customer.class, conditionList);

		conditionList.clear();
		arg1 = new WhereClauseArgs.Builder<Boolean>()
				.setFieldName("isGovernment").setValue(false).build();
		conditionList.add(arg1);
		contractors = manager.selectFromWhereQuerie(Customer.class, conditionList);
	}
	
	public void goToNewTenderPage(){
		
		FacesContextUtils.redirect(config.getPageUrl().NEW_TENDER_PAGE);
	}
	
	public void goToManageTenderPage(){
		
		FacesContextUtils.redirect(config.getPageUrl().MANAGER_TENDER_PAGE);
	}
	
	/**
	 * 	GETTERS AND SETTERS
	 */
	public List<Customer> getGovernments() {
		return governments;
	}

	public void setGovernments(List<Customer> governments) {
		this.governments = governments;
	}

	public List<Customer> getContractors() {
		return contractors;
	}

	public void setContractors(List<Customer> contractors) {
		this.contractors = contractors;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Customer getSelectedGov() {
		return selectedGov;
	}

	public void setSelectedGov(Customer selectedGov) {
		this.selectedGov = selectedGov;
	}

	public Customer getSelectedCon() {
		return selectedCon;
	}

	public void setSelectedCon(Customer selectedCon) {
		this.selectedCon = selectedCon;
	}
	
}
