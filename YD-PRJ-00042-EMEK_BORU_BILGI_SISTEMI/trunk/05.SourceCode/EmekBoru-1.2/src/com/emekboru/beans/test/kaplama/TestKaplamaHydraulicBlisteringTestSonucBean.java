/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaHydraulicBlisteringTestSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaHydraulicBlisteringTestSonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testKaplamaHydraulicBlisteringTestSonucBean")
@ViewScoped
public class TestKaplamaHydraulicBlisteringTestSonucBean {

	private TestKaplamaHydraulicBlisteringTestSonuc testKaplamaHydraulicBlisteringTestSonucForm = new TestKaplamaHydraulicBlisteringTestSonuc();
	private List<TestKaplamaHydraulicBlisteringTestSonuc> allTestKaplamaHydraulicBlisteringTestSonucList = new ArrayList<TestKaplamaHydraulicBlisteringTestSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateTestKaplamaHydraulicBlisteringTestSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaHydraulicBlisteringTestSonucManager tkbebManager = new TestKaplamaHydraulicBlisteringTestSonucManager();

		if (testKaplamaHydraulicBlisteringTestSonucForm.getId() == null) {

			testKaplamaHydraulicBlisteringTestSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaHydraulicBlisteringTestSonucForm
					.setEkleyenKullanici(userBean.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaHydraulicBlisteringTestSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaHydraulicBlisteringTestSonucForm
					.setBagliTestId(bagliTest);
			testKaplamaHydraulicBlisteringTestSonucForm
					.setBagliGlobalId(bagliTest);

			tkbebManager.enterNew(testKaplamaHydraulicBlisteringTestSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			testKaplamaHydraulicBlisteringTestSonucForm = new TestKaplamaHydraulicBlisteringTestSonuc();
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaHydraulicBlisteringTestSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaHydraulicBlisteringTestSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			tkbebManager
					.updateEntity(testKaplamaHydraulicBlisteringTestSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addTestKaplamaHydraulicBlisteringTestSonuc() {

		testKaplamaHydraulicBlisteringTestSonucForm = new TestKaplamaHydraulicBlisteringTestSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaHydraulicBlisteringTestSonucListener(
			SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaHydraulicBlisteringTestSonucList = TestKaplamaHydraulicBlisteringTestSonucManager
				.getAllHydraulicBlisteringTestSonuc(globalId, pipeId);
		testKaplamaHydraulicBlisteringTestSonucForm = new TestKaplamaHydraulicBlisteringTestSonuc();
	}

	public void deleteTestKaplamaHydraulicBlisteringTestSonuc() {

		bagliTestId = testKaplamaHydraulicBlisteringTestSonucForm
				.getBagliGlobalId().getId();

		if (testKaplamaHydraulicBlisteringTestSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		TestKaplamaHydraulicBlisteringTestSonucManager tkbebManager = new TestKaplamaHydraulicBlisteringTestSonucManager();

		tkbebManager.delete(testKaplamaHydraulicBlisteringTestSonucForm);
		testKaplamaHydraulicBlisteringTestSonucForm = new TestKaplamaHydraulicBlisteringTestSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters

	/**
	 * @return the testKaplamaHydraulicBlisteringTestSonucForm
	 */
	public TestKaplamaHydraulicBlisteringTestSonuc getTestKaplamaHydraulicBlisteringTestSonucForm() {
		return testKaplamaHydraulicBlisteringTestSonucForm;
	}

	/**
	 * @param testKaplamaHydraulicBlisteringTestSonucForm
	 *            the testKaplamaHydraulicBlisteringTestSonucForm to set
	 */
	public void setTestKaplamaHydraulicBlisteringTestSonucForm(
			TestKaplamaHydraulicBlisteringTestSonuc testKaplamaHydraulicBlisteringTestSonucForm) {
		this.testKaplamaHydraulicBlisteringTestSonucForm = testKaplamaHydraulicBlisteringTestSonucForm;
	}

	/**
	 * @return the allTestKaplamaHydraulicBlisteringTestSonucList
	 */
	public List<TestKaplamaHydraulicBlisteringTestSonuc> getAllTestKaplamaHydraulicBlisteringTestSonucList() {
		return allTestKaplamaHydraulicBlisteringTestSonucList;
	}

	/**
	 * @param allTestKaplamaHydraulicBlisteringTestSonucList
	 *            the allTestKaplamaHydraulicBlisteringTestSonucList to set
	 */
	public void setAllTestKaplamaHydraulicBlisteringTestSonucList(
			List<TestKaplamaHydraulicBlisteringTestSonuc> allTestKaplamaHydraulicBlisteringTestSonucList) {
		this.allTestKaplamaHydraulicBlisteringTestSonucList = allTestKaplamaHydraulicBlisteringTestSonucList;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

}
