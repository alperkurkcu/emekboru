package com.emekboru.beans.sales.customer;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.emekboru.beans.sales.SalesMenuBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Employee;
import com.emekboru.jpa.sales.customer.SalesCRMVisit;
import com.emekboru.jpa.sales.customer.SalesContactPeople;
import com.emekboru.jpa.sales.customer.SalesJoinCRMVisitContactPeople;
import com.emekboru.jpa.sales.customer.SalesJoinCRMVisitEmployee;
import com.emekboru.jpaman.EmployeeManager;
import com.emekboru.jpaman.sales.customer.SalesCRMVisitManager;
import com.emekboru.jpaman.sales.customer.SalesContactPeopleManager;
import com.emekboru.jpaman.sales.customer.SalesJoinCRMVisitContactPeopleManager;
import com.emekboru.jpaman.sales.customer.SalesJoinCRMVisitEmployeeManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "salesCRMVisitBean")
@ViewScoped
public class SalesCRMVisitBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8337319016859979633L;

	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{salesMenuBean}")
	private SalesMenuBean salesMenuBean;

	private SalesCRMVisit newVisit;
	private SalesCRMVisit selectedVisit;
	private SalesCRMVisit reminderVisit;

	private List<SalesCRMVisit> visitList;
	private List<SalesCRMVisit> filteredList;
	private List<SalesCRMVisit> visitsOfCustomer;

	private String downloadedFileName;
	private StreamedContent downloadedFile;

	private File[] fileArray;

	private File theFile = null;
	OutputStream output = null;
	InputStream input = null;

	private String path;
	private String privatePath;
	private String toBeDeleted;

	int length;

	public List<Employee> employeeList;
	private Employee[] selectedEmployees;
	public Integer departmentId;

	private List<SalesContactPeople> newContactPeopleList;
	private List<SalesContactPeople> selectedContactPeopleList;

	public SalesCRMVisitBean() {
		newVisit = new SalesCRMVisit();
		selectedVisit = new SalesCRMVisit();
		employeeList = new ArrayList<Employee>();

		privatePath = File.separatorChar + "sales" + File.separatorChar
				+ "uploadedDocuments" + File.separatorChar + "visit"
				+ File.separatorChar;
	}

	@PostConstruct
	public void load() {
		System.out.println("SalesCRMVisitBean.load()");

		try {
			this.setPath(config.getConfig().getFolder().getAbsolutePath()
					+ privatePath);

			System.out.println("Path for Upload File (Sales):			" + path);
		} catch (Exception ex) {
			System.out.println("Path for Upload File (Sales):			"
					+ FacesContext.getCurrentInstance().getExternalContext()
							.getRealPath(privatePath));

			System.out.print(path);
			System.out.println("CAUSE OF " + ex.toString());
		}

		theFile = new File(path);
		if (!theFile.isDirectory()) {
			theFile.mkdirs();
		}

		fileArray = theFile.listFiles();

		try {
			SalesCRMVisitManager manager = new SalesCRMVisitManager();
			visitList = manager.findAll(SalesCRMVisit.class);
			/* JOIN FUNCTIONS */
			for (SalesCRMVisit visit : visitList) {
				this.loadJoinCRMVisitEmployee(visit);
				this.loadJoinCRMVisitContactPeople(visit);
			}
			/* end of JOIN FUNCTIONS */
			selectedVisit = visitList.get(0);
		} catch (Exception e) {
			System.out.println("SalesCRMVisitBean:" + e.toString());
		}
	}

	public void add() {
		System.out.println("SalesCRMVisitBean.add()");

		try {

			for (int i = 0; i < selectedEmployees.length; i++) {
				newVisit.getParticipantList().add(selectedEmployees[i]);
			}

			SalesCRMVisitManager manager = new SalesCRMVisitManager();
			manager.enterNew(newVisit);
			/* JOIN FUNCTIONS */
			this.addJoinCRMVisitEmployee(newVisit);
			this.addJoinCRMVisitContactPeople(newVisit);
			/* end of JOIN FUNCTIONS */
			visitList.add(newVisit);
			newVisit = new SalesCRMVisit();
			FacesContextUtils.addInfoMessage("SubmitMessage");
		} catch (Exception e) {
			System.out.println("SalesCRMVisitBean:" + e.toString());
		}
	}

	public void update() {
		System.out.println("SalesCRMVisitBean.update()");

		try {
			SalesCRMVisitManager manager = new SalesCRMVisitManager();

			/* JOIN FUNCTIONS */
			this.deleteJoinCRMVisitEmployee(selectedVisit);
			this.addJoinCRMVisitEmployee(selectedVisit);
			this.deleteJoinCRMVisitContactPeople(selectedVisit);
			this.addJoinCRMVisitContactPeople(selectedVisit);
			/* end of JOIN FUNCTIONS */
			manager.updateEntity(selectedVisit);

			FacesContextUtils.addInfoMessage("UpdateItemMessage");
		} catch (Exception e) {
			System.out.println("SalesCRMVisitBean:" + e.toString());
		}
	}

	public void delete() {
		System.out.println("SalesCRMVisitBean.delete()");

		boolean isDeleted = true;
		try {

			if (selectedVisit.getFileNumber() > 0) {
				isDeleted = false;

				// Delete the uploaded file
				theFile = new File(path);
				fileArray = theFile.listFiles();

				for (int i = 0; i < selectedVisit.getFileNames().size(); i++) {
					System.out
							.println("document that will be deleted: "
									+ privatePath
									+ selectedVisit.getFileNames().get(i));
					for (int j = 0; j < fileArray.length; j++) {
						System.out.println("fileArray[j].toString(): "
								+ fileArray[j].toString());
						if (fileArray[j].toString().contains(
								privatePath
										+ selectedVisit.getFileNames().get(i))) {
							isDeleted = fileArray[j].delete();
						}
					}
				}
			}

			if (isDeleted) {
				/* JOIN FUNCTIONS */
				this.deleteJoinCRMVisitEmployee(selectedVisit);
				this.deleteJoinCRMVisitContactPeople(selectedVisit);
				/* end of JOIN FUNCTIONS */
				SalesCRMVisitManager manager = new SalesCRMVisitManager();
				manager.delete(selectedVisit);

				visitList.remove(selectedVisit);
				selectedVisit = visitList.get(0);
				FacesContextUtils.addWarnMessage("DeletedMessage");
			}

		} catch (Exception e) {
			System.out.println("SalesCRMVisitBean:" + e.toString());
		}
	}

	/* JOIN FUNCTIONS */
	private void addJoinCRMVisitEmployee(SalesCRMVisit e) {
		SalesJoinCRMVisitEmployeeManager joinManager = new SalesJoinCRMVisitEmployeeManager();
		SalesJoinCRMVisitEmployee newJoin;

		for (int i = 0; i < e.getParticipantList().size(); i++) {
			newJoin = new SalesJoinCRMVisitEmployee();
			newJoin.setEmployee(e.getParticipantList().get(i));
			newJoin.setVisit(e);
			joinManager.enterNew(newJoin);
		}
	}

	private void loadJoinCRMVisitEmployee(SalesCRMVisit visit) {
		SalesJoinCRMVisitEmployeeManager joinManager = new SalesJoinCRMVisitEmployeeManager();
		List<SalesJoinCRMVisitEmployee> joinList = joinManager
				.findAll(SalesJoinCRMVisitEmployee.class);

		visit.setParticipantList(new ArrayList<Employee>());

		for (int i = 0; i < joinList.size(); i++) {
			if (joinList.get(i).getVisit().equals(visit)) {
				visit.getParticipantList().add(joinList.get(i).getEmployee());
			}
		}
	}

	private void deleteJoinCRMVisitEmployee(SalesCRMVisit visit) {
		SalesJoinCRMVisitEmployeeManager joinManager = new SalesJoinCRMVisitEmployeeManager();
		List<SalesJoinCRMVisitEmployee> joinList = joinManager
				.findAll(SalesJoinCRMVisitEmployee.class);

		for (int i = 0; i < joinList.size(); i++) {
			if (joinList.get(i).getVisit().equals(visit)) {
				joinManager.delete(joinList.get(i));
			}
		}
	}

	private void addJoinCRMVisitContactPeople(SalesCRMVisit e) {
		SalesJoinCRMVisitContactPeopleManager joinManager = new SalesJoinCRMVisitContactPeopleManager();
		SalesJoinCRMVisitContactPeople newJoin;

		for (int i = 0; i < e.getContactPeopleList().size(); i++) {
			newJoin = new SalesJoinCRMVisitContactPeople();
			newJoin.setContactPeople(e.getContactPeopleList().get(i));
			newJoin.setVisit(e);
			joinManager.enterNew(newJoin);
		}
	}

	private void loadJoinCRMVisitContactPeople(SalesCRMVisit visit) {
		SalesJoinCRMVisitContactPeopleManager joinManager = new SalesJoinCRMVisitContactPeopleManager();
		List<SalesJoinCRMVisitContactPeople> joinList = joinManager
				.findAll(SalesJoinCRMVisitContactPeople.class);

		visit.setContactPeopleList(new ArrayList<SalesContactPeople>());

		for (int i = 0; i < joinList.size(); i++) {
			if (joinList.get(i).getVisit().equals(visit)) {
				visit.getContactPeopleList().add(
						joinList.get(i).getContactPeople());
			}
		}
	}

	private void deleteJoinCRMVisitContactPeople(SalesCRMVisit visit) {
		SalesJoinCRMVisitContactPeopleManager joinManager = new SalesJoinCRMVisitContactPeopleManager();
		List<SalesJoinCRMVisitContactPeople> joinList = joinManager
				.findAll(SalesJoinCRMVisitContactPeople.class);

		for (int i = 0; i < joinList.size(); i++) {
			if (joinList.get(i).getVisit().equals(visit)) {
				joinManager.delete(joinList.get(i));
			}
		}
	}

	/* end of JOIN FUNCTIONS */

	public String checkFileName(String fileName) {

		fileName = fileName.replace(" ", "_");
		fileName = fileName.replace("ç", "c");
		fileName = fileName.replace("Ç", "C");
		fileName = fileName.replace("ð", "g");
		fileName = fileName.replace("Ð", "G");
		fileName = fileName.replace("Ý", "i");
		fileName = fileName.replace("ý", "i");
		fileName = fileName.replace("ö", "o");
		fileName = fileName.replace("Ö", "O");
		fileName = fileName.replace("þ", "s");
		fileName = fileName.replace("Þ", "S");
		fileName = fileName.replace("ü", "u");
		fileName = fileName.replace("Ü", "U");

		return fileName;
	}

	public void upload(FileUploadEvent event) throws AbortProcessingException,
			IOException {
		System.out.println("SalesCRMVisitBean.upload()");

		try {
			String name = this.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				newVisit.setDocuments("//" + name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				SalesCRMVisitManager manager = new SalesCRMVisitManager();
				manager.updateEntity(selectedVisit);

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				FacesContextUtils.addWarnMessage("salesFileUploadedMessage");
			} else {
				System.out.println("File already exists.");

				newVisit.setDocuments("//");
				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("SalesCRMVisitBean:" + e.toString());
		}
	}

	public void addUploadedFile(FileUploadEvent event)
			throws AbortProcessingException, IOException {
		System.out.println("SalesCRMVisitBean.addUploadedFile()");

		try {
			String name = this.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				selectedVisit.setDocuments(selectedVisit.getDocuments() + "//"
						+ name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				SalesCRMVisitManager manager = new SalesCRMVisitManager();
				manager.updateEntity(selectedVisit);

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				FacesContextUtils.addWarnMessage("salesFileUploadedMessage");
			} else {
				System.out.println("File already exists.");

				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("SalesCRMVisitBean:" + e.toString());
		}
	}

	public void deleteTheSelectedFile() {
		System.out.println("SalesCRMVisitBean.deleteTheSelectedFile()");
		try {
			// Delete the uploaded file
			theFile = new File(path);
			fileArray = theFile.listFiles();

			System.out.println("document that will be deleted: " + privatePath
					+ toBeDeleted);
			for (int j = 0; j < fileArray.length; j++) {
				System.out.println("fileArray[j].toString(): "
						+ fileArray[j].toString());
				if (fileArray[j].toString().contains(privatePath + toBeDeleted)) {
					fileArray[j].delete();

					if (selectedVisit.getDocuments().contentEquals(
							"//" + toBeDeleted)) {
						selectedVisit.setDocuments(selectedVisit.getDocuments()
								.replace("//" + toBeDeleted, "null"));
					} else {
						selectedVisit.setDocuments(selectedVisit.getDocuments()
								.replace("//" + toBeDeleted, ""));
					}

					FacesContextUtils
							.addWarnMessage("salesTheFileIsDeletedMessage");
				}
			}
		} catch (Exception e) {
			System.out.println("SalesCRMVisitBean:" + e.toString());
		}
	}

	public void deleteAndUpload(FileUploadEvent event)
			throws AbortProcessingException, IOException {

		System.out.println("SalesCRMVisitBean.deleteAndUpload()");

		try {
			// Delete the uploaded file
			theFile = new File(path);
			fileArray = theFile.listFiles();

			for (int i = 0; i < selectedVisit.getFileNames().size(); i++) {
				for (int j = 0; j < fileArray.length; j++) {
					System.out.println("fileArray[j].toString(): "
							+ fileArray[j].toString());
					if (fileArray[j].toString().contains(
							privatePath + selectedVisit.getFileNames().get(i))) {
						fileArray[j].delete();
					}
				}
			}

			String name = this.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				selectedVisit.setDocuments("//" + name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				SalesCRMVisitManager manager = new SalesCRMVisitManager();
				manager.updateEntity(selectedVisit);

				FacesContextUtils.addInfoMessage("UpdateItemMessage");
			} else {
				System.out.println("File already exists.");

				selectedVisit.setDocuments(null);
				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("SalesCRMVisitBean:" + e.toString());
		}
	}

	public void selectNothing() {
		reminderVisit = null;
	}

	public void loadEmployeeListForNewVisit() {
		System.out.println("SalesCRMVisitBean.loadEmployeeListForNewEvent()");

		try {
			EmployeeManager employeeManager = new EmployeeManager();
			employeeList = employeeManager.findByField(Employee.class,
					"department.departmentId", departmentId);
		} catch (Exception e) {
			System.out.println("SalesCRMVisitBean:" + e.toString());
		}
	}

	public void loadContactListForNewVisit() {
		System.out.println("SalesCRMVisitBean.loadContactListForNewVisit()");

		try {
			SalesContactPeopleManager peopleManager = new SalesContactPeopleManager();
			newContactPeopleList = peopleManager.findByField(
					SalesContactPeople.class, "salesCustomer",
					newVisit.getSalesCustomer());
		} catch (Exception e) {
			System.out.println("SalesCRMVisitBean:" + e.toString());
		}
	}

	public void loadContactListForSelectedVisit() {
		System.out
				.println("SalesCRMVisitBean.loadContactListForSelectedVisit()");

		try {
			SalesContactPeopleManager peopleManager = new SalesContactPeopleManager();
			selectedContactPeopleList = peopleManager.findByField(
					SalesContactPeople.class, "salesCustomer",
					selectedVisit.getSalesCustomer());
		} catch (Exception e) {
			System.out.println("SalesCRMVisitBean:" + e.toString());
		}
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public SalesCRMVisit getNewVisit() {
		return newVisit;
	}

	public void setNewVisit(SalesCRMVisit newVisit) {
		this.newVisit = newVisit;
	}

	public SalesCRMVisit getSelectedVisit() {
		try {
			if (selectedVisit == null)
				selectedVisit = new SalesCRMVisit();
		} catch (Exception ex) {
			selectedVisit = new SalesCRMVisit();
		}
		return selectedVisit;
	}

	public void setSelectedVisit(SalesCRMVisit selectedVisit) {
		this.selectedVisit = selectedVisit;
	}

	public SalesCRMVisit getReminderVisit() {
		return reminderVisit;
	}

	public void setReminderVisit(SalesCRMVisit reminderVisit) {
		this.reminderVisit = reminderVisit;
	}

	public List<SalesCRMVisit> getVisitList() {
		return visitList;
	}

	public void setVisitList(List<SalesCRMVisit> visitList) {
		this.visitList = visitList;
	}

	public List<SalesCRMVisit> getFilteredList() {
		try {
			selectedVisit = filteredList.get(0);
			salesMenuBean.showNewType1Menu();
		} catch (Exception ex) {
			System.out.println("getFilteredList: " + ex);
		}
		return filteredList;
	}

	public void setFilteredList(List<SalesCRMVisit> filteredList) {
		this.filteredList = filteredList;
	}

	public SalesMenuBean getSalesMenuBean() {
		return salesMenuBean;
	}

	public void setSalesMenuBean(SalesMenuBean salesMenuBean) {
		this.salesMenuBean = salesMenuBean;
	}

	public List<SalesCRMVisit> getVisitsOfCustomer() {
		try {
			visitsOfCustomer = new ArrayList<SalesCRMVisit>();

			for (int i = 0; i < visitList.size(); i++) {
				visitsOfCustomer.add(visitList.get(i));
			}
		} catch (Exception e) {
			System.out.println("SalesCRMVisitBean:" + e.toString());
		}
		return visitsOfCustomer;
	}

	public void setVisitsOfCustomer(List<SalesCRMVisit> visitsOfCustomer) {
		this.visitsOfCustomer = visitsOfCustomer;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getDownloadedFileName() {
		return downloadedFileName;
	}

	public void setDownloadedFileName(String downloadedFileName) {
		this.downloadedFileName = downloadedFileName;
	}

	@SuppressWarnings("resource")
	public StreamedContent getDownloadedFile() {
		System.out.println("SalesCRMVisitBean.getDownloadedFile()");

		try {
			if (downloadedFileName != null) {
				RandomAccessFile accessFile = new RandomAccessFile(path
						+ downloadedFileName, "r");
				byte[] downloadByte = new byte[(int) accessFile.length()];
				accessFile.read(downloadByte);
				InputStream stream = new ByteArrayInputStream(downloadByte);

				String suffix = FilenameUtils.getExtension(downloadedFileName);
				String contentType = "text/plain";
				if (suffix.contentEquals("jpg") || suffix.contentEquals("png")
						|| suffix.contentEquals("gif")) {
					contentType = "image/" + contentType;
				} else if (suffix.contentEquals("doc")
						|| suffix.contentEquals("docx")) {
					contentType = "application/msword";
				} else if (suffix.contentEquals("xls")
						|| suffix.contentEquals("xlsx")) {
					contentType = "application/vnd.ms-excel";
				} else if (suffix.contentEquals("pdf")) {
					contentType = "application/pdf";
				}
				downloadedFile = new DefaultStreamedContent(stream,
						"contentType", downloadedFileName);
				stream.close();
			}
		} catch (Exception e) {
			System.out.println("SalesCRMVisitBean:" + e.toString());
		}
		return downloadedFile;
	}

	public String getToBeDeleted() {
		return toBeDeleted;
	}

	public void setToBeDeleted(String toBeDeleted) {
		this.toBeDeleted = toBeDeleted;
	}

	/**
	 * @return the employeeList
	 */
	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	/**
	 * @param employeeList
	 *            the employeeList to set
	 */
	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}

	/**
	 * @return the departmentId
	 */
	public Integer getDepartmentId() {
		return departmentId;
	}

	/**
	 * @param departmentId
	 *            the departmentId to set
	 */
	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * @return the newContactPeopleList
	 */
	public List<SalesContactPeople> getNewContactPeopleList() {
		return newContactPeopleList;
	}

	/**
	 * @param newContactPeopleList
	 *            the newContactPeopleList to set
	 */
	public void setNewContactPeopleList(
			List<SalesContactPeople> newContactPeopleList) {
		this.newContactPeopleList = newContactPeopleList;
	}

	/**
	 * @return the selectedContactPeopleList
	 */
	public List<SalesContactPeople> getSelectedContactPeopleList() {
		return selectedContactPeopleList;
	}

	/**
	 * @param selectedContactPeopleList
	 *            the selectedContactPeopleList to set
	 */
	public void setSelectedContactPeopleList(
			List<SalesContactPeople> selectedContactPeopleList) {
		this.selectedContactPeopleList = selectedContactPeopleList;
	}

	/**
	 * @return the selectedEmployees
	 */
	public Employee[] getSelectedEmployees() {
		return selectedEmployees;
	}

	/**
	 * @param selectedEmployees
	 *            the selectedEmployees to set
	 */
	public void setSelectedEmployees(Employee[] selectedEmployees) {
		this.selectedEmployees = selectedEmployees;
	}
}
