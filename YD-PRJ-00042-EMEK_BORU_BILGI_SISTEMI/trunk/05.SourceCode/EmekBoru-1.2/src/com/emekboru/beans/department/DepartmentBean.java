package com.emekboru.beans.department;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;

import com.emekboru.jpa.Department;
import com.emekboru.jpa.Employee;
import com.emekboru.jpaman.DepartmentManager;
import com.emekboru.jpaman.Factory;
import com.emekboru.utils.FacesContextUtils;


@ManagedBean(name = "departmentBean")
@ViewScoped
public class DepartmentBean implements Serializable{

	private static final long serialVersionUID = 1L;
	private Department currentDepartment;
	private Department newDepartment;
	private Employee selectedEmployee;

	
	public DepartmentBean(){
		
		currentDepartment = new Department();
		newDepartment = new Department();
		selectedEmployee = new Employee();
		
	}

	public void addDepartment(){
		
		System.out.println("DepartmentBean.addDepartment()");
		DepartmentManager departmentManager = new DepartmentManager();
		List<Department> departmentList = (List<Department>) departmentManager.
				            findByField(Department.class, "name", newDepartment.getName());
		if(departmentList.size()>0){
			
			FacesContextUtils.addErrorMessage("ItemExistMessage");
			newDepartment.setName("");
			return;
		}
		departmentManager.enterNew(newDepartment);
		
		EntityManager manager = Factory.getInstance().createEntityManager();
		newDepartment = manager.find(Department.class, newDepartment.getDepartmentId());
		manager.refresh(newDepartment);
		manager.close();
		newDepartment = new Department();
		DepartmentListBean.loadDepartmentList();
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}
	
	public void cancelDepartmentSubmittion(){
		
		newDepartment = new Department();
	}

	public void viewDepartmentDetails(Department department){
		currentDepartment = department;
	}
	
	public void loadDepartmentEmployeeList(Department department){
		
		currentDepartment = department;
		EntityManager manager = Factory.getInstance().createEntityManager();
		currentDepartment = manager.find(Department.class, 
				      currentDepartment.getDepartmentId());
		manager.getTransaction().begin();
		currentDepartment.getEmployees();
		manager.getTransaction().commit();
		manager.close();
		
	}
	
	public void updateDepartment(){
		
		DepartmentManager departmentManager = new DepartmentManager();
		departmentManager.updateEntity(currentDepartment);
		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}
	
	public void deleteDepartment(){
		
		if(currentDepartment == null 
				|| currentDepartment.getDepartmentId() == null){
			
			FacesContextUtils.addErrorMessage("CannotDeleteMessage");
			return;
		}
		
		EntityManager manager = Factory.getInstance().createEntityManager();
		currentDepartment = manager.find(Department.class, 
				      currentDepartment.getDepartmentId());
		manager.getTransaction().begin();
		
		if(currentDepartment.getEmployees().size() > 0){
			
			FacesContextUtils.addErrorMessage("EmployeeRelatedMessage");
			return;
		}
		
		manager.remove(currentDepartment);
		manager.getTransaction().commit();
		manager.close();
		DepartmentListBean.loadDepartmentList();
		currentDepartment = new Department();
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}
	
	/*
	 * 			GETTERS AND SETTERS
	 */
	
	public Department getCurrentDepartment() {
		return currentDepartment;
	}

	public void setCurrentDepartment(Department currentDepartment) {
		this.currentDepartment = currentDepartment;
	}

	public Department getNewDepartment() {
		return newDepartment;
	}

	public void setNewDepartment(Department newDepartment) {
		this.newDepartment = newDepartment;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Employee getSelectedEmployee() {
		return selectedEmployee;
	}

	public void setSelectedEmployee(Employee selectedEmployee) {
		this.selectedEmployee = selectedEmployee;
	}
}
