/**
 * 
 */
package com.emekboru.beans.istakipformu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.istakipformu.IsTakipFormuBetonKaplamaSonraIslemler;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isTakipFormuBetonKaplamaSonraIslemlerBean")
@ViewScoped
public class IsTakipFormuBetonKaplamaSonraIslemlerBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<IsTakipFormuBetonKaplamaSonraIslemler> allIsTakipFormuBetonKaplamaSonraIslemlerList = new ArrayList<IsTakipFormuBetonKaplamaSonraIslemler>();
	private IsTakipFormuBetonKaplamaSonraIslemler isTakipFormuBetonKaplamaSonraIslemlerForm = new IsTakipFormuBetonKaplamaSonraIslemler();

	// setters getters
	public List<IsTakipFormuBetonKaplamaSonraIslemler> getAllIsTakipFormuBetonKaplamaSonraIslemlerList() {
		return allIsTakipFormuBetonKaplamaSonraIslemlerList;
	}

	public void setAllIsTakipFormuBetonKaplamaSonraIslemlerList(
			List<IsTakipFormuBetonKaplamaSonraIslemler> allIsTakipFormuBetonKaplamaSonraIslemlerList) {
		this.allIsTakipFormuBetonKaplamaSonraIslemlerList = allIsTakipFormuBetonKaplamaSonraIslemlerList;
	}

	public IsTakipFormuBetonKaplamaSonraIslemler getIsTakipFormuBetonKaplamaSonraIslemlerForm() {
		return isTakipFormuBetonKaplamaSonraIslemlerForm;
	}

	public void setIsTakipFormuBetonKaplamaSonraIslemlerForm(
			IsTakipFormuBetonKaplamaSonraIslemler isTakipFormuBetonKaplamaSonraIslemlerForm) {
		this.isTakipFormuBetonKaplamaSonraIslemlerForm = isTakipFormuBetonKaplamaSonraIslemlerForm;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
