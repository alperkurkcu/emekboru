/**
 * 
 */
package com.emekboru.beans.satinalma;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.satinalma.SatinalmaSatinalmaKarari;
import com.emekboru.jpa.satinalma.SatinalmaSatinalmaKarariIcerik;
import com.emekboru.jpa.satinalma.SatinalmaSiparisMektubu;
import com.emekboru.jpa.satinalma.SatinalmaSiparisMektubuIcerik;
import com.emekboru.jpa.satinalma.SatinalmaTakipDosyalarIcerik;
import com.emekboru.jpa.satinalma.SatinalmaTeklifIsteme;
import com.emekboru.jpaman.satinalma.SatinalmaSatinalmaKarariIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaSatinalmaKarariManager;
import com.emekboru.jpaman.satinalma.SatinalmaSiparisMektubuIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaSiparisMektubuManager;
import com.emekboru.jpaman.satinalma.SatinalmaTakipDosyalarIcerikManager;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "satinalmaSiparisMektubuBean")
@ViewScoped
public class SatinalmaSiparisMektubuBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<SatinalmaSiparisMektubu> allSatinalmaSiparisMektubuList = new ArrayList<SatinalmaSiparisMektubu>();
	private List<SatinalmaSiparisMektubu> allSatinalmaSiparisMektubuListByKararId = new ArrayList<SatinalmaSiparisMektubu>();
	private List<SatinalmaSiparisMektubuIcerik> allSatinalmaSiparisMektubuIcerikListByKararIdMusteriId = new ArrayList<SatinalmaSiparisMektubuIcerik>();
	private List<SatinalmaSiparisMektubuIcerik> allSatinalmaSiparisMektubuIcerikListByKararId = new ArrayList<SatinalmaSiparisMektubuIcerik>();
	private List<SatinalmaSatinalmaKarariIcerik> satinalmaSatinalmaKarariIcerikList = new ArrayList<SatinalmaSatinalmaKarariIcerik>();

	private List<SatinalmaTeklifIsteme> satinalmaTeklifIstemeList = new ArrayList<SatinalmaTeklifIsteme>();

	private SatinalmaSiparisMektubu satinalmaSiparisMektubuForm = new SatinalmaSiparisMektubu();
	private SatinalmaSiparisMektubuIcerik satinalmaSiparisMektubuIcerik = new SatinalmaSiparisMektubuIcerik();
	private SatinalmaSatinalmaKarari satinalmaSatinalmaKarariForm = new SatinalmaSatinalmaKarari();

	private List<SatinalmaSatinalmaKarari> satinalmaSatinalmaKarariList = new ArrayList<SatinalmaSatinalmaKarari>();

	private List<SatinalmaTakipDosyalarIcerik> dosyaIcerikList = new ArrayList<SatinalmaTakipDosyalarIcerik>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	private Integer tedarikciId = 0;

	@ManagedProperty(value = "#{satinalmaSiparisBildirimiBean}")
	private SatinalmaSiparisBildirimiBean satinalmaSiparisBildirimiBean;

	double toplam = 0.0;

	double toplamKdv = 0.0;

	double toplamTutar = 0.0;

	double kdv = 0.0;

	public void addFromKarar(SatinalmaSatinalmaKarari satinalmaSatinalmaKarari) {

		SatinalmaSiparisMektubuManager manager = new SatinalmaSiparisMektubuManager();

		this.satinalmaSatinalmaKarariForm = satinalmaSatinalmaKarari;

		SatinalmaSatinalmaKarariIcerikManager sskim = new SatinalmaSatinalmaKarariIcerikManager();
		// satinalmaSatinalmaKarariIcerikList = sskim
		// .findKabulEdilenByKararId(satinalmaSatinalmaKarariForm
		// .getId());

		satinalmaTeklifIstemeList = sskim
				.teklifIstemeByKararId(satinalmaSatinalmaKarariForm.getId());
		satinalmaSatinalmaKarariIcerikList = sskim
				.kararIcerikByKararId(satinalmaSatinalmaKarariForm.getId());

		satinalmaSiparisMektubuForm
				.setSatinalmaSatinalmaKarari(satinalmaSatinalmaKarari);
		satinalmaSiparisMektubuForm
				.setEklemeZamani(UtilInsCore.getTarihZaman());
		satinalmaSiparisMektubuForm.setEkleyenKullanici(userBean.getUser()
				.getId());

		// kac tane tedarikçi seçilmişse o kadar sipariş formu acılıyor
		for (int i = 0; i < satinalmaTeklifIstemeList.size(); i++) {
			try {
				satinalmaSiparisMektubuForm.setId(null);
				satinalmaSiparisMektubuForm
						.setSatinalmaMusteri(satinalmaTeklifIstemeList.get(i)
								.getSatinalmaMusteri());
				allSatinalmaSiparisMektubuList.add(satinalmaSiparisMektubuForm);
				manager.enterNew(satinalmaSiparisMektubuForm);

				tedarikciId = satinalmaTeklifIstemeList.get(i)
						.getSatinalmaMusteri().getId();

				satinalmaSiparisMektubuIcerikEkle();
			} catch (Exception e) {
				System.out.println("satinalmaSiparisMektubuBean.addFromKarar: "
						+ e.toString());
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"SİPARİŞ MEKTUBU OLUŞTURULAMADI, HATA!", null));
			}
		}

		satinalmaSiparisMektubuForm = new SatinalmaSiparisMektubu();
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				"SİPARİŞ MEKTUBU BAŞARIYLA OLUŞTURULDU!", null));
	}

	// karar içeriği otomatik olarak teklife ekleniyor
	@SuppressWarnings({ "unused" })
	public void satinalmaSiparisMektubuIcerikEkle() {

		SatinalmaSiparisMektubuIcerikManager smim = new SatinalmaSiparisMektubuIcerikManager();
		SatinalmaSatinalmaKarariManager sskm = new SatinalmaSatinalmaKarariManager();
		// SatinalmaSatinalmaKarariIcerikManager sskim = new
		// SatinalmaSatinalmaKarariIcerikManager();
		//
		// satinalmaSatinalmaKarariIcerikList = sskim
		// .findKabulEdilenByKararId(satinalmaSatinalmaKarariForm.getId());

		satinalmaSiparisMektubuIcerik
				.setSatinalmaSatinalmaKarari(satinalmaSatinalmaKarariForm);
		satinalmaSiparisMektubuIcerik
				.setSatinalmaSiparisMektubu(satinalmaSiparisMektubuForm);
		satinalmaSiparisMektubuIcerik.setEklemeZamani(UtilInsCore
				.getTarihZaman());
		satinalmaSiparisMektubuIcerik.setEkleyenKullanici(userBean.getUser()
				.getId());

		for (int i = 0; i < satinalmaSatinalmaKarariIcerikList.size(); i++) {
			satinalmaSiparisMektubuIcerik.setId(null);
			if (tedarikciId == satinalmaSatinalmaKarariIcerikList.get(i)
					.getSatinalmaTeklifIsteme().getSatinalmaMusteri().getId()) {
				satinalmaSiparisMektubuIcerik
						.setSatinalmaSatinalmaKarariIcerik(satinalmaSatinalmaKarariIcerikList
								.get(i));
				try {
					smim.enterNew(satinalmaSiparisMektubuIcerik);
				} catch (Exception e) {
					FacesContext context = FacesContext.getCurrentInstance();
					context.addMessage(null, new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"SİPARİŞ MEKTUBU İÇERİKLERİ OLUŞTURULAMADI, HATA!",
							null));
				}
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"SİPARİŞ MEKTUBU İÇERİKLERİ BAŞARIYLA OLUŞTURULDU!",
						null));
			}
		}
	}

	public void fillTestList() {

		updateButtonRender = false;
		satinalmaSiparisMektubuForm = new SatinalmaSiparisMektubu();

		SatinalmaSiparisMektubuIcerikManager manager = new SatinalmaSiparisMektubuIcerikManager();
		allSatinalmaSiparisMektubuIcerikListByKararId = manager
				.findByKararId(satinalmaSatinalmaKarariForm.getId());
		SatinalmaSiparisMektubuManager manager1 = new SatinalmaSiparisMektubuManager();
		allSatinalmaSiparisMektubuListByKararId = manager1
				.findByKararId(satinalmaSatinalmaKarariForm.getId());
		SatinalmaTakipDosyalarIcerikManager manager2 = new SatinalmaTakipDosyalarIcerikManager();
		dosyaIcerikList = manager2.findByDosyaIdFormId(
				satinalmaSatinalmaKarariForm.getSatinalmaTakipDosyalar()
						.getId(), satinalmaSatinalmaKarariForm
						.getSatinalmaTakipDosyalar()
						.getSatinalmaMalzemeHizmetTalepFormu().getId());
	}

	public void siparisMektuplariniDuzenle() {

		SatinalmaSiparisMektubuIcerikManager manager = new SatinalmaSiparisMektubuIcerikManager();
		allSatinalmaSiparisMektubuIcerikListByKararIdMusteriId = manager
				.findByKararIdMusteriId(satinalmaSatinalmaKarariForm.getId(),
						satinalmaSiparisMektubuForm.getSatinalmaMusteri()
								.getId());

		toplam(allSatinalmaSiparisMektubuIcerikListByKararIdMusteriId);
	}

	private void toplam(List<SatinalmaSiparisMektubuIcerik> List) {

		toplam = 0.0;
		toplamKdv = 0.0;
		toplamTutar = 0.0;

		for (int i = 0; i < List.size(); i++) {

			toplam = toplam
					+ ((double) List.get(i).getSatinalmaSatinalmaKarariIcerik()
							.getNihaiTeklifBirimFiyat() * (double) List.get(i)
							.getSatinalmaSatinalmaKarariIcerik()
							.getSatinalmaTeklifIstemeIcerik()
							.getSatinalmaTakipDosyalarIcerik()
							.getSatinalmaMalzemeHizmetTalepFormuIcerik()
							.getMiktar());

			kdv = (double) List.get(i).getSatinalmaSatinalmaKarariIcerik()
					.getSatinalmaTeklifIstemeIcerik().getKdv();
		}

		toplamKdv = toplam * (kdv * 0.01);
		toplamTutar = toplam + toplamKdv;

		NumberFormat formatter = new DecimalFormat("#0.0");
		NumberFormat nf = NumberFormat.getInstance();

		SatinalmaSiparisMektubuManager manager = new SatinalmaSiparisMektubuManager();
		try {
			satinalmaSiparisMektubuForm.setToplam(nf.parse(
					formatter.format(toplam)).doubleValue());
			satinalmaSiparisMektubuForm.setToplamKdv(nf.parse(
					formatter.format(toplamKdv)).doubleValue());
			satinalmaSiparisMektubuForm.setToplamTutar(nf.parse(
					formatter.format(toplamTutar)).doubleValue());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		manager.updateEntity(satinalmaSiparisMektubuForm);
	}

	public void siparisBildirimiOlustur() {

		satinalmaSiparisBildirimiBean
				.addFromSiparisMektubu(satinalmaSiparisMektubuForm);

	}

	public void aciklamaOlustur() {
		if (satinalmaSiparisMektubuForm.getAciklama() == null) {
			satinalmaSiparisMektubuForm
					.setAciklama("İŞBU SİPARİŞ MEKTUBU  TARİH VE  SAYILI TEKLİFİNİZE VE  TARİHLİ TELEFON GÖRÜŞMEMİZE İSTİNADEN DÜZENLENMİŞ OLUP,  TARİHİNE KADAR ONAYLANARAK 0312-267 1359 NO.LU FAKSIMIZA GÖNDERİLMESİNİ RİCA EDERİZ.");
		}
	}

	public void addOrUpdateAciklama() {
		SatinalmaSiparisMektubuManager manager = new SatinalmaSiparisMektubuManager();
		satinalmaSiparisMektubuForm.setGuncellemeZamani(UtilInsCore
				.getTarihZaman());
		satinalmaSiparisMektubuForm.setGuncelleyenKullanici(userBean.getUser()
				.getId());
		manager.updateEntity(satinalmaSiparisMektubuForm);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				"AÇIKLAMA BAŞARIYLA GÜNCELLENDİ!", null));
	}

	// setters getters

	/**
	 * @return the allSatinalmaSiparisMektubuList
	 */
	public List<SatinalmaSiparisMektubu> getAllSatinalmaSiparisMektubuList() {
		return allSatinalmaSiparisMektubuList;
	}

	/**
	 * @param allSatinalmaSiparisMektubuList
	 *            the allSatinalmaSiparisMektubuList to set
	 */
	public void setAllSatinalmaSiparisMektubuList(
			List<SatinalmaSiparisMektubu> allSatinalmaSiparisMektubuList) {
		this.allSatinalmaSiparisMektubuList = allSatinalmaSiparisMektubuList;
	}

	/**
	 * @return the satinalmaSiparisMektubuForm
	 */
	public SatinalmaSiparisMektubu getSatinalmaSiparisMektubuForm() {
		return satinalmaSiparisMektubuForm;
	}

	/**
	 * @param satinalmaSiparisMektubuForm
	 *            the satinalmaSiparisMektubuForm to set
	 */
	public void setSatinalmaSiparisMektubuForm(
			SatinalmaSiparisMektubu satinalmaSiparisMektubuForm) {
		this.satinalmaSiparisMektubuForm = satinalmaSiparisMektubuForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the satinalmaSatinalmaKarariForm
	 */
	public SatinalmaSatinalmaKarari getSatinalmaSatinalmaKarariForm() {
		return satinalmaSatinalmaKarariForm;
	}

	/**
	 * @param satinalmaSatinalmaKarariForm
	 *            the satinalmaSatinalmaKarariForm to set
	 */
	public void setSatinalmaSatinalmaKarariForm(
			SatinalmaSatinalmaKarari satinalmaSatinalmaKarariForm) {
		this.satinalmaSatinalmaKarariForm = satinalmaSatinalmaKarariForm;
	}

	/**
	 * @return the allSatinalmaSiparisMektubuIcerikListByKararIdMusteriId
	 */
	public List<SatinalmaSiparisMektubuIcerik> getAllSatinalmaSiparisMektubuIcerikListByKararIdMusteriId() {
		return allSatinalmaSiparisMektubuIcerikListByKararIdMusteriId;
	}

	/**
	 * @param allSatinalmaSiparisMektubuIcerikListByKararIdMusteriId
	 *            the allSatinalmaSiparisMektubuIcerikListByKararIdMusteriId to
	 *            set
	 */
	public void setAllSatinalmaSiparisMektubuIcerikListByKararIdMusteriId(
			List<SatinalmaSiparisMektubuIcerik> allSatinalmaSiparisMektubuIcerikListByKararIdMusteriId) {
		this.allSatinalmaSiparisMektubuIcerikListByKararIdMusteriId = allSatinalmaSiparisMektubuIcerikListByKararIdMusteriId;
	}

	/**
	 * @return the dosyaIcerikList
	 */
	public List<SatinalmaTakipDosyalarIcerik> getDosyaIcerikList() {
		return dosyaIcerikList;
	}

	/**
	 * @param dosyaIcerikList
	 *            the dosyaIcerikList to set
	 */
	public void setDosyaIcerikList(
			List<SatinalmaTakipDosyalarIcerik> dosyaIcerikList) {
		this.dosyaIcerikList = dosyaIcerikList;
	}

	/**
	 * @return the allSatinalmaSiparisMektubuListByKararId
	 */
	public List<SatinalmaSiparisMektubu> getAllSatinalmaSiparisMektubuListByKararId() {
		return allSatinalmaSiparisMektubuListByKararId;
	}

	/**
	 * @param allSatinalmaSiparisMektubuListByKararId
	 *            the allSatinalmaSiparisMektubuListByKararId to set
	 */
	public void setAllSatinalmaSiparisMektubuListByKararId(
			List<SatinalmaSiparisMektubu> allSatinalmaSiparisMektubuListByKararId) {
		this.allSatinalmaSiparisMektubuListByKararId = allSatinalmaSiparisMektubuListByKararId;
	}

	/**
	 * @return the satinalmaSiparisMektubuIcerik
	 */
	public SatinalmaSiparisMektubuIcerik getSatinalmaSiparisMektubuIcerik() {
		return satinalmaSiparisMektubuIcerik;
	}

	/**
	 * @param satinalmaSiparisMektubuIcerik
	 *            the satinalmaSiparisMektubuIcerik to set
	 */
	public void setSatinalmaSiparisMektubuIcerik(
			SatinalmaSiparisMektubuIcerik satinalmaSiparisMektubuIcerik) {
		this.satinalmaSiparisMektubuIcerik = satinalmaSiparisMektubuIcerik;
	}

	/**
	 * @return the satinalmaSiparisBildirimiBean
	 */
	public SatinalmaSiparisBildirimiBean getSatinalmaSiparisBildirimiBean() {
		return satinalmaSiparisBildirimiBean;
	}

	/**
	 * @param satinalmaSiparisBildirimiBean
	 *            the satinalmaSiparisBildirimiBean to set
	 */
	public void setSatinalmaSiparisBildirimiBean(
			SatinalmaSiparisBildirimiBean satinalmaSiparisBildirimiBean) {
		this.satinalmaSiparisBildirimiBean = satinalmaSiparisBildirimiBean;
	}

	/**
	 * @return the toplam
	 */
	public double getToplam() {
		return toplam;
	}

	/**
	 * @param toplam
	 *            the toplam to set
	 */
	public void setToplam(double toplam) {
		this.toplam = toplam;
	}

	/**
	 * @return the toplamKdv
	 */
	public double getToplamKdv() {
		return toplamKdv;
	}

	/**
	 * @param toplamKdv
	 *            the toplamKdv to set
	 */
	public void setToplamKdv(double toplamKdv) {
		this.toplamKdv = toplamKdv;
	}

	/**
	 * @return the toplamTutar
	 */
	public double getToplamTutar() {
		return toplamTutar;
	}

	/**
	 * @param toplamTutar
	 *            the toplamTutar to set
	 */
	public void setToplamTutar(double toplamTutar) {
		this.toplamTutar = toplamTutar;
	}

	/**
	 * @return the kdv
	 */
	public double getKdv() {
		return kdv;
	}

	/**
	 * @param kdv
	 *            the kdv to set
	 */
	public void setKdv(double kdv) {
		this.kdv = kdv;
	}

}
