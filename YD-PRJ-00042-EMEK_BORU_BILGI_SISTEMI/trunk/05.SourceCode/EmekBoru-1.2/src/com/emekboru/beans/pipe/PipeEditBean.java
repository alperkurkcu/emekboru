/**
 * 
 */
package com.emekboru.beans.pipe;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.RowEditEvent;
import org.primefaces.model.LazyDataModel;

import com.emekboru.jpa.Pipe;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.utils.lazymodels.LazyPipeDataModel;

/**
 * @author KRKC
 * 
 */

@ManagedBean(name = "pipeEditBean")
@ViewScoped
public class PipeEditBean implements Serializable {

	private static final long serialVersionUID = 7506162397277463796L;

	// private List<Pipe> pipeList = new ArrayList<Pipe>();

	private String barkodNo = null;

	private LazyDataModel<Pipe> lazyModel;

	private Pipe selectedPipe = new Pipe();

	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();

		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!", null));
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);

			lazyModel = new LazyPipeDataModel(selectedPipe.getSalesItem()
					.getItemId());

		}
	}

	public void onRowEdit(RowEditEvent event) {

		PipeManager manager = new PipeManager();
		manager.updateEntity(((Pipe) event.getObject()));

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				"BİLGİLER DEĞİŞTİRİLMİŞTİR!", null));
	}

	// setters getters

	/**
	 * @return the barkodNo
	 */
	public String getBarkodNo() {
		return barkodNo;
	}

	/**
	 * @param barkodNo
	 *            the barkodNo to set
	 */
	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	/**
	 * @return the selectedPipe
	 */
	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	/**
	 * @param selectedPipe
	 *            the selectedPipe to set
	 */
	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	/**
	 * @return the lazyModel
	 */
	public LazyDataModel<Pipe> getLazyModel() {
		return lazyModel;
	}

	/**
	 * @param lazyModel
	 *            the lazyModel to set
	 */
	public void setLazyModel(LazyDataModel<Pipe> lazyModel) {
		this.lazyModel = lazyModel;
	}

}
