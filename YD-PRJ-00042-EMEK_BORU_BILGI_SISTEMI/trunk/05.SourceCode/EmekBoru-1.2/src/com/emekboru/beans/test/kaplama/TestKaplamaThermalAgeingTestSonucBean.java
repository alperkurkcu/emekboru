/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaThermalAgeingTestSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaThermalAgeingTestSonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testKaplamaThermalAgeingTestSonucBean")
@ViewScoped
public class TestKaplamaThermalAgeingTestSonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private TestKaplamaThermalAgeingTestSonuc testKaplamaThermalAgeingTestSonucForm = new TestKaplamaThermalAgeingTestSonuc();
	private List<TestKaplamaThermalAgeingTestSonuc> allTestKaplamaThermalAgeingTestSonucList = new ArrayList<TestKaplamaThermalAgeingTestSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateTestKaplamaThermalAgeingTestSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaThermalAgeingTestSonucManager tkbebManager = new TestKaplamaThermalAgeingTestSonucManager();

		if (testKaplamaThermalAgeingTestSonucForm.getId() == null) {

			testKaplamaThermalAgeingTestSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaThermalAgeingTestSonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaThermalAgeingTestSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaThermalAgeingTestSonucForm.setBagliTestId(bagliTest);
			testKaplamaThermalAgeingTestSonucForm.setBagliGlobalId(bagliTest);

			tkbebManager.enterNew(testKaplamaThermalAgeingTestSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			testKaplamaThermalAgeingTestSonucForm = new TestKaplamaThermalAgeingTestSonuc();
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaThermalAgeingTestSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaThermalAgeingTestSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			tkbebManager.updateEntity(testKaplamaThermalAgeingTestSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addTestKaplamaThermalAgeingTestSonuc() {

		testKaplamaThermalAgeingTestSonucForm = new TestKaplamaThermalAgeingTestSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaThermalAgeingTestSonucListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaThermalAgeingTestSonucList = TestKaplamaThermalAgeingTestSonucManager
				.getAllThermalAgeingTestSonuc(globalId, pipeId);
		testKaplamaThermalAgeingTestSonucForm = new TestKaplamaThermalAgeingTestSonuc();
	}

	public void deleteTestKaplamaThermalAgeingTestSonuc() {

		bagliTestId = testKaplamaThermalAgeingTestSonucForm.getBagliGlobalId()
				.getId();

		if (testKaplamaThermalAgeingTestSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		TestKaplamaThermalAgeingTestSonucManager tkbebManager = new TestKaplamaThermalAgeingTestSonucManager();

		tkbebManager.delete(testKaplamaThermalAgeingTestSonucForm);
		testKaplamaThermalAgeingTestSonucForm = new TestKaplamaThermalAgeingTestSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters

	/**
	 * @return the testKaplamaThermalAgeingTestSonucForm
	 */
	public TestKaplamaThermalAgeingTestSonuc getTestKaplamaThermalAgeingTestSonucForm() {
		return testKaplamaThermalAgeingTestSonucForm;
	}

	/**
	 * @param testKaplamaThermalAgeingTestSonucForm
	 *            the testKaplamaThermalAgeingTestSonucForm to set
	 */
	public void setTestKaplamaThermalAgeingTestSonucForm(
			TestKaplamaThermalAgeingTestSonuc testKaplamaThermalAgeingTestSonucForm) {
		this.testKaplamaThermalAgeingTestSonucForm = testKaplamaThermalAgeingTestSonucForm;
	}

	/**
	 * @return the allTestKaplamaThermalAgeingTestSonucList
	 */
	public List<TestKaplamaThermalAgeingTestSonuc> getAllTestKaplamaThermalAgeingTestSonucList() {
		return allTestKaplamaThermalAgeingTestSonucList;
	}

	/**
	 * @param allTestKaplamaThermalAgeingTestSonucList
	 *            the allTestKaplamaThermalAgeingTestSonucList to set
	 */
	public void setAllTestKaplamaThermalAgeingTestSonucList(
			List<TestKaplamaThermalAgeingTestSonuc> allTestKaplamaThermalAgeingTestSonucList) {
		this.allTestKaplamaThermalAgeingTestSonucList = allTestKaplamaThermalAgeingTestSonucList;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
