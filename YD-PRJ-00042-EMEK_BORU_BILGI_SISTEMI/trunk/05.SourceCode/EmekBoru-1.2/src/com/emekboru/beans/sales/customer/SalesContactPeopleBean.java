package com.emekboru.beans.sales.customer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.emekboru.beans.sales.SalesMenuBean;
import com.emekboru.jpa.sales.customer.SalesContactPeople;
import com.emekboru.jpaman.sales.customer.SalesContactPeopleManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "salesContactPeopleBean")
@ViewScoped
public class SalesContactPeopleBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 285664059031573725L;

	@ManagedProperty(value = "#{salesMenuBean}")
	private SalesMenuBean salesMenuBean;

	private SalesContactPeople newContact = new SalesContactPeople();
	private SalesContactPeople selectedContact;

	private List<SalesContactPeople> contactList;
	private List<SalesContactPeople> filteredList;
	private List<SalesContactPeople> contactsOfCustomer;

	public SalesContactPeopleBean() {
		newContact = new SalesContactPeople();
		selectedContact = new SalesContactPeople();
	}

	@PostConstruct
	public void load() {
		System.out.println("SalesContactPeopleBean.load()");

		try {
			SalesContactPeopleManager manager = new SalesContactPeopleManager();
			contactList = manager.findAll(SalesContactPeople.class);

			selectedContact = contactList.get(0);
		} catch (Exception e) {
			System.out.println("SalesContactPeopleBean:"+e.toString());
		}
	}

	public void add() {
		System.out.println("SalesContactPeopleBean.add()");

		try {
			SalesContactPeopleManager manager = new SalesContactPeopleManager();

			contactList.add(newContact);
			manager.enterNew(newContact);

			newContact = new SalesContactPeople();
			FacesContextUtils.addInfoMessage("SubmitMessage");
		} catch (Exception e) {
			System.out.println("SalesContactPeopleBean:"+e.toString());
		}
	}

	public void update() {
		System.out.println("SalesContactPeopleBean.update()");

		try {
			SalesContactPeopleManager manager = new SalesContactPeopleManager();
			manager.updateEntity(selectedContact);

			FacesContextUtils.addInfoMessage("UpdateItemMessage");
		} catch (Exception e) {
			System.out.println("SalesContactPeopleBean:"+e.toString());
		}
	}

	public void delete() {
		System.out.println("SalesContactPeopleBean.delete()");

		try {
			SalesContactPeopleManager manager = new SalesContactPeopleManager();
			manager.delete(selectedContact);

			contactList.remove(selectedContact);
			selectedContact = new SalesContactPeople();
			selectedContact = contactList.get(0);
			FacesContextUtils.addWarnMessage("DeletedMessage");
		} catch (Exception e) {
			System.out.println("SalesContactPeopleBean.delete(): "
					+ e.toString());
			FacesContextUtils.addWarnMessage("salesCannotDelete");
		}
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public SalesContactPeople getNewContact() {
		return newContact;
	}

	public void setNewContact(SalesContactPeople newContact) {
		this.newContact = newContact;
	}

	public SalesContactPeople getSelectedContact() {
		try {
			if (selectedContact == null)
				selectedContact = new SalesContactPeople();
		} catch (Exception ex) {
			selectedContact = new SalesContactPeople();
		}
		return selectedContact;
	}

	public void setSelectedContact(SalesContactPeople selectedContact) {
		this.selectedContact = selectedContact;
	}

	public List<SalesContactPeople> getContactList() {
		return contactList;
	}

	public void setContactList(List<SalesContactPeople> contactList) {
		this.contactList = contactList;
	}

	public List<SalesContactPeople> getFilteredList() {
		try {
			selectedContact = filteredList.get(0);
			salesMenuBean.showNewType1Menu();
		} catch (Exception ex) {
			System.out.println("getFilteredList: " + ex);
		}
		return filteredList;
	}

	public void setFilteredList(List<SalesContactPeople> filteredList) {
		this.filteredList = filteredList;
	}

	public SalesMenuBean getSalesMenuBean() {
		return salesMenuBean;
	}

	public void setSalesMenuBean(SalesMenuBean salesMenuBean) {
		this.salesMenuBean = salesMenuBean;
	}

	public List<SalesContactPeople> getContactsOfCustomer() {
		try {
			contactsOfCustomer = new ArrayList<SalesContactPeople>();

			for (int i = 0; i < contactList.size(); i++) {
				contactsOfCustomer.add(contactList.get(i));
			}
		} catch (Exception e) {
			System.out.println("SalesContactPeopleBean:"+e.toString());
		}
		return contactsOfCustomer;
	}

	public void setContactsOfCustomer(
			List<SalesContactPeople> contactsOfCustomer) {
		this.contactsOfCustomer = contactsOfCustomer;
	}
}
