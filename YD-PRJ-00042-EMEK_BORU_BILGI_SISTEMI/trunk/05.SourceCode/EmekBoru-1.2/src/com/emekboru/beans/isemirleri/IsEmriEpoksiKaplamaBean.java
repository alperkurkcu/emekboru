/**
 * 
 */
package com.emekboru.beans.isemirleri;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.emekboru.beans.istakipformu.IsTakipFormuEpoksiKaplamaBean;
import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.isemri.IsEmriEpoksiKaplama;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.CoatRawMaterialManager;
import com.emekboru.jpaman.isemirleri.IsEmriEpoksiKaplamaManager;
import com.emekboru.jpaman.sales.order.SalesItemManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isEmriEpoksiKaplamaBean")
@ViewScoped
public class IsEmriEpoksiKaplamaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private IsEmriEpoksiKaplama isEmriEpoksiKaplamaForm = new IsEmriEpoksiKaplama();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private List<CoatRawMaterial> allEpoksiKaplamaMalzemeList = new ArrayList<CoatRawMaterial>();

	@ManagedProperty(value = "#{isTakipFormuEpoksiKaplamaBean}")
	private IsTakipFormuEpoksiKaplamaBean isTakipFormuEpoksiKaplamaBean;

	public IsEmriEpoksiKaplamaBean() {

	}

	public void addOrUpdateIsEmriEpoksiKaplama(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmItemId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		SalesItem salesItem = new SalesItemManager().loadObject(
				SalesItem.class, prmItemId);

		IsEmriEpoksiKaplamaManager isMan = new IsEmriEpoksiKaplamaManager();

		if (isEmriEpoksiKaplamaForm.getId() == null) {

			isEmriEpoksiKaplamaForm.setSalesItem(salesItem);
			isEmriEpoksiKaplamaForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			isEmriEpoksiKaplamaForm.setEkleyenEmployee(userBean.getUser());

			isMan.enterNew(isEmriEpoksiKaplamaForm);

			// iç kumlama iş takip formu ekleme
			isTakipFormuEpoksiKaplamaBean.addFromIsEmri(salesItem);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"EPOKSİ KAPLAMA İŞ EMRİ BAŞARIYLA EKLENMİŞTİR!"));
		} else {

			isEmriEpoksiKaplamaForm.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			isEmriEpoksiKaplamaForm.setGuncelleyenEmployee(userBean.getUser());
			isMan.updateEntity(isEmriEpoksiKaplamaForm);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_WARN,
					"EPOKSİ KAPLAMA İŞ EMRİ BAŞARIYLA GÜNCELLENMİŞTİR!", null));
		}
	}

	public void deleteIsEmriEpoksiKaplama() {

		IsEmriEpoksiKaplamaManager isMan = new IsEmriEpoksiKaplamaManager();

		if (isEmriEpoksiKaplamaForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		isMan.delete(isEmriEpoksiKaplamaForm);
		isEmriEpoksiKaplamaForm = new IsEmriEpoksiKaplama();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
				"EPOKSİ KAPLAMA İŞ EMRİ BAŞARIYLA SİLİNMİŞTİR!", null));
	}

	@SuppressWarnings("static-access")
	public void loadIsEmri(Integer itemId) {

		IsEmriEpoksiKaplamaManager isMan = new IsEmriEpoksiKaplamaManager();

		if (isMan.getAllIsEmriEpoksiKaplama(itemId).size() > 0) {
			isEmriEpoksiKaplamaForm = new IsEmriEpoksiKaplamaManager()
					.getAllIsEmriEpoksiKaplama(itemId).get(0);
		} else {
			isEmriEpoksiKaplamaForm = new IsEmriEpoksiKaplama();
		}
	}

	public void isEmriMalzemeEkle() {

		IsEmriEpoksiKaplamaManager isMan = new IsEmriEpoksiKaplamaManager();

		isMan.updateEntity(isEmriEpoksiKaplamaForm);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
				"EPOKSİ KAPLAMA MALZEMESİ BAŞARIYLA EKLENMİŞTİR!", null));
	}

	public void loadEpoksiMalzemeleri() {

		CoatRawMaterialManager materialMan = new CoatRawMaterialManager();
		// 8 boya turudur - coat_material_type tablosundan
		allEpoksiKaplamaMalzemeList = materialMan
				.findUnfinishedCoatMaterialByType(8);
	}

	public IsEmriEpoksiKaplama getIsEmriEpoksiKaplamaForm() {
		return isEmriEpoksiKaplamaForm;
	}

	public void setIsEmriEpoksiKaplamaForm(
			IsEmriEpoksiKaplama isEmriEpoksiKaplamaForm) {
		this.isEmriEpoksiKaplamaForm = isEmriEpoksiKaplamaForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public List<CoatRawMaterial> getAllEpoksiKaplamaMalzemeList() {
		return allEpoksiKaplamaMalzemeList;
	}

	public void setAllEpoksiKaplamaMalzemeList(
			List<CoatRawMaterial> allEpoksiKaplamaMalzemeList) {
		this.allEpoksiKaplamaMalzemeList = allEpoksiKaplamaMalzemeList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public IsTakipFormuEpoksiKaplamaBean getIsTakipFormuEpoksiKaplamaBean() {
		return isTakipFormuEpoksiKaplamaBean;
	}

	public void setIsTakipFormuEpoksiKaplamaBean(
			IsTakipFormuEpoksiKaplamaBean isTakipFormuEpoksiKaplamaBean) {
		this.isTakipFormuEpoksiKaplamaBean = isTakipFormuEpoksiKaplamaBean;
	}

}
