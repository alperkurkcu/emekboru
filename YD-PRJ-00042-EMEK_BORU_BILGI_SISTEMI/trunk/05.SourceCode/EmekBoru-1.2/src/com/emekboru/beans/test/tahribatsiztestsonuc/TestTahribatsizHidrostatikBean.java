/**
 * 
 */
package com.emekboru.beans.test.tahribatsiztestsonuc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.HidrostaticTestsSpec;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizFloroskopikSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizHidrostatik;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizManuelUtSonuc;
import com.emekboru.jpaman.HidrostaticTestsSpecManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizGozOlcuSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizHidrostatikManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */

@ManagedBean(name = "testTahribatsizHidrostatikBean")
@ViewScoped
public class TestTahribatsizHidrostatikBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<TestTahribatsizHidrostatik> allTestTahribatsizHidrostatikList = new ArrayList<TestTahribatsizHidrostatik>();
	private TestTahribatsizHidrostatik testTahribatsizHidrostatikForm = new TestTahribatsizHidrostatik();

	private TestTahribatsizHidrostatik newHidro = new TestTahribatsizHidrostatik();
	private List<TestTahribatsizHidrostatik> hidroList;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	int index = 0;

	private boolean updateButtonRender;

	private Pipe selectedPipe = new Pipe();

	private String barkodNo = null;

	@ManagedProperty(value = "#{testTahribatsizGozOlcuSonucBean}")
	private TestTahribatsizGozOlcuSonucBean gozOlcuBean;

	private boolean kontrol = false;

	private List<HidrostaticTestsSpec> hidroSpecs = new ArrayList<HidrostaticTestsSpec>();

	public TestTahribatsizHidrostatikBean() {

	}

	public void addFromManuel(
			List<TestTahribatsizManuelUtSonuc> allTestTahribatsizManuelUtSonucList,
			UserCredentialsBean userBean) {

		try {
			TestTahribatsizHidrostatikManager manager = new TestTahribatsizHidrostatikManager();
			// bağ koparıldı
			kontrol = true;
			// while (allTestTahribatsizManuelUtSonucList.size() > index) {
			// if (allTestTahribatsizManuelUtSonucList.get(index)
			// .getTestTahribatsizHidrostatik() == null
			// && allTestTahribatsizManuelUtSonucList.get(index)
			// .getDegerlendirme() != null
			// && (allTestTahribatsizManuelUtSonucList.get(index)
			// .getDegerlendirme() == 1 // 1=kabul
			// || allTestTahribatsizManuelUtSonucList.get(index)
			// .getDegerlendirme() == 2 // 2=tamir
			// )
			// && allTestTahribatsizManuelUtSonucList.get(index)
			// .getTamirSonrasiDurum() == 1) {// 5=hidro
			// // 1=kabul
			//
			// kontrol = true;
			// } else {
			// FacesContext context = FacesContext.getCurrentInstance();
			// context.addMessage(null, new FacesMessage(
			// FacesMessage.SEVERITY_ERROR,
			// "HİDROSTATİK TEST EKLENEMEDİ!", null));
			// kontrol = false;
			// break;
			// }
			// index++;
			// }

			if (kontrol) {
				newHidro.setDurum(null);
				newHidro.setEklemeZamani(UtilInsCore.getTarihZaman());
				newHidro.setUser(userBean.getUser());
				newHidro.setTestTahribatsizManuelUtSonuc(allTestTahribatsizManuelUtSonucList
						.get(0));
				newHidro.setPipe(allTestTahribatsizManuelUtSonucList.get(0)
						.getPipe());
				manager.enterNew(newHidro);
				newHidro = new TestTahribatsizHidrostatik();

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								"BORU, MANUET UT TARAFINDAN HİDROSTATİK TESTE BAŞARIYLA GÖNDERİLMİŞTİR!"));
			} else {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"BORU HİDROSTATİK TESTE GÖNDERİLEMEDİ. BORUYLA İLGİLİ KABUL EDİLMEMİŞ VERİLER VARDIR, LÜTFEN KONTROL EDİNİZ!",
								null));
			}

		} catch (Exception e) {
			System.out
					.println("TestTahribatsizHidrostatikBean.addFromManuel-HATA");
		}
	}

	public void addFromFl(
			List<TestTahribatsizFloroskopikSonuc> allTestTahribatsizFloroskopikSonucList,
			UserCredentialsBean userBean) {

		try {
			TestTahribatsizHidrostatikManager manager = new TestTahribatsizHidrostatikManager();

			// bağ koparıldı
			kontrol = true;
			// while (allTestTahribatsizFloroskopikSonucList.size() > index) {
			// if (allTestTahribatsizFloroskopikSonucList.get(index)
			// .getTestTahribatsizHidrostatik() == null
			// && allTestTahribatsizFloroskopikSonucList.get(index)
			// .getResult() != null
			// && allTestTahribatsizFloroskopikSonucList.get(index)
			// .getResult() == 1) {// 5=hidro 1=kabul
			// kontrol = true;
			// } else {
			// kontrol = false;
			// }
			// index++;
			// }
			if (kontrol) {
				// newHidro.setDurum(false);
				newHidro.setEklemeZamani(UtilInsCore.getTarihZaman());
				newHidro.setUser(userBean.getUser());
				newHidro.setTestTahribatsizFloroskopikSonuc(allTestTahribatsizFloroskopikSonucList
						.get(index));
				newHidro.setPipe(allTestTahribatsizFloroskopikSonucList.get(
						index).getPipe());
				manager.enterNew(newHidro);
				newHidro = new TestTahribatsizHidrostatik();

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								"BORU, FLOROSKOPİ TARAFINDAN HİDROSTATİK TESTE BAŞARIYLA GÖNDERİLMİŞTİR!"));
			} else {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"BORU HİDROSTATİK TESTE GÖNDERİLEMEDİ. BORUYLA İLGİLİ KABUL EDİLMEMİŞ VERİLER VARDIR, LÜTFEN KONTROL EDİNİZ!",
								null));
			}

		} catch (Exception e) {
			System.out.println("TestTahribatsizHidrostatikBean.addFromFl-HATA");
		}
	}

	public void addHidro(ActionEvent ae) {

		UICommand cmd = (UICommand) ae.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId != null) {
			PipeManager pipeMan = new PipeManager();
			selectedPipe = pipeMan.loadObject(Pipe.class, prmPipeId);
		}

		try {
			TestTahribatsizHidrostatikManager manager = new TestTahribatsizHidrostatikManager();

			newHidro.setEklemeZamani(UtilInsCore.getTarihZaman());
			newHidro.setUser(userBean.getUser());
			newHidro.setPipe(selectedPipe);
			manager.enterNew(newHidro);
			newHidro = new TestTahribatsizHidrostatik();

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"HİDROSTATİK İŞLEMİ BAŞARIYLA EKLENMİŞTİR!"));

		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL,
					"HİDROSTATİK İŞLEMİ EKLENEMEDİ!", null));
			System.out.println("TestTahribatsizHidrostatikBean.addHidro-HATA");
		}

		fillTestList(prmPipeId);
	}

	public void addHidroSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		Boolean prmKontrol = testTahribatsizHidrostatikForm.getDurum();
		TestTahribatsizHidrostatikManager hidroManager = new TestTahribatsizHidrostatikManager();
		// TestTahribatsizTornaManager tornaManager = new
		// TestTahribatsizTornaManager();
		TestTahribatsizGozOlcuSonucManager gozManager = new TestTahribatsizGozOlcuSonucManager();

		// Boolean tornaKontrol = true;
		Boolean gozOlcuKontrol = false;

		if (prmKontrol == null) {

			try {

				testTahribatsizHidrostatikForm.setDurum(false);
				testTahribatsizHidrostatikForm
						.setMuayeneBaslamaZamani(UtilInsCore.getTarihZaman());
				testTahribatsizHidrostatikForm.setMuayeneBaslamaUser(userBean
						.getUser());
				hidroManager.updateEntity(testTahribatsizHidrostatikForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"HİDROSTATİK İŞLEMİ BAŞARIYLA BAŞLANMIŞTIR!"));
			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"HİDROSTATİK İŞLEMİ BAŞLANAMADI!", null));
				System.out
						.println("TestTahribatsizHidrostatikBean.addHidroSonuc-HATA-BAŞLAMA");
			}
		} else if (prmKontrol == false) {

			try {

				testTahribatsizHidrostatikForm.setDurum(true);
				testTahribatsizHidrostatikForm
						.setMuayeneBitisZamani(UtilInsCore.getTarihZaman());
				testTahribatsizHidrostatikForm.setMuayeneBitisUser(userBean
						.getUser());
				hidroManager.updateEntity(testTahribatsizHidrostatikForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"HİDROSTATİK İŞLEMİ BAŞARIYLA BİTİRİLMİŞTİR!"));

				// if (tornaManager.getAllTestTahribatsizTorna(
				// testTahribatsizHidrostatikForm.getPipe().getPipeId())
				// .size() != 0) {
				// tornaKontrol = true;
				// }

				if (gozManager.getAllTestTahribatsizGozOlcuSonuc(
						testTahribatsizHidrostatikForm.getPipe().getPipeId())
						.size() != 0) {
					gozOlcuKontrol = true;
				}

				if (!gozOlcuKontrol) {// eger seçili boru için
										// torna tamamsa ve göz
					// ölçü yoksa, göz olcu eklenir.

					gozOlcuEkle();
				} else if (testTahribatsizHidrostatikForm.getPipe()
						.getSalesItem().getPipeType().equals("Kazık Borusu")
						&& !gozOlcuKontrol) {// eğer kazık
					// borusuysa ve goz
					// olcu yoksa, goz
					// olcu eklenir
					gozOlcuEkle();
				} else {
					context.addMessage(
							null,
							new FacesMessage(
									FacesMessage.SEVERITY_ERROR,
									"HİDROSTATİK TEST EKSİK OLDUĞUNDAN YA DA GÖZ ÖLÇÜ VERİSİ OLDUĞUNDAN, GÖZ ÖLÇÜ EKLENEMEDİ!",
									null));
				}

			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"HİDROSTATİK İŞLEMİ BİTİRİLEMEDİ!", null));
				System.out
						.println("TestTahribatsizHidrostatikBean.addHidroSonuc-HATA-BİTİŞ"
								+ e.toString());
			}

		}

		fillTestList(prmPipeId);
	}

	public void fillTestList(Integer prmPipeId) {

		TestTahribatsizHidrostatikManager hidroManager = new TestTahribatsizHidrostatikManager();
		allTestTahribatsizHidrostatikList = hidroManager
				.getAllTestTahribatsizHidrostatik(prmPipeId);
	}

	public void hidrostatikListener(SelectEvent event) {
		updateButtonRender = true;
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		HidrostaticTestsSpecManager specManager = new HidrostaticTestsSpecManager();
		FacesContext context = FacesContext.getCurrentInstance();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!", null));
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			TestTahribatsizHidrostatikManager hidroManager = new TestTahribatsizHidrostatikManager();
			allTestTahribatsizHidrostatikList = hidroManager
					.getAllTestTahribatsizHidrostatik(selectedPipe.getPipeId());

			try {
				hidroSpecs = specManager.findByItemId(selectedPipe
						.getSalesItem().getItemId());
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								prmBarkod
										+ " BARKOD NUMARALI BORU İÇİN HİDROSTATİK KALİTE VERİLERİ EKSİKTİR, LÜTFEN İMALAT FORMENİNİ ARAYINIZ!",
								null));
				return;
			}

			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
		}
	}

	public void deleteHidroSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		TestTahribatsizHidrostatikManager hidroManager = new TestTahribatsizHidrostatikManager();

		if (testTahribatsizHidrostatikForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		hidroManager.delete(testTahribatsizHidrostatikForm);
		testTahribatsizHidrostatikForm = new TestTahribatsizHidrostatik();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"HİDROSTATİK BAŞARIYLA SİLİNMİŞTİR!"));

		fillTestList(prmPipeId);
	}

	public void gozOlcuEkle() {

		gozOlcuBean.addFromHidro(testTahribatsizHidrostatikForm, userBean);
		fillTestList(allTestTahribatsizHidrostatikList.get(0).getPipe()
				.getPipeId());
	}

	public void stokBoruGozOlcuEkle() {

		gozOlcuBean.stokBoruGozOlcuEkle(selectedPipe, userBean);
	}

	// setters getters
	public TestTahribatsizHidrostatik getNewHidro() {
		return newHidro;
	}

	public void setNewHidro(TestTahribatsizHidrostatik newHidro) {
		this.newHidro = newHidro;
	}

	public List<TestTahribatsizHidrostatik> getHidroList() {
		return hidroList;
	}

	public void setHidroList(List<TestTahribatsizHidrostatik> hidroList) {
		this.hidroList = hidroList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public List<TestTahribatsizHidrostatik> getAllTestTahribatsizHidrostatikaList() {
		return allTestTahribatsizHidrostatikList;
	}

	public void setAllTestTahribatsizHidrostatikaList(
			List<TestTahribatsizHidrostatik> allTestTahribatsizHidrostatikaList) {
		this.allTestTahribatsizHidrostatikList = allTestTahribatsizHidrostatikaList;
	}

	public TestTahribatsizHidrostatik getTestTahribatsizHidrostatikForm() {
		return testTahribatsizHidrostatikForm;
	}

	public void setTestTahribatsizHidrostatikForm(
			TestTahribatsizHidrostatik testTahribatsizHidrostatikForm) {
		this.testTahribatsizHidrostatikForm = testTahribatsizHidrostatikForm;
	}

	public List<TestTahribatsizHidrostatik> getAllTestTahribatsizHidrostatikList() {
		return allTestTahribatsizHidrostatikList;
	}

	public void setAllTestTahribatsizHidrostatikList(
			List<TestTahribatsizHidrostatik> allTestTahribatsizHidrostatikList) {
		this.allTestTahribatsizHidrostatikList = allTestTahribatsizHidrostatikList;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	public String getBarkodNo() {
		return barkodNo;
	}

	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	public TestTahribatsizGozOlcuSonucBean getGozOlcuBean() {
		return gozOlcuBean;
	}

	public void setGozOlcuBean(TestTahribatsizGozOlcuSonucBean gozOlcuBean) {
		this.gozOlcuBean = gozOlcuBean;
	}

	/**
	 * @return the hidroSpecs
	 */
	public List<HidrostaticTestsSpec> getHidroSpecs() {
		return hidroSpecs;
	}

	/**
	 * @param hidroSpecs
	 *            the hidroSpecs to set
	 */
	public void setHidroSpecs(List<HidrostaticTestsSpec> hidroSpecs) {
		this.hidroSpecs = hidroSpecs;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
