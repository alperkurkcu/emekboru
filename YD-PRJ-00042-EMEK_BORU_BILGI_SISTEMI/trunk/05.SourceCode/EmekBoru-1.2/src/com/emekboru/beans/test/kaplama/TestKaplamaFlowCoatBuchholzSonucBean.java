/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaFlowCoatBuchholzSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaFlowCoatBuchholzSonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testKaplamaFlowCoatBuchholzSonucBean")
@ViewScoped
public class TestKaplamaFlowCoatBuchholzSonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<TestKaplamaFlowCoatBuchholzSonuc> allKaplamaFlowCoatBuchholzSonucList = new ArrayList<TestKaplamaFlowCoatBuchholzSonuc>();
	private TestKaplamaFlowCoatBuchholzSonuc testKaplamaFlowCoatBuchholzSonucForm = new TestKaplamaFlowCoatBuchholzSonuc();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;
	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	float ortalama = 0F;

	public void addKaplamaFlowCoatBuchholzSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaFlowCoatBuchholzSonucManager tkdsManager = new TestKaplamaFlowCoatBuchholzSonucManager();

		if (testKaplamaFlowCoatBuchholzSonucForm.getId() == null) {

			testKaplamaFlowCoatBuchholzSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaFlowCoatBuchholzSonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaFlowCoatBuchholzSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaFlowCoatBuchholzSonucForm.setBagliTestId(bagliTest);
			testKaplamaFlowCoatBuchholzSonucForm.setBagliGlobalId(bagliTest);

			ortalamaBul();

			tkdsManager.enterNew(testKaplamaFlowCoatBuchholzSonucForm);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaFlowCoatBuchholzSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaFlowCoatBuchholzSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());

			ortalamaBul();

			tkdsManager.updateEntity(testKaplamaFlowCoatBuchholzSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");

		}
		fillTestList(prmGlobalId, prmPipeId);
	}

	private void ortalamaBul() {

		ortalama = (testKaplamaFlowCoatBuchholzSonucForm.getVeri1()
				.floatValue()
				+ testKaplamaFlowCoatBuchholzSonucForm.getVeri2().floatValue()
				+ testKaplamaFlowCoatBuchholzSonucForm.getVeri3().floatValue()
				+ testKaplamaFlowCoatBuchholzSonucForm.getVeri4().floatValue() + testKaplamaFlowCoatBuchholzSonucForm
				.getVeri5().floatValue()) / 5;
		testKaplamaFlowCoatBuchholzSonucForm.setAritmetikMean(BigDecimal
				.valueOf(ortalama));
	}

	public void addKaplamaFlowCoatBuchholz() {
		testKaplamaFlowCoatBuchholzSonucForm = new TestKaplamaFlowCoatBuchholzSonuc();
		updateButtonRender = false;
	}

	public void kaplamaFlowCoatBuchholzListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(int globalId, Integer pipeId2) {
		allKaplamaFlowCoatBuchholzSonucList = TestKaplamaFlowCoatBuchholzSonucManager
				.getAllTestKaplamaFlowCoatBuchholzSonuc(globalId, pipeId2);
		testKaplamaFlowCoatBuchholzSonucForm = new TestKaplamaFlowCoatBuchholzSonuc();
	}

	public void deleteTestKaplamaFlowCoatBuchholzSonuc() {

		bagliTestId = testKaplamaFlowCoatBuchholzSonucForm.getBagliTestId()
				.getId();

		if (testKaplamaFlowCoatBuchholzSonucForm == null
				|| testKaplamaFlowCoatBuchholzSonucForm.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaFlowCoatBuchholzSonucManager tkdsManager = new TestKaplamaFlowCoatBuchholzSonucManager();
		tkdsManager.delete(testKaplamaFlowCoatBuchholzSonucForm);
		testKaplamaFlowCoatBuchholzSonucForm = new TestKaplamaFlowCoatBuchholzSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setters getters

	/**
	 * @return the allKaplamaFlowCoatBuchholzSonucList
	 */
	public List<TestKaplamaFlowCoatBuchholzSonuc> getAllKaplamaFlowCoatBuchholzSonucList() {
		return allKaplamaFlowCoatBuchholzSonucList;
	}

	/**
	 * @param allKaplamaFlowCoatBuchholzSonucList
	 *            the allKaplamaFlowCoatBuchholzSonucList to set
	 */
	public void setAllKaplamaFlowCoatBuchholzSonucList(
			List<TestKaplamaFlowCoatBuchholzSonuc> allKaplamaFlowCoatBuchholzSonucList) {
		this.allKaplamaFlowCoatBuchholzSonucList = allKaplamaFlowCoatBuchholzSonucList;
	}

	/**
	 * @return the testKaplamaFlowCoatBuchholzSonucForm
	 */
	public TestKaplamaFlowCoatBuchholzSonuc getTestKaplamaFlowCoatBuchholzSonucForm() {
		return testKaplamaFlowCoatBuchholzSonucForm;
	}

	/**
	 * @param testKaplamaFlowCoatBuchholzSonucForm
	 *            the testKaplamaFlowCoatBuchholzSonucForm to set
	 */
	public void setTestKaplamaFlowCoatBuchholzSonucForm(
			TestKaplamaFlowCoatBuchholzSonuc testKaplamaFlowCoatBuchholzSonucForm) {
		this.testKaplamaFlowCoatBuchholzSonucForm = testKaplamaFlowCoatBuchholzSonucForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
