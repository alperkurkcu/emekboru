package com.emekboru.beans.machine;

import java.util.ArrayList;
import java.util.List;

import org.primefaces.model.LazyDataModel;

import com.emekboru.jpa.ElectrodeDustWire;
import com.emekboru.jpa.rulo.Rulo;
import com.emekboru.jpa.sales.order.SalesItem;

public class AllocatedMaterials {

	// private List<Rulo> allocatedRulos;
	private LazyDataModel<Rulo> allocatedRulosLazy;
	private List<ElectrodeDustWire> allocatedDusts;
	private List<ElectrodeDustWire> allocatedWires;
	// private List<SalesItem> allocatedSalesItems;
	private LazyDataModel<SalesItem> allocatedSalesItemsLazy;

	private Rulo[] selectedRulos;
	private ElectrodeDustWire[] selectedDusts;
	private ElectrodeDustWire[] selectedWires;
	private ElectrodeDustWire selectedDust;
	private ElectrodeDustWire selectedWire;
	private SalesItem selectedSalesItems;

	public AllocatedMaterials() {

		// allocatedRulos = new ArrayList<Rulo>();
		allocatedDusts = new ArrayList<ElectrodeDustWire>();
		allocatedWires = new ArrayList<ElectrodeDustWire>();
		// allocatedSalesItems = new ArrayList<SalesItem>();
	}

	// ********************************************************************** //
	// ************************** GETTERS AND SETTERS *********************** //
	// ********************************************************************** //

	public List<ElectrodeDustWire> getAllocatedDusts() {
		return allocatedDusts;
	}

	public void setAllocatedDusts(List<ElectrodeDustWire> allocatedDusts) {
		this.allocatedDusts = allocatedDusts;
	}

	public List<ElectrodeDustWire> getAllocatedWires() {
		return allocatedWires;
	}

	public void setAllocatedWires(List<ElectrodeDustWire> allocatedWires) {
		this.allocatedWires = allocatedWires;
	}

	public Rulo[] getSelectedRulos() {
		return selectedRulos;
	}

	public void setSelectedRulos(Rulo[] selectedRulos) {
		this.selectedRulos = selectedRulos;
	}

	public ElectrodeDustWire[] getSelectedDusts() {
		return selectedDusts;
	}

	public void setSelectedDusts(ElectrodeDustWire[] selectedDusts) {
		this.selectedDusts = selectedDusts;
	}

	public ElectrodeDustWire[] getSelectedWires() {
		return selectedWires;
	}

	public void setSelectedWires(ElectrodeDustWire[] selectedWires) {
		this.selectedWires = selectedWires;
	}

	public SalesItem getSelectedSalesItems() {
		return selectedSalesItems;
	}

	public void setSelectedSalesItems(SalesItem selectedSalesItems) {
		this.selectedSalesItems = selectedSalesItems;
	}

	/**
	 * @return the selectedDust
	 */
	public ElectrodeDustWire getSelectedDust() {
		return selectedDust;
	}

	/**
	 * @param selectedDust
	 *            the selectedDust to set
	 */
	public void setSelectedDust(ElectrodeDustWire selectedDust) {
		this.selectedDust = selectedDust;
	}

	/**
	 * @return the selectedWire
	 */
	public ElectrodeDustWire getSelectedWire() {
		return selectedWire;
	}

	/**
	 * @param selectedWire
	 *            the selectedWire to set
	 */
	public void setSelectedWire(ElectrodeDustWire selectedWire) {
		this.selectedWire = selectedWire;
	}

	/**
	 * @return the allocatedSalesItemsLazy
	 */
	public LazyDataModel<SalesItem> getAllocatedSalesItemsLazy() {
		return allocatedSalesItemsLazy;
	}

	/**
	 * @param allocatedSalesItemsLazy
	 *            the allocatedSalesItemsLazy to set
	 */
	public void setAllocatedSalesItemsLazy(
			LazyDataModel<SalesItem> allocatedSalesItemsLazy) {
		this.allocatedSalesItemsLazy = allocatedSalesItemsLazy;
	}

	/**
	 * @return the allocatedRulosLazy
	 */
	public LazyDataModel<Rulo> getAllocatedRulosLazy() {
		return allocatedRulosLazy;
	}

	/**
	 * @param allocatedRulosLazy
	 *            the allocatedRulosLazy to set
	 */
	public void setAllocatedRulosLazy(LazyDataModel<Rulo> allocatedRulosLazy) {
		this.allocatedRulosLazy = allocatedRulosLazy;
	}

}
