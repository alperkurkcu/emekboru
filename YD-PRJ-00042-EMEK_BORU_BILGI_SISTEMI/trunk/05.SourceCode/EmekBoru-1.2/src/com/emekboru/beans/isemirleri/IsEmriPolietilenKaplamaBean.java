/**
 * 
 */
package com.emekboru.beans.isemirleri;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.emekboru.beans.istakipformu.IsTakipFormuPolietilenKaplamaBean;
import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.isemri.IsEmriPolietilenKaplama;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.CoatRawMaterialManager;
import com.emekboru.jpaman.isemirleri.IsEmriPolietilenKaplamaManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.sales.order.SalesItemManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isEmriPolietilenKaplamaBean")
@ViewScoped
public class IsEmriPolietilenKaplamaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private IsEmriPolietilenKaplama isEmriPolietilenKaplamaForm = new IsEmriPolietilenKaplama();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private List<CoatRawMaterial> allPolietilenKaplamaMalzemeList = new ArrayList<CoatRawMaterial>();

	@ManagedProperty(value = "#{isTakipFormuPolietilenKaplamaBean}")
	private IsTakipFormuPolietilenKaplamaBean isTakipFormuPolietilenKaplamaBean;

	// kaplama kalite
	private List<IsolationTestDefinition> internalIsolationTestDefinitions = new ArrayList<IsolationTestDefinition>();
	private String tozEpoksiKalinligi = null;
	private String yapistiriciKalinligi = null;
	private String polietilenKalinligi = null;

	@PostConstruct
	public void load() {

		isEmriPolietilenKaplamaForm
				.getIsEmriPolietilenKaplamaPolietilenSicaklik();
		isEmriPolietilenKaplamaForm
				.getIsEmriPolietilenKaplamaKontrolParametreleri();
		isEmriPolietilenKaplamaForm
				.getIsEmriPolietilenKaplamaYapistiriciSicaklik();
	}

	public IsEmriPolietilenKaplamaBean() {

	}

	public void addOrUpdateIsEmriPolietilenKaplama(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmItemId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		SalesItem salesItem = new SalesItemManager().loadObject(
				SalesItem.class, prmItemId);

		IsEmriPolietilenKaplamaManager isMan = new IsEmriPolietilenKaplamaManager();

		if (isEmriPolietilenKaplamaForm.getId() == null) {

			isEmriPolietilenKaplamaForm.setSalesItem(salesItem);
			isEmriPolietilenKaplamaForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			isEmriPolietilenKaplamaForm.setEkleyenEmployee(userBean.getUser());

			isMan.enterNew(isEmriPolietilenKaplamaForm);

			// polietilen iş takip formu ekleme
			isTakipFormuPolietilenKaplamaBean.addFromIsEmri(salesItem);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"POLİETİLEN KAPLAMA İŞ EMRİ BAŞARIYLA EKLENMİŞTİR!"));
		} else {

			isEmriPolietilenKaplamaForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			isEmriPolietilenKaplamaForm.setGuncelleyenEmployee(userBean
					.getUser());
			isMan.updateEntity(isEmriPolietilenKaplamaForm);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_WARN,
					"POLİETİLEN KAPLAMA İŞ EMRİ BAŞARIYLA GÜNCELLENMİŞTİR!",
					null));
		}
	}

	public void deleteIsEmriPolietilenKaplama() {

		IsEmriPolietilenKaplamaManager isMan = new IsEmriPolietilenKaplamaManager();

		if (isEmriPolietilenKaplamaForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		isMan.delete(isEmriPolietilenKaplamaForm);
		isEmriPolietilenKaplamaForm = new IsEmriPolietilenKaplama();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
				"POLİETİLEN KAPLAMA İŞ EMRİ BAŞARIYLA SİLİNMİŞTİR!", null));
	}

	@SuppressWarnings("static-access")
	public void loadIsEmri(Integer itemId) {

		IsEmriPolietilenKaplamaManager isMan = new IsEmriPolietilenKaplamaManager();
		IsolationTestDefinitionManager kaliteMan = new IsolationTestDefinitionManager();

		if (isMan.getAllIsEmriPolietilenKaplama(itemId).size() > 0) {
			isEmriPolietilenKaplamaForm = new IsEmriPolietilenKaplamaManager()
					.getAllIsEmriPolietilenKaplama(itemId).get(0);
		} else {
			isEmriPolietilenKaplamaForm = new IsEmriPolietilenKaplama();
		}

		internalIsolationTestDefinitions = kaliteMan
				.findIsolationByItemIdAndIsolationType(0,
						isEmriPolietilenKaplamaForm.getSalesItem());
		for (int i = 0; i < internalIsolationTestDefinitions.size(); i++) {
			if (internalIsolationTestDefinitions.get(i).getGlobalId() == 2024) {
				yapistiriciKalinligi = internalIsolationTestDefinitions.get(i)
						.getAciklama();
			} else if (internalIsolationTestDefinitions.get(i).getGlobalId() == 2023) {
				tozEpoksiKalinligi = internalIsolationTestDefinitions.get(i)
						.getAciklama();
			} else if (internalIsolationTestDefinitions.get(i).getGlobalId() == 2014) {
				polietilenKalinligi = internalIsolationTestDefinitions.get(i)
						.getAciklama();
			}
		}
	}

	public void isEmriMalzemeEkle() {

		IsEmriPolietilenKaplamaManager formManager = new IsEmriPolietilenKaplamaManager();
		formManager.updateEntity(isEmriPolietilenKaplamaForm);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
				"POLİETİLEN MALZEMESİ BAŞARIYLA GİRİLMİŞTİR!", null));

	}

	public void loadEpoksiMalzemeleri() {

		CoatRawMaterialManager materialMan = new CoatRawMaterialManager();
		// 8 boya turudur - coat_material_type tablosundan
		allPolietilenKaplamaMalzemeList = materialMan
				.findUnfinishedCoatMaterialByType(8);
	}

	public void loadYapistiriciMalzemeleri() {

		CoatRawMaterialManager materialMan = new CoatRawMaterialManager();
		// 1002 yapistirici turudur - coat_material_type tablosundan
		allPolietilenKaplamaMalzemeList = materialMan
				.findUnfinishedCoatMaterialByType(1002);
	}

	public void loadPolietilenMalzemeleri() {

		CoatRawMaterialManager materialMan = new CoatRawMaterialManager();
		// 1000 poolietilen turudur - coat_material_type tablosundan
		allPolietilenKaplamaMalzemeList = materialMan
				.findUnfinishedCoatMaterialByType(1000);
	}

	// setters getters

	public IsEmriPolietilenKaplama getIsEmriPolietilenKaplamaForm() {
		return isEmriPolietilenKaplamaForm;
	}

	public void setIsEmriPolietilenKaplamaForm(
			IsEmriPolietilenKaplama isEmriPolietilenKaplamaForm) {
		this.isEmriPolietilenKaplamaForm = isEmriPolietilenKaplamaForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public List<CoatRawMaterial> getAllPolietilenKaplamaMalzemeList() {
		return allPolietilenKaplamaMalzemeList;
	}

	public void setAllPolietilenKaplamaMalzemeList(
			List<CoatRawMaterial> allPolietilenKaplamaMalzemeList) {
		this.allPolietilenKaplamaMalzemeList = allPolietilenKaplamaMalzemeList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public IsTakipFormuPolietilenKaplamaBean getIsTakipFormuPolietilenKaplamaBean() {
		return isTakipFormuPolietilenKaplamaBean;
	}

	public void setIsTakipFormuPolietilenKaplamaBean(
			IsTakipFormuPolietilenKaplamaBean isTakipFormuPolietilenKaplamaBean) {
		this.isTakipFormuPolietilenKaplamaBean = isTakipFormuPolietilenKaplamaBean;
	}

	/**
	 * @return the tozEpoksiKalinligi
	 */
	public String getTozEpoksiKalinligi() {
		return tozEpoksiKalinligi;
	}

	/**
	 * @param tozEpoksiKalinligi
	 *            the tozEpoksiKalinligi to set
	 */
	public void setTozEpoksiKalinligi(String tozEpoksiKalinligi) {
		this.tozEpoksiKalinligi = tozEpoksiKalinligi;
	}

	/**
	 * @return the yapistiriciKalinligi
	 */
	public String getYapistiriciKalinligi() {
		return yapistiriciKalinligi;
	}

	/**
	 * @param yapistiriciKalinligi
	 *            the yapistiriciKalinligi to set
	 */
	public void setYapistiriciKalinligi(String yapistiriciKalinligi) {
		this.yapistiriciKalinligi = yapistiriciKalinligi;
	}

	/**
	 * @return the polietilenKalinligi
	 */
	public String getPolietilenKalinligi() {
		return polietilenKalinligi;
	}

	/**
	 * @param polietilenKalinligi
	 *            the polietilenKalinligi to set
	 */
	public void setPolietilenKalinligi(String polietilenKalinligi) {
		this.polietilenKalinligi = polietilenKalinligi;
	}

}
