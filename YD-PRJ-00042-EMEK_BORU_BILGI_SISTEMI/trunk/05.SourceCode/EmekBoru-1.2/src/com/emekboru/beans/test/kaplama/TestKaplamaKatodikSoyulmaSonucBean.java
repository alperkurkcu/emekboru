/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaKatodikSoyulmaSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaKatodikSoyulmaSonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */

@ManagedBean(name = "testKaplamaKatodikSoyulmaSonucBean")
@ViewScoped
public class TestKaplamaKatodikSoyulmaSonucBean {

	private TestKaplamaKatodikSoyulmaSonuc testKaplamaKatodikSoyulmaSonucForm = new TestKaplamaKatodikSoyulmaSonuc();
	private List<TestKaplamaKatodikSoyulmaSonuc> allKatodikSoyulmaSonucList = new ArrayList<TestKaplamaKatodikSoyulmaSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateKatodikSoyulmaTestSonuc(ActionEvent e) {
		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaKatodikSoyulmaSonucManager manager = new TestKaplamaKatodikSoyulmaSonucManager();

		if (testKaplamaKatodikSoyulmaSonucForm.getId() == null) {

			testKaplamaKatodikSoyulmaSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaKatodikSoyulmaSonucForm.setEkleyenEmployee(userBean
					.getUser());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaKatodikSoyulmaSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaKatodikSoyulmaSonucForm.setBagliTestId(bagliTest);
			testKaplamaKatodikSoyulmaSonucForm.setBagliGlobalId(bagliTest);

			manager.enterNew(testKaplamaKatodikSoyulmaSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			testKaplamaKatodikSoyulmaSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaKatodikSoyulmaSonucForm.setGuncelleyenEmployee(userBean
					.getUser());
			manager.updateEntity(testKaplamaKatodikSoyulmaSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addKatodikSoyulmaTest() {

		testKaplamaKatodikSoyulmaSonucForm = new TestKaplamaKatodikSoyulmaSonuc();
		updateButtonRender = false;
	}

	public void katodikSoyulmaListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allKatodikSoyulmaSonucList = TestKaplamaKatodikSoyulmaSonucManager
				.getAllKatodikSoyulmaSonucTest(globalId, pipeId);
	}

	public void deleteTestKaplamaKatodikSoyulmaSonuc() {

		bagliTestId = testKaplamaKatodikSoyulmaSonucForm.getBagliTestId()
				.getId();

		if (testKaplamaKatodikSoyulmaSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaKatodikSoyulmaSonucManager manager = new TestKaplamaKatodikSoyulmaSonucManager();
		manager.delete(testKaplamaKatodikSoyulmaSonucForm);
		testKaplamaKatodikSoyulmaSonucForm = new TestKaplamaKatodikSoyulmaSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	/**
	 * @return the testKaplamaKatodikSoyulmaSonucForm
	 */
	public TestKaplamaKatodikSoyulmaSonuc getTestKaplamaKatodikSoyulmaSonucForm() {
		return testKaplamaKatodikSoyulmaSonucForm;
	}

	/**
	 * @param testKaplamaKatodikSoyulmaSonucForm
	 *            the testKaplamaKatodikSoyulmaSonucForm to set
	 */
	public void setTestKaplamaKatodikSoyulmaSonucForm(
			TestKaplamaKatodikSoyulmaSonuc testKaplamaKatodikSoyulmaSonucForm) {
		this.testKaplamaKatodikSoyulmaSonucForm = testKaplamaKatodikSoyulmaSonucForm;
	}

	/**
	 * @return the allKatodikSoyulmaSonucList
	 */
	public List<TestKaplamaKatodikSoyulmaSonuc> getAllKatodikSoyulmaSonucList() {
		return allKatodikSoyulmaSonucList;
	}

	/**
	 * @param allKatodikSoyulmaSonucList
	 *            the allKatodikSoyulmaSonucList to set
	 */
	public void setAllKatodikSoyulmaSonucList(
			List<TestKaplamaKatodikSoyulmaSonuc> allKatodikSoyulmaSonucList) {
		this.allKatodikSoyulmaSonucList = allKatodikSoyulmaSonucList;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

}
