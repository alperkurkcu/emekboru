package com.emekboru.beans.test.kaplama;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaKumlamaOncesiOnIsitmaSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaKumlamaOncesiOnIsitmaSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaplamaKumlamaOncesiOnIsitmaSonucBean")
@ViewScoped
public class TestKaplamaKumlamaOncesiOnIsitmaSonucBean {

	private TestKaplamaKumlamaOncesiOnIsitmaSonuc testKaplamaKumlamaOncesiOnIsitmaSonucForm = new TestKaplamaKumlamaOncesiOnIsitmaSonuc();
	private List<TestKaplamaKumlamaOncesiOnIsitmaSonuc> allTestKaplamaKumlamaOncesiOnIsitmaSonucList = new ArrayList<TestKaplamaKumlamaOncesiOnIsitmaSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateTestKaplamaKumlamaOncesiOnIsitmaSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaKumlamaOncesiOnIsitmaSonucManager manager = new TestKaplamaKumlamaOncesiOnIsitmaSonucManager();

		if (testKaplamaKumlamaOncesiOnIsitmaSonucForm.getId() == null) {

			testKaplamaKumlamaOncesiOnIsitmaSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaKumlamaOncesiOnIsitmaSonucForm
					.setEkleyenKullanici(userBean.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaKumlamaOncesiOnIsitmaSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaKumlamaOncesiOnIsitmaSonucForm.setBagliTestId(bagliTest);
			testKaplamaKumlamaOncesiOnIsitmaSonucForm
					.setBagliGlobalId(bagliTest);

			manager.enterNew(testKaplamaKumlamaOncesiOnIsitmaSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaKumlamaOncesiOnIsitmaSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaKumlamaOncesiOnIsitmaSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			manager.updateEntity(testKaplamaKumlamaOncesiOnIsitmaSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addTestKaplamaKumlamaOncesiOnIsitmaSonuc() {

		testKaplamaKumlamaOncesiOnIsitmaSonucForm = new TestKaplamaKumlamaOncesiOnIsitmaSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaKumlamaOncesiOnIsitmaSonucListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaKumlamaOncesiOnIsitmaSonucList = TestKaplamaKumlamaOncesiOnIsitmaSonucManager
				.getAllTestKaplamaKumlamaOncesiOnIsitmaSonuc(globalId, pipeId);
		testKaplamaKumlamaOncesiOnIsitmaSonucForm = new TestKaplamaKumlamaOncesiOnIsitmaSonuc();
	}

	public void deleteTestKaplamaKumlamaOncesiOnIsitmaSonuc() {

		bagliTestId = testKaplamaKumlamaOncesiOnIsitmaSonucForm
				.getBagliGlobalId().getId();

		if (testKaplamaKumlamaOncesiOnIsitmaSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaKumlamaOncesiOnIsitmaSonucManager manager = new TestKaplamaKumlamaOncesiOnIsitmaSonucManager();
		manager.delete(testKaplamaKumlamaOncesiOnIsitmaSonucForm);
		testKaplamaKumlamaOncesiOnIsitmaSonucForm = new TestKaplamaKumlamaOncesiOnIsitmaSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters
	public TestKaplamaKumlamaOncesiOnIsitmaSonuc getTestKaplamaKumlamaOncesiOnIsitmaSonucForm() {
		return testKaplamaKumlamaOncesiOnIsitmaSonucForm;
	}

	public void setTestKaplamaKumlamaOncesiOnIsitmaSonucForm(
			TestKaplamaKumlamaOncesiOnIsitmaSonuc testKaplamaKumlamaOncesiOnIsitmaSonucForm) {
		this.testKaplamaKumlamaOncesiOnIsitmaSonucForm = testKaplamaKumlamaOncesiOnIsitmaSonucForm;
	}

	public List<TestKaplamaKumlamaOncesiOnIsitmaSonuc> getAllTestKaplamaKumlamaOncesiOnIsitmaSonucList() {
		return allTestKaplamaKumlamaOncesiOnIsitmaSonucList;
	}

	public void setAllTestKaplamaKumlamaOncesiOnIsitmaSonucList(
			List<TestKaplamaKumlamaOncesiOnIsitmaSonuc> allTestKaplamaKumlamaOncesiOnIsitmaSonucList) {
		this.allTestKaplamaKumlamaOncesiOnIsitmaSonucList = allTestKaplamaKumlamaOncesiOnIsitmaSonucList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
