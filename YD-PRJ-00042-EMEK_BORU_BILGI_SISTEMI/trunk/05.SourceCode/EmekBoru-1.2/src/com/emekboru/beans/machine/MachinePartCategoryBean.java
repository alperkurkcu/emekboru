package com.emekboru.beans.machine;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import com.emekboru.jpa.MachinePartCategory;
import com.emekboru.jpaman.MachinePartCategoryManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "machinePartCategoryBean")
@ViewScoped
public class MachinePartCategoryBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private MachinePartCategory selectedPartCategory;
	private MachinePartCategory newPartCategory;

	public MachinePartCategoryBean() {
		selectedPartCategory = new MachinePartCategory();
		newPartCategory = new MachinePartCategory();
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void cancelMachinePartCategorySubmittion(ActionEvent event) {
		newPartCategory = new MachinePartCategory();
	}
	
	public void submitNewMachinePartCategory(ActionEvent event)
	{
		System.out.println("MachinePartCategoryBean.submitNewMachinePartCategory()");			
		

		MachinePartCategoryManager categoryManager = new MachinePartCategoryManager();

		if(newPartCategory != null && newPartCategory.getMachinePartCategoryId() != null){			
			categoryManager.updateEntity(newPartCategory);			
		}			
		else{			

			categoryManager.enterNew(newPartCategory);
		}
		FacesContextUtils.addInfoMessage("SubmitMessage");

		// load new activity to datatable
		MachineUtilsListBean.loadMachinePartCategories();
	
		newPartCategory = new MachinePartCategory();		

		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void updateMachinePartCategory(ActionEvent event) {
		MachinePartCategoryManager partCategoryManager = new MachinePartCategoryManager();
		if(selectedPartCategory != null && selectedPartCategory.getMachinePartCategoryId() != null) {
			partCategoryManager.updateEntity(selectedPartCategory);			

		}

		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}

	public void deleteMachinePartCategory(ActionEvent event) {

		MachinePartCategoryManager partCategoryManager = new MachinePartCategoryManager();
		if(selectedPartCategory != null && selectedPartCategory.getMachinePartCategoryId() != null) {		
			partCategoryManager.deleteByField(MachinePartCategory.class, 
					"machinePartCategoryId", selectedPartCategory.getMachinePartCategoryId());

		}

		FacesContextUtils.addWarnMessage("DeletedMessage");
		selectedPartCategory = new MachinePartCategory();
		MachineUtilsListBean.setAllMachinePartCategories();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public MachinePartCategory getSelectedPart() {
		return selectedPartCategory;
	}

	public void setSelectedPart(MachinePartCategory selectedPart) {
		this.selectedPartCategory = selectedPart;
	}

	public MachinePartCategory getNewPart() {
		return newPartCategory;
	}

	public void setNewPart(MachinePartCategory newPart) {
		this.newPartCategory = newPart;
	}

}
