package com.emekboru.beans.pipe;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Customer;
import com.emekboru.jpa.Machine;
import com.emekboru.jpaman.CustomerManager;

@ManagedBean(name = "companyListBean")
@ViewScoped
public class CompanyListBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	private List<Customer> companyList;
	private List<Customer> otherCompanyList;
	private Customer selectedCompany;
	private List<Machine> productionMachineList;

	public CompanyListBean() {

		companyList = new ArrayList<Customer>();
		selectedCompany = new Customer();
		otherCompanyList = new ArrayList<Customer>();
		productionMachineList = new ArrayList<Machine>();
	}

	/*
	 * SET THE COMPANY LIST
	 */

	public void viewCompanyList() {

		CustomerManager companyManager = new CustomerManager();
		companyList = companyManager.findAll(Customer.class);
	}

	public void viewCompanyListAL(ActionEvent event) {

		viewCompanyList();
	}

	public void loadProductionMachineList() {

		// MachineManager machineManager = new MachineManager();
		// List<Machine> spiralMachineList = (List<Machine>) machineManager
		// .findByField(Machine.class,
		// "type", MachineType.SPIRAL_MACHINE);
		// productionMachineList = machineManager.findProductionMachine();
		// productionMachineList.addAll(spiralMachineList);
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public List<Machine> getProductionMachineList() {
		return productionMachineList;
	}

	public void setProductionMachineList(List<Machine> productionMachineList) {
		this.productionMachineList = productionMachineList;
	}

	public List<Customer> getCompanyList() {
		return companyList;
	}

	public void setCompanyList(List<Customer> companyList) {
		this.companyList = companyList;
	}

	public List<Customer> getOtherCompanyList() {
		return otherCompanyList;
	}

	public void setOtherCompanyList(List<Customer> otherCompanyList) {
		this.otherCompanyList = otherCompanyList;
	}

	public Customer getSelectedCompany() {
		return selectedCompany;
	}

	public void setSelectedCompany(Customer selectedCompany) {
		this.selectedCompany = selectedCompany;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
