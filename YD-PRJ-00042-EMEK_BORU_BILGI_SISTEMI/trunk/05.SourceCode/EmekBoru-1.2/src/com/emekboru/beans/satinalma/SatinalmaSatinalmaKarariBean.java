/**
 * 
 */
package com.emekboru.beans.satinalma;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.satinalma.SatinalmaMalzemeHizmetTalepFormu;
import com.emekboru.jpa.satinalma.SatinalmaSatinalmaKarari;
import com.emekboru.jpa.satinalma.SatinalmaSatinalmaKarariIcerik;
import com.emekboru.jpa.satinalma.SatinalmaTakipDosyalar;
import com.emekboru.jpa.satinalma.SatinalmaTakipDosyalarIcerik;
import com.emekboru.jpa.satinalma.SatinalmaTeklifIsteme;
import com.emekboru.jpa.satinalma.SatinalmaTeklifIstemeIcerik;
import com.emekboru.jpaman.satinalma.SatinalmaSatinalmaKarariIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaSatinalmaKarariManager;
import com.emekboru.jpaman.satinalma.SatinalmaTakipDosyalarIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaTeklifIstemeIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaTeklifIstemeManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "satinalmaSatinalmaKarariBean")
@ViewScoped
public class SatinalmaSatinalmaKarariBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	private List<SatinalmaSatinalmaKarari> allSatinalmaSatinalmaKarariList = new ArrayList<SatinalmaSatinalmaKarari>();

	private SatinalmaSatinalmaKarari satinalmaSatinalmaKarariForm = new SatinalmaSatinalmaKarari();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	// dosya içerik
	private SatinalmaTakipDosyalar satinalmaTakipDosyalarForm = new SatinalmaTakipDosyalar();
	private List<SatinalmaTakipDosyalarIcerik> satinalmaTakipDosyalarIcerikList = new ArrayList<SatinalmaTakipDosyalarIcerik>();
	private SatinalmaSatinalmaKarariIcerik satinalmaSatinalmaKarariIcerik = new SatinalmaSatinalmaKarariIcerik();

	// private SatinalmaTeklifIsteme satinalmaTeklifIsteme = new
	// SatinalmaTeklifIsteme();
	private List<SatinalmaTeklifIsteme> satinalmaTeklifIstemeList = new ArrayList<SatinalmaTeklifIsteme>();
	private List<SatinalmaTeklifIstemeIcerik> satinalmaTeklifIstemeIcerikList = new ArrayList<SatinalmaTeklifIstemeIcerik>();

	@ManagedProperty(value = "#{satinalmaSiparisMektubuBean}")
	private SatinalmaSiparisMektubuBean satinalmaSiparisMektubuBean;

	private String downloadedFileName;
	private StreamedContent downloadedFile;

	private File[] fileArray;

	private File theFile = null;
	OutputStream output = null;
	InputStream input = null;

	private String path;
	private String privatePath;
	private String toBeDeleted;

	private boolean isThisFirstTime;

	public SatinalmaSatinalmaKarariBean() {

		fillTestList();
		privatePath = File.separatorChar + "satinalma" + File.separatorChar
				+ "uploadedDocuments" + File.separatorChar + "satinalmaKarari"
				+ File.separatorChar;

		isThisFirstTime = true;
	}

	@PostConstruct
	public void load() {
		System.out.println("satinalmaSatinalmaKarariBean.load()");

		if (isThisFirstTime) {
			try {
				this.setPath(config.getConfig().getFolder().getAbsolutePath()
						+ privatePath);

				System.out.println("Path for Upload File (Satinalma):			"
						+ path);
			} catch (Exception ex) {
				System.out.println("Path for Upload File (Satinalma):			"
						+ FacesContext.getCurrentInstance()
								.getExternalContext().getRealPath(privatePath));

				System.out.print(path);
				System.out.println("CAUSE OF " + ex.toString());
			}
			theFile = new File(path);
			if (!theFile.isDirectory()) {
				theFile.mkdirs();
			}
			fileArray = theFile.listFiles();
			isThisFirstTime = false;
		}
	}

	public void addForm() {

		satinalmaSatinalmaKarariForm = new SatinalmaSatinalmaKarari();
		updateButtonRender = false;

	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList() {

		SatinalmaSatinalmaKarariManager manager = new SatinalmaSatinalmaKarariManager();
		allSatinalmaSatinalmaKarariList = manager
				.getAllSatinalmaSatinalmaKarari();
		satinalmaSatinalmaKarariForm = new SatinalmaSatinalmaKarari();
		updateButtonRender = false;
	}

	public void fillByDosyaId(Integer prmDosyaId) {

		SatinalmaTakipDosyalarIcerikManager dosyaIcerikMan = new SatinalmaTakipDosyalarIcerikManager();
		SatinalmaTeklifIstemeIcerikManager teklifIcerikMan = new SatinalmaTeklifIstemeIcerikManager();

		satinalmaTakipDosyalarIcerikList = dosyaIcerikMan
				.getAllSatinalmaTakipDosyalarIcerikByDosyaId(prmDosyaId);

		satinalmaTeklifIstemeIcerikList = teklifIcerikMan.findByField(
				SatinalmaTeklifIstemeIcerik.class,
				"satinalmaTeklifIsteme.satinalmaTakipDosyalar.id", prmDosyaId);
	}

	public void addFromMalzeme(SatinalmaMalzemeHizmetTalepFormu malzemeIstek) {

		try {
			SatinalmaSatinalmaKarariManager manager = new SatinalmaSatinalmaKarariManager();

			satinalmaSatinalmaKarariForm
					.setSatinalmaMalzemeHizmetTalepFormu(malzemeIstek);
			satinalmaSatinalmaKarariForm.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			satinalmaSatinalmaKarariForm.setEkleyenKullanici(userBean.getUser()
					.getId());
			// sayiBul();
			allSatinalmaSatinalmaKarariList.add(satinalmaSatinalmaKarariForm);
			manager.enterNew(satinalmaSatinalmaKarariForm);

			satinalmaSatinalmaKarariForm = new SatinalmaSatinalmaKarari();
			FacesContextUtils.addInfoMessage("SubmitMessage");

		} catch (Exception e) {
			System.out.println("satinalmaSatinalmaKarariBean: " + e.toString());
		}
	}

	public void addOrUpdateForm() {

		SatinalmaSatinalmaKarariManager formManager = new SatinalmaSatinalmaKarariManager();
		try {

			if (satinalmaSatinalmaKarariForm.getId() == null) {

				satinalmaSatinalmaKarariForm.setEklemeZamani(UtilInsCore
						.getTarihZaman());
				satinalmaSatinalmaKarariForm.setEkleyenKullanici(userBean
						.getUser().getId());
				// sayiBul();

				formManager.enterNew(satinalmaSatinalmaKarariForm);
				this.satinalmaSatinalmaKarariForm = new SatinalmaSatinalmaKarari();
				FacesContextUtils.addInfoMessage("FormSubmitMessage");

			} else {

				satinalmaSatinalmaKarariForm.setGuncellemeZamani(UtilInsCore
						.getTarihZaman());
				satinalmaSatinalmaKarariForm.setGuncelleyenKullanici(userBean
						.getUser().getId());
				formManager.updateEntity(satinalmaSatinalmaKarariForm);

				FacesContextUtils.addInfoMessage("FormUpdateMessage");
			}
		} catch (Exception e) {
			System.out
					.println("SatinalmaAmbarMalzemeTalepFormBean - addOrUpdateForm(): "
							+ e.toString());
		}

		fillTestList();
	}

	// public void sayiBul() {
	//
	// satinalmaSatinalmaKarariForm.setDosyaNo(SatinalmaSatinalmaKarariManager
	// .getOtomatikSayi());
	// }

	// Dönüştürülecek Karar onceden var mı?
	public boolean isMalzemeTransformedToKarar(int theMalzemeId) {

		for (int i = 0; i < allSatinalmaSatinalmaKarariList.size(); i++) {
			if (allSatinalmaSatinalmaKarariList.get(i)
					.getSatinalmaMalzemeHizmetTalepFormu() != null) {
				if (allSatinalmaSatinalmaKarariList.get(i)
						.getSatinalmaMalzemeHizmetTalepFormu().getId() == theMalzemeId) {
					return true;
				}
			}
		}

		return false;
	}

	public void addFromTeklif(SatinalmaTakipDosyalar satinalmaTakipDosyalar) {

		try {
			SatinalmaSatinalmaKarariManager manager = new SatinalmaSatinalmaKarariManager();
			SatinalmaTeklifIstemeManager teklifManager = new SatinalmaTeklifIstemeManager();

			this.satinalmaTakipDosyalarForm = satinalmaTakipDosyalar;

			if (manager.findByField(SatinalmaSatinalmaKarari.class,
					"satinalmaTakipDosyalar.id",
					satinalmaTakipDosyalarForm.getId()).size() > 0) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"SATINALMA KARARI DAHA ÖNCEDEN OLUŞTURULMUŞ, TEKRAR OLUŞTURULAMAZ, HATA!",
								null));
				return;
			}

			satinalmaTeklifIstemeList = teklifManager
					.findByDosyaId(satinalmaTakipDosyalar.getId());

			satinalmaSatinalmaKarariForm
					.setSatinalmaTakipDosyalar(satinalmaTakipDosyalarForm);
			satinalmaSatinalmaKarariForm.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			satinalmaSatinalmaKarariForm.setEkleyenKullanici(userBean.getUser()
					.getId());
			// sayiBul();
			allSatinalmaSatinalmaKarariList.add(satinalmaSatinalmaKarariForm);
			manager.enterNew(satinalmaSatinalmaKarariForm);

			satinalmaKarariIcerikEkle();

			satinalmaSatinalmaKarariForm = new SatinalmaSatinalmaKarari();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"SATINALMA KARARI OLUŞTURULDU!", null));
		} catch (Exception e) {
			System.out.println("satinalmaSatinalmaKarariBean: " + e.toString());
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"SATINALMA KARARI OLUŞTURULAMADI, HATA!", null));
		}
	}

	// dosya içeriği otomatik olarak teklife ekleniyor
	@SuppressWarnings("unused")
	public void satinalmaKarariIcerikEkle() {

		SatinalmaSatinalmaKarariIcerikManager sskim = new SatinalmaSatinalmaKarariIcerikManager();
		SatinalmaTeklifIstemeManager tim = new SatinalmaTeklifIstemeManager();
		SatinalmaTeklifIstemeIcerikManager tiim = new SatinalmaTeklifIstemeIcerikManager();

		satinalmaTeklifIstemeIcerikList = tiim.findByField(
				SatinalmaTeklifIstemeIcerik.class,
				"satinalmaTeklifIsteme.satinalmaTakipDosyalar.id",
				satinalmaTakipDosyalarForm.getId());

		satinalmaSatinalmaKarariIcerik
				.setSatinalmaSatinalmaKarari(satinalmaSatinalmaKarariForm);
		satinalmaSatinalmaKarariIcerik.setEklemeZamani(UtilInsCore
				.getTarihZaman());
		satinalmaSatinalmaKarariIcerik.setEkleyenKullanici(userBean.getUser()
				.getId());

		for (int i = 0; i < satinalmaTeklifIstemeIcerikList.size(); i++) {
			satinalmaSatinalmaKarariIcerik.setId(null);
			satinalmaSatinalmaKarariIcerik
					.setSatinalmaTeklifIstemeIcerik(satinalmaTeklifIstemeIcerikList
							.get(i));
			satinalmaSatinalmaKarariIcerik
					.setSatinalmaTeklifIsteme(satinalmaTeklifIstemeIcerikList
							.get(i).getSatinalmaTeklifIsteme());
			satinalmaSatinalmaKarariIcerik
					.setIlkTeklifBirimFiyat(satinalmaTeklifIstemeIcerikList
							.get(i).getBirimFiyat());
			try {
				sskim.enterNew(satinalmaSatinalmaKarariIcerik);
			} catch (Exception e) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"SATINALMA KARARI İCERİKLERİ OLUŞTURULAMADI, HATA!",
						null));
			}
		}
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				"SATINALMA KARARI İÇERİKLERİ OLUŞTURULDU!", null));
	}

	public void siparisMektubuOlustur() {

		satinalmaSiparisMektubuBean.addFromKarar(satinalmaSatinalmaKarariForm);

	}

	public void addUploadedFile(FileUploadEvent event)
			throws AbortProcessingException, IOException {
		System.out.println("satinalmaSatinalmaKarariBean.addUploadedFile()");

		try {
			String name = this.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				satinalmaSatinalmaKarariForm
						.setDocuments(satinalmaSatinalmaKarariForm
								.getDocuments() + "//" + name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				SatinalmaSatinalmaKarariManager manager = new SatinalmaSatinalmaKarariManager();
				manager.updateEntity(satinalmaSatinalmaKarariForm);

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				FacesContextUtils.addWarnMessage("salesFileUploadedMessage");
			} else {
				System.out.println("File already exists.");

				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("satinalmaSatinalmaKarariBean: " + e.toString());
		}
	}

	public void deleteTheSelectedFile() {
		System.out
				.println("satinalmaSatinalmaKarariBean.deleteTheSelectedFile()");
		try {
			// Delete the uploaded file
			theFile = new File(path);
			fileArray = theFile.listFiles();

			System.out.println("document that will be deleted: " + privatePath
					+ toBeDeleted);
			for (int j = 0; j < fileArray.length; j++) {
				System.out.println("fileArray[j].toString(): "
						+ fileArray[j].toString());
				if (fileArray[j].toString().contains(privatePath + toBeDeleted)) {
					fileArray[j].delete();

					if (satinalmaSatinalmaKarariForm.getDocuments()
							.contentEquals("//" + toBeDeleted)) {
						satinalmaSatinalmaKarariForm
								.setDocuments(satinalmaSatinalmaKarariForm
										.getDocuments().replace(
												"//" + toBeDeleted, "null"));
					} else {
						satinalmaSatinalmaKarariForm
								.setDocuments(satinalmaSatinalmaKarariForm
										.getDocuments().replace(
												"//" + toBeDeleted, ""));
					}
					FacesContextUtils
							.addWarnMessage("salesTheFileIsDeletedMessage");
				}
			}
		} catch (Exception e) {
			System.out.println("satinalmaSatinalmaKarariBean: " + e.toString());
		}
	}

	public void deleteAndUpload(FileUploadEvent event)
			throws AbortProcessingException, IOException {

		System.out.println("satinalmaSatinalmaKarari.deleteAndUpload()");

		try {
			// Delete the uploaded file
			theFile = new File(path);
			fileArray = theFile.listFiles();

			for (int i = 0; i < satinalmaSatinalmaKarariForm.getFileNames()
					.size(); i++) {
				for (int j = 0; j < fileArray.length; j++) {
					System.out.println("fileArray[j].toString(): "
							+ fileArray[j].toString());
					if (fileArray[j].toString().contains(
							privatePath
									+ satinalmaSatinalmaKarariForm
											.getFileNames().get(i))) {
						fileArray[j].delete();
					}
				}
			}

			String name = this.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				satinalmaSatinalmaKarariForm.setDocuments("//" + name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				SatinalmaSatinalmaKarariManager manager = new SatinalmaSatinalmaKarariManager();
				manager.updateEntity(satinalmaSatinalmaKarariForm);

				FacesContextUtils.addInfoMessage("UpdateItemMessage");
			} else {
				System.out.println("File already exists.");

				satinalmaSatinalmaKarariForm.setDocuments(null);
				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("satinalmaSatinalmaKarariBean: " + e.toString());
		}
	}

	public String checkFileName(String fileName) {

		fileName = fileName.replace(" ", "_");
		fileName = fileName.replace("ç", "c");
		fileName = fileName.replace("Ç", "C");
		fileName = fileName.replace("ğ", "g");
		fileName = fileName.replace("Ğ", "G");
		fileName = fileName.replace("İ", "i");
		fileName = fileName.replace("ı", "i");
		fileName = fileName.replace("ö", "o");
		fileName = fileName.replace("Ö", "O");
		fileName = fileName.replace("ş", "s");
		fileName = fileName.replace("Ş", "S");
		fileName = fileName.replace("ü", "u");
		fileName = fileName.replace("Ü", "U");

		return fileName;
	}

	// setters getters

	public List<SatinalmaSatinalmaKarari> getAllSatinalmaSatinalmaKarariList() {
		return allSatinalmaSatinalmaKarariList;
	}

	public void setAllSatinalmaSatinalmaKarariList(
			List<SatinalmaSatinalmaKarari> allSatinalmaSatinalmaKarariList) {
		this.allSatinalmaSatinalmaKarariList = allSatinalmaSatinalmaKarariList;
	}

	public SatinalmaSatinalmaKarari getSatinalmaSatinalmaKarariForm() {
		return satinalmaSatinalmaKarariForm;
	}

	public void setSatinalmaSatinalmaKarariForm(
			SatinalmaSatinalmaKarari satinalmaSatinalmaKarariForm) {
		this.satinalmaSatinalmaKarariForm = satinalmaSatinalmaKarariForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the satinalmaTakipDosyalarIcerikList
	 */
	public List<SatinalmaTakipDosyalarIcerik> getSatinalmaTakipDosyalarIcerikList() {
		return satinalmaTakipDosyalarIcerikList;
	}

	/**
	 * @param satinalmaTakipDosyalarIcerikList
	 *            the satinalmaTakipDosyalarIcerikList to set
	 */
	public void setSatinalmaTakipDosyalarIcerikList(
			List<SatinalmaTakipDosyalarIcerik> satinalmaTakipDosyalarIcerikList) {
		this.satinalmaTakipDosyalarIcerikList = satinalmaTakipDosyalarIcerikList;
	}

	/**
	 * @return the satinalmaTeklifIstemeIcerikList
	 */
	public List<SatinalmaTeklifIstemeIcerik> getSatinalmaTeklifIstemeIcerikList() {
		return satinalmaTeklifIstemeIcerikList;
	}

	/**
	 * @param satinalmaTeklifIstemeIcerikList
	 *            the satinalmaTeklifIstemeIcerikList to set
	 */
	public void setSatinalmaTeklifIstemeIcerikList(
			List<SatinalmaTeklifIstemeIcerik> satinalmaTeklifIstemeIcerikList) {
		this.satinalmaTeklifIstemeIcerikList = satinalmaTeklifIstemeIcerikList;
	}

	/**
	 * @return the satinalmaSiparisMektubuBean
	 */
	public SatinalmaSiparisMektubuBean getSatinalmaSiparisMektubuBean() {
		return satinalmaSiparisMektubuBean;
	}

	/**
	 * @param satinalmaSiparisMektubuBean
	 *            the satinalmaSiparisMektubuBean to set
	 */
	public void setSatinalmaSiparisMektubuBean(
			SatinalmaSiparisMektubuBean satinalmaSiparisMektubuBean) {
		this.satinalmaSiparisMektubuBean = satinalmaSiparisMektubuBean;
	}

	/**
	 * @return the downloadedFileName
	 */
	public String getDownloadedFileName() {
		return downloadedFileName;
	}

	/**
	 * @param downloadedFileName
	 *            the downloadedFileName to set
	 */
	public void setDownloadedFileName(String downloadedFileName) {
		this.downloadedFileName = downloadedFileName;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path
	 *            the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the toBeDeleted
	 */
	public String getToBeDeleted() {
		return toBeDeleted;
	}

	/**
	 * @param toBeDeleted
	 *            the toBeDeleted to set
	 */
	public void setToBeDeleted(String toBeDeleted) {
		this.toBeDeleted = toBeDeleted;
	}

	@SuppressWarnings("resource")
	public StreamedContent getDownloadedFile() {
		System.out.println("satinalmaSatinalmaKarariBean.getDownloadedFile()");

		try {
			if (downloadedFileName != null) {
				RandomAccessFile accessFile = new RandomAccessFile(path
						+ downloadedFileName, "r");
				byte[] downloadByte = new byte[(int) accessFile.length()];
				accessFile.read(downloadByte);
				InputStream stream = new ByteArrayInputStream(downloadByte);

				String suffix = FilenameUtils.getExtension(downloadedFileName);
				String contentType = "text/plain";
				if (suffix.contentEquals("jpg") || suffix.contentEquals("png")
						|| suffix.contentEquals("gif")) {
					contentType = "image/" + contentType;
				} else if (suffix.contentEquals("doc")
						|| suffix.contentEquals("docx")) {
					contentType = "application/msword";
				} else if (suffix.contentEquals("xls")
						|| suffix.contentEquals("xlsx")) {
					contentType = "application/vnd.ms-excel";
				} else if (suffix.contentEquals("pdf")) {
					contentType = "application/pdf";
				}
				downloadedFile = new DefaultStreamedContent(stream,
						"contentType", downloadedFileName);

				stream.close();
			}
		} catch (Exception e) {
			System.out
					.println("satinalmaSatinalmaKarariBean.getDownloadedFile():"
							+ e.toString());
		}
		return downloadedFile;
	}

	/**
	 * @param downloadedFile
	 *            the downloadedFile to set
	 */
	public void setDownloadedFile(StreamedContent downloadedFile) {
		this.downloadedFile = downloadedFile;
	}

}
