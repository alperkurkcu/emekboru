/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaFlowCoatCureSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaFlowCoatCureSonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testKaplamaFlowCoatCureSonucBean")
@ViewScoped
public class TestKaplamaFlowCoatCureSonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<TestKaplamaFlowCoatCureSonuc> allKaplamaFlowCoatCureSonucList = new ArrayList<TestKaplamaFlowCoatCureSonuc>();
	private TestKaplamaFlowCoatCureSonuc testKaplamaFlowCoatCureSonucForm = new TestKaplamaFlowCoatCureSonuc();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;
	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addKaplamaFlowCoatCureSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaFlowCoatCureSonucManager tkdsManager = new TestKaplamaFlowCoatCureSonucManager();

		if (testKaplamaFlowCoatCureSonucForm.getId() == null) {

			testKaplamaFlowCoatCureSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaFlowCoatCureSonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaFlowCoatCureSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaFlowCoatCureSonucForm.setBagliTestId(bagliTest);
			testKaplamaFlowCoatCureSonucForm.setBagliGlobalId(bagliTest);

			tkdsManager.enterNew(testKaplamaFlowCoatCureSonucForm);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaFlowCoatCureSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaFlowCoatCureSonucForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			tkdsManager.updateEntity(testKaplamaFlowCoatCureSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");

		}
		fillTestList(prmGlobalId, prmPipeId);
	}

	public void addKaplamaFlowCoatCure() {
		testKaplamaFlowCoatCureSonucForm = new TestKaplamaFlowCoatCureSonuc();
		updateButtonRender = false;
	}

	public void kaplamaFlowCoatCureListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(int globalId, Integer pipeId2) {
		allKaplamaFlowCoatCureSonucList = TestKaplamaFlowCoatCureSonucManager
				.getAllTestKaplamaFlowCoatCureSonuc(globalId, pipeId2);
		testKaplamaFlowCoatCureSonucForm = new TestKaplamaFlowCoatCureSonuc();
	}

	public void deleteTestKaplamaFlowCoatCureSonuc() {

		bagliTestId = testKaplamaFlowCoatCureSonucForm.getBagliTestId().getId();

		if (testKaplamaFlowCoatCureSonucForm == null
				|| testKaplamaFlowCoatCureSonucForm.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaFlowCoatCureSonucManager tkdsManager = new TestKaplamaFlowCoatCureSonucManager();
		tkdsManager.delete(testKaplamaFlowCoatCureSonucForm);
		testKaplamaFlowCoatCureSonucForm = new TestKaplamaFlowCoatCureSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setters getters
	/**
	 * @return the allKaplamaFlowCoatCureSonucList
	 */
	public List<TestKaplamaFlowCoatCureSonuc> getAllKaplamaFlowCoatCureSonucList() {
		return allKaplamaFlowCoatCureSonucList;
	}

	/**
	 * @param allKaplamaFlowCoatCureSonucList
	 *            the allKaplamaFlowCoatCureSonucList to set
	 */
	public void setAllKaplamaFlowCoatCureSonucList(
			List<TestKaplamaFlowCoatCureSonuc> allKaplamaFlowCoatCureSonucList) {
		this.allKaplamaFlowCoatCureSonucList = allKaplamaFlowCoatCureSonucList;
	}

	/**
	 * @return the testKaplamaFlowCoatCureSonucForm
	 */
	public TestKaplamaFlowCoatCureSonuc getTestKaplamaFlowCoatCureSonucForm() {
		return testKaplamaFlowCoatCureSonucForm;
	}

	/**
	 * @param testKaplamaFlowCoatCureSonucForm
	 *            the testKaplamaFlowCoatCureSonucForm to set
	 */
	public void setTestKaplamaFlowCoatCureSonucForm(
			TestKaplamaFlowCoatCureSonuc testKaplamaFlowCoatCureSonucForm) {
		this.testKaplamaFlowCoatCureSonucForm = testKaplamaFlowCoatCureSonucForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
