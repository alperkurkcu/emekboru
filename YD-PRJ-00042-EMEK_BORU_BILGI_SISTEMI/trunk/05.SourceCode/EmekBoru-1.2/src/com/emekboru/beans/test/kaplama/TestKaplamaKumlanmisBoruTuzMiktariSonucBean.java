package com.emekboru.beans.test.kaplama;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaKumlanmisBoruTuzMiktariSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaKumlanmisBoruTuzMiktariSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaplamaKumlanmisBoruTuzMiktariSonucBean")
@ViewScoped
public class TestKaplamaKumlanmisBoruTuzMiktariSonucBean {

	private TestKaplamaKumlanmisBoruTuzMiktariSonuc testKaplamaKumlanmisBoruTuzMiktariSonucForm = new TestKaplamaKumlanmisBoruTuzMiktariSonuc();
	private List<TestKaplamaKumlanmisBoruTuzMiktariSonuc> allTestKaplamaKumlanmisBoruTuzMiktariSonucList = new ArrayList<TestKaplamaKumlanmisBoruTuzMiktariSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateTestKaplamaKumlanmisBoruTuzMiktariSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaKumlanmisBoruTuzMiktariSonucManager manager = new TestKaplamaKumlanmisBoruTuzMiktariSonucManager();

		if (testKaplamaKumlanmisBoruTuzMiktariSonucForm.getId() == null) {

			testKaplamaKumlanmisBoruTuzMiktariSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaKumlanmisBoruTuzMiktariSonucForm
					.setEkleyenKullanici(userBean.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaKumlanmisBoruTuzMiktariSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaKumlanmisBoruTuzMiktariSonucForm
					.setBagliTestId(bagliTest);
			testKaplamaKumlanmisBoruTuzMiktariSonucForm
					.setBagliGlobalId(bagliTest);

			manager.enterNew(testKaplamaKumlanmisBoruTuzMiktariSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaKumlanmisBoruTuzMiktariSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaKumlanmisBoruTuzMiktariSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			manager.updateEntity(testKaplamaKumlanmisBoruTuzMiktariSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addTestKaplamaKumlanmisBoruTuzMiktariSonuc() {

		testKaplamaKumlanmisBoruTuzMiktariSonucForm = new TestKaplamaKumlanmisBoruTuzMiktariSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaKumlanmisBoruTuzMiktariSonucListener(
			SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaKumlanmisBoruTuzMiktariSonucList = TestKaplamaKumlanmisBoruTuzMiktariSonucManager
				.getAllTestKaplamaKumlanmisBoruTuzMiktariSonuc(globalId, pipeId);
		testKaplamaKumlanmisBoruTuzMiktariSonucForm = new TestKaplamaKumlanmisBoruTuzMiktariSonuc();
	}

	public void deleteTestKaplamaKumlanmisBoruTuzMiktariSonuc() {

		bagliTestId = testKaplamaKumlanmisBoruTuzMiktariSonucForm
				.getBagliGlobalId().getId();

		if (testKaplamaKumlanmisBoruTuzMiktariSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaKumlanmisBoruTuzMiktariSonucManager manager = new TestKaplamaKumlanmisBoruTuzMiktariSonucManager();
		manager.delete(testKaplamaKumlanmisBoruTuzMiktariSonucForm);
		testKaplamaKumlanmisBoruTuzMiktariSonucForm = new TestKaplamaKumlanmisBoruTuzMiktariSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters

	/**
	 * @return the testKaplamaKumlanmisBoruTuzMiktariSonucForm
	 */
	public TestKaplamaKumlanmisBoruTuzMiktariSonuc getTestKaplamaKumlanmisBoruTuzMiktariSonucForm() {
		return testKaplamaKumlanmisBoruTuzMiktariSonucForm;
	}

	/**
	 * @param testKaplamaKumlanmisBoruTuzMiktariSonucForm
	 *            the testKaplamaKumlanmisBoruTuzMiktariSonucForm to set
	 */
	public void setTestKaplamaKumlanmisBoruTuzMiktariSonucForm(
			TestKaplamaKumlanmisBoruTuzMiktariSonuc testKaplamaKumlanmisBoruTuzMiktariSonucForm) {
		this.testKaplamaKumlanmisBoruTuzMiktariSonucForm = testKaplamaKumlanmisBoruTuzMiktariSonucForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the allTestKaplamaKumlanmisBoruTuzMiktariSonucList
	 */
	public List<TestKaplamaKumlanmisBoruTuzMiktariSonuc> getAllTestKaplamaKumlanmisBoruTuzMiktariSonucList() {
		return allTestKaplamaKumlanmisBoruTuzMiktariSonucList;
	}

	/**
	 * @param allTestKaplamaKumlanmisBoruTuzMiktariSonucList
	 *            the allTestKaplamaKumlanmisBoruTuzMiktariSonucList to set
	 */
	public void setAllTestKaplamaKumlanmisBoruTuzMiktariSonucList(
			List<TestKaplamaKumlanmisBoruTuzMiktariSonuc> allTestKaplamaKumlanmisBoruTuzMiktariSonucList) {
		this.allTestKaplamaKumlanmisBoruTuzMiktariSonucList = allTestKaplamaKumlanmisBoruTuzMiktariSonucList;
	}
}
