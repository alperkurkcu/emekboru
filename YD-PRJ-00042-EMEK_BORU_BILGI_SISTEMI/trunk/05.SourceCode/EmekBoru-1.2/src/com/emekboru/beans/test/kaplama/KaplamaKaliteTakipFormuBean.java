/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.BoruKaliteKaplamaTurleri;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiBeton;
import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiEpoksi;
import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiPolietilen;
import com.emekboru.jpa.kaplamatest.KaplamaKaliteTakipFormu;
import com.emekboru.jpaman.BoruKaliteKaplamaTurleriManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.kaplamamakinesi.KaplamaMakinesiBetonManager;
import com.emekboru.jpaman.kaplamamakinesi.KaplamaMakinesiEpoksiManager;
import com.emekboru.jpaman.kaplamamakinesi.KaplamaMakinesiPolietilenManager;
import com.emekboru.jpaman.kaplamatest.KaplamaKaliteTakipFormuManager;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "kaplamaKaliteTakipFormuBean")
@ViewScoped
public class KaplamaKaliteTakipFormuBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private List<KaplamaKaliteTakipFormu> allKaplamaKaliteTakipFormuList = new ArrayList<KaplamaKaliteTakipFormu>();
	private KaplamaKaliteTakipFormu kaplamaKaliteTakipFormuForm = new KaplamaKaliteTakipFormu();

	private BoruKaliteKaplamaTurleri boruKaliteKaplamaTurleri = new BoruKaliteKaplamaTurleri();
	private KaplamaMakinesiPolietilen kaplamaMakinesiPolietilen = new KaplamaMakinesiPolietilen();
	private KaplamaMakinesiEpoksi kaplamaMakinesiEpoksi = new KaplamaMakinesiEpoksi();
	private KaplamaMakinesiBeton kaplamaMakinesiBeton = new KaplamaMakinesiBeton();

	private Pipe selectedPipe = new Pipe();

	private String barkodNo = null;

	private boolean updateButtonRender;
	private Integer icKaplamaRender;
	private Integer disKaplamaRender;
	private boolean tabRender;

	public KaplamaKaliteTakipFormuBean() {

		updateButtonRender = false;
		icKaplamaRender = 0;
		disKaplamaRender = 0;
		tabRender = false;
		this.kaplamaKaliteTakipFormuForm = new KaplamaKaliteTakipFormu();
	}

	public void addOrUpdateSonuc(ActionEvent e) {

		KaplamaKaliteTakipFormuManager kktfManager = new KaplamaKaliteTakipFormuManager();

		if (kaplamaKaliteTakipFormuForm.getId() == null) {

			try {

				kaplamaKaliteTakipFormuForm.setEklemeZamani(UtilInsCore
						.getTarihZaman());
				kaplamaKaliteTakipFormuForm.setEkleyenKullanici(userBean
						.getUser().getId());
				kaplamaKaliteTakipFormuForm.setPipe(selectedPipe);

				kktfManager.enterNew(kaplamaKaliteTakipFormuForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								"KAPLAMA KALİTE TAKİP FORMU VERİSİ BAŞARIYLA EKLENMİŞTİR!"));

			} catch (Exception e2) {

				System.out
						.println("kaplamaKaliteTakipFormuBean.addOrUpdateSonuc-HATA EKLEME");
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								"KAPLAMA KALİTE TAKİP FORMU VERİSİ EKLENEMEDİ, TEKRAR DENEYİNİZ!"));
				return;
			}

		} else {

			try {

				kaplamaKaliteTakipFormuForm.setGuncellemeZamani(UtilInsCore
						.getTarihZaman());
				kaplamaKaliteTakipFormuForm.setGuncelleyenKullanici(userBean
						.getUser().getId());

				kktfManager.updateEntity(kaplamaKaliteTakipFormuForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								"KAPLAMA KALİTE TAKİP FORMU VERİSİ BAŞARIYLA GÜNCELLENMİŞTİR!"));

			} catch (Exception e2) {

				System.err
						.println("kaplamaKaliteTakipFormuBean.addOrUpdateSonuc-HATA GUNCELLEME");
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								"KAPLAMA KALİTE TAKİP FORMU VERİSİ GÜNCELLENEMEMİŞTİR, TEKRAR DENEYİNİZ!"));
				return;
			}
		}
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!", null));
			tabRender = false;
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			KaplamaKaliteTakipFormuManager kktfManager = new KaplamaKaliteTakipFormuManager();
			allKaplamaKaliteTakipFormuList = kktfManager
					.getAllKaplamaKaliteTakipFormu(selectedPipe.getPipeId());
			kktfManager.refreshCollection(allKaplamaKaliteTakipFormuList);

			if (allKaplamaKaliteTakipFormuList.size() > 0) {
				kaplamaKaliteTakipFormuForm = allKaplamaKaliteTakipFormuList
						.get(0);
			} else {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								prmBarkod
										+ " BARKOD NUMARALI BORU KAPLAMA KALİTE TAKİBİ YOKTUR!"));
				tabRender = false;
			}

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));

			tabRender();

		}
	}

	@SuppressWarnings("static-access")
	public void tabRender() {

		BoruKaliteKaplamaTurleriManager bktManager = new BoruKaliteKaplamaTurleriManager();
		// kaplama kalite kontrolu
		if (bktManager.getAllBoruKaliteKaplamaTurleri(
				selectedPipe.getSalesItem().getItemId()).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"SEÇİLİ BORUNUN KAPLAMA KALİTESİ GİRİLMEMİŞTİR, LÜTFEN KALİTE VERİLERİNİ TAMAMLAYINIZ!",
							null));
			tabRender = false;
			return;
		} else {
			boruKaliteKaplamaTurleri = bktManager
					.getAllBoruKaliteKaplamaTurleri(
							selectedPipe.getSalesItem().getItemId()).get(0);
			// tab render oluşturma
			kaplamaKalite();
		}
	}

	@SuppressWarnings("static-access")
	public void kaplamaKalite() {

		KaplamaMakinesiPolietilenManager poliManager = new KaplamaMakinesiPolietilenManager();
		KaplamaMakinesiEpoksiManager epxManager = new KaplamaMakinesiEpoksiManager();
		KaplamaMakinesiBetonManager betonManager = new KaplamaMakinesiBetonManager();
		if (boruKaliteKaplamaTurleri.getDisPolietilen()) {
			if (poliManager.getAllKaplamaMakinesiPolietilen(
					selectedPipe.getPipeId()).size() == 0) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"SEÇİLİ BORUNUN DIŞ POLİETİLEN KAPLAMA İŞLEMİ BİTMEMİŞTİR, LÜTFEN KAPLAMA İŞLEMİNİ TAMAMLAYINIZ!",
								null));
				tabRender = false;
				// return;
			} else {
				kaplamaMakinesiPolietilen = poliManager
						.getAllKaplamaMakinesiPolietilen(
								selectedPipe.getPipeId()).get(0);
				disKaplamaRender = 1;
				tabRender = true;
			}
		}
		if (boruKaliteKaplamaTurleri.getDisBoya()) {

			if (epxManager
					.getAllKaplamaMakinesiEpoksi(selectedPipe.getPipeId())
					.size() == 0) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"SEÇİLİ BORUNUN DIŞ BOYA KAPLAMA (EPOKSİ) İŞLEMİ BİTMEMİŞTİR, LÜTFEN KAPLAMA İŞLEMİNİ TAMAMLAYINIZ!",
								null));
				tabRender = true;
				// return;
			} else {
				kaplamaMakinesiEpoksi = epxManager.getAllKaplamaMakinesiEpoksi(
						selectedPipe.getPipeId()).get(0);
				disKaplamaRender = 2;
				tabRender = true;
			}
		}
		if (boruKaliteKaplamaTurleri.getIcBoya()) {

			if (epxManager
					.getAllKaplamaMakinesiEpoksi(selectedPipe.getPipeId())
					.size() == 0) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"SEÇİLİ BORUNUN İÇ BOYA KAPLAMA (EPOKSİ) İŞLEMİ BİTMEMİŞTİR, LÜTFEN KAPLAMA İŞLEMİNİ TAMAMLAYINIZ!",
								null));
				tabRender = true;
				// return;
			} else {
				kaplamaMakinesiEpoksi = epxManager.getAllKaplamaMakinesiEpoksi(
						selectedPipe.getPipeId()).get(0);
				icKaplamaRender = 1;
				tabRender = true;
			}
		}
		if (boruKaliteKaplamaTurleri.getIcBeton()) {

			if (betonManager.getAllKaplamaMakinesiBeton(
					selectedPipe.getPipeId()).size() == 0) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"SEÇİLİ BORUNUN BETON KAPLAMA İŞLEMİ BİTMEMİŞTİR, LÜTFEN KAPLAMA İŞLEMİNİ TAMAMLAYINIZ!",
								null));
				tabRender = true;
				// return;
			} else {
				kaplamaMakinesiBeton = betonManager.getAllKaplamaMakinesiBeton(
						selectedPipe.getPipeId()).get(0);
				icKaplamaRender = 2;
				tabRender = true;
			}
		}

		if (icKaplamaRender == 0 && disKaplamaRender == 0) {
			tabRender = false;
		}
	}

	// setters getters
	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<KaplamaKaliteTakipFormu> getAllKaplamaKaliteTakipFormuList() {
		return allKaplamaKaliteTakipFormuList;
	}

	public void setAllKaplamaKaliteTakipFormuList(
			List<KaplamaKaliteTakipFormu> allKaplamaKaliteTakipFormuList) {
		this.allKaplamaKaliteTakipFormuList = allKaplamaKaliteTakipFormuList;
	}

	public KaplamaKaliteTakipFormu getKaplamaKaliteTakipFormuForm() {
		return kaplamaKaliteTakipFormuForm;
	}

	public void setKaplamaKaliteTakipFormuForm(
			KaplamaKaliteTakipFormu kaplamaKaliteTakipFormuForm) {
		this.kaplamaKaliteTakipFormuForm = kaplamaKaliteTakipFormuForm;
	}

	public String getBarkodNo() {
		return barkodNo;
	}

	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public Integer getIcKaplamaRender() {
		return icKaplamaRender;
	}

	public void setIcKaplamaRender(Integer icKaplamaRender) {
		this.icKaplamaRender = icKaplamaRender;
	}

	public Integer getDisKaplamaRender() {
		return disKaplamaRender;
	}

	public void setDisKaplamaRender(Integer disKaplamaRender) {
		this.disKaplamaRender = disKaplamaRender;
	}

	public boolean isTabRender() {
		return tabRender;
	}

	public void setTabRender(boolean tabRender) {
		this.tabRender = tabRender;
	}

	public KaplamaMakinesiPolietilen getKaplamaMakinesiPolietilen() {
		return kaplamaMakinesiPolietilen;
	}

	public void setKaplamaMakinesiPolietilen(
			KaplamaMakinesiPolietilen kaplamaMakinesiPolietilen) {
		this.kaplamaMakinesiPolietilen = kaplamaMakinesiPolietilen;
	}

	public KaplamaMakinesiEpoksi getKaplamaMakinesiEpoksi() {
		return kaplamaMakinesiEpoksi;
	}

	public void setKaplamaMakinesiEpoksi(
			KaplamaMakinesiEpoksi kaplamaMakinesiEpoksi) {
		this.kaplamaMakinesiEpoksi = kaplamaMakinesiEpoksi;
	}

	public KaplamaMakinesiBeton getKaplamaMakinesiBeton() {
		return kaplamaMakinesiBeton;
	}

	public void setKaplamaMakinesiBeton(
			KaplamaMakinesiBeton kaplamaMakinesiBeton) {
		this.kaplamaMakinesiBeton = kaplamaMakinesiBeton;
	}

}
