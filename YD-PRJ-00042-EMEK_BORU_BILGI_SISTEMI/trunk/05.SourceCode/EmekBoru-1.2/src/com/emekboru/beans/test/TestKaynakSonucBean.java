package com.emekboru.beans.test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.sales.order.SalesOrderBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.DestructiveTestsSpec;
import com.emekboru.jpa.EdwPipeLink;
import com.emekboru.jpa.MachineEdwLink;
import com.emekboru.jpa.MachinePipeLink;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.TestKaynakSonuc;
import com.emekboru.jpaman.DestructiveTestsSpecManager;
import com.emekboru.jpaman.EdwPipeLinkManager;
import com.emekboru.jpaman.MachineEdwLinkManager;
import com.emekboru.jpaman.MachinePipeLinkManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.TestKaynakSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaynakSonucBean")
@ViewScoped
public class TestKaynakSonucBean {

	private List<TestKaynakSonuc> allKaynakSonucList = new ArrayList<TestKaynakSonuc>();
	private TestKaynakSonuc testKaynakSonucForm = new TestKaynakSonuc();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;
	private boolean updateButtonRender;

	Integer prmGlobalId = null;
	Integer prmTestId = null;
	Integer prmPipeId = null;

	private Pipe pipe = null;
	private DestructiveTestsSpec bagliTest = null;
	private String sampleNo = null;

	private List<MachinePipeLink> machinePipeLink = new ArrayList<MachinePipeLink>();
	private List<MachineEdwLink> machineEdwLink = new ArrayList<MachineEdwLink>();
	private List<EdwPipeLink> edwPipeLink = new ArrayList<EdwPipeLink>();

	public void sampleNoOlustur() {
		Integer count = 0;
		TestKaynakSonucManager manager = new TestKaynakSonucManager();
		count = manager.getAllTestCekmeSonuc(prmGlobalId, prmPipeId).size() + 1;
		sampleNo = bagliTest.getCode() + pipe.getPipeIndex() + "-" + count;
	}

	private void kaynakParametreleriBul() {

		MachinePipeLinkManager machinePipeLinkManager = new MachinePipeLinkManager();
		MachineEdwLinkManager machineEdwLinkManager = new MachineEdwLinkManager();
		EdwPipeLinkManager edwPipeLinkManager = new EdwPipeLinkManager();

		machinePipeLink = machinePipeLinkManager.findByPipeId(pipe.getPipeId());
		edwPipeLink = edwPipeLinkManager.findByPrmPipeId(pipe.getPipeId());
	}

	// methods

	@SuppressWarnings("unused")
	public void addKaynakTestSonuc(ActionEvent e) {
		UICommand cmd = (UICommand) e.getComponent();
		prmGlobalId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");
		prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");
		Boolean testM01Render = (Boolean) cmd.getChildren().get(3)
				.getAttributes().get("value");

		TestKaynakSonucManager tksManager = new TestKaynakSonucManager();

		if (testKaynakSonucForm.getId() == null) {

			testKaynakSonucForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			testKaynakSonucForm.setEkleyenKullanici(userBean.getUser().getId());

			pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaynakSonucForm.setPipe(pipe);

			bagliTest = new DestructiveTestsSpecManager().loadObject(
					DestructiveTestsSpec.class, prmTestId);
			testKaynakSonucForm.setBagliTestId(bagliTest);
			testKaynakSonucForm.setBagliGlobalId(bagliTest);

			sampleNoOlustur();
			testKaynakSonucForm.setSampleNo(sampleNo);

			kaynakParametreleriBul();

			tksManager.enterNew(testKaynakSonucForm);

			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 2, true);
			DestructiveTestsSpecManager desMan = new DestructiveTestsSpecManager();
			desMan.testSonucKontrol(prmTestId, true);

			fillTestList(prmGlobalId, prmPipeId);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaynakSonucForm.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			testKaynakSonucForm.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			tksManager.updateEntity(testKaynakSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		TestKaynakSonucManager manager = new TestKaynakSonucManager();
		allKaynakSonucList = manager.getAllTestCekmeSonuc(prmGlobalId,
				prmPipeId);

	}

	public void addKaynakTest() {
		testKaynakSonucForm = new TestKaynakSonuc();
	}

	public void kaynakListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(int prmGlobalId, Integer prmPipeId) {

		TestKaynakSonucManager manager = new TestKaynakSonucManager();
		allKaynakSonucList = manager.getAllTestCekmeSonuc(prmGlobalId,
				prmPipeId);

	}

	public void deleteTestKaynakSonuc() {

		TestKaynakSonucManager tksManager = new TestKaynakSonucManager();

		if (testKaynakSonucForm == null || testKaynakSonucForm.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		tksManager.delete(testKaynakSonucForm);

		// sonuc var mı false
		DestructiveTestsSpecManager desMan = new DestructiveTestsSpecManager();
		desMan.testSonucKontrol(testKaynakSonucForm.getBagliTestId()
				.getTestId(), false);

		testKaynakSonucForm = new TestKaynakSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");

	}

	// document upload

	@EJB
	private ConfigBean config;

	private String path;
	private String privatePath;

	private File theFile = null;
	OutputStream output = null;
	InputStream input = null;

	@SuppressWarnings("unused")
	private File[] fileArray;

	private StreamedContent downloadedFile;
	private String downloadedFileName;

	SalesOrderBean sob = new SalesOrderBean();

	public TestKaynakSonucBean() {

		privatePath = File.separatorChar + "testDocs" + File.separatorChar
				+ "uploadedDocuments" + File.separatorChar + "kaynakSonuc"
				+ File.separatorChar;
	}

	@PostConstruct
	public void load() {
		System.out.println("TestKaynakSonucBean.load()");
		try {

			this.setPath("/home/emekboru/Desktop/emekfiles" + privatePath);

			System.out.println("Path for Upload File (TestKaynakSonuc):			"
					+ path);
		} catch (Exception ex) {
			System.out.println("Path for Upload File (TestKaynakSonuc):			"
					+ FacesContext.getCurrentInstance().getExternalContext()
							.getRealPath(privatePath));

			System.out.print(path);
			System.out.println("CAUSE OF " + ex.toString());
		}

		theFile = new File(path);
		if (!theFile.isDirectory()) {
			theFile.mkdirs();
		}

		fileArray = theFile.listFiles();

	}

	public void addUploadedFile(FileUploadEvent event)
			throws AbortProcessingException, IOException {
		System.out.println("TestKaynakSonucBean.addUploadedFile()");

		try {
			String name = sob.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				testKaynakSonucForm.setDocuments(testKaynakSonucForm
						.getDocuments() + "//" + name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				TestKaynakSonucManager manager = new TestKaynakSonucManager();
				manager.updateEntity(testKaynakSonucForm);

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				FacesContextUtils.addWarnMessage("salesFileUploadedMessage");
			} else {
				System.out.println("File already exists.");

				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("TestKaynakSonucBean: " + e.toString());
		}
	}

	@SuppressWarnings("resource")
	public StreamedContent getDownloadedFile() {
		System.out.println("TestKaynakSonucBean.getDownloadedFile()");

		try {
			if (downloadedFileName != null) {
				RandomAccessFile accessFile = new RandomAccessFile(path
						+ downloadedFileName, "r");
				byte[] downloadByte = new byte[(int) accessFile.length()];
				accessFile.read(downloadByte);
				InputStream stream = new ByteArrayInputStream(downloadByte);

				String suffix = FilenameUtils.getExtension(downloadedFileName);
				String contentType = "text/plain";
				if (suffix.contentEquals("jpg") || suffix.contentEquals("png")
						|| suffix.contentEquals("gif")) {
					contentType = "image/" + contentType;
				} else if (suffix.contentEquals("doc")
						|| suffix.contentEquals("docx")) {
					contentType = "application/msword";
				} else if (suffix.contentEquals("xls")
						|| suffix.contentEquals("xlsx")) {
					contentType = "application/vnd.ms-excel";
				} else if (suffix.contentEquals("pdf")) {
					contentType = "application/pdf";
				}
				downloadedFile = new DefaultStreamedContent(stream,
						"contentType", downloadedFileName);

				stream.close();
			}
		} catch (Exception e) {
			System.out.println("TestKaynakSonucBean.getDownloadedFile():"
					+ e.toString());
		}
		return downloadedFile;
	}

	public String getDownloadedFileName() {
		return downloadedFileName;
	}

	public void setDownloadedFileName(String downloadedFileName) {
		try {
			this.downloadedFileName = downloadedFileName;
		} catch (Exception e) {
			System.out
					.println("TestAgirlikDusurmeSonucBean.setDownloadedFileName():"
							+ e.toString());
		}
	}

	public void setPath(String path) {
		this.path = path;
	}

	// document upload

	// setters getters

	public List<TestKaynakSonuc> getAllKaynakSonucList() {
		return allKaynakSonucList;
	}

	public void setAllKaynakSonucList(List<TestKaynakSonuc> allKaynakSonucList) {
		this.allKaynakSonucList = allKaynakSonucList;
	}

	public TestKaynakSonuc getTestKaynakSonucForm() {
		return testKaynakSonucForm;
	}

	public void setTestKaynakSonucForm(TestKaynakSonuc testKaynakSonucForm) {
		this.testKaynakSonucForm = testKaynakSonucForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
