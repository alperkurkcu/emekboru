/**
 * 
 */
package com.emekboru.beans.test.tahribatsiztestsonuc;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.test.tahribatsiz.TahribatsizTestOrderListBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.kalibrasyon.KalibrasyonRadyografik;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizRadyografikSonuc;
import com.emekboru.jpaman.NondestructiveTestsSpecManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.kalibrasyon.KalibrasyonRadyografikManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizRadyografikSonucManager;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */

@ManagedBean(name = "testTahribatsizRadyografikSonucBean")
@ViewScoped
public class TestTahribatsizRadyografikSonucBean {

	private static final long serialVersionUID = 1L;

	private List<TestTahribatsizRadyografikSonuc> allTestTahribatsizRadyografikSonucList = new ArrayList<TestTahribatsizRadyografikSonuc>();
	private TestTahribatsizRadyografikSonuc testTahribatsizRadyografikSonucForm = new TestTahribatsizRadyografikSonuc();

	int index = 0;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	private Pipe selectedPipe = new Pipe();

	private String barkodNo = null;

	private List<KalibrasyonRadyografik> kalibrasyonlar = new ArrayList<KalibrasyonRadyografik>();
	private KalibrasyonRadyografik kalibrasyon = new KalibrasyonRadyografik();

	public void radyografikListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer pipeId) {

		TestTahribatsizRadyografikSonucManager testTahribatsizRadyografikSonucManager = new TestTahribatsizRadyografikSonucManager();
		allTestTahribatsizRadyografikSonucList = testTahribatsizRadyografikSonucManager
				.getAllTestTahribatsizRadyografikSonuc(pipeId);
	}

	public void addTestTahribatsizRadyografikSonuc() {
		testTahribatsizRadyografikSonucForm = new TestTahribatsizRadyografikSonuc();
		updateButtonRender = false;
	}

	@SuppressWarnings({ "static-access", "unused" })
	public void addOrUpdateRadyografikSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();

		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");
		Integer prmItemId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");

		Integer testId = 0;

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}
		if (prmItemId == null) {
			prmItemId = selectedPipe.getSalesItem().getItemId();
		}

		TestTahribatsizRadyografikSonucManager radyografikManager = new TestTahribatsizRadyografikSonucManager();
		NondestructiveTestsSpecManager nonManager = new NondestructiveTestsSpecManager();

		try {
			testId = nonManager.findByItemIdGlobalId(prmItemId, 1038).get(0)
					.getTestId();

		} catch (Exception e2) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"KOORDİNAT EKLEME BAŞARISIZ, KALİTE TANIMLARINDA RADYOGRAFİK TEST EKSİKTİR!",
							null));
			return;
		}

		if (testTahribatsizRadyografikSonucForm.getId() == null) {

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);

			// testTahribatsizRadyografikSonucForm.setTestId(testId);
			testTahribatsizRadyografikSonucForm.setPipe(pipe);

			testTahribatsizRadyografikSonucForm.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			testTahribatsizRadyografikSonucForm.setEkleyenEmployee(userBean
					.getUser());

			sonKalibrasyonuGoster();
			testTahribatsizRadyografikSonucForm.setKalibrasyon(kalibrasyon);

			radyografikManager.enterNew(testTahribatsizRadyografikSonucForm);

			this.testTahribatsizRadyografikSonucForm = new TestTahribatsizRadyografikSonuc();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							"RADYOGRAFİK MUAYENE KOORDİNATI BAŞARI İLE KAYDEDİLMİŞTİR!"));
		} else {

			testTahribatsizRadyografikSonucForm.setGuncellemeZamani(UtilInsCore
					.getTarihZaman());
			testTahribatsizRadyografikSonucForm.setGuncelleyenEmployee(userBean
					.getUser());
			// testTahribatsizRadyografikSonucForm.setTestId(testId);

			radyografikManager
					.updateEntity(testTahribatsizRadyografikSonucForm);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"RADYOGRAFİK MUAYENE BAŞARI İLE GÜNCELLENMİŞTİR!"));
		}

		fillTestList(prmPipeId);
		testTahribatsizRadyografikSonucForm = new TestTahribatsizRadyografikSonuc();
		updateButtonRender = false;
		// sayfada barkoddan okutunca order item pipe seçimi olmadıgı için
		// sıkıntı cıkıyordu
		TahribatsizTestOrderListBean ttolb = new TahribatsizTestOrderListBean();
		ttolb.loadOrderDetails(selectedPipe.getSalesItem().getItemId());
	}

	public void deleteRadyografikSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		TestTahribatsizRadyografikSonucManager radyografikManager = new TestTahribatsizRadyografikSonucManager();

		if (testTahribatsizRadyografikSonucForm.getId() == null) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"LİSTEDEN SEÇİLMİŞ BİR DEĞER YOKTUR!", null));
			return;
		}

		radyografikManager.delete(testTahribatsizRadyografikSonucForm);
		testTahribatsizRadyografikSonucForm = new TestTahribatsizRadyografikSonuc();
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
				"RADYOGRAFİK MUAYENE KOORDİNATI SİLİNMİŞTİR!", null));

		fillTestList(prmPipeId);
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " BARKOD NUMARALI BORU SİSTEMDE YOKTUR!", null));
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			TestTahribatsizRadyografikSonucManager radyografikManager = new TestTahribatsizRadyografikSonucManager();
			allTestTahribatsizRadyografikSonucList = radyografikManager
					.getAllTestTahribatsizRadyografikSonuc(selectedPipe
							.getPipeId());
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
		}

		KalibrasyonRadyografikManager kalibrasyonManager = new KalibrasyonRadyografikManager();
		kalibrasyonlar = kalibrasyonManager.getAllKalibrasyon();
	}

	@SuppressWarnings("static-access")
	public void sonKalibrasyonuGoster() {
		kalibrasyon = new KalibrasyonRadyografik();
		KalibrasyonRadyografikManager kalibrasyonManager = new KalibrasyonRadyografikManager();
		kalibrasyon = kalibrasyonManager.getAllKalibrasyon().get(0);
	}

	public void kalibrasyonGoster() {
		kalibrasyon = new KalibrasyonRadyografik();
		kalibrasyon = testTahribatsizRadyografikSonucForm.getKalibrasyon();
	}

	public void kalibrasyonAta() {
		TestTahribatsizRadyografikSonucManager radyoManager = new TestTahribatsizRadyografikSonucManager();
		try {
			for (int i = 0; i < allTestTahribatsizRadyografikSonucList.size(); i++) {
				allTestTahribatsizRadyografikSonucList.get(i).setKalibrasyon(
						kalibrasyon);
				radyoManager
						.updateEntity(allTestTahribatsizRadyografikSonucList
								.get(i));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"KALİBRASYON ATAMA BAŞARISIZ, TEKRAR DENEYİNİZ!", null));
			return;
		}
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				"KALİBRASYON ATAMA BAŞARILI!", null));
	}

	// setter getter

	/**
	 * @return the allTestTahribatsizRadyografikSonucList
	 */
	public List<TestTahribatsizRadyografikSonuc> getAllTestTahribatsizRadyografikSonucList() {
		return allTestTahribatsizRadyografikSonucList;
	}

	/**
	 * @param allTestTahribatsizRadyografikSonucList
	 *            the allTestTahribatsizRadyografikSonucList to set
	 */
	public void setAllTestTahribatsizRadyografikSonucList(
			List<TestTahribatsizRadyografikSonuc> allTestTahribatsizRadyografikSonucList) {
		this.allTestTahribatsizRadyografikSonucList = allTestTahribatsizRadyografikSonucList;
	}

	/**
	 * @return the testTahribatsizRadyografikSonucForm
	 */
	public TestTahribatsizRadyografikSonuc getTestTahribatsizRadyografikSonucForm() {
		return testTahribatsizRadyografikSonucForm;
	}

	/**
	 * @param testTahribatsizRadyografikSonucForm
	 *            the testTahribatsizRadyografikSonucForm to set
	 */
	public void setTestTahribatsizRadyografikSonucForm(
			TestTahribatsizRadyografikSonuc testTahribatsizRadyografikSonucForm) {
		this.testTahribatsizRadyografikSonucForm = testTahribatsizRadyografikSonucForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @param index
	 *            the index to set
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * @return the selectedPipe
	 */
	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	/**
	 * @param selectedPipe
	 *            the selectedPipe to set
	 */
	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	/**
	 * @return the barkodNo
	 */
	public String getBarkodNo() {
		return barkodNo;
	}

	/**
	 * @param barkodNo
	 *            the barkodNo to set
	 */
	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	/**
	 * @return the kalibrasyonlar
	 */
	public List<KalibrasyonRadyografik> getKalibrasyonlar() {
		return kalibrasyonlar;
	}

	/**
	 * @param kalibrasyonlar
	 *            the kalibrasyonlar to set
	 */
	public void setKalibrasyonlar(List<KalibrasyonRadyografik> kalibrasyonlar) {
		this.kalibrasyonlar = kalibrasyonlar;
	}

	/**
	 * @return the kalibrasyon
	 */
	public KalibrasyonRadyografik getKalibrasyon() {
		return kalibrasyon;
	}

	/**
	 * @param kalibrasyon
	 *            the kalibrasyon to set
	 */
	public void setKalibrasyon(KalibrasyonRadyografik kalibrasyon) {
		this.kalibrasyon = kalibrasyon;
	}

}
