package com.emekboru.beans.test;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "testRendersBean")
@SessionScoped
public class TestRendersBean {

	// tahribatlı tab renderlari
	private boolean testCekmeRender;
	private boolean testCentikDarbeRender;
	private boolean testAgirlikRender;
	private boolean testKaynakRender;
	private boolean testKimyasalRender;
	private boolean testBukmeRender;

	// kaplama test renderlari
	private boolean testKaplamaDarbeRender;
	private boolean testYuzeyKontrolleriRender;
	private boolean testSicakSuRender;
	private boolean testKaplamaBukmeRender;
	private boolean testTozBoyaRender;
	private boolean testYapismaRender;
	private boolean testDeliciRender;
	private boolean testYuzeyPuruzlulukRender;
	private boolean testPolyolefinRender;
	private boolean testKatodikRender;
	private boolean testSertlikRender;
	private boolean testBetonRender;
	private boolean testKaplamaKalinligiRender;
	private boolean testKaplamaBetonYuzeyHazirligiSonucRender;
	private boolean testKaplamaBetonGozKontrolSonucRender;
	private boolean testKaplamaNihaiKontrolSonucRender;
	private boolean testKaplamaKumlanmisBoruTozMiktariSonucRender;
	private boolean testKaplamaKaplamasizBolgeSonucRender;
	private boolean testKaplamaUygulamaSicaklikSonucRender;
	private boolean testKaplamaOnlineHolidaySonucRender;
	private boolean testKaplamaHolidayPinholeSonucRender;
	private boolean testKaplamaYuzeyPuruzlulukTestiRender;
	private boolean testKaplamaGozKontrolSonucRender;
	private boolean testKaplamaKumlanmisBoruTuzMiktariSonucRender;
	private boolean testKaplamaBuchholzSonucRender;
	private boolean testKaplamaTamirTestSonucRender;
	private boolean testKaplamaKumlamaMalzemesiTuzMiktariSonucRender;
	private boolean testKaplamaProcessBozulmaSonucRender;
	private boolean testKaplamaKumlamaOrtamSartlariSonucRender;
	private boolean testKaplamaBetonOranBelirlenmesiSonucRender;
	private boolean testKaplamaBetonEgilmeBasincSonucRender;
	private boolean testKaplamaKumlamaMalzemesiBoyutSekilOzellikKontroluSonucRender;
	private boolean testKaplamaDaldirmaTestiSonucRender;
	private boolean testKaplamaTozEpoksiXcutSonucRender;
	private boolean testKaplamaPulloffYapismaSonucRender;
	private boolean testKaplamaFlowCoatBendSonucRender;
	private boolean testKaplamaFlowCoatBondSonucRender;
	private boolean testKaplamaFlowCoatBuchholzSonucRender;
	private boolean testKaplamaFlowCoatCureSonucRender;
	private boolean testKaplamaFlowCoatFilmThicknessSonucRender;
	private boolean testKaplamaFlowCoatPinholeSonucRender;
	private boolean testKaplamaFlowCoatStrippingSonucRender;
	private boolean testKaplamaFlowCoatWaterSonucRender;
	private boolean testKaplamaSonrasiYuzeyKontrolRender;
	private boolean testKaplamaOncesiYuzeyKontrolRender;
	private boolean TestKaplamaMpqtKaplamaKalinligiRender;
	private boolean TestKaplamaGasBlisteringSonucRender;
	private boolean TestKaplamaHydraulicBlisteringSonucRender;
	private boolean TestKaplamaThermalAgeingTestSonucRender;
	private boolean TestKaplamaUvAgeingTestSonucRender;
	private boolean TestKaplamaElektrikselDirencTestSonucRender;
	private boolean TestKaplamaStorageConditionTestSonucRender;
	private boolean TestKaplamaMixedWetPaintTestSonucRender;

	// taribatsız tab renderlari
	private boolean testOtomatikUtKaynak;
	private boolean testOtomatikUtLaminasyon;
	private boolean testOfflineUt;
	private boolean testManuelUt;
	private boolean testBoruUcuLaminasyon;
	private boolean testFloroskopik;
	private boolean testManyetikParcacik;
	private boolean testOtomatikUtHaz;
	private boolean testPenetrant;
	private boolean testCiplakBoruNihaiKabul;
	private boolean testHidrostatik;

	// Tahribatlı Test Sonucları renderları
	// cekme
	private boolean testZ01Render = false;
	private boolean testZ02Render = false;
	private boolean testZ03Render = false;
	private boolean testZ06Render = false;
	private boolean testZ07Render = false;
	private boolean testZ02Z03Z07Render = false;

	// bukme
	private boolean testF01Render = false;
	private boolean testF02Render = false;
	private boolean testF03Render = false;
	private boolean testF04Render = false;
	private boolean testF05Render = false;
	private boolean testF06Render = false;
	// centik
	private boolean testK01Render = false;
	private boolean testK02Render = false;
	private boolean testK03Render = false;
	private boolean testK04Render = false;
	private boolean testK05Render = false;
	private boolean testU01Render = false;
	private boolean testU02Render = false;
	private boolean testU03Render = false;
	private boolean testU04Render = false;
	private boolean testU05Render = false;
	// agirlik
	private boolean testD01Render = false;
	private boolean testD02Render = false;
	// kaynak
	private boolean testM01Render = false;
	private boolean testH01Render = false;

	// kaplama test renderları
	// kaplama darbe
	private boolean testCIT1Render = false;
	private boolean testLIT1Render = false;
	// yuzey kontrolleri
	private boolean testCSC1Render = false;
	private boolean testLSC1Render = false;
	private boolean testCSP1Render = false;
	private boolean testLSP1Render = false;
	// sıcak su direnc
	private boolean testCHW1Render = false;
	// kaplama bukme
	private boolean testCBT1Render = false;
	private boolean testLBT1Render = false;
	// toz boya testi
	private boolean testCDS1Render = false;
	private boolean testEXC1Render = false;
	// holiday
	private boolean testOHT1Render = false;
	private boolean testLHT1Render = false;
	// yapısma
	private boolean testCAT1Render = false;
	private boolean testLAT1Render = false;
	// delici uca direnc
	private boolean testCIT2Render = false;
	private boolean testLIT2Render = false;
	// polyolefin uzama
	private boolean testCET1Render = false;
	// katodik soyulma
	private boolean testCCD1Render = false;
	private boolean testLCD1Render = false;
	// sertlik
	private boolean testCHT2Render = false;
	private boolean testLHT2Render = false;
	// beton
	private boolean testBCB1Render = false;
	private boolean testBYH1Render = false;
	private boolean testBGK1Render = false;
	// kaplama kalınlıgı
	private boolean testCCT1Render = false;
	private boolean testLCT1Render = false;
	// kaplamadan once yüzey kontrol
	private boolean testLSK1Render = false;
	// testKaplamaNihaiKontrolSonuc
	private boolean testLFI1Render = false;
	private boolean testCFI1Render = false;
	// testKaplamaNihaiKontrolSonuc
	private boolean testLSD1Render = false;
	private boolean testCSD1Render = false;
	// TestKaplamaKaplamasizBolgeSonuc
	private boolean testCKB1Render = false;
	private boolean testLKB1Render = false;
	// TestKaplamaKaplamasizBolgeSonuc
	private boolean testKSK1Render = false;
	private boolean testCGK1Render = false;
	private boolean testLGK1Render = false;
	private boolean testCSS1Render = false;
	private boolean testLSS1Render = false;
	private boolean testLBZ1Render = false;
	private boolean testCKT1Render = false;
	private boolean testCKT2Render = false;
	private boolean testLKT1Render = false;
	private boolean testCAS1Render = false;
	private boolean testLAS1Render = false;
	private boolean testCPP1Render = false;
	private boolean testCPP2Render = false;
	private boolean testLPP1Render = false;
	private boolean testCAC1Render = false;
	private boolean testLAC1Render = false;
	private boolean testBSC1Render = false;
	private boolean testLCB1Render = false;
	private boolean testLCC1Render = false;
	// Kumlama Malzemesi, boyut şekil ozellik kontrolu
	private boolean testCKK1Render = false;
	private boolean testLKK1Render = false;
	// flow coat için lab testleri
	private boolean testFPT1Render = false;
	private boolean testFBH1Render = false;
	private boolean testFFT1Render = false;
	private boolean testFCT1Render = false;
	private boolean testFBT1Render = false;
	private boolean testFWT1Render = false;
	private boolean testFAT1Render = false;
	private boolean testFST1Render = false;

	private boolean testLDT1Render = false;

	private boolean testBKK1Render = false;
	private boolean testPOY1Render = false;

	private boolean testLGBRender = false;
	private boolean testLHBRender = false;
	private boolean testCTYRender = false;
	private boolean testCUVRender = false;
	private boolean testCEDRender = false;
	private boolean testCSTRender = false;
	private boolean testLMPRender = false;

	// Getters & Setters

	public boolean isTestZ01Render() {
		return testZ01Render;
	}

	/**
	 * @return the testYuzeyPuruzlulukRender
	 */
	public boolean isTestYuzeyPuruzlulukRender() {
		return testYuzeyPuruzlulukRender;
	}

	/**
	 * @param testYuzeyPuruzlulukRender
	 *            the testYuzeyPuruzlulukRender to set
	 */

	public void setTestYuzeyPuruzlulukRender(boolean testYuzeyPuruzlulukRender) {
		this.testYuzeyPuruzlulukRender = testYuzeyPuruzlulukRender;
	}

	/**
	 * @return the testBYH1Render
	 */
	public boolean isTestBYH1Render() {
		return testBYH1Render;
	}

	/**
	 * @param testBYH1Render
	 *            the testBYH1Render to set
	 */
	public void setTestBYH1Render(boolean testBYH1Render) {
		this.testBYH1Render = testBYH1Render;
	}

	public void setTestZ01Render(boolean testZ01Render) {
		this.testZ01Render = testZ01Render;
	}

	public boolean isTestZ02Render() {
		return testZ02Render;
	}

	/**
	 * @return the testKaplamaBetonGozKontrolSonucRender
	 */
	public boolean isTestKaplamaBetonGozKontrolSonucRender() {
		return testKaplamaBetonGozKontrolSonucRender;
	}

	/**
	 * @param testKaplamaBetonGozKontrolSonucRender
	 *            the testKaplamaBetonGozKontrolSonucRender to set
	 */
	public void setTestKaplamaBetonGozKontrolSonucRender(
			boolean testKaplamaBetonGozKontrolSonucRender) {
		this.testKaplamaBetonGozKontrolSonucRender = testKaplamaBetonGozKontrolSonucRender;
	}

	/**
	 * @return the testBGK1Render
	 */
	public boolean isTestBGK1Render() {
		return testBGK1Render;
	}

	/**
	 * @param testBGK1Render
	 *            the testBGK1Render to set
	 */
	public void setTestBGK1Render(boolean testBGK1Render) {
		this.testBGK1Render = testBGK1Render;
	}

	public void setTestZ02Render(boolean testZ02Render) {
		this.testZ02Render = testZ02Render;
	}

	public boolean isTestZ03Render() {
		return testZ03Render;
	}

	public void setTestZ03Render(boolean testZ03Render) {
		this.testZ03Render = testZ03Render;
	}

	public boolean isTestZ06Render() {
		return testZ06Render;
	}

	public void setTestZ06Render(boolean testZ06Render) {
		this.testZ06Render = testZ06Render;
	}

	public boolean isTestZ07Render() {
		return testZ07Render;
	}

	public void setTestZ07Render(boolean testZ07Render) {
		this.testZ07Render = testZ07Render;
	}

	public boolean isTestF01Render() {
		return testF01Render;
	}

	public void setTestF01Render(boolean testF01Render) {
		this.testF01Render = testF01Render;
	}

	public boolean isTestF02Render() {
		return testF02Render;
	}

	public void setTestF02Render(boolean testF02Render) {
		this.testF02Render = testF02Render;
	}

	public boolean isTestF03Render() {
		return testF03Render;
	}

	public void setTestF03Render(boolean testF03Render) {
		this.testF03Render = testF03Render;
	}

	public boolean isTestF04Render() {
		return testF04Render;
	}

	public void setTestF04Render(boolean testF04Render) {
		this.testF04Render = testF04Render;
	}

	public boolean isTestF05Render() {
		return testF05Render;
	}

	public void setTestF05Render(boolean testF05Render) {
		this.testF05Render = testF05Render;
	}

	public boolean isTestF06Render() {
		return testF06Render;
	}

	public void setTestF06Render(boolean testF06Render) {
		this.testF06Render = testF06Render;
	}

	public boolean isTestK01Render() {
		return testK01Render;
	}

	public void setTestK01Render(boolean testK01Render) {
		this.testK01Render = testK01Render;
	}

	public boolean isTestK02Render() {
		return testK02Render;
	}

	public void setTestK02Render(boolean testK02Render) {
		this.testK02Render = testK02Render;
	}

	public boolean isTestK03Render() {
		return testK03Render;
	}

	public void setTestK03Render(boolean testK03Render) {
		this.testK03Render = testK03Render;
	}

	public boolean isTestK04Render() {
		return testK04Render;
	}

	public void setTestK04Render(boolean testK04Render) {
		this.testK04Render = testK04Render;
	}

	public boolean isTestK05Render() {
		return testK05Render;
	}

	public void setTestK05Render(boolean testK05Render) {
		this.testK05Render = testK05Render;
	}

	public boolean isTestD01Render() {
		return testD01Render;
	}

	public void setTestD01Render(boolean testD01Render) {
		this.testD01Render = testD01Render;
	}

	public boolean isTestD02Render() {
		return testD02Render;
	}

	public void setTestD02Render(boolean testD02Render) {
		this.testD02Render = testD02Render;
	}

	public boolean isTestM01Render() {
		return testM01Render;
	}

	public void setTestM01Render(boolean testM01Render) {
		this.testM01Render = testM01Render;
	}

	public boolean isTestH01Render() {
		return testH01Render;
	}

	public void setTestH01Render(boolean testH01Render) {
		this.testH01Render = testH01Render;
	}

	public boolean isTestCekmeRender() {
		return testCekmeRender;
	}

	public void setTestCekmeRender(boolean testCekmeRender) {
		this.testCekmeRender = testCekmeRender;
	}

	public boolean isTestCentikDarbeRender() {
		return testCentikDarbeRender;
	}

	public void setTestCentikDarbeRender(boolean testCentikDarbeRender) {
		this.testCentikDarbeRender = testCentikDarbeRender;
	}

	public boolean isTestAgirlikRender() {
		return testAgirlikRender;
	}

	public void setTestAgirlikRender(boolean testAgirlikRender) {
		this.testAgirlikRender = testAgirlikRender;
	}

	public boolean isTestKaynakRender() {
		return testKaynakRender;
	}

	public void setTestKaynakRender(boolean testKaynakRender) {
		this.testKaynakRender = testKaynakRender;
	}

	public boolean isTestKimyasalRender() {
		return testKimyasalRender;
	}

	public void setTestKimyasalRender(boolean testKimyasalRender) {
		this.testKimyasalRender = testKimyasalRender;
	}

	public boolean isTestBukmeRender() {
		return testBukmeRender;
	}

	public void setTestBukmeRender(boolean testBukmeRender) {
		this.testBukmeRender = testBukmeRender;
	}

	public boolean isTestZ02Z03Z07Render() {
		return testZ02Z03Z07Render;
	}

	public void setTestZ02Z03Z07Render(boolean testZ02Z03Z07Render) {
		this.testZ02Z03Z07Render = testZ02Z03Z07Render;
	}

	public boolean isTestKaplamaHolidayPinholeSonucRender() {
		return testKaplamaHolidayPinholeSonucRender;
	}

	public void setTestKaplamaHolidayPinholeSonucRender(
			boolean testKaplamaHolidayPinholeSonucRender) {
		this.testKaplamaHolidayPinholeSonucRender = testKaplamaHolidayPinholeSonucRender;
	}

	public boolean isTestKaplamaDarbeRender() {
		return testKaplamaDarbeRender;
	}

	public void setTestKaplamaDarbeRender(boolean testKaplamaDarbeRender) {
		this.testKaplamaDarbeRender = testKaplamaDarbeRender;
	}

	public boolean isTestCIT1Render() {
		return testCIT1Render;
	}

	public void setTestCIT1Render(boolean testCIT1Render) {
		this.testCIT1Render = testCIT1Render;
	}

	public boolean isTestLIT1Render() {
		return testLIT1Render;
	}

	public void setTestLIT1Render(boolean testLIT1Render) {
		this.testLIT1Render = testLIT1Render;
	}

	public boolean isTestCSC1Render() {
		return testCSC1Render;
	}

	public void setTestCSC1Render(boolean testCSC1Render) {
		this.testCSC1Render = testCSC1Render;
	}

	public boolean isTestLSC1Render() {
		return testLSC1Render;
	}

	public void setTestLSC1Render(boolean testLSC1Render) {
		this.testLSC1Render = testLSC1Render;
	}

	public boolean isTestCSP1Render() {
		return testCSP1Render;
	}

	public void setTestCSP1Render(boolean testCSP1Render) {
		this.testCSP1Render = testCSP1Render;
	}

	public boolean isTestLSP1Render() {
		return testLSP1Render;
	}

	public void setTestLSP1Render(boolean testLSP1Render) {
		this.testLSP1Render = testLSP1Render;
	}

	// public boolean isProfilTestRender() {
	// return profilTestRender;
	// }
	//
	// public void setProfilTestRender(boolean profilTestRender) {
	// this.profilTestRender = profilTestRender;
	// }

	public boolean isTestYuzeyKontrolleriRender() {
		return testYuzeyKontrolleriRender;
	}

	public void setTestYuzeyKontrolleriRender(boolean testYuzeyKontrolleriRender) {
		this.testYuzeyKontrolleriRender = testYuzeyKontrolleriRender;
	}

	public boolean isTestCHW1Render() {
		return testCHW1Render;
	}

	public void setTestCHW1Render(boolean testCHW1Render) {
		this.testCHW1Render = testCHW1Render;
	}

	public boolean isTestSicakSuRender() {
		return testSicakSuRender;
	}

	public void setTestSicakSuRender(boolean testSicakSuRender) {
		this.testSicakSuRender = testSicakSuRender;
	}

	public boolean isTestCBT1Render() {
		return testCBT1Render;
	}

	public void setTestCBT1Render(boolean testCBT1Render) {
		this.testCBT1Render = testCBT1Render;
	}

	public boolean isTestLBT1Render() {
		return testLBT1Render;
	}

	public void setTestLBT1Render(boolean testLBT1Render) {
		this.testLBT1Render = testLBT1Render;
	}

	public boolean isTestCDS1Render() {
		return testCDS1Render;
	}

	public void setTestCDS1Render(boolean testCDS1Render) {
		this.testCDS1Render = testCDS1Render;
	}

	public boolean isTestKaplamaBukmeRender() {
		return testKaplamaBukmeRender;
	}

	public void setTestKaplamaBukmeRender(boolean testKaplamaBukmeRender) {
		this.testKaplamaBukmeRender = testKaplamaBukmeRender;
	}

	public boolean isTestTozBoyaRender() {
		return testTozBoyaRender;
	}

	public void setTestTozBoyaRender(boolean testTozBoyaRender) {
		this.testTozBoyaRender = testTozBoyaRender;
	}

	public boolean isTestLHT1Render() {
		return testLHT1Render;
	}

	public void setTestLHT1Render(boolean testLHT1Render) {
		this.testLHT1Render = testLHT1Render;
	}

	public boolean isTestYapismaRender() {
		return testYapismaRender;
	}

	public void setTestYapismaRender(boolean testYapismaRender) {
		this.testYapismaRender = testYapismaRender;
	}

	public boolean isTestCAT1Render() {
		return testCAT1Render;
	}

	public void setTestCAT1Render(boolean testCAT1Render) {
		this.testCAT1Render = testCAT1Render;
	}

	public boolean isTestLAT1Render() {
		return testLAT1Render;
	}

	public void setTestLAT1Render(boolean testLAT1Render) {
		this.testLAT1Render = testLAT1Render;
	}

	public boolean isTestDeliciRender() {
		return testDeliciRender;
	}

	public void setTestDeliciRender(boolean testDeliciRender) {
		this.testDeliciRender = testDeliciRender;
	}

	/**
	 * @return the testKaplamaBetonYuzeyHazirligiSonucRender
	 */
	public boolean istestKaplamaBetonYuzeyHazirligiSonucRender() {
		return testKaplamaBetonYuzeyHazirligiSonucRender;
	}

	/**
	 * @param testKaplamaBetonYuzeyHazirligiSonucRender
	 *            the testKaplamaBetonYuzeyHazirligiSonucRender to set
	 */
	public void settestKaplamaBetonYuzeyHazirligiSonucRender(
			boolean testKaplamaBetonYuzeyHazirligiSonucRender) {
		this.testKaplamaBetonYuzeyHazirligiSonucRender = testKaplamaBetonYuzeyHazirligiSonucRender;
	}

	public boolean isTestCIT2Render() {
		return testCIT2Render;
	}

	public void setTestCIT2Render(boolean testCIT2Render) {
		this.testCIT2Render = testCIT2Render;
	}

	public boolean isTestLIT2Render() {
		return testLIT2Render;
	}

	public void setTestLIT2Render(boolean testLIT2Render) {
		this.testLIT2Render = testLIT2Render;
	}

	public boolean isTestPolyolefinRender() {
		return testPolyolefinRender;
	}

	public void setTestPolyolefinRender(boolean testPolyolefinRender) {
		this.testPolyolefinRender = testPolyolefinRender;
	}

	public boolean isTestCET1Render() {
		return testCET1Render;
	}

	public void setTestCET1Render(boolean testCET1Render) {
		this.testCET1Render = testCET1Render;
	}

	public boolean isTestKatodikRender() {
		return testKatodikRender;
	}

	public void setTestKatodikRender(boolean testKatodikRender) {
		this.testKatodikRender = testKatodikRender;
	}

	public boolean isTestCCD1Render() {
		return testCCD1Render;
	}

	public void setTestCCD1Render(boolean testCCD1Render) {
		this.testCCD1Render = testCCD1Render;
	}

	public boolean isTestLCD1Render() {
		return testLCD1Render;
	}

	public void setTestLCD1Render(boolean testLCD1Render) {
		this.testLCD1Render = testLCD1Render;
	}

	public boolean isTestSertlikRender() {
		return testSertlikRender;
	}

	public void setTestSertlikRender(boolean testSertlikRender) {
		this.testSertlikRender = testSertlikRender;
	}

	public boolean isTestCHT2Render() {
		return testCHT2Render;
	}

	public void setTestCHT2Render(boolean testCHT2Render) {
		this.testCHT2Render = testCHT2Render;
	}

	public boolean isTestLHT2Render() {
		return testLHT2Render;
	}

	public void setTestLHT2Render(boolean testLHT2Render) {
		this.testLHT2Render = testLHT2Render;
	}

	public boolean isTestBetonRender() {
		return testBetonRender;
	}

	public void setTestBetonRender(boolean testBetonRender) {
		this.testBetonRender = testBetonRender;
	}

	public boolean isTestKaplamaKalinligiRender() {
		return testKaplamaKalinligiRender;
	}

	public void setTestKaplamaKalinligiRender(boolean testKaplamaKalinligiRender) {
		this.testKaplamaKalinligiRender = testKaplamaKalinligiRender;
	}

	public boolean isTestCCT1Render() {
		return testCCT1Render;
	}

	public void setTestCCT1Render(boolean testCCT1Render) {
		this.testCCT1Render = testCCT1Render;
	}

	public boolean isTestLCT1Render() {
		return testLCT1Render;
	}

	public void setTestLCT1Render(boolean testLCT1Render) {
		this.testLCT1Render = testLCT1Render;
	}

	public boolean isTestOtomatikUtKaynak() {
		return testOtomatikUtKaynak;
	}

	public void setTestOtomatikUtKaynak(boolean testOtomatikUtKaynak) {
		this.testOtomatikUtKaynak = testOtomatikUtKaynak;
	}

	public boolean isTestOtomatikUtLaminasyon() {
		return testOtomatikUtLaminasyon;
	}

	public void setTestOtomatikUtLaminasyon(boolean testOtomatikUtLaminasyon) {
		this.testOtomatikUtLaminasyon = testOtomatikUtLaminasyon;
	}

	public boolean isTestOfflineUt() {
		return testOfflineUt;
	}

	public void setTestOfflineUt(boolean testOfflineUt) {
		this.testOfflineUt = testOfflineUt;
	}

	public boolean isTestManuelUt() {
		return testManuelUt;
	}

	public void setTestManuelUt(boolean testManuelUt) {
		this.testManuelUt = testManuelUt;
	}

	public boolean isTestBoruUcuLaminasyon() {
		return testBoruUcuLaminasyon;
	}

	public void setTestBoruUcuLaminasyon(boolean testBoruUcuLaminasyon) {
		this.testBoruUcuLaminasyon = testBoruUcuLaminasyon;
	}

	public boolean isTestFloroskopik() {
		return testFloroskopik;
	}

	public void setTestFloroskopik(boolean testFloroskopik) {
		this.testFloroskopik = testFloroskopik;
	}

	public boolean isTestManyetikParcacik() {
		return testManyetikParcacik;
	}

	public void setTestManyetikParcacik(boolean testManyetikParcacik) {
		this.testManyetikParcacik = testManyetikParcacik;
	}

	public boolean isTestOtomatikUtHaz() {
		return testOtomatikUtHaz;
	}

	public void setTestOtomatikUtHaz(boolean testOtomatikUtHaz) {
		this.testOtomatikUtHaz = testOtomatikUtHaz;
	}

	public boolean isTestPenetrant() {
		return testPenetrant;
	}

	public void setTestPenetrant(boolean testPenetrant) {
		this.testPenetrant = testPenetrant;
	}

	public boolean isTestCiplakBoruNihaiKabul() {
		return testCiplakBoruNihaiKabul;
	}

	public void setTestCiplakBoruNihaiKabul(boolean testCiplakBoruNihaiKabul) {
		this.testCiplakBoruNihaiKabul = testCiplakBoruNihaiKabul;
	}

	public boolean isTestHidrostatik() {
		return testHidrostatik;
	}

	public void setTestHidrostatik(boolean testHidrostatik) {
		this.testHidrostatik = testHidrostatik;
	}

	public boolean isTestU01Render() {
		return testU01Render;
	}

	public void setTestU01Render(boolean testU01Render) {
		this.testU01Render = testU01Render;
	}

	public boolean isTestU02Render() {
		return testU02Render;
	}

	public void setTestU02Render(boolean testU02Render) {
		this.testU02Render = testU02Render;
	}

	public boolean isTestU03Render() {
		return testU03Render;
	}

	public void setTestU03Render(boolean testU03Render) {
		this.testU03Render = testU03Render;
	}

	public boolean isTestU04Render() {
		return testU04Render;
	}

	public void setTestU04Render(boolean testU04Render) {
		this.testU04Render = testU04Render;
	}

	public boolean isTestU05Render() {
		return testU05Render;
	}

	public void setTestU05Render(boolean testU05Render) {
		this.testU05Render = testU05Render;
	}

	/**
	 * @return the testLSD1Render
	 */
	public boolean isTestLSD1Render() {
		return testLSD1Render;
	}

	/**
	 * @param testLSD1Render
	 *            the testLSD1Render to set
	 */
	public void setTestLSD1Render(boolean testLSD1Render) {
		this.testLSD1Render = testLSD1Render;
	}

	/**
	 * @return the testBCB1Render
	 */
	public boolean isTestBCB1Render() {
		return testBCB1Render;
	}

	/**
	 * @param testBCB1Render
	 *            the testBCB1Render to set
	 */
	public void setTestBCB1Render(boolean testBCB1Render) {
		this.testBCB1Render = testBCB1Render;
	}

	/**
	 * @return the testKaplamaBetonYuzeyHazirligiSonucRender
	 */
	public boolean isTestKaplamaBetonYuzeyHazirligiSonucRender() {
		return testKaplamaBetonYuzeyHazirligiSonucRender;
	}

	/**
	 * @param testKaplamaBetonYuzeyHazirligiSonucRender
	 *            the testKaplamaBetonYuzeyHazirligiSonucRender to set
	 */
	public void setTestKaplamaBetonYuzeyHazirligiSonucRender(
			boolean testKaplamaBetonYuzeyHazirligiSonucRender) {
		this.testKaplamaBetonYuzeyHazirligiSonucRender = testKaplamaBetonYuzeyHazirligiSonucRender;
	}

	/**
	 * @return the testLSK1Render
	 */
	public boolean isTestLSK1Render() {
		return testLSK1Render;
	}

	/**
	 * @param testLSK1Render
	 *            the testLSK1Render to set
	 */
	public void setTestLSK1Render(boolean testLSK1Render) {
		this.testLSK1Render = testLSK1Render;
	}

	/**
	 * @return the testKaplamaNihaiKontrolSonucRender
	 */
	public boolean isTestKaplamaNihaiKontrolSonucRender() {
		return testKaplamaNihaiKontrolSonucRender;
	}

	/**
	 * @param testKaplamaNihaiKontrolSonucRender
	 *            the testKaplamaNihaiKontrolSonucRender to set
	 */
	public void setTestKaplamaNihaiKontrolSonucRender(
			boolean testKaplamaNihaiKontrolSonucRender) {
		this.testKaplamaNihaiKontrolSonucRender = testKaplamaNihaiKontrolSonucRender;
	}

	/**
	 * @return the testLFI1Render
	 */
	public boolean isTestLFI1Render() {
		return testLFI1Render;
	}

	/**
	 * @param testLFI1Render
	 *            the testLFI1Render to set
	 */
	public void setTestLFI1Render(boolean testLFI1Render) {
		this.testLFI1Render = testLFI1Render;
	}

	/**
	 * @return the testCFI1Render
	 */
	public boolean isTestCFI1Render() {
		return testCFI1Render;
	}

	/**
	 * @param testCFI1Render
	 *            the testCFI1Render to set
	 */
	public void setTestCFI1Render(boolean testCFI1Render) {
		this.testCFI1Render = testCFI1Render;
	}

	/**
	 * @return the testKaplamaKumlanmisBoruTozMiktariSonucRender
	 */
	public boolean isTestKaplamaKumlanmisBoruTozMiktariSonucRender() {
		return testKaplamaKumlanmisBoruTozMiktariSonucRender;
	}

	/**
	 * @param testKaplamaKumlanmisBoruTozMiktariSonucRender
	 *            the testKaplamaKumlanmisBoruTozMiktariSonucRender to set
	 */
	public void setTestKaplamaKumlanmisBoruTozMiktariSonucRender(
			boolean testKaplamaKumlanmisBoruTozMiktariSonucRender) {
		this.testKaplamaKumlanmisBoruTozMiktariSonucRender = testKaplamaKumlanmisBoruTozMiktariSonucRender;
	}

	/**
	 * @return the testCSD1Render
	 */
	public boolean isTestCSD1Render() {
		return testCSD1Render;
	}

	/**
	 * @param testCSD1Render
	 *            the testCSD1Render to set
	 */
	public void setTestCSD1Render(boolean testCSD1Render) {
		this.testCSD1Render = testCSD1Render;
	}

	/**
	 * @return the testKaplamaKaplamasizBolgeSonucRender
	 */
	public boolean isTestKaplamaKaplamasizBolgeSonucRender() {
		return testKaplamaKaplamasizBolgeSonucRender;
	}

	/**
	 * @param testKaplamaKaplamasizBolgeSonucRender
	 *            the testKaplamaKaplamasizBolgeSonucRender to set
	 */
	public void setTestKaplamaKaplamasizBolgeSonucRender(
			boolean testKaplamaKaplamasizBolgeSonucRender) {
		this.testKaplamaKaplamasizBolgeSonucRender = testKaplamaKaplamasizBolgeSonucRender;
	}

	/**
	 * @return the testCKB1Render
	 */
	public boolean isTestCKB1Render() {
		return testCKB1Render;
	}

	/**
	 * @param testCKB1Render
	 *            the testCKB1Render to set
	 */
	public void setTestCKB1Render(boolean testCKB1Render) {
		this.testCKB1Render = testCKB1Render;
	}

	/**
	 * @return the testLKB1Render
	 */
	public boolean isTestLKB1Render() {
		return testLKB1Render;
	}

	/**
	 * @param testLKB1Render
	 *            the testLKB1Render to set
	 */
	public void setTestLKB1Render(boolean testLKB1Render) {
		this.testLKB1Render = testLKB1Render;
	}

	/**
	 * @return the testKaplamaUygulamaSicaklikSonucRender
	 */
	public boolean isTestKaplamaUygulamaSicaklikSonucRender() {
		return testKaplamaUygulamaSicaklikSonucRender;
	}

	/**
	 * @param testKaplamaUygulamaSicaklikSonucRender
	 *            the testKaplamaUygulamaSicaklikSonucRender to set
	 */
	public void setTestKaplamaUygulamaSicaklikSonucRender(
			boolean testKaplamaUygulamaSicaklikSonucRender) {
		this.testKaplamaUygulamaSicaklikSonucRender = testKaplamaUygulamaSicaklikSonucRender;
	}

	/**
	 * @return the testKSK1Render
	 */
	public boolean isTestKSK1Render() {
		return testKSK1Render;
	}

	/**
	 * @param testKSK1Render
	 *            the testKSK1Render to set
	 */
	public void setTestKSK1Render(boolean testKSK1Render) {
		this.testKSK1Render = testKSK1Render;
	}

	/**
	 * @return the testKaplamaYuzeyPuruzlulukTestiRender
	 */
	public boolean isTestKaplamaYuzeyPuruzlulukTestiRender() {
		return testKaplamaYuzeyPuruzlulukTestiRender;
	}

	/**
	 * @param testKaplamaYuzeyPuruzlulukTestiRender
	 *            the testKaplamaYuzeyPuruzlulukTestiRender to set
	 */
	public void setTestKaplamaYuzeyPuruzlulukTestiRender(
			boolean testKaplamaYuzeyPuruzlulukTestiRender) {
		this.testKaplamaYuzeyPuruzlulukTestiRender = testKaplamaYuzeyPuruzlulukTestiRender;
	}

	/**
	 * @return the testKaplamaGozKontrolSonucRender
	 */
	public boolean isTestKaplamaGozKontrolSonucRender() {
		return testKaplamaGozKontrolSonucRender;
	}

	/**
	 * @param testKaplamaGozKontrolSonucRender
	 *            the testKaplamaGozKontrolSonucRender to set
	 */
	public void setTestKaplamaGozKontrolSonucRender(
			boolean testKaplamaGozKontrolSonucRender) {
		this.testKaplamaGozKontrolSonucRender = testKaplamaGozKontrolSonucRender;
	}

	/**
	 * @return the testCGK1Render
	 */
	public boolean isTestCGK1Render() {
		return testCGK1Render;
	}

	/**
	 * @param testCGK1Render
	 *            the testCGK1Render to set
	 */
	public void setTestCGK1Render(boolean testCGK1Render) {
		this.testCGK1Render = testCGK1Render;
	}

	/**
	 * @return the testLGK1Render
	 */
	public boolean isTestLGK1Render() {
		return testLGK1Render;
	}

	/**
	 * @param testLGK1Render
	 *            the testLGK1Render to set
	 */
	public void setTestLGK1Render(boolean testLGK1Render) {
		this.testLGK1Render = testLGK1Render;
	}

	/**
	 * @return the testCSS1Render
	 */
	public boolean isTestCSS1Render() {
		return testCSS1Render;
	}

	/**
	 * @param testCSS1Render
	 *            the testCSS1Render to set
	 */
	public void setTestCSS1Render(boolean testCSS1Render) {
		this.testCSS1Render = testCSS1Render;
	}

	/**
	 * @return the testLSS1Render
	 */
	public boolean isTestLSS1Render() {
		return testLSS1Render;
	}

	/**
	 * @param testLSS1Render
	 *            the testLSS1Render to set
	 */
	public void setTestLSS1Render(boolean testLSS1Render) {
		this.testLSS1Render = testLSS1Render;
	}

	/**
	 * @return the testKaplamaKumlanmisBoruTuzMiktariSonucRender
	 */
	public boolean isTestKaplamaKumlanmisBoruTuzMiktariSonucRender() {
		return testKaplamaKumlanmisBoruTuzMiktariSonucRender;
	}

	/**
	 * @param testKaplamaKumlanmisBoruTuzMiktariSonucRender
	 *            the testKaplamaKumlanmisBoruTuzMiktariSonucRender to set
	 */
	public void setTestKaplamaKumlanmisBoruTuzMiktariSonucRender(
			boolean testKaplamaKumlanmisBoruTuzMiktariSonucRender) {
		this.testKaplamaKumlanmisBoruTuzMiktariSonucRender = testKaplamaKumlanmisBoruTuzMiktariSonucRender;
	}

	/**
	 * @return the testKaplamaBuchholzSonucRender
	 */
	public boolean isTestKaplamaBuchholzSonucRender() {
		return testKaplamaBuchholzSonucRender;
	}

	/**
	 * @param testKaplamaBuchholzSonucRender
	 *            the testKaplamaBuchholzSonucRender to set
	 */
	public void setTestKaplamaBuchholzSonucRender(
			boolean testKaplamaBuchholzSonucRender) {
		this.testKaplamaBuchholzSonucRender = testKaplamaBuchholzSonucRender;
	}

	/**
	 * @return the testLBZ1Render
	 */
	public boolean isTestLBZ1Render() {
		return testLBZ1Render;
	}

	/**
	 * @param testLBZ1Render
	 *            the testLBZ1Render to set
	 */
	public void setTestLBZ1Render(boolean testLBZ1Render) {
		this.testLBZ1Render = testLBZ1Render;
	}

	/**
	 * @return the testKaplamaTamirTestSonucRender
	 */
	public boolean isTestKaplamaTamirTestSonucRender() {
		return testKaplamaTamirTestSonucRender;
	}

	/**
	 * @param testKaplamaTamirTestSonucRender
	 *            the testKaplamaTamirTestSonucRender to set
	 */
	public void setTestKaplamaTamirTestSonucRender(
			boolean testKaplamaTamirTestSonucRender) {
		this.testKaplamaTamirTestSonucRender = testKaplamaTamirTestSonucRender;
	}

	/**
	 * @return the testCKT1Render
	 */
	public boolean isTestCKT1Render() {
		return testCKT1Render;
	}

	/**
	 * @param testCKT1Render
	 *            the testCKT1Render to set
	 */
	public void setTestCKT1Render(boolean testCKT1Render) {
		this.testCKT1Render = testCKT1Render;
	}

	/**
	 * @return the testKaplamaKumlamaMalzemesiTuzMiktariSonucRender
	 */
	public boolean isTestKaplamaKumlamaMalzemesiTuzMiktariSonucRender() {
		return testKaplamaKumlamaMalzemesiTuzMiktariSonucRender;
	}

	/**
	 * @param testKaplamaKumlamaMalzemesiTuzMiktariSonucRender
	 *            the testKaplamaKumlamaMalzemesiTuzMiktariSonucRender to set
	 */
	public void setTestKaplamaKumlamaMalzemesiTuzMiktariSonucRender(
			boolean testKaplamaKumlamaMalzemesiTuzMiktariSonucRender) {
		this.testKaplamaKumlamaMalzemesiTuzMiktariSonucRender = testKaplamaKumlamaMalzemesiTuzMiktariSonucRender;
	}

	/**
	 * @return the testCAS1Render
	 */
	public boolean isTestCAS1Render() {
		return testCAS1Render;
	}

	/**
	 * @param testCAS1Render
	 *            the testCAS1Render to set
	 */
	public void setTestCAS1Render(boolean testCAS1Render) {
		this.testCAS1Render = testCAS1Render;
	}

	/**
	 * @return the testLAS1Render
	 */
	public boolean isTestLAS1Render() {
		return testLAS1Render;
	}

	/**
	 * @param testLAS1Render
	 *            the testLAS1Render to set
	 */
	public void setTestLAS1Render(boolean testLAS1Render) {
		this.testLAS1Render = testLAS1Render;
	}

	/**
	 * @return the testKaplamaProcessBozulmaSonucRender
	 */
	public boolean isTestKaplamaProcessBozulmaSonucRender() {
		return testKaplamaProcessBozulmaSonucRender;
	}

	/**
	 * @param testKaplamaProcessBozulmaSonucRender
	 *            the testKaplamaProcessBozulmaSonucRender to set
	 */
	public void setTestKaplamaProcessBozulmaSonucRender(
			boolean testKaplamaProcessBozulmaSonucRender) {
		this.testKaplamaProcessBozulmaSonucRender = testKaplamaProcessBozulmaSonucRender;
	}

	/**
	 * @return the testCPP1Render
	 */
	public boolean isTestCPP1Render() {
		return testCPP1Render;
	}

	/**
	 * @param testCPP1Render
	 *            the testCPP1Render to set
	 */
	public void setTestCPP1Render(boolean testCPP1Render) {
		this.testCPP1Render = testCPP1Render;
	}

	/**
	 * @return the testLPP1Render
	 */
	public boolean isTestLPP1Render() {
		return testLPP1Render;
	}

	/**
	 * @param testLPP1Render
	 *            the testLPP1Render to set
	 */
	public void setTestLPP1Render(boolean testLPP1Render) {
		this.testLPP1Render = testLPP1Render;
	}

	/**
	 * @return the testKaplamaKumlamaOrtamSartlariSonucRender
	 */
	public boolean isTestKaplamaKumlamaOrtamSartlariSonucRender() {
		return testKaplamaKumlamaOrtamSartlariSonucRender;
	}

	/**
	 * @param testKaplamaKumlamaOrtamSartlariSonucRender
	 *            the testKaplamaKumlamaOrtamSartlariSonucRender to set
	 */
	public void setTestKaplamaKumlamaOrtamSartlariSonucRender(
			boolean testKaplamaKumlamaOrtamSartlariSonucRender) {
		this.testKaplamaKumlamaOrtamSartlariSonucRender = testKaplamaKumlamaOrtamSartlariSonucRender;
	}

	/**
	 * @return the testCAC1Render
	 */
	public boolean isTestCAC1Render() {
		return testCAC1Render;
	}

	/**
	 * @param testCAC1Render
	 *            the testCAC1Render to set
	 */
	public void setTestCAC1Render(boolean testCAC1Render) {
		this.testCAC1Render = testCAC1Render;
	}

	/**
	 * @return the testLAC1Render
	 */
	public boolean isTestLAC1Render() {
		return testLAC1Render;
	}

	/**
	 * @param testLAC1Render
	 *            the testLAC1Render to set
	 */
	public void setTestLAC1Render(boolean testLAC1Render) {
		this.testLAC1Render = testLAC1Render;
	}

	/**
	 * @return the testKaplamaBetonOranBelirlenmesiSonucRender
	 */
	public boolean isTestKaplamaBetonOranBelirlenmesiSonucRender() {
		return testKaplamaBetonOranBelirlenmesiSonucRender;
	}

	/**
	 * @param testKaplamaBetonOranBelirlenmesiSonucRender
	 *            the testKaplamaBetonOranBelirlenmesiSonucRender to set
	 */
	public void setTestKaplamaBetonOranBelirlenmesiSonucRender(
			boolean testKaplamaBetonOranBelirlenmesiSonucRender) {
		this.testKaplamaBetonOranBelirlenmesiSonucRender = testKaplamaBetonOranBelirlenmesiSonucRender;
	}

	/**
	 * @return the testBSC1Render
	 */
	public boolean isTestBSC1Render() {
		return testBSC1Render;
	}

	/**
	 * @param testBSC1Render
	 *            the testBSC1Render to set
	 */
	public void setTestBSC1Render(boolean testBSC1Render) {
		this.testBSC1Render = testBSC1Render;
	}

	/**
	 * @return the testKaplamaBetonEgilmeBasincSonucRender
	 */
	public boolean isTestKaplamaBetonEgilmeBasincSonucRender() {
		return testKaplamaBetonEgilmeBasincSonucRender;
	}

	/**
	 * @param testKaplamaBetonEgilmeBasincSonucRender
	 *            the testKaplamaBetonEgilmeBasincSonucRender to set
	 */
	public void setTestKaplamaBetonEgilmeBasincSonucRender(
			boolean testKaplamaBetonEgilmeBasincSonucRender) {
		this.testKaplamaBetonEgilmeBasincSonucRender = testKaplamaBetonEgilmeBasincSonucRender;
	}

	/**
	 * @return the testLCB1Render
	 */
	public boolean isTestLCB1Render() {
		return testLCB1Render;
	}

	/**
	 * @param testLCB1Render
	 *            the testLCB1Render to set
	 */
	public void setTestLCB1Render(boolean testLCB1Render) {
		this.testLCB1Render = testLCB1Render;
	}

	/**
	 * @return the testLCC1Render
	 */
	public boolean isTestLCC1Render() {
		return testLCC1Render;
	}

	/**
	 * @param testLCC1Render
	 *            the testLCC1Render to set
	 */
	public void setTestLCC1Render(boolean testLCC1Render) {
		this.testLCC1Render = testLCC1Render;
	}

	/**
	 * @return the
	 *         testKaplamaKumlamaMalzemesiBoyutSekilOzellikKontroluSonucRender
	 */
	public boolean isTestKaplamaKumlamaMalzemesiBoyutSekilOzellikKontroluSonucRender() {
		return testKaplamaKumlamaMalzemesiBoyutSekilOzellikKontroluSonucRender;
	}

	/**
	 * @param testKaplamaKumlamaMalzemesiBoyutSekilOzellikKontroluSonucRender
	 *            the
	 *            testKaplamaKumlamaMalzemesiBoyutSekilOzellikKontroluSonucRender
	 *            to set
	 */
	public void setTestKaplamaKumlamaMalzemesiBoyutSekilOzellikKontroluSonucRender(
			boolean testKaplamaKumlamaMalzemesiBoyutSekilOzellikKontroluSonucRender) {
		this.testKaplamaKumlamaMalzemesiBoyutSekilOzellikKontroluSonucRender = testKaplamaKumlamaMalzemesiBoyutSekilOzellikKontroluSonucRender;
	}

	/**
	 * @return the testCKK1Render
	 */
	public boolean isTestCKK1Render() {
		return testCKK1Render;
	}

	/**
	 * @param testCKK1Render
	 *            the testCKK1Render to set
	 */
	public void setTestCKK1Render(boolean testCKK1Render) {
		this.testCKK1Render = testCKK1Render;
	}

	/**
	 * @return the testLKK1Render
	 */
	public boolean isTestLKK1Render() {
		return testLKK1Render;
	}

	/**
	 * @param testLKK1Render
	 *            the testLKK1Render to set
	 */
	public void setTestLKK1Render(boolean testLKK1Render) {
		this.testLKK1Render = testLKK1Render;
	}

	/**
	 * @return the testKaplamaDaldirmaTestiSonucRender
	 */
	public boolean isTestKaplamaDaldirmaTestiSonucRender() {
		return testKaplamaDaldirmaTestiSonucRender;
	}

	/**
	 * @param testKaplamaDaldirmaTestiSonucRender
	 *            the testKaplamaDaldirmaTestiSonucRender to set
	 */
	public void setTestKaplamaDaldirmaTestiSonucRender(
			boolean testKaplamaDaldirmaTestiSonucRender) {
		this.testKaplamaDaldirmaTestiSonucRender = testKaplamaDaldirmaTestiSonucRender;
	}

	/**
	 * @return the testLDT1Render
	 */
	public boolean isTestLDT1Render() {
		return testLDT1Render;
	}

	/**
	 * @param testLDT1Render
	 *            the testLDT1Render to set
	 */
	public void setTestLDT1Render(boolean testLDT1Render) {
		this.testLDT1Render = testLDT1Render;
	}

	/**
	 * @return the testCPP2Render
	 */
	public boolean isTestCPP2Render() {
		return testCPP2Render;
	}

	/**
	 * @param testCPP2Render
	 *            the testCPP2Render to set
	 */
	public void setTestCPP2Render(boolean testCPP2Render) {
		this.testCPP2Render = testCPP2Render;
	}

	/**
	 * @return the testCKT2Render
	 */
	public boolean isTestCKT2Render() {
		return testCKT2Render;
	}

	/**
	 * @param testCKT2Render
	 *            the testCKT2Render to set
	 */
	public void setTestCKT2Render(boolean testCKT2Render) {
		this.testCKT2Render = testCKT2Render;
	}

	/**
	 * @return the testKaplamaOnlineHolidaySonucRender
	 */
	public boolean isTestKaplamaOnlineHolidaySonucRender() {
		return testKaplamaOnlineHolidaySonucRender;
	}

	/**
	 * @param testKaplamaOnlineHolidaySonucRender
	 *            the testKaplamaOnlineHolidaySonucRender to set
	 */
	public void setTestKaplamaOnlineHolidaySonucRender(
			boolean testKaplamaOnlineHolidaySonucRender) {
		this.testKaplamaOnlineHolidaySonucRender = testKaplamaOnlineHolidaySonucRender;
	}

	/**
	 * @return the testOHT1Render
	 */
	public boolean isTestOHT1Render() {
		return testOHT1Render;
	}

	/**
	 * @param testOHT1Render
	 *            the testOHT1Render to set
	 */
	public void setTestOHT1Render(boolean testOHT1Render) {
		this.testOHT1Render = testOHT1Render;
	}

	/**
	 * @return the testEXC1Render
	 */
	public boolean isTestEXC1Render() {
		return testEXC1Render;
	}

	/**
	 * @param testEXC1Render
	 *            the testEXC1Render to set
	 */
	public void setTestEXC1Render(boolean testEXC1Render) {
		this.testEXC1Render = testEXC1Render;
	}

	/**
	 * @return the testKaplamaTozEpoksiXcutSonucRender
	 */
	public boolean isTestKaplamaTozEpoksiXcutSonucRender() {
		return testKaplamaTozEpoksiXcutSonucRender;
	}

	/**
	 * @param testKaplamaTozEpoksiXcutSonucRender
	 *            the testKaplamaTozEpoksiXcutSonucRender to set
	 */
	public void setTestKaplamaTozEpoksiXcutSonucRender(
			boolean testKaplamaTozEpoksiXcutSonucRender) {
		this.testKaplamaTozEpoksiXcutSonucRender = testKaplamaTozEpoksiXcutSonucRender;
	}

	/**
	 * @return the testFPT1Render
	 */
	public boolean isTestFPT1Render() {
		return testFPT1Render;
	}

	/**
	 * @param testFPT1Render
	 *            the testFPT1Render to set
	 */
	public void setTestFPT1Render(boolean testFPT1Render) {
		this.testFPT1Render = testFPT1Render;
	}

	/**
	 * @return the testFFT1Render
	 */
	public boolean isTestFFT1Render() {
		return testFFT1Render;
	}

	/**
	 * @param testFFT1Render
	 *            the testFFT1Render to set
	 */
	public void setTestFFT1Render(boolean testFFT1Render) {
		this.testFFT1Render = testFFT1Render;
	}

	/**
	 * @return the testFCT1Render
	 */
	public boolean isTestFCT1Render() {
		return testFCT1Render;
	}

	/**
	 * @param testFCT1Render
	 *            the testFCT1Render to set
	 */
	public void setTestFCT1Render(boolean testFCT1Render) {
		this.testFCT1Render = testFCT1Render;
	}

	/**
	 * @return the testFBT1Render
	 */
	public boolean isTestFBT1Render() {
		return testFBT1Render;
	}

	/**
	 * @param testFBT1Render
	 *            the testFBT1Render to set
	 */
	public void setTestFBT1Render(boolean testFBT1Render) {
		this.testFBT1Render = testFBT1Render;
	}

	/**
	 * @return the testFWT1Render
	 */
	public boolean isTestFWT1Render() {
		return testFWT1Render;
	}

	/**
	 * @param testFWT1Render
	 *            the testFWT1Render to set
	 */
	public void setTestFWT1Render(boolean testFWT1Render) {
		this.testFWT1Render = testFWT1Render;
	}

	/**
	 * @return the testFAT1Render
	 */
	public boolean isTestFAT1Render() {
		return testFAT1Render;
	}

	/**
	 * @param testFAT1Render
	 *            the testFAT1Render to set
	 */
	public void setTestFAT1Render(boolean testFAT1Render) {
		this.testFAT1Render = testFAT1Render;
	}

	/**
	 * @return the testFST1Render
	 */
	public boolean isTestFST1Render() {
		return testFST1Render;
	}

	/**
	 * @param testFST1Render
	 *            the testFST1Render to set
	 */
	public void setTestFST1Render(boolean testFST1Render) {
		this.testFST1Render = testFST1Render;
	}

	/**
	 * @return the testFBH1Render
	 */
	public boolean isTestFBH1Render() {
		return testFBH1Render;
	}

	/**
	 * @param testFBH1Render
	 *            the testFBH1Render to set
	 */
	public void setTestFBH1Render(boolean testFBH1Render) {
		this.testFBH1Render = testFBH1Render;
	}

	/**
	 * @return the testKaplamaSonrasiYuzeyKontrolRender
	 */
	public boolean isTestKaplamaSonrasiYuzeyKontrolRender() {
		return testKaplamaSonrasiYuzeyKontrolRender;
	}

	/**
	 * @param testKaplamaSonrasiYuzeyKontrolRender
	 *            the testKaplamaSonrasiYuzeyKontrolRender to set
	 */
	public void setTestKaplamaSonrasiYuzeyKontrolRender(
			boolean testKaplamaSonrasiYuzeyKontrolRender) {
		this.testKaplamaSonrasiYuzeyKontrolRender = testKaplamaSonrasiYuzeyKontrolRender;
	}

	/**
	 * @return the testBKK1Render
	 */
	public boolean isTestBKK1Render() {
		return testBKK1Render;
	}

	/**
	 * @param testBKK1Render
	 *            the testBKK1Render to set
	 */
	public void setTestBKK1Render(boolean testBKK1Render) {
		this.testBKK1Render = testBKK1Render;
	}

	/**
	 * @return the testLKT1Render
	 */
	public boolean isTestLKT1Render() {
		return testLKT1Render;
	}

	/**
	 * @param testLKT1Render
	 *            the testLKT1Render to set
	 */
	public void setTestLKT1Render(boolean testLKT1Render) {
		this.testLKT1Render = testLKT1Render;
	}

	/**
	 * @return the testPOY1Render
	 */
	public boolean isTestPOY1Render() {
		return testPOY1Render;
	}

	/**
	 * @param testPOY1Render
	 *            the testPOY1Render to set
	 */
	public void setTestPOY1Render(boolean testPOY1Render) {
		this.testPOY1Render = testPOY1Render;
	}

	/**
	 * @return the testKaplamaPulloffYapismaSonucRender
	 */
	public boolean isTestKaplamaPulloffYapismaSonucRender() {
		return testKaplamaPulloffYapismaSonucRender;
	}

	/**
	 * @param testKaplamaPulloffYapismaSonucRender
	 *            the testKaplamaPulloffYapismaSonucRender to set
	 */
	public void setTestKaplamaPulloffYapismaSonucRender(
			boolean testKaplamaPulloffYapismaSonucRender) {
		this.testKaplamaPulloffYapismaSonucRender = testKaplamaPulloffYapismaSonucRender;
	}

	/**
	 * @return the testKaplamaOncesiYuzeyKontrolRender
	 */
	public boolean isTestKaplamaOncesiYuzeyKontrolRender() {
		return testKaplamaOncesiYuzeyKontrolRender;
	}

	/**
	 * @param testKaplamaOncesiYuzeyKontrolRender
	 *            the testKaplamaOncesiYuzeyKontrolRender to set
	 */
	public void setTestKaplamaOncesiYuzeyKontrolRender(
			boolean testKaplamaOncesiYuzeyKontrolRender) {
		this.testKaplamaOncesiYuzeyKontrolRender = testKaplamaOncesiYuzeyKontrolRender;
	}

	/**
	 * @return the testKaplamaMpqtKaplamaKalinligiRender
	 */
	public boolean isTestKaplamaMpqtKaplamaKalinligiRender() {
		return TestKaplamaMpqtKaplamaKalinligiRender;
	}

	/**
	 * @param testKaplamaMpqtKaplamaKalinligiRender
	 *            the testKaplamaMpqtKaplamaKalinligiRender to set
	 */
	public void setTestKaplamaMpqtKaplamaKalinligiRender(
			boolean testKaplamaMpqtKaplamaKalinligiRender) {
		TestKaplamaMpqtKaplamaKalinligiRender = testKaplamaMpqtKaplamaKalinligiRender;
	}

	/**
	 * @return the testKaplamaFlowCoatBendSonucRender
	 */
	public boolean isTestKaplamaFlowCoatBendSonucRender() {
		return testKaplamaFlowCoatBendSonucRender;
	}

	/**
	 * @param testKaplamaFlowCoatBendSonucRender
	 *            the testKaplamaFlowCoatBendSonucRender to set
	 */
	public void setTestKaplamaFlowCoatBendSonucRender(
			boolean testKaplamaFlowCoatBendSonucRender) {
		this.testKaplamaFlowCoatBendSonucRender = testKaplamaFlowCoatBendSonucRender;
	}

	/**
	 * @return the testKaplamaFlowCoatBondSonucRender
	 */
	public boolean isTestKaplamaFlowCoatBondSonucRender() {
		return testKaplamaFlowCoatBondSonucRender;
	}

	/**
	 * @param testKaplamaFlowCoatBondSonucRender
	 *            the testKaplamaFlowCoatBondSonucRender to set
	 */
	public void setTestKaplamaFlowCoatBondSonucRender(
			boolean testKaplamaFlowCoatBondSonucRender) {
		this.testKaplamaFlowCoatBondSonucRender = testKaplamaFlowCoatBondSonucRender;
	}

	/**
	 * @return the testKaplamaFlowCoatBuchholzSonucRender
	 */
	public boolean isTestKaplamaFlowCoatBuchholzSonucRender() {
		return testKaplamaFlowCoatBuchholzSonucRender;
	}

	/**
	 * @param testKaplamaFlowCoatBuchholzSonucRender
	 *            the testKaplamaFlowCoatBuchholzSonucRender to set
	 */
	public void setTestKaplamaFlowCoatBuchholzSonucRender(
			boolean testKaplamaFlowCoatBuchholzSonucRender) {
		this.testKaplamaFlowCoatBuchholzSonucRender = testKaplamaFlowCoatBuchholzSonucRender;
	}

	/**
	 * @return the testKaplamaFlowCoatCureSonucRender
	 */
	public boolean isTestKaplamaFlowCoatCureSonucRender() {
		return testKaplamaFlowCoatCureSonucRender;
	}

	/**
	 * @param testKaplamaFlowCoatCureSonucRender
	 *            the testKaplamaFlowCoatCureSonucRender to set
	 */
	public void setTestKaplamaFlowCoatCureSonucRender(
			boolean testKaplamaFlowCoatCureSonucRender) {
		this.testKaplamaFlowCoatCureSonucRender = testKaplamaFlowCoatCureSonucRender;
	}

	/**
	 * @return the testKaplamaFlowCoatFilmThicknessSonucRender
	 */
	public boolean isTestKaplamaFlowCoatFilmThicknessSonucRender() {
		return testKaplamaFlowCoatFilmThicknessSonucRender;
	}

	/**
	 * @param testKaplamaFlowCoatFilmThicknessSonucRender
	 *            the testKaplamaFlowCoatFilmThicknessSonucRender to set
	 */
	public void setTestKaplamaFlowCoatFilmThicknessSonucRender(
			boolean testKaplamaFlowCoatFilmThicknessSonucRender) {
		this.testKaplamaFlowCoatFilmThicknessSonucRender = testKaplamaFlowCoatFilmThicknessSonucRender;
	}

	/**
	 * @return the testKaplamaFlowCoatPinholeSonucRender
	 */
	public boolean isTestKaplamaFlowCoatPinholeSonucRender() {
		return testKaplamaFlowCoatPinholeSonucRender;
	}

	/**
	 * @param testKaplamaFlowCoatPinholeSonucRender
	 *            the testKaplamaFlowCoatPinholeSonucRender to set
	 */
	public void setTestKaplamaFlowCoatPinholeSonucRender(
			boolean testKaplamaFlowCoatPinholeSonucRender) {
		this.testKaplamaFlowCoatPinholeSonucRender = testKaplamaFlowCoatPinholeSonucRender;
	}

	/**
	 * @return the testKaplamaFlowCoatStrippingSonucRender
	 */
	public boolean isTestKaplamaFlowCoatStrippingSonucRender() {
		return testKaplamaFlowCoatStrippingSonucRender;
	}

	/**
	 * @param testKaplamaFlowCoatStrippingSonucRender
	 *            the testKaplamaFlowCoatStrippingSonucRender to set
	 */
	public void setTestKaplamaFlowCoatStrippingSonucRender(
			boolean testKaplamaFlowCoatStrippingSonucRender) {
		this.testKaplamaFlowCoatStrippingSonucRender = testKaplamaFlowCoatStrippingSonucRender;
	}

	/**
	 * @return the testKaplamaFlowCoatWaterSonucRender
	 */
	public boolean isTestKaplamaFlowCoatWaterSonucRender() {
		return testKaplamaFlowCoatWaterSonucRender;
	}

	/**
	 * @param testKaplamaFlowCoatWaterSonucRender
	 *            the testKaplamaFlowCoatWaterSonucRender to set
	 */
	public void setTestKaplamaFlowCoatWaterSonucRender(
			boolean testKaplamaFlowCoatWaterSonucRender) {
		this.testKaplamaFlowCoatWaterSonucRender = testKaplamaFlowCoatWaterSonucRender;
	}

	/**
	 * @return the testKaplamaGasBlisteringSonucRender
	 */
	public boolean isTestKaplamaGasBlisteringSonucRender() {
		return TestKaplamaGasBlisteringSonucRender;
	}

	/**
	 * @param testKaplamaGasBlisteringSonucRender
	 *            the testKaplamaGasBlisteringSonucRender to set
	 */
	public void setTestKaplamaGasBlisteringSonucRender(
			boolean testKaplamaGasBlisteringSonucRender) {
		TestKaplamaGasBlisteringSonucRender = testKaplamaGasBlisteringSonucRender;
	}

	/**
	 * @return the testKaplamaHydraulicBlisteringSonucRender
	 */
	public boolean isTestKaplamaHydraulicBlisteringSonucRender() {
		return TestKaplamaHydraulicBlisteringSonucRender;
	}

	/**
	 * @param testKaplamaHydraulicBlisteringSonucRender
	 *            the testKaplamaHydraulicBlisteringSonucRender to set
	 */
	public void setTestKaplamaHydraulicBlisteringSonucRender(
			boolean testKaplamaHydraulicBlisteringSonucRender) {
		TestKaplamaHydraulicBlisteringSonucRender = testKaplamaHydraulicBlisteringSonucRender;
	}

	/**
	 * @return the testKaplamaThermalAgeingTestSonucRender
	 */
	public boolean isTestKaplamaThermalAgeingTestSonucRender() {
		return TestKaplamaThermalAgeingTestSonucRender;
	}

	/**
	 * @param testKaplamaThermalAgeingTestSonucRender
	 *            the testKaplamaThermalAgeingTestSonucRender to set
	 */
	public void setTestKaplamaThermalAgeingTestSonucRender(
			boolean testKaplamaThermalAgeingTestSonucRender) {
		TestKaplamaThermalAgeingTestSonucRender = testKaplamaThermalAgeingTestSonucRender;
	}

	/**
	 * @return the testKaplamaUvAgeingTestSonucRender
	 */
	public boolean isTestKaplamaUvAgeingTestSonucRender() {
		return TestKaplamaUvAgeingTestSonucRender;
	}

	/**
	 * @param testKaplamaUvAgeingTestSonucRender
	 *            the testKaplamaUvAgeingTestSonucRender to set
	 */
	public void setTestKaplamaUvAgeingTestSonucRender(
			boolean testKaplamaUvAgeingTestSonucRender) {
		TestKaplamaUvAgeingTestSonucRender = testKaplamaUvAgeingTestSonucRender;
	}

	/**
	 * @return the testKaplamaElektrikselDirencTestSonucRender
	 */
	public boolean isTestKaplamaElektrikselDirencTestSonucRender() {
		return TestKaplamaElektrikselDirencTestSonucRender;
	}

	/**
	 * @param testKaplamaElektrikselDirencTestSonucRender
	 *            the testKaplamaElektrikselDirencTestSonucRender to set
	 */
	public void setTestKaplamaElektrikselDirencTestSonucRender(
			boolean testKaplamaElektrikselDirencTestSonucRender) {
		TestKaplamaElektrikselDirencTestSonucRender = testKaplamaElektrikselDirencTestSonucRender;
	}

	/**
	 * @return the testKaplamaStorageConditionTestSonucRender
	 */
	public boolean isTestKaplamaStorageConditionTestSonucRender() {
		return TestKaplamaStorageConditionTestSonucRender;
	}

	/**
	 * @param testKaplamaStorageConditionTestSonucRender
	 *            the testKaplamaStorageConditionTestSonucRender to set
	 */
	public void setTestKaplamaStorageConditionTestSonucRender(
			boolean testKaplamaStorageConditionTestSonucRender) {
		TestKaplamaStorageConditionTestSonucRender = testKaplamaStorageConditionTestSonucRender;
	}

	/**
	 * @return the testKaplamaMixedWetPaintTestSonucRender
	 */
	public boolean isTestKaplamaMixedWetPaintTestSonucRender() {
		return TestKaplamaMixedWetPaintTestSonucRender;
	}

	/**
	 * @param testKaplamaMixedWetPaintTestSonucRender
	 *            the testKaplamaMixedWetPaintTestSonucRender to set
	 */
	public void setTestKaplamaMixedWetPaintTestSonucRender(
			boolean testKaplamaMixedWetPaintTestSonucRender) {
		TestKaplamaMixedWetPaintTestSonucRender = testKaplamaMixedWetPaintTestSonucRender;
	}

	/**
	 * @return the testLGBRender
	 */
	public boolean isTestLGBRender() {
		return testLGBRender;
	}

	/**
	 * @param testLGBRender
	 *            the testLGBRender to set
	 */
	public void setTestLGBRender(boolean testLGBRender) {
		this.testLGBRender = testLGBRender;
	}

	/**
	 * @return the testLHBRender
	 */
	public boolean isTestLHBRender() {
		return testLHBRender;
	}

	/**
	 * @param testLHBRender
	 *            the testLHBRender to set
	 */
	public void setTestLHBRender(boolean testLHBRender) {
		this.testLHBRender = testLHBRender;
	}

	/**
	 * @return the testCTYRender
	 */
	public boolean isTestCTYRender() {
		return testCTYRender;
	}

	/**
	 * @param testCTYRender
	 *            the testCTYRender to set
	 */
	public void setTestCTYRender(boolean testCTYRender) {
		this.testCTYRender = testCTYRender;
	}

	/**
	 * @return the testCUVRender
	 */
	public boolean isTestCUVRender() {
		return testCUVRender;
	}

	/**
	 * @param testCUVRender
	 *            the testCUVRender to set
	 */
	public void setTestCUVRender(boolean testCUVRender) {
		this.testCUVRender = testCUVRender;
	}

	/**
	 * @return the testCEDRender
	 */
	public boolean isTestCEDRender() {
		return testCEDRender;
	}

	/**
	 * @param testCEDRender
	 *            the testCEDRender to set
	 */
	public void setTestCEDRender(boolean testCEDRender) {
		this.testCEDRender = testCEDRender;
	}

	/**
	 * @return the testCSTRender
	 */
	public boolean isTestCSTRender() {
		return testCSTRender;
	}

	/**
	 * @param testCSTRender
	 *            the testCSTRender to set
	 */
	public void setTestCSTRender(boolean testCSTRender) {
		this.testCSTRender = testCSTRender;
	}

	/**
	 * @return the testLMPRender
	 */
	public boolean isTestLMPRender() {
		return testLMPRender;
	}

	/**
	 * @param testLMPRender
	 *            the testLMPRender to set
	 */
	public void setTestLMPRender(boolean testLMPRender) {
		this.testLMPRender = testLMPRender;
	}

}
