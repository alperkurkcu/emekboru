package com.emekboru.beans.sales.customer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.emekboru.beans.sales.SalesMenuBean;
import com.emekboru.jpa.sales.customer.SalesContactCompany;
import com.emekboru.jpa.sales.customer.SalesCustomerFunctionType;
import com.emekboru.jpa.sales.customer.SalesJoinContactCompanyFunctionType;
import com.emekboru.jpaman.sales.customer.SalesContactCompanyManager;
import com.emekboru.jpaman.sales.customer.SalesJoinContactCompanyFunctionTypeManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "salesContactCompanyBean")
@ViewScoped
public class SalesContactCompanyBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1893094289385194467L;

	@ManagedProperty(value = "#{salesMenuBean}")
	private SalesMenuBean salesMenuBean;

	private SalesContactCompany newContact = new SalesContactCompany();
	private SalesContactCompany selectedContact;

	private List<SalesContactCompany> contactList;
	private List<SalesContactCompany> filteredList;
	private List<SalesContactCompany> contactsOfCustomer;

	public SalesContactCompanyBean() {
		newContact = new SalesContactCompany();
		selectedContact = new SalesContactCompany();
	}

	@PostConstruct
	public void load() {
		System.out.println("SalesContactCompanyBean.load()");

		try {
			SalesContactCompanyManager manager = new SalesContactCompanyManager();
			contactList = manager.findAll(SalesContactCompany.class);

			/* JOIN FUNCTIONS */
			for (SalesContactCompany contact : contactList) {
				this.loadJoinContactCompanyFunctionType(contact);
			}
			/* end of JOIN FUNCTIONS */
			selectedContact = contactList.get(0);
		} catch (Exception e) {
			System.out.println("SalesContactCompanyBean:"+e.toString());
		}
	}

	public void add() {
		System.out.println("SalesContactCompanyBean.add()");

		try {
			SalesContactCompanyManager manager = new SalesContactCompanyManager();

			contactList.add(newContact);
			/* JOIN FUNCTIONS */
			this.addJoinContactCompanyFunctionType(newContact);
			/* end of JOIN FUNCTIONS */
			manager.enterNew(newContact);

			newContact = new SalesContactCompany();
			FacesContextUtils.addInfoMessage("SubmitMessage");
		} catch (Exception e) {
			System.out.println("SalesContactCompanyBean:"+e.toString());
		}
	}

	public void update() {
		System.out.println("SalesContactCompanyBean.update()");

		try {
			SalesContactCompanyManager manager = new SalesContactCompanyManager();

			/* JOIN FUNCTIONS */
			this.deleteJoinContactCompanyFunctionType(selectedContact);
			this.addJoinContactCompanyFunctionType(selectedContact);
			/* end of JOIN FUNCTIONS */
			manager.updateEntity(selectedContact);

			FacesContextUtils.addInfoMessage("UpdateItemMessage");
		} catch (Exception e) {
			System.out.println("SalesContactCompanyBean:"+e.toString());
		}
	}

	public void delete() {
		System.out.println("SalesContactCompanyBean.delete()");

		try {
			SalesContactCompanyManager manager = new SalesContactCompanyManager();
			/* JOIN FUNCTIONS */
			this.deleteJoinContactCompanyFunctionType(selectedContact);
			/* end of JOIN FUNCTIONS */
			manager.delete(selectedContact);
			contactList.remove(selectedContact);
			selectedContact = contactList.get(0);
			FacesContextUtils.addWarnMessage("DeletedMessage");
		} catch (Exception e) {
			System.out.println("SalesContactCompanyBean:"+e.toString());
		}
	}

	/* JOIN FUNCTIONS */
	private void addJoinContactCompanyFunctionType(SalesContactCompany e) {
		SalesJoinContactCompanyFunctionTypeManager joinManager = new SalesJoinContactCompanyFunctionTypeManager();
		SalesJoinContactCompanyFunctionType newJoin;

		for (int i = 0; i < e.getFunctionTypeList().size(); i++) {
			newJoin = new SalesJoinContactCompanyFunctionType();
			newJoin.setFunctionType(e.getFunctionTypeList().get(i));
			newJoin.setContactCompany(e);
			joinManager.enterNew(newJoin);
		}
	}

	private void loadJoinContactCompanyFunctionType(SalesContactCompany contact) {
		SalesJoinContactCompanyFunctionTypeManager joinManager = new SalesJoinContactCompanyFunctionTypeManager();
		List<SalesJoinContactCompanyFunctionType> joinList = joinManager
				.findAll(SalesJoinContactCompanyFunctionType.class);

		contact.setFunctionTypeList(new ArrayList<SalesCustomerFunctionType>());

		for (int i = 0; i < joinList.size(); i++) {
			if (joinList.get(i).getContactCompany().equals(contact)) {
				contact.getFunctionTypeList().add(
						joinList.get(i).getFunctionType());
			}
		}
	}

	private void deleteJoinContactCompanyFunctionType(
			SalesContactCompany contact) {
		SalesJoinContactCompanyFunctionTypeManager joinManager = new SalesJoinContactCompanyFunctionTypeManager();
		List<SalesJoinContactCompanyFunctionType> joinList = joinManager
				.findAll(SalesJoinContactCompanyFunctionType.class);

		for (int i = 0; i < joinList.size(); i++) {
			if (joinList.get(i).getContactCompany().equals(contact)) {
				joinManager.delete(joinList.get(i));
			}
		}
	}

	/* end of JOIN FUNCTIONS */
	/*
	 * GETTERS AND SETTERS
	 */

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public SalesContactCompany getNewContact() {
		return newContact;
	}

	public void setNewContact(SalesContactCompany newContact) {
		this.newContact = newContact;
	}

	public SalesContactCompany getSelectedContact() {
		try {
			if (selectedContact == null)
				selectedContact = new SalesContactCompany();
		} catch (Exception ex) {
			selectedContact = new SalesContactCompany();
		}
		return selectedContact;
	}

	public void setSelectedContact(SalesContactCompany selectedContact) {
		this.selectedContact = selectedContact;
	}

	public List<SalesContactCompany> getContactList() {
		return contactList;
	}

	public void setContactList(List<SalesContactCompany> contactList) {
		this.contactList = contactList;
	}

	public List<SalesContactCompany> getFilteredList() {
		try {
			selectedContact = filteredList.get(0);
			salesMenuBean.showNewType1Menu();
		} catch (Exception ex) {
			System.out.println("getFilteredList: " + ex);
		}
		return filteredList;
	}

	public void setFilteredList(List<SalesContactCompany> filteredList) {
		this.filteredList = filteredList;
	}

	public SalesMenuBean getSalesMenuBean() {
		return salesMenuBean;
	}

	public void setSalesMenuBean(SalesMenuBean salesMenuBean) {
		this.salesMenuBean = salesMenuBean;
	}

	public List<SalesContactCompany> getContactsOfCustomer() {
		try {
			contactsOfCustomer = new ArrayList<SalesContactCompany>();

			for (int i = 0; i < contactList.size(); i++) {
				contactsOfCustomer.add(contactList.get(i));
			}
		} catch (Exception e) {
			System.out.println("SalesContactCompanyBean:"+e.toString());
		}
		return contactsOfCustomer;
	}

	public void setContactsOfCustomer(
			List<SalesContactCompany> contactsOfCustomer) {
		this.contactsOfCustomer = contactsOfCustomer;
	}
}
