/**
 * 
 */
package com.emekboru.beans.isemirleri;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.emekboru.beans.istakipformu.IsTakipFormuBetonKaplamaBean;
import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.isemri.IsEmriBetonKaplama;
import com.emekboru.jpa.kaplamatestspecs.KaplamaKaliteCutBackKoruma;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.CoatRawMaterialManager;
import com.emekboru.jpaman.isemirleri.IsEmriBetonKaplamaManager;
import com.emekboru.jpaman.kaplamatest.KaplamaKaliteCutBackKorumaManager;
import com.emekboru.jpaman.sales.order.SalesItemManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isEmriBetonKaplamaBean")
@ViewScoped
public class IsEmriBetonKaplamaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private IsEmriBetonKaplama isEmriBetonKaplamaForm = new IsEmriBetonKaplama();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private List<CoatRawMaterial> allBetonKaplamaMalzemeList = new ArrayList<CoatRawMaterial>();

	@ManagedProperty(value = "#{isTakipFormuBetonKaplamaBean}")
	private IsTakipFormuBetonKaplamaBean isTakipFormuBetonKaplamaBean;

	// kaplama kalite
	private List<KaplamaKaliteCutBackKoruma> kaplamaKaliteCutBackKorumaList = new ArrayList<KaplamaKaliteCutBackKoruma>();
	private String cutBack = null;

	public IsEmriBetonKaplamaBean() {

	}

	public void addOrUpdateIsEmriBetonKaplama(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmItemId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		SalesItem salesItem = new SalesItemManager().loadObject(
				SalesItem.class, prmItemId);

		IsEmriBetonKaplamaManager isMan = new IsEmriBetonKaplamaManager();

		if (isEmriBetonKaplamaForm.getId() == null) {

			isEmriBetonKaplamaForm.setSalesItem(salesItem);
			isEmriBetonKaplamaForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			isEmriBetonKaplamaForm.setEkleyenEmployee(userBean.getUser());

			isMan.enterNew(isEmriBetonKaplamaForm);

			// iç kumlama iş takip formu ekleme
			isTakipFormuBetonKaplamaBean.addFromIsEmri(salesItem);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"BETON KAPLAMA İŞ EMRİ BAŞARIYLA EKLENMİŞTİR!"));
		} else {

			isEmriBetonKaplamaForm.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			isEmriBetonKaplamaForm.setGuncelleyenEmployee(userBean.getUser());
			isMan.updateEntity(isEmriBetonKaplamaForm);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_WARN,
					"BETON KAPLAMA İŞ EMRİ BAŞARIYLA GÜNCELLENMİŞTİR!", null));
		}
	}

	public void deleteIsEmriBetonKaplama() {

		IsEmriBetonKaplamaManager isMan = new IsEmriBetonKaplamaManager();

		if (isEmriBetonKaplamaForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		isMan.delete(isEmriBetonKaplamaForm);
		isEmriBetonKaplamaForm = new IsEmriBetonKaplama();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
				"BETON KAPLAMA İŞ EMRİ BAŞARIYLA SİLİNMİŞTİR!", null));
	}

	@SuppressWarnings("static-access")
	public void loadIsEmri(Integer itemId) {

		IsEmriBetonKaplamaManager isMan = new IsEmriBetonKaplamaManager();
		KaplamaKaliteCutBackKorumaManager cutMan = new KaplamaKaliteCutBackKorumaManager();

		if (isMan.getAllIsEmriBetonKaplama(itemId).size() > 0) {
			isEmriBetonKaplamaForm = new IsEmriBetonKaplamaManager()
					.getAllIsEmriBetonKaplama(itemId).get(0);
		} else {
			isEmriBetonKaplamaForm = new IsEmriBetonKaplama();
		}

		kaplamaKaliteCutBackKorumaList = cutMan
				.getAllKaplamaKaliteCutBackKoruma(isEmriBetonKaplamaForm
						.getSalesItem().getItemId());
		cutBack = kaplamaKaliteCutBackKorumaList.get(0).getAciklama();
	}

	public void isEmriMalzemeEkle() {

		IsEmriBetonKaplamaManager isMan = new IsEmriBetonKaplamaManager();

		isMan.updateEntity(isEmriBetonKaplamaForm);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
				"BETON KAPLAMA MALZEMESİ BAŞARIYLA EKLENMİŞTİR!", null));
	}

	public void loadKumlamaMalzemeleri() {

		CoatRawMaterialManager materialMan = new CoatRawMaterialManager();
		// 7 beton turudur - coat_material_type tablosundan
		allBetonKaplamaMalzemeList = materialMan
				.findUnfinishedCoatMaterialByType(7);
	}

	// setters getters

	public IsEmriBetonKaplama getIsEmriBetonKaplamaForm() {
		return isEmriBetonKaplamaForm;
	}

	public void setIsEmriBetonKaplamaForm(
			IsEmriBetonKaplama isEmriBetonKaplamaForm) {
		this.isEmriBetonKaplamaForm = isEmriBetonKaplamaForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public List<CoatRawMaterial> getAllBetonKaplamaMalzemeList() {
		return allBetonKaplamaMalzemeList;
	}

	public void setAllBetonKaplamaMalzemeList(
			List<CoatRawMaterial> allBetonKaplamaMalzemeList) {
		this.allBetonKaplamaMalzemeList = allBetonKaplamaMalzemeList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public IsTakipFormuBetonKaplamaBean getIsTakipFormuBetonKaplamaBean() {
		return isTakipFormuBetonKaplamaBean;
	}

	public void setIsTakipFormuBetonKaplamaBean(
			IsTakipFormuBetonKaplamaBean isTakipFormuBetonKaplamaBean) {
		this.isTakipFormuBetonKaplamaBean = isTakipFormuBetonKaplamaBean;
	}

	/**
	 * @return the cutBack
	 */
	public String getCutBack() {
		return cutBack;
	}

	/**
	 * @param cutBack
	 *            the cutBack to set
	 */
	public void setCutBack(String cutBack) {
		this.cutBack = cutBack;
	}

}
