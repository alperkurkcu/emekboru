package com.emekboru.beans.pipe;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.UploadedFile;

@ManagedBean(name = "fileUploadForEmployeeBean")
@ViewScoped
public class FileUploadForEmployeeBean {
	private UploadedFile file;

	public FileUploadForEmployeeBean() {
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

}
