/**
 * 
 */
package com.emekboru.beans.satinalma;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.satinalma.SatinalmaTakipDosyalar;
import com.emekboru.jpaman.satinalma.SatinalmaTakipDosyalarManager;

/**
 * @author Alper K
 * 
 */

@ManagedBean(name = "satinalmaTakipDosyalarBean")
@ViewScoped
public class SatinalmaTakipDosyalarBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<SatinalmaTakipDosyalar> allSatinalmaTakipDosyalarList = new ArrayList<SatinalmaTakipDosyalar>();
	private List<SatinalmaTakipDosyalar> personalSatinalmaTakipDosyalarList = new ArrayList<SatinalmaTakipDosyalar>();

	private SatinalmaTakipDosyalar satinalmaTakipDosyalar = new SatinalmaTakipDosyalar();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	@PostConstruct
	public void load() {

		fillTestList();
	}

	public void fillTestList() {

		SatinalmaTakipDosyalarManager manager = new SatinalmaTakipDosyalarManager();
		allSatinalmaTakipDosyalarList = manager.getAllSatinalmaTakipDosyalar();
		personalSatinalmaTakipDosyalarList = manager
				.getPersonalSatinalmaTakipDosyalar(userBean.getUser().getId());
		satinalmaTakipDosyalar = new SatinalmaTakipDosyalar();
		updateButtonRender = false;
	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	// setters getters

	/**
	 * @return the allSatinalmaTakipDosyalarList
	 */
	public List<SatinalmaTakipDosyalar> getAllSatinalmaTakipDosyalarList() {
		return allSatinalmaTakipDosyalarList;
	}

	/**
	 * @param allSatinalmaTakipDosyalarList
	 *            the allSatinalmaTakipDosyalarList to set
	 */
	public void setAllSatinalmaTakipDosyalarList(
			List<SatinalmaTakipDosyalar> allSatinalmaTakipDosyalarList) {
		this.allSatinalmaTakipDosyalarList = allSatinalmaTakipDosyalarList;
	}

	/**
	 * @return the satinalmaTakipDosyalar
	 */
	public SatinalmaTakipDosyalar getSatinalmaTakipDosyalar() {
		return satinalmaTakipDosyalar;
	}

	/**
	 * @param satinalmaTakipDosyalar
	 *            the satinalmaTakipDosyalar to set
	 */
	public void setSatinalmaTakipDosyalar(
			SatinalmaTakipDosyalar satinalmaTakipDosyalar) {
		this.satinalmaTakipDosyalar = satinalmaTakipDosyalar;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the personalSatinalmaTakipDosyalarList
	 */
	public List<SatinalmaTakipDosyalar> getPersonalSatinalmaTakipDosyalarList() {
		return personalSatinalmaTakipDosyalarList;
	}

	/**
	 * @param personalSatinalmaTakipDosyalarList
	 *            the personalSatinalmaTakipDosyalarList to set
	 */
	public void setPersonalSatinalmaTakipDosyalarList(
			List<SatinalmaTakipDosyalar> personalSatinalmaTakipDosyalarList) {
		this.personalSatinalmaTakipDosyalarList = personalSatinalmaTakipDosyalarList;
	}
}
