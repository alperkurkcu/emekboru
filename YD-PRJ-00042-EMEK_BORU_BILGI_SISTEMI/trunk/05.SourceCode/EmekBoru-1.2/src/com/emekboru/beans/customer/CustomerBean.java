package com.emekboru.beans.customer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.persistence.EntityManager;

import com.emekboru.jpa.Customer;
import com.emekboru.jpa.ElectrodeDustWire;
import com.emekboru.jpa.Order;
import com.emekboru.jpa.Ulkeler;
import com.emekboru.jpa.rulo.Rulo;
import com.emekboru.jpaman.CustomerManager;
import com.emekboru.jpaman.ElectrodeDustWireManager;
import com.emekboru.jpaman.Factory;
import com.emekboru.jpaman.OrderManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "customerBean")
@ViewScoped
public class CustomerBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Customer currentCustomer;
	private Customer newCustomer;
	private Order selectedOrder;
	private List<Order> orderItemList;
	private Order selectedOrderItem;

	// private List<Coil> boughtCoilList;
	private List<Rulo> boughtRuloList;
	private List<ElectrodeDustWire> electrodeList;
	private List<ElectrodeDustWire> dustList;
	private List<ElectrodeDustWire> wireList;

	@ManagedProperty(value = "#{customerListBean}")
	private CustomerListBean injectedCustomerBean;

	public CustomerBean() {

		currentCustomer = new Customer();
		newCustomer = new Customer();
		selectedOrder = new Order();
		orderItemList = new ArrayList<Order>();
		selectedOrderItem = new Order();
		// boughtCoilList = new ArrayList<Coil>();
		boughtRuloList = new ArrayList<Rulo>();
		electrodeList = new ArrayList<ElectrodeDustWire>();
		dustList = new ArrayList<ElectrodeDustWire>();
		wireList = new ArrayList<ElectrodeDustWire>();
	}

	public void addCompany() {

		System.out.println("CompanyBean.addCompany()");
		CustomerManager companyManager = new CustomerManager();
		List<Customer> companyList = (List<Customer>) companyManager
				.findByField(Customer.class, "name", newCustomer.getName());
		// check if there is a company with the same name
		if (companyList.size() > 0) {
			newCustomer.setName("");
			FacesContextUtils.addErrorMessage("ItemExistMessage");
			return;
		}

		// add the country to the company
		EntityManager manager = Factory.getInstance().createEntityManager();
		Ulkeler ulke = manager.find(Ulkeler.class, newCustomer.getUlkeler()
				.getId());
		manager.close();
		newCustomer.setUlkeler(ulke);
		// add the company and reload the company LIST
		companyManager.enterNew(newCustomer);
		newCustomer = new Customer();
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void loadCompanyList() {

		injectedCustomerBean.loadCustomers();
	}

	public void loadOrderItems(ActionEvent event) {

		EntityManager man = Factory.getInstance().createEntityManager();
		currentCustomer = man.find(Customer.class,
				currentCustomer.getCustomerId());
		man.getTransaction().begin();
		if (currentCustomer.getOrders().size() == 0) {

			FacesContextUtils.addWarnMessage("NoItemsMessage");
			return;
		}
		man.getTransaction().commit();
		man.close();
	}

	public void updateCompany() {

		CustomerManager companyManager = new CustomerManager();
		List<Customer> companyList = (List<Customer>) companyManager
				.findByField(Customer.class, "name", currentCustomer.getName());

		if (companyList.size() > 0
				&& companyList.get(0).getCustomerId() != currentCustomer
						.getCustomerId()) {
			currentCustomer = (Customer) companyManager.findByField(
					Customer.class, "customerId",
					currentCustomer.getCustomerId()).get(0);
			FacesContextUtils.addErrorMessage("ItemExistMessage");
			return;
		}
		companyManager.updateEntity(currentCustomer);
		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}

	public void deleteCompany() {

		CustomerManager companyManager = new CustomerManager();
		List<Customer> companyList = (List<Customer>) companyManager
				.findByField(Customer.class, "customerId",
						currentCustomer.getCustomerId());

		if (companyList.size() == 0) {
			FacesContextUtils.addErrorMessage("NoExistingItemMessage");
			return;
		}
		OrderManager orderManager = new OrderManager();
		List<Order> orderList = (List<Order>) orderManager.findByField(
				Order.class, "customerId", currentCustomer.getCustomerId());
		if (orderList.size() > 0) {
			FacesContextUtils.addErrorMessage("OrderRelatedMessage");
			return;
		}

		ElectrodeDustWireManager edwManager = new ElectrodeDustWireManager();
		List<ElectrodeDustWire> edwList = (List<ElectrodeDustWire>) edwManager
				.findByField(ElectrodeDustWire.class, "customerId",
						currentCustomer.getCustomerId());
		if (edwList.size() > 0) {
			FacesContextUtils.addErrorMessage("EdwRelatedMessage");
			return;
		}

		companyManager.deleteByField(Customer.class, "companyId",
				currentCustomer.getCustomerId());
		currentCustomer = new Customer();
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Order getSelectedOrder() {
		return selectedOrder;
	}

	public void setSelectedOrder(Order selectedOrder) {
		this.selectedOrder = selectedOrder;
	}

	public List<Order> getOrderItemList() {
		return orderItemList;
	}

	public void setOrderItemList(List<Order> orderItemList) {
		this.orderItemList = orderItemList;
	}

	public Order getSelectedOrderItem() {
		return selectedOrderItem;
	}

	public void setSelectedOrderItem(Order selectedOrderItem) {
		this.selectedOrderItem = selectedOrderItem;
	}

	/*
	 * public List<Coil> getBoughtCoilList() { return boughtCoilList; }
	 * 
	 * public void setBoughtCoilList(List<Coil> boughtCoilList) {
	 * this.boughtCoilList = boughtCoilList; }
	 */
	public List<ElectrodeDustWire> getElectrodeList() {
		return electrodeList;
	}

	public void setElectrodeList(List<ElectrodeDustWire> electrodeList) {
		this.electrodeList = electrodeList;
	}

	public List<ElectrodeDustWire> getDustList() {
		return dustList;
	}

	public void setDustList(List<ElectrodeDustWire> dustList) {
		this.dustList = dustList;
	}

	public List<ElectrodeDustWire> getWireList() {
		return wireList;
	}

	public void setWireList(List<ElectrodeDustWire> wireList) {
		this.wireList = wireList;
	}

	public Customer getCurrentCustomer() {
		return currentCustomer;
	}

	public void setCurrentCustomer(Customer currentCustomer) {
		this.currentCustomer = currentCustomer;
	}

	public Customer getNewCustomer() {
		return newCustomer;
	}

	public void setNewCustomer(Customer newCustomer) {
		this.newCustomer = newCustomer;
	}

	public CustomerListBean getInjectedCustomerBean() {
		return injectedCustomerBean;
	}

	public void setInjectedCustomerBean(CustomerListBean injectedCustomerBean) {
		this.injectedCustomerBean = injectedCustomerBean;
	}

	public List<Rulo> getBoughtRuloList() {
		return boughtRuloList;
	}

	public void setBoughtRuloList(List<Rulo> boughtRuloList) {
		this.boughtRuloList = boughtRuloList;
	}

}
