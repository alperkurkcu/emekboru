package com.emekboru.beans.sales.order;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.sales.order.SalesProposalLanguage;
import com.emekboru.jpaman.sales.order.SalesProposalLanguageManager;


@ManagedBean(name = "salesProposalLanguageBean")
@ViewScoped
public class SalesProposalLanguageBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5930477171571608412L;
	private List<SalesProposalLanguage> proposalLanguageList;

	public SalesProposalLanguageBean() {

		SalesProposalLanguageManager manager = new SalesProposalLanguageManager();
		proposalLanguageList = manager
				.findAll(SalesProposalLanguage.class);
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public List<SalesProposalLanguage> getProposalLanguageList() {
		return proposalLanguageList;
	}

	public void setProposalLanguageList(
			List<SalesProposalLanguage> proposalLanguageList) {
		this.proposalLanguageList = proposalLanguageList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}