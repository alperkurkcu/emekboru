/**
 * 
 */
package com.emekboru.beans.quality;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.kaplamatestspecs.KaliteKaplamaMarkalama;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "kaliteKaplamaMarkalamaBean")
@ViewScoped
public class KaliteKaplamaMarkalamaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private KaliteKaplamaMarkalama kaliteKaplamaMarkalamaForm = new KaliteKaplamaMarkalama();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private List<KaliteKaplamaMarkalama> allKaliteKaplamaMarkalamaList = new ArrayList<KaliteKaplamaMarkalama>();

	// setters getters
	/**
	 * @return the kaliteKaplamaMarkalamaForm
	 */
	public KaliteKaplamaMarkalama getKaliteKaplamaMarkalamaForm() {
		return kaliteKaplamaMarkalamaForm;
	}

	/**
	 * @param kaliteKaplamaMarkalamaForm
	 *            the kaliteKaplamaMarkalamaForm to set
	 */
	public void setKaliteKaplamaMarkalamaForm(
			KaliteKaplamaMarkalama kaliteKaplamaMarkalamaForm) {
		this.kaliteKaplamaMarkalamaForm = kaliteKaplamaMarkalamaForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the allKaliteKaplamaMarkalamaList
	 */
	public List<KaliteKaplamaMarkalama> getAllKaliteKaplamaMarkalamaList() {
		return allKaliteKaplamaMarkalamaList;
	}

	/**
	 * @param allKaliteKaplamaMarkalamaList
	 *            the allKaliteKaplamaMarkalamaList to set
	 */
	public void setAllKaliteKaplamaMarkalamaList(
			List<KaliteKaplamaMarkalama> allKaliteKaplamaMarkalamaList) {
		this.allKaliteKaplamaMarkalamaList = allKaliteKaplamaMarkalamaList;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
