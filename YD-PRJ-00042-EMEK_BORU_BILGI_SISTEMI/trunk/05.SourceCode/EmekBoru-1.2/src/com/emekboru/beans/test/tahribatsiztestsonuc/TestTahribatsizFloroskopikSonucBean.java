/**
 * 
 */
package com.emekboru.beans.test.tahribatsiztestsonuc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.test.tahribatsiz.TahribatsizTestOrderListBean;
import com.emekboru.jpa.NondestructiveTestsSpec;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.kalibrasyon.KalibrasyonFloroskopik;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizFloroskopikSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizManuelUtSonuc;
import com.emekboru.jpaman.NondestructiveTestsSpecManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.kalibrasyon.KalibrasyonFloroskopikManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizFloroskopikSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizGozOlcuSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizKoordinatManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizManuelUtSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizTamirManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */

@ManagedBean(name = "testTahribatsizFloroskopikSonucBean")
@ViewScoped
public class TestTahribatsizFloroskopikSonucBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<TestTahribatsizFloroskopikSonuc> allTestTahribatsizFloroskopikSonucList = new ArrayList<TestTahribatsizFloroskopikSonuc>();
	private TestTahribatsizFloroskopikSonuc testTahribatsizFloroskopikSonucForm = new TestTahribatsizFloroskopikSonuc();
	private TestTahribatsizManuelUtSonuc testTahribatsizManuelUtSonucForm = new TestTahribatsizManuelUtSonuc();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private boolean visibleButtonRender;

	@ManagedProperty(value = "#{testTahribatsizTamirBean}")
	private TestTahribatsizTamirBean tamirBean;

	@ManagedProperty(value = "#{testTahribatsizTornaBean}")
	private TestTahribatsizTornaBean tornaBean;

	@ManagedProperty(value = "#{testTahribatsizHidrostatikBean}")
	private TestTahribatsizHidrostatikBean hidroBean;

	@ManagedProperty(value = "#{testTahribatsizGozOlcuSonucBean}")
	private TestTahribatsizGozOlcuSonucBean gozOlcuBean;

	private Pipe selectedPipe = new Pipe();

	private String barkodNo = null;

	private NondestructiveTestsSpec selectedNonTest = new NondestructiveTestsSpec();

	private List<KalibrasyonFloroskopik> kalibrasyonlar = new ArrayList<KalibrasyonFloroskopik>();
	private KalibrasyonFloroskopik kalibrasyon = new KalibrasyonFloroskopik();

	private Boolean tamirKabulKontrol = false;

	public TestTahribatsizFloroskopikSonucBean() {

		visibleButtonRender = false;
	}

	public void addOrUpdateFloroskopikSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmTestId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		TestTahribatsizFloroskopikSonucManager floroskopikSonucManager = new TestTahribatsizFloroskopikSonucManager();
		TestTahribatsizKoordinatManager koordinatManager = new TestTahribatsizKoordinatManager();

		if (testTahribatsizFloroskopikSonucForm.getId() == null) {

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);

			// aynı koordinatı eklememe işlemi
			for (int i = 0; i < allTestTahribatsizFloroskopikSonucList.size(); i++) {
				if ((testTahribatsizFloroskopikSonucForm.getKoordinatQ()
						.equals(allTestTahribatsizFloroskopikSonucList.get(i)
								.getKoordinatQ()))
						&& (testTahribatsizFloroskopikSonucForm.getKoordinatL()
								.equals(allTestTahribatsizFloroskopikSonucList
										.get(i).getKoordinatL()))) {
					System.out
							.println(" testTahribatsizFloroskopikSonucBean.addOrUpdateFloroskopikSonuc-HATA SameCoordinates");
					FacesContext context = FacesContext.getCurrentInstance();
					context.addMessage(
							null,
							new FacesMessage(
									FacesMessage.SEVERITY_FATAL,
									"AYNI KOORDİNAT TEKRARDAN EKLENEMEZ, KAYDETME HATASI!",
									null));
					return;
				}
			}

			testTahribatsizFloroskopikSonucForm.setTestId(prmTestId);
			testTahribatsizFloroskopikSonucForm.setPipe(pipe);
			testTahribatsizFloroskopikSonucForm.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			testTahribatsizFloroskopikSonucForm.setUser(userBean.getUser());

			sonKalibrasyonuGoster();
			testTahribatsizFloroskopikSonucForm.setKalibrasyon(kalibrasyon);

			// floroskopik ekleme
			floroskopikSonucManager
					.enterNew(testTahribatsizFloroskopikSonucForm);

			// tüm diğer muayenelere koordinatı ekleme
			koordinatManager.tumTablolaraKoordinatEkleme(
					testTahribatsizFloroskopikSonucForm.getKoordinatQ(),
					testTahribatsizFloroskopikSonucForm.getKoordinatL(), pipe,
					"f", userBean,
					testTahribatsizFloroskopikSonucForm.getKaynakIslemi(), "",
					testTahribatsizFloroskopikSonucForm.getTamirKesim());

			this.testTahribatsizFloroskopikSonucForm = new TestTahribatsizFloroskopikSonuc();

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"FLOROSKOPİK VERİSİ BAŞARI İLE EKLENMİŞTİR!"));
		} else {

			// tamiri kabul eden kullanıcı verileri
			// (kabul ve seçiniz) ya da (red ve seçiniz)
			if ((testTahribatsizFloroskopikSonucForm.getResult() == 1 && tamirKabulKontrol)
					|| (testTahribatsizFloroskopikSonucForm.getResult() == 0 && tamirKabulKontrol)) {
				testTahribatsizFloroskopikSonucForm
						.setTamirKabulKullanici(userBean.getUser().getId());
				testTahribatsizFloroskopikSonucForm
						.setTamirKabulZamani(UtilInsCore.getTarihZaman());
			}

			// koordinatlar
			testTahribatsizFloroskopikSonucForm.setGuncellemeZamani(UtilInsCore
					.getTarihZaman());
			testTahribatsizFloroskopikSonucForm.setGuncelleyenEmployee(userBean
					.getUser());

			// otomatik ut
			testTahribatsizFloroskopikSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testTahribatsizFloroskopikSonucForm.setGuncelleyenEmployee(userBean
					.getUser());

			floroskopikSonucManager
					.updateEntity(testTahribatsizFloroskopikSonucForm);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"FLOROSKOPİK VERİSİ BAŞARI İLE GÜNCELLENMİŞTİR!"));
		}

		fillTestList(prmPipeId);

		// sayfada barkoddan okutunca order item pipe seçimi olmadıgı için
		// sıkıntı cıkıyordu
		TahribatsizTestOrderListBean ttolb = new TahribatsizTestOrderListBean();
		ttolb.loadOrderDetails(selectedPipe.getSalesItem().getItemId());
	}

	public void fillTestList(Integer pipeId) {

		try {
			TestTahribatsizFloroskopikSonucManager manager = new TestTahribatsizFloroskopikSonucManager();
			allTestTahribatsizFloroskopikSonucList = manager
					.getAllTestTahribatsizFloroskopikSonuc(pipeId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void addTestTahribatsizFloroskopikSonuc() {
		testTahribatsizFloroskopikSonucForm = new TestTahribatsizFloroskopikSonuc();
		updateButtonRender = false;
	}

	public void floroskopikListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void deleteFloroskopikSonuc(ActionEvent e) {

		TestTahribatsizFloroskopikSonucManager floroskopikSonucManager = new TestTahribatsizFloroskopikSonucManager();
		TestTahribatsizManuelUtSonucManager manuelUtSonucManager = new TestTahribatsizManuelUtSonucManager();

		if (testTahribatsizFloroskopikSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		Integer manuelId = 0;
		try {
			TestTahribatsizManuelUtSonucManager manager = new TestTahribatsizManuelUtSonucManager();
			manuelId = manager
					.findByKoordinatsPipeId(
							testTahribatsizFloroskopikSonucForm.getKoordinatQ(),
							testTahribatsizFloroskopikSonucForm.getKoordinatL(),
							prmPipeId).get(0).getId();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		// tamir kontrol
		TestTahribatsizTamirManager testTahribatsizTamirManager = new TestTahribatsizTamirManager();
		if (testTahribatsizTamirManager.tamirKontrol(prmPipeId, manuelId)) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"KOORDİNATA BAĞLI TAMİR VERİLERİ VARDIR, SİLME İŞLEMİ BAŞARISIZ!",
							null));
			return;
		}

		if (manuelId > 0) {
			testTahribatsizManuelUtSonucForm = manuelUtSonucManager
					.findByKoordinatsPipeId(
							testTahribatsizFloroskopikSonucForm.getKoordinatQ(),
							testTahribatsizFloroskopikSonucForm.getKoordinatL(),
							testTahribatsizFloroskopikSonucForm.getPipe()
									.getPipeId()).get(0);
			manuelUtSonucManager.delete(testTahribatsizManuelUtSonucForm);
		}
		floroskopikSonucManager.delete(testTahribatsizFloroskopikSonucForm);
		testTahribatsizFloroskopikSonucForm = new TestTahribatsizFloroskopikSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");

		fillTestList(prmPipeId);
	}

	public void detayGoster() {

		try {
			testTahribatsizManuelUtSonucForm = new TestTahribatsizManuelUtSonuc();
			TestTahribatsizManuelUtSonucManager mm = new TestTahribatsizManuelUtSonucManager();
			testTahribatsizManuelUtSonucForm = mm.findByKoordinatsPipeId(
					testTahribatsizFloroskopikSonucForm.getKoordinatQ(),
					testTahribatsizFloroskopikSonucForm.getKoordinatL(),
					testTahribatsizFloroskopikSonucForm.getPipe().getPipeId())
					.get(0);
		} catch (Exception e) {

			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"SEÇİLİ KORDİNAT MANUEL TESTLERDE BULUNAMAMIŞTIR, TEKRAR DENEYİNİZ!",
							null));
		}
	}

	public void tamir() {

		// tamirBean = new TestTahribatsizTamirBean();
		tamirBean.addFromFl(allTestTahribatsizFloroskopikSonucList, userBean);
		fillTestList(allTestTahribatsizFloroskopikSonucList.get(0).getPipe()
				.getPipeId());
	}

	public void torna() {

		tornaBean.addFromFl(allTestTahribatsizFloroskopikSonucList, userBean);
		fillTestList(allTestTahribatsizFloroskopikSonucList.get(0).getPipe()
				.getPipeId());
	}

	public void hidrostatik() {

		hidroBean.addFromFl(allTestTahribatsizFloroskopikSonucList, userBean);
		fillTestList(allTestTahribatsizFloroskopikSonucList.get(0).getPipe()
				.getPipeId());
	}

	public void gozOlcu() {

		int index = 0;
		Boolean kontrol = false;
		// tamirBean = new TestTahribatsizTamirBean();
		TestTahribatsizGozOlcuSonucManager manager = new TestTahribatsizGozOlcuSonucManager();
		while (allTestTahribatsizFloroskopikSonucList.size() > index) {
			if (manager.getAllTestTahribatsizGozOlcuSonuc(
					allTestTahribatsizFloroskopikSonucList.get(index).getPipe()
							.getPipeId()).size() > 0) {

				kontrol = true;
			}

			if (kontrol) {
				break;
			}
			index++;
		}
		if (!kontrol) {
			gozOlcuBean.addFromFl(allTestTahribatsizFloroskopikSonucList,
					userBean);
			fillTestList(allTestTahribatsizFloroskopikSonucList.get(0)
					.getPipe().getPipeId());
		} else {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"GÖNDERİLECEK GÖZ ÖLÇÜ ÖNCEDEN EKLENMİŞ!", null));
		}
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " BARKOD NUMARALI BORU SİSTEMDE YOKTUR!", null));
			visibleButtonRender = false;
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			TestTahribatsizFloroskopikSonucManager floroskopikManager = new TestTahribatsizFloroskopikSonucManager();
			allTestTahribatsizFloroskopikSonucList = floroskopikManager
					.getAllTestTahribatsizFloroskopikSonuc(selectedPipe
							.getPipeId());
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
			visibleButtonRender = true;
		}

		KalibrasyonFloroskopikManager kalibrasyonManager = new KalibrasyonFloroskopikManager();
		kalibrasyonlar = kalibrasyonManager.getAllKalibrasyon();
	}

	@SuppressWarnings("static-access")
	public void kaliteGoster() {

		NondestructiveTestsSpecManager ndtsm = new NondestructiveTestsSpecManager();
		selectedNonTest = ndtsm.findByItemIdGlobalId(
				selectedPipe.getSalesItem().getItemId(), 1006).get(0);
	}

	@SuppressWarnings("static-access")
	public void sonKalibrasyonuGoster() {
		kalibrasyon = new KalibrasyonFloroskopik();
		KalibrasyonFloroskopikManager kalibrasyonManager = new KalibrasyonFloroskopikManager();
		kalibrasyon = kalibrasyonManager.getAllKalibrasyon().get(0);
	}

	public void kalibrasyonGoster() {
		kalibrasyon = new KalibrasyonFloroskopik();
		kalibrasyon = testTahribatsizFloroskopikSonucForm.getKalibrasyon();
	}

	public void kalibrasyonAta() {
		TestTahribatsizFloroskopikSonucManager flManager = new TestTahribatsizFloroskopikSonucManager();
		try {
			for (int i = 0; i < allTestTahribatsizFloroskopikSonucList.size(); i++) {
				allTestTahribatsizFloroskopikSonucList.get(i).setKalibrasyon(
						kalibrasyon);
				flManager.updateEntity(allTestTahribatsizFloroskopikSonucList
						.get(i));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"KALİBRASYON ATAMA BAŞARISIZ, TEKRAR DENEYİNİZ!", null));
			return;
		}
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				"KALİBRASYON ATAMA BAŞARILI!", null));
	}

	// setters getters
	public List<TestTahribatsizFloroskopikSonuc> getAllTestTahribatsizFloroskopikSonucList() {
		return allTestTahribatsizFloroskopikSonucList;
	}

	public void setAllTestTahribatsizFloroskopikSonucList(
			List<TestTahribatsizFloroskopikSonuc> allTestTahribatsizFloroskopikSonucList) {
		this.allTestTahribatsizFloroskopikSonucList = allTestTahribatsizFloroskopikSonucList;
	}

	public TestTahribatsizFloroskopikSonuc getTestTahribatsizFloroskopikSonucForm() {
		return testTahribatsizFloroskopikSonucForm;
	}

	public void setTestTahribatsizFloroskopikSonucForm(
			TestTahribatsizFloroskopikSonuc testTahribatsizFloroskopikSonucForm) {
		this.testTahribatsizFloroskopikSonucForm = testTahribatsizFloroskopikSonucForm;

		// tamirKontrol
		if (testTahribatsizFloroskopikSonucForm.getResult() == -1) {
			tamirKabulKontrol = true;
		} else {
			tamirKabulKontrol = false;
		}
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public TestTahribatsizManuelUtSonuc getTestTahribatsizManuelUtSonucForm() {
		return testTahribatsizManuelUtSonucForm;
	}

	public void setTestTahribatsizManuelUtSonucForm(
			TestTahribatsizManuelUtSonuc testTahribatsizManuelUtSonucForm) {
		this.testTahribatsizManuelUtSonucForm = testTahribatsizManuelUtSonucForm;
	}

	public TestTahribatsizTamirBean getTamirBean() {
		return tamirBean;
	}

	public void setTamirBean(TestTahribatsizTamirBean tamirBean) {
		this.tamirBean = tamirBean;
	}

	public TestTahribatsizTornaBean getTornaBean() {
		return tornaBean;
	}

	public void setTornaBean(TestTahribatsizTornaBean tornaBean) {
		this.tornaBean = tornaBean;
	}

	public TestTahribatsizHidrostatikBean getHidroBean() {
		return hidroBean;
	}

	public void setHidroBean(TestTahribatsizHidrostatikBean hidroBean) {
		this.hidroBean = hidroBean;
	}

	public TestTahribatsizGozOlcuSonucBean getGozOlcuBean() {
		return gozOlcuBean;
	}

	public void setGozOlcuBean(TestTahribatsizGozOlcuSonucBean gozOlcuBean) {
		this.gozOlcuBean = gozOlcuBean;
	}

	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	public String getBarkodNo() {
		return barkodNo;
	}

	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public NondestructiveTestsSpec getSelectedNonTest() {
		return selectedNonTest;
	}

	public void setSelectedNonTest(NondestructiveTestsSpec selectedNonTest) {
		this.selectedNonTest = selectedNonTest;
	}

	/**
	 * @return the kalibrasyon
	 */
	public KalibrasyonFloroskopik getKalibrasyon() {
		return kalibrasyon;
	}

	/**
	 * @param kalibrasyon
	 *            the kalibrasyon to set
	 */
	public void setKalibrasyon(KalibrasyonFloroskopik kalibrasyon) {
		this.kalibrasyon = kalibrasyon;
	}

	/**
	 * @return the kalibrasyonlar
	 */
	public List<KalibrasyonFloroskopik> getKalibrasyonlar() {
		return kalibrasyonlar;
	}

	/**
	 * @param kalibrasyonlar
	 *            the kalibrasyonlar to set
	 */
	public void setKalibrasyonlar(List<KalibrasyonFloroskopik> kalibrasyonlar) {
		this.kalibrasyonlar = kalibrasyonlar;
	}

	/**
	 * @return the visibleButtonRender
	 */
	public boolean isVisibleButtonRender() {
		return visibleButtonRender;
	}

}
