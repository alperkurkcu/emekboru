package com.emekboru.beans.sales.customer;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.sales.customer.SalesCustomerFunctionType;
import com.emekboru.jpaman.sales.customer.SalesCustomerFunctionTypeManager;

@ManagedBean(name = "salesCustomerFunctionTypeBean")
@ViewScoped
public class SalesCustomerFunctionTypeBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2087363790688694966L;
	private List<SalesCustomerFunctionType> functionTypeList;

	public SalesCustomerFunctionTypeBean() {

		SalesCustomerFunctionTypeManager manager = new SalesCustomerFunctionTypeManager();
		functionTypeList = manager.findAll(SalesCustomerFunctionType.class);
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public List<SalesCustomerFunctionType> getFunctionTypeList() {
		return functionTypeList;
	}

	public void setFunctionTypeList(
			List<SalesCustomerFunctionType> functionTypeList) {
		this.functionTypeList = functionTypeList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}