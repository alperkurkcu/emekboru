package com.emekboru.beans.order;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.config.Standard;
import com.emekboru.jpa.Layer;
import com.emekboru.jpa.Order;
import com.emekboru.jpa.PlannedIsolation;
import com.emekboru.jpaman.CustomerManager;
import com.emekboru.jpaman.LayerManager;
import com.emekboru.jpaman.OrderManager;
import com.emekboru.jpaman.PlannedIsolationManager;
import com.emekboru.utils.ConstsCore;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.OrderUtil;
import com.emekboru.utils.lazymodels.LazyOrderDataModel;

@ManagedBean(name = "listOrderBean")
@ViewScoped
public class ListOrderBean {

	@EJB
	private ConfigBean config;
	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private LazyOrderDataModel orders;
	private Order newOrder = new Order();
	private Order selectedOrder = new Order();
	private PlannedIsolation selectedInternalIsolation = new PlannedIsolation();
	private PlannedIsolation selectedExternalIsolation = new PlannedIsolation();
	private Layer selectedLayer = new Layer();
	private Layer newLayer = new Layer();
	private List<Standard> izolasyonStandartlariList = new ArrayList<Standard>();
	private String kaplamaTuru = "";

	@PostConstruct
	public void init() {
		orders = new LazyOrderDataModel();
	}

	public void addNewOrder() {
		OrderManager orderManager = new OrderManager();
		newOrder = OrderUtil.getOrderKimlikId(orderManager, newOrder);
		CustomerManager customerManager = new CustomerManager();
		newOrder.setCustomer(customerManager.getCustomerByCustormerId(newOrder
				.getCustomerId()));
		orderManager.enterNew(newOrder);
		PlannedIsolationManager plannedIsolationManager = new PlannedIsolationManager();
		PlannedIsolation plannedIsolation = new PlannedIsolation();
		plannedIsolation.setOrder(newOrder);
		plannedIsolation.setInternalExternal(PlannedIsolation.EXTERNAL);
		plannedIsolationManager.enterNew(plannedIsolation);
		plannedIsolation = new PlannedIsolation();
		plannedIsolation.setOrder(newOrder);
		plannedIsolation.setInternalExternal(PlannedIsolation.INTERNAL);
		plannedIsolationManager.enterNew(plannedIsolation);
		init();
		this.newOrder = new Order();
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void updateOrder() {
		OrderManager orderManager = new OrderManager();
		CustomerManager customerManager = new CustomerManager();
		selectedOrder.setCustomer(customerManager
				.getCustomerByCustormerId(selectedOrder.getCustomerId()));
		orderManager.updateEntity(selectedOrder);
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void loadSelectedOrder() {
		OrderManager orderManager = new OrderManager();
		selectedOrder = orderManager.loadObject(Order.class,
				selectedOrder.getOrderId());
		for (PlannedIsolation plannedIsolation : selectedOrder
				.getPlannedIsolation()) {
			if (plannedIsolation.getInternalExternal().equals(
					PlannedIsolation.EXTERNAL)) {
				selectedExternalIsolation = plannedIsolation;
			} else {
				selectedInternalIsolation = plannedIsolation;
			}
		}
	}

	public void updateSelectedLayerInternal() {
		LayerManager layerManager = new LayerManager();
		layerManager.updateEntity(selectedLayer);
		PlannedIsolationManager plannedIsolationManager = new PlannedIsolationManager();
		selectedInternalIsolation = plannedIsolationManager.loadObject(
				PlannedIsolation.class,
				selectedInternalIsolation.getIsolationId());
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void updateSelectedLayerExternal() {
		LayerManager layerManager = new LayerManager();
		layerManager.updateEntity(selectedLayer);
		PlannedIsolationManager plannedIsolationManager = new PlannedIsolationManager();
		selectedExternalIsolation = plannedIsolationManager.loadObject(
				PlannedIsolation.class,
				selectedExternalIsolation.getIsolationId());
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void saveSelectedLayerExternal() {
		LayerManager layerManager = new LayerManager();
		newLayer.setIsolation(selectedExternalIsolation);
		layerManager.enterNew(newLayer);
		PlannedIsolationManager plannedIsolationManager = new PlannedIsolationManager();
		selectedExternalIsolation = plannedIsolationManager.loadObject(
				PlannedIsolation.class,
				selectedExternalIsolation.getIsolationId());
		newLayer = new Layer();
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void saveSelectedLayerInternal() {
		LayerManager layerManager = new LayerManager();
		newLayer.setIsolation(selectedInternalIsolation);
		layerManager.enterNew(newLayer);
		PlannedIsolationManager plannedIsolationManager = new PlannedIsolationManager();
		selectedInternalIsolation = plannedIsolationManager.loadObject(
				PlannedIsolation.class,
				selectedInternalIsolation.getIsolationId());
		newLayer = new Layer();
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void deleteSelectedLayerInternal() {
		LayerManager layerManager = new LayerManager();
		layerManager.delete(selectedLayer);
		PlannedIsolationManager plannedIsolationManager = new PlannedIsolationManager();
		selectedInternalIsolation = plannedIsolationManager.loadObject(
				PlannedIsolation.class,
				selectedInternalIsolation.getIsolationId());
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void deleteSelectedLayerExternal() {
		LayerManager layerManager = new LayerManager();
		layerManager.delete(selectedLayer);
		PlannedIsolationManager plannedIsolationManager = new PlannedIsolationManager();
		selectedExternalIsolation = plannedIsolationManager.loadObject(
				PlannedIsolation.class,
				selectedExternalIsolation.getIsolationId());
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void saveInternalIsolation() {
		OrderManager orderManager = new OrderManager();
		PlannedIsolationManager plannedIsolationManager = new PlannedIsolationManager();
		orderManager.updateEntity(selectedOrder);
		plannedIsolationManager.updateEntity(selectedInternalIsolation);
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void saveExternalIsolation() {
		OrderManager orderManager = new OrderManager();
		PlannedIsolationManager plannedIsolationManager = new PlannedIsolationManager();
		orderManager.updateEntity(selectedOrder);
		plannedIsolationManager.updateEntity(selectedExternalIsolation);
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}
	
	public void izolasyonSekliChanged(AjaxBehaviorEvent  e)
	{
		String izolasyonSekli="";
		if(kaplamaTuru.equalsIgnoreCase(ConstsCore.DIS_KAPLAMA))
			izolasyonSekli=selectedExternalIsolation.getIzolasyonSekli();
		else if(kaplamaTuru.equalsIgnoreCase(ConstsCore.IC_KAPLAMA))
			izolasyonSekli=selectedInternalIsolation.getIzolasyonSekli();
		
		List<Standard> standards=config.getConfig().getPipe().getCoating().getStandards();
		izolasyonStandartlariList=new ArrayList<Standard>();
		
			for(Standard s:standards)
			{
				if(s.getParentDescription().equalsIgnoreCase(izolasyonSekli) )
				{
					izolasyonStandartlariList.add(s);
				}

			}
		}

	

	public void disKaplamaListener(ActionEvent e) {
		kaplamaTuru = ConstsCore.DIS_KAPLAMA;
	}

	public void icKaplamaListener(ActionEvent e) {
		kaplamaTuru = ConstsCore.IC_KAPLAMA;
	}

	public Layer getNewLayer() {
		return newLayer;
	}

	public void setNewLayer(Layer newLayer) {
		this.newLayer = newLayer;
	}

	public Layer getSelectedLayer() {
		return selectedLayer;
	}

	public void setSelectedLayer(Layer selectedLayer) {
		this.selectedLayer = selectedLayer;
	}

	public PlannedIsolation getSelectedInternalIsolation() {
		return selectedInternalIsolation;
	}

	public void setSelectedInternalIsolation(
			PlannedIsolation selectedInternalIsolation) {
		this.selectedInternalIsolation = selectedInternalIsolation;
	}

	public PlannedIsolation getSelectedExternalIsolation() {
		return selectedExternalIsolation;
	}

	public void setSelectedExternalIsolation(
			PlannedIsolation selectedExternalIsolation) {
		this.selectedExternalIsolation = selectedExternalIsolation;
	}

	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public LazyOrderDataModel getOrders() {
		return orders;
	}

	public void setOrders(LazyOrderDataModel orders) {
		this.orders = orders;
	}

	public Order getNewOrder() {
		return newOrder;
	}

	public void setNewOrder(Order newOrder) {
		this.newOrder = newOrder;
	}

	public Order getSelectedOrder() {
		return selectedOrder;
	}

	public void setSelectedOrder(Order selectedOrder) {
		this.selectedOrder = selectedOrder;
	}

	public List<Standard> getIzolasyonStandartlariList() {
		return izolasyonStandartlariList;
	}

	public void setIzolasyonStandartlariList(
			List<Standard> izolasyonStandartlariList) {
		this.izolasyonStandartlariList = izolasyonStandartlariList;
	}

}
