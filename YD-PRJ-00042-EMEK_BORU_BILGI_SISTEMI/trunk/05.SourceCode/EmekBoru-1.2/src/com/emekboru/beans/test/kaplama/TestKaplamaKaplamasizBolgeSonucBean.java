package com.emekboru.beans.test.kaplama;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaKaplamasizBolgeSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaKaplamasizBolgeSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaplamaKaplamasizBolgeSonucBean")
@ViewScoped
public class TestKaplamaKaplamasizBolgeSonucBean {

	private TestKaplamaKaplamasizBolgeSonuc testKaplamaKaplamasizBolgeSonucForm = new TestKaplamaKaplamasizBolgeSonuc();
	private List<TestKaplamaKaplamasizBolgeSonuc> allTestKaplamaKaplamasizBolgeSonucList = new ArrayList<TestKaplamaKaplamasizBolgeSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateTestKaplamaKaplamasizBolgeSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaKaplamasizBolgeSonucManager manager = new TestKaplamaKaplamasizBolgeSonucManager();

		if (testKaplamaKaplamasizBolgeSonucForm.getId() == null) {

			testKaplamaKaplamasizBolgeSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaKaplamasizBolgeSonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaKaplamasizBolgeSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaKaplamasizBolgeSonucForm.setBagliTestId(bagliTest);
			testKaplamaKaplamasizBolgeSonucForm.setBagliGlobalId(bagliTest);

			manager.enterNew(testKaplamaKaplamasizBolgeSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaKaplamasizBolgeSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaKaplamasizBolgeSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			manager.updateEntity(testKaplamaKaplamasizBolgeSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addTestKaplamaKaplamasizBolgeSonuc() {

		testKaplamaKaplamasizBolgeSonucForm = new TestKaplamaKaplamasizBolgeSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaKaplamasizBolgeSonucListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaKaplamasizBolgeSonucList = TestKaplamaKaplamasizBolgeSonucManager
				.getAllTestKaplamaKaplamasizBolgeSonuc(globalId, pipeId);
		testKaplamaKaplamasizBolgeSonucForm = new TestKaplamaKaplamasizBolgeSonuc();
	}

	public void deleteTestKaplamaKaplamasizBolgeSonuc() {

		bagliTestId = testKaplamaKaplamasizBolgeSonucForm.getBagliGlobalId()
				.getId();

		if (testKaplamaKaplamasizBolgeSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaKaplamasizBolgeSonucManager manager = new TestKaplamaKaplamasizBolgeSonucManager();
		manager.delete(testKaplamaKaplamasizBolgeSonucForm);
		testKaplamaKaplamasizBolgeSonucForm = new TestKaplamaKaplamasizBolgeSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters
	public TestKaplamaKaplamasizBolgeSonuc getTestKaplamaKaplamasizBolgeSonucForm() {
		return testKaplamaKaplamasizBolgeSonucForm;
	}

	public void setTestKaplamaKaplamasizBolgeSonucForm(
			TestKaplamaKaplamasizBolgeSonuc testKaplamaKaplamasizBolgeSonucForm) {
		this.testKaplamaKaplamasizBolgeSonucForm = testKaplamaKaplamasizBolgeSonucForm;
	}

	public List<TestKaplamaKaplamasizBolgeSonuc> getAllTestKaplamaKaplamasizBolgeSonucList() {
		return allTestKaplamaKaplamasizBolgeSonucList;
	}

	public void setAllTestKaplamaKaplamasizBolgeSonucList(
			List<TestKaplamaKaplamasizBolgeSonuc> allTestKaplamaKaplamasizBolgeSonucList) {
		this.allTestKaplamaKaplamasizBolgeSonucList = allTestKaplamaKaplamasizBolgeSonucList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
