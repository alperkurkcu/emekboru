package com.emekboru.beans.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.MethodExpressionActionListener;

import org.primefaces.component.menuitem.MenuItem;
import org.primefaces.component.submenu.Submenu;
import org.primefaces.model.DefaultMenuModel;
import org.primefaces.model.MenuModel;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.YetkiMenu;
import com.emekboru.jpa.YetkiMenuKullaniciErisim;
import com.emekboru.jpaman.YetkiMenuKullaniciErisimManager;
import com.emekboru.jpaman.YetkiMenuManager;

@ManagedBean(name = "dynamicMenuModel")
@RequestScoped
public class DynamicMenuModel {
	private static HashMap<Integer, Submenu> parentMenuHm = new HashMap<Integer, Submenu>();
	private static HashMap<Integer, Submenu> grandParentMenuHm = new HashMap<Integer, Submenu>();
	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;
	private static MenuModel model;
	private static int subMenuId = 0;

	@SuppressWarnings("unused")
	private static String userFullName;

	public static MenuModel createModel(SystemUser user) {
		model = new DefaultMenuModel();
		parentMenuHm = new HashMap<Integer, Submenu>();
		grandParentMenuHm.clear();

		HashMap<Integer, YetkiMenuKullaniciErisim> kullaniciSayfaErisimMap = YetkiMenuKullaniciErisimManager
				.findUserYetkiMenuErisim(user.getId());

		if (kullaniciSayfaErisimMap.size() == 0)
			createModel(user.getEmployee().getSupervisor().getUsers().get(0));

		MenuItem item = null;
		int menuItemId = 0;

		for (Map.Entry<Integer, YetkiMenuKullaniciErisim> allowedMenus : kullaniciSayfaErisimMap
				.entrySet()) {
			YetkiMenuKullaniciErisim yetkiMenuKullaniciErisim = allowedMenus
					.getValue();
			YetkiMenu yetkiMenu = yetkiMenuKullaniciErisim.getYetkiMenu();
			if (yetkiMenu.isMenuDurum()) {
				item = new MenuItem();
				++menuItemId;
				item.setId("menuItem" + menuItemId
						+ new Random().nextInt(150000));
				// System.out.println("EKRAN: "+yetkiMenu.getEkranTanimi());
				item.setValue(yetkiMenu.getEkranTanimi());
				item.setImmediate(true);
				item.setInView(true);
				// item.setDisabled(!yetkiMenu.isMenuDurum());
				// item.setUrl(yetkiMenu.getActionLinkAd());
				// item.setProcess("@this");
				// item.setUpdate("mainForm");
				item.setAjax(true);
				item.setActionExpression(endConversationBinding(yetkiMenu));
				// String action = "#{" + yetkiMenu.getActionSinifMetot() + "}";
				// item.addActionListener(createMethodActionListener(action,
				// Void.TYPE, new Class[0]));

				// /parent submenuleri parentmenuhm ye ekler
				if (!parentMenuHm.containsKey(yetkiMenu.getParentMenuId())) {
					setParentSubMenusVisible(yetkiMenu.getParentMenuId());
				}
				// itemı parentmenuhmdeki parentının içine atar
				addMenuItemToSubMenu(yetkiMenu.getParentMenuId(), item);
			}
		}
		// submenuleri iç içe ekler
		for (Map.Entry<Integer, Submenu> submenux : parentMenuHm.entrySet()) {
			Integer key = submenux.getKey();
			Submenu value = submenux.getValue();
			addToGrandParents(key, value);
		}
		for (Map.Entry<Integer, Submenu> submenux : grandParentMenuHm
				.entrySet()) {
			if (!model.getSubmenus().contains(submenux.getValue()))
				model.addSubmenu(submenux.getValue());

		}

		return model;
	}

	public MenuModel getMenu() {
		if (model == null || model.getSubmenus().size() == 0)
			return createModel(userBean.getUser());
		return model;
	}

	private static void addToGrandParents(Integer yetkiId, Submenu menu) {
		YetkiMenu childYetkiMenu = YetkiMenuManager.getYetkiMenuById(yetkiId);
		if (childYetkiMenu != null) {
			if (childYetkiMenu.getParentMenuId() == null) {
				if (grandParentMenuHm.get(yetkiId) == null)
					grandParentMenuHm.put(yetkiId, menu);
			} else {
				YetkiMenu parentYetkiMenu = YetkiMenuManager
						.getYetkiMenuById(childYetkiMenu.getParentMenuId());
				Submenu parentSubmenu = parentMenuHm.get(parentYetkiMenu
						.getId());
				Submenu submenu3 = grandParentMenuHm.get(parentYetkiMenu
						.getId());
				if (submenu3 == null) {
					if (!grandParentMenuHm.containsKey(parentYetkiMenu.getId()))
						grandParentMenuHm.put(parentYetkiMenu.getId(),
								parentSubmenu);

				} else {
					if (!submenu3.getChildren().contains(
							parentMenuHm.get(childYetkiMenu.getId()))) {
						submenu3.getChildren().add(
								parentMenuHm.get(childYetkiMenu.getId()));
						grandParentMenuHm.remove(parentYetkiMenu.getId());
						grandParentMenuHm
								.put(parentYetkiMenu.getId(), submenu3);
					}
				}

				if (parentYetkiMenu.getParentMenuId() != null)
					addToGrandParents(parentYetkiMenu.getParentMenuId(),
							submenu3);
			}
		}
	}

	private static void addMenuItemToSubMenu(Integer parentMenuId, MenuItem item) {

		Submenu submenu = parentMenuHm.get(parentMenuId);
		if (submenu != null) {
			submenu.getChildren().add(item);
		}

	}

	private static void setParentSubMenusVisible(Integer parentMenuId) {

		YetkiMenu ya2 = (YetkiMenu) YetkiMenuManager
				.getYetkiMenuById(parentMenuId);
		if (ya2.isMenuDurum()) {
			if (!parentMenuHm.containsKey(ya2.getId())) {
				Submenu submenu = new Submenu();
				subMenuId++;
				submenu.setId("submenu" + subMenuId);
				submenu.setLabel(ya2.getEkranTanimi());
				parentMenuHm.put(ya2.getId(), submenu);
				if (ya2.getParentMenuId() != null)
					setParentSubMenusVisible(ya2.getParentMenuId());
			}
		}
	}

	public static MethodExpression endConversationBinding(YetkiMenu yetkiMenu) {
		String action = "#{" + yetkiMenu.getActionSinifMetot() + "}";
		return FacesContext.getCurrentInstance()
				.getApplication()
				.getExpressionFactory()
				//
				.createMethodExpression(
						FacesContext.getCurrentInstance().getELContext(), //
						action, Void.class, new Class[0]);
	}

	public MethodExpression endConversationBinding2(YetkiMenu yetkiMenu) {
		String action = "#{" + yetkiMenu.getActionSinifMetot() + "}";
		return FacesContext
				.getCurrentInstance()
				.getApplication()
				.getExpressionFactory()
				.createMethodExpression(
						FacesContext.getCurrentInstance().getELContext(),
						action, Void.class, new Class[] { ActionEvent.class });
	}

	@SuppressWarnings("unused")
	private MethodExpressionActionListener createMethodActionListener(
			String valueExpression, Class<?> valueType,
			Class<?>[] expectedParamTypes) {
		MethodExpressionActionListener actionListener = null;
		try {
			actionListener = new MethodExpressionActionListener(
					createMethodExpression(valueExpression, valueType,
							expectedParamTypes));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return actionListener;
	}

	private MethodExpression createMethodExpression(String valueExpression,
			Class<?> valueType, Class<?>[] expectedParamTypes) {
		MethodExpression methodExpression = null;
		try {
			ExpressionFactory factory = FacesContext.getCurrentInstance()
					.getApplication().getExpressionFactory();
			methodExpression = factory.createMethodExpression(FacesContext
					.getCurrentInstance().getELContext(), valueExpression,
					valueType, expectedParamTypes);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return methodExpression;
	}

	@SuppressWarnings("unused")
	private boolean checkYetkiConstraints(
			HashMap<Integer, YetkiMenuKullaniciErisim> kullaniciSayfaErisimList,
			Integer menuId) {
		if (kullaniciSayfaErisimList.containsKey(menuId))
			return true;
		return false;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

}
