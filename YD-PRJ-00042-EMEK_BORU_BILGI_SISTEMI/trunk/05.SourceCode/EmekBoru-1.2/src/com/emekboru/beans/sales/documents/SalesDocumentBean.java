package com.emekboru.beans.sales.documents;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.emekboru.beans.sales.SalesMenuBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Employee;
import com.emekboru.jpa.sales.documents.SalesDocument;
import com.emekboru.jpa.sales.documents.SalesJoinDocumentEmployee;
import com.emekboru.jpaman.EmployeeManager;
import com.emekboru.jpaman.sales.documents.SalesDocumentManager;
import com.emekboru.jpaman.sales.documents.SalesJoinDocumentEmployeeManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "salesDocumentBean")
@ViewScoped
public class SalesDocumentBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4416819595651539714L;

	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{salesMenuBean}")
	private SalesMenuBean salesMenuBean;

	private SalesDocument newDocument;
	private SalesDocument selectedDocument;
	private SalesDocument reminderDocument;

	private List<SalesDocument> documentList;
	private List<SalesDocument> filteredList;
	private List<SalesDocument> reminderList;

	private String downloadedFileName;
	private StreamedContent downloadedFile;

	private File[] fileArray;

	private File theFile = null;
	OutputStream output = null;
	InputStream input = null;

	private String path;
	private String privatePath;
	private String toBeDeleted;

	int length;

	public List<Employee> employeeList;
	private Employee[] selectedEmployees;
	public Integer departmentId;

	public SalesDocumentBean() {
		newDocument = new SalesDocument();
		selectedDocument = new SalesDocument();
		employeeList = new ArrayList<Employee>();

		privatePath = File.separatorChar + "sales" + File.separatorChar
				+ "uploadedDocuments" + File.separatorChar + "document"
				+ File.separatorChar;
	}

	@PostConstruct
	public void load() {
		System.out.println("SalesDocumentsBean.load()-BAŞLA");

		try {
			this.setPath(config.getConfig().getFolder().getAbsolutePath()
					+ privatePath);

			System.out.println("Path for Upload File (Sales):			" + path);
		} catch (Exception ex) {
			System.out.println("Path for Upload File (Sales):			"
					+ FacesContext.getCurrentInstance().getExternalContext()
							.getRealPath(privatePath));

			System.out.print(path);
			System.out.println("CAUSE OF " + ex.toString());
		}

		theFile = new File(path);
		if (!theFile.isDirectory()) {
			theFile.mkdirs();
		}

		fileArray = theFile.listFiles();

		try {

			SalesDocumentManager manager = new SalesDocumentManager();
			documentList = manager.findAll(SalesDocument.class);

			/* JOIN FUNCTIONS */
			for (SalesDocument document : documentList) {
				this.loadJoinDocumentEmployee(document);
			}
			/* end of JOIN FUNCTIONS */
			selectedDocument = documentList.get(0);
		} catch (Exception e) {
			System.out.println("SalesDocumentBean:" + e.toString());
		}

		System.out.println("SalesDocumentsBean.load()-BİT");

	}

	public String checkFileName(String fileName) {

		fileName = fileName.replace(" ", "_");
		fileName = fileName.replace("ç", "c");
		fileName = fileName.replace("Ç", "C");
		fileName = fileName.replace("ð", "g");
		fileName = fileName.replace("Ð", "G");
		fileName = fileName.replace("Ý", "i");
		fileName = fileName.replace("ý", "i");
		fileName = fileName.replace("ö", "o");
		fileName = fileName.replace("Ö", "O");
		fileName = fileName.replace("þ", "s");
		fileName = fileName.replace("Þ", "S");
		fileName = fileName.replace("ü", "u");
		fileName = fileName.replace("Ü", "U");

		return fileName;
	}

	public void upload(FileUploadEvent event) throws AbortProcessingException,
			IOException {
		System.out.println("SalesCRMMeetingBean.upload()-BAŞLA");

		try {
			String name = this.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				newDocument.setName("//" + name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				SalesDocumentManager manager = new SalesDocumentManager();
				manager.updateEntity(selectedDocument);

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);
				FacesContextUtils.addWarnMessage("salesFileUploadedMessage");
			} else {
				System.out.println("File already exists.");

				newDocument.setName("//");
				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("SalesDocumentBean:" + e.toString());
		}

		System.out.println("SalesCRMMeetingBean.upload()-BİT");

	}

	public void addUploadedFile(FileUploadEvent event)
			throws AbortProcessingException, IOException {
		System.out.println("SalesCRMMeetingBean.addUploadedFile()");

		try {
			String name = this.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				selectedDocument.setName(selectedDocument.getName() + "//"
						+ name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				SalesDocumentManager manager = new SalesDocumentManager();
				manager.updateEntity(selectedDocument);

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				FacesContextUtils.addWarnMessage("salesFileUploadedMessage");
			} else {
				System.out.println("File already exists.");

				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("SalesDocumentBean:" + e.toString());
		}
	}

	public void deleteTheSelectedFile() {
		System.out.println("SalesCRMVisitBean.deleteTheSelectedFile()");
		try {
			// Delete the uploaded file
			theFile = new File(path);
			fileArray = theFile.listFiles();

			System.out.println("document that will be deleted: " + privatePath
					+ toBeDeleted);
			for (int j = 0; j < fileArray.length; j++) {
				System.out.println("fileArray[j].toString(): "
						+ fileArray[j].toString());
				if (fileArray[j].toString().contains(privatePath + toBeDeleted)) {
					fileArray[j].delete();

					if (selectedDocument.getName().contentEquals(
							"//" + toBeDeleted)) {
						selectedDocument.setName(selectedDocument.getName()
								.replace("//" + toBeDeleted, "null"));
					} else {
						selectedDocument.setName(selectedDocument.getName()
								.replace("//" + toBeDeleted, ""));
					}
					FacesContextUtils
							.addWarnMessage("salesTheFileIsDeletedMessage");
				}
			}
		} catch (Exception e) {
			System.out.println("SalesDocumentBean:" + e.toString());
		}
	}

	public void deleteAndUpload(FileUploadEvent event)
			throws AbortProcessingException, IOException {

		System.out.println("SalesCRMMeetingBean.deleteAndUpload()");

		try {
			// Delete the uploaded file
			theFile = new File(path);
			fileArray = theFile.listFiles();

			for (int i = 0; i < selectedDocument.getFileNames().size(); i++) {
				for (int j = 0; j < fileArray.length; j++) {
					System.out.println("fileArray[j].toString(): "
							+ fileArray[j].toString());
					if (fileArray[j].toString().contains(
							privatePath
									+ selectedDocument.getFileNames().get(i))) {
						fileArray[j].delete();
					}
				}
			}

			String name = this.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				selectedDocument.setName("//" + name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				SalesDocumentManager manager = new SalesDocumentManager();
				manager.updateEntity(selectedDocument);

				FacesContextUtils.addInfoMessage("UpdateItemMessage");
			} else {
				System.out.println("File already exists.");

				selectedDocument.setName(null);
				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("SalesDocumentBean:" + e.toString());
		}
	}

	public void add() {

		System.out.println("SalesDocumentsBean.add()");

		try {
			if (newDocument.getName() != null) {

				for (int i = 0; i < selectedEmployees.length; i++) {
					newDocument.getResponsibleList().add(selectedEmployees[i]);
				}

				SalesDocumentManager manager = new SalesDocumentManager();
				manager.enterNew(newDocument);
				/* JOIN FUNCTIONS */
				this.addJoinDocumentEmployee(newDocument);
				/* end of JOIN FUNCTIONS */
				documentList.add(newDocument);

				newDocument = new SalesDocument();

				this.load();

				FacesContextUtils.addInfoMessage("SubmitMessage");
			} else {
				FacesContextUtils.addErrorMessage("salesNoFileUploaded");
			}
		} catch (Exception e) {
			System.out.println("SalesDocumentBean:" + e.toString());
		}
	}

	public void update() {
		System.out.println("SalesDocumentsBean.update()");

		try {
			SalesDocumentManager manager = new SalesDocumentManager();

			/* JOIN FUNCTIONS */
			this.deleteJoinDocumentEmployee(selectedDocument);
			this.addJoinDocumentEmployee(selectedDocument);
			/* end of JOIN FUNCTIONS */

			manager.updateEntity(selectedDocument);
			FacesContextUtils.addInfoMessage("UpdateItemMessage");
		} catch (Exception e) {
			System.out.println("SalesDocumentBean:" + e.toString());
		}
	}

	public void delete() throws IOException {

		System.out.println("SalesDocumentsBean.delete(): "
				+ selectedDocument.getName());

		boolean isDeleted = true;
		try {
			if (selectedDocument.getFileNumber() > 0) {
				isDeleted = false;

				// Delete the uploaded file
				theFile = new File(path);
				fileArray = theFile.listFiles();

				for (int i = 0; i < selectedDocument.getFileNames().size(); i++) {
					System.out.println("document that will be deleted: "
							+ privatePath
							+ selectedDocument.getFileNames().get(i));
					for (int j = 0; j < fileArray.length; j++) {
						System.out.println("path has the files: "
								+ fileArray[j].toString());
						if (fileArray[j].toString().contains(
								privatePath
										+ selectedDocument.getFileNames()
												.get(i))) {
							isDeleted = fileArray[j].delete();
						}
					}
				}
			}
			if (isDeleted) {
				/* JOIN FUNCTIONS */
				this.deleteJoinDocumentEmployee(selectedDocument);
				/* end of JOIN FUNCTIONS */

				SalesDocumentManager manager = new SalesDocumentManager();
				manager.delete(selectedDocument);
				documentList.remove(selectedDocument);

				selectedDocument = documentList.get(0);

				FacesContextUtils.addWarnMessage("DeletedMessage");
			}
		} catch (Exception e) {
			System.out.println("SalesDocumentBean:" + e.toString());
		}
	}

	/* JOIN FUNCTIONS */
	private void addJoinDocumentEmployee(SalesDocument e) {
		SalesJoinDocumentEmployeeManager joinManager = new SalesJoinDocumentEmployeeManager();
		SalesJoinDocumentEmployee newJoin;

		for (int i = 0; i < e.getResponsibleList().size(); i++) {
			newJoin = new SalesJoinDocumentEmployee();
			newJoin.setEmployee(e.getResponsibleList().get(i));
			newJoin.setDocument(e);
			joinManager.enterNew(newJoin);
		}
	}

	private void loadJoinDocumentEmployee(SalesDocument document) {
		SalesJoinDocumentEmployeeManager joinManager = new SalesJoinDocumentEmployeeManager();
		List<SalesJoinDocumentEmployee> joinList = joinManager
				.findAll(SalesJoinDocumentEmployee.class);

		document.setResponsibleList(new ArrayList<Employee>());

		for (int i = 0; i < joinList.size(); i++) {
			if (joinList.get(i).getDocument().equals(document)) {
				document.getResponsibleList()
						.add(joinList.get(i).getEmployee());
			}
		}
	}

	private void deleteJoinDocumentEmployee(SalesDocument document) {
		SalesJoinDocumentEmployeeManager joinManager = new SalesJoinDocumentEmployeeManager();
		List<SalesJoinDocumentEmployee> joinList = joinManager
				.findAll(SalesJoinDocumentEmployee.class);

		for (int i = 0; i < joinList.size(); i++) {
			if (joinList.get(i).getDocument().equals(document)) {
				joinManager.delete(joinList.get(i));
			}
		}
	}

	public void loadEmployeeListForNewEvent() {
		System.out.println("SalesDocumentBean.loadEmployeeListForNewEvent()");

		try {
			EmployeeManager employeeManager = new EmployeeManager();
			employeeList = employeeManager.findByField(Employee.class,
					"department.departmentId", departmentId);
		} catch (Exception e) {
			System.out.println("SalesDocumentBean:" + e.toString());
		}
	}

	/* end of JOIN FUNCTIONS */

	public void selectNothing() {
		reminderDocument = null;
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public SalesDocument getNewDocument() {
		return newDocument;
	}

	public void setNewDocument(SalesDocument newDocument) {
		this.newDocument = newDocument;
	}

	public SalesDocument getSelectedDocument() {
		try {
			if (selectedDocument == null)
				selectedDocument = new SalesDocument();
		} catch (Exception ex) {
			selectedDocument = new SalesDocument();
		}
		return selectedDocument;
	}

	public void setSelectedDocument(SalesDocument selectedDocument) {
		this.selectedDocument = selectedDocument;
	}

	public SalesDocument getReminderDocument() {
		return reminderDocument;
	}

	public void setReminderDocument(SalesDocument reminderDocument) {
		this.reminderDocument = reminderDocument;
	}

	public List<SalesDocument> getDocumentList() {
		return documentList;
	}

	public void setDocumentList(List<SalesDocument> documentList) {
		this.documentList = documentList;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getDownloadedFileName() {
		return downloadedFileName;
	}

	public void setDownloadedFileName(String downloadedFileName) {
		this.downloadedFileName = downloadedFileName;
	}

	@SuppressWarnings("resource")
	public StreamedContent getDownloadedFile() {
		System.out.println("SalesDocumentBean.getDownloadedFile()");

		try {
			if (downloadedFileName != null) {
				RandomAccessFile accessFile = new RandomAccessFile(path
						+ downloadedFileName, "r");
				byte[] downloadByte = new byte[(int) accessFile.length()];
				accessFile.read(downloadByte);
				InputStream stream = new ByteArrayInputStream(downloadByte);

				String suffix = FilenameUtils.getExtension(downloadedFileName);
				String contentType = "text/plain";
				if (suffix.contentEquals("jpg") || suffix.contentEquals("png")
						|| suffix.contentEquals("gif")) {
					contentType = "image/" + contentType;
				} else if (suffix.contentEquals("doc")
						|| suffix.contentEquals("docx")) {
					contentType = "application/msword";
				} else if (suffix.contentEquals("xls")
						|| suffix.contentEquals("xlsx")) {
					contentType = "application/vnd.ms-excel";
				} else if (suffix.contentEquals("pdf")) {
					contentType = "application/pdf";
				}
				downloadedFile = new DefaultStreamedContent(stream,
						"contentType", downloadedFileName);
				stream.close();
			}
		} catch (Exception e) {
			System.out.println("SalesDocumentBean:" + e.toString());
		}
		return downloadedFile;
	}

	public List<SalesDocument> getFilteredList() {
		try {
			selectedDocument = filteredList.get(0);
			salesMenuBean.showNewType1Menu();
		} catch (Exception ex) {
			System.out.println("getFilteredList: " + ex);
		}
		return filteredList;
	}

	public void setFilteredList(List<SalesDocument> filteredList) {
		this.filteredList = filteredList;
	}

	public SalesMenuBean getSalesMenuBean() {
		return salesMenuBean;
	}

	public void setSalesMenuBean(SalesMenuBean salesMenuBean) {
		this.salesMenuBean = salesMenuBean;
	}

	@SuppressWarnings("deprecation")
	public List<SalesDocument> getReminderList() {
		try {
			Calendar currentDate = Calendar.getInstance();
			currentDate.add(Calendar.DAY_OF_MONTH, -30);
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
			String dateNow = formatter.format(currentDate.getTime());
			Date startDate = new Date(dateNow);

			reminderList = new ArrayList<SalesDocument>();

			for (int i = 0; i < documentList.size(); i++) {
				if (documentList.get(i).getEndDate().after(startDate)) {
					reminderList.add(documentList.get(i));
				}
			}

		} catch (Exception e) {
			System.out.println("SalesDocumentBean:" + e.toString());
		}

		return reminderList;
	}

	public void setReminderList(List<SalesDocument> reminderList) {
		this.reminderList = reminderList;
	}

	public String getToBeDeleted() {
		return toBeDeleted;
	}

	public void setToBeDeleted(String toBeDeleted) {
		this.toBeDeleted = toBeDeleted;
	}

	/**
	 * @return the employeeList
	 */
	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	/**
	 * @param employeeList
	 *            the employeeList to set
	 */
	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}

	/**
	 * @return the departmentId
	 */
	public Integer getDepartmentId() {
		return departmentId;
	}

	/**
	 * @param departmentId
	 *            the departmentId to set
	 */
	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * @return the selectedEmployees
	 */
	public Employee[] getSelectedEmployees() {
		return selectedEmployees;
	}

	/**
	 * @param selectedEmployees
	 *            the selectedEmployees to set
	 */
	public void setSelectedEmployees(Employee[] selectedEmployees) {
		this.selectedEmployees = selectedEmployees;
	}
}
