/**
 * 
 */
package com.emekboru.beans.satinalma;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.satinalma.SatinalmaDurum;
import com.emekboru.jpaman.satinalma.SatinalmaDurumManager;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "satinalmaDurumBean")
@ViewScoped
public class SatinalmaDurumBean implements Serializable {

	private static final long serialVersionUID = -4616107632599940175L;
	private List<SatinalmaDurum> durumListAmbar;
	private List<SatinalmaDurum> durumListMalzeme;
	private List<SatinalmaDurum> durumListTeklif;
	private List<SatinalmaDurum> durumListSatinalma;
	private List<SatinalmaDurum> durumListSiparisFormu;
	private List<SatinalmaDurum> durumListSiparisBildirimi;
	private List<SatinalmaDurum> durumListTedarikci;

	public SatinalmaDurumBean() {

		SatinalmaDurumManager manager = new SatinalmaDurumManager();
		durumListAmbar = manager.getAllSatinalmaDurum(1);
		durumListMalzeme = manager.getAllSatinalmaDurum(2);
		durumListTeklif = manager.getAllSatinalmaDurum(3);
		durumListSatinalma = manager.getAllSatinalmaDurum(4);
		durumListSiparisFormu = manager.getAllSatinalmaDurum(5);
		durumListSiparisBildirimi = manager.getAllSatinalmaDurum(6);
		durumListTedarikci = manager.getAllSatinalmaDurum(7);
	}

	// getters setters

	public List<SatinalmaDurum> getDurumListAmbar() {
		return durumListAmbar;
	}

	public void setDurumListAmbar(List<SatinalmaDurum> durumListAmbar) {
		this.durumListAmbar = durumListAmbar;
	}

	public List<SatinalmaDurum> getDurumListMalzeme() {
		return durumListMalzeme;
	}

	public void setDurumListMalzeme(List<SatinalmaDurum> durumListMalzeme) {
		this.durumListMalzeme = durumListMalzeme;
	}

	public List<SatinalmaDurum> getDurumListTeklif() {
		return durumListTeklif;
	}

	public void setDurumListTeklif(List<SatinalmaDurum> durumListTeklif) {
		this.durumListTeklif = durumListTeklif;
	}

	public List<SatinalmaDurum> getDurumListSiparisFormu() {
		return durumListSiparisFormu;
	}

	public void setDurumListSiparisFormu(
			List<SatinalmaDurum> durumListSiparisFormu) {
		this.durumListSiparisFormu = durumListSiparisFormu;
	}

	public List<SatinalmaDurum> getDurumListSiparisBildirimi() {
		return durumListSiparisBildirimi;
	}

	public void setDurumListSiparisBildirimi(
			List<SatinalmaDurum> durumListSiparisBildirimi) {
		this.durumListSiparisBildirimi = durumListSiparisBildirimi;
	}

	public List<SatinalmaDurum> getDurumListTedarikci() {
		return durumListTedarikci;
	}

	public void setDurumListTedarikci(List<SatinalmaDurum> durumListTedarikci) {
		this.durumListTedarikci = durumListTedarikci;
	}

	public List<SatinalmaDurum> getDurumListSatinalma() {
		return durumListSatinalma;
	}

	public void setDurumListSatinalma(List<SatinalmaDurum> durumListSatinalma) {
		this.durumListSatinalma = durumListSatinalma;
	}

}
