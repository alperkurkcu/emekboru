package com.emekboru.beans.license;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;

import com.emekboru.beans.crm.ActiveEmployeeList;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.License;
import com.emekboru.jpa.LicenseRenewResponsible;
import com.emekboru.jpaman.Factory;
import com.emekboru.jpaman.JpqlComparativeClauses;
import com.emekboru.jpaman.LicenseManager;
import com.emekboru.jpaman.LicenseRenewResponsibleManager;
import com.emekboru.jpaman.WhereClauseArgs;
import com.emekboru.utils.FacesContextUtils;


@ManagedBean
@ViewScoped
public class Licenses implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean              config;
	
	private List<License> 			licenses;
	private License 				selected;
	private LicenseRenewResponsible selectedRenewer;

	public Licenses() {

		licenses 		= new ArrayList<License>();
		selected 		= new License();
		selectedRenewer = new LicenseRenewResponsible();
	}

	@SuppressWarnings("rawtypes")
	@PostConstruct
	public void init(){
		
		LicenseManager manager = new LicenseManager();
		
		WhereClauseArgs<Boolean> arg1 = new WhereClauseArgs.Builder<Boolean>() 
				                                       .setFieldName("status")
				        .setComparativeClause(JpqlComparativeClauses.NQ_EQUAL)
				                 .setComplexKey(false).setValue(false).build();
		
		ArrayList<WhereClauseArgs> conditionList 
		                                    = new ArrayList<WhereClauseArgs>();
		conditionList.add(arg1);
		licenses = manager.selectFromWhereQuerie(License.class, conditionList);
	}
	
	public void add() {

		NewLicense licenseBean = 
				(NewLicense) FacesContextUtils.getRequestBean("newLicense");
		licenseBean.preprocess();
		LicenseManager manager  = new LicenseManager();
		manager.enterNew(licenseBean.getLicense());
		licenses.add(licenseBean.getLicense());
		licenseBean.setLicense(new License());
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void update() {

		LicenseManager manager = new LicenseManager();
		manager.updateEntity(selected);
		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}

	public void delete() {

		if(!canRemove(selected)){
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		EntityManager manager = Factory.getInstance().createEntityManager();
		selected = manager.find(License.class, selected.getLicenseId());
		manager.getTransaction().begin();
		manager.remove(selected);
		manager.getTransaction().commit();
		manager.close();
		licenses.remove(selected);
		selected = new License();
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	public void changeRenewer(){
		
		ActiveEmployeeList empBean = (ActiveEmployeeList) FacesContextUtils.
				 getViewBean("activeEmployeeList", ActiveEmployeeList.class);
		LicenseRenewResponsibleManager manager = new LicenseRenewResponsibleManager();
		int employeeId = selectedRenewer.getEmployee().getEmployeeId();
		selectedRenewer.setEmployee(empBean.find(employeeId));
		manager.updateEntity(selectedRenewer);
		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}

	
	
	public boolean canRemove(License license){
		
		if(license.getLicenseId()!=null)
			return true;
		return false;
	}
	
	public void goToPlanLicensesPage(){
		
		FacesContextUtils.redirect(config.getPageUrl().LICENSE_MANAGEMENT_PAGE);
	}
	
	/**
	 * GETTERS AND SETTERS
	 */
	public List<License> getLicenses() {
		return licenses;
	}

	public void setLicenses(List<License> licenses) {
		this.licenses = licenses;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public License getSelected() {
		return selected;
	}

	public void setSelected(License selected) {
		this.selected = selected;
	}

	public LicenseRenewResponsible getSelectedRenewer() {
		return selectedRenewer;
	}

	public void setSelectedRenewer(LicenseRenewResponsible selectedRenewer) {
		this.selectedRenewer = selectedRenewer;
	}
}
