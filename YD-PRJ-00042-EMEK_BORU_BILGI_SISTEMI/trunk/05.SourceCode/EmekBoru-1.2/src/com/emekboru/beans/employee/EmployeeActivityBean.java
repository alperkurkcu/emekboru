/**
 * 
 */
package com.emekboru.beans.employee;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Employee;
import com.emekboru.jpa.employee.EmployeeActivity;
import com.emekboru.jpa.employee.EmployeeActivityParticipation;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.EmployeeManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author kursat
 * 
 */
@ManagedBean(name = "employeeActivityBean")
@ViewScoped
public class EmployeeActivityBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean configBean;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userCredentialsBean;

	private List<EmployeeActivity> allEmployeeActivities = new ArrayList<EmployeeActivity>();
	private EmployeeActivity selectedEmployeeActivity = new EmployeeActivity();
	private EmployeeActivity newEmployeeActivity = new EmployeeActivity();

	private List<Employee> allEmployees = new ArrayList<Employee>();
	private Employee[] selectedEmployees;

	private CommonQueries<EmployeeActivity> employeeActivityManager = new CommonQueries<EmployeeActivity>();

	public EmployeeActivityBean() {
		super();
		this.load();
	}

	private void load() {
		this.allEmployeeActivities = employeeActivityManager
				.findAll(EmployeeActivity.class);

		EmployeeManager employeeManager = new EmployeeManager();
		allEmployees = employeeManager.findAll(Employee.class);
	}

	public void goToEmployeeActivityPage() {
		FacesContextUtils.redirect(configBean.getPageUrl().EMPLOYEE_ACTIVITY_PAGE);
	}

	public void addNewEmployeeActivity() {

		int ekleyenKullanici = this.userCredentialsBean.getUser().getId();
		Timestamp eklemeZamani = new java.sql.Timestamp(
				new java.util.Date().getTime());

		this.newEmployeeActivity.setEklemeZamani(eklemeZamani);
		this.newEmployeeActivity.setEkleyenKullanici(ekleyenKullanici);

		this.employeeActivityManager.enterNew(newEmployeeActivity);

		this.newEmployeeActivity = new EmployeeActivity();

		this.load();
	}

	public void updateSelectedEmployeeActivity() {

		int guncelleyenKullanici = this.userCredentialsBean.getUser().getId();
		Timestamp guncellemeZamani = new java.sql.Timestamp(
				new java.util.Date().getTime());

		this.selectedEmployeeActivity.setGuncellemeZamani(guncellemeZamani);
		this.selectedEmployeeActivity
				.setGuncelleyenKullanici(guncelleyenKullanici);

		this.employeeActivityManager.updateEntity(selectedEmployeeActivity);

		this.selectedEmployeeActivity = new EmployeeActivity();

		this.load();

	}

	public void deleteSelectedEmployeeActivity() {
		this.employeeActivityManager.delete(selectedEmployeeActivity);
		this.selectedEmployeeActivity = new EmployeeActivity();

		this.load();
	}

	public void addEmployeeActivityParticipations() {

		if (selectedEmployeeActivity == null || selectedEmployees.length == 0) {
			FacesContextUtils.addWarnMessage("UpdateItemMessage");
			return;
		}

		CommonQueries<EmployeeActivityParticipation> employeeActivityParticipationManager = new CommonQueries<EmployeeActivityParticipation>();

		EmployeeActivityParticipation employeeActivityParticipation;
		int ekleyenKullanici;
		Timestamp eklemeZamani;

		for (Employee employee : selectedEmployees) {

			ekleyenKullanici = this.userCredentialsBean.getUser().getId();
			eklemeZamani = new java.sql.Timestamp(
					new java.util.Date().getTime());

			employeeActivityParticipation = new EmployeeActivityParticipation();

			employeeActivityParticipation.setEmployee(employee);
			employeeActivityParticipation
					.setEmployeeActivity(selectedEmployeeActivity);
			employeeActivityParticipation.setEkleyenKullanici(ekleyenKullanici);
			employeeActivityParticipation.setEklemeZamani(eklemeZamani);

			employeeActivityParticipationManager
					.enterNew(employeeActivityParticipation);
		}

		FacesContext.getCurrentInstance().addMessage(
				null,
				new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Seçilen çalışanlar aktiviteye eklendi.", null));

	}

	/*
	 * 
	 * S E T T E R S
	 * 
	 * A N D
	 * 
	 * G E T T E R S
	 */

	public ConfigBean getConfigBean() {
		return configBean;
	}

	public void setConfigBean(ConfigBean configBean) {
		this.configBean = configBean;
	}

	public UserCredentialsBean getUserCredentialsBean() {
		return userCredentialsBean;
	}

	public void setUserCredentialsBean(UserCredentialsBean userCredentialsBean) {
		this.userCredentialsBean = userCredentialsBean;
	}

	public List<EmployeeActivity> getAllEmployeeActivities() {
		return allEmployeeActivities;
	}

	public void setAllEmployeeActivities(
			List<EmployeeActivity> allEmployeeActivities) {
		this.allEmployeeActivities = allEmployeeActivities;
	}

	public EmployeeActivity getSelectedEmployeeActivity() {
		return selectedEmployeeActivity;
	}

	public void setSelectedEmployeeActivity(
			EmployeeActivity selectedEmployeeActivity) {
		this.selectedEmployeeActivity = selectedEmployeeActivity;
	}

	public EmployeeActivity getNewEmployeeActivity() {
		return newEmployeeActivity;
	}

	public void setNewEmployeeActivity(EmployeeActivity newEmployeeActivity) {
		this.newEmployeeActivity = newEmployeeActivity;
	}

	public List<Employee> getAllEmployees() {
		return allEmployees;
	}

	public void setAllEmployees(List<Employee> allEmployees) {
		this.allEmployees = allEmployees;
	}

	public Employee[] getSelectedEmployees() {
		return selectedEmployees;
	}

	public void setSelectedEmployees(Employee[] selectedEmployees) {
		this.selectedEmployees = selectedEmployees;
	}

}
