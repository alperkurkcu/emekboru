package com.emekboru.beans.yetki;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;

import org.primefaces.event.NodeSelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.YetkiMenu;
import com.emekboru.jpaman.LkYetkiRolManager;
import com.emekboru.jpaman.YetkiMenuKullaniciErisimManager;
import com.emekboru.jpaman.YetkiMenuManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.YetkiTreeNode;

@ManagedBean(name = "yetkiMenuBean")
@SessionScoped
public class YetkiMenuBean implements Serializable {

	private static final long serialVersionUID = 9009185176901506810L;

	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private List<YetkiMenu> menuList;
	private YetkiTreeNode allYetkiMenuTree = null;
	private YetkiTreeNode selectedNode;
	private YetkiMenu selectedMenu = new YetkiMenu();

	private String actionSinifMetot;
	private String ekranTanimi;
	private Integer menuSira;
	private Integer parentMenuId;
	private boolean isSubMenu;

	private boolean addPageRendered;
	private boolean removePageRendered;

	public YetkiMenuBean() {
		menuList = new ArrayList<YetkiMenu>();
	}

	public void goToNewYetkiMenuPage() {

		allYetkiMenuTree = YetkiMenuManager.getAllYetkiMenuTree(false);
		FacesContextUtils.redirect(config.getPageUrl().NEW_YETKI_MENU_PAGE);
	}

	public void onRowSelect(NodeSelectEvent event) {
		if (selectedNode != null)
			selectedMenu = YetkiMenuManager.getYetkiMenuById(selectedNode
					.getObjectId());

	}

	public void goToManageYetkiPage() {
		allYetkiMenuTree = YetkiMenuManager.getAllYetkiMenuTree(true);
		FacesContextUtils.redirect(config.getPageUrl().MANAGE_YETKI_MENU_PAGE);
	}

	public void cancelNewPage() {

		allYetkiMenuTree = YetkiMenuManager.getAllYetkiMenuTree(false);
		selectedNode = null;
		addPageRendered = false;
	}

	public void newPage(ActionEvent e) {
		addPageRendered = true;
		YetkiMenu yetkiMenu = new YetkiMenu();
		yetkiMenu.setEkranTanimi(ekranTanimi);
		yetkiMenu.setMenuSira(menuSira);
		yetkiMenu.setActionSinifMetot(actionSinifMetot);
		yetkiMenu.setSubMenu(isSubMenu);
		yetkiMenu.setEkleyenKullanici(userBean.getUser().getId());
		yetkiMenu.setEklemeZamani(new java.sql.Timestamp(new java.util.Date()
				.getTime()));
		if (selectedNode != null) {
			yetkiMenu.setParentMenuId(selectedNode.getObjectId());
		}
		YetkiMenuManager.addOrUpdateLkYetkiMenu(yetkiMenu);

		allYetkiMenuTree = YetkiMenuManager.getAllYetkiMenuTree(false);

	}

	public void updatePage(ActionEvent e) {
		YetkiMenuManager.addOrUpdateLkYetkiMenu(selectedMenu);
		allYetkiMenuTree = YetkiMenuManager.getAllYetkiMenuTree(true);
		// userBean.refreshModel();
	}

	@SuppressWarnings("unused")
	public void deletePage(ActionEvent event) {
		if (selectedMenu == null)
			FacesContextUtils.addInfoMessage("SelectPageBeforeRemoving");
		int menuKullaniciErisimListSize = YetkiMenuKullaniciErisimManager
				.getYetkiMenuKullaniciErisimByMenuId(selectedMenu.getId())
				.size();
		int menuRolListSize = LkYetkiRolManager.getRoleByMenuId(
				selectedMenu.getId()).size();

		if (menuKullaniciErisimListSize > 0) {
			// FacesContextUtils.addInfoMessage("RemoveErisimBeforeRemovingPage");
			removePageRendered = true;
		}
	}

	@SuppressWarnings("rawtypes")
	public List getActionMenuList() {
		YetkiMenuManager manager = new YetkiMenuManager();
		return manager.findAll(YetkiMenu.class);
	}

	public List<YetkiMenu> getMenuList() {
		return menuList;
	}

	public void setMenuList(List<YetkiMenu> menuList) {
		this.menuList = menuList;
	}

	public YetkiTreeNode getAllYetkiMenuTree() {
		return allYetkiMenuTree;
	}

	public void setAllYetkiMenuTree(YetkiTreeNode allYetkiMenuTree) {
		this.allYetkiMenuTree = allYetkiMenuTree;
	}

	public YetkiTreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(YetkiTreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public String getActionSinifMetot() {
		return actionSinifMetot;
	}

	public void setActionSinifMetot(String actionSinifMetot) {
		this.actionSinifMetot = actionSinifMetot;
	}

	public String getEkranTanimi() {
		return ekranTanimi;
	}

	public void setEkranTanimi(String ekranTanimi) {
		this.ekranTanimi = ekranTanimi;
	}

	public Integer getMenuSira() {
		return menuSira;
	}

	public void setMenuSira(Integer menuSira) {
		this.menuSira = menuSira;
	}

	public Integer getParentMenuId() {
		return parentMenuId;
	}

	public void setParentMenuId(Integer parentMenuId) {
		this.parentMenuId = parentMenuId;
	}

	public boolean isSubMenu() {
		return isSubMenu;
	}

	public void setSubMenu(boolean isSubMenu) {
		this.isSubMenu = isSubMenu;
	}

	public boolean isAddPageRendered() {
		return addPageRendered;
	}

	public void setAddPageRendered(boolean addPageRendered) {
		this.addPageRendered = addPageRendered;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public YetkiMenu getSelectedMenu() {
		return selectedMenu;
	}

	public void setSelectedMenu(YetkiMenu selectedMenu) {
		this.selectedMenu = selectedMenu;
	}

	public boolean isRemovePageRendered() {
		return removePageRendered;
	}

	public void setRemovePageRendered(boolean removePageRendered) {
		this.removePageRendered = removePageRendered;
	}

}
