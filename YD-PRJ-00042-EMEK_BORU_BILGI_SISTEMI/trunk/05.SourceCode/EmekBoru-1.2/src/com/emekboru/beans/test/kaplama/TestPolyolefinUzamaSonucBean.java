package com.emekboru.beans.test.kaplama;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestPolyolefinUzamaSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestPolyolefinUzamaSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testPolyolefinUzamaSonucBean")
@ViewScoped
public class TestPolyolefinUzamaSonucBean {

	private List<TestPolyolefinUzamaSonuc> allPolyolefinUzamaSonucList = new ArrayList<TestPolyolefinUzamaSonuc>();
	private TestPolyolefinUzamaSonuc testPolyolefinUzamaSonucForm = new TestPolyolefinUzamaSonuc();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;
	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addPolyolefinUzamaTestSonuc(ActionEvent e) {
		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestPolyolefinUzamaSonucManager manager = new TestPolyolefinUzamaSonucManager();

		if (testPolyolefinUzamaSonucForm.getId() == null) {

			testPolyolefinUzamaSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testPolyolefinUzamaSonucForm.setEkleyenKullanici(userBean.getUser()
					.getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testPolyolefinUzamaSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testPolyolefinUzamaSonucForm.setBagliTestId(bagliTest);
			testPolyolefinUzamaSonucForm.setBagliGlobalId(bagliTest);

			manager.enterNew(testPolyolefinUzamaSonucForm);
			FacesContextUtils.addInfoMessage("SubmitMessage");
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testPolyolefinUzamaSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testPolyolefinUzamaSonucForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			manager.updateEntity(testPolyolefinUzamaSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");

		}

		fillTestList(prmGlobalId, prmPipeId);
	}

	public void addPolyolefinUzamaTest() {
		testPolyolefinUzamaSonucForm = new TestPolyolefinUzamaSonuc();
		updateButtonRender = false;
	}

	public void polyolefinUzamaListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(int globalId, Integer pipeId2) {
		allPolyolefinUzamaSonucList = TestPolyolefinUzamaSonucManager
				.getAllPolyolefinUzamaTestSonuc(globalId, pipeId2);
	}

	public void deleteTestPolyolefinUzamaSonuc() {

		bagliTestId = testPolyolefinUzamaSonucForm.getBagliTestId().getId();

		if (testPolyolefinUzamaSonucForm == null
				|| testPolyolefinUzamaSonucForm.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestPolyolefinUzamaSonucManager manager = new TestPolyolefinUzamaSonucManager();
		manager.delete(testPolyolefinUzamaSonucForm);
		testPolyolefinUzamaSonucForm = new TestPolyolefinUzamaSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setters getters
	public List<TestPolyolefinUzamaSonuc> getAllPolyolefinUzamaSonucList() {
		return allPolyolefinUzamaSonucList;
	}

	public void setAllPolyolefinUzamaSonucList(
			List<TestPolyolefinUzamaSonuc> allPolyolefinUzamaSonucList) {
		this.allPolyolefinUzamaSonucList = allPolyolefinUzamaSonucList;
	}

	public TestPolyolefinUzamaSonuc getTestPolyolefinUzamaSonucForm() {
		return testPolyolefinUzamaSonucForm;
	}

	public void setTestPolyolefinUzamaSonucForm(
			TestPolyolefinUzamaSonuc testPolyolefinUzamaSonucForm) {
		this.testPolyolefinUzamaSonucForm = testPolyolefinUzamaSonucForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
