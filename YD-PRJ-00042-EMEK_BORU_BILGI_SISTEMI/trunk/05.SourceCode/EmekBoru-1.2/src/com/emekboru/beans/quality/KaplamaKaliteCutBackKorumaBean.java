/**
 * 
 */
package com.emekboru.beans.quality;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.kaplamatestspecs.KaplamaKaliteCutBackKoruma;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "kaplamaKaliteCutBackKorumaBean")
@ViewScoped
public class KaplamaKaliteCutBackKorumaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private KaplamaKaliteCutBackKoruma kaplamaKaliteCutBackKorumaaForm = new KaplamaKaliteCutBackKoruma();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private List<KaplamaKaliteCutBackKoruma> allKaplamaKaliteCutBackKorumaList = new ArrayList<KaplamaKaliteCutBackKoruma>();

	// setters getters
	/**
	 * @return the kaplamaKaliteCutBackKorumaaForm
	 */
	public KaplamaKaliteCutBackKoruma getKaplamaKaliteCutBackKorumaaForm() {
		return kaplamaKaliteCutBackKorumaaForm;
	}

	/**
	 * @param kaplamaKaliteCutBackKorumaaForm
	 *            the kaplamaKaliteCutBackKorumaaForm to set
	 */
	public void setKaplamaKaliteCutBackKorumaaForm(
			KaplamaKaliteCutBackKoruma kaplamaKaliteCutBackKorumaaForm) {
		this.kaplamaKaliteCutBackKorumaaForm = kaplamaKaliteCutBackKorumaaForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the allKaplamaKaliteCutBackKorumaList
	 */
	public List<KaplamaKaliteCutBackKoruma> getAllKaplamaKaliteCutBackKorumaList() {
		return allKaplamaKaliteCutBackKorumaList;
	}

	/**
	 * @param allKaplamaKaliteCutBackKorumaList
	 *            the allKaplamaKaliteCutBackKorumaList to set
	 */
	public void setAllKaplamaKaliteCutBackKorumaList(
			List<KaplamaKaliteCutBackKoruma> allKaplamaKaliteCutBackKorumaList) {
		this.allKaplamaKaliteCutBackKorumaList = allKaplamaKaliteCutBackKorumaList;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
