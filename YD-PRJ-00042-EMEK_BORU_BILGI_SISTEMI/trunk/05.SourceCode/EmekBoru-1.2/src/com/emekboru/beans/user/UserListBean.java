package com.emekboru.beans.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Department;
import com.emekboru.jpa.Employee;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.UserType;
import com.emekboru.jpaman.DepartmentManager;
import com.emekboru.jpaman.EmployeeManager;
import com.emekboru.jpaman.JpqlComparativeClauses;
import com.emekboru.jpaman.UserManager;
import com.emekboru.jpaman.UserTypeManager;
import com.emekboru.jpaman.WhereClauseArgs;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "userListBean")
@SessionScoped
public class UserListBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	private List<SystemUser> departmentUserList;
	private static List<SystemUser> userList;
	private static List<Department> departmentList;
	private static List<UserType> userTypeList;
	private static List<Employee> noUserEmployeeList;

	public UserListBean() {

		userList = new ArrayList<SystemUser>();
		departmentUserList = new ArrayList<SystemUser>();
		departmentList = new ArrayList<Department>();
		userTypeList = new ArrayList<UserType>();
		noUserEmployeeList = new ArrayList<Employee>();
	}

	public static void loadUserTypeList() {

		UserTypeManager typeManager = new UserTypeManager();
		// userTypeList = typeManager.findAll(UserType.class);
		userTypeList = typeManager.findAllOrderByASC(UserType.class, "type");
	}

	public static void loadDepartmentList() {

		DepartmentManager departmentManager = new DepartmentManager();
		// departmentList = departmentManager.findAll(Department.class);
		departmentList = departmentManager.findAllOrderByASC(Department.class,
				"name");
	}

	public static void loadUserList() {

		UserManager userManager = new UserManager();
		// userList = userManager.findAll(SystemUser.class);
		userList = userManager.findAllOrderByASC(SystemUser.class, "username");
		loadUserTypeList();
		loadNoUserEmployeeList();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void loadDepartmentUserList(int departmentId) {

		UserManager userManager = new UserManager();
		ArrayList<WhereClauseArgs> conditionList = new ArrayList<WhereClauseArgs>();
		WhereClauseArgs arg1 = new WhereClauseArgs.Builder()
				.setComparativeClause(JpqlComparativeClauses.NQ_EQUAL)
				.setFieldName("employee.department.departmentId")
				.setValue(departmentId).build();
		conditionList.add(arg1);
		departmentUserList = userManager.selectFromWhereQuerieComplexName(
				SystemUser.class, conditionList);
	}

	public static void loadNoUserEmployeeList() {

		EmployeeManager employeeManager = new EmployeeManager();
		// List<Employee> list = employeeManager.findAll(Employee.class);
		List<Employee> list = employeeManager.findAllOrderByASC(Employee.class,
				"firstname");
		for (Employee e : list) {
			if (e.getUsers().size() == 0)
				noUserEmployeeList.add(e);
		}
	}

	/*
	 * public void goToNewUserPage(ActionEvent event) {
	 * 
	 * loadUserList();
	 * FacesContextUtils.redirect(config.getPageUrl().NEW_USER_PAGE); } public
	 * void goToUserManagePage(ActionEvent event) {
	 * 
	 * loadUserList();
	 * FacesContextUtils.redirect(config.getPageUrl().MANAGE_USER_PAGE); }
	 * 
	 * public void goToNewUserTypePage(ActionEvent event) {
	 * 
	 * loadUserTypeList();
	 * FacesContextUtils.redirect(config.getPageUrl().NEW_USERTYPE_PAGE); }
	 * 
	 * public void goToManageUserTypePage(ActionEvent event) {
	 * 
	 * loadUserTypeList();
	 * FacesContextUtils.redirect(config.getPageUrl().MANAGE_USERTYPE_PAGE); }
	 */

	public void goToNewUserPage() {

		loadUserList();

		FacesContextUtils.redirect(config.getPageUrl().NEW_USER_PAGE);
		// FacesContextUtils.redirect("user/newuser.jsf");
	}

	public void goToUserManagePage() {

		loadUserList();
		FacesContextUtils.redirect(config.getPageUrl().MANAGE_USER_PAGE);
		// FacesContextUtils.redirect("user/manageuser.jsf");
	}

	public void goToNewUserTypePage() {

		loadUserTypeList();
		FacesContextUtils.redirect(config.getPageUrl().NEW_USERTYPE_PAGE);
	}

	public void goToManageUserTypePage() {

		loadUserTypeList();
		FacesContextUtils.redirect(config.getPageUrl().MANAGE_USERTYPE_PAGE);
	}

	/**
	 * GETTERS AND SETTERS
	 */
	public List<SystemUser> getDepartmentUserList() {
		return departmentUserList;
	}

	public void setDepartmentUserList(List<SystemUser> departmentUserList) {
		this.departmentUserList = departmentUserList;
	}

	public List<SystemUser> getUserList() {
		return userList;
	}

	@SuppressWarnings("static-access")
	public void setUserList(List<SystemUser> userList) {
		this.userList = userList;
	}

	public List<Department> getDepartmentList() {
		return departmentList;
	}

	@SuppressWarnings("static-access")
	public void setDepartmentList(List<Department> departmentList) {
		this.departmentList = departmentList;
	}

	public List<UserType> getUserTypeList() {
		return userTypeList;
	}

	@SuppressWarnings("static-access")
	public void setUserTypeList(List<UserType> userTypeList) {
		this.userTypeList = userTypeList;
	}

	public List<Employee> getNoUserEmployeeList() {
		return noUserEmployeeList;
	}

	@SuppressWarnings("static-access")
	public void setNoUserEmployeeList(List<Employee> noUserEmployeeList) {
		this.noUserEmployeeList = noUserEmployeeList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
