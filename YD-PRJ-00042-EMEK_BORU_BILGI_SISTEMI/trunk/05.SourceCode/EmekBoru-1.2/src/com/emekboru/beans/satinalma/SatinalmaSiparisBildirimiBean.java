/**
 * 
 */
package com.emekboru.beans.satinalma;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.satinalma.SatinalmaSiparisBildirimi;
import com.emekboru.jpa.satinalma.SatinalmaSiparisMektubu;
import com.emekboru.jpa.satinalma.SatinalmaSiparisMektubuIcerik;
import com.emekboru.jpaman.satinalma.SatinalmaSiparisBildirimiManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "satinalmaSiparisBildirimiBean")
@ViewScoped
public class SatinalmaSiparisBildirimiBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	@ManagedProperty(value = "#{satinalmaOdemeBildirimiBean}")
	private SatinalmaOdemeBildirimiBean satinalmaOdemeBildirimiBean;

	private boolean updateButtonRender;

	private SatinalmaSiparisBildirimi satinalmaSiparisBildirimiForm = new SatinalmaSiparisBildirimi();

	private SatinalmaSiparisMektubu satinalmaSiparisMektubuForm = new SatinalmaSiparisMektubu();
	private SatinalmaSiparisMektubuIcerik satinalmaSiparisMektubuIcerikForm = new SatinalmaSiparisMektubuIcerik();

	private List<SatinalmaSiparisBildirimi> allSatinalmaSiparisBildirimiList = new ArrayList<SatinalmaSiparisBildirimi>();

	private String downloadedFileName;
	private StreamedContent downloadedFile;

	private File[] fileArray;

	private File theFile = null;
	OutputStream output = null;
	InputStream input = null;

	private String path;
	private String privatePath;
	private String toBeDeleted;

	@PostConstruct
	public void load() {
		updateButtonRender = false;
		fillTestList();
		System.out.println("satinalmaOdemeBildirimiBean.load()");

		try {
			this.setPath(config.getConfig().getFolder().getAbsolutePath()
					+ privatePath);

			System.out
					.println("Path for Upload File (satinalmaSiparisBildirimi):			"
							+ path);
		} catch (Exception ex) {
			System.out
					.println("Path for Upload File (satinalmaSiparisBildirimi):			"
							+ FacesContext.getCurrentInstance()
									.getExternalContext()
									.getRealPath(privatePath));

			System.out.print(path);
			System.out.println("CAUSE OF " + ex.toString());
		}

		theFile = new File(path);
		if (!theFile.isDirectory()) {
			theFile.mkdirs();
		}

		fileArray = theFile.listFiles();
	}

	public SatinalmaSiparisBildirimiBean() {

		// fillTestList();
		privatePath = File.separatorChar + "satinalma" + File.separatorChar
				+ "uploadedDocuments" + File.separatorChar + "siparisBildirimi"
				+ File.separatorChar;
	}

	public void fillTestList() {

		SatinalmaSiparisBildirimiManager manager = new SatinalmaSiparisBildirimiManager();
		allSatinalmaSiparisBildirimiList = manager
				.getAllSatinalmaSiparisBildirimi();
		satinalmaSiparisBildirimiForm = new SatinalmaSiparisBildirimi();
		updateButtonRender = false;
	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void addOrUpdateForm() {

		SatinalmaSiparisBildirimiManager manager = new SatinalmaSiparisBildirimiManager();

		if (satinalmaSiparisBildirimiForm.getId() == null) {

			satinalmaSiparisBildirimiForm.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			satinalmaSiparisBildirimiForm.setEkleyenKullanici(userBean
					.getUser().getId());
			try {
				manager.enterNew(satinalmaSiparisBildirimiForm);
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"SİPARİŞ BİLDİRİMİ BAŞARIYLA EKLENDİ!", null));
			} catch (Exception e) {
				System.out
						.println("satinalmaSiparisMektubuBean.addOrUpdateForm: "
								+ e.toString());
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"SİPARİŞ BİLDİRİMİ OLUŞTURULAMADI, HATA!", null));
			}
		} else {

			satinalmaSiparisBildirimiForm.setGuncellemeZamani(UtilInsCore
					.getTarihZaman());
			satinalmaSiparisBildirimiForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			try {
				manager.updateEntity(satinalmaSiparisBildirimiForm);
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"SİPARİŞ BİLDİRİMİ BAŞARIYLA GÜNCELLENDİ!", null));
			} catch (Exception e) {
				System.out.println("satinalmaSiparisMektubuBean.addFromKarar: "
						+ e.toString());
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"SİPARİŞ BİLDİRİMİ GÜNCELLENEMEDİ, HATA!", null));
			}
		}
		fillTestList();
	}

	public void addFromSiparisMektubu(
			SatinalmaSiparisMektubu satinalmaSiparisMektubu) {

		SatinalmaSiparisBildirimiManager manager = new SatinalmaSiparisBildirimiManager();

		this.satinalmaSiparisMektubuForm = satinalmaSiparisMektubu;

		satinalmaSiparisBildirimiForm
				.setSatinalmaSiparisMektubu(satinalmaSiparisMektubuForm);
		satinalmaSiparisBildirimiForm.setEklemeZamani(UtilInsCore
				.getTarihZaman());
		satinalmaSiparisBildirimiForm.setEkleyenKullanici(userBean.getUser()
				.getId());
		try {
			manager.enterNew(satinalmaSiparisBildirimiForm);
		} catch (Exception e) {
			System.out.println("satinalmaSiparisMektubuBean.addFromKarar: "
					+ e.toString());
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"SİPARİŞ BİLDİRİMİ OLUŞTURULAMADI, HATA!", null));
		}

		satinalmaSiparisBildirimiForm = new SatinalmaSiparisBildirimi();
		satinalmaSiparisMektubuIcerikForm = new SatinalmaSiparisMektubuIcerik();
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				"SİPARİŞ BİLDİRİMİ BAŞARIYLA OLUŞTURULDU!", null));
	}

	public void odemeBildirimiOlustur() {

		satinalmaOdemeBildirimiBean
				.addFromSiparisBildirimi(satinalmaSiparisBildirimiForm);
	}

	public void addUploadedFile(FileUploadEvent event)
			throws AbortProcessingException, IOException {
		System.out.println("satinalmaSiparisBildirimiBean.addUploadedFile()");

		try {
			String name = this.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				satinalmaSiparisBildirimiForm
						.setDocuments(satinalmaSiparisBildirimiForm
								.getDocuments() + "//" + name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				SatinalmaSiparisBildirimiManager manager = new SatinalmaSiparisBildirimiManager();
				manager.updateEntity(satinalmaSiparisBildirimiForm);

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				FacesContextUtils.addWarnMessage("salesFileUploadedMessage");
			} else {
				System.out.println("File already exists.");

				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out
					.println("satinalmaSiparisBildirimiBean: " + e.toString());
		}
	}

	public void deleteTheSelectedFile() {
		System.out
				.println("satinalmaSiparisBildirimiBean.deleteTheSelectedFile()");
		try {
			// Delete the uploaded file
			theFile = new File(path);
			fileArray = theFile.listFiles();

			System.out.println("document that will be deleted: " + privatePath
					+ toBeDeleted);
			for (int j = 0; j < fileArray.length; j++) {
				System.out.println("fileArray[j].toString(): "
						+ fileArray[j].toString());
				if (fileArray[j].toString().contains(privatePath + toBeDeleted)) {
					fileArray[j].delete();

					if (satinalmaSiparisBildirimiForm.getDocuments()
							.contentEquals("//" + toBeDeleted)) {
						satinalmaSiparisBildirimiForm
								.setDocuments(satinalmaSiparisBildirimiForm
										.getDocuments().replace(
												"//" + toBeDeleted, "null"));
					} else {
						satinalmaSiparisBildirimiForm
								.setDocuments(satinalmaSiparisBildirimiForm
										.getDocuments().replace(
												"//" + toBeDeleted, ""));
					}
					FacesContextUtils
							.addWarnMessage("salesTheFileIsDeletedMessage");
				}
			}
		} catch (Exception e) {
			System.out
					.println("satinalmaSiparisBildirimiBean: " + e.toString());
		}
	}

	public void deleteAndUpload(FileUploadEvent event)
			throws AbortProcessingException, IOException {

		System.out.println("satinalmaSiparisBildirimiBean.deleteAndUpload()");

		try {
			// Delete the uploaded file
			theFile = new File(path);
			fileArray = theFile.listFiles();

			for (int i = 0; i < satinalmaSiparisBildirimiForm.getFileNames()
					.size(); i++) {
				for (int j = 0; j < fileArray.length; j++) {
					System.out.println("fileArray[j].toString(): "
							+ fileArray[j].toString());
					if (fileArray[j].toString().contains(
							privatePath
									+ satinalmaSiparisBildirimiForm
											.getFileNames().get(i))) {
						fileArray[j].delete();
					}
				}
			}

			String name = this.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				satinalmaSiparisBildirimiForm.setDocuments("//" + name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				SatinalmaSiparisBildirimiManager manager = new SatinalmaSiparisBildirimiManager();
				manager.updateEntity(satinalmaSiparisBildirimiForm);

				FacesContextUtils.addInfoMessage("UpdateItemMessage");
			} else {
				System.out.println("File already exists.");

				satinalmaSiparisBildirimiForm.setDocuments(null);
				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out
					.println("satinalmaSiparisBildirimiBean: " + e.toString());
		}
	}

	public String checkFileName(String fileName) {

		fileName = fileName.replace(" ", "_");
		fileName = fileName.replace("ç", "c");
		fileName = fileName.replace("Ç", "C");
		fileName = fileName.replace("ğ", "g");
		fileName = fileName.replace("Ğ", "G");
		fileName = fileName.replace("İ", "i");
		fileName = fileName.replace("ı", "i");
		fileName = fileName.replace("ö", "o");
		fileName = fileName.replace("Ö", "O");
		fileName = fileName.replace("ş", "s");
		fileName = fileName.replace("Ş", "S");
		fileName = fileName.replace("ü", "u");
		fileName = fileName.replace("Ü", "U");

		return fileName;
	}

	// getters setters
	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the satinalmaSiparisBildirimiForm
	 */
	public SatinalmaSiparisBildirimi getSatinalmaSiparisBildirimiForm() {
		return satinalmaSiparisBildirimiForm;
	}

	/**
	 * @param satinalmaSiparisBildirimiForm
	 *            the satinalmaSiparisBildirimiForm to set
	 */
	public void setSatinalmaSiparisBildirimiForm(
			SatinalmaSiparisBildirimi satinalmaSiparisBildirimiForm) {
		this.satinalmaSiparisBildirimiForm = satinalmaSiparisBildirimiForm;
	}

	/**
	 * @return the satinalmaSiparisMektubuIcerikForm
	 */
	public SatinalmaSiparisMektubuIcerik getSatinalmaSiparisMektubuIcerikForm() {
		return satinalmaSiparisMektubuIcerikForm;
	}

	/**
	 * @param satinalmaSiparisMektubuIcerikForm
	 *            the satinalmaSiparisMektubuIcerikForm to set
	 */
	public void setSatinalmaSiparisMektubuIcerikForm(
			SatinalmaSiparisMektubuIcerik satinalmaSiparisMektubuIcerikForm) {
		this.satinalmaSiparisMektubuIcerikForm = satinalmaSiparisMektubuIcerikForm;
	}

	/**
	 * @return the allSatinalmaSiparisBildirimiList
	 */
	public List<SatinalmaSiparisBildirimi> getAllSatinalmaSiparisBildirimiList() {
		return allSatinalmaSiparisBildirimiList;
	}

	/**
	 * @param allSatinalmaSiparisBildirimiList
	 *            the allSatinalmaSiparisBildirimiList to set
	 */
	public void setAllSatinalmaSiparisBildirimiList(
			List<SatinalmaSiparisBildirimi> allSatinalmaSiparisBildirimiList) {
		this.allSatinalmaSiparisBildirimiList = allSatinalmaSiparisBildirimiList;
	}

	/**
	 * @return the satinalmaOdemeBildirimiBean
	 */
	public SatinalmaOdemeBildirimiBean getSatinalmaOdemeBildirimiBean() {
		return satinalmaOdemeBildirimiBean;
	}

	/**
	 * @param satinalmaOdemeBildirimiBean
	 *            the satinalmaOdemeBildirimiBean to set
	 */
	public void setSatinalmaOdemeBildirimiBean(
			SatinalmaOdemeBildirimiBean satinalmaOdemeBildirimiBean) {
		this.satinalmaOdemeBildirimiBean = satinalmaOdemeBildirimiBean;
	}

	/**
	 * @return the satinalmaSiparisMektubuForm
	 */
	public SatinalmaSiparisMektubu getSatinalmaSiparisMektubuForm() {
		return satinalmaSiparisMektubuForm;
	}

	/**
	 * @param satinalmaSiparisMektubuForm
	 *            the satinalmaSiparisMektubuForm to set
	 */
	public void setSatinalmaSiparisMektubuForm(
			SatinalmaSiparisMektubu satinalmaSiparisMektubuForm) {
		this.satinalmaSiparisMektubuForm = satinalmaSiparisMektubuForm;
	}

	@SuppressWarnings("resource")
	public StreamedContent getDownloadedFile() {
		System.out.println("satinalmaSiparisBildirimiBean.getDownloadedFile()");

		try {
			if (downloadedFileName != null) {
				RandomAccessFile accessFile = new RandomAccessFile(path
						+ downloadedFileName, "r");
				byte[] downloadByte = new byte[(int) accessFile.length()];
				accessFile.read(downloadByte);
				InputStream stream = new ByteArrayInputStream(downloadByte);

				String suffix = FilenameUtils.getExtension(downloadedFileName);
				String contentType = "text/plain";
				if (suffix.contentEquals("jpg") || suffix.contentEquals("png")
						|| suffix.contentEquals("gif")) {
					contentType = "image/" + contentType;
				} else if (suffix.contentEquals("doc")
						|| suffix.contentEquals("docx")) {
					contentType = "application/msword";
				} else if (suffix.contentEquals("xls")
						|| suffix.contentEquals("xlsx")) {
					contentType = "application/vnd.ms-excel";
				} else if (suffix.contentEquals("pdf")) {
					contentType = "application/pdf";
				}
				downloadedFile = new DefaultStreamedContent(stream,
						"contentType", downloadedFileName);

				stream.close();
			}
		} catch (Exception e) {
			System.out
					.println("satinalmaSiparisBildirimiBean.getDownloadedFile():"
							+ e.toString());
		}
		return downloadedFile;
	}

	/**
	 * @return the downloadedFileName
	 */
	public String getDownloadedFileName() {
		return downloadedFileName;
	}

	/**
	 * @param downloadedFileName
	 *            the downloadedFileName to set
	 */
	public void setDownloadedFileName(String downloadedFileName) {
		this.downloadedFileName = downloadedFileName;
	}

	/**
	 * @param downloadedFile
	 *            the downloadedFile to set
	 */
	public void setDownloadedFile(StreamedContent downloadedFile) {
		this.downloadedFile = downloadedFile;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path
	 *            the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}
}
