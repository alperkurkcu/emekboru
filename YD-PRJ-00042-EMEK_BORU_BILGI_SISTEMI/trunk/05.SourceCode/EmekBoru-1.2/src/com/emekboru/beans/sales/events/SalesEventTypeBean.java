package com.emekboru.beans.sales.events;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.sales.events.SalesEventType;
import com.emekboru.jpaman.sales.events.SalesEventTypeManager;

@ManagedBean(name = "salesEventTypeBean")
@ViewScoped
public class SalesEventTypeBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6297926975171818537L;
	private List<SalesEventType> eventTypeList;

	public SalesEventTypeBean() {

		SalesEventTypeManager manager = new SalesEventTypeManager();
		eventTypeList = manager.findAll(SalesEventType.class);
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public List<SalesEventType> geteventTypeList() {
		return eventTypeList;
	}

	public void setEventTypeList(List<SalesEventType> eventTypeList) {
		this.eventTypeList = eventTypeList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}