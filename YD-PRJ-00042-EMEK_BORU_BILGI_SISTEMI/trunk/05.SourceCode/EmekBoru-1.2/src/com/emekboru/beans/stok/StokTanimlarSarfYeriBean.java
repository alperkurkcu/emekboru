/**
 * 
 */
package com.emekboru.beans.stok;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.stok.StokTanimlarSarfYeri;
import com.emekboru.jpaman.stok.StokTanimlarSarfYeriManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "stokTanimlarSarfYeriBean")
@ViewScoped
public class StokTanimlarSarfYeriBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<StokTanimlarSarfYeri> allStokTanimlarSarfYeriList = new ArrayList<StokTanimlarSarfYeri>();

	private StokTanimlarSarfYeri stokTanimlarSarfYeriForm = new StokTanimlarSarfYeri();

	StokTanimlarSarfYeriManager formManager = new StokTanimlarSarfYeriManager();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	public StokTanimlarSarfYeriBean() {

		fillTestList();
	}

	public void addForm() {

		stokTanimlarSarfYeriForm = new StokTanimlarSarfYeri();
		updateButtonRender = false;
	}

	public void addOrUpdateForm() {

		if (stokTanimlarSarfYeriForm.getId() == null) {

			stokTanimlarSarfYeriForm.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			stokTanimlarSarfYeriForm.setEkleyenKullanici(userBean.getUser()
					.getId());

			formManager.enterNew(stokTanimlarSarfYeriForm);

			this.stokTanimlarSarfYeriForm = new StokTanimlarSarfYeri();
			FacesContextUtils.addInfoMessage("FormSubmitMessage");

		} else {

			stokTanimlarSarfYeriForm.setGuncellemeZamani(UtilInsCore
					.getTarihZaman());
			stokTanimlarSarfYeriForm.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			formManager.updateEntity(stokTanimlarSarfYeriForm);

			FacesContextUtils.addInfoMessage("FormUpdateMessage");
		}

		fillTestList();
	}

	public void deleteForm() {

		if (stokTanimlarSarfYeriForm.getId() == null) {

			FacesContextUtils.addWarnMessage("NoSelectMessage");
			return;
		}
		formManager.delete(stokTanimlarSarfYeriForm);
		fillTestList();
		FacesContextUtils.addWarnMessage("TestDeleteMessage");
	}

	public void fillTestList() {

		StokTanimlarSarfYeriManager manager = new StokTanimlarSarfYeriManager();
		allStokTanimlarSarfYeriList = manager
				.getAllStokTanimlarSarfYeri();
	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	// setters getters
	public List<StokTanimlarSarfYeri> getAllStokTanimlarSarfYeriList() {
		return allStokTanimlarSarfYeriList;
	}

	public void setAllStokTanimlarSarfYeriList(
			List<StokTanimlarSarfYeri> allStokTanimlarSarfYeriList) {
		this.allStokTanimlarSarfYeriList = allStokTanimlarSarfYeriList;
	}

	public StokTanimlarSarfYeri getStokTanimlarSarfYeriForm() {
		return stokTanimlarSarfYeriForm;
	}

	public void setStokTanimlarSarfYeriForm(
			StokTanimlarSarfYeri stokTanimlarSarfYeriForm) {
		this.stokTanimlarSarfYeriForm = stokTanimlarSarfYeriForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
