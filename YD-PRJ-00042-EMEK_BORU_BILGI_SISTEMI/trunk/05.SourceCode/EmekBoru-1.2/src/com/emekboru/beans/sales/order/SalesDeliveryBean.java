package com.emekboru.beans.sales.order;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.sales.order.SalesDelivery;
import com.emekboru.jpaman.sales.order.SalesDeliveryManager;

@ManagedBean(name = "salesDeliveryBean")
@ViewScoped
public class SalesDeliveryBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7889428677836102384L;
	private List<SalesDelivery> deliveryList;

	public SalesDeliveryBean() {

		SalesDeliveryManager manager = new SalesDeliveryManager();
		deliveryList = manager.findAll(SalesDelivery.class);
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public List<SalesDelivery> getDeliveryList() {
		return deliveryList;
	}

	public void setDeliveryList(List<SalesDelivery> deliveryList) {
		this.deliveryList = deliveryList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}