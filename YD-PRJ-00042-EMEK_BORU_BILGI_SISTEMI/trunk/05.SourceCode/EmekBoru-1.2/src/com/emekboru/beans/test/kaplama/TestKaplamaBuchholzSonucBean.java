package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaBuchholzSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaBuchholzSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaplamaBuchholzSonucBean")
@ViewScoped
public class TestKaplamaBuchholzSonucBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private TestKaplamaBuchholzSonuc testKaplamaBuchholzSonucForm = new TestKaplamaBuchholzSonuc();
	private List<TestKaplamaBuchholzSonuc> allTestKaplamaBuchholzSonucList = new ArrayList<TestKaplamaBuchholzSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	float ortalama = 0F;

	public void addOrUpdateTestKaplamaBuchholzSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaBuchholzSonucManager tkbsManager = new TestKaplamaBuchholzSonucManager();

		if (testKaplamaBuchholzSonucForm.getId() == null) {

			testKaplamaBuchholzSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaBuchholzSonucForm.setEkleyenKullanici(userBean.getUser()
					.getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaBuchholzSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaBuchholzSonucForm.setBagliTestId(bagliTest);
			testKaplamaBuchholzSonucForm.setBagliGlobalId(bagliTest);

			ortalamaBul();

			tkbsManager.enterNew(testKaplamaBuchholzSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaBuchholzSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaBuchholzSonucForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			ortalamaBul();
			tkbsManager.updateEntity(testKaplamaBuchholzSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	private void ortalamaBul() {

		ortalama = (testKaplamaBuchholzSonucForm.getOlculenTestDegeri1()
				.floatValue()
				+ testKaplamaBuchholzSonucForm.getOlculenTestDegeri2()
						.floatValue()
				+ testKaplamaBuchholzSonucForm.getOlculenTestDegeri3()
						.floatValue()
				+ testKaplamaBuchholzSonucForm.getOlculenTestDegeri4()
						.floatValue() + testKaplamaBuchholzSonucForm
				.getOlculenTestDegeri5().floatValue()) / 5;
		testKaplamaBuchholzSonucForm.setOrtalama(BigDecimal.valueOf(ortalama));
	}

	public void addTestKaplamaBuchholzSonuc() {

		testKaplamaBuchholzSonucForm = new TestKaplamaBuchholzSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaBuchholzSonucListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaBuchholzSonucList = TestKaplamaBuchholzSonucManager
				.getAllTestKaplamaBuchholzSonuc(globalId, pipeId);
		testKaplamaBuchholzSonucForm = new TestKaplamaBuchholzSonuc();

	}

	public void deleteTestKaplamaBuchholzSonuc() {

		bagliTestId = testKaplamaBuchholzSonucForm.getBagliGlobalId().getId();

		if (testKaplamaBuchholzSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaBuchholzSonucManager tkbsManager = new TestKaplamaBuchholzSonucManager();
		tkbsManager.delete(testKaplamaBuchholzSonucForm);
		testKaplamaBuchholzSonucForm = new TestKaplamaBuchholzSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters
	public TestKaplamaBuchholzSonuc getTestKaplamaBuchholzSonucForm() {
		return testKaplamaBuchholzSonucForm;
	}

	public void setTestKaplamaBuchholzSonucForm(
			TestKaplamaBuchholzSonuc testKaplamaBuchholzSonucForm) {
		this.testKaplamaBuchholzSonucForm = testKaplamaBuchholzSonucForm;
	}

	public List<TestKaplamaBuchholzSonuc> getAllTestKaplamaBuchholzSonucList() {
		return allTestKaplamaBuchholzSonucList;
	}

	public void setAllTestKaplamaBuchholzSonucList(
			List<TestKaplamaBuchholzSonuc> allTestKaplamaBuchholzSonucList) {
		this.allTestKaplamaBuchholzSonucList = allTestKaplamaBuchholzSonucList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
