package com.emekboru.beans.sales.events;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.emekboru.beans.sales.SalesMenuBean;
import com.emekboru.jpa.Employee;
import com.emekboru.jpa.sales.customer.SalesContactPeople;
import com.emekboru.jpa.sales.events.SalesEvent;
import com.emekboru.jpa.sales.events.SalesJoinEventContactPeople;
import com.emekboru.jpa.sales.events.SalesJoinEventEmployee;
import com.emekboru.jpaman.EmployeeManager;
import com.emekboru.jpaman.sales.customer.SalesContactPeopleManager;
import com.emekboru.jpaman.sales.events.SalesEventManager;
import com.emekboru.jpaman.sales.events.SalesJoinEventContactPeopleManager;
import com.emekboru.jpaman.sales.events.SalesJoinEventEmployeeManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "salesEventBean")
@ViewScoped
public class SalesEventBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3193198052063982974L;

	@ManagedProperty(value = "#{salesMenuBean}")
	private SalesMenuBean salesMenuBean;

	private SalesEvent newEvent;
	private SalesEvent selectedEvent;
	private SalesEvent reminderEvent;

	private List<SalesEvent> eventList;
	private List<SalesEvent> filteredList;
	private List<SalesEvent> reminderList;
	private List<SalesContactPeople> newContactPeopleList;
	private List<SalesContactPeople> selectedContactPeopleList;

	public List<Employee> employeeList;
	private Employee[] selectedEmployees;
	public Integer departmentId;

	public SalesEventBean() {
		newEvent = new SalesEvent();
		selectedEvent = new SalesEvent();
		employeeList = new ArrayList<Employee>();
		newContactPeopleList = new ArrayList<SalesContactPeople>();
		selectedContactPeopleList = new ArrayList<SalesContactPeople>();
	}

	@PostConstruct
	public void load() {
		System.out.println("SalesEventBean.load()");

		try {
			SalesEventManager manager = new SalesEventManager();
			eventList = manager.findAll(SalesEvent.class);

			// SalesContactPeopleManager peopleManager = new
			// SalesContactPeopleManager();
			// selectedContactPeopleList = peopleManager
			// .findAll(SalesContactPeople.class);
			// newContactPeopleList = peopleManager
			// .findAll(SalesContactPeople.class);

			/* JOIN FUNCTIONS */
			// for (SalesEvent event : eventList) {
			// this.loadJoinEventEmployee(event);
			// this.loadJoinEventContactPeople(event);
			// }
			/* end of JOIN FUNCTIONS */
			if (eventList.size() > 0) {
				selectedEvent = eventList.get(0);
			} else {
				selectedEvent = new SalesEvent();
			}
		} catch (Exception e) {
			System.out.println("SalesEventBean:" + e.toString());
		}
		System.out.println("SalesEventBean.load() - BİTTİ");
	}

	public void add() {
		System.out.println("SalesEventsBean.add()");

		try {

			for (int i = 0; i < selectedEmployees.length; i++) {
				newEvent.getParticipantList().add(selectedEmployees[i]);
			}

			SalesEventManager manager = new SalesEventManager();
			manager.enterNew(newEvent);
			/* JOIN FUNCTIONS */
			this.addJoinEventEmployee(newEvent);
			this.addJoinEventContactPeople(newEvent);
			/* end of JOIN FUNCTIONS */
			eventList.add(newEvent);
			newEvent = new SalesEvent();
			this.load();
			FacesContextUtils.addInfoMessage("SubmitMessage");
		} catch (Exception e) {
			System.out.println("SalesEventBean:" + e.toString());
		}
	}

	public void update() {
		System.out.println("SalesEventsBean.update()");

		try {
			SalesEventManager manager = new SalesEventManager();

			/* JOIN FUNCTIONS */
			this.deleteJoinEventEmployee(selectedEvent);
			this.addJoinEventEmployee(selectedEvent);
			this.deleteJoinEventContactPeople(selectedEvent);
			this.addJoinEventContactPeople(selectedEvent);
			/* end of JOIN FUNCTIONS */

			manager.updateEntity(selectedEvent);
			FacesContextUtils.addInfoMessage("UpdateItemMessage");
		} catch (Exception e) {
			System.out.println("SalesEventBean:" + e.toString());
		}
	}

	public void delete() {
		System.out.println("SalesEventsBean.delete()");

		try {
			/* JOIN FUNCTIONS */
			this.deleteJoinEventEmployee(selectedEvent);
			this.deleteJoinEventContactPeople(selectedEvent);
			/* end of JOIN FUNCTIONS */

			SalesEventManager manager = new SalesEventManager();
			manager.delete(selectedEvent);
			eventList.remove(selectedEvent);
			selectedEvent = eventList.get(0);

			FacesContextUtils.addWarnMessage("DeletedMessage");
		} catch (Exception e) {
			System.out.println("SalesEventBean:" + e.toString());
		}
	}

	/* JOIN FUNCTIONS */
	private void addJoinEventEmployee(SalesEvent e) {
		SalesJoinEventEmployeeManager joinManager = new SalesJoinEventEmployeeManager();
		SalesJoinEventEmployee newJoin;

		for (int i = 0; i < e.getParticipantList().size(); i++) {
			newJoin = new SalesJoinEventEmployee();
			newJoin.setEmployee(e.getParticipantList().get(i));
			newJoin.setEvent(e);
			joinManager.enterNew(newJoin);
		}
	}

	private void loadJoinEventEmployee(SalesEvent event) {
		SalesJoinEventEmployeeManager joinManager = new SalesJoinEventEmployeeManager();
		List<SalesJoinEventEmployee> joinList = joinManager
				.findAll(SalesJoinEventEmployee.class);

		event.setParticipantList(new ArrayList<Employee>());

		for (int i = 0; i < joinList.size(); i++) {
			if (joinList.get(i).getEvent().equals(event)) {
				event.getParticipantList().add(joinList.get(i).getEmployee());
			}
		}
	}

	private void deleteJoinEventEmployee(SalesEvent event) {
		SalesJoinEventEmployeeManager joinManager = new SalesJoinEventEmployeeManager();
		List<SalesJoinEventEmployee> joinList = joinManager
				.findAll(SalesJoinEventEmployee.class);

		for (int i = 0; i < joinList.size(); i++) {
			if (joinList.get(i).getEvent().equals(event)) {
				joinManager.delete(joinList.get(i));
			}
		}
	}

	private void addJoinEventContactPeople(SalesEvent e) {
		SalesJoinEventContactPeopleManager joinManager = new SalesJoinEventContactPeopleManager();
		SalesJoinEventContactPeople newJoin;

		for (int i = 0; i < e.getContactPeopleList().size(); i++) {
			newJoin = new SalesJoinEventContactPeople();
			newJoin.setContactPeople(e.getContactPeopleList().get(i));
			newJoin.setEvent(e);
			joinManager.enterNew(newJoin);
		}
	}

	private void loadJoinEventContactPeople(SalesEvent event) {
		SalesJoinEventContactPeopleManager joinManager = new SalesJoinEventContactPeopleManager();
		List<SalesJoinEventContactPeople> joinList = joinManager
				.findAll(SalesJoinEventContactPeople.class);

		event.setContactPeopleList(new ArrayList<SalesContactPeople>());

		for (int i = 0; i < joinList.size(); i++) {
			if (joinList.get(i).getEvent().equals(event)) {
				event.getContactPeopleList().add(
						joinList.get(i).getContactPeople());
			}
		}
	}

	private void deleteJoinEventContactPeople(SalesEvent event) {
		SalesJoinEventContactPeopleManager joinManager = new SalesJoinEventContactPeopleManager();
		List<SalesJoinEventContactPeople> joinList = joinManager
				.findAll(SalesJoinEventContactPeople.class);

		for (int i = 0; i < joinList.size(); i++) {
			if (joinList.get(i).getEvent().equals(event)) {
				joinManager.delete(joinList.get(i));
			}
		}
	}

	/* end of JOIN FUNCTIONS */

	public void loadContactListForSelectedEvent() {
		System.out.println("SalesEventsBean.loadContactListForSelectedEvent()");

		try {
			SalesContactPeopleManager peopleManager = new SalesContactPeopleManager();
			selectedContactPeopleList = peopleManager.findByField(
					SalesContactPeople.class, "salesCustomer",
					selectedEvent.getTheCustomer());
		} catch (Exception e) {
			System.out.println("SalesEventBean:" + e.toString());
		}
	}

	public void loadContactListForNewEvent() {
		System.out.println("SalesEventsBean.loadContactListForNewEvent()");

		try {
			SalesContactPeopleManager peopleManager = new SalesContactPeopleManager();
			newContactPeopleList = peopleManager.findByField(
					SalesContactPeople.class, "salesCustomer",
					newEvent.getTheCustomer());
		} catch (Exception e) {
			System.out.println("SalesEventBean:" + e.toString());
		}
	}

	public void selectNothing() {
		reminderEvent = null;
	}

	public void loadEmployeeListForNewEvent() {
		System.out.println("SalesEventsBean.loadEmployeeListForNewEvent()");

		try {
			EmployeeManager employeeManager = new EmployeeManager();
			employeeList = employeeManager.findByField(Employee.class,
					"department.departmentId", departmentId);
		} catch (Exception e) {
			System.out.println("SalesEventsBean:" + e.toString());
		}
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public SalesEvent getNewEvent() {
		return newEvent;
	}

	public void setNewEvent(SalesEvent newEvent) {
		this.newEvent = newEvent;
	}

	public SalesEvent getSelectedEvent() {
		return selectedEvent;
	}

	public void setSelectedEvent(SalesEvent selectedEvent) {
		try {
			if (selectedEvent == null)
				selectedEvent = new SalesEvent();
		} catch (Exception ex) {
			selectedEvent = new SalesEvent();
		}
		this.selectedEvent = selectedEvent;
	}

	public SalesEvent getReminderEvent() {
		return reminderEvent;
	}

	public void setReminderEvent(SalesEvent reminderEvent) {
		this.reminderEvent = reminderEvent;
	}

	public List<SalesEvent> getEventList() {
		return eventList;
	}

	public void setEventList(List<SalesEvent> eventList) {
		this.eventList = eventList;
	}

	public List<SalesEvent> getFilteredList() {
		try {
			selectedEvent = filteredList.get(0);
			salesMenuBean.showNewType1Menu();
		} catch (Exception ex) {
			System.out.println("getFilteredList: " + ex);
		}
		return filteredList;
	}

	public void setFilteredList(List<SalesEvent> filteredList) {
		this.filteredList = filteredList;
	}

	public SalesMenuBean getSalesMenuBean() {
		return salesMenuBean;
	}

	public void setSalesMenuBean(SalesMenuBean salesMenuBean) {
		this.salesMenuBean = salesMenuBean;
	}

	@SuppressWarnings("deprecation")
	public List<SalesEvent> getReminderList() {
		try {
			Calendar currentDate = Calendar.getInstance();
			currentDate.add(Calendar.DAY_OF_MONTH, -3);
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
			String dateNow = formatter.format(currentDate.getTime());
			Date startDate = new Date(dateNow);

			reminderList = new ArrayList<SalesEvent>();

			for (int i = 0; i < eventList.size(); i++) {
				if (eventList.get(i).getEventDate().after(startDate)) {
					reminderList.add(eventList.get(i));
				}
			}

		} catch (Exception e) {
			System.out.println("SalesEventBean:" + e.toString());
		}

		return reminderList;
	}

	public void setReminderList(List<SalesEvent> reminderList) {
		this.reminderList = reminderList;
	}

	public List<SalesContactPeople> getNewContactPeopleList() {
		return newContactPeopleList;
	}

	public void setNewContactPeopleList(
			List<SalesContactPeople> newContactPeopleList) {
		this.newContactPeopleList = newContactPeopleList;
	}

	public List<SalesContactPeople> getSelectedContactPeopleList() {
		return selectedContactPeopleList;
	}

	public void setSelectedContactPeopleList(
			List<SalesContactPeople> selectedContactPeopleList) {
		this.selectedContactPeopleList = selectedContactPeopleList;
	}

	/**
	 * @return the departmentId
	 */
	public Integer getDepartmentId() {
		return departmentId;
	}

	/**
	 * @param departmentId
	 *            the departmentId to set
	 */
	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the employeeList
	 */
	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	/**
	 * @param employeeList
	 *            the employeeList to set
	 */
	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}

	/**
	 * @return the selectedEmployees
	 */
	public Employee[] getSelectedEmployees() {
		return selectedEmployees;
	}

	/**
	 * @param selectedEmployees
	 *            the selectedEmployees to set
	 */
	public void setSelectedEmployees(Employee[] selectedEmployees) {
		this.selectedEmployees = selectedEmployees;
	}

}
