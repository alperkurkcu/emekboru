/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaOnlineHolidaySonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaOnlineHolidaySonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testKaplamaOnlineHolidaySonucBean")
@ViewScoped
public class TestKaplamaOnlineHolidaySonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private TestKaplamaOnlineHolidaySonuc testKaplamaOnlineHolidaySonucForm = new TestKaplamaOnlineHolidaySonuc();
	private List<TestKaplamaOnlineHolidaySonuc> allKaplamaOnlineHolidaySonucList = new ArrayList<TestKaplamaOnlineHolidaySonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateKaplamaOnlineHolidayTestSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaOnlineHolidaySonucManager tkbsManager = new TestKaplamaOnlineHolidaySonucManager();

		if (testKaplamaOnlineHolidaySonucForm.getId() == null) {

			testKaplamaOnlineHolidaySonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaOnlineHolidaySonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaOnlineHolidaySonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaOnlineHolidaySonucForm.setBagliTestId(bagliTest);
			testKaplamaOnlineHolidaySonucForm.setBagliGlobalId(bagliTest);

			tkbsManager.enterNew(testKaplamaOnlineHolidaySonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaOnlineHolidaySonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaOnlineHolidaySonucForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			tkbsManager.updateEntity(testKaplamaOnlineHolidaySonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addKaplamaOnlineHolidayTest() {

		testKaplamaOnlineHolidaySonucForm = new TestKaplamaOnlineHolidaySonuc();
		updateButtonRender = false;
	}

	public void kaplamaOnlineHolidayListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allKaplamaOnlineHolidaySonucList = TestKaplamaOnlineHolidaySonucManager
				.getAllTestKaplamaOnlineHolidaySonucTest(globalId, pipeId);
		testKaplamaOnlineHolidaySonucForm = new TestKaplamaOnlineHolidaySonuc();
	}

	public void deleteTestKaplamaOnlineHolidaySonuc() {
		bagliTestId = testKaplamaOnlineHolidaySonucForm.getBagliTestId()
				.getId();
		if (testKaplamaOnlineHolidaySonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaOnlineHolidaySonucManager tkbsManager = new TestKaplamaOnlineHolidaySonucManager();
		allKaplamaOnlineHolidaySonucList
				.remove(testKaplamaOnlineHolidaySonucForm);
		tkbsManager.delete(testKaplamaOnlineHolidaySonucForm);
		testKaplamaOnlineHolidaySonucForm = new TestKaplamaOnlineHolidaySonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setters getters

	/**
	 * @return the testKaplamaOnlineHolidaySonucForm
	 */
	public TestKaplamaOnlineHolidaySonuc getTestKaplamaOnlineHolidaySonucForm() {
		return testKaplamaOnlineHolidaySonucForm;
	}

	/**
	 * @param testKaplamaOnlineHolidaySonucForm
	 *            the testKaplamaOnlineHolidaySonucForm to set
	 */
	public void setTestKaplamaOnlineHolidaySonucForm(
			TestKaplamaOnlineHolidaySonuc testKaplamaOnlineHolidaySonucForm) {
		this.testKaplamaOnlineHolidaySonucForm = testKaplamaOnlineHolidaySonucForm;
	}

	/**
	 * @return the allKaplamaOnlineHolidaySonucList
	 */
	public List<TestKaplamaOnlineHolidaySonuc> getAllKaplamaOnlineHolidaySonucList() {
		return allKaplamaOnlineHolidaySonucList;
	}

	/**
	 * @param allKaplamaOnlineHolidaySonucList
	 *            the allKaplamaOnlineHolidaySonucList to set
	 */
	public void setAllKaplamaOnlineHolidaySonucList(
			List<TestKaplamaOnlineHolidaySonuc> allKaplamaOnlineHolidaySonucList) {
		this.allKaplamaOnlineHolidaySonucList = allKaplamaOnlineHolidaySonucList;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
