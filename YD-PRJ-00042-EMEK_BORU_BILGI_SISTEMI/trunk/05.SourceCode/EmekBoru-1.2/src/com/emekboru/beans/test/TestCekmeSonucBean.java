package com.emekboru.beans.test;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.sales.order.SalesOrderBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.DestructiveTestsSpec;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.TestCekmeSonuc;
import com.emekboru.jpaman.DestructiveTestsSpecManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.TestCekmeSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testCekmeSonucBean")
@ViewScoped
public class TestCekmeSonucBean {

	private List<TestCekmeSonuc> allCekmeSonucList = new ArrayList<TestCekmeSonuc>();
	private TestCekmeSonuc testCekmeSonucForm = new TestCekmeSonuc();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;
	private boolean updateButtonRender;

	Integer prmGlobalId = null;
	Integer prmTestId = null;
	Integer prmPipeId = null;

	private Pipe pipe = null;
	private DestructiveTestsSpec bagliTest = null;
	private String sampleNo = null;

	public void sampleNoOlustur() {
		Integer count = 0;
		TestCekmeSonucManager manager = new TestCekmeSonucManager();
		count = manager.getAllTestCekmeSonuc(prmGlobalId, prmPipeId).size() + 1;
		sampleNo = bagliTest.getCode() + pipe.getPipeIndex() + "-" + count;
	}

	// methods
	public void addCekmeTestSonuc(ActionEvent e) {
		UICommand cmd = (UICommand) e.getComponent();
		prmGlobalId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");
		prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestCekmeSonucManager tcsManager = new TestCekmeSonucManager();

		if (testCekmeSonucForm.getId() == null) {

			testCekmeSonucForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			testCekmeSonucForm.setEkleyenKullanici(userBean.getUser().getId());

			pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testCekmeSonucForm.setPipe(pipe);

			bagliTest = new DestructiveTestsSpecManager().loadObject(
					DestructiveTestsSpec.class, prmTestId);
			testCekmeSonucForm.setBagliTestId(bagliTest);
			testCekmeSonucForm.setBagliGlobalId(bagliTest);

			sampleNoOlustur();
			testCekmeSonucForm.setSampleNo(sampleNo);

			tcsManager.enterNew(testCekmeSonucForm);

			// testlerin hangi borudan yapılacagı kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 2, true);
			DestructiveTestsSpecManager desMan = new DestructiveTestsSpecManager();
			desMan.testSonucKontrol(prmTestId, true);

			fillTestList(prmGlobalId, prmPipeId);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");

		} else {
			testCekmeSonucForm.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			testCekmeSonucForm.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			tcsManager.updateEntity(testCekmeSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");

		}
		TestCekmeSonucManager manager = new TestCekmeSonucManager();
		allCekmeSonucList = manager
				.getAllTestCekmeSonuc(prmGlobalId, prmPipeId);
	}

	public void addCekmeTest() {
		testCekmeSonucForm = new TestCekmeSonuc();
	}

	public void cekmeListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		TestCekmeSonucManager manager = new TestCekmeSonucManager();
		allCekmeSonucList = manager.getAllTestCekmeSonuc(globalId, pipeId);

	}

	public void deleteTestCekmeSonuc() {

		if (testCekmeSonucForm == null || testCekmeSonucForm.getId() <= 0) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		TestCekmeSonucManager tcsManager = new TestCekmeSonucManager();
		tcsManager.delete(testCekmeSonucForm);

		// destructive sonuc var mı false
		DestructiveTestsSpecManager desMan = new DestructiveTestsSpecManager();
		desMan.testSonucKontrol(
				testCekmeSonucForm.getBagliTestId().getTestId(), false);

		testCekmeSonucForm = new TestCekmeSonuc();
		FacesContextUtils.addWarnMessage("TestDeleteMessage");

	}

	// document upload

	@EJB
	private ConfigBean config;

	private String path;
	private String privatePath;

	private File theFile = null;
	OutputStream output = null;
	InputStream input = null;

	@SuppressWarnings("unused")
	private File[] fileArray;

	private StreamedContent downloadedFile;
	private String downloadedFileName;

	SalesOrderBean sob = new SalesOrderBean();

	public TestCekmeSonucBean() {

		privatePath = File.separatorChar + "testDocs" + File.separatorChar
				+ "uploadedDocuments" + File.separatorChar + "cekmeSonuc"
				+ File.separatorChar;
	}

	@PostConstruct
	public void load() {
		System.out.println("TestCekmeSonucBean.load()");

		try {
			// this.setPath(config.getConfig().getFolder().getAbsolutePath()
			// + privatePath);

			if (System.getProperty("os.name").startsWith("Windows")) {

				this.setPath("C:/emekfiles" + privatePath);
			} else {

				this.setPath("/home/emekboru/Desktop/emekfiles" + privatePath);
			}

			System.out.println("Path for Upload File (CekmeSonuc):			" + path);
		} catch (Exception ex) {
			System.out.println("Path for Upload File (CekmeSonuc):			"
					+ FacesContext.getCurrentInstance().getExternalContext()
							.getRealPath(privatePath));

			System.out.print(path);
			System.out.println("CAUSE OF " + ex.toString());
		}

		theFile = new File(path);
		if (!theFile.isDirectory()) {
			theFile.mkdirs();
		}

		fileArray = theFile.listFiles();

	}

	public void addUploadedFile(FileUploadEvent event)
			throws AbortProcessingException, IOException {
		System.out.println("TestCekmeSonucBean.addUploadedFile()");

		try {
			String name = sob.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				testCekmeSonucForm.setDocuments(testCekmeSonucForm
						.getDocuments() + "//" + name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				TestCekmeSonucManager manager = new TestCekmeSonucManager();
				manager.updateEntity(testCekmeSonucForm);

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				FacesContextUtils.addWarnMessage("salesFileUploadedMessage");
			} else {
				System.out.println("File already exists.");

				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("TestCekmeSonucBean: " + e.toString());
		}
	}

	@SuppressWarnings("resource")
	public StreamedContent getDownloadedFile() {
		System.out.println("TestCekmeSonucBean.getDownloadedFile()");

		try {
			if (downloadedFileName != null) {
				RandomAccessFile accessFile = new RandomAccessFile(path
						+ downloadedFileName, "r");
				byte[] downloadByte = new byte[(int) accessFile.length()];
				accessFile.read(downloadByte);
				InputStream stream = new ByteArrayInputStream(downloadByte);

				String suffix = FilenameUtils.getExtension(downloadedFileName);
				String contentType = "text/plain";
				if (suffix.contentEquals("jpg") || suffix.contentEquals("png")
						|| suffix.contentEquals("gif")) {
					contentType = "image/" + contentType;
				} else if (suffix.contentEquals("doc")
						|| suffix.contentEquals("docx")) {
					contentType = "application/msword";
				} else if (suffix.contentEquals("xls")
						|| suffix.contentEquals("xlsx")) {
					contentType = "application/vnd.ms-excel";
				} else if (suffix.contentEquals("pdf")) {
					contentType = "application/pdf";
				}
				downloadedFile = new DefaultStreamedContent(stream,
						"contentType", downloadedFileName);

				stream.close();
			}
		} catch (Exception e) {
			System.out.println("TestCekmeSonucBean.getDownloadedFile():"
					+ e.toString());
		}
		return downloadedFile;
	}

	public String getDownloadedFileName() {
		return downloadedFileName;
	}

	public void setDownloadedFileName(String downloadedFileName) {
		try {
			this.downloadedFileName = downloadedFileName;
		} catch (Exception e) {
			System.out.println("TestCekmeSonucBean.setDownloadedFileName():"
					+ e.toString());
		}
	}

	public void setPath(String path) {
		this.path = path;
	}

	// document upload

	// Getters & Setters

	public List<TestCekmeSonuc> getAllCekmeSonucList() {
		return allCekmeSonucList;
	}

	public void setAllCekmeSonucList(List<TestCekmeSonuc> allCekmeSonucList) {
		this.allCekmeSonucList = allCekmeSonucList;
	}

	public TestCekmeSonuc getTestCekmeSonucForm() {
		return testCekmeSonucForm;
	}

	public void setTestCekmeSonucForm(TestCekmeSonuc testCekmeSonucForm) {
		this.testCekmeSonucForm = testCekmeSonucForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
