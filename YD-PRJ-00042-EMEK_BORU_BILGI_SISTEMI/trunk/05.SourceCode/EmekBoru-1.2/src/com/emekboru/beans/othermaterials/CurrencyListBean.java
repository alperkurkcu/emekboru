package com.emekboru.beans.othermaterials;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.Currency;
import com.emekboru.jpaman.CurrencyManager;



@ManagedBean (name = "currencyListBean")
@ViewScoped
public class CurrencyListBean implements Serializable {

	private static final long serialVersionUID = -1136804927316503541L;
	
	private List<Currency> currencyList;
	
	public CurrencyListBean(){
		CurrencyManager currencyManager = new CurrencyManager();
		currencyList = currencyManager.findAll(Currency.class);
	}

	public List<Currency> getCurrencyList() {
		return currencyList;
	}

	public void setCurrencyList(List<Currency> currencyList) {
		this.currencyList = currencyList;
	}

}
