/**
 * 
 */
package com.emekboru.beans.personel;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Rıdvan
 * 
 */

@ManagedBean(name = "personelOrtakBean")
@ViewScoped
public class PersonelOrtakBean {

	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	public void goToPersonelOrtakGorev() {

		FacesContextUtils
				.redirect(config.getPageUrl().PERSONEL_ORTAKGOREV_PAGE);
	}

	public void goToPersonelKimlik() {

		FacesContextUtils.redirect(config.getPageUrl().PERSONEL_KIMLIK_PAGE);
	}

	/**
	 * @return the config
	 */
	public ConfigBean getConfig() {
		return config;
	}

	/**
	 * @param config
	 *            the config to set
	 */
	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

}
