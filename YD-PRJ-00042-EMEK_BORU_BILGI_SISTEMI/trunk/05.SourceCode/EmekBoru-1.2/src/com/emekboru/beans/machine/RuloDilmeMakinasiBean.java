/**
 * 
 */
package com.emekboru.beans.machine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.machines.RuloDilmeMakinasi;
import com.emekboru.jpa.rulo.Rulo;
import com.emekboru.jpa.rulo.RuloOzellikBoyutsal;
import com.emekboru.jpaman.rulo.RuloManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "ruloDilmeMakinasiBean")
@ViewScoped
public class RuloDilmeMakinasiBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private RuloDilmeMakinasi ruloDilmeMakinasiForm = new RuloDilmeMakinasi();
	private List<Rulo> selectedRulos = new ArrayList<Rulo>();
	private Rulo selectedRulo = new Rulo();
	// private Rulo lastRulo = new Rulo();
	private List<Rulo> allRulos = new ArrayList<Rulo>();

	private RuloOzellikBoyutsal selectedRuloBoyutsal = new RuloOzellikBoyutsal();

	// dilme makinası acılıs sayfası
	public void goToRuloDilme() {

		FacesContextUtils.redirect(config.getPageUrl().RULO_DILME_MAIN_PAGE);
	}

	public void loadAllocatedRulos() {

		selectedRulos.clear();
		RuloManager ruloManager = new RuloManager();
		allRulos = ruloManager.findAll(Rulo.class);
	}

	public void sendRulosToMachine() {

		if (selectedRulo != null) {
			selectedRulos.add(selectedRulo);
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RULO BAŞARI İLE SEÇİLMİŞTİR!",
					null));
		} else {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"RULO SEÇİLMEMİŞ, LÜTFEN RULO SEÇİNİZ!", null));
		}
	}

	private void yeniRuloHazirla(Rulo newRulo) {

		newRulo.setRuloStokNo(selectedRulo.getRuloStokNo());
		newRulo.setRuloBobinNo(selectedRulo.getRuloBobinNo());
		newRulo.setRuloEtiketNo(selectedRulo.getRuloEtiketNo());
		newRulo.setRuloDokumNo(selectedRulo.getRuloDokumNo());
		newRulo.setRuloBarkodNo(selectedRulo.getRuloBarkodNo());
		newRulo.setRuloKalite(selectedRulo.getRuloKalite());
		newRulo.setRuloYer(selectedRulo.getRuloYer());
		newRulo.setRuloTarih(selectedRulo.getRuloTarih());
		newRulo.setRuloFaturaNo(selectedRulo.getRuloFaturaNo());
		newRulo.setRuloGenelDurum(selectedRulo.getRuloGenelDurum());
		newRulo.setRuloIrsaliyeNo(selectedRulo.getRuloIrsaliyeNo());
		newRulo.setRuloId(null);
		newRulo.setRuloYil(selectedRulo.getRuloYil());
		newRulo.setRuloDurum(selectedRulo.getRuloDurum());
		newRulo.setRuloNot(selectedRulo.getRuloNot());
		newRulo.setIslemYapanId(selectedRulo.getIslemYapanId());
		newRulo.setIslemYapanIsim(selectedRulo.getIslemYapanIsim());
		newRulo.setSalesItemId(selectedRulo.getSalesItemId());
		newRulo.setSalesCustomer(selectedRulo.getSalesCustomer());
		newRulo.setSalesCustomerId(selectedRulo.getSalesCustomerId());
	}

	private void yeniRuloBoyutsalHazirla(Rulo newRulo) {

		newRulo.getRuloOzellikBoyutsal().setRuloOzellikBoyutsalEtKalinligi(
				selectedRulo.getRuloOzellikBoyutsal()
						.getRuloOzellikBoyutsalEtKalinligi());
		newRulo.getRuloOzellikBoyutsal()
				.setRuloOzellikBoyutsalOlculenEtKalinligi(
						selectedRulo.getRuloOzellikBoyutsal()
								.getRuloOzellikBoyutsalOlculenEtKalinligi());
		newRulo.getRuloOzellikBoyutsal().setDurum(1);
		newRulo.getRuloOzellikBoyutsal().setIslemYapanId(
				selectedRulo.getRuloOzellikBoyutsal().getIslemYapanId());
		newRulo.getRuloOzellikBoyutsal().setIslemYapanIsim(
				selectedRulo.getRuloOzellikBoyutsal().getIslemYapanIsim());
	}

	private List<Rulo> miktaraGoreDilimle() {

		RuloManager ruloManager = new RuloManager();

		List<Rulo> newRulos = new ArrayList<Rulo>();
		List<Integer> yeniEbatlar = new ArrayList<Integer>();

		if (selectedRulo.getRuloOzellikBoyutsal()
				.getRuloOzellikBoyutsalKalanMiktar() == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL,
					"DİLME HATASI, SEÇİLEN RULODA 'KALAN MİKTAR' BOŞ OLAMAZ!",
					null));
			return null;
		}

		Float dilinecekEbat = selectedRulo.getRuloOzellikBoyutsal()
				.getRuloOzellikBoyutsalGenislik();
		float dilinecekMiktar = ruloDilmeMakinasiForm.getRuloDilinecekMiktar();
		Integer parcaSayisi = ruloDilmeMakinasiForm.getRuloDilmeSayisi();

		switch (parcaSayisi) {
		case 1: // 4 Parça
			yeniEbatlar.add(ruloDilmeMakinasiForm.getDilmeEbadiBir());
			newRulos.add(new Rulo());
		case 2: // 3 Parça
			yeniEbatlar.add(ruloDilmeMakinasiForm.getDilmeEbadiIki());
			newRulos.add(new Rulo());
		case 3: // 2 Parça
			yeniEbatlar.add(ruloDilmeMakinasiForm.getDilmeEbadiUc());
			newRulos.add(new Rulo());
		case 4:
			yeniEbatlar.add(ruloDilmeMakinasiForm.getDilmeEbadiDort());
			newRulos.add(new Rulo());
		default:
			break;
		}

		String kimlikId = selectedRulo.getRuloKimlikId();

		selectedRulos.clear();

		for (int i = 0; i < newRulos.size(); i++) {

			yeniRuloHazirla(newRulos.get(i));
			yeniRuloBoyutsalHazirla(newRulos.get(i));

			newRulos.get(i)
					.getRuloOzellikBoyutsal()
					.setRuloOzellikBoyutsalGenislik(
							yeniEbatlar.get(i).floatValue());

			newRulos.get(i)
					.getRuloOzellikBoyutsal()
					.setRuloOzellikBoyutsalOlculenGenislik(
							yeniEbatlar.get(i).floatValue());

			newRulos.get(i)
					.getRuloOzellikBoyutsal()
					.setRuloOzellikBoyutsalMiktar(
							(float) Math
									.round(dilinecekMiktar
											* (yeniEbatlar.get(i).floatValue() / dilinecekEbat)));
			newRulos.get(i)
					.getRuloOzellikBoyutsal()
					.setRuloOzellikBoyutsalKalanMiktar(
							(float) Math
									.round(dilinecekMiktar
											* (yeniEbatlar.get(i).floatValue() / dilinecekEbat)));

			newRulos.get(i).setRuloKimlikId(kimlikId + "-" + (i + 1));

			ruloManager.enterNew(newRulos.get(i));

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "DİLME TAMAMLANDI, " + kimlikId
							+ "-" + (i + 1) + "!", null));

			selectedRulos.add(newRulos.get(i));
		}

		selectedRulo.getRuloOzellikBoyutsal().setRuloOzellikBoyutsalMiktar(
				(float) Math.round(selectedRulo.getRuloOzellikBoyutsal()
						.getRuloOzellikBoyutsalMiktar() - dilinecekMiktar));

		selectedRulo.getRuloOzellikBoyutsal()
				.setRuloOzellikBoyutsalKalanMiktar(
						(float) Math.round(selectedRulo
								.getRuloOzellikBoyutsal()
								.getRuloOzellikBoyutsalKalanMiktar()
								- dilinecekMiktar));

		selectedRulo.setRuloKimlikId(kimlikId + "-0");
		ruloManager.updateEntity(selectedRulo);
		selectedRulos.add(0, selectedRulo);

		return newRulos;
	}

	private List<Rulo> tumunuDilimle() {

		RuloManager ruloManager = new RuloManager();

		List<Rulo> newRulos = new ArrayList<Rulo>();
		List<Integer> yeniEbatlar = new ArrayList<Integer>();

		if (selectedRulo.getRuloOzellikBoyutsal()
				.getRuloOzellikBoyutsalKalanMiktar() == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL,
					"DİLME HATASI, SEÇİLEN RULODA 'KALAN MİKTAR' BOŞ OLAMAZ!",
					null));
			return null;
		}

		float dilinecekEbat = selectedRulo.getRuloOzellikBoyutsal()
				.getRuloOzellikBoyutsalGenislik();
		float dilinecekMiktar = selectedRulo.getRuloOzellikBoyutsal()
				.getRuloOzellikBoyutsalKalanMiktar();
		Integer parcaSayisi = ruloDilmeMakinasiForm.getRuloDilmeSayisi();

		switch (parcaSayisi) {
		case 1: // 4 Parça
			yeniEbatlar.add(ruloDilmeMakinasiForm.getDilmeEbadiBir());
			newRulos.add(new Rulo());
		case 2: // 3 Parça
			yeniEbatlar.add(ruloDilmeMakinasiForm.getDilmeEbadiIki());
			newRulos.add(new Rulo());
		case 3: // 2 Parça
			yeniEbatlar.add(ruloDilmeMakinasiForm.getDilmeEbadiUc());
			newRulos.add(new Rulo());
			yeniEbatlar.add(ruloDilmeMakinasiForm.getDilmeEbadiDort());
			newRulos.add(selectedRulo);
		default:
			break;
		}

		Collections.reverse(newRulos);

		String kimlikId = newRulos.get(0).getRuloKimlikId();

		selectedRulos.clear();

		for (int i = 0; i < newRulos.size(); i++) {

			if (i > 0) {
				yeniRuloHazirla(newRulos.get(i));
				yeniRuloBoyutsalHazirla(newRulos.get(i));
			}

			newRulos.get(i)
					.getRuloOzellikBoyutsal()
					.setRuloOzellikBoyutsalGenislik(
							yeniEbatlar.get(i).floatValue());

			newRulos.get(i)
					.getRuloOzellikBoyutsal()
					.setRuloOzellikBoyutsalOlculenGenislik(
							yeniEbatlar.get(i).floatValue());

			newRulos.get(i)
					.getRuloOzellikBoyutsal()
					.setRuloOzellikBoyutsalMiktar(
							(float) Math
									.round(dilinecekMiktar
											* (yeniEbatlar.get(i).floatValue() / dilinecekEbat)));
			newRulos.get(i)
					.getRuloOzellikBoyutsal()
					.setRuloOzellikBoyutsalKalanMiktar(
							(float) Math
									.round(dilinecekMiktar
											* (yeniEbatlar.get(i).floatValue() / dilinecekEbat)));

			newRulos.get(i).setRuloKimlikId(kimlikId + "-" + i);

			if (i > 0)
				ruloManager.enterNew(newRulos.get(i));
			else
				ruloManager.updateEntity(newRulos.get(i));

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "DİLME TAMAMLANDI, " + kimlikId
							+ "-" + i + "!", null));

			selectedRulos.add(newRulos.get(i));
		}

		return newRulos;
	}

	public void ruloDilimle() {

		if (ruloDilmeMakinasiForm.getRuloDilmeSayisi() == null) {
			FacesContextUtils
					.addPlainErrorMessage("DİLİMLEME SAYISI SEÇİLMEMİŞ, LÜTFEN SEÇİNİZ!");
			return;
		}

		int sum = 0;

		sum += (ruloDilmeMakinasiForm.getDilmeEbadiBir() == null) ? 0
				: ruloDilmeMakinasiForm.getDilmeEbadiBir();
		sum += (ruloDilmeMakinasiForm.getDilmeEbadiIki() == null) ? 0
				: ruloDilmeMakinasiForm.getDilmeEbadiIki();
		sum += (ruloDilmeMakinasiForm.getDilmeEbadiUc() == null) ? 0
				: ruloDilmeMakinasiForm.getDilmeEbadiUc();
		sum += (ruloDilmeMakinasiForm.getDilmeEbadiDort() == null) ? 0
				: ruloDilmeMakinasiForm.getDilmeEbadiDort();

		if (sum != selectedRulo.getRuloOzellikBoyutsal()
				.getRuloOzellikBoyutsalGenislik()) {
			FacesContextUtils
					.addPlainErrorMessage("EBATLARIN TOPLAMI RULONUN EBATLARINA EŞİT OLMALIDIR!");
			return;
		}

		if (ruloDilmeMakinasiForm.getTumu() == true) {
			tumunuDilimle();
		} else {
			miktaraGoreDilimle();
		}

	}

	// setters getters

	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public RuloDilmeMakinasi getRuloDilmeMakinasiForm() {
		return ruloDilmeMakinasiForm;
	}

	public void setRuloDilmeMakinasiForm(RuloDilmeMakinasi ruloDilmeMakinasiForm) {
		this.ruloDilmeMakinasiForm = ruloDilmeMakinasiForm;
	}

	public List<Rulo> getSelectedRulos() {
		return selectedRulos;
	}

	public void setSelectedRulos(List<Rulo> selectedRulos) {
		this.selectedRulos = selectedRulos;
	}

	public List<Rulo> getAllRulos() {
		return allRulos;
	}

	public void setAllRulos(List<Rulo> allRulos) {
		this.allRulos = allRulos;
	}

	public Rulo getSelectedRulo() {
		return selectedRulo;
	}

	public void setSelectedRulo(Rulo selectedRulo) {
		this.selectedRulo = selectedRulo;
	}

	public RuloOzellikBoyutsal getSelectedRuloBoyutsal() {
		return selectedRuloBoyutsal;
	}

	public void setSelectedRuloBoyutsal(RuloOzellikBoyutsal selectedRuloBoyutsal) {
		this.selectedRuloBoyutsal = selectedRuloBoyutsal;
	}

}
