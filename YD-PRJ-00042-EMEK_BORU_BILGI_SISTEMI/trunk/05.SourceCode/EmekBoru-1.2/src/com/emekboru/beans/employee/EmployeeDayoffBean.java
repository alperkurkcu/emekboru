package com.emekboru.beans.employee;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Employee;
import com.emekboru.jpa.common.CommonDayoffType;
import com.emekboru.jpa.employee.EmployeeDayoff;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.EmployeeManager;
import com.emekboru.jpaman.employee.EmployeeDayoffManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.FileOperations;
import com.emekboru.utils.MailUtil;

import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;

@ManagedBean(name = "employeeDayoffBean")
@ViewScoped
public class EmployeeDayoffBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private List<EmployeeDayoff> employeeDayoffRequests = new ArrayList<EmployeeDayoff>();
	private List<EmployeeDayoff> subEmployeeDayoffRequests = new ArrayList<EmployeeDayoff>();
	private List<EmployeeDayoff> currentEmployeeDayoffRequests = new ArrayList<EmployeeDayoff>();
	private List<EmployeeDayoff> bmEmployeeDayoffRequests = new ArrayList<EmployeeDayoff>();
	private Employee currentEmployee;
	// private Employee talebiYapanKullanici;
	private Employee birUstAmir;
	private Employee ikiUstAmir;
	private Employee nullEmployee;

	private EmployeeDayoff newEmployeeDayoff = new EmployeeDayoff();

	private EmployeeDayoff selectedSubEmployeeDayoff = new EmployeeDayoff();
	private EmployeeDayoff selectedSelfDayoff = new EmployeeDayoff();
	private EmployeeDayoff HRSelectedEmployeeDayoff = new EmployeeDayoff();
	private EmployeeDayoff selectedBMEmployeeDayoff = new EmployeeDayoff();
	private EmployeeDayoff REDO = new EmployeeDayoff();

	private List<CommonDayoffType> allCommonDayoffTypes = new ArrayList<CommonDayoffType>();

	private boolean renderTab = false;

	public EmployeeDayoffBean() {
	}

	@PostConstruct
	public void load() {

		EmployeeDayoffManager employeeDayoffManager = new EmployeeDayoffManager();
		employeeDayoffRequests = employeeDayoffManager.findAll();
		currentEmployee = (Employee) userBean.getUser().getEmployee();

		currentEmployeeDayoffRequests = employeeDayoffManager
				.findAllByEmployee(currentEmployee);

		subEmployeeDayoffRequests = employeeDayoffManager
				.findAllBySupervisors(currentEmployee);

		bmEmployeeDayoffRequests = employeeDayoffManager
				.findAllByBM(currentEmployee);

		CommonQueries<CommonDayoffType> commonDayoffTypeManager = new CommonQueries<CommonDayoffType>();

		allCommonDayoffTypes = commonDayoffTypeManager
				.findAll(CommonDayoffType.class);

		if (currentEmployee == currentEmployee.getDepartment()
				.getDepartmentManagerEmployee()) {
			renderTab = true;
		}

		selectedSubEmployeeDayoff = null;
		selectedSelfDayoff = null;
		HRSelectedEmployeeDayoff = null;
		selectedBMEmployeeDayoff = null;
		EmployeeManager manager = new EmployeeManager();
		nullEmployee = manager.findByField(Employee.class, "employeeId", 1)
				.get(0);

	}

	public void makeDayoffRequest() {

		int ekleyenKullanici = this.userBean.getUser().getId();
		Timestamp eklemeZamani = new java.sql.Timestamp(
				new java.util.Date().getTime());

		newEmployeeDayoff.setActive(true);
		// newEmployeeDayoff.setEmployee(talebiYapanKullanici);
		newEmployeeDayoff.setSupervisorApproval(EmployeeDayoff.PENDING);
		newEmployeeDayoff.setSecondSupervisorApproval(EmployeeDayoff.PENDING);
		newEmployeeDayoff.setHrApproval(EmployeeDayoff.PENDING);
		newEmployeeDayoff.setBmApproval(EmployeeDayoff.PENDING);
		newEmployeeDayoff.setSupervisor(birUstAmir);
		newEmployeeDayoff.setSecondSupervisor(ikiUstAmir);
		newEmployeeDayoff.setRequestDate(eklemeZamani);
		newEmployeeDayoff.setEklemeZamani(eklemeZamani);
		newEmployeeDayoff.setEkleyenKullanici(ekleyenKullanici);

		if (newEmployeeDayoff.getDeputy() == null) {
			newEmployeeDayoff.setDeputy(nullEmployee);
		}

		if (newEmployeeDayoff.getAddress() == null) {
			newEmployeeDayoff.setAddress(newEmployeeDayoff.getEmployee()
					.getEmployeeAddresses().get(0).getAddress());
		}

		if (newEmployeeDayoff.getPhone() == null) {
			newEmployeeDayoff.setPhone(newEmployeeDayoff.getEmployee()
					.getEmployeePhoneNumbers().get(0).getPhoneNumber());
		}

		EmployeeDayoffManager employeeDayoffManager = new EmployeeDayoffManager();
		employeeDayoffManager.enterNew(newEmployeeDayoff);

		FacesContextUtils.addPlainWarnMessage("Kayıt başarıyla girildi!");

		newEmployeeDayoff = new EmployeeDayoff();

		this.load();
	}

	public void declineSubEmployeeDayoffRequest() {

		if (selectedSubEmployeeDayoff == null) {

			FacesContextUtils
					.addPlainWarnMessage("Lüften reddetmek istediğiniz izni seçiniz!");
			return;
		}

		if (selectedSubEmployeeDayoff.getSupervisor().getEmployeeId() == currentEmployee
				.getEmployeeId()) {
			if (selectedSubEmployeeDayoff.getSupervisorApproval() != EmployeeDayoff.PENDING) {

				FacesContextUtils
						.addPlainWarnMessage("Onayladığınız ya da reddettiğiniz bir izni değiştiremezsiniz!");
				return;
			}

			selectedSubEmployeeDayoff
					.setSupervisorApproval(EmployeeDayoff.DECLINED);

		} else if (selectedSubEmployeeDayoff.getSecondSupervisor()
				.getEmployeeId() == currentEmployee.getEmployeeId()) {

			if (selectedSubEmployeeDayoff.getSecondSupervisorApproval() != EmployeeDayoff.PENDING) {

				FacesContextUtils
						.addPlainWarnMessage("Onayladığınız ya da reddettiğiniz bir izni değiştiremezsiniz!");
				return;
			}

			selectedSubEmployeeDayoff
					.setSecondSupervisorApproval(EmployeeDayoff.DECLINED);
		}

		EmployeeDayoffManager employeeDayoffManager = new EmployeeDayoffManager();

		employeeDayoffManager.updateEntity(selectedSubEmployeeDayoff);

		FacesContextUtils.addPlainWarnMessage("İzni reddettiniz!");

		checkStatus(selectedSubEmployeeDayoff);

		this.load();

	}

	public void approveSubEmployeeDayoffRequest() {

		if (selectedSubEmployeeDayoff == null) {

			FacesContextUtils
					.addPlainWarnMessage("Lüften onaylamak istediğiniz izni seçiniz!");
			return;
		}

		if (selectedSubEmployeeDayoff.getSupervisor().getEmployeeId() == currentEmployee
				.getEmployeeId()) {
			if (selectedSubEmployeeDayoff.getSupervisorApproval() != EmployeeDayoff.PENDING) {

				FacesContextUtils
						.addPlainWarnMessage("Onayladığınız ya da reddettiğiniz bir izni değiştiremezsiniz!");
				return;
			}

			selectedSubEmployeeDayoff
					.setSupervisorApproval(EmployeeDayoff.APPROVED);

		} else if (selectedSubEmployeeDayoff.getSecondSupervisor()
				.getEmployeeId() == currentEmployee.getEmployeeId()) {

			if (selectedSubEmployeeDayoff.getSecondSupervisorApproval() != EmployeeDayoff.PENDING) {

				FacesContextUtils
						.addPlainWarnMessage("Onayladığınız ya da reddettiğiniz bir izni değiştiremezsiniz!");
				return;
			}

			selectedSubEmployeeDayoff
					.setSecondSupervisorApproval(EmployeeDayoff.APPROVED);
		}

		EmployeeDayoffManager employeeDayoffManager = new EmployeeDayoffManager();

		employeeDayoffManager.updateEntity(selectedSubEmployeeDayoff);

		FacesContextUtils.addPlainWarnMessage("İzni onayladınız!");

		checkStatus(selectedSubEmployeeDayoff);

		this.load();

	}

	public void declineEmployeeDayoffRequest() {

		if (HRSelectedEmployeeDayoff == null) {

			FacesContextUtils
					.addPlainWarnMessage("Lüften reddetmek istediğiniz izni seçiniz!");
			return;
		}

		if (HRSelectedEmployeeDayoff.getHrApproval() != EmployeeDayoff.PENDING) {

			FacesContextUtils
					.addPlainWarnMessage("Onayladığınız ya da reddettiğiniz bir izni değiştiremezsiniz!");
			return;
		}

		HRSelectedEmployeeDayoff.setHrApproval(EmployeeDayoff.DECLINED);
		EmployeeDayoffManager employeeDayoffManager = new EmployeeDayoffManager();

		employeeDayoffManager.updateEntity(HRSelectedEmployeeDayoff);

		FacesContextUtils.addPlainWarnMessage("İzni reddettiniz!");

		checkStatus(HRSelectedEmployeeDayoff);

		this.load();
	}

	public void approveEmployeeDayoffRequest() {

		if (HRSelectedEmployeeDayoff == null) {

			FacesContextUtils
					.addPlainWarnMessage("Lüften onaylamak istediğiniz izni seçiniz!");
			return;
		}

		if (HRSelectedEmployeeDayoff.getHrApproval() != EmployeeDayoff.PENDING) {

			FacesContextUtils
					.addPlainWarnMessage("Onayladığınız ya da reddettiğiniz bir izni değiştiremezsiniz!");
			return;
		}

		HRSelectedEmployeeDayoff.setHrApproval(EmployeeDayoff.APPROVED);
		EmployeeDayoffManager employeeDayoffManager = new EmployeeDayoffManager();

		employeeDayoffManager.updateEntity(HRSelectedEmployeeDayoff);

		FacesContextUtils.addPlainWarnMessage("İzni onayladınız!");

		checkStatus(HRSelectedEmployeeDayoff);

		this.load();
	}

	public void declineBMEmployeeDayoffRequest() {

		if (selectedBMEmployeeDayoff == null) {

			FacesContextUtils
					.addPlainWarnMessage("Lüften reddetmek istediğiniz izni seçiniz!");
			return;
		}

		if (selectedBMEmployeeDayoff.getBmApproval() != EmployeeDayoff.PENDING) {

			FacesContextUtils
					.addPlainWarnMessage("Onayladığınız ya da reddettiğiniz bir izni değiştiremezsiniz!");
			return;
		}

		selectedBMEmployeeDayoff.setBmApproval(EmployeeDayoff.DECLINED);
		EmployeeDayoffManager employeeDayoffManager = new EmployeeDayoffManager();

		employeeDayoffManager.updateEntity(selectedBMEmployeeDayoff);

		FacesContextUtils.addPlainWarnMessage("İzni reddettiniz!");

		checkStatus(selectedBMEmployeeDayoff);

		this.load();

	}

	public void approveBMEmployeeDayoffRequest() {

		if (selectedBMEmployeeDayoff == null) {

			FacesContextUtils
					.addPlainWarnMessage("Lüften onaylamak istediğiniz izni seçiniz!");
			return;
		}

		if (selectedBMEmployeeDayoff.getBmApproval() != EmployeeDayoff.PENDING) {

			FacesContextUtils
					.addPlainWarnMessage("Onayladığınız ya da reddettiğiniz bir izni değiştiremezsiniz!");
			return;
		}

		selectedBMEmployeeDayoff.setBmApproval(EmployeeDayoff.APPROVED);
		EmployeeDayoffManager employeeDayoffManager = new EmployeeDayoffManager();

		employeeDayoffManager.updateEntity(selectedBMEmployeeDayoff);

		FacesContextUtils.addPlainWarnMessage("İzni onayladınız!");

		checkStatus(selectedBMEmployeeDayoff);

		this.load();

	}

	public void exportToWord() {

		String realFilePath = getRealFilePath("/reportformats/ik/FR-392.docx");
		String realOutFilePath = getRealFilePath("/reportformats/ik/FR-392.docx"
				.replace(".", "OUT" + "."));

		if (selectedSubEmployeeDayoff != null) {
			REDO = selectedSubEmployeeDayoff;
		} else if (selectedSelfDayoff != null) {
			REDO = selectedSelfDayoff;
		} else if (HRSelectedEmployeeDayoff != null) {
			REDO = HRSelectedEmployeeDayoff;
		} else if (selectedBMEmployeeDayoff != null) {
			REDO = selectedBMEmployeeDayoff;
		}

		try {

			// 1) Load Docx file by filling Velocity template engine and cache
			// it to the registry
			InputStream in = new FileInputStream(new File(realFilePath));
			IXDocReport report = XDocReportRegistry.getRegistry().loadReport(
					in, TemplateEngineKind.Velocity);

			// 2) Create context Java model

			IContext context = report.createContext();

			context.put("dayoff", REDO);

			if (REDO.getCommonDayoffType().getId() == 1) {
				context.put("yillikUcretliIzin", "X");
				context.put("ucretliIzin", "");
				context.put("idariIzin", "");
				context.put("dogumIzni", "");
				context.put("olumIzni", "");
				context.put("evlenmeIzni", "");
				context.put("vizitiyeCikma", "");
				context.put("gorevli", "");

			} else if (REDO.getCommonDayoffType().getId() == 2) {
				context.put("yillikUcretliIzin", "");
				context.put("ucretliIzin", "X");
				context.put("idariIzin", "");
				context.put("dogumIzni", "");
				context.put("olumIzni", "");
				context.put("evlenmeIzni", "");
				context.put("vizitiyeCikma", "");
				context.put("gorevli", "");

			} else if (REDO.getCommonDayoffType().getId() == 3) {
				context.put("yillikUcretliIzin", "");
				context.put("ucretliIzin", "");
				context.put("idariIzin", "X");
				context.put("dogumIzni", "");
				context.put("olumIzni", "");
				context.put("evlenmeIzni", "");
				context.put("vizitiyeCikma", "");
				context.put("gorevli", "");

			} else if (REDO.getCommonDayoffType().getId() == 4) {
				context.put("yillikUcretliIzin", "");
				context.put("ucretliIzin", "");
				context.put("idariIzin", "");
				context.put("dogumIzni", "X");
				context.put("olumIzni", "");
				context.put("evlenmeIzni", "");
				context.put("vizitiyeCikma", "");
				context.put("gorevli", "");

			} else if (REDO.getCommonDayoffType().getId() == 5) {
				context.put("yillikUcretliIzin", "");
				context.put("ucretliIzin", "");
				context.put("idariIzin", "");
				context.put("dogumIzni", "");
				context.put("olumIzni", "X");
				context.put("evlenmeIzni", "");
				context.put("vizitiyeCikma", "");
				context.put("gorevli", "");

			} else if (REDO.getCommonDayoffType().getId() == 6) {
				context.put("yillikUcretliIzin", "");
				context.put("ucretliIzin", "");
				context.put("idariIzin", "");
				context.put("dogumIzni", "");
				context.put("olumIzni", "");
				context.put("evlenmeIzni", "X");
				context.put("vizitiyeCikma", "");
				context.put("gorevli", "");

			} else if (REDO.getCommonDayoffType().getId() == 7) {
				context.put("yillikUcretliIzin", "");
				context.put("ucretliIzin", "");
				context.put("idariIzin", "");
				context.put("dogumIzni", "");
				context.put("olumIzni", "");
				context.put("evlenmeIzni", "");
				context.put("vizitiyeCikma", "X");
				context.put("gorevli", "");

			} else if (REDO.getCommonDayoffType().getId() == 8) {
				context.put("yillikUcretliIzin", "");
				context.put("ucretliIzin", "");
				context.put("idariIzin", "");
				context.put("dogumIzni", "");
				context.put("olumIzni", "");
				context.put("evlenmeIzni", "");
				context.put("vizitiyeCikma", "");
				context.put("gorevli", "X");
			}

			List<String> date = new ArrayList<String>();

			date.add(new SimpleDateFormat("dd.MM.yyyy").format(REDO
					.getStartDate()));
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(REDO
					.getEndDate()));
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(REDO
					.getWorkStartDate()));

			context.put("Durum", REDO.getEmployee().getKalanIzin());
			context.put("StartDate", date.get(0));
			context.put("EndDate", date.get(1));
			context.put("WorkStartDate", date.get(2));
			context.put("RemainedDayoffs", REDO.getEmployee().getKalanIzin()
					- REDO.getDayCount());
			if (REDO.getDeputy() == null) {
				context.put("Vekil", " ");
			} else {
				context.put("Vekil", REDO.getDeputy().getFirstname() + " "
						+ REDO.getDeputy().getLastname());
			}

			if (REDO.getSupervisorApproval() == 1) {
				context.put("firstSupervisor", "ONAYLANDI");
			} else {
				context.put("firstSupervisor", " ");
			}
			if (REDO.getSecondSupervisorApproval() == 1) {
				context.put("secondSupervisor", "ONAYLANDI");
			} else {
				context.put("secondSupervisor", " ");
			}
			if (REDO.getBmApproval() == 1) {
				context.put("bm", "ONAYLANDI");
			} else {
				context.put("bm", " ");
			}
			if (REDO.getHrApproval() == 1) {
				context.put("ik", "ONAYLANDI");
			} else {
				context.put("ik", " ");
			}

			// 3) Generate report by merging Java model with the Docx
			OutputStream out = new FileOutputStream(new File(realOutFilePath));
			report.process(context, out);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (XDocReportException e) {
			e.printStackTrace();
		}

		try {
			FileOperations.getInstance().streamFile(realOutFilePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String getRealFilePath(String filePath) {

		String realFilePath = FacesContext.getCurrentInstance()
				.getExternalContext().getRealPath(filePath);

		return realFilePath;
	}

	@SuppressWarnings("static-access")
	public void sendMail() {

		MailUtil mailler = new MailUtil();
		String mailList = null;
		String mailHeader = null;
		String mailContent = null;
		mailler.SendMail(mailList, mailHeader, mailContent);

	}

	/**
	 * 
	 */
	private void checkStatus(EmployeeDayoff employyeDayoff) {

		EmployeeDayoffManager manager = new EmployeeDayoffManager();
		EmployeeManager empManager = new EmployeeManager();
		// pasif yapılıyor
		if (employyeDayoff.getSupervisorApproval() != 0
				&& employyeDayoff.getSecondSupervisorApproval() != 0
				&& employyeDayoff.getHrApproval() != 0
				&& employyeDayoff.getBmApproval() != 0) {
			employyeDayoff.setActive(false);
			manager.updateEntity(employyeDayoff);
		}
		// kalan izinden düşülüyor
		if (employyeDayoff.getSupervisorApproval() == 1
				&& employyeDayoff.getSecondSupervisorApproval() == 1
				&& employyeDayoff.getHrApproval() == 1
				&& employyeDayoff.getBmApproval() == 1) {

			employyeDayoff.getEmployee().setKalanIzin(
					employyeDayoff.getEmployee().getKalanIzin()
							- employyeDayoff.getDayCount());
			empManager.updateEntity(employyeDayoff.getEmployee());
		}

	}

	// ///////////////////////////////////////////////////
	// ///////// G E T T E R S - S E T T E R S ///////////
	// ///////////////////////////////////////////////////

	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public List<EmployeeDayoff> getEmployeeDayoffRequests() {
		return employeeDayoffRequests;
	}

	public void setEmployeeDayoffRequests(
			List<EmployeeDayoff> employeeDayoffRequests) {
		this.employeeDayoffRequests = employeeDayoffRequests;
	}

	public Employee getCurrentEmployee() {
		return currentEmployee;
	}

	public void setCurrentEmployee(Employee currentEmployee) {
		this.currentEmployee = currentEmployee;
	}

	public EmployeeDayoff getNewEmployeeDayoff() {
		return newEmployeeDayoff;
	}

	public void setNewEmployeeDayoff(EmployeeDayoff newEmployeeDayoff) {
		this.newEmployeeDayoff = newEmployeeDayoff;
	}

	public EmployeeDayoff getSelectedSubEmployeeDayoff() {
		return selectedSubEmployeeDayoff;
	}

	public void setSelectedSubEmployeeDayoff(
			EmployeeDayoff selectedSubEmployeeDayoff) {
		this.selectedSubEmployeeDayoff = selectedSubEmployeeDayoff;
	}

	public EmployeeDayoff getSelectedSelfDayoff() {
		return selectedSelfDayoff;
	}

	public void setSelectedSelfDayoff(EmployeeDayoff selectedSelfDayoff) {
		this.selectedSelfDayoff = selectedSelfDayoff;
	}

	public EmployeeDayoff getHRSelectedEmployeeDayoff() {
		return HRSelectedEmployeeDayoff;
	}

	public void setHRSelectedEmployeeDayoff(
			EmployeeDayoff hRSelectedEmployeeDayoff) {
		HRSelectedEmployeeDayoff = hRSelectedEmployeeDayoff;
	}

	public List<EmployeeDayoff> getCurrentEmployeeDayoffRequests() {
		return currentEmployeeDayoffRequests;
	}

	public void setCurrentEmployeeDayoffRequests(
			List<EmployeeDayoff> currentEmployeeDayoffRequests) {
		this.currentEmployeeDayoffRequests = currentEmployeeDayoffRequests;
	}

	public List<EmployeeDayoff> getSubEmployeeDayoffRequests() {
		return subEmployeeDayoffRequests;
	}

	public void setSubEmployeeDayoffRequests(
			List<EmployeeDayoff> subEmployeeDayoffRequests) {
		this.subEmployeeDayoffRequests = subEmployeeDayoffRequests;
	}

	public List<CommonDayoffType> getAllCommonDayoffTypes() {
		return allCommonDayoffTypes;
	}

	public void setAllCommonDayoffTypes(
			List<CommonDayoffType> allCommonDayoffTypes) {
		this.allCommonDayoffTypes = allCommonDayoffTypes;
	}

	/**
	 * @return the bmEmployeeDayoffRequests
	 */
	public List<EmployeeDayoff> getBmEmployeeDayoffRequests() {
		return bmEmployeeDayoffRequests;
	}

	/**
	 * @param bmEmployeeDayoffRequests
	 *            the bmEmployeeDayoffRequests to set
	 */
	public void setBmEmployeeDayoffRequests(
			List<EmployeeDayoff> bmEmployeeDayoffRequests) {
		this.bmEmployeeDayoffRequests = bmEmployeeDayoffRequests;
	}

	/**
	 * @return the selectedBMEmployeeDayoff
	 */
	public EmployeeDayoff getSelectedBMEmployeeDayoff() {
		return selectedBMEmployeeDayoff;
	}

	/**
	 * @param selectedBMEmployeeDayoff
	 *            the selectedBMEmployeeDayoff to set
	 */
	public void setSelectedBMEmployeeDayoff(
			EmployeeDayoff selectedBMEmployeeDayoff) {
		this.selectedBMEmployeeDayoff = selectedBMEmployeeDayoff;
	}

	/**
	 * @return the renderTab
	 */
	public boolean isRenderTab() {
		return renderTab;
	}

	/**
	 * @param renderTab
	 *            the renderTab to set
	 */
	public void setRenderTab(boolean renderTab) {
		this.renderTab = renderTab;
	}

	/**
	 * @return the birUstAmir
	 */
	public Employee getBirUstAmir() {
		return birUstAmir;
	}

	/**
	 * @param birUstAmir
	 *            the birUstAmir to set
	 */
	public void setBirUstAmir(Employee birUstAmir) {
		this.birUstAmir = birUstAmir;
	}

	/**
	 * @return the ikiUstAmir
	 */
	public Employee getIkiUstAmir() {
		return ikiUstAmir;
	}

	/**
	 * @param ikiUstAmir
	 *            the ikiUstAmir to set
	 */
	public void setIkiUstAmir(Employee ikiUstAmir) {
		this.ikiUstAmir = ikiUstAmir;
	}

}
