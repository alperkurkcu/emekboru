/**
 * 
 */
package com.emekboru.beans.istakipformu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.istakipformu.IsTakipFormuPolietilenKaplamaSonraIslemler;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isTakipFormuPolietilenKaplamaSonraIslemlerBean")
@ViewScoped
public class IsTakipFormuPolietilenKaplamaSonraIslemlerBean implements
		Serializable {

	private static final long serialVersionUID = 1L;

	private List<IsTakipFormuPolietilenKaplamaSonraIslemler> allIsTakipFormuPolietilenKaplamaSonraIslemlerList = new ArrayList<IsTakipFormuPolietilenKaplamaSonraIslemler>();
	private IsTakipFormuPolietilenKaplamaSonraIslemler isTakipFormuPolietilenKaplamaSonraIslemlerForm = new IsTakipFormuPolietilenKaplamaSonraIslemler();

	// setters getters
	public List<IsTakipFormuPolietilenKaplamaSonraIslemler> getAllIsTakipFormuPolietilenKaplamaSonraIslemlerList() {
		return allIsTakipFormuPolietilenKaplamaSonraIslemlerList;
	}

	public void setAllIsTakipFormuPolietilenKaplamaSonraIslemlerList(
			List<IsTakipFormuPolietilenKaplamaSonraIslemler> allIsTakipFormuPolietilenKaplamaSonraIslemlerList) {
		this.allIsTakipFormuPolietilenKaplamaSonraIslemlerList = allIsTakipFormuPolietilenKaplamaSonraIslemlerList;
	}

	public IsTakipFormuPolietilenKaplamaSonraIslemler getIsTakipFormuPolietilenKaplamaSonraIslemlerForm() {
		return isTakipFormuPolietilenKaplamaSonraIslemlerForm;
	}

	public void setIsTakipFormuPolietilenKaplamaSonraIslemlerForm(
			IsTakipFormuPolietilenKaplamaSonraIslemler isTakipFormuPolietilenKaplamaSonraIslemlerForm) {
		this.isTakipFormuPolietilenKaplamaSonraIslemlerForm = isTakipFormuPolietilenKaplamaSonraIslemlerForm;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
