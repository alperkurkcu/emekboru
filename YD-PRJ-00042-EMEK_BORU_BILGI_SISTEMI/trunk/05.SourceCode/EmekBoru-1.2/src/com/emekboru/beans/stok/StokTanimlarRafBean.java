/**
 * 
 */
package com.emekboru.beans.stok;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.stok.StokTanimlarRaf;
import com.emekboru.jpaman.stok.StokTanimlarRafManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "stokTanimlarRafBean")
@ViewScoped
public class StokTanimlarRafBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<StokTanimlarRaf> allStokTanimlarRafList = new ArrayList<StokTanimlarRaf>();

	private StokTanimlarRaf stokTanimlarRafForm = new StokTanimlarRaf();

	StokTanimlarRafManager formManager = new StokTanimlarRafManager();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	public StokTanimlarRafBean() {

		fillTestList();
	}

	public void addForm() {

		stokTanimlarRafForm = new StokTanimlarRaf();
		updateButtonRender = false;
	}

	public void addOrUpdateForm() {

		if (stokTanimlarRafForm.getId() == null) {

			stokTanimlarRafForm.setEklemeZamani(UtilInsCore.getTarihZaman());
			stokTanimlarRafForm.setEkleyenKullanici(userBean.getUser().getId());

			formManager.enterNew(stokTanimlarRafForm);

			this.stokTanimlarRafForm = new StokTanimlarRaf();
			FacesContextUtils.addInfoMessage("FormSubmitMessage");

		} else {

			stokTanimlarRafForm
					.setGuncellemeZamani(UtilInsCore.getTarihZaman());
			stokTanimlarRafForm.setGuncelleyenKullanici(userBean.getUser()
					.getId());
			formManager.updateEntity(stokTanimlarRafForm);

			FacesContextUtils.addInfoMessage("FormUpdateMessage");
		}

		fillTestList();
	}

	public void deleteForm() {

		if (stokTanimlarRafForm.getId() == null) {

			FacesContextUtils.addWarnMessage("NoSelectMessage");
			return;
		}
		formManager.delete(stokTanimlarRafForm);
		fillTestList();
		FacesContextUtils.addWarnMessage("TestDeleteMessage");
	}

	public void fillTestList() {

		StokTanimlarRafManager manager = new StokTanimlarRafManager();
		allStokTanimlarRafList = manager.getAllStokTanimlarRaf();
	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	// setters getters

	public List<StokTanimlarRaf> getAllStokTanimlarRafList() {
		return allStokTanimlarRafList;
	}

	public void setAllStokTanimlarRafList(
			List<StokTanimlarRaf> allStokTanimlarRafList) {
		this.allStokTanimlarRafList = allStokTanimlarRafList;
	}

	public StokTanimlarRaf getStokTanimlarRafForm() {
		return stokTanimlarRafForm;
	}

	public void setStokTanimlarRafForm(StokTanimlarRaf stokTanimlarRafForm) {
		this.stokTanimlarRafForm = stokTanimlarRafForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
