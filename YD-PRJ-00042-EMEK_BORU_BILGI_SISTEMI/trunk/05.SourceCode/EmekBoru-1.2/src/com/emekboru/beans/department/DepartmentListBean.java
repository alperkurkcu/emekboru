package com.emekboru.beans.department;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Department;
import com.emekboru.jpaman.DepartmentManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "departmentListBean")
@SessionScoped
public class DepartmentListBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	private static List<Department> departmentList;

	@PostConstruct
	private void load() {

		DepartmentManager manager = new DepartmentManager();
		try {
			// departmentList = manager.findAll(Department.class);
			departmentList = manager
					.findAllOrderByASC(Department.class, "name");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out
					.println("HATA - departmentList - departmentListBean.load()");
			e.printStackTrace();
		}
	}

	public DepartmentListBean() {
		departmentList = new ArrayList<Department>();
	}

	public static void loadDepartmentList() {
		System.out.println("DepartmentListBean.loadDepartmentList()");
		DepartmentManager departmentManager = new DepartmentManager();
		departmentList = departmentManager.findAll(Department.class);
	}

	public void goToNewDepartmentPage() {
		loadDepartmentList();
		FacesContextUtils.redirect(config.getPageUrl().NEW_DEPARTMENT_PAGE);
	}

	public void goToDepartmentManagePage() {
		loadDepartmentList();
		FacesContextUtils.redirect(config.getPageUrl().MANAGE_DEPARTMENT_PAGE);
	}

	public List<Department> getDepartmentList() {
		return departmentList;
	}

	@SuppressWarnings("static-access")
	public void setDepartmentList(List<Department> departmentList) {
		this.departmentList = departmentList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
