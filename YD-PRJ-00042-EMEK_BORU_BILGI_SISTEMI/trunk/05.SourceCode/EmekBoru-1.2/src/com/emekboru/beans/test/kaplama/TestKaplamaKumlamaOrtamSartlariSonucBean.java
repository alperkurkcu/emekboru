package com.emekboru.beans.test.kaplama;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaKumlamaOrtamSartlariSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaKumlamaOrtamSartlariSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaplamaKumlamaOrtamSartlariSonucBean")
@ViewScoped
public class TestKaplamaKumlamaOrtamSartlariSonucBean {

	private TestKaplamaKumlamaOrtamSartlariSonuc testKaplamaKumlamaOrtamSartlariSonucForm = new TestKaplamaKumlamaOrtamSartlariSonuc();
	private List<TestKaplamaKumlamaOrtamSartlariSonuc> allTestKaplamaKumlamaOrtamSartlariSonucList = new ArrayList<TestKaplamaKumlamaOrtamSartlariSonuc>();
	private TestKaplamaKumlamaOrtamSartlariSonucManager manager = new TestKaplamaKumlamaOrtamSartlariSonucManager();
	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateTestKaplamaKumlamaOrtamSartlariSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		if (testKaplamaKumlamaOrtamSartlariSonucForm.getId() == null) {

			testKaplamaKumlamaOrtamSartlariSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaKumlamaOrtamSartlariSonucForm
					.setEkleyenKullanici(userBean.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaKumlamaOrtamSartlariSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);
			testKaplamaKumlamaOrtamSartlariSonucForm.setBagliTestId(bagliTest);
			testKaplamaKumlamaOrtamSartlariSonucForm
					.setBagliGlobalId(bagliTest);

			manager.updateEntity(testKaplamaKumlamaOrtamSartlariSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaKumlamaOrtamSartlariSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaKumlamaOrtamSartlariSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			manager.enterNew(testKaplamaKumlamaOrtamSartlariSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addTestKaplamaKumlamaOrtamSartlariSonuc() {

		testKaplamaKumlamaOrtamSartlariSonucForm = new TestKaplamaKumlamaOrtamSartlariSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaKumlamaOrtamSartlariSonucListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaKumlamaOrtamSartlariSonucList = TestKaplamaKumlamaOrtamSartlariSonucManager
				.getAllTestKaplamaKumlamaOrtamSartlariSonuc(globalId, pipeId);
		testKaplamaKumlamaOrtamSartlariSonucForm = new TestKaplamaKumlamaOrtamSartlariSonuc();
	}

	public void deleteTestKaplamaKumlamaOrtamSartlariSonuc() {

		bagliTestId = testKaplamaKumlamaOrtamSartlariSonucForm
				.getBagliGlobalId().getId();

		if (testKaplamaKumlamaOrtamSartlariSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		manager.delete(testKaplamaKumlamaOrtamSartlariSonucForm);
		testKaplamaKumlamaOrtamSartlariSonucForm = new TestKaplamaKumlamaOrtamSartlariSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters
	public TestKaplamaKumlamaOrtamSartlariSonuc getTestKaplamaKumlamaOrtamSartlariSonucForm() {
		return testKaplamaKumlamaOrtamSartlariSonucForm;
	}

	public void setTestKaplamaKumlamaOrtamSartlariSonucForm(
			TestKaplamaKumlamaOrtamSartlariSonuc testKaplamaKumlamaOrtamSartlariSonucForm) {
		this.testKaplamaKumlamaOrtamSartlariSonucForm = testKaplamaKumlamaOrtamSartlariSonucForm;
	}

	public List<TestKaplamaKumlamaOrtamSartlariSonuc> getAllTestKaplamaKumlamaOrtamSartlariSonucList() {
		return allTestKaplamaKumlamaOrtamSartlariSonucList;
	}

	public void setAllTestKaplamaKumlamaOrtamSartlariSonucList(
			List<TestKaplamaKumlamaOrtamSartlariSonuc> allTestKaplamaKumlamaOrtamSartlariSonucList) {
		this.allTestKaplamaKumlamaOrtamSartlariSonucList = allTestKaplamaKumlamaOrtamSartlariSonucList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
