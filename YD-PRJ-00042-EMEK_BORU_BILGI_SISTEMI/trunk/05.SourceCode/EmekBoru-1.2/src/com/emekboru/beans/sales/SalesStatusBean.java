package com.emekboru.beans.sales;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.sales.SalesStatus;
import com.emekboru.jpaman.sales.SalesStatusManager;

@ManagedBean(name = "salesStatusBean")
@ViewScoped
public class SalesStatusBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7927953383023715147L;
	private List<SalesStatus> statusList;

	public SalesStatusBean() {

		SalesStatusManager manager = new SalesStatusManager();
		statusList = manager.findAll(SalesStatus.class);
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public List<SalesStatus> getStatusList() {
		return statusList;
	}

	public void setStatusList(List<SalesStatus> statusList) {
		this.statusList = statusList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}