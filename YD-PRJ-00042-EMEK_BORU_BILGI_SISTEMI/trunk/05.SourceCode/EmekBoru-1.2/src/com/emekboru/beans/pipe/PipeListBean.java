/**
 * 
 */
package com.emekboru.beans.pipe;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;

import com.emekboru.beans.test.TestRendersBean;
import com.emekboru.beans.test.tahribatsiztestsonuc.TestTahribatsizFloroskopikSonucBean;
import com.emekboru.beans.test.tahribatsiztestsonuc.TestTahribatsizManuelUtSonucBean;
import com.emekboru.beans.test.tahribatsiztestsonuc.TestTahribatsizOtomatikUtSonucBean;
import com.emekboru.jpa.NondestructiveTestsSpec;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpaman.NondestructiveTestsSpecManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.utils.UtilInsCore;
import com.emekboru.utils.lazymodels.LazyPipeDataModel;

/**
 * @author KRKC
 * 
 */
@ManagedBean(name = "pipeListBean")
@ViewScoped
public class PipeListBean implements Serializable {

	private static final long serialVersionUID = 7506162397277463796L;

	// Pipes
	private LazyDataModel<Pipe> lazyModel;
	private Pipe selectedPipe = new Pipe();
	private Pipe selectedBarcodePipe = new Pipe();
	private String barkodNo = null;
	private Integer itemNo = 0;

	// Tahribatsiz Testler Kalite Sartlari
	private List<NondestructiveTestsSpec> nondestructiveTests = new ArrayList<NondestructiveTestsSpec>();
	private NondestructiveTestsSpec selectedNonTest = new NondestructiveTestsSpec();

	// tahribatsız test sonuc
	@ManagedProperty(value = "#{testTahribatsizOtomatikUtSonucBean}")
	private TestTahribatsizOtomatikUtSonucBean testTahribatsizOtomatikUtSonucBean;

	@ManagedProperty(value = "#{testTahribatsizManuelUtSonucBean}")
	private TestTahribatsizManuelUtSonucBean testTahribatsizManuelUtSonucBean;

	@ManagedProperty(value = "#{testTahribatsizFloroskopikSonucBean}")
	private TestTahribatsizFloroskopikSonucBean testTahribatsizFloroskopikSonucBean;

	@ManagedProperty(value = "#{testRendersBean}")
	private TestRendersBean testRendersBean;

	public void loadSelectedOrder(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeManager = new PipeManager();

		if (pipeManager.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!", null));
			return;
		} else {

			selectedBarcodePipe = pipeManager.findByBarkodNo(prmBarkod).get(0);

			lazyModel = new LazyPipeDataModel(selectedBarcodePipe
					.getSalesItem().getItemId());
			selectedBarcodePipe.getSalesItem().getNondestructiveTestsSpecs();

			NondestructiveTestsSpecManager nonMan = new NondestructiveTestsSpecManager();
			nondestructiveTests = (List<NondestructiveTestsSpec>) nonMan
					.findByFieldOrderBy(NondestructiveTestsSpec.class,
							"salesItem.itemId", selectedBarcodePipe
									.getSalesItem().getItemId(), "globalId");
		}
	}

	public void nonTestResultListener(SelectEvent event) {
		// tahribats�z testler
		// otomatik Ut kaynak
		if (selectedNonTest.getGlobalId() == 1001) {
			falseAllRenders();
			testRendersBean.setTestOtomatikUtKaynak(true);
			testTahribatsizOtomatikUtSonucBean.fillTestList(selectedPipe
					.getPipeId());
		}// otomatik Ut laminasyon
		else if (selectedNonTest.getGlobalId() == 1002) {
			falseAllRenders();
			testRendersBean.setTestOtomatikUtKaynak(true);
			testTahribatsizOtomatikUtSonucBean.fillTestList(selectedPipe
					.getPipeId());
		}// manuel ut
		else if (selectedNonTest.getGlobalId() == 1004) {
			falseAllRenders();
			testRendersBean.setTestManuelUt(true);
			testTahribatsizManuelUtSonucBean.fillTestList(selectedPipe
					.getPipeId());
		}// floroskopik
		else if (selectedNonTest.getGlobalId() == 1006) {
			falseAllRenders();
			testRendersBean.setTestFloroskopik(true);
			testTahribatsizFloroskopikSonucBean.fillTestList(selectedPipe
					.getPipeId());
		}
	}

	public void falseAllRenders() {
		UtilInsCore.setProperties(testRendersBean, false);
	}

	/**
	 * @return the lazyModel
	 */
	public LazyDataModel<Pipe> getLazyModel() {
		return lazyModel;
	}

	/**
	 * @param lazyModel
	 *            the lazyModel to set
	 */
	public void setLazyModel(LazyDataModel<Pipe> lazyModel) {
		this.lazyModel = lazyModel;
	}

	/**
	 * @return the selectedPipe
	 */
	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	/**
	 * @param selectedPipe
	 *            the selectedPipe to set
	 */
	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	/**
	 * @return the selectedBarcodePipe
	 */
	public Pipe getSelectedBarcodePipe() {
		return selectedBarcodePipe;
	}

	/**
	 * @param selectedBarcodePipe
	 *            the selectedBarcodePipe to set
	 */
	public void setSelectedBarcodePipe(Pipe selectedBarcodePipe) {
		this.selectedBarcodePipe = selectedBarcodePipe;
	}

	/**
	 * @return the barkodNo
	 */
	public String getBarkodNo() {
		return barkodNo;
	}

	/**
	 * @param barkodNo
	 *            the barkodNo to set
	 */
	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	/**
	 * @return the nondestructiveTests
	 */
	public List<NondestructiveTestsSpec> getNondestructiveTests() {
		return nondestructiveTests;
	}

	/**
	 * @param nondestructiveTests
	 *            the nondestructiveTests to set
	 */
	public void setNondestructiveTests(
			List<NondestructiveTestsSpec> nondestructiveTests) {
		this.nondestructiveTests = nondestructiveTests;
	}

	/**
	 * @return the selectedNonTest
	 */
	public NondestructiveTestsSpec getSelectedNonTest() {
		return selectedNonTest;
	}

	/**
	 * @param selectedNonTest
	 *            the selectedNonTest to set
	 */
	public void setSelectedNonTest(NondestructiveTestsSpec selectedNonTest) {
		this.selectedNonTest = selectedNonTest;
	}

	/**
	 * @return the testTahribatsizOtomatikUtSonucBean
	 */
	public TestTahribatsizOtomatikUtSonucBean getTestTahribatsizOtomatikUtSonucBean() {
		return testTahribatsizOtomatikUtSonucBean;
	}

	/**
	 * @param testTahribatsizOtomatikUtSonucBean
	 *            the testTahribatsizOtomatikUtSonucBean to set
	 */
	public void setTestTahribatsizOtomatikUtSonucBean(
			TestTahribatsizOtomatikUtSonucBean testTahribatsizOtomatikUtSonucBean) {
		this.testTahribatsizOtomatikUtSonucBean = testTahribatsizOtomatikUtSonucBean;
	}

	/**
	 * @return the testTahribatsizManuelUtSonucBean
	 */
	public TestTahribatsizManuelUtSonucBean getTestTahribatsizManuelUtSonucBean() {
		return testTahribatsizManuelUtSonucBean;
	}

	/**
	 * @param testTahribatsizManuelUtSonucBean
	 *            the testTahribatsizManuelUtSonucBean to set
	 */
	public void setTestTahribatsizManuelUtSonucBean(
			TestTahribatsizManuelUtSonucBean testTahribatsizManuelUtSonucBean) {
		this.testTahribatsizManuelUtSonucBean = testTahribatsizManuelUtSonucBean;
	}

	/**
	 * @return the testTahribatsizFloroskopikSonucBean
	 */
	public TestTahribatsizFloroskopikSonucBean getTestTahribatsizFloroskopikSonucBean() {
		return testTahribatsizFloroskopikSonucBean;
	}

	/**
	 * @param testTahribatsizFloroskopikSonucBean
	 *            the testTahribatsizFloroskopikSonucBean to set
	 */
	public void setTestTahribatsizFloroskopikSonucBean(
			TestTahribatsizFloroskopikSonucBean testTahribatsizFloroskopikSonucBean) {
		this.testTahribatsizFloroskopikSonucBean = testTahribatsizFloroskopikSonucBean;
	}

	/**
	 * @return the testRendersBean
	 */
	public TestRendersBean getTestRendersBean() {
		return testRendersBean;
	}

	/**
	 * @param testRendersBean
	 *            the testRendersBean to set
	 */
	public void setTestRendersBean(TestRendersBean testRendersBean) {
		this.testRendersBean = testRendersBean;
	}

}
