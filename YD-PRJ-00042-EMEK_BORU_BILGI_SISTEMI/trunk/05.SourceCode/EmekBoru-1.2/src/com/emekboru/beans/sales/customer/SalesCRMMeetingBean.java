package com.emekboru.beans.sales.customer;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.emekboru.beans.sales.SalesMenuBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Employee;
import com.emekboru.jpa.sales.customer.SalesCRMMeeting;
import com.emekboru.jpa.sales.customer.SalesContactPeople;
import com.emekboru.jpa.sales.customer.SalesJoinCRMMeetingContactPeople;
import com.emekboru.jpa.sales.customer.SalesJoinCRMMeetingEmployee;
import com.emekboru.jpaman.EmployeeManager;
import com.emekboru.jpaman.sales.customer.SalesCRMMeetingManager;
import com.emekboru.jpaman.sales.customer.SalesContactPeopleManager;
import com.emekboru.jpaman.sales.customer.SalesJoinCRMMeetingContactPeopleManager;
import com.emekboru.jpaman.sales.customer.SalesJoinCRMMeetingEmployeeManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "salesCRMMeetingBean")
@ViewScoped
public class SalesCRMMeetingBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -376627643425845835L;

	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{salesMenuBean}")
	private SalesMenuBean salesMenuBean;

	private SalesCRMMeeting newMeeting = new SalesCRMMeeting();
	private SalesCRMMeeting selectedMeeting;

	private List<SalesContactPeople> newContactPeopleList;
	private List<SalesContactPeople> selectedContactPeopleList;

	private List<SalesCRMMeeting> meetingList;
	private List<SalesCRMMeeting> filteredList;
	private List<SalesCRMMeeting> meetingsOfCustomer;
	private StreamedContent downloadedFile;
	private String downloadedFileName;

	private File[] fileArray;

	private File theFile = null;
	OutputStream output = null;
	InputStream input = null;

	private String path;
	private String privatePath;
	private String toBeDeleted;

	int length;

	public List<Employee> employeeList;
	private Employee[] selectedEmployees;
	public Integer departmentId;

	public SalesCRMMeetingBean() {
		newMeeting = new SalesCRMMeeting();
		selectedMeeting = new SalesCRMMeeting();
		employeeList = new ArrayList<Employee>();

		privatePath = File.separatorChar + "sales" + File.separatorChar
				+ "uploadedDocuments" + File.separatorChar + "meeting"
				+ File.separatorChar;
	}

	@PostConstruct
	public void load() {
		System.out.println("SalesCRMMeetingBean.load()");

		try {
			this.setPath(config.getConfig().getFolder().getAbsolutePath()
					+ privatePath);

			System.out.println("Path for Upload File (Sales):			" + path);
		} catch (Exception ex) {
			System.out.println("Path for Upload File (Sales):			"
					+ FacesContext.getCurrentInstance().getExternalContext()
							.getRealPath(privatePath));

			System.out.print(path);
			System.out.println("CAUSE OF " + ex.toString());
		}

		theFile = new File(path);
		if (!theFile.isDirectory()) {
			theFile.mkdirs();
		}

		fileArray = theFile.listFiles();

		try {
			SalesCRMMeetingManager manager = new SalesCRMMeetingManager();
			meetingList = manager.findAll(SalesCRMMeeting.class);
			/* JOIN FUNCTIONS */
			for (SalesCRMMeeting meeting : meetingList) {
				this.loadJoinCRMMeetingEmployee(meeting);
				this.loadJoinCRMMeetingContactPeople(meeting);
			}
			/* end of JOIN FUNCTIONS */
			selectedMeeting = meetingList.get(0);
		} catch (Exception e) {
			System.out.println("SalesCRMMeetingBean:" + e.toString());
		}

		SalesContactPeopleManager peopleManager = new SalesContactPeopleManager();
		selectedContactPeopleList = peopleManager
				.findAll(SalesContactPeople.class);
		// newContactPeopleList =
		// peopleManager.findAll(SalesContactPeople.class);
	}

	public void add() {
		System.out.println("SalesCRMMeetingBean.add()");

		try {

			for (int i = 0; i < selectedEmployees.length; i++) {
				newMeeting.getParticipantList().add(selectedEmployees[i]);
			}

			SalesCRMMeetingManager manager = new SalesCRMMeetingManager();
			manager.enterNew(newMeeting);
			/* JOIN FUNCTIONS */
			this.addJoinCRMMeetingEmployee(newMeeting);
			this.addJoinCRMMeetingContactPeople(newMeeting);
			/* end of JOIN FUNCTIONS */
			meetingList.add(newMeeting);
			newMeeting = new SalesCRMMeeting();
			FacesContextUtils.addInfoMessage("SubmitMessage");
		} catch (Exception e) {
			System.out.println("SalesCRMMeetingBean:" + e.toString());
		}
	}

	public void update() {
		System.out.println("SalesCRMMeetingBean.update()");

		try {
			SalesCRMMeetingManager manager = new SalesCRMMeetingManager();

			/* JOIN FUNCTIONS */
			this.deleteJoinCRMMeetingEmployee(selectedMeeting);
			this.addJoinCRMMeetingEmployee(selectedMeeting);
			this.deleteJoinCRMMeetingContactPeople(selectedMeeting);
			this.addJoinCRMMeetingContactPeople(selectedMeeting);
			/* end of JOIN FUNCTIONS */
			manager.updateEntity(selectedMeeting);

			FacesContextUtils.addInfoMessage("UpdateItemMessage");
		} catch (Exception e) {
			System.out.println("SalesCRMMeetingBean:" + e.toString());
		}
	}

	public void delete() {
		System.out.println("SalesCRMMeetingBean.delete()");

		boolean isDeleted = true;
		try {
			if (selectedMeeting.getFileNumber() > 0) {
				isDeleted = false;

				// Delete the uploaded file
				theFile = new File(path);
				fileArray = theFile.listFiles();

				for (int i = 0; i < selectedMeeting.getFileNames().size(); i++) {
					System.out.println("document that will be deleted: "
							+ privatePath
							+ selectedMeeting.getFileNames().get(i));
					for (int j = 0; j < fileArray.length; j++) {
						System.out.println("fileArray[j].toString(): "
								+ fileArray[j].toString());
						if (fileArray[j].toString()
								.contains(
										privatePath
												+ selectedMeeting
														.getFileNames().get(i))) {
							isDeleted = fileArray[j].delete();
						}
					}
				}
			}
			if (isDeleted) {
				/* JOIN FUNCTIONS */
				this.deleteJoinCRMMeetingEmployee(selectedMeeting);
				this.deleteJoinCRMMeetingContactPeople(selectedMeeting);
				/* end of JOIN FUNCTIONS */
				SalesCRMMeetingManager manager = new SalesCRMMeetingManager();
				manager.delete(selectedMeeting);

				meetingList.remove(selectedMeeting);
				selectedMeeting = meetingList.get(0);

				FacesContextUtils.addWarnMessage("DeletedMessage");
			}

		} catch (Exception e) {
			System.out.println("SalesCRMMeetingBean:" + e.toString());
		}
	}

	public void loadContactListForNewEvent() {
		System.out.println("salesCRMMeetingBean.loadContactListForNewEvent()");

		try {
			SalesContactPeopleManager peopleManager = new SalesContactPeopleManager();
			newContactPeopleList = peopleManager.findByField(
					SalesContactPeople.class, "salesCustomer",
					newMeeting.getSalesCustomer());
		} catch (Exception e) {
			System.out.println("salesCRMMeetingBean:" + e.toString());
		}
	}

	public void loadContactListForSelectedEvent() {
		System.out
				.println("salesCRMMeetingBean.loadContactListForSelectedEvent()");

		try {
			SalesContactPeopleManager peopleManager = new SalesContactPeopleManager();
			selectedContactPeopleList = peopleManager.findByField(
					SalesContactPeople.class, "salesCustomer",
					selectedMeeting.getSalesCustomer());
		} catch (Exception e) {
			System.out.println("salesCRMMeetingBean:" + e.toString());
		}
	}

	/* JOIN FUNCTIONS */
	private void addJoinCRMMeetingEmployee(SalesCRMMeeting e) {
		SalesJoinCRMMeetingEmployeeManager joinManager = new SalesJoinCRMMeetingEmployeeManager();
		SalesJoinCRMMeetingEmployee newJoin;

		for (int i = 0; i < e.getParticipantList().size(); i++) {
			newJoin = new SalesJoinCRMMeetingEmployee();
			newJoin.setEmployee(e.getParticipantList().get(i));
			newJoin.setMeeting(e);
			joinManager.enterNew(newJoin);
		}
	}

	private void loadJoinCRMMeetingEmployee(SalesCRMMeeting meeting) {
		SalesJoinCRMMeetingEmployeeManager joinManager = new SalesJoinCRMMeetingEmployeeManager();
		List<SalesJoinCRMMeetingEmployee> joinList = joinManager
				.findAll(SalesJoinCRMMeetingEmployee.class);

		meeting.setParticipantList(new ArrayList<Employee>());

		for (int i = 0; i < joinList.size(); i++) {
			if (joinList.get(i).getMeeting().equals(meeting)) {
				meeting.getParticipantList().add(joinList.get(i).getEmployee());
			}
		}
	}

	private void deleteJoinCRMMeetingEmployee(SalesCRMMeeting meeting) {
		SalesJoinCRMMeetingEmployeeManager joinManager = new SalesJoinCRMMeetingEmployeeManager();
		List<SalesJoinCRMMeetingEmployee> joinList = joinManager
				.findAll(SalesJoinCRMMeetingEmployee.class);

		for (int i = 0; i < joinList.size(); i++) {
			if (joinList.get(i).getMeeting().equals(meeting)) {
				joinManager.delete(joinList.get(i));
			}
		}
	}

	private void addJoinCRMMeetingContactPeople(SalesCRMMeeting e) {
		SalesJoinCRMMeetingContactPeopleManager joinManager = new SalesJoinCRMMeetingContactPeopleManager();
		SalesJoinCRMMeetingContactPeople newJoin;

		for (int i = 0; i < e.getContactPeopleList().size(); i++) {
			newJoin = new SalesJoinCRMMeetingContactPeople();
			newJoin.setContactPeople(e.getContactPeopleList().get(i));
			newJoin.setMeeting(e);
			joinManager.enterNew(newJoin);
		}
	}

	private void loadJoinCRMMeetingContactPeople(SalesCRMMeeting meeting) {
		SalesJoinCRMMeetingContactPeopleManager joinManager = new SalesJoinCRMMeetingContactPeopleManager();
		List<SalesJoinCRMMeetingContactPeople> joinList = joinManager
				.findAll(SalesJoinCRMMeetingContactPeople.class);

		meeting.setContactPeopleList(new ArrayList<SalesContactPeople>());

		for (int i = 0; i < joinList.size(); i++) {
			if (joinList.get(i).getMeeting().equals(meeting)) {
				meeting.getContactPeopleList().add(
						joinList.get(i).getContactPeople());
			}
		}
	}

	private void deleteJoinCRMMeetingContactPeople(SalesCRMMeeting meeting) {
		SalesJoinCRMMeetingContactPeopleManager joinManager = new SalesJoinCRMMeetingContactPeopleManager();
		List<SalesJoinCRMMeetingContactPeople> joinList = joinManager
				.findAll(SalesJoinCRMMeetingContactPeople.class);

		for (int i = 0; i < joinList.size(); i++) {
			if (joinList.get(i).getMeeting().equals(meeting)) {
				joinManager.delete(joinList.get(i));
			}
		}
	}

	/* end of JOIN FUNCTIONS */

	public String checkFileName(String fileName) {

		fileName = fileName.replace(" ", "_");
		fileName = fileName.replace("ç", "c");
		fileName = fileName.replace("Ç", "C");
		fileName = fileName.replace("ð", "g");
		fileName = fileName.replace("Ð", "G");
		fileName = fileName.replace("Ý", "i");
		fileName = fileName.replace("ý", "i");
		fileName = fileName.replace("ö", "o");
		fileName = fileName.replace("Ö", "O");
		fileName = fileName.replace("þ", "s");
		fileName = fileName.replace("Þ", "S");
		fileName = fileName.replace("ü", "u");
		fileName = fileName.replace("Ü", "U");

		return fileName;
	}

	public void upload(FileUploadEvent event) throws AbortProcessingException,
			IOException {
		System.out.println("SalesCRMMeetingBean.upload()");

		try {
			String name = this.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				newMeeting.setDocuments("//" + name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				SalesCRMMeetingManager manager = new SalesCRMMeetingManager();
				manager.updateEntity(selectedMeeting);

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				FacesContextUtils.addWarnMessage("salesFileUploadedMessage");
			} else {
				System.out.println("File already exists.");

				newMeeting.setDocuments("//");
				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("SalesCRMMeetingBean:" + e.toString());
		}
	}

	public void addUploadedFile(FileUploadEvent event)
			throws AbortProcessingException, IOException {
		System.out.println("SalesCRMMeetingBean.addUploadedFile()");

		try {
			String name = this.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				selectedMeeting.setDocuments(selectedMeeting.getDocuments()
						+ "//" + name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				SalesCRMMeetingManager manager = new SalesCRMMeetingManager();
				manager.updateEntity(selectedMeeting);

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				FacesContextUtils.addWarnMessage("salesFileUploadedMessage");
			} else {
				System.out.println("File already exists.");

				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("SalesCRMMeetingBean:" + e.toString());
		}
	}

	public void deleteTheSelectedFile() {
		System.out.println("SalesCRMVisitBean.deleteTheSelectedFile()");
		try {
			// Delete the uploaded file
			theFile = new File(path);
			fileArray = theFile.listFiles();

			System.out.println("document that will be deleted: " + privatePath
					+ toBeDeleted);
			for (int j = 0; j < fileArray.length; j++) {
				System.out.println("fileArray[j].toString(): "
						+ fileArray[j].toString());
				if (fileArray[j].toString().contains(privatePath + toBeDeleted)) {
					fileArray[j].delete();

					if (selectedMeeting.getDocuments().contentEquals(
							"//" + toBeDeleted)) {
						selectedMeeting.setDocuments(selectedMeeting
								.getDocuments().replace("//" + toBeDeleted,
										"null"));
					} else {
						selectedMeeting
								.setDocuments(selectedMeeting.getDocuments()
										.replace("//" + toBeDeleted, ""));
					}
					FacesContextUtils
							.addWarnMessage("salesTheFileIsDeletedMessage");
				}
			}
		} catch (Exception e) {
			System.out.println("SalesCRMMeetingBean:" + e.toString());
		}
	}

	public void deleteAndUpload(FileUploadEvent event)
			throws AbortProcessingException, IOException {

		System.out.println("SalesCRMMeetingBean.deleteAndUpload()");

		try {
			// Delete the uploaded file
			theFile = new File(path);
			fileArray = theFile.listFiles();

			for (int i = 0; i < selectedMeeting.getFileNames().size(); i++) {
				for (int j = 0; j < fileArray.length; j++) {
					System.out.println("fileArray[j].toString(): "
							+ fileArray[j].toString());
					if (fileArray[j].toString()
							.contains(
									privatePath
											+ selectedMeeting.getFileNames()
													.get(i))) {
						fileArray[j].delete();
					}
				}
			}

			String name = this.checkFileName(event.getFile().getFileName());

			String prefix = FilenameUtils.getBaseName(name);
			String suffix = FilenameUtils.getExtension(name);

			theFile = new File(path + prefix + "." + suffix);

			if (theFile.createNewFile()) {
				System.out.println("File is created!");
				selectedMeeting.setDocuments("//" + name);

				output = new FileOutputStream(theFile);
				IOUtils.copy(event.getFile().getInputstream(), output);
				output.close();

				System.out.println(theFile.getName() + " is uploaded to "
						+ path);

				SalesCRMMeetingManager manager = new SalesCRMMeetingManager();
				manager.updateEntity(selectedMeeting);

				FacesContextUtils.addInfoMessage("UpdateItemMessage");
			} else {
				System.out.println("File already exists.");

				selectedMeeting.setDocuments(null);
				FacesContextUtils.addErrorMessage("salesFileAlreadyExists");
			}
		} catch (Exception e) {
			System.out.println("SalesCRMMeetingBean:" + e.toString());
		}
	}

	public void loadEmployeeListForNewEvent() {
		System.out.println("SalesCRMMeetingBean.loadEmployeeListForNewEvent()");

		try {
			EmployeeManager employeeManager = new EmployeeManager();
			employeeList = employeeManager.findByField(Employee.class,
					"department.departmentId", departmentId);
		} catch (Exception e) {
			System.out.println("SalesCRMMeetingBean:" + e.toString());
		}
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public SalesCRMMeeting getNewMeeting() {
		return newMeeting;
	}

	public void setNewMeeting(SalesCRMMeeting newMeeting) {
		this.newMeeting = newMeeting;
	}

	public SalesCRMMeeting getSelectedMeeting() {
		try {
			if (selectedMeeting == null)
				selectedMeeting = new SalesCRMMeeting();
		} catch (Exception ex) {
			selectedMeeting = new SalesCRMMeeting();
		}
		return selectedMeeting;
	}

	public void setSelectedMeeting(SalesCRMMeeting selectedMeeting) {
		this.selectedMeeting = selectedMeeting;
	}

	public List<SalesCRMMeeting> getMeetingList() {
		return meetingList;
	}

	public void setMeetingList(List<SalesCRMMeeting> meetingList) {
		this.meetingList = meetingList;
	}

	public List<SalesCRMMeeting> getFilteredList() {
		try {
			selectedMeeting = filteredList.get(0);
			salesMenuBean.showNewType1Menu();
		} catch (Exception ex) {
			System.out.println("getFilteredList: " + ex);
		}
		return filteredList;
	}

	public void setFilteredList(List<SalesCRMMeeting> filteredList) {
		this.filteredList = filteredList;
	}

	public SalesMenuBean getSalesMenuBean() {
		return salesMenuBean;
	}

	public void setSalesMenuBean(SalesMenuBean salesMenuBean) {
		this.salesMenuBean = salesMenuBean;
	}

	public List<SalesCRMMeeting> getMeetingsOfCustomer() {
		try {
			meetingsOfCustomer = new ArrayList<SalesCRMMeeting>();

			for (int i = 0; i < meetingList.size(); i++) {
				meetingsOfCustomer.add(meetingList.get(i));
			}
		} catch (Exception e) {
			System.out.println("SalesCRMMeetingBean:" + e.toString());
		}
		return meetingsOfCustomer;
	}

	public void setMeetingsOfCustomer(List<SalesCRMMeeting> meetingsOfCustomer) {
		this.meetingsOfCustomer = meetingsOfCustomer;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getDownloadedFileName() {
		return downloadedFileName;
	}

	public void setDownloadedFileName(String downloadedFileName) {
		this.downloadedFileName = downloadedFileName;
	}

	@SuppressWarnings("resource")
	public StreamedContent getDownloadedFile() {
		System.out.println("SalesCRMMeetingBean.getDownloadedFile()");

		try {
			if (downloadedFileName != null) {
				RandomAccessFile accessFile = new RandomAccessFile(path
						+ downloadedFileName, "r");
				byte[] downloadByte = new byte[(int) accessFile.length()];
				accessFile.read(downloadByte);
				InputStream stream = new ByteArrayInputStream(downloadByte);

				String suffix = FilenameUtils.getExtension(downloadedFileName);
				String contentType = "text/plain";
				if (suffix.contentEquals("jpg") || suffix.contentEquals("png")
						|| suffix.contentEquals("gif")) {
					contentType = "image/" + contentType;
				} else if (suffix.contentEquals("doc")
						|| suffix.contentEquals("docx")) {
					contentType = "application/msword";
				} else if (suffix.contentEquals("xls")
						|| suffix.contentEquals("xlsx")) {
					contentType = "application/vnd.ms-excel";
				} else if (suffix.contentEquals("pdf")) {
					contentType = "application/pdf";
				}
				downloadedFile = new DefaultStreamedContent(stream,
						"contentType", downloadedFileName);

				stream.close();
			}
		} catch (Exception e) {
			System.out.println("SalesCRMMeetingBean.getDownloadedFile():"
					+ e.toString());
		}
		return downloadedFile;
	}

	public String getToBeDeleted() {
		return toBeDeleted;
	}

	public void setToBeDeleted(String toBeDeleted) {
		this.toBeDeleted = toBeDeleted;
	}

	/**
	 * @return the newContactPeopleList
	 */
	public List<SalesContactPeople> getNewContactPeopleList() {
		return newContactPeopleList;
	}

	/**
	 * @param newContactPeopleList
	 *            the newContactPeopleList to set
	 */
	public void setNewContactPeopleList(
			List<SalesContactPeople> newContactPeopleList) {
		this.newContactPeopleList = newContactPeopleList;
	}

	/**
	 * @return the selectedContactPeopleList
	 */
	public List<SalesContactPeople> getSelectedContactPeopleList() {
		return selectedContactPeopleList;
	}

	/**
	 * @param selectedContactPeopleList
	 *            the selectedContactPeopleList to set
	 */
	public void setSelectedContactPeopleList(
			List<SalesContactPeople> selectedContactPeopleList) {
		this.selectedContactPeopleList = selectedContactPeopleList;
	}

	/**
	 * @return the employeeList
	 */
	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	/**
	 * @param employeeList
	 *            the employeeList to set
	 */
	public void setEmployeeList(List<Employee> employeeList) {
		this.employeeList = employeeList;
	}

	/**
	 * @return the departmentId
	 */
	public Integer getDepartmentId() {
		return departmentId;
	}

	/**
	 * @param departmentId
	 *            the departmentId to set
	 */
	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * @return the selectedEmployees
	 */
	public Employee[] getSelectedEmployees() {
		return selectedEmployees;
	}

	/**
	 * @param selectedEmployees
	 *            the selectedEmployees to set
	 */
	public void setSelectedEmployees(Employee[] selectedEmployees) {
		this.selectedEmployees = selectedEmployees;
	}

}
