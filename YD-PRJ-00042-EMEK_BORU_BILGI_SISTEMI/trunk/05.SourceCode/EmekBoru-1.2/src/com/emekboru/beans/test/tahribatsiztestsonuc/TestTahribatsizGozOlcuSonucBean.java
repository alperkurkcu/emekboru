/**
 * 
 */
package com.emekboru.beans.test.tahribatsiztestsonuc;

import java.awt.Color;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.test.tahribatsiz.TahribatsizTestOrderListBean;
import com.emekboru.jpa.BoruBoyutsalKontrolTolerans;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizFloroskopikSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizGozOlcuSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizHidrostatik;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizManuelUtSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizOtomatikUtSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizTorna;
import com.emekboru.jpaman.BoruBoyutsalKontrolToleransManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizGozOlcuSonucManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.FileOperations;
import com.emekboru.utils.ReportUtil;
import com.emekboru.utils.UtilInsCore;
import com.itextpdf.text.pdf.Barcode128;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testTahribatsizGozOlcuSonucBean")
@ViewScoped
public class TestTahribatsizGozOlcuSonucBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<TestTahribatsizGozOlcuSonuc> allTestTahribatsizGozOlcuSonucList = new ArrayList<TestTahribatsizGozOlcuSonuc>();

	private TestTahribatsizGozOlcuSonuc testTahribatsizGozOlcuSonucForm = new TestTahribatsizGozOlcuSonuc();

	private TestTahribatsizGozOlcuSonuc newGozOlcu = new TestTahribatsizGozOlcuSonuc();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private boolean visibleButtonRender;
	private boolean hidroControlRender = false;

	int index = 0;

	private Pipe selectedPipe = new Pipe();

	private String barkodNo = null;

	private BoruBoyutsalKontrolTolerans boyutsalKontrol = new BoruBoyutsalKontrolTolerans();

	private Boolean kazikRender = false;

	BigDecimal binSayisi = new BigDecimal("1000");

	public TestTahribatsizGozOlcuSonucBean() {

		visibleButtonRender = false;
	}

	public void addOrUpdate(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmTestId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		TestTahribatsizGozOlcuSonucManager formManager = new TestTahribatsizGozOlcuSonucManager();

		if (testTahribatsizGozOlcuSonucForm.getId() == null) {

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);

			testTahribatsizGozOlcuSonucForm.setTestId(prmTestId);
			testTahribatsizGozOlcuSonucForm.setPipe(pipe);
			testTahribatsizGozOlcuSonucForm.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			testTahribatsizGozOlcuSonucForm.setEkleyenEmployee(userBean
					.getUser());

			// göz ölçü ekleme ekleme
			formManager.enterNew(testTahribatsizGozOlcuSonucForm);

			this.testTahribatsizGozOlcuSonucForm = new TestTahribatsizGozOlcuSonuc();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"GÖZ ÖLÇÜ BAŞARIYLA EKLENMİŞTİR!"));

		} else {

			testTahribatsizGozOlcuSonucForm.setGuncellemeZamani(UtilInsCore
					.getTarihZaman());
			testTahribatsizGozOlcuSonucForm.setGuncelleyenEmployee(userBean
					.getUser());

			formManager.updateEntity(testTahribatsizGozOlcuSonucForm);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"GÖZ ÖLÇÜ BAŞARIYLA GÜNCELLENMİŞTİR!"));
		}

		fillTestList(prmPipeId);

		// sayfada barkoddan okutunca order item pipe seçimi olmadıgı için
		// sıkıntı cıkıyordu
		TahribatsizTestOrderListBean ttolb = new TahribatsizTestOrderListBean();
		ttolb.loadOrderDetails(selectedPipe.getSalesItem().getItemId());
	}

	public void fillTestList(Integer pipeId) {

		try {
			TestTahribatsizGozOlcuSonucManager manager = new TestTahribatsizGozOlcuSonucManager();
			allTestTahribatsizGozOlcuSonucList = manager
					.getAllTestTahribatsizGozOlcuSonuc(pipeId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void addNew() {
		testTahribatsizGozOlcuSonucForm = new TestTahribatsizGozOlcuSonuc();
		updateButtonRender = false;
	}

	public void gozOlcuListener(SelectEvent event) {
		updateButtonRender = true;
		visibleButtonRender = false;
		hidroKontrol();
	}

	public void hidroKontrol() {
		if (testTahribatsizGozOlcuSonucForm.getPipe()
				.getTestTahribatsizHidrostatik() != null) {
			hidroControlRender = false;
		} else {
			hidroControlRender = true;
		}
	}

	public void delete(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		TestTahribatsizGozOlcuSonucManager formManager = new TestTahribatsizGozOlcuSonucManager();

		if (testTahribatsizGozOlcuSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		formManager.delete(testTahribatsizGozOlcuSonucForm);
		testTahribatsizGozOlcuSonucForm = new TestTahribatsizGozOlcuSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");

		fillTestList(prmPipeId);
	}

	public void addFromManuel(
			List<TestTahribatsizManuelUtSonuc> allTestTahribatsizManuelUtSonucList,
			UserCredentialsBean userBean) {
		System.out.println("TestTahribatsizGozOlcuSonucBean.add()");

		TestTahribatsizGozOlcuSonucManager formManager = new TestTahribatsizGozOlcuSonucManager();

		try {

			while (allTestTahribatsizManuelUtSonucList.size() > index) {
				if (allTestTahribatsizManuelUtSonucList.get(index)
						.getTestTahribatsizHidrostatik() == null
						&& allTestTahribatsizManuelUtSonucList.get(index)
								.getDegerlendirme() != null
						&& (allTestTahribatsizManuelUtSonucList.get(index)
								.getDegerlendirme() == 1 || allTestTahribatsizManuelUtSonucList
								.get(index).getDegerlendirme() == 6)) {
					newGozOlcu.setResult(null);
					newGozOlcu.setEklemeZamani(UtilInsCore.getTarihZaman());
					newGozOlcu.setEkleyenEmployee(userBean.getUser());
					newGozOlcu
							.setTestTahribatsizManuelUtSonuc(allTestTahribatsizManuelUtSonucList
									.get(index));
					newGozOlcu.setPipe(allTestTahribatsizManuelUtSonucList.get(
							index).getPipe());
					formManager.enterNew(newGozOlcu);
					newGozOlcu = new TestTahribatsizGozOlcuSonuc();
					return;
				}
				index++;
			}

			FacesContextUtils.addInfoMessage("SubmitMessage");

		} catch (Exception e) {
			System.out
					.println("TestTahribatsizHidrostatikBean:" + e.toString());
		}
	}

	public void addFromFl(
			List<TestTahribatsizFloroskopikSonuc> allTestTahribatsizFloroskopikSonucList,
			UserCredentialsBean userBean) {
		System.out.println("TestTahribatsizGozOlcuSonucBean.add()");

		TestTahribatsizGozOlcuSonucManager formManager = new TestTahribatsizGozOlcuSonucManager();

		try {

			while (allTestTahribatsizFloroskopikSonucList.size() > index) {
				if (allTestTahribatsizFloroskopikSonucList.get(index)
						.getTestTahribatsizHidrostatik() == null
						&& allTestTahribatsizFloroskopikSonucList.get(index)
								.getResult() != null
						&& (allTestTahribatsizFloroskopikSonucList.get(index)
								.getResult() == 1 || allTestTahribatsizFloroskopikSonucList
								.get(index).getResult() == 6)) {
					newGozOlcu.setResult(null);
					newGozOlcu.setEklemeZamani(UtilInsCore.getTarihZaman());
					newGozOlcu.setEkleyenEmployee(userBean.getUser());
					newGozOlcu
							.setTestTahribatsizFloroskopikSonuc(allTestTahribatsizFloroskopikSonucList
									.get(index));
					newGozOlcu.setPipe(allTestTahribatsizFloroskopikSonucList
							.get(index).getPipe());
					formManager.enterNew(newGozOlcu);
					newGozOlcu = new TestTahribatsizGozOlcuSonuc();
					FacesContextUtils.addInfoMessage("SubmitMessage");
					return;
				}
				index++;
			}

		} catch (Exception e) {
			System.out
					.println("TestTahribatsizHidrostatikBean:" + e.toString());
		}
	}

	// UT eklenme kısmı
	public void addFromOtomatik(
			TestTahribatsizOtomatikUtSonuc testTahribatsizOtomatikUtSonuc,
			UserCredentialsBean userBean) {
		System.out.println("TestTahribatsizGozOlcuSonucBean.addFromOtomatik()");

		try {
			TestTahribatsizGozOlcuSonucManager formManager = new TestTahribatsizGozOlcuSonucManager();

			newGozOlcu.setResult(null);
			newGozOlcu.setEklemeZamani(UtilInsCore.getTarihZaman());
			newGozOlcu.setEkleyenEmployee(userBean.getUser());

			newGozOlcu.setPipe(testTahribatsizOtomatikUtSonuc.getPipe());
			formManager.enterNew(newGozOlcu);
			newGozOlcu = new TestTahribatsizGozOlcuSonuc();

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"OTOMATİK UT TARAFINDAN GÖZ ÖLÇÜ BAŞARIYLA EKLENMİŞTİR!"));

		} catch (Exception e) {
			System.out
					.println("TestTahribatsizGozOlcuSonucBean.addFromOtomatik():"
							+ e.toString());
		}
	}

	// tornadan eklenme kısmı
	public void addFromTorna(TestTahribatsizTorna testTahribatsizTorna,
			UserCredentialsBean userBean) {
		System.out.println("TestTahribatsizGozOlcuSonucBean.addFromTorna()");

		try {
			TestTahribatsizGozOlcuSonucManager formManager = new TestTahribatsizGozOlcuSonucManager();

			if (formManager.getAllTestTahribatsizGozOlcuSonuc(
					testTahribatsizTorna.getPipe().getPipeId()).size() > 0) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"ÖNCEDEN EKLENMİŞ GÖ VARDIR!"));
			} else {

				newGozOlcu.setResult(null);
				newGozOlcu.setEklemeZamani(UtilInsCore.getTarihZaman());
				newGozOlcu.setEkleyenEmployee(userBean.getUser());

				newGozOlcu.setPipe(testTahribatsizTorna.getPipe());
				formManager.enterNew(newGozOlcu);
				newGozOlcu = new TestTahribatsizGozOlcuSonuc();

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"TORNA TARAFINDAN GÖZ ÖLÇÜ BAŞARIYLA EKLENMİŞTİR!"));
			}

		} catch (Exception e) {
			System.out
					.println("TestTahribatsizGozOlcuSonucBean.addFromTorna():"
							+ e.toString());
		}
	}

	// hidro tarafından eklenme
	public void addFromHidro(
			TestTahribatsizHidrostatik testTahribatsizHidrostatik,
			UserCredentialsBean userBean) {
		System.out.println("TestTahribatsizGozOlcuSonucBean.addFromHidro()");

		try {
			TestTahribatsizGozOlcuSonucManager formManager = new TestTahribatsizGozOlcuSonucManager();

			if (formManager.getAllTestTahribatsizGozOlcuSonuc(
					testTahribatsizHidrostatik.getPipe().getPipeId()).size() > 0) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"ÖNCEDEN EKLENMİŞ GÖ VARDIR!"));
			} else {

				newGozOlcu.setResult(null);
				newGozOlcu.setEklemeZamani(UtilInsCore.getTarihZaman());
				newGozOlcu.setEkleyenEmployee(userBean.getUser());

				newGozOlcu.setPipe(testTahribatsizHidrostatik.getPipe());
				formManager.enterNew(newGozOlcu);
				newGozOlcu = new TestTahribatsizGozOlcuSonuc();

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								"HİDROSTATİK TEST TARAFINDAN GÖZ ÖLÇÜ BAŞARIYLA EKLENMİŞTİR!"));
			}

		} catch (Exception e) {
			System.out.println("TestTahribatsizTornaBean:" + e.toString());
		}
	}

	// hidro tarafından eklenme
	public void stokBoruGozOlcuEkle(Pipe pipe, UserCredentialsBean userBean) {
		System.out.println("TestTahribatsizGozOlcuSonucBean.addFromHidro()");

		try {
			TestTahribatsizGozOlcuSonucManager formManager = new TestTahribatsizGozOlcuSonucManager();

			if (formManager.getAllTestTahribatsizGozOlcuSonuc(pipe.getPipeId())
					.size() > 0) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"ÖNCEDEN EKLENMİŞ GÖ VARDIR!"));
			} else {

				newGozOlcu.setResult(null);
				newGozOlcu.setEklemeZamani(UtilInsCore.getTarihZaman());
				newGozOlcu.setEkleyenEmployee(userBean.getUser());

				newGozOlcu.setPipe(pipe);
				formManager.enterNew(newGozOlcu);
				newGozOlcu = new TestTahribatsizGozOlcuSonuc();

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								"HİDROSTATİK TEST TARAFINDAN STOK BORU İÇİN GÖZ ÖLÇÜ BAŞARIYLA EKLENMİŞTİR!"));
			}

		} catch (Exception e) {
			System.out.println("TestTahribatsizTornaBean:" + e.toString());
		}
	}

	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_WARN, prmBarkod
							+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!", null));
			visibleButtonRender = false;
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			TestTahribatsizGozOlcuSonucManager gozManager = new TestTahribatsizGozOlcuSonucManager();
			allTestTahribatsizGozOlcuSonucList = gozManager
					.getAllTestTahribatsizGozOlcuSonuc(selectedPipe.getPipeId());

			if (selectedPipe.getSalesItem().getPipeType()
					.equals("Kazık Borusu")) {
				kazikRender = true;
			} else {
				kazikRender = false;
			}

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
			visibleButtonRender = true;
		}
	}

	public void kaliteGoster() {

		try {
			BoruBoyutsalKontrolToleransManager bbktm = new BoruBoyutsalKontrolToleransManager();
			boyutsalKontrol = bbktm.seciliBoyutsalKontrol(
					selectedPipe.getSalesItem().getItemId()).get(0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_WARN, "ÖNCE BARKOD OKUTUNUZ!", null));
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void tanapBarkoduBas() {

		if (selectedPipe == null || selectedPipe.getPipeId() == null
				|| selectedPipe.getPipeId() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		ReportUtil reportUtil = new ReportUtil();

		Barcode128 code128 = new Barcode128();
		code128.setCode(selectedPipe.getPipeBarkodNo());
		code128.setChecksumText(true);
		code128.setGenerateChecksum(true);
		code128.setStartStopText(true);

		java.awt.Image image = code128.createAwtImage(Color.BLACK, Color.WHITE);

		try {
			HSSFWorkbook hssfWorkbook = reportUtil
					.getWorkbookFromFilePath("/reportformats/barcode/tanapBarcode.xls");

			reportUtil.insertImageToWorkbook(hssfWorkbook, image, "testImage",
					image.getWidth(null) * 2, image.getHeight(null) * 5);

			reportUtil.writeWorkbookToFile(
					hssfWorkbook,
					"/reportformats/barcode/tanapBarcode-"
							+ selectedPipe.getPipeIndex() + ".xls");
		} catch (IOException e1) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"BARKOD İMAJI OLUŞTURULAMADI, HATA!", null));
			e1.printStackTrace();
		}

		try {

			Map dataMap = new HashMap();
			dataMap.put("barcodeInfo", selectedPipe);
			dataMap.put(
					"boruUzunluk",
					selectedPipe.getTestTahribatsizGozOlcuSonuc()
							.getBoruUzunluk()
							.divide(binSayisi, 2, RoundingMode.HALF_UP));

			dataMap.put("kaplama", "Bare");
			dataMap.put("nominalEtKalinligi", selectedPipe.getSalesItem()
					.getThickness());

			reportUtil.jxlsExportAsXls(
					dataMap,
					"/reportformats/barcode/tanapBarcode-"
							+ selectedPipe.getPipeIndex() + ".xls", "");

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"BARKOD BAŞARIYLA OLUŞTURULDU!", null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"BARKOD EXCELİ OLUŞTURULAMADI, HATA!", null));
			e.printStackTrace();
		}

	}

	public void barcodeYazdir() {

		try {
			FileOperations.getInstance().streamFile(
					FacesContext
							.getCurrentInstance()
							.getExternalContext()
							.getRealPath(
									"/reportformats/barcode/tanapBarcode-"
											+ selectedPipe.getPipeIndex()
											+ ".xls"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// setters getters

	public List<TestTahribatsizGozOlcuSonuc> getAllTestTahribatsizGozOlcuSonucList() {
		return allTestTahribatsizGozOlcuSonucList;
	}

	public void setAllTestTahribatsizGozOlcuSonucList(
			List<TestTahribatsizGozOlcuSonuc> allTestTahribatsizGozOlcuSonucList) {
		this.allTestTahribatsizGozOlcuSonucList = allTestTahribatsizGozOlcuSonucList;
	}

	public TestTahribatsizGozOlcuSonuc getTestTestTahribatsizGozOlcuSonucForm() {
		return testTahribatsizGozOlcuSonucForm;
	}

	public void setTestTestTahribatsizGozOlcuSonucForm(
			TestTahribatsizGozOlcuSonuc testTestTahribatsizGozOlcuSonucForm) {
		this.testTahribatsizGozOlcuSonucForm = testTestTahribatsizGozOlcuSonucForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public TestTahribatsizGozOlcuSonuc getTestTahribatsizGozOlcuSonucForm() {
		return testTahribatsizGozOlcuSonucForm;
	}

	public void setTestTahribatsizGozOlcuSonucForm(
			TestTahribatsizGozOlcuSonuc testTahribatsizGozOlcuSonucForm) {
		this.testTahribatsizGozOlcuSonucForm = testTahribatsizGozOlcuSonucForm;
	}

	public TestTahribatsizGozOlcuSonuc getNewGozOlcu() {
		return newGozOlcu;
	}

	public void setNewGozOlcu(TestTahribatsizGozOlcuSonuc newGozOlcu) {
		this.newGozOlcu = newGozOlcu;
	}

	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	public String getBarkodNo() {
		return barkodNo;
	}

	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	public BoruBoyutsalKontrolTolerans getBoyutsalKontrol() {
		return boyutsalKontrol;
	}

	public void setBoyutsalKontrol(BoruBoyutsalKontrolTolerans boyutsalKontrol) {
		this.boyutsalKontrol = boyutsalKontrol;
	}

	/**
	 * @return the kazikRender
	 */
	public Boolean getKazikRender() {
		return kazikRender;
	}

	/**
	 * @param kazikRender
	 *            the kazikRender to set
	 */
	public void setKazikRender(Boolean kazikRender) {
		this.kazikRender = kazikRender;
	}

	/**
	 * @return the visibleButtonRender
	 */
	public boolean isVisibleButtonRender() {
		return visibleButtonRender;
	}

	/**
	 * @param visibleButtonRender
	 *            the visibleButtonRender to set
	 */
	public void setVisibleButtonRender(boolean visibleButtonRender) {
		this.visibleButtonRender = visibleButtonRender;
	}

	/**
	 * @return the hidroControlRender
	 */
	public boolean isHidroControlRender() {
		return hidroControlRender;
	}

	/**
	 * @param hidroControlRender
	 *            the hidroControlRender to set
	 */
	public void setHidroControlRender(boolean hidroControlRender) {
		this.hidroControlRender = hidroControlRender;
	}

}
