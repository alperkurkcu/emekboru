package com.emekboru.beans.machine;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import com.emekboru.jpa.MaintenanceActivity;
import com.emekboru.jpaman.MaintenanceActivityManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "maintenanceActivityBean")
@ViewScoped
public class MaintenanceActivityBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private MaintenanceActivity selectedActivity;
	private MaintenanceActivity newActivity;

	public MaintenanceActivityBean() {
		selectedActivity = new MaintenanceActivity();
		newActivity = new MaintenanceActivity();
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void cancelMaintenanceActivitySubmittion(ActionEvent event) {
		newActivity = new MaintenanceActivity();
	}

	public void submitNewMaintenanceActivity(ActionEvent event) {
		System.out.println("MachineTypeBean.submitNewMachine()");

		MaintenanceActivityManager activityManager = new MaintenanceActivityManager();
		if (newActivity != null
				&& newActivity.getMaintenanceActivityId() != null) {
			activityManager.updateEntity(newActivity);
		} else {
			activityManager.enterNew(newActivity);
		}
		FacesContextUtils.addInfoMessage("SubmitMessage");

		// load new activity to datatable
		MachineUtilsListBean.loadMaintenanceActivitiesList();

		newActivity = new MaintenanceActivity();
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void updateMaintenanceActivity(ActionEvent event) {
		MaintenanceActivityManager activityManager = new MaintenanceActivityManager();
		if (selectedActivity != null
				&& selectedActivity.getMaintenanceActivityId() != null) {
			activityManager.updateEntity(selectedActivity);
		}

		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}

	public void deleteMaintenanceActivity(ActionEvent event) {

		MaintenanceActivityManager activityManager = new MaintenanceActivityManager();
		if (selectedActivity != null
				&& selectedActivity.getMaintenanceActivityId() != null) {
			activityManager.deleteByField(MaintenanceActivity.class,
					"maintenanceActivityId",
					selectedActivity.getMaintenanceActivityId());
		}

		FacesContextUtils.addWarnMessage("DeletedMessage");
		selectedActivity = new MaintenanceActivity();
		MachineUtilsListBean.setAllMaintenanceActivities();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public MaintenanceActivity getSelectedActivity() {
		return selectedActivity;
	}

	public void setSelectedActivity(MaintenanceActivity selectedActivity) {
		this.selectedActivity = selectedActivity;
	}

	public MaintenanceActivity getNewActivity() {
		return newActivity;
	}

	public void setNewActivity(MaintenanceActivity newActivity) {
		this.newActivity = newActivity;
	}

}
