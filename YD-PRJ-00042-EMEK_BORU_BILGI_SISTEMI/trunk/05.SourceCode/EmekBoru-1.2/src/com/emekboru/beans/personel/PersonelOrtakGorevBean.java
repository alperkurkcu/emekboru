/**
 * 
 */
package com.emekboru.beans.personel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.RowEditEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.personel.PersonelOrtakGorev;
import com.emekboru.jpaman.personel.PersonelOrtakGorevManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Rıdvan
 * 
 */
@ManagedBean(name = "personelOrtakGorevBean")
@ViewScoped
public class PersonelOrtakGorevBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private PersonelOrtakGorev personelOrtakGorevForm = new PersonelOrtakGorev();
	private List<PersonelOrtakGorev> allPersonelOrtakGorevList = new ArrayList<PersonelOrtakGorev>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	// private PersonelOrtakGorevManager perMen = new
	// PersonelOrtakGorevManager();

	public PersonelOrtakGorevBean() {

		fillTestList();
		// personelOrtakGorevForm = new PersonelOrtakGorev();
	}

	public void addOrUpdateGorevler(RowEditEvent event) {

		PersonelOrtakGorevManager perMen = new PersonelOrtakGorevManager();

		if (((PersonelOrtakGorev) event.getObject()).getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		} else {

			((PersonelOrtakGorev) event.getObject())
					.setSonGuncellenmeTarihi(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			((PersonelOrtakGorev) event.getObject())
					.setKullaniciIdEkleyen(userBean.getUser().getId());
			perMen.updateEntity(((PersonelOrtakGorev) event.getObject()));
			FacesMessage msg = new FacesMessage("Görev Edited", "Success");

			FacesContext.getCurrentInstance().addMessage(null, msg);

		}
		fillTestList();
	}

	public void addGorev() {

		PersonelOrtakGorevManager perMen = new PersonelOrtakGorevManager();

		personelOrtakGorevForm.setEklenmeTarihi(new java.sql.Timestamp(
				new java.util.Date().getTime()));
		personelOrtakGorevForm.setKullaniciIdEkleyen((userBean.getUser()
				.getId()));
		perMen.enterNew(personelOrtakGorevForm);
		FacesMessage msg = new FacesMessage("Görev Eklendi", " ");
		FacesContext.getCurrentInstance().addMessage(null, msg);
		personelOrtakGorevForm = new PersonelOrtakGorev();
		fillTestList();
	}

	public void fillTestList() {

		PersonelOrtakGorevManager perMen = new PersonelOrtakGorevManager();

		allPersonelOrtakGorevList = perMen.findAllOrderBy(
				PersonelOrtakGorev.class, "gorevAdi");
	}

	public void deleteGorevler() {

		PersonelOrtakGorevManager perMen = new PersonelOrtakGorevManager();

		if (personelOrtakGorevForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		perMen.deleteByField(PersonelOrtakGorev.class, "id",
				personelOrtakGorevForm.getId());
		FacesContextUtils.addWarnMessage("DeletedMessage");

		fillTestList();
	}

	// setters getters

	/**
	 * @return the personelOrtakGorevForm
	 */
	public PersonelOrtakGorev getPersonelOrtakGorevForm() {
		return personelOrtakGorevForm;
	}

	/**
	 * @param personelOrtakGorevForm
	 *            the personelOrtakGorevForm to set
	 */
	public void setPersonelOrtakGorevForm(
			PersonelOrtakGorev personelOrtakGorevForm) {
		this.personelOrtakGorevForm = personelOrtakGorevForm;
	}

	/**
	 * @return the allPersonelOrtakGorevList
	 */
	public List<PersonelOrtakGorev> getAllPersonelOrtakGorevList() {
		return allPersonelOrtakGorevList;
	}

	/**
	 * @param allPersonelOrtakGorevList
	 *            the allPersonelOrtakGorevList to set
	 */
	public void setAllPersonelOrtakGorevList(
			List<PersonelOrtakGorev> allPersonelOrtakGorevList) {
		this.allPersonelOrtakGorevList = allPersonelOrtakGorevList;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

}
