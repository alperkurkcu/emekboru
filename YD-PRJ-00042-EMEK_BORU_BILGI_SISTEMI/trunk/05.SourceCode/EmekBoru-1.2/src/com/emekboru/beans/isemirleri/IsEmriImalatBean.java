/**
 * 
 */
package com.emekboru.beans.isemirleri;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import com.emekboru.beans.istakipformu.IsTakipFormuImalatBean;
import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.isemri.IsEmriImalat;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.CoatRawMaterialManager;
import com.emekboru.jpaman.isemirleri.IsEmriImalatManager;
import com.emekboru.jpaman.sales.order.SalesItemManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isEmriImalatBean")
@ViewScoped
public class IsEmriImalatBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private IsEmriImalat isEmriImalatForm = new IsEmriImalat();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	@ManagedProperty(value = "#{isTakipFormuImalatBean}")
	private IsTakipFormuImalatBean isTakipFormuImalatBean;

	private List<CoatRawMaterial> allImalatMalzemeList = new ArrayList<CoatRawMaterial>();

	public IsEmriImalatBean() {

	}

	public void addOrUpdateIsEmriImalat(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmItemId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		SalesItem salesItem = new SalesItemManager().loadObject(
				SalesItem.class, prmItemId);

		IsEmriImalatManager isMan = new IsEmriImalatManager();

		if (isEmriImalatForm.getId() == null) {

			isEmriImalatForm.setSalesItem(salesItem);
			isEmriImalatForm.setEklemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			isEmriImalatForm.setEkleyenEmployee(userBean.getUser());

			isMan.enterNew(isEmriImalatForm);

			// imalat iş takip formu ekleme
			isTakipFormuImalatBean.addFromIsEmri(salesItem);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"İMALAT İŞ EMRİ BAŞARIYLA EKLENMİŞTİR!"));
		} else {

			isEmriImalatForm.setGuncellemeZamani(new java.sql.Timestamp(
					new java.util.Date().getTime()));
			isEmriImalatForm.setGuncelleyenEmployee(userBean.getUser());
			isMan.updateEntity(isEmriImalatForm);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_WARN,
					"İMALAT İŞ EMRİ BAŞARIYLA GÜNCELLENMİŞTİR!", null));
		}
	}

	public void deleteIsEmriImalat() {

		IsEmriImalatManager isMan = new IsEmriImalatManager();

		if (isEmriImalatForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		isMan.delete(isEmriImalatForm);
		isEmriImalatForm = new IsEmriImalat();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
				"İMALAT İŞ EMRİ BAŞARIYLA SİLİNMİŞTİR!", null));
	}

	@SuppressWarnings("static-access")
	public void loadIsEmri(Integer itemId) {

		IsEmriImalatManager isMan = new IsEmriImalatManager();

		if (isMan.getAllIsEmriImalat(itemId).size() > 0) {
			isEmriImalatForm = new IsEmriImalatManager().getAllIsEmriImalat(
					itemId).get(0);
		} else {
			isEmriImalatForm = new IsEmriImalat();
		}
	}

	public void isEmriMalzemeEkle() {

		IsEmriImalatManager isMan = new IsEmriImalatManager();

		isMan.updateEntity(isEmriImalatForm);
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
				"İMALAT MALZEMESİ BAŞARIYLA EKLENMİŞTİR!", null));
	}

	public void loadImalatMalzemeleri() {

		CoatRawMaterialManager materialMan = new CoatRawMaterialManager();
		// 1001 kumlama turudur - coat_material_type tablosundan
		allImalatMalzemeList = materialMan
				.findUnfinishedCoatMaterialByType(1001);
	}

	// getters setters

	/**
	 * @return the isEmriImalatForm
	 */
	public IsEmriImalat getIsEmriImalatForm() {
		return isEmriImalatForm;
	}

	/**
	 * @param isEmriImalatForm
	 *            the isEmriImalatForm to set
	 */
	public void setIsEmriImalatForm(IsEmriImalat isEmriImalatForm) {
		this.isEmriImalatForm = isEmriImalatForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the isTakipFormuImalatBean
	 */
	public IsTakipFormuImalatBean getIsTakipFormuImalatBean() {
		return isTakipFormuImalatBean;
	}

	/**
	 * @param isTakipFormuImalatBean
	 *            the isTakipFormuImalatBean to set
	 */
	public void setIsTakipFormuImalatBean(
			IsTakipFormuImalatBean isTakipFormuImalatBean) {
		this.isTakipFormuImalatBean = isTakipFormuImalatBean;
	}

	/**
	 * @return the allImalatMalzemeList
	 */
	public List<CoatRawMaterial> getAllImalatMalzemeList() {
		return allImalatMalzemeList;
	}

	/**
	 * @param allImalatMalzemeList
	 *            the allImalatMalzemeList to set
	 */
	public void setAllImalatMalzemeList(
			List<CoatRawMaterial> allImalatMalzemeList) {
		this.allImalatMalzemeList = allImalatMalzemeList;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
