package com.emekboru.beans.rulo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.config.ConfigBean;
import com.emekboru.config.Materials.Statuses;
import com.emekboru.jpa.rulo.Rulo;
import com.emekboru.jpaman.rulo.RuloManager;

@ManagedBean(name = "availRuloBean")
@ViewScoped
public class AvailRuloBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
	private ConfigBean config;

	public List<Rulo> availableRulos;

	public AvailRuloBean() {

		availableRulos = new ArrayList<Rulo>(0);
	}

	public void loadAvailableRulos() {
		/*
		 * availableRulos.clear(); RuloManager man = new RuloManager();
		 * ArrayList<WhereClauseArgs> conditionList = new
		 * ArrayList<WhereClauseArgs>(); WhereClauseArgs arg1 = new
		 * WhereClauseArgs.Builder() .setFieldName("remainingAmount")
		 * .setComparativeClause(JpqlComparativeClauses.NQ_GREATER_THEN)
		 * .setValue(0).build(); conditionList.add(arg1); List<Rulo> list =
		 * man.selectFromWhereQuerie(Rulo.class, conditionList); for (Rulo c :
		 * list) {
		 * 
		 * if (isAvailable(c)) availableRulos.add(c); }
		 */

		availableRulos.clear();
		RuloManager man = new RuloManager();
		List<Rulo> list = man.getAvailableRulos();
		for (Rulo c : list) {

			// if (isAvailable(c))
			availableRulos.add(c);
		}

		// availableRulos.addAll(list);

	}

	// Coil is available if its attribute Available is true
	// its not depleted and not sold
	protected boolean isAvailable(Rulo rulo) {

		Statuses s = rulo.getStatus(config.getConfig());

		if (s.isAvailable() && !s.isDepleted() && !s.isSold())
			return true;

		return false;
	}

	// *****************************************************************************//
	// *************************** GETTERS AND SETTERS
	// ****************************//
	// *****************************************************************************//
	public List<Rulo> getAvailableRulos() {
		return availableRulos;
	}

	public void setAvailableRulos(List<Rulo> availableRulos) {
		this.availableRulos = availableRulos;
	}

}
