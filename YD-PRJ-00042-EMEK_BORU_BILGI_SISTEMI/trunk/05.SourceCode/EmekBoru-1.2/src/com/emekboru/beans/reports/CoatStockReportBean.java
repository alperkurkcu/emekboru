package com.emekboru.beans.reports;

import java.io.Serializable;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "coatStockReportBean")
@ViewScoped
public class CoatStockReportBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String language;
	private String materialName;
	private Integer orderBy;
	private Boolean sortingOrder;
	private Date startDate;
	private Date endDate;
	private String coatTypeName;

	public CoatStockReportBean() {

	}

	/*
	 * GETTERS AND SETTERS
	 */

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getMaterialName() {
		return materialName;
	}

	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}

	public Integer getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(Integer orderBy) {
		this.orderBy = orderBy;
	}

	public Boolean getSortingOrder() {
		return sortingOrder;
	}

	public void setSortingOrder(Boolean sortingOrder) {
		this.sortingOrder = sortingOrder;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getCoatTypeName() {
		return coatTypeName;
	}

	public void setCoatTypeName(String coatTypeName) {
		this.coatTypeName = coatTypeName;
	}
}
