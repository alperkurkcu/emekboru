/**
 * 
 */
package com.emekboru.beans.satinalma;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.satinalma.SatinalmaTeklifIsteme;
import com.emekboru.jpa.satinalma.SatinalmaTeklifIstemeIcerik;
import com.emekboru.jpaman.satinalma.SatinalmaTeklifIstemeIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaTeklifIstemeManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "satinalmaTeklifIstemeIcerikBean")
@ViewScoped
public class SatinalmaTeklifIstemeIcerikBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<SatinalmaTeklifIstemeIcerik> allSatinalmaTeklifIstemeIcerikList = new ArrayList<SatinalmaTeklifIstemeIcerik>();

	private SatinalmaTeklifIstemeIcerik satinalmaTeklifIstemeIcerikForm = new SatinalmaTeklifIstemeIcerik();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	int siraNo = 1;

	double toplam = 0.0;

	double toplamKdv = 0.0;

	double toplamTutar = 0.0;

	double kdv = 0.0;

	NumberFormat formatter = new DecimalFormat("#0.00");

	private SatinalmaTeklifIsteme satinalmaTeklifIsteme = new SatinalmaTeklifIsteme();

	public SatinalmaTeklifIstemeIcerikBean() {

		updateButtonRender = false;
	}

	public void addIcerik() {

		satinalmaTeklifIstemeIcerikForm = new SatinalmaTeklifIstemeIcerik();
	}

	public void addOrUpdateIcerik(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmFormId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		siraNoBul(prmFormId);

		double tutar = 0;

		SatinalmaTeklifIstemeManager manager = new SatinalmaTeklifIstemeManager();
		SatinalmaTeklifIstemeIcerikManager formManager = new SatinalmaTeklifIstemeIcerikManager();

		if (satinalmaTeklifIstemeIcerikForm.getId() == null) {

			satinalmaTeklifIstemeIcerikForm.setSatinalmaTeklifIsteme(manager
					.findByField(SatinalmaTeklifIsteme.class, "id", prmFormId)
					.get(0));

			satinalmaTeklifIstemeIcerikForm.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			satinalmaTeklifIstemeIcerikForm.setEkleyenKullanici(userBean
					.getUser().getId());

			satinalmaTeklifIstemeIcerikForm.setSiraNo(siraNo);

			tutar = Integer.parseInt(satinalmaTeklifIstemeIcerikForm
					.getMiktar())
					* satinalmaTeklifIstemeIcerikForm.getBirimFiyat();

			satinalmaTeklifIstemeIcerikForm.setTutar(tutar);

			satinalmaTeklifIstemeIcerikForm.setToplam((int) toplam);

			satinalmaTeklifIstemeIcerikForm.setToplamTutar((int) toplamTutar);

			formManager.enterNew(satinalmaTeklifIstemeIcerikForm);

			this.satinalmaTeklifIstemeIcerikForm = new SatinalmaTeklifIstemeIcerik();
			FacesContextUtils.addInfoMessage("FormSubmitMessage");

		} else {

			satinalmaTeklifIstemeIcerikForm.setGuncellemeZamani(UtilInsCore
					.getTarihZaman());
			satinalmaTeklifIstemeIcerikForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			tutar = Integer.parseInt(satinalmaTeklifIstemeIcerikForm
					.getMiktar())
					* satinalmaTeklifIstemeIcerikForm.getBirimFiyat();

			satinalmaTeklifIstemeIcerikForm.setTutar(tutar);

			satinalmaTeklifIstemeIcerikForm.setToplam((int) toplam);

			satinalmaTeklifIstemeIcerikForm.setToplamTutar((int) toplamTutar);
			formManager.updateEntity(satinalmaTeklifIstemeIcerikForm);

			FacesContextUtils.addInfoMessage("FormUpdateMessage");
		}

		fillTestList(prmFormId);
	}

	public void deleteForm(ActionEvent e) {

		if (satinalmaTeklifIstemeIcerikForm.getId() == null) {

			FacesContextUtils.addWarnMessage("NoSelectMessage");
			return;
		}

		SatinalmaTeklifIstemeIcerikManager formManager = new SatinalmaTeklifIstemeIcerikManager();

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmFormId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");
		formManager.delete(satinalmaTeklifIstemeIcerikForm);
		fillTestList(prmFormId);
		FacesContextUtils.addWarnMessage("TestDeleteMessage");
	}

	public void fillTestList(Integer formId) {

		SatinalmaTeklifIstemeIcerikManager manager = new SatinalmaTeklifIstemeIcerikManager();
		allSatinalmaTeklifIstemeIcerikList = manager
				.getAllSatinalmaTeklifIstemeIcerik(formId);

		toplam(formId);

		// satinalmaTeklifIsteme = allSatinalmaTeklifIstemeIcerikList.get(0)
		// .getSatinalmaTeklifIsteme();
	}

	public void formListener(SelectEvent event) {

		updateButtonRender = true;
	}

	private void siraNoBul(int formId) {

		SatinalmaTeklifIstemeIcerikManager formManager = new SatinalmaTeklifIstemeIcerikManager();

		if (formManager.findByField(SatinalmaTeklifIstemeIcerik.class,
				"satinalmaTeklifIsteme.id", formId) != null) {
			siraNo = formManager.findByField(SatinalmaTeklifIstemeIcerik.class,
					"satinalmaTeklifIsteme.id", formId).size() + 1;
		}
	}

	private void toplam(int formId) {

		toplam = 0.0;
		toplamKdv = 0.0;
		toplamTutar = 0.0;

		SatinalmaTeklifIstemeIcerikManager formManager = new SatinalmaTeklifIstemeIcerikManager();

		List<SatinalmaTeklifIstemeIcerik> List = formManager.findByField(
				SatinalmaTeklifIstemeIcerik.class, "satinalmaTeklifIsteme.id",
				formId);

		for (int i = 0; i < List.size(); i++) {

			if (String.valueOf(List.get(i).getTutar()) != null) {
				toplam = toplam + (double) List.get(i).getTutar();
			}
			kdv = (double) List.get(i).getKdv();
		}

		toplamKdv = toplam * (kdv * 0.01);
		toplamTutar = toplam + toplamKdv;

		formatter.format(toplamKdv);
		formatter.format(toplamTutar);
	}

	// setters getters

	public List<SatinalmaTeklifIstemeIcerik> getAllSatinalmaTeklifIstemeIcerikList() {
		return allSatinalmaTeklifIstemeIcerikList;
	}

	public void setAllSatinalmaTeklifIstemeIcerikList(
			List<SatinalmaTeklifIstemeIcerik> allSatinalmaTeklifIstemeIcerikList) {
		this.allSatinalmaTeklifIstemeIcerikList = allSatinalmaTeklifIstemeIcerikList;
	}

	public SatinalmaTeklifIstemeIcerik getSatinalmaTeklifIstemeIcerikForm() {
		return satinalmaTeklifIstemeIcerikForm;
	}

	public void setSatinalmaTeklifIstemeIcerikForm(
			SatinalmaTeklifIstemeIcerik satinalmaTeklifIstemeIcerikForm) {
		this.satinalmaTeklifIstemeIcerikForm = satinalmaTeklifIstemeIcerikForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public double getToplam() {
		return toplam;
	}

	public void setToplam(double toplam) {
		this.toplam = toplam;
	}

	public double getToplamKdv() {
		return toplamKdv;
	}

	public void setToplamKdv(double toplamKdv) {
		this.toplamKdv = toplamKdv;
	}

	public double getToplamTutar() {
		return toplamTutar;
	}

	public void setToplamTutar(double toplamTutar) {
		this.toplamTutar = toplamTutar;
	}

	public double getKdv() {
		return kdv;
	}

	public void setKdv(double kdv) {
		this.kdv = kdv;
	}

	/**
	 * @return the satinalmaTeklifIsteme
	 */
	public SatinalmaTeklifIsteme getSatinalmaTeklifIsteme() {
		return satinalmaTeklifIsteme;
	}

	/**
	 * @param satinalmaTeklifIsteme
	 *            the satinalmaTeklifIsteme to set
	 */
	public void setSatinalmaTeklifIsteme(
			SatinalmaTeklifIsteme satinalmaTeklifIsteme) {
		this.satinalmaTeklifIsteme = satinalmaTeklifIsteme;
	}

}
