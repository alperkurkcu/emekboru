/**
 * 
 */
package com.emekboru.beans.istakipformu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.istakipformu.IsTakipFormuEpoksiKaplama;
import com.emekboru.jpa.istakipformu.IsTakipFormuEpoksiKaplamaDevamIslemler;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.istakipformu.IsTakipFormuEpoksiKaplamaDevamIslemlerManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isTakipFormuEpoksiKaplamaDevamIslemlerBean")
@ViewScoped
public class IsTakipFormuEpoksiKaplamaDevamIslemlerBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<IsTakipFormuEpoksiKaplamaDevamIslemler> allIsTakipFormuEpoksiKaplamaDevamIslemlerList = new ArrayList<IsTakipFormuEpoksiKaplamaDevamIslemler>();
	private IsTakipFormuEpoksiKaplamaDevamIslemler isTakipFormuEpoksiKaplamaDevamIslemlerForm = new IsTakipFormuEpoksiKaplamaDevamIslemler();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private boolean buttonRender;

	private Pipe selectedPipe = new Pipe();

	private String barkodNo = null;

	private SalesItem selectedSalesItem = new SalesItem();
	private IsTakipFormuEpoksiKaplama selectedIsTakipFormuKaplama = new IsTakipFormuEpoksiKaplama();

	Integer salesItemId = null;

	public IsTakipFormuEpoksiKaplamaDevamIslemlerBean() {

	}

	public void reset() {

		isTakipFormuEpoksiKaplamaDevamIslemlerForm = new IsTakipFormuEpoksiKaplamaDevamIslemler();
		updateButtonRender = false;
		buttonRender = false;
	};

	public void addOrUpdateFormSonuc() {

		IsTakipFormuEpoksiKaplamaDevamIslemlerManager devamManager = new IsTakipFormuEpoksiKaplamaDevamIslemlerManager();

		if (isTakipFormuEpoksiKaplamaDevamIslemlerForm.getId() == null) {

			try {

				isTakipFormuEpoksiKaplamaDevamIslemlerForm
						.setEklemeZamani(UtilInsCore.getTarihZaman());
				isTakipFormuEpoksiKaplamaDevamIslemlerForm
						.setEkleyenEmployee(userBean.getUser());
				isTakipFormuEpoksiKaplamaDevamIslemlerForm
						.setPipe(selectedPipe);
				isTakipFormuEpoksiKaplamaDevamIslemlerForm
						.setSalesItem(selectedPipe.getSalesItem());

				devamManager
						.enterNew(isTakipFormuEpoksiKaplamaDevamIslemlerForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"İÇ KUMLAMA İŞ TAKİP FORMU BAŞARIYLA EKLENMİŞTİR!"));
			} catch (Exception ex) {

				System.out
						.println("isTakipFormuKumlamaDevamIslemlerBean.addOrUpdateFormSonuc-HATA");
			}

		} else {

			try {

				isTakipFormuEpoksiKaplamaDevamIslemlerForm
						.setGuncellemeZamani(UtilInsCore.getTarihZaman());
				isTakipFormuEpoksiKaplamaDevamIslemlerForm
						.setGuncelleyenEmployee(userBean.getUser());
				devamManager
						.updateEntity(isTakipFormuEpoksiKaplamaDevamIslemlerForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"İÇ KUMLAMA İŞ TAKİP FORMU BAŞARIYLA GÜNCELLENMİŞTİR!"));
			} catch (Exception ex) {

				System.out
						.println("isTakipFormuKumlamaDevamIslemlerBean.addOrUpdateFormSonuc-HATA"
								+ ex.toString());
			}
		}
		fillTestList(selectedPipe.getSalesItem().getItemId());
	}

	@SuppressWarnings("static-access")
	public void fillTestList(Integer prmItemId) {

		IsTakipFormuEpoksiKaplamaDevamIslemlerManager formManager = new IsTakipFormuEpoksiKaplamaDevamIslemlerManager();
		allIsTakipFormuEpoksiKaplamaDevamIslemlerList = formManager
				.getAllIsTakipFormuEpoksiKaplamaDevamIslemler(prmItemId);
		buttonRender = true;
		salesItemId = prmItemId;
	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_WARN, prmBarkod
							+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!", null));
			return;
		} else {

			if (salesItemId != pipeMan.findByBarkodNo(prmBarkod).get(0)
					.getSalesItem().getItemId()) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_WARN,
								prmBarkod
										+ " BARKOD NUMARALI BORU, YANLIŞ SİPARİŞ BORUSUDUR, LÜTFEN KONTROL EDİNİZ!",
								null));
				return;
			}

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			IsTakipFormuEpoksiKaplamaDevamIslemlerManager formManager = new IsTakipFormuEpoksiKaplamaDevamIslemlerManager();
			allIsTakipFormuEpoksiKaplamaDevamIslemlerList = formManager
					.getAllIsTakipFormuEpoksiKaplamaDevamIslemler(selectedPipe
							.getSalesItem().getItemId());
			;
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
		}
	}

	public void deleteFormSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		IsTakipFormuEpoksiKaplamaDevamIslemlerManager formManager = new IsTakipFormuEpoksiKaplamaDevamIslemlerManager();

		if (isTakipFormuEpoksiKaplamaDevamIslemlerForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		formManager.delete(isTakipFormuEpoksiKaplamaDevamIslemlerForm);
		isTakipFormuEpoksiKaplamaDevamIslemlerForm = new IsTakipFormuEpoksiKaplamaDevamIslemler();
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"İÇ KUMLAMA İŞ TAKİP FORMU BAŞARIYLA SİLİNMİŞTİR!"));

		fillTestList(selectedPipe.getSalesItem().getItemId());
	}

	// setters getters

	public List<IsTakipFormuEpoksiKaplamaDevamIslemler> getAllIsTakipFormuEpoksiKaplamaDevamIslemlerList() {
		return allIsTakipFormuEpoksiKaplamaDevamIslemlerList;
	}

	public void setAllIsTakipFormuEpoksiKaplamaDevamIslemlerList(
			List<IsTakipFormuEpoksiKaplamaDevamIslemler> allIsTakipFormuEpoksiKaplamaDevamIslemlerList) {
		this.allIsTakipFormuEpoksiKaplamaDevamIslemlerList = allIsTakipFormuEpoksiKaplamaDevamIslemlerList;
	}

	public IsTakipFormuEpoksiKaplamaDevamIslemler getIsTakipFormuEpoksiKaplamaDevamIslemlerForm() {
		return isTakipFormuEpoksiKaplamaDevamIslemlerForm;
	}

	public void setIsTakipFormuEpoksiKaplamaDevamIslemlerForm(
			IsTakipFormuEpoksiKaplamaDevamIslemler isTakipFormuEpoksiKaplamaDevamIslemlerForm) {
		this.isTakipFormuEpoksiKaplamaDevamIslemlerForm = isTakipFormuEpoksiKaplamaDevamIslemlerForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public boolean isButtonRender() {
		return buttonRender;
	}

	public void setButtonRender(boolean buttonRender) {
		this.buttonRender = buttonRender;
	}

	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	public String getBarkodNo() {
		return barkodNo;
	}

	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	public SalesItem getSelectedSalesItem() {
		return selectedSalesItem;
	}

	public void setSelectedSalesItem(SalesItem selectedSalesItem) {
		this.selectedSalesItem = selectedSalesItem;
	}

	public IsTakipFormuEpoksiKaplama getSelectedIsTakipFormuKaplama() {
		return selectedIsTakipFormuKaplama;
	}

	public void setSelectedIsTakipFormuKaplama(
			IsTakipFormuEpoksiKaplama selectedIsTakipFormuKaplama) {
		this.selectedIsTakipFormuKaplama = selectedIsTakipFormuKaplama;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
