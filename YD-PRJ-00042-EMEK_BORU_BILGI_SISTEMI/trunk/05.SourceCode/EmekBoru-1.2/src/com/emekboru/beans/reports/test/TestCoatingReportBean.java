/**
 * 
 */
package com.emekboru.beans.reports.test;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaBetonGozKontrolSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaBetonYuzeyHazirligiSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaBuchholzSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaDeliciUcDirencSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaFlowCoatBendSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaFlowCoatBondSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaFlowCoatBuchholzSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaFlowCoatCureSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaFlowCoatFilmThicknessSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaFlowCoatPinholeSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaFlowCoatStrippingSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaFlowCoatWaterSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaGozKontrolSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaHolidayPinholeSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaKalinligiSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaKaplamasizBolgeSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaKatodikSoyulmaSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaKumlamaOrtamSartlariSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaMpqtKaplamaKalinligliSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaNihaiKontrolSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaOncesiYuzeyKontroluSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaOnlineHolidaySonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaProcessBozulmaSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaPulloffYapismaSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaSertlikSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaSonrasiYuzeyKontroluSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaTamirTestSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaTozBoyaSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaTozEpoksiXcutSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaUygulamaSicaklikSonuc;
import com.emekboru.jpa.kaplamatest.TestKaplamaYapismaSonuc;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaBetonGozKontrolSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaBetonYuzeyHazirligiSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaBuchholzSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaDeliciUcDirencSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaFlowCoatBendSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaFlowCoatBondSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaFlowCoatBuchholzSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaFlowCoatCureSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaFlowCoatFilmThicknessSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaFlowCoatPinholeSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaFlowCoatStrippingSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaFlowCoatWaterSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaGozKontrolSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaHolidayPinholeSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaKalinligiSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaKaplamasizBolgeSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaKatodikSoyulmaSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaKumlamaOrtamSartlariSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaKumlanmisBoruYuzeyTemizligiSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaMpqtKaplamaKalinligliSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaNihaiKontrolSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaOncesiYuzeyKontroluSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaOnlineHolidaySonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaProcessBozulmaSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaPulloffYapismaSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaSertlikSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaSonrasiYuzeyKontroluSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaTamirTestSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaTozBoyaSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaTozEpoksiXcutSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaUygulamaSicaklikSonucManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaYapismaSonucManager;
import com.emekboru.jpaman.sales.order.SalesItemManager;
import com.emekboru.utils.ReportUtil;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testCoatingReportBean")
@ViewScoped
public class TestCoatingReportBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	Integer raporNo = 0;
	String muayeneyiYapan = null;
	String hazirlayan = null;

	Integer raporVardiya;
	Date raporTarihi;
	String raporTarihiAString;
	String raporTarihiBString;

	Boolean kontrol = false;

	// private String downloadedFileName;
	// private StreamedContent downloadedFile;
	// private String realFilePath;

	@PostConstruct
	public void init() {
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr34ReportXlsx(Integer prmPipeId) { // Flowcoats

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : pipes.get(0)
					.getSalesItem().getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2153)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaFlowCoatBuchholzSonucManager buchholzManager = new TestKaplamaFlowCoatBuchholzSonucManager();
			TestKaplamaFlowCoatPinholeSonucManager pinholemanager = new TestKaplamaFlowCoatPinholeSonucManager();
			TestKaplamaFlowCoatFilmThicknessSonucManager filmManager = new TestKaplamaFlowCoatFilmThicknessSonucManager();
			TestKaplamaFlowCoatCureSonucManager cureManager = new TestKaplamaFlowCoatCureSonucManager();
			TestKaplamaFlowCoatBendSonucManager bendManager = new TestKaplamaFlowCoatBendSonucManager();
			TestKaplamaFlowCoatWaterSonucManager waterManager = new TestKaplamaFlowCoatWaterSonucManager();
			TestKaplamaFlowCoatBondSonucManager bondManager = new TestKaplamaFlowCoatBondSonucManager();
			TestKaplamaFlowCoatStrippingSonucManager strippingManager = new TestKaplamaFlowCoatStrippingSonucManager();

			List<TestKaplamaFlowCoatBuchholzSonuc> buchholzSonucs = new ArrayList<TestKaplamaFlowCoatBuchholzSonuc>();
			List<TestKaplamaFlowCoatPinholeSonuc> pinholeSonucs = new ArrayList<TestKaplamaFlowCoatPinholeSonuc>();
			List<TestKaplamaFlowCoatFilmThicknessSonuc> filmSonucs = new ArrayList<TestKaplamaFlowCoatFilmThicknessSonuc>();
			List<TestKaplamaFlowCoatCureSonuc> cureSonucs = new ArrayList<TestKaplamaFlowCoatCureSonuc>();
			List<TestKaplamaFlowCoatBendSonuc> bendSonucs = new ArrayList<TestKaplamaFlowCoatBendSonuc>();
			List<TestKaplamaFlowCoatWaterSonuc> waterSonucs = new ArrayList<TestKaplamaFlowCoatWaterSonuc>();
			List<TestKaplamaFlowCoatBondSonuc> bondSonucs = new ArrayList<TestKaplamaFlowCoatBondSonuc>();
			List<TestKaplamaFlowCoatStrippingSonuc> strippingSonucs = new ArrayList<TestKaplamaFlowCoatStrippingSonuc>();

			buchholzSonucs = buchholzManager
					.getAllTestKaplamaFlowCoatBuchholzSonuc(2153, prmPipeId);
			pinholeSonucs = pinholemanager
					.getAllTestKaplamaFlowCoatPinholeSonuc(2152, prmPipeId);
			filmSonucs = filmManager
					.getAllTestKaplamaFlowCoatFilmThicknessSonuc(2154,
							prmPipeId);
			cureSonucs = cureManager.getAllTestKaplamaFlowCoatCureSonuc(2155,
					prmPipeId);
			bendSonucs = bendManager.getAllTestKaplamaFlowCoatBendSonuc(2156,
					prmPipeId);
			waterSonucs = waterManager.getAllTestKaplamaFlowCoatWaterSonuc(
					2157, prmPipeId);
			bondSonucs = bondManager.getAllTestKaplamaFlowCoatBondSonuc(2158,
					prmPipeId);
			strippingSonucs = strippingManager
					.getAllTestKaplamaFlowCoatStrippingSonuc(2159, prmPipeId);

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm")
					.format(buchholzSonucs.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = buchholzSonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ buchholzSonucs.get(0).getEkleyenEmployee()
								.getEmployee().getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", pipes.get(0).getSalesItem());
			dataMap.put("specs", specs);
			dataMap.put("pipeNo", pipes.get(0).getPipeBarkodNo());
			dataMap.put("buchholzSonucs", buchholzSonucs);
			dataMap.put("pinholeSonucs", pinholeSonucs);
			dataMap.put("filmSonucs", filmSonucs);
			dataMap.put("cureSonucs", cureSonucs);
			dataMap.put("bendSonucs", bendSonucs);
			dataMap.put("waterSonucs", waterSonucs);
			dataMap.put("bondSonucs", bondSonucs);
			dataMap.put("strippingSonucs", strippingSonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-34.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr179ReportXlsx(Integer prmPipeId) {// LKT1

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2149)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaTamirTestSonucManager sonucManager = new TestKaplamaTamirTestSonucManager();
			List<TestKaplamaTamirTestSonuc> sonucs = new ArrayList<TestKaplamaTamirTestSonuc>();
			sonucs = sonucManager.getAllTestKaplamaTamirTestSonuc(2149,
					prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-179.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr180ReportXlsx(Integer prmPipeId, Integer prmGlobalId) {// CIT

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();

			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == prmGlobalId)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaKalinligiSonucManager sonucManager = new TestKaplamaKalinligiSonucManager();
			List<TestKaplamaKalinligiSonuc> sonucs = new ArrayList<TestKaplamaKalinligiSonuc>();
			sonucs = sonucManager.getAllKaplamaKalinligiSonucTest(prmGlobalId,
					prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-180.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr187ReportXlsx(Integer prmPipeId) {// CCT

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2014)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaMpqtKaplamaKalinligliSonucManager sonucManager = new TestKaplamaMpqtKaplamaKalinligliSonucManager();
			List<TestKaplamaMpqtKaplamaKalinligliSonuc> sonucs = new ArrayList<TestKaplamaMpqtKaplamaKalinligliSonuc>();
			sonucs = sonucManager.getAllTestKaplamaMpqtKaplamaKalinligliSonuc(
					2014, prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-187.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr190ReportXlsx(Integer prmPipeId) { // CHT2

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : pipes.get(0)
					.getSalesItem().getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2021)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaSertlikSonucManager sonucManager = new TestKaplamaSertlikSonucManager();
			List<TestKaplamaSertlikSonuc> sonucs = new ArrayList<TestKaplamaSertlikSonuc>();
			sonucs = sonucManager
					.getAllKaplamaSertlikSonucTest(2021, prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", pipes.get(0).getSalesItem());
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-190.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr208ReportXlsx(Integer prmPipeId) {// CCD1

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : pipes.get(0)
					.getSalesItem().getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2020)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaKatodikSoyulmaSonucManager sonucManager = new TestKaplamaKatodikSoyulmaSonucManager();
			List<TestKaplamaKatodikSoyulmaSonuc> sonucs = new ArrayList<TestKaplamaKatodikSoyulmaSonuc>();
			sonucs = sonucManager
					.getAllKatodikSoyulmaSonucTest(2020, prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", pipes.get(0).getSalesItem());
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("pipeNo", pipes.get(0).getPipeBarkodNo());
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-208.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr209ReportXlsx(Integer prmPipeId) {// CIT2

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : pipes.get(0)
					.getSalesItem().getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2018)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaDeliciUcDirencSonucManager sonucManager = new TestKaplamaDeliciUcDirencSonucManager();
			List<TestKaplamaDeliciUcDirencSonuc> sonucs = new ArrayList<TestKaplamaDeliciUcDirencSonuc>();
			sonucs = sonucManager
					.getAllDeliciUcDirencSonucTest(2018, prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", pipes.get(0).getSalesItem());
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-209.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr211ReportXlsx(Integer prmPipeId) {// bitmedi

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2130)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaKalinligiSonucManager sonucManager = new TestKaplamaKalinligiSonucManager();
			List<TestKaplamaKalinligiSonuc> sonucs = new ArrayList<TestKaplamaKalinligiSonuc>();
			sonucs = sonucManager.getAllKaplamaKalinligiSonucTest(2130,
					prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-211.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr231ReportXlsx(Integer prmPipeId) {// CDS1

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : pipes.get(0)
					.getSalesItem().getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2013)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaTozBoyaSonucManager sonucManager = new TestKaplamaTozBoyaSonucManager();
			List<TestKaplamaTozBoyaSonuc> sonucs = new ArrayList<TestKaplamaTozBoyaSonuc>();
			sonucs = sonucManager
					.getAllKaplamaTozBoyaSonucTest(2013, prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", pipes.get(0).getSalesItem());
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-231.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr235ReportXlsx(Integer prmPipeId) {// OHT

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2015)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaOnlineHolidaySonucManager sonucManager = new TestKaplamaOnlineHolidaySonucManager();
			List<TestKaplamaOnlineHolidaySonuc> sonucs = new ArrayList<TestKaplamaOnlineHolidaySonuc>();
			sonucs = sonucManager.getAllTestKaplamaOnlineHolidaySonucTest(2015,
					prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-235.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr236ReportXlsx(Integer prmPipeId) {// LAT

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2133)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaYapismaSonucManager sonucManager = new TestKaplamaYapismaSonucManager();
			List<TestKaplamaYapismaSonuc> sonucs = new ArrayList<TestKaplamaYapismaSonuc>();
			sonucs = sonucManager
					.getAllKaplamaYapismaSonucTest(2133, prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-236.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr237ReportXlsx(Integer prmPipeId) {// LCT

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2130)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaKalinligiSonucManager sonucManager = new TestKaplamaKalinligiSonucManager();
			List<TestKaplamaKalinligiSonuc> sonucs = new ArrayList<TestKaplamaKalinligiSonuc>();
			sonucs = sonucManager.getAllKaplamaKalinligiSonucTest(2130,
					prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-237.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr238ReportXlsx(Integer prmPipeId) {// BKK

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2145)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaSonrasiYuzeyKontroluSonucManager sonucManager = new TestKaplamaSonrasiYuzeyKontroluSonucManager();
			List<TestKaplamaSonrasiYuzeyKontroluSonuc> sonucs = new ArrayList<TestKaplamaSonrasiYuzeyKontroluSonuc>();
			sonucs = sonucManager.getAllTestKaplamaSonrasiYuzeyKontroluSonuc(
					2145, prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-238.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr239ReportXlsx(Integer prmPipeId) {// LBZ

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2139)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaBuchholzSonucManager sonucManager = new TestKaplamaBuchholzSonucManager();
			List<TestKaplamaBuchholzSonuc> sonucs = new ArrayList<TestKaplamaBuchholzSonuc>();
			sonucs = sonucManager.getAllTestKaplamaBuchholzSonuc(2139,
					prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-239.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr240ReportXlsx(Integer prmPipeId) {// CGK

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2035)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaGozKontrolSonucManager sonucManager = new TestKaplamaGozKontrolSonucManager();
			List<TestKaplamaGozKontrolSonuc> sonucs = new ArrayList<TestKaplamaGozKontrolSonuc>();
			sonucs = sonucManager.getAllTestKaplamaGozKontrolSonuc(prmPipeId,
					2035);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-240.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "static-access", "rawtypes", "unchecked" })
	public void fr246ReportXlsx(Integer prmPipeId, Integer prmGlobalId) { // LCS-CSC
		FacesContext context = FacesContext.getCurrentInstance();

		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == prmGlobalId)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaKumlanmisBoruYuzeyTemizligiSonucManager sonucManager = new TestKaplamaKumlanmisBoruYuzeyTemizligiSonucManager();
			List<TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc> sonucs = new ArrayList<TestKaplamaKumlanmisBoruYuzeyTemizligiSonuc>();
			sonucs = sonucManager
					.getAllTestKaplamaKumlanmisBoruYuzeyTemizligiSonuc(
							prmGlobalId, prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-246.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "static-access", "rawtypes", "unchecked" })
	public void fr248ReportXlsx(Integer prmPipeId, Integer prmGlobalId) {// LAC-CAC

		FacesContext context = FacesContext.getCurrentInstance();

		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == prmGlobalId)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaKumlamaOrtamSartlariSonucManager sonucManager = new TestKaplamaKumlamaOrtamSartlariSonucManager();
			List<TestKaplamaKumlamaOrtamSartlariSonuc> sonucs = new ArrayList<TestKaplamaKumlamaOrtamSartlariSonuc>();
			sonucs = sonucManager.getAllTestKaplamaKumlamaOrtamSartlariSonuc(
					prmGlobalId, prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			List<String> date1 = new ArrayList<String>();
			List<String> date2 = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));
			date1.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getKumlamaOncesiTarihSaat()));
			date2.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getKumlamaSonrasiTarihSaat()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("date1", date1);
			dataMap.put("date2", date2);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-248.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr258ReportXlsx(Integer prmPipeId) {// bitmedi -

		FacesContext context = FacesContext.getCurrentInstance();

		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2130)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaKalinligiSonucManager sonucManager = new TestKaplamaKalinligiSonucManager();
			List<TestKaplamaKalinligiSonuc> sonucs = new ArrayList<TestKaplamaKalinligiSonuc>();
			sonucs = sonucManager.findByItemIdSonuc(salesItems.get(0)
					.getItemId());

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-258.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr267ReportXlsx(Integer prmPipeId) {// bitmedi

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2130)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaKalinligiSonucManager sonucManager = new TestKaplamaKalinligiSonucManager();
			List<TestKaplamaKalinligiSonuc> sonucs = new ArrayList<TestKaplamaKalinligiSonuc>();
			sonucs = sonucManager.getAllKaplamaKalinligiSonucTest(2130,
					prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-267.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr293ReportXlsx(Integer prmPipeId) {// LSK1

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2022)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaOncesiYuzeyKontroluSonucManager sonucManager = new TestKaplamaOncesiYuzeyKontroluSonucManager();
			List<TestKaplamaOncesiYuzeyKontroluSonuc> sonucs = new ArrayList<TestKaplamaOncesiYuzeyKontroluSonuc>();
			sonucs = sonucManager.getAllTestKaplamaOncesiYuzeyKontroluSonuc(
					2005, prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-293.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr294ReportXlsx(Integer prmPipeId) {// CFI

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2022)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaNihaiKontrolSonucManager sonucManager = new TestKaplamaNihaiKontrolSonucManager();
			List<TestKaplamaNihaiKontrolSonuc> sonucs = new ArrayList<TestKaplamaNihaiKontrolSonuc>();
			sonucs = sonucManager.getAllTestKaplamaNihaiKontrolSonuc(2022,
					prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-294.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr302ReportXlsx(Integer prmPipeId, Integer prmGlobalId) {// LSP-CSP

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();

			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == prmGlobalId)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonucManager sonucManager = new TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonucManager();
			List<TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc> sonucs = new ArrayList<TestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc>();
			sonucs = sonucManager
					.getAllTestKaplamaKumlanmisBoruYuzeyPuruzluluguSonuc(
							prmGlobalId, prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-302.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr464ReportXlsx(Integer prmPipeId, Integer prmGlobalId) { // CPP1-CPP2

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : pipes.get(0)
					.getSalesItem().getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == prmGlobalId)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaProcessBozulmaSonucManager sonucManager = new TestKaplamaProcessBozulmaSonucManager();
			List<TestKaplamaProcessBozulmaSonuc> sonucs = new ArrayList<TestKaplamaProcessBozulmaSonuc>();
			sonucs = sonucManager.getAllTestKaplamaProcessBozulmaSonuc(
					prmGlobalId, prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", pipes.get(0).getSalesItem());
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-465.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr465ReportXlsx(Integer prmPipeId) {// bitmedi

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2130)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaKalinligiSonucManager sonucManager = new TestKaplamaKalinligiSonucManager();
			List<TestKaplamaKalinligiSonuc> sonucs = new ArrayList<TestKaplamaKalinligiSonuc>();
			sonucs = sonucManager.getAllKaplamaKalinligiSonucTest(2130,
					prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-465.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr466ReportXlsx(Integer prmPipeId) {// LSK

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2150)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaOncesiYuzeyKontroluSonucManager sonucManager = new TestKaplamaOncesiYuzeyKontroluSonucManager();
			List<TestKaplamaOncesiYuzeyKontroluSonuc> sonucs = new ArrayList<TestKaplamaOncesiYuzeyKontroluSonuc>();
			sonucs = sonucManager.getAllTestKaplamaOncesiYuzeyKontroluSonuc(
					2150, prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-466.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr468ReportXlsx(Integer prmPipeId) {// CKB

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2033)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaKaplamasizBolgeSonucManager sonucManager = new TestKaplamaKaplamasizBolgeSonucManager();
			List<TestKaplamaKaplamasizBolgeSonuc> sonucs = new ArrayList<TestKaplamaKaplamasizBolgeSonuc>();
			sonucs = sonucManager.getAllTestKaplamaKaplamasizBolgeSonuc(2033,
					prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-468.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr469ReportXlsx(Integer prmPipeId) {// LKB

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2146)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaKaplamasizBolgeSonucManager sonucManager = new TestKaplamaKaplamasizBolgeSonucManager();
			List<TestKaplamaKaplamasizBolgeSonuc> sonucs = new ArrayList<TestKaplamaKaplamasizBolgeSonuc>();
			sonucs = sonucManager.getAllTestKaplamaKaplamasizBolgeSonuc(2146,
					prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-469.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr472ReportXlsx(Integer prmPipeId) {// EXC

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2140)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaTozEpoksiXcutSonucManager sonucManager = new TestKaplamaTozEpoksiXcutSonucManager();
			List<TestKaplamaTozEpoksiXcutSonuc> sonucs = new ArrayList<TestKaplamaTozEpoksiXcutSonuc>();
			sonucs = sonucManager.getAllTestKaplamaTozEpoksiXcutSonuc(2140,
					prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-472.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr473ReportXlsx(Integer prmPipeId) {// CKT

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2034)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaTamirTestSonucManager sonucManager = new TestKaplamaTamirTestSonucManager();
			List<TestKaplamaTamirTestSonuc> sonucs = new ArrayList<TestKaplamaTamirTestSonuc>();
			sonucs = sonucManager.getAllTestKaplamaTamirTestSonuc(2034,
					prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-473.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr483ReportXlsx(Integer prmPipeId) {// LHT

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2131)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaHolidayPinholeSonucManager sonucManager = new TestKaplamaHolidayPinholeSonucManager();
			List<TestKaplamaHolidayPinholeSonuc> sonucs = new ArrayList<TestKaplamaHolidayPinholeSonuc>();
			sonucs = sonucManager.getAllTestKaplamaHolidayPinholeSonucTest(
					2131, prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-483.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr484ReportXlsx(Integer prmPipeId) {// POY

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2142)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaPulloffYapismaSonucManager sonucManager = new TestKaplamaPulloffYapismaSonucManager();
			List<TestKaplamaPulloffYapismaSonuc> sonucs = new ArrayList<TestKaplamaPulloffYapismaSonuc>();
			sonucs = sonucManager.getAllTestKaplamaPulloffYapismaSonuc(2142,
					prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date1 = new ArrayList<String>();
			List<String> date2 = new ArrayList<String>();
			date1.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getTestTarihi()));
			date2.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getBoyaTarihi()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date1", date1);
			dataMap.put("date2", date2);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-484.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr485ReportXlsx(Integer prmPipeId) {// KSK

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2030)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaUygulamaSicaklikSonucManager sonucManager = new TestKaplamaUygulamaSicaklikSonucManager();
			List<TestKaplamaUygulamaSicaklikSonuc> sonucs = new ArrayList<TestKaplamaUygulamaSicaklikSonuc>();
			sonucs = sonucManager.getAllTestKaplamaUygulamaSicaklikSonuc(2030,
					prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-485.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr486ReportXlsx(Integer prmPipeId) {// bitmedi - BGK

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2144)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaBetonGozKontrolSonucManager sonucManager = new TestKaplamaBetonGozKontrolSonucManager();
			List<TestKaplamaBetonGozKontrolSonuc> sonucs = new ArrayList<TestKaplamaBetonGozKontrolSonuc>();
			sonucs = sonucManager.getAllTestKaplamaBetonGozKontrolSonuc(2144,
					prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-486.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr487ReportXlsx(Integer prmPipeId) {// BYH

		FacesContext context = FacesContext.getCurrentInstance();
		if (prmPipeId == 0) {
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<IsolationTestDefinition> specs = new ArrayList<IsolationTestDefinition>();
			for (IsolationTestDefinition isolationTestDefinition : salesItems
					.get(0).getIsolationTestDefinitions()) {
				if (isolationTestDefinition.getGlobalId() == 2143)
					specs.add(isolationTestDefinition);
			}

			if (specs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"KALİTE TANIMLARINDA EKSİKLİK OLDUĞU İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			TestKaplamaBetonYuzeyHazirligiSonucManager sonucManager = new TestKaplamaBetonYuzeyHazirligiSonucManager();
			List<TestKaplamaBetonYuzeyHazirligiSonuc> sonucs = new ArrayList<TestKaplamaBetonYuzeyHazirligiSonuc>();
			sonucs = sonucManager.getAllTestKaplamaBetonYuzeyHazirligiSonuc(
					2143, prmPipeId);

			if (sonucs.size() < 1) {

				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"GİRİLMİŞ TEST VERİSİ BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(sonucs
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			try {
				muayeneyiYapan = sonucs.get(0).getEkleyenEmployee()
						.getEmployee().getFirstname()
						+ " "
						+ sonucs.get(0).getEkleyenEmployee().getEmployee()
								.getLastname();
			} catch (Exception e) {
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								"MUAYENE YAPAN BULUNAMADIĞI İÇİN RAPOR OLUŞTURULAMADI, KALİTE SORUMLUSU İLE GÖRÜŞÜNÜZ!",
								null));
				return;
			}
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("specs", specs);
			dataMap.put("sonucs", sonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/kaplama/FR-487.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	/**
	 * @return the config
	 */
	public ConfigBean getConfig() {
		return config;
	}

	/**
	 * @param config
	 *            the config to set
	 */
	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
