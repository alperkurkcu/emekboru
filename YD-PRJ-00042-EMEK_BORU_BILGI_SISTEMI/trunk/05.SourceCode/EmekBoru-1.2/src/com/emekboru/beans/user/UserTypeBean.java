package com.emekboru.beans.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.persistence.EntityManager;

import com.emekboru.jpa.GeneralPrivilege;
import com.emekboru.jpa.OperationPrivilege;
import com.emekboru.jpa.UserType;
import com.emekboru.jpaman.Factory;
import com.emekboru.jpaman.GeneralPrivilegeManager;
import com.emekboru.jpaman.OperationPrivilegeManager;
import com.emekboru.jpaman.UserTypeManager;
import com.emekboru.utils.FacesContextUtils;


@ManagedBean(name = "userTypeBean")
@ViewScoped
public class UserTypeBean implements Serializable{

	private static final long serialVersionUID = 1L;
	private UserType currentUserType;
	private UserType newUserType;
	
	// temp objects only for submission purposes
	private OperationPrivilege opPrivilege;
	private GeneralPrivilege   genPrivilege;
	
	public UserTypeBean(){
		
		newUserType     = new UserType();
		currentUserType = new UserType();
		opPrivilege     = new OperationPrivilege();
		genPrivilege    = new GeneralPrivilege();
	}

	public void cancelUserTypeSubmittion(ActionEvent event){
		
		newUserType = new UserType();
	}
	
	public void setPrivileges(){
		
		genPrivilege = currentUserType.getGeneralPrivileges().get(0);
		opPrivilege  = currentUserType.getOpPrivileges().get(0);
	}
	
	public void addUserType(ActionEvent event){
		
		UserTypeManager userTypeManager = new UserTypeManager();
		genPrivilege.setUserType(newUserType);
		opPrivilege.setUserType(newUserType);
		List<GeneralPrivilege> genList = new ArrayList<GeneralPrivilege>();
		genList.add(genPrivilege);
		List<OperationPrivilege> opList = new ArrayList<OperationPrivilege>();
		opList.add(opPrivilege);
		newUserType.setGeneralPrivileges(genList);
		newUserType.setOpPrivileges(opList);
		userTypeManager.enterNew(newUserType);
		
		newUserType = new UserType();
		UserListBean.loadUserTypeList();
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}
	
	public void updateGeneralPrivileges(){
		
		System.out.println("UserTypeBean.updateGeneralPrivileges()");
		GeneralPrivilegeManager generalManager = new GeneralPrivilegeManager();
		generalManager.updateEntity(currentUserType.getGeneralPrivileges().get(0));
		genPrivilege = currentUserType.getGeneralPrivileges().get(0);
		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}
	
	public void updateOperationPrivileges(){
		
		System.out.println("UserTypeBean.updateOperationPrivileges()");
		OperationPrivilegeManager operationManager = new OperationPrivilegeManager();
		operationManager.updateEntity(currentUserType.getOpPrivileges().get(0));
		opPrivilege = currentUserType.getOpPrivileges().get(0);
		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}
	
	public void updateAll(){
		
		System.out.println("UserTypeBean.updateAll()");
//		updateGeneralPrivileges();
//		updateOperationPrivileges();
		UserTypeManager man = new UserTypeManager();
		man.updateEntity(currentUserType);
		genPrivilege = currentUserType.getGeneralPrivileges().get(0);
		opPrivilege = currentUserType.getOpPrivileges().get(0);
		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}
	
	public void deleteUserType(){
		
		System.out.println("UserTypeBean.deleteUserType()");
		EntityManager typeManager = Factory.getInstance()
				                               .createEntityManager();
		currentUserType = typeManager.find(UserType.class
				                   , currentUserType.getUserTypeId());
		
		if(currentUserType.getUsers().size() != 0){
			
			FacesContextUtils.addErrorMessage("CannotDeleteMessage");
			return;
		}
		
		GeneralPrivilegeManager generalManager 
		                           = new GeneralPrivilegeManager();
		generalManager.deleteByField(GeneralPrivilege.class, "userTypeId", currentUserType
				                          .getUserTypeId());
		OperationPrivilegeManager operationManager  
		                        =  new OperationPrivilegeManager();
		operationManager.deleteByField(OperationPrivilege.class, "userTypeId"
				         , currentUserType.getUserTypeId());
		typeManager.remove(currentUserType);
		UserListBean.loadUserTypeList();	
		currentUserType = new UserType();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		
	}
	
	public void viewUsersOfUserType(){
		
		if(currentUserType == null || currentUserType.getUserTypeId()==null){
			
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		
		EntityManager typeManager = Factory.getInstance().createEntityManager();
		currentUserType = typeManager.find(UserType.class
				                             , currentUserType.getUserTypeId());
		
		currentUserType.getUsers();
		typeManager.close();
	}
	
	/**
	 * 			GETTERS AND SETTERS
	 */
	
	public UserType getCurrentUserType() {
		return currentUserType;
	}

	public void setCurrentUserType(UserType currentUserType) {
		this.currentUserType = currentUserType;
	}

	public UserType getNewUserType() {
		return newUserType;
	}

	public void setNewUserType(UserType newUserType) {
		this.newUserType = newUserType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public OperationPrivilege getOpPrivilege() {
		return opPrivilege;
	}

	public void setOpPrivilege(OperationPrivilege opPrivilege) {
		this.opPrivilege = opPrivilege;
	}

	public GeneralPrivilege getGenPrivilege() {
		return genPrivilege;
	}

	public void setGenPrivilege(GeneralPrivilege genPrivilege) {
		this.genPrivilege = genPrivilege;
	}
	
}
