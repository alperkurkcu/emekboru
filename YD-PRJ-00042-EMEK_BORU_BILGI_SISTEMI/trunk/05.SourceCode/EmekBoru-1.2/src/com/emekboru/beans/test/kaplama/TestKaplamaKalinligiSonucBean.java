package com.emekboru.beans.test.kaplama;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaKalinligiSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaKalinligiSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaplamaKalinligiSonucBean")
@ViewScoped
public class TestKaplamaKalinligiSonucBean {

	private TestKaplamaKalinligiSonuc testKaplamaKalinligiSonucForm = new TestKaplamaKalinligiSonuc();
	private List<TestKaplamaKalinligiSonuc> allKaplamaKalinligiSonucList = new ArrayList<TestKaplamaKalinligiSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	float ortalama = 0F;

	public void addOrUpdateKaplamaKalinligiTestSonuc(ActionEvent e) {
		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaKalinligiSonucManager kalinlikManager = new TestKaplamaKalinligiSonucManager();

		if (testKaplamaKalinligiSonucForm.getId() == null) {

			testKaplamaKalinligiSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaKalinligiSonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaKalinligiSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaKalinligiSonucForm.setBagliTestId(bagliTest);
			testKaplamaKalinligiSonucForm.setBagliGlobalId(bagliTest);

			ortalamaBul();

			kalinlikManager.enterNew(testKaplamaKalinligiSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaKalinligiSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaKalinligiSonucForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			ortalamaBul();
			kalinlikManager.updateEntity(testKaplamaKalinligiSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);
	}

	private void ortalamaBul() {

		ortalama = (testKaplamaKalinligiSonucForm.getKalinlikMin().floatValue() + testKaplamaKalinligiSonucForm
				.getKalinlikMax().floatValue()) / 2;
		testKaplamaKalinligiSonucForm.setKalinlikOrt(BigDecimal
				.valueOf(ortalama));
	}

	public void addKaplamaKalinligiTest() {

		testKaplamaKalinligiSonucForm = new TestKaplamaKalinligiSonuc();
		updateButtonRender = false;
	}

	public void kaplamaKalinligiListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allKaplamaKalinligiSonucList = TestKaplamaKalinligiSonucManager
				.getAllKaplamaKalinligiSonucTest(globalId, pipeId);
		testKaplamaKalinligiSonucForm = new TestKaplamaKalinligiSonuc();
	}

	public void deleteTestKaplamaKalinligiSonuc() {

		bagliTestId = testKaplamaKalinligiSonucForm.getBagliTestId().getId();

		if (testKaplamaKalinligiSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaKalinligiSonucManager kalinlikManager = new TestKaplamaKalinligiSonucManager();
		kalinlikManager.delete(testKaplamaKalinligiSonucForm);
		testKaplamaKalinligiSonucForm = new TestKaplamaKalinligiSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setters getters
	public TestKaplamaKalinligiSonuc getTestKaplamaKalinligiSonucForm() {
		return testKaplamaKalinligiSonucForm;
	}

	public void setTestKaplamaKalinligiSonucForm(
			TestKaplamaKalinligiSonuc testKaplamaKalinligiSonucForm) {
		this.testKaplamaKalinligiSonucForm = testKaplamaKalinligiSonucForm;
	}

	public List<TestKaplamaKalinligiSonuc> getAllKaplamaKalinligiSonucList() {
		return allKaplamaKalinligiSonucList;
	}

	public void setAllKaplamaKalinligiSonucList(
			List<TestKaplamaKalinligiSonuc> allKaplamaKalinligiSonucList) {
		this.allKaplamaKalinligiSonucList = allKaplamaKalinligiSonucList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}
}
