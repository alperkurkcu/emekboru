package com.emekboru.beans.order;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "lotListBean")
@ViewScoped
public class LotListBean implements Serializable {

	private static final long serialVersionUID = -8174407403709139689L;
	private List<Integer> lotList;

	public LotListBean() {
		lotList = new ArrayList<Integer>();
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public List<Integer> getLotList() {
		return lotList;
	}

	public void setLotList(List<Integer> lotList) {
		this.lotList = lotList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
