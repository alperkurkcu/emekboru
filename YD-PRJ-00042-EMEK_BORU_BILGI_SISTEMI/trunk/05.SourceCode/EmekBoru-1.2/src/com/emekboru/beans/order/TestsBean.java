package com.emekboru.beans.order;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.config.ConfigBean;
import com.emekboru.config.Test;
import com.emekboru.jpa.AgirlikTestsSpec;
import com.emekboru.jpa.BukmeTestsSpec;
import com.emekboru.jpa.CekmeTestsSpec;
import com.emekboru.jpa.CentikDarbeTestsSpec;
import com.emekboru.jpa.DestructiveTestsSpec;
import com.emekboru.jpa.KaynakMakroTestSpec;
import com.emekboru.jpa.KaynakSertlikTestsSpec;
import com.emekboru.jpa.NondestructiveTestsSpec;

@ManagedBean(name = "testsBean")
@ViewScoped
public class TestsBean implements Serializable {

	private static final long serialVersionUID = 3290034007605806828L;
	public static final String DESTRUCTIVE = "Destructive";
	public static final String NONDESTRUCTIVE = "Nondestructive";
	public static final String BENDING = "Bending";
	public static final String CHARPY = "Charpy";
	public static final String MACROGRAPHIC = "Macrographic";
	public static final String WEIGHT = "Weight";

	@EJB
	private ConfigBean config;

	// tests to be selected from the user
	private List<DestructiveTestsSpec> destructiveTests = null;
	private List<NondestructiveTestsSpec> nondestructiveTests = null;
	private List<BukmeTestsSpec> bukmeTests = null;
	private List<AgirlikTestsSpec> agirlikTests = null;
	private List<CentikDarbeTestsSpec> centikTests = null;

	private CekmeTestsSpec cekme = null;
	private CekmeVisibleFields cekmeVisibleFields = null;

	private KaynakMakroTestSpec kaynakMacroTest = null;
	private KaynakSertlikTestsSpec kaynakSertlikTest = null;

	// Single tests just for Selection/deletion
	private DestructiveTestsSpec selectedDest = null;
	private NondestructiveTestsSpec selectedNondest = null;
	private DestructiveTestsSpec destTest = null;
	private AgirlikTestsSpec agirlik = null;
	private BukmeTestsSpec bukme = null;
	private CentikDarbeTestsSpec centik = null;
	private boolean hasSpecs = false;

	public TestsBean() {

		destructiveTests = new ArrayList<DestructiveTestsSpec>(0);
		nondestructiveTests = new ArrayList<NondestructiveTestsSpec>(0);
		bukmeTests = new ArrayList<BukmeTestsSpec>(0);
		agirlikTests = new ArrayList<AgirlikTestsSpec>(0);
		centikTests = new ArrayList<CentikDarbeTestsSpec>(0);
		agirlik = new AgirlikTestsSpec();
		destTest = new DestructiveTestsSpec();
		bukme = new BukmeTestsSpec();
		centik = new CentikDarbeTestsSpec();
		cekme = new CekmeTestsSpec();
		kaynakMacroTest = new KaynakMakroTestSpec();
		kaynakSertlikTest = new KaynakSertlikTestsSpec();
		selectedDest = new DestructiveTestsSpec();
		selectedNondest = new NondestructiveTestsSpec();
		cekmeVisibleFields = new CekmeVisibleFields();
	}

	@PostConstruct
	public void init() {
		// initialize available tests. which will eventually be displayed
		// in the jsf page for selection
		List<Test> list = config.getConfig().getPipe().getManufacturing()
				.getGroupTests(DESTRUCTIVE);

		for (Test t : list) {

			DestructiveTestsSpec test = new DestructiveTestsSpec(t);
			destructiveTests.add(test);
		}

		list = config.getConfig().getPipe().getManufacturing()
				.getGroupTests(NONDESTRUCTIVE);
		for (Test t : list) {

			NondestructiveTestsSpec test = new NondestructiveTestsSpec(t);
			nondestructiveTests.add(test);
		}

		// load the BUKME TEST from CONFIG and initiate the possible
		// BukmeTestSpec
		list = config.getConfig().getPipe().getManufacturing()
				.getMiniGroupTests(BENDING);
		for (Test t : list) {

			BukmeTestsSpec test = new BukmeTestsSpec(t);
			bukmeTests.add(test);
		}

		// load the CENTIK DARBE TEST from CONFIG and initiate the tests
		list = config.getConfig().getPipe().getManufacturing()
				.getMiniGroupTests(CHARPY);
		for (Test t : list) {

			CentikDarbeTestsSpec test = new CentikDarbeTestsSpec(t);
			centikTests.add(test);
		}

		// load the WEIGHT TEST from CONFIG and initiate the tests
		list = config.getConfig().getPipe().getManufacturing()
				.getMiniGroupTests(WEIGHT);
		for (Test t : list) {

			AgirlikTestsSpec test = new AgirlikTestsSpec(t);
			agirlikTests.add(test);
		}

	}

	public void setAgirlikTest(AgirlikTestsSpec test) {

		for (AgirlikTestsSpec t : agirlikTests) {
			if (t.getGlobalId().equals(test.getGlobalId()))
				t = test;
		}
	}

	public void setCekmeVisibleFields(int globalId) {

		switch (globalId) {
		case CekmeTestsSpec.KAYNAKLI:
			cekmeVisibleFields.setKaynakli();
			break;

		case CekmeTestsSpec.MALZEME_ENINE:
			cekmeVisibleFields.setMenine();
			break;

		case CekmeTestsSpec.MALZEME_BOYUNA:
			cekmeVisibleFields.setMboyuna();
			break;

		case CekmeTestsSpec.BANT_EKI_KAYNAGI:
			cekmeVisibleFields.setBant();
			break;

		case CekmeTestsSpec.TAM_KAYNAK:
			cekmeVisibleFields.setTamKaynak();
			break;

		default:
			break;
		}
	}

	// *************************************************************************//
	// GETTERS AND SETTERS //
	// *************************************************************************//
	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	public List<DestructiveTestsSpec> getDestructiveTests() {
		return destructiveTests;
	}

	public void setDestructiveTests(List<DestructiveTestsSpec> destructiveTests) {
		this.destructiveTests = destructiveTests;
	}

	public List<NondestructiveTestsSpec> getNondestructiveTests() {
		return nondestructiveTests;
	}

	public void setNondestructiveTests(
			List<NondestructiveTestsSpec> nondestructiveTests) {
		this.nondestructiveTests = nondestructiveTests;
	}

	public List<BukmeTestsSpec> getBukmeTests() {
		return bukmeTests;
	}

	public void setBukmeTests(List<BukmeTestsSpec> bukmeTests) {
		this.bukmeTests = bukmeTests;
	}

	public List<AgirlikTestsSpec> getAgirlikTests() {
		return agirlikTests;
	}

	public void setAgirlikTests(List<AgirlikTestsSpec> agirlikTests) {
		this.agirlikTests = agirlikTests;
	}

	public List<CentikDarbeTestsSpec> getCentikTests() {
		return centikTests;
	}

	public void setCentikTests(List<CentikDarbeTestsSpec> centikTests) {
		this.centikTests = centikTests;
	}

	public CekmeTestsSpec getCekme() {
		return cekme;
	}

	public void setCekme(CekmeTestsSpec cekme) {
		this.cekme = cekme;
	}

	public KaynakMakroTestSpec getKaynakMacroTest() {
		return kaynakMacroTest;
	}

	public void setKaynakMacroTest(KaynakMakroTestSpec kaynakMacroTest) {
		this.kaynakMacroTest = kaynakMacroTest;
	}

	public AgirlikTestsSpec getAgirlik() {
		return agirlik;
	}

	public void setAgirlik(AgirlikTestsSpec agirlik) {
		this.agirlik = agirlik;
	}

	public KaynakSertlikTestsSpec getKaynakSertlikTest() {
		return kaynakSertlikTest;
	}

	public void setKaynakSertlikTest(KaynakSertlikTestsSpec kaynakSertlikTest) {
		this.kaynakSertlikTest = kaynakSertlikTest;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public DestructiveTestsSpec getSelectedDest() {
		return selectedDest;
	}

	public void setSelectedDest(DestructiveTestsSpec selectedDest) {
		this.selectedDest = selectedDest;
	}

	public NondestructiveTestsSpec getSelectedNondest() {
		return selectedNondest;
	}

	public void setSelectedNondest(NondestructiveTestsSpec selectedNondest) {
		this.selectedNondest = selectedNondest;
	}

	public CekmeVisibleFields getCekmeVisibleFields() {
		return cekmeVisibleFields;
	}

	public void setCekmeVisibleFields(CekmeVisibleFields cekmeVisibleFields) {
		this.cekmeVisibleFields = cekmeVisibleFields;
	}

	public DestructiveTestsSpec getDestTest() {
		return destTest;
	}

	public void setDestTest(DestructiveTestsSpec destTest) {
		this.destTest = destTest;
	}

	public BukmeTestsSpec getBukme() {
		return bukme;
	}

	public void setBukme(BukmeTestsSpec bukme) {
		this.bukme = bukme;
	}

	public CentikDarbeTestsSpec getCentik() {
		return centik;
	}

	public void setCentik(CentikDarbeTestsSpec centik) {
		this.centik = centik;
	}

	public boolean isHasSpecs() {
		return hasSpecs;
	}

	public void setHasSpecs(boolean hasSpecs) {
		this.hasSpecs = hasSpecs;
	}

}
