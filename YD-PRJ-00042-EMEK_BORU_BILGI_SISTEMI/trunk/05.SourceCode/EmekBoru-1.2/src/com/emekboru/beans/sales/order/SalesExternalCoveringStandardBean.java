package com.emekboru.beans.sales.order;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.sales.order.SalesExternalCoveringStandard;
import com.emekboru.jpaman.sales.order.SalesExternalCoveringStandardManager;


@ManagedBean(name = "salesExternalCoveringStandardBean")
@ViewScoped
public class SalesExternalCoveringStandardBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1508279762342256087L;
	private List<SalesExternalCoveringStandard> standardList;

	public SalesExternalCoveringStandardBean() {

		SalesExternalCoveringStandardManager manager = new SalesExternalCoveringStandardManager();
		standardList = manager.findAll(SalesExternalCoveringStandard.class);
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public List<SalesExternalCoveringStandard> getStandardList() {
		return standardList;
	}

	public void setStandardList(List<SalesExternalCoveringStandard> standardList) {
		this.standardList = standardList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}