/**
 * 
 */
package com.emekboru.beans.stok;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.stok.StokJoinUreticiKoduBarkod;
import com.emekboru.jpa.stok.StokTanimlarMarka;
import com.emekboru.jpaman.stok.StokJoinUreticiKoduBarkodManager;
import com.emekboru.jpaman.stok.StokTanimlarMarkaManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "stokJoinUreticiKoduBarkodBean")
@ViewScoped
public class StokJoinUreticiKoduBarkodBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<StokJoinUreticiKoduBarkod> allStokJoinUreticiKoduBarkodList = new ArrayList<StokJoinUreticiKoduBarkod>();

	private StokJoinUreticiKoduBarkod stokJoinUreticiKoduBarkodForm = new StokJoinUreticiKoduBarkod();

	private StokJoinUreticiKoduBarkod[] selectedMarkalar;

	StokJoinUreticiKoduBarkodManager formManager = new StokJoinUreticiKoduBarkodManager();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	Integer prmUrunJoinId = 0;
	Integer prmMarkaId = 0;

	public StokJoinUreticiKoduBarkodBean() {

		allStokJoinUreticiKoduBarkodList = null;
		updateButtonRender = false;
	}

	public void addForm(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		prmMarkaId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");
		prmUrunJoinId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");

		stokJoinUreticiKoduBarkodForm = new StokJoinUreticiKoduBarkod();
		updateButtonRender = false;
	}

	public void addOrUpdateForm() {

		StokTanimlarMarkaManager markaManager = new StokTanimlarMarkaManager();

		if (stokJoinUreticiKoduBarkodForm.getJoinId() == null) {

			stokJoinUreticiKoduBarkodForm.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			stokJoinUreticiKoduBarkodForm.setEkleyenKullanici(userBean
					.getUser().getId());
			stokJoinUreticiKoduBarkodForm.getStokJoinMarkaUreticiKodu()
					.setJoinId(prmUrunJoinId);
			stokJoinUreticiKoduBarkodForm.setStokTanimlarMarka(markaManager
					.loadObject(StokTanimlarMarka.class, prmMarkaId));

			formManager.enterNew(stokJoinUreticiKoduBarkodForm);

			this.stokJoinUreticiKoduBarkodForm = new StokJoinUreticiKoduBarkod();

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"ÜRÜN BARKODU BAŞARIYLA EKLENMİŞTİR!"));

		} else {

			stokJoinUreticiKoduBarkodForm.setGuncellemeZamani(UtilInsCore
					.getTarihZaman());
			stokJoinUreticiKoduBarkodForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			formManager.updateEntity(stokJoinUreticiKoduBarkodForm);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"ÜRÜN BARKODU BAŞARIYLA GÜNCELLENMİŞTİR!"));
		}

		fillTestList(prmUrunJoinId);
	}

	public void deleteForm() {

		if (stokJoinUreticiKoduBarkodForm.getJoinId() == null) {

			FacesContextUtils.addWarnMessage("NoSelectMessage");
			return;
		}

		formManager.delete(stokJoinUreticiKoduBarkodForm);
		fillTestList(prmUrunJoinId);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"ÜRÜN BARKODU BAŞARIYLA SİLİNMİŞTİR!"));
	}

	public void fillTestList(Integer prmUrunJoinId) {

		StokJoinUreticiKoduBarkodManager manager = new StokJoinUreticiKoduBarkodManager();
		allStokJoinUreticiKoduBarkodList = manager
				.getAllStokJoinUreticiKoduBarkod(prmUrunJoinId);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"ÜRÜN BARKODLARI BAŞARI İLE LİSTELENMİŞTİR"));
	}

	public void fillTestList(ActionEvent e) {// sayfa ilk açılırken prmFormId
		// alıyor

		UICommand cmd = (UICommand) e.getComponent();
		prmMarkaId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");
		prmUrunJoinId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		StokJoinUreticiKoduBarkodManager manager = new StokJoinUreticiKoduBarkodManager();
		allStokJoinUreticiKoduBarkodList = manager
				.findAllByUreticiJoinIdMarkaId(prmMarkaId, prmUrunJoinId);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"ÜRÜN BARKODLARI BAŞARI İLE LİSTELENMİŞTİR"));
	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	// setters getters

	public List<StokJoinUreticiKoduBarkod> getAllStokJoinUreticiKoduBarkodList() {
		return allStokJoinUreticiKoduBarkodList;
	}

	public void setAllStokJoinUreticiKoduBarkodList(
			List<StokJoinUreticiKoduBarkod> allStokJoinUreticiKoduBarkodList) {
		this.allStokJoinUreticiKoduBarkodList = allStokJoinUreticiKoduBarkodList;
	}

	public StokJoinUreticiKoduBarkod[] getSelectedMarkalar() {
		return selectedMarkalar;
	}

	public void setSelectedMarkalar(StokJoinUreticiKoduBarkod[] selectedMarkalar) {
		this.selectedMarkalar = selectedMarkalar;
	}

	public StokJoinUreticiKoduBarkodManager getFormManager() {
		return formManager;
	}

	public void setFormManager(StokJoinUreticiKoduBarkodManager formManager) {
		this.formManager = formManager;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public Integer getPrmUrunJoinId() {
		return prmUrunJoinId;
	}

	public void setPrmUrunJoinId(Integer prmUrunJoinId) {
		this.prmUrunJoinId = prmUrunJoinId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public StokJoinUreticiKoduBarkod getStokJoinUreticiKoduBarkodForm() {
		return stokJoinUreticiKoduBarkodForm;
	}

	public void setStokJoinUreticiKoduBarkodForm(
			StokJoinUreticiKoduBarkod stokJoinUreticiKoduBarkodForm) {
		this.stokJoinUreticiKoduBarkodForm = stokJoinUreticiKoduBarkodForm;
	}

}
