/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaMixedWetPaintTestSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaMixedWetPaintTestSonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testKaplamaMixedWetPaintTestSonucBean")
@ViewScoped
public class TestKaplamaMixedWetPaintTestSonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private TestKaplamaMixedWetPaintTestSonuc testKaplamaMixedWetPaintTestSonucForm = new TestKaplamaMixedWetPaintTestSonuc();
	private List<TestKaplamaMixedWetPaintTestSonuc> allTestKaplamaMixedWetPaintTestSonucList = new ArrayList<TestKaplamaMixedWetPaintTestSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateTestKaplamaMixedWetPaintTestSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaMixedWetPaintTestSonucManager tkbebManager = new TestKaplamaMixedWetPaintTestSonucManager();

		if (testKaplamaMixedWetPaintTestSonucForm.getId() == null) {

			testKaplamaMixedWetPaintTestSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaMixedWetPaintTestSonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaMixedWetPaintTestSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaMixedWetPaintTestSonucForm.setBagliTestId(bagliTest);
			testKaplamaMixedWetPaintTestSonucForm.setBagliGlobalId(bagliTest);

			tkbebManager.enterNew(testKaplamaMixedWetPaintTestSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			testKaplamaMixedWetPaintTestSonucForm = new TestKaplamaMixedWetPaintTestSonuc();
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaMixedWetPaintTestSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaMixedWetPaintTestSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			tkbebManager.updateEntity(testKaplamaMixedWetPaintTestSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addTestKaplamaMixedWetPaintTestSonuc() {

		testKaplamaMixedWetPaintTestSonucForm = new TestKaplamaMixedWetPaintTestSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaMixedWetPaintTestSonucListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaMixedWetPaintTestSonucList = TestKaplamaMixedWetPaintTestSonucManager
				.getAllMixedWetPaintTestSonuc(globalId, pipeId);
		testKaplamaMixedWetPaintTestSonucForm = new TestKaplamaMixedWetPaintTestSonuc();
	}

	public void deleteTestKaplamaMixedWetPaintTestSonuc() {

		bagliTestId = testKaplamaMixedWetPaintTestSonucForm.getBagliGlobalId()
				.getId();

		if (testKaplamaMixedWetPaintTestSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		TestKaplamaMixedWetPaintTestSonucManager tkbebManager = new TestKaplamaMixedWetPaintTestSonucManager();

		tkbebManager.delete(testKaplamaMixedWetPaintTestSonucForm);
		testKaplamaMixedWetPaintTestSonucForm = new TestKaplamaMixedWetPaintTestSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters

	/**
	 * @return the testKaplamaMixedWetPaintTestSonucForm
	 */
	public TestKaplamaMixedWetPaintTestSonuc getTestKaplamaMixedWetPaintTestSonucForm() {
		return testKaplamaMixedWetPaintTestSonucForm;
	}

	/**
	 * @param testKaplamaMixedWetPaintTestSonucForm
	 *            the testKaplamaMixedWetPaintTestSonucForm to set
	 */
	public void setTestKaplamaMixedWetPaintTestSonucForm(
			TestKaplamaMixedWetPaintTestSonuc testKaplamaMixedWetPaintTestSonucForm) {
		this.testKaplamaMixedWetPaintTestSonucForm = testKaplamaMixedWetPaintTestSonucForm;
	}

	/**
	 * @return the allTestKaplamaMixedWetPaintTestSonucList
	 */
	public List<TestKaplamaMixedWetPaintTestSonuc> getAllTestKaplamaMixedWetPaintTestSonucList() {
		return allTestKaplamaMixedWetPaintTestSonucList;
	}

	/**
	 * @param allTestKaplamaMixedWetPaintTestSonucList
	 *            the allTestKaplamaMixedWetPaintTestSonucList to set
	 */
	public void setAllTestKaplamaMixedWetPaintTestSonucList(
			List<TestKaplamaMixedWetPaintTestSonuc> allTestKaplamaMixedWetPaintTestSonucList) {
		this.allTestKaplamaMixedWetPaintTestSonucList = allTestKaplamaMixedWetPaintTestSonucList;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
