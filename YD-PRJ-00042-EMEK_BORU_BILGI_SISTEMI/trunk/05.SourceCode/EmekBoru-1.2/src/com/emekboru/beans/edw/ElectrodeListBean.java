package com.emekboru.beans.edw;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.event.AjaxBehaviorEvent;

import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.ElectrodeDustWire;
import com.emekboru.jpaman.ElectrodeDustWireManager;
import com.emekboru.messages.ElectrodeDustWireType;
import com.emekboru.utils.DateTrans;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "electrodeListBean")
@ViewScoped
public class ElectrodeListBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	private List<ElectrodeDustWire> recentElectrodes;
	private List<ElectrodeDustWire> electrodes;
	private List<Integer> electrodeYearList;
	private int currentYear;

	public ElectrodeListBean() {

		electrodes = new ArrayList<ElectrodeDustWire>();
		recentElectrodes = new ArrayList<ElectrodeDustWire>();
		electrodeYearList = new ArrayList<Integer>();
		currentYear = DateTrans.getYearOfDate(new Date());
	}

	@PostConstruct
	public void init() {

		loadElectrodeYears();
		loadRecentElectrodes();
		loadYearElectrodes(currentYear);
	}

	public void loadRecentElectrodes() {

		ElectrodeDustWireManager man = new ElectrodeDustWireManager();
		recentElectrodes = man.findRecent(100, ElectrodeDustWireType.ELECTRODE);
	}

	public void loadYearElectrodes(int year) {

		DateTrans.calendar.set(year, Calendar.JANUARY, 1, 0, 1);
		Date startDate = DateTrans.calendar.getTime();
		DateTrans.calendar.set(year, Calendar.DECEMBER, 31, 23, 59);
		Date endDate = DateTrans.calendar.getTime();
		ElectrodeDustWireManager dustManager = new ElectrodeDustWireManager();
		electrodes = dustManager.findTypeBetweenDates(
				ElectrodeDustWireType.ELECTRODE, startDate, endDate);
	}

	public void loadSelectedYearElectrodes(AjaxBehaviorEvent event) {

		HtmlSelectOneMenu one = (HtmlSelectOneMenu) event.getSource();
		currentYear = Integer.valueOf(one.getValue().toString());
		loadYearElectrodes(currentYear);
	}

	/******************************************************************************
	 ******************* HELPER METHODS SECTION ******************** /
	 ******************************************************************************/
	protected void loadElectrodeYears() {

		electrodeYearList = new ArrayList<Integer>();
		ElectrodeDustWireManager dustManager = new ElectrodeDustWireManager();
		List<Date> minList = dustManager
				.findMinYearForType(ElectrodeDustWireType.ELECTRODE);
		Integer min, max;
		if (minList.size() == 1 && minList.get(0) == null) {

			min = 0;
			max = 0;
			return;
		}
		min = DateTrans.getYearOfDate(minList.get(0));
		max = DateTrans.getYearOfDate(new Date());
		for (int i = min; i <= max; i++)
			electrodeYearList.add(i);
	}

	public void goToElectrodePage() {

		FacesContextUtils.redirect(config.getPageUrl().ELECTRODE_PAGE);
	}

	/******************************************************************************
	 ******************* GETTERS AND SETTERS ******************** /
	 ******************************************************************************/
	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	public List<ElectrodeDustWire> getRecentElectrodes() {
		return recentElectrodes;
	}

	public void setRecentElectrodes(List<ElectrodeDustWire> recentElectrodes) {
		this.recentElectrodes = recentElectrodes;
	}

	public List<ElectrodeDustWire> getElectrodes() {
		return electrodes;
	}

	public void setElectrodes(List<ElectrodeDustWire> electrodes) {
		this.electrodes = electrodes;
	}

	public List<Integer> getElectrodeYearList() {
		return electrodeYearList;
	}

	public void setElectrodeYearList(List<Integer> electrodeYearList) {
		this.electrodeYearList = electrodeYearList;
	}

	public int getCurrentYear() {
		return currentYear;
	}

	public void setCurrentYear(int currentYear) {
		this.currentYear = currentYear;
	}
}
