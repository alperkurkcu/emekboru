/**
 * 
 */
package com.emekboru.beans.coatingmaterials;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.CoatRawMaterial;
import com.emekboru.jpa.coatingmaterials.KaplamaMalzemeKullanmaIcKumlama;
import com.emekboru.jpaman.CoatRawMaterialManager;
import com.emekboru.jpaman.coatingmaterials.KaplamaMalzemeKullanmaIcKumlamaManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "kaplamaMalzemeKullanmaIcKumlamaBean")
@ViewScoped
public class KaplamaMalzemeKullanmaIcKumlamaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<KaplamaMalzemeKullanmaIcKumlama> allKaplamaMalzemeKullanmaIcKumlamaList = new ArrayList<KaplamaMalzemeKullanmaIcKumlama>();
	private KaplamaMalzemeKullanmaIcKumlama kaplamaMalzemeKullanmaIcKumlamaForm = new KaplamaMalzemeKullanmaIcKumlama();

	private CoatRawMaterial selectedMaterial = new CoatRawMaterial();
	private List<CoatRawMaterial> coatRawMaterialList;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	private BigDecimal kullanilanMiktar;

	private boolean durum = false;

	public KaplamaMalzemeKullanmaIcKumlamaBean() {

		coatRawMaterialList = new ArrayList<CoatRawMaterial>();
		CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
		coatRawMaterialList = materialManager
				.findUnfinishedCoatMaterialByType(1001);// kumlama type
	}

	public void addNew() {
		kaplamaMalzemeKullanmaIcKumlamaForm = new KaplamaMalzemeKullanmaIcKumlama();
		updateButtonRender = false;
	}

	public void addMalzemeKullanmaSonuc(ActionEvent e) {

		if (kaplamaMalzemeKullanmaIcKumlamaForm.getHarcananMiktar() == null) {
			kaplamaMalzemeKullanmaIcKumlamaForm
					.setHarcananMiktar(BigDecimal.ZERO);
		}

		if (!malzemeKontrol()) {
			return;
		}

		KaplamaMalzemeKullanmaIcKumlamaManager malzemeKullanmaManager = new KaplamaMalzemeKullanmaIcKumlamaManager();

		if (kaplamaMalzemeKullanmaIcKumlamaForm.getId() == null) {

			try {
				kaplamaMalzemeKullanmaIcKumlamaForm.setEklemeZamani(UtilInsCore
						.getTarihZaman());
				kaplamaMalzemeKullanmaIcKumlamaForm
						.setEkleyenKullanici(userBean.getUser().getId());
				kaplamaMalzemeKullanmaIcKumlamaForm
						.setCoatRawMaterial(selectedMaterial);
				malzemeKullanmaManager
						.enterNew(kaplamaMalzemeKullanmaIcKumlamaForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"İÇ KUMLAMA MALZEMESİ İŞLEMİ BAŞARIYLA EKLENMİŞTİR!"));

				kullanilanMiktar = kaplamaMalzemeKullanmaIcKumlamaForm
						.getHarcananMiktar();
				durum = true;
				malzemeGuncelle();

			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"İÇ KUMLAMA MALZEMESİ İŞLEMİ EKLENEMEDİ!", null));
				System.out
						.println("KaplamaMalzemeKullanmaIcKumlamaBean.addMalzemeKullanmaSonuc-HATA-EKLEME");
			}
		} else {

			try {

				kaplamaMalzemeKullanmaIcKumlamaForm
						.setGuncellemeZamani(UtilInsCore.getTarihZaman());
				kaplamaMalzemeKullanmaIcKumlamaForm
						.setGuncelleyenKullanici(userBean.getUser().getId());
				malzemeKullanmaManager
						.updateEntity(kaplamaMalzemeKullanmaIcKumlamaForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								"İÇ KUMLAMA MALZEMESİ İŞLEMİ BAŞARIYLA GÜNCELLENMİŞTİR!"));

			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"İÇ KUMLAMA MALZEMESİ KULLANMA İŞLEMİ GÜNCELLENEMEDİ!",
						null));
				System.out
						.println("KaplamaMalzemeKullanmaIcKumlamaBean.addMalzemeKullanmaSonuc-HATA-GÜNCELLEME"
								+ e.toString());
			}

		}

		fillTestList();
	}

	@SuppressWarnings("static-access")
	public void fillTestList() {

		KaplamaMalzemeKullanmaIcKumlamaManager malzemeKullanmaManager = new KaplamaMalzemeKullanmaIcKumlamaManager();
		allKaplamaMalzemeKullanmaIcKumlamaList = malzemeKullanmaManager
				.getAllKaplamaMalzemeKullanmaIcKumlama();
	}

	public void kaplamaListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void deleteMalzemeKullanmaSonuc(ActionEvent e) {

		KaplamaMalzemeKullanmaIcKumlamaManager malzemeKullanmaManager = new KaplamaMalzemeKullanmaIcKumlamaManager();

		if (kaplamaMalzemeKullanmaIcKumlamaForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		kullanilanMiktar = kaplamaMalzemeKullanmaIcKumlamaForm
				.getHarcananMiktar();

		durum = false;
		malzemeGuncelle();

		malzemeKullanmaManager.delete(kaplamaMalzemeKullanmaIcKumlamaForm);
		kaplamaMalzemeKullanmaIcKumlamaForm = new KaplamaMalzemeKullanmaIcKumlama();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"İÇ KUMLAMA MALZEMESİ KULLANMA İŞLEMİ BAŞARIYLA SİLİNMİŞTİR!"));

		fillTestList();
	}

	public void malzemeGuncelle() {

		CoatRawMaterialManager materialManager = new CoatRawMaterialManager();
		if (durum) {// enternew
			selectedMaterial.setRemainingAmount(selectedMaterial
					.getRemainingAmount() - kullanilanMiktar.doubleValue());
			materialManager.updateEntity(selectedMaterial);
		} else if (!durum) {// delete
			kaplamaMalzemeKullanmaIcKumlamaForm.getCoatRawMaterial()
					.setRemainingAmount(
							kaplamaMalzemeKullanmaIcKumlamaForm
									.getCoatRawMaterial().getRemainingAmount()
									+ kullanilanMiktar.doubleValue());
			materialManager.updateEntity(kaplamaMalzemeKullanmaIcKumlamaForm
					.getCoatRawMaterial());
		}
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"İÇ KUMLAMA MALZEMESİ BAŞARIYLA GÜNCELLENMİŞTİR!"));
	}

	public boolean malzemeKontrol() {

		FacesContext context = FacesContext.getCurrentInstance();
		if (kaplamaMalzemeKullanmaIcKumlamaForm.getHarcananMiktar()
				.floatValue() > selectedMaterial.getRemainingAmount()) {
			context.addMessage(
					null,
					new FacesMessage(
							"KULLANILACAK MİKTAR KALAN MALZEME MİKTARINDAN BÜYÜKTÜR! LÜTFEN KONTROL EDİNİZ!"));
			return false;
		} else {
			return true;
		}
	}

	// setters getters

	/**
	 * @return the allKaplamaMalzemeKullanmaIcKumlamaList
	 */
	public List<KaplamaMalzemeKullanmaIcKumlama> getAllKaplamaMalzemeKullanmaIcKumlamaList() {
		return allKaplamaMalzemeKullanmaIcKumlamaList;
	}

	/**
	 * @param allKaplamaMalzemeKullanmaIcKumlamaList
	 *            the allKaplamaMalzemeKullanmaIcKumlamaList to set
	 */
	public void setAllKaplamaMalzemeKullanmaIcKumlamaList(
			List<KaplamaMalzemeKullanmaIcKumlama> allKaplamaMalzemeKullanmaIcKumlamaList) {
		this.allKaplamaMalzemeKullanmaIcKumlamaList = allKaplamaMalzemeKullanmaIcKumlamaList;
	}

	/**
	 * @return the kaplamaMalzemeKullanmaIcKumlamaForm
	 */
	public KaplamaMalzemeKullanmaIcKumlama getKaplamaMalzemeKullanmaIcKumlamaForm() {
		return kaplamaMalzemeKullanmaIcKumlamaForm;
	}

	/**
	 * @param kaplamaMalzemeKullanmaIcKumlamaForm
	 *            the kaplamaMalzemeKullanmaIcKumlamaForm to set
	 */
	public void setKaplamaMalzemeKullanmaIcKumlamaForm(
			KaplamaMalzemeKullanmaIcKumlama kaplamaMalzemeKullanmaIcKumlamaForm) {
		this.kaplamaMalzemeKullanmaIcKumlamaForm = kaplamaMalzemeKullanmaIcKumlamaForm;
	}

	/**
	 * @return the selectedMaterial
	 */
	public CoatRawMaterial getSelectedMaterial() {
		return selectedMaterial;
	}

	/**
	 * @param selectedMaterial
	 *            the selectedMaterial to set
	 */
	public void setSelectedMaterial(CoatRawMaterial selectedMaterial) {
		this.selectedMaterial = selectedMaterial;
	}

	/**
	 * @return the coatRawMaterialList
	 */
	public List<CoatRawMaterial> getCoatRawMaterialList() {
		return coatRawMaterialList;
	}

	/**
	 * @param coatRawMaterialList
	 *            the coatRawMaterialList to set
	 */
	public void setCoatRawMaterialList(List<CoatRawMaterial> coatRawMaterialList) {
		this.coatRawMaterialList = coatRawMaterialList;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the kullanilanMiktar
	 */
	public BigDecimal getKullanilanMiktar() {
		return kullanilanMiktar;
	}

	/**
	 * @param kullanilanMiktar
	 *            the kullanilanMiktar to set
	 */
	public void setKullanilanMiktar(BigDecimal kullanilanMiktar) {
		this.kullanilanMiktar = kullanilanMiktar;
	}

	/**
	 * @return the durum
	 */
	public boolean isDurum() {
		return durum;
	}

	/**
	 * @param durum
	 *            the durum to set
	 */
	public void setDurum(boolean durum) {
		this.durum = durum;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
