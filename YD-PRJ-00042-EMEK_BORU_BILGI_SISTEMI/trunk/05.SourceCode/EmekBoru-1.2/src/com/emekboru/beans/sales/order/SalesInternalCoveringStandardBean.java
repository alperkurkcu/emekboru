package com.emekboru.beans.sales.order;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.jpa.sales.order.SalesInternalCoveringStandard;
import com.emekboru.jpaman.sales.order.SalesInternalCoveringStandardManager;

@ManagedBean(name = "salesInternalCoveringStandardBean")
@ViewScoped
public class SalesInternalCoveringStandardBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8620674822410821692L;
	private List<SalesInternalCoveringStandard> standardList;

	public SalesInternalCoveringStandardBean() {

		SalesInternalCoveringStandardManager manager = new SalesInternalCoveringStandardManager();
		standardList = manager.findAll(SalesInternalCoveringStandard.class);
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public List<SalesInternalCoveringStandard> getStandardList() {
		return standardList;
	}

	public void setStandardList(List<SalesInternalCoveringStandard> standardList) {
		this.standardList = standardList;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}