/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaHolidayPinholeSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaHolidayPinholeSonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testKaplamaHolidayPinholeSonucBean")
@ViewScoped
public class TestKaplamaHolidayPinholeSonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private TestKaplamaHolidayPinholeSonuc testKaplamaHolidayPinholeSonucForm = new TestKaplamaHolidayPinholeSonuc();
	private List<TestKaplamaHolidayPinholeSonuc> allKaplamaHolidayPinholeSonucList = new ArrayList<TestKaplamaHolidayPinholeSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateKaplamaHolidayPinholeTestSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaHolidayPinholeSonucManager tkbsManager = new TestKaplamaHolidayPinholeSonucManager();

		if (testKaplamaHolidayPinholeSonucForm.getId() == null) {

			testKaplamaHolidayPinholeSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaHolidayPinholeSonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaHolidayPinholeSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaHolidayPinholeSonucForm.setBagliTestId(bagliTest);
			testKaplamaHolidayPinholeSonucForm.setBagliGlobalId(bagliTest);

			tkbsManager.enterNew(testKaplamaHolidayPinholeSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaHolidayPinholeSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaHolidayPinholeSonucForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			tkbsManager.updateEntity(testKaplamaHolidayPinholeSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addKaplamaHolidayPinholeTest() {

		testKaplamaHolidayPinholeSonucForm = new TestKaplamaHolidayPinholeSonuc();
		updateButtonRender = false;
	}

	public void kaplamaHolidayPinholeListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allKaplamaHolidayPinholeSonucList = TestKaplamaHolidayPinholeSonucManager
				.getAllTestKaplamaHolidayPinholeSonucTest(globalId, pipeId);
		testKaplamaHolidayPinholeSonucForm = new TestKaplamaHolidayPinholeSonuc();
	}

	public void deleteTestKaplamaHolidayPinholeSonuc() {
		bagliTestId = testKaplamaHolidayPinholeSonucForm.getBagliTestId()
				.getId();
		if (testKaplamaHolidayPinholeSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaHolidayPinholeSonucManager tkbsManager = new TestKaplamaHolidayPinholeSonucManager();

		tkbsManager.delete(testKaplamaHolidayPinholeSonucForm);
		testKaplamaHolidayPinholeSonucForm = new TestKaplamaHolidayPinholeSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setters getters

	/**
	 * @return the testKaplamaHolidayPinholeSonucForm
	 */
	public TestKaplamaHolidayPinholeSonuc getTestKaplamaHolidayPinholeSonucForm() {
		return testKaplamaHolidayPinholeSonucForm;
	}

	/**
	 * @param testKaplamaHolidayPinholeSonucForm
	 *            the testKaplamaHolidayPinholeSonucForm to set
	 */
	public void setTestKaplamaHolidayPinholeSonucForm(
			TestKaplamaHolidayPinholeSonuc testKaplamaHolidayPinholeSonucForm) {
		this.testKaplamaHolidayPinholeSonucForm = testKaplamaHolidayPinholeSonucForm;
	}

	/**
	 * @return the allKaplamaHolidayPinholeSonucList
	 */
	public List<TestKaplamaHolidayPinholeSonuc> getAllKaplamaHolidayPinholeSonucList() {
		return allKaplamaHolidayPinholeSonucList;
	}

	/**
	 * @param allKaplamaHolidayPinholeSonucList
	 *            the allKaplamaHolidayPinholeSonucList to set
	 */
	public void setAllKaplamaHolidayPinholeSonucList(
			List<TestKaplamaHolidayPinholeSonuc> allKaplamaHolidayPinholeSonucList) {
		this.allKaplamaHolidayPinholeSonucList = allKaplamaHolidayPinholeSonucList;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
