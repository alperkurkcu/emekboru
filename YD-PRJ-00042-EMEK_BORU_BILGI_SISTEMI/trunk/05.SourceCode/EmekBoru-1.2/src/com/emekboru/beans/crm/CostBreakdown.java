package com.emekboru.beans.crm;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.emekboru.jpa.EventCostBreakdown;


@ManagedBean
@RequestScoped
public class CostBreakdown implements Serializable{

	private static final long serialVersionUID = 860399243817066685L;
	
	private EventCostBreakdown cost;
	
	public CostBreakdown(){
		
		cost = new EventCostBreakdown();
	}	
	
	/**
	 * 		GETTERS AND SETTERS
	 */
	public EventCostBreakdown getCost() {
		return cost;
	}

	public void setCost(EventCostBreakdown cost) {
		this.cost = cost;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
