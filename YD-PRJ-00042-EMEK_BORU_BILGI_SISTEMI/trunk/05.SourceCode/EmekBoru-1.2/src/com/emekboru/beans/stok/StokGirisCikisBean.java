/**
 * 
 */
package com.emekboru.beans.stok;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.converters.AmbarTalepConverter;
import com.emekboru.converters.SalesItemConverter;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpa.satinalma.SatinalmaAmbarMalzemeTalepForm;
import com.emekboru.jpa.satinalma.SatinalmaAmbarMalzemeTalepFormIcerik;
import com.emekboru.jpa.satinalma.SatinalmaDurum;
import com.emekboru.jpa.satinalma.SatinalmaMalzemeHizmetTalepFormu;
import com.emekboru.jpa.satinalma.SatinalmaMalzemeHizmetTalepFormuIcerik;
import com.emekboru.jpa.satinalma.SatinalmaSarfMerkezi;
import com.emekboru.jpa.stok.StokGirisCikis;
import com.emekboru.jpa.stok.StokJoinUreticiKoduBarkod;
import com.emekboru.jpa.stok.StokTanimlarBirim;
import com.emekboru.jpaman.satinalma.SatinalmaAmbarMalzemeTalepFormIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaAmbarMalzemeTalepFormManager;
import com.emekboru.jpaman.satinalma.SatinalmaDurumManager;
import com.emekboru.jpaman.satinalma.SatinalmaMalzemeHizmetTalepFormuIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaMalzemeHizmetTalepFormuManager;
import com.emekboru.jpaman.satinalma.SatinalmaSarfMerkeziManager;
import com.emekboru.jpaman.stok.StokGirisCikisManager;
import com.emekboru.jpaman.stok.StokTanimlarBirimManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;
import com.emekboru.utils.lazymodels.LazyStokDataModel;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "stokGirisCikisBean")
@ViewScoped
public class StokGirisCikisBean implements Serializable {

	private static final long serialVersionUID = 1L;

	// private List<StokGirisCikis> allStokGirisCikisList = new
	// ArrayList<StokGirisCikis>();
	private LazyDataModel<StokGirisCikis> allStokGirisCikisLazyList;

	private List<StokGirisCikis> stokGirisCikisListesi = new ArrayList<StokGirisCikis>();
	private LazyDataModel<StokGirisCikis> stokGirisCikisLazyListesi;

	private StokGirisCikis stokGirisCikisForm = new StokGirisCikis();
	private StokJoinUreticiKoduBarkod stokJoinUreticiKoduBarkod = new StokJoinUreticiKoduBarkod();

	private SatinalmaAmbarMalzemeTalepForm satinalmaAmbarMalzemeTalepForm = new SatinalmaAmbarMalzemeTalepForm();
	private SatinalmaAmbarMalzemeTalepFormIcerik satinalmaAmbarMalzemeTalepFormIcerik = new SatinalmaAmbarMalzemeTalepFormIcerik();
	private List<SatinalmaAmbarMalzemeTalepFormIcerik> allSatinalmaAmbarMalzemeTalepFormIcerikList = new ArrayList<SatinalmaAmbarMalzemeTalepFormIcerik>();

	private SatinalmaMalzemeHizmetTalepFormu satinalmaMalzemeHizmetTalepFormu = new SatinalmaMalzemeHizmetTalepFormu();
	private SatinalmaMalzemeHizmetTalepFormuIcerik satinalmaMalzemeHizmetTalepFormuIcerik = new SatinalmaMalzemeHizmetTalepFormuIcerik();
	private List<SatinalmaMalzemeHizmetTalepFormuIcerik> allSatinalmaMalzemeHizmetTalepFormuIcerikList = new ArrayList<SatinalmaMalzemeHizmetTalepFormuIcerik>();

	private SatinalmaDurum satinalmaDurum = new SatinalmaDurum();
	// private StokGirisCikisManager formManager = new StokGirisCikisManager();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	public static List<SalesItem> items;
	public static List<SatinalmaAmbarMalzemeTalepForm> talepItems;

	private List<Object[]> kritikStokList = null;
	private List<Object[]> mainKritikStokList = null;
	private List<Object[]> mevcutStokList = null;
	private List<Object[]> stokHareketleriList = null;

	Long giris;
	Long cikis;
	Long toplam;

	// raporlamalar kısmı için ortaklar
	Integer evrakNo = null;
	Integer projeId = null;
	String urunAdi = null;
	Integer urunGrubu = null;
	Integer depo = null;
	String urunKodu = null;
	Integer urunJoinId = null;
	Integer sarfYeri = null;
	Integer teslimEden = null;
	Integer teslimAlan = null;
	String sorguTarihIlk = null;
	String sorguTarihSon = null;

	// ureticibarkod ve ureticikodu
	String ureticiBarkod = null;
	String ureticiKodu = null;

	String prmFormNo = null;

	public StokGirisCikisBean() {

		fillTestList();
		// fillKritikStokList();
		// son100Getir();
		// items = SalesItemConverter.salesItemDB;
		// talepItems = AmbarTalepConverter.taleplerItemDB;
	}

	protected void ambarCikisHazirla() {
		talepItems = AmbarTalepConverter.taleplerItemDB;

	}

	protected void malzemeCikisHazirla() {
		items = SalesItemConverter.salesItemDB;

	}

	public void cikisHazirla() {
		stokGirisCikisForm.setTeslimEdenKullanici(userBean.getUser()
				.getEmployee());
		stokGirisCikisForm.setTeslimAlanKullanici(stokGirisCikisForm
				.getSatinalmaAmbarMalzemeTalepForm().getTalepSahibi());
	}

	public void addForm() {

		stokGirisCikisForm = new StokGirisCikis();
		updateButtonRender = false;
	}

	public void ambarTalepGetir() {

		SatinalmaAmbarMalzemeTalepFormManager formManager = new SatinalmaAmbarMalzemeTalepFormManager();
		SatinalmaAmbarMalzemeTalepFormIcerikManager icerikManager = new SatinalmaAmbarMalzemeTalepFormIcerikManager();

		if (formManager.getSatinalmaAmbarMalzemeTalepForm(
				stokGirisCikisForm.getSatinalmaAmbarMalzemeTalepForm()
						.getFormSayisi()).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, stokGirisCikisForm
							.getSatinalmaAmbarMalzemeTalepForm()
							.getFormSayisi()
							+ " NUMARALI AMBAR TALEP FORMU SİSTEMDE YOKTUR!",
					null));
			return;
		} else {

			satinalmaAmbarMalzemeTalepForm = formManager
					.getSatinalmaAmbarMalzemeTalepForm(
							stokGirisCikisForm
									.getSatinalmaAmbarMalzemeTalepForm()
									.getFormSayisi()).get(0);
			// if (!satinalmaAmbarMalzemeTalepForm.isTamamlanma()) {
			//
			// FacesContext context = FacesContext.getCurrentInstance();
			// context.addMessage(
			// null,
			// new FacesMessage(
			// FacesMessage.SEVERITY_ERROR,
			// stokGirisCikisForm
			// .getSatinalmaAmbarMalzemeTalepForm()
			// .getFormSayisi()
			// + " NUMARALI AMBAR TALEP FORMU KİTLENMEMİŞTİR!",
			// null));
			// return;
			// }

			allSatinalmaAmbarMalzemeTalepFormIcerikList = icerikManager
					.getAllSatinalmaAmbarMalzemeTalepFormIcerik(satinalmaAmbarMalzemeTalepForm
							.getId());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(stokGirisCikisForm
					.getSatinalmaAmbarMalzemeTalepForm().getFormSayisi()
					+ " NUMARALI AMBAR TALEP FORMU SEÇİLMİŞTİR!"));

			cikisHazirla();
		}

	}

	public void malzemeTalepGetir() {

		SatinalmaMalzemeHizmetTalepFormuManager formManager = new SatinalmaMalzemeHizmetTalepFormuManager();
		SatinalmaMalzemeHizmetTalepFormuIcerikManager icerikManager = new SatinalmaMalzemeHizmetTalepFormuIcerikManager();

		if (formManager.findByField(
				SatinalmaMalzemeHizmetTalepFormu.class,
				"id",
				stokGirisCikisForm.getSatinalmaMalzemeHizmetTalepFormu()
						.getId()).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, stokGirisCikisForm
							.getSatinalmaMalzemeHizmetTalepFormu()
							.getFormSayisi()
							+ " NUMARALI MALZEME TALEP FORMU SİSTEMDE YOKTUR!",
					null));
			// fillTestList();
			sifirla();
			return;
		} else {

			satinalmaMalzemeHizmetTalepFormu = formManager.findByField(
					SatinalmaMalzemeHizmetTalepFormu.class,
					"id",
					stokGirisCikisForm.getSatinalmaMalzemeHizmetTalepFormu()
							.getId()).get(0);
			if (!satinalmaMalzemeHizmetTalepFormu.isTamamlanma()) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								stokGirisCikisForm
										.getSatinalmaAmbarMalzemeTalepForm()
										.getFormSayisi()
										+ " NUMARALI MALZEME TALEP FORMU KİTLENMEMİŞTİR!",
								null));
				// fillTestList();
				sifirla();
				return;
			} else if (satinalmaMalzemeHizmetTalepFormu.getGmApproval() == 0
					|| satinalmaMalzemeHizmetTalepFormu.getSupervisorApproval() == 0) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_FATAL,
								stokGirisCikisForm
										.getSatinalmaAmbarMalzemeTalepForm()
										.getFormSayisi()
										+ " NUMARALI MALZEME TALEP FORMU ONAYLARI EKSİKTİR!",
								null));
				// fillTestList();
				sifirla();
				return;
			}

			allSatinalmaMalzemeHizmetTalepFormuIcerikList = icerikManager
					.getAllSatinalmaMalzemeHizmetTalepFormuIcerikTrue(satinalmaMalzemeHizmetTalepFormu
							.getId());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(stokGirisCikisForm
					.getSatinalmaAmbarMalzemeTalepForm().getFormSayisi()
					+ " NUMARALI MALZEME TALEP FORMU SEÇİLMİŞTİR!"));
		}

	}

	// public void depoDurum() {
	//
	// StokGirisCikisManager formManager = new StokGirisCikisManager();
	//
	// giris = formManager.getGiris(
	// stokGirisCikisForm.getStokJoinUreticiKoduBarkod().getJoinId())
	// .get(0);
	// cikis = formManager.getCikis(
	// stokGirisCikisForm.getStokJoinUreticiKoduBarkod().getJoinId())
	// .get(0);
	// toplam = giris - cikis;
	// }

	public void depoDurum() {
		StokGirisCikisManager formManager = new StokGirisCikisManager();
		giris = formManager.getUrunGiris(stokGirisCikisForm
				.getStokJoinUreticiKoduBarkod().getStokJoinMarkaUreticiKodu()
				.getStokUrunKartlari().getId());
		cikis = formManager.getUrunCikis(stokGirisCikisForm
				.getStokJoinUreticiKoduBarkod().getStokJoinMarkaUreticiKodu()
				.getStokUrunKartlari().getId());
		toplam = giris - cikis;
	}

	public void addOrUpdateForm(ActionEvent ae) {

		String prmForm = null;
		UICommand cmd = (UICommand) ae.getComponent();
		if (cmd.getChildren().size() == 0) {
			prmForm = "G";
		} else {
			prmForm = (String) cmd.getChildren().get(0).getAttributes()
					.get("value");
		}

		StokGirisCikisManager formManager = new StokGirisCikisManager();
		SatinalmaAmbarMalzemeTalepFormIcerikManager ambarManager = new SatinalmaAmbarMalzemeTalepFormIcerikManager();
		// SatinalmaMalzemeHizmetTalepFormuIcerikManager malzemeManager = new
		// SatinalmaMalzemeHizmetTalepFormuIcerikManager();
		SatinalmaSarfMerkeziManager sarfManager = new SatinalmaSarfMerkeziManager();
		StokTanimlarBirimManager birimManager = new StokTanimlarBirimManager();

		try {

			if (stokGirisCikisForm.getId() == null) {

				if (stokGirisCikisForm.getIslemDurumu().equals("0")) {
					// yetersiz stoksa
					depoDurum();
					if (prmForm.equals("A")) {
						// Ambar Talep formundan gelenler için
						if (toplam < stokGirisCikisForm.getMiktar()
								|| satinalmaAmbarMalzemeTalepFormIcerik
										.getKalan() == 0) {
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(
									null,
									new FacesMessage(
											FacesMessage.SEVERITY_FATAL,
											"YETERSİZ STOK, "
													+ satinalmaAmbarMalzemeTalepFormIcerik
															.getStokUrunKartlari()
															.getUrunAdi()
													+ " - KALAN MİKTAR = "
													+ toplam, null));

							// this.stokGirisCikisForm = new StokGirisCikis();
							// this.satinalmaAmbarMalzemeTalepForm = new
							// SatinalmaAmbarMalzemeTalepForm();
							// this.satinalmaAmbarMalzemeTalepFormIcerik = new
							// SatinalmaAmbarMalzemeTalepFormIcerik();
							this.stokGirisCikisListesi = new ArrayList<StokGirisCikis>();
							this.allSatinalmaAmbarMalzemeTalepFormIcerikList = new ArrayList<SatinalmaAmbarMalzemeTalepFormIcerik>();

							fillTestList();
							sifirla();
							fillKritikStokList();
							return;
						}

						// istenilenden fazla veriliyorsa
						if (stokGirisCikisForm.getMiktar() > satinalmaAmbarMalzemeTalepFormIcerik
								.getKalan()) {
							FacesContext context = FacesContext
									.getCurrentInstance();
							context.addMessage(
									null,
									new FacesMessage(
											FacesMessage.SEVERITY_ERROR,
											"İSTENİLENDEN FAZLA ÜRÜN VERİLEMEZ, TALEP EDİLEN MİKTAR = "
													+ satinalmaAmbarMalzemeTalepFormIcerik
															.getMiktar()
													+ ", ÇIKIŞ İSTENEN MİKTAR = "
													+ stokGirisCikisForm
															.getMiktar(), null));

							// this.stokGirisCikisForm = new StokGirisCikis();
							// this.satinalmaAmbarMalzemeTalepForm = new
							// SatinalmaAmbarMalzemeTalepForm();
							// this.satinalmaAmbarMalzemeTalepFormIcerik = new
							// SatinalmaAmbarMalzemeTalepFormIcerik();
							this.stokGirisCikisListesi = new ArrayList<StokGirisCikis>();
							this.allSatinalmaAmbarMalzemeTalepFormIcerikList = new ArrayList<SatinalmaAmbarMalzemeTalepFormIcerik>();

							// fillTestList();
							sifirla();
							fillKritikStokList();
							return;
						}

					}
					// else if (prmForm.equals("M")) {
					// // Malzeme Talep formundan gelenler için
					// if (toplam <= stokGirisCikisForm.getMiktar()
					// || satinalmaMalzemeHizmetTalepFormuIcerik
					// .getKalan() == 0) {
					// FacesContext context = FacesContext
					// .getCurrentInstance();
					// context.addMessage(
					// null,
					// new FacesMessage(
					// FacesMessage.SEVERITY_ERROR,
					// "YETERSİZ STOK, "
					// + satinalmaMalzemeHizmetTalepFormuIcerik
					// .getStokUrunKartlari()
					// .getUrunAdi()
					// + " - KALAN MİKTAR = "
					// + toplam, null));
					//
					// this.stokGirisCikisForm = new StokGirisCikis();
					// this.satinalmaMalzemeHizmetTalepFormu = new
					// SatinalmaMalzemeHizmetTalepFormu();
					// this.satinalmaMalzemeHizmetTalepFormuIcerik = new
					// SatinalmaMalzemeHizmetTalepFormuIcerik();
					// this.stokGirisCikisListesi = new
					// ArrayList<StokGirisCikis>();
					// this.allSatinalmaMalzemeHizmetTalepFormuIcerikList = new
					// ArrayList<SatinalmaMalzemeHizmetTalepFormuIcerik>();
					//
					// // fillTestList();
					// sifirla();
					// fillKritikStokList();
					// return;
					// }
					//
					// // istenilenden fazla veriliyorsa
					// if (stokGirisCikisForm.getMiktar() >
					// satinalmaMalzemeHizmetTalepFormuIcerik
					// .getKalan()) {
					// FacesContext context = FacesContext
					// .getCurrentInstance();
					// context.addMessage(
					// null,
					// new FacesMessage(
					// FacesMessage.SEVERITY_ERROR,
					// "İSTENİLENDEN FAZLA ÜRÜN VERİLEMEZ, TALEP EDİLEN MİKTAR = "
					// + satinalmaMalzemeHizmetTalepFormuIcerik
					// .getMiktar()
					// + ", ÇIKIŞ İSTENEN MİKTAR = "
					// + stokGirisCikisForm
					// .getMiktar(), null));
					//
					// this.stokGirisCikisForm = new StokGirisCikis();
					// this.satinalmaMalzemeHizmetTalepFormu = new
					// SatinalmaMalzemeHizmetTalepFormu();
					// this.satinalmaMalzemeHizmetTalepFormuIcerik = new
					// SatinalmaMalzemeHizmetTalepFormuIcerik();
					// this.stokGirisCikisListesi = new
					// ArrayList<StokGirisCikis>();
					// this.allSatinalmaAmbarMalzemeTalepFormIcerikList = new
					// ArrayList<SatinalmaAmbarMalzemeTalepFormIcerik>();
					//
					// // fillTestList();
					// sifirla();
					// fillKritikStokList();
					// return;
					// }
					// }
				}

				stokGirisCikisForm.setStokUrunKartlari(stokGirisCikisForm
						.getStokJoinUreticiKoduBarkod()
						.getStokJoinMarkaUreticiKodu().getStokUrunKartlari());
				stokGirisCikisForm.setEklemeZamani(UtilInsCore.getTarihZaman());
				stokGirisCikisForm.setEkleyenKullanici(userBean.getUser()
						.getId());
				// cikiş işlemi ise
				if (satinalmaAmbarMalzemeTalepFormIcerik.getId() != null) {
					// sarf merkezini istekten getiriyor
					stokGirisCikisForm
							.setSatinalmaSarfMerkezi(sarfManager.loadObject(
									SatinalmaSarfMerkezi.class,
									satinalmaAmbarMalzemeTalepFormIcerik
											.getSatinalmaSarfMerkezi().getId()));
				}
				// birimi üründen getiriyor
				stokGirisCikisForm.setStokTanimlarBirim(birimManager
						.loadObject(StokTanimlarBirim.class, stokGirisCikisForm
								.getStokUrunKartlari().getStokTanimlarBirim()
								.getId()));

				formManager.enterNew(stokGirisCikisForm);

				if (prmForm.equals("A")) {
					if ((stokGirisCikisForm.getIslemDurumu().equals("0"))) {
						Integer kalan = satinalmaAmbarMalzemeTalepFormIcerik
								.getKalan() - stokGirisCikisForm.getMiktar();
						satinalmaAmbarMalzemeTalepFormIcerik.setKalan(kalan);
						ambarManager
								.updateEntity(satinalmaAmbarMalzemeTalepFormIcerik);
						talepDurumDuzenle();
					}
				}
				// else if (prmForm.equals("M")) {
				// if ((stokGirisCikisForm.getIslemDurumu() == 0)) {
				// Integer kalan = satinalmaMalzemeHizmetTalepFormuIcerik
				// .getKalan() - stokGirisCikisForm.getMiktar();
				// satinalmaMalzemeHizmetTalepFormuIcerik.setKalan(kalan);
				// malzemeManager
				// .updateEntity(satinalmaMalzemeHizmetTalepFormuIcerik);
				// talepDurumDuzenle();
				// }
				// }

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage("ÜRÜN HAREKETİ TAMAMLANDI, "
								+ stokGirisCikisForm
										.getStokJoinUreticiKoduBarkod()
										.getStokJoinMarkaUreticiKodu()
										.getStokUrunKartlari().getUrunAdi()
								+ " - MİKTAR = "
								+ stokGirisCikisForm.getMiktar() + " BİRİM"));

			} else {

				stokGirisCikisForm.setGuncellemeZamani(UtilInsCore
						.getTarihZaman());
				stokGirisCikisForm.setGuncelleyenKullanici(userBean.getUser()
						.getId());
				formManager.updateEntity(stokGirisCikisForm);

				FacesContextUtils.addInfoMessage("FormUpdateMessage");
			}
		} catch (Exception e) {
			System.out.println("StokGirisCikisBean() - addOrUpdateForm(): "
					+ e.toString());
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "HATA , "
							+ satinalmaMalzemeHizmetTalepFormuIcerik
									.getStokUrunKartlari().getUrunAdi(), null));
		}

		// this.stokGirisCikisForm = new StokGirisCikis();
		// this.satinalmaAmbarMalzemeTalepForm = new
		// SatinalmaAmbarMalzemeTalepForm();
		// this.satinalmaAmbarMalzemeTalepFormIcerik = new
		// SatinalmaAmbarMalzemeTalepFormIcerik();
		// this.satinalmaMalzemeHizmetTalepFormu = new
		// SatinalmaMalzemeHizmetTalepFormu();
		// this.satinalmaMalzemeHizmetTalepFormuIcerik = new
		// SatinalmaMalzemeHizmetTalepFormuIcerik();
		// this.stokGirisCikisListesi = new ArrayList<StokGirisCikis>();
		// this.allSatinalmaAmbarMalzemeTalepFormIcerikList = new
		// ArrayList<SatinalmaAmbarMalzemeTalepFormIcerik>();

		this.stokGirisCikisForm.setId(null);

		// fillTestList();
		fillKritikStokList();

	}

	public void talepDurumDuzenle() {

		SatinalmaDurumManager durumManager = new SatinalmaDurumManager();
		SatinalmaAmbarMalzemeTalepFormManager talepMan = new SatinalmaAmbarMalzemeTalepFormManager();

		// depo çıkışıyla ambarTalep kitlenir
		satinalmaAmbarMalzemeTalepForm.setTamamlanma(true);

		Boolean render = true;

		for (int i = 0; i < allSatinalmaAmbarMalzemeTalepFormIcerikList.size(); i++) {
			if (allSatinalmaAmbarMalzemeTalepFormIcerikList.get(i).getKalan() != 0) {
				render = false;
				break;
			}
		}
		if (render) {
			// kapandı
			satinalmaDurum = durumManager.loadObject(SatinalmaDurum.class, 14);
			satinalmaAmbarMalzemeTalepForm.setSatinalmaDurum(satinalmaDurum);
			talepMan.updateEntity(satinalmaAmbarMalzemeTalepForm);
		} else {
			// kısmı tamamlandı
			satinalmaDurum = durumManager.loadObject(SatinalmaDurum.class, 3);
			satinalmaAmbarMalzemeTalepForm.setSatinalmaDurum(satinalmaDurum);
			talepMan.updateEntity(satinalmaAmbarMalzemeTalepForm);
		}
	}

	public void deleteForm() {

		StokGirisCikisManager formManager = new StokGirisCikisManager();

		try {

			if (stokGirisCikisForm.getId() == null) {

				FacesContextUtils.addWarnMessage("NoSelectMessage");
				return;
			}
			formManager.delete(stokGirisCikisForm);
			// fillTestList();
			sifirla();
			FacesContextUtils.addWarnMessage("TestDeleteMessage");
		} catch (Exception e) {
			System.out.println("StokGirisCikisBean() - deleteForm(): "
					+ e.toString());
		}
	}

	// public void fillTestList() {
	//
	// Thread thread = new Thread() {
	//
	// @Override
	// public void run() {
	//
	// StokGirisCikisManager manager = new StokGirisCikisManager();
	// allStokGirisCikisList = manager.getAllStokGirisCikis();
	// stokGirisCikisListesi = allStokGirisCikisList;
	// }
	//
	// };
	// thread.run();
	//
	// }

	public void fillTestList() {

		allStokGirisCikisLazyList = new LazyStokDataModel();
		stokGirisCikisLazyListesi = allStokGirisCikisLazyList;

	}

	// public void son100Getir() {
	//
	// Thread thread = new Thread() {
	//
	// @Override
	// public void run() {
	//
	// StokGirisCikisManager manager = new StokGirisCikisManager();
	// allStokGirisCikisList = manager.findLimited();
	// stokGirisCikisListesi = allStokGirisCikisList;
	// }
	//
	// };
	// thread.run();
	//
	// }

	public void sifirla() {

		stokGirisCikisForm = new StokGirisCikis();
		stokJoinUreticiKoduBarkod = new StokJoinUreticiKoduBarkod();
		satinalmaAmbarMalzemeTalepForm = new SatinalmaAmbarMalzemeTalepForm();
		satinalmaAmbarMalzemeTalepFormIcerik = new SatinalmaAmbarMalzemeTalepFormIcerik();
		allSatinalmaAmbarMalzemeTalepFormIcerikList.clear();
		satinalmaMalzemeHizmetTalepFormu = new SatinalmaMalzemeHizmetTalepFormu();
		satinalmaMalzemeHizmetTalepFormuIcerik = new SatinalmaMalzemeHizmetTalepFormuIcerik();
		allSatinalmaMalzemeHizmetTalepFormuIcerikList.clear();
	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	// kritikMinStok listesi
	public void fillKritikStokList() {

		Thread thread = new Thread() {

			@Override
			public void run() {

				Long toplamMiktar = 0L;
				Integer minStok = 0;
				// sorgudan gelen listenin toplam miktarı minstokdan buyuk
				// eşitse
				// listeden cıkarılıyor, boylece sadece minStok altı veriler
				// kalıyor.
				try {
					StokGirisCikisManager manager = new StokGirisCikisManager();
					kritikStokList = manager.getKritikStokManuelSQL();
				} catch (Exception e) {
					System.out
							.println("HATA - kritikStokList - StokGirisCikisBean.fillKritikStokList");
					e.printStackTrace();
				}
				try {
					StokGirisCikisManager manager = new StokGirisCikisManager();
					mainKritikStokList = manager.getKritikStokManuelSQL();
				} catch (Exception e) {
					System.out
							.println("HATA - mainKritikStokList - StokGirisCikisBean.fillKritikStokList");
					e.printStackTrace();
				}

				try {
					StokGirisCikisManager manager = new StokGirisCikisManager();
					mevcutStokList = manager.getKritikStokManuelSQL();
				} catch (Exception e) {
					System.out
							.println("HATA - mainKritikStokList - StokGirisCikisBean.fillKritikStokList");
					e.printStackTrace();
				}

				mainKritikStokList.clear();
				mevcutStokList.clear();

				for (int i = 0; i < kritikStokList.size(); i++) {
					Object[] object = kritikStokList.get(i);
					toplamMiktar = (Long) object[6];
					minStok = (Integer) object[7];
					if (toplamMiktar <= minStok) {
						mainKritikStokList.add(kritikStokList.get(i));

					}
				}

				for (int i = 0; i < kritikStokList.size(); i++) {
					Object[] object = kritikStokList.get(i);
					toplamMiktar = (Long) object[6];
					minStok = (Integer) object[7];
					if (toplamMiktar > 0) {
						mevcutStokList.add(kritikStokList.get(i));

					}
				}

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO, "LİSTE YENİLENMİŞTİR!",
						null));

			}

		};
		thread.run();
	}

	@SuppressWarnings("deprecation")
	public void stokRaporListeleriDoldur(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		String prmRaporType = (String) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (stokGirisCikisForm.getSorguIlkTarih() != null
				&& stokGirisCikisForm.getSorguSonTarih() != null) {
			sorguTarihIlk = Integer.toString(stokGirisCikisForm
					.getSorguIlkTarih().getYear() + 1900)
					+ "/"
					+ Integer.toString(stokGirisCikisForm.getSorguIlkTarih()
							.getMonth() + 1)
					+ "/"
					+ Integer.toString(stokGirisCikisForm.getSorguIlkTarih()
							.getDate())
					+ " "
					+ Integer.toString(stokGirisCikisForm.getSorguIlkTarih()
							.getHours())
					+ ":"
					+ Integer.toString(stokGirisCikisForm.getSorguIlkTarih()
							.getMinutes());
			sorguTarihSon = Integer.toString(stokGirisCikisForm
					.getSorguSonTarih().getYear() + 1900)
					+ "/"
					+ Integer.toString(stokGirisCikisForm.getSorguSonTarih()
							.getMonth() + 1)
					+ "/"
					+ Integer.toString(stokGirisCikisForm.getSorguSonTarih()
							.getDate())
					+ " "
					+ Integer.toString(stokGirisCikisForm.getSorguSonTarih()
							.getHours())
					+ ":"
					+ Integer.toString(stokGirisCikisForm.getSorguSonTarih()
							.getMinutes());
		} else {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL,
					"TARİH SEÇİNİZ, TEKRAR DENEYİNİZ!", null));
			return;
		}
		if (stokGirisCikisForm.getStokTanimlarDepoTanimlari() != null) {
			depo = stokGirisCikisForm.getStokTanimlarDepoTanimlari().getId();
		}
		// if (stokGirisCikisForm.getStokUrunKartlari().getUrunKodu() != null) {
		// urunKodu = stokGirisCikisForm.getStokUrunKartlari().getUrunKodu();
		// }
		if (stokGirisCikisForm.getStokJoinUreticiKoduBarkod()
				.getStokJoinMarkaUreticiKodu().getStokUrunKartlari()
				.getUrunKodu() != null) {
			urunKodu = stokGirisCikisForm.getStokJoinUreticiKoduBarkod()
					.getStokJoinMarkaUreticiKodu().getStokUrunKartlari()
					.getUrunKodu();
		}
		// if (stokGirisCikisForm.getStokTanimlarSarfYeri().getId() != null) {
		// sarfYeri = stokGirisCikisForm.getStokTanimlarSarfYeri().getId();
		// }
		if (stokGirisCikisForm.getSatinalmaSarfMerkezi().getId() != null) {
			sarfYeri = stokGirisCikisForm.getSatinalmaSarfMerkezi().getId();
		}
		if (stokGirisCikisForm.getTeslimAlanKullanici() != null) {
			teslimAlan = stokGirisCikisForm.getTeslimAlanKullanici()
					.getEmployeeId();
		}
		if (stokGirisCikisForm.getTeslimEdenKullanici() != null) {
			teslimEden = stokGirisCikisForm.getTeslimEdenKullanici()
					.getEmployeeId();
		}// hareket listesine özel
		if (stokGirisCikisForm.getSalesItem() != null) {
			projeId = stokGirisCikisForm.getSalesItem().getItemId();
		}
		if (stokGirisCikisForm.getSatinalmaAmbarMalzemeTalepForm() != null) {
			evrakNo = stokGirisCikisForm.getSatinalmaAmbarMalzemeTalepForm()
					.getId();
		}
		// if (stokGirisCikisForm.getStokUrunKartlari().getUrunAdi() != null) {
		// urunAdi = stokGirisCikisForm.getStokUrunKartlari().getUrunAdi();
		// }
		if (stokGirisCikisForm.getStokJoinUreticiKoduBarkod()
				.getStokJoinMarkaUreticiKodu().getStokUrunKartlari()
				.getUrunAdi() != null) {
			urunAdi = stokGirisCikisForm.getStokJoinUreticiKoduBarkod()
					.getStokJoinMarkaUreticiKodu().getStokUrunKartlari()
					.getUrunAdi();
		}
		// if (stokGirisCikisForm.getStokUrunKartlari().getUrunKodu() != null) {
		// urunKodu = stokGirisCikisForm.getStokUrunKartlari().getUrunKodu();
		// }
		if (stokGirisCikisForm.getStokJoinUreticiKoduBarkod()
				.getStokJoinMarkaUreticiKodu().getStokUrunKartlari()
				.getUrunKodu() != null) {
			urunKodu = stokGirisCikisForm.getStokJoinUreticiKoduBarkod()
					.getStokJoinMarkaUreticiKodu().getStokUrunKartlari()
					.getUrunKodu();
		}
		// if (stokGirisCikisForm.getStokTanimlarSarfYeri().getId() != null) {
		// sarfYeri = stokGirisCikisForm.getStokTanimlarSarfYeri().getId();
		// }
		if (stokGirisCikisForm.getSatinalmaSarfMerkezi().getId() != null) {
			sarfYeri = stokGirisCikisForm.getSatinalmaSarfMerkezi().getId();
		}
		// if
		// (stokGirisCikisForm.getStokUrunKartlari().getStokTanimlarUrunGrubu()
		// .getId() != null) {
		// urunGrubu = stokGirisCikisForm.getStokUrunKartlari()
		// .getStokTanimlarUrunGrubu().getId();
		// }
		if (stokGirisCikisForm.getStokJoinUreticiKoduBarkod()
				.getStokJoinMarkaUreticiKodu().getStokUrunKartlari()
				.getStokTanimlarUrunGrubu().getId() != null) {
			urunGrubu = stokGirisCikisForm.getStokJoinUreticiKoduBarkod()
					.getStokJoinMarkaUreticiKodu().getStokUrunKartlari()
					.getStokTanimlarUrunGrubu().getId();
		}

		if (prmRaporType.equals("1")) {// giriscikislistesi
			StokGirisCikisManager manager = new StokGirisCikisManager();
			stokGirisCikisListesi = manager
					.getStokGirisCikisListesi(sorguTarihIlk, sorguTarihSon,
							depo, stokGirisCikisForm.getIslemDurumu(),
							teslimEden, teslimAlan);
		} else if (prmRaporType.equals("2")) {// hareketlistesi
			StokGirisCikisManager manager = new StokGirisCikisManager();
			stokGirisCikisListesi = manager.getHareketListesi(projeId, evrakNo,
					urunAdi, urunKodu, sorguTarihIlk, sorguTarihSon, depo,
					stokGirisCikisForm.getIslemDurumu(), sarfYeri, urunGrubu);
		} else if (prmRaporType.equals("3")) {// durumraporu
			StokGirisCikisManager manager = new StokGirisCikisManager();
			stokGirisCikisListesi = manager.getDurumRaporu(sorguTarihIlk,
					sorguTarihSon, depo, stokGirisCikisForm.getIslemDurumu(),
					teslimEden, teslimAlan);
		}
	}

	@SuppressWarnings("deprecation")
	public void stokHareketleriListesi() {
		if (stokGirisCikisForm.getSorguIlkTarih() != null
				&& stokGirisCikisForm.getSorguSonTarih() != null) {
			sorguTarihIlk = Integer.toString(stokGirisCikisForm
					.getSorguIlkTarih().getYear() + 1900)
					+ "/"
					+ Integer.toString(stokGirisCikisForm.getSorguIlkTarih()
							.getMonth() + 1)
					+ "/"
					+ Integer.toString(stokGirisCikisForm.getSorguIlkTarih()
							.getDate());
			sorguTarihSon = Integer.toString(stokGirisCikisForm
					.getSorguSonTarih().getYear() + 1900)
					+ "/"
					+ Integer.toString(stokGirisCikisForm.getSorguSonTarih()
							.getMonth() + 1)
					+ "/"
					+ Integer.toString(stokGirisCikisForm.getSorguSonTarih()
							.getDate());
		} else {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL,
					"TARİH SEÇİNİZ, TEKRAR DENEYİNİZ!", null));
			return;
		}

		Long toplamMiktar = 0L;
		Integer minStok = 0;
		// sorgudan gelen listenin toplam miktarı minstokdan buyuk eşitse
		// listeden cıkarılıyor, boylece sadece minStok altı veriler kalıyor.

		try {

			StokGirisCikisManager manager = new StokGirisCikisManager();
			kritikStokList = manager.getStokHareketListManualSQL(sorguTarihIlk,
					sorguTarihSon);
		} catch (Exception e) {
			System.out
					.println("HATA - kritikStokList - StokGirisCikisBean.stokHareketleriListesi");
			e.printStackTrace();
		}

		try {

			StokGirisCikisManager manager = new StokGirisCikisManager();
			stokHareketleriList = manager.getStokHareketListManualSQL(
					sorguTarihIlk, sorguTarihSon);
		} catch (Exception e) {
			System.out
					.println("HATA - stokHareketleriList - StokGirisCikisBean.stokHareketleriListesi");
			e.printStackTrace();
		}

		stokHareketleriList.clear();

		for (int i = 0; i < kritikStokList.size(); i++) {
			Object[] object = kritikStokList.get(i);
			toplamMiktar = (Long) object[6];
			minStok = (Integer) object[7];
			if (toplamMiktar <= minStok) {
				stokHareketleriList.add(kritikStokList.get(i));

			}
		}
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				"LİSTE YENİLENMİŞTİR!", null));
	}

	public void stokHareketListesiDoldur() {

	}

	public void onRowSelect(SelectEvent event) {

		stokGirisCikisForm = (StokGirisCikis) event.getObject();

	}

	public void onRowEdit(RowEditEvent event) {

		StokGirisCikisManager hareketManager = new StokGirisCikisManager();
		hareketManager.updateEntity(((StokGirisCikis) event.getObject()));

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				"BİLGİLER DEĞİŞTİRİLMİŞTİR!", null));
	}

	// setters getters
	// public List<StokGirisCikis> getAllStokGirisCikisList() {
	// return allStokGirisCikisList;
	// }
	//
	// public void setAllStokGirisCikisList(
	// List<StokGirisCikis> allStokGirisCikisList) {
	// this.allStokGirisCikisList = allStokGirisCikisList;
	// }

	public StokGirisCikis getStokGirisCikisForm() {
		return stokGirisCikisForm;
	}

	public void setStokGirisCikisForm(StokGirisCikis stokGirisCikisForm) {
		this.stokGirisCikisForm = stokGirisCikisForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public List<StokGirisCikis> getStokGirisCikisListesi() {
		return stokGirisCikisListesi;
	}

	public void setStokGirisCikisListesi(
			List<StokGirisCikis> stokGirisCikisListesi) {
		this.stokGirisCikisListesi = stokGirisCikisListesi;
	}

	public static List<SalesItem> getItems() {
		return items;
	}

	public static void setItems(List<SalesItem> items) {
		StokGirisCikisBean.items = items;
	}

	public List<Object[]> getKritikStokList() {
		return kritikStokList;
	}

	public void setKritikStokList(List<Object[]> kritikStokList) {
		this.kritikStokList = kritikStokList;
	}

	public Long getGiris() {
		return giris;
	}

	public void setGiris(Long giris) {
		this.giris = giris;
	}

	public Long getCikis() {
		return cikis;
	}

	public void setCikis(Long cikis) {
		this.cikis = cikis;
	}

	public Long getToplam() {
		return toplam;
	}

	public void setToplam(Long toplam) {
		this.toplam = toplam;
	}

	public String getUreticiBarkod() {
		return ureticiBarkod;
	}

	public void setUreticiBarkod(String ureticiBarkod) {
		this.ureticiBarkod = ureticiBarkod;
	}

	public String getUreticiKodu() {
		return ureticiKodu;
	}

	public void setUreticiKodu(String ureticiKodu) {
		this.ureticiKodu = ureticiKodu;
	}

	public StokJoinUreticiKoduBarkod getStokJoinUreticiKoduBarkod() {
		return stokJoinUreticiKoduBarkod;
	}

	public void setStokJoinUreticiKoduBarkod(
			StokJoinUreticiKoduBarkod stokJoinUreticiKoduBarkod) {
		this.stokJoinUreticiKoduBarkod = stokJoinUreticiKoduBarkod;
	}

	public SatinalmaAmbarMalzemeTalepForm getSatinalmaAmbarMalzemeTalepForm() {
		return satinalmaAmbarMalzemeTalepForm;
	}

	public void setSatinalmaAmbarMalzemeTalepForm(
			SatinalmaAmbarMalzemeTalepForm satinalmaAmbarMalzemeTalepForm) {
		this.satinalmaAmbarMalzemeTalepForm = satinalmaAmbarMalzemeTalepForm;
	}

	public List<SatinalmaAmbarMalzemeTalepFormIcerik> getAllSatinalmaAmbarMalzemeTalepFormIcerikList() {
		return allSatinalmaAmbarMalzemeTalepFormIcerikList;
	}

	public void setAllSatinalmaAmbarMalzemeTalepFormIcerikList(
			List<SatinalmaAmbarMalzemeTalepFormIcerik> allSatinalmaAmbarMalzemeTalepFormIcerikList) {
		this.allSatinalmaAmbarMalzemeTalepFormIcerikList = allSatinalmaAmbarMalzemeTalepFormIcerikList;
	}

	public String getPrmFormNo() {
		return prmFormNo;
	}

	public void setPrmFormNo(String prmFormNo) {
		this.prmFormNo = prmFormNo;
	}

	public SatinalmaAmbarMalzemeTalepFormIcerik getSatinalmaAmbarMalzemeTalepFormIcerik() {
		return satinalmaAmbarMalzemeTalepFormIcerik;
	}

	public void setSatinalmaAmbarMalzemeTalepFormIcerik(
			SatinalmaAmbarMalzemeTalepFormIcerik satinalmaAmbarMalzemeTalepFormIcerik) {
		this.satinalmaAmbarMalzemeTalepFormIcerik = satinalmaAmbarMalzemeTalepFormIcerik;
	}

	public SatinalmaMalzemeHizmetTalepFormu getSatinalmaMalzemeHizmetTalepFormu() {
		return satinalmaMalzemeHizmetTalepFormu;
	}

	public void setSatinalmaMalzemeHizmetTalepFormu(
			SatinalmaMalzemeHizmetTalepFormu satinalmaMalzemeHizmetTalepFormu) {
		this.satinalmaMalzemeHizmetTalepFormu = satinalmaMalzemeHizmetTalepFormu;
	}

	public List<SatinalmaMalzemeHizmetTalepFormuIcerik> getAllSatinalmaMalzemeHizmetTalepFormuIcerikList() {
		return allSatinalmaMalzemeHizmetTalepFormuIcerikList;
	}

	public void setAllSatinalmaMalzemeHizmetTalepFormuIcerikList(
			List<SatinalmaMalzemeHizmetTalepFormuIcerik> allSatinalmaMalzemeHizmetTalepFormuIcerikList) {
		this.allSatinalmaMalzemeHizmetTalepFormuIcerikList = allSatinalmaMalzemeHizmetTalepFormuIcerikList;
	}

	public SatinalmaMalzemeHizmetTalepFormuIcerik getSatinalmaMalzemeHizmetTalepFormuIcerik() {
		return satinalmaMalzemeHizmetTalepFormuIcerik;
	}

	public void setSatinalmaMalzemeHizmetTalepFormuIcerik(
			SatinalmaMalzemeHizmetTalepFormuIcerik satinalmaMalzemeHizmetTalepFormuIcerik) {
		this.satinalmaMalzemeHizmetTalepFormuIcerik = satinalmaMalzemeHizmetTalepFormuIcerik;
	}

	/**
	 * @return the mainKritikStokList
	 */
	public List<Object[]> getMainKritikStokList() {
		return mainKritikStokList;
	}

	/**
	 * @param mainKritikStokList
	 *            the mainKritikStokList to set
	 */
	public void setMainKritikStokList(List<Object[]> mainKritikStokList) {
		this.mainKritikStokList = mainKritikStokList;
	}

	/**
	 * @return the mevcutStokList
	 */
	public List<Object[]> getMevcutStokList() {
		return mevcutStokList;
	}

	/**
	 * @param mevcutStokList
	 *            the mevcutStokList to set
	 */
	public void setMevcutStokList(List<Object[]> mevcutStokList) {
		this.mevcutStokList = mevcutStokList;
	}

	/**
	 * @return the stokHareketleriList
	 */
	public List<Object[]> getStokHareketleriList() {
		return stokHareketleriList;
	}

	/**
	 * @param stokHareketleriList
	 *            the stokHareketleriList to set
	 */
	public void setStokHareketleriList(List<Object[]> stokHareketleriList) {
		this.stokHareketleriList = stokHareketleriList;
	}

	/**
	 * @return the allStokGirisCikisLazyList
	 */
	public LazyDataModel<StokGirisCikis> getAllStokGirisCikisLazyList() {
		return allStokGirisCikisLazyList;
	}

	/**
	 * @param allStokGirisCikisLazyList
	 *            the allStokGirisCikisLazyList to set
	 */
	public void setAllStokGirisCikisLazyList(
			LazyDataModel<StokGirisCikis> allStokGirisCikisLazyList) {
		this.allStokGirisCikisLazyList = allStokGirisCikisLazyList;
	}

	/**
	 * @return the stokGirisCikisLazyListesi
	 */
	public LazyDataModel<StokGirisCikis> getStokGirisCikisLazyListesi() {
		return stokGirisCikisLazyListesi;
	}

	/**
	 * @param stokGirisCikisLazyListesi
	 *            the stokGirisCikisLazyListesi to set
	 */
	public void setStokGirisCikisLazyListesi(
			LazyDataModel<StokGirisCikis> stokGirisCikisLazyListesi) {
		this.stokGirisCikisLazyListesi = stokGirisCikisLazyListesi;
	}

}
