/**
 * 
 */
package com.emekboru.beans.satinalma;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.converters.SatinalmaMusteriConverter;
import com.emekboru.jpa.satinalma.SatinalmaMusteri;
import com.emekboru.jpa.satinalma.SatinalmaTakipDosyalar;
import com.emekboru.jpa.satinalma.SatinalmaTakipDosyalarIcerik;
import com.emekboru.jpa.satinalma.SatinalmaTeklifIsteme;
import com.emekboru.jpa.satinalma.SatinalmaTeklifIstemeIcerik;
import com.emekboru.jpa.stok.StokTanimlarBirim;
import com.emekboru.jpaman.satinalma.SatinalmaTakipDosyalarIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaTakipDosyalarManager;
import com.emekboru.jpaman.satinalma.SatinalmaTeklifIstemeIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaTeklifIstemeManager;
import com.emekboru.jpaman.stok.StokTanimlarBirimManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "satinalmaTeklifIstemeBean")
@ViewScoped
public class SatinalmaTeklifIstemeBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<SatinalmaTeklifIsteme> allSatinalmaTeklifIstemeList = new ArrayList<SatinalmaTeklifIsteme>();
	private List<SatinalmaTeklifIsteme> satinalmaTeklifIstemeByTakipDosyaList = new ArrayList<SatinalmaTeklifIsteme>();
	// dosya içerik
	private List<SatinalmaTakipDosyalarIcerik> satinalmaTakipDosyalarIcerikList = new ArrayList<SatinalmaTakipDosyalarIcerik>();

	private SatinalmaTeklifIsteme satinalmaTeklifIstemeForm = new SatinalmaTeklifIsteme();
	private SatinalmaTeklifIstemeIcerik satinalmaTeklifIstemeIcerikForm = new SatinalmaTeklifIstemeIcerik();
	private SatinalmaTakipDosyalar satinalmaTakipDosyalarForm = new SatinalmaTakipDosyalar();

	// private SatinalmaMalzemeHizmetTalepFormuIcerik
	// satinalmaMalzemeHizmetTalepFormuIcerik = new
	// SatinalmaMalzemeHizmetTalepFormuIcerik();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	@ManagedProperty(value = "#{satinalmaSatinalmaKarariBean}")
	private SatinalmaSatinalmaKarariBean satinalmaSatinalmaKarariBean;

	private boolean updateButtonRender;

	private Integer prmDosyaId = null;

	// // MalzemeHizmetten gelen veri için
	// private SatinalmaTeklifIsteme newTeklif = new SatinalmaTeklifIsteme();

	public List<SatinalmaMusteri> firmaItems;

	public SatinalmaTeklifIstemeBean() {

		// fillTestList();
		firmaItems = SatinalmaMusteriConverter.firmalarDB;
	}

	public void addForm() {

		satinalmaTeklifIstemeForm = new SatinalmaTeklifIsteme();
		updateButtonRender = false;
	}

	public void addTedaikci() {

		satinalmaTeklifIstemeForm.setSatinalmaMusteri(new SatinalmaMusteri());
	}

	public void addOrUpdateForm(ActionEvent ae) {

		UICommand cmd = (UICommand) ae.getComponent();

		prmDosyaId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		SatinalmaTeklifIstemeManager formManager = new SatinalmaTeklifIstemeManager();
		SatinalmaTakipDosyalarManager dosyaManager = new SatinalmaTakipDosyalarManager();

		FacesContext context = FacesContext.getCurrentInstance();

		if (satinalmaTeklifIstemeForm.getId() == null) {

			satinalmaTeklifIstemeForm.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			satinalmaTeklifIstemeForm.setEkleyenKullanici(userBean.getUser()
					.getId());
			sayiBul();
			satinalmaTeklifIstemeForm.setSatinalmaTakipDosyalar(dosyaManager
					.loadObject(SatinalmaTakipDosyalar.class, prmDosyaId));

			formManager.enterNew(satinalmaTeklifIstemeForm);

			// dosya içeriği otomatik olarak teklife ekleniyor
			teklifIcerikEkle();

			this.satinalmaTeklifIstemeForm = new SatinalmaTeklifIsteme();

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"TEKLİF BAŞARIYLA EKLENMİŞTİR!", null));

		} else {

			satinalmaTeklifIstemeForm.setGuncellemeZamani(UtilInsCore
					.getTarihZaman());
			satinalmaTeklifIstemeForm.setGuncelleyenKullanici(userBean
					.getUser().getId());

			// Teklif Şirkete Gönderilince tarih ekleme.
			try {
				if (satinalmaTeklifIstemeForm.getSatinalmaDurum().getTitle()
						.toLowerCase().contains("gönderildi")) {
					satinalmaTeklifIstemeForm.setGondermeTarihi(UtilInsCore
							.getTarihZaman());

					context.addMessage(null, new FacesMessage(
							FacesMessage.SEVERITY_INFO,
							"TEKLİF BAŞARIYLA ŞİRKETE GÖNDERİLMİŞTİR!", null));
				}

			} catch (Exception e) {
				System.out
						.println("SatinalmaTeklifIstemeBean: " + e.toString());
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"TEKLİF ŞİRKETE GÖNDERİLEMEDİ, HATA!", null));
			}

			// Teklif Şirketten Gelince tarih ekleme.
			try {
				if (satinalmaTeklifIstemeForm.getSatinalmaDurum().getTitle()
						.toLowerCase().contains("şirket")) {
					satinalmaTeklifIstemeForm.setTeklifTarihi(UtilInsCore
							.getTarihZaman());

					context.addMessage(null, new FacesMessage(
							FacesMessage.SEVERITY_INFO,
							"TEKLİF BAŞARIYLA ŞİRKETTEN GELMİŞTİR!", null));
				}

			} catch (Exception e) {
				System.out
						.println("SatinalmaTeklifIstemeBean: " + e.toString());
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"TEKLİF ŞİRKETTEN ALINAMADI, HATA!", null));
			}

			formManager.updateEntity(satinalmaTeklifIstemeForm);

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"TEKLİF BAŞARIYLA GÜNCELLENMİŞTİR!", null));
		}

		fillTestList();
	}

	// dosya içeriği otomatik olarak teklife ekleniyor
	public void teklifIcerikEkle() {

		SatinalmaTakipDosyalarIcerikManager dim = new SatinalmaTakipDosyalarIcerikManager();
		SatinalmaTeklifIstemeIcerikManager ticm = new SatinalmaTeklifIstemeIcerikManager();
		// SatinalmaMalzemeHizmetTalepFormuIcerikManager smtfiMan = new
		// SatinalmaMalzemeHizmetTalepFormuIcerikManager();
		StokTanimlarBirimManager birimMan = new StokTanimlarBirimManager();

		satinalmaTakipDosyalarIcerikList = dim
				.getAllSatinalmaTakipDosyalarIcerikByDosyaId(satinalmaTakipDosyalarForm
						.getId());

		satinalmaTeklifIstemeIcerikForm.setEklemeZamani(UtilInsCore
				.getTarihZaman());
		satinalmaTeklifIstemeIcerikForm.setEkleyenKullanici(userBean.getUser()
				.getId());
		satinalmaTeklifIstemeIcerikForm
				.setSatinalmaTeklifIsteme(satinalmaTeklifIstemeForm);

		for (int i = 0; i < satinalmaTakipDosyalarIcerikList.size(); i++) {
			satinalmaTeklifIstemeIcerikForm.setId(null);
			satinalmaTeklifIstemeIcerikForm
					.setSatinalmaTakipDosyalarIcerik(satinalmaTakipDosyalarIcerikList
							.get(i));
			// satinalmaMalzemeHizmetTalepFormuIcerik = smtfiMan.findByField(
			// SatinalmaMalzemeHizmetTalepFormuIcerik.class, "id",
			// satinalmaTakipDosyalarIcerikList.get(i).getTalepIcerikId())
			// .get(0);
			satinalmaTeklifIstemeIcerikForm
					.setMiktar(satinalmaTakipDosyalarIcerikList.get(i)
							.getSatinalmaMalzemeHizmetTalepFormuIcerik()
							.getMiktar().toString());
			satinalmaTeklifIstemeIcerikForm.setStokTanimlarBirim(birimMan
					.loadObject(StokTanimlarBirim.class, 5));
			ticm.enterNew(satinalmaTeklifIstemeIcerikForm);
		}

	}

	public void deleteForm() {

		SatinalmaTeklifIstemeManager formManager = new SatinalmaTeklifIstemeManager();

		if (satinalmaTeklifIstemeForm.getId() == null) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_WARN, "ÖNCE TEKLİF SEÇİNİZ!", null));
			return;
		}
		try {
			formManager.delete(satinalmaTeklifIstemeForm);
			fillTestList();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"TEKLİF BAŞARIYLA SİLİNMİŞTİR!", null));
		} catch (Exception e) {
			fillTestList();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "TEKLİF SİLİNEMEDİ, HATA!",
					null));
		}
	}

	public void fillTestList() {

		SatinalmaTeklifIstemeManager manager = new SatinalmaTeklifIstemeManager();
		allSatinalmaTeklifIstemeList = manager.getAllSatinalmaTeklifIsteme();
		updateButtonRender = false;
		satinalmaTeklifIstemeForm = new SatinalmaTeklifIsteme();

		// dosya takip numarasına göre liste

		satinalmaTeklifIstemeByTakipDosyaList = manager
				.findByDosyaId(satinalmaTakipDosyalarForm.getId());
	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void sayiBul() {

		SatinalmaTeklifIstemeManager manager = new SatinalmaTeklifIstemeManager();
		satinalmaTeklifIstemeForm.setTeklifSayi(manager
				.getOtomatikSayi(satinalmaTakipDosyalarForm.getId()));
	}

	// autocomplete için
	public List<SatinalmaMusteri> completeSatinalmaMusteri(String query) {

		List<SatinalmaMusteri> suggestions = new ArrayList<SatinalmaMusteri>();
		for (SatinalmaMusteri i : firmaItems) {
			if (i.getShortName().startsWith(query))
				suggestions.add(i);
		}

		return suggestions;
	}

	public void addOrUpdateFirma() {

		SatinalmaTeklifIstemeManager formManager = new SatinalmaTeklifIstemeManager();
		formManager.updateEntity(satinalmaTeklifIstemeForm);

		FacesContextUtils.addInfoMessage("FirmaAddUpdateMessage");

		fillTestList();
	}

	public void kararOlustur() {

		satinalmaSatinalmaKarariBean.addFromTeklif(satinalmaTakipDosyalarForm);

	}

	// setters getters

	public List<SatinalmaTeklifIsteme> getAllSatinalmaTeklifIstemeList() {
		return allSatinalmaTeklifIstemeList;
	}

	public void setAllSatinalmaTeklifIstemeList(
			List<SatinalmaTeklifIsteme> allSatinalmaTeklifIstemeList) {
		this.allSatinalmaTeklifIstemeList = allSatinalmaTeklifIstemeList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public SatinalmaTeklifIsteme getSatinalmaTeklifIstemeForm() {
		return satinalmaTeklifIstemeForm;
	}

	public void setSatinalmaTeklifIstemeForm(
			SatinalmaTeklifIsteme satinalmaTeklifIstemeForm) {
		this.satinalmaTeklifIstemeForm = satinalmaTeklifIstemeForm;
	}

	/**
	 * @return the satinalmaTeklifIstemeByTakipDosyaList
	 */
	public List<SatinalmaTeklifIsteme> getSatinalmaTeklifIstemeByTakipDosyaList() {
		return satinalmaTeklifIstemeByTakipDosyaList;
	}

	/**
	 * @param satinalmaTeklifIstemeByTakipDosyaList
	 *            the satinalmaTeklifIstemeByTakipDosyaList to set
	 */
	public void setSatinalmaTeklifIstemeByTakipDosyaList(
			List<SatinalmaTeklifIsteme> satinalmaTeklifIstemeByTakipDosyaList) {
		this.satinalmaTeklifIstemeByTakipDosyaList = satinalmaTeklifIstemeByTakipDosyaList;
	}

	/**
	 * @return the satinalmaTakipDosyalarForm
	 */
	public SatinalmaTakipDosyalar getSatinalmaTakipDosyalarForm() {
		return satinalmaTakipDosyalarForm;
	}

	/**
	 * @param satinalmaTakipDosyalarForm
	 *            the satinalmaTakipDosyalarForm to set
	 */
	public void setSatinalmaTakipDosyalarForm(
			SatinalmaTakipDosyalar satinalmaTakipDosyalarForm) {
		this.satinalmaTakipDosyalarForm = satinalmaTakipDosyalarForm;
	}

	/**
	 * @return the satinalmaSatinalmaKarariBean
	 */
	public SatinalmaSatinalmaKarariBean getSatinalmaSatinalmaKarariBean() {
		return satinalmaSatinalmaKarariBean;
	}

	/**
	 * @param satinalmaSatinalmaKarariBean
	 *            the satinalmaSatinalmaKarariBean to set
	 */
	public void setSatinalmaSatinalmaKarariBean(
			SatinalmaSatinalmaKarariBean satinalmaSatinalmaKarariBean) {
		this.satinalmaSatinalmaKarariBean = satinalmaSatinalmaKarariBean;
	}
}
