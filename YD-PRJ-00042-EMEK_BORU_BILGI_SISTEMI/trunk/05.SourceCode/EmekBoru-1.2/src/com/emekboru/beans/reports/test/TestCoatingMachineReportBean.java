/**
 * 
 */
package com.emekboru.beans.reports.test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiEpoksi;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.kaplamamakinesi.KaplamaMakinesiEpoksiManager;
import com.emekboru.jpaman.sales.order.SalesItemManager;
import com.emekboru.utils.ReportUtil;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testCoatingMachineReportBean")
@ViewScoped
public class TestCoatingMachineReportBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	Integer raporNo = 0;
	String muayeneyiYapan = null;
	String hazirlayan = null;

	Integer raporVardiya;
	Date raporTarihi;
	String raporTarihiAString;
	String raporTarihiBString;

	Boolean kontrol = false;

	private String downloadedFileName;
	private StreamedContent downloadedFile;
	private String realFilePath;

	@PostConstruct
	public void init() {
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr68ReportXlsx(Integer prmPipeId) {

		if (raporVardiya == -1) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "VARDİYA SEÇİNİZ!", null));
			return;
		}
		if (prmPipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}
		if (raporTarihi == null || raporVardiya == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		tarihBul(raporTarihi);

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			KaplamaMakinesiEpoksiManager makinaManager = new KaplamaMakinesiEpoksiManager();
			List<KaplamaMakinesiEpoksi> raporIceriks = new ArrayList<KaplamaMakinesiEpoksi>();
			if (raporVardiya == 1) {
				raporIceriks = makinaManager.getByDateVardiya(
						raporTarihiAString, raporTarihiAString, raporVardiya);
			} else if (raporVardiya == 0) {
				raporIceriks = makinaManager.getByDateVardiya(
						raporTarihiAString, raporTarihiBString, raporVardiya);
			}

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(raporIceriks
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			muayeneyiYapan = raporIceriks.get(0).getEkleyenEmployee()
					.getEmployee().getFirstname()
					+ " "
					+ raporIceriks.get(0).getEkleyenEmployee().getEmployee()
							.getLastname();
			hazirlayan = userBean.getUserFullName();

			ReportUtil reportUtil = new ReportUtil();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("raporIceriks", raporIceriks);

			if (raporVardiya == 1) {
				dataMap.put("vardiya", "GÜNDÜZ");
			} else if (raporVardiya == 0) {
				dataMap.put("vardiya", "GECE");
			}

			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/kaplamatezgah/FR-68.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			downloadedFileName = "FR-68"
					+ pipes.get(0).getSalesItem().getProposal().getCustomer()
							.getShortName() + "-" + pipes.get(0).getPipeIndex()
					+ ".xls";

			getRealFilePath("/reportformats/kaplamatezgah/"
					+ downloadedFileName);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "FR-68 BAŞARIYLA OLUŞTURULDU!",
					null));

		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr68_2ReportXlsx(Integer prmPipeId) {

		if (raporVardiya == -1) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "VARDİYA SEÇİNİZ!", null));
			return;
		}
		if (prmPipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}
		if (raporTarihi == null || raporVardiya == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		tarihBul(raporTarihi);

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			KaplamaMakinesiEpoksiManager makinaManager = new KaplamaMakinesiEpoksiManager();
			List<KaplamaMakinesiEpoksi> raporIceriks = new ArrayList<KaplamaMakinesiEpoksi>();
			if (raporVardiya == 1) {
				raporIceriks = makinaManager.getByDateVardiya(
						raporTarihiAString, raporTarihiAString, raporVardiya);
			} else if (raporVardiya == 0) {
				raporIceriks = makinaManager.getByDateVardiya(
						raporTarihiAString, raporTarihiBString, raporVardiya);
			}

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(raporIceriks
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			muayeneyiYapan = raporIceriks.get(0).getEkleyenEmployee()
					.getEmployee().getFirstname()
					+ " "
					+ raporIceriks.get(0).getEkleyenEmployee().getEmployee()
							.getLastname();
			hazirlayan = userBean.getUserFullName();

			ReportUtil reportUtil = new ReportUtil();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("raporIceriks", raporIceriks);

			if (raporVardiya == 1) {
				dataMap.put("vardiya", "GÜNDÜZ");
			} else if (raporVardiya == 0) {
				dataMap.put("vardiya", "GECE");
			}

			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/kaplamatezgah/FR-68-2.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			downloadedFileName = "FR-68-2"
					+ pipes.get(0).getSalesItem().getProposal().getCustomer()
							.getShortName() + "-" + pipes.get(0).getPipeIndex()
					+ ".xls";

			getRealFilePath("/reportformats/kaplamatezgah/"
					+ downloadedFileName);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"FR-68-2 BAŞARIYLA OLUŞTURULDU!", null));

		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr71ReportXlsx(Integer prmPipeId) {

		if (raporVardiya == -1) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "VARDİYA SEÇİNİZ!", null));
			return;
		}
		if (prmPipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}
		if (raporTarihi == null || raporVardiya == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		tarihBul(raporTarihi);

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			KaplamaMakinesiEpoksiManager makinaManager = new KaplamaMakinesiEpoksiManager();
			List<KaplamaMakinesiEpoksi> raporIceriks = new ArrayList<KaplamaMakinesiEpoksi>();
			if (raporVardiya == 1) {
				raporIceriks = makinaManager.getByDateVardiya(
						raporTarihiAString, raporTarihiAString, raporVardiya);
			} else if (raporVardiya == 0) {
				raporIceriks = makinaManager.getByDateVardiya(
						raporTarihiAString, raporTarihiBString, raporVardiya);
			}

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(raporIceriks
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			muayeneyiYapan = raporIceriks.get(0).getEkleyenEmployee()
					.getEmployee().getFirstname()
					+ " "
					+ raporIceriks.get(0).getEkleyenEmployee().getEmployee()
							.getLastname();
			hazirlayan = userBean.getUserFullName();

			ReportUtil reportUtil = new ReportUtil();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("raporIceriks", raporIceriks);

			if (raporVardiya == 1) {
				dataMap.put("vardiya", "GÜNDÜZ");
			} else if (raporVardiya == 0) {
				dataMap.put("vardiya", "GECE");
			}

			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/kaplamatezgah/FR-71.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			downloadedFileName = "FR-71"
					+ pipes.get(0).getSalesItem().getProposal().getCustomer()
							.getShortName() + "-" + pipes.get(0).getPipeIndex()
					+ ".xls";

			getRealFilePath("/reportformats/kaplamatezgah/"
					+ downloadedFileName);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "FR-71 BAŞARIYLA OLUŞTURULDU!",
					null));

		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr71_2ReportXlsx(Integer prmPipeId) {

		if (raporVardiya == -1) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "VARDİYA SEÇİNİZ!", null));
			return;
		}
		if (prmPipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}
		if (raporTarihi == null || raporVardiya == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		tarihBul(raporTarihi);

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			KaplamaMakinesiEpoksiManager makinaManager = new KaplamaMakinesiEpoksiManager();
			List<KaplamaMakinesiEpoksi> raporIceriks = new ArrayList<KaplamaMakinesiEpoksi>();
			if (raporVardiya == 1) {
				raporIceriks = makinaManager.getByDateVardiya(
						raporTarihiAString, raporTarihiAString, raporVardiya);
			} else if (raporVardiya == 0) {
				raporIceriks = makinaManager.getByDateVardiya(
						raporTarihiAString, raporTarihiBString, raporVardiya);
			}

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(raporIceriks
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			muayeneyiYapan = raporIceriks.get(0).getEkleyenEmployee()
					.getEmployee().getFirstname()
					+ " "
					+ raporIceriks.get(0).getEkleyenEmployee().getEmployee()
							.getLastname();
			hazirlayan = userBean.getUserFullName();

			ReportUtil reportUtil = new ReportUtil();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("raporIceriks", raporIceriks);

			if (raporVardiya == 1) {
				dataMap.put("vardiya", "GÜNDÜZ");
			} else if (raporVardiya == 0) {
				dataMap.put("vardiya", "GECE");
			}

			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/kaplamatezgah/FR-71-2.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			downloadedFileName = "FR-71-2"
					+ pipes.get(0).getSalesItem().getProposal().getCustomer()
							.getShortName() + "-" + pipes.get(0).getPipeIndex()
					+ ".xls";

			getRealFilePath("/reportformats/kaplamatezgah/"
					+ downloadedFileName);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"FR-71-2 BAŞARIYLA OLUŞTURULDU!", null));

		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr72ReportXlsx(Integer prmPipeId) {

		if (raporVardiya == -1) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "VARDİYA SEÇİNİZ!", null));
			return;
		}
		if (prmPipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}
		if (raporTarihi == null || raporVardiya == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		tarihBul(raporTarihi);

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			KaplamaMakinesiEpoksiManager makinaManager = new KaplamaMakinesiEpoksiManager();
			List<KaplamaMakinesiEpoksi> raporIceriks = new ArrayList<KaplamaMakinesiEpoksi>();
			if (raporVardiya == 1) {
				raporIceriks = makinaManager.getByDateVardiya(
						raporTarihiAString, raporTarihiAString, raporVardiya);
			} else if (raporVardiya == 0) {
				raporIceriks = makinaManager.getByDateVardiya(
						raporTarihiAString, raporTarihiBString, raporVardiya);
			}

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(raporIceriks
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			muayeneyiYapan = raporIceriks.get(0).getEkleyenEmployee()
					.getEmployee().getFirstname()
					+ " "
					+ raporIceriks.get(0).getEkleyenEmployee().getEmployee()
							.getLastname();
			hazirlayan = userBean.getUserFullName();

			ReportUtil reportUtil = new ReportUtil();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("raporIceriks", raporIceriks);

			if (raporVardiya == 1) {
				dataMap.put("vardiya", "GÜNDÜZ");
			} else if (raporVardiya == 0) {
				dataMap.put("vardiya", "GECE");
			}

			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/kaplamatezgah/FR-72.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			downloadedFileName = "FR-72"
					+ pipes.get(0).getSalesItem().getProposal().getCustomer()
							.getShortName() + "-" + pipes.get(0).getPipeIndex()
					+ ".xls";

			getRealFilePath("/reportformats/kaplamatezgah/"
					+ downloadedFileName);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "FR-72 BAŞARIYLA OLUŞTURULDU!",
					null));

		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr146ReportXlsx(Integer prmPipeId) {

		if (raporVardiya == -1) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "VARDİYA SEÇİNİZ!", null));
			return;
		}
		if (prmPipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}
		if (raporTarihi == null || raporVardiya == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		tarihBul(raporTarihi);

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			KaplamaMakinesiEpoksiManager makinaManager = new KaplamaMakinesiEpoksiManager();
			List<KaplamaMakinesiEpoksi> raporIceriks = new ArrayList<KaplamaMakinesiEpoksi>();
			if (raporVardiya == 1) {
				raporIceriks = makinaManager.getByDateVardiya(
						raporTarihiAString, raporTarihiAString, raporVardiya);
			} else if (raporVardiya == 0) {
				raporIceriks = makinaManager.getByDateVardiya(
						raporTarihiAString, raporTarihiBString, raporVardiya);
			}

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(raporIceriks
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			muayeneyiYapan = raporIceriks.get(0).getEkleyenEmployee()
					.getEmployee().getFirstname()
					+ " "
					+ raporIceriks.get(0).getEkleyenEmployee().getEmployee()
							.getLastname();
			hazirlayan = userBean.getUserFullName();

			ReportUtil reportUtil = new ReportUtil();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("raporIceriks", raporIceriks);

			if (raporVardiya == 1) {
				dataMap.put("vardiya", "GÜNDÜZ");
			} else if (raporVardiya == 0) {
				dataMap.put("vardiya", "GECE");
			}

			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/kaplamatezgah/FR-146.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			downloadedFileName = "FR-146"
					+ pipes.get(0).getSalesItem().getProposal().getCustomer()
							.getShortName() + "-" + pipes.get(0).getPipeIndex()
					+ ".xls";

			getRealFilePath("/reportformats/kaplamatezgah/"
					+ downloadedFileName);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"FR-146 BAŞARIYLA OLUŞTURULDU!", null));

		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr223ReportXlsx(Integer prmPipeId) {

		if (raporVardiya == -1) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "VARDİYA SEÇİNİZ!", null));
			return;
		}
		if (prmPipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}
		if (raporTarihi == null || raporVardiya == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		tarihBul(raporTarihi);

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			KaplamaMakinesiEpoksiManager makinaManager = new KaplamaMakinesiEpoksiManager();
			List<KaplamaMakinesiEpoksi> raporIceriks = new ArrayList<KaplamaMakinesiEpoksi>();
			if (raporVardiya == 1) {
				raporIceriks = makinaManager.getByDateVardiya(
						raporTarihiAString, raporTarihiAString, raporVardiya);
			} else if (raporVardiya == 0) {
				raporIceriks = makinaManager.getByDateVardiya(
						raporTarihiAString, raporTarihiBString, raporVardiya);
			}

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(raporIceriks
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			muayeneyiYapan = raporIceriks.get(0).getEkleyenEmployee()
					.getEmployee().getFirstname()
					+ " "
					+ raporIceriks.get(0).getEkleyenEmployee().getEmployee()
							.getLastname();
			hazirlayan = userBean.getUserFullName();

			ReportUtil reportUtil = new ReportUtil();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("raporIceriks", raporIceriks);

			if (raporVardiya == 1) {
				dataMap.put("vardiya", "GÜNDÜZ");
			} else if (raporVardiya == 0) {
				dataMap.put("vardiya", "GECE");
			}

			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/kaplamatezgah/FR-223.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			downloadedFileName = "FR-223"
					+ pipes.get(0).getSalesItem().getProposal().getCustomer()
							.getShortName() + "-" + pipes.get(0).getPipeIndex()
					+ ".xls";

			getRealFilePath("/reportformats/kaplamatezgah/"
					+ downloadedFileName);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"FR-223 BAŞARIYLA OLUŞTURULDU!", null));

		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr229ReportXlsx(Integer prmPipeId) {

		if (raporVardiya == -1) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "VARDİYA SEÇİNİZ!", null));
			return;
		}
		if (prmPipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}
		if (raporTarihi == null || raporVardiya == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		tarihBul(raporTarihi);

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			KaplamaMakinesiEpoksiManager makinaManager = new KaplamaMakinesiEpoksiManager();
			List<KaplamaMakinesiEpoksi> raporIceriks = new ArrayList<KaplamaMakinesiEpoksi>();
			if (raporVardiya == 1) {
				raporIceriks = makinaManager.getByDateVardiya(
						raporTarihiAString, raporTarihiAString, raporVardiya);
			} else if (raporVardiya == 0) {
				raporIceriks = makinaManager.getByDateVardiya(
						raporTarihiAString, raporTarihiBString, raporVardiya);
			}

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(raporIceriks
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			muayeneyiYapan = raporIceriks.get(0).getEkleyenEmployee()
					.getEmployee().getFirstname()
					+ " "
					+ raporIceriks.get(0).getEkleyenEmployee().getEmployee()
							.getLastname();
			hazirlayan = userBean.getUserFullName();

			ReportUtil reportUtil = new ReportUtil();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("raporIceriks", raporIceriks);

			if (raporVardiya == 1) {
				dataMap.put("vardiya", "GÜNDÜZ");
			} else if (raporVardiya == 0) {
				dataMap.put("vardiya", "GECE");
			}

			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/kaplamatezgah/FR-229.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			downloadedFileName = "FR-229"
					+ pipes.get(0).getSalesItem().getProposal().getCustomer()
							.getShortName() + "-" + pipes.get(0).getPipeIndex()
					+ ".xls";

			getRealFilePath("/reportformats/kaplamatezgah/"
					+ downloadedFileName);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"FR-229 BAŞARIYLA OLUŞTURULDU!", null));

		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void fr230ReportXlsx(Integer prmPipeId) {

		if (raporVardiya == -1) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "VARDİYA SEÇİNİZ!", null));
			return;
		}
		if (prmPipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}
		if (raporTarihi == null || raporVardiya == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		tarihBul(raporTarihi);

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			KaplamaMakinesiEpoksiManager makinaManager = new KaplamaMakinesiEpoksiManager();
			List<KaplamaMakinesiEpoksi> raporIceriks = new ArrayList<KaplamaMakinesiEpoksi>();
			if (raporVardiya == 1) {
				raporIceriks = makinaManager.getByDateVardiya(
						raporTarihiAString, raporTarihiAString, raporVardiya);
			} else if (raporVardiya == 0) {
				raporIceriks = makinaManager.getByDateVardiya(
						raporTarihiAString, raporTarihiBString, raporVardiya);
			}

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<String> date = new ArrayList<String>();
			date.add(new SimpleDateFormat("dd.MM.yyyy").format(raporIceriks
					.get(0).getEklemeZamani()));

			raporNo = pipes.get(0).getPipeIndex();
			muayeneyiYapan = raporIceriks.get(0).getEkleyenEmployee()
					.getEmployee().getFirstname()
					+ " "
					+ raporIceriks.get(0).getEkleyenEmployee().getEmployee()
							.getLastname();
			hazirlayan = userBean.getUserFullName();

			ReportUtil reportUtil = new ReportUtil();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("raporIceriks", raporIceriks);

			if (raporVardiya == 1) {
				dataMap.put("vardiya", "GÜNDÜZ");
			} else if (raporVardiya == 0) {
				dataMap.put("vardiya", "GECE");
			}

			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/kaplamatezgah/FR-230.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			downloadedFileName = "FR-230"
					+ pipes.get(0).getSalesItem().getProposal().getCustomer()
							.getShortName() + "-" + pipes.get(0).getPipeIndex()
					+ ".xls";

			getRealFilePath("/reportformats/kaplamatezgah/"
					+ downloadedFileName);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"FR-230 BAŞARIYLA OLUŞTURULDU!", null));

		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	public void tarihBul(Date prmTestDate) {

		raporTarihiAString = Integer.toString(raporTarihi.getYear() + 1900)
				+ "/" + Integer.toString(raporTarihi.getMonth() + 1) + "/"
				+ Integer.toString(raporTarihi.getDate());
		raporTarihiBString = Integer.toString(raporTarihi.getYear() + 1900)
				+ "/" + Integer.toString(raporTarihi.getMonth() + 1) + "/"
				+ Integer.toString(raporTarihi.getDate() + 1);
	}

	// download için
	@SuppressWarnings("resource")
	public StreamedContent getDownloadedFile() {

		try {
			if (downloadedFileName != null) {
				RandomAccessFile accessFile = new RandomAccessFile(
						realFilePath, "r");
				byte[] downloadByte = new byte[(int) accessFile.length()];
				accessFile.read(downloadByte);
				InputStream stream = new ByteArrayInputStream(downloadByte);

				String suffix = FilenameUtils.getExtension(downloadedFileName);
				String contentType = "text/plain";
				if (suffix.contentEquals("jpg") || suffix.contentEquals("png")
						|| suffix.contentEquals("gif")) {
					contentType = "image/" + contentType;
				} else if (suffix.contentEquals("doc")
						|| suffix.contentEquals("docx")) {
					contentType = "application/msword";
				} else if (suffix.contentEquals("xls")
						|| suffix.contentEquals("xlsx")) {
					contentType = "application/vnd.ms-excel";
				} else if (suffix.contentEquals("pdf")) {
					contentType = "application/pdf";
				}
				downloadedFile = new DefaultStreamedContent(stream,
						"contentType", downloadedFileName);

				stream.close();
			} else {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"LÜTFEN RAPOR OLUŞTURUNUZ, RAPOR BULUNAMADI!"));
			}
		} catch (Exception e) {
			System.out
					.println("testNondestructiveReportBean.getDownloadedFile():"
							+ e.toString());
		}
		return downloadedFile;
	}

	private String getRealFilePath(String filePath) {

		realFilePath = FacesContext.getCurrentInstance().getExternalContext()
				.getRealPath(filePath);

		return realFilePath;
	}

	// setters getters

	/**
	 * @return the config
	 */
	public ConfigBean getConfig() {
		return config;
	}

	/**
	 * @param config
	 *            the config to set
	 */
	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the raporVardiya
	 */
	public Integer getRaporVardiya() {
		return raporVardiya;
	}

	/**
	 * @param raporVardiya
	 *            the raporVardiya to set
	 */
	public void setRaporVardiya(Integer raporVardiya) {
		this.raporVardiya = raporVardiya;
	}

	/**
	 * @return the raporTarihi
	 */
	public Date getRaporTarihi() {
		return raporTarihi;
	}

	/**
	 * @param raporTarihi
	 *            the raporTarihi to set
	 */
	public void setRaporTarihi(Date raporTarihi) {
		this.raporTarihi = raporTarihi;
	}

	/**
	 * @return the downloadedFileName
	 */
	public String getDownloadedFileName() {
		return downloadedFileName;
	}

	/**
	 * @param downloadedFileName
	 *            the downloadedFileName to set
	 */
	public void setDownloadedFileName(String downloadedFileName) {
		this.downloadedFileName = downloadedFileName;
	}

	/**
	 * @param downloadedFile
	 *            the downloadedFile to set
	 */
	public void setDownloadedFile(StreamedContent downloadedFile) {
		this.downloadedFile = downloadedFile;
	}

}