/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaFlowCoatBondSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaFlowCoatBondSonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testKaplamaFlowCoatBondSonucBean")
@ViewScoped
public class TestKaplamaFlowCoatBondSonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<TestKaplamaFlowCoatBondSonuc> allKaplamaFlowCoatBondSonucList = new ArrayList<TestKaplamaFlowCoatBondSonuc>();
	private TestKaplamaFlowCoatBondSonuc testKaplamaFlowCoatBondSonucForm = new TestKaplamaFlowCoatBondSonuc();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;
	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addKaplamaFlowCoatBondSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaFlowCoatBondSonucManager tkdsManager = new TestKaplamaFlowCoatBondSonucManager();

		if (testKaplamaFlowCoatBondSonucForm.getId() == null) {

			testKaplamaFlowCoatBondSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaFlowCoatBondSonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaFlowCoatBondSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaFlowCoatBondSonucForm.setBagliTestId(bagliTest);
			testKaplamaFlowCoatBondSonucForm.setBagliGlobalId(bagliTest);

			tkdsManager.enterNew(testKaplamaFlowCoatBondSonucForm);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaFlowCoatBondSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaFlowCoatBondSonucForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			tkdsManager.updateEntity(testKaplamaFlowCoatBondSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");

		}
		fillTestList(prmGlobalId, prmPipeId);
	}

	public void addKaplamaFlowCoatBond() {
		testKaplamaFlowCoatBondSonucForm = new TestKaplamaFlowCoatBondSonuc();
		updateButtonRender = false;
	}

	public void kaplamaFlowCoatBondListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(int globalId, Integer pipeId2) {
		allKaplamaFlowCoatBondSonucList = TestKaplamaFlowCoatBondSonucManager
				.getAllTestKaplamaFlowCoatBondSonuc(globalId, pipeId2);
		testKaplamaFlowCoatBondSonucForm = new TestKaplamaFlowCoatBondSonuc();
	}

	public void deleteTestKaplamaFlowCoatBondSonuc() {

		bagliTestId = testKaplamaFlowCoatBondSonucForm.getBagliTestId().getId();

		if (testKaplamaFlowCoatBondSonucForm == null
				|| testKaplamaFlowCoatBondSonucForm.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaFlowCoatBondSonucManager tkdsManager = new TestKaplamaFlowCoatBondSonucManager();
		tkdsManager.delete(testKaplamaFlowCoatBondSonucForm);
		testKaplamaFlowCoatBondSonucForm = new TestKaplamaFlowCoatBondSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setters getters

	/**
	 * @return the allKaplamaFlowCoatBondSonucList
	 */
	public List<TestKaplamaFlowCoatBondSonuc> getAllKaplamaFlowCoatBondSonucList() {
		return allKaplamaFlowCoatBondSonucList;
	}

	/**
	 * @param allKaplamaFlowCoatBondSonucList
	 *            the allKaplamaFlowCoatBondSonucList to set
	 */
	public void setAllKaplamaFlowCoatBondSonucList(
			List<TestKaplamaFlowCoatBondSonuc> allKaplamaFlowCoatBondSonucList) {
		this.allKaplamaFlowCoatBondSonucList = allKaplamaFlowCoatBondSonucList;
	}

	/**
	 * @return the testKaplamaFlowCoatBondSonucForm
	 */
	public TestKaplamaFlowCoatBondSonuc getTestKaplamaFlowCoatBondSonucForm() {
		return testKaplamaFlowCoatBondSonucForm;
	}

	/**
	 * @param testKaplamaFlowCoatBondSonucForm
	 *            the testKaplamaFlowCoatBondSonucForm to set
	 */
	public void setTestKaplamaFlowCoatBondSonucForm(
			TestKaplamaFlowCoatBondSonuc testKaplamaFlowCoatBondSonucForm) {
		this.testKaplamaFlowCoatBondSonucForm = testKaplamaFlowCoatBondSonucForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
