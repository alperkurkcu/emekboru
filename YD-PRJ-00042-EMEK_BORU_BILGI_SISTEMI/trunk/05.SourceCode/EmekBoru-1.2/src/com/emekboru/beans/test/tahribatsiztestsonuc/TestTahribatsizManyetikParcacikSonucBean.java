/**
 * 
 */
package com.emekboru.beans.test.tahribatsiztestsonuc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.test.tahribatsiz.TahribatsizTestOrderListBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.kalibrasyon.KalibrasyonManyetik;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizManyetikParcacikSonuc;
import com.emekboru.jpaman.NondestructiveTestsSpecManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.kalibrasyon.KalibrasyonManyetikManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizManyetikParcacikSonucManager;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testTahribatsizManyetikParcacikSonucBean")
@ViewScoped
public class TestTahribatsizManyetikParcacikSonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<TestTahribatsizManyetikParcacikSonuc> allTestTahribatsizManyetikParcacikSonucList = new ArrayList<TestTahribatsizManyetikParcacikSonuc>();
	private TestTahribatsizManyetikParcacikSonuc testTahribatsizManyetikParcacikSonucForm = new TestTahribatsizManyetikParcacikSonuc();

	int index = 0;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private boolean visibleButtonRender;

	private Pipe selectedPipe = new Pipe();

	private String barkodNo = null;

	private List<KalibrasyonManyetik> kalibrasyonlar = new ArrayList<KalibrasyonManyetik>();
	private KalibrasyonManyetik kalibrasyon = new KalibrasyonManyetik();

	// private KalibrasyonManyetikBoruucu kalibrasyonBoruucu = new
	// KalibrasyonManyetikBoruucu();

	public void manyetikParcacikListener(SelectEvent event) {
		updateButtonRender = true;
		visibleButtonRender = false;
	}

	public void fillTestList(Integer pipeId) {

		try {
			TestTahribatsizManyetikParcacikSonucManager manager = new TestTahribatsizManyetikParcacikSonucManager();
			allTestTahribatsizManyetikParcacikSonucList = manager
					.getAllTestTahribatsizManyetikParcacikSonuc(pipeId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void addTestTahribatsizManyetikParcacikSonuc() {
		testTahribatsizManyetikParcacikSonucForm = new TestTahribatsizManyetikParcacikSonuc();
		updateButtonRender = false;
	}

	@SuppressWarnings({ "static-access", "unused" })
	public void addOrUpdateManyetikParcacikSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();

		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");
		Integer prmItemId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");

		Integer testId = 0;

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}
		if (prmItemId == null) {
			prmItemId = selectedPipe.getSalesItem().getItemId();
		}

		TestTahribatsizManyetikParcacikSonucManager manyetikParcacikManager = new TestTahribatsizManyetikParcacikSonucManager();
		NondestructiveTestsSpecManager nonManager = new NondestructiveTestsSpecManager();

		try {
			testId = nonManager.findByItemIdGlobalId(prmItemId, 1007).get(0)
					.getTestId();

		} catch (Exception e2) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"KOORDİNAT EKLEME BAŞARISIZ, KALİTE TANIMLARINDA MANYETİK PARÇACIK TESTİ EKSİKTİR!",
							null));
			return;
		}

		if (testTahribatsizManyetikParcacikSonucForm.getId() == null) {

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);

			// testTahribatsizManyetikParcacikSonucForm.setTestId(testId);
			testTahribatsizManyetikParcacikSonucForm.setPipe(pipe);

			testTahribatsizManyetikParcacikSonucForm
					.setEklemeZamani(UtilInsCore.getTarihZaman());
			testTahribatsizManyetikParcacikSonucForm
					.setEkleyenEmployee(userBean.getUser());

			if (testTahribatsizManyetikParcacikSonucForm.getHataTipi() == 1) {
				sonKalibrasyonuGoster(1);
			} else if (testTahribatsizManyetikParcacikSonucForm.getHataTipi() == 2) {
				sonKalibrasyonuGoster(2);
			} else if (testTahribatsizManyetikParcacikSonucForm.getHataTipi() == 3) {
				sonKalibrasyonuGoster(3);
			}
			testTahribatsizManyetikParcacikSonucForm
					.setKalibrasyon(kalibrasyon);

			manyetikParcacikManager
					.enterNew(testTahribatsizManyetikParcacikSonucForm);

			this.testTahribatsizManyetikParcacikSonucForm = new TestTahribatsizManyetikParcacikSonuc();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							"MANYETİK PARÇACIK MUAYENE KOORDİNATI BAŞARI İLE KAYDEDİLMİŞTİR!"));
		} else {

			testTahribatsizManyetikParcacikSonucForm
					.setGuncellemeZamani(UtilInsCore.getTarihZaman());
			testTahribatsizManyetikParcacikSonucForm
					.setEkleyenEmployee(userBean.getUser());
			// testTahribatsizManyetikParcacikSonucForm.setTestId(testId);

			manyetikParcacikManager
					.updateEntity(testTahribatsizManyetikParcacikSonucForm);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"MANYETİK PARÇACIK MUAYENE BAŞARI İLE GÜNCELLENMİŞTİR!"));
		}

		fillTestList(prmPipeId);
		testTahribatsizManyetikParcacikSonucForm = new TestTahribatsizManyetikParcacikSonuc();
		updateButtonRender = false;
		// sayfada barkoddan okutunca order item pipe seçimi olmadıgı için
		// sıkıntı cıkıyordu
		TahribatsizTestOrderListBean ttolb = new TahribatsizTestOrderListBean();
		ttolb.loadOrderDetails(selectedPipe.getSalesItem().getItemId());
	}

	public void deleteManyetikParcacikSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		TestTahribatsizManyetikParcacikSonucManager manyetikParcacikManager = new TestTahribatsizManyetikParcacikSonucManager();

		if (testTahribatsizManyetikParcacikSonucForm.getId() == null) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"LİSTEDEN SEÇİLMİŞ BİR DEĞER YOKTUR!", null));
			return;
		}

		manyetikParcacikManager
				.delete(testTahribatsizManyetikParcacikSonucForm);
		testTahribatsizManyetikParcacikSonucForm = new TestTahribatsizManyetikParcacikSonuc();
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
				"MANYETİK PARÇACIK MUAYENE KOORDİNATI SİLİNMİŞTİR!", null));

		fillTestList(prmPipeId);
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0
				|| prmBarkod.equals("")) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " BARKOD NUMARALI BORU SİSTEMDE YOKTUR!", null));
			visibleButtonRender = false;
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			TestTahribatsizManyetikParcacikSonucManager manyetikParcacikManager = new TestTahribatsizManyetikParcacikSonucManager();
			allTestTahribatsizManyetikParcacikSonucList = manyetikParcacikManager
					.getAllTestTahribatsizManyetikParcacikSonuc(selectedPipe
							.getPipeId());
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
			visibleButtonRender = true;
		}
		KalibrasyonManyetikManager kalibrasyonManager = new KalibrasyonManyetikManager();
		kalibrasyonlar = kalibrasyonManager.getAllKalibrasyon();
	}

	// @SuppressWarnings("static-access")
	// public void sonKalibrasyonuGoster() {
	// kalibrasyon = new KalibrasyonManyetik();
	// KalibrasyonManyetikManager kalibrasyonManager = new
	// KalibrasyonManyetikManager();
	// kalibrasyon = kalibrasyonManager.getAllKalibrasyon().get(0);
	//
	// kalibrasyonBoruucu = new KalibrasyonManyetikBoruucu();
	// KalibrasyonManyetikBoruucuManager kalibrasyonBoruucuManager = new
	// KalibrasyonManyetikBoruucuManager();
	// kalibrasyonBoruucu = kalibrasyonBoruucuManager.getAllKalibrasyon().get(
	// 0);
	// }

	@SuppressWarnings("static-access")
	public void sonKalibrasyonuGoster(Integer prmType) {
		kalibrasyon = new KalibrasyonManyetik();
		KalibrasyonManyetikManager kalibrasyonManager = new KalibrasyonManyetikManager();
		kalibrasyon = kalibrasyonManager.getAllKalibrasyonByType(prmType)
				.get(0);

		// kalibrasyonBoruucu = new KalibrasyonManyetikBoruucu();
		// KalibrasyonManyetikBoruucuManager kalibrasyonBoruucuManager = new
		// KalibrasyonManyetikBoruucuManager();
		// kalibrasyonBoruucu =
		// kalibrasyonBoruucuManager.getAllKalibrasyon().get(
		// 0);
	}

	public void kalibrasyonGoster() {
		kalibrasyon = new KalibrasyonManyetik();
		kalibrasyon = testTahribatsizManyetikParcacikSonucForm.getKalibrasyon();
	}

	@SuppressWarnings("static-access")
	public void kalibrasyonlarByType(Integer prmType) {
		KalibrasyonManyetikManager kalibrasyonManager = new KalibrasyonManyetikManager();
		kalibrasyonlar = kalibrasyonManager.getAllKalibrasyonByType(prmType);
	}

	public void kalibrasyonAta(Integer prmType) {
		TestTahribatsizManyetikParcacikSonucManager manyetikManager = new TestTahribatsizManyetikParcacikSonucManager();
		try {
			for (int i = 0; i < allTestTahribatsizManyetikParcacikSonucList
					.size(); i++) {
				if (allTestTahribatsizManyetikParcacikSonucList.get(i)
						.getHataTipi() == prmType) {
					allTestTahribatsizManyetikParcacikSonucList.get(i)
							.setKalibrasyon(kalibrasyon);
				}
				manyetikManager
						.updateEntity(allTestTahribatsizManyetikParcacikSonucList
								.get(i));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"KALİBRASYON ATAMA BAŞARISIZ, TEKRAR DENEYİNİZ!", null));
			return;
		}
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				"KALİBRASYON ATAMA BAŞARILI!", null));
	}

	/**
	 * @return the allTestTahribatsizManyetikParcacikSonucList
	 */
	public List<TestTahribatsizManyetikParcacikSonuc> getAllTestTahribatsizManyetikParcacikSonucList() {
		return allTestTahribatsizManyetikParcacikSonucList;
	}

	/**
	 * @param allTestTahribatsizManyetikParcacikSonucList
	 *            the allTestTahribatsizManyetikParcacikSonucList to set
	 */
	public void setAllTestTahribatsizManyetikParcacikSonucList(
			List<TestTahribatsizManyetikParcacikSonuc> allTestTahribatsizManyetikParcacikSonucList) {
		this.allTestTahribatsizManyetikParcacikSonucList = allTestTahribatsizManyetikParcacikSonucList;
	}

	/**
	 * @return the index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @param index
	 *            the index to set
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the selectedPipe
	 */
	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	/**
	 * @param selectedPipe
	 *            the selectedPipe to set
	 */
	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	/**
	 * @return the barkodNo
	 */
	public String getBarkodNo() {
		return barkodNo;
	}

	/**
	 * @param barkodNo
	 *            the barkodNo to set
	 */
	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the testTahribatsizManyetikParcacikSonucForm
	 */
	public TestTahribatsizManyetikParcacikSonuc getTestTahribatsizManyetikParcacikSonucForm() {
		return testTahribatsizManyetikParcacikSonucForm;
	}

	/**
	 * @param testTahribatsizManyetikParcacikSonucForm
	 *            the testTahribatsizManyetikParcacikSonucForm to set
	 */
	public void setTestTahribatsizManyetikParcacikSonucForm(
			TestTahribatsizManyetikParcacikSonuc testTahribatsizManyetikParcacikSonucForm) {
		this.testTahribatsizManyetikParcacikSonucForm = testTahribatsizManyetikParcacikSonucForm;
	}

	/**
	 * @return the kalibrasyonlar
	 */
	public List<KalibrasyonManyetik> getKalibrasyonlar() {
		return kalibrasyonlar;
	}

	/**
	 * @param kalibrasyonlar
	 *            the kalibrasyonlar to set
	 */
	public void setKalibrasyonlar(List<KalibrasyonManyetik> kalibrasyonlar) {
		this.kalibrasyonlar = kalibrasyonlar;
	}

	/**
	 * @return the kalibrasyon
	 */
	public KalibrasyonManyetik getKalibrasyon() {
		return kalibrasyon;
	}

	/**
	 * @param kalibrasyon
	 *            the kalibrasyon to set
	 */
	public void setKalibrasyon(KalibrasyonManyetik kalibrasyon) {
		this.kalibrasyon = kalibrasyon;
	}

	/**
	 * @return the visibleButtonRender
	 */
	public boolean isVisibleButtonRender() {
		return visibleButtonRender;
	}

}
