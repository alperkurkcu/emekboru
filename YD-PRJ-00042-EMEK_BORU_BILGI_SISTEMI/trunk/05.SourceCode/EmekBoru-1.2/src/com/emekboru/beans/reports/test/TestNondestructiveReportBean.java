/**
 * 
 */
package com.emekboru.beans.reports.test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.BoruBoyutsalKontrolTolerans;
import com.emekboru.jpa.HidrostaticTestsSpec;
import com.emekboru.jpa.NondestructiveTestsSpec;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpa.tahribatsiztestsonuc.SpecMagnetic;
import com.emekboru.jpa.tahribatsiztestsonuc.SpecPenetrant;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizFloroskopikSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizGozOlcuSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizHidrostatik;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizKaynakGozleMuayeneSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizManuelUtSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizManyetikParcacikSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizOfflineOtomatikUtSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizOtomatikUtSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizPenetrantSonuc;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizRadyografikSonuc;
import com.emekboru.jpaman.BoruBoyutsalKontrolToleransManager;
import com.emekboru.jpaman.HidrostaticTestsSpecManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.sales.order.SalesItemManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.SpecMagneticManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.SpecPenetrantManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizFloroskopikSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizGozOlcuSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizHidrostatikManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizKaynakGozleMuayeneSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizManuelUtSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizManyetikParcacikSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizOfflineOtomatikUtSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizOtomatikUtSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizPenetrantSonucManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizRadyografikSonucManager;
import com.emekboru.utils.ReportUtil;

@ManagedBean(name = "testNondestructiveReportBean")
@ViewScoped
public class TestNondestructiveReportBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	Integer raporNo = 0;
	String muayeneyiYapan = null;
	String hazirlayan = null;

	Integer raporVardiya;
	Date raporTarihi;
	String raporTarihiAString;
	String raporTarihiBString;

	private Date baslangicTarihi;
	private Date bitisTarihi;

	Boolean kontrol = false;

	private String downloadedFileName;
	private StreamedContent downloadedFile;
	private String realFilePath;

	public String firstBarkodNo = null;
	public String secondBarkodNo = null;

	@PostConstruct
	public void init() {
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void otomatikUtInspectionResultReportXlsx(Integer pipeId,
			Integer type) {

		if (pipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", pipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<NondestructiveTestsSpec> kaynakSpecs = new ArrayList<NondestructiveTestsSpec>();
			List<NondestructiveTestsSpec> hazSpecs = new ArrayList<NondestructiveTestsSpec>();
			List<NondestructiveTestsSpec> laminasyonSpecs = new ArrayList<NondestructiveTestsSpec>();
			for (NondestructiveTestsSpec nondestructiveTestsSpec : salesItems
					.get(0).getNondestructiveTestsSpecs()) {
				if (type == 1) {// kaynak
					if (nondestructiveTestsSpec.getGlobalId() == 1001) {
						kaynakSpecs.add(nondestructiveTestsSpec);
					}
					if (nondestructiveTestsSpec.getGlobalId() == 1008) {
						hazSpecs.add(nondestructiveTestsSpec);
					}
				} else if (type == 2) {// laminasyon
					if (nondestructiveTestsSpec.getGlobalId() == 1002) {
						laminasyonSpecs.add(nondestructiveTestsSpec);
					}
				}
			}

			TestTahribatsizOtomatikUtSonucManager testTahribatsizOtomatikUtSonucManager = new TestTahribatsizOtomatikUtSonucManager();
			List<TestTahribatsizOtomatikUtSonuc> testTahribatsizOtomatikUtSonucs = new ArrayList<TestTahribatsizOtomatikUtSonuc>();
			List<TestTahribatsizOtomatikUtSonuc> kaynakHazSonucs = new ArrayList<TestTahribatsizOtomatikUtSonuc>();
			// List<TestTahribatsizOtomatikUtSonuc> hazSonucs = new
			// ArrayList<TestTahribatsizOtomatikUtSonuc>();
			List<TestTahribatsizOtomatikUtSonuc> laminasyonSonucs = new ArrayList<TestTahribatsizOtomatikUtSonuc>();
			testTahribatsizOtomatikUtSonucs = testTahribatsizOtomatikUtSonucManager
					.getAllTestTahribatsizOtomatikUtSonuc(pipeId);

			if (!listControl(testTahribatsizOtomatikUtSonucs)) {
				return;
			}

			for (int i = 0; i < testTahribatsizOtomatikUtSonucs.size(); i++) {
				if (testTahribatsizOtomatikUtSonucs.get(i)
						.getKaynakLaminasyon() == 2) {
					laminasyonSonucs
							.add(testTahribatsizOtomatikUtSonucs.get(i));
				} else {
					kaynakHazSonucs.add(testTahribatsizOtomatikUtSonucs.get(i));
				}
			}

			List<String> date = new ArrayList<String>();
			if (testTahribatsizOtomatikUtSonucs.size() > 0) {
				date.add(new SimpleDateFormat("dd.MM.yyyy")
						.format(testTahribatsizOtomatikUtSonucs.get(0)
								.getEklemeZamani()));
			} else {
				date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			}

			raporNo = pipes.get(0).getPipeIndex();
			muayeneyiYapan = testTahribatsizOtomatikUtSonucs.get(0)
					.getEkleyenEmployee().getEmployee().getFirstname()
					+ " "
					+ testTahribatsizOtomatikUtSonucs.get(0)
							.getEkleyenEmployee().getEmployee().getLastname();
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();

			if (laminasyonSpecs.size() != 0
					&& laminasyonSpecs.get(0).getCode().equals("T02")) {// laminasyon
																		// ise
																		// -
																		// FR-473

				dataMap.put("laminasyonOzellik", laminasyonSpecs);
				dataMap.put("laminasyonSonuc", laminasyonSonucs);
				if (laminasyonSonucs.size() > 0) {
					dataMap.put("kalibrasyon", laminasyonSonucs.get(0)
							.getKalibrasyonLaminasyon());
				}

				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/test/tahribatsiz/FR-473.xls", pipes
								.get(0).getSalesItem().getProposal()
								.getCustomer().getShortName()
								+ "-" + pipes.get(0).getPipeIndex());
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"FR-473 BAŞARIYLA OLUŞTURULDU!", null));
			} else {
				// kaynakHazSonucs.get(0).getPipe().getRuloPipeLink();
				dataMap.put("hazOzellik", hazSpecs);
				dataMap.put("kaynakOzellik", kaynakSpecs);
				dataMap.put("kaynakHazSonuc", kaynakHazSonucs);
				if (kaynakHazSonucs.size() > 0) {
					dataMap.put("kalibrasyon", kaynakHazSonucs.get(0)
							.getKalibrasyonOnline());
				}

				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/test/tahribatsiz/FR-127.xls", pipes
								.get(0).getSalesItem().getProposal()
								.getCustomer().getShortName()
								+ "-" + pipes.get(0).getPipeIndex());
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"FR-127 BAŞARIYLA OLUŞTURULDU!", null));
			}
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void otomatikUtInspectionResultReportAllXlsx(Integer pipeId,
			Integer type) {

		if (pipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = (List<Pipe>) pipeManager.findBetweenTwoParameters(
					Pipe.class, "pipeBarkodNo", firstBarkodNo, secondBarkodNo);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<NondestructiveTestsSpec> kaynakSpecs = new ArrayList<NondestructiveTestsSpec>();
			List<NondestructiveTestsSpec> hazSpecs = new ArrayList<NondestructiveTestsSpec>();
			List<NondestructiveTestsSpec> laminasyonSpecs = new ArrayList<NondestructiveTestsSpec>();
			for (NondestructiveTestsSpec nondestructiveTestsSpec : salesItems
					.get(0).getNondestructiveTestsSpecs()) {
				if (type == 1) {// kaynak
					if (nondestructiveTestsSpec.getGlobalId() == 1001) {
						kaynakSpecs.add(nondestructiveTestsSpec);
					}
					if (nondestructiveTestsSpec.getGlobalId() == 1008) {
						hazSpecs.add(nondestructiveTestsSpec);
					}
				} else if (type == 2) {// laminasyon
					if (nondestructiveTestsSpec.getGlobalId() == 1002) {
						laminasyonSpecs.add(nondestructiveTestsSpec);
					}
				}
			}

			TestTahribatsizOtomatikUtSonucManager testTahribatsizOtomatikUtSonucManager = new TestTahribatsizOtomatikUtSonucManager();
			List<TestTahribatsizOtomatikUtSonuc> testTahribatsizOtomatikUtSonucs = new ArrayList<TestTahribatsizOtomatikUtSonuc>();
			List<TestTahribatsizOtomatikUtSonuc> kaynakHazSonucs = new ArrayList<TestTahribatsizOtomatikUtSonuc>();
			// List<TestTahribatsizOtomatikUtSonuc> hazSonucs = new
			// ArrayList<TestTahribatsizOtomatikUtSonuc>();
			List<TestTahribatsizOtomatikUtSonuc> laminasyonSonucs = new ArrayList<TestTahribatsizOtomatikUtSonuc>();
			testTahribatsizOtomatikUtSonucs = testTahribatsizOtomatikUtSonucManager
					.findBetweenTwoParameters(firstBarkodNo, secondBarkodNo);

			if (!listControl(testTahribatsizOtomatikUtSonucs)) {
				return;
			}

			for (int i = 0; i < testTahribatsizOtomatikUtSonucs.size(); i++) {
				if (testTahribatsizOtomatikUtSonucs.get(i)
						.getKaynakLaminasyon() == 2) {
					laminasyonSonucs
							.add(testTahribatsizOtomatikUtSonucs.get(i));
				} else {
					kaynakHazSonucs.add(testTahribatsizOtomatikUtSonucs.get(i));
				}
			}

			List<String> date = new ArrayList<String>();
			if (testTahribatsizOtomatikUtSonucs.size() > 0) {
				date.add(new SimpleDateFormat("dd.MM.yyyy")
						.format(testTahribatsizOtomatikUtSonucs.get(0)
								.getEklemeZamani()));
			} else {
				date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			}

			raporNo = pipes.get(0).getPipeIndex();
			muayeneyiYapan = testTahribatsizOtomatikUtSonucs.get(0)
					.getEkleyenEmployee().getEmployee().getFirstname()
					+ " "
					+ testTahribatsizOtomatikUtSonucs.get(0)
							.getEkleyenEmployee().getEmployee().getLastname();
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();

			if (laminasyonSpecs.size() != 0
					&& laminasyonSpecs.get(0).getCode().equals("T02")) {// laminasyon
																		// ise
																		// -
																		// FR-473

				dataMap.put("laminasyonOzellik", laminasyonSpecs);
				dataMap.put("laminasyonSonuc", laminasyonSonucs);
				if (laminasyonSonucs.size() > 0) {
					dataMap.put("kalibrasyon", laminasyonSonucs.get(0)
							.getKalibrasyonLaminasyon());
				}

				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/test/tahribatsiz/FR-473.xls", pipes
								.get(0).getSalesItem().getProposal()
								.getCustomer().getShortName()
								+ "-" + pipes.get(0).getPipeIndex());
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"FR-473 BAŞARIYLA OLUŞTURULDU!", null));
			} else {
				// kaynakHazSonucs.get(0).getPipe().getRuloPipeLink();
				dataMap.put("hazOzellik", hazSpecs);
				dataMap.put("kaynakOzellik", kaynakSpecs);
				dataMap.put("kaynakHazSonuc", kaynakHazSonucs);
				if (kaynakHazSonucs.size() > 0) {
					dataMap.put("kalibrasyon", kaynakHazSonucs.get(0)
							.getKalibrasyonOnline());
				}

				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/test/tahribatsiz/FR-127.xls", pipes
								.get(0).getSalesItem().getProposal()
								.getCustomer().getShortName()
								+ "-" + pipes.get(0).getPipeIndex());
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"FR-127 BAŞARIYLA OLUŞTURULDU!", null));
			}
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void manuelUtInspectionResultReportXlsx(Integer pipeId) {

		if (pipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;

		}

		try {

			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", pipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<NondestructiveTestsSpec> manuelSpecs = new ArrayList<NondestructiveTestsSpec>();
			for (NondestructiveTestsSpec nondestructiveTestsSpec : salesItems
					.get(0).getNondestructiveTestsSpecs()) {
				if (nondestructiveTestsSpec.getGlobalId() == 1004)
					manuelSpecs.add(nondestructiveTestsSpec);
			}
			if (manuelSpecs.size() < 1) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"KALİTE TANIMLARI EKSİKTİR. RAPOR OLUŞTURULAMADI, HATA!",
								null));
				return;
			}

			TestTahribatsizManuelUtSonucManager testTahribatsizManuelUtSonucManager = new TestTahribatsizManuelUtSonucManager();
			List<TestTahribatsizManuelUtSonuc> testTahribatsizManuelUtSonucs = new ArrayList<TestTahribatsizManuelUtSonuc>();
			// List<TestTahribatsizManuelUtSonuc> smawSonucs = new
			// ArrayList<TestTahribatsizManuelUtSonuc>();

			testTahribatsizManuelUtSonucs = testTahribatsizManuelUtSonucManager
					.getAllTestTahribatsizManuelUtSonuc(pipeId);

			if (!listControl(testTahribatsizManuelUtSonucs)) {
				return;
			}

			// raporNo = pipes.get(0).getPipeIndex();

			for (int i = 0; i < testTahribatsizManuelUtSonucs.size(); i++) {
				if (testTahribatsizManuelUtSonucs.get(i)
						.getGuncelleyenEmployee() != null) {
					muayeneyiYapan = testTahribatsizManuelUtSonucs.get(i)
							.getGuncelleyenEmployee().getEmployee()
							.getFirstname()
							+ " "
							+ testTahribatsizManuelUtSonucs.get(i)
									.getGuncelleyenEmployee().getEmployee()
									.getLastname();
					break;
				}
			}

			hazirlayan = userBean.getUserFullName();

			List<String> date = new ArrayList<String>();
			if (testTahribatsizManuelUtSonucs.size() > 0) {
				date.add(new SimpleDateFormat("dd.MM.yyyy")
						.format(testTahribatsizManuelUtSonucs.get(0)
								.getKalibrasyon().getKalibrasyonZamani()));
			} else {
				date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			}

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("manuelOzellik", manuelSpecs);
			dataMap.put("manuelSonuc", testTahribatsizManuelUtSonucs);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("date", date);
			dataMap.put("kalibrasyon", testTahribatsizManuelUtSonucs.get(0)
					.getKalibrasyon());

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/tahribatsiz/FR-164.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void manuelUtInspectionResultReportAllXlsx(Integer pipeId) {

		if (pipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;

		}

		try {

			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			if (firstBarkodNo != null || secondBarkodNo != null) {
				pipes = (List<Pipe>) pipeManager.findBetweenTwoParameters(
						Pipe.class, "pipeBarkodNo", firstBarkodNo,
						secondBarkodNo);
			} else {
				pipes = pipeManager.findByField(Pipe.class, "pipeId", pipeId);
			}

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<NondestructiveTestsSpec> manuelSpecs = new ArrayList<NondestructiveTestsSpec>();
			for (NondestructiveTestsSpec nondestructiveTestsSpec : salesItems
					.get(0).getNondestructiveTestsSpecs()) {
				if (nondestructiveTestsSpec.getGlobalId() == 1004)
					manuelSpecs.add(nondestructiveTestsSpec);
			}
			if (manuelSpecs.size() < 1) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"KALİTE TANIMLARI EKSİKTİR. RAPOR OLUŞTURULAMADI, HATA!",
								null));
				return;
			}

			TestTahribatsizManuelUtSonucManager testTahribatsizManuelUtSonucManager = new TestTahribatsizManuelUtSonucManager();
			List<TestTahribatsizManuelUtSonuc> testTahribatsizManuelUtSonucs = new ArrayList<TestTahribatsizManuelUtSonuc>();

			tarihFormatla(baslangicTarihi, bitisTarihi);

			if (raporTarihiAString == null && raporTarihiBString == null) {
				testTahribatsizManuelUtSonucs = testTahribatsizManuelUtSonucManager
						.findBetweenTwoParameters(firstBarkodNo, secondBarkodNo);
			} else {
				testTahribatsizManuelUtSonucs = testTahribatsizManuelUtSonucManager
						.getByItemDate(salesItems.get(0).getItemId(),
								baslangicTarihi, bitisTarihi);
			}

			if (!listControl(testTahribatsizManuelUtSonucs)) {
				return;
			}

			// raporNo = pipes.get(0).getPipeIndex();
			for (int i = 0; i < testTahribatsizManuelUtSonucs.size(); i++) {
				if (testTahribatsizManuelUtSonucs.get(i)
						.getGuncelleyenEmployee() != null) {
					muayeneyiYapan = testTahribatsizManuelUtSonucs.get(i)
							.getGuncelleyenEmployee().getEmployee()
							.getFirstname()
							+ " "
							+ testTahribatsizManuelUtSonucs.get(i)
									.getGuncelleyenEmployee().getEmployee()
									.getLastname();
					break;
				}
			}

			hazirlayan = userBean.getUserFullName();

			List<String> date = new ArrayList<String>();
			if (testTahribatsizManuelUtSonucs.size() > 0) {
				date.add(new SimpleDateFormat("dd.MM.yyyy")
						.format(testTahribatsizManuelUtSonucs.get(0)
								.getKalibrasyon().getKalibrasyonZamani()));
			} else {
				date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			}

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("manuelOzellik", manuelSpecs);
			dataMap.put("manuelSonuc", testTahribatsizManuelUtSonucs);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("date", date);
			dataMap.put("kalibrasyon", testTahribatsizManuelUtSonucs.get(0)
					.getKalibrasyon());

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/tahribatsiz/FR-164.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void floroskopikInspectionResultReportXlsx(Integer pipeId) {

		if (pipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {

			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", pipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<NondestructiveTestsSpec> florosSpecs = new ArrayList<NondestructiveTestsSpec>();
			for (NondestructiveTestsSpec nondestructiveTestsSpec : salesItems
					.get(0).getNondestructiveTestsSpecs()) {
				if (nondestructiveTestsSpec.getGlobalId() == 1006)
					florosSpecs.add(nondestructiveTestsSpec);
			}
			if (florosSpecs.size() < 1) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"KALİTE TANIMLARI EKSİKTİR. RAPOR OLUŞTURULAMADI, HATA!",
								null));
				return;
			}

			TestTahribatsizFloroskopikSonucManager testTahribatsizFloroskopikSonucManager = new TestTahribatsizFloroskopikSonucManager();
			List<TestTahribatsizFloroskopikSonuc> testTahribatsizFloroskopikSonucs = new ArrayList<TestTahribatsizFloroskopikSonuc>();
			testTahribatsizFloroskopikSonucs = testTahribatsizFloroskopikSonucManager
					.getAllTestTahribatsizFloroskopikSonuc(pipeId);

			if (!listControl(testTahribatsizFloroskopikSonucs)) {
				return;
			}

			List<String> date = new ArrayList<String>();
			if (testTahribatsizFloroskopikSonucs.size() > 0) {
				date.add(new SimpleDateFormat("dd.MM.yyyy")
						.format(testTahribatsizFloroskopikSonucs.get(0)
								.getEklemeZamani()));
			} else {
				date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			}

			raporNo = pipes.get(0).getPipeIndex();
			for (int i = 0; i < testTahribatsizFloroskopikSonucs.size(); i++) {
				if (testTahribatsizFloroskopikSonucs.get(i)
						.getGuncelleyenEmployee() != null) {
					muayeneyiYapan = testTahribatsizFloroskopikSonucs.get(i)
							.getGuncelleyenEmployee().getEmployee()
							.getFirstname()
							+ " "
							+ testTahribatsizFloroskopikSonucs.get(i)
									.getGuncelleyenEmployee().getEmployee()
									.getLastname();
					break;
				}
			}

			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("florosSpecs", florosSpecs);
			dataMap.put("floroskopikSonuc", testTahribatsizFloroskopikSonucs);
			dataMap.put("date", date);
			dataMap.put("boruUcu", "A");
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("kalibrasyon", testTahribatsizFloroskopikSonucs.get(0)
					.getKalibrasyon());

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/tahribatsiz/FR-122.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void floroskopikInspectionResultReportAllXlsx(Integer pipeId) {

		if (pipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {

			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			if (firstBarkodNo != null || secondBarkodNo != null) {
				pipes = (List<Pipe>) pipeManager.findBetweenTwoParameters(
						Pipe.class, "pipeBarkodNo", firstBarkodNo,
						secondBarkodNo);
			} else {
				pipes = pipeManager.findByField(Pipe.class, "pipeId", pipeId);
			}

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<NondestructiveTestsSpec> florosSpecs = new ArrayList<NondestructiveTestsSpec>();
			for (NondestructiveTestsSpec nondestructiveTestsSpec : salesItems
					.get(0).getNondestructiveTestsSpecs()) {
				if (nondestructiveTestsSpec.getGlobalId() == 1006)
					florosSpecs.add(nondestructiveTestsSpec);
			}
			if (florosSpecs.size() < 1) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"KALİTE TANIMLARI EKSİKTİR. RAPOR OLUŞTURULAMADI, HATA!",
								null));
				return;
			}

			TestTahribatsizFloroskopikSonucManager testTahribatsizFloroskopikSonucManager = new TestTahribatsizFloroskopikSonucManager();
			List<TestTahribatsizFloroskopikSonuc> testTahribatsizFloroskopikSonucs = new ArrayList<TestTahribatsizFloroskopikSonuc>();

			tarihFormatla(baslangicTarihi, bitisTarihi);

			if (raporTarihiAString == null && raporTarihiBString == null) {
				testTahribatsizFloroskopikSonucs = testTahribatsizFloroskopikSonucManager
						.findBetweenTwoParameters(firstBarkodNo, secondBarkodNo);
			} else {
				testTahribatsizFloroskopikSonucs = testTahribatsizFloroskopikSonucManager
						.getByItemDate(salesItems.get(0).getItemId(),
								baslangicTarihi, bitisTarihi);
			}

			if (!listControl(testTahribatsizFloroskopikSonucs)) {
				return;
			}

			List<String> date = new ArrayList<String>();
			if (testTahribatsizFloroskopikSonucs.size() > 0) {
				date.add(new SimpleDateFormat("dd.MM.yyyy")
						.format(testTahribatsizFloroskopikSonucs.get(0)
								.getEklemeZamani()));
			} else {
				date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			}

			raporNo = pipes.get(0).getPipeIndex();

			for (int i = 0; i < testTahribatsizFloroskopikSonucs.size(); i++) {
				if (testTahribatsizFloroskopikSonucs.get(i)
						.getGuncelleyenEmployee() != null) {
					muayeneyiYapan = testTahribatsizFloroskopikSonucs.get(i)
							.getGuncelleyenEmployee().getEmployee()
							.getFirstname()
							+ " "
							+ testTahribatsizFloroskopikSonucs.get(i)
									.getGuncelleyenEmployee().getEmployee()
									.getLastname();
					break;
				}
			}

			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("floroskopikOzellik", florosSpecs);
			dataMap.put("floroskopikSonuc", testTahribatsizFloroskopikSonucs);
			dataMap.put("date", date);
			dataMap.put("boruUcu", "A");
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("kalibrasyon", testTahribatsizFloroskopikSonucs.get(0)
					.getKalibrasyon());

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/tahribatsiz/FR-122.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void gozOlcuInspectionResultReportXlsx(Integer pipeId) {

		if (pipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", pipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			BoruBoyutsalKontrolToleransManager toleransManager = new BoruBoyutsalKontrolToleransManager();
			List<BoruBoyutsalKontrolTolerans> boruBoyutsalKontrolTolerans = new ArrayList<BoruBoyutsalKontrolTolerans>();
			boruBoyutsalKontrolTolerans = toleransManager
					.seciliBoyutsalKontrol(salesItems.get(0).getItemId());
			if (boruBoyutsalKontrolTolerans.size() < 1) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"BORU BOYUTSAL KONTROL ve TOLERANSLARI GİRİLMEMİŞTİR, KALİTE VERİLERİ BOŞTUR. RAPOR OLUŞTURULAMADI, HATA!",
								null));
				return;
			}

			TestTahribatsizGozOlcuSonucManager testTahribatsizGozOlcuSonucManager = new TestTahribatsizGozOlcuSonucManager();
			List<TestTahribatsizGozOlcuSonuc> testTahribatsizGozOlcuSonucs = new ArrayList<TestTahribatsizGozOlcuSonuc>();
			testTahribatsizGozOlcuSonucs = testTahribatsizGozOlcuSonucManager
					.findByField(TestTahribatsizGozOlcuSonuc.class,
							"pipe.pipeId", pipeId);

			if (!listControl(testTahribatsizGozOlcuSonucs)) {
				return;
			}

			List<String> date = new ArrayList<String>();
			if (testTahribatsizGozOlcuSonucs.size() > 0) {
				date.add(new SimpleDateFormat("dd.MM.yyyy")
						.format(testTahribatsizGozOlcuSonucs.get(0)
								.getEklemeZamani()));
			} else {
				date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			}

			raporNo = pipes.get(0).getPipeIndex();

			for (int i = 0; i < testTahribatsizGozOlcuSonucs.size(); i++) {
				if (testTahribatsizGozOlcuSonucs.get(i)
						.getGuncelleyenEmployee() != null) {
					muayeneyiYapan = testTahribatsizGozOlcuSonucs.get(i)
							.getGuncelleyenEmployee().getEmployee()
							.getFirstname()
							+ " "
							+ testTahribatsizGozOlcuSonucs.get(i)
									.getGuncelleyenEmployee().getEmployee()
									.getLastname();
					break;
				} else {
					muayeneyiYapan = testTahribatsizGozOlcuSonucs.get(i)
							.getEkleyenEmployee().getEmployee().getFirstname()
							+ " "
							+ testTahribatsizGozOlcuSonucs.get(i)
									.getEkleyenEmployee().getEmployee()
									.getLastname();
				}
			}

			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("gozOlcuSonuc", testTahribatsizGozOlcuSonucs);
			dataMap.put("tolerans", boruBoyutsalKontrolTolerans.get(0));
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/tahribatsiz/FR-18.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void gozOlcuInspectionResultReportAllKabulXlsx(SalesItem salesItem) {

		try {

			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByItemId(salesItem.getItemId());

			BoruBoyutsalKontrolToleransManager toleransManager = new BoruBoyutsalKontrolToleransManager();
			List<BoruBoyutsalKontrolTolerans> boruBoyutsalKontrolTolerans = new ArrayList<BoruBoyutsalKontrolTolerans>();
			boruBoyutsalKontrolTolerans = toleransManager
					.seciliBoyutsalKontrol(salesItem.getItemId());
			if (boruBoyutsalKontrolTolerans.size() < 1) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"BORU BOYUTSAL KONTROL ve TOLERANSLARI GİRİLMEMİŞTİR, KALİTE VERİLERİ BOŞTUR. RAPOR OLUŞTURULAMADI, HATA!",
								null));
				return;
			}

			TestTahribatsizGozOlcuSonucManager testTahribatsizGozOlcuSonucManager = new TestTahribatsizGozOlcuSonucManager();
			List<TestTahribatsizGozOlcuSonuc> testTahribatsizGozOlcuSonucs = new ArrayList<TestTahribatsizGozOlcuSonuc>();

			tarihFormatla(baslangicTarihi, bitisTarihi);

			if (raporTarihiAString == null && raporTarihiBString == null) {
				testTahribatsizGozOlcuSonucs = testTahribatsizGozOlcuSonucManager
						.findByItemIdSonuc(salesItem.getItemId());
			} else {
				testTahribatsizGozOlcuSonucs = testTahribatsizGozOlcuSonucManager
						.getByItemDate(salesItem.getItemId(), baslangicTarihi,
								bitisTarihi, 1);
			}

			if (!listControl(testTahribatsizGozOlcuSonucs)) {
				return;
			}

			List<String> date = new ArrayList<String>();
			if (testTahribatsizGozOlcuSonucs.size() > 0) {
				date.add(new SimpleDateFormat("dd.MM.yyyy")
						.format(testTahribatsizGozOlcuSonucs.get(0)
								.getBoruKabulTarihi()));
			} else {
				date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			}

			raporNo = pipes.get(0).getPipeIndex();
			// muayeneyiYapan = testTahribatsizGozOlcuSonucs.get(0)
			// .getGuncelleyenEmployee().getEmployee().getFirstname()
			// + " "
			// + testTahribatsizGozOlcuSonucs.get(0)
			// .getGuncelleyenEmployee().getEmployee()
			// .getLastname();
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItem);
			dataMap.put("gozOlcuSonuc", testTahribatsizGozOlcuSonucs);
			dataMap.put("tolerans", boruBoyutsalKontrolTolerans.get(0));
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/tahribatsiz/FR-18.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	private boolean listControl(List<?> liste) {
		if (liste == null || liste.size() == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"RAPOR OLUŞTURACAK VERİ YOKTUR.", null));
			return false;
		}
		return true;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void gozOlcuInspectionResultReportAllXlsx(SalesItem salesItem) {

		try {

			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByItemId(salesItem.getItemId());

			BoruBoyutsalKontrolToleransManager toleransManager = new BoruBoyutsalKontrolToleransManager();
			List<BoruBoyutsalKontrolTolerans> boruBoyutsalKontrolTolerans = new ArrayList<BoruBoyutsalKontrolTolerans>();
			boruBoyutsalKontrolTolerans = toleransManager
					.seciliBoyutsalKontrol(salesItem.getItemId());
			if (boruBoyutsalKontrolTolerans.size() < 1) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"BORU BOYUTSAL KONTROL ve TOLERANSLARI GİRİLMEMİŞTİR, KALİTE VERİLERİ BOŞTUR. RAPOR OLUŞTURULAMADI, HATA!",
								null));
				return;
			}

			TestTahribatsizGozOlcuSonucManager testTahribatsizGozOlcuSonucManager = new TestTahribatsizGozOlcuSonucManager();
			List<TestTahribatsizGozOlcuSonuc> testTahribatsizGozOlcuSonucs = new ArrayList<TestTahribatsizGozOlcuSonuc>();

			tarihFormatla(baslangicTarihi, bitisTarihi);

			if (raporTarihiAString == null && raporTarihiBString == null) {
				testTahribatsizGozOlcuSonucs = testTahribatsizGozOlcuSonucManager
						.findByItemIdSonucAll(salesItem.getItemId());
			} else {
				testTahribatsizGozOlcuSonucs = testTahribatsizGozOlcuSonucManager
						.getByItemDate(salesItem.getItemId(), baslangicTarihi,
								bitisTarihi, 0);
			}

			if (!listControl(testTahribatsizGozOlcuSonucs)) {
				return;
			}

			List<String> date = new ArrayList<String>();
			if (testTahribatsizGozOlcuSonucs.size() > 0) {
				date.add(new SimpleDateFormat("dd.MM.yyyy")
						.format(testTahribatsizGozOlcuSonucs.get(0)
								.getBoruKabulTarihi()));
			} else {
				date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			}

			raporNo = pipes.get(0).getPipeIndex();
			// muayeneyiYapan = testTahribatsizGozOlcuSonucs.get(0)
			// .getGuncelleyenEmployee().getEmployee().getFirstname()
			// + " "
			// + testTahribatsizGozOlcuSonucs.get(0)
			// .getGuncelleyenEmployee().getEmployee()
			// .getLastname();
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItem);
			dataMap.put("gozOlcuSonuc", testTahribatsizGozOlcuSonucs);
			dataMap.put("tolerans", boruBoyutsalKontrolTolerans.get(0));
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/tahribatsiz/FR-18.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void radyografikInspectionResultReportXlsx(Integer pipeId) {

		if (pipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {

			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", pipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<NondestructiveTestsSpec> nondestructiveTestsSpecs = new ArrayList<NondestructiveTestsSpec>();
			for (NondestructiveTestsSpec nondestructiveTestsSpec : salesItems
					.get(0).getNondestructiveTestsSpecs()) {
				if (nondestructiveTestsSpec.getGlobalId() == 1038)
					nondestructiveTestsSpecs.add(nondestructiveTestsSpec);
			}
			if (nondestructiveTestsSpecs.size() < 1) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"KALİTE TANIMLARI EKSİKTİR. RAPOR OLUŞTURULAMADI, HATA!",
								null));
				return;
			}

			TestTahribatsizRadyografikSonucManager testTahribatsizRadyografikSonucManager = new TestTahribatsizRadyografikSonucManager();
			List<TestTahribatsizRadyografikSonuc> testTahribatsizRadyografikSonucs = new ArrayList<TestTahribatsizRadyografikSonuc>();
			testTahribatsizRadyografikSonucs = testTahribatsizRadyografikSonucManager
					.getAllTestTahribatsizRadyografikSonuc(pipeId);

			if (!listControl(testTahribatsizRadyografikSonucs)) {
				return;
			}

			List<String> date = new ArrayList<String>();
			if (testTahribatsizRadyografikSonucs.size() > 0) {
				date.add(new SimpleDateFormat("dd.MM.yyyy")
						.format(testTahribatsizRadyografikSonucs.get(0)
								.getEklemeZamani()));
			} else {
				date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			}

			raporNo = pipes.get(0).getPipeIndex();
			muayeneyiYapan = testTahribatsizRadyografikSonucs.get(0)
					.getEkleyenEmployee().getEmployee().getFirstname()
					+ " "
					+ testTahribatsizRadyografikSonucs.get(0)
							.getEkleyenEmployee().getEmployee().getLastname();
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("siparisRadyografikOzellik", nondestructiveTestsSpecs);
			dataMap.put("radyografikSonuc", testTahribatsizRadyografikSonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("kalibrasyon", testTahribatsizRadyografikSonucs.get(0)
					.getKalibrasyon());

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/tahribatsiz/FR-109.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void radyografikInspectionResultReportAllXlsx(Integer pipeId) {

		if (pipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {

			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = (List<Pipe>) pipeManager.findBetweenTwoParameters(
					Pipe.class, "pipeBarkodNo", firstBarkodNo, secondBarkodNo);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<NondestructiveTestsSpec> nondestructiveTestsSpecs = new ArrayList<NondestructiveTestsSpec>();
			for (NondestructiveTestsSpec nondestructiveTestsSpec : salesItems
					.get(0).getNondestructiveTestsSpecs()) {
				if (nondestructiveTestsSpec.getGlobalId() == 1038)
					nondestructiveTestsSpecs.add(nondestructiveTestsSpec);
			}
			if (nondestructiveTestsSpecs.size() < 1) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"KALİTE TANIMLARI EKSİKTİR. RAPOR OLUŞTURULAMADI, HATA!",
								null));
				return;
			}

			TestTahribatsizRadyografikSonucManager testTahribatsizRadyografikSonucManager = new TestTahribatsizRadyografikSonucManager();
			List<TestTahribatsizRadyografikSonuc> testTahribatsizRadyografikSonucs = new ArrayList<TestTahribatsizRadyografikSonuc>();
			testTahribatsizRadyografikSonucs = testTahribatsizRadyografikSonucManager
					.findBetweenTwoParameters(firstBarkodNo, secondBarkodNo);

			if (!listControl(testTahribatsizRadyografikSonucs)) {
				return;
			}

			List<String> date = new ArrayList<String>();
			if (testTahribatsizRadyografikSonucs.size() > 0) {
				date.add(new SimpleDateFormat("dd.MM.yyyy")
						.format(testTahribatsizRadyografikSonucs.get(0)
								.getEklemeZamani()));
			} else {
				date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			}

			raporNo = pipes.get(0).getPipeIndex();
			muayeneyiYapan = testTahribatsizRadyografikSonucs.get(0)
					.getEkleyenEmployee().getEmployee().getFirstname()
					+ " "
					+ testTahribatsizRadyografikSonucs.get(0)
							.getEkleyenEmployee().getEmployee().getLastname();
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("siparisRadyografikOzellik", nondestructiveTestsSpecs);
			dataMap.put("radyografikSonuc", testTahribatsizRadyografikSonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("kalibrasyon", testTahribatsizRadyografikSonucs.get(0)
					.getKalibrasyon());

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/tahribatsiz/FR-109.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void manyetikParcacikInspectionResultReportXlsx(Integer pipeId,
			Integer prmType) {

		if (pipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", pipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<NondestructiveTestsSpec> manyetikSpecs = new ArrayList<NondestructiveTestsSpec>();
			for (NondestructiveTestsSpec nondestructiveTestsSpec : salesItems
					.get(0).getNondestructiveTestsSpecs()) {
				if (nondestructiveTestsSpec.getGlobalId() == 1007)
					manyetikSpecs.add(nondestructiveTestsSpec);
			}
			if (manyetikSpecs.size() < 1) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"KALİTE TANIMLARI EKSİKTİR. RAPOR OLUŞTURULAMADI, HATA!",
								null));
				return;
			}

			TestTahribatsizManyetikParcacikSonucManager testTahribatsizManyetikParcacikSonucManager = new TestTahribatsizManyetikParcacikSonucManager();
			List<TestTahribatsizManyetikParcacikSonuc> testTahribatsizManyetikParcacikSonucs = new ArrayList<TestTahribatsizManyetikParcacikSonuc>();
			List<TestTahribatsizManyetikParcacikSonuc> selectedSonucs = new ArrayList<TestTahribatsizManyetikParcacikSonuc>();
			testTahribatsizManyetikParcacikSonucs = testTahribatsizManyetikParcacikSonucManager
					.getAllTestTahribatsizManyetikParcacikSonuc(pipeId);

			if (!listControl(testTahribatsizManyetikParcacikSonucs)) {
				return;
			}

			SpecMagneticManager specManager = new SpecMagneticManager();
			SpecMagnetic spec = new SpecMagnetic();
			try {
				spec = specManager.getAllSpecMagnetic(pipeId).get(0);
			} catch (Exception e) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"YÜZEY, ISIL İŞLEM ve YÜZEY SICAKLIĞI GİRİLMEMİŞ!",
						null));
				return;
			}

			for (int i = 0; i < testTahribatsizManyetikParcacikSonucs.size(); i++) {
				if (prmType == 1) {
					if (testTahribatsizManyetikParcacikSonucs.get(i)
							.getHataTipi() == 1) {
						selectedSonucs
								.add(testTahribatsizManyetikParcacikSonucs
										.get(i));
					}
				} else if (prmType == 2) {
					if (testTahribatsizManyetikParcacikSonucs.get(i)
							.getHataTipi() == 2) {
						selectedSonucs
								.add(testTahribatsizManyetikParcacikSonucs
										.get(i));
					}
				} else if (prmType == 3) {
					if (testTahribatsizManyetikParcacikSonucs.get(i)
							.getHataTipi() == 3) {
						selectedSonucs
								.add(testTahribatsizManyetikParcacikSonucs
										.get(i));
					}
				}

			}

			List<String> date = new ArrayList<String>();
			if (testTahribatsizManyetikParcacikSonucs.size() > 0) {
				date.add(new SimpleDateFormat("dd.MM.yyyy")
						.format(testTahribatsizManyetikParcacikSonucs.get(0)
								.getEklemeZamani()));
			} else {
				date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			}

			raporNo = pipes.get(0).getPipeIndex();
			muayeneyiYapan = testTahribatsizManyetikParcacikSonucs.get(0)
					.getEkleyenEmployee().getEmployee().getFirstname()
					+ " "
					+ testTahribatsizManyetikParcacikSonucs.get(0)
							.getEkleyenEmployee().getEmployee().getLastname();
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("manyetikSpecs", manyetikSpecs);
			dataMap.put("manyetikParcacikSonuc", selectedSonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("kalibrasyon", selectedSonucs.get(0).getKalibrasyon());
			dataMap.put("spec", spec);

			ReportUtil reportUtil = new ReportUtil();
			if (prmType == 1) {
				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/test/tahribatsiz/FR-242.xls", pipes
								.get(0).getSalesItem().getProposal()
								.getCustomer().getShortName()
								+ "-" + pipes.get(0).getPipeIndex());
			} else if (prmType == 2) {
				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/test/tahribatsiz/FR-242Taslanmis.xls",
						pipes.get(0).getSalesItem().getProposal().getCustomer()
								.getShortName()
								+ "-" + pipes.get(0).getPipeIndex());
			} else if (prmType == 3) {
				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/test/tahribatsiz/FR-242Tamir.xls",
						pipes.get(0).getSalesItem().getProposal().getCustomer()
								.getShortName()
								+ "-" + pipes.get(0).getPipeIndex());
			}

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "unchecked", "static-access", "rawtypes" })
	public void manyetikParcacikInspectionResultReportAllXlsx(Integer pipeId,
			Integer prmType) {

		if (pipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = (List<Pipe>) pipeManager.findBetweenTwoParameters(
					Pipe.class, "pipeBarkodNo", firstBarkodNo, secondBarkodNo);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<NondestructiveTestsSpec> manyetikSpecs = new ArrayList<NondestructiveTestsSpec>();
			for (NondestructiveTestsSpec nondestructiveTestsSpec : salesItems
					.get(0).getNondestructiveTestsSpecs()) {
				if (nondestructiveTestsSpec.getGlobalId() == 1007)
					manyetikSpecs.add(nondestructiveTestsSpec);
			}
			if (manyetikSpecs.size() < 1) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"KALİTE TANIMLARI EKSİKTİR. RAPOR OLUŞTURULAMADI, HATA!",
								null));
				return;
			}

			TestTahribatsizManyetikParcacikSonucManager testTahribatsizManyetikParcacikSonucManager = new TestTahribatsizManyetikParcacikSonucManager();
			List<TestTahribatsizManyetikParcacikSonuc> testTahribatsizManyetikParcacikSonucs = new ArrayList<TestTahribatsizManyetikParcacikSonuc>();
			List<TestTahribatsizManyetikParcacikSonuc> selectedSonucs = new ArrayList<TestTahribatsizManyetikParcacikSonuc>();
			testTahribatsizManyetikParcacikSonucs = testTahribatsizManyetikParcacikSonucManager
					.findBetweenTwoParameters(firstBarkodNo, secondBarkodNo);

			if (!listControl(testTahribatsizManyetikParcacikSonucs)) {
				return;
			}

			SpecMagneticManager specManager = new SpecMagneticManager();
			SpecMagnetic spec = new SpecMagnetic();
			try {
				spec = specManager.getAllSpecMagnetic(pipeId).get(0);
			} catch (Exception e) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"YÜZEY, ISIL İŞLEM ve YÜZEY SICAKLIĞI GİRİLMEMİŞ!",
						null));
				return;
			}

			for (int i = 0; i < testTahribatsizManyetikParcacikSonucs.size(); i++) {
				if (prmType == 1) {
					if (testTahribatsizManyetikParcacikSonucs.get(i)
							.getHataTipi() == 1) {
						selectedSonucs
								.add(testTahribatsizManyetikParcacikSonucs
										.get(i));
					}
				} else if (prmType == 2) {
					if (testTahribatsizManyetikParcacikSonucs.get(i)
							.getHataTipi() == 2) {
						selectedSonucs
								.add(testTahribatsizManyetikParcacikSonucs
										.get(i));
					}
				} else if (prmType == 3) {
					if (testTahribatsizManyetikParcacikSonucs.get(i)
							.getHataTipi() == 3) {
						selectedSonucs
								.add(testTahribatsizManyetikParcacikSonucs
										.get(i));
					}
				}

			}

			List<String> date = new ArrayList<String>();
			if (testTahribatsizManyetikParcacikSonucs.size() > 0) {
				date.add(new SimpleDateFormat("dd.MM.yyyy")
						.format(testTahribatsizManyetikParcacikSonucs.get(0)
								.getEklemeZamani()));
			} else {
				date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			}

			raporNo = pipes.get(0).getPipeIndex();
			muayeneyiYapan = testTahribatsizManyetikParcacikSonucs.get(0)
					.getEkleyenEmployee().getEmployee().getFirstname()
					+ " "
					+ testTahribatsizManyetikParcacikSonucs.get(0)
							.getEkleyenEmployee().getEmployee().getLastname();
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("manyetikSpecs", manyetikSpecs);
			dataMap.put("manyetikParcacikSonuc", selectedSonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("spec", spec);
			ReportUtil reportUtil = new ReportUtil();

			if (prmType == 1) {
				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/test/tahribatsiz/FR-242.xls", pipes
								.get(0).getSalesItem().getProposal()
								.getCustomer().getShortName()
								+ "-" + pipes.get(0).getPipeIndex());
			} else if (prmType == 2) {
				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/test/tahribatsiz/FR-242Taslanmis.xls",
						pipes.get(0).getSalesItem().getProposal().getCustomer()
								.getShortName()
								+ "-" + pipes.get(0).getPipeIndex());
			} else if (prmType == 3) {
				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/test/tahribatsiz/FR-242Tamir.xls",
						pipes.get(0).getSalesItem().getProposal().getCustomer()
								.getShortName()
								+ "-" + pipes.get(0).getPipeIndex());
			}

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void penetrantInspectionResultReportXlsx(Integer pipeId) {

		if (pipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {

			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", pipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<NondestructiveTestsSpec> penetrantSpecs = new ArrayList<NondestructiveTestsSpec>();
			for (NondestructiveTestsSpec nondestructiveTestsSpec : salesItems
					.get(0).getNondestructiveTestsSpecs()) {
				if (nondestructiveTestsSpec.getGlobalId() == 1009)
					penetrantSpecs.add(nondestructiveTestsSpec);
			}
			if (penetrantSpecs.size() < 1) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"KALİTE TANIMLARI EKSİKTİR. RAPOR OLUŞTURULAMADI, HATA!",
								null));
				return;
			}

			TestTahribatsizPenetrantSonucManager testTahribatsizPenetrantSonucManager = new TestTahribatsizPenetrantSonucManager();
			List<TestTahribatsizPenetrantSonuc> testTahribatsizPenetrantSonucs = new ArrayList<TestTahribatsizPenetrantSonuc>();
			testTahribatsizPenetrantSonucs = testTahribatsizPenetrantSonucManager
					.getAllTestTahribatsizPenetrantSonuc(pipeId);

			if (!listControl(testTahribatsizPenetrantSonucs)) {
				return;
			}

			SpecPenetrantManager specManager = new SpecPenetrantManager();
			SpecPenetrant spec = new SpecPenetrant();
			try {
				spec = specManager.getAllSpecPenetrant(pipeId).get(0);
			} catch (Exception e) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"YÜZEY, ISIL İŞLEM ve YÜZEY SICAKLIĞI GİRİLMEMİŞ!",
						null));
				return;
			}

			List<String> date = new ArrayList<String>();
			if (testTahribatsizPenetrantSonucs.size() > 0) {
				date.add(new SimpleDateFormat("dd.MM.yyyy")
						.format(testTahribatsizPenetrantSonucs.get(0)
								.getEklemeZamani()));
			} else {
				date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			}

			raporNo = pipes.get(0).getPipeIndex();
			muayeneyiYapan = testTahribatsizPenetrantSonucs.get(0)
					.getEkleyenEmployee().getEmployee().getFirstname()
					+ " "
					+ testTahribatsizPenetrantSonucs.get(0)
							.getEkleyenEmployee().getEmployee().getLastname();
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("penetrantSpecs", penetrantSpecs);
			dataMap.put("penetrantSonuc", testTahribatsizPenetrantSonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("kalibrasyon", testTahribatsizPenetrantSonucs.get(0)
					.getKalibrasyon());
			dataMap.put("spec", spec);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/tahribatsiz/FR-243.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void penetrantInspectionResultReportAllXlsx(Integer pipeId) {

		if (pipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {

			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = (List<Pipe>) pipeManager.findBetweenTwoParameters(
					Pipe.class, "pipeBarkodNo", firstBarkodNo, secondBarkodNo);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<NondestructiveTestsSpec> nondestructiveTestsSpecs = new ArrayList<NondestructiveTestsSpec>();
			for (NondestructiveTestsSpec nondestructiveTestsSpec : salesItems
					.get(0).getNondestructiveTestsSpecs()) {
				if (nondestructiveTestsSpec.getGlobalId() == 1009)
					nondestructiveTestsSpecs.add(nondestructiveTestsSpec);
			}
			if (nondestructiveTestsSpecs.size() < 1) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"KALİTE TANIMLARI EKSİKTİR. RAPOR OLUŞTURULAMADI, HATA!",
								null));
				return;
			}

			TestTahribatsizPenetrantSonucManager testTahribatsizPenetrantSonucManager = new TestTahribatsizPenetrantSonucManager();
			List<TestTahribatsizPenetrantSonuc> testTahribatsizPenetrantSonucs = new ArrayList<TestTahribatsizPenetrantSonuc>();
			testTahribatsizPenetrantSonucs = testTahribatsizPenetrantSonucManager
					.findBetweenTwoParameters(firstBarkodNo, secondBarkodNo);

			if (!listControl(testTahribatsizPenetrantSonucs)) {
				return;
			}

			SpecPenetrantManager specManager = new SpecPenetrantManager();
			SpecPenetrant spec = new SpecPenetrant();
			try {
				spec = specManager.getAllSpecPenetrant(pipeId).get(0);
			} catch (Exception e) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"YÜZEY, ISIL İŞLEM ve YÜZEY SICAKLIĞI GİRİLMEMİŞ!",
						null));
				return;
			}

			List<String> date = new ArrayList<String>();
			if (testTahribatsizPenetrantSonucs.size() > 0) {
				date.add(new SimpleDateFormat("dd.MM.yyyy")
						.format(testTahribatsizPenetrantSonucs.get(0)
								.getEklemeZamani()));
			} else {
				date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			}

			raporNo = pipes.get(0).getPipeIndex();
			muayeneyiYapan = testTahribatsizPenetrantSonucs.get(0)
					.getEkleyenEmployee().getEmployee().getFirstname()
					+ " "
					+ testTahribatsizPenetrantSonucs.get(0)
							.getEkleyenEmployee().getEmployee().getLastname();
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("siparisPenetrantOzellik", nondestructiveTestsSpecs);
			dataMap.put("penetrantSonuc", testTahribatsizPenetrantSonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("kalibrasyon", testTahribatsizPenetrantSonucs.get(0)
					.getKalibrasyon());
			dataMap.put("spec", spec);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/tahribatsiz/FR-243.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void kaynakGozleInspectionResultReportXlsx(Integer pipeId) {

		if (pipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {

			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", pipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<NondestructiveTestsSpec> nondestructiveTestsSpecs = new ArrayList<NondestructiveTestsSpec>();
			for (NondestructiveTestsSpec nondestructiveTestsSpec : salesItems
					.get(0).getNondestructiveTestsSpecs()) {
				if (nondestructiveTestsSpec.getGlobalId() == 1039)
					nondestructiveTestsSpecs.add(nondestructiveTestsSpec);
			}
			if (nondestructiveTestsSpecs.size() < 1) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"KALİTE TANIMLARI EKSİKTİR. RAPOR OLUŞTURULAMADI, HATA!",
								null));
				return;
			}

			TestTahribatsizKaynakGozleMuayeneSonucManager testTahribatsizKaynakGozleSonucManager = new TestTahribatsizKaynakGozleMuayeneSonucManager();
			List<TestTahribatsizKaynakGozleMuayeneSonuc> testTahribatsizKaynakGozleSonucs = new ArrayList<TestTahribatsizKaynakGozleMuayeneSonuc>();
			testTahribatsizKaynakGozleSonucs = testTahribatsizKaynakGozleSonucManager
					.getAllTestTahribatsizKaynakGozleMuayeneSonuc(pipeId);

			if (!listControl(testTahribatsizKaynakGozleSonucs)) {
				return;
			}

			List<String> date = new ArrayList<String>();
			if (testTahribatsizKaynakGozleSonucs.size() > 0) {
				date.add(new SimpleDateFormat("dd.MM.yyyy")
						.format(testTahribatsizKaynakGozleSonucs.get(0)
								.getEklemeZamani()));
			} else {
				date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			}

			raporNo = pipes.get(0).getPipeIndex();
			muayeneyiYapan = testTahribatsizKaynakGozleSonucs.get(0)
					.getEkleyenEmployee().getEmployee().getFirstname()
					+ " "
					+ testTahribatsizKaynakGozleSonucs.get(0)
							.getEkleyenEmployee().getEmployee().getLastname();
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("siparisKaynakGozleOzellik", nondestructiveTestsSpecs);
			dataMap.put("kaynakGozleSonuc", testTahribatsizKaynakGozleSonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/tahribatsiz/FR-298.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void kaynakGozleInspectionResultReportAllXlsx(Integer pipeId) {

		if (pipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {

			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = (List<Pipe>) pipeManager.findBetweenTwoParameters(
					Pipe.class, "pipeBarkodNo", firstBarkodNo, secondBarkodNo);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<NondestructiveTestsSpec> nondestructiveTestsSpecs = new ArrayList<NondestructiveTestsSpec>();
			for (NondestructiveTestsSpec nondestructiveTestsSpec : salesItems
					.get(0).getNondestructiveTestsSpecs()) {
				if (nondestructiveTestsSpec.getGlobalId() == 1039)
					nondestructiveTestsSpecs.add(nondestructiveTestsSpec);
			}
			if (nondestructiveTestsSpecs.size() < 1) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"KALİTE TANIMLARI EKSİKTİR. RAPOR OLUŞTURULAMADI, HATA!",
								null));
				return;
			}

			TestTahribatsizKaynakGozleMuayeneSonucManager testTahribatsizKaynakGozleSonucManager = new TestTahribatsizKaynakGozleMuayeneSonucManager();
			List<TestTahribatsizKaynakGozleMuayeneSonuc> testTahribatsizKaynakGozleSonucs = new ArrayList<TestTahribatsizKaynakGozleMuayeneSonuc>();
			testTahribatsizKaynakGozleSonucs = testTahribatsizKaynakGozleSonucManager
					.findBetweenTwoParameters(firstBarkodNo, secondBarkodNo);

			if (!listControl(testTahribatsizKaynakGozleSonucs)) {
				return;
			}

			List<String> date = new ArrayList<String>();
			if (testTahribatsizKaynakGozleSonucs.size() > 0) {
				date.add(new SimpleDateFormat("dd.MM.yyyy")
						.format(testTahribatsizKaynakGozleSonucs.get(0)
								.getEklemeZamani()));
			} else {
				date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			}

			raporNo = pipes.get(0).getPipeIndex();
			muayeneyiYapan = testTahribatsizKaynakGozleSonucs.get(0)
					.getEkleyenEmployee().getEmployee().getFirstname()
					+ " "
					+ testTahribatsizKaynakGozleSonucs.get(0)
							.getEkleyenEmployee().getEmployee().getLastname();
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("siparisKaynakGozleOzellik", nondestructiveTestsSpecs);
			dataMap.put("kaynakGozleSonuc", testTahribatsizKaynakGozleSonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();
			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/tahribatsiz/FR-298.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "RAPOR BAŞARIYLA OLUŞTURULDU!",
					null));
		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void offlineOtomatikUtInspectionResultReportXlsx(Integer pipeId,
			Integer type) {

		if (pipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", pipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<NondestructiveTestsSpec> boruUcuSpecs = new ArrayList<NondestructiveTestsSpec>();
			List<NondestructiveTestsSpec> kaynakSpecs = new ArrayList<NondestructiveTestsSpec>();
			List<NondestructiveTestsSpec> hazSpecs = new ArrayList<NondestructiveTestsSpec>();
			List<NondestructiveTestsSpec> laminasyonSpecs = new ArrayList<NondestructiveTestsSpec>();
			for (NondestructiveTestsSpec nondestructiveTestsSpec : salesItems
					.get(0).getNondestructiveTestsSpecs()) {
				if (type == 1) {// kaynak haz
					if (nondestructiveTestsSpec.getGlobalId() == 1042) {
						hazSpecs.add(nondestructiveTestsSpec);
					} else if (nondestructiveTestsSpec.getGlobalId() == 1003) {
						kaynakSpecs.add(nondestructiveTestsSpec);
					}
				} else if (type == 3) {// laminasyon
					if (nondestructiveTestsSpec.getGlobalId() == 1041) {
						laminasyonSpecs.add(nondestructiveTestsSpec);
					}
				} else if (type == 4) {// boru ucu
					if (nondestructiveTestsSpec.getGlobalId() == 1005) {
						boruUcuSpecs.add(nondestructiveTestsSpec);
					}
				}
			}

			TestTahribatsizOfflineOtomatikUtSonucManager testTahribatsizOfflineOtomatikUtSonucManager = new TestTahribatsizOfflineOtomatikUtSonucManager(); // ///////////////
			List<TestTahribatsizOfflineOtomatikUtSonuc> testTahribatsizOfflineOtomatikUtSonucs = new ArrayList<TestTahribatsizOfflineOtomatikUtSonuc>();
			List<TestTahribatsizOfflineOtomatikUtSonuc> boruUcuSonucs = new ArrayList<TestTahribatsizOfflineOtomatikUtSonuc>();
			List<TestTahribatsizOfflineOtomatikUtSonuc> kaynakHazSonucs = new ArrayList<TestTahribatsizOfflineOtomatikUtSonuc>();
			// List<TestTahribatsizOfflineOtomatikUtSonuc> hazSonucs = new
			// ArrayList<TestTahribatsizOfflineOtomatikUtSonuc>();
			List<TestTahribatsizOfflineOtomatikUtSonuc> laminasyonSonucs = new ArrayList<TestTahribatsizOfflineOtomatikUtSonuc>();
			testTahribatsizOfflineOtomatikUtSonucs = testTahribatsizOfflineOtomatikUtSonucManager
					.getAllTestTahribatsizOfflineOtomatikUtSonuc(pipeId);
			for (int i = 0; i < testTahribatsizOfflineOtomatikUtSonucs.size(); i++) {
				if (testTahribatsizOfflineOtomatikUtSonucs.get(i)
						.getKaynakLaminasyon() == 1
						|| testTahribatsizOfflineOtomatikUtSonucs.get(i)
								.getKaynakLaminasyon() == 3) {// kaynak haz
					kaynakHazSonucs.add(testTahribatsizOfflineOtomatikUtSonucs
							.get(i));
				} else if (testTahribatsizOfflineOtomatikUtSonucs.get(i)
						.getKaynakLaminasyon() == 2) {// laminasyon
					laminasyonSonucs.add(testTahribatsizOfflineOtomatikUtSonucs
							.get(i));
				} else if (testTahribatsizOfflineOtomatikUtSonucs.get(i)
						.getKaynakLaminasyon() > 3) {// boru ucu
					boruUcuSonucs.add(testTahribatsizOfflineOtomatikUtSonucs
							.get(i));
				}
			}

			if (!listControl(testTahribatsizOfflineOtomatikUtSonucs)) {
				return;
			}

			List<String> date = new ArrayList<String>();
			if (testTahribatsizOfflineOtomatikUtSonucs.size() > 0) {
				date.add(new SimpleDateFormat("dd.MM.yyyy")
						.format(testTahribatsizOfflineOtomatikUtSonucs.get(0)
								.getEklemeZamani()));
			} else {
				date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			}

			raporNo = pipes.get(0).getPipeIndex();
			muayeneyiYapan = testTahribatsizOfflineOtomatikUtSonucs.get(0)
					.getEkleyenEmployee().getEmployee().getFirstname()
					+ " "
					+ testTahribatsizOfflineOtomatikUtSonucs.get(0)
							.getEkleyenEmployee().getEmployee().getLastname();
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("offlineUTSonuc",
					testTahribatsizOfflineOtomatikUtSonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();

			if (kaynakSpecs.size() > 0
					&& kaynakSpecs.get(0).getCode().equals("T03")) {// kaynak
																	// haz
				// ise -
				// FR-127

				dataMap.put("kaynakOzellik", kaynakSpecs);
				dataMap.put("hazOzellik", hazSpecs);
				dataMap.put("kaynakHazSonuc", kaynakHazSonucs);
				dataMap.put("kalibrasyon", kaynakHazSonucs.get(0)
						.getKalibrasyonOffline());

				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/test/tahribatsiz/FR-127.xls", pipes
								.get(0).getSalesItem().getProposal()
								.getCustomer().getShortName()
								+ "-" + pipes.get(0).getPipeIndex());
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"FR-127 BAŞARIYLA OLUŞTURULDU!", null));

			} else if (laminasyonSpecs.size() > 0
					&& laminasyonSpecs.get(0).getCode().equals("T13")) {// laminasyon
																		// ise
																		// -
																		// FR-473

				dataMap.put("laminasyonOzellik", laminasyonSpecs);
				dataMap.put("laminasyonSonuc", laminasyonSonucs);
				dataMap.put("kalibrasyon", laminasyonSonucs.get(0)
						.getKalibrasyonLaminasyon());

				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/test/tahribatsiz/FR-473.xls", pipes
								.get(0).getSalesItem().getProposal()
								.getCustomer().getShortName()
								+ "-" + pipes.get(0).getPipeIndex());
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"FR-473 BAŞARIYLA OLUŞTURULDU!", null));
			} else if (boruUcuSpecs.size() != 0
					&& boruUcuSpecs.get(0).getCode().equals("T05")) {// boru
																		// ucu
																		// ise
																		// -
																		// FR472

				dataMap.put("boruUcuOzellik", boruUcuSpecs);
				dataMap.put("boruUcuSonuc", boruUcuSonucs);
				dataMap.put("kalibrasyon", boruUcuSonucs.get(0)
						.getKalibrasyonBoruUcu());

				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/test/tahribatsiz/FR-472.xls", pipes
								.get(0).getSalesItem().getProposal()
								.getCustomer().getShortName()
								+ "-" + pipes.get(0).getPipeIndex());
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"FR-473 BAŞARIYLA OLUŞTURULDU!", null));
			}

		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void offlineOtomatikUtInspectionResultReportAllXlsx(Integer pipeId,
			Integer type) {

		if (pipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = (List<Pipe>) pipeManager.findBetweenTwoParameters(
					Pipe.class, "pipeBarkodNo", firstBarkodNo, secondBarkodNo);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<NondestructiveTestsSpec> boruUcuSpecs = new ArrayList<NondestructiveTestsSpec>();
			List<NondestructiveTestsSpec> kaynakSpecs = new ArrayList<NondestructiveTestsSpec>();
			List<NondestructiveTestsSpec> hazSpecs = new ArrayList<NondestructiveTestsSpec>();
			List<NondestructiveTestsSpec> laminasyonSpecs = new ArrayList<NondestructiveTestsSpec>();
			for (NondestructiveTestsSpec nondestructiveTestsSpec : salesItems
					.get(0).getNondestructiveTestsSpecs()) {
				if (type == 1) {// kaynak haz
					if (nondestructiveTestsSpec.getGlobalId() == 1003) {
						kaynakSpecs.add(nondestructiveTestsSpec);
					}
					if (nondestructiveTestsSpec.getGlobalId() == 1003) {
						hazSpecs.add(nondestructiveTestsSpec);
					}
				} else if (type == 3) {// laminasyon
					if (nondestructiveTestsSpec.getGlobalId() == 1003) {
						laminasyonSpecs.add(nondestructiveTestsSpec);
					}
				} else if (type == 4) {// boru ucu
					if (nondestructiveTestsSpec.getGlobalId() == 1005) {
						boruUcuSpecs.add(nondestructiveTestsSpec);
					}
				}
			}

			TestTahribatsizOfflineOtomatikUtSonucManager testTahribatsizOfflineOtomatikUtSonucManager = new TestTahribatsizOfflineOtomatikUtSonucManager(); // ///////////////
			List<TestTahribatsizOfflineOtomatikUtSonuc> testTahribatsizOfflineOtomatikUtSonucs = new ArrayList<TestTahribatsizOfflineOtomatikUtSonuc>();
			List<TestTahribatsizOfflineOtomatikUtSonuc> boruUcuSonucs = new ArrayList<TestTahribatsizOfflineOtomatikUtSonuc>();
			List<TestTahribatsizOfflineOtomatikUtSonuc> kaynakHazSonucs = new ArrayList<TestTahribatsizOfflineOtomatikUtSonuc>();
			// List<TestTahribatsizOfflineOtomatikUtSonuc> hazSonucs = new
			// ArrayList<TestTahribatsizOfflineOtomatikUtSonuc>();
			List<TestTahribatsizOfflineOtomatikUtSonuc> laminasyonSonucs = new ArrayList<TestTahribatsizOfflineOtomatikUtSonuc>();
			testTahribatsizOfflineOtomatikUtSonucs = testTahribatsizOfflineOtomatikUtSonucManager
					.findBetweenTwoParameters(firstBarkodNo, secondBarkodNo);
			for (int i = 0; i < testTahribatsizOfflineOtomatikUtSonucs.size(); i++) {
				if (testTahribatsizOfflineOtomatikUtSonucs.get(i)
						.getKaynakLaminasyon() == 1) {
					kaynakHazSonucs.add(testTahribatsizOfflineOtomatikUtSonucs
							.get(i));
				} else if (testTahribatsizOfflineOtomatikUtSonucs.get(i)
						.getKaynakLaminasyon() == 2) {
					laminasyonSonucs.add(testTahribatsizOfflineOtomatikUtSonucs
							.get(i));
				} else {
					boruUcuSonucs.add(testTahribatsizOfflineOtomatikUtSonucs
							.get(i));
				}
			}

			if (!listControl(testTahribatsizOfflineOtomatikUtSonucs)) {
				return;
			}

			List<String> date = new ArrayList<String>();
			if (testTahribatsizOfflineOtomatikUtSonucs.size() > 0) {
				date.add(new SimpleDateFormat("dd.MM.yyyy")
						.format(testTahribatsizOfflineOtomatikUtSonucs.get(0)
								.getEklemeZamani()));
			} else {
				date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			}

			raporNo = pipes.get(0).getPipeIndex();
			muayeneyiYapan = testTahribatsizOfflineOtomatikUtSonucs.get(0)
					.getEkleyenEmployee().getEmployee().getFirstname()
					+ " "
					+ testTahribatsizOfflineOtomatikUtSonucs.get(0)
							.getEkleyenEmployee().getEmployee().getLastname();
			hazirlayan = userBean.getUserFullName();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("offlineUTSonuc",
					testTahribatsizOfflineOtomatikUtSonucs);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);

			ReportUtil reportUtil = new ReportUtil();

			if (kaynakSpecs.size() > 0
					&& kaynakSpecs.get(0).getCode().equals("T03")) {// kaynak
																	// haz
				// ise -
				// FR-127

				dataMap.put("kaynakOzellik", kaynakSpecs);
				dataMap.put("hazOzellik", hazSpecs);
				dataMap.put("kaynakHazSonuc", kaynakHazSonucs);
				dataMap.put("kalibrasyon", kaynakHazSonucs.get(0)
						.getKalibrasyonOffline());

				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/test/tahribatsiz/FR-127.xls", pipes
								.get(0).getSalesItem().getProposal()
								.getCustomer().getShortName()
								+ "-" + pipes.get(0).getPipeIndex());
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"FR-127 BAŞARIYLA OLUŞTURULDU!", null));

			} else if (laminasyonSpecs.size() > 0
					&& laminasyonSpecs.get(0).getCode().equals("T03")) {// laminasyon
																		// ise
																		// -
																		// FR-473

				dataMap.put("laminasyonOzellik", laminasyonSpecs);
				dataMap.put("laminasyonSonuc", laminasyonSonucs);
				dataMap.put("kalibrasyon", laminasyonSonucs.get(0)
						.getKalibrasyonLaminasyon());

				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/test/tahribatsiz/FR-473.xls", pipes
								.get(0).getSalesItem().getProposal()
								.getCustomer().getShortName()
								+ "-" + pipes.get(0).getPipeIndex());
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"FR-473 BAŞARIYLA OLUŞTURULDU!", null));
			} else if (boruUcuSpecs.size() != 0
					&& boruUcuSpecs.get(0).getCode().equals("T05")) {// boru
																		// ucu
																		// ise
																		// -
																		// FR472

				dataMap.put("boruUcuOzellik", boruUcuSpecs);
				dataMap.put("boruUcuSonuc", boruUcuSonucs);
				dataMap.put("kalibrasyon", boruUcuSonucs.get(0)
						.getKalibrasyonBoruUcu());

				reportUtil.jxlsExportAsXls(dataMap,
						"/reportformats/test/tahribatsiz/FR-472.xls", pipes
								.get(0).getSalesItem().getProposal()
								.getCustomer().getShortName()
								+ "-" + pipes.get(0).getPipeIndex());
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"FR-473 BAŞARIYLA OLUŞTURULDU!", null));
			}

		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "rawtypes", "unchecked", "static-access" })
	public void hidrostatikResultReportXlsx(Integer prmPipeId) {

		if (raporVardiya == -1) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "VARDİYA SEÇİNİZ!", null));
			return;
		}
		if (prmPipeId == 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "ÖNCE BORU SEÇİNİZ!", null));
			return;
		}
		if (raporTarihi == null || raporVardiya == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"ÖNCE 'RAPOR HAZIRLA' TUŞUYA RAPOR HAZIRLAYINIZ!", null));
			return;
		}

		tarihBul(raporTarihi);

		try {
			PipeManager pipeManager = new PipeManager();
			List<Pipe> pipes = new ArrayList<Pipe>();
			pipes = pipeManager.findByField(Pipe.class, "pipeId", prmPipeId);

			SalesItemManager salesItemManager = new SalesItemManager();
			List<SalesItem> salesItems = new ArrayList<SalesItem>();
			salesItems = salesItemManager.findByField(SalesItem.class,
					"itemId", pipes.get(0).getSalesItem().getItemId());

			List<HidrostaticTestsSpec> hidroSpecs = new ArrayList<HidrostaticTestsSpec>();
			HidrostaticTestsSpecManager specManager = new HidrostaticTestsSpecManager();
			hidroSpecs = specManager
					.findByItemId(salesItems.get(0).getItemId());

			TestTahribatsizHidrostatikManager testTahribatsizHidrostatikManager = new TestTahribatsizHidrostatikManager();
			List<TestTahribatsizHidrostatik> testTahribatsizHidrostatiks = new ArrayList<TestTahribatsizHidrostatik>();
			if (raporVardiya == 1) {
				testTahribatsizHidrostatiks = testTahribatsizHidrostatikManager
						.getByItemDate(pipes.get(0).getPipeId(),
								raporTarihiAString, raporTarihiBString,
								raporVardiya);
			} else if (raporVardiya == 0) {
				testTahribatsizHidrostatiks = testTahribatsizHidrostatikManager
						.getByItemDate(pipes.get(0).getPipeId(),
								raporTarihiAString, raporTarihiBString,
								raporVardiya);
			}

			List<String> date = new ArrayList<String>();
			if (testTahribatsizHidrostatiks.size() > 0) {
				date.add(new SimpleDateFormat("dd.MM.yyyy")
						.format(testTahribatsizHidrostatiks.get(0)
								.getEklemeZamani()));
			} else {
				date.add(new SimpleDateFormat("dd.MM.yyyy").format(new Date()));
			}

			raporNo = pipes.get(0).getPipeIndex();
			muayeneyiYapan = testTahribatsizHidrostatiks.get(0).getUser()
					.getEmployee().getFirstname()
					+ " "
					+ testTahribatsizHidrostatiks.get(0).getUser()
							.getEmployee().getLastname();
			hazirlayan = userBean.getUserFullName();

			ReportUtil reportUtil = new ReportUtil();

			Map dataMap = new HashMap();
			dataMap.put("siparis", salesItems);
			dataMap.put("date", date);
			dataMap.put("raporNo", raporNo);
			dataMap.put("muayeneyiYapan", muayeneyiYapan);
			dataMap.put("hazirlayan", hazirlayan);
			dataMap.put("hidroSonuc", testTahribatsizHidrostatiks);
			dataMap.put("hidroOzellik", hidroSpecs);
			dataMap.put("hidroSize", testTahribatsizHidrostatiks.size());

			if (raporVardiya == 1) {
				dataMap.put("vardiya", "GÜNDÜZ");
			} else if (raporVardiya == 0) {
				dataMap.put("vardiya", "GECE");
			}

			reportUtil.jxlsExportAsXls(dataMap,
					"/reportformats/test/tahribatsiz/FR-144.xls", pipes.get(0)
							.getSalesItem().getProposal().getCustomer()
							.getShortName()
							+ "-" + pipes.get(0).getPipeIndex());

			downloadedFileName = "FR-144"
					+ pipes.get(0).getSalesItem().getProposal().getCustomer()
							.getShortName() + "-" + pipes.get(0).getPipeIndex()
					+ ".xls";

			getRealFilePath("/reportformats/test/tahribatsiz/"
					+ downloadedFileName);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO,
					"FR-144 BAŞARIYLA OLUŞTURULDU!", null));

		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "RAPOR OLUŞTURULAMADI, HATA!",
					null));
			e.printStackTrace();
		}

	}

	@SuppressWarnings("deprecation")
	public void tarihBul(Date prmTestDate) {

		if (prmTestDate == null)
			return;

		raporTarihiAString = Integer.toString(raporTarihi.getYear() + 1900)
				+ "/" + Integer.toString(raporTarihi.getMonth() + 1) + "/"
				+ Integer.toString(raporTarihi.getDate());
		raporTarihiBString = Integer.toString(raporTarihi.getYear() + 1900)
				+ "/" + Integer.toString(raporTarihi.getMonth() + 1) + "/"
				+ Integer.toString(raporTarihi.getDate() + 1);
	}

	public void tarihFormatla(Date prmTestDate1, Date prmTestDate2) {

		if (prmTestDate1 == null || prmTestDate2 == null)
			return;

		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		raporTarihiAString = format.format(prmTestDate1);
		raporTarihiBString = format.format(prmTestDate2);
	}

	// download için
	@SuppressWarnings("resource")
	public StreamedContent getDownloadedFile() {

		try {
			if (downloadedFileName != null) {
				RandomAccessFile accessFile = new RandomAccessFile(
						realFilePath, "r");
				byte[] downloadByte = new byte[(int) accessFile.length()];
				accessFile.read(downloadByte);
				InputStream stream = new ByteArrayInputStream(downloadByte);

				String suffix = FilenameUtils.getExtension(downloadedFileName);
				String contentType = "text/plain";
				if (suffix.contentEquals("jpg") || suffix.contentEquals("png")
						|| suffix.contentEquals("gif")) {
					contentType = "image/" + contentType;
				} else if (suffix.contentEquals("doc")
						|| suffix.contentEquals("docx")) {
					contentType = "application/msword";
				} else if (suffix.contentEquals("xls")
						|| suffix.contentEquals("xlsx")) {
					contentType = "application/vnd.ms-excel";
				} else if (suffix.contentEquals("pdf")) {
					contentType = "application/pdf";
				}
				downloadedFile = new DefaultStreamedContent(stream,
						"contentType", downloadedFileName);

				stream.close();
			} else {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"LÜTFEN RAPOR OLUŞTURUNUZ, RAPOR BULUNAMADI!"));
			}
		} catch (Exception e) {
			System.out
					.println("testNondestructiveReportBean.getDownloadedFile():"
							+ e.toString());
		}
		return downloadedFile;
	}

	private String getRealFilePath(String filePath) {

		realFilePath = FacesContext.getCurrentInstance().getExternalContext()
				.getRealPath(filePath);

		return realFilePath;
	}

	// setters getters

	/**
	 * @return the config
	 */
	public ConfigBean getConfig() {
		return config;
	}

	/**
	 * @param config
	 *            the config to set
	 */
	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the raporVardiya
	 */
	public Integer getRaporVardiya() {
		return raporVardiya;
	}

	/**
	 * @param raporVardiya
	 *            the raporVardiya to set
	 */
	public void setRaporVardiya(Integer raporVardiya) {
		this.raporVardiya = raporVardiya;
	}

	/**
	 * @return the raporTarihi
	 */
	public Date getRaporTarihi() {
		return raporTarihi;
	}

	/**
	 * @param raporTarihi
	 *            the raporTarihi to set
	 */
	public void setRaporTarihi(Date raporTarihi) {
		this.raporTarihi = raporTarihi;
	}

	/**
	 * @return the downloadedFileName
	 */
	public String getDownloadedFileName() {
		return downloadedFileName;
	}

	/**
	 * @param downloadedFileName
	 *            the downloadedFileName to set
	 */
	public void setDownloadedFileName(String downloadedFileName) {
		this.downloadedFileName = downloadedFileName;
	}

	/**
	 * @param downloadedFile
	 *            the downloadedFile to set
	 */
	public void setDownloadedFile(StreamedContent downloadedFile) {
		this.downloadedFile = downloadedFile;
	}

	/**
	 * @return the firstBarkodNo
	 */
	public String getFirstBarkodNo() {
		return firstBarkodNo;
	}

	/**
	 * @param firstBarkodNo
	 *            the firstBarkodNo to set
	 */
	public void setFirstBarkodNo(String firstBarkodNo) {
		this.firstBarkodNo = firstBarkodNo;
	}

	/**
	 * @return the secondBarkodNo
	 */
	public String getSecondBarkodNo() {
		return secondBarkodNo;
	}

	/**
	 * @param secondBarkodNo
	 *            the secondBarkodNo to set
	 */
	public void setSecondBarkodNo(String secondBarkodNo) {
		this.secondBarkodNo = secondBarkodNo;
	}

	/**
	 * @return the baslangicTarihi
	 */
	public Date getBaslangicTarihi() {
		return baslangicTarihi;
	}

	/**
	 * @param baslangicTarihi
	 *            the baslangicTarihi to set
	 */
	public void setBaslangicTarihi(Date baslangicTarihi) {
		this.baslangicTarihi = baslangicTarihi;
	}

	/**
	 * @return the bitisTarihi
	 */
	public Date getBitisTarihi() {
		return bitisTarihi;
	}

	/**
	 * @param bitisTarihi
	 *            the bitisTarihi to set
	 */
	public void setBitisTarihi(Date bitisTarihi) {
		this.bitisTarihi = bitisTarihi;
	}

}
