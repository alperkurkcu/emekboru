/**
 * 
 */
package com.emekboru.beans.kaplamamakinesi;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isemri.IsEmriPolietilenKaplama;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiPolietilen;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isemirleri.IsEmriPolietilenKaplamaManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamamakinesi.KaplamaMakinesiPolietilenManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "kaplamaMakinesiPolietilenBean")
@ViewScoped
public class KaplamaMakinesiPolietilenBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<KaplamaMakinesiPolietilen> allKaplamaMakinesiPolietilenList = new ArrayList<KaplamaMakinesiPolietilen>();
	private KaplamaMakinesiPolietilen kaplamaMakinesiPolietilenForm = new KaplamaMakinesiPolietilen();

	private KaplamaMakinesiPolietilen newPolietilen = new KaplamaMakinesiPolietilen();
	private List<KaplamaMakinesiPolietilen> polietilenList;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	private Pipe selectedPipe = new Pipe();

	private boolean kontrol = false;

	private String barkodNo = null;

	private IsEmriPolietilenKaplama selectedIsEmriKaplama = new IsEmriPolietilenKaplama();

	// kaplama kalite
	private List<IsolationTestDefinition> internalIsolationTestDefinitions = new ArrayList<IsolationTestDefinition>();
	private String tozEpoksiKalinligi = null;
	private String yapistiriciKalinligi = null;
	private String polietilenKalinligi = null;

	public KaplamaMakinesiPolietilenBean() {

	}

	public void addPolietilen(ActionEvent ae) {

		UICommand cmd = (UICommand) ae.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId != null) {
			PipeManager pipeMan = new PipeManager();
			selectedPipe = pipeMan.loadObject(Pipe.class, prmPipeId);
		}

		try {
			KaplamaMakinesiPolietilenManager manager = new KaplamaMakinesiPolietilenManager();

			newPolietilen.setEklemeZamani(UtilInsCore.getTarihZaman());
			newPolietilen.setEkleyenKullanici(userBean.getUser().getId());
			newPolietilen.setPipe(selectedPipe);
			manager.enterNew(newPolietilen);
			newPolietilen = new KaplamaMakinesiPolietilen();

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"POLİETİLEN KAPLAMA İŞLEMİ BAŞARIYLA EKLENMİŞTİR!"));

		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL,
					"POLİETİLEN KAPLAMA İŞLEMİ EKLENEMEDİ!", null));
			System.out
					.println("KaplamaMakinesiPolietilenBean.addPolietilen-HATA");
		}

		fillTestList(prmPipeId);
	}

	public void addPolietilenSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		Boolean prmKontrol = kaplamaMakinesiPolietilenForm.getDurum();
		KaplamaMakinesiPolietilenManager polietilenManager = new KaplamaMakinesiPolietilenManager();

		if (prmKontrol == null) {

			try {

				kaplamaMakinesiPolietilenForm.setDurum(false);
				kaplamaMakinesiPolietilenForm
						.setKaplamaBaslamaZamani(UtilInsCore.getTarihZaman());
				kaplamaMakinesiPolietilenForm
						.setKaplamaBaslamaKullanici(userBean.getUser().getId());
				polietilenManager.updateEntity(kaplamaMakinesiPolietilenForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"POLİETİLEN KAPLAMA İŞLEMİ BAŞARIYLA BAŞLANMIŞTIR!"));
			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"POLİETİLEN KAPLAMA İŞLEMİ BAŞLANAMADI!", null));
				System.out
						.println("KaplamaMakinesiPolietilenBean.addPolietilenSonuc-HATA-BAŞLAMA");
			}
		} else if (prmKontrol == false) {

			try {

				kaplamaMakinesiPolietilenForm.setDurum(true);
				kaplamaMakinesiPolietilenForm.setKaplamaBitisZamani(UtilInsCore
						.getTarihZaman());
				kaplamaMakinesiPolietilenForm.setKaplamaBitisKullanici(userBean
						.getUser().getId());
				polietilenManager.updateEntity(kaplamaMakinesiPolietilenForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"POLİETİLEN KAPLAMA İŞLEMİ BAŞARIYLA BİTİRİLMİŞTİR!"));

			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"POLİETİLEN KAPLAMA İŞLEMİ BİTİRİLEMEDİ!", null));
				System.out
						.println("KaplamaMakinesiPolietilenBean.addPolietilenSonuc-HATA-BİTİŞ"
								+ e.toString());
			}

		}

		fillTestList(prmPipeId);
	}

	@SuppressWarnings("static-access")
	public void fillTestList(Integer prmPipeId) {

		KaplamaMakinesiPolietilenManager polietilenManager = new KaplamaMakinesiPolietilenManager();
		allKaplamaMakinesiPolietilenList = polietilenManager
				.getAllKaplamaMakinesiPolietilen(prmPipeId);
	}

	public void kaplamaListener(SelectEvent event) {
		updateButtonRender = true;
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!", null));
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			KaplamaMakinesiPolietilenManager polietilenManager = new KaplamaMakinesiPolietilenManager();
			allKaplamaMakinesiPolietilenList = polietilenManager
					.getAllKaplamaMakinesiPolietilen(selectedPipe.getPipeId());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
		}
	}

	public void deletePolietilenSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		KaplamaMakinesiPolietilenManager polietilenManager = new KaplamaMakinesiPolietilenManager();

		if (kaplamaMakinesiPolietilenForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		polietilenManager.delete(kaplamaMakinesiPolietilenForm);
		kaplamaMakinesiPolietilenForm = new KaplamaMakinesiPolietilen();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"POLİETİLEN KAPLAMA BAŞARIYLA SİLİNMİŞTİR!"));

		fillTestList(prmPipeId);
	}

	@SuppressWarnings("static-access")
	public void isEmriGoster() {

		IsolationTestDefinitionManager kaliteMan = new IsolationTestDefinitionManager();
		IsEmriPolietilenKaplamaManager isEmriManager = new IsEmriPolietilenKaplamaManager();
		if (isEmriManager.getAllIsEmriPolietilenKaplama(
				selectedPipe.getSalesItem().getItemId()).size() > 0) {
			selectedIsEmriKaplama = isEmriManager
					.getAllIsEmriPolietilenKaplama(
							selectedPipe.getSalesItem().getItemId()).get(0);
		} else {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"POLİETİLEN İŞ EMRİ SİSTEMDE YOKTUR, LÜTFEN EKLETİNİZ!",
					null));
			return;
		}

		internalIsolationTestDefinitions = kaliteMan
				.findIsolationByItemIdAndIsolationType(0,
						selectedIsEmriKaplama.getSalesItem());
		for (int i = 0; i < internalIsolationTestDefinitions.size(); i++) {
			if (internalIsolationTestDefinitions.get(i).getGlobalId() == 2024) {
				yapistiriciKalinligi = internalIsolationTestDefinitions.get(i)
						.getAciklama();
			} else if (internalIsolationTestDefinitions.get(i).getGlobalId() == 2023) {
				tozEpoksiKalinligi = internalIsolationTestDefinitions.get(i)
						.getAciklama();
			} else if (internalIsolationTestDefinitions.get(i).getGlobalId() == 2014) {
				polietilenKalinligi = internalIsolationTestDefinitions.get(i)
						.getAciklama();
			}
		}
	}

	// setters getters

	public List<KaplamaMakinesiPolietilen> getAllKaplamaMakinesiPolietilenList() {
		return allKaplamaMakinesiPolietilenList;
	}

	public void setAllKaplamaMakinesiPolietilenList(
			List<KaplamaMakinesiPolietilen> allKaplamaMakinesiPolietilenList) {
		this.allKaplamaMakinesiPolietilenList = allKaplamaMakinesiPolietilenList;
	}

	public KaplamaMakinesiPolietilen getKaplamaMakinesiPolietilenForm() {
		return kaplamaMakinesiPolietilenForm;
	}

	public void setKaplamaMakinesiPolietilenForm(
			KaplamaMakinesiPolietilen kaplamaMakinesiPolietilenForm) {
		this.kaplamaMakinesiPolietilenForm = kaplamaMakinesiPolietilenForm;
	}

	public KaplamaMakinesiPolietilen getNewPolietilen() {
		return newPolietilen;
	}

	public void setNewPolietilen(KaplamaMakinesiPolietilen newPolietilen) {
		this.newPolietilen = newPolietilen;
	}

	public List<KaplamaMakinesiPolietilen> getPolietilenList() {
		return polietilenList;
	}

	public void setPolietilenList(List<KaplamaMakinesiPolietilen> polietilenList) {
		this.polietilenList = polietilenList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	public boolean isKontrol() {
		return kontrol;
	}

	public void setKontrol(boolean kontrol) {
		this.kontrol = kontrol;
	}

	public String getBarkodNo() {
		return barkodNo;
	}

	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public IsEmriPolietilenKaplama getSelectedIsEmriKaplama() {
		return selectedIsEmriKaplama;
	}

	public void setSelectedIsEmriKaplama(
			IsEmriPolietilenKaplama selectedIsEmriKaplama) {
		this.selectedIsEmriKaplama = selectedIsEmriKaplama;
	}

	/**
	 * @return the tozEpoksiKalinligi
	 */
	public String getTozEpoksiKalinligi() {
		return tozEpoksiKalinligi;
	}

	/**
	 * @param tozEpoksiKalinligi
	 *            the tozEpoksiKalinligi to set
	 */
	public void setTozEpoksiKalinligi(String tozEpoksiKalinligi) {
		this.tozEpoksiKalinligi = tozEpoksiKalinligi;
	}

	/**
	 * @return the yapistiriciKalinligi
	 */
	public String getYapistiriciKalinligi() {
		return yapistiriciKalinligi;
	}

	/**
	 * @param yapistiriciKalinligi
	 *            the yapistiriciKalinligi to set
	 */
	public void setYapistiriciKalinligi(String yapistiriciKalinligi) {
		this.yapistiriciKalinligi = yapistiriciKalinligi;
	}

	/**
	 * @return the polietilenKalinligi
	 */
	public String getPolietilenKalinligi() {
		return polietilenKalinligi;
	}

	/**
	 * @param polietilenKalinligi
	 *            the polietilenKalinligi to set
	 */
	public void setPolietilenKalinligi(String polietilenKalinligi) {
		this.polietilenKalinligi = polietilenKalinligi;
	}
}
