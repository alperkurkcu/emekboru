/**
 * 
 */
package com.emekboru.beans.satinalma;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.component.accordionpanel.AccordionPanel;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.converters.EmployeeConverter;
import com.emekboru.jpa.Employee;
import com.emekboru.jpa.satinalma.SatinalmaDurum;
import com.emekboru.jpa.satinalma.SatinalmaMalzemeHizmetTalepFormu;
import com.emekboru.jpa.satinalma.SatinalmaMalzemeHizmetTalepFormuIcerik;
import com.emekboru.jpa.satinalma.SatinalmaTakipDosyalar;
import com.emekboru.jpa.satinalma.SatinalmaTakipDosyalarIcerik;
import com.emekboru.jpaman.satinalma.SatinalmaDurumManager;
import com.emekboru.jpaman.satinalma.SatinalmaMalzemeHizmetTalepFormuIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaMalzemeHizmetTalepFormuManager;
import com.emekboru.jpaman.satinalma.SatinalmaTakipDosyalarIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaTakipDosyalarManager;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "satinalmaTakipDosyalarIcerikBean")
@ViewScoped
public class SatinalmaTakipDosyalarIcerikBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<SatinalmaTakipDosyalarIcerik> allSatinalmaTakipDosyalarIcerikList = new ArrayList<SatinalmaTakipDosyalarIcerik>();
	private List<SatinalmaTakipDosyalarIcerik> allSatinalmaTakipDosyalarIcerikByGorevliIdList = new ArrayList<SatinalmaTakipDosyalarIcerik>();
	private List<SatinalmaTakipDosyalarIcerik> allSatinalmaTakipDosyalarIcerikByGorevliIdListMain = new ArrayList<SatinalmaTakipDosyalarIcerik>();

	private List<SatinalmaTakipDosyalarIcerik> dosyaIcerikList = new ArrayList<SatinalmaTakipDosyalarIcerik>();

	private SatinalmaTakipDosyalarIcerik satinalmaTakipDosyalarIcerikForm = new SatinalmaTakipDosyalarIcerik();
	private SatinalmaMalzemeHizmetTalepFormu satinalmaMalzemeHizmetTalepFormu = new SatinalmaMalzemeHizmetTalepFormu();

	private List<SatinalmaMalzemeHizmetTalepFormu> allYonlendirilenler = new ArrayList<SatinalmaMalzemeHizmetTalepFormu>();
	private List<SatinalmaMalzemeHizmetTalepFormu> allYonlendirilenlerMain = new ArrayList<SatinalmaMalzemeHizmetTalepFormu>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	private List<SatinalmaMalzemeHizmetTalepFormuIcerik> malzemeIcerikTrueList = new ArrayList<SatinalmaMalzemeHizmetTalepFormuIcerik>();

	private SatinalmaDurum satinalmaDurum = new SatinalmaDurum();

	public static List<Employee> employees;

	private SatinalmaTakipDosyalarIcerik[] selectedIcerik;

	private SatinalmaTakipDosyalar satinalmaTakipDosyalarForm = new SatinalmaTakipDosyalar();

	// kalem kalem liste
	private List<SatinalmaTakipDosyalarIcerik> banaYonlendirilenlerListesi = new ArrayList<SatinalmaTakipDosyalarIcerik>();

	String activeIndex = "0";

	@PostConstruct
	public void load() {

		// fillTestList();
		allSatinalmaTakipDosyalarIcerikList.clear();
		satinalmaTakipDosyalarIcerikForm = new SatinalmaTakipDosyalarIcerik();
		satinalmaMalzemeHizmetTalepFormu = new SatinalmaMalzemeHizmetTalepFormu();
		malzemeIcerikTrueList.clear();
		allSatinalmaTakipDosyalarIcerikByGorevliIdList.clear();

		employees = EmployeeConverter.employeeDB;
	}

	public void onTabChange(TabChangeEvent event) {
		activeIndex = ((AccordionPanel) event.getComponent()).getActiveIndex();

	}

	public void fillTestList() {

		allYonlendirilenlerMain.clear();
		SatinalmaTakipDosyalarIcerikManager manager = new SatinalmaTakipDosyalarIcerikManager();
		allSatinalmaTakipDosyalarIcerikList = manager
				.getAllSatinalmaTakipDosyalarIcerik();
		SatinalmaMalzemeHizmetTalepFormuManager manager1 = new SatinalmaMalzemeHizmetTalepFormuManager();
		allYonlendirilenler = manager1.findYonlendirilenler(userBean.getUser()
				.getEmployee().getEmployeeId());

		for (int i = 0; i < allYonlendirilenler.size(); i++) {

			if (i >= 1) {
				if (allYonlendirilenler.get(i).getFormSayisi() != allYonlendirilenler
						.get(i - 1).getFormSayisi()) {
					allYonlendirilenlerMain.add(allYonlendirilenler.get(i));
				}
			} else {
				allYonlendirilenlerMain.add(allYonlendirilenler.get(i));
			}
		}

		satinalmaTakipDosyalarIcerikForm = new SatinalmaTakipDosyalarIcerik();
		updateButtonRender = false;

		SatinalmaTakipDosyalarIcerikManager manager2 = new SatinalmaTakipDosyalarIcerikManager();
		banaYonlendirilenlerListesi = manager2
				.getAllSatinalmaMalzemeHizmetTalepFormuByGorevliId(userBean
						.getUser().getEmployee().getEmployeeId());

	}

	public void icerikListesiGetir(Integer malzemeFormId) {

		allSatinalmaTakipDosyalarIcerikByGorevliIdListMain.clear();
		SatinalmaTakipDosyalarIcerikManager manager = new SatinalmaTakipDosyalarIcerikManager();
		allSatinalmaTakipDosyalarIcerikList = manager
				.getAllSatinalmaTakipDosyalarIcerikByMalzemeFormId(malzemeFormId);
		allSatinalmaTakipDosyalarIcerikByGorevliIdList = manager
				.findByFormIdUserId(userBean.getUser().getEmployee()
						.getEmployeeId(), malzemeFormId);

		for (int i = 0; i < allSatinalmaTakipDosyalarIcerikByGorevliIdList
				.size(); i++) {

			if (allSatinalmaTakipDosyalarIcerikByGorevliIdList.get(i)
					.getSatinalmaTakipDosyalar() == null) {
				allSatinalmaTakipDosyalarIcerikByGorevliIdListMain
						.add(allSatinalmaTakipDosyalarIcerikByGorevliIdList
								.get(i));
			}

		}

		dosyaIcerikList = manager.findByDosyaIdFormId(
				satinalmaTakipDosyalarForm.getId(), malzemeFormId);

		satinalmaTakipDosyalarIcerikForm = new SatinalmaTakipDosyalarIcerik();
		updateButtonRender = false;
	}

	public void onRowSelect(SelectEvent event) {

		satinalmaTakipDosyalarForm = (SatinalmaTakipDosyalar) event.getObject();

	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	// Dönüştürülecek malzemeTalepId var mı?
	public boolean isMalzemeTransformedToSatinAlma(int malzemeTalepId) {

		for (int i = 0; i < allSatinalmaTakipDosyalarIcerikList.size(); i++) {
			if (allSatinalmaTakipDosyalarIcerikList.get(i)
					.getSatinalmaMalzemeHizmetTalepFormu() != null) {
				if (allSatinalmaTakipDosyalarIcerikList.get(i)
						.getSatinalmaMalzemeHizmetTalepFormu().getId() == malzemeTalepId) {

					FacesContext context = FacesContext.getCurrentInstance();
					context.addMessage(null, new FacesMessage(
							FacesMessage.SEVERITY_WARN,
							"ÖNCEDEN SATIN ALMAYA GÖNDERİLMİŞ İSTEK!", null));
					return true;
				}
			}
		}

		return false;
	}

	public void addFromMalzemeIcerik(Integer malzemeFormId) {

		SatinalmaMalzemeHizmetTalepFormuManager malzemeManager = new SatinalmaMalzemeHizmetTalepFormuManager();
		SatinalmaMalzemeHizmetTalepFormuIcerikManager malzemeIcerikManager = new SatinalmaMalzemeHizmetTalepFormuIcerikManager();
		SatinalmaTakipDosyalarIcerikManager takipIcerikManager = new SatinalmaTakipDosyalarIcerikManager();

		// 30 - Satın almaya gönderildi.
		SatinalmaDurumManager manager = new SatinalmaDurumManager();
		satinalmaDurum = manager.loadObject(SatinalmaDurum.class, 30);

		try {

			malzemeIcerikTrueList = malzemeIcerikManager
					.getAllSatinalmaMalzemeHizmetTalepFormuIcerikTrue(malzemeFormId);
			satinalmaMalzemeHizmetTalepFormu = malzemeManager.loadObject(
					SatinalmaMalzemeHizmetTalepFormu.class, malzemeFormId);

		} catch (Exception e) {
			System.out.println("satinalmaTakipDosyalarIcerikBean: "
					+ e.toString());
		}

		for (int i = 0; i < malzemeIcerikTrueList.size(); i++) {

			satinalmaTakipDosyalarIcerikForm
					.setSatinalmaMalzemeHizmetTalepFormu(satinalmaMalzemeHizmetTalepFormu);
			satinalmaTakipDosyalarIcerikForm
					.setSatinalmaMalzemeHizmetTalepFormuIcerik(malzemeIcerikTrueList
							.get(i));
			satinalmaTakipDosyalarIcerikForm.setEklemeZamani(UtilInsCore
					.getTarihZaman());
			satinalmaTakipDosyalarIcerikForm.setEkleyenKullanici(userBean
					.getUser().getId());

			// siraNoBul(satinalmaMalzemeHizmetTalepFormu.getId());

			takipIcerikManager.enterNew(satinalmaTakipDosyalarIcerikForm);

			satinalmaTakipDosyalarIcerikForm = new SatinalmaTakipDosyalarIcerik();
		}

		satinalmaMalzemeHizmetTalepFormu.setSatinalmaDurum(satinalmaDurum);
		malzemeManager.updateEntity(satinalmaMalzemeHizmetTalepFormu);

	}

	public List<Employee> completeEmployee(String query) {
		List<Employee> suggestions = new ArrayList<Employee>();

		for (Employee e : employees) {
			if (e.getLastname().startsWith(query))
				suggestions.add(e);
		}

		return suggestions;
	}

	public void onRowEdit(RowEditEvent event) {

		SatinalmaTakipDosyalarIcerikManager icerikManager = new SatinalmaTakipDosyalarIcerikManager();
		icerikManager.updateEntity(((SatinalmaTakipDosyalarIcerik) event
				.getObject()));

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				"İSTEK İLGİLİ PERSONELE İLETİLDİ!", null));
	}

	public void dosyaNumarasiVer() {
		SatinalmaTakipDosyalarManager dosyaManager = new SatinalmaTakipDosyalarManager();
		SatinalmaTakipDosyalarIcerikManager icerikManager = new SatinalmaTakipDosyalarIcerikManager();

		Integer dosyaNo = 0;

		if (selectedIcerik[0].getSatinalmaTakipDosyalar() != null) {
			dosyaNo = selectedIcerik[0].getSatinalmaTakipDosyalar().getId();
		}

		try {
			// dosya numarası kontrolu, yoksa olusturuluyor
			if (!isIcerikTransformedToDosya(dosyaNo)) {

				// yeni dosya olusturma
				satinalmaTakipDosyalarForm = new SatinalmaTakipDosyalar();
				satinalmaTakipDosyalarForm.setEklemeZamani(UtilInsCore
						.getTarihZaman());
				satinalmaTakipDosyalarForm.setEkleyenKullanici(userBean
						.getUser().getId());
				satinalmaTakipDosyalarForm
						.setSatinalmaMalzemeHizmetTalepFormu(selectedIcerik[0]
								.getSatinalmaMalzemeHizmetTalepFormu());
				sayiBul();
				dosyaManager.enterNew(satinalmaTakipDosyalarForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO, "DOSYA OLUŞTURULDU!", null));

				// iceriklere dosya no yazılıyor
				for (int i = 0; i < selectedIcerik.length; i++) {

					selectedIcerik[i]
							.setSatinalmaTakipDosyalar(satinalmaTakipDosyalarForm);
					icerikManager.updateEntity(selectedIcerik[i]);

				}

				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO,
						"İÇERİKLERE DOSYA NUMARASI ATANDI!", null));

			} else {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_WARN,
						"ÖNCEDEN DOSYA NUMARASI VERİLMİŞ!", null));
			}
		} catch (Exception e) {
			System.out.println("SalesProposalBean: " + e.toString());
		}
		fillTestList();
	}

	public void sayiBul() {

		SatinalmaTakipDosyalarManager manager = new SatinalmaTakipDosyalarManager();
		satinalmaTakipDosyalarForm.setDosyaTakipNo(manager.getOtomatikSayi(
				userBean.getUser().getEmployee().getEmployeeId()).toString());
	}

	// Dönüştürülecek DosyaId var mı?
	public boolean isIcerikTransformedToDosya(Integer theDosyaId) {

		for (int i = 0; i < selectedIcerik.length; i++) {
			if (selectedIcerik[i].getSatinalmaTakipDosyalar() != null) {
				if (selectedIcerik[i].getSatinalmaTakipDosyalar().getId() == theDosyaId) {
					return true;
				}
			}
		}

		return false;
	}

	// setters getters

	/**
	 * @return the allSatinalmaTakipDosyalarIcerikList
	 */
	public List<SatinalmaTakipDosyalarIcerik> getAllSatinalmaTakipDosyalarIcerikList() {
		return allSatinalmaTakipDosyalarIcerikList;
	}

	/**
	 * @param allSatinalmaTakipDosyalarIcerikList
	 *            the allSatinalmaTakipDosyalarIcerikList to set
	 */
	public void setAllSatinalmaTakipDosyalarIcerikList(
			List<SatinalmaTakipDosyalarIcerik> allSatinalmaTakipDosyalarIcerikList) {
		this.allSatinalmaTakipDosyalarIcerikList = allSatinalmaTakipDosyalarIcerikList;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the satinalmaTakipDosyalarIcerikForm
	 */
	public SatinalmaTakipDosyalarIcerik getSatinalmaTakipDosyalarIcerikForm() {
		return satinalmaTakipDosyalarIcerikForm;
	}

	/**
	 * @param satinalmaTakipDosyalarIcerikForm
	 *            the satinalmaTakipDosyalarIcerikForm to set
	 */
	public void setSatinalmaTakipDosyalarIcerikForm(
			SatinalmaTakipDosyalarIcerik satinalmaTakipDosyalarIcerikForm) {
		this.satinalmaTakipDosyalarIcerikForm = satinalmaTakipDosyalarIcerikForm;
	}

	/**
	 * @return the satinalmaMalzemeHizmetTalepFormu
	 */
	public SatinalmaMalzemeHizmetTalepFormu getSatinalmaMalzemeHizmetTalepFormu() {
		return satinalmaMalzemeHizmetTalepFormu;
	}

	/**
	 * @param satinalmaMalzemeHizmetTalepFormu
	 *            the satinalmaMalzemeHizmetTalepFormu to set
	 */
	public void setSatinalmaMalzemeHizmetTalepFormu(
			SatinalmaMalzemeHizmetTalepFormu satinalmaMalzemeHizmetTalepFormu) {
		this.satinalmaMalzemeHizmetTalepFormu = satinalmaMalzemeHizmetTalepFormu;
	}

	/**
	 * @return the employees
	 */
	public static List<Employee> getEmployees() {
		return employees;
	}

	/**
	 * @param employees
	 *            the employees to set
	 */
	public static void setEmployees(List<Employee> employees) {
		SatinalmaTakipDosyalarIcerikBean.employees = employees;
	}

	/**
	 * @return the allSatinalmaTakipDosyalarIcerikByGorevliIdList
	 */
	public List<SatinalmaTakipDosyalarIcerik> getAllSatinalmaTakipDosyalarIcerikByGorevliIdList() {
		return allSatinalmaTakipDosyalarIcerikByGorevliIdList;
	}

	/**
	 * @param allSatinalmaTakipDosyalarIcerikByGorevliIdList
	 *            the allSatinalmaTakipDosyalarIcerikByGorevliIdList to set
	 */
	public void setAllSatinalmaTakipDosyalarIcerikByGorevliIdList(
			List<SatinalmaTakipDosyalarIcerik> allSatinalmaTakipDosyalarIcerikByGorevliIdList) {
		this.allSatinalmaTakipDosyalarIcerikByGorevliIdList = allSatinalmaTakipDosyalarIcerikByGorevliIdList;
	}

	/**
	 * @return the allYonlendirilenler
	 */
	public List<SatinalmaMalzemeHizmetTalepFormu> getAllYonlendirilenler() {
		return allYonlendirilenler;
	}

	/**
	 * @param allYonlendirilenler
	 *            the allYonlendirilenler to set
	 */
	public void setAllYonlendirilenler(
			List<SatinalmaMalzemeHizmetTalepFormu> allYonlendirilenler) {
		this.allYonlendirilenler = allYonlendirilenler;
	}

	/**
	 * @return the selectedIcerik
	 */
	public SatinalmaTakipDosyalarIcerik[] getSelectedIcerik() {
		return selectedIcerik;
	}

	/**
	 * @param selectedIcerik
	 *            the selectedIcerik to set
	 */
	public void setSelectedIcerik(SatinalmaTakipDosyalarIcerik[] selectedIcerik) {
		this.selectedIcerik = selectedIcerik;
	}

	/**
	 * @return the allSatinalmaTakipDosyalarIcerikByGorevliIdListMain
	 */
	public List<SatinalmaTakipDosyalarIcerik> getAllSatinalmaTakipDosyalarIcerikByGorevliIdListMain() {
		return allSatinalmaTakipDosyalarIcerikByGorevliIdListMain;
	}

	/**
	 * @param allSatinalmaTakipDosyalarIcerikByGorevliIdListMain
	 *            the allSatinalmaTakipDosyalarIcerikByGorevliIdListMain to set
	 */
	public void setAllSatinalmaTakipDosyalarIcerikByGorevliIdListMain(
			List<SatinalmaTakipDosyalarIcerik> allSatinalmaTakipDosyalarIcerikByGorevliIdListMain) {
		this.allSatinalmaTakipDosyalarIcerikByGorevliIdListMain = allSatinalmaTakipDosyalarIcerikByGorevliIdListMain;
	}

	/**
	 * @return the allYonlendirilenlerMain
	 */
	public List<SatinalmaMalzemeHizmetTalepFormu> getAllYonlendirilenlerMain() {
		return allYonlendirilenlerMain;
	}

	/**
	 * @param allYonlendirilenlerMain
	 *            the allYonlendirilenlerMain to set
	 */
	public void setAllYonlendirilenlerMain(
			List<SatinalmaMalzemeHizmetTalepFormu> allYonlendirilenlerMain) {
		this.allYonlendirilenlerMain = allYonlendirilenlerMain;
	}

	/**
	 * @return the dosyaIcerikList
	 */
	public List<SatinalmaTakipDosyalarIcerik> getDosyaIcerikList() {
		return dosyaIcerikList;
	}

	/**
	 * @param dosyaIcerikList
	 *            the dosyaIcerikList to set
	 */
	public void setDosyaIcerikList(
			List<SatinalmaTakipDosyalarIcerik> dosyaIcerikList) {
		this.dosyaIcerikList = dosyaIcerikList;
	}

	/**
	 * @return the satinalmaTakipDosyalarForm
	 */
	public SatinalmaTakipDosyalar getSatinalmaTakipDosyalarForm() {
		return satinalmaTakipDosyalarForm;
	}

	/**
	 * @param satinalmaTakipDosyalarForm
	 *            the satinalmaTakipDosyalarForm to set
	 */
	public void setSatinalmaTakipDosyalarForm(
			SatinalmaTakipDosyalar satinalmaTakipDosyalarForm) {
		this.satinalmaTakipDosyalarForm = satinalmaTakipDosyalarForm;
	}

	/**
	 * @return the banaYonlendirilenlerListesi
	 */
	public List<SatinalmaTakipDosyalarIcerik> getBanaYonlendirilenlerListesi() {
		return banaYonlendirilenlerListesi;
	}

	/**
	 * @param banaYonlendirilenlerListesi
	 *            the banaYonlendirilenlerListesi to set
	 */
	public void setBanaYonlendirilenlerListesi(
			List<SatinalmaTakipDosyalarIcerik> banaYonlendirilenlerListesi) {
		this.banaYonlendirilenlerListesi = banaYonlendirilenlerListesi;
	}

	/**
	 * @return the activeIndex
	 */
	public String getActiveIndex() {
		return activeIndex;
	}

	/**
	 * @param activeIndex
	 *            the activeIndex to set
	 */
	public void setActiveIndex(String activeIndex) {
		this.activeIndex = activeIndex;
	}

}
