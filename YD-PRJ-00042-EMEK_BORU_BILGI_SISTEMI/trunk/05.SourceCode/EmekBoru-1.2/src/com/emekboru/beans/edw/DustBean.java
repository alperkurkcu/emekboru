package com.emekboru.beans.edw;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;

import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.EdwPipeLink;
import com.emekboru.jpa.ElectrodeDustWire;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.sales.customer.SalesCustomer;
import com.emekboru.jpaman.ElectrodeDustWireManager;
import com.emekboru.jpaman.Factory;
import com.emekboru.messages.ElectrodeDustWireType;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "dustBean")
@ViewScoped
public class DustBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
	private ConfigBean config;

	private ElectrodeDustWire selectedDust;

	// Attributes necessary to add a new dust
	private ElectrodeDustWire newDust;
	// private Customer selectedCustomer;
	private SalesCustomer selectedSalesCustomer;

	// to view the list of pipes produced from the Selected Dust
	private List<Pipe> pipeList;

	public DustBean() {

		selectedDust = new ElectrodeDustWire();
		newDust = new ElectrodeDustWire();
		// selectedCustomer = new Customer();
		selectedSalesCustomer = new SalesCustomer();
		pipeList = new ArrayList<Pipe>();
	}

	/******************************************************************************
	 ************** DUST MAIN METHODS SECTION ******************* /
	 ******************************************************************************/
	public void deleteDust() {

		if (!isDustOk())
			return;

		EntityManager man = Factory.getInstance().createEntityManager();
		man.getTransaction().begin();
		selectedDust = man.find(ElectrodeDustWire.class,
				selectedDust.getElectrodeDustWireId());
		man.remove(selectedDust);
		man.getTransaction().commit();
		// remove it from the DustLists as well
		DustListBean bean = (DustListBean) FacesContextUtils.getViewBean(
				"dustListBean", DustListBean.class);
		bean.getDusts().remove(selectedDust);
		bean.getRecentDusts().remove(selectedDust);

		selectedDust = new ElectrodeDustWire();
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	public void addDust() {

		EntityManager man = Factory.getInstance().createEntityManager();
		man.getTransaction().begin();

		// newDust.setCustomer(selectedCustomer);
		newDust.setSalesCustomer(selectedSalesCustomer);
		newDust.setRemainingAmount(newDust.getMiktari());
		newDust.setType(ElectrodeDustWireType.DUST);
		newDust.setStatus(config.getConfig().getMaterials().getDefaultStatus());
		// newDust.setOrderId(0);
		newDust.setSalesItemId(0);
		man.persist(newDust);
		man.getTransaction().commit();
		man.close();
		// add the bean to the BeanList
		DustListBean bean = (DustListBean) FacesContextUtils.getViewBean(
				"dustListBean", DustListBean.class);
		bean.getRecentDusts().add(newDust);
		newDust = new ElectrodeDustWire();
		// selectedCustomer = new Customer();
		selectedSalesCustomer = new SalesCustomer();
		FacesContextUtils.addInfoMessage("SubmitMessage");
	}

	public void loadHistory() {

		EntityManager man = Factory.getInstance().createEntityManager();
		man.getTransaction().begin();
		selectedDust = man.find(ElectrodeDustWire.class,
				selectedDust.getElectrodeDustWireId());
		for (EdwPipeLink l : selectedDust.getEdwPipeLinks()) {
			pipeList.add(l.getPipe());
		}
		man.getTransaction().commit();
		man.close();
	}

	public void updateDust() {

		if (!isDustOk())
			return;

		ElectrodeDustWireManager man = new ElectrodeDustWireManager();
		man.updateEntity(selectedDust);
		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}

	/******************************************************************************
	 *************** HELPER PROTECTED METHODS ******************* /
	 ******************************************************************************/
	protected boolean isDustOk() {

		if (selectedDust.getStatus(config.getConfig()).isDepleted()
				|| selectedDust.getStatus(config.getConfig()).isSold()) {

			System.out.println("Trying to update sold Dust!!");
			FacesContextUtils.addWarnMessage("NotAvailableMessage");
			return false;
		}
		return true;
	}

	/******************************************************************************
	 ******************* GETTERS AND SETTERS ********************* /
	 ******************************************************************************/
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ElectrodeDustWire getNewDust() {
		return newDust;
	}

	public void setNewDust(ElectrodeDustWire newDust) {
		this.newDust = newDust;
	}

	public List<Pipe> getPipeList() {
		return pipeList;
	}

	public void setPipeList(List<Pipe> pipeList) {
		this.pipeList = pipeList;
	}

	public ElectrodeDustWire getSelectedDust() {
		return selectedDust;
	}

	public void setSelectedDust(ElectrodeDustWire selectedDust) {
		this.selectedDust = selectedDust;
	}

	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	public SalesCustomer getSelectedSalesCustomer() {
		return selectedSalesCustomer;
	}

	public void setSelectedSalesCustomer(SalesCustomer selectedSalesCustomer) {
		this.selectedSalesCustomer = selectedSalesCustomer;
	}

}
