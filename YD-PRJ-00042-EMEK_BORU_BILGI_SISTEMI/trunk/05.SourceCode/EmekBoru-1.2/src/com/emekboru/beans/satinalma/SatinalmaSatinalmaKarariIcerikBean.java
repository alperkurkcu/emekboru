/**
 * 
 */
package com.emekboru.beans.satinalma;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.satinalma.SatinalmaSatinalmaKarariIcerik;
import com.emekboru.jpa.satinalma.SatinalmaTeklifIsteme;
import com.emekboru.jpaman.satinalma.SatinalmaMalzemeHizmetTalepFormuIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaSatinalmaKarariIcerikManager;
import com.emekboru.jpaman.satinalma.SatinalmaTeklifIstemeManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "satinalmaSatinalmaKarariIcerikBean")
@ViewScoped
public class SatinalmaSatinalmaKarariIcerikBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private SatinalmaSatinalmaKarariIcerik satinalmaSatinalmaKarariIcerikForm = new SatinalmaSatinalmaKarariIcerik();
	// satınalma kararı için
	private List<SatinalmaSatinalmaKarariIcerik> satinalmaSatinalmaKarariIcerikList = new ArrayList<SatinalmaSatinalmaKarariIcerik>();
	private List<SatinalmaSatinalmaKarariIcerik> firma1List = new ArrayList<SatinalmaSatinalmaKarariIcerik>();
	private List<SatinalmaSatinalmaKarariIcerik> firma2List = new ArrayList<SatinalmaSatinalmaKarariIcerik>();
	private List<SatinalmaSatinalmaKarariIcerik> firma3List = new ArrayList<SatinalmaSatinalmaKarariIcerik>();
	private List<SatinalmaSatinalmaKarariIcerik> firma4List = new ArrayList<SatinalmaSatinalmaKarariIcerik>();
	private List<SatinalmaSatinalmaKarariIcerik> firma5List = new ArrayList<SatinalmaSatinalmaKarariIcerik>();
	private List<SatinalmaSatinalmaKarariIcerik> firma6List = new ArrayList<SatinalmaSatinalmaKarariIcerik>();
	private List<SatinalmaSatinalmaKarariIcerik> firma7List = new ArrayList<SatinalmaSatinalmaKarariIcerik>();
	private int firmaCount = 0;

	private List<SatinalmaTeklifIsteme> teklifList = new ArrayList<SatinalmaTeklifIsteme>();
	// private List<SatinalmaTakipDosyalarIcerik> dosyaIcerikList = new
	// ArrayList<SatinalmaTakipDosyalarIcerik>();

	private boolean firma1Control = false;
	private boolean firma2Control = false;
	private boolean firma3Control = false;
	private boolean firma4Control = false;
	private boolean firma5Control = false;
	private boolean firma6Control = false;
	private boolean firma7Control = false;

	private double firma1IlkToplam = 0.0;
	private double firma1NihaiToplam = 0.0;
	private double firma2IlkToplam = 0.0;
	private double firma2NihaiToplam = 0.0;
	private double firma3IlkToplam = 0.0;
	private double firma3NihaiToplam = 0.0;
	private double firma4IlkToplam = 0.0;
	private double firma4NihaiToplam = 0.0;
	private double firma5IlkToplam = 0.0;
	private double firma5NihaiToplam = 0.0;
	private double firma6IlkToplam = 0.0;
	private double firma6NihaiToplam = 0.0;
	private double firma7IlkToplam = 0.0;
	private double firma7NihaiToplam = 0.0;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	private SatinalmaSatinalmaKarariIcerik rowEditKararIcerik = new SatinalmaSatinalmaKarariIcerik();

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void addIcerik() {
		satinalmaSatinalmaKarariIcerikForm = new SatinalmaSatinalmaKarariIcerik();
	}

	public void satinalmaKarariListDoldur(Integer prmKararId) {

		SatinalmaSatinalmaKarariIcerikManager manager = new SatinalmaSatinalmaKarariIcerikManager();
		satinalmaSatinalmaKarariIcerikList = manager
				.satinalmaKarariListDoldur(prmKararId);

		SatinalmaTeklifIstemeManager teklifManager = new SatinalmaTeklifIstemeManager();
		teklifList = teklifManager
				.findByDosyaId(satinalmaSatinalmaKarariIcerikList.get(0)
						.getSatinalmaSatinalmaKarari().getDosyaNo());
		firmaCount = teklifList.size();
		// SatinalmaTakipDosyalarIcerikManager dosyaManager = new
		// SatinalmaTakipDosyalarIcerikManager();
		// dosyaIcerikList = dosyaManager.findByDosyaIdFormId(
		// satinalmaSatinalmaKarariIcerikList.get(0)
		// .getSatinalmaTeklifIsteme().getSatinalmaTakipDosyalar()
		// .getId(), satinalmaSatinalmaKarariIcerikList.get(0)
		// .getSatinalmaTeklifIsteme().getSatinalmaTakipDosyalar()
		// .getSatinalmaMalzemeHizmetTalepFormu().getId());

		firmaControlCheck(prmKararId);

	}

	private void firmaControlCheck(Integer prmKararId) {

		if (firmaCount == 1) {
			firma1Control = true;
			firma2Control = false;
			firma3Control = false;
			firma4Control = false;
			firma5Control = false;
			firma6Control = false;
			firma7Control = false;
			SatinalmaSatinalmaKarariIcerikManager manager = new SatinalmaSatinalmaKarariIcerikManager();
			firma1List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(0).getSatinalmaMusteri().getId());
			firma1IlkToplam = getIlkTeklifTotal(firma1List);
			firma1NihaiToplam = getNihaiTeklifTotal(firma1List);
		} else if (firmaCount == 2) {
			firma1Control = true;
			firma2Control = true;
			firma3Control = false;
			firma4Control = false;
			firma5Control = false;
			firma6Control = false;
			firma7Control = false;

			SatinalmaSatinalmaKarariIcerikManager manager = new SatinalmaSatinalmaKarariIcerikManager();
			firma1List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(0).getSatinalmaMusteri().getId());
			firma2List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(1).getSatinalmaMusteri().getId());
			firma1IlkToplam = getIlkTeklifTotal(firma1List);
			firma1NihaiToplam = getNihaiTeklifTotal(firma1List);
			firma2IlkToplam = getIlkTeklifTotal(firma2List);
			firma2NihaiToplam = getNihaiTeklifTotal(firma2List);

		} else if (firmaCount == 3) {
			firma1Control = true;
			firma2Control = true;
			firma3Control = true;
			firma4Control = false;
			firma5Control = false;
			firma6Control = false;
			firma7Control = false;
			SatinalmaSatinalmaKarariIcerikManager manager = new SatinalmaSatinalmaKarariIcerikManager();
			firma1List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(0).getSatinalmaMusteri().getId());
			firma2List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(1).getSatinalmaMusteri().getId());
			firma3List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(2).getSatinalmaMusteri().getId());
			firma1IlkToplam = getIlkTeklifTotal(firma1List);
			firma1NihaiToplam = getNihaiTeklifTotal(firma1List);
			firma2IlkToplam = getIlkTeklifTotal(firma2List);
			firma2NihaiToplam = getNihaiTeklifTotal(firma2List);
			firma3IlkToplam = getIlkTeklifTotal(firma3List);
			firma3NihaiToplam = getNihaiTeklifTotal(firma3List);

		} else if (firmaCount == 4) {
			firma1Control = true;
			firma2Control = true;
			firma3Control = true;
			firma4Control = true;
			firma5Control = false;
			firma6Control = false;
			firma7Control = false;
			SatinalmaSatinalmaKarariIcerikManager manager = new SatinalmaSatinalmaKarariIcerikManager();
			firma1List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(0).getSatinalmaMusteri().getId());
			firma2List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(1).getSatinalmaMusteri().getId());
			firma3List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(2).getSatinalmaMusteri().getId());
			firma4List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(3).getSatinalmaMusteri().getId());
			firma1IlkToplam = getIlkTeklifTotal(firma1List);
			firma1NihaiToplam = getNihaiTeklifTotal(firma1List);
			firma2IlkToplam = getIlkTeklifTotal(firma2List);
			firma2NihaiToplam = getNihaiTeklifTotal(firma2List);
			firma3IlkToplam = getIlkTeklifTotal(firma3List);
			firma3NihaiToplam = getNihaiTeklifTotal(firma3List);
			firma4IlkToplam = getIlkTeklifTotal(firma4List);
			firma4NihaiToplam = getNihaiTeklifTotal(firma4List);

		} else if (firmaCount == 5) {
			firma1Control = true;
			firma2Control = true;
			firma3Control = true;
			firma4Control = true;
			firma5Control = true;
			firma6Control = false;
			firma7Control = false;
			SatinalmaSatinalmaKarariIcerikManager manager = new SatinalmaSatinalmaKarariIcerikManager();
			firma1List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(0).getSatinalmaMusteri().getId());
			firma2List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(1).getSatinalmaMusteri().getId());
			firma3List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(2).getSatinalmaMusteri().getId());
			firma4List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(3).getSatinalmaMusteri().getId());
			firma5List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(4).getSatinalmaMusteri().getId());
			firma1IlkToplam = getIlkTeklifTotal(firma1List);
			firma1NihaiToplam = getNihaiTeklifTotal(firma1List);
			firma2IlkToplam = getIlkTeklifTotal(firma2List);
			firma2NihaiToplam = getNihaiTeklifTotal(firma2List);
			firma3IlkToplam = getIlkTeklifTotal(firma3List);
			firma3NihaiToplam = getNihaiTeklifTotal(firma3List);
			firma4IlkToplam = getIlkTeklifTotal(firma4List);
			firma4NihaiToplam = getNihaiTeklifTotal(firma4List);
			firma5IlkToplam = getIlkTeklifTotal(firma5List);
			firma5NihaiToplam = getNihaiTeklifTotal(firma5List);

		} else if (firmaCount == 6) {
			firma1Control = true;
			firma2Control = true;
			firma3Control = true;
			firma4Control = true;
			firma5Control = true;
			firma6Control = true;
			firma7Control = false;
			SatinalmaSatinalmaKarariIcerikManager manager = new SatinalmaSatinalmaKarariIcerikManager();
			firma1List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(0).getSatinalmaMusteri().getId());
			firma2List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(1).getSatinalmaMusteri().getId());
			firma3List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(2).getSatinalmaMusteri().getId());
			firma4List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(3).getSatinalmaMusteri().getId());
			firma5List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(4).getSatinalmaMusteri().getId());
			firma6List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(5).getSatinalmaMusteri().getId());
			firma1IlkToplam = getIlkTeklifTotal(firma1List);
			firma1NihaiToplam = getNihaiTeklifTotal(firma1List);
			firma2IlkToplam = getIlkTeklifTotal(firma2List);
			firma2NihaiToplam = getNihaiTeklifTotal(firma2List);
			firma3IlkToplam = getIlkTeklifTotal(firma3List);
			firma3NihaiToplam = getNihaiTeklifTotal(firma3List);
			firma4IlkToplam = getIlkTeklifTotal(firma4List);
			firma4NihaiToplam = getNihaiTeklifTotal(firma4List);
			firma5IlkToplam = getIlkTeklifTotal(firma5List);
			firma5NihaiToplam = getNihaiTeklifTotal(firma5List);
			firma6IlkToplam = getIlkTeklifTotal(firma6List);
			firma6NihaiToplam = getNihaiTeklifTotal(firma6List);

		} else if (firmaCount == 7) {
			firma1Control = true;
			firma2Control = true;
			firma3Control = true;
			firma4Control = true;
			firma5Control = true;
			firma6Control = true;
			firma7Control = true;
			SatinalmaSatinalmaKarariIcerikManager manager = new SatinalmaSatinalmaKarariIcerikManager();
			firma1List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(0).getSatinalmaMusteri().getId());
			firma2List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(1).getSatinalmaMusteri().getId());
			firma3List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(2).getSatinalmaMusteri().getId());
			firma4List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(3).getSatinalmaMusteri().getId());
			firma5List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(4).getSatinalmaMusteri().getId());
			firma6List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(5).getSatinalmaMusteri().getId());
			firma7List = manager.satinalmaKarariListDoldurByFirma(prmKararId,
					teklifList.get(6).getSatinalmaMusteri().getId());
			firma1IlkToplam = getIlkTeklifTotal(firma1List);
			firma1NihaiToplam = getNihaiTeklifTotal(firma1List);
			firma2IlkToplam = getIlkTeklifTotal(firma2List);
			firma2NihaiToplam = getNihaiTeklifTotal(firma2List);
			firma3IlkToplam = getIlkTeklifTotal(firma3List);
			firma3NihaiToplam = getNihaiTeklifTotal(firma3List);
			firma4IlkToplam = getIlkTeklifTotal(firma4List);
			firma4NihaiToplam = getNihaiTeklifTotal(firma4List);
			firma5IlkToplam = getIlkTeklifTotal(firma5List);
			firma5NihaiToplam = getNihaiTeklifTotal(firma5List);
			firma6IlkToplam = getIlkTeklifTotal(firma6List);
			firma6NihaiToplam = getNihaiTeklifTotal(firma6List);
			firma7IlkToplam = getIlkTeklifTotal(firma7List);
			firma7NihaiToplam = getNihaiTeklifTotal(firma7List);
		}

	}

	public double getIlkTeklifTotal(List<SatinalmaSatinalmaKarariIcerik> prmList) {
		double quantityIlk = 0.0;
		for (SatinalmaSatinalmaKarariIcerik p : prmList) {
			if (String.valueOf(p.getIlkTeklifBirimFiyat()) != null)
				quantityIlk += p.getIlkTeklifBirimFiyat()
						* p.getSatinalmaTeklifIstemeIcerik()
								.getSatinalmaTakipDosyalarIcerik()
								.getSatinalmaMalzemeHizmetTalepFormuIcerik()
								.getMiktar();
		}
		return quantityIlk;
	}

	public double getNihaiTeklifTotal(
			List<SatinalmaSatinalmaKarariIcerik> prmList) {
		double quantityNihai = 0.0;
		for (SatinalmaSatinalmaKarariIcerik p : prmList) {
			if (String.valueOf(p.getNihaiTeklifBirimFiyat()) != null)
				quantityNihai += p.getNihaiTeklifBirimFiyat()
						* p.getSatinalmaTeklifIstemeIcerik()
								.getSatinalmaTakipDosyalarIcerik()
								.getSatinalmaMalzemeHizmetTalepFormuIcerik()
								.getMiktar();
		}
		return quantityNihai;
	}

	public void addOrUpdateIcerik() {

		SatinalmaSatinalmaKarariIcerikManager formManager = new SatinalmaSatinalmaKarariIcerikManager();

		satinalmaSatinalmaKarariIcerikForm.setGuncellemeZamani(UtilInsCore
				.getTarihZaman());
		satinalmaSatinalmaKarariIcerikForm.setGuncelleyenKullanici(userBean
				.getUser().getId());

		formManager.updateEntity(satinalmaSatinalmaKarariIcerikForm);
		satinalmaKarariListDoldur(satinalmaSatinalmaKarariIcerikForm
				.getSatinalmaSatinalmaKarari().getId());
		satinalmaSatinalmaKarariIcerikForm = new SatinalmaSatinalmaKarariIcerik();

		FacesContextUtils.addInfoMessage("FormUpdateMessage");

	}

	public void onRowSelect(SelectEvent event) {

		satinalmaSatinalmaKarariIcerikForm = (SatinalmaSatinalmaKarariIcerik) event
				.getObject();

	}

	public void onRowEdit(RowEditEvent event) {

		SatinalmaSatinalmaKarariIcerikManager icerikManager = new SatinalmaSatinalmaKarariIcerikManager();
		rowEditKararIcerik = (SatinalmaSatinalmaKarariIcerik) event.getObject();
		icerikManager.updateEntity(rowEditKararIcerik);

		SatinalmaMalzemeHizmetTalepFormuIcerikManager malzemeManager = new SatinalmaMalzemeHizmetTalepFormuIcerikManager();
		rowEditKararIcerik
				.getSatinalmaTeklifIstemeIcerik()
				.getSatinalmaTakipDosyalarIcerik()
				.getSatinalmaMalzemeHizmetTalepFormuIcerik()
				.setSatinalmaTedarikci(
						rowEditKararIcerik.getSatinalmaTeklifIsteme()
								.getSatinalmaMusteri());
		rowEditKararIcerik
				.getSatinalmaTeklifIstemeIcerik()
				.getSatinalmaTakipDosyalarIcerik()
				.getSatinalmaMalzemeHizmetTalepFormuIcerik()
				.setTeslimTarihi(
						rowEditKararIcerik.getSatinalmaTeklifIstemeIcerik()
								.getTeslimTarihi());
		malzemeManager.updateEntity(rowEditKararIcerik
				.getSatinalmaTeklifIstemeIcerik()
				.getSatinalmaTakipDosyalarIcerik()
				.getSatinalmaMalzemeHizmetTalepFormuIcerik());

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
				"FİRMA SEÇİLMİŞTİR!", null));
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the satinalmaSatinalmaKarariIcerikList
	 */
	public List<SatinalmaSatinalmaKarariIcerik> getSatinalmaSatinalmaKarariIcerikList() {
		return satinalmaSatinalmaKarariIcerikList;
	}

	/**
	 * @param satinalmaSatinalmaKarariIcerikList
	 *            the satinalmaSatinalmaKarariIcerikList to set
	 */
	public void setSatinalmaSatinalmaKarariIcerikList(
			List<SatinalmaSatinalmaKarariIcerik> satinalmaSatinalmaKarariIcerikList) {
		this.satinalmaSatinalmaKarariIcerikList = satinalmaSatinalmaKarariIcerikList;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the satinalmaSatinalmaKarariIcerikForm
	 */
	public SatinalmaSatinalmaKarariIcerik getSatinalmaSatinalmaKarariIcerikForm() {
		return satinalmaSatinalmaKarariIcerikForm;
	}

	/**
	 * @param satinalmaSatinalmaKarariIcerikForm
	 *            the satinalmaSatinalmaKarariIcerikForm to set
	 */
	public void setSatinalmaSatinalmaKarariIcerikForm(
			SatinalmaSatinalmaKarariIcerik satinalmaSatinalmaKarariIcerikForm) {
		this.satinalmaSatinalmaKarariIcerikForm = satinalmaSatinalmaKarariIcerikForm;
	}

	/**
	 * @return the firma1List
	 */
	public List<SatinalmaSatinalmaKarariIcerik> getFirma1List() {
		return firma1List;
	}

	/**
	 * @param firma1List
	 *            the firma1List to set
	 */
	public void setFirma1List(List<SatinalmaSatinalmaKarariIcerik> firma1List) {
		this.firma1List = firma1List;
	}

	/**
	 * @return the firma2List
	 */
	public List<SatinalmaSatinalmaKarariIcerik> getFirma2List() {
		return firma2List;
	}

	/**
	 * @param firma2List
	 *            the firma2List to set
	 */
	public void setFirma2List(List<SatinalmaSatinalmaKarariIcerik> firma2List) {
		this.firma2List = firma2List;
	}

	/**
	 * @return the firma3List
	 */
	public List<SatinalmaSatinalmaKarariIcerik> getFirma3List() {
		return firma3List;
	}

	/**
	 * @param firma3List
	 *            the firma3List to set
	 */
	public void setFirma3List(List<SatinalmaSatinalmaKarariIcerik> firma3List) {
		this.firma3List = firma3List;
	}

	/**
	 * @return the firma4List
	 */
	public List<SatinalmaSatinalmaKarariIcerik> getFirma4List() {
		return firma4List;
	}

	/**
	 * @param firma4List
	 *            the firma4List to set
	 */
	public void setFirma4List(List<SatinalmaSatinalmaKarariIcerik> firma4List) {
		this.firma4List = firma4List;
	}

	/**
	 * @return the firma5List
	 */
	public List<SatinalmaSatinalmaKarariIcerik> getFirma5List() {
		return firma5List;
	}

	/**
	 * @param firma5List
	 *            the firma5List to set
	 */
	public void setFirma5List(List<SatinalmaSatinalmaKarariIcerik> firma5List) {
		this.firma5List = firma5List;
	}

	/**
	 * @return the firma6List
	 */
	public List<SatinalmaSatinalmaKarariIcerik> getFirma6List() {
		return firma6List;
	}

	/**
	 * @param firma6List
	 *            the firma6List to set
	 */
	public void setFirma6List(List<SatinalmaSatinalmaKarariIcerik> firma6List) {
		this.firma6List = firma6List;
	}

	/**
	 * @return the firma7List
	 */
	public List<SatinalmaSatinalmaKarariIcerik> getFirma7List() {
		return firma7List;
	}

	/**
	 * @param firma7List
	 *            the firma7List to set
	 */
	public void setFirma7List(List<SatinalmaSatinalmaKarariIcerik> firma7List) {
		this.firma7List = firma7List;
	}

	/**
	 * @return the firma1Control
	 */
	public boolean isFirma1Control() {
		return firma1Control;
	}

	/**
	 * @param firma1Control
	 *            the firma1Control to set
	 */
	public void setFirma1Control(boolean firma1Control) {
		this.firma1Control = firma1Control;
	}

	/**
	 * @return the firma2Control
	 */
	public boolean isFirma2Control() {
		return firma2Control;
	}

	/**
	 * @param firma2Control
	 *            the firma2Control to set
	 */
	public void setFirma2Control(boolean firma2Control) {
		this.firma2Control = firma2Control;
	}

	/**
	 * @return the firma3Control
	 */
	public boolean isFirma3Control() {
		return firma3Control;
	}

	/**
	 * @param firma3Control
	 *            the firma3Control to set
	 */
	public void setFirma3Control(boolean firma3Control) {
		this.firma3Control = firma3Control;
	}

	/**
	 * @return the firma4Control
	 */
	public boolean isFirma4Control() {
		return firma4Control;
	}

	/**
	 * @param firma4Control
	 *            the firma4Control to set
	 */
	public void setFirma4Control(boolean firma4Control) {
		this.firma4Control = firma4Control;
	}

	/**
	 * @return the firma5Control
	 */
	public boolean isFirma5Control() {
		return firma5Control;
	}

	/**
	 * @param firma5Control
	 *            the firma5Control to set
	 */
	public void setFirma5Control(boolean firma5Control) {
		this.firma5Control = firma5Control;
	}

	/**
	 * @return the firma6Control
	 */
	public boolean isFirma6Control() {
		return firma6Control;
	}

	/**
	 * @param firma6Control
	 *            the firma6Control to set
	 */
	public void setFirma6Control(boolean firma6Control) {
		this.firma6Control = firma6Control;
	}

	/**
	 * @return the firma7Control
	 */
	public boolean isFirma7Control() {
		return firma7Control;
	}

	/**
	 * @param firma7Control
	 *            the firma7Control to set
	 */
	public void setFirma7Control(boolean firma7Control) {
		this.firma7Control = firma7Control;
	}

	/**
	 * @return the firma1IlkToplam
	 */
	public double getFirma1IlkToplam() {
		return firma1IlkToplam;
	}

	/**
	 * @param firma1IlkToplam
	 *            the firma1IlkToplam to set
	 */
	public void setFirma1IlkToplam(double firma1IlkToplam) {
		this.firma1IlkToplam = firma1IlkToplam;
	}

	/**
	 * @return the firma1NihaiToplam
	 */
	public double getFirma1NihaiToplam() {
		return firma1NihaiToplam;
	}

	/**
	 * @param firma1NihaiToplam
	 *            the firma1NihaiToplam to set
	 */
	public void setFirma1NihaiToplam(double firma1NihaiToplam) {
		this.firma1NihaiToplam = firma1NihaiToplam;
	}

	/**
	 * @return the firma2IlkToplam
	 */
	public double getFirma2IlkToplam() {
		return firma2IlkToplam;
	}

	/**
	 * @param firma2IlkToplam
	 *            the firma2IlkToplam to set
	 */
	public void setFirma2IlkToplam(double firma2IlkToplam) {
		this.firma2IlkToplam = firma2IlkToplam;
	}

	/**
	 * @return the firma2NihaiToplam
	 */
	public double getFirma2NihaiToplam() {
		return firma2NihaiToplam;
	}

	/**
	 * @param firma2NihaiToplam
	 *            the firma2NihaiToplam to set
	 */
	public void setFirma2NihaiToplam(double firma2NihaiToplam) {
		this.firma2NihaiToplam = firma2NihaiToplam;
	}

	/**
	 * @return the firma3IlkToplam
	 */
	public double getFirma3IlkToplam() {
		return firma3IlkToplam;
	}

	/**
	 * @param firma3IlkToplam
	 *            the firma3IlkToplam to set
	 */
	public void setFirma3IlkToplam(double firma3IlkToplam) {
		this.firma3IlkToplam = firma3IlkToplam;
	}

	/**
	 * @return the firma3NihaiToplam
	 */
	public double getFirma3NihaiToplam() {
		return firma3NihaiToplam;
	}

	/**
	 * @param firma3NihaiToplam
	 *            the firma3NihaiToplam to set
	 */
	public void setFirma3NihaiToplam(double firma3NihaiToplam) {
		this.firma3NihaiToplam = firma3NihaiToplam;
	}

	/**
	 * @return the firma4IlkToplam
	 */
	public double getFirma4IlkToplam() {
		return firma4IlkToplam;
	}

	/**
	 * @param firma4IlkToplam
	 *            the firma4IlkToplam to set
	 */
	public void setFirma4IlkToplam(double firma4IlkToplam) {
		this.firma4IlkToplam = firma4IlkToplam;
	}

	/**
	 * @return the firma4NihaiToplam
	 */
	public double getFirma4NihaiToplam() {
		return firma4NihaiToplam;
	}

	/**
	 * @param firma4NihaiToplam
	 *            the firma4NihaiToplam to set
	 */
	public void setFirma4NihaiToplam(double firma4NihaiToplam) {
		this.firma4NihaiToplam = firma4NihaiToplam;
	}

	/**
	 * @return the firma5IlkToplam
	 */
	public double getFirma5IlkToplam() {
		return firma5IlkToplam;
	}

	/**
	 * @param firma5IlkToplam
	 *            the firma5IlkToplam to set
	 */
	public void setFirma5IlkToplam(double firma5IlkToplam) {
		this.firma5IlkToplam = firma5IlkToplam;
	}

	/**
	 * @return the firma5NihaiToplam
	 */
	public double getFirma5NihaiToplam() {
		return firma5NihaiToplam;
	}

	/**
	 * @param firma5NihaiToplam
	 *            the firma5NihaiToplam to set
	 */
	public void setFirma5NihaiToplam(double firma5NihaiToplam) {
		this.firma5NihaiToplam = firma5NihaiToplam;
	}

	/**
	 * @return the firma6IlkToplam
	 */
	public double getFirma6IlkToplam() {
		return firma6IlkToplam;
	}

	/**
	 * @param firma6IlkToplam
	 *            the firma6IlkToplam to set
	 */
	public void setFirma6IlkToplam(double firma6IlkToplam) {
		this.firma6IlkToplam = firma6IlkToplam;
	}

	/**
	 * @return the firma6NihaiToplam
	 */
	public double getFirma6NihaiToplam() {
		return firma6NihaiToplam;
	}

	/**
	 * @param firma6NihaiToplam
	 *            the firma6NihaiToplam to set
	 */
	public void setFirma6NihaiToplam(double firma6NihaiToplam) {
		this.firma6NihaiToplam = firma6NihaiToplam;
	}

	/**
	 * @return the firma7IlkToplam
	 */
	public double getFirma7IlkToplam() {
		return firma7IlkToplam;
	}

	/**
	 * @param firma7IlkToplam
	 *            the firma7IlkToplam to set
	 */
	public void setFirma7IlkToplam(double firma7IlkToplam) {
		this.firma7IlkToplam = firma7IlkToplam;
	}

	/**
	 * @return the firma7NihaiToplam
	 */
	public double getFirma7NihaiToplam() {
		return firma7NihaiToplam;
	}

	/**
	 * @param firma7NihaiToplam
	 *            the firma7NihaiToplam to set
	 */
	public void setFirma7NihaiToplam(double firma7NihaiToplam) {
		this.firma7NihaiToplam = firma7NihaiToplam;
	}

	// /**
	// * @return the dosyaIcerikList
	// */
	// public List<SatinalmaTakipDosyalarIcerik> getDosyaIcerikList() {
	// return dosyaIcerikList;
	// }
	//
	// /**
	// * @param dosyaIcerikList
	// * the dosyaIcerikList to set
	// */
	// public void setDosyaIcerikList(
	// List<SatinalmaTakipDosyalarIcerik> dosyaIcerikList) {
	// this.dosyaIcerikList = dosyaIcerikList;
	// }

}
