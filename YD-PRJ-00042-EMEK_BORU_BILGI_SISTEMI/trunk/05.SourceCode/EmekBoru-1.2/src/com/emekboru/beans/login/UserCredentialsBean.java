package com.emekboru.beans.login;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.model.MenuModel;

import com.emekboru.beans.utils.DynamicMenuModel;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.GeneralPrivilege;
import com.emekboru.jpa.SystemUser;
import com.emekboru.jpa.YetkiLoginLog;
import com.emekboru.jpaman.Factory;
import com.emekboru.jpaman.GeneralPrivilegeManager;
import com.emekboru.jpaman.UserManager;
import com.emekboru.utils.DBUtil;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;
import com.emekboru.utils.UtilViewControl;

@ManagedBean(name = "userCredentialsBean")
@SessionScoped
public class UserCredentialsBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	private String username;
	private String password;
	private SystemUser user;
	private String currentPassword;
	private String newPassword;
	private String repeatedPassword;
	private String dateFormat;
	private String userFullName;
	private GeneralPrivilege generalPrivileges;
	private String userPicturePath;
	private String dateFormatWithTime;

	public UserCredentialsBean() {

		dateFormat = new String("dd/MM/yyyy");
		dateFormatWithTime = new String("dd/MM/yyyy HH:mm");
		username = new String();
		password = new String();
		user = new SystemUser();
		currentPassword = new String();
		newPassword = new String();
		repeatedPassword = new String();
		generalPrivileges = new GeneralPrivilege();
		userPicturePath = null;
	}

	public void loginUser(ActionEvent e) {
		UserManager userManager = new UserManager();
		List<SystemUser> systemUser = userManager.userLogin(username, password);
		if (systemUser.size() == 0) {

			FacesContextUtils.addErrorMessage("LoginError");
			return;
		}
		user = systemUser.get(0);
		if (!user.getIsActive()) {

			FacesContextUtils.addErrorMessage("UserDisabledError");
			return;
		}
		/**
		 * @author aefe Kullanicinin giris yaptigini kaydeder
		 */
		YetkiLoginLog yetkiLoginLog = new YetkiLoginLog();
		String sessionId = UtilViewControl.getSessionId();
		yetkiLoginLog.setUserName(username);
		yetkiLoginLog.setSessionId(sessionId);
		EntityManager em = Factory.getInstance().createEntityManager();
		DBUtil.insertLoginLog(em, username, sessionId,
				UtilInsCore.getTarihZaman());
		FacesContextUtils.redirect("mainpage.jsf");

		if (user.getUserMenu() == null)
			user.setUserMenu(DynamicMenuModel.createModel(user));

		userFullName = user.getEmployee().getFirstname() + " "
				+ user.getEmployee().getLastname();
		UtilViewControl.setSessionAttr("user", userFullName);
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		request.setAttribute("user", user.getEmployee().getFirstname() + " "
				+ user.getEmployee().getLastname());

		// A.K. 04.07.2013
		GeneralPrivilegeManager gmp = new GeneralPrivilegeManager();
		List<GeneralPrivilege> testList = (List<GeneralPrivilege>) gmp
				.findByField(GeneralPrivilege.class, "userType",
						user.getUserType());
		if (testList.size() > 0)
			generalPrivileges = testList.get(0);

		userPicturePath = user.getEmployee().getFirstname() + "_"
				+ user.getEmployee().getLastname() + "_"
				+ user.getEmployee().getEmployeeId();
	}

	/**
	 * @author aefe
	 * 
	 *         Kullaniciya gore menu create eder
	 */
	public MenuModel createMenu() {
		return DynamicMenuModel.createModel(user);
	}

	protected void cancel() {

		username = new String();
		password = new String();
		user = null;
		if (Factory.getEntityManager().getTransaction().isActive())
			Factory.getEntityManager().close();
		FacesContextUtils.closeSession();
	}

	public void logout() {

		cancel();
		FacesContextUtils.redirect(config.getPageUrl().LOGIN_PAGE);
	}

	public void changeUserPassword(ActionEvent event) {

		if (!user.getPassword().equals(currentPassword)
				|| !newPassword.equals(repeatedPassword)) {

			FacesContextUtils.addErrorMessage("NoPasswordMatch");
			return;
		}
		UserManager userManager = new UserManager();
		user.setPassword(newPassword);
		userManager.updateEntity(user);
		currentPassword = new String();
		newPassword = new String();
		repeatedPassword = new String();
		user = (SystemUser) userManager.findByField(SystemUser.class,
				"username", user.getUsername()).get(0);
		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}

	/*************************************************************************/
	/*************************** NAVIGATION FUNCTIONALITIES ******************/
	/*************************************************************************/

	public void goToMainPage() {

		FacesContextUtils.redirect(config.getPageUrl().MAIN_PAGE);
	}

	public void goToLoginPage() {

		FacesContextUtils.redirect(config.getPageUrl().LOGIN_PAGE);
	}

	public void goToYonetimPage() {

		FacesContextUtils.redirect(config.getPageUrl().YONETIM_PAGE);
	}

	/**
	 * REMOVE AFTER PUBLISHING
	 */

	public void close() {

		System.out.println("UserCredentialsBean.close()");
		Factory.getInstance().close();
	}

	// public void refreshModel()
	// {
	// model=createMenu();
	// }

	class MenuActionListener2 implements ActionListener {

		public void processAction(ActionEvent arg0)
				throws AbortProcessingException {

			System.out
					.println("test11... " + arg0.getComponent().getClientId());
			test(arg0.getComponent().getClientId());
		}

		public void actionPerformed(javax.faces.event.ActionEvent e) {
			System.out.println("test22... " + e.getComponent().getClientId());
		}

		public void test(String test) {
			System.out.println("testedddd..." + test);
		}

	}

	/*
	 * GETTERS AND SETTERS
	 */

	public SystemUser getUser() {
		return user;
	}

	public void setUser(SystemUser user1) {
		this.user = user1;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCurrentPassword() {
		return currentPassword;
	}

	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getRepeatedPassword() {
		return repeatedPassword;
	}

	public void setRepeatedPassword(String repeatedPassword) {
		this.repeatedPassword = repeatedPassword;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	public String getUserFullName() {
		return userFullName;
	}

	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}

	/**
	 * @return the generalPrivileges
	 */
	public GeneralPrivilege getGeneralPrivileges() {
		return generalPrivileges;
	}

	/**
	 * @param generalPrivileges
	 *            the generalPrivileges to set
	 */
	public void setGeneralPrivileges(GeneralPrivilege generalPrivileges) {
		this.generalPrivileges = generalPrivileges;
	}

	public String getUserPicturePath() {
		return userPicturePath;
	}

	/**
	 * @return the dateFormatWithTime
	 */
	public String getDateFormatWithTime() {
		return dateFormatWithTime;
	}

	/**
	 * @param dateFormatWithTime
	 *            the dateFormatWithTime to set
	 */
	public void setDateFormatWithTime(String dateFormatWithTime) {
		this.dateFormatWithTime = dateFormatWithTime;
	}

}
