/**
 * 
 */
package com.emekboru.beans.test.tahribatsiztestsonuc;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.test.tahribatsiz.TahribatsizTestOrderListBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.kalibrasyon.KalibrasyonOfflineBoruUcu;
import com.emekboru.jpa.kalibrasyon.KalibrasyonOtomatikUltrasonik;
import com.emekboru.jpa.kalibrasyon.KalibrasyonUltrasonikLaminasyon;
import com.emekboru.jpa.tahribatsiztestsonuc.TestTahribatsizOfflineOtomatikUtSonuc;
import com.emekboru.jpaman.NondestructiveTestsSpecManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.kalibrasyon.KalibrasyonOfflineBoruUcuManager;
import com.emekboru.jpaman.kalibrasyon.KalibrasyonOtomatikUltrasonikManager;
import com.emekboru.jpaman.kalibrasyon.KalibrasyonUltrasonikLaminasyonManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizKoordinatManager;
import com.emekboru.jpaman.tahribatsiztestsonuc.TestTahribatsizOfflineOtomatikUtSonucManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */

@ManagedBean(name = "testTahribatsizOfflineOtomatikUtSonucBean")
@ViewScoped
public class TestTahribatsizOfflineOtomatikUtSonucBean {

	private List<TestTahribatsizOfflineOtomatikUtSonuc> allTestTahribatsizOfflineOtomatikUtSonucList = new ArrayList<TestTahribatsizOfflineOtomatikUtSonuc>();
	private TestTahribatsizOfflineOtomatikUtSonuc testTahribatsizOfflineOtomatikUtSonucForm = new TestTahribatsizOfflineOtomatikUtSonuc();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private boolean visibleButtonRender;

	private Pipe selectedPipe = new Pipe();

	private String barkodNo = null;

	Integer ilkQ = 0;
	Integer ilkL = 0;

	private List<KalibrasyonOtomatikUltrasonik> kalibrasyonOfflinelar = new ArrayList<KalibrasyonOtomatikUltrasonik>();
	private KalibrasyonOtomatikUltrasonik kalibrasyonOffline = new KalibrasyonOtomatikUltrasonik();

	private List<KalibrasyonUltrasonikLaminasyon> kalibrasyonLaminasyonlar = new ArrayList<KalibrasyonUltrasonikLaminasyon>();
	private KalibrasyonUltrasonikLaminasyon kalibrasyonLaminasyon = new KalibrasyonUltrasonikLaminasyon();

	private List<KalibrasyonOfflineBoruUcu> kalibrasyonBoruUcular = new ArrayList<KalibrasyonOfflineBoruUcu>();
	private KalibrasyonOfflineBoruUcu kalibrasyonBoruUcu = new KalibrasyonOfflineBoruUcu();

	public void addTestTahribatsizOfflineOtomatikUtSonuc() {
		testTahribatsizOfflineOtomatikUtSonucForm = new TestTahribatsizOfflineOtomatikUtSonuc();
		updateButtonRender = false;
		visibleButtonRender = false;
	}

	public void offlineOtomatikUtListener(SelectEvent event) {

		updateButtonRender = true;
		ilkQ = testTahribatsizOfflineOtomatikUtSonucForm.getKoordinatQ();
		ilkL = testTahribatsizOfflineOtomatikUtSonucForm.getKoordinatL();
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " BARKOD NUMARALI BORU SİSTEMDE YOKTUR!", null));
			visibleButtonRender = false;
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			TestTahribatsizOfflineOtomatikUtSonucManager offlineOtomatikUtManager = new TestTahribatsizOfflineOtomatikUtSonucManager();
			allTestTahribatsizOfflineOtomatikUtSonucList = offlineOtomatikUtManager
					.getAllTestTahribatsizOfflineOtomatikUtSonuc(selectedPipe
							.getPipeId());
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
			visibleButtonRender = true;
		}

		KalibrasyonOtomatikUltrasonikManager koManager = new KalibrasyonOtomatikUltrasonikManager();
		kalibrasyonOfflinelar = koManager.getAllKalibrasyon(0);

		KalibrasyonUltrasonikLaminasyonManager klManager = new KalibrasyonUltrasonikLaminasyonManager();
		kalibrasyonLaminasyonlar = klManager.getAllKalibrasyon(0);

		KalibrasyonOfflineBoruUcuManager kbuManager = new KalibrasyonOfflineBoruUcuManager();
		kalibrasyonBoruUcular = kbuManager.getAllKalibrasyon();
	}

	@SuppressWarnings("static-access")
	public void addOrUpdateOfflineOtomatikUtSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();

		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");
		Integer prmItemId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}
		if (prmItemId == null) {
			prmItemId = selectedPipe.getSalesItem().getItemId();
		}

		Integer testId = 0;

		TestTahribatsizOfflineOtomatikUtSonucManager offlineOtomatikUtManager = new TestTahribatsizOfflineOtomatikUtSonucManager();
		NondestructiveTestsSpecManager nonManager = new NondestructiveTestsSpecManager();
		TestTahribatsizKoordinatManager koordinatManager = new TestTahribatsizKoordinatManager();

		try {
			if (testTahribatsizOfflineOtomatikUtSonucForm.getKaynakLaminasyon() == 1
					|| testTahribatsizOfflineOtomatikUtSonucForm
							.getKaynakLaminasyon() == 2
					|| testTahribatsizOfflineOtomatikUtSonucForm
							.getKaynakLaminasyon() == 3) {

				testId = nonManager.findByItemIdGlobalId(prmItemId, 1003)
						.get(0).getTestId();
			} else {

				testId = nonManager.findByItemIdGlobalId(prmItemId, 1005)
						.get(0).getTestId();
			}
		} catch (Exception e2) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"KOORDİNAT EKLEME BAŞARISIZ, KALİTE TANIMLARINDA EKSİK VARDIR!",
							null));
			return;
		}

		if (testTahribatsizOfflineOtomatikUtSonucForm.getId() == null) {

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);

			// aynı koordinatı eklememe işlemi
			for (int i = 0; i < allTestTahribatsizOfflineOtomatikUtSonucList
					.size(); i++) {
				if ((testTahribatsizOfflineOtomatikUtSonucForm.getKoordinatQ()
						.equals(allTestTahribatsizOfflineOtomatikUtSonucList
								.get(i).getKoordinatQ()))
						&& (testTahribatsizOfflineOtomatikUtSonucForm
								.getKoordinatL()
								.equals(allTestTahribatsizOfflineOtomatikUtSonucList
										.get(i).getKoordinatL()))) {
					System.out
							.println(" testTahribatsizOfflineOtomatikUtSonucBean.addOrUpdateOtomatikUtSonuc-HATA SameCoordinates");
					FacesContext context = FacesContext.getCurrentInstance();
					context.addMessage(
							null,
							new FacesMessage(
									FacesMessage.SEVERITY_FATAL,
									"AYNI KOORDİNAT TEKRARDAN EKLENEMEZ, KAYDETME HATASI!",
									null));
					return;
				}
			}

			testTahribatsizOfflineOtomatikUtSonucForm.setTestId(testId);
			testTahribatsizOfflineOtomatikUtSonucForm.setPipe(pipe);

			testTahribatsizOfflineOtomatikUtSonucForm
					.setEklemeZamani(UtilInsCore.getTarihZaman());
			testTahribatsizOfflineOtomatikUtSonucForm
					.setEkleyenEmployee(userBean.getUser());

			sonKalibrasyonuGoster(1);
			testTahribatsizOfflineOtomatikUtSonucForm
					.setKalibrasyonOffline(kalibrasyonOffline);

			sonKalibrasyonuGoster(2);
			testTahribatsizOfflineOtomatikUtSonucForm
					.setKalibrasyonLaminasyon(kalibrasyonLaminasyon);

			sonKalibrasyonuGoster(3);
			testTahribatsizOfflineOtomatikUtSonucForm
					.setKalibrasyonBoruUcu(kalibrasyonBoruUcu);

			// otomatik muayene ekleme
			offlineOtomatikUtManager
					.enterNew(testTahribatsizOfflineOtomatikUtSonucForm);

			// kalite speclerde manuelUt ve floroskopi var mi kontrolu
			Integer i = 0;
			Boolean kontrol = false;
			while (i < testTahribatsizOfflineOtomatikUtSonucForm.getPipe()
					.getSalesItem().getNondestructiveTestsSpecs().size()
					&& kontrol == false) {
				// manuelUt ve floroskopi var mi kontrolu, varsa tum diger
				// muayenelere koordinati ekleme
				if (testTahribatsizOfflineOtomatikUtSonucForm.getPipe()
						.getSalesItem().getNondestructiveTestsSpecs().get(i)
						.getGlobalId() == 1004
						|| testTahribatsizOfflineOtomatikUtSonucForm.getPipe()
								.getSalesItem().getNondestructiveTestsSpecs()
								.get(i).getGlobalId() == 1006) {
					koordinatManager.tumTablolaraKoordinatEkleme(
							testTahribatsizOfflineOtomatikUtSonucForm
									.getKoordinatQ(),
							testTahribatsizOfflineOtomatikUtSonucForm
									.getKoordinatL(), pipe, "o", userBean,
							null, testTahribatsizOfflineOtomatikUtSonucForm
									.getKaynakLaminasyon().toString(), 0);
					kontrol = true;
				}
				i++;
			}

			// testlerin hangi borudan yapilacagi kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 3, true);

			this.testTahribatsizOfflineOtomatikUtSonucForm = new TestTahribatsizOfflineOtomatikUtSonuc();
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"OTOMATİK UT KOORDİNATI BAŞARI İLE KAYDEDİLMİŞTİR!"));
		} else {

			// otomatik ut
			testTahribatsizOfflineOtomatikUtSonucForm
					.setGuncellemeZamani(UtilInsCore.getTarihZaman());
			testTahribatsizOfflineOtomatikUtSonucForm
					.setEkleyenEmployee(userBean.getUser());
			testTahribatsizOfflineOtomatikUtSonucForm.setTestId(testId);

			offlineOtomatikUtManager
					.updateEntity(testTahribatsizOfflineOtomatikUtSonucForm);

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"OTOMATİK UT KOORDİNATI BAŞARI İLE GÜNCELLENMİŞTİR!"));
		}

		fillTestList(prmPipeId);
		testTahribatsizOfflineOtomatikUtSonucForm = new TestTahribatsizOfflineOtomatikUtSonuc();
		updateButtonRender = false;
		// sayfada barkoddan okutunca order item pipe seçimi olmadıgı için
		// sıkıntı cıkıyordu
		TahribatsizTestOrderListBean ttolb = new TahribatsizTestOrderListBean();
		ttolb.loadOrderDetails(selectedPipe.getSalesItem().getItemId());
	}

	public void fillTestList(Integer pipeId) {

		try {
			TestTahribatsizOfflineOtomatikUtSonucManager manager = new TestTahribatsizOfflineOtomatikUtSonucManager();
			allTestTahribatsizOfflineOtomatikUtSonucList = manager
					.getAllTestTahribatsizOfflineOtomatikUtSonuc(pipeId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void deleteOfflineOtomatikUtSonuc(ActionEvent e) {

		if (testTahribatsizOfflineOtomatikUtSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		TestTahribatsizOfflineOtomatikUtSonucManager manuelUtSonucManager = new TestTahribatsizOfflineOtomatikUtSonucManager();

		manuelUtSonucManager.delete(testTahribatsizOfflineOtomatikUtSonucForm);
		testTahribatsizOfflineOtomatikUtSonucForm = new TestTahribatsizOfflineOtomatikUtSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");

		fillTestList(prmPipeId);
	}

	@SuppressWarnings("static-access")
	public void sonKalibrasyonuGoster(Integer prmType) {
		if (prmType == 1) {
			kalibrasyonOffline = new KalibrasyonOtomatikUltrasonik();
			KalibrasyonOtomatikUltrasonikManager kalibrasyonManager = new KalibrasyonOtomatikUltrasonikManager();
			kalibrasyonOffline = kalibrasyonManager.getAllKalibrasyon(0).get(0);
		} else if (prmType == 2) {
			kalibrasyonLaminasyon = new KalibrasyonUltrasonikLaminasyon();
			KalibrasyonUltrasonikLaminasyonManager kalibrasyonManager = new KalibrasyonUltrasonikLaminasyonManager();
			kalibrasyonLaminasyon = kalibrasyonManager.getAllKalibrasyon(0)
					.get(0);
		} else if (prmType == 3) {
			kalibrasyonBoruUcu = new KalibrasyonOfflineBoruUcu();
			KalibrasyonOfflineBoruUcuManager kalibrasyonManager = new KalibrasyonOfflineBoruUcuManager();
			kalibrasyonBoruUcu = kalibrasyonManager.getAllKalibrasyon().get(0);
		}
	}

	public void kalibrasyonGoster(Integer prmType) {
		if (prmType == 1) {
			kalibrasyonLaminasyon = new KalibrasyonUltrasonikLaminasyon();
			kalibrasyonBoruUcu = new KalibrasyonOfflineBoruUcu();
			kalibrasyonOffline = new KalibrasyonOtomatikUltrasonik();
			kalibrasyonOffline = testTahribatsizOfflineOtomatikUtSonucForm
					.getKalibrasyonOffline();
		} else if (prmType == 2) {
			kalibrasyonBoruUcu = new KalibrasyonOfflineBoruUcu();
			kalibrasyonOffline = new KalibrasyonOtomatikUltrasonik();
			kalibrasyonLaminasyon = new KalibrasyonUltrasonikLaminasyon();
			kalibrasyonLaminasyon = testTahribatsizOfflineOtomatikUtSonucForm
					.getKalibrasyonLaminasyon();
		} else if (prmType == 2) {
			kalibrasyonLaminasyon = new KalibrasyonUltrasonikLaminasyon();
			kalibrasyonOffline = new KalibrasyonOtomatikUltrasonik();
			kalibrasyonBoruUcu = new KalibrasyonOfflineBoruUcu();
			kalibrasyonBoruUcu = testTahribatsizOfflineOtomatikUtSonucForm
					.getKalibrasyonBoruUcu();
		}

	}

	public void kalibrasyonAta(Integer prmType) {
		TestTahribatsizOfflineOtomatikUtSonucManager otomatikManager = new TestTahribatsizOfflineOtomatikUtSonucManager();
		if (prmType == 1) {
			try {
				for (int i = 0; i < allTestTahribatsizOfflineOtomatikUtSonucList
						.size(); i++) {
					allTestTahribatsizOfflineOtomatikUtSonucList.get(i)
							.setKalibrasyonOffline(kalibrasyonOffline);
					otomatikManager
							.updateEntity(allTestTahribatsizOfflineOtomatikUtSonucList
									.get(i));
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"KALİBRASYON ATAMA BAŞARISIZ, TEKRAR DENEYİNİZ!", null));
				return;
			}
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "KALİBRASYON ATAMA BAŞARILI!",
					null));
		} else if (prmType == 2) {
			try {
				for (int i = 0; i < allTestTahribatsizOfflineOtomatikUtSonucList
						.size(); i++) {
					allTestTahribatsizOfflineOtomatikUtSonucList.get(i)
							.setKalibrasyonLaminasyon(kalibrasyonLaminasyon);
					otomatikManager
							.updateEntity(allTestTahribatsizOfflineOtomatikUtSonucList
									.get(i));
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"KALİBRASYON ATAMA BAŞARISIZ, TEKRAR DENEYİNİZ!", null));
				return;
			}
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "KALİBRASYON ATAMA BAŞARILI!",
					null));
		} else if (prmType == 3) {
			try {
				for (int i = 0; i < allTestTahribatsizOfflineOtomatikUtSonucList
						.size(); i++) {
					allTestTahribatsizOfflineOtomatikUtSonucList.get(i)
							.setKalibrasyonBoruUcu(kalibrasyonBoruUcu);
					otomatikManager
							.updateEntity(allTestTahribatsizOfflineOtomatikUtSonucList
									.get(i));
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR,
						"KALİBRASYON ATAMA BAŞARISIZ, TEKRAR DENEYİNİZ!", null));
				return;
			}
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "KALİBRASYON ATAMA BAŞARILI!",
					null));
		}
	}

	// setters getters

	/**
	 * @return the allTestTahribatsizOfflineOtomatikUtSonucList
	 */
	public List<TestTahribatsizOfflineOtomatikUtSonuc> getAllTestTahribatsizOfflineOtomatikUtSonucList() {
		return allTestTahribatsizOfflineOtomatikUtSonucList;
	}

	/**
	 * @param allTestTahribatsizOfflineOtomatikUtSonucList
	 *            the allTestTahribatsizOfflineOtomatikUtSonucList to set
	 */
	public void setAllTestTahribatsizOfflineOtomatikUtSonucList(
			List<TestTahribatsizOfflineOtomatikUtSonuc> allTestTahribatsizOfflineOtomatikUtSonucList) {
		this.allTestTahribatsizOfflineOtomatikUtSonucList = allTestTahribatsizOfflineOtomatikUtSonucList;
	}

	/**
	 * @return the testTahribatsizOfflineOtomatikUtSonucForm
	 */
	public TestTahribatsizOfflineOtomatikUtSonuc getTestTahribatsizOfflineOtomatikUtSonucForm() {
		return testTahribatsizOfflineOtomatikUtSonucForm;
	}

	/**
	 * @param testTahribatsizOfflineOtomatikUtSonucForm
	 *            the testTahribatsizOfflineOtomatikUtSonucForm to set
	 */
	public void setTestTahribatsizOfflineOtomatikUtSonucForm(
			TestTahribatsizOfflineOtomatikUtSonuc testTahribatsizOfflineOtomatikUtSonucForm) {
		this.testTahribatsizOfflineOtomatikUtSonucForm = testTahribatsizOfflineOtomatikUtSonucForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the selectedPipe
	 */
	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	/**
	 * @param selectedPipe
	 *            the selectedPipe to set
	 */
	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	/**
	 * @return the barkodNo
	 */
	public String getBarkodNo() {
		return barkodNo;
	}

	/**
	 * @param barkodNo
	 *            the barkodNo to set
	 */
	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	/**
	 * @return the ilkQ
	 */
	public Integer getIlkQ() {
		return ilkQ;
	}

	/**
	 * @param ilkQ
	 *            the ilkQ to set
	 */
	public void setIlkQ(Integer ilkQ) {
		this.ilkQ = ilkQ;
	}

	/**
	 * @return the ilkL
	 */
	public Integer getIlkL() {
		return ilkL;
	}

	/**
	 * @param ilkL
	 *            the ilkL to set
	 */
	public void setIlkL(Integer ilkL) {
		this.ilkL = ilkL;
	}

	/**
	 * @return the kalibrasyonOfflinelar
	 */
	public List<KalibrasyonOtomatikUltrasonik> getKalibrasyonOfflinelar() {
		return kalibrasyonOfflinelar;
	}

	/**
	 * @param kalibrasyonOfflinelar
	 *            the kalibrasyonOfflinelar to set
	 */
	public void setKalibrasyonOfflinelar(
			List<KalibrasyonOtomatikUltrasonik> kalibrasyonOfflinelar) {
		this.kalibrasyonOfflinelar = kalibrasyonOfflinelar;
	}

	/**
	 * @return the kalibrasyonOffline
	 */
	public KalibrasyonOtomatikUltrasonik getKalibrasyonOffline() {
		return kalibrasyonOffline;
	}

	/**
	 * @param kalibrasyonOffline
	 *            the kalibrasyonOffline to set
	 */
	public void setKalibrasyonOffline(
			KalibrasyonOtomatikUltrasonik kalibrasyonOffline) {
		this.kalibrasyonOffline = kalibrasyonOffline;
	}

	/**
	 * @return the kalibrasyonLaminasyonlar
	 */
	public List<KalibrasyonUltrasonikLaminasyon> getKalibrasyonLaminasyonlar() {
		return kalibrasyonLaminasyonlar;
	}

	/**
	 * @param kalibrasyonLaminasyonlar
	 *            the kalibrasyonLaminasyonlar to set
	 */
	public void setKalibrasyonLaminasyonlar(
			List<KalibrasyonUltrasonikLaminasyon> kalibrasyonLaminasyonlar) {
		this.kalibrasyonLaminasyonlar = kalibrasyonLaminasyonlar;
	}

	/**
	 * @return the kalibrasyonLaminasyon
	 */
	public KalibrasyonUltrasonikLaminasyon getKalibrasyonLaminasyon() {
		return kalibrasyonLaminasyon;
	}

	/**
	 * @param kalibrasyonLaminasyon
	 *            the kalibrasyonLaminasyon to set
	 */
	public void setKalibrasyonLaminasyon(
			KalibrasyonUltrasonikLaminasyon kalibrasyonLaminasyon) {
		this.kalibrasyonLaminasyon = kalibrasyonLaminasyon;
	}

	/**
	 * @return the kalibrasyonBoruUcular
	 */
	public List<KalibrasyonOfflineBoruUcu> getKalibrasyonBoruUcular() {
		return kalibrasyonBoruUcular;
	}

	/**
	 * @param kalibrasyonBoruUcular
	 *            the kalibrasyonBoruUcular to set
	 */
	public void setKalibrasyonBoruUcular(
			List<KalibrasyonOfflineBoruUcu> kalibrasyonBoruUcular) {
		this.kalibrasyonBoruUcular = kalibrasyonBoruUcular;
	}

	/**
	 * @return the kalibrasyonBoruUcu
	 */
	public KalibrasyonOfflineBoruUcu getKalibrasyonBoruUcu() {
		return kalibrasyonBoruUcu;
	}

	/**
	 * @param kalibrasyonBoruUcu
	 *            the kalibrasyonBoruUcu to set
	 */
	public void setKalibrasyonBoruUcu(
			KalibrasyonOfflineBoruUcu kalibrasyonBoruUcu) {
		this.kalibrasyonBoruUcu = kalibrasyonBoruUcu;
	}

	/**
	 * @return the visibleButtonRender
	 */
	public boolean isVisibleButtonRender() {
		return visibleButtonRender;
	}

}
