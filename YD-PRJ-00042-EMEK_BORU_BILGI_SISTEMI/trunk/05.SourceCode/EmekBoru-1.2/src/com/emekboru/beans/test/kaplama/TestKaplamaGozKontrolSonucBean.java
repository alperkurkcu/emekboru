package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaGozKontrolSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaGozKontrolSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaplamaGozKontrolSonucBean")
@ViewScoped
public class TestKaplamaGozKontrolSonucBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private TestKaplamaGozKontrolSonuc testKaplamaGozKontrolSonucForm = new TestKaplamaGozKontrolSonuc();
	private List<TestKaplamaGozKontrolSonuc> allTestKaplamaGozKontrolSonucList = new ArrayList<TestKaplamaGozKontrolSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateTestKaplamaGozKontrolSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaGozKontrolSonucManager manager = new TestKaplamaGozKontrolSonucManager();

		if (testKaplamaGozKontrolSonucForm.getId() == null) {

			testKaplamaGozKontrolSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaGozKontrolSonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaGozKontrolSonucForm.setBagliTestId(bagliTest);
			testKaplamaGozKontrolSonucForm.setBagliGlobalId(bagliTest);

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaGozKontrolSonucForm.setPipe(pipe);

			manager.enterNew(testKaplamaGozKontrolSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaGozKontrolSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaGozKontrolSonucForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			manager.updateEntity(testKaplamaGozKontrolSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addTestKaplamaGozKontrolSonuc() {

		testKaplamaGozKontrolSonucForm = new TestKaplamaGozKontrolSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaGozKontrolSonucListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaGozKontrolSonucList = TestKaplamaGozKontrolSonucManager
				.getAllTestKaplamaGozKontrolSonuc(pipeId, globalId);
		testKaplamaGozKontrolSonucForm = new TestKaplamaGozKontrolSonuc();
	}

	public void deleteTestKaplamaGozKontrolSonuc() {

		bagliTestId = testKaplamaGozKontrolSonucForm.getBagliGlobalId().getId();

		if (testKaplamaGozKontrolSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaGozKontrolSonucManager manager = new TestKaplamaGozKontrolSonucManager();
		manager.delete(testKaplamaGozKontrolSonucForm);
		testKaplamaGozKontrolSonucForm = new TestKaplamaGozKontrolSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters
	public TestKaplamaGozKontrolSonuc getTestKaplamaGozKontrolSonucForm() {
		return testKaplamaGozKontrolSonucForm;
	}

	public void setTestKaplamaGozKontrolSonucForm(
			TestKaplamaGozKontrolSonuc testKaplamaGozKontrolSonucForm) {
		this.testKaplamaGozKontrolSonucForm = testKaplamaGozKontrolSonucForm;
	}

	public List<TestKaplamaGozKontrolSonuc> getAllTestKaplamaGozKontrolSonucList() {
		return allTestKaplamaGozKontrolSonucList;
	}

	public void setAllTestKaplamaGozKontrolSonucList(
			List<TestKaplamaGozKontrolSonuc> allTestKaplamaGozKontrolSonucList) {
		this.allTestKaplamaGozKontrolSonucList = allTestKaplamaGozKontrolSonucList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
