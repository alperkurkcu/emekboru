package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaNihaiKontrolSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaNihaiKontrolSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaplamaNihaiKontrolSonucBean")
@ViewScoped
public class TestKaplamaNihaiKontrolSonucBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private TestKaplamaNihaiKontrolSonuc testKaplamaNihaiKontrolSonucForm = new TestKaplamaNihaiKontrolSonuc();
	private List<TestKaplamaNihaiKontrolSonuc> allTestKaplamaNihaiKontrolSonucList = new ArrayList<TestKaplamaNihaiKontrolSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateTestKaplamaNihaiKontrolSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaNihaiKontrolSonucManager manager = new TestKaplamaNihaiKontrolSonucManager();

		if (testKaplamaNihaiKontrolSonucForm.getId() == null) {

			testKaplamaNihaiKontrolSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaNihaiKontrolSonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaNihaiKontrolSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaNihaiKontrolSonucForm.setBagliTestId(bagliTest);
			testKaplamaNihaiKontrolSonucForm.setBagliGlobalId(bagliTest);

			manager.enterNew(testKaplamaNihaiKontrolSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaNihaiKontrolSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaNihaiKontrolSonucForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			manager.updateEntity(testKaplamaNihaiKontrolSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addTestKaplamaNihaiKontrolSonuc() {

		testKaplamaNihaiKontrolSonucForm = new TestKaplamaNihaiKontrolSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaNihaiKontrolSonucListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaNihaiKontrolSonucList = TestKaplamaNihaiKontrolSonucManager
				.getAllTestKaplamaNihaiKontrolSonuc(globalId, pipeId);
		testKaplamaNihaiKontrolSonucForm = new TestKaplamaNihaiKontrolSonuc();
	}

	public void deleteTestKaplamaNihaiKontrolSonuc() {

		bagliTestId = testKaplamaNihaiKontrolSonucForm.getBagliGlobalId()
				.getId();

		if (testKaplamaNihaiKontrolSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaNihaiKontrolSonucManager manager = new TestKaplamaNihaiKontrolSonucManager();
		manager.delete(testKaplamaNihaiKontrolSonucForm);
		testKaplamaNihaiKontrolSonucForm = new TestKaplamaNihaiKontrolSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters
	public TestKaplamaNihaiKontrolSonuc getTestKaplamaNihaiKontrolSonucForm() {
		return testKaplamaNihaiKontrolSonucForm;
	}

	public void setTestKaplamaNihaiKontrolSonucForm(
			TestKaplamaNihaiKontrolSonuc testKaplamaNihaiKontrolSonucForm) {
		this.testKaplamaNihaiKontrolSonucForm = testKaplamaNihaiKontrolSonucForm;
	}

	public List<TestKaplamaNihaiKontrolSonuc> getAllTestKaplamaNihaiKontrolSonucList() {
		return allTestKaplamaNihaiKontrolSonucList;
	}

	public void setAllTestKaplamaNihaiKontrolSonucList(
			List<TestKaplamaNihaiKontrolSonuc> allTestKaplamaNihaiKontrolSonucList) {
		this.allTestKaplamaNihaiKontrolSonucList = allTestKaplamaNihaiKontrolSonucList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
