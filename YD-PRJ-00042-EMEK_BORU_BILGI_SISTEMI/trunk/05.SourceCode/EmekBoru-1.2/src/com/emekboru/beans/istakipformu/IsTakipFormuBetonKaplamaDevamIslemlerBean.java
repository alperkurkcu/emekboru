/**
 * 
 */
package com.emekboru.beans.istakipformu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.istakipformu.IsTakipFormuBetonKaplama;
import com.emekboru.jpa.istakipformu.IsTakipFormuBetonKaplamaDevamIslemler;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.istakipformu.IsTakipFormuBetonKaplamaDevamIslemlerManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isTakipFormuBetonKaplamaDevamIslemlerBean")
@ViewScoped
public class IsTakipFormuBetonKaplamaDevamIslemlerBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<IsTakipFormuBetonKaplamaDevamIslemler> allIsTakipFormuBetonKaplamaDevamIslemlerList = new ArrayList<IsTakipFormuBetonKaplamaDevamIslemler>();
	private IsTakipFormuBetonKaplamaDevamIslemler isTakipFormuBetonKaplamaDevamIslemlerForm = new IsTakipFormuBetonKaplamaDevamIslemler();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private boolean buttonRender;

	private Pipe selectedPipe = new Pipe();

	private String barkodNo = null;

	private SalesItem selectedSalesItem = new SalesItem();
	private IsTakipFormuBetonKaplama selectedIsTakipFormuBetonKaplama = new IsTakipFormuBetonKaplama();

	private Integer salesItemId = null;

	public IsTakipFormuBetonKaplamaDevamIslemlerBean() {

	}

	public void reset() {

		isTakipFormuBetonKaplamaDevamIslemlerForm = new IsTakipFormuBetonKaplamaDevamIslemler();
		updateButtonRender = false;
		buttonRender = false;
	};

	public void addOrUpdateFormSonuc() {

		IsTakipFormuBetonKaplamaDevamIslemlerManager devamManager = new IsTakipFormuBetonKaplamaDevamIslemlerManager();

		if (isTakipFormuBetonKaplamaDevamIslemlerForm.getId() == null) {

			try {

				isTakipFormuBetonKaplamaDevamIslemlerForm
						.setEklemeZamani(UtilInsCore.getTarihZaman());
				isTakipFormuBetonKaplamaDevamIslemlerForm
						.setEkleyenEmployee(userBean.getUser());
				isTakipFormuBetonKaplamaDevamIslemlerForm.setPipe(selectedPipe);
				isTakipFormuBetonKaplamaDevamIslemlerForm
						.setSalesItem(selectedPipe.getSalesItem());

				devamManager
						.enterNew(isTakipFormuBetonKaplamaDevamIslemlerForm);

				FacesContext context = FacesContext.getCurrentInstance();

				context.addMessage(null, new FacesMessage(
						"BETON KAPLAMA İŞ TAKİP FORMU BAŞARIYLA EKLENMİŞTİR!"));

			} catch (Exception ex) {

				System.out
						.println("isTakipFormuBetonKaplamaDevamIslemlerBean.addOrUpdateFormSonuc-HATA");
			}

		} else {

			try {

				isTakipFormuBetonKaplamaDevamIslemlerForm
						.setGuncellemeZamani(UtilInsCore.getTarihZaman());
				isTakipFormuBetonKaplamaDevamIslemlerForm
						.setGuncelleyenEmployee(userBean.getUser());
				devamManager
						.updateEntity(isTakipFormuBetonKaplamaDevamIslemlerForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								"BETON KAPLAMA İŞ TAKİP FORMU BAŞARIYLA GÜNCELLENMİŞTİR!"));
			} catch (Exception ex) {

				System.out
						.println("isTakipFormuBetonKaplamaDevamIslemlerBean.addOrUpdateFormSonuc-HATA"
								+ ex.toString());
			}
		}
		fillTestList(selectedPipe.getSalesItem().getItemId());
	}

	@SuppressWarnings("static-access")
	public void fillTestList(Integer prmItemId) {

		IsTakipFormuBetonKaplamaDevamIslemlerManager formManager = new IsTakipFormuBetonKaplamaDevamIslemlerManager();
		allIsTakipFormuBetonKaplamaDevamIslemlerList = formManager
				.getAllIsTakipFormuBetonKaplamaDevamIslemler(prmItemId);
		buttonRender = true;
		salesItemId = prmItemId;
	}

	public void formListener(SelectEvent event) {
		updateButtonRender = true;
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_WARN, prmBarkod
							+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!", null));
			return;
		} else {

			if (salesItemId != pipeMan.findByBarkodNo(prmBarkod).get(0)
					.getSalesItem().getItemId()) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(
						null,
						new FacesMessage(
								FacesMessage.SEVERITY_WARN,
								prmBarkod
										+ " BARKOD NUMARALI BORU, YANLIŞ SİPARİŞ BORUSUDUR, LÜTFEN KONTROL EDİNİZ!",
								null));
				return;
			}

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			IsTakipFormuBetonKaplamaDevamIslemlerManager formManager = new IsTakipFormuBetonKaplamaDevamIslemlerManager();
			allIsTakipFormuBetonKaplamaDevamIslemlerList = formManager
					.getAllIsTakipFormuBetonKaplamaDevamIslemler(selectedPipe
							.getSalesItem().getItemId());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
		}
	}

	public void deleteFormSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		IsTakipFormuBetonKaplamaDevamIslemlerManager formManager = new IsTakipFormuBetonKaplamaDevamIslemlerManager();

		if (isTakipFormuBetonKaplamaDevamIslemlerForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		formManager.delete(isTakipFormuBetonKaplamaDevamIslemlerForm);
		isTakipFormuBetonKaplamaDevamIslemlerForm = new IsTakipFormuBetonKaplamaDevamIslemler();
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"BETON KAPLAMA İŞ TAKİP FORMU BAŞARIYLA SİLİNMİŞTİR!"));

		fillTestList(selectedPipe.getSalesItem().getItemId());
	}

	// setters getters

	public List<IsTakipFormuBetonKaplamaDevamIslemler> getAllIsTakipFormuBetonKaplamaDevamIslemlerList() {
		return allIsTakipFormuBetonKaplamaDevamIslemlerList;
	}

	public void setAllIsTakipFormuBetonKaplamaDevamIslemlerList(
			List<IsTakipFormuBetonKaplamaDevamIslemler> allIsTakipFormuBetonKaplamaDevamIslemlerList) {
		this.allIsTakipFormuBetonKaplamaDevamIslemlerList = allIsTakipFormuBetonKaplamaDevamIslemlerList;
	}

	public IsTakipFormuBetonKaplamaDevamIslemler getIsTakipFormuBetonKaplamaDevamIslemlerForm() {
		return isTakipFormuBetonKaplamaDevamIslemlerForm;
	}

	public void setIsTakipFormuBetonKaplamaDevamIslemlerForm(
			IsTakipFormuBetonKaplamaDevamIslemler isTakipFormuBetonKaplamaDevamIslemlerForm) {
		this.isTakipFormuBetonKaplamaDevamIslemlerForm = isTakipFormuBetonKaplamaDevamIslemlerForm;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public boolean isButtonRender() {
		return buttonRender;
	}

	public void setButtonRender(boolean buttonRender) {
		this.buttonRender = buttonRender;
	}

	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	public String getBarkodNo() {
		return barkodNo;
	}

	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	public SalesItem getSelectedSalesItem() {
		return selectedSalesItem;
	}

	public void setSelectedSalesItem(SalesItem selectedSalesItem) {
		this.selectedSalesItem = selectedSalesItem;
	}

	public IsTakipFormuBetonKaplama getSelectedIsTakipFormuBetonKaplama() {
		return selectedIsTakipFormuBetonKaplama;
	}

	public void setSelectedIsTakipFormuBetonKaplama(
			IsTakipFormuBetonKaplama selectedIsTakipFormuBetonKaplama) {
		this.selectedIsTakipFormuBetonKaplama = selectedIsTakipFormuBetonKaplama;
	}

	public Integer getSalesItemId() {
		return salesItemId;
	}

	public void setSalesItemId(Integer salesItemId) {
		this.salesItemId = salesItemId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
