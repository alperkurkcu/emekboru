package com.emekboru.beans.test.tahribatsiz;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.beans.test.TestRendersBean;
import com.emekboru.beans.test.tahribatsiztestsonuc.TestTahribatsizFloroskopikSonucBean;
import com.emekboru.beans.test.tahribatsiztestsonuc.TestTahribatsizManuelUtSonucBean;
import com.emekboru.beans.test.tahribatsiztestsonuc.TestTahribatsizOtomatikUtSonucBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.NondestructiveTestsSpec;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.NondestructiveTestsSpecManager;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.sales.order.SalesItemManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;
import com.emekboru.utils.lazymodels.LazyPipeDataModel;

@ManagedBean(name = "tahribatsizTestOrderListBean")
@ViewScoped
public class TahribatsizTestOrderListBean {

	@EJB
	private ConfigBean config;
	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	// tahribatsız test sonuc
	@ManagedProperty(value = "#{testTahribatsizOtomatikUtSonucBean}")
	private TestTahribatsizOtomatikUtSonucBean testTahribatsizOtomatikUtSonucBean;

	@ManagedProperty(value = "#{testTahribatsizManuelUtSonucBean}")
	private TestTahribatsizManuelUtSonucBean testTahribatsizManuelUtSonucBean;

	@ManagedProperty(value = "#{testTahribatsizFloroskopikSonucBean}")
	private TestTahribatsizFloroskopikSonucBean testTahribatsizFloroskopikSonucBean;

	@ManagedProperty(value = "#{testRendersBean}")
	private TestRendersBean testRendersBean;

	// SalesItems
	private SalesItem selectedSalesItem;

	// Pipes
	// private List<Pipe> selectedPipes = new ArrayList<Pipe>();
	private LazyDataModel<Pipe> lazyModel;
	private Pipe selectedPipe = new Pipe();
	private Pipe selectedBarcodePipe = new Pipe();
	private String barkodNo = null;

	// Tahribats�z Testler Kalite �artlar�
	private List<NondestructiveTestsSpec> nondestructiveTests = new ArrayList<NondestructiveTestsSpec>();
	private NondestructiveTestsSpec selectedNonTest = new NondestructiveTestsSpec();

	// Tahribatsiz test acılış sayfası
	public void goTo() {

		falseAll();
		FacesContextUtils.redirect(config.getPageUrl().PIPE_TAHRIBATSIZ_TEST);
	}

	// Otomatik Ut acılış sayfası
	public void goToOtomatikUt() {

		falseAll();
		FacesContextUtils
				.redirect(config.getPageUrl().PIPE_TAHRIBATSIZ_OTOMATIK_UT);
	}

	// Manuel Ut acılış sayfası
	public void goToManuelUt() {

		falseAll();
		FacesContextUtils
				.redirect(config.getPageUrl().PIPE_TAHRIBATSIZ_MANUEL_UT);
	}

	// Floroskopik acılış sayfası
	public void goToFloroskopik() {

		falseAll();
		FacesContextUtils
				.redirect(config.getPageUrl().PIPE_TAHRIBATSIZ_FLOROSKOPIK);
	}

	// Tamir acılış sayfası
	public void goToTamir() {

		falseAll();
		FacesContextUtils.redirect(config.getPageUrl().PIPE_TAHRIBATSIZ_TAMIR);
	}

	// Torna acılış sayfası
	public void goToTorna() {

		falseAll();
		FacesContextUtils.redirect(config.getPageUrl().PIPE_TAHRIBATSIZ_TORNA);
	}

	// Hidrostatik acılış sayfası
	public void goToHidro() {

		falseAll();
		FacesContextUtils
				.redirect(config.getPageUrl().PIPE_TAHRIBATSIZ_HIDROSTATIK);
	}

	// Göz Ölçü acılış sayfası
	public void goToGozOlcu() {

		falseAll();
		FacesContextUtils
				.redirect(config.getPageUrl().PIPE_TAHRIBATSIZ_GOZ_OLCU);
	}

	// Kesim acılış sayfası
	public void goToKesim() {

		falseAll();
		FacesContextUtils.redirect(config.getPageUrl().PIPE_TAHRIBATSIZ_KESIM);
	}

	// Boru Listesi acılış sayfası
	public void goToBoruListesi() {

		FacesContextUtils.redirect(config.getPageUrl().PIPE_LIST_PAGE);
	}

	// Tahribatsiz Test yapılmıs boruların listesi
	public void goToTahribatsizTestBoruListesi() {

		FacesContextUtils
				.redirect(config.getPageUrl().PIPE_DESTRUCTIVE_TEST_LIST_PAGE);
	}

	// Kaynak Gözle Muayene Ekranı
	public void goToKaynakGozleMuayene() {

		FacesContextUtils.redirect(config.getPageUrl().PIPE_KAYNAK_GOZLE_PAGE);
	}

	public void goToRadyografikMuayene() {

		FacesContextUtils.redirect(config.getPageUrl().PIPE_RADYOGRAFIK_PAGE);
	}

	public void goToPenetrantMuayene() {

		FacesContextUtils.redirect(config.getPageUrl().PIPE_PENETRANT_PAGE);
	}

	public void goToManyetikParcacikMuayene() {

		FacesContextUtils
				.redirect(config.getPageUrl().PIPE_MANYETIK_PARCACIK_PAGE);
	}

	public void goToOfflineOtomatikUt() {

		FacesContextUtils
				.redirect(config.getPageUrl().PIPE_OFFLINE_OTOMATIK_UT_PAGE);
	}

	public void goToKaplamaBoruListesi() {

		FacesContextUtils.redirect(config.getPageUrl().PIPE_COATING_LIST_PAGE);
	}

	public void goToPipeEdit() {

		FacesContextUtils.redirect(config.getPageUrl().PIPE_EDIT_PAGE);
	}

	// eski true olmus tabların yeni sayfada görünmemesi, sayfa yeni açılırken
	// tumunun false edilmesi.
	public void falseAll() {

		QualitySpecBean qb = new QualitySpecBean();
		qb.falseAllRenders();
	}

	/*
	 * LOAD
	 */

	// public void loadSelectedOrder() {
	// PipeManager pipeManager = new PipeManager();
	// if (selectedOrder != null) {
	// selectedPipes = pipeManager.findByOrderId(selectedOrder
	// .getOrderId());
	// selectedSonuc = new Sonuc();
	// selectedOrder.getNondestructiveTestsSpecs();
	// } else {
	// selectedPipes = new ArrayList<Pipe>();
	// selectedSonuc = new Sonuc();
	// }
	// // order i�in eklenmi� tahribats�z testleri listeler
	// NondestructiveTestsSpecManager nonMan = new
	// NondestructiveTestsSpecManager();
	// nondestructiveTests = (List<NondestructiveTestsSpec>) nonMan
	// .findByFieldOrderBy(NondestructiveTestsSpec.class,
	// "order.orderId", selectedOrder.getOrderId(), "globalId");
	//
	// }

	// public void loadSelectedOrder() {
	// PipeManager pipeManager = new PipeManager();
	// if (selectedSalesItem != null) {
	// selectedPipes = pipeManager.findByItemId(selectedSalesItem
	// .getItemId());
	// selectedSalesItem.getNondestructiveTestsSpecs();
	// } else {
	// selectedPipes = new ArrayList<Pipe>();
	// }
	// // order için eklenmiş tahribatsız testleri listeler
	// NondestructiveTestsSpecManager nonMan = new
	// NondestructiveTestsSpecManager();
	// nondestructiveTests = (List<NondestructiveTestsSpec>) nonMan
	// .findByFieldOrderBy(NondestructiveTestsSpec.class,
	// "salesItem.itemId", selectedSalesItem.getItemId(),
	// "globalId");
	// }

	public void loadSelectedOrder(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeManager = new PipeManager();

		if (pipeManager.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!", null));
			return;
		} else {

			selectedBarcodePipe = pipeManager.findByBarkodNo(prmBarkod).get(0);
			lazyModel = new LazyPipeDataModel(selectedBarcodePipe
					.getSalesItem().getItemId());
			selectedBarcodePipe.getSalesItem().getNondestructiveTestsSpecs();

			NondestructiveTestsSpecManager nonMan = new NondestructiveTestsSpecManager();
			nondestructiveTests = (List<NondestructiveTestsSpec>) nonMan
					.findByFieldOrderBy(NondestructiveTestsSpec.class,
							"salesItem.itemId", selectedBarcodePipe
									.getSalesItem().getItemId(), "globalId");
		}
	}

	public void nonTestResultListener(SelectEvent event) {
		// tahribats�z testler
		// otomatik Ut kaynak
		if (selectedNonTest.getGlobalId() == 1001) {
			falseAllRenders();
			testRendersBean.setTestOtomatikUtKaynak(true);
			testTahribatsizOtomatikUtSonucBean.fillTestList(selectedPipe
					.getPipeId());
		}// otomatik Ut laminasyon
		else if (selectedNonTest.getGlobalId() == 1002) {
			falseAllRenders();
			testRendersBean.setTestOtomatikUtKaynak(true);
			testTahribatsizOtomatikUtSonucBean.fillTestList(selectedPipe
					.getPipeId());
		}// manuel ut
		else if (selectedNonTest.getGlobalId() == 1004) {
			falseAllRenders();
			testRendersBean.setTestManuelUt(true);
			testTahribatsizManuelUtSonucBean.fillTestList(selectedPipe
					.getPipeId());
		}// floroskopik
		else if (selectedNonTest.getGlobalId() == 1006) {
			falseAllRenders();
			testRendersBean.setTestFloroskopik(true);
			testTahribatsizFloroskopikSonucBean.fillTestList(selectedPipe
					.getPipeId());
		}
	}

	public void falseAllRenders() {
		UtilInsCore.setProperties(testRendersBean, false);
	}

	public void loadOrderDetails() {

		SalesItemManager siMan = new SalesItemManager();
		selectedSalesItem = siMan.loadObject(SalesItem.class,
				selectedSalesItem.getItemId());
		selectedSalesItem.getNondestructiveTestsSpecs();
	}

	// selectedSalesItem olmadıgı için kulanılıyor, barkoddan scan sonrası için
	public void loadOrderDetails(Integer prmItemId) {

		SalesItemManager siMan = new SalesItemManager();
		selectedSalesItem = siMan.loadObject(SalesItem.class, prmItemId);
		selectedSalesItem.getNondestructiveTestsSpecs();
	}

	/*
	 * GETTER SETTER
	 */

	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	// public List<Pipe> getSelectedPipes() {
	// return selectedPipes;
	// }
	//
	// public void setSelectedPipes(List<Pipe> selectedPipes) {
	// this.selectedPipes = selectedPipes;
	// }

	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	public List<NondestructiveTestsSpec> getNondestructiveTests() {
		return nondestructiveTests;
	}

	public void setNondestructiveTests(
			List<NondestructiveTestsSpec> nondestructiveTests) {
		this.nondestructiveTests = nondestructiveTests;
	}

	public TestTahribatsizOtomatikUtSonucBean getTestTahribatsizOtomatikUtSonucBean() {
		return testTahribatsizOtomatikUtSonucBean;
	}

	public void setTestTahribatsizOtomatikUtSonucBean(
			TestTahribatsizOtomatikUtSonucBean testTahribatsizOtomatikUtSonucBean) {
		this.testTahribatsizOtomatikUtSonucBean = testTahribatsizOtomatikUtSonucBean;
	}

	public TestTahribatsizFloroskopikSonucBean getTestTahribatsizFloroskopikSonucBean() {
		return testTahribatsizFloroskopikSonucBean;
	}

	public void setTestTahribatsizFloroskopikSonucBean(
			TestTahribatsizFloroskopikSonucBean testTahribatsizFloroskopikSonucBean) {
		this.testTahribatsizFloroskopikSonucBean = testTahribatsizFloroskopikSonucBean;
	}

	public TestRendersBean getTestRendersBean() {
		return testRendersBean;
	}

	public void setTestRendersBean(TestRendersBean testRendersBean) {
		this.testRendersBean = testRendersBean;
	}

	public NondestructiveTestsSpec getSelectedNonTest() {
		return selectedNonTest;
	}

	public void setSelectedNonTest(NondestructiveTestsSpec selectedNonTest) {
		this.selectedNonTest = selectedNonTest;
	}

	public TestTahribatsizManuelUtSonucBean getTestTahribatsizManuelUtSonucBean() {
		return testTahribatsizManuelUtSonucBean;
	}

	public void setTestTahribatsizManuelUtSonucBean(
			TestTahribatsizManuelUtSonucBean testTahribatsizManuelUtSonucBean) {
		this.testTahribatsizManuelUtSonucBean = testTahribatsizManuelUtSonucBean;
	}

	public SalesItem getSelectedSalesItem() {
		return selectedSalesItem;
	}

	public void setSelectedSalesItem(SalesItem selectedSalesItem) {
		this.selectedSalesItem = selectedSalesItem;
	}

	/**
	 * @return the lazyModel
	 */
	public LazyDataModel<Pipe> getLazyModel() {
		return lazyModel;
	}

	/**
	 * @param lazyModel
	 *            the lazyModel to set
	 */
	public void setLazyModel(LazyDataModel<Pipe> lazyModel) {
		this.lazyModel = lazyModel;
	}

	/**
	 * @return the barkodNo
	 */
	public String getBarkodNo() {
		return barkodNo;
	}

	/**
	 * @param barkodNo
	 *            the barkodNo to set
	 */
	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	/**
	 * @return the selectedBarcodePipe
	 */
	public Pipe getSelectedBarcodePipe() {
		return selectedBarcodePipe;
	}

	/**
	 * @param selectedBarcodePipe
	 *            the selectedBarcodePipe to set
	 */
	public void setSelectedBarcodePipe(Pipe selectedBarcodePipe) {
		this.selectedBarcodePipe = selectedBarcodePipe;
	}
}
