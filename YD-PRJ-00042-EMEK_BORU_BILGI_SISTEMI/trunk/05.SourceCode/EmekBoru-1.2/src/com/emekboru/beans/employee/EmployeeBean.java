package com.emekboru.beans.employee;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.persistence.EntityManager;

import org.apache.commons.io.IOUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Department;
import com.emekboru.jpa.Employee;
import com.emekboru.jpaman.DepartmentManager;
import com.emekboru.jpaman.EmployeeManager;
import com.emekboru.jpaman.Factory;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "employeeBean")
@ViewScoped
public class EmployeeBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ConfigBean config;

	private Employee newEmployee;
	private Employee currentEmployee;
	private UploadedFile employeeFile;
	private File file;
	private List<Department> departmentList;
	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private String userPicturePath;

	private String path;
	private String privatePath;

	private File theFile = null;

	public EmployeeBean() {

		newEmployee = new Employee();
		currentEmployee = new Employee();
		departmentList = new ArrayList<Department>();
		DepartmentManager departmentManager = new DepartmentManager();
		departmentList = departmentManager.findAll(Department.class);
		userPicturePath = null;
	}

	@PostConstruct
	public void load() {
		System.out.println("EmployeeBean.load()-BAŞLADI");

		try {
			this.setPath(config.getConfig().getEmployee().getFolder()
					.getAbsolutePath());

			System.out.println("Path for Upload File (Picture):			" + path);
		} catch (Exception ex) {
			System.out.println("Path for Upload File (Picture):			"
					+ FacesContext.getCurrentInstance().getExternalContext()
							.getRealPath(privatePath));

			System.out.print(path);
			System.out.println("CAUSE OF " + ex.toString());
		}

		theFile = new File(path);
		if (!theFile.isDirectory()) {
			theFile.mkdirs();
		}

		System.out.println("EmployeeBean.load()-BİTTİ");
	}

	public void activateDeactivateUser() {

		if (currentEmployee == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		if (currentEmployee.getActive()) {
			currentEmployee.setActive(false);
		} else {
			currentEmployee.setActive(true);
		}

		EmployeeManager manager = new EmployeeManager();
		manager.updateEntity(currentEmployee);
		FacesContextUtils.addErrorMessage("FormUpdateMessage");
	}

	public void deleteEmployee() {

		if (currentEmployee == null || currentEmployee.getEmployeeId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		EntityManager empManager = Factory.getInstance().createEntityManager();
		currentEmployee = empManager.find(Employee.class,
				currentEmployee.getEmployeeId());

		currentEmployee.setActive(false);
		EmployeeManager employeeManager = new EmployeeManager();
		employeeManager.updateEntity(currentEmployee);

		empManager.getTransaction().begin();
		if (currentEmployee.getEmployeeEventParticipations().size() > 0
				|| currentEmployee.getLicenseRenewResponsibles().size() > 0
				|| currentEmployee.getOfferRelatedWiths().size() > 0
				|| currentEmployee.getUsers().size() > 0) {

			EmployeeListBean.loadEmployeeList();
			FacesContextUtils.addErrorMessage("EmployeeBusyMessage");
			return;
		}

		String empImg = path;

		String imageFullPath = empImg.concat(currentEmployee.getFirstname()
				+ "_" + currentEmployee.getLastname() + "_"
				+ currentEmployee.getEmployeeId() + ".JPG");

		File file = new File(imageFullPath);
		if (file.exists())
			file.delete();
		empManager.remove(currentEmployee);
		empManager.getTransaction().commit();
		empManager.close();
		EmployeeListBean.removeEmployeeFromList(currentEmployee);
		currentEmployee = new Employee();
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	public void updateEmployee() {
		EntityManager manager = Factory.getInstance().createEntityManager();
		Employee employeeFromDb = manager.find(Employee.class,
				currentEmployee.getEmployeeId());
		manager.close();
		// update the image file if it exists (first we have to check if the
		// file with existing EMPLOYEE name and lastname exist,
		// if yes we change the name)
		String imageNameBefore = employeeFromDb.getFirstname() + "_"
				+ employeeFromDb.getLastname() + "_"
				+ employeeFromDb.getEmployeeId() + ".JPG";
		String imageNameAfter = currentEmployee.getFirstname() + "_"
				+ currentEmployee.getLastname() + "_"
				+ currentEmployee.getEmployeeId() + ".JPG";

		String folder = path;

		System.out.println("updatign employee IMAGE PATH: " + folder
				+ imageNameBefore);

		File file = new File(folder + imageNameBefore);
		if (file.exists())
			file.renameTo(new File(folder + imageNameAfter));

		EmployeeManager employeeManager = new EmployeeManager();
		employeeManager.updateEntity(currentEmployee);
		EmployeeListBean.updateOneEmployeeFromList(currentEmployee);
		FacesContextUtils.addInfoMessage("UpdateItemMessage");
	}

	public void addEmployee(ActionEvent event) throws IOException {

		EmployeeManager empManager = new EmployeeManager();
		newEmployee.setSupervisor(empManager.loadObject(Employee.class, 1001));
		empManager.enterNew(newEmployee);

		EntityManager manager = Factory.getInstance().createEntityManager();
		newEmployee = manager.find(Employee.class, newEmployee.getEmployeeId());
		newEmployee.setPhotoPath(newEmployee.getFirstname() + "_"
				+ newEmployee.getLastname() + "_" + newEmployee.getEmployeeId()
				+ ".JPG");
		empManager.updateEntity(newEmployee);
		manager.refresh(newEmployee);
		manager.close();
		EmployeeListBean.addEmployeeToList(newEmployee);
		FacesContextUtils.addInfoMessage("SubmitMessage");
		if (employeeFile != null && employeeFile.getInputstream() != null)
			writeUploadedPicture(newEmployee);

		newEmployee = new Employee();
		return;
	}

	public void cancelEmployeeSubmittion(ActionEvent event) {
		newEmployee = new Employee();
	}

	public void uploadPicture(FileUploadEvent event) {

		employeeFile = event.getFile();
		FacesContextUtils.addInfoMessage("FileUploadedMessage");
	}

	public void updateEmployeePicture(FileUploadEvent event) {

		employeeFile = event.getFile();

		String empImageFolderPath = path;

		String imageFullPath = empImageFolderPath.concat(currentEmployee
				.getFirstname()
				+ "_"
				+ currentEmployee.getLastname()
				+ "_"
				+ currentEmployee.getEmployeeId() + ".JPG");

		File file = new File(imageFullPath);
		if (file.exists()) {

			EmployeeManager employeeManager = new EmployeeManager();
			employeeManager.updateEntity(currentEmployee);
			return;
		}
		file.delete();
		writeUploadedPicture(currentEmployee);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"Personel Resmi Başarıyla Eklenmiştir!"));
	}

	// write a picture in the PICTURE directory and return the path(null if no
	// writing)
	public String writeUploadedPicture(Employee employee) {

		File file = null;
		File tempfile = null;
		OutputStream stream = null;
		OutputStream tempstream = null;
		try {

			String folder = path;

			String fileName = folder + employee.getFirstname() + "_"
					+ employee.getLastname() + "_" + employee.getEmployeeId()
					+ ".JPG";

			String tempfileName = folder + employee.getFirstname() + "_"
					+ employee.getLastname() + "_" + employee.getEmployeeId()
					+ ".JPG";

			file = new File(fileName);
			tempfile = new File(tempfileName);
			stream = new FileOutputStream(file);
			tempstream = new FileOutputStream(tempfile);
			IOUtils.copy(employeeFile.getInputstream(), stream);
			IOUtils.copy(employeeFile.getInputstream(), tempstream);
			stream.close();
			tempstream.close();
			return getWebFilePath(fileName);
		} catch (IOException e) {
			file.delete();
			FacesContextUtils.addErrorMessage("FileNotFoundMessage");
			return null;
		}
	}

	public String getWebFilePath(String originalPath) {

		String folder = path;

		Integer startIndex = originalPath.lastIndexOf("\\");
		String finalPath = originalPath.substring(startIndex + 1);
		finalPath = folder + finalPath;
		return finalPath;
	}

	/*
	 * GETTERS AND SETTERS
	 */

	public Employee getNewEmployee() {
		return newEmployee;
	}

	public void setNewEmployee(Employee newEmployee) {
		this.newEmployee = newEmployee;
	}

	public Employee getCurrentEmployee() {
		return currentEmployee;
	}

	public void setCurrentEmployee(Employee currentEmployee) {
		this.currentEmployee = currentEmployee;
		if (currentEmployee != null) {
			userPicturePath = currentEmployee.getFirstname() + "_"
					+ currentEmployee.getLastname() + "_"
					+ currentEmployee.getEmployeeId();
		}

	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public UploadedFile getEmployeeFile() {
		return employeeFile;
	}

	public void setEmployeeFile(UploadedFile employeeFile) {
		this.employeeFile = employeeFile;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public List<Department> getDepartmentList() {
		return departmentList;
	}

	public void setDepartmentList(List<Department> departmentList) {
		this.departmentList = departmentList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public String getUserPicturePath() {
		return userPicturePath;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}
