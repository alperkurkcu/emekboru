/**
 * 
 */
package com.emekboru.beans.machine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.Machine;
import com.emekboru.jpa.MachineType;
import com.emekboru.jpa.machines.MachineDuruslarAriza;
import com.emekboru.jpaman.MachineManager;
import com.emekboru.jpaman.MachineTypeManager;
import com.emekboru.jpaman.machines.MachineDuruslarArizaManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "machineDuruslarArizaBean")
@ViewScoped
public class MachineDuruslarArizaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<MachineDuruslarAriza> allMachineDuruslarArizaList = new ArrayList<MachineDuruslarAriza>();
	private MachineDuruslarAriza machineDuruslarArizaForm = new MachineDuruslarAriza();
	private MachineDuruslarAriza machineDuruslarArizaAramaForm = new MachineDuruslarAriza();

	private List<MachineDuruslarAriza> arizaList;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	private Integer prmMachineTypeId = null;

	private List<Machine> allMachineList;
	private List<Machine> allKaplamaMachineList;
	private List<Machine> allUretimMachineList;
	private List<Machine> allTamirMachineList;

	public MachineDuruslarArizaBean() {

		allMachineList = new ArrayList<Machine>(0);
		allKaplamaMachineList = new ArrayList<Machine>(0);
		allUretimMachineList = new ArrayList<Machine>(0);
		allTamirMachineList = new ArrayList<Machine>(0);
		loadAllMachines();
		fillTestList();
	}

	public void loadAllMachines() {

		MachineManager machineManager = new MachineManager();
		allMachineList = machineManager.findAllOrdered(Machine.class, "name");
		allKaplamaMachineList = machineManager.findByField(Machine.class,
				"machineType.machineTypeId", 1);
		allUretimMachineList = machineManager.findByField(Machine.class,
				"machineType.machineTypeId", 2);
		allTamirMachineList = machineManager.findByField(Machine.class,
				"machineType.machineTypeId", 3);
	}

	public void yenile() {

		machineDuruslarArizaForm = new MachineDuruslarAriza();
		machineDuruslarArizaAramaForm = new MachineDuruslarAriza();
		allMachineList = new ArrayList<Machine>(0);
		allKaplamaMachineList = new ArrayList<Machine>(0);
		allUretimMachineList = new ArrayList<Machine>(0);
		allTamirMachineList = new ArrayList<Machine>(0);
	}

	public void addArizaDuraklama() {

		MachineDuruslarArizaManager arizaManager = new MachineDuruslarArizaManager();
		MachineManager machineMan = new MachineManager();
		MachineTypeManager typeMan = new MachineTypeManager();

		if (machineDuruslarArizaForm.getId() == null) {

			try {

				machineDuruslarArizaForm.setEklemeZamani(UtilInsCore
						.getTarihZaman());
				machineDuruslarArizaForm.setEkleyenKullanici(userBean.getUser()
						.getId());
				machineDuruslarArizaForm.setMachine(machineMan.loadObject(
						Machine.class, machineDuruslarArizaForm.getMachine()
								.getMachineId()));
				machineDuruslarArizaForm.setMachineType(typeMan.loadObject(
						MachineType.class, machineDuruslarArizaForm
								.getMachine().getMachineType()
								.getMachineTypeId()));
				arizaManager.enterNew(machineDuruslarArizaForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"ARIZA - DURAKLAMA BİLGİSİ BAŞARIYLA EKLENMİŞTİR!"));
				machineDuruslarArizaForm = new MachineDuruslarAriza();
			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"ARIZA - DURAKLAMA BİLGİSİ EKLENME HATASI!", null));
				System.out
						.println("-HATA-MachineDuruslarArizaBean.addArizaDuraklama-enterNew");
				machineDuruslarArizaForm = new MachineDuruslarAriza();
			}
		} else {

			try {

				machineDuruslarArizaForm.setGuncellemeZamani(UtilInsCore
						.getTarihZaman());
				machineDuruslarArizaForm.setGuncelleyenKullanici(userBean
						.getUser().getId());
				machineDuruslarArizaForm.setMachine(machineMan.loadObject(
						Machine.class, machineDuruslarArizaForm.getMachine()
								.getMachineId()));
				machineDuruslarArizaForm.setMachineType(typeMan.loadObject(
						MachineType.class, machineDuruslarArizaForm
								.getMachine().getMachineType()
								.getMachineTypeId()));
				arizaManager.updateEntity(machineDuruslarArizaForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"ARIZA - DURAKLAMA BİLGİSİ BAŞARIYLA GÜNCELLENMİŞTİR!"));
				machineDuruslarArizaForm = new MachineDuruslarAriza();
			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"ARIZA - DURAKLAMA BİLGİSİ GÜNCELLEME HATASI!", null));
				System.out
						.println("-HATA-MachineDuruslarArizaBean.addArizaDuraklama-updateEntity");
				machineDuruslarArizaForm = new MachineDuruslarAriza();
			}

		}

		fillTestList();
	}

	@SuppressWarnings("static-access")
	public void fillTestList() {

		machineDuruslarArizaForm = new MachineDuruslarAriza();
		MachineDuruslarArizaManager arizaManager = new MachineDuruslarArizaManager();
		allMachineDuruslarArizaList = arizaManager.getAllMachineDuruslarAriza();
	}

	public void hidrostatikListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void deleteAriza() {

		MachineDuruslarArizaManager arizaManager = new MachineDuruslarArizaManager();

		if (machineDuruslarArizaForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		arizaManager.delete(machineDuruslarArizaForm);
		machineDuruslarArizaForm = new MachineDuruslarAriza();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"ARIZA - DURAKLAMA BİLGİSİ BAŞARIYLA SİLİNMİŞTİR!"));

		fillTestList();
	}

	public void sorgula() {

	}

	/**
	 * @return the allMachineDuruslarArizaList
	 */
	public List<MachineDuruslarAriza> getAllMachineDuruslarArizaList() {
		return allMachineDuruslarArizaList;
	}

	/**
	 * @param allMachineDuruslarArizaList
	 *            the allMachineDuruslarArizaList to set
	 */
	public void setAllMachineDuruslarArizaList(
			List<MachineDuruslarAriza> allMachineDuruslarArizaList) {
		this.allMachineDuruslarArizaList = allMachineDuruslarArizaList;
	}

	/**
	 * @return the machineDuruslarArizaForm
	 */
	public MachineDuruslarAriza getMachineDuruslarArizaForm() {
		return machineDuruslarArizaForm;
	}

	/**
	 * @param machineDuruslarArizaForm
	 *            the machineDuruslarArizaForm to set
	 */
	public void setMachineDuruslarArizaForm(
			MachineDuruslarAriza machineDuruslarArizaForm) {
		this.machineDuruslarArizaForm = machineDuruslarArizaForm;
	}

	/**
	 * @return the arizaList
	 */
	public List<MachineDuruslarAriza> getArizaList() {
		return arizaList;
	}

	/**
	 * @param arizaList
	 *            the arizaList to set
	 */
	public void setArizaList(List<MachineDuruslarAriza> arizaList) {
		this.arizaList = arizaList;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the allMachineList
	 */
	public List<Machine> getAllMachineList() {
		return allMachineList;
	}

	/**
	 * @return the machineDuruslarArizaAramaForm
	 */
	public MachineDuruslarAriza getMachineDuruslarArizaAramaForm() {
		return machineDuruslarArizaAramaForm;
	}

	/**
	 * @param machineDuruslarArizaAramaForm
	 *            the machineDuruslarArizaAramaForm to set
	 */
	public void setMachineDuruslarArizaAramaForm(
			MachineDuruslarAriza machineDuruslarArizaAramaForm) {
		this.machineDuruslarArizaAramaForm = machineDuruslarArizaAramaForm;
	}

	/**
	 * @return the allKaplamaMachineList
	 */
	public List<Machine> getAllKaplamaMachineList() {
		return allKaplamaMachineList;
	}

	/**
	 * @param allKaplamaMachineList
	 *            the allKaplamaMachineList to set
	 */
	public void setAllKaplamaMachineList(List<Machine> allKaplamaMachineList) {
		this.allKaplamaMachineList = allKaplamaMachineList;
	}

	/**
	 * @return the allUretimMachineList
	 */
	public List<Machine> getAllUretimMachineList() {
		return allUretimMachineList;
	}

	/**
	 * @param allUretimMachineList
	 *            the allUretimMachineList to set
	 */
	public void setAllUretimMachineList(List<Machine> allUretimMachineList) {
		this.allUretimMachineList = allUretimMachineList;
	}

	/**
	 * @return the allTamirMachineList
	 */
	public List<Machine> getAllTamirMachineList() {
		return allTamirMachineList;
	}

	/**
	 * @param allTamirMachineList
	 *            the allTamirMachineList to set
	 */
	public void setAllTamirMachineList(List<Machine> allTamirMachineList) {
		this.allTamirMachineList = allTamirMachineList;
	}

	/**
	 * @param allMachineList
	 *            the allMachineList to set
	 */
	public void setAllMachineList(List<Machine> allMachineList) {
		this.allMachineList = allMachineList;
	}

}
