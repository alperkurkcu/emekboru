/**
 * 
 */
package com.emekboru.beans.personel;

import java.io.File;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.StreamedContent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.Employee;
import com.emekboru.jpa.common.CommonBloodGroup;
import com.emekboru.jpa.common.CommonEducationalStatus;
import com.emekboru.jpa.common.CommonHealthStatus;
import com.emekboru.jpa.common.CommonPaymentType;
import com.emekboru.jpa.employee.EmployeeActivity;
import com.emekboru.jpa.employee.EmployeeActivityParticipation;
import com.emekboru.jpa.employee.EmployeeAddress;
import com.emekboru.jpa.employee.EmployeeBankAccount;
import com.emekboru.jpa.employee.EmployeeDiscipline;
import com.emekboru.jpa.employee.EmployeeEducation;
import com.emekboru.jpa.employee.EmployeeEmail;
import com.emekboru.jpa.employee.EmployeeHealth;
import com.emekboru.jpa.employee.EmployeeIdentity;
import com.emekboru.jpa.employee.EmployeePayment;
import com.emekboru.jpa.employee.EmployeePerformance;
import com.emekboru.jpa.employee.EmployeePhoneNumber;
import com.emekboru.jpa.employee.EmployeePreviousExperience;
import com.emekboru.jpa.employee.EmployeeVocationalFile;
import com.emekboru.jpa.personel.PersonelAktiviteBilgileri;
import com.emekboru.jpa.personel.PersonelCalismaBilgileri;
import com.emekboru.jpa.personel.PersonelDisiplinBilgileri;
import com.emekboru.jpa.personel.PersonelEgitimBilgileri;
import com.emekboru.jpa.personel.PersonelOrtakOzurOrani;
import com.emekboru.jpa.personel.PersonelOrtakUcretTuru;
import com.emekboru.jpa.personel.PersonelPerformansBilgileri;
import com.emekboru.jpa.personel.PersonelSaglikBilgileri;
import com.emekboru.jpa.personel.PersonelSirketDisiDeneyimBilgileri;
import com.emekboru.jpa.personel.PersonelUcretBilgileri;
import com.emekboru.jpaman.CommonQueries;
import com.emekboru.jpaman.EmployeeManager;
import com.emekboru.jpaman.employee.EmployeeAddressManager;
import com.emekboru.jpaman.employee.EmployeeBankAccountManager;
import com.emekboru.jpaman.employee.EmployeeEducationManager;
import com.emekboru.jpaman.employee.EmployeeEmailManager;
import com.emekboru.jpaman.employee.EmployeePhoneNumberManager;
import com.emekboru.jpaman.employee.EmployeeVocationalFileManager;
import com.emekboru.jpaman.personel.PersonelAktiviteBilgileriManager;
import com.emekboru.jpaman.personel.PersonelDisiplinBilgileriManager;
import com.emekboru.jpaman.personel.PersonelEgitimBilgileriManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.FileOperations;

/**
 * @author kursat
 * 
 */
@ManagedBean(name = "personelKartBean")
@ViewScoped
public class PersonelKartBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<Employee> allEmployees = new ArrayList<Employee>();
	private List<CommonBloodGroup> allCommonBloodGroups = new ArrayList<CommonBloodGroup>();
	private List<CommonHealthStatus> allCommonHealthStatus = new ArrayList<CommonHealthStatus>();
	private List<CommonEducationalStatus> allCommonEducationalStatus = new ArrayList<CommonEducationalStatus>();
	private List<CommonPaymentType> allCommonPaymentTypes = new ArrayList<CommonPaymentType>();
	private List<EmployeeActivity> allEmployeeActivities = new ArrayList<EmployeeActivity>();

	private Employee selectedEmployee = new Employee();
	private Employee editableEmployee = new Employee();
	private EmployeeAddress selectedEmployeeAddress = new EmployeeAddress();
	private EmployeeEmail selectedEmployeeEmail = new EmployeeEmail();
	private EmployeePhoneNumber selectedEmployeePhoneNumber = new EmployeePhoneNumber();
	private EmployeeBankAccount selectedEmployeeBankAccount = new EmployeeBankAccount();
	private EmployeeEducation selectedEmployeeEducation = new EmployeeEducation();
	private EmployeeVocationalFile selectedEmployeeVocationalFile = new EmployeeVocationalFile();
	private EmployeeHealth selectedEmployeeHealth = new EmployeeHealth();
	private EmployeeDiscipline selectedEmployeeDiscipline = new EmployeeDiscipline();
	private EmployeePreviousExperience selectedEmployeePreviousExperience = new EmployeePreviousExperience();
	private EmployeePerformance selectedEmployeePerformance = new EmployeePerformance();
	private EmployeeActivityParticipation selectedEmployeeActivitityParticipation = new EmployeeActivityParticipation();

	private EmployeeAddress newEmployeeAddress = new EmployeeAddress();
	private EmployeePhoneNumber newEmployeePhoneNumber = new EmployeePhoneNumber();
	private EmployeeEmail newEmployeeEmail = new EmployeeEmail();
	private EmployeeBankAccount newEmployeeBankAccount = new EmployeeBankAccount();
	private EmployeeEducation newEmployeeEducation = new EmployeeEducation();
	private EmployeeVocationalFile newEmployeeVocationalFile = new EmployeeVocationalFile();
	private EmployeeHealth newEmployeeHealth = new EmployeeHealth();
	private EmployeeDiscipline newEmployeeDiscipline = new EmployeeDiscipline();
	private EmployeePreviousExperience newEmployeePreviousExperience = new EmployeePreviousExperience();
	private EmployeePerformance newEmployeePerformance = new EmployeePerformance();
	private EmployeeActivityParticipation newEmployeeActivitityParticipation = new EmployeeActivityParticipation();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private EmployeeManager employeeManager = new EmployeeManager();

	private String path;
	private String privateMeslekiBelgePath;
	private String privateAktiviteBelgePath;
	private String privateEmployeeImagePath;
	private String employeeImageDeployPath;

	private String downloadedMeslekiBelgeFileName;
	private String downloadedAktiviteBelgeFileName;

	@SuppressWarnings("unused")
	private StreamedContent downloadedMeslekiBelgeFile;
	@SuppressWarnings("unused")
	private StreamedContent downloadedAktiviteBelgeFile;

	private ConfigBean config = new ConfigBean();
	private int kimlik_id = 0;

	// ---------------------------------

	private PersonelAktiviteBilgileri newPersonelAktiviteBilgisi = new PersonelAktiviteBilgileri();
	private PersonelEgitimBilgileri newpersonelEgitimBilgi = new PersonelEgitimBilgileri();
	private PersonelDisiplinBilgileri newDisiplinBilgileri = new PersonelDisiplinBilgileri();

	private PersonelAktiviteBilgileri selectedAktiviteBilgisi = new PersonelAktiviteBilgileri();
	private PersonelDisiplinBilgileri selectedDisiplinBilgisi = new PersonelDisiplinBilgileri();

	private PersonelSaglikBilgileri kimlikSaglik = new PersonelSaglikBilgileri();
	private PersonelSirketDisiDeneyimBilgileri kimlikDeneyim = new PersonelSirketDisiDeneyimBilgileri();
	private PersonelUcretBilgileri kimlikUcret = new PersonelUcretBilgileri();
	private PersonelPerformansBilgileri kimlikPerformans = new PersonelPerformansBilgileri();
	private PersonelCalismaBilgileri kimlikCalisma = new PersonelCalismaBilgileri();
	private PersonelEgitimBilgileri kimlikEgitim = new PersonelEgitimBilgileri();

	private List<PersonelAktiviteBilgileri> aktiviteBilgileriList = new ArrayList<PersonelAktiviteBilgileri>();
	private List<PersonelDisiplinBilgileri> disiplinBilgileriList = new ArrayList<PersonelDisiplinBilgileri>();

	private List<PersonelOrtakOzurOrani> ozurList = new ArrayList<PersonelOrtakOzurOrani>();
	private List<PersonelOrtakUcretTuru> ucretList = new ArrayList<PersonelOrtakUcretTuru>();

	public PersonelKartBean() {
		fillTestList();
	}

	@PostConstruct
	public void fillTestList() {
		this.setPrivateMeslekiBelgePath(File.separatorChar + "employee"
				+ File.separatorChar + "mesleki_belge" + File.separatorChar);

		this.setPrivateAktiviteBelgePath(File.separatorChar + "employee"
				+ File.separatorChar + "aktivite_belge" + File.separatorChar);

		this.setPrivateEmployeeImagePath(File.separatorChar + "employee"
				+ File.separatorChar + "employeeimages" + File.separatorChar);

		this.setPath(config.getConfig().getFolder().getAbsolutePath());

		selectedEmployee = employeeManager.loadObject(Employee.class,
				this.kimlik_id);

		editableEmployee = employeeManager.loadObject(Employee.class,
				this.kimlik_id);

		if (selectedEmployee != null) {
			selectedEmployee.getEmployeeAddresses().size();
			selectedEmployee.getEmployeeDisciplines().size();
			selectedEmployee.getEmployeeActivitityParticipations().size();
		}

		if (selectedEmployee != null) {
			initializeEmployeeData();
		}

		CommonQueries<CommonEducationalStatus> commonEducationalStatusManager = new CommonQueries<CommonEducationalStatus>();
		this.allCommonEducationalStatus = commonEducationalStatusManager
				.findAll(CommonEducationalStatus.class);

		CommonQueries<CommonBloodGroup> commonBloodGroupManager = new CommonQueries<CommonBloodGroup>();
		allCommonBloodGroups = commonBloodGroupManager
				.findAll(CommonBloodGroup.class);

		CommonQueries<CommonHealthStatus> commonHealthStatusManager = new CommonQueries<CommonHealthStatus>();
		allCommonHealthStatus = commonHealthStatusManager
				.findAll(CommonHealthStatus.class);

		CommonQueries<CommonPaymentType> commonPaymentTypeManager = new CommonQueries<CommonPaymentType>();
		allCommonPaymentTypes = commonPaymentTypeManager
				.findAll(CommonPaymentType.class);

		CommonQueries<EmployeeActivity> employeeActivityManager = new CommonQueries<EmployeeActivity>();
		allEmployeeActivities = employeeActivityManager
				.findAll(EmployeeActivity.class);

		allEmployees = employeeManager.findAll(Employee.class);
	}

	public void editPersonelAdres() {

		EmployeeAddressManager employeeAddressManager = new EmployeeAddressManager();

		selectedEmployeeAddress.setGuncelleyenKullanici(userBean.getUser()
				.getId());
		selectedEmployeeAddress.setGuncellemeZamani(new java.sql.Timestamp(
				new java.util.Date().getTime()));

		employeeAddressManager.updateEntity(selectedEmployeeAddress);

		// FacesContextUtils.addWarnMessage("AddedMessage");

		selectedEmployeeAddress = new EmployeeAddress();

		fillTestList();
	}

	public void editPersonelTelefon() {

		EmployeePhoneNumberManager employeePhoneNumberManager = new EmployeePhoneNumberManager();

		selectedEmployeePhoneNumber.setGuncelleyenKullanici(userBean.getUser()
				.getId());
		selectedEmployeePhoneNumber.setGuncellemeZamani(new java.sql.Timestamp(
				new java.util.Date().getTime()));

		employeePhoneNumberManager.updateEntity(selectedEmployeePhoneNumber);

		FacesContextUtils.addWarnMessage("UpdateItemMessage");

		fillTestList();
	}

	public void editPersonelEmail() {

		EmployeeEmailManager employeeEmailManager = new EmployeeEmailManager();

		selectedEmployeeEmail.setGuncelleyenKullanici(userBean.getUser()
				.getId());
		selectedEmployeeEmail.setGuncellemeZamani(new java.sql.Timestamp(
				new java.util.Date().getTime()));

		employeeEmailManager.updateEntity(selectedEmployeeEmail);

		FacesContextUtils.addWarnMessage("UpdateItemMessage");

		fillTestList();
	}

	public void editEmployeeVocationalFile() {

		EmployeeVocationalFileManager employeeVocationalFileManager = new EmployeeVocationalFileManager();
		employeeVocationalFileManager
				.updateEntity(selectedEmployeeVocationalFile);

		FacesContextUtils.addWarnMessage("UpdateItemMessage");

		fillTestList();
	}

	public void editPersonelAktiviteBilgisi() {

		PersonelAktiviteBilgileriManager personelAktiviteBilgileriManager = new PersonelAktiviteBilgileriManager();

		selectedAktiviteBilgisi.setKullaniciIdGuncelleyen(userBean.getUser()
				.getId());
		selectedAktiviteBilgisi.setSonGuncellenmeTarihi(new java.sql.Timestamp(
				new java.util.Date().getTime()));

		personelAktiviteBilgileriManager.updateEntity(selectedAktiviteBilgisi);

		FacesContextUtils.addWarnMessage("UpdateItemMessage");

		fillTestList();
	}

	public void editPersonelEgitimBilgi() {

		PersonelEgitimBilgileriManager personelEgitimBilgileriManager = new PersonelEgitimBilgileriManager();

		kimlikEgitim.setKullaniciIdGuncelleyen(userBean.getUser().getId());
		kimlikEgitim.setSonGuncellenmeTarihi(new java.sql.Timestamp(
				new java.util.Date().getTime()));

		personelEgitimBilgileriManager.updateEntity(kimlikEgitim);

		FacesContextUtils.addWarnMessage("UpdateItemMessage");

		fillTestList();
	}

	public void editEmployeeBankAccount() {
		EmployeeBankAccountManager employeeBankAccountManager = new EmployeeBankAccountManager();
		employeeBankAccountManager.updateEntity(selectedEmployeeBankAccount);

		this.selectedEmployeeBankAccount = new EmployeeBankAccount();

		FacesContextUtils.addWarnMessage("UpdateItemMessage");

		fillTestList();

	}

	public void editEmployeeEducation() {
		EmployeeEducationManager employeeEducationManager = new EmployeeEducationManager();
		employeeEducationManager.updateEntity(selectedEmployeeEducation);

		this.selectedEmployeeEducation = new EmployeeEducation();

		FacesContextUtils.addWarnMessage("UpdateItemMessage");

		fillTestList();
	}

	public void editEmployeeDiscipline() {
		CommonQueries<EmployeeDiscipline> employeeDisciplineManager = new CommonQueries<EmployeeDiscipline>();
		employeeDisciplineManager.updateEntity(selectedEmployeeDiscipline);

		this.selectedEmployeeDiscipline = new EmployeeDiscipline();

		FacesContextUtils.addWarnMessage("UpdateItemMessage");

		fillTestList();
	}

	public void editEmployeePreviousExperience() {
		CommonQueries<EmployeePreviousExperience> employeePreviousExperienceManager = new CommonQueries<EmployeePreviousExperience>();
		employeePreviousExperienceManager
				.updateEntity(selectedEmployeePreviousExperience);

		this.selectedEmployeePreviousExperience = new EmployeePreviousExperience();

		FacesContextUtils.addWarnMessage("UpdateItemMessage");

		fillTestList();
	}

	public void editEmployeePerformance() {
		CommonQueries<EmployeePerformance> employeePerformanceManager = new CommonQueries<EmployeePerformance>();
		employeePerformanceManager.updateEntity(selectedEmployeePerformance);

		this.selectedEmployeePerformance = new EmployeePerformance();

		FacesContextUtils.addWarnMessage("UpdateItemMessage");

		fillTestList();
	}

	public void editEmployeeActivityParticipation() {

		CommonQueries<EmployeeActivityParticipation> employeeActivityParticipationManager = new CommonQueries<EmployeeActivityParticipation>();
		employeeActivityParticipationManager
				.updateEntity(selectedEmployeeActivitityParticipation);

		this.selectedEmployeeActivitityParticipation = new EmployeeActivityParticipation();

		FacesContextUtils.addWarnMessage("UpdateItemMessage");

		fillTestList();

	}

	public void updateEmployee() {
		employeeManager.updateEntity(selectedEmployee);
		FacesContextUtils.addWarnMessage("UpdateItemMessage");
	}

	public void updateEditableEmployee() {
		employeeManager.updateEntity(editableEmployee);
		FacesContextUtils.addWarnMessage("UpdateItemMessage");
	}

	public void updateEmployeeHealth() {

		selectedEmployee.getEmployeeHealth().setGuncellemeZamani(
				new Timestamp(new Date().getTime()));
		selectedEmployee.getEmployeeHealth().setGuncelleyenKullanici(
				userBean.getUser().getId());
		updateEmployee();

	}

	public void updateEmployeePayment() {

		selectedEmployee.getEmployeePayment().setGuncellemeZamani(
				new Timestamp(new Date().getTime()));
		selectedEmployee.getEmployeePayment().setGuncelleyenKullanici(
				userBean.getUser().getId());
		updateEmployee();
	}

	public void addPersonelAdres() {

		EmployeeAddressManager employeeAddressManager = new EmployeeAddressManager();

		newEmployeeAddress.setEmployee(selectedEmployee);
		newEmployeeAddress.setEkleyenKullanici(userBean.getUser().getId());
		newEmployeeAddress.setEklemeZamani(new java.sql.Timestamp(
				new java.util.Date().getTime()));

		employeeAddressManager.enterNew(newEmployeeAddress);

		// FacesContextUtils.addWarnMessage("AddedMessage");

		this.newEmployeeAddress = new EmployeeAddress();
		fillTestList();
	}

	public void addPersonelTelefon() {

		EmployeePhoneNumberManager employeePhoneNumberManager = new EmployeePhoneNumberManager();

		newEmployeePhoneNumber.setEmployee(selectedEmployee);
		newEmployeePhoneNumber.setEkleyenKullanici(userBean.getUser().getId());
		newEmployeePhoneNumber.setEklemeZamani(new java.sql.Timestamp(
				new java.util.Date().getTime()));

		employeePhoneNumberManager.enterNew(newEmployeePhoneNumber);

		// FacesContextUtils.addWarnMessage("AddedMessage");

		this.newEmployeePhoneNumber = new EmployeePhoneNumber();
		fillTestList();
	}

	public void addPersonelEmail() {

		EmployeeEmailManager employeeEmailManager = new EmployeeEmailManager();

		newEmployeeEmail.setEmployee(selectedEmployee);
		newEmployeeEmail.setEkleyenKullanici(userBean.getUser().getId());
		newEmployeeEmail.setEklemeZamani(new java.sql.Timestamp(
				new java.util.Date().getTime()));

		employeeEmailManager.enterNew(newEmployeeEmail);

		// FacesContextUtils.addWarnMessage("AddedMessage");

		this.newEmployeeEmail = new EmployeeEmail();
		fillTestList();
	}

	public void addEmployeeVocationalFile() {

		EmployeeVocationalFileManager employeeVocationalFileManager = new EmployeeVocationalFileManager();

		newEmployeeVocationalFile.setEmployee(selectedEmployee);
		employeeVocationalFileManager.enterNew(newEmployeeVocationalFile);

		// FacesContextUtils.addWarnMessage("AddedMessage");

		this.newEmployeeVocationalFile = new EmployeeVocationalFile();
		fillTestList();
	}

	public void addPersonelAktiviteBilgisi() {

		PersonelAktiviteBilgileriManager personelAktiviteBilgileriManager = new PersonelAktiviteBilgileriManager();

		newPersonelAktiviteBilgisi.setEklenmeTarihi(new java.sql.Timestamp(
				new java.util.Date().getTime()));
		newPersonelAktiviteBilgisi.setKullaniciIdEkleyen(userBean.getUser()
				.getId());
		newPersonelAktiviteBilgisi
				.setKimlikId(selectedEmployee.getEmployeeId());

		personelAktiviteBilgileriManager.enterNew(newPersonelAktiviteBilgisi);

		// FacesContextUtils.addWarnMessage("AddedMessage");

		this.newPersonelAktiviteBilgisi = new PersonelAktiviteBilgileri();
		fillTestList();
	}

	public void addPersonelEgitimBilgi() {

		PersonelEgitimBilgileriManager personelEgitimBilgileriManager = new PersonelEgitimBilgileriManager();

		newpersonelEgitimBilgi.setEklenmeTarihi(new java.sql.Timestamp(
				new java.util.Date().getTime()));
		newpersonelEgitimBilgi
				.setKullaniciIdEkleyen(userBean.getUser().getId());
		newpersonelEgitimBilgi.setKimlikId(selectedEmployee.getEmployeeId());

		personelEgitimBilgileriManager.enterNew(newpersonelEgitimBilgi);

		// FacesContextUtils.addWarnMessage("AddedMessage");

		this.newpersonelEgitimBilgi = new PersonelEgitimBilgileri();
		fillTestList();
	}

	public void addPersonelDisiplinBilgi() {

		PersonelDisiplinBilgileriManager personelDisiplinBilgileriManager = new PersonelDisiplinBilgileriManager();

		newDisiplinBilgileri.setEklenmeTarihi(new java.sql.Timestamp(
				new java.util.Date().getTime()));
		newDisiplinBilgileri.setKullaniciIdEkleyen(userBean.getUser().getId());
		newDisiplinBilgileri.setKimlikId(selectedEmployee.getEmployeeId());

		personelDisiplinBilgileriManager.enterNew(newDisiplinBilgileri);

		// FacesContextUtils.addWarnMessage("AddedMessage");

		this.newDisiplinBilgileri = new PersonelDisiplinBilgileri();
		fillTestList();
	}

	public void addEmployeeBankAccount() {
		EmployeeBankAccountManager employeeBankAccountManager = new EmployeeBankAccountManager();

		newEmployeeBankAccount.setEmployee(selectedEmployee);

		employeeBankAccountManager.enterNew(newEmployeeBankAccount);
		this.newEmployeeBankAccount = new EmployeeBankAccount();

		fillTestList();
	}

	public void addEmployeeEducation() {
		EmployeeEducationManager employeeEducationManager = new EmployeeEducationManager();

		newEmployeeEducation.setEmployee(selectedEmployee);

		employeeEducationManager.enterNew(newEmployeeEducation);
		this.newEmployeeEducation = new EmployeeEducation();

		fillTestList();
	}

	public void addEmployeeDiscipline() {

		CommonQueries<EmployeeDiscipline> employeeDisciplineManager = new CommonQueries<EmployeeDiscipline>();

		newEmployeeDiscipline.setEmployee(selectedEmployee);

		employeeDisciplineManager.enterNew(newEmployeeDiscipline);
		this.newEmployeeEducation = new EmployeeEducation();

		fillTestList();
	}

	public void addEmployeePreviousExperience() {
		CommonQueries<EmployeePreviousExperience> employeePreviousExperienceManager = new CommonQueries<EmployeePreviousExperience>();

		newEmployeePreviousExperience.setEmployee(selectedEmployee);

		employeePreviousExperienceManager
				.enterNew(newEmployeePreviousExperience);
		this.newEmployeePreviousExperience = new EmployeePreviousExperience();

		fillTestList();
	}

	public void addEmployeePerformance() {
		CommonQueries<EmployeePerformance> employeePerformanceManager = new CommonQueries<EmployeePerformance>();

		newEmployeePerformance.setEmployee(selectedEmployee);

		employeePerformanceManager.enterNew(newEmployeePerformance);

		this.newEmployeePerformance = new EmployeePerformance();

		fillTestList();
	}

	public void addEmployeeActivityParticipation() {
		CommonQueries<EmployeeActivityParticipation> employeeActivityParticipationManager = new CommonQueries<EmployeeActivityParticipation>();

		newEmployeeActivitityParticipation.setEmployee(selectedEmployee);

		employeeActivityParticipationManager
				.enterNew(newEmployeeActivitityParticipation);

		this.newEmployeeActivitityParticipation = new EmployeeActivityParticipation();

		fillTestList();

	}

	public void uploadBelgeFile(FileUploadEvent event) {

		FileOperations fileOperations = FileOperations.getInstance();
		String filePath = getPath() + getPrivateMeslekiBelgePath();
		String uploadedFileName = fileOperations.uploadFile(filePath, null,
				event);

		newEmployeeVocationalFile.setFileLocation(uploadedFileName);

	}

	public void uploadAktiviteFile(FileUploadEvent event) {

		FileOperations fileOperations = FileOperations.getInstance();
		String filePath = getPath() + getPrivateAktiviteBelgePath();
		String uploadedFileName = fileOperations.uploadFile(filePath, null,
				event);

		newEmployeeActivitityParticipation
				.setCertificateFileLocation(uploadedFileName);

	}

	public void uploadEmployeePhoto(FileUploadEvent event) {

		FileOperations fileOperations = FileOperations.getInstance();
		String filePath = getPath() + getPrivateEmployeeImagePath();
		String fileName = this.getEditableEmployee().getFirstname() + "_"
				+ this.getEditableEmployee().getLastname() + "_"
				+ this.getEditableEmployee().getEmployeeId();

		String fullFileName = fileName + "."
				+ FilenameUtils.getExtension(event.getFile().getFileName());

		if (fileOperations.exists(filePath, fullFileName)) {
			fileOperations.deleteFile(filePath, fullFileName);
		}

		String photoPath = fileOperations.uploadFile(filePath, fileName, event);

		this.getEditableEmployee().setPhotoPath(photoPath);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"Personel Resmi Başarıyla Eklenmiştir!"));
	}

	public String goKart(int id) {
		return config.getPageUrl().PERSONEL_ORTAKGOREV_PAGE + "&kimlik_id";
	}

	public void deletePersonelAdres() {

		if (this.selectedEmployeeAddress.getIsDefault()) {
			FacesContext context = FacesContext.getCurrentInstance();

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"VARSAYILAN ADRESİ SİLEMEZSİNİZ!", null));
			return;
		}

		EmployeeAddressManager employeeAddressManager = new EmployeeAddressManager();
		employeeAddressManager.deleteByField(EmployeeAddress.class,
				"employeeAddressId",
				this.selectedEmployeeAddress.getEmployeeAddressId());
		FacesContextUtils.addWarnMessage("DeletedMessage");

		selectedEmployeeAddress = new EmployeeAddress();

		fillTestList();
	}

	public void deletePersonelTelefon() {

		if (this.selectedEmployeePhoneNumber.getIsDefault()) {
			FacesContext context = FacesContext.getCurrentInstance();

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"VARSAYILAN TELEFONU SİLEMEZSİNİZ!", null));
			return;
		}

		EmployeePhoneNumberManager employeePhoneNumberManager = new EmployeePhoneNumberManager();
		employeePhoneNumberManager.deleteByField(EmployeePhoneNumber.class,
				"employeePhoneNumberId",
				this.selectedEmployeePhoneNumber.getEmployeePhoneNumberId());
		FacesContextUtils.addWarnMessage("DeletedMessage");

		selectedEmployeePhoneNumber = new EmployeePhoneNumber();

		fillTestList();
	}

	public void deletePersonelEmail() {

		if (this.selectedEmployeeEmail.getIsDefault()) {
			FacesContext context = FacesContext.getCurrentInstance();

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"VARSAYILAN EMAİLİ SİLEMEZSİNİZ!", null));
			return;
		}

		EmployeeEmailManager employeeEmailManager = new EmployeeEmailManager();
		employeeEmailManager.deleteByField(EmployeeEmail.class,
				"employeeEmailId",
				this.selectedEmployeeEmail.getEmployeeEmailId());
		FacesContextUtils.addWarnMessage("DeletedMessage");

		selectedEmployeeEmail = new EmployeeEmail();

		fillTestList();
	}

	public void deleteEmployeeVocationalFile() {
		EmployeeVocationalFileManager employeeVocationalFileManager = new EmployeeVocationalFileManager();
		employeeVocationalFileManager.delete(selectedEmployeeVocationalFile);

		selectedEmployeeVocationalFile = new EmployeeVocationalFile();

		FacesContextUtils.addWarnMessage("DeletedMessage");

		fillTestList();
	}

	public void deletePersonelAktiviteBilgileri() {
		PersonelAktiviteBilgileriManager personelAktiviteBilgileriManager = new PersonelAktiviteBilgileriManager();
		personelAktiviteBilgileriManager.delete(selectedAktiviteBilgisi);

		selectedAktiviteBilgisi = new PersonelAktiviteBilgileri();

		FacesContextUtils.addWarnMessage("DeletedMessage");

		fillTestList();
	}

	public void deletePersonelEgitimBilgi() {
		PersonelEgitimBilgileriManager personelEgitimBilgileriManager = new PersonelEgitimBilgileriManager();
		personelEgitimBilgileriManager.delete(kimlikEgitim);

		// TODO

		FacesContextUtils.addWarnMessage("DeletedMessage");

		fillTestList();
	}

	public void deletePersonelDisiplinBilgi() {
		PersonelDisiplinBilgileriManager personelDisiplinBilgileriManager = new PersonelDisiplinBilgileriManager();
		personelDisiplinBilgileriManager.delete(selectedDisiplinBilgisi);

		selectedDisiplinBilgisi = new PersonelDisiplinBilgileri();

		FacesContextUtils.addWarnMessage("DeletedMessage");

		fillTestList();
	}

	public void deleteEmployeeBankAccount() {

		if (this.selectedEmployeeBankAccount.getIsDefault()) {
			FacesContext context = FacesContext.getCurrentInstance();

			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"VARSAYILAN BANKA HESABINI SİLEMEZSİNİZ!", null));
			return;
		}

		EmployeeBankAccountManager employeeBankAccountManager = new EmployeeBankAccountManager();
		employeeBankAccountManager.delete(selectedEmployeeBankAccount);

		this.selectedEmployeeBankAccount = new EmployeeBankAccount();
		FacesContextUtils.addWarnMessage("DeletedMessage");

		fillTestList();
	}

	public void deleteEmployeeEducation() {
		EmployeeEducationManager employeeEducationManager = new EmployeeEducationManager();
		employeeEducationManager.delete(selectedEmployeeEducation);

		this.selectedEmployeeEducation = new EmployeeEducation();
		FacesContextUtils.addWarnMessage("DeletedMessage");

		fillTestList();
	}

	public void deleteEmployeeDiscipline() {
		selectedEmployee.getEmployeeDisciplines().remove(
				selectedEmployeeDiscipline);

		employeeManager.updateEntity(selectedEmployee);
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	public void deleteEmployeePreviousExperience() {
		selectedEmployee.getEmployeePreviousExperiences().remove(
				selectedEmployeePreviousExperience);

		employeeManager.updateEntity(selectedEmployee);
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	public void deleteEmployeePerformance() {
		selectedEmployee.getEmployeePerformances().remove(
				selectedEmployeePerformance);

		employeeManager.updateEntity(selectedEmployee);
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	public void deleteEmployeeActivityParticipation() {
		selectedEmployee.getEmployeeActivitityParticipations().remove(
				selectedEmployeeActivitityParticipation);

		employeeManager.updateEntity(selectedEmployee);
		FacesContextUtils.addWarnMessage("DeletedMessage");
	}

	private void initializeEmployeeData() {

		Integer ekleyenKullanici = userBean.getUser().getId();
		Timestamp eklemeZamani = new java.sql.Timestamp(
				new java.util.Date().getTime());

		EmployeeEmail employeeEmail = new EmployeeEmail();
		EmployeePhoneNumber employeePhoneNumber = new EmployeePhoneNumber();
		EmployeeAddress employeeAddress = new EmployeeAddress();
		EmployeeHealth employeeHealth = new EmployeeHealth();
		EmployeePayment employeePayment = new EmployeePayment();
		EmployeeIdentity employeeIdentity = new EmployeeIdentity();

		if (selectedEmployee.getEmployeeEmails().size() <= 0) {
			employeeEmail.setEmail("-");
			employeeEmail.setEmployee(selectedEmployee);
			employeeEmail.setIsDefault(true);
			employeeEmail.setEklemeZamani(eklemeZamani);
			employeeEmail.setEkleyenKullanici(ekleyenKullanici);

			selectedEmployee.getEmployeeEmails().add(employeeEmail);
		}

		if (selectedEmployee.getEmployeeAddresses().size() <= 0) {
			employeeAddress.setAddress(selectedEmployee.getAddress());
			employeeAddress.setEmployee(selectedEmployee);
			employeeAddress.setIsDefault(true);
			employeeAddress.setEklemeZamani(eklemeZamani);
			employeeAddress.setEkleyenKullanici(ekleyenKullanici);

			selectedEmployee.getEmployeeAddresses().add(employeeAddress);
		}

		if (selectedEmployee.getEmployeePhoneNumbers().size() <= 0) {
			employeePhoneNumber.setPhoneNumber(selectedEmployee
					.getPhoneNumber());
			employeePhoneNumber.setEmployee(selectedEmployee);
			employeePhoneNumber.setIsDefault(true);
			employeePhoneNumber.setEklemeZamani(eklemeZamani);
			employeePhoneNumber.setEkleyenKullanici(ekleyenKullanici);

			selectedEmployee.getEmployeePhoneNumbers().add(employeePhoneNumber);
		}

		if (selectedEmployee.getEmployeeHealth() == null) {

			CommonQueries<CommonBloodGroup> commonBloodGroupManager = new CommonQueries<CommonBloodGroup>();
			CommonQueries<CommonHealthStatus> commonHealthStatusManager = new CommonQueries<CommonHealthStatus>();

			employeeHealth.setCommonBloodGroup(commonBloodGroupManager
					.findByField(CommonBloodGroup.class, "id", -1).get(0));
			employeeHealth.setCommonHealthStatus(commonHealthStatusManager
					.findByField(CommonHealthStatus.class, "id", -1).get(0));
			employeeHealth.setEmployee(selectedEmployee);
			selectedEmployee.setEmployeeHealth(employeeHealth);
		}

		if (selectedEmployee.getEmployeePayment() == null) {

			CommonQueries<CommonPaymentType> commonPaymentTypeManager = new CommonQueries<CommonPaymentType>();

			employeePayment.setCommonPaymentType(commonPaymentTypeManager
					.findByField(CommonPaymentType.class, "id", -1).get(0));
			employeePayment.setEmployee(selectedEmployee);
			selectedEmployee.setEmployeePayment(employeePayment);
		}

		if (selectedEmployee.getEmployeeIdentity() == null) {

			employeeIdentity.setEklemeZamani(eklemeZamani);
			employeeIdentity.setEkleyenKullanici(ekleyenKullanici);
			employeeIdentity.setEmployee(selectedEmployee);

			selectedEmployee.setEmployeeIdentity(employeeIdentity);
		}

		employeeManager.updateEntity(selectedEmployee);

		selectedEmployee = employeeManager.loadObject(Employee.class,
				this.kimlik_id);
	}

	public List<Employee> autoCompleteEmployee(String query) {
		List<Employee> suggestions = new ArrayList<Employee>();

		for (Employee e : allEmployees) {
			if (e.getFirstname().startsWith(query))
				suggestions.add(e);
		}

		return suggestions;
	}

	public void updateEmployeeDayoff() {

		selectedEmployee.setGuncelleyenKullanici(userBean.getUser().getId());
		updateEmployee();
	}

	/*
	 * GETTERS AND SETTERS START
	 */

	public StreamedContent getDownloadedMeslekiBelgeFile() {

		FileOperations fileOperations = FileOperations.getInstance();
		String filePath = this.getPath() + this.getPrivateMeslekiBelgePath();
		return fileOperations.downloadFile(filePath,
				downloadedMeslekiBelgeFileName);
	}

	public void setDownloadedMeslekiBelgeFile(
			StreamedContent downloadedMeslekiBelgeFile) {
		this.downloadedMeslekiBelgeFile = downloadedMeslekiBelgeFile;
	}

	public StreamedContent getDownloadedAktiviteBelgeFile() {

		FileOperations fileOperations = FileOperations.getInstance();
		String filePath = this.getPath() + this.getPrivateAktiviteBelgePath();
		return fileOperations.downloadFile(filePath,
				downloadedAktiviteBelgeFileName);
	}

	public void setDownloadedAktiviteBelgeFile(
			StreamedContent downloadedAktiviteBelgeFile) {
		this.downloadedAktiviteBelgeFile = downloadedAktiviteBelgeFile;
	}

	public Employee getSelectedEmployee() {
		return selectedEmployee;
	}

	public void setSelectedEmployee(Employee selectedEmployee) {

		this.selectedEmployee = selectedEmployee;
		// this.kimlik_id = this.selectedEmployee.getEmployeeId();
	}

	public EmployeeAddress getSelectedEmployeeAddress() {
		return selectedEmployeeAddress;
	}

	public void setSelectedEmployeeAddress(
			EmployeeAddress selectedEmployeeAddress) {
		this.selectedEmployeeAddress = selectedEmployeeAddress;
	}

	public EmployeeEmail getSelectedEmployeeEmail() {
		return selectedEmployeeEmail;
	}

	public void setSelectedEmployeeEmail(EmployeeEmail selectedEmployeeEmail) {
		this.selectedEmployeeEmail = selectedEmployeeEmail;
	}

	public EmployeePhoneNumber getSelectedEmployeePhoneNumber() {
		return selectedEmployeePhoneNumber;
	}

	public void setSelectedEmployeePhoneNumber(
			EmployeePhoneNumber selectedEmployeePhoneNumber) {
		this.selectedEmployeePhoneNumber = selectedEmployeePhoneNumber;
	}

	public EmployeeAddress getNewEmployeeAddress() {
		return newEmployeeAddress;
	}

	public void setNewEmployeeAddress(EmployeeAddress newEmployeeAddress) {
		this.newEmployeeAddress = newEmployeeAddress;
	}

	public EmployeePhoneNumber getNewEmployeePhoneNumber() {
		return newEmployeePhoneNumber;
	}

	public void setNewEmployeePhoneNumber(
			EmployeePhoneNumber newEmployeePhoneNumber) {
		this.newEmployeePhoneNumber = newEmployeePhoneNumber;
	}

	public EmployeeEmail getNewEmployeeEmail() {
		return newEmployeeEmail;
	}

	public void setNewEmployeeEmail(EmployeeEmail newEmployeeEmail) {
		this.newEmployeeEmail = newEmployeeEmail;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public EmployeeManager getEmployeeManager() {
		return employeeManager;
	}

	public void setEmployeeManager(EmployeeManager employeeManager) {
		this.employeeManager = employeeManager;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getPrivateMeslekiBelgePath() {
		return privateMeslekiBelgePath;
	}

	public void setPrivateMeslekiBelgePath(String privateMeslekiBelgePath) {
		this.privateMeslekiBelgePath = privateMeslekiBelgePath;
	}

	public String getPrivateAktiviteBelgePath() {
		return privateAktiviteBelgePath;
	}

	public void setPrivateAktiviteBelgePath(String privateAktiviteBelgePath) {
		this.privateAktiviteBelgePath = privateAktiviteBelgePath;
	}

	public String getDownloadedMeslekiBelgeFileName() {
		return downloadedMeslekiBelgeFileName;
	}

	public void setDownloadedMeslekiBelgeFileName(
			String downloadedMeslekiBelgeFileName) {
		this.downloadedMeslekiBelgeFileName = downloadedMeslekiBelgeFileName;
	}

	public String getDownloadedAktiviteBelgeFileName() {
		return downloadedAktiviteBelgeFileName;
	}

	public void setDownloadedAktiviteBelgeFileName(
			String downloadedAktiviteBelgeFileName) {
		this.downloadedAktiviteBelgeFileName = downloadedAktiviteBelgeFileName;
	}

	public PersonelAktiviteBilgileri getNewPersonelAktiviteBilgisi() {
		return newPersonelAktiviteBilgisi;
	}

	public void setNewPersonelAktiviteBilgisi(
			PersonelAktiviteBilgileri newPersonelAktiviteBilgisi) {
		this.newPersonelAktiviteBilgisi = newPersonelAktiviteBilgisi;
	}

	public PersonelEgitimBilgileri getNewpersonelEgitimBilgi() {
		return newpersonelEgitimBilgi;
	}

	public void setNewpersonelEgitimBilgi(
			PersonelEgitimBilgileri newpersonelEgitimBilgi) {
		this.newpersonelEgitimBilgi = newpersonelEgitimBilgi;
	}

	public PersonelDisiplinBilgileri getNewDisiplinBilgileri() {
		return newDisiplinBilgileri;
	}

	public void setNewDisiplinBilgileri(
			PersonelDisiplinBilgileri newDisiplinBilgileri) {
		this.newDisiplinBilgileri = newDisiplinBilgileri;
	}

	public PersonelAktiviteBilgileri getSelectedAktiviteBilgisi() {
		return selectedAktiviteBilgisi;
	}

	public void setSelectedAktiviteBilgisi(
			PersonelAktiviteBilgileri selectedAktiviteBilgisi) {
		this.selectedAktiviteBilgisi = selectedAktiviteBilgisi;
	}

	public PersonelDisiplinBilgileri getSelectedDisiplinBilgisi() {
		return selectedDisiplinBilgisi;
	}

	public void setSelectedDisiplinBilgisi(
			PersonelDisiplinBilgileri selectedDisiplinBilgisi) {
		this.selectedDisiplinBilgisi = selectedDisiplinBilgisi;
	}

	public PersonelSaglikBilgileri getKimlikSaglik() {
		return kimlikSaglik;
	}

	public void setKimlikSaglik(PersonelSaglikBilgileri kimlikSaglik) {
		this.kimlikSaglik = kimlikSaglik;
	}

	public PersonelSirketDisiDeneyimBilgileri getKimlikDeneyim() {
		return kimlikDeneyim;
	}

	public void setKimlikDeneyim(
			PersonelSirketDisiDeneyimBilgileri kimlikDeneyim) {
		this.kimlikDeneyim = kimlikDeneyim;
	}

	public PersonelUcretBilgileri getKimlikUcret() {
		return kimlikUcret;
	}

	public void setKimlikUcret(PersonelUcretBilgileri kimlikUcret) {
		this.kimlikUcret = kimlikUcret;
	}

	public PersonelPerformansBilgileri getKimlikPerformans() {
		return kimlikPerformans;
	}

	public void setKimlikPerformans(PersonelPerformansBilgileri kimlikPerformans) {
		this.kimlikPerformans = kimlikPerformans;
	}

	public PersonelCalismaBilgileri getKimlikCalisma() {
		return kimlikCalisma;
	}

	public void setKimlikCalisma(PersonelCalismaBilgileri kimlikCalisma) {
		this.kimlikCalisma = kimlikCalisma;
	}

	public PersonelEgitimBilgileri getKimlikEgitim() {
		return kimlikEgitim;
	}

	public void setKimlikEgitim(PersonelEgitimBilgileri kimlikEgitim) {
		this.kimlikEgitim = kimlikEgitim;
	}

	public List<PersonelAktiviteBilgileri> getAktiviteBilgileriList() {
		return aktiviteBilgileriList;
	}

	public void setAktiviteBilgileriList(
			List<PersonelAktiviteBilgileri> aktiviteBilgileriList) {
		this.aktiviteBilgileriList = aktiviteBilgileriList;
	}

	public List<PersonelDisiplinBilgileri> getDisiplinBilgileriList() {
		return disiplinBilgileriList;
	}

	public void setDisiplinBilgileriList(
			List<PersonelDisiplinBilgileri> disiplinBilgileriList) {
		this.disiplinBilgileriList = disiplinBilgileriList;
	}

	public List<PersonelOrtakOzurOrani> getOzurList() {
		return ozurList;
	}

	public void setOzurList(List<PersonelOrtakOzurOrani> ozurList) {
		this.ozurList = ozurList;
	}

	public List<PersonelOrtakUcretTuru> getUcretList() {
		return ucretList;
	}

	public void setUcretList(List<PersonelOrtakUcretTuru> ucretList) {
		this.ucretList = ucretList;
	}

	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	public int getKimlik_id() {
		return kimlik_id;
	}

	public void setKimlik_id(int kimlik_id) {
		this.kimlik_id = kimlik_id;
		this.selectedEmployee = employeeManager.loadObject(Employee.class,
				this.kimlik_id);

		fillTestList();
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public EmployeeBankAccount getSelectedEmployeeBankAccount() {
		return selectedEmployeeBankAccount;
	}

	public void setSelectedEmployeeBankAccount(
			EmployeeBankAccount selectedEmployeeBankAccount) {
		this.selectedEmployeeBankAccount = selectedEmployeeBankAccount;
	}

	public EmployeeBankAccount getNewEmployeeBankAccount() {
		return newEmployeeBankAccount;
	}

	public void setNewEmployeeBankAccount(
			EmployeeBankAccount newEmployeeBankAccount) {
		this.newEmployeeBankAccount = newEmployeeBankAccount;
	}

	public EmployeeEducation getSelectedEmployeeEducation() {
		return selectedEmployeeEducation;
	}

	public void setSelectedEmployeeEducation(
			EmployeeEducation selectedEmployeeEducation) {
		this.selectedEmployeeEducation = selectedEmployeeEducation;
	}

	public EmployeeEducation getNewEmployeeEducation() {
		return newEmployeeEducation;
	}

	public void setNewEmployeeEducation(EmployeeEducation newEmployeeEducation) {
		this.newEmployeeEducation = newEmployeeEducation;
	}

	public List<CommonEducationalStatus> getAllCommonEducationalStatus() {
		return allCommonEducationalStatus;
	}

	public void setAllCommonEducationalStatus(
			List<CommonEducationalStatus> allCommonEducationalStatus) {
		this.allCommonEducationalStatus = allCommonEducationalStatus;
	}

	public EmployeeVocationalFile getNewEmployeeVocationalFile() {
		return newEmployeeVocationalFile;
	}

	public void setNewEmployeeVocationalFile(
			EmployeeVocationalFile newEmployeeVocationalFile) {
		this.newEmployeeVocationalFile = newEmployeeVocationalFile;
	}

	public EmployeeVocationalFile getSelectedEmployeeVocationalFile() {
		return selectedEmployeeVocationalFile;
	}

	public void setSelectedEmployeeVocationalFile(
			EmployeeVocationalFile selectedEmployeeVocationalFile) {
		this.selectedEmployeeVocationalFile = selectedEmployeeVocationalFile;
	}

	public List<CommonBloodGroup> getAllCommonBloodGroups() {
		return allCommonBloodGroups;
	}

	public void setAllCommonBloodGroups(
			List<CommonBloodGroup> allCommonBloodGroups) {
		this.allCommonBloodGroups = allCommonBloodGroups;
	}

	public List<CommonHealthStatus> getAllCommonHealthStatus() {
		return allCommonHealthStatus;
	}

	public void setAllCommonHealthStatus(
			List<CommonHealthStatus> allCommonHealthStatus) {
		this.allCommonHealthStatus = allCommonHealthStatus;
	}

	public EmployeeHealth getSelectedEmployeeHealth() {
		return selectedEmployeeHealth;
	}

	public void setSelectedEmployeeHealth(EmployeeHealth selectedEmployeeHealth) {
		this.selectedEmployeeHealth = selectedEmployeeHealth;
	}

	public EmployeeHealth getNewEmployeeHealth() {
		return newEmployeeHealth;
	}

	public void setNewEmployeeHealth(EmployeeHealth newEmployeeHealth) {
		this.newEmployeeHealth = newEmployeeHealth;
	}

	public EmployeeDiscipline getSelectedEmployeeDiscipline() {
		return selectedEmployeeDiscipline;
	}

	public void setSelectedEmployeeDiscipline(
			EmployeeDiscipline selectedEmployeeDiscipline) {
		this.selectedEmployeeDiscipline = selectedEmployeeDiscipline;
	}

	public EmployeeDiscipline getNewEmployeeDiscipline() {
		return newEmployeeDiscipline;
	}

	public void setNewEmployeeDiscipline(
			EmployeeDiscipline newEmployeeDiscipline) {
		this.newEmployeeDiscipline = newEmployeeDiscipline;
	}

	public List<CommonPaymentType> getAllCommonPaymentTypes() {
		return allCommonPaymentTypes;
	}

	public void setAllCommonPaymentTypes(
			List<CommonPaymentType> allCommonPaymentTypes) {
		this.allCommonPaymentTypes = allCommonPaymentTypes;
	}

	public List<Employee> getAllEmployees() {
		return allEmployees;
	}

	public void setAllEmployees(List<Employee> allEmployees) {
		this.allEmployees = allEmployees;
	}

	public EmployeePreviousExperience getSelectedEmployeePreviousExperience() {
		return selectedEmployeePreviousExperience;
	}

	public void setSelectedEmployeePreviousExperience(
			EmployeePreviousExperience selectedEmployeePreviousExperience) {
		this.selectedEmployeePreviousExperience = selectedEmployeePreviousExperience;
	}

	public EmployeePreviousExperience getNewEmployeePreviousExperience() {
		return newEmployeePreviousExperience;
	}

	public void setNewEmployeePreviousExperience(
			EmployeePreviousExperience newEmployeePreviousExperience) {
		this.newEmployeePreviousExperience = newEmployeePreviousExperience;
	}

	public EmployeePerformance getSelectedEmployeePerformance() {
		return selectedEmployeePerformance;
	}

	public void setSelectedEmployeePerformance(
			EmployeePerformance selectedEmployeePerformance) {
		this.selectedEmployeePerformance = selectedEmployeePerformance;
	}

	public EmployeePerformance getNewEmployeePerformance() {
		return newEmployeePerformance;
	}

	public void setNewEmployeePerformance(
			EmployeePerformance newEmployeePerformance) {
		this.newEmployeePerformance = newEmployeePerformance;
	}

	public EmployeeActivityParticipation getSelectedEmployeeActivitityParticipation() {
		return selectedEmployeeActivitityParticipation;
	}

	public void setSelectedEmployeeActivitityParticipation(
			EmployeeActivityParticipation selectedEmployeeActivitityParticipation) {
		this.selectedEmployeeActivitityParticipation = selectedEmployeeActivitityParticipation;
	}

	public EmployeeActivityParticipation getNewEmployeeActivitityParticipation() {
		return newEmployeeActivitityParticipation;
	}

	public void setNewEmployeeActivitityParticipation(
			EmployeeActivityParticipation newEmployeeActivitityParticipation) {
		this.newEmployeeActivitityParticipation = newEmployeeActivitityParticipation;
	}

	public List<EmployeeActivity> getAllEmployeeActivities() {
		return allEmployeeActivities;
	}

	public void setAllEmployeeActivities(
			List<EmployeeActivity> allEmployeeActivities) {
		this.allEmployeeActivities = allEmployeeActivities;
	}

	public Employee getEditableEmployee() {
		return editableEmployee;
	}

	public void setEditableEmployee(Employee editableEmployee) {
		this.editableEmployee = editableEmployee;
	}

	public String getPrivateEmployeeImagePath() {
		return privateEmployeeImagePath;
	}

	public void setPrivateEmployeeImagePath(String privateEmployeeImagePath) {
		this.privateEmployeeImagePath = privateEmployeeImagePath;
	}

	public String getEmployeeImageDeployPath() {
		return employeeImageDeployPath;
	}

	public void setEmployeeImageDeployPath(String employeeImageDeployPath) {
		this.employeeImageDeployPath = employeeImageDeployPath;
	}

	/*
	 * GETTERS AND SETTERS END
	 */

}
