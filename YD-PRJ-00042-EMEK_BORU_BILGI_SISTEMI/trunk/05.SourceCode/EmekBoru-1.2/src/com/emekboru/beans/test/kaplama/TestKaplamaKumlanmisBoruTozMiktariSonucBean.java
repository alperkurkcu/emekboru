package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaKumlanmisBoruTozMiktariSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaKumlanmisBoruTozMiktariSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaplamaKumlanmisBoruTozMiktariSonucBean")
@ViewScoped
public class TestKaplamaKumlanmisBoruTozMiktariSonucBean implements
		Serializable {
	private static final long serialVersionUID = 1L;

	private TestKaplamaKumlanmisBoruTozMiktariSonuc testKaplamaKumlanmisBoruTozMiktariSonucForm = new TestKaplamaKumlanmisBoruTozMiktariSonuc();
	private List<TestKaplamaKumlanmisBoruTozMiktariSonuc> allTestKaplamaKumlanmisBoruTozMiktariSonucList = new ArrayList<TestKaplamaKumlanmisBoruTozMiktariSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addOrUpdateTestKaplamaKumlanmisBoruTozMiktariSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaKumlanmisBoruTozMiktariSonucManager manager = new TestKaplamaKumlanmisBoruTozMiktariSonucManager();

		if (testKaplamaKumlanmisBoruTozMiktariSonucForm.getId() == null) {

			testKaplamaKumlanmisBoruTozMiktariSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaKumlanmisBoruTozMiktariSonucForm
					.setEkleyenKullanici(userBean.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaKumlanmisBoruTozMiktariSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaKumlanmisBoruTozMiktariSonucForm
					.setBagliTestId(bagliTest);
			testKaplamaKumlanmisBoruTozMiktariSonucForm
					.setBagliGlobalId(bagliTest);

			manager.enterNew(testKaplamaKumlanmisBoruTozMiktariSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaKumlanmisBoruTozMiktariSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaKumlanmisBoruTozMiktariSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			manager.updateEntity(testKaplamaKumlanmisBoruTozMiktariSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	public void addTestKaplamaKumlanmisBoruTozMiktariSonuc() {

		testKaplamaKumlanmisBoruTozMiktariSonucForm = new TestKaplamaKumlanmisBoruTozMiktariSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaKumlanmisBoruTozMiktariSonucListener(
			SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaKumlanmisBoruTozMiktariSonucList = TestKaplamaKumlanmisBoruTozMiktariSonucManager
				.getAllTestKaplamaKumlanmisBoruTozMiktariSonuc(globalId, pipeId);
		testKaplamaKumlanmisBoruTozMiktariSonucForm = new TestKaplamaKumlanmisBoruTozMiktariSonuc();
	}

	public void deleteTestKaplamaKumlanmisBoruTozMiktariSonuc() {

		bagliTestId = testKaplamaKumlanmisBoruTozMiktariSonucForm
				.getBagliGlobalId().getId();

		if (testKaplamaKumlanmisBoruTozMiktariSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaKumlanmisBoruTozMiktariSonucManager manager = new TestKaplamaKumlanmisBoruTozMiktariSonucManager();
		manager.delete(testKaplamaKumlanmisBoruTozMiktariSonucForm);
		testKaplamaKumlanmisBoruTozMiktariSonucForm = new TestKaplamaKumlanmisBoruTozMiktariSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters
	public TestKaplamaKumlanmisBoruTozMiktariSonuc getTestKaplamaKumlanmisBoruTozMiktariSonucForm() {
		return testKaplamaKumlanmisBoruTozMiktariSonucForm;
	}

	public void setTestKaplamaKumlanmisBoruTozMiktariSonucForm(
			TestKaplamaKumlanmisBoruTozMiktariSonuc testKaplamaKumlanmisBoruTozMiktariSonucForm) {
		this.testKaplamaKumlanmisBoruTozMiktariSonucForm = testKaplamaKumlanmisBoruTozMiktariSonucForm;
	}

	public List<TestKaplamaKumlanmisBoruTozMiktariSonuc> getAllTestKaplamaKumlanmisBoruTozMiktariSonucList() {
		return allTestKaplamaKumlanmisBoruTozMiktariSonucList;
	}

	public void setAllTestKaplamaKumlanmisBoruTozMiktariSonucList(
			List<TestKaplamaKumlanmisBoruTozMiktariSonuc> allTestKaplamaKumlanmisBoruTozMiktariSonucList) {
		this.allTestKaplamaKumlanmisBoruTozMiktariSonucList = allTestKaplamaKumlanmisBoruTozMiktariSonucList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
