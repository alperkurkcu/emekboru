package com.emekboru.beans.edw;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.emekboru.config.ConfigBean;
import com.emekboru.config.Materials.Statuses;
import com.emekboru.jpa.ElectrodeDustWire;
import com.emekboru.jpaman.ElectrodeDustWireManager;
import com.emekboru.jpaman.JpqlComparativeClauses;
import com.emekboru.jpaman.WhereClauseArgs;
import com.emekboru.messages.ElectrodeDustWireType;

@ManagedBean(name = "availDustBean")
@ViewScoped
public class AvailDustBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
	private ConfigBean config;

	public List<ElectrodeDustWire> availableDusts;

	public AvailDustBean() {

		availableDusts = new ArrayList<ElectrodeDustWire>(0);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void loadAvailableDusts() {

		availableDusts.clear();
		ElectrodeDustWireManager man = new ElectrodeDustWireManager();
		ArrayList<WhereClauseArgs> conditionList = new ArrayList<WhereClauseArgs>();

		WhereClauseArgs arg1 = new WhereClauseArgs.Builder()
				.setFieldName("type")
				.setComparativeClause(JpqlComparativeClauses.NQ_EQUAL)
				.setValue(ElectrodeDustWireType.DUST).build();
		WhereClauseArgs arg2 = new WhereClauseArgs.Builder()
				.setFieldName("remainingAmount")
				.setComparativeClause(JpqlComparativeClauses.NQ_GREATER_THEN)
				.setValue(0).build();

		conditionList.add(arg1);
		conditionList.add(arg2);
		List<ElectrodeDustWire> list = man.selectFromWhereQuerie(
				ElectrodeDustWire.class, conditionList);

		for (ElectrodeDustWire e : list) {

			if (isAvailable(e))
				availableDusts.add(e);
		}
	}

	// EDW is available if its attribute Available is true
	// its not depleted and not sold
	protected boolean isAvailable(ElectrodeDustWire edw) {

		Statuses s = edw.getStatus(config.getConfig());

		if (s.isAvailable() && !s.isDepleted() && !s.isSold())
			return true;

		return false;
	}

	// *****************************************************************************//
	// *************************** GETTERS AND SETTERS
	// ****************************//
	// *****************************************************************************//
	public ConfigBean getConfig() {
		return config;
	}

	public void setConfig(ConfigBean config) {
		this.config = config;
	}

	public List<ElectrodeDustWire> getAvailableDusts() {
		return availableDusts;
	}

	public void setAvailableDusts(List<ElectrodeDustWire> availableDust) {
		this.availableDusts = availableDust;
	}

}
