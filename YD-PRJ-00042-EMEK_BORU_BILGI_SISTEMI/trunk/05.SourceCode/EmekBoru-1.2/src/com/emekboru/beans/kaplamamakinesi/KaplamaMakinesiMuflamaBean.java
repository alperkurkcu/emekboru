/**
 * 
 */
package com.emekboru.beans.kaplamamakinesi;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.kaplamamakinesi.KaplamaMakinesiMuflama;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.kaplamamakinesi.KaplamaMakinesiMuflamaManager;
import com.emekboru.utils.FacesContextUtils;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "kaplamaMakinesiMuflamaBean")
@ViewScoped
public class KaplamaMakinesiMuflamaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<KaplamaMakinesiMuflama> allKaplamaMakinesiMuflamaList = new ArrayList<KaplamaMakinesiMuflama>();
	private KaplamaMakinesiMuflama kaplamaMakinesiMuflamaForm = new KaplamaMakinesiMuflama();

	private KaplamaMakinesiMuflama newMuflama = new KaplamaMakinesiMuflama();
	private List<KaplamaMakinesiMuflama> muflamaList;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;

	private Pipe selectedPipe = new Pipe();

	private boolean kontrol = false;

	private String barkodNo = null;

	public KaplamaMakinesiMuflamaBean() {

	}

	public void addMuflama(ActionEvent ae) {

		UICommand cmd = (UICommand) ae.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId != null) {
			PipeManager pipeMan = new PipeManager();
			selectedPipe = pipeMan.loadObject(Pipe.class, prmPipeId);
		}

		try {
			KaplamaMakinesiMuflamaManager manager = new KaplamaMakinesiMuflamaManager();

			newMuflama.setEklemeZamani(UtilInsCore.getTarihZaman());
			newMuflama.setEkleyenKullanici(userBean.getUser().getId());
			newMuflama.setPipe(selectedPipe);
			manager.enterNew(newMuflama);
			newMuflama = new KaplamaMakinesiMuflama();

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"MUFLAMA İŞLEMİ BAŞARIYLA EKLENMİŞTİR!"));

		} catch (Exception e) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_FATAL, "MUFLAMA İŞLEMİ EKLENEMEDİ!",
					null));
			System.out.println("KaplamaMakinesiMuflamaBean.addMuflama-HATA");
		}

		fillTestList(prmPipeId);
	}

	public void addMuflamaSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		Boolean prmKontrol = kaplamaMakinesiMuflamaForm.getDurum();
		KaplamaMakinesiMuflamaManager muflamaManager = new KaplamaMakinesiMuflamaManager();

		if (prmKontrol == null) {

			try {

				kaplamaMakinesiMuflamaForm.setDurum(false);
				kaplamaMakinesiMuflamaForm.setKaplamaBaslamaZamani(UtilInsCore
						.getTarihZaman());
				kaplamaMakinesiMuflamaForm.setKaplamaBaslamaKullanici(userBean
						.getUser().getId());
				muflamaManager.updateEntity(kaplamaMakinesiMuflamaForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"MUFLAMA İŞLEMİ BAŞARIYLA BAŞLANMIŞTIR!"));
			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"MUFLAMA İŞLEMİ BAŞLANAMADI!", null));
				System.out
						.println("KaplamaMakinesiMuflamaBean.addMuflamaSonuc-HATA-BAŞLAMA");
			}
		} else if (prmKontrol == false) {

			try {

				kaplamaMakinesiMuflamaForm.setDurum(true);
				kaplamaMakinesiMuflamaForm.setKaplamaBitisZamani(UtilInsCore
						.getTarihZaman());
				kaplamaMakinesiMuflamaForm.setKaplamaBitisKullanici(userBean
						.getUser().getId());
				muflamaManager.updateEntity(kaplamaMakinesiMuflamaForm);

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						"MUFLAMA İŞLEMİ BAŞARIYLA BİTİRİLMİŞTİR!"));

			} catch (Exception ex) {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_FATAL,
						"MUFLAMA İŞLEMİ BİTİRİLEMEDİ!", null));
				System.out
						.println("KaplamaMakinesiMuflamaBean.addMuflamaSonuc-HATA-BİTİŞ"
								+ e.toString());
			}

		}

		fillTestList(prmPipeId);
	}

	@SuppressWarnings("static-access")
	public void fillTestList(Integer prmPipeId) {

		KaplamaMakinesiMuflamaManager muflamaManager = new KaplamaMakinesiMuflamaManager();
		allKaplamaMakinesiMuflamaList = muflamaManager
				.getAllKaplamaMakinesiMuflama(prmPipeId);
	}

	public void kaplamaListener(SelectEvent event) {
		updateButtonRender = true;
	}

	@SuppressWarnings("static-access")
	public void fillTestListFromBarkod(String prmBarkod) {

		// barkodNo ya göre pipe bulma
		PipeManager pipeMan = new PipeManager();
		if (pipeMan.findByBarkodNo(prmBarkod).size() == 0) {

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, prmBarkod
							+ " BARKOD NUMARALI BORU, SİSTEMDE YOKTUR!", null));
			return;
		} else {

			selectedPipe = pipeMan.findByBarkodNo(prmBarkod).get(0);
			KaplamaMakinesiMuflamaManager muflamaManager = new KaplamaMakinesiMuflamaManager();
			allKaplamaMakinesiMuflamaList = muflamaManager
					.getAllKaplamaMakinesiMuflama(selectedPipe.getPipeId());

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(prmBarkod
					+ " BARKOD NUMARALI BORU SEÇİLMİŞTİR!"));
		}
	}

	public void deleteMuflamaSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmPipeId = (Integer) cmd.getChildren().get(0).getAttributes()
				.get("value");

		if (prmPipeId == null) {
			prmPipeId = selectedPipe.getPipeId();
		}

		KaplamaMakinesiMuflamaManager muflamaManager = new KaplamaMakinesiMuflamaManager();

		if (kaplamaMakinesiMuflamaForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		muflamaManager.delete(kaplamaMakinesiMuflamaForm);
		kaplamaMakinesiMuflamaForm = new KaplamaMakinesiMuflama();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(
				"MUFLAMA BAŞARIYLA SİLİNMİŞTİR!"));

		fillTestList(prmPipeId);
	}

	// setters getters

	public List<KaplamaMakinesiMuflama> getAllKaplamaMakinesiMuflamaList() {
		return allKaplamaMakinesiMuflamaList;
	}

	public void setAllKaplamaMakinesiMuflamaList(
			List<KaplamaMakinesiMuflama> allKaplamaMakinesiMuflamaList) {
		this.allKaplamaMakinesiMuflamaList = allKaplamaMakinesiMuflamaList;
	}

	public KaplamaMakinesiMuflama getNewMuflama() {
		return newMuflama;
	}

	public void setNewMuflama(KaplamaMakinesiMuflama newMuflama) {
		this.newMuflama = newMuflama;
	}

	public List<KaplamaMakinesiMuflama> getMuflamaList() {
		return muflamaList;
	}

	public void setMuflamaList(List<KaplamaMakinesiMuflama> muflamaList) {
		this.muflamaList = muflamaList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	public Pipe getSelectedPipe() {
		return selectedPipe;
	}

	public void setSelectedPipe(Pipe selectedPipe) {
		this.selectedPipe = selectedPipe;
	}

	public boolean isKontrol() {
		return kontrol;
	}

	public void setKontrol(boolean kontrol) {
		this.kontrol = kontrol;
	}

	public String getBarkodNo() {
		return barkodNo;
	}

	public void setBarkodNo(String barkodNo) {
		this.barkodNo = barkodNo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public KaplamaMakinesiMuflama getKaplamaMakinesiMuflamaForm() {
		return kaplamaMakinesiMuflamaForm;
	}

	public void setKaplamaMakinesiMuflamaForm(
			KaplamaMakinesiMuflama kaplamaMakinesiMuflamaForm) {
		this.kaplamaMakinesiMuflamaForm = kaplamaMakinesiMuflamaForm;
	}

}
