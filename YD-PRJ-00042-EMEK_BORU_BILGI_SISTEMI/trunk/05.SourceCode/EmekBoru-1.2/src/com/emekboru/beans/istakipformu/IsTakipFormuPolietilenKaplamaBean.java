/**
 * 
 */
package com.emekboru.beans.istakipformu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.jpa.istakipformu.IsTakipFormuPolietilenKaplama;
import com.emekboru.jpa.istakipformu.IsTakipFormuPolietilenKaplamaBitinceIslemler;
import com.emekboru.jpa.istakipformu.IsTakipFormuPolietilenKaplamaOnceIslemler;
import com.emekboru.jpa.istakipformu.IsTakipFormuPolietilenKaplamaSonraIslemler;
import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.istakipformu.IsTakipFormuPolietilenKaplamaBitinceIslemlerManager;
import com.emekboru.jpaman.istakipformu.IsTakipFormuPolietilenKaplamaManager;
import com.emekboru.jpaman.istakipformu.IsTakipFormuPolietilenKaplamaOnceIslemlerManager;
import com.emekboru.jpaman.istakipformu.IsTakipFormuPolietilenKaplamaSonraIslemlerManager;
import com.emekboru.utils.UtilInsCore;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "isTakipFormuPolietilenKaplamaBean")
@ViewScoped
public class IsTakipFormuPolietilenKaplamaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private SalesItem selectedItem = new SalesItem();

	private List<IsTakipFormuPolietilenKaplama> allIsTakipFormuPolietilenKaplamaList = new ArrayList<IsTakipFormuPolietilenKaplama>();
	private IsTakipFormuPolietilenKaplama isTakipFormuPolietilenKaplamaForm = new IsTakipFormuPolietilenKaplama();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private IsTakipFormuPolietilenKaplamaOnceIslemler selectedIsTakipFormuPolietilenKaplamaOnceIslemler = new IsTakipFormuPolietilenKaplamaOnceIslemler();
	private IsTakipFormuPolietilenKaplamaSonraIslemler selectedIsTakipFormuPolietilenKaplamaSonraIslemler = new IsTakipFormuPolietilenKaplamaSonraIslemler();
	private IsTakipFormuPolietilenKaplamaBitinceIslemler selectedIsTakipFormuPolietilenKaplamaBitinceIslemler = new IsTakipFormuPolietilenKaplamaBitinceIslemler();
	private IsTakipFormuPolietilenKaplamaOnceIslemler newIsTakipFormuPolietilenKaplamaOnceIslemler = new IsTakipFormuPolietilenKaplamaOnceIslemler();
	private IsTakipFormuPolietilenKaplamaSonraIslemler newIsTakipFormuPolietilenKaplamaSonraIslemler = new IsTakipFormuPolietilenKaplamaSonraIslemler();
	private IsTakipFormuPolietilenKaplamaBitinceIslemler newIsTakipFormuPolietilenKaplamaBitinceIslemler = new IsTakipFormuPolietilenKaplamaBitinceIslemler();
	private IsTakipFormuPolietilenKaplama selectedIsTakipFormuPolietilenKaplama = new IsTakipFormuPolietilenKaplama();

	private IsTakipFormuPolietilenKaplama newIsTakip = new IsTakipFormuPolietilenKaplama();

	public void addFromIsEmri(SalesItem salesItem) {

		System.out.println("isTakipFormuPolietilenKaplamaBean.addFromIsEmri()");

		try {
			IsTakipFormuPolietilenKaplamaManager manager = new IsTakipFormuPolietilenKaplamaManager();

			newIsTakip.setSalesItem(salesItem);
			newIsTakip.setEklemeZamani(UtilInsCore.getTarihZaman());
			newIsTakip.setEkleyenEmployee(userBean.getUser());

			newIsTakip.getIsTakipFormuPolietilenKaplamaOnceIslemler()
					.setEklemeZamani(UtilInsCore.getTarihZaman());
			newIsTakip.getIsTakipFormuPolietilenKaplamaOnceIslemler()
					.setEkleyenEmployee(userBean.getUser());

			newIsTakip.getIsTakipFormuPolietilenKaplamaSonraIslemler()
					.setEklemeZamani(UtilInsCore.getTarihZaman());
			newIsTakip.getIsTakipFormuPolietilenKaplamaSonraIslemler()
					.setEkleyenEmployee(userBean.getUser());

			newIsTakip.getIsTakipFormuPolietilenKaplamaBitinceIslemler()
					.setEklemeZamani(UtilInsCore.getTarihZaman());
			newIsTakip.getIsTakipFormuPolietilenKaplamaBitinceIslemler()
					.setEkleyenEmployee(userBean.getUser());

			manager.enterNew(newIsTakip);
			newIsTakip = new IsTakipFormuPolietilenKaplama();

			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(
					"POLİETİLEN KAPLAMA İŞ TAKİP FORMU EKLENMİŞTİR!"));

		} catch (Exception e) {
			System.out
					.println("isTakipFormuPolietilenKaplamaBean.addFromIsEmri-HATA");
		}
	}

	public void loadSelectedTakipForm() {

		reset();
		loadSelectedIsTakipFormuPolietilenKaplamaOnceIslemler();
		loadSelectedIsTakipFormuPolietilenKaplamaSonraIslemler();
		loadSelectedIsTakipFormuPolietilenKaplamaBitinceIslemler();
	}

	@SuppressWarnings("static-access")
	public void loadReplySelectedTakipForm() {
		IsTakipFormuPolietilenKaplamaManager takipManager = new IsTakipFormuPolietilenKaplamaManager();
		if (takipManager.getAllIsTakipFormuPolietilenKaplama(
				selectedItem.getItemId()).size() > 0) {
			selectedIsTakipFormuPolietilenKaplama = takipManager
					.getAllIsTakipFormuPolietilenKaplama(
							selectedItem.getItemId()).get(0);
		} else {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"POLİETİLEN KAPLAMA İŞ EMRİ EKLENMEMİŞ, LÜTFEN ÖNCE İŞ EMİRLERİNİ EKLEYİNİZ!",
							null));
			return;
		}
	}

	public void reset() {

		selectedIsTakipFormuPolietilenKaplamaOnceIslemler = new IsTakipFormuPolietilenKaplamaOnceIslemler();
		selectedIsTakipFormuPolietilenKaplamaSonraIslemler = new IsTakipFormuPolietilenKaplamaSonraIslemler();
		selectedIsTakipFormuPolietilenKaplamaBitinceIslemler = new IsTakipFormuPolietilenKaplamaBitinceIslemler();
	}

	public void loadSelectedIsTakipFormuPolietilenKaplamaOnceIslemler() {
		loadReplySelectedTakipForm();
		selectedIsTakipFormuPolietilenKaplamaOnceIslemler = selectedIsTakipFormuPolietilenKaplama
				.getIsTakipFormuPolietilenKaplamaOnceIslemler();
	}

	public void loadSelectedIsTakipFormuPolietilenKaplamaSonraIslemler() {
		loadReplySelectedTakipForm();
		selectedIsTakipFormuPolietilenKaplamaSonraIslemler = selectedIsTakipFormuPolietilenKaplama
				.getIsTakipFormuPolietilenKaplamaSonraIslemler();
	}

	public void loadSelectedIsTakipFormuPolietilenKaplamaBitinceIslemler() {
		loadReplySelectedTakipForm();
		selectedIsTakipFormuPolietilenKaplamaBitinceIslemler = selectedIsTakipFormuPolietilenKaplama
				.getIsTakipFormuPolietilenKaplamaBitinceIslemler();
	}

	public void updateSelectedIsTakipFormuPolietilenKaplamaOnceIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuPolietilenKaplamaOnceIslemlerManager onceManager = new IsTakipFormuPolietilenKaplamaOnceIslemlerManager();

		selectedIsTakipFormuPolietilenKaplamaOnceIslemler
				.setGuncellemeZamani(UtilInsCore.getTarihZaman());
		selectedIsTakipFormuPolietilenKaplamaOnceIslemler
				.setGuncelleyenEmployee(userBean.getUser());
		selectedIsTakipFormuPolietilenKaplamaOnceIslemler
				.setIsTakipFormuPolietilenKaplama(selectedIsTakipFormuPolietilenKaplama);

		onceManager
				.updateEntity(selectedIsTakipFormuPolietilenKaplamaOnceIslemler);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"POLİETİLEN KAPLAMADA İŞE BAŞLAMADAN YAPILACAK İŞLEMLER BAŞARIYLA TANIMLANMIŞTIR!"));

	}

	public void updateSelectedIsTakipFormuPolietilenKaplamaSonraIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuPolietilenKaplamaSonraIslemlerManager sonraManager = new IsTakipFormuPolietilenKaplamaSonraIslemlerManager();

		selectedIsTakipFormuPolietilenKaplamaSonraIslemler
				.setGuncellemeZamani(UtilInsCore.getTarihZaman());
		selectedIsTakipFormuPolietilenKaplamaSonraIslemler
				.setGuncelleyenEmployee(userBean.getUser());

		sonraManager
				.updateEntity(selectedIsTakipFormuPolietilenKaplamaSonraIslemler);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"POLİETİLEN KAPLAMADA AYAR BİTTİKTEN SONRA YAPILACAK İŞLEMLER BAŞARIYLA TANIMLANMIŞTIR!"));

	}

	public void updateSelectedIsTakipFormuPolietilenKaplamaBitinceIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuPolietilenKaplamaBitinceIslemlerManager bitinceManager = new IsTakipFormuPolietilenKaplamaBitinceIslemlerManager();

		selectedIsTakipFormuPolietilenKaplamaBitinceIslemler
				.setGuncellemeZamani(UtilInsCore.getTarihZaman());
		selectedIsTakipFormuPolietilenKaplamaBitinceIslemler
				.setGuncelleyenEmployee(userBean.getUser());

		bitinceManager
				.updateEntity(selectedIsTakipFormuPolietilenKaplamaBitinceIslemler);

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"POLİETİLEN KAPLAMADA ÜRETİM BİTTİKTEN SONRA YAPILACAK İŞLEMLER BAŞARIYLA TANIMLANMIŞTIR!"));

	}

	public void deleteSelectedIsTakipFormuPolietilenKaplamaOnceIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuPolietilenKaplamaOnceIslemlerManager onceManager = new IsTakipFormuPolietilenKaplamaOnceIslemlerManager();

		newIsTakipFormuPolietilenKaplamaOnceIslemler
				.setId(selectedIsTakipFormuPolietilenKaplamaOnceIslemler
						.getId());
		newIsTakipFormuPolietilenKaplamaOnceIslemler
				.setEklemeZamani(selectedIsTakipFormuPolietilenKaplamaOnceIslemler
						.getEklemeZamani());
		newIsTakipFormuPolietilenKaplamaOnceIslemler
				.setEkleyenEmployee(selectedIsTakipFormuPolietilenKaplamaOnceIslemler
						.getEkleyenEmployee());

		selectedIsTakipFormuPolietilenKaplamaOnceIslemler = new IsTakipFormuPolietilenKaplamaOnceIslemler();

		selectedIsTakipFormuPolietilenKaplamaOnceIslemler = newIsTakipFormuPolietilenKaplamaOnceIslemler;

		onceManager
				.updateEntity(selectedIsTakipFormuPolietilenKaplamaOnceIslemler);

		newIsTakipFormuPolietilenKaplamaOnceIslemler = new IsTakipFormuPolietilenKaplamaOnceIslemler();
		selectedIsTakipFormuPolietilenKaplamaOnceIslemler = new IsTakipFormuPolietilenKaplamaOnceIslemler();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"POLİETİLEN KAPLAMADA İŞE BAŞLAMADAN YAPILACAK İŞLEMLER BAŞARIYLA SİLİNMİŞTİR!"));

	}

	public void deleteSelectedIsTakipFormuPolietilenKaplamaSonraIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuPolietilenKaplamaSonraIslemlerManager sonraManager = new IsTakipFormuPolietilenKaplamaSonraIslemlerManager();

		newIsTakipFormuPolietilenKaplamaSonraIslemler
				.setId(selectedIsTakipFormuPolietilenKaplamaSonraIslemler
						.getId());
		newIsTakipFormuPolietilenKaplamaSonraIslemler
				.setEklemeZamani(selectedIsTakipFormuPolietilenKaplamaSonraIslemler
						.getEklemeZamani());
		newIsTakipFormuPolietilenKaplamaSonraIslemler
				.setEkleyenEmployee(selectedIsTakipFormuPolietilenKaplamaSonraIslemler
						.getEkleyenEmployee());

		selectedIsTakipFormuPolietilenKaplamaSonraIslemler = new IsTakipFormuPolietilenKaplamaSonraIslemler();

		selectedIsTakipFormuPolietilenKaplamaSonraIslemler = newIsTakipFormuPolietilenKaplamaSonraIslemler;

		sonraManager
				.updateEntity(selectedIsTakipFormuPolietilenKaplamaSonraIslemler);

		newIsTakipFormuPolietilenKaplamaSonraIslemler = new IsTakipFormuPolietilenKaplamaSonraIslemler();
		selectedIsTakipFormuPolietilenKaplamaSonraIslemler = new IsTakipFormuPolietilenKaplamaSonraIslemler();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"POLİETİLEN KAPLAMADA AYAR BİTTİKTEN SONRA YAPILACAK İŞLEMLER BAŞARIYLA SİLİNMİŞTİR!"));

	}

	public void deleteSelectedIsTakipFormuPolietilenKaplamaBitinceIslemler() {

		loadReplySelectedTakipForm();
		IsTakipFormuPolietilenKaplamaBitinceIslemlerManager bitinceManager = new IsTakipFormuPolietilenKaplamaBitinceIslemlerManager();

		newIsTakipFormuPolietilenKaplamaBitinceIslemler
				.setId(selectedIsTakipFormuPolietilenKaplamaBitinceIslemler
						.getId());
		newIsTakipFormuPolietilenKaplamaBitinceIslemler
				.setEklemeZamani(selectedIsTakipFormuPolietilenKaplamaBitinceIslemler
						.getEklemeZamani());
		newIsTakipFormuPolietilenKaplamaBitinceIslemler
				.setEkleyenEmployee(selectedIsTakipFormuPolietilenKaplamaBitinceIslemler
						.getEkleyenEmployee());

		selectedIsTakipFormuPolietilenKaplamaBitinceIslemler = new IsTakipFormuPolietilenKaplamaBitinceIslemler();

		selectedIsTakipFormuPolietilenKaplamaBitinceIslemler = newIsTakipFormuPolietilenKaplamaBitinceIslemler;

		bitinceManager
				.updateEntity(selectedIsTakipFormuPolietilenKaplamaBitinceIslemler);

		newIsTakipFormuPolietilenKaplamaBitinceIslemler = new IsTakipFormuPolietilenKaplamaBitinceIslemler();
		selectedIsTakipFormuPolietilenKaplamaBitinceIslemler = new IsTakipFormuPolietilenKaplamaBitinceIslemler();

		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(
				null,
				new FacesMessage(
						"POLİETİLEN KAPLAMADA ÜRETİM BİTİNCE YAPILACAK İŞLEMLER BAŞARIYLA SİLİNMİŞTİR!"));

	}

	// setters getters

	public SalesItem getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(SalesItem selectedItem) {
		this.selectedItem = selectedItem;
	}

	public List<IsTakipFormuPolietilenKaplama> getAllIsTakipFormuPolietilenKaplamaList() {
		return allIsTakipFormuPolietilenKaplamaList;
	}

	public void setAllIsTakipFormuPolietilenKaplamaList(
			List<IsTakipFormuPolietilenKaplama> allIsTakipFormuPolietilenKaplamaList) {
		this.allIsTakipFormuPolietilenKaplamaList = allIsTakipFormuPolietilenKaplamaList;
	}

	public IsTakipFormuPolietilenKaplama getIsTakipFormuPolietilenKaplamaForm() {
		return isTakipFormuPolietilenKaplamaForm;
	}

	public void setIsTakipFormuPolietilenKaplamaForm(
			IsTakipFormuPolietilenKaplama isTakipFormuPolietilenKaplamaForm) {
		this.isTakipFormuPolietilenKaplamaForm = isTakipFormuPolietilenKaplamaForm;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public IsTakipFormuPolietilenKaplamaOnceIslemler getSelectedIsTakipFormuPolietilenKaplamaOnceIslemler() {
		return selectedIsTakipFormuPolietilenKaplamaOnceIslemler;
	}

	public void setSelectedIsTakipFormuPolietilenKaplamaOnceIslemler(
			IsTakipFormuPolietilenKaplamaOnceIslemler selectedIsTakipFormuPolietilenKaplamaOnceIslemler) {
		this.selectedIsTakipFormuPolietilenKaplamaOnceIslemler = selectedIsTakipFormuPolietilenKaplamaOnceIslemler;
	}

	public IsTakipFormuPolietilenKaplamaSonraIslemler getSelectedIsTakipFormuPolietilenKaplamaSonraIslemler() {
		return selectedIsTakipFormuPolietilenKaplamaSonraIslemler;
	}

	public void setSelectedIsTakipFormuPolietilenKaplamaSonraIslemler(
			IsTakipFormuPolietilenKaplamaSonraIslemler selectedIsTakipFormuPolietilenKaplamaSonraIslemler) {
		this.selectedIsTakipFormuPolietilenKaplamaSonraIslemler = selectedIsTakipFormuPolietilenKaplamaSonraIslemler;
	}

	public IsTakipFormuPolietilenKaplamaBitinceIslemler getSelectedIsTakipFormuPolietilenKaplamaBitinceIslemler() {
		return selectedIsTakipFormuPolietilenKaplamaBitinceIslemler;
	}

	public void setSelectedIsTakipFormuPolietilenKaplamaBitinceIslemler(
			IsTakipFormuPolietilenKaplamaBitinceIslemler selectedIsTakipFormuPolietilenKaplamaBitinceIslemler) {
		this.selectedIsTakipFormuPolietilenKaplamaBitinceIslemler = selectedIsTakipFormuPolietilenKaplamaBitinceIslemler;
	}

	public IsTakipFormuPolietilenKaplamaOnceIslemler getNewIsTakipFormuPolietilenKaplamaOnceIslemler() {
		return newIsTakipFormuPolietilenKaplamaOnceIslemler;
	}

	public void setNewIsTakipFormuPolietilenKaplamaOnceIslemler(
			IsTakipFormuPolietilenKaplamaOnceIslemler newIsTakipFormuPolietilenKaplamaOnceIslemler) {
		this.newIsTakipFormuPolietilenKaplamaOnceIslemler = newIsTakipFormuPolietilenKaplamaOnceIslemler;
	}

	public IsTakipFormuPolietilenKaplamaSonraIslemler getNewIsTakipFormuPolietilenKaplamaSonraIslemler() {
		return newIsTakipFormuPolietilenKaplamaSonraIslemler;
	}

	public void setNewIsTakipFormuPolietilenKaplamaSonraIslemler(
			IsTakipFormuPolietilenKaplamaSonraIslemler newIsTakipFormuPolietilenKaplamaSonraIslemler) {
		this.newIsTakipFormuPolietilenKaplamaSonraIslemler = newIsTakipFormuPolietilenKaplamaSonraIslemler;
	}

	public IsTakipFormuPolietilenKaplamaBitinceIslemler getNewIsTakipFormuPolietilenKaplamaBitinceIslemler() {
		return newIsTakipFormuPolietilenKaplamaBitinceIslemler;
	}

	public void setNewIsTakipFormuPolietilenKaplamaBitinceIslemler(
			IsTakipFormuPolietilenKaplamaBitinceIslemler newIsTakipFormuPolietilenKaplamaBitinceIslemler) {
		this.newIsTakipFormuPolietilenKaplamaBitinceIslemler = newIsTakipFormuPolietilenKaplamaBitinceIslemler;
	}

	public IsTakipFormuPolietilenKaplama getSelectedIsTakipFormuPolietilenKaplama() {
		return selectedIsTakipFormuPolietilenKaplama;
	}

	public void setSelectedIsTakipFormuPolietilenKaplama(
			IsTakipFormuPolietilenKaplama selectedIsTakipFormuPolietilenKaplama) {
		this.selectedIsTakipFormuPolietilenKaplama = selectedIsTakipFormuPolietilenKaplama;
	}

	public IsTakipFormuPolietilenKaplama getNewIsTakip() {
		return newIsTakip;
	}

	public void setNewIsTakip(IsTakipFormuPolietilenKaplama newIsTakip) {
		this.newIsTakip = newIsTakip;
	}

}
