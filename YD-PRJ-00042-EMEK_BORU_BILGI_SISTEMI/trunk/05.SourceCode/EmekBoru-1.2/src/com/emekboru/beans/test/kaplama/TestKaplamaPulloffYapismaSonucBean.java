/**
 * 
 */
package com.emekboru.beans.test.kaplama;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaPulloffYapismaSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaPulloffYapismaSonucManager;
import com.emekboru.utils.FacesContextUtils;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "testKaplamaPulloffYapismaSonucBean")
@ViewScoped
public class TestKaplamaPulloffYapismaSonucBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<TestKaplamaPulloffYapismaSonuc> allKaplamaPulloffYapismaSonucList = new ArrayList<TestKaplamaPulloffYapismaSonuc>();
	private TestKaplamaPulloffYapismaSonuc testKaplamaPulloffYapismaSonucForm = new TestKaplamaPulloffYapismaSonuc();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;
	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	public void addKaplamaPulloffYapismaSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaPulloffYapismaSonucManager tkdsManager = new TestKaplamaPulloffYapismaSonucManager();

		if (testKaplamaPulloffYapismaSonucForm.getId() == null) {

			testKaplamaPulloffYapismaSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaPulloffYapismaSonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaPulloffYapismaSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaPulloffYapismaSonucForm.setBagliTestId(bagliTest);
			testKaplamaPulloffYapismaSonucForm.setBagliGlobalId(bagliTest);

			tkdsManager.enterNew(testKaplamaPulloffYapismaSonucForm);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaPulloffYapismaSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaPulloffYapismaSonucForm.setGuncelleyenKullanici(userBean
					.getUser().getId());
			tkdsManager.updateEntity(testKaplamaPulloffYapismaSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");

		}
		fillTestList(prmGlobalId, prmPipeId);
	}

	public void addKaplamaPulloffYapisma() {
		testKaplamaPulloffYapismaSonucForm = new TestKaplamaPulloffYapismaSonuc();
		updateButtonRender = false;
	}

	public void kaplamaDarbeListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(int globalId, Integer pipeId2) {
		allKaplamaPulloffYapismaSonucList = TestKaplamaPulloffYapismaSonucManager
				.getAllTestKaplamaPulloffYapismaSonuc(globalId, pipeId2);
		testKaplamaPulloffYapismaSonucForm = new TestKaplamaPulloffYapismaSonuc();
	}

	public void deleteTestKaplamaPulloffYapismaSonuc() {

		bagliTestId = testKaplamaPulloffYapismaSonucForm.getBagliTestId()
				.getId();

		if (testKaplamaPulloffYapismaSonucForm == null
				|| testKaplamaPulloffYapismaSonucForm.getId() <= 0) {
			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}
		TestKaplamaPulloffYapismaSonucManager tkdsManager = new TestKaplamaPulloffYapismaSonucManager();
		tkdsManager.delete(testKaplamaPulloffYapismaSonucForm);
		testKaplamaPulloffYapismaSonucForm = new TestKaplamaPulloffYapismaSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setters gettters

	/**
	 * @return the allKaplamaPulloffYapismaSonucList
	 */
	public List<TestKaplamaPulloffYapismaSonuc> getAllKaplamaPulloffYapismaSonucList() {
		return allKaplamaPulloffYapismaSonucList;
	}

	/**
	 * @param allKaplamaPulloffYapismaSonucList
	 *            the allKaplamaPulloffYapismaSonucList to set
	 */
	public void setAllKaplamaPulloffYapismaSonucList(
			List<TestKaplamaPulloffYapismaSonuc> allKaplamaPulloffYapismaSonucList) {
		this.allKaplamaPulloffYapismaSonucList = allKaplamaPulloffYapismaSonucList;
	}

	/**
	 * @return the testKaplamaPulloffYapismaSonucForm
	 */
	public TestKaplamaPulloffYapismaSonuc getTestKaplamaPulloffYapismaSonucForm() {
		return testKaplamaPulloffYapismaSonucForm;
	}

	/**
	 * @param testKaplamaPulloffYapismaSonucForm
	 *            the testKaplamaPulloffYapismaSonucForm to set
	 */
	public void setTestKaplamaPulloffYapismaSonucForm(
			TestKaplamaPulloffYapismaSonuc testKaplamaPulloffYapismaSonucForm) {
		this.testKaplamaPulloffYapismaSonucForm = testKaplamaPulloffYapismaSonucForm;
	}

	/**
	 * @return the userBean
	 */
	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	/**
	 * @param userBean
	 *            the userBean to set
	 */
	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the updateButtonRender
	 */
	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	/**
	 * @param updateButtonRender
	 *            the updateButtonRender to set
	 */
	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

	/**
	 * @return the bagliTestId
	 */
	public Integer getBagliTestId() {
		return bagliTestId;
	}

	/**
	 * @param bagliTestId
	 *            the bagliTestId to set
	 */
	public void setBagliTestId(Integer bagliTestId) {
		this.bagliTestId = bagliTestId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
