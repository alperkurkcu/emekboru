package com.emekboru.beans.test.kaplama;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UICommand;
import javax.faces.event.ActionEvent;

import org.primefaces.event.SelectEvent;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.beans.order.QualitySpecBean;
import com.emekboru.jpa.Pipe;
import com.emekboru.jpa.isolation.IsolationTestDefinition;
import com.emekboru.jpa.kaplamatest.TestKaplamaBetonEgilmeBasincSonuc;
import com.emekboru.jpaman.PipeManager;
import com.emekboru.jpaman.isolation.IsolationTestDefinitionManager;
import com.emekboru.jpaman.kaplamatest.TestKaplamaBetonEgilmeBasincSonucManager;
import com.emekboru.utils.FacesContextUtils;

@ManagedBean(name = "testKaplamaBetonEgilmeBasincSonucBean")
@ViewScoped
public class TestKaplamaBetonEgilmeBasincSonucBean {

	private TestKaplamaBetonEgilmeBasincSonuc testKaplamaBetonEgilmeBasincSonucForm = new TestKaplamaBetonEgilmeBasincSonuc();
	private List<TestKaplamaBetonEgilmeBasincSonuc> allTestKaplamaBetonEgilmeBasincSonucList = new ArrayList<TestKaplamaBetonEgilmeBasincSonuc>();

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userBean;

	private boolean updateButtonRender;
	private Integer bagliTestId = 0;

	float basinc2GunlukOrtalama = 0F;
	float basinc7GunlukOrtalama = 0F;
	float basinc28GunlukOrtalama = 0F;

	float egilme2GunlukOrtalama = 0F;
	float egilme7GunlukOrtalama = 0F;
	float egilme28GunlukOrtalama = 0F;

	public void addOrUpdateTestKaplamaBetonEgilmeBasincSonuc(ActionEvent e) {

		UICommand cmd = (UICommand) e.getComponent();
		Integer prmGlobalId = (Integer) cmd.getChildren().get(0)
				.getAttributes().get("value");
		Integer prmTestId = (Integer) cmd.getChildren().get(1).getAttributes()
				.get("value");
		Integer prmPipeId = (Integer) cmd.getChildren().get(2).getAttributes()
				.get("value");

		TestKaplamaBetonEgilmeBasincSonucManager tkbebManager = new TestKaplamaBetonEgilmeBasincSonucManager();

		if (testKaplamaBetonEgilmeBasincSonucForm.getId() == null) {

			testKaplamaBetonEgilmeBasincSonucForm
					.setEklemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaBetonEgilmeBasincSonucForm.setEkleyenKullanici(userBean
					.getUser().getId());

			Pipe pipe = new PipeManager().loadObject(Pipe.class, prmPipeId);
			testKaplamaBetonEgilmeBasincSonucForm.setPipe(pipe);

			IsolationTestDefinition bagliTest = new IsolationTestDefinitionManager()
					.loadObject(IsolationTestDefinition.class, prmTestId);

			testKaplamaBetonEgilmeBasincSonucForm.setBagliTestId(bagliTest);
			testKaplamaBetonEgilmeBasincSonucForm.setBagliGlobalId(bagliTest);

			ortalamaBul();

			tkbebManager.enterNew(testKaplamaBetonEgilmeBasincSonucForm);
			// hasSonuc true
			QualitySpecBean kontrol = new QualitySpecBean();
			kontrol.sonucKontrol(bagliTest.getId(), true);
			// testlerin hangi borudan yap�lacag� kontrolu eklenmesi
			PipeManager pipMan = new PipeManager();
			pipMan.testYapilanBoruEkle(prmPipeId, 1, true);
			testKaplamaBetonEgilmeBasincSonucForm = new TestKaplamaBetonEgilmeBasincSonuc();
			FacesContextUtils.addInfoMessage("TestSubmitMessage");
		} else {

			testKaplamaBetonEgilmeBasincSonucForm
					.setGuncellemeZamani(new java.sql.Timestamp(
							new java.util.Date().getTime()));
			testKaplamaBetonEgilmeBasincSonucForm
					.setGuncelleyenKullanici(userBean.getUser().getId());
			ortalamaBul();
			tkbebManager.updateEntity(testKaplamaBetonEgilmeBasincSonucForm);
			FacesContextUtils.addInfoMessage("TestUpdateMessage");
		}

		fillTestList(prmGlobalId, prmPipeId);

	}

	private void ortalamaBul() {

		if (testKaplamaBetonEgilmeBasincSonucForm.getBasinc2Gunluk1() != null) {
			basinc2GunlukOrtalama = (testKaplamaBetonEgilmeBasincSonucForm
					.getBasinc2Gunluk1().floatValue()
					+ testKaplamaBetonEgilmeBasincSonucForm.getBasinc2Gunluk2()
							.floatValue()
					+ testKaplamaBetonEgilmeBasincSonucForm.getBasinc2Gunluk3()
							.floatValue()
					+ testKaplamaBetonEgilmeBasincSonucForm.getBasinc2Gunluk4()
							.floatValue()
					+ testKaplamaBetonEgilmeBasincSonucForm.getBasinc2Gunluk5()
							.floatValue() + testKaplamaBetonEgilmeBasincSonucForm
					.getBasinc2Gunluk6().floatValue()) / 6;
			testKaplamaBetonEgilmeBasincSonucForm
					.setBasinc2GunlukOrtalama(BigDecimal
							.valueOf(basinc2GunlukOrtalama));
		}
		if (testKaplamaBetonEgilmeBasincSonucForm.getBasinc7Gunluk1() != null) {
			basinc7GunlukOrtalama = (testKaplamaBetonEgilmeBasincSonucForm
					.getBasinc7Gunluk1().floatValue()
					+ testKaplamaBetonEgilmeBasincSonucForm.getBasinc7Gunluk2()
							.floatValue()
					+ testKaplamaBetonEgilmeBasincSonucForm.getBasinc7Gunluk3()
							.floatValue()
					+ testKaplamaBetonEgilmeBasincSonucForm.getBasinc7Gunluk4()
							.floatValue()
					+ testKaplamaBetonEgilmeBasincSonucForm.getBasinc7Gunluk5()
							.floatValue() + testKaplamaBetonEgilmeBasincSonucForm
					.getBasinc7Gunluk6().floatValue()) / 6;
			testKaplamaBetonEgilmeBasincSonucForm
					.setBasinc7GunlukOrtalama(BigDecimal
							.valueOf(basinc7GunlukOrtalama));
		}
		if (testKaplamaBetonEgilmeBasincSonucForm.getBasinc28Gunluk1() != null) {
			basinc28GunlukOrtalama = (testKaplamaBetonEgilmeBasincSonucForm
					.getBasinc28Gunluk1().floatValue()
					+ testKaplamaBetonEgilmeBasincSonucForm
							.getBasinc28Gunluk2().floatValue()
					+ testKaplamaBetonEgilmeBasincSonucForm
							.getBasinc28Gunluk3().floatValue()
					+ testKaplamaBetonEgilmeBasincSonucForm
							.getBasinc28Gunluk4().floatValue()
					+ testKaplamaBetonEgilmeBasincSonucForm
							.getBasinc28Gunluk5().floatValue() + testKaplamaBetonEgilmeBasincSonucForm
					.getBasinc28Gunluk5().floatValue()) / 6;
			testKaplamaBetonEgilmeBasincSonucForm
					.setBasinc28GunlukOrtalama(BigDecimal
							.valueOf(basinc28GunlukOrtalama));
		}

		if (testKaplamaBetonEgilmeBasincSonucForm.getEgilme2Gunluk1() != null) {
			egilme2GunlukOrtalama = (testKaplamaBetonEgilmeBasincSonucForm
					.getEgilme2Gunluk1().floatValue()
					+ testKaplamaBetonEgilmeBasincSonucForm.getEgilme2Gunluk2()
							.floatValue() + testKaplamaBetonEgilmeBasincSonucForm
					.getEgilme2Gunluk3().floatValue()) / 3;
			testKaplamaBetonEgilmeBasincSonucForm
					.setEgilme2GunlukOrtalama(BigDecimal
							.valueOf(egilme2GunlukOrtalama));
		}
		if (testKaplamaBetonEgilmeBasincSonucForm.getEgilme7Gunluk1() != null) {
			egilme7GunlukOrtalama = (testKaplamaBetonEgilmeBasincSonucForm
					.getEgilme7Gunluk1().floatValue()
					+ testKaplamaBetonEgilmeBasincSonucForm.getEgilme7Gunluk2()
							.floatValue() + testKaplamaBetonEgilmeBasincSonucForm
					.getEgilme7Gunluk3().floatValue()) / 3;
			testKaplamaBetonEgilmeBasincSonucForm
					.setEgilme7GunlukOrtalama(BigDecimal
							.valueOf(egilme7GunlukOrtalama));
		}
		if (testKaplamaBetonEgilmeBasincSonucForm.getEgilme28Gunluk1() != null) {
			egilme28GunlukOrtalama = (testKaplamaBetonEgilmeBasincSonucForm
					.getEgilme28Gunluk1().floatValue()
					+ testKaplamaBetonEgilmeBasincSonucForm
							.getEgilme28Gunluk2().floatValue() + testKaplamaBetonEgilmeBasincSonucForm
					.getEgilme28Gunluk3().floatValue()) / 3;
			testKaplamaBetonEgilmeBasincSonucForm
					.setEgilme28GunlukOrtalama(BigDecimal
							.valueOf(egilme28GunlukOrtalama));
		}
	}

	public void addTestKaplamaBetonEgilmeBasincSonuc() {

		testKaplamaBetonEgilmeBasincSonucForm = new TestKaplamaBetonEgilmeBasincSonuc();
		updateButtonRender = false;
	}

	public void testKaplamaBetonEgilmeBasincSonucListener(SelectEvent event) {
		updateButtonRender = true;
	}

	public void fillTestList(Integer globalId, Integer pipeId) {
		allTestKaplamaBetonEgilmeBasincSonucList = TestKaplamaBetonEgilmeBasincSonucManager
				.getAllTestKaplamaBetonEgilmeBasincSonuc(globalId, pipeId);
		testKaplamaBetonEgilmeBasincSonucForm = new TestKaplamaBetonEgilmeBasincSonuc();
	}

	public void deleteTestKaplamaBetonEgilmeBasincSonuc() {

		bagliTestId = testKaplamaBetonEgilmeBasincSonucForm.getBagliGlobalId()
				.getId();

		if (testKaplamaBetonEgilmeBasincSonucForm.getId() == null) {

			FacesContextUtils.addErrorMessage("NoSelectMessage");
			return;
		}

		TestKaplamaBetonEgilmeBasincSonucManager tkbebManager = new TestKaplamaBetonEgilmeBasincSonucManager();

		tkbebManager.delete(testKaplamaBetonEgilmeBasincSonucForm);
		testKaplamaBetonEgilmeBasincSonucForm = new TestKaplamaBetonEgilmeBasincSonuc();
		FacesContextUtils.addWarnMessage("DeletedMessage");
		// hasSonuc false
		QualitySpecBean kontrol = new QualitySpecBean();
		kontrol.sonucKontrol(bagliTestId, false);
	}

	// setter getters
	public TestKaplamaBetonEgilmeBasincSonuc getTestKaplamaBetonEgilmeBasincSonucForm() {
		return testKaplamaBetonEgilmeBasincSonucForm;
	}

	public void setTestKaplamaBetonEgilmeBasincSonucForm(
			TestKaplamaBetonEgilmeBasincSonuc testKaplamaBetonEgilmeBasincSonucForm) {
		this.testKaplamaBetonEgilmeBasincSonucForm = testKaplamaBetonEgilmeBasincSonucForm;
	}

	public List<TestKaplamaBetonEgilmeBasincSonuc> getAllTestKaplamaBetonEgilmeBasincSonucList() {
		return allTestKaplamaBetonEgilmeBasincSonucList;
	}

	public void setAllTestKaplamaBetonEgilmeBasincSonucList(
			List<TestKaplamaBetonEgilmeBasincSonuc> allTestKaplamaBetonEgilmeBasincSonucList) {
		this.allTestKaplamaBetonEgilmeBasincSonucList = allTestKaplamaBetonEgilmeBasincSonucList;
	}

	public UserCredentialsBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserCredentialsBean userBean) {
		this.userBean = userBean;
	}

	public boolean isUpdateButtonRender() {
		return updateButtonRender;
	}

	public void setUpdateButtonRender(boolean updateButtonRender) {
		this.updateButtonRender = updateButtonRender;
	}

}
