package com.emekboru.messages;

public class PipeStatus {
	
	public final static Integer FINISHED = 1;
	public final static Integer FREE = 2;
	public final static Integer ON_REPAIR = 3;
	
	public final static Integer ON_SPIRAL_MACHINE = 10;
	public final static Integer ON_HYDROSTATIC_MACHINE = 11;
	public final static Integer ON_BEVELLING_MACHINE = 12;
	public final static Integer ON_PIPE_END_PREPARATION_MACHINE = 13;

	public final static Integer ON_POLIETILEN_MACHINE = 101;
	public final static Integer ON_POLIPROPILEN_MACHINE = 102;
	public final static Integer ON_EPOXY_BOYA_MACHINE = 103;
	public final static Integer ON_SANDBLASTING_MACHINE = 104;
	public final static Integer ON_CEMENT_MACHINE = 105;

	public final static Integer DEPLETED = 200;
}

