package com.emekboru.messages;

public class EntranceTestType {

	public static final Integer  WIRE_PULL_TEST = 1;
	public static final Integer  WIRE_HARDNESS_TEST = 2;
	public static final Integer  WIRE_MEASURE_TEST = 3;
	public static final Integer  ELECTRODE_MEASURE_TEST = 4;
}
