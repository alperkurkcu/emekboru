package com.emekboru.messages;

public class TensileTestType {
	
	public static final Integer TENSILE_TEST = 1;
	public static final Integer WELD_TENSILE_TEST = 2;
	public static final Integer ALL_WELD_TENSILE_TEST = 3;
	public static final Integer STRIP_TENSILE_TEST = 4;
	
}
