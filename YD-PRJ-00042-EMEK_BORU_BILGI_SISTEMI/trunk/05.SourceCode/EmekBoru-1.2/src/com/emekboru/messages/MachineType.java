package com.emekboru.messages;

public class MachineType {

	public final static Integer SPIRAL_MACHINE = 10;
	public final static Integer HYDROSTATIC_MACHINE = 11;
	public final static Integer BEVELLING_MACHINE = 12;
	public final static Integer PIPEENDPREPARATION_MACHINE = 13;

	
	public final static Integer POLIETILEN_MACHINE = 101;
	public final static Integer POLIPROPILEN_MACHINE = 102;
	public final static Integer EPOXYBOYA_MACHINE = 103;
	public final static Integer INNERBLASTING_MACHINE = 104;
	public final static Integer OUTERBLASTING_MACHINE = 105;
	public final static Integer CEMENT_MACHINE = 106;
}
