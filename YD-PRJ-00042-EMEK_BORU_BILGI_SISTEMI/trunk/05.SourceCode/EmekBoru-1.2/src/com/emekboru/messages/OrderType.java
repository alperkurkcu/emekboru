package com.emekboru.messages;

public class OrderType {
	
	public static final Integer OIL = 1;
	public static final Integer GAS = 2;
	public static final Integer WATER = 3;
}
