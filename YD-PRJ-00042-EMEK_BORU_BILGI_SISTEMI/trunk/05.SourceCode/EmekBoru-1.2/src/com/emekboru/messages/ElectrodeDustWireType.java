package com.emekboru.messages;

public class ElectrodeDustWireType {
	
	public static final Integer ELECTRODE = 1;
	public static final Integer DUST = 2;
	public static final Integer WIRE = 3;
}
