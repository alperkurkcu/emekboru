package com.emekboru.messages;

public class RawMaterialStatus {

	public static final Integer SOLD = 1;
	public static final Integer IN_PRODUCTION = 2;
	public static final Integer AVAILABLE_ON_STOCK = 3;
	public static final Integer ALLOCATED = 10;
	public final static Integer DEPLETED = 200;
}
