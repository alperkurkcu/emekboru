package com.emekboru.messages;

public class IsolationType {

	public static final String INTERNAL_ISOLATION = "I";
	public static final String EXTERNAL_ISOLATION = "O";
	public static final Integer INTERNAL_COAT = 1;
	public static final Integer EXTERNAL_COAT = 2;
}
