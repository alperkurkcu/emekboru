package com.emekboru.messages;

public enum CustomerType {

	CUSTOMER (1),
	POTENTIAL_CUSTOMER(2);
	
	private int type;
	
	CustomerType(int type)
	{
		this.type = type;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}
