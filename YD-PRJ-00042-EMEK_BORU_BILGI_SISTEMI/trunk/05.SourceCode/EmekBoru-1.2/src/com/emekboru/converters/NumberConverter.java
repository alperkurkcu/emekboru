package com.emekboru.converters;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter("numberConverter")
public class NumberConverter implements Converter {
	@Override
	public Float getAsObject(FacesContext context, UIComponent component,
			String value) {
		try {
			if (value == null || value.equals("")) {
				return null;
			}

			String[] tokens = value.split("[,]");

			if (tokens.length > 1)
				return new Float(tokens[0] + "." + tokens[1]);
			else
				return new Float(tokens[0] + ".0");
		} catch (Exception e) {
			String msg = "Lutfen gecerli bir deger giriniz.";
			FacesMessage fmsg = new FacesMessage(FacesMessage.SEVERITY_WARN,
					msg, null);
			FacesContext.getCurrentInstance().addMessage(null, fmsg);
			throw new ConverterException();
		}

	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		Float value1 = (Float) value;
		String valueString = value1 + "";
		String[] tokens = valueString.split("[.]");
		if (tokens.length > 1)
			return tokens[0] + "," + tokens[1];
		else
			return tokens[0] + ",0";

	}
}
