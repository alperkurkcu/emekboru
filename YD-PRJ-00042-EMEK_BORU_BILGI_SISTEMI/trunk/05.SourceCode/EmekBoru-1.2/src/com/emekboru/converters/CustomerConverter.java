package com.emekboru.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import com.emekboru.jpa.Customer;

public class CustomerConverter implements Converter {

	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		if (value == null || value.equals("")) {
			return null;
		}

		Customer customer = new Customer();
		customer.setCustomerId(Integer.parseInt(value));
		return customer;
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		Customer customer = (Customer) value;
		return customer.getCustomerId() + "";
	}
}