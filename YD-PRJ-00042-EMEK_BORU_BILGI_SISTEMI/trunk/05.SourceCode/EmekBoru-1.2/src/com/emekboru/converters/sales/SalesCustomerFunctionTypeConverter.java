package com.emekboru.converters.sales;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.emekboru.jpa.sales.customer.SalesCustomerFunctionType;

@ManagedBean(name = "salesCustomerFunctionTypeConverterBean")
@FacesConverter("salesCustomerFunctionTypeConverter")
public class SalesCustomerFunctionTypeConverter implements Converter {
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		if (value == null || value.equals("")) {
			return null;
		}

		SalesCustomerFunctionType functionType = new SalesCustomerFunctionType();
		functionType.setFunctionId(Integer.parseInt(value));

		return functionType;
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value == null) {
			return "";
		} else {
			SalesCustomerFunctionType functionType = (SalesCustomerFunctionType) value;
			return functionType.getFunctionId() + "";
		}
	}
}