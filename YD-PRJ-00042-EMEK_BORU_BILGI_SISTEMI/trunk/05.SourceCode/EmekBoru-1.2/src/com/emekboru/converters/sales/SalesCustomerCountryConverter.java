package com.emekboru.converters.sales;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.emekboru.jpa.Ulkeler;

@ManagedBean(name = "salesCustomerCountryConverterBean")
@FacesConverter("salesCustomerCountryConverter")
public class SalesCustomerCountryConverter implements Converter {
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		if (value == null || value.equals("")) {
			return null;
		}

		Ulkeler ulkeler = new Ulkeler();
		ulkeler.setId(Integer.parseInt(value));

		return ulkeler;
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		Ulkeler ulkeler = (Ulkeler) value;

		return ulkeler.getId() + "";
	}
}