package com.emekboru.converters.sales;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.emekboru.jpa.sales.documents.SalesDocumentType;

@ManagedBean(name = "salesDocumentTypeConverterBean")
@FacesConverter("salesDocumentTypeConverter")
public class SalesDocumentTypeConverter implements Converter {
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null || value.equals("")) {
			return null;
		}

		SalesDocumentType documentType = new SalesDocumentType();
		documentType.setTypeId(Integer.parseInt(value));
		
		return documentType;
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		SalesDocumentType documentType = (SalesDocumentType) value;

		return documentType.getTypeId() + "";
	}
}