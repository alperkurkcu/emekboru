package com.emekboru.converters.sales;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;

import com.emekboru.jpa.sales.customer.SalesContactPeople;
import com.emekboru.jpaman.Factory;

@ManagedBean(name = "salesContactPeopleConverterBean")
@FacesConverter("salesContactPeopleConverter")
public class SalesContactPeopleConverter implements Converter {
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		if (value == null || value.equals("")) {
			return null;
		}

		EntityManager entityManager = Factory.getInstance()
				.createEntityManager();
		SalesContactPeople contact = entityManager.find(
				SalesContactPeople.class, Integer.parseInt(value));
		return contact;
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value == null) {
			return "";
		} else {
			SalesContactPeople contact = (SalesContactPeople) value;
			return contact.getSalesContactPeopleId() + "";
		}
	}
}