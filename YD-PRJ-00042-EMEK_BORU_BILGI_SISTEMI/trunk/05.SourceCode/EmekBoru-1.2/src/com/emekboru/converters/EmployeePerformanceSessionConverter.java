/**
 * 
 */
package com.emekboru.converters;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.emekboru.jpa.employee.EmployeePerformance;
import com.emekboru.jpaman.CommonQueries;

/**
 * @author kursat
 * 
 */

@FacesConverter("employeePerformanceSessionConverter")
public class EmployeePerformanceSessionConverter implements Converter {

	static public List<String> sessions = new ArrayList<String>();

	static {
		CommonQueries<EmployeePerformance> employeePerformanceManager = new CommonQueries<EmployeePerformance>();

		List<EmployeePerformance> employeePerformances = new ArrayList<EmployeePerformance>();
		employeePerformances = employeePerformanceManager
				.findAll(EmployeePerformance.class);

		for (EmployeePerformance employeePerformance : employeePerformances) {
			sessions.add(employeePerformance.getSession());
		}

	}

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1,
			String submittedValue) {
		if (submittedValue.trim().equals("")) {
			return null;
		} else {
			return submittedValue;
		}
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object value) {
		if (value == null || value.equals("")) {
			return "";
		} else {
			return String.valueOf(((String) value));
		}
	}

}
