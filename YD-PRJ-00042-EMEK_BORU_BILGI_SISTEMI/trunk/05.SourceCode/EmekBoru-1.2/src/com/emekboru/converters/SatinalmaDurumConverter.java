/**
 * 
 */
package com.emekboru.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;

import com.emekboru.jpa.satinalma.SatinalmaDurum;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
@FacesConverter("satinalmaDurumConverter")
public class SatinalmaDurumConverter implements Converter {
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		if (value == null || value.equals("")) {
			return null;
		}
		EntityManager entityManager = Factory.getInstance()
				.createEntityManager();
		SatinalmaDurum status = entityManager.find(SatinalmaDurum.class,
				Integer.parseInt(value));
		status.setId(Integer.parseInt(value));
		return status;
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		SatinalmaDurum status = (SatinalmaDurum) value;

		return status.getId() + "";
	}
}
