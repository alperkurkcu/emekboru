package com.emekboru.converters.sales;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;

import com.emekboru.jpa.Employee;
import com.emekboru.jpaman.Factory;

@ManagedBean(name = "salesEmployeeConverterBean")
@FacesConverter("salesEmployeeConverter")
public class SalesEmployeeConverter implements Converter {
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		if (value == null || value.equals("")) {
			return null;
		}

		EntityManager entityManager = Factory.getInstance()
				.createEntityManager();
		Employee employee = entityManager.find(Employee.class,
				Integer.parseInt(value));
		return employee;
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value == null) {
			return "";
		} else {
			Employee employee = (Employee) value;
			return employee.getEmployeeId() + "";
		}
	}
}