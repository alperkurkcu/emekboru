package com.emekboru.converters.sales;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;

import com.emekboru.jpa.sales.customer.SalesCustomer;
import com.emekboru.jpaman.Factory;

@ManagedBean(name = "salesCustomerConverterBean")
@FacesConverter("salesCustomerConverter")
public class SalesCustomerConverter implements Converter {
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null || value.equals("")) {
			return null;
		}

		EntityManager entityManager = Factory.getInstance().createEntityManager();
		SalesCustomer customer = entityManager.find(SalesCustomer.class, Integer.parseInt(value));
		return customer;
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		SalesCustomer customer = (SalesCustomer) value;
		return customer.getSalesCustomerId() + "";
	}
}