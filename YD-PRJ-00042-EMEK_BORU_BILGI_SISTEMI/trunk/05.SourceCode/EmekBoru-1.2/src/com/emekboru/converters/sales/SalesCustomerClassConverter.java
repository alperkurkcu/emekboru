package com.emekboru.converters.sales;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;

import com.emekboru.jpa.sales.customer.SalesCustomerClass;
import com.emekboru.jpaman.Factory;

@ManagedBean(name = "salesCustomerClassConverterBean")
@FacesConverter("salesCustomerClassConverter")
public class SalesCustomerClassConverter implements Converter {
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null || value.equals("")) {
			return null;
		}
		EntityManager entityManager = Factory.getInstance().createEntityManager();
		SalesCustomerClass customerClass = entityManager.find(SalesCustomerClass.class, Integer.parseInt(value));
		customerClass.setClassId(Integer.parseInt(value));
		return customerClass;
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		SalesCustomerClass customerClass = (SalesCustomerClass) value;
		
		return customerClass.getClassId() + "";
	}
}