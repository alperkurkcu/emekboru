package com.emekboru.converters.sales;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.emekboru.jpa.sales.events.SalesEventType;

@ManagedBean(name = "salesEventTypeConverterBean")
@FacesConverter("salesEventTypeConverter")
public class SalesEventTypeConverter implements Converter {
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null || value.equals("")) {
			return null;
		}

		SalesEventType eventType = new SalesEventType();
		eventType.setTypeId(Integer.parseInt(value));
		
		return eventType;
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		SalesEventType eventType = (SalesEventType) value;

		return eventType.getTypeId() + "";
	}
}