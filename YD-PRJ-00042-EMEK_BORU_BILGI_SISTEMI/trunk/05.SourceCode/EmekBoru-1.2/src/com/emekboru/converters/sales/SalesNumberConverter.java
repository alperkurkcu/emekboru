package com.emekboru.converters.sales;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.emekboru.utils.FacesContextUtils;

@FacesConverter("salesNumberConverter")
public class SalesNumberConverter implements Converter {
	@Override
	public Double getAsObject(FacesContext context, UIComponent component,
			String value) {

		Double returnValue = new Double(0);

		try {
			if (value == null || value.equals(""))
				return new Double(0);
			else {
				value = value.replaceAll("[,]", ".");
				return new Double(value);
			}

		} catch (Exception e) {
			FacesContextUtils.addInfoMessage("salesNotNumber");
			e.printStackTrace();
		}
		return returnValue;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {

		String returnValue = "";

		try {

			Double doubleValue = (Double) value;
			if (doubleValue == 0)
				return returnValue;

			returnValue = doubleValue.toString().replaceAll("[.]", ",");

		} catch (Exception e) {
			System.out.println(e.toString());
			return returnValue;
		}

		return returnValue;
	}
}
