/**
 * 
 */
package com.emekboru.converters;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import com.emekboru.jpa.stok.StokUrunKartlari;
import com.emekboru.jpaman.stok.StokUrunKartlariManager;

/**
 * @author Alper K
 * 
 */
@FacesConverter("stokUrunKartlariConverter")
public class StokUrunKartlariConverter implements Converter {

	public static List<StokUrunKartlari> urunlerItemDB;

	static {
		urunlerItemDB = new ArrayList<StokUrunKartlari>();

		StokUrunKartlariManager itemMan = new StokUrunKartlariManager();
		urunlerItemDB = itemMan.findAllOrderByASC(StokUrunKartlari.class,
				"urunKodu");

	}

	public Object getAsObject(FacesContext facesContext, UIComponent component,
			String submittedValue) {

		StokUrunKartlariManager itemMan = new StokUrunKartlariManager();
		urunlerItemDB = itemMan.findAllOrderByASC(StokUrunKartlari.class,
				"urunKodu");

		// stoktan cıkıs yaparken null gelen veri nedeniyle editlendi
		if (submittedValue == null) {
			return null;
		} else if (submittedValue.trim().equals("")) {
			return null;
		} else {
			try {
				int urunId = Integer.parseInt(submittedValue);

				for (StokUrunKartlari i : urunlerItemDB) {
					if (i.getId() == urunId) {
						return i;
					}
				}

			} catch (NumberFormatException exception) {
				throw new ConverterException(new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "Conversion Error",
						"Not a valid Ürün"));
			}
		}

		return null;
	}

	public String getAsString(FacesContext facesContext, UIComponent component,
			Object value) {
		if (value == null || value.equals("")) {
			return "";
		} else {
			return String.valueOf(((StokUrunKartlari) value).getId());
		}
	}
}
