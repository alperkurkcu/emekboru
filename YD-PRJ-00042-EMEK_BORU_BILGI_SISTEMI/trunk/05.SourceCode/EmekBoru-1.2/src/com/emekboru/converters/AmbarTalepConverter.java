package com.emekboru.converters;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import com.emekboru.jpa.satinalma.SatinalmaAmbarMalzemeTalepForm;
import com.emekboru.jpaman.satinalma.SatinalmaAmbarMalzemeTalepFormManager;

/**
 * 
 */

/**
 * @author Alper K
 * 
 */
@FacesConverter("ambarTalepConverter")
public class AmbarTalepConverter implements Converter {

	public static List<SatinalmaAmbarMalzemeTalepForm> taleplerItemDB;
	private static SatinalmaAmbarMalzemeTalepFormManager itemMan = new SatinalmaAmbarMalzemeTalepFormManager();

	static {
		taleplerItemDB = new ArrayList<SatinalmaAmbarMalzemeTalepForm>();
		taleplerItemDB = itemMan.findAllOrderByASC(
				SatinalmaAmbarMalzemeTalepForm.class, "sayi");

	}

	public Object getAsObject(FacesContext facesContext, UIComponent component,
			String submittedValue) {
		taleplerItemDB = itemMan.findAllOrderByASC(
				SatinalmaAmbarMalzemeTalepForm.class, "sayi");
		// stoktan cıkıs yaparken null gelen veri nedeniyle editlendi
		if (submittedValue == null) {
			return null;
		} else if (submittedValue.trim().equals("")) {
			return null;
		} else {
			try {
				int talepId = Integer.parseInt(submittedValue);

				for (SatinalmaAmbarMalzemeTalepForm i : taleplerItemDB) {
					if (i.getId() == talepId) {
						return i;
					}
				}

			} catch (NumberFormatException exception) {
				throw new ConverterException(new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "Conversion Error",
						"Not a valid Talep"));
			}
		}

		return null;
	}

	public String getAsString(FacesContext facesContext, UIComponent component,
			Object value) {
		if (value == null || value.equals("")) {
			return "";
		} else {
			return String.valueOf(((SatinalmaAmbarMalzemeTalepForm) value)
					.getId());
		}
	}
}
