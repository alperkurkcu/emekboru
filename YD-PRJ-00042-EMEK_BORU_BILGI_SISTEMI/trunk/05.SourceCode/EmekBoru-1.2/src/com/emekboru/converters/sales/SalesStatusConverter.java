package com.emekboru.converters.sales;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;

import com.emekboru.jpa.sales.SalesStatus;
import com.emekboru.jpaman.Factory;

@ManagedBean(name = "salesStatusConverterBean")
@FacesConverter("salesStatusConverter")
public class SalesStatusConverter implements Converter {
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null || value.equals("")) {
			return null;
		}
		EntityManager entityManager = Factory.getInstance().createEntityManager();
		SalesStatus status = entityManager.find(SalesStatus.class, Integer.parseInt(value));
		status.setStatusId(Integer.parseInt(value));
		return status;
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		SalesStatus status = (SalesStatus) value;
		
		return status.getStatusId() + "";
	}
}