package com.emekboru.converters.sales;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;

import com.emekboru.jpa.sales.customer.SalesCustomerVisitingFrequency;
import com.emekboru.jpaman.Factory;

@ManagedBean(name = "salesCustomerVisitingFrequencyConverterBean")
@FacesConverter("salesCustomerVisitingFrequencyConverter")
public class SalesCustomerVisitingFrequencyConverter implements Converter {
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null || value.equals("")) {
			return null;
		}
		EntityManager entityManager = Factory.getInstance().createEntityManager();
		SalesCustomerVisitingFrequency frequency = entityManager.find(SalesCustomerVisitingFrequency.class, Integer.parseInt(value));
		frequency.setFrequencyId(Integer.parseInt(value));
		return frequency;
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		SalesCustomerVisitingFrequency frequency = (SalesCustomerVisitingFrequency) value;
		
		return frequency.getFrequencyId()+ "";
	}
}