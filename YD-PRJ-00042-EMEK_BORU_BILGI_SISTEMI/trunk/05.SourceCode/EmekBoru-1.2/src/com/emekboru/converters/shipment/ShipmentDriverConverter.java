/**
 * 
 */
package com.emekboru.converters.shipment;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.emekboru.beans.login.UserCredentialsBean;
import com.emekboru.config.ConfigBean;
import com.emekboru.jpa.shipment.ShipmentDriver;
import com.emekboru.jpaman.shipment.ShipmentDriverManager;

/**
 * @author kursat
 * 
 */

@FacesConverter("shipmentDriverConverter")
public class ShipmentDriverConverter implements Converter {

	@EJB
	private ConfigBean configBean;

	@ManagedProperty(value = "#{userCredentialsBean}")
	private UserCredentialsBean userCredentialsBean;
	static public List<ShipmentDriver> shipmentDrivers = new ArrayList<ShipmentDriver>();

	static {
		ShipmentDriverManager shipmentDriverManager = new ShipmentDriverManager();
		shipmentDrivers = shipmentDriverManager.findAll(ShipmentDriver.class);
	}
	
	@Override
	public Object getAsObject(FacesContext facesContext, UIComponent component,
			String submittedValue) {
		if (submittedValue == null) {
			return null;
		} else if (submittedValue.trim().equals("")) {
			return null;
		} else if (submittedValue.trim().equals("null")) {
			return null;
		} else {
			try {
				int id = Integer.parseInt(submittedValue);

				for (ShipmentDriver sd : shipmentDrivers) {
					if (sd.getId() == id) {
						return sd;
					}
				}

			} catch (NumberFormatException exception) {

				ShipmentDriverManager shipmentDriverManager = new ShipmentDriverManager();
				ShipmentDriver newShipmentDriver = new ShipmentDriver();

				newShipmentDriver.setName(submittedValue);

				shipmentDriverManager.enterNew(newShipmentDriver);

				return newShipmentDriver;
			}
		}

		ShipmentDriverManager shipmentDriverManager = new ShipmentDriverManager();
		ShipmentDriver newShipmentDriver = new ShipmentDriver();

		newShipmentDriver.setName(submittedValue);

		shipmentDriverManager.enterNew(newShipmentDriver);

		return newShipmentDriver;
	}

	@Override
	public String getAsString(FacesContext facesContext, UIComponent component,
			Object value) {
		if (value == null || value.equals("")) {
			return "";
		} else {
			return String.valueOf(((ShipmentDriver) value).getId());
		}
	}

}
