package com.emekboru.converters.sales;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;

import com.emekboru.jpa.sales.order.SalesProposalStatus;
import com.emekboru.jpaman.Factory;

@ManagedBean(name = "salesProposalStatusConverterBean")
@FacesConverter("salesProposalStatusConverter")
public class SalesProposalStatusConverter implements Converter {
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		if (value == null || value.equals("")) {
			return null;
		}
		EntityManager entityManager = Factory.getInstance()
				.createEntityManager();
		SalesProposalStatus status = entityManager.find(
				SalesProposalStatus.class, Integer.parseInt(value));
		status.setStatusId(Integer.parseInt(value));
		return status;
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		SalesProposalStatus status = (SalesProposalStatus) value;

		return status.getStatusId() + "";
	}
}