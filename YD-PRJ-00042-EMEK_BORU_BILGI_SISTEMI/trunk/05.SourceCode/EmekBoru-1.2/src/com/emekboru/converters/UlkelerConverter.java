/**
 * 
 */
package com.emekboru.converters;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;

import com.emekboru.jpa.Ulkeler;
import com.emekboru.jpaman.Factory;

/**
 * @author Alper K
 * 
 */
@ManagedBean(name = "ulkelerConverterBean")
@FacesConverter("ulkelerConverter")
public class UlkelerConverter implements Converter {
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		if (value == null || value.equals("")) {
			return null;
		}
		EntityManager entityManager = Factory.getInstance()
				.createEntityManager();
		Ulkeler status = entityManager.find(Ulkeler.class,
				Integer.parseInt(value));
		status.setId(Integer.parseInt(value));
		return status;
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		Ulkeler status = (Ulkeler) value;

		return status.getId() + "";
	}

}
