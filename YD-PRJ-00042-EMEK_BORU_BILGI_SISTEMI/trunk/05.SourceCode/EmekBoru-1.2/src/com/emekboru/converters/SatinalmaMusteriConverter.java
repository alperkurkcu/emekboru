/**
 * 
 */
package com.emekboru.converters;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import com.emekboru.jpa.satinalma.SatinalmaMusteri;
import com.emekboru.jpaman.satinalma.SatinalmaMusteriManager;

/**
 * @author Alper K
 * 
 */
@FacesConverter("satinalmaMusteriConverter")
public class SatinalmaMusteriConverter implements Converter {

	public static List<SatinalmaMusteri> firmalarDB;

	static {
		firmalarDB = new ArrayList<SatinalmaMusteri>();

		SatinalmaMusteriManager firmaMan = new SatinalmaMusteriManager();
		firmalarDB = firmaMan.findAllOrderByASC(SatinalmaMusteri.class,
				"shortName");

	}

	public Object getAsObject(FacesContext facesContext, UIComponent component,
			String submittedValue) {

		if (submittedValue == null) {
			return null;
		} else if (submittedValue.trim().equals("")) {
			return null;
		} else {
			try {
				int urunId = Integer.parseInt(submittedValue);

				for (SatinalmaMusteri i : firmalarDB) {
					if (i.getId() == urunId) {
						return i;
					}
				}

			} catch (NumberFormatException exception) {
				throw new ConverterException(new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "Conversion Error",
						"Not a valid Firma"));
			}
		}

		return null;
	}

	public String getAsString(FacesContext facesContext, UIComponent component,
			Object value) {
		if (value == null || value.equals("")) {
			return "";
		} else {
			return String.valueOf(((SatinalmaMusteri) value).getId());
		}
	}
}
