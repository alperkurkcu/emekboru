package com.emekboru.converters.sales;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;

import com.emekboru.jpa.sales.order.SalesDelivery;
import com.emekboru.jpaman.Factory;

@ManagedBean(name = "salesDeliveryConverterBean")
@FacesConverter("salesDeliveryConverter")
public class SalesDeliveryConverter implements Converter {
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null || value.equals("")) {
			return null;
		}
		EntityManager entityManager = Factory.getInstance().createEntityManager();
		SalesDelivery delivery = entityManager.find(SalesDelivery.class, Integer.parseInt(value));
		delivery.setDeliveryId(Integer.parseInt(value));
		return delivery;
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		SalesDelivery delivery = (SalesDelivery) value;
		
		return delivery.getDeliveryId() + "";
	}
}