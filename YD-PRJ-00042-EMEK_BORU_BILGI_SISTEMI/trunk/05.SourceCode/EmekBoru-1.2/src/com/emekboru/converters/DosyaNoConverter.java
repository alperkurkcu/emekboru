package com.emekboru.converters;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import com.emekboru.jpa.satinalma.SatinalmaTakipDosyalar;
import com.emekboru.jpaman.satinalma.SatinalmaTakipDosyalarManager;

/**
 * 
 */

/**
 * @author Alper K
 * 
 */
@FacesConverter("dosyaNoConverter")
public class DosyaNoConverter implements Converter {

	public static List<SatinalmaTakipDosyalar> dosyalarDB;

	static {
		dosyalarDB = new ArrayList<SatinalmaTakipDosyalar>();

		SatinalmaTakipDosyalarManager dosyaMan = new SatinalmaTakipDosyalarManager();
		dosyalarDB = dosyaMan.findAll(SatinalmaTakipDosyalar.class);

	}

	public Object getAsObject(FacesContext facesContext, UIComponent component,
			String submittedValue) {
		// stoktan cıkıs yaparken null gelen veri nedeniyle editlendi
		if (submittedValue == null) {
			return null;
		} else if (submittedValue.trim().equals("")) {
			return null;
		} else if (submittedValue.trim().equals("null")) {
			return null;
		} else {
			try {
				int dosyaId = Integer.parseInt(submittedValue);

				for (SatinalmaTakipDosyalar i : dosyalarDB) {
					if (i.getId() == dosyaId) {
						return i;
					}
				}

			} catch (NumberFormatException exception) {
				throw new ConverterException(new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "Conversion Error",
						"Not a valid DOSYA No"));
			}
		}

		return null;
	}

	public String getAsString(FacesContext facesContext, UIComponent component,
			Object value) {
		if (value == null || value.equals("")) {
			return "";
		} else {
			return String.valueOf(((SatinalmaTakipDosyalar) value).getId());
		}
	}
}
