/**
 * 
 */
package com.emekboru.converters;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import com.emekboru.jpa.sales.order.SalesItem;
import com.emekboru.jpaman.sales.order.SalesItemManager;

/**
 * @author Alper K
 * 
 */
@FacesConverter("salesItemConverter")
public class SalesItemConverter implements Converter {

	public static List<SalesItem> salesItemDB;

	static {
		salesItemDB = new ArrayList<SalesItem>();

		SalesItemManager itemMan = new SalesItemManager();
		salesItemDB = itemMan.findAllOrderByASC(SalesItem.class,
				"proposal.proposalNumber, c.itemNo");

	}

	public Object getAsObject(FacesContext facesContext, UIComponent component,
			String submittedValue) {
		// Stoktan cıkıs yaparken null gelme durumu eklendi
		if (submittedValue == null) {
			return null;
		} else if (submittedValue.trim().equals("")) {
			return null;
		} else {
			try {
				int itemId = Integer.parseInt(submittedValue);

				for (SalesItem i : salesItemDB) {
					if (i.getItemId() == itemId) {
						return i;
					}
				}

			} catch (NumberFormatException exception) {
				throw new ConverterException(new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "Conversion Error",
						"Not a valid Sale"));
			}
		}

		return null;
	}

	public String getAsString(FacesContext facesContext, UIComponent component,
			Object value) {
		if (value == null || value.equals("")) {
			return "";
		} else {
			return String.valueOf(((SalesItem) value).getItemId());
		}
	}
}
