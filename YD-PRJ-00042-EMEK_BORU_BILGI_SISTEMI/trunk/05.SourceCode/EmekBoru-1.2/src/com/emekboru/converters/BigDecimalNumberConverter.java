/**
 * 
 */
package com.emekboru.converters;

import java.math.BigDecimal;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 * @author Alper K
 * 
 */
@FacesConverter("bigDecimalNumberConverter")
public class BigDecimalNumberConverter implements Converter {
	@Override
	public BigDecimal getAsObject(FacesContext context, UIComponent component,
			String value) {
		try {
			if (value == null || value.equals("")) {
				return null;
			}

			String[] tokens = value.split("[,]");

			if (tokens.length > 1)
				return new BigDecimal(tokens[0] + "." + tokens[1]);
			else
				return new BigDecimal(tokens[0] + ".0");
		} catch (Exception e) {
			String msg = "Lutfen gecerli bir deger giriniz.";
			FacesMessage fmsg = new FacesMessage(FacesMessage.SEVERITY_WARN,
					msg, null);
			FacesContext.getCurrentInstance().addMessage(null, fmsg);
			throw new ConverterException();
		}

	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		BigDecimal value1 = (BigDecimal) value;
		String valueString = value1 + "";
		String[] tokens = valueString.split("[.]");
		if (tokens.length > 1)
			return tokens[0] + "," + tokens[1];
		else
			return tokens[0] + ",0";

	}
}
