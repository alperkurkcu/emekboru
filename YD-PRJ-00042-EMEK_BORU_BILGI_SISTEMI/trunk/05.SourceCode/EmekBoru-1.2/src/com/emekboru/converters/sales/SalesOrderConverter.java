package com.emekboru.converters.sales;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;

import com.emekboru.jpa.sales.order.SalesOrder;
import com.emekboru.jpaman.Factory;

@ManagedBean(name = "salesOrderConverterBean")
@FacesConverter("salesOrderConverter")
public class SalesOrderConverter implements Converter {
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null || value.equals("")) {
			return null;
		}
		EntityManager entityManager = Factory.getInstance().createEntityManager();
		SalesOrder order = entityManager.find(SalesOrder.class, Integer.parseInt(value));
		order.setOrderId(Integer.parseInt(value));
		return order;
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		SalesOrder order = (SalesOrder) value;
		
		return order.getOrderId() + "";
	}
}