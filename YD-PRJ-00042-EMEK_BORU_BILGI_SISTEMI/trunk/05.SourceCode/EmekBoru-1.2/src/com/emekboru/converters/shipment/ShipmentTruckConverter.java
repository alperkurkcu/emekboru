/**
 * 
 */
package com.emekboru.converters.shipment;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.emekboru.jpa.shipment.ShipmentTruck;
import com.emekboru.jpaman.shipment.ShipmentTruckManager;

/**
 * @author kursat
 * 
 */

@FacesConverter("shipmentTruckConverter")
public class ShipmentTruckConverter implements Converter {

	static public List<ShipmentTruck> shipmentTrucks = new ArrayList<ShipmentTruck>();

	static {
		ShipmentTruckManager shipmentTruckManager = new ShipmentTruckManager();
		shipmentTrucks = shipmentTruckManager.findAll(ShipmentTruck.class);
	}

	@Override
	public Object getAsObject(FacesContext facesContext, UIComponent component,
			String submittedValue) {
		if (submittedValue == null) {
			return null;
		} else if (submittedValue.trim().equals("")) {
			return null;
		} else if (submittedValue.trim().equals("null")) {
			return null;
		} else {
			try {
				int id = Integer.parseInt(submittedValue);

				for (ShipmentTruck st : shipmentTrucks) {
					if (st.getId() == id) {
						return st;
					}
				}

			} catch (NumberFormatException exception) {

				ShipmentTruckManager shipmentTruckManager = new ShipmentTruckManager();
				ShipmentTruck newShipmentTruck = new ShipmentTruck();

				newShipmentTruck.setPlate(submittedValue);

				shipmentTruckManager.enterNew(newShipmentTruck);

				return newShipmentTruck;
			}
		}

		ShipmentTruckManager shipmentTruckManager = new ShipmentTruckManager();
		ShipmentTruck newShipmentTruck = new ShipmentTruck();

		newShipmentTruck.setPlate(submittedValue);

		shipmentTruckManager.enterNew(newShipmentTruck);

		return newShipmentTruck;
	}

	@Override
	public String getAsString(FacesContext facesContext, UIComponent component,
			Object value) {
		if (value == null || value.equals("")) {
			return "";
		} else {
			return String.valueOf(((ShipmentTruck) value).getId());
		}
	}

}
