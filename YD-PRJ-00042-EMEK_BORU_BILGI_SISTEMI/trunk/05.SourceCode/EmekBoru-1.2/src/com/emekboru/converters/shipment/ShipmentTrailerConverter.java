/**
 * 
 */
package com.emekboru.converters.shipment;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.emekboru.jpa.shipment.ShipmentTrailer;
import com.emekboru.jpaman.shipment.ShipmentTrailerManager;

/**
 * @author kursat
 * 
 */

@FacesConverter("shipmentTrailerConverter")
public class ShipmentTrailerConverter implements Converter {

	static public List<ShipmentTrailer> shipmentTrailers = new ArrayList<ShipmentTrailer>();

	static {
		ShipmentTrailerManager shipmentTrailerManager = new ShipmentTrailerManager();
		shipmentTrailers = shipmentTrailerManager
				.findAll(ShipmentTrailer.class);
	}

	@Override
	public Object getAsObject(FacesContext facesContext, UIComponent component,
			String submittedValue) {
		if (submittedValue == null) {
			return null;
		} else if (submittedValue.trim().equals("")) {
			return null;
		} else if (submittedValue.trim().equals("null")) {
			return null;
		} else {
			try {
				int id = Integer.parseInt(submittedValue);

				for (ShipmentTrailer st : shipmentTrailers) {
					if (st.getId() == id) {
						return st;
					}
				}

			} catch (NumberFormatException exception) {

				ShipmentTrailerManager shipmentTrailerManager = new ShipmentTrailerManager();
				ShipmentTrailer newShipmentTrailer = new ShipmentTrailer();

				newShipmentTrailer.setPlate(submittedValue);

				shipmentTrailerManager.enterNew(newShipmentTrailer);

				return newShipmentTrailer;
			}
		}

		ShipmentTrailerManager shipmentTrailerManager = new ShipmentTrailerManager();
		ShipmentTrailer newShipmentTrailer = new ShipmentTrailer();

		newShipmentTrailer.setPlate(submittedValue);

		shipmentTrailerManager.enterNew(newShipmentTrailer);

		return newShipmentTrailer;
	}

	@Override
	public String getAsString(FacesContext facesContext, UIComponent component,
			Object value) {
		if (value == null || value.equals("")) {
			return "";
		} else {
			return String.valueOf(((ShipmentTrailer) value).getId());
		}
	}

}
