package com.emekboru.converters;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;

import com.emekboru.jpa.Department;
import com.emekboru.jpaman.Factory;

@ManagedBean(name = "departmentConverterBean")
@FacesConverter("departmentConverter")
public class DepartmentConverter implements Converter {
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		if (value == null || value.isEmpty()) {
			return null;
		}

		EntityManager entityManager = Factory.getInstance()
				.createEntityManager();
		Department contact = entityManager.find(Department.class,
				Integer.parseInt(value));
		try {
			return contact;
		} catch (NumberFormatException e) {
			throw new ConverterException(new FacesMessage(String.format(
					"%s is not a valid departmentId", value)), e);
		}
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value == null) {
			return "";
		}

		if (value instanceof Department) {
			return String.valueOf(((Department) value).getDepartmentId());
		} else {
			throw new ConverterException(new FacesMessage(String.format(
					"%s is not a valid Department", value)));
		}
	}
}