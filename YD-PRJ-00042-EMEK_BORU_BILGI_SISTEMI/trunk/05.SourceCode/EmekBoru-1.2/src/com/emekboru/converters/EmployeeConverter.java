/**
 * 
 */
package com.emekboru.converters;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import com.emekboru.jpa.Employee;
import com.emekboru.jpaman.EmployeeManager;

/**
 * @author Alper K
 * 
 */
@FacesConverter("employeeConverter")
public class EmployeeConverter implements Converter {

	public static List<Employee> employeeDB;

	static {
		employeeDB = new ArrayList<Employee>();

		EmployeeManager empMan = new EmployeeManager();
		employeeDB = empMan.findAll(Employee.class);

	}

	public Object getAsObject(FacesContext facesContext, UIComponent component,
			String submittedValue) {
		// stokdan null gelme durumu
		if (submittedValue == null) {
			return null;
		} else if (submittedValue.trim().equals("")) {
			return null;
		} else if (submittedValue.trim().equals("null")) {
			return null;
		} else {
			try {
				int employeeId = Integer.parseInt(submittedValue);

				for (Employee e : employeeDB) {
					if (e.getEmployeeId() == employeeId) {
						return e;
					}
				}

			} catch (NumberFormatException exception) {
				throw new ConverterException(new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "Conversion Error",
						"Not a valid player"));
			}
		}

		return null;
	}

	public String getAsString(FacesContext facesContext, UIComponent component,
			Object value) {
		if (value == null) {
			return "";
		} else {
			return String.valueOf(((Employee) value).getEmployeeId());
		}
	}
}
