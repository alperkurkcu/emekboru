package com.emekboru.converters.sales;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;

import com.emekboru.jpa.sales.order.SalesCurrency;
import com.emekboru.jpaman.Factory;

@ManagedBean(name = "salesCurrencyConverterBean")
@FacesConverter("salesCurrencyConverter")
public class SalesCurrencyConverter implements Converter {
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null || value.equals("")) {
			return null;
		}
		EntityManager entityManager = Factory.getInstance().createEntityManager();
		SalesCurrency currency = entityManager.find(SalesCurrency.class, Integer.parseInt(value));
		currency.setCurrencyId(Integer.parseInt(value));
		return currency;
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		SalesCurrency currency = (SalesCurrency) value;
		
		return currency.getCurrencyId() + "";
	}
}