
package com.emekboru.config;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Pipe complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Pipe">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Coating">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Folder" type="{http://emekboru.com/schema/emek}Folder"/>
 *                   &lt;element name="Internal">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Types" type="{http://emekboru.com/schema/emek}Type" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="External">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Types" type="{http://emekboru.com/schema/emek}Type" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Standards" type="{http://emekboru.com/schema/emek}Standard" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="Tests" type="{http://emekboru.com/schema/emek}Test" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Manufacturing">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Folder" type="{http://emekboru.com/schema/emek}Folder"/>
 *                   &lt;element name="Tests" type="{http://emekboru.com/schema/emek}Test" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Types" type="{http://emekboru.com/schema/emek}Type" maxOccurs="unbounded" minOccurs="0" form="unqualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Pipe", propOrder = {
    "coating",
    "manufacturing",
    "types",
    "barcode"
})
public class Pipe {

    @XmlElement(name = "Coating", required = true)
    protected Pipe.Coating coating;
    @XmlElement(name = "Manufacturing", required = true)
    protected Pipe.Manufacturing manufacturing;
    @XmlElement(name = "Types", required = true)
    protected List<Type> types;
    @XmlElement(name = "Barcode", required = true)
    protected Barcode barcode;

    
    public Test findTestByGlobalId(int globalId){
    	
    	for(Test t : manufacturing.getTests()){
    		if(t.getGlobalId().equals(globalId))
    			return t;
    	}
    	for(Test t : coating.getTests()){
    		if(t.getGlobalId().equals(globalId))
    			return t;
    	}
    	return null;
    }
    
    /**
     * Gets the value of the coating property.
     * 
     * @return
     *     possible object is
     *     {@link Pipe.Coating }
     *     
     */
    public Pipe.Coating getCoating() {
        return coating;
    }

    /**
     * Sets the value of the coating property.
     * 
     * @param value
     *     allowed object is
     *     {@link Pipe.Coating }
     *     
     */
    public void setCoating(Pipe.Coating value) {
        this.coating = value;
    }

    /**
     * Gets the value of the manufacturing property.
     * 
     * @return
     *     possible object is
     *     {@link Pipe.Manufacturing }
     *     
     */
    public Pipe.Manufacturing getManufacturing() {
        return manufacturing;
    }

    /**
     * Sets the value of the manufacturing property.
     * 
     * @param value
     *     allowed object is
     *     {@link Pipe.Manufacturing }
     *     
     */
    public void setManufacturing(Pipe.Manufacturing value) {
        this.manufacturing = value;
    }

    /**
     * Gets the value of the types property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the types property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTypes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Type }
     * 
     * 
     */
    public List<Type> getTypes() {
        if (types == null) {
            types = new ArrayList<Type>();
        }
        return this.types;
    }

    /**
     * Gets the value of the barcode property.
     * 
     * @return
     *     possible object is
     *     {@link Barcode }
     *     
     */
    public Barcode getBarcode() {
        return barcode;
    }

    /**
     * Sets the value of the barcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Barcode }
     *     
     */
    public void setBarcode(Barcode value) {
        this.barcode = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Folder" type="{http://emekboru.com/schema/emek}Folder"/>
     *         &lt;element name="Internal">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Types" type="{http://emekboru.com/schema/emek}Type" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="External">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Types" type="{http://emekboru.com/schema/emek}Type" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Standards" type="{http://emekboru.com/schema/emek}Standard" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="Tests" type="{http://emekboru.com/schema/emek}Test" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "folder",
        "internal",
        "external",
        "standards",
        "tests"
    })
    public static class Coating {

        @XmlElement(name = "Folder", required = true)
        protected Folder folder;
        @XmlElement(name = "Internal", required = true)
        protected Pipe.Coating.Internal internal;
        @XmlElement(name = "External", required = true)
        protected Pipe.Coating.External external;
        @XmlElement(name = "Standards")
        protected List<Standard> standards;
        @XmlElement(name = "Tests")
        protected List<Test> tests;

        /**
         * Gets the value of the folder property.
         * 
         * @return
         *     possible object is
         *     {@link Folder }
         *     
         */
        public Folder getFolder() {
            return folder;
        }

        /**
         * Sets the value of the folder property.
         * 
         * @param value
         *     allowed object is
         *     {@link Folder }
         *     
         */
        public void setFolder(Folder value) {
            this.folder = value;
        }

        /**
         * Gets the value of the internal property.
         * 
         * @return
         *     possible object is
         *     {@link Pipe.Coating.Internal }
         *     
         */
        public Pipe.Coating.Internal getInternal() {
            return internal;
        }

        /**
         * Sets the value of the internal property.
         * 
         * @param value
         *     allowed object is
         *     {@link Pipe.Coating.Internal }
         *     
         */
        public void setInternal(Pipe.Coating.Internal value) {
            this.internal = value;
        }

        /**
         * Gets the value of the external property.
         * 
         * @return
         *     possible object is
         *     {@link Pipe.Coating.External }
         *     
         */
        public Pipe.Coating.External getExternal() {
            return external;
        }

        /**
         * Sets the value of the external property.
         * 
         * @param value
         *     allowed object is
         *     {@link Pipe.Coating.External }
         *     
         */
        public void setExternal(Pipe.Coating.External value) {
            this.external = value;
        }

        /**
         * Gets the value of the standards property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the standards property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getStandards().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Standard }
         * 
         * 
         */
        public List<Standard> getStandards() {
            if (standards == null) {
                standards = new ArrayList<Standard>();
            }
            return this.standards;
        }

        /**
         * Gets the value of the tests property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the tests property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTests().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Test }
         * 
         * 
         */
        public List<Test> getTests() {
            if (tests == null) {
                tests = new ArrayList<Test>();
            }
            return this.tests;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Types" type="{http://emekboru.com/schema/emek}Type" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "types"
        })
        public static class External {

            @XmlElement(name = "Types")
            protected List<Type> types;

            /**
             * Gets the value of the types property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the types property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getTypes().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Type }
             * 
             * 
             */
            public List<Type> getTypes() {
                if (types == null) {
                    types = new ArrayList<Type>();
                }
                return this.types;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Types" type="{http://emekboru.com/schema/emek}Type" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "types"
        })
        public static class Internal {

            @XmlElement(name = "Types")
            protected List<Type> types;

            /**
             * Gets the value of the types property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the types property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getTypes().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Type }
             * 
             * 
             */
            public List<Type> getTypes() {
                if (types == null) {
                    types = new ArrayList<Type>();
                }
                return this.types;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Folder" type="{http://emekboru.com/schema/emek}Folder"/>
     *         &lt;element name="Tests" type="{http://emekboru.com/schema/emek}Test" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "folder",
        "tests"
    })
    public static class Manufacturing {

        @XmlElement(name = "Folder", required = true)
        protected Folder folder;
        @XmlElement(name = "Tests")
        protected List<Test> tests;

        
        public List<Test> getActiveTests(){
        	
        	List<Test> list = new ArrayList<Test>();
        	for(Test t : tests)
        		if(t.active)
        			list.add(t);
        	
        	return list;
        }
        
        public List<Test> getGroupTests(String groupName){
        	
        	List<Test> list = new ArrayList<Test>(0);
        	for(Test t : tests){
        		if(t.active && t.getGroup().equals(groupName))
        			list.add(t);
        	}
        	
        	return list;
        }
        
        public List<Test> getMiniGroupTests(String miniGroup){
        	
        	List<Test> list = new ArrayList<Test>();
        	for(Test t : tests){
        		if(t.getMiniGroup().equals(miniGroup))
        			list.add(t);
        	}
        	return list;
        }
        /**
         * Gets the value of the folder property.
         * 
         * @return
         *     possible object is
         *     {@link Folder }
         *     
         */
        public Folder getFolder() {
            return folder;
        }

        /**
         * Sets the value of the folder property.
         * 
         * @param value
         *     allowed object is
         *     {@link Folder }
         *     
         */
        public void setFolder(Folder value) {
            this.folder = value;
        }

        /**
         * Gets the value of the tests property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the tests property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTests().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Test }
         * 
         * 
         */
        public List<Test> getTests() {
            if (tests == null) {
                tests = new ArrayList<Test>();
            }
            return this.tests;
        }

    }

}
