package com.emekboru.config;

public class AVkey {

	public static final String MAN_REPORT_EN = "com.emekboru.config.report.man.en";

	public static final String MAN_REPORT_TR = "com.emekboru.config.report.man.tr";

	public static final String EDW_REPORT_EN = "com.emekboru.config.report.edw.en";

	public static final String EDW_REPORT_TR = "com.emekboru.config.report.edw.tr";

	public static final String TEST_REPORT_EN = "com.emekboru.config.report.test.en";

	public static final String REPORT_PATH = "com.emekboru.config.report.path";

	public static final String RULO_REPORT_EN = "com.emekboru.config.report.rulo.en";

	public static final String RULO_REPORT_TR = "com.emekboru.config.report.rulo.tr";

}
