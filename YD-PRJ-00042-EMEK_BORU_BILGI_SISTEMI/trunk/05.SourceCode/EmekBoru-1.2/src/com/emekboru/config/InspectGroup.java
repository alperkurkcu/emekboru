package com.emekboru.config;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "inspectGroup", propOrder = { "status" })
public class InspectGroup {
	@XmlElement(name = "Status", required = true)
	protected List<Status> status;

	public Status findStatus(int id) {

		if (status == null || status.size() == 0)
			return null;

		for (Status s : status) {

			if (s.getId().equals(id))
				return s;
		}
		return null;
	}

	public List<Status> findUnendedStatuses() {

		List<Status> resultList = new ArrayList<Status>(0);
		for (Status s : status) {

			if (s.isEnd == false)
				resultList.add(s);
		}
		return resultList;
	}

	public Status findDefaultStatus() {
		for (Status s : status) {

			if (s.isDefault)
				return s;
		}
		return null;
	}

	public List<Status> getStatus() {
		if (status == null) {
			status = new ArrayList<Status>();
		}
		return this.status;
	}

}
