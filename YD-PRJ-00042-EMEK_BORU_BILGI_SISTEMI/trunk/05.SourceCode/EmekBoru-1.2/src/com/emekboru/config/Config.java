package com.emekboru.config;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for config complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="config">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EntryPoint">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Http">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;attribute name="status" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Https">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;attribute name="status" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                           &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Employee" type="{http://emekboru.com/schema/emek}Employee"/>
 *         &lt;element name="Department" type="{http://emekboru.com/schema/emek}Department"/>
 *         &lt;element name="Pipe" type="{http://emekboru.com/schema/emek}Pipe"/>
 *         &lt;element name="Folder" type="{http://emekboru.com/schema/emek}Folder"/>
 *         &lt;element name="Attributes" type="{http://emekboru.com/schema/emek}Attribute" maxOccurs="unbounded"/>
 *         &lt;element name="UnitMeasure" type="{http://emekboru.com/schema/emek}UnitMeasure" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Order" type="{http://emekboru.com/schema/emek}Order"/>
 *       &lt;/sequence>
 *       &lt;attribute name="date" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="version" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="author" type="{http://www.w3.org/2001/XMLSchema}string" />
 *        &lt;element name="TestDocs" type="{http://emekboru.com/schema/emek}TestDocs"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Config")
@XmlType(name = "config", propOrder = { "entryPoint", "employee", "department",
		"pipe", "folder", "attributes", "unitMeasure", "order", "materials",
		"machines", "testDocs", "inspectGroup" })
public class Config {

	@XmlElement(name = "EntryPoint", required = true)
	protected Config.EntryPoint entryPoint;
	@XmlElement(name = "Employee", required = true)
	protected Employee employee;
	@XmlElement(name = "Department", required = true)
	protected Department department;
	@XmlElement(name = "Pipe", required = true)
	protected Pipe pipe;
	@XmlElement(name = "Folder", required = true)
	protected Folder folder;
	@XmlElement(name = "Attributes", required = true)
	protected List<Attribute> attributes;
	@XmlElement(name = "UnitMeasure", required = true)
	protected List<UnitMeasure> unitMeasure;
	@XmlElement(name = "Order", required = true)
	protected Order order;
	@XmlElement(name = "Materials", required = true)
	protected Materials materials;
	@XmlElement(name = "Machines", required = true)
	protected Machine machines;
	@XmlAttribute(name = "date", required = true)
	protected String date;
	@XmlAttribute(name = "version", required = true)
	protected String version;
	@XmlAttribute(name = "author", required = true)
	protected String author;
	@XmlElement(name = "TestDocs", required = true)
	protected Test testDocs;
	@XmlElement(name = "InspectGroup", required = true)
	protected InspectGroup inspectGroup;

	/**
	 * Gets the value of the entryPoint property.
	 * 
	 * @return possible object is {@link Config.EntryPoint }
	 * 
	 */
	public Config.EntryPoint getEntryPoint() {
		return entryPoint;
	}

	/**
	 * Sets the value of the entryPoint property.
	 * 
	 * @param value
	 *            allowed object is {@link Config.EntryPoint }
	 * 
	 */
	public void setEntryPoint(Config.EntryPoint value) {
		this.entryPoint = value;
	}

	/**
	 * Gets the value of the employee property.
	 * 
	 * @return possible object is {@link Employee }
	 * 
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 * Sets the value of the employee property.
	 * 
	 * @param value
	 *            allowed object is {@link Employee }
	 * 
	 */
	public void setEmployee(Employee value) {
		this.employee = value;
	}

	/**
	 * Gets the value of the department property.
	 * 
	 * @return possible object is {@link Department }
	 * 
	 */
	public Department getDepartment() {
		return department;
	}

	/**
	 * Sets the value of the department property.
	 * 
	 * @param value
	 *            allowed object is {@link Department }
	 * 
	 */
	public void setDepartment(Department value) {
		this.department = value;
	}

	/**
	 * Gets the value of the pipe property.
	 * 
	 * @return possible object is {@link Pipe }
	 * 
	 */
	public Pipe getPipe() {
		return pipe;
	}

	/**
	 * Sets the value of the pipe property.
	 * 
	 * @param value
	 *            allowed object is {@link Pipe }
	 * 
	 */
	public void setPipe(Pipe value) {
		this.pipe = value;
	}

	/**
	 * Gets the value of the folder property.
	 * 
	 * @return possible object is {@link Folder }
	 * 
	 */
	public Folder getFolder() {
		return folder;
	}

	/**
	 * Sets the value of the folder property.
	 * 
	 * @param value
	 *            allowed object is {@link Folder }
	 * 
	 */
	public void setFolder(Folder value) {
		this.folder = value;
	}

	public Test getTestDocs() {
		return testDocs;
	}

	public void setTestDocs(Test value) {
		this.testDocs = value;
	}

	public InspectGroup getInspectGroup() {
		return inspectGroup;
	}

	public void setInspectGroup(InspectGroup value) {
		this.inspectGroup = value;
	}

	/**
	 * Gets the value of the attributes property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the attributes property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getAttributes().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link Attribute }
	 * 
	 * 
	 */
	public List<Attribute> getAttributes() {
		if (attributes == null) {
			attributes = new ArrayList<Attribute>();
		}
		return this.attributes;
	}

	/**
	 * Gets the value of the unitMeasure property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the unitMeasure property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getUnitMeasure().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link UnitMeasure }
	 * 
	 * 
	 */
	public List<UnitMeasure> getUnitMeasure() {
		if (unitMeasure == null) {
			unitMeasure = new ArrayList<UnitMeasure>();
		}
		return this.unitMeasure;
	}

	/**
	 * Gets the value of the order property.
	 * 
	 * @return possible object is {@link Order }
	 * 
	 */
	public Order getOrder() {
		return order;
	}

	/**
	 * Sets the value of the order property.
	 * 
	 * @param value
	 *            allowed object is {@link Order }
	 * 
	 */
	public void setOrder(Order value) {
		this.order = value;
	}

	/**
	 * Gets the value of the materials property.
	 * 
	 * @return possible object is {@link Materials }
	 * 
	 */
	public Materials getMaterials() {
		return materials;
	}

	/**
	 * Sets the value of the materials property.
	 * 
	 * @param value
	 *            allowed object is {@link Materials }
	 * 
	 */
	public void setMaterials(Materials value) {
		this.materials = value;
	}

	/**
	 * Gets the value of the machines property.
	 * 
	 * @return possible object is {@link Machine }
	 * 
	 */
	public Machine getMachines() {
		return machines;
	}

	/**
	 * Sets the value of the machines property.
	 * 
	 * @param value
	 *            allowed object is {@link Machine }
	 * 
	 */
	public void setMachines(Machine value) {
		this.machines = value;
	}

	/**
	 * Gets the value of the date property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Sets the value of the date property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setDate(String value) {
		this.date = value;
	}

	/**
	 * Gets the value of the version property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Sets the value of the version property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setVersion(String value) {
		this.version = value;
	}

	/**
	 * Gets the value of the author property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * Sets the value of the author property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setAuthor(String value) {
		this.author = value;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="Http">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;attribute name="status" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                 &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="Https">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;attribute name="status" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *                 &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}string" />
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "http", "https" })
	public static class EntryPoint {

		@XmlElement(name = "Http", required = true)
		protected Config.EntryPoint.Http http;
		@XmlElement(name = "Https", required = true)
		protected Config.EntryPoint.Https https;

		/**
		 * Gets the value of the http property.
		 * 
		 * @return possible object is {@link Config.EntryPoint.Http }
		 * 
		 */
		public Config.EntryPoint.Http getHttp() {
			return http;
		}

		/**
		 * Sets the value of the http property.
		 * 
		 * @param value
		 *            allowed object is {@link Config.EntryPoint.Http }
		 * 
		 */
		public void setHttp(Config.EntryPoint.Http value) {
			this.http = value;
		}

		/**
		 * Gets the value of the https property.
		 * 
		 * @return possible object is {@link Config.EntryPoint.Https }
		 * 
		 */
		public Config.EntryPoint.Https getHttps() {
			return https;
		}

		/**
		 * Sets the value of the https property.
		 * 
		 * @param value
		 *            allowed object is {@link Config.EntryPoint.Https }
		 * 
		 */
		public void setHttps(Config.EntryPoint.Https value) {
			this.https = value;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;attribute name="status" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *       &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "")
		public static class Http {

			@XmlAttribute(name = "status")
			protected String status;
			@XmlAttribute(name = "value")
			protected String value;

			/**
			 * Gets the value of the status property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getStatus() {
				return status;
			}

			/**
			 * Sets the value of the status property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setStatus(String value) {
				this.status = value;
			}

			/**
			 * Gets the value of the value property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getValue() {
				return value;
			}

			/**
			 * Sets the value of the value property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setValue(String value) {
				this.value = value;
			}

		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;attribute name="status" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *       &lt;attribute name="value" type="{http://www.w3.org/2001/XMLSchema}string" />
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "")
		public static class Https {

			@XmlAttribute(name = "status")
			protected String status;
			@XmlAttribute(name = "value")
			protected String value;

			/**
			 * Gets the value of the status property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getStatus() {
				return status;
			}

			/**
			 * Sets the value of the status property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setStatus(String value) {
				this.status = value;
			}

			/**
			 * Gets the value of the value property.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getValue() {
				return value;
			}

			/**
			 * Sets the value of the value property.
			 * 
			 * @param value
			 *            allowed object is {@link String }
			 * 
			 */
			public void setValue(String value) {
				this.value = value;
			}

		}

	}

}
