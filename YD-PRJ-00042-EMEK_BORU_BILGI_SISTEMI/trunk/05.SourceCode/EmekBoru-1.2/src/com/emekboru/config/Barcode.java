package com.emekboru.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
* <p>Java class for Barcode complex type.
* 
* <p>The following schema fragment specifies the expected content contained within this class.
* 
* <pre>
* &lt;complexType name="Barcode">
*   &lt;complexContent>
*     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
*       &lt;attribute name="preffix" type="{http://www.w3.org/2001/XMLSchema}string" />
*       &lt;attribute name="suffix" type="{http://www.w3.org/2001/XMLSchema}string" />
*       &lt;attribute name="description" type="{http://www.w3.org/2001/XMLSchema}string" />
*       &lt;attribute name="printable" type="{http://www.w3.org/2001/XMLSchema}boolean" />
*       &lt;attribute name="scanable" type="{http://www.w3.org/2001/XMLSchema}boolean" />
*     &lt;/restriction>
*   &lt;/complexContent>
* &lt;/complexType>
* </pre>
* 
* 
*/
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Barcode")
public class Barcode {

 @XmlAttribute(name = "preffix")
 protected String preffix;
 @XmlAttribute(name = "suffix")
 protected String suffix;
 @XmlAttribute(name = "description")
 protected String description;
 @XmlAttribute(name = "printable")
 protected Boolean printable;
 @XmlAttribute(name = "scanable")
 protected Boolean scanable;

 /**
  * Gets the value of the preffix property.
  * 
  * @return
  *     possible object is
  *     {@link String }
  *     
  */
 public String getPreffix() {
     return preffix;
 }

 /**
  * Sets the value of the preffix property.
  * 
  * @param value
  *     allowed object is
  *     {@link String }
  *     
  */
 public void setPreffix(String value) {
     this.preffix = value;
 }

 /**
  * Gets the value of the suffix property.
  * 
  * @return
  *     possible object is
  *     {@link String }
  *     
  */
 public String getSuffix() {
     return suffix;
 }

 /**
  * Sets the value of the suffix property.
  * 
  * @param value
  *     allowed object is
  *     {@link String }
  *     
  */
 public void setSuffix(String value) {
     this.suffix = value;
 }

 /**
  * Gets the value of the description property.
  * 
  * @return
  *     possible object is
  *     {@link String }
  *     
  */
 public String getDescription() {
     return description;
 }

 /**
  * Sets the value of the description property.
  * 
  * @param value
  *     allowed object is
  *     {@link String }
  *     
  */
 public void setDescription(String value) {
     this.description = value;
 }

 /**
  * Gets the value of the printable property.
  * 
  * @return
  *     possible object is
  *     {@link Boolean }
  *     
  */
 public Boolean isPrintable() {
     return printable;
 }

 /**
  * Sets the value of the printable property.
  * 
  * @param value
  *     allowed object is
  *     {@link Boolean }
  *     
  */
 public void setPrintable(Boolean value) {
     this.printable = value;
 }

 /**
  * Gets the value of the scanable property.
  * 
  * @return
  *     possible object is
  *     {@link Boolean }
  *     
  */
 public Boolean isScanable() {
     return scanable;
 }

 /**
  * Sets the value of the scanable property.
  * 
  * @param value
  *     allowed object is
  *     {@link Boolean }
  *     
  */
 public void setScanable(Boolean value) {
     this.scanable = value;
 }

}
