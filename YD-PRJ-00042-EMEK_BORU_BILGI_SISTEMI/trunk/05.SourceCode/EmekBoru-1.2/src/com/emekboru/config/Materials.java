package com.emekboru.config;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
* <p>Java class for Materials complex type.
* 
* <p>The following schema fragment specifies the expected content contained within this class.
* 
* <pre>
* &lt;complexType name="Materials">
*   &lt;complexContent>
*     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
*       &lt;sequence>
*         &lt;element name="Production">
*           &lt;complexType>
*             &lt;complexContent>
*               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
*                 &lt;sequence>
*                   &lt;element name="Coil">
*                     &lt;complexType>
*                       &lt;complexContent>
*                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
*                           &lt;sequence>
*                             &lt;element name="Tests" type="{http://emekboru.com/schema/emek}Test" maxOccurs="unbounded" minOccurs="0"/>
*                             &lt;element name="Barcode" type="{http://emekboru.com/schema/emek}Barcode"/>
*                           &lt;/sequence>
*                         &lt;/restriction>
*                       &lt;/complexContent>
*                     &lt;/complexType>
*                   &lt;/element>
*                   &lt;element name="Edw">
*                     &lt;complexType>
*                       &lt;complexContent>
*                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
*                           &lt;sequence>
*                             &lt;element name="Tests" type="{http://emekboru.com/schema/emek}Test" maxOccurs="unbounded" minOccurs="0"/>
*                             &lt;element name="Barcode" type="{http://emekboru.com/schema/emek}Barcode"/>
*                           &lt;/sequence>
*                         &lt;/restriction>
*                       &lt;/complexContent>
*                     &lt;/complexType>
*                   &lt;/element>
*                 &lt;/sequence>
*               &lt;/restriction>
*             &lt;/complexContent>
*           &lt;/complexType>
*         &lt;/element>
*         &lt;element name="Coating">
*           &lt;complexType>
*             &lt;complexContent>
*               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
*                 &lt;sequence>
*                   &lt;element name="Tests" type="{http://emekboru.com/schema/emek}Test" maxOccurs="unbounded" minOccurs="0"/>
*                   &lt;element name="Barcode" type="{http://emekboru.com/schema/emek}Barcode"/>
*                 &lt;/sequence>
*               &lt;/restriction>
*             &lt;/complexContent>
*           &lt;/complexType>
*         &lt;/element>
*         &lt;element name="Statuses" maxOccurs="unbounded" minOccurs="0">
*           &lt;complexType>
*             &lt;complexContent>
*               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
*                 &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
*                 &lt;attribute name="displayName" type="{http://www.w3.org/2001/XMLSchema}string" />
*                 &lt;attribute name="available" type="{http://www.w3.org/2001/XMLSchema}boolean" />
*                 &lt;attribute name="depleted" type="{http://www.w3.org/2001/XMLSchema}boolean" />
*                 &lt;attribute name="checked" type="{http://www.w3.org/2001/XMLSchema}boolean" />
*                 &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}integer" />
*                 &lt;attribute name="description" type="{http://www.w3.org/2001/XMLSchema}string" />
*               &lt;/restriction>
*             &lt;/complexContent>
*           &lt;/complexType>
*         &lt;/element>
*       &lt;/sequence>
*     &lt;/restriction>
*   &lt;/complexContent>
* &lt;/complexType>
* </pre>
* 
* 
*/
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Materials", propOrder = {
 "production",
 "coating",
 "statuses"
})
public class Materials {

 @XmlElement(name = "Production", required = true)
 protected Materials.Production production;
 @XmlElement(name = "Coating", required = true)
 protected Materials.Coating coating;
 @XmlElement(name = "Statuses")
 protected List<Materials.Statuses> statuses;

 
 public Statuses findStatusById(int id){
	 
	 for(Statuses s : statuses){
		 
		 if(s.getId().equals(id))
			 return s;
	 }
	 return null;
 }
 
 public Statuses findStatusByName(String name){
	 
	 for(Statuses s : statuses){
		 
		 if(s.getName().equals(name))
			 return s;
	 }
	 return null;
	 
 }
 
 public Statuses getDefaultStatus(){
	 
	 for(Statuses s : statuses){
		 if(s.isDefault)
			 return s;
	 }
	 return null;
 }
 
 public Statuses getSoldStatus(){
	 
	 for(Statuses s : statuses){
		 
		 if(s.isSold)
			 return s;
	 }
	 return null;
 }
 
 public Test findTestByGlobalId(int globalId){
	 
	 for(Test t : production.coil.tests){
		 if(t.globalId.equals(globalId))
			 return t;
	 }
	 
	 for(Test t : production.edw.tests){
		 if(t.globalId.equals(globalId))
			 return t;
	 }
	 
	 return null;
 }
 /**
  * Gets the value of the production property.
  * 
  * @return
  *     possible object is
  *     {@link Materials.Production }
  *     
  */
 public Materials.Production getProduction() {
     return production;
 }

 /**
  * Sets the value of the production property.
  * 
  * @param value
  *     allowed object is
  *     {@link Materials.Production }
  *     
  */
 public void setProduction(Materials.Production value) {
     this.production = value;
 }

 /**
  * Gets the value of the coating property.
  * 
  * @return
  *     possible object is
  *     {@link Materials.Coating }
  *     
  */
 public Materials.Coating getCoating() {
     return coating;
 }

 /**
  * Sets the value of the coating property.
  * 
  * @param value
  *     allowed object is
  *     {@link Materials.Coating }
  *     
  */
 public void setCoating(Materials.Coating value) {
     this.coating = value;
 }

 /**
  * Gets the value of the statuses property.
  * 
  * <p>
  * This accessor method returns a reference to the live list,
  * not a snapshot. Therefore any modification you make to the
  * returned list will be present inside the JAXB object.
  * This is why there is not a <CODE>set</CODE> method for the statuses property.
  * 
  * <p>
  * For example, to add a new item, do as follows:
  * <pre>
  *    getStatuses().add(newItem);
  * </pre>
  * 
  * 
  * <p>
  * Objects of the following type(s) are allowed in the list
  * {@link Materials.Statuses }
  * 
  * 
  */
 public List<Materials.Statuses> getStatuses() {
     if (statuses == null) {
         statuses = new ArrayList<Materials.Statuses>();
     }
     return this.statuses;
 }


 /**
  * <p>Java class for anonymous complex type.
  * 
  * <p>The following schema fragment specifies the expected content contained within this class.
  * 
  * <pre>
  * &lt;complexType>
  *   &lt;complexContent>
  *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
  *       &lt;sequence>
  *         &lt;element name="Tests" type="{http://emekboru.com/schema/emek}Test" maxOccurs="unbounded" minOccurs="0"/>
  *         &lt;element name="Barcode" type="{http://emekboru.com/schema/emek}Barcode"/>
  *       &lt;/sequence>
  *     &lt;/restriction>
  *   &lt;/complexContent>
  * &lt;/complexType>
  * </pre>
  * 
  * 
  */
 @XmlAccessorType(XmlAccessType.FIELD)
 @XmlType(name = "", propOrder = {
     "tests",
     "barcode"
 })
 public static class Coating {

     @XmlElement(name = "Tests")
     protected List<Test> tests;
     @XmlElement(name = "Barcode", required = true)
     protected Barcode barcode;

     /**
      * Gets the value of the tests property.
      * 
      * <p>
      * This accessor method returns a reference to the live list,
      * not a snapshot. Therefore any modification you make to the
      * returned list will be present inside the JAXB object.
      * This is why there is not a <CODE>set</CODE> method for the tests property.
      * 
      * <p>
      * For example, to add a new item, do as follows:
      * <pre>
      *    getTests().add(newItem);
      * </pre>
      * 
      * 
      * <p>
      * Objects of the following type(s) are allowed in the list
      * {@link Test }
      * 
      * 
      */
     public List<Test> getTests() {
         if (tests == null) {
             tests = new ArrayList<Test>();
         }
         return this.tests;
     }

     /**
      * Gets the value of the barcode property.
      * 
      * @return
      *     possible object is
      *     {@link Barcode }
      *     
      */
     public Barcode getBarcode() {
         return barcode;
     }

     /**
      * Sets the value of the barcode property.
      * 
      * @param value
      *     allowed object is
      *     {@link Barcode }
      *     
      */
     public void setBarcode(Barcode value) {
         this.barcode = value;
     }

 }


 /**
  * <p>Java class for anonymous complex type.
  * 
  * <p>The following schema fragment specifies the expected content contained within this class.
  * 
  * <pre>
  * &lt;complexType>
  *   &lt;complexContent>
  *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
  *       &lt;sequence>
  *         &lt;element name="Coil">
  *           &lt;complexType>
  *             &lt;complexContent>
  *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
  *                 &lt;sequence>
  *                   &lt;element name="Tests" type="{http://emekboru.com/schema/emek}Test" maxOccurs="unbounded" minOccurs="0"/>
  *                   &lt;element name="Barcode" type="{http://emekboru.com/schema/emek}Barcode"/>
  *                 &lt;/sequence>
  *               &lt;/restriction>
  *             &lt;/complexContent>
  *           &lt;/complexType>
  *         &lt;/element>
  *         &lt;element name="Edw">
  *           &lt;complexType>
  *             &lt;complexContent>
  *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
  *                 &lt;sequence>
  *                   &lt;element name="Tests" type="{http://emekboru.com/schema/emek}Test" maxOccurs="unbounded" minOccurs="0"/>
  *                   &lt;element name="Barcode" type="{http://emekboru.com/schema/emek}Barcode"/>
  *                 &lt;/sequence>
  *               &lt;/restriction>
  *             &lt;/complexContent>
  *           &lt;/complexType>
  *         &lt;/element>
  *       &lt;/sequence>
  *     &lt;/restriction>
  *   &lt;/complexContent>
  * &lt;/complexType>
  * </pre>
  * 
  * 
  */
 @XmlAccessorType(XmlAccessType.FIELD)
 @XmlType(name = "", propOrder = {
     "coil",
     "edw"
 })
 public static class Production {

     @XmlElement(name = "Coil", required = true)
     protected Materials.Production.Coil coil;
     @XmlElement(name = "Edw", required = true)
     protected Materials.Production.Edw edw;

     /**
      * Gets the value of the coil property.
      * 
      * @return
      *     possible object is
      *     {@link Materials.Production.Coil }
      *     
      */
     public Materials.Production.Coil getCoil() {
         return coil;
     }

     /**
      * Sets the value of the coil property.
      * 
      * @param value
      *     allowed object is
      *     {@link Materials.Production.Coil }
      *     
      */
     public void setCoil(Materials.Production.Coil value) {
         this.coil = value;
     }

     /**
      * Gets the value of the edw property.
      * 
      * @return
      *     possible object is
      *     {@link Materials.Production.Edw }
      *     
      */
     public Materials.Production.Edw getEdw() {
         return edw;
     }

     /**
      * Sets the value of the edw property.
      * 
      * @param value
      *     allowed object is
      *     {@link Materials.Production.Edw }
      *     
      */
     public void setEdw(Materials.Production.Edw value) {
         this.edw = value;
     }


     /**
      * <p>Java class for anonymous complex type.
      * 
      * <p>The following schema fragment specifies the expected content contained within this class.
      * 
      * <pre>
      * &lt;complexType>
      *   &lt;complexContent>
      *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
      *       &lt;sequence>
      *         &lt;element name="Tests" type="{http://emekboru.com/schema/emek}Test" maxOccurs="unbounded" minOccurs="0"/>
      *         &lt;element name="Barcode" type="{http://emekboru.com/schema/emek}Barcode"/>
      *       &lt;/sequence>
      *     &lt;/restriction>
      *   &lt;/complexContent>
      * &lt;/complexType>
      * </pre>
      * 
      * 
      */
     @XmlAccessorType(XmlAccessType.FIELD)
     @XmlType(name = "", propOrder = {
         "tests",
         "barcode"
     })
     public static class Coil {

         @XmlElement(name = "Tests")
         protected List<Test> tests;
         @XmlElement(name = "Barcode", required = true)
         protected Barcode barcode;

         
         /**
          * Gets the value of the tests property.
          * 
          * <p>
          * This accessor method returns a reference to the live list,
          * not a snapshot. Therefore any modification you make to the
          * returned list will be present inside the JAXB object.
          * This is why there is not a <CODE>set</CODE> method for the tests property.
          * 
          * <p>
          * For example, to add a new item, do as follows:
          * <pre>
          *    getTests().add(newItem);
          * </pre>
          * 
          * 
          * <p>
          * Objects of the following type(s) are allowed in the list
          * {@link Test }
          * 
          * 
          */
         public List<Test> getTests() {
             if (tests == null) {
                 tests = new ArrayList<Test>();
             }
             return this.tests;
         }

         /**
          * Gets the value of the barcode property.
          * 
          * @return
          *     possible object is
          *     {@link Barcode }
          *     
          */
         public Barcode getBarcode() {
             return barcode;
         }

         /**
          * Sets the value of the barcode property.
          * 
          * @param value
          *     allowed object is
          *     {@link Barcode }
          *     
          */
         public void setBarcode(Barcode value) {
             this.barcode = value;
         }

     }


     /**
      * <p>Java class for anonymous complex type.
      * 
      * <p>The following schema fragment specifies the expected content contained within this class.
      * 
      * <pre>
      * &lt;complexType>
      *   &lt;complexContent>
      *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
      *       &lt;sequence>
      *         &lt;element name="Tests" type="{http://emekboru.com/schema/emek}Test" maxOccurs="unbounded" minOccurs="0"/>
      *         &lt;element name="Barcode" type="{http://emekboru.com/schema/emek}Barcode"/>
      *       &lt;/sequence>
      *     &lt;/restriction>
      *   &lt;/complexContent>
      * &lt;/complexType>
      * </pre>
      * 
      * 
      */
     @XmlAccessorType(XmlAccessType.FIELD)
     @XmlType(name = "", propOrder = {
         "tests",
         "barcode"
     })
     public static class Edw {

         @XmlElement(name = "Tests")
         protected List<Test> tests;
         @XmlElement(name = "Barcode", required = true)
         protected Barcode barcode;

         /**
          * Gets the value of the tests property.
          * 
          * <p>
          * This accessor method returns a reference to the live list,
          * not a snapshot. Therefore any modification you make to the
          * returned list will be present inside the JAXB object.
          * This is why there is not a <CODE>set</CODE> method for the tests property.
          * 
          * <p>
          * For example, to add a new item, do as follows:
          * <pre>
          *    getTests().add(newItem);
          * </pre>
          * 
          * 
          * <p>
          * Objects of the following type(s) are allowed in the list
          * {@link Test }
          * 
          * 
          */
         public List<Test> getTests() {
             if (tests == null) {
                 tests = new ArrayList<Test>();
             }
             return this.tests;
         }

         /**
          * Gets the value of the barcode property.
          * 
          * @return
          *     possible object is
          *     {@link Barcode }
          *     
          */
         public Barcode getBarcode() {
             return barcode;
         }

         /**
          * Sets the value of the barcode property.
          * 
          * @param value
          *     allowed object is
          *     {@link Barcode }
          *     
          */
         public void setBarcode(Barcode value) {
             this.barcode = value;
         }

     }

 }


 /**
  * <p>Java class for anonymous complex type.
  * 
  * <p>The following schema fragment specifies the expected content contained within this class.
  * 
  * <pre>
  * &lt;complexType>
  *   &lt;complexContent>
  *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
  *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
  *       &lt;attribute name="displayName" type="{http://www.w3.org/2001/XMLSchema}string" />
  *       &lt;attribute name="available" type="{http://www.w3.org/2001/XMLSchema}boolean" />
  *       &lt;attribute name="depleted" type="{http://www.w3.org/2001/XMLSchema}boolean" />
  *       &lt;attribute name="checked" type="{http://www.w3.org/2001/XMLSchema}boolean" />
  *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}integer" />
  *       &lt;attribute name="description" type="{http://www.w3.org/2001/XMLSchema}string" />
  *     &lt;/restriction>
  *   &lt;/complexContent>
  * &lt;/complexType>
  * </pre>
  * 
  * 
  */
 @XmlAccessorType(XmlAccessType.FIELD)
 @XmlType(name = "")
 public static class Statuses {

     @XmlAttribute(name = "name")
     protected String name;
     @XmlAttribute(name = "displayName")
     protected String displayName;
     @XmlAttribute(name = "available")
     protected Boolean available;
     @XmlAttribute(name = "depleted")
     protected Boolean depleted;
     @XmlAttribute(name = "checked")
     protected Boolean checked;
     @XmlAttribute(name = "id")
     protected Integer id;
     @XmlAttribute(name = "description")
     protected String description;
     @XmlAttribute(name = "isDefault")
     protected Boolean isDefault;
     @XmlAttribute(name = "isSold")
     protected Boolean isSold;

     /**
      * Gets the value of the name property.
      * 
      * @return
      *     possible object is
      *     {@link String }
      *     
      */
     public String getName() {
         return name;
     }

     /**
      * Sets the value of the name property.
      * 
      * @param value
      *     allowed object is
      *     {@link String }
      *     
      */
     public void setName(String value) {
         this.name = value;
     }

     /**
      * Gets the value of the displayName property.
      * 
      * @return
      *     possible object is
      *     {@link String }
      *     
      */
     public String getDisplayName() {
         return displayName;
     }

     /**
      * Sets the value of the displayName property.
      * 
      * @param value
      *     allowed object is
      *     {@link String }
      *     
      */
     public void setDisplayName(String value) {
         this.displayName = value;
     }

     /**
      * Gets the value of the available property.
      * 
      * @return
      *     possible object is
      *     {@link Boolean }
      *     
      */
     public Boolean isAvailable() {
         return available;
     }

     /**
      * Sets the value of the available property.
      * 
      * @param value
      *     allowed object is
      *     {@link Boolean }
      *     
      */
     public void setAvailable(Boolean value) {
         this.available = value;
     }

     /**
      * Gets the value of the depleted property.
      * 
      * @return
      *     possible object is
      *     {@link Boolean }
      *     
      */
     public Boolean isDepleted() {
         return depleted;
     }

     /**
      * Sets the value of the depleted property.
      * 
      * @param value
      *     allowed object is
      *     {@link Boolean }
      *     
      */
     public void setDepleted(Boolean value) {
         this.depleted = value;
     }

     /**
      * Gets the value of the checked property.
      * 
      * @return
      *     possible object is
      *     {@link Boolean }
      *     
      */
     public Boolean isChecked() {
         return checked;
     }

     /**
      * Sets the value of the checked property.
      * 
      * @param value
      *     allowed object is
      *     {@link Boolean }
      *     
      */
     public void setChecked(Boolean value) {
         this.checked = value;
     }

     /**
      * Gets the value of the id property.
      * 
      * @return
      *     possible object is
      *     {@link BigInteger }
      *     
      */
     public Integer getId() {
         return id;
     }

     /**
      * Sets the value of the id property.
      * 
      * @param value
      *     allowed object is
      *     {@link BigInteger }
      *     
      */
     public void setId(Integer value) {
         this.id = value;
     }

     /**
      * Gets the value of the description property.
      * 
      * @return
      *     possible object is
      *     {@link String }
      *     
      */
     public String getDescription() {
         return description;
     }

     /**
      * Sets the value of the description property.
      * 
      * @param value
      *     allowed object is
      *     {@link String }
      *     
      */
     public void setDescription(String value) {
         this.description = value;
     }

	public Boolean isDefault() {
		return isDefault;
	}

	public void setDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public Boolean isSold() {
		return isSold;
	}

	public void setSold(Boolean isSold) {
		this.isSold = isSold;
	}

 }

}
