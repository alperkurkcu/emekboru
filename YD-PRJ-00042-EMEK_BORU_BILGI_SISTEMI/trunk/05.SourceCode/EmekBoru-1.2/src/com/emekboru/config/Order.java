
package com.emekboru.config;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Order complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Order">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Status" type="{http://emekboru.com/schema/emek}Status" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Order", propOrder = {
    "status"
})
public class Order {

    @XmlElement(name = "Status", required = true	)
    protected List<Status> status;

    
    public Status findStatus(int id){
    	
    	if(status == null || status.size() == 0)
    		return null;
    	
    	for(Status s : status){
    		
    		if(s.getId().equals(id))
    			return s;
    	}
    	return null;
    }
    
    public List<Status> findUnendedStatuses(){
    	
    	List<Status> resultList = new ArrayList<Status>(0);
    	for(Status s : status){
    		
    		if(s.isEnd == false)
    			resultList.add(s);
    	}
    	return resultList;
    }
    
    public Status findDefaultStatus(){
    	for(Status s : status){
    		
    		if(s.isDefault)
    			return s;
    	}
    	return null;
    }
    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Status }
     * 
     * 
     */
    public List<Status> getStatus() {
        if (status == null) {
            status = new ArrayList<Status>();
        }
        return this.status;
    }

}
