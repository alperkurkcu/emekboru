package com.emekboru.config;

import java.io.File;
import java.io.Serializable;
import java.net.URL;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

@Singleton
public class ConfigBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Config config;
	private File configFile;
	private String entryPoint;
	private PageUrl pageUrl;

	public ConfigBean() {

		System.getProperty("java.class.path");
		URL resourcePath = getClass().getResource("emek.xml");

		configFile = new File(resourcePath.getPath());
		try {
			JAXBContext jaxb = JAXBContext.newInstance(Config.class);
			Unmarshaller unM = jaxb.createUnmarshaller();
			config = (Config) unM.unmarshal(configFile);

		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	@PostConstruct
	public void init() {

		boolean httpStatus = Boolean.parseBoolean(config.getEntryPoint()
				.getHttp().getStatus());
		boolean httpsStatus = Boolean.parseBoolean(config.getEntryPoint()
				.getHttps().getStatus());

		if (httpStatus)
			entryPoint = config.getEntryPoint().getHttp().getValue();
		else if (httpsStatus)
			entryPoint = config.getEntryPoint().getHttps().getValue();

		else {

			String msg = "Error parsing xml configuration file!"
					+ "At least one entry point should be active!";

			throw new IllegalArgumentException(msg);
		}

		pageUrl = new PageUrl(entryPoint);
	}

	public Attribute findAttr(String key) {

		List<Attribute> list = config.getAttributes();
		for (Attribute a : list) {

			if (a.getKey().equals(key))
				return a;
		}
		return null;
	}

	public Config getConfig() {

		return config;
	}

	public void setConfig(Config config) {
		this.config = config;
	}

	public PageUrl getPageUrl() {

		return pageUrl;
	}

	public String getEntryPoint() {

		return entryPoint;
	}

	public static class PageUrl {

		public String MANAGE_YETKI_MENU_PAGE;
		public String DUST_PAGE;
		public String WIRE_PAGE;
		public String ELECTRODE_PAGE;
		public String COIL_PAGE;
		public String COATING_MATERIALS_PAGE;
		public String NEW_EMPLOYEE_PAGE;
		public String MANAGE_EMPLOYEE_PAGE;
		public String NEW_USER_PAGE;
		public String MANAGE_USER_PAGE;
		public String NEW_USERTYPE_PAGE;
		public String MANAGE_USERTYPE_PAGE;
		public String MANAGE_DEPARTMENT_PAGE;
		public String NEW_DEPARTMENT_PAGE;
		public String MANAGE_CUSTOMER_PAGE;
		public String NEW_CUSTOMER_PAGE;
		public String MAIN_PAGE;
		public String LOGIN_PAGE;

		public String KAPLAMA_KALITE_TAKIP_FORMU_PAGE;

		public String RULO_DILME_MAIN_PAGE;
		public String RULO_PIPE_LINK_EDIT_PAGE;
		public String NEW_MACHINE_PAGE;
		public String NEW_MACHINE_PART_PAGE;
		public String SPIRAL_MACHINE_PAGE;
		public String PRODUCTION_MACHINE_PAGE;
		public String COAT_MACHINE_PAGE;
		public String PIPE_PRODUCTION_REPORT;
		public String STOCK__REPORT_PAGE;
		public String TEST__REPORT_PAGE;
		public String TEST_REPORT_FOLDER;

		public String MY_CUSTOMERS_PAGE;
		public String MY_LICENSES_PAGE;
		public String CUSTOMER_MANAGEMENT_PAGE;
		public String LICENSE_MANAGEMENT_PAGE;
		public String MACHINE_MAINTENANCE_PAGE;
		public String MACHINE_KAPLAMA_ARIZA_PAGE;
		public String MACHINE_URETIM_ARIZA_PAGE;
		public String PERIODIC_MAINTENANCE_PLAN_PAGE;
		public String MACHINE_UTILS_PAGE;
		public String MANAGE_MACHINE_UTILS_PAGE;
		public String PLAN_EVENTS_PAGE;
		public String MY_EVENTS_PAGE;
		public String CRM_UTILS_PAGE;
		public String NEW_TENDER_PAGE;
		public String MANAGER_TENDER_PAGE;
		public String QUALITY_SPEC_PAGE;
		public String NEW_YETKI_MENU_PAGE;
		public String NEW_YETKI_ROL_PAGE;
		public String MANAGE_YETKI_ROL_PAGE;
		public String NEW_YETKI_MENU_KULLANICI_ERISIM;
		public String MANAGE_YETKI_MENU_KULLANICI_ERISIM;
		public String PIPE_DESTRUCTIVE_TEST;
		public String PIPE_COATING_TEST;
		public String ULKELER_PAGE;
		public String INCELEME_GRUBU_PAGE;

		public String PIPE_TAHRIBATSIZ_OTOMATIK_UT;
		public String PIPE_TAHRIBATSIZ_MANUEL_UT;
		public String PIPE_TAHRIBATSIZ_FLOROSKOPIK;
		public String PIPE_TAHRIBATSIZ_TEST;
		public String PIPE_TAHRIBATSIZ_TAMIR;
		public String PIPE_TAHRIBATSIZ_TORNA;
		public String PIPE_TAHRIBATSIZ_HIDROSTATIK;
		public String PIPE_TAHRIBATSIZ_GOZ_OLCU;
		public String PIPE_TAHRIBATSIZ_KESIM;
		public String PIPE_KAYNAK_GOZLE_PAGE;
		public String PIPE_RADYOGRAFIK_PAGE;
		public String PIPE_PENETRANT_PAGE;
		public String PIPE_MANYETIK_PARCACIK_PAGE;
		public String PIPE_OFFLINE_OTOMATIK_UT_PAGE;

		public String PIPE_LIST_PAGE;
		public String PIPE_DESTRUCTIVE_TEST_LIST_PAGE;
		public String PIPE_COATING_LIST_PAGE;
		public String PIPE_EDIT_PAGE;

		/*----SALES----*/
		public String SALES_CUSTOMER_PAGE;
		public String SALES_CRM_PAGE;
		public String SALES_EVENTS_PAGE;
		public String SALES_DOCUMENTS_PAGE;
		public String SALES_REMINDER_PAGE;
		public String SALES_PROPOSAL_PAGE;
		public String SALES_ORDER_PAGE;
		public String SALES_INVOICE_PAGE;
		public String SALES_LIST_PAGE;
		/*----END OF SALES----*/

		/*----STOK----*/
		public String STOK_MAIN_PAGE;
		public String STOK_GIRIS_CIKIS_LISTESI_PAGE;
		/*----STOK----*/

		/*----YETKI----*/
		public String YETKI_EMPLOYEE_DEPARTMENT_SUPERVISOR_PAGE;
		public String YETKI_USER_AUTH_PAGE;
		/*----YETKI----*/

		/*----SATIN ALMA----*/
		public String SATINALMA_MALZEME_TALEP_FORM_PAGE;
		public String SATINALMA_AMBAR_MALZEME_TALEP_FORM_PAGE;
		public String SATINALMA_TEKLIF_ISTEME_PAGE;
		public String SATINALMA_SATIN_ALMA_KARARI_PAGE;
		public String SATINALMA_SIPARIS_MEKTUBU_PAGE;
		public String SATINALMA_MUSTERI_PAGE;
		public String SATINALMA_SARF_MERKEZI_PAGE;
		public String SATINALMA_DOSYA_TAKIP_PAGE;
		public String SATINALMA_ACILIS_PAGE;
		public String SATINALMA_SIPARIS_BILDIRIMI_PAGE;
		public String SATINALMA_ODEME_BILDIRIMI_PAGE;
		/*----SATIN ALMA----*/

		// Is Emirleri
		public String IS_EMIRLERI_KAPLAMA_PAGE;
		public String IS_EMIRLERI_KAPLAMA_LIST_PAGE;
		public String IS_EMIRLERI_IMALAT_LIST_PAGE;
		public String IS_EMIRLERI_IMALAT_PAGE;
		// Is Emirleri

		// is takip formları
		public String IS_TAKIP_FORMU_DIS_KUMLAMA_PAGE;
		public String IS_TAKIP_FORMU_IC_KUMLAMA_PAGE;
		public String IS_TAKIP_FORMU_EPOKSI_PAGE;
		public String IS_TAKIP_FORMU_POLIETILEN_PAGE;
		public String IS_TAKIP_FORMU_BETON_PAGE;
		// is takip formları

		// Kaplama Makinaları
		public String KAPLAMA_MAKINESI_DIS_KUMLAMA_PAGE;
		public String KAPLAMA_MAKINESI_IC_KUMLAMA_PAGE;
		public String KAPLAMA_MAKINESI_EPOKSI_PAGE;
		public String KAPLAMA_MAKINESI_POLIETILEN_PAGE;
		public String KAPLAMA_MAKINESI_BETON_PAGE;
		public String KAPLAMA_MAKINESI_MUFLAMA_PAGE;
		public String KAPLAMA_MAKINESI_FIRCALAMA_PAGE;
		public String KAPLAMA_MAKINESI_ASIT_YIKAMA_PAGE;
		public String KAPLAMA_MALZEMESI_KULLANMA_PAGE;
		public String MARKALAMA_MALZEMESI_KULLANMA_PAGE;
		// Kaplama Makinaları

		// Personel
		public String PERSONEL_ORTAKGOREV_PAGE;
		public String PERSONEL_KIMLIK_PAGE;
		public String PERSONEL_KART_PAGE;
		public String EMPLOYEE_ADVANCE_REQUEST_PAGE;
		public String EMPLOYEE_ACTIVITY_PAGE;
		// Personel

		// Bak�m
		public String MAINTENANCE_SCHEDULE_PAGE;

		// Bak�m

		// Shipment
		public String NEW_SHIPMENT_PAGE;
		public String PREPARED_SHIPMENT_PAGE;
		// Shipment

		public String RELEASE_NOTE_FORM_PAGE;
		public String PARTILENDIRME_FORM_PAGE;

		public String YONETIM_PAGE;

		// Kalibrasyon
		public String KALIBRASYON_ONLINE_PAGE;
		public String KALIBRASYON_ONLINE_LAMINASYON_PAGE;
		public String KALIBRASYON_OFFLINE_PAGE;
		public String KALIBRASYON_OFFLINE_BORU_UCU_PAGE;
		public String KALIBRASYON_OFFLINE_LAMINASYON_PAGE;
		public String KALIBRASYON_MANUEL_PAGE;
		public String KALIBRASYON_FLOROSKOPIK_PAGE;
		public String KALIBRASYON_RADYOGRAFIK_PAGE;
		public String KALIBRASYON_MANYETIK_PAGE;
		public String KALIBRASYON_PENETRANT_PAGE;
		public String KALIBRASYON_MANYETIK_BORU_UCU_PAGE;

		// Kalibrasyon
		public String PLAZMA_SARF_MALZEMELERI_KULLANIM_PAGE;

		public String GUNLUK_RAPORLAR_PAGE;

		public PageUrl(String baseUrl) {

			// init(baseUrl);
			init("/EmekBoru-1.2/");
		}

		public void init(String baseUrl) {

			DUST_PAGE = baseUrl + "manmaterials/dust.jsf?faces-redirect=true";
			WIRE_PAGE = baseUrl + "manmaterials/wire.jsf?faces-redirect=true";
			ELECTRODE_PAGE = baseUrl
					+ "manmaterials/electrode.jsf?faces-redirect=true";
			COIL_PAGE = baseUrl + "manmaterials/rulo.jsf?faces-redirect=true";
			COATING_MATERIALS_PAGE = baseUrl
					+ "manmaterials/coatmaterials.jsf?faces-redirect=true";
			NEW_EMPLOYEE_PAGE = baseUrl
					+ "employee/newemployee.jsf?faces-redirect=true";
			MANAGE_EMPLOYEE_PAGE = baseUrl
					+ "employee/manageemployees.jsf?faces-redirect=true";
			NEW_USER_PAGE = baseUrl + "user/newuser.jsf?faces-redirect=true";
			MANAGE_USER_PAGE = baseUrl
					+ "user/manageuser.jsf?faces-redirect=true";
			NEW_USERTYPE_PAGE = baseUrl
					+ "user/newusertype.jsf?faces-redirect=true";
			MANAGE_USERTYPE_PAGE = baseUrl
					+ "user/manageusertype.jsf?faces-redirect=true";
			MANAGE_DEPARTMENT_PAGE = baseUrl
					+ "department/managedepartments.jsf?faces-redirect=true";
			NEW_DEPARTMENT_PAGE = baseUrl
					+ "department/newdepartment.jsf?faces-redirect=true";
			MANAGE_CUSTOMER_PAGE = baseUrl
					+ "customer/managecustomers.jsf?faces-redirect=true";
			NEW_CUSTOMER_PAGE = baseUrl
					+ "customer/newcustomer.jsf?faces-redirect=true";

			MAIN_PAGE = baseUrl + "mainpage.jsf?faces-redirect=true";
			LOGIN_PAGE = baseUrl + "login.jsf?faces-redirect=true";

			QUALITY_SPEC_PAGE = baseUrl
					+ "order/qualityspec.jsf?faces-redirect=true";

			KAPLAMA_KALITE_TAKIP_FORMU_PAGE = baseUrl
					+ "order/kaplamakalitetakipformu.jsf?faces-redirect=true";

			NEW_MACHINE_PAGE = baseUrl
					+ "machine/newmachine.jsf?faces-redirect=true";
			NEW_MACHINE_PART_PAGE = baseUrl
					+ "machine/newmachinepart.jsf?faces-redirect=true";
			SPIRAL_MACHINE_PAGE = baseUrl
					+ "machine/spiralmachine.jsf?faces-redirect=true";// boru
																		// üretim
																		// sayfası
																		// -
																		// kullanılıyor
			PRODUCTION_MACHINE_PAGE = baseUrl
					+ "machine/productionmachine.jsf?faces-redirect=true";// bilinmeyen
																			// eski
																			// sayfa
																			// -
																			// kullanılmıyor
			COAT_MACHINE_PAGE = baseUrl
					+ "machine/coatingmachine.jsf?faces-redirect=true";// bilinmeyen
																		// eski
																		// sayfa
																		// -
																		// kullanılmıyor

			PIPE_PRODUCTION_REPORT = baseUrl
					+ "orderreports/manreport.jsf?faces-redirect=true";// makine
																		// bazlı
																		// boru
																		// raporu
			STOCK__REPORT_PAGE = baseUrl
					+ "orderreports/stockreports.jsf?faces-redirect=true";// stockReportBean
																			// bulunamıyor,
																			// silinmiş
																			// -
																			// kullanılmıyor
			TEST__REPORT_PAGE = baseUrl + "reportformats/test_en.xls";// bilinmiyor
																		// -
																		// kullanılmıyor
			TEST_REPORT_FOLDER = baseUrl + "reportformats/";// bilinmiyor -
															// kullanılmıyor

			MY_CUSTOMERS_PAGE = baseUrl
					+ "crm/mycustomers.jsf?faces-redirect=true";
			CUSTOMER_MANAGEMENT_PAGE = baseUrl
					+ "crm/crmplan.jsf?faces-redirect=true";

			MY_LICENSES_PAGE = baseUrl
					+ "license/mylicenses.jsf?faces-redirect=true";
			LICENSE_MANAGEMENT_PAGE = baseUrl
					+ "license/managelicenses.jsf?faces-redirect=true";

			MACHINE_MAINTENANCE_PAGE = baseUrl
					+ "machine/machinemaintenance.jsf?faces-redirect=true";
			PERIODIC_MAINTENANCE_PLAN_PAGE = baseUrl
					+ "machine/periodicmaintenanceplan.jsf?faces-redirect=true";
			RULO_DILME_MAIN_PAGE = baseUrl
					+ "machine/rulodilmemachine.jsf?faces-redirect=true";

			MACHINE_UTILS_PAGE = baseUrl
					+ "machine/machineutils.jsf?faces-redirect=true";
			MACHINE_KAPLAMA_ARIZA_PAGE = baseUrl
					+ "machine/machineduraklamaarizakaplama.jsf?faces-redirect=true";
			MACHINE_URETIM_ARIZA_PAGE = baseUrl
					+ "machine/machineduraklamaarizauretim.jsf?faces-redirect=true";
			MANAGE_MACHINE_UTILS_PAGE = baseUrl
					+ "machine/managemachineutils.jsf?faces-redirect=true";
			PLAN_EVENTS_PAGE = baseUrl
					+ "crm/planevents.jsf?faces-redirect=true";
			MY_EVENTS_PAGE = baseUrl + "crm/myevents.jsf?faces-redirect=true";
			CRM_UTILS_PAGE = baseUrl + "utils/crm.jsf?faces-redirect=true";

			NEW_TENDER_PAGE = baseUrl + "tender/new.jsf?faces-redirect=true";
			NEW_YETKI_MENU_PAGE = baseUrl
					+ "yetki/newYetkiMenuDescription.jsf?faces-redirect=true";
			MANAGE_YETKI_MENU_PAGE = baseUrl
					+ "yetki/manageYetkiMenuDescription.jsf?faces-redirect=true";
			NEW_YETKI_ROL_PAGE = baseUrl
					+ "yetki/newYetkiRolDescription.jsf?faces-redirect=true";
			MANAGE_YETKI_ROL_PAGE = baseUrl
					+ "yetki/manageYetkiRolDescription.jsf?faces-redirect=true";
			NEW_YETKI_MENU_KULLANICI_ERISIM = baseUrl
					+ "yetki/newYetkiKullaniciErisim.jsf?faces-redirect=true";
			MANAGE_YETKI_MENU_KULLANICI_ERISIM = baseUrl
					+ "yetki/manageYetkiKullaniciErisim.jsf?faces-redirect=true";
			PIPE_DESTRUCTIVE_TEST = baseUrl
					+ "test/pipeDestructiveTest.jsf?faces-redirect=true";
			PIPE_COATING_TEST = baseUrl
					+ "test/pipeCoatingTest.jsf?faces-redirect=true";
			ULKELER_PAGE = baseUrl + "utils/ulkeler.jsf?faces-redirect=true";
			INCELEME_GRUBU_PAGE = baseUrl
					+ "utils/incelemeGrubu.jsf?faces-redirect=true";
			PIPE_TAHRIBATSIZ_TEST = baseUrl
					+ "test/tahribatsizTest.jsf?faces-redirect=true";
			PIPE_TAHRIBATSIZ_OTOMATIK_UT = baseUrl
					+ "test/otomatikUt.jsf?faces-redirect=true";
			PIPE_TAHRIBATSIZ_MANUEL_UT = baseUrl
					+ "test/manuelUt.jsf?faces-redirect=true";
			PIPE_TAHRIBATSIZ_FLOROSKOPIK = baseUrl
					+ "test/floroskopik.jsf?faces-redirect=true";
			PIPE_TAHRIBATSIZ_TAMIR = baseUrl
					+ "test/tamir.jsf?faces-redirect=true";
			PIPE_TAHRIBATSIZ_TORNA = baseUrl
					+ "test/torna.jsf?faces-redirect=true";
			PIPE_TAHRIBATSIZ_HIDROSTATIK = baseUrl
					+ "test/hidrostatik.jsf?faces-redirect=true";
			PIPE_TAHRIBATSIZ_GOZ_OLCU = baseUrl
					+ "test/gozolcu.jsf?faces-redirect=true";
			PIPE_TAHRIBATSIZ_KESIM = baseUrl
					+ "test/kesim.jsf?faces-redirect=true";
			PIPE_KAYNAK_GOZLE_PAGE = baseUrl
					+ "test/kaynakgozle.jsf?faces-redirect=true";
			PIPE_PENETRANT_PAGE = baseUrl
					+ "test/penetrant.jsf?faces-redirect=true";
			PIPE_RADYOGRAFIK_PAGE = baseUrl
					+ "test/radyografik.jsf?faces-redirect=true";
			PIPE_MANYETIK_PARCACIK_PAGE = baseUrl
					+ "test/manyetikparcacik.jsf?faces-redirect=true";
			PIPE_OFFLINE_OTOMATIK_UT_PAGE = baseUrl
					+ "test/offlineotomatikut.jsf?faces-redirect=true";
			PIPE_LIST_PAGE = baseUrl + "pipe/pipeList.jsf?faces-redirect=true";
			PIPE_DESTRUCTIVE_TEST_LIST_PAGE = baseUrl
					+ "pipe/pipeDestructionTestList.jsf?faces-redirect=true";
			RULO_PIPE_LINK_EDIT_PAGE = baseUrl
					+ "utils/boruRuloDuzenle.jsf?faces-redirect=true";
			PIPE_COATING_LIST_PAGE = baseUrl
					+ "pipe/pipeCoatingList.jsf?faces-redirect=true";

			/*----SALES----*/
			SALES_CUSTOMER_PAGE = baseUrl + "sales/customer.jsf";
			SALES_CRM_PAGE = baseUrl + "sales/crm.jsf";
			SALES_EVENTS_PAGE = baseUrl + "sales/events.jsf";
			SALES_DOCUMENTS_PAGE = baseUrl + "sales/documents.jsf";
			SALES_REMINDER_PAGE = baseUrl + "sales/reminder.jsf";
			SALES_PROPOSAL_PAGE = baseUrl + "sales/proposal.jsf";
			SALES_ORDER_PAGE = baseUrl + "sales/order.jsf";
			SALES_INVOICE_PAGE = baseUrl + "sales/invoice.jsf";
			SALES_LIST_PAGE = baseUrl + "sales/salesList.jsf";
			/*----END OF SALES----*/

			/*----STOK----*/
			STOK_MAIN_PAGE = baseUrl + "stok/stok.jsf?faces-redirect=true";
			STOK_GIRIS_CIKIS_LISTESI_PAGE = baseUrl
					+ "stok/stokgiriscikislistesi.jsf?faces-redirect=true";
			/*----STOK----*/

			/*----YETKI----*/
			YETKI_EMPLOYEE_DEPARTMENT_SUPERVISOR_PAGE = baseUrl
					+ "employee/employeedepartmentsupervisor.jsf?faces-redirect=true";
			YETKI_USER_AUTH_PAGE = baseUrl
					+ "user/userauthorization.jsf?faces-redirect=true";
			/*----YETKI----*/

			/*--IS EMIRLERI--*/
			IS_EMIRLERI_KAPLAMA_PAGE = baseUrl
					+ "isemirleri/kaplamaisemirleri.jsf?faces-redirect=true";
			IS_EMIRLERI_KAPLAMA_LIST_PAGE = baseUrl
					+ "isemirleri/kaplamaisemirlerilist.jsf?faces-redirect=true";
			IS_EMIRLERI_IMALAT_PAGE = baseUrl
					+ "isemirleri/imalatisemirleri.jsf?faces-redirect=true";
			IS_EMIRLERI_IMALAT_LIST_PAGE = baseUrl
					+ "isemirleri/imalatisemirlerilist.jsf?faces-redirect=true";
			/*--IS EMIRLERI--*/

			/*--IS TAKİP FORMLARI--*/
			IS_TAKIP_FORMU_DIS_KUMLAMA_PAGE = baseUrl
					+ "istakipformu/istakipformudiskumlama.jsf?faces-redirect=true";
			IS_TAKIP_FORMU_IC_KUMLAMA_PAGE = baseUrl
					+ "istakipformu/istakipformuickumlama.jsf?faces-redirect=true";
			IS_TAKIP_FORMU_EPOKSI_PAGE = baseUrl
					+ "istakipformu/istakipformuepoksi.jsf?faces-redirect=true";
			IS_TAKIP_FORMU_POLIETILEN_PAGE = baseUrl
					+ "istakipformu/istakipformupolietilen.jsf?faces-redirect=true";
			IS_TAKIP_FORMU_BETON_PAGE = baseUrl
					+ "istakipformu/istakipformubeton.jsf?faces-redirect=true";
			/*--IS TAKİP FORMLARI--*/

			/*--KAPLAMA MAKINELERI--*/
			KAPLAMA_MAKINESI_DIS_KUMLAMA_PAGE = baseUrl
					+ "machine/kaplamadiskumlama.jsf?faces-redirect=true";
			KAPLAMA_MAKINESI_IC_KUMLAMA_PAGE = baseUrl
					+ "machine/kaplamaickumlama.jsf?faces-redirect=true";
			KAPLAMA_MAKINESI_EPOKSI_PAGE = baseUrl
					+ "machine/kaplamaepoksi.jsf?faces-redirect=true";
			KAPLAMA_MAKINESI_POLIETILEN_PAGE = baseUrl
					+ "machine/kaplamapolietilen.jsf?faces-redirect=true";
			KAPLAMA_MAKINESI_BETON_PAGE = baseUrl
					+ "machine/kaplamabeton.jsf?faces-redirect=true";
			KAPLAMA_MAKINESI_MUFLAMA_PAGE = baseUrl
					+ "machine/kaplamamuflama.jsf?faces-redirect=true";
			KAPLAMA_MAKINESI_FIRCALAMA_PAGE = baseUrl
					+ "machine/kaplamafircalama.jsf?faces-redirect=true";
			KAPLAMA_MAKINESI_ASIT_YIKAMA_PAGE = baseUrl
					+ "machine/kaplamaasityikama.jsf?faces-redirect=true";
			KAPLAMA_MALZEMESI_KULLANMA_PAGE = baseUrl
					+ "machine/kaplamamalzemesikullanma.jsf?faces-redirect=true";
			MARKALAMA_MALZEMESI_KULLANMA_PAGE = baseUrl
					+ "machine/markalamaMakinesi.jsf?faces-redirect=true";
			/*--KAPLAMA MAKINELERI--*/

			/*----SATIN ALMA----*/
			SATINALMA_MALZEME_TALEP_FORM_PAGE = baseUrl
					+ "satinalma/malzemetalepformu.jsf?faces-redirect=true";
			SATINALMA_AMBAR_MALZEME_TALEP_FORM_PAGE = baseUrl
					+ "satinalma/ambarmalzemetalepformu.jsf?faces-redirect=true";
			SATINALMA_TEKLIF_ISTEME_PAGE = baseUrl
					+ "satinalma/teklifisteme.jsf?faces-redirect=true";
			SATINALMA_SATIN_ALMA_KARARI_PAGE = baseUrl
					+ "satinalma/satinalmakarari.jsf?faces-redirect=true";
			SATINALMA_SIPARIS_MEKTUBU_PAGE = baseUrl
					+ "satinalma/siparismektubu.jsf?faces-redirect=true";
			SATINALMA_MUSTERI_PAGE = baseUrl
					+ "satinalma/musteri.jsf?faces-redirect=true";
			SATINALMA_SARF_MERKEZI_PAGE = baseUrl
					+ "satinalma/sarfmerkezi.jsf?faces-redirect=true";
			SATINALMA_DOSYA_TAKIP_PAGE = baseUrl
					+ "satinalma/dosyaTakip.jsf?faces-redirect=true";
			SATINALMA_ACILIS_PAGE = baseUrl
					+ "satinalma/satinalma.jsf?faces-redirect=true";
			SATINALMA_SIPARIS_BILDIRIMI_PAGE = baseUrl
					+ "satinalma/siparisbildirimi.jsf?faces-redirect=true";
			SATINALMA_ODEME_BILDIRIMI_PAGE = baseUrl
					+ "satinalma/odemebildirimi.jsf?faces-redirect=true";
			/*----SATIN ALMA SONU----*/

			// Personel
			PERSONEL_ORTAKGOREV_PAGE = baseUrl
					+ "personel/personelOrtakGorev.jsf?faces-redirect=true";
			PERSONEL_KIMLIK_PAGE = baseUrl
					+ "personel/personelKimlik.jsf?faces-redirect=true";
			PERSONEL_KART_PAGE = baseUrl
					+ "personel/personelKart.jsf?faces-redirect=true";
			EMPLOYEE_ADVANCE_REQUEST_PAGE = baseUrl
					+ "employee/advancerequests.jsf?faces-redirect=true";
			EMPLOYEE_ACTIVITY_PAGE = baseUrl
					+ "employee/employeeactivities.jsf?faces-redirect=true";
			// Personel

			// Bak�m

			MAINTENANCE_SCHEDULE_PAGE = baseUrl
					+ "maintenance/maintenanceschedule.jsf?faces-redirect=true";
			// Bak�m

			// Shipment
			NEW_SHIPMENT_PAGE = baseUrl
					+ "shipment/newshipment.jsf?faces-redirect=true";
			PREPARED_SHIPMENT_PAGE = baseUrl
					+ "shipment/preparedshipment.jsf?faces-redirect=true";
			// Shipment

			YONETIM_PAGE = baseUrl
					+ "utils/yonetimSayfasi.jsf?faces-redirect=true";

			RELEASE_NOTE_FORM_PAGE = baseUrl
					+ "reportformats/releaseNote/releasenotesayfasi.jsf?faces-redirect=true";
			PARTILENDIRME_FORM_PAGE = baseUrl
					+ "reportformats/releaseNote/partilendirme.jsf?faces-redirect=true";

			// Kalibrasyon
			KALIBRASYON_ONLINE_PAGE = baseUrl
					+ "kalibrasyon/kalibrasyononline.jsf?faces-redirect=true";
			KALIBRASYON_ONLINE_LAMINASYON_PAGE = baseUrl
					+ "kalibrasyon/kalibrasyononlinelaminasyon.jsf?faces-redirect=true";
			KALIBRASYON_OFFLINE_PAGE = baseUrl
					+ "kalibrasyon/kalibrasyonoffline.jsf?faces-redirect=true";
			KALIBRASYON_OFFLINE_LAMINASYON_PAGE = baseUrl
					+ "kalibrasyon/kalibrasyonofflinelaminasyon.jsf?faces-redirect=true";
			KALIBRASYON_OFFLINE_BORU_UCU_PAGE = baseUrl
					+ "kalibrasyon/kalibrasyonofflineboruucu.jsf?faces-redirect=true";
			KALIBRASYON_MANUEL_PAGE = baseUrl
					+ "kalibrasyon/kalibrasyonmanuel.jsf?faces-redirect=true";
			KALIBRASYON_FLOROSKOPIK_PAGE = baseUrl
					+ "kalibrasyon/kalibrasyonfloroskopik.jsf?faces-redirect=true";
			KALIBRASYON_RADYOGRAFIK_PAGE = baseUrl
					+ "kalibrasyon/kalibrasyonradyografik.jsf?faces-redirect=true";
			KALIBRASYON_MANYETIK_PAGE = baseUrl
					+ "kalibrasyon/kalibrasyonmanyetik.jsf?faces-redirect=true";
			KALIBRASYON_PENETRANT_PAGE = baseUrl
					+ "kalibrasyon/kalibrasyonpenetrant.jsf?faces-redirect=true";
			KALIBRASYON_MANYETIK_BORU_UCU_PAGE = baseUrl
					+ "kalibrasyon/kalibrasyonmanyetikboruucu.jsf?faces-redirect=true";

			PLAZMA_SARF_MALZEMELERI_KULLANIM_PAGE = baseUrl
					+ "machine/plazmasarfmalzemeleri.jsf?faces-redirect=true";

			GUNLUK_RAPORLAR_PAGE = baseUrl
					+ "reportformats/gunlukraporlar.jsf?faces-redirect=true";

			PIPE_EDIT_PAGE = baseUrl + "pipe/pipeEdit.jsf?faces-redirect=true";
		}
	}
}
