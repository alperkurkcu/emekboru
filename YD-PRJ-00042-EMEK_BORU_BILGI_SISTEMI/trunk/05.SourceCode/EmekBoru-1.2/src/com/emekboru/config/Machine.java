package com.emekboru.config;

import java.math.BigInteger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for Machine complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="Machine">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Manufacturing">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Spiral">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;attribute name="coilNumber" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *                           &lt;attribute name="electrodeNumber" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *                           &lt;attribute name="wireNumber" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Coating" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Machine", propOrder = { "manufacturing", "coating" })
public class Machine {

	@XmlElement(name = "Manufacturing", required = true)
	protected Machine.Manufacturing manufacturing;
	@XmlElement(name = "Coating", required = true)
	protected Object coating;

	/**
	 * Gets the value of the manufacturing property.
	 * 
	 * @return possible object is {@link Machine.Manufacturing }
	 * 
	 */
	public Machine.Manufacturing getManufacturing() {
		return manufacturing;
	}

	/**
	 * Sets the value of the manufacturing property.
	 * 
	 * @param value
	 *            allowed object is {@link Machine.Manufacturing }
	 * 
	 */
	public void setManufacturing(Machine.Manufacturing value) {
		this.manufacturing = value;
	}

	/**
	 * Gets the value of the coating property.
	 * 
	 * @return possible object is {@link Object }
	 * 
	 */
	public Object getCoating() {
		return coating;
	}

	/**
	 * Sets the value of the coating property.
	 * 
	 * @param value
	 *            allowed object is {@link Object }
	 * 
	 */
	public void setCoating(Object value) {
		this.coating = value;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="Spiral">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;attribute name="coilNumber" type="{http://www.w3.org/2001/XMLSchema}integer" />
	 *                 &lt;attribute name="electrodeNumber" type="{http://www.w3.org/2001/XMLSchema}integer" />
	 *                 &lt;attribute name="wireNumber" type="{http://www.w3.org/2001/XMLSchema}integer" />
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "spiral" })
	public static class Manufacturing {

		@XmlElement(name = "Spiral", required = true)
		protected Machine.Manufacturing.Spiral spiral;

		/**
		 * Gets the value of the spiral property.
		 * 
		 * @return possible object is {@link Machine.Manufacturing.Spiral }
		 * 
		 */
		public Machine.Manufacturing.Spiral getSpiral() {
			return spiral;
		}

		/**
		 * Sets the value of the spiral property.
		 * 
		 * @param value
		 *            allowed object is {@link Machine.Manufacturing.Spiral }
		 * 
		 */
		public void setSpiral(Machine.Manufacturing.Spiral value) {
			this.spiral = value;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;attribute name="ruloNumber" type="{http://www.w3.org/2001/XMLSchema}integer" />
		 *       &lt;attribute name="electrodeNumber" type="{http://www.w3.org/2001/XMLSchema}integer" />
		 *       &lt;attribute name="wireNumber" type="{http://www.w3.org/2001/XMLSchema}integer" />
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "")
		public static class Spiral {
			/*
			 * @XmlAttribute(name = "coilNumber") protected Integer coilNumber;
			 */
			@XmlAttribute(name = "ruloNumber")
			protected Integer ruloNumber;
			@XmlAttribute(name = "electrodeNumber")
			protected Integer electrodeNumber;
			@XmlAttribute(name = "wireNumber")
			protected Integer wireNumber;
			@XmlAttribute(name = "dustNumber")
			protected Integer dustNumber;
			@XmlAttribute(name = "maxWireIcNumber")
			protected Integer maxWireIcNumber;
			@XmlAttribute(name = "maxDustIcNumber")
			protected Integer maxDustIcNumber;
			@XmlAttribute(name = "maxWireDisNumber")
			protected Integer maxWireDisNumber;
			@XmlAttribute(name = "maxDustDisNumber")
			protected Integer maxDustDisNumber;

			/**
			 * Gets the value of the coilNumber property.
			 * 
			 * @return possible object is {@link BigInteger }
			 * 
			 */
			/*
			 * public Integer getCoilNumber() { return coilNumber; }
			 */
			/**
			 * Gets the value of the ruloNumber property.
			 * 
			 * @return possible object is {@link BigInteger }
			 * 
			 */
			public Integer getRuloNumber() {
				return ruloNumber;
			}

			/**
			 * Sets the value of the coilNumber property.
			 * 
			 * @param value
			 *            allowed object is {@link BigInteger }
			 * 
			 */
			/*
			 * public void setCoilNumber(Integer value) { this.coilNumber =
			 * value; }
			 */

			public void setRuloNumber(Integer value) {
				this.ruloNumber = value;
			}

			/**
			 * Gets the value of the electrodeNumber property.
			 * 
			 * @return possible object is {@link BigInteger }
			 * 
			 */
			public Integer getElectrodeNumber() {
				return electrodeNumber;
			}

			/**
			 * Sets the value of the electrodeNumber property.
			 * 
			 * @param value
			 *            allowed object is {@link BigInteger }
			 * 
			 */
			public void setElectrodeNumber(Integer value) {
				this.electrodeNumber = value;
			}

			/**
			 * Gets the value of the wireNumber property.
			 * 
			 * @return possible object is {@link BigInteger }
			 * 
			 */
			public Integer getWireNumber() {
				return wireNumber;
			}

			/**
			 * Sets the value of the wireNumber property.
			 * 
			 * @param value
			 *            allowed object is {@link BigInteger }
			 * 
			 */
			public void setWireNumber(Integer value) {
				this.wireNumber = value;
			}

			/**
			 * Gets the value of the wireNumber property.
			 * 
			 * @return possible object is {@link BigInteger }
			 * 
			 */
			public Integer getDustNumber() {
				return dustNumber;
			}

			/**
			 * Sets the value of the wireNumber property.
			 * 
			 * @param value
			 *            allowed object is {@link BigInteger }
			 * 
			 */
			public void setDustNumber(Integer value) {
				this.dustNumber = value;
			}

			/**
			 * @return the maxWireIcNumber
			 */
			public Integer getMaxWireIcNumber() {
				return maxWireIcNumber;
			}

			/**
			 * @param maxWireIcNumber
			 *            the maxWireIcNumber to set
			 */
			public void setMaxWireIcNumber(Integer maxWireIcNumber) {
				this.maxWireIcNumber = maxWireIcNumber;
			}

			/**
			 * @return the maxDustIcNumber
			 */
			public Integer getMaxDustIcNumber() {
				return maxDustIcNumber;
			}

			/**
			 * @param maxDustIcNumber
			 *            the maxDustIcNumber to set
			 */
			public void setMaxDustIcNumber(Integer maxDustIcNumber) {
				this.maxDustIcNumber = maxDustIcNumber;
			}

			/**
			 * @return the maxWireDisNumber
			 */
			public Integer getMaxWireDisNumber() {
				return maxWireDisNumber;
			}

			/**
			 * @param maxWireDisNumber
			 *            the maxWireDisNumber to set
			 */
			public void setMaxWireDisNumber(Integer maxWireDisNumber) {
				this.maxWireDisNumber = maxWireDisNumber;
			}

			/**
			 * @return the maxDustDisNumber
			 */
			public Integer getMaxDustDisNumber() {
				return maxDustDisNumber;
			}

			/**
			 * @param maxDustDisNumber
			 *            the maxDustDisNumber to set
			 */
			public void setMaxDustDisNumber(Integer maxDustDisNumber) {
				this.maxDustDisNumber = maxDustDisNumber;
			}

		}

	}

}
